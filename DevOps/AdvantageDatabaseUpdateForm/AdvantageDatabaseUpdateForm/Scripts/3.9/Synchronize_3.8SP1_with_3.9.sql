-- =========================================================================================================
--Synchronize_3.8SP1_with_3.9.sql
-- =========================================================================================================
/*                            Ver 007       2017-08-18
Run this script on:

 
to synchronize it with:

        3.8SP1  with  3.9 (TFS)

You are recommended to back up your database before running this script


*/
SET NUMERIC_ROUNDABORT OFF;
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON;
GO
SET XACT_ABORT ON;
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;
GO
BEGIN TRANSACTION;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Drop constraints DF_syKlassAppConfigurationSetting_KlassAppIdentification, if exists';
GO
IF EXISTS (
              SELECT 1
              FROM   sys.columns
              WHERE  name = N'KlassAppId'
                     AND object_id = OBJECT_ID(N'[dbo].[syKlassAppConfigurationSetting]', 'U')
                     AND default_object_id = OBJECT_ID(N'[dbo].[DF_syKlassAppConfigurationSetting_KlassAppIdentification]', 'D')
          )
	BEGIN
		PRINT N'    Dropping contrantint DF_syKlassAppConfigurationSetting_KlassAppIdentification';
		ALTER TABLE dbo.syKlassAppConfigurationSetting
		DROP CONSTRAINT DF_syKlassAppConfigurationSetting_KlassAppIdentification;
	END
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping trigger [dbo].[adLeads_Audit_Delete] from [dbo].[adLeads]';
GO
IF OBJECT_ID(N'[dbo].[adLeads_Audit_Delete]', 'TR') IS NOT NULL
    DROP TRIGGER dbo.adLeads_Audit_Delete;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping [dbo].[USP_KlassAppMissingStudentRequirementStudentNumberEmail]';
GO
IF OBJECT_ID(N'[dbo].[USP_KlassAppMissingStudentRequirementStudentNumberEmail]', 'P') IS NOT NULL
    DROP PROCEDURE dbo.USP_KlassAppMissingStudentRequirementStudentNumberEmail;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping [dbo].[USP_KlassAppMissingStudentRequirementDuplicateEmail]';
GO
IF OBJECT_ID(N'[dbo].[USP_KlassAppMissingStudentRequirementDuplicateEmail]', 'P') IS NOT NULL
    DROP PROCEDURE dbo.USP_KlassAppMissingStudentRequirementDuplicateEmail;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping [dbo].[USP_GetPrgVersionsByCampusId]';
GO
IF OBJECT_ID(N'[dbo].[USP_GetPrgVersionsByCampusId]', 'P') IS NOT NULL
    DROP PROCEDURE dbo.USP_GetPrgVersionsByCampusId;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping [dbo].[USP_EnrollLead]';
GO
IF OBJECT_ID(N'[dbo].[USP_EnrollLead]', 'P') IS NOT NULL
    DROP PROCEDURE dbo.USP_EnrollLead;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Dropping [dbo].[USP_ListMissingEnrollmentTestRequirements]';
GO
IF OBJECT_ID(N'[dbo].[USP_ListMissingEnrollmentTestRequirements]', 'P') IS NOT NULL
    DROP PROCEDURE dbo.USP_ListMissingEnrollmentTestRequirements;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Refreshing [dbo].[arStudent]';
GO
IF OBJECT_ID(N'[dbo].[arStudent]', 'V') IS NOT NULL
    EXEC sp_refreshview N'[dbo].[arStudent]';
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'If Exists sp USP_KlassAppMissingStudentRequirementDuplicateEmail, delete it '
GO
IF EXISTS ( SELECT  1 
            FROM    sys.objects 
            WHERE   object_id = OBJECT_ID(N'[dbo].[USP_KlassAppMissingStudentRequirementDuplicateEmail]') 
                    AND type IN ( N'P',N'PC' ) ) 
    BEGIN 
		PRINT N'    Dropping SP USP_KlassAppMissingStudentRequirementDuplicateEmail'; 
        DROP PROCEDURE USP_KlassAppMissingStudentRequirementDuplicateEmail; 
    END; 
GO 
IF @@ERROR <> 0 
    SET NOEXEC ON; 
GO 
PRINT N'Creating USP_KlassAppMissingStudentRequirementDuplicateEmail'; 
GO 
--=================================================================================================
-- USP_KlassAppMissingStudentRequirementDuplicateEmail
--=================================================================================================
CREATE PROCEDURE dbo.USP_KlassAppMissingStudentRequirementDuplicateEmail
AS
    BEGIN
        DECLARE @Lead TABLE
            (
                FirstName VARCHAR(50) NOT NULL
               ,LastName VARCHAR(50) NOT NULL
               ,LeadId UNIQUEIDENTIFIER NOT NULL
               ,StudentId UNIQUEIDENTIFIER NOT NULL
               ,StudentNumber VARCHAR(50) NULL
               ,Email VARCHAR(100) NOT NULL
            );

        INSERT INTO @Lead (
                              FirstName
                             ,LastName
                             ,LeadId
                             ,StudentId
                             ,Email
                             ,StudentNumber
                          )
                    SELECT FirstName
                          ,LastName
                          ,l.LeadId
                          ,StudentId
                          ,EMail
                          ,l.StudentNumber
                    FROM   dbo.adLeads l
                    JOIN   dbo.AdLeadEmail em ON em.LeadId = l.LeadId
                    WHERE  StudentId <> '00000000-0000-0000-0000-000000000000';

        SELECT   FirstName
                ,LastName
                ,StudentNumber
                ,Email
        FROM     @Lead
        WHERE    Email IN (
                              SELECT mail
                              FROM   (
                                         SELECT   l2.Email AS mail
                                                 ,COUNT(*) AS Co
                                         FROM     @Lead l1
                                         JOIN     @Lead l2 ON l1.Email = l2.Email
                                                              AND l1.LeadId <> l2.LeadId
                                         GROUP BY l2.Email
                                         HAVING   COUNT(*) > 1
                                     ) AS T
                          )
        ORDER BY Email
                ,FirstName;

    END;
--=================================================================================================
-- END  --  USP_KlassAppMissingStudentRequirementDuplicateEmail
--=================================================================================================
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'If Exists sp USP_KlassAppMissingStudentRequirementStudentNumberEmail, delete it '
GO
IF EXISTS ( SELECT  1 
            FROM    sys.objects 
            WHERE   object_id = OBJECT_ID(N'[dbo].[USP_KlassAppMissingStudentRequirementStudentNumberEmail]') 
                    AND type IN ( N'P',N'PC' ) ) 
    BEGIN 
		PRINT N'    Dropping SP USP_KlassAppMissingStudentRequirementStudentNumberEmail'; 
        DROP PROCEDURE USP_KlassAppMissingStudentRequirementStudentNumberEmail; 
    END; 
GO 
IF @@ERROR <> 0 
    SET NOEXEC ON; 
GO 
PRINT N'Creating USP_KlassAppMissingStudentRequirementStudentNumberEmail'; 
GO 
-- =============================================
-- Author:		JAGG
-- Create date: 6/29/2017
-- Description:	
-- =============================================
CREATE PROCEDURE dbo.USP_KlassAppMissingStudentRequirementStudentNumberEmail
AS
    BEGIN
        -- SET NOCOUNT ON added to prevent extra result sets from
        -- interfering with SELECT statements.
        SET NOCOUNT ON;

        -- Insert statements for procedure here
        SELECT   DISTINCT l.FirstName
                ,LastName
                ,l.StudentNumber
                ,em.EMail
        FROM     dbo.adLeads l
        LEFT JOIN AdLeadEmail em ON em.LeadId = l.LeadId
        WHERE    l.StudentId <> '00000000-0000-0000-0000-000000000000'
                 AND (
                     l.StudentNumber IS NULL
                     OR l.StudentNumber = ''
                     OR em.EMail IS NULL
                     OR em.EMail = ''
                     )
        ORDER BY LastName
                ,FirstName;
    END;
--=================================================================================================
-- END  --  USP_KlassAppMissingStudentRequirementStudentNumberEmail
--=================================================================================================
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'If Exists sp USP_KlassAppMissingStudentLeadPotentialDuplicateEmail, delete it '
GO
IF EXISTS ( SELECT  1 
            FROM    sys.objects 
            WHERE   object_id = OBJECT_ID(N'[dbo].[USP_KlassAppMissingStudentLeadPotentialDuplicateEmail]') 
                    AND type IN ( N'P',N'PC' ) ) 
    BEGIN 
		PRINT N'    Dropping SP USP_KlassAppMissingStudentLeadPotentialDuplicateEmail'; 
        DROP PROCEDURE USP_KlassAppMissingStudentLeadPotentialDuplicateEmail; 
    END; 
GO 
IF @@ERROR <> 0 
    SET NOEXEC ON; 
GO 
PRINT N'Creating USP_KlassAppMissingStudentLeadPotentialDuplicateEmail'; 
GO 
--==========================================================================================
-- USP_KlassAppMissingStudentLeadPotentialDuplicateEmail
--==========================================================================================
CREATE PROCEDURE dbo.USP_KlassAppMissingStudentLeadPotentialDuplicateEmail
AS
    BEGIN
        DECLARE @Lead TABLE
            (
                FirstName VARCHAR(50) NOT NULL
               ,LastName VARCHAR(50) NOT NULL
               ,LeadId UNIQUEIDENTIFIER NOT NULL
               ,StudentId UNIQUEIDENTIFIER NOT NULL
               ,StudentNumber VARCHAR(50) NULL
               ,Email VARCHAR(100) NOT NULL
            );

        INSERT INTO @Lead (
                              FirstName
                             ,LastName
                             ,LeadId
                             ,StudentId
                             ,Email
                             ,StudentNumber
                          )
                    SELECT FirstName
                          ,LastName
                          ,l.LeadId
                          ,StudentId
                          ,EMail
                          ,l.StudentNumber
                    FROM   dbo.adLeads l
                    JOIN   dbo.AdLeadEmail em ON em.LeadId = l.LeadId;

        SELECT   FirstName
                ,LastName
                ,StudentNumber
                ,CASE WHEN StudentId <> '00000000-0000-0000-0000-000000000000' THEN 'STUDENT'
                      ELSE 'LEAD'
                 END AS Status
                ,Email
        FROM     @Lead
        WHERE    Email IN (
                              SELECT mail
                              FROM   (
                                         SELECT   l2.Email AS mail
                                                 ,COUNT(*) AS Co
                                         FROM     @Lead l1
                                         JOIN     @Lead l2 ON l1.Email = l2.Email
                                                              AND l1.LeadId <> l2.LeadId
                                         GROUP BY l2.Email
                                         HAVING   COUNT(*) > 1
                                     ) AS T
                          )
        ORDER BY Email
                ,FirstName;

    END;
--==========================================================================================
-- END  --  USP_KlassAppMissingStudentLeadPotentialDuplicateEmail
--==========================================================================================
GO
PRINT N'Refreshing [dbo].[arStudentPhone]';
GO
IF OBJECT_ID(N'[dbo].[arStudentPhone]', 'V') IS NOT NULL
    EXEC sp_refreshview N'[dbo].[arStudentPhone]';
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Refreshing [dbo].[arStudAddresses]';
GO
IF OBJECT_ID(N'[dbo].[arStudAddresses]', 'V') IS NOT NULL
    EXEC sp_refreshview N'[dbo].[arStudAddresses]';
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Refreshing [dbo].[GlTransView1]';
GO
IF OBJECT_ID(N'[dbo].[GlTransView1]', 'V') IS NOT NULL
    EXEC sp_refreshview N'[dbo].[GlTransView1]';
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Refreshing [dbo].[GlTransView2]';
GO
IF OBJECT_ID(N'[dbo].[GlTransView2]', 'V') IS NOT NULL
    EXEC sp_refreshview N'[dbo].[GlTransView2]';
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Refreshing [dbo].[syStudentNotes]';
GO
IF OBJECT_ID(N'[dbo].[syStudentNotes]', 'V') IS NOT NULL
    EXEC sp_refreshview N'[dbo].[syStudentNotes]';
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Refreshing [dbo].[syStudentContacts]';
GO
IF OBJECT_ID(N'[dbo].[syStudentContacts]', 'V') IS NOT NULL
    EXEC sp_refreshview N'[dbo].[syStudentContacts]';
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Refreshing [dbo].[syStudentContactPhones]';
GO
IF OBJECT_ID(N'[dbo].[syStudentContactPhones]', 'V') IS NOT NULL
    EXEC sp_refreshview N'[dbo].[syStudentContactPhones]';
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'If Exists sp USP_ListMissingEnrollmentTestRequirements, delete it ';
GO
IF EXISTS (
          SELECT 1
          FROM   sys.objects
          WHERE  object_id = OBJECT_ID(N'[dbo].[USP_ListMissingEnrollmentTestRequirements]')
                 AND type IN ( N'P', N'PC' )
          )
    BEGIN
        PRINT N'    Dropping SP USP_ListMissingEnrollmentTestRequirements';
        DROP PROCEDURE USP_ListMissingEnrollmentTestRequirements;
    END;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating USP_ListMissingEnrollmentTestRequirements';
GO
CREATE PROCEDURE dbo.USP_ListMissingEnrollmentTestRequirements
    (
        @StuEnrollID UNIQUEIDENTIFIER
    )
AS 
/*----------------------------------------------------------------------------------------------------  
 Author          : Saraswathi Lakshmanan  
      
    Create date  : 28/09/2010  
      
 Procedure Name : USP_ListMissingEnrollmentTestRequirements  
  
 Objective  : Get the count of test not approved for Enrollment  
    
 Parameters  : Name   Type Data Type   Required?    
      =====   ==== =========   =========   
      @StuEnrollId IN  Uniqueuidentifier Yes  
       
   
 Output   : Returns the requested details   
        
*/
    -----------------------------------------------------------------------------------------------------  
    BEGIN


        --Declare @StuEnrollId as UniqueIdentifier  
        --Declare @CampusID as UniqueIdentifier  

        --Set @StuEnrollId='60DB3379-3DAD-4443-ABC2-F9E211C9AAB8'  
        --Set @CampusID='2CD3849A-DECD-48C5-8C9E-3FBE11351437'  
        DECLARE @StudentId AS UNIQUEIDENTIFIER;
        DECLARE @PrgVerId AS UNIQUEIDENTIFIER;
        DECLARE @CampusID AS UNIQUEIDENTIFIER;
        DECLARE @LeadID AS UNIQUEIDENTIFIER;
        DECLARE @StudentStartDate AS DATETIME;

        SET @StudentId = (
                         SELECT StudentId
                         FROM   arStuEnrollments
                         WHERE  StuEnrollId = @StuEnrollID
                         );
        SET @PrgVerId = (
                        SELECT PrgVerId
                        FROM   arStuEnrollments
                        WHERE  StuEnrollId = @StuEnrollID
                        );
        SET @CampusID = (
                        SELECT CampusId
                        FROM   arStuEnrollments
                        WHERE  StuEnrollId = @StuEnrollID
                        );
        SET @StudentStartDate = (
                                SELECT StartDate
                                FROM   arStuEnrollments
                                WHERE  StuEnrollId = @StuEnrollID
                                );
        SET @LeadID = (
                      SELECT LeadId
                      FROM   arStuEnrollments
                      WHERE  StuEnrollId = @StuEnrollID
                      );
        DECLARE @ActiveStatusID AS UNIQUEIDENTIFIER;
        SET @ActiveStatusID = (
                              SELECT StatusId
                              FROM   syStatuses
                              WHERE  Status = 'Active'
                              );



        SELECT adReqId
              ,Descrip
        FROM   (
               SELECT DISTINCT adReqId
                     ,Descrip
                     ,ReqGrpId
                     ,StartDate
                     ,EndDate
                     ,ActualScore
                     ,TestTaken
                     ,Comments
                     ,CASE WHEN override >= 1 THEN 'True'
                           ELSE 'False'
                      END AS OverRide
                     ,CASE WHEN (
                                SELECT COUNT(*)
                                FROM   adPrgVerTestDetails
                                WHERE  adReqId = R2.adReqId
                                       AND PrgVerId = @PrgVerId
                                ) >= 1 THEN (
                                            SELECT MinScore
                                            FROM   adPrgVerTestDetails
                                            WHERE  adReqId = R2.adReqId
                                                   AND PrgVerId = @PrgVerId
                                            )
                           ELSE MinScore
                      END AS MinScore
                     ,Required
                     ,DocSubmittedCount
                     ,DocStatusDescrip
                     ,CASE WHEN (
                                TestTaken IS NOT NULL
                                AND ActualScore >= 1
                                ) THEN 1
                           ELSE 0
                      END AS TestTakenCount
                     ,Pass
                     ,CampGrpId
               FROM   (
                      SELECT DISTINCT adReqId
                            ,Descrip
                            ,ReqGrpId
                            ,StartDate
                            ,EndDate
                            ,ActualScore
                            ,TestTaken
                            ,Comments
                            ,override
                            ,MinScore
                            ,Required
                            ,CASE WHEN ActualScore >= MinScore THEN 'True'
                                  ELSE 'False'
                             END AS Pass
                            ,DocSubmittedCount
                            ,DocStatusDescrip
                            ,CampGrpId
                      FROM   (
                             SELECT DISTINCT t1.adReqId
                                   ,t1.Descrip
                                   ,CASE WHEN (
                                              SELECT COUNT(*)
                                              FROM   adReqGroups
                                              WHERE  IsMandatoryReqGrp = 1
                                              ) >= 1 THEN (
                                                          SELECT ReqGrpId
                                                          FROM   adReqGroups
                                                          WHERE  IsMandatoryReqGrp = 1
                                                          )
                                         ELSE '00000000-0000-0000-0000-000000000000'
                                    END AS ReqGrpId
                                   -- GETDATE() AS CurrentDate ,  
                                   ,@StudentStartDate AS CurrentDate
                                   ,t2.StartDate
                                   ,t2.EndDate
                                   ,(
                                    SELECT ActualScore
                                    FROM   adLeadEntranceTest
                                    WHERE  EntrTestId = t1.adReqId
                                           AND LeadId = @LeadID
                                    ) AS ActualScore
                                   ,(
                                    SELECT TestTaken
                                    FROM   adLeadEntranceTest
                                    WHERE  EntrTestId = t1.adReqId
                                           AND LeadId = @LeadID
                                    ) AS TestTaken
                                   ,(
                                    SELECT Comments
                                    FROM   adLeadEntranceTest
                                    WHERE  EntrTestId = t1.adReqId
                                           AND LeadId = @LeadID
                                    ) AS Comments
                                   ,(
                                    SELECT OverRide
                                    FROM   adEntrTestOverRide
                                    WHERE  EntrTestId = t1.adReqId
                                           AND LeadId = @LeadID
                                    ) AS override
                                   ,(
                                    SELECT SUM(Res1.DocSubmittedCount)
                                    FROM   (
                                           (SELECT COUNT(*) AS DocSubmittedCount
                                            FROM   dbo.adLeadDocsReceived
                                            WHERE  DocumentId = t1.adReqId
                                                   AND LeadId = @LeadID)
                                           UNION ALL
                                           (SELECT COUNT(*) AS DocSubmittedCount
                                            FROM   plStudentDocs
                                            WHERE  DocumentId = t1.adReqId
                                                   AND StudentId = @StudentId)
                                           ) AS Res1
                                    ) AS DocSubmittedCount
                                   ,t2.MinScore
                                   ,1 AS Required
                                   ,(
                                    SELECT DISTINCT s1.DocStatusDescrip
                                    FROM   sySysDocStatuses s1
                                          ,syDocStatuses s2
                                          ,dbo.adLeadDocsReceived s3
                                    WHERE  s1.SysDocStatusId = s2.SysDocStatusId
                                           AND s2.DocStatusId = s3.DocStatusId
                                           AND s3.LeadId = @LeadID
                                           AND s3.DocumentId = t1.adReqId
                                    ) AS DocStatusDescrip
                                   ,t1.CampGrpId
                             FROM   adReqs t1
                                   ,adReqsEffectiveDates t2
                             WHERE  t1.adReqId = t2.adReqId
                                    AND t2.MandatoryRequirement = 1
                                    AND t1.ReqforEnrollment = 1
                                    AND t1.adReqTypeId IN ( 1 )
                                    AND t1.StatusId = @ActiveStatusID
                             ) R1
                      WHERE  R1.CurrentDate >= R1.StartDate
                             AND (
                                 R1.CurrentDate <= R1.EndDate
                                 OR R1.EndDate IS NULL
                                 )
                      UNION
                      SELECT DISTINCT adReqId
                            ,Descrip
                            ,reqGrpId
                            ,StartDate
                            ,EndDate
                            ,ActualScore
                            ,TestTaken
                            ,Comments
                            ,override
                            ,MinScore
                            ,Required
                            ,CASE WHEN ActualScore >= MinScore THEN 'True'
                                  ELSE 'False'
                             END AS Pass
                            ,DocSubmittedCount
                            ,DocStatusDescrip
                            ,CampGrpId
                      FROM   (
                             SELECT DISTINCT t1.adReqId
                                   ,t1.Descrip
                                   ,'00000000-0000-0000-0000-000000000000' AS reqGrpId
                                   -- GETDATE() AS CurrentDate ,  
                                   ,@StudentStartDate AS CurrentDate
                                   ,t2.StartDate
                                   ,t2.EndDate
                                   ,(
                                    SELECT ActualScore
                                    FROM   adLeadEntranceTest
                                    WHERE  EntrTestId = t1.adReqId
                                           AND LeadId = @LeadID
                                    ) AS ActualScore
                                   ,(
                                    SELECT TestTaken
                                    FROM   adLeadEntranceTest
                                    WHERE  EntrTestId = t1.adReqId
                                           AND LeadId = @LeadID
                                    ) AS TestTaken
                                   ,(
                                    SELECT Comments
                                    FROM   adLeadEntranceTest
                                    WHERE  EntrTestId = t1.adReqId
                                           AND LeadId = @LeadID
                                    ) AS Comments
                                   ,(
                                    SELECT OverRide
                                    FROM   adEntrTestOverRide
                                    WHERE  EntrTestId = t1.adReqId
                                           AND LeadId = @LeadID
                                    ) AS override
                                   ,(
                                    SELECT SUM(Res1.DocSubmittedCount)
                                    FROM   (
                                           (SELECT COUNT(*) AS DocSubmittedCount
                                            FROM   dbo.adLeadDocsReceived
                                            WHERE  DocumentId = t1.adReqId
                                                   AND LeadId = @LeadID)
                                           UNION ALL
                                           (SELECT COUNT(*) AS DocSubmittedCount
                                            FROM   plStudentDocs
                                            WHERE  DocumentId = t1.adReqId
                                                   AND StudentId = @StudentId)
                                           ) AS Res1
                                    ) AS DocSubmittedCount
                                   ,CASE WHEN (
                                              SELECT COUNT(*)
                                              FROM   adPrgVerTestDetails
                                              WHERE  adReqId = t1.adReqId
                                                     AND PrgVerId = @PrgVerId
                                              ) >= 1 THEN (
                                                          SELECT MinScore
                                                          FROM   adPrgVerTestDetails
                                                          WHERE  adReqId = t1.adReqId
                                                                 AND PrgVerId = @PrgVerId
                                                          )
                                         ELSE t2.MinScore
                                    END AS MinScore
                                   ,1 AS Required
                                   ,(
                                    SELECT DISTINCT s1.DocStatusDescrip
                                    FROM   sySysDocStatuses s1
                                          ,syDocStatuses s2
                                          ,dbo.adLeadDocsReceived s3
                                    WHERE  s1.SysDocStatusId = s2.SysDocStatusId
                                           AND s2.DocStatusId = s3.DocStatusId
                                           AND s3.LeadId = @LeadID
                                           AND s3.DocumentId = t1.adReqId
                                    ) AS DocStatusDescrip
                                   ,t1.CampGrpId
                             FROM   adReqs t1
                                   ,adReqsEffectiveDates t2
                                   ,adReqLeadGroups t3
                                   ,adLeads t4
                                   ,adPrgVerTestDetails t5
                             WHERE  t1.adReqId = t2.adReqId
                                    AND t2.MandatoryRequirement <> 1
                                    AND t1.ReqforEnrollment = 1
                                    AND t1.adReqTypeId IN ( 1 )
                                    AND t1.StatusId = @ActiveStatusID
                                    AND t2.adReqEffectiveDateId = t3.adReqEffectiveDateId
                                    AND t4.PrgVerId = t5.PrgVerId
                                    AND t1.adReqId = t5.adReqId
                                    AND t5.PrgVerId = @PrgVerId
                                    --AND t3.LeadGrpId IN (  
                                    --SELECT DISTINCT  
                                    --        LeadGrpId  
                                    --FROM    adLeadByLeadGroups  
                                    --WHERE   LeadId = @LeadID )  
                                    AND t5.adReqId IS NOT NULL
                             ) R1
                      WHERE  R1.CurrentDate >= R1.StartDate
                             AND (
                                 R1.CurrentDate <= R1.EndDate
                                 OR R1.EndDate IS NULL
                                 )
                      UNION
                      SELECT DISTINCT adReqId
                            ,Descrip
                            ,reqGrpId
                            ,StartDate
                            ,EndDate
                            ,ActualScore
                            ,TestTaken
                            ,Comments
                            ,override
                            ,MinScore
                            ,Required
                            ,CASE WHEN ActualScore >= MinScore THEN 'True'
                                  ELSE 'False'
                             END AS Pass
                            ,DocSubmittedCount
                            ,DocStatusDescrip
                            ,CampGrpId
                      FROM   (
                             SELECT DISTINCT t1.adReqId
                                   ,t1.Descrip
                                   ,'00000000-0000-0000-0000-000000000000' AS reqGrpId
                                   -- GETDATE() AS CurrentDate ,  
                                   ,@StudentStartDate AS CurrentDate
                                   ,t2.StartDate
                                   ,t2.EndDate
                                   ,(
                                    SELECT ActualScore
                                    FROM   adLeadEntranceTest
                                    WHERE  EntrTestId = t1.adReqId
                                           AND LeadId = @LeadID
                                    ) AS ActualScore
                                   ,(
                                    SELECT TestTaken
                                    FROM   adLeadEntranceTest
                                    WHERE  EntrTestId = t1.adReqId
                                           AND LeadId = @LeadID
                                    ) AS TestTaken
                                   ,(
                                    SELECT Comments
                                    FROM   adLeadEntranceTest
                                    WHERE  EntrTestId = t1.adReqId
                                           AND LeadId = @LeadID
                                    ) AS Comments
                                   ,(
                                    SELECT OverRide
                                    FROM   adEntrTestOverRide
                                    WHERE  EntrTestId = t1.adReqId
                                           AND LeadId = @LeadID
                                    ) AS override
                                   ,(
                                    SELECT SUM(Res1.DocSubmittedCount)
                                    FROM   (
                                           (SELECT COUNT(*) AS DocSubmittedCount
                                            FROM   dbo.adLeadDocsReceived
                                            WHERE  DocumentId = t1.adReqId
                                                   AND LeadId = @LeadID)
                                           UNION ALL
                                           (SELECT COUNT(*) AS DocSubmittedCount
                                            FROM   plStudentDocs
                                            WHERE  DocumentId = t1.adReqId
                                                   AND StudentId = @StudentId)
                                           ) AS Res1
                                    ) AS DocSubmittedCount
                                   ,t2.MinScore AS MinScore
                                   ,t3.IsRequired AS Required
                                   ,(
                                    SELECT DISTINCT s1.DocStatusDescrip
                                    FROM   sySysDocStatuses s1
                                          ,syDocStatuses s2
                                          ,dbo.adLeadDocsReceived s3
                                    WHERE  s1.SysDocStatusId = s2.SysDocStatusId
                                           AND s2.DocStatusId = s3.DocStatusId
                                           AND s3.LeadId = @LeadID
                                           AND s3.DocumentId = t1.adReqId
                                    ) AS DocStatusDescrip
                                   ,t1.CampGrpId
                             FROM   adReqs t1
                                   ,adReqsEffectiveDates t2
                                   ,adReqLeadGroups t3
                                   ,adPrgVerTestDetails t5
                                   ,adReqGrpDef t6
                                   ,adLeadByLeadGroups t7
                             WHERE  t1.adReqId = t2.adReqId
                                    AND t1.adReqTypeId IN ( 1 )
                                    AND t1.StatusId = @ActiveStatusID
                                    AND t1.ReqforEnrollment = 1
                                    AND t2.adReqEffectiveDateId = t3.adReqEffectiveDateId
                                    AND t5.ReqGrpId = t6.ReqGrpId
                                    AND t3.LeadGrpId = t7.LeadGrpId
                                    AND t6.LeadGrpId = t7.LeadGrpId
                                    AND t1.adReqId = t6.adReqId
                                    AND t5.ReqGrpId IS NOT NULL
                                    AND ( LeadId = @LeadID )
                                    AND t5.PrgVerId = @PrgVerId
                             ) R1
                      WHERE  R1.CurrentDate >= R1.StartDate
                             AND (
                                 R1.CurrentDate <= R1.EndDate
                                 OR R1.EndDate IS NULL
                                 )
                      UNION
                      SELECT DISTINCT adReqId
                            ,Descrip
                            ,ReqGrpId
                            ,StartDate
                            ,EndDate
                            ,ActualScore
                            ,TestTaken
                            ,Comments
                            ,override
                            ,MinScore
                            ,Required
                            ,CASE WHEN ActualScore >= MinScore THEN 'True'
                                  ELSE 'False'
                             END AS Pass
                            ,DocSubmittedCount
                            ,DocStatusDescrip
                            ,CampGrpId
                      FROM   (
                             SELECT DISTINCT t1.adReqId
                                   ,t1.Descrip
                                   ,'00000000-0000-0000-0000-000000000000' AS ReqGrpId
                                   -- GETDATE() AS CurrentDate ,  
                                   ,@StudentStartDate AS CurrentDate
                                   ,t2.StartDate
                                   ,t2.EndDate
                                   ,(
                                    SELECT ActualScore
                                    FROM   adLeadEntranceTest
                                    WHERE  EntrTestId = t1.adReqId
                                           AND LeadId = @LeadID
                                    ) AS ActualScore
                                   ,(
                                    SELECT TestTaken
                                    FROM   adLeadEntranceTest
                                    WHERE  EntrTestId = t1.adReqId
                                           AND LeadId = @LeadID
                                    ) AS TestTaken
                                   ,(
                                    SELECT Comments
                                    FROM   adLeadEntranceTest
                                    WHERE  EntrTestId = t1.adReqId
                                           AND LeadId = @LeadID
                                    ) AS Comments
                                   ,(
                                    SELECT OverRide
                                    FROM   adEntrTestOverRide
                                    WHERE  EntrTestId = t1.adReqId
                                           AND LeadId = @LeadID
                                    ) AS override
                                   ,(
                                    SELECT SUM(Res1.DocSubmittedCount)
                                    FROM   (
                                           (SELECT COUNT(*) AS DocSubmittedCount
                                            FROM   dbo.adLeadDocsReceived
                                            WHERE  DocumentId = t1.adReqId
                                                   AND LeadId = @LeadID)
                                           UNION ALL
                                           (SELECT COUNT(*) AS DocSubmittedCount
                                            FROM   plStudentDocs
                                            WHERE  DocumentId = t1.adReqId
                                                   AND StudentId = @StudentId)
                                           ) AS Res1
                                    ) AS DocSubmittedCount
                                   ,t2.MinScore AS MinScore
                                   ,t3.IsRequired AS Required
                                   ,(
                                    SELECT DISTINCT s1.DocStatusDescrip
                                    FROM   sySysDocStatuses s1
                                          ,syDocStatuses s2
                                          ,dbo.adLeadDocsReceived s3
                                    WHERE  s1.SysDocStatusId = s2.SysDocStatusId
                                           AND s2.DocStatusId = s3.DocStatusId
                                           AND s3.LeadId = @LeadID
                                           AND s3.DocumentId = t1.adReqId
                                    ) AS DocStatusDescrip
                                   ,t1.CampGrpId
                             FROM   adReqs t1
                                   ,adReqsEffectiveDates t2
                                   ,adReqLeadGroups t3
                             WHERE  t1.adReqId = t2.adReqId
                                    AND t2.adReqEffectiveDateId = t3.adReqEffectiveDateId
                                    AND t1.StatusId = @ActiveStatusID
                                    AND t1.ReqforEnrollment = 1
                                    AND t1.adReqId NOT IN (
                                                          SELECT DISTINCT s1.adReqId
                                                          FROM   adReqGrpDef s1
                                                                ,adPrgVerTestDetails s2
                                                          WHERE  s1.ReqGrpId = s2.ReqGrpId
                                                                 AND s2.PrgVerId = @PrgVerId
                                                                 AND s2.adReqId IS NULL
                                                          )
                                    AND t3.LeadGrpId IN (
                                                        SELECT LeadGrpId
                                                        FROM   adLeadByLeadGroups
                                                        WHERE  LeadId = @LeadID
                                                        )
                                    AND t1.adReqTypeId IN ( 1 )
                                    AND t2.MandatoryRequirement <> 1
                                    AND t1.adReqId NOT IN (
                                                          SELECT DISTINCT adReqId
                                                          FROM   adPrgVerTestDetails
                                                          WHERE  adReqId IS NOT NULL
                                                                 AND PrgVerId = @PrgVerId
                                                          )
                             ) R1
                      WHERE  R1.CurrentDate >= R1.StartDate
                             AND (
                                 R1.CurrentDate <= R1.EndDate
                                 OR R1.EndDate IS NULL
                                 )
                      ) R2
               WHERE  R2.Required = 1
                      AND R2.Pass = 'False'
               ) R3
        WHERE  R3.CampGrpId IN (
                               SELECT DISTINCT t2.CampGrpId
                               FROM   syCmpGrpCmps t1
                                     ,syCampGrps t2
                               WHERE  t1.CampGrpId = t2.CampGrpId
                                      AND t1.CampusId = @CampusID
                                      AND t2.StatusId = @ActiveStatusID
                               );
    --        AND R3.OverRide <> 'True';  -- DE13873 -- US10746
    --GROUP BY OverRide  
    --HAVING  R3.OverRide = 'False'   

    END;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Refreshing [dbo].[syStudentContactAddresses]';
GO
IF OBJECT_ID(N'[dbo].[syStudentContactAddresses]', 'V') IS NOT NULL
    EXEC sp_refreshview N'[dbo].[syStudentContactAddresses]';
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Refreshing [dbo].[plStudentSkills]';
GO
IF OBJECT_ID(N'[dbo].[plStudentSkills]', 'V') IS NOT NULL
    EXEC sp_refreshview N'[dbo].[plStudentSkills]';
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Refreshing [dbo].[plStudentExtraCurriculars]';
GO
IF OBJECT_ID(N'[dbo].[plStudentExtraCurriculars]', 'V') IS NOT NULL
    EXEC sp_refreshview N'[dbo].[plStudentExtraCurriculars]';
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Refreshing [dbo].[plStudentEmployment]';
GO
IF OBJECT_ID(N'[dbo].[plStudentEmployment]', 'V') IS NOT NULL
    EXEC sp_refreshview N'[dbo].[plStudentEmployment]';
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Refreshing [dbo].[plStudentEducation]';
GO
IF OBJECT_ID(N'[dbo].[plStudentEducation]', 'V') IS NOT NULL
    EXEC sp_refreshview N'[dbo].[plStudentEducation]';
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'If Exists sp USP_EnrollLead, delete it '
GO
IF EXISTS ( SELECT  1 
            FROM    sys.objects 
            WHERE   object_id = OBJECT_ID(N'[dbo].[USP_EnrollLead]') 
                    AND type IN ( N'P',N'PC' ) ) 
    BEGIN 
		PRINT N'    Dropping SP USP_EnrollLead'; 
        DROP PROCEDURE USP_EnrollLead; 
    END; 
GO 
IF @@ERROR <> 0 
    SET NOEXEC ON; 
GO 
PRINT N'Creating USP_EnrollLead'; 
GO
-- =========================================================================================================    
-- USP_EnrollLead    
-- =========================================================================================================    
CREATE PROCEDURE [dbo].[USP_EnrollLead]
    (
        @leadId AS UNIQUEIDENTIFIER
       ,@campus UNIQUEIDENTIFIER
       ,@gender AS UNIQUEIDENTIFIER
       ,@familyIncome UNIQUEIDENTIFIER = NULL
       ,@housingType UNIQUEIDENTIFIER = NULL
       ,@educationLevel UNIQUEIDENTIFIER = NULL
       ,@adminCriteria UNIQUEIDENTIFIER = NULL
       ,@degSeekCert UNIQUEIDENTIFIER = NULL
       ,@attendType UNIQUEIDENTIFIER = NULL
       ,@race UNIQUEIDENTIFIER = NULL
       ,@citizenship UNIQUEIDENTIFIER = NULL
       ,@prgVersion UNIQUEIDENTIFIER = NULL
       ,@programId UNIQUEIDENTIFIER = NULL
       ,@areaId UNIQUEIDENTIFIER = NULL
       ,@prgVerType INT = 0
       ,@startDt DATETIME
       --,@studentId UNIQUEIDENTIFIER    
       ,@expectedStartDt DATETIME = NULL
       ,@contractGrdDt DATETIME
       ,@tuitionCategory UNIQUEIDENTIFIER
       ,@enrollDt DATETIME
       ,@transferhr DECIMAL(18, 2) = NULL
       ,@dob AS DATETIME = NULL
       ,@ssn AS VARCHAR(50) = NULL
       ,@depencytype UNIQUEIDENTIFIER = NULL
       ,@maritalstatus UNIQUEIDENTIFIER = NULL
       ,@nationality UNIQUEIDENTIFIER = NULL
       ,@geographicType UNIQUEIDENTIFIER = NULL
       ,@state UNIQUEIDENTIFIER = NULL
       ,@disabled BIT = NULL
       ,@firsttimeSchool BIT = NULL
       ,@firsttimePostSec BIT = NULL
       ,@schedule UNIQUEIDENTIFIER = NULL
       ,@shift UNIQUEIDENTIFIER = NULL
       ,@midptDate DATETIME = NULL
       ,@transferDt DATETIME = NULL
       ,@entranceInterviewDt DATETIME = NULL
       ,@chargingMethod UNIQUEIDENTIFIER = NULL
       ,@fAAvisor UNIQUEIDENTIFIER = NULL
       ,@acedemicAdvisor UNIQUEIDENTIFIER = NULL
       ,@badgeNum VARCHAR(50) = NULL
       ,@studentNumber VARCHAR(50) = ''
       --,@studentStatusId UNIQUEIDENTIFIER = NULL -- should be active as it is in adleads    
       --,@stuEnrollId UNIQUEIDENTIFIER = NULL        
       ,@modUser UNIQUEIDENTIFIER = NULL
       ,@enrollId VARCHAR(50) = NULL
    )
AS
    BEGIN
        SET NOCOUNT ON;
        DECLARE @transDate DATETIME;
        DECLARE @MyError AS INTEGER;
        DECLARE @Message AS NVARCHAR(MAX);
        DECLARE @modUserN VARCHAR(50)
               ,@studentStatusId UNIQUEIDENTIFIER
               ,@studentEnrolStatus UNIQUEIDENTIFIER
               ,@studentId UNIQUEIDENTIFIER
               ,@stuEnrollId UNIQUEIDENTIFIER
               ,@admissionRepId UNIQUEIDENTIFIER;
        SET @transDate = GETDATE();
        SET @studentStatusId = (
                               SELECT StatusId
                               FROM   syStatuses
                               WHERE  StatusCode = 'A'
                               );
        --,@originalStatus UNIQUEIDENTIFIER;     
        SET @studentId = NEWID();
        --set @studentEnrolStatus to system future start    
        SET @studentEnrolStatus = (
                                  SELECT DISTINCT TOP 1 A.StatusCodeId
                                  FROM   syStatusCodes A
                                        ,sySysStatus B
                                        ,syStatuses C
                                        ,syStatusLevels D
                                  WHERE  A.SysStatusId = B.SysStatusId
                                         AND A.StatusId = C.StatusId
                                         AND B.StatusLevelId = D.StatusLevelId
                                         AND C.Status = 'Active'
                                         AND D.StatusLevelId = 2
                                         AND B.SysStatusId = 7
                                  --ORDER BY A.StatusCodeDescrip    
                                  );
        DECLARE @acCalendarID INT;
        SELECT @modUserN = UserName
        FROM   syUsers
        WHERE  UserId = @modUser;
        SET @MyError = 0; -- all good    
        SET @Message = '';
        BEGIN TRANSACTION;
        BEGIN TRY
            --step 1: temperary insert into arstudent    
            DECLARE @lName VARCHAR(50)
                   ,@fName VARCHAR(50)
                   ,@mName VARCHAR(50);

            SET @lName = (
                         SELECT LastName
                         FROM   adLeads
                         WHERE  LeadId = @leadId
                         );
            SET @fName = (
                         SELECT FirstName
                         FROM   adLeads
                         WHERE  LeadId = @leadId
                         );
            SET @mName = (
                         SELECT MiddleName
                         FROM   adLeads
                         WHERE  LeadId = @leadId
                         );
            --IF ( @MyError = 0 )    
            --    BEGIN    
            --        INSERT  INTO arStudent    
            --                (    
            --                 StudentId    
            --                ,StudentStatus    
            --                ,LastName    
            --                ,SSN    
            --                ,MiddleName    
            --                ,FirstName    
            --                )    
            --        VALUES  (    
            --                 @studentId    
            --                ,@studentStatusId    
            --                ,@lName  -- StudentStatus - uniqueidentifier               
            --                ,@ssn    
            --                ,@mName    
            --                ,@fName    
            --                );     
            --        SET @MyError = @@ERROR;        
            --        IF ( @MyError <> 0 )    
            --            BEGIN    
            --                SET @MyError = 1;    
            --                SET @Message = @Message + 'Failed to insert in arStudent';    
            --            END;    
            --    END;    
            IF ( @MyError = 0 )
                BEGIN
                    --update the adleads table    
                    DECLARE @enrolLeadStatus UNIQUEIDENTIFIER;
                    SET @enrolLeadStatus = (
                                           SELECT   TOP 1 StatusCodeId
                                           FROM     syStatusCodes
                                           WHERE    SysStatusId = 6
                                                    AND StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
                                           ORDER BY ModDate
                                           );
                    DECLARE @stuNum VARCHAR(50);
                    SET @stuNum = (
                                  SELECT StudentNumber
                                  FROM   adLeads
                                  WHERE  LeadId = @leadId
                                  );
                    IF ( @stuNum = '' )
                        BEGIN
                            -- generate student number    
                            DECLARE @formatType INT
                                   ,@yrNo INT
                                   ,@monthNo INT
                                   ,@dateNo INT
                                   ,@lNameNo INT
                                   ,@fNameNo INT;
                            DECLARE @lNameStu VARCHAR(50)
                                   ,@fNameStu VARCHAR(50)
                                   ,@yr VARCHAR(10)
                                   ,@mon VARCHAR(10)
                                   ,@dt VARCHAR(10)
                                   ,@seqStartNo INT;
                            SELECT @formatType = FormatType
                                  ,@yrNo = ISNULL(YearNumber, 0)
                                  ,@monthNo = ISNULL(MonthNumber, 0)
                                  ,@dateNo = ISNULL(DateNumber, 0)
                                  ,@lNameNo = ISNULL(LNameNumber, 0)
                                  ,@fNameNo = ISNULL(FNameNumber, 0)
                                  ,@seqStartNo = SeqStartingNumber
                            FROM   syStudentFormat;
                            IF ( @formatType = 1 )
                                BEGIN
                                    SET @stuNum = '';
                                END;
                            ELSE IF ( @formatType = 3 )
                                     BEGIN
                                         SET @lNameStu = SUBSTRING(@lName, 1, ISNULL(@lNameNo, 0));
                                         SET @fNameStu = SUBSTRING(@fName, 1, ISNULL(@fNameNo, 0));
                                         SET @yr = '';
                                         SET @yr = CASE @yrNo
                                                        WHEN 0 THEN ''
                                                        WHEN 4 THEN SUBSTRING(CAST(YEAR(GETDATE()) AS VARCHAR(10)), 1, 4)
                                                        WHEN 3 THEN SUBSTRING(CAST(YEAR(GETDATE()) AS VARCHAR(10)), 2, 3)
                                                        WHEN 2 THEN SUBSTRING(CAST(YEAR(GETDATE()) AS VARCHAR(10)), 3, 2)
                                                        WHEN 1 THEN SUBSTRING(CAST(YEAR(GETDATE()) AS VARCHAR(10)), 4, 1)
                                                   END;
                                         SET @mon = SUBSTRING(CAST(MONTH(GETDATE()) AS VARCHAR(10)), 1, @monthNo);
                                         SET @dt = SUBSTRING(CAST(DAY(GETDATE()) AS VARCHAR(10)), 1, @dateNo);
                                         IF (
                                            LEN(@mon) = 1
                                            AND @monthNo = 2
                                            )
                                             BEGIN
                                                 SET @mon = '0' + @mon;
                                             END;
                                         IF (
                                            LEN(@dt) = 1
                                            AND @dateNo = 2
                                            )
                                             BEGIN
                                                 SET @dt = '0' + @dt;
                                             END;
                                         SET NOCOUNT ON;
                                         DECLARE @stuId INT;
                                         SET @stuId = (
                                                      SELECT MAX(Student_SeqId) AS Student_SeqID
                                                      FROM   syGenerateStudentFormatID
                                                      ) + 1;
                                         INSERT INTO syGenerateStudentFormatID (
                                                                               Student_SeqId
                                                                              ,ModDate
                                                                               )
                                         VALUES ( @stuId, GETDATE());

                                         IF ( @yr = '' )
                                             BEGIN
                                                 SET @stuNum = @mon + @dt + @lNameStu + @fNameStu + CAST(@stuId AS VARCHAR(5));
                                             END;
                                         ELSE
                                             BEGIN
                                                 SET @stuNum = @yr + @mon + @dt + @lNameStu + @fNameStu + CAST(@stuId AS VARCHAR(5));
                                             END;
                                     --SELECT @stuNum    
                                     END;
                            ELSE
                                     BEGIN
                                         SET NOCOUNT ON;

                                         IF EXISTS (
                                                   SELECT *
                                                   FROM   syGenerateStudentSeq
                                                   )
                                             BEGIN
                                                 SET @stuId = (
                                                              SELECT MAX(Student_SeqId) AS Student_SeqID
                                                              FROM   syGenerateStudentSeq
                                                              ) + 1;
                                             END;
                                         ELSE
                                             BEGIN
                                                 SET @stuId = @seqStartNo;
                                             END;
                                         INSERT INTO syGenerateStudentSeq (
                                                                          Student_SeqId
                                                                         ,ModDate
                                                                          )
                                         VALUES ( @stuId, GETDATE());
                                         SET @stuNum = CAST(@stuId AS VARCHAR(10));
                                     END;

                        END;
                    UPDATE adLeads
                    SET    Gender = @gender
                          ,BirthDate = @dob                             --ISNULL(@dob,BirthDate)    
                          ,SSN = @ssn                                   --ISNULL(@ssn,SSN)    
                          ,DependencyTypeId = @depencytype              --ISNULL(@depencytype,DependencyTypeId)    
                          ,MaritalStatus = @maritalstatus               --ISNULL(@maritalstatus,MaritalStatus)    
                          ,FamilyIncome = @familyIncome
                          ,HousingId = @housingType
                          ,admincriteriaid = @adminCriteria
                          ,DegCertSeekingId = @degSeekCert
                          ,AttendTypeId = @attendType
                          ,Race = @race
                          ,Nationality = @nationality                   --ISNULL(@nationality,Nationality)    
                          ,Citizen = @citizenship
                          ,GeographicTypeId = @geographicType           --ISNULL(@geographicType,GeographicTypeId)    
                          ,IsDisabled = @disabled                       --ISNULL(@disabled,IsDisabled)    
                          ,IsFirstTimeInSchool = @firsttimeSchool       --ISNULL(@firsttimeSchool,IsFirstTimeInSchool)    
                          ,CampusId = @campus
                          ,IsFirstTimePostSecSchool = @firsttimePostSec --ISNULL(@firsttimePostSec,IsFirstTimePostSecSchool)    
                          ,PrgVerId = @prgVersion
                          ,ProgramID = @programId
                          ,AreaID = @areaId
                          ,ProgramScheduleId = @schedule                --ISNULL(@schedule,ProgramScheduleId)    
                          ,ShiftID = @shift                             --ISNULL(@shift,ShiftID)    
                          ,ExpectedStart = @expectedStartDt
                          ,EntranceInterviewDate = @entranceInterviewDt --ISNULL(@entranceInterviewDt,EntranceInterviewDate)    
                          ,StudentNumber = ISNULL(@stuNum, '')          --ISNULL(@studentNumber,StudentNumber)    
                          ,StudentStatusId = ISNULL(@studentStatusId, StudentStatusId)
                          ,StudentId = ISNULL(@studentId, StudentId)
                          ,ModUser = ISNULL(@modUserN, ModUser)
                          ,ModDate = @transDate
                          ,LeadStatus = @enrolLeadStatus                --update to enrolled    
                          ,PreviousEducation = @educationLevel
                          ,EnrollStateId = @state
                    WHERE  LeadId = @leadId;
                    SET @MyError = @@ERROR;
                    IF ( @MyError <> 0 )
                        BEGIN
                            SET @MyError = 1;
                            SET @Message = @Message + 'Failed to update in adleads';
                        END;
                    SET @admissionRepId = (
                                          SELECT AdmissionsRep
                                          FROM   adLeads
                                          WHERE  LeadId = @leadId
                                          );
                END;

            IF ( @MyError = 0 )
                BEGIN
                    --insert into arStuEnrollments    
                    SET @stuEnrollId = NEWID();
                    INSERT INTO arStuEnrollments (
                                                 StuEnrollId
                                                ,StudentId
                                                ,EnrollDate
                                                ,PrgVerId
                                                ,StartDate
                                                ,ExpStartDate
                                                ,MidPtDate
                                                ,TransferDate
                                                ,ShiftId
                                                ,BillingMethodId
                                                ,CampusId
                                                ,StatusCodeId
                                                ,ModDate
                                                ,ModUser
                                                ,EnrollmentId
                                                ,AcademicAdvisor
                                                ,LeadId
                                                ,TuitionCategoryId
                                                ,EdLvlId
                                                ,FAAdvisorId
                                                ,attendtypeid
                                                ,degcertseekingid
                                                ,BadgeNumber
                                                ,ContractedGradDate
                                                ,TransferHours
                                                ,PrgVersionTypeId
                                                ,IsDisabled
                                                ,IsFirstTimeInSchool
                                                ,IsFirstTimePostSecSchool
                                                ,EntranceInterviewDate
                                                ,AdmissionsRep
                                                ,ExpGradDate
                                                 )
                    VALUES ( @stuEnrollId
                            ,@studentId           -- StudentId - uniqueidentifier    
                            ,@enrollDt            -- EnrollDate - datetime    
                            ,@prgVersion          -- PrgVerId - uniqueidentifier    
                            ,@startDt             -- StartDate - datetime    
                            ,@expectedStartDt     -- ExpStartDate - datetime    
                            ,@midptDate           -- MidPtDate - datetime             
                            ,@transferDt          -- TransferDate - datetime    
                            ,@shift               -- ShiftId - uniqueidentifier    
                            ,@chargingMethod      -- BillingMethodId - uniqueidentifier    
                            ,@campus              -- CampusId - uniqueidentifier    
                            ,@studentEnrolStatus  -- StatusCodeId - uniqueidentifier             
                            ,@transDate           -- ModDate - datetime    
                            ,@modUserN            -- ModUser - varchar(50)    
                            ,@enrollId            -- EnrollmentId - varchar(50)             
                            ,@acedemicAdvisor     -- AcademicAdvisor - uniqueidentifier    
                            ,@leadId              -- LeadId - uniqueidentifier    
                            ,@tuitionCategory     -- TuitionCategoryId - uniqueidentifier    
                            ,@educationLevel      -- EdLvlId - uniqueidentifier    
                            ,@fAAvisor            -- FAAdvisorId - uniqueidentifier    
                            ,@attendType          -- attendtypeid - uniqueidentifier    
                            ,@degSeekCert         -- degcertseekingid - uniqueidentifier    
                            ,@badgeNum            -- BadgeNumber - varchar(50)    
                            ,@contractGrdDt       -- ContractedGradDate - datetime    
                            ,@transferhr          -- TransferHours - decimal    
                            ,@prgVerType          -- PrgVersionTypeId - int    
                            ,@disabled            -- IsDisabled - bit    
                            ,@firsttimeSchool     -- IsFirstTimeInSchool - bit    
                            ,@firsttimePostSec    -- IsFirstTimePostSecSchool - bit    
                            ,@entranceInterviewDt -- EntranceInterviewDate - datetime    
                            ,@admissionRepId      -- AdmissionsRep uniqueidentifier    
                            ,@contractGrdDt       -- ExpGradDate DateTime    
                           );
                    SET @MyError = @@ERROR;
                    IF ( @MyError <> 0 )
                        BEGIN
                            SET @MyError = 1;
                            SET @Message = @Message + 'Failed to insert in arStuEnrollments';
                        END;
                END;
            IF ( @MyError = 0 )
                BEGIN
                    IF @stuEnrollId IS NOT NULL
                        BEGIN
                            IF ( @MyError = 0 )
                                BEGIN
                                    --insert into syStudentStatusChanges    
                                    DECLARE @OriginalleadStatus UNIQUEIDENTIFIER;
                                    SET @OriginalleadStatus = (
                                                              SELECT LeadStatus
                                                              FROM   adLeads
                                                              WHERE  LeadId = @leadId
                                                              );
                                    INSERT INTO syStudentStatusChanges (
                                                                       StudentStatusChangeId
                                                                      ,StuEnrollId
                                                                      ,OrigStatusId
                                                                      ,NewStatusId
                                                                      ,CampusId
                                                                      ,ModDate
                                                                      ,ModUser
                                                                      ,IsReversal
                                                                      ,DropReasonId
                                                                      ,DateOfChange
                                                                      ,Lda
                                                                       )
                                    VALUES ( NEWID()
                                            ,@stuEnrollId
                                            ,@OriginalleadStatus
                                            ,@studentEnrolStatus
                                            ,@campus
                                            ,@transDate
                                            ,@modUserN
                                            ,0
                                            ,NULL
                                            ,@enrollDt
                                            ,NULL
                                           );
                                    SET @MyError = @@ERROR;
                                    IF ( @MyError <> 0 )
                                        BEGIN
                                            SET @MyError = 1;
                                            SET @Message = @Message + 'Failed to insert in syStudentStatusChanges';
                                        END;
                                END;
                            --insert into plStudentDocs    
                            INSERT INTO plStudentDocs (
                                                      StudentId
                                                     ,DocumentId
                                                     ,DocStatusId
                                                     ,ReceiveDate
                                                     ,ScannedId
                                                     ,ModDate
                                                     ,ModUser
                                                     ,ModuleID
                                                      )
                                        SELECT @studentId
                                              ,DocumentId
                                              ,DocStatusId
                                              ,ReceiveDate
                                              ,1
                                              ,@transDate
                                              ,@modUserN
                                              ,(
                                               SELECT DISTINCT ModuleId
                                               FROM   adReqs
                                               WHERE  adReqId = adLeadDocsReceived.DocumentId
                                               )
                                        FROM   adLeadDocsReceived
                                        WHERE  LeadId = @leadId;
                        END;
                    SET @MyError = @@ERROR;
                    IF ( @MyError <> 0 )
                        BEGIN
                            SET @MyError = 1;
                            SET @Message = @Message + 'Failed to insert in plStudentDocs';
                        END;
                END;
            IF ( @MyError = 0 )
                BEGIN
                    --update syDocumentHistory    
                    UPDATE syDocumentHistory
                    SET    StudentId = @studentId
                          ,ModUser = @modUserN
                          ,ModDate = @transDate
                    WHERE  LeadId = @leadId;
                    SET @MyError = @@ERROR;
                    IF ( @MyError <> 0 )
                        BEGIN
                            SET @MyError = 1;
                            SET @Message = @Message + 'Failed to update in syDocumentHistory';
                        END;
                END;
            --update adLeadByLeadGroups    
            IF ( @MyError = 0 )
                BEGIN
                    UPDATE adLeadByLeadGroups
                    SET    StuEnrollId = @stuEnrollId
                          ,ModUser = @modUserN
                          ,ModDate = @transDate
                    WHERE  LeadId = @leadId;
                    SET @MyError = @@ERROR;
                    IF ( @MyError <> 0 )
                        BEGIN
                            SET @MyError = 1;
                            SET @Message = @Message + 'Failed to update in adLeadByLeadGroups';
                        END;

                END;
            --insert in adLeadEntranceTest    
            IF ( @MyError = 0 )
                BEGIN

                    UPDATE adLeadEntranceTest
                    SET    StudentId = @studentId
                    WHERE  LeadId = @leadId;
                    SET @MyError = @@ERROR;
                    IF ( @MyError <> 0 )
                        BEGIN
                            SET @MyError = 1;
                            SET @Message = @Message + 'Failed to update in adLeadEntranceTest';

                        END;
                END;
            --copy all the lead requirements adEntrTestOverRide    
            IF ( @MyError = 0 )
                BEGIN
                    --documents    
                    --                   INSERT  INTO adEntrTestOverRide  
                    --                           (  
                    --                            EntrTestOverRideId  
                    --                           ,LeadId  
                    --                           ,EntrTestId  
                    --                           ,ModUser  
                    --                           ,ModDate  
                    --                           ,OverRide  
                    --                           ,StudentId    
                    --                           )  
                    --                           SELECT  NEWID()  
                    --                                  ,@leadId  
                    --                                  ,DocumentId  
                    --,ModUser  
                    --                                  ,@transDate  
                    --                                  ,Override  
                    --                                  ,@studentId  
                    --                           FROM    adLeadDocsReceived  
                    --                           WHERE   LeadId = @leadId;    
                    --    --test    
                    --                   INSERT  INTO adEntrTestOverRide  
                    --                           (  
                    --                            EntrTestOverRideId  
                    --                           ,LeadId  
                    --                           ,EntrTestId  
                    --                           ,ModUser  
                    --                           ,ModDate  
                    --                           ,OverRide  
                    --                           ,StudentId    
                    --                           )  
                    --                           SELECT  NEWID()  
                    --                                  ,@leadId  
                    --                                  ,EntrTestId  
                    --                                  ,ModUser  
                    --                                  ,@transDate  
                    --                                  ,OverRide  
                    --                                  ,@studentId  
                    --                           FROM    adLeadEntranceTest  
                    --                           WHERE   LeadId = @leadId;    
                    --    --tours    
                    --                   INSERT  INTO adEntrTestOverRide  
                    --                           (  
                    --                            EntrTestOverRideId  
                    --                           ,LeadId  
                    --                           ,EntrTestId  
                    --                           ,ModUser  
                    --                           ,ModDate  
                    --                           ,OverRide  
                    --                           ,StudentId    
                    --                           )  
                    --                           SELECT  NEWID()  
                    --                                  ,@leadId  
                    --                                  ,DocumentId  
                    --                                  ,ModUser  
                    --                                  ,@transDate  
                    --                                  ,Override  
                    --                                  ,@studentId  
                    --                           FROM    AdLeadReqsReceived  
                    --                           WHERE   LeadId = @leadId;    
                    --    --fee    
                    --                   INSERT  INTO adEntrTestOverRide  
                    --                           (  
                    --                            EntrTestOverRideId  
                    --                           ,LeadId  
                    --                           ,EntrTestId  
                    --                           ,ModUser  
                    --                           ,ModDate  
                    --                           ,OverRide  
                    --                           ,StudentId    
                    --                           )  
                    --                           SELECT  NEWID()  
                    --                                  ,@leadId  
                    --                                  ,DocumentId  
                    --                                  ,ModUser  
                    --                                  ,@transDate  
                    --                                  ,Override  
                    --                                  ,@studentId  
                    --                           FROM    adLeadTranReceived  
                    --                           WHERE   LeadId = @leadId;    
                    UPDATE adEntrTestOverRide
                    SET    StudentId = @studentId
                    WHERE  LeadId = @leadId;

                    SET @MyError = @@ERROR;
                    IF ( @MyError <> 0 )
                        BEGIN
                            SET @MyError = 1;
                            SET @Message = @Message + 'Failed to update in adEntrTestOverRide';
                        END;
                END;
            --exec USP_AD_CopyLeadTransactions    
            IF ( @MyError = 0 )
                BEGIN
                    DECLARE @rc INT;
                    EXECUTE @rc = dbo.USP_AD_CopyLeadTransactions @leadId
                                                                 ,@stuEnrollId
                                                                 ,@campus;
                    SET @MyError = @@ERROR;
                    IF (
                       @MyError <> 0
                       AND @rc = 0
                       )
                        BEGIN
                            SET @MyError = 1;
                            SET @Message = @Message + 'Failed to execute USP_AD_CopyLeadTransactions';
                        END;
                END;
            --exec fees related SP    
            SET @acCalendarID = (
                                SELECT COUNT(P.ACId)
                                FROM   arPrograms P
                                      ,arPrgVersions PV
                                      ,arStuEnrollments SE
                                WHERE  PV.PrgVerId = SE.PrgVerId
                                       AND SE.StuEnrollId = @stuEnrollId
                                       AND P.ProgId = PV.ProgId
                                       AND ACId = 5
                                );
            --IF @acCalendarID >= 1    
            --    BEGIN    
            --apply fees by progver for clock hr programs    
            INSERT INTO saTransactions (
                                       TransactionId
                                      ,StuEnrollId
                                      ,TermId
                                      ,CampusId
                                      ,TransDate
                                      ,TransCodeId
                                      ,TransReference
                                      ,TransDescrip
                                      ,TransAmount
                                      ,FeeLevelId
                                      ,AcademicYearId
                                      ,TransTypeId
                                      ,IsPosted
                                      ,FeeId
                                      ,CreateDate
                                      ,IsAutomatic
                                      ,ModUser
                                      ,ModDate
                                       )
                        SELECT NEWID()
                              ,@stuEnrollId
                              ,NULL
                              ,@campus
                              ,SE.ExpStartDate
                              ,PVF.TransCodeId
                              ,PV.PrgVerDescrip AS TransDescrip
                              ,ST.TransCodeDescrip + '-' + PV.PrgVerDescrip AS TransCodeDescrip
                              ,CASE PVF.UnitId
                                    WHEN 0 THEN PVF.Amount * PV.Credits
                                    WHEN 1 THEN PVF.Amount * PV.Hours
                                    WHEN 2 THEN PVF.Amount
                               END AS TransAmount
                              ,2 -- program version fees    
                              ,NULL
                              ,0
                              ,1
                              ,PVF.PrgVerFeeId
                              ,@transDate
                              ,0
                              ,@modUserN
                              ,@transDate
                        FROM   saProgramVersionFees AS PVF
                        INNER JOIN arPrgVersions PV ON PV.PrgVerId = PVF.PrgVerId
                        INNER JOIN arStuEnrollments SE ON SE.PrgVerId = PVF.PrgVerId
                        INNER JOIN saTransCodes AS ST ON ST.TransCodeId = PVF.TransCodeId
                        WHERE  SE.StuEnrollId = @stuEnrollId
                               AND PVF.TuitionCategoryId = SE.TuitionCategoryId
                               AND RateScheduleId IS NULL;

            --charge Program Fees if applicable    
            IF ( @MyError = 0 )
                BEGIN
                    DECLARE @Result INT;
                    DECLARE @currdt DATETIME;
                    SET @currdt = @transDate;
                    EXEC USP_ApplyFeesByProgramVersion @stuEnrollId    -- uniqueidentifier    
                                                      ,@modUserN       -- varchar(100)    
                                                      ,@campus         -- uniqueidentifier    
                                                      ,@currdt         -- datetime    
                                                      ,@Result OUTPUT; -- int    
                    SET @MyError = @@ERROR;
                END;
            IF (
               @MyError = 0
               AND @Result = 1
               )
                BEGIN
                    COMMIT TRANSACTION;
                    SET @Message = 'Successful COMMIT TRANSACTION'; -- do not change this message. It is used for verification in C# code.    
                    SELECT @Message;
                END;
            ELSE
                BEGIN
                    ROLLBACK TRANSACTION;
                    SET @Message = @Message + 'rollback failed transaction';
                    SELECT @Message;
                END;
        END TRY
        BEGIN CATCH
            ROLLBACK TRANSACTION;
            SET @Message = @Message + 'Failed ROLLBACK TRANSACTION  Catching error';
            --SELECT  ERROR_NUMBER() AS ErrorNumber    
            --       ,ERROR_SEVERITY() AS ErrorSeverity    
            --       ,ERROR_STATE() AS ErrorState    
            --       ,ERROR_PROCEDURE() AS ErrorProcedure    
            --       ,ERROR_LINE() AS ErrorLine    
            --       ,ERROR_MESSAGE() AS ErrorMessage;       

            SET @Message = ERROR_MESSAGE() + @Message + 'try failed transaction';
            SELECT @Message;
        END CATCH;
    END;
-- =========================================================================================================    
-- END  --  USP_EnrollLead    
-- =========================================================================================================    
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'If Exists sp usp_pr_main_ssrs_subreport3_byterm, delete it '
GO
IF EXISTS ( SELECT  1 
            FROM    sys.objects 
            WHERE   object_id = OBJECT_ID(N'[dbo].[usp_pr_main_ssrs_subreport3_byterm]') 
                    AND type IN ( N'P',N'PC' ) ) 
    BEGIN 
		PRINT N'    Dropping SP usp_pr_main_ssrs_subreport3_byterm'; 
        DROP PROCEDURE usp_pr_main_ssrs_subreport3_byterm; 
    END; 
GO 
IF @@ERROR <> 0 
    SET NOEXEC ON; 
GO 
PRINT N'Creating usp_pr_main_ssrs_subreport3_byterm'; 
GO 
CREATE PROCEDURE [dbo].[usp_pr_main_ssrs_subreport3_byterm]
    @StuEnrollId VARCHAR(50)
   ,@TermId VARCHAR(50) = NULL
   ,@GradesFormat VARCHAR(50)
   ,@GPAMethod VARCHAR(50)
AS ---new changes jg
    SELECT  *
    FROM    (
              SELECT    *
                       ,ROW_NUMBER() OVER ( PARTITION BY StuEnrollId,TermId ORDER BY TermStartDate, TermEndDate, TermDescription, CourseDescription ) AS rownumber2
              FROM      (
                          SELECT DISTINCT
                                    3 AS Tag
                                   ,2 AS Parent
                                   ,PV.PrgVerId
                                   ,PV.PrgVerDescrip
                                   ,NULL AS ProgramCredits
                                   ,T.TermId
                                   ,T.TermDescrip AS TermDescription
                                   ,T.StartDate AS TermStartDate
                                   ,T.EndDate AS TermEndDate
                                   ,CS.ReqId AS CourseId
                                   ,R.Code AS CourseCode
                                   ,R.Descrip AS CourseDescription
                                   ,'(' + R.Code + ')' + R.Descrip AS CourseCodeDescription
                                   ,R.Credits AS CourseCredits
                                   ,R.FinAidCredits AS CourseFinAidCredits
                                   ,(
                                      SELECT    MIN(MinVal)
                                      FROM      arGradeScaleDetails GCD
                                               ,arGradeSystemDetails GSD
                                      WHERE     GCD.GrdSysDetailId = GSD.GrdSysDetailId
                                                AND GSD.IsPass = 1
                                                AND GCD.GrdScaleId = CS.GrdScaleId
                                    ) AS MinVal
                                   ,RES.Score AS CourseScore
                                   ,NULL AS GradeBook_ResultId
                                   ,NULL AS GradeBookDescription
                                   ,NULL AS GradeBookScore
                                   ,NULL AS GradeBookPostDate
                                   ,NULL AS GradeBookPassingGrade
                                   ,NULL AS GradeBookWeight
                                   ,NULL AS GradeBookRequired
                                   ,NULL AS GradeBookMustPass
                                   ,NULL AS GradeBookSysComponentTypeId
                                   ,NULL AS GradeBookHoursRequired
                                   ,NULL AS GradeBookHoursCompleted
                                   ,SE.StuEnrollId
                                   ,NULL AS MinResult
                                   ,NULL AS GradeComponentDescription -- Student data   
                                   ,SCS.CreditsAttempted AS CreditsAttempted
                                   ,SCS.CreditsEarned AS CreditsEarned
                                   ,SCS.Completed AS Completed
                                   ,SCS.CurrentScore AS CurrentScore
                                   ,SCS.CurrentGrade AS CurrentGrade
                                   ,SCS.FinalScore AS FinalScore
                                   ,SCS.FinalGrade AS FinalGrade
                                   ,C.CampusId
                                   ,C.CampDescrip
                                   ,NULL AS rownumber
                                   ,S.FirstName AS FirstName
                                   ,S.LastName AS LastName
                                   ,S.MiddleName
                                   ,(
                                      SELECT    COUNT(*) AS GrdBkWgtDetailsCount
                                      FROM      arGrdBkResults
                                      WHERE     StuEnrollId = SE.StuEnrollId
                                                AND ClsSectionId = RES.TestId
                                    ) AS GrdBkWgtDetailsCount
                                   ,CASE WHEN P.ACId = 5 THEN 'True'
                                         ELSE 'False'
                                    END AS ClockHourProgram
                          FROM      arClassSections CS
                          INNER JOIN arResults GBR ON CS.ClsSectionId = GBR.TestId
                          INNER JOIN arStuEnrollments SE ON GBR.StuEnrollId = SE.StuEnrollId
                          INNER JOIN (
                                       SELECT StudentId,FirstName,LastName,MiddleName FROM arStudent
                                     ) S ON S.StudentId = SE.StudentId
                          INNER JOIN syCampuses C ON SE.CampusId = C.CampusId
                          INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                          INNER JOIN arPrograms P ON P.ProgId = PV.ProgId
                          INNER JOIN arTerm T ON CS.TermId = T.TermId
                          INNER JOIN arReqs R ON CS.ReqId = R.ReqId
                          INNER JOIN arResults RES ON RES.StuEnrollId = GBR.StuEnrollId
                                                      AND RES.TestId = CS.ClsSectionId
                          LEFT JOIN syCreditSummary SCS ON SE.StuEnrollId = SCS.StuEnrollId
                                                           AND T.TermId = SCS.TermId
                                                           AND R.ReqId = SCS.ReqId
                          WHERE     SE.StuEnrollId = @StuEnrollId
                                    AND (
                                          @TermId IS NULL
                                          OR T.TermId = @TermId
                                        )
                                    AND R.IsAttendanceOnly = 0
                          UNION
                          SELECT DISTINCT
                                    3
                                   ,2
                                   ,PV.PrgVerId
                                   ,PV.PrgVerDescrip
                                   ,NULL
                                   ,T.TermId
                                   ,T.TermDescrip
                                   ,T.StartDate
                                   ,T.EndDate
                                   ,GBCR.ReqId
                                   ,R.Code AS CourseCode
                                   ,R.Descrip AS CourseDescrip
                                   ,'(' + R.Code + ' ) ' + R.Descrip AS CourseCodeDescrip
                                   ,R.Credits
                                   ,R.FinAidCredits
                                   ,(
                                      SELECT    MIN(MinVal)
                                      FROM      arGradeScaleDetails GCD
                                               ,arGradeSystemDetails GSD
                                      WHERE     GCD.GrdSysDetailId = GSD.GrdSysDetailId
                                                AND GSD.IsPass = 1
                                    )
                                   ,ISNULL(GBCR.Score,0)
                                   ,NULL
                                   ,NULL
                                   ,NULL
                                   ,NULL
                                   ,NULL
                                   ,NULL
                                   ,NULL
                                   ,NULL
                                   ,NULL
                                   ,NULL
                                   ,NULL
                                   ,SE.StuEnrollId
                                   ,NULL AS MinResult
                                   ,NULL AS GradeComponentDescription -- Student data    
                                   ,SCS.CreditsAttempted AS CreditsAttempted
                                   ,SCS.CreditsEarned AS CreditsEarned
                                   ,SCS.Completed AS Completed
                                   ,SCS.CurrentScore AS CurrentScore
                                   ,SCS.CurrentGrade AS CurrentGrade
                                   ,SCS.FinalScore AS FinalScore
                                   ,SCS.FinalGrade AS FinalGrade
                                   ,C.CampusId
                                   ,C.CampDescrip
                                   ,NULL AS rownumber
                                   ,S.FirstName AS FirstName
                                   ,S.LastName AS LastName
                                   ,S.MiddleName
                                   ,(
                                      SELECT    COUNT(*) AS GrdBkWgtDetailsCount
                                      FROM      arGrdBkConversionResults
                                      WHERE     StuEnrollId = SE.StuEnrollId
                                                AND TermId = GBCR.TermId
                                                AND ReqId = GBCR.ReqId
                                    ) AS GrdBkWgtDetailsCount
                                   ,CASE WHEN P.ACId = 5 THEN 'True'
                                         ELSE 'False'
                                    END AS ClockHourProgram
                          FROM      arTransferGrades GBCR
                          INNER JOIN arStuEnrollments SE ON GBCR.StuEnrollId = SE.StuEnrollId
                          INNER JOIN (
                                       SELECT StudentId,FirstName,LastName,MiddleName FROM arStudent
                                     ) S ON S.StudentId = SE.StudentId
                          INNER JOIN syCampuses C ON SE.CampusId = C.CampusId
                          INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                          INNER JOIN arPrograms P ON P.ProgId = PV.ProgId
                          INNER JOIN arTerm T ON GBCR.TermId = T.TermId
                          INNER JOIN arReqs R ON GBCR.ReqId = R.ReqId
						--INNER JOIN arTransferGrades AR ON GBCR.StuEnrollId = AR.StuEnrollId
                          LEFT JOIN syCreditSummary SCS ON SE.StuEnrollId = SCS.StuEnrollId
                                                           AND T.TermId = SCS.TermId
                                                           AND R.ReqId = SCS.ReqId
                          WHERE     SE.StuEnrollId = @StuEnrollId
                                    AND (
                                          @TermId IS NULL
                                          OR T.TermId = @TermId
                                        )
                                    AND R.IsAttendanceOnly = 0
                        ) dt
            ) dt1
		--WHERE rownumber2<=2
    ORDER BY TermStartDate
           ,TermEndDate
           ,TermDescription
           ,CourseCode
           ,CASE WHEN LTRIM(RTRIM(LOWER(@GradesFormat))) = 'letter' THEN ( RANK() OVER ( ORDER BY FinalGrade DESC ) )
            END
           ,CASE WHEN LTRIM(RTRIM(LOWER(@GradesFormat))) <> 'letter' THEN ( RANK() OVER ( ORDER BY FinalScore DESC ) )
            END;          


GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'If Exists sp usp_StudentSummaryFormConversionHours, delete it '
GO
IF EXISTS ( SELECT  1 
            FROM    sys.objects 
            WHERE   object_id = OBJECT_ID(N'[dbo].[usp_StudentSummaryFormConversionHours]') 
                    AND type IN ( N'P',N'PC' ) ) 
    BEGIN 
		PRINT N'    Dropping SP usp_StudentSummaryFormConversionHours'; 
        DROP PROCEDURE usp_StudentSummaryFormConversionHours; 
    END; 
GO 
IF @@ERROR <> 0 
    SET NOEXEC ON; 
GO 
PRINT N'Creating usp_StudentSummaryFormConversionHours'; 
GO 
CREATE PROCEDURE [dbo].[usp_StudentSummaryFormConversionHours]
    (
     @stuEnrollId UNIQUEIDENTIFIER
    ,@cutOffDate DATETIME
    )
AS
    SET NOCOUNT ON;

     --Select Distinct 
     --                  SCA.StuEnrollId, 
     --                  (select Sum(Schedule) from atConversionAttendance 
     --                  where StuEnrollId = SCA.StuEnrollId and  (Actual is not null and Actual <> 999.00 and Actual <> 9999.00)) as SchedHours,
     --                  (select Sum(Actual) from atConversionAttendance 
     --                  where StuEnrollId = SCA.StuEnrollId and ( Actual <> 999.00 and Actual <> 9999.00)) as TotalPresentHours,
     --                  (select Sum(Schedule-Actual) from atConversionAttendance 
     --                  where StuEnrollId = SCA.StuEnrollId and  (Actual <> 999.00 and Actual <> 9999.00)and (ActualHours < SchedHours)) 
     --                  as TotalHoursAbsent,
     --                  (select Sum(Actual-Schedule) from atConversionAttendance 
     --                  where StuEnrollId = SCA.StuEnrollId and 
     --                  ((Actual <> 999.00 and Actual <> 9999.00) and (ActualHours >SchedHours)))
     --                  as TotalMakeUpHours,
     --                  (select Sum(t4.Total) from 
     --                  arStuEnrollments t1,arStudentSchedules t2, arProgSchedules t3, arProgScheduleDetails t4 
     --                  where t1.StuEnrollId = t2.StuEnrollId and t1.PrgVerId=t3.PrgVerId 
     --                  and t2.ScheduleId=t3.ScheduleId and t3.scheduleId=t4.ScheduleId and 
     --                  t2.StuEnrollId=SCA.StuEnrollId  and t4.total is not null) as ScheduleHoursByWeek, 
     --                 (select max(MeetDate) from atConversionAttendance 
     --                  where StuEnrollId = SCA.StuEnrollId and (Actual>=1.00 and Actual <> 999.00 and Actual <> 9999.00)) as LastDateAttended, 
     --                  (select count(*) from  atConversionAttendance where StuEnrollId = SCA.StuEnrollId and Tardy=1) as TardyCount 
     --                  from atConversionAttendance SCA 
     --                  where SCA.StuEnrollId=@stuEnrollId
	 --					and SCA.MeetDate <=@cutOffDate  order by Schedhours desc,TotalPresentHours desc 


GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Refreshing [dbo].[NewStudentSearch]';
GO
IF OBJECT_ID(N'[dbo].[NewStudentSearch]', 'V') IS NOT NULL
    EXEC sp_refreshview N'[dbo].[NewStudentSearch]';
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Refreshing [dbo].[VIEW_GetHistory]';
GO
IF OBJECT_ID(N'[dbo].[VIEW_GetHistory]', 'V') IS NOT NULL
    EXEC sp_refreshview N'[dbo].[VIEW_GetHistory]';
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Refreshing [dbo].[SettingStdScoreLimit]';
GO
IF OBJECT_ID(N'[dbo].[SettingStdScoreLimit]', 'V') IS NOT NULL
    EXEC sp_refreshview N'[dbo].[SettingStdScoreLimit]';
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Refreshing [dbo].[StudentEnrollmentSearch]';
GO
IF OBJECT_ID(N'[dbo].[StudentEnrollmentSearch]', 'V') IS NOT NULL
    EXEC sp_refreshview N'[dbo].[StudentEnrollmentSearch]';
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Refreshing [dbo].[VIEW1]';
GO
IF OBJECT_ID(N'[dbo].[VIEW1]', 'V') IS NOT NULL
    EXEC sp_refreshview N'[dbo].[VIEW1]';
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO

PRINT N'If Exists trigger adLeads_Audit_Delete on adLeads, delete it';
GO
IF EXISTS ( SELECT  1
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'[adLeads_Audit_Delete]')
                    AND type IN (N'TR') )
    BEGIN
		PRINT N'    Dropping trigger  adLeads_Audit_Delete on adLeads';
        DROP TRIGGER [adLeads_Audit_Delete]
    END
GO
PRINT N'Creating trigger  adLeads_Audit_Delete ON adLeads';
GO
CREATE TRIGGER [dbo].[adLeads_Audit_Delete] ON [dbo].[adLeads]
    FOR DELETE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID();
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Deleted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Deleted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Deleted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'adLeads','D',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadId
                               ,'LastName'
                               ,CONVERT(VARCHAR(8000),Old.LastName,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadId
                               ,'MiddleName'
                               ,CONVERT(VARCHAR(8000),Old.MiddleName,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadId
                               ,'HomeEmail'
                               ,CONVERT(VARCHAR(8000),Old.HomeEmail,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadId
                               ,'City'
                               ,CONVERT(VARCHAR(8000),Old.City,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadId
                               ,'LeadStatus'
                               ,CONVERT(VARCHAR(8000),Old.LeadStatus,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadId
                               ,'AddressType'
                               ,CONVERT(VARCHAR(8000),Old.AddressType,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
               (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadId
                               ,'Prefix'
                               ,CONVERT(VARCHAR(8000),Old.Prefix,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadId
                               ,'BirthDate'
                               ,CONVERT(VARCHAR(8000),Old.BirthDate,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadId
                               ,'AssignedDate'
                               ,CONVERT(VARCHAR(8000),Old.AssignedDate,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadId
                               ,'MaritalStatus'
                               ,CONVERT(VARCHAR(8000),Old.MaritalStatus,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadId
                               ,'PhoneType'
                               ,CONVERT(VARCHAR(8000),Old.PhoneType,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadId
                               ,'SourceCategoryID'
                               ,CONVERT(VARCHAR(8000),Old.SourceCategoryID,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadId
                               ,'SourceTypeID'
                               ,CONVERT(VARCHAR(8000),Old.SourceTypeID,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadId
                               ,'AreaId'
                               ,CONVERT(VARCHAR(8000),Old.AreaID,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadId
                               ,'ShiftId'
                               ,CONVERT(VARCHAR(8000),Old.ShiftID,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadId
                               ,'DrivLicStateID'
                               ,CONVERT(VARCHAR(8000),Old.DrivLicStateID,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadId
                               ,'Comments'
                               ,CONVERT(VARCHAR(8000),Old.Comments,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadId
                               ,'CampusId'
                               ,CONVERT(VARCHAR(8000),Old.CampusId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadId
                               ,'PrgVerId'
                               ,CONVERT(VARCHAR(8000),Old.PrgVerId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadId
                               ,'Age'
                               ,CONVERT(VARCHAR(8000),Old.Age,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadId
                               ,'PreviousEducation'
                               ,CONVERT(VARCHAR(8000),Old.PreviousEducation,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadId
                               ,'CreatedDate'
                               ,CONVERT(VARCHAR(8000),Old.CreatedDate,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadId
                               ,'Address1'
                               ,CONVERT(VARCHAR(8000),Old.Address1,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadId
                               ,'Sponsor'
                               ,CONVERT(VARCHAR(8000),Old.Sponsor,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadId
                               ,'Gender'
                               ,CONVERT(VARCHAR(8000),Old.Gender,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadId
                               ,'ProgramID'
                               ,CONVERT(VARCHAR(8000),Old.ProgramID,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadId
                               ,'Nationality'
                               ,CONVERT(VARCHAR(8000),Old.Nationality,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadId
                               ,'FirstName'
                               ,CONVERT(VARCHAR(8000),Old.FirstName,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadId
                               ,'StateId'
                               ,CONVERT(VARCHAR(8000),Old.StateId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadId
                               ,'WorkEmail'
                               ,CONVERT(VARCHAR(8000),Old.WorkEmail,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadId
                               ,'FamilyIncome'
                               ,CONVERT(VARCHAR(8000),Old.FamilyIncome,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadId
                               ,'PhoneStatus'
                               ,CONVERT(VARCHAR(8000),Old.PhoneStatus,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadId
                               ,'DrivLicNumber'
                               ,CONVERT(VARCHAR(8000),Old.DrivLicNumber,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadId
                               ,'SourceAdvertisement'
                               ,CONVERT(VARCHAR(8000),Old.SourceAdvertisement,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadId
                               ,'County'
                               ,CONVERT(VARCHAR(8000),Old.County,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadId
                               ,'Phone'
                               ,CONVERT(VARCHAR(8000),Old.Phone,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadId
                               ,'SSN'
                               ,CONVERT(VARCHAR(8000),Old.SSN,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadId
                               ,'Suffix'
                               ,CONVERT(VARCHAR(8000),Old.Suffix,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadId
                               ,'AdmissionsRep'
                               ,CONVERT(VARCHAR(8000),Old.AdmissionsRep,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadId
                               ,'SourceDate'
                               ,CONVERT(VARCHAR(8000),Old.SourceDate,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadId
                               ,'ExpectedStart'
                               ,CONVERT(VARCHAR(8000),Old.ExpectedStart,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadId
                               ,'Country'
                               ,CONVERT(VARCHAR(8000),Old.Country,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadId
                               ,'AddressStatus'
                               ,CONVERT(VARCHAR(8000),Old.AddressStatus,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadId
                               ,'Address2'
                               ,CONVERT(VARCHAR(8000),Old.Address2,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadId
                               ,'Zip'
                               ,CONVERT(VARCHAR(8000),Old.Zip,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadId
                               ,'Race'
                               ,CONVERT(VARCHAR(8000),Old.Race,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadId
                               ,'Children'
                               ,CONVERT(VARCHAR(8000),Old.Children,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadId
                               ,'Citizen'
                               ,CONVERT(VARCHAR(8000),Old.Citizen,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadId
                               ,'AlienNumber'
                               ,CONVERT(VARCHAR(8000),Old.AlienNumber,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadId
                               ,'RecruitmentOffice'
                               ,CONVERT(VARCHAR(8000),Old.RecruitmentOffice,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadId
                               ,'OtherState'
                               ,CONVERT(VARCHAR(8000),Old.OtherState,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadId
                               ,'ForeignZip'
                               ,CONVERT(VARCHAR(8000),Old.ForeignZip,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.LeadId
                               ,'ForeignPhone'
                               ,CONVERT(VARCHAR(8000),Old.ForeignPhone,121)
                        FROM    Deleted Old; 
            END; 
        END;



    SET NOCOUNT OFF;

GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO

PRINT N'If Exists trigger TR_ExternshipAttendance on arExternshipAttendance, delete it';
GO
IF EXISTS ( SELECT  1
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'[TR_ExternshipAttendance]')
                    AND type IN (N'TR') )
    BEGIN
		PRINT N'    Dropping trigger  TR_ExternshipAttendance on arExternshipAttendance';
        DROP TRIGGER [TR_ExternshipAttendance]
    END
GO
PRINT N'Creating trigger  TR_ExternshipAttendance ON arExternshipAttendance';
GO
--==========================================================================================
-- TRIGGER TR_ExternshipAttendance
-- AFTER UPDATE  
--==========================================================================================
CREATE TRIGGER [dbo].[TR_ExternshipAttendance] ON [dbo].[arExternshipAttendance]
    AFTER INSERT,UPDATE
AS
    SET NOCOUNT ON; 

    DECLARE @PrgVerId UNIQUEIDENTIFIER
       ,@rownumber INT
       ,@StuEnrollId UNIQUEIDENTIFIER;
    DECLARE @PrevStuEnrollId UNIQUEIDENTIFIER
       ,@PrevReqId UNIQUEIDENTIFIER
       ,@PrevTermId UNIQUEIDENTIFIER
       ,@CreditsAttempted DECIMAL(18,2)
       ,@CreditsEarned DECIMAL(18,2)
       ,@TermId UNIQUEIDENTIFIER
       ,@TermDescrip VARCHAR(50);
    DECLARE @reqid UNIQUEIDENTIFIER
       ,@CourseCodeDescrip VARCHAR(50)
       ,@FinalGrade UNIQUEIDENTIFIER
       ,@FinalScore DECIMAL(18,2)
       ,@ClsSectionId UNIQUEIDENTIFIER
       ,@Grade VARCHAR(50)
       ,@IsGradeBookNotSatisified BIT
       ,@TermStartDate DATETIME;
    DECLARE @IsPass BIT
       ,@IsCreditsAttempted BIT
       ,@IsCreditsEarned BIT
       ,@Completed BIT
       ,@CurrentScore DECIMAL(18,2)
       ,@CurrentGrade VARCHAR(10)
       ,@FinalGradeDesc VARCHAR(50)
       ,@FinalGPA DECIMAL(18,2)
       ,@GrdBkResultId UNIQUEIDENTIFIER;
    DECLARE @Product_WeightedAverage_Credits_GPA DECIMAL(18,2)
       ,@Count_WeightedAverage_Credits DECIMAL(18,2)
       ,@Product_SimpleAverage_Credits_GPA DECIMAL(18,2)
       ,@Count_SimpleAverage_Credits DECIMAL(18,2);
    DECLARE @CreditsPerService DECIMAL(18,2)
       ,@NumberOfServicesAttempted INT
       ,@boolCourseHasLabWorkOrLabHours INT
       ,@sysComponentTypeId INT
       ,@RowCount INT;
    DECLARE @decGPALoop DECIMAL(18,2)
       ,@intCourseCount INT
       ,@decWeightedGPALoop DECIMAL(18,2)
       ,@IsInGPA BIT
       ,@isGradeEligibleForCreditsEarned BIT
       ,@isGradeEligibleForCreditsAttempted BIT;
    DECLARE @ComputedSimpleGPA DECIMAL(18,2)
       ,@ComputedWeightedGPA DECIMAL(18,2)
       ,@CourseCredits DECIMAL(18,2);
    DECLARE @FinAidCreditsEarned DECIMAL(18,2)
       ,@FinAidCredits DECIMAL(18,2)
       ,@TermAverage DECIMAL(18,2)
       ,@TermAverageCount INT;
    DECLARE @IsWeighted INT;
    DECLARE @CampusId UNIQUEIDENTIFIER;
    SET @decGPALoop = 0;
    SET @intCourseCount = 0;
    SET @decWeightedGPALoop = 0;
    SET @ComputedSimpleGPA = 0;
    SET @ComputedWeightedGPA = 0;
    SET @CourseCredits = 0;
    DECLARE GetExternshipAttendance_Cursor CURSOR
    FOR
        SELECT	DISTINCT
                SE.StuEnrollId
               ,T.TermId
               ,T.TermDescrip
               ,T.StartDate
               ,R.ReqId
               ,'(' + R.Code + ' ) ' + R.Descrip AS CourseCodeDescrip
               ,RES.Score AS FinalScore
               ,RES.GrdSysDetailId AS FinalGrade
               ,GCT.SysComponentTypeId
               ,R.Credits AS CreditsAttempted
               ,CS.ClsSectionId
               ,GSD.Grade
               ,GSD.IsPass
               ,GSD.IsCreditsAttempted
               ,GSD.IsCreditsEarned
               ,SE.PrgVerId
               ,GSD.IsInGPA
               ,R.FinAidCredits AS FinAidCredits
        FROM    arStuEnrollments SE
        INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
        INNER JOIN arResults RES ON SE.StuEnrollId = RES.StuEnrollId
        INNER JOIN arExternshipAttendance RES1 ON RES1.StuEnrollId = SE.StuEnrollId
        INNER JOIN Inserted t4 ON RES1.ExternshipAttendanceId = t4.ExternshipAttendanceId
        INNER JOIN arClassSections CS ON RES.TestId = CS.ClsSectionId
        INNER JOIN arTerm T ON CS.TermId = T.TermId
        INNER JOIN arReqs R ON CS.ReqId = R.ReqId
        LEFT JOIN arGrdBkResults GBR ON CS.ClsSectionId = GBR.ClsSectionId
        LEFT JOIN arGrdBkWgtDetails GBWD ON GBR.InstrGrdBkWgtDetailId = GBWD.InstrGrdBkWgtDetailId
        LEFT JOIN arGrdComponentTypes GCT ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
        LEFT JOIN arGradeSystemDetails GSD ON RES.GrdSysDetailId = GSD.GrdSysDetailId
        WHERE   --SE.StuENrollId='9ED10C7C-0DFE-4549-9FC8-C5E1B7DCCD1D' AND 
                GCT.SysComponentTypeId = 544
        ORDER BY T.StartDate
               ,T.TermDescrip
               ,R.ReqId; 
    OPEN GetExternshipAttendance_Cursor;
    SET @PrevStuEnrollId = NULL;
    SET @PrevTermId = NULL;
    SET @PrevReqId = NULL;
    SET @RowCount = 0;
    FETCH NEXT FROM GetExternshipAttendance_Cursor INTO @StuEnrollId,@TermId,@TermDescrip,@TermStartDate,@reqid,@CourseCodeDescrip,@FinalScore,@FinalGrade,
        @sysComponentTypeId,@CreditsAttempted,@ClsSectionId,@Grade,@IsPass,@IsCreditsAttempted,@IsCreditsEarned,@PrgVerId,@IsInGPA,@FinAidCredits; --,@GrdBkResultId
    WHILE @@FETCH_STATUS = 0
        BEGIN
				
            SET @CampusId = (
                              SELECT    CampusId
                              FROM      dbo.arStuEnrollments
                              WHERE     StuEnrollId = @StuEnrollId
                            );
            SET @CourseCredits = @CreditsAttempted; 
            SET @RowCount = @RowCount + 1;
			-- Changes made on 12/28/2010 starts here
            DECLARE @ShowROSSOnlyTabsForStudent_Value BIT
               ,@SetGradeBookAt VARCHAR(50)
               ,@GradesFormat VARCHAR(50);
            SET @ShowROSSOnlyTabsForStudent_Value = (
                                                      SELECT    dbo.GetAppSettingValue(68,@CampusId)
                                                    );
            SET @SetGradeBookAt = (
                                    SELECT  dbo.GetAppSettingValue(43,@CampusId)
                                  );
            SET @GradesFormat = (
                                  SELECT    dbo.GetAppSettingValue(47,@CampusId)
                                ); -- 47 refers to grades format
            SET @FinalGradeDesc = @FinalGrade;
				-- Changes made on 12/28/2010 ends here
            SET @IsGradeBookNotSatisified = (
                                              SELECT    COUNT(*) AS UnsatisfiedWorkUnits
                                              FROM      (
                                                          SELECT DISTINCT
                                                                    D.*
                                                                   ,
															-- Changes made on 12/28/2010 starts here
                                                                    CASE WHEN D.MinimumScore > D.Score THEN ( D.MinimumScore - D.Score )
                                                                         ELSE 0
                                                                    END AS Remaining
                                                                   ,CASE WHEN ( D.MinimumScore > D.Score )
                                                                              AND ( D.MustPass = 1 ) THEN 0
                                                                         WHEN (
                                                                                SysComponentTypeId = 500
                                                                                OR SysComponentTypeId = 503
                                                                                OR SysComponentTypeId = 544
                                                                              )
                                                                              AND ( @ShowROSSOnlyTabsForStudent_Value = 0 )
                                                                              AND ( D.Score IS NOT NULL )
                                                                              AND ( D.MinimumScore > D.Score ) THEN 0
															--Non Ross/ Course Level/ No LabHrLabWorkExternship/Numeric/FinalScore is not posted
                                                                         WHEN @ShowROSSOnlyTabsForStudent_Value = 0
                                                                              AND LOWER(LTRIM(RTRIM(@SetGradeBookAt))) = 'courselevel'
                                                                              AND LOWER(LTRIM(RTRIM(@GradesFormat))) <> 'letter'
                                                                              AND (
                                                                                    SysComponentTypeId IS NULL
                                                                                    OR (
                                                                                         SysComponentTypeId <> 500
                                                                                         AND SysComponentTypeId <> 503
                                                                                         AND SysComponentTypeId <> 544
                                                                                       )
                                                                                  )
                                                                              AND @FinalScore IS NULL THEN 0
															--Non Ross/ Course Level/ No LabHrLabWorkExternship/Letter/Final Grade is not posted
                                                                         WHEN @ShowROSSOnlyTabsForStudent_Value = 0
                                                                              AND LOWER(LTRIM(RTRIM(@SetGradeBookAt))) = 'courselevel'
                                                                              AND LOWER(LTRIM(RTRIM(@GradesFormat))) = 'letter'
                                                                              AND (
                                                                                    SysComponentTypeId IS NULL
                                                                                    OR (
                                                                                         SysComponentTypeId <> 500
                                                                                         AND SysComponentTypeId <> 503
                                                                                         AND SysComponentTypeId <> 544
                                                                                       )
                                                                                  )
                                                                              AND @FinalGradeDesc IS NULL THEN 0
                                                                         WHEN @ShowROSSOnlyTabsForStudent_Value = 1
                                                                              AND D.Score IS NULL
                                                                              AND D.Required = 1 THEN 0
                                                                         WHEN D.Score IS NULL
                                                                              AND D.FinalScore IS NULL
                                                                              AND ( D.Required = 1 ) THEN 0
                                                                         ELSE 1
                                                                    END AS IsWorkUnitSatisfied
															-- Changes made on 12/28/2010 ends here
															--Case When D.MinimumScore > D.Score Then (D.MinimumScore-D.Score) else 0 end as Remaining,
															--Case When (D.MinimumScore > D.Score) And (D.MustPass=1) then 0
															--When (SysComponentTypeId=500 OR SysComponentTypeId=503 OR SysComponentTypeId=544) AND (@ShowROSSOnlyTabsForStudent_Value=0) and (D.Score is not null) and (D.MinimumScore > D.Score) then 0
															--When (@ShowROSSOnlyTabsForStudent_Value=0) and (D.MinimumScore > D.Score) then 0
															--When (@ShowROSSOnlyTabsForStudent_Value=1) and (D.Score is not null) and (D.MinimumScore > D.Score) and D.Required=1 then 0
															--When @ShowROSSOnlyTabsForStudent_Value=1 and D.Score is null and D.Required=1 then 0 
															--When D.Score is Null and D.FinalScore is null And (D.Required=1) then 0 
															--else 1 end as IsWorkUnitSatisfied
															--Case When D.MinimumScore > D.Score Then (D.MinimumScore-D.Score) else 0 end as Remaining,
															--Case When (D.MinimumScore > D.Score) And (D.MustPass=1) then 0 
															--When (SysComponentTypeId=500 OR SysComponentTypeId=503 OR SysComponentTypeId=544) AND (@ShowROSSOnlyTabsForStudent_Value=0) and (D.Score is not null) and (D.MinimumScore > D.Score) then 0
															----When (@ShowROSSOnlyTabsForStudent_Value=0) and (D.Score is not null) and (D.MinimumScore > D.Score) then 0
															----When (@ShowROSSOnlyTabsForStudent_Value=1) and (D.Score is not null) and (D.MinimumScore > D.Score) and D.Required=1 then 0
															--When @ShowROSSOnlyTabsForStudent_Value=1 and D.Score is null and D.Required=1 then 0
															--When D.Score is Null and D.FinalScore is null And (D.Required=1) then 0
															--else 1 end as IsWorkUnitSatisfied
                                                          FROM      (
                                                                      SELECT	DISTINCT
                                                                                T.TermId
                                                                               ,T.TermDescrip
                                                                               ,'(' + R.Code + ' ) ' + R.Descrip AS CourseCodeDescrip
                                                                               ,R.ReqId
                                                                               ,RTRIM(GCT.Descrip) AS GradeBookDescription
                                                                               ,( CASE WHEN GCT.SysComponentTypeId IN ( 500,503,544 ) THEN GBWD.Number
                                                                                       ELSE (
                                                                                              SELECT    MIN(MinVal)
                                                                                              FROM      arGradeScaleDetails GSD
                                                                                                       ,arGradeSystemDetails GSS
                                                                                              WHERE     GSD.GrdSysDetailId = GSS.GrdSysDetailId
                                                                                                        AND GSS.IsPass = 1
                                                                                            )
                                                                                  END ) AS MinimumScore
                                                                               ,
                														--GBR.Score as Score,  
                                                                                GBWD.Weight AS Weight
                                                                               ,RES.Score AS FinalScore
                                                                               ,RES.GrdSysDetailId AS FinalGrade
                                                                               ,GBWD.Required
                                                                               ,GBWD.MustPass
                                                                               ,GBWD.GrdPolicyId
          ,( CASE GCT.SysComponentTypeId
                                                                                    WHEN 544 THEN (
                                                                                                    SELECT  SUM(HoursAttended)
                                                                                                    FROM    arExternshipAttendance
                                                                                                    WHERE   StuEnrollId = SE.StuEnrollId
                                                                                                  )
                                                                                    WHEN 503
                                                                                    THEN (
                                                                                           SELECT   SUM(Score)
                                                                                           FROM     arGrdBkResults
                                                                                           WHERE    StuEnrollId = SE.StuEnrollId
                                                                                                    AND ClsSectionId = CS.ClsSectionId
                                                                                                    AND InstrGrdBkWgtDetailId = GBWD.InstrGrdBkWgtDetailId
                                                                                         )
                                                                                    ELSE GBR.Score
                                                                                  END ) AS Score
                                                                               ,GCT.SysComponentTypeId
                                                                               ,SE.StuEnrollId
                                                                               ,
																		--GBR.GrdBkResultId,
                                                                                R.Credits AS CreditsAttempted
                                                                               ,CS.ClsSectionId
                                                                               ,GSD.Grade
                                                                               ,GSD.IsPass
                                                                               ,GSD.IsCreditsAttempted
                                                                               ,GSD.IsCreditsEarned
                                                                      FROM      arStuEnrollments SE
                                                                      INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                                                                      INNER JOIN arResults RES ON RES.StuEnrollId = SE.StuEnrollId
                                                                      INNER JOIN arExternshipAttendance RES1 ON RES1.StuEnrollId = SE.StuEnrollId
                                                                      INNER JOIN Inserted t4 ON RES1.ExternshipAttendanceId = t4.ExternshipAttendanceId
                                                                      INNER JOIN arClassSections CS ON RES.TestId = CS.ClsSectionId
                                                                      INNER JOIN arTerm T ON CS.TermId = T.TermId
                                                                      INNER JOIN arReqs R ON CS.ReqId = R.ReqId
                                                                      LEFT JOIN arGrdBkResults GBR ON CS.ClsSectionId = GBR.ClsSectionId
                                                                      LEFT JOIN arGrdBkWgtDetails GBWD ON GBR.InstrGrdBkWgtDetailId = GBWD.InstrGrdBkWgtDetailId
                                                                      LEFT JOIN arGrdComponentTypes GCT ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
                                                                      LEFT JOIN arGradeSystemDetails GSD ON RES.GrdSysDetailId = GSD.GrdSysDetailId
                                                                      WHERE     SE.StuEnrollId = @StuEnrollId
                                                                                AND T.TermId = @TermId
                                                                                AND R.ReqId = @reqid
                                                                                AND GCT.SysComponentTypeId = 544
                                                                    ) D
                                                        ) E
                                              WHERE     IsWorkUnitSatisfied = 0
                                            ); 
														
            SET @IsWeighted = (
                                SELECT  COUNT(*) AS WeightsCount
                                FROM    (
                                          SELECT    T.TermId
                                                   ,T.TermDescrip
                                                   ,'(' + R.Code + ' ) ' + R.Descrip AS CourseCodeDescrip
                                                   ,R.ReqId
                                                   ,RTRIM(GCT.Descrip) AS GradeBookDescription
                                                   ,( CASE WHEN GCT.SysComponentTypeId IN ( 500,503,544 ) THEN GBWD.Number
                                                           ELSE (
                                                                  SELECT    MIN(MinVal)
                                                                  FROM      arGradeScaleDetails GSD
                                                                           ,arGradeSystemDetails GSS
                                                                  WHERE     GSD.GrdSysDetailId = GSS.GrdSysDetailId
                                                                            AND GSS.IsPass = 1
                                                                )
                                                      END ) AS MinimumScore
                                                   ,GBR.Score AS Score
                                                   ,GBWD.Weight AS Weight
                                                   ,RES.Score AS FinalScore
                                                   ,RES.GrdSysDetailId AS FinalGrade
                                                   ,GBWD.Required
                                                   ,GBWD.MustPass
                                                   ,GBWD.GrdPolicyId
                                                   ,( CASE GCT.SysComponentTypeId
                                                        WHEN 544 THEN (
                                                                        SELECT  SUM(HoursAttended)
                                                                        FROM    arExternshipAttendance
                                                                        WHERE   StuEnrollId = SE.StuEnrollId
                                                                      )
                                                        ELSE GBR.Score
                                                      END ) AS GradeBookResult
                                                   ,GCT.SysComponentTypeId
                                                   ,SE.StuEnrollId
                                                   ,GBR.GrdBkResultId
                                                   ,R.Credits AS CreditsAttempted
                                                   ,CS.ClsSectionId
                           ,GSD.Grade
                                                   ,GSD.IsPass
                                                   ,GSD.IsCreditsAttempted
                                                   ,GSD.IsCreditsEarned
                                          FROM      arStuEnrollments SE
                                          INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                                          INNER JOIN arResults RES ON RES.StuEnrollId = SE.StuEnrollId
                                          INNER JOIN arExternshipAttendance RES1 ON RES1.StuEnrollId = SE.StuEnrollId
                                          INNER JOIN Inserted t4 ON RES1.ExternshipAttendanceId = t4.ExternshipAttendanceId
                                          INNER JOIN arClassSections CS ON RES.TestId = CS.ClsSectionId
                                          INNER JOIN arTerm T ON CS.TermId = T.TermId
                                          INNER JOIN arReqs R ON CS.ReqId = R.ReqId
                                          LEFT JOIN arGrdBkResults GBR ON CS.ClsSectionId = GBR.ClsSectionId
                                                                          AND GBR.StuEnrollId = SE.StuEnrollId
                                          LEFT JOIN arGrdBkWgtDetails GBWD ON GBR.InstrGrdBkWgtDetailId = GBWD.InstrGrdBkWgtDetailId
                                          LEFT JOIN arGrdComponentTypes GCT ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
                                          LEFT JOIN arGradeSystemDetails GSD ON RES.GrdSysDetailId = GSD.GrdSysDetailId
                                          WHERE     SE.StuEnrollId = @StuEnrollId
                                                    AND T.TermId = @TermId
                                                    AND R.ReqId = @reqid
                                                    AND GCT.SysComponentTypeId = 544
                                        ) D
                                WHERE   Weight >= 1
                              );	
														
				--Check if IsCreditsAttempted is set to True
            IF (
                 @IsCreditsAttempted IS NULL
                 OR @IsCreditsAttempted = 0
               )
                BEGIN
                    SET @CreditsAttempted = 0;
                END;
            IF (
                 @IsCreditsEarned IS NULL
                 OR @IsCreditsEarned = 0
               )
                BEGIN
                    SET @CreditsEarned = 0;
                END;		
				

            IF ( @IsGradeBookNotSatisified >= 1 )
                BEGIN
                    SET @CreditsEarned = 0;
                    SET @Completed = 0;
                END;
            ELSE
                BEGIN
                    SET @GrdBkResultId = (
                                           SELECT TOP 1
                                                    GrdBkResultId
                                           FROM     arGrdBkResults
                                           WHERE    StuEnrollId = @StuEnrollId
                                                    AND ClsSectionId = @ClsSectionId
                                         ); 
                    IF @GrdBkResultId IS NOT NULL
                        BEGIN
                            IF ( @IsCreditsEarned = 1 )
                                BEGIN
                                    SET @CreditsEarned = @CreditsAttempted;
                                    SET @FinAidCreditsEarned = @FinAidCredits;
                                END; 
                            SET @Completed = 1;										
                        END;
                    IF (
                         @GrdBkResultId IS NULL
                         AND @Grade IS NOT NULL
                       )
                        BEGIN
                            IF ( @IsCreditsEarned = 1 )
                   BEGIN
                                    SET @CreditsEarned = @CreditsAttempted;
                                    SET @FinAidCreditsEarned = @FinAidCredits;
                                END; 
                            SET @Completed = 1;	
                        END;
                END;

				------ Print  'Completed='
				------ Print  @Completed
				
            IF (
                 @FinalScore IS NOT NULL
                 AND @Grade IS NOT NULL
               )
                BEGIN
                    IF ( @IsCreditsEarned = 1 )
                        BEGIN
                            SET @CreditsEarned = @CreditsAttempted;
                            SET @FinAidCreditsEarned = @FinAidCredits;
                        END; 
                    SET @Completed = 1;
				
                END;
				
				-- If course is not part of the program version definition do not add credits earned and credits attempted
				-- set the credits earned and attempted to zero
            DECLARE @coursepartofdefinition INT;
            SET @coursepartofdefinition = 0;
				
            SET @coursepartofdefinition = (
                                            SELECT  COUNT(*) AS RowCountOfProgramDefinition
                                            FROM    (
                                                      SELECT    *
                                                      FROM      arProgVerDef
                                                      WHERE     PrgVerId = @PrgVerId
                                                                AND ReqId = @reqid
                                                      UNION
                                                      SELECT    *
                                                      FROM      arProgVerDef
                                                      WHERE     PrgVerId = @PrgVerId
                                                                AND ReqId IN ( SELECT   GrpId
                                                                               FROM     arReqGrpDef
                                                                               WHERE    ReqId = @reqid )
                                                    ) dt
                                          );
            IF ( @coursepartofdefinition = 0 )
                BEGIN
                    SET @CreditsEarned = 0;
                    SET @CreditsAttempted = 0;
                    SET @FinAidCreditsEarned = 0;
                END;
               
				-- Check the grade scale associated with the class section and figure out of the final score was a passing score
            DECLARE @coursepassrowcount INT;
            SET @coursepassrowcount = 0;
            IF ( @FinalScore IS NOT NULL )
					
					-- If the student scores 56 and the score is a passing score then we consider this course as completed
                BEGIN
                    SET @coursepassrowcount = (
                                                SELECT  COUNT(t2.MinVal) AS IsCourseCompleted
                                                FROM    arClassSections t1
                                                INNER JOIN arGradeScaleDetails t2 ON t1.GrdScaleId = t2.GrdScaleId
                                                INNER JOIN arGradeSystemDetails t3 ON t2.GrdSysDetailId = t3.GrdSysDetailId
                                                WHERE   t1.ClsSectionId = @ClsSectionId
                                                        AND t3.IsPass = 1
                                                        AND @FinalScore >= t2.MinVal
                                              );
                    IF @coursepassrowcount >= 1
                        BEGIN
                            SET @Completed = 1;
                        END;
                    ELSE
                        BEGIN
                            SET @Completed = 0;
                        END;
                END;

				-- If Student Scored a Failing Grade (IsPass set to 0 in Grade System)
				-- then mark this course as Incomplete
            IF ( @FinalGrade IS NOT NULL )
                BEGIN
                    IF ( @IsPass = 0 )
                        BEGIN
                            SET @Completed = 0;
                            IF ( @IsCreditsEarned = 0 )
                                BEGIN
                                    SET @CreditsEarned = 0; 
                                    SET @FinAidCreditsEarned = 0;
                                END; 
                            IF ( @IsCreditsAttempted = 0 )
                                BEGIN
                                    SET @CreditsAttempted = 0;
                                END;
                        END;
                END;
				------ Print  'Completed='
				------ Print  @Completed
            SET @CurrentScore = (
                                  SELECT    CASE WHEN SUM(GradeBookWeight) > 0 THEN ( SUM(ActualWeight) / SUM(GradeBookWeight) ) * 100
                                                 ELSE NULL
                                            END AS CurrentScore
                                  FROM      (
                                              SELECT    InstrGrdBkWgtDetailId
                                                       ,Code
                                                       ,Descrip
                                                       ,Weight AS GradeBookWeight
                                                       ,CASE WHEN S.NumberOfComponents > 0 THEN SUM(( Weight * ( Score / NumberOfComponents ) ) / 100)
                                                             ELSE 0
                                                        END AS ActualWeight
                                              FROM      (
                                                          SELECT    C.InstrGrdBkWgtDetailId
                                                                   ,D.Code
                                                                   ,D.Descrip
                                                                   ,ISNULL(C.Weight,0) AS Weight
                                                                   ,C.Number AS MinNumber
                                                                   ,C.GrdPolicyId
                                                                   ,C.Parameter AS Param
                                                                   ,X.GrdScaleId
                                                                   ,SUM(GR.Score) AS Score
                                                                   ,COUNT(D.Descrip) AS NumberOfComponents
                                                          FROM      (
                                                                      SELECT DISTINCT TOP 1
                                                                                A.InstrGrdBkWgtId
                                                                               ,A.EffectiveDate
                                                                               ,B.GrdScaleId
                                                                      FROM      arGrdBkWeights A
                                                                               ,arClassSections B
                                                                      WHERE     A.ReqId = B.ReqId
                                                                                AND A.EffectiveDate <= B.StartDate
                                                                                AND B.ClsSectionId = @ClsSectionId
                                                                      ORDER BY  A.EffectiveDate DESC
                                                                    ) X
                                                  ,arGrdBkWgtDetails C
                                                                   ,arGrdComponentTypes D
                                                                   ,arGrdBkResults GR
                                                          WHERE     X.InstrGrdBkWgtId = C.InstrGrdBkWgtId
                                                                    AND C.GrdComponentTypeId = D.GrdComponentTypeId
                                                                    AND C.InstrGrdBkWgtDetailId = GR.InstrGrdBkWgtDetailId
                                                                    AND D.SysComponentTypeId NOT IN ( 500,503 )
                                                                    AND GR.StuEnrollId = @StuEnrollId
                                                                    AND GR.ClsSectionId = @ClsSectionId
                                                                    AND GR.Score IS NOT NULL
                                                          GROUP BY  C.InstrGrdBkWgtDetailId
                                                                   ,D.Code
                                                                   ,D.Descrip
                                                                   ,C.Weight
                                                                   ,C.Number
                                                                   ,C.GrdPolicyId
                                                                   ,C.Parameter
                                                                   ,X.GrdScaleId
                                                        ) S
                                              GROUP BY  InstrGrdBkWgtDetailId
                                                       ,Code
                                                       ,Descrip
                                                       ,Weight
                                                       ,NumberOfComponents
                                            ) FinalTblToComputeCurrentScore
                                );
			
            IF ( @CurrentScore IS NULL )
                BEGIN
				-- instructor grade books
                    SET @CurrentScore = (
                                          SELECT    CASE WHEN SUM(GradeBookWeight) > 0 THEN ( SUM(ActualWeight) / SUM(GradeBookWeight) ) * 100
                                                         ELSE NULL
                                                    END AS CurrentScore
                                          FROM      (
                                                      SELECT    InstrGrdBkWgtDetailId
                                                               ,Code
                                                               ,Descrip
                                                               ,Weight AS GradeBookWeight
                                                               ,CASE WHEN S.NumberOfComponents > 0 THEN SUM(( Weight * ( Score / NumberOfComponents ) ) / 100)
                                                                     ELSE 0
                                                                END AS ActualWeight
                                                      FROM      (
                                                                  SELECT    C.InstrGrdBkWgtDetailId
                                                                           ,D.Code
                                                                           ,D.Descrip
                                                                           ,ISNULL(C.Weight,0) AS Weight
                                                                           ,C.Number AS MinNumber
                                                                           ,C.GrdPolicyId
                    ,C.Parameter AS Param
                                                                           ,X.GrdScaleId
                                                                           ,SUM(GR.Score) AS Score
                                                                           ,COUNT(D.Descrip) AS NumberOfComponents
                                                                  FROM      (
                                                                              --SELECT Distinct Top 1 A.InstrGrdBkWgtId,A.EffectiveDate,B.GrdScaleId        
															--FROM          arGrdBkWeights A,arClassSections B        
															--WHERE         A.ReqId=B.ReqId AND A.EffectiveDate<=B.StartDate AND B.ClsSectionId=@ClsSectionId
															--ORDER BY      A.EffectiveDate DESC
															
															SELECT DISTINCT TOP 1
                                                                    t1.InstrGrdBkWgtId
                                                                   ,t1.GrdScaleId
                                                            FROM    arClassSections t1
                                                                   ,arGrdBkWeights t2
                                                            WHERE   t1.InstrGrdBkWgtId = t2.InstrGrdBkWgtId
                                                                    AND t1.ClsSectionId = @ClsSectionId
                                                                            ) X
                                                                           ,arGrdBkWgtDetails C
                                                                           ,arGrdComponentTypes D
                                                                           ,arGrdBkResults GR
                                                                  WHERE     X.InstrGrdBkWgtId = C.InstrGrdBkWgtId
                                                                            AND C.GrdComponentTypeId = D.GrdComponentTypeId
                                                                            AND C.InstrGrdBkWgtDetailId = GR.InstrGrdBkWgtDetailId
                                                                            AND
													-- D.SysComponentTypeID not in (500,503) and 
                                                                            GR.StuEnrollId = @StuEnrollId
                                                                            AND GR.ClsSectionId = @ClsSectionId
                                                                            AND GR.Score IS NOT NULL
                                                                  GROUP BY  C.InstrGrdBkWgtDetailId
                                                                           ,D.Code
                                                                           ,D.Descrip
                                                                           ,C.Weight
                                                                           ,C.Number
                                                                           ,C.GrdPolicyId
                                                                           ,C.Parameter
                                                                           ,X.GrdScaleId
                                                                ) S
                                                      GROUP BY  InstrGrdBkWgtDetailId
                                                               ,Code
                                                               ,Descrip
                                                               ,Weight
                                                               ,NumberOfComponents
                                                    ) FinalTblToComputeCurrentScore
                                        );	
			
                END;

            IF ( @CurrentScore IS NOT NULL )
                BEGIN
                    SET @CurrentGrade = (
                                          SELECT    t2.Grade
                                          FROM      arGradeScaleDetails t1
                                                   ,arGradeSystemDetails t2
                                          WHERE     t1.GrdSysDetailId = t2.GrdSysDetailId
                                                    AND t1.GrdScaleId IN ( SELECT   GrdScaleId
                                                                           FROM     arClassSections
                                                                           WHERE    ClsSectionId = @ClsSectionId )
                                                    AND @CurrentScore >= t1.MinVal
                                                    AND @CurrentScore <= t1.MaxVal
                                        );
						
                END;	
            ELSE
                BEGIN
                    SET @CurrentGrade = NULL;
                END;
				
			
            IF (
                 @CurrentScore IS NULL
                 AND @CurrentGrade IS NULL
                 AND @FinalScore IS NULL
                 AND @FinalGrade IS NULL
               )
                BEGIN
                    SET @Completed = 0;
                    SET @CreditsAttempted = 0;
                    SET @CreditsEarned = 0;
                    SET @FinAidCreditsEarned = 0;
                END;
            IF (
                 @FinalScore IS NOT NULL
                 OR @FinalGrade IS NOT NULL
               )
                BEGIN

                    SET @FinalGradeDesc = (
                                            SELECT  Grade
                                            FROM    arGradeSystemDetails
                                            WHERE   GrdSysDetailId = @FinalGrade
                                          );
					

				
                    IF ( @FinalGradeDesc IS NULL )
                        BEGIN
                            SET @FinalGradeDesc = (
                                                    SELECT  t2.Grade
                                                    FROM    arGradeScaleDetails t1
                                                           ,arGradeSystemDetails t2
                                                    WHERE   t1.GrdSysDetailId = t2.GrdSysDetailId
                                                            AND t1.GrdScaleId IN ( SELECT   GrdScaleId
                                                                                   FROM     arClassSections
                                                                                   WHERE    ClsSectionId = @ClsSectionId )
                                                            AND @FinalScore >= t1.MinVal
                                                            AND @FinalScore <= t1.MaxVal
                                                  );
                        END;
                    SET @FinalGPA = (
                                      SELECT    GPA
                                      FROM      arGradeSystemDetails
                                      WHERE     GrdSysDetailId = @FinalGrade
                                    );
                    IF @FinalGPA IS NULL
                        BEGIN
                            SET @FinalGPA = (
                                              SELECT    t2.GPA
                                              FROM      arGradeScaleDetails t1
                                                       ,arGradeSystemDetails t2
                                              WHERE     t1.GrdSysDetailId = t2.GrdSysDetailId
                                                        AND t1.GrdScaleId IN ( SELECT   GrdScaleId
                                                                               FROM     arClassSections
                                                       WHERE    ClsSectionId = @ClsSectionId )
                                                        AND @FinalScore >= t1.MinVal
                                                        AND @FinalScore <= t1.MaxVal
                                            );
                        END;
                END;
            ELSE
                BEGIN
                    SET @FinalGradeDesc = NULL;
                    SET @FinalGPA = NULL;
                END;

			--set @IsInGPA = (SELECT t2.IsInGPA FROM arGradeScaleDetails t1,arGradeSystemDetails t2
			--									WHERE  t1.GrdSysDetailId=t2.GrdSysDetailId and 
			--									t1.GrdScaleId In (Select GrdScaleId from arClassSections where ClsSectionId =@ClsSectionId) 
			--									and t2.Grade=@FinalGradeDesc)
												
            SET @isGradeEligibleForCreditsEarned = (
                                                     SELECT t2.IsCreditsEarned
                                                     FROM   arGradeScaleDetails t1
                                                           ,arGradeSystemDetails t2
                                                     WHERE  t1.GrdSysDetailId = t2.GrdSysDetailId
                                                            AND t1.GrdScaleId IN ( SELECT   GrdScaleId
                                                                                   FROM     arClassSections
                                                                                   WHERE    ClsSectionId = @ClsSectionId )
                                                            AND t2.Grade = @FinalGradeDesc
                                                   ); 
												
            SET @isGradeEligibleForCreditsAttempted = (
                                                        SELECT  t2.IsCreditsAttempted
                                                        FROM    arGradeScaleDetails t1
                                                               ,arGradeSystemDetails t2
                                                        WHERE   t1.GrdSysDetailId = t2.GrdSysDetailId
                                                                AND t1.GrdScaleId IN ( SELECT   GrdScaleId
                                                                                       FROM     arClassSections
                                                                                       WHERE    ClsSectionId = @ClsSectionId )
                                                                AND t2.Grade = @FinalGradeDesc
                                                      ); 
												
            IF ( @isGradeEligibleForCreditsEarned IS NULL )
                BEGIN
					------ Print    'Credits Earned is NULL'
                    SET @isGradeEligibleForCreditsEarned = (
                                                             SELECT TOP 1
                                                                    t2.IsCreditsEarned
                                                             FROM   arGradeSystemDetails t2
                                                             WHERE  t2.Grade = @FinalGradeDesc
                                                           );
                END;
												
            IF ( @isGradeEligibleForCreditsAttempted IS NULL )
                BEGIN
					------ Print    'Credits Attempted is NULL'
                    SET @isGradeEligibleForCreditsAttempted = (
                                                                SELECT TOP 1
                                                                        t2.IsCreditsAttempted
                                                                FROM    arGradeSystemDetails t2
                                                                WHERE   t2.Grade = @FinalGradeDesc
                                                              );
                END;			
				
            IF @isGradeEligibleForCreditsEarned = 0
                BEGIN
                    SET @CreditsEarned = 0;
                    SET @FinAidCreditsEarned = 0;
                END;
            IF @isGradeEligibleForCreditsAttempted = 0
                BEGIN
                    SET @CreditsAttempted = 0;
                END;
				
            IF ( @IsPass = 0 ) -- if grade is a failing grade set credits earned=0
                BEGIN
                    SET @CreditsEarned = 0;
                    SET @FinAidCreditsEarned = 0;
                END;
				
			--For Letter Grade Schools if the score is null but final grade was posted then the 
			--Final grade will be the current grade
            IF @CurrentGrade IS NULL
                AND @FinalGradeDesc IS NOT NULL
                BEGIN
                    SET @CurrentGrade = @FinalGradeDesc;
                END;

--			if Trim(@PrevTermId) = Trim(@TermId)
--			begin
--				set @Sum_Product_WeightedAverage_Credits_GPA = @Sum_Product_WeightedAverage_Credits_GPA + (@Product_WeightedAverage_Credits_GPA)
--				set @Sum
--			end


			--Check if course has lab work or lab hours
--			set @boolCourseHasLabWorkOrLabHours = (select distinct Count(GC.Descrip) from arGrdBkWeights GBW,arGrdComponentTypes GC, arGrdBkWgtDetails GD  where 
--													GBW.InstrGrdBkWgtId = GD.InstrGrdBkWgtId And GC.GrdComponentTypeId = GD.GrdComponentTypeId 
--													and     GBW.ReqId = @ReqId and GC.SysComponentTypeID in (500,503))
			
            IF (
                 @sysComponentTypeId = 503
                 OR @sysComponentTypeId = 500
               ) -- Lab work or Lab Hours
                BEGIN
				-- This course has lab work and lab hours
                    IF ( @Completed = 0 )
                        BEGIN
                            SET @CreditsPerService = (
                                                       SELECT TOP 1
                                                                GD.CreditsPerService
                                                       FROM     arGrdBkWeights GBW
                                                               ,arGrdComponentTypes GC
                                                               ,arGrdBkWgtDetails GD
                                                       WHERE    GBW.InstrGrdBkWgtId = GD.InstrGrdBkWgtId
                                                                AND GC.GrdComponentTypeId = GD.GrdComponentTypeId
                                                                AND GBW.ReqId = @reqid
                                                                AND GC.SysComponentTypeId IN ( 500,503 )
                                                     );
                            SET @NumberOfServicesAttempted = (
                                                               SELECT TOP 1
                                                                        GBR.Score AS NumberOfServicesAttempted
                                                               FROM     arStuEnrollments SE
                                                               INNER JOIN arGrdBkResults GBR ON SE.StuEnrollId = GBR.StuEnrollId
                                                                                                AND GBR.ClsSectionId = @ClsSectionId
                                                             );

                            SET @CreditsEarned = ISNULL(@CreditsPerService,0) * ISNULL(@NumberOfServicesAttempted,0);
                        END;
                END;

            DECLARE @rowAlreadyInserted INT; 
            SET @rowAlreadyInserted = 0;
			
			
			-- Get the final Gpa only when IsCreditsAttempted is set to 1 and IsInGPA is set to 1
            IF @IsInGPA = 1
                BEGIN
                    IF ( @IsCreditsAttempted = 0 )
                        BEGIN
                            SET @FinalGPA = NULL; 
                END;
                END;
            ELSE
                BEGIN
                    SET @FinalGPA = NULL; 
                END;
				
            IF @FinalScore IS NOT NULL -- and @CurrentScore is null
                BEGIN
                    SET @CurrentScore = @FinalScore; 
                END;
			

			-- Get the number of components that have scores
            DECLARE @CountComponentsThatHasScores INT;
            SET @CountComponentsThatHasScores = (
                                                  SELECT    COUNT(*)
                                                  FROM      arGrdBkResults
                                                  WHERE     StuEnrollId = @StuEnrollId
                                                            AND ClsSectionId IN ( SELECT DISTINCT
                                                                                            ClsSectionId
                                                                                  FROM      arClassSections
                                                                                  WHERE     TermId = @TermId
                                                                                            AND ReqId = @reqid )
                                                            AND Score IS NOT NULL
                                                );
												

            DECLARE @CourseComponentsThatNeedsToBeScored INT;
            DECLARE @clsStartDate DATETIME;
            SET @clsStartDate = (
                                  SELECT TOP 1
                                            StartDate
                                  FROM      arClassSections
                                  WHERE     TermId = @TermId
                                            AND ReqId = @reqid
                                );
            IF LOWER(@SetGradeBookAt) = 'instructorlevel'
                BEGIN
                    SET @CourseComponentsThatNeedsToBeScored = (
                                                                 SELECT COUNT(*)
                                                                 FROM   (
                                                                          SELECT    4 AS Tag
                                                                                   ,3 AS Parent
                                                                                   ,PV.PrgVerId
                                                                                   ,PV.PrgVerDescrip
                                                                                   ,NULL AS ProgramCredits
                                                                                   ,T.TermId
                                                                                   ,T.TermDescrip AS TermDescription
                                                                                   ,T.StartDate AS TermStartDate
                                                                                   ,T.EndDate AS TermEndDate
                                                                                   ,R.ReqId AS CourseId
                                                                                   ,R.Descrip AS CourseDescription
                                                                                   ,'(' + R.Code + ' ) ' + R.Descrip AS CourseCodeDescrip
                                                                                   ,NULL AS CourseCredits
                                                                                   ,NULL AS CourseFinAidCredits
                                                                                   ,NULL AS CoursePassingGrade
                                                                                   ,NULL AS CourseScore
    ,(
                                                                                      SELECT TOP 1
                                                                                                GrdBkResultId
                                                                                      FROM      arGrdBkResults
                                                                                      WHERE     StuEnrollId = SE.StuEnrollId
                                                                                                AND InstrGrdBkWgtDetailId = GBWD.InstrGrdBkWgtDetailId
                                                                                                AND ClsSectionId = CS.ClsSectionId
                                                                                    ) AS GrdBkResultId
                                                                                   ,CASE WHEN LOWER(@SetGradeBookAt) = 'instructorlevel'
                                                                                         THEN RTRIM(GBWD.Descrip)
                                                                                         ELSE GCT.Descrip
                                                                                    END AS GradeBookDescription
                                                                                   ,( CASE GCT.SysComponentTypeId
                                                                                        WHEN 544 THEN (
                                                                                                        SELECT  SUM(HoursAttended)
                                                                                                        FROM    arExternshipAttendance
                                                                                                        WHERE   StuEnrollId = SE.StuEnrollId
                                                                                                      )
                                                                                        ELSE (
                                                                                               SELECT TOP 1
                                                                                                        Score
                                                                                               FROM     arGrdBkResults
                                                                                               WHERE    StuEnrollId = SE.StuEnrollId
                                                                                                        AND InstrGrdBkWgtDetailId = GBWD.InstrGrdBkWgtDetailId
                                                                                                        AND ClsSectionId = CS.ClsSectionId
                                                                                               ORDER BY ModDate DESC
                                                                                             )
                                                                                      END ) AS GradeBookScore
                                                                                   ,NULL AS GradeBookPostDate
                                                                                   ,NULL AS GradeBookPassingGrade
                                                                                   ,NULL AS GradeBookWeight
                                                                                   ,NULL AS GradeBookRequired
                                                                                   ,NULL AS GradeBookMustPass
                                                                                   ,GCT.SysComponentTypeId AS GradeBookSysComponentTypeId
                           ,NULL AS GradeBookHoursRequired
                                                                                   ,NULL AS GradeBookHoursCompleted
                                                                                   ,SE.StuEnrollId
                                                                                   ,( CASE WHEN GCT.SysComponentTypeId IN ( 500,503,504,544 ) THEN GBWD.Number
                                                                                           ELSE (
                                                                                                  SELECT    MIN(MinVal)
                                                                                                  FROM      arGradeScaleDetails GSD
                                                                                                           ,arGradeSystemDetails GSS
                                                                                                  WHERE     GSD.GrdSysDetailId = GSS.GrdSysDetailId
                                                                                                            AND GSS.IsPass = 1
                                                                                                            AND GSD.GrdScaleId = CS.GrdScaleId
                                                                                                )
                                                                                      END ) AS MinResult
                                                                                   ,SYRES.Resource AS GradeComponentDescription
                                                                                   ,NULL AS CreditsAttempted
                                                                                   ,NULL AS CreditsEarned
                                                                                   ,NULL AS Completed
                                                                                   ,NULL AS CurrentScore
                                                                                   ,NULL AS CurrentGrade
                                                                                   ,GCT.SysComponentTypeId AS FinalScore
                                                                                   ,NULL AS FinalGrade
                                                                                   ,NULL AS WeightedAverage_GPA
                                                                                   ,NULL AS SimpleAverage_GPA
                                                                                   ,NULL AS WeightedAverage_CumGPA
                                                                                   ,NULL AS SimpleAverage_CumGPA
                                                                                   ,C.CampusId
                                                                                   ,C.CampDescrip
                                                                                   ,ROW_NUMBER() OVER ( PARTITION BY SE.StuEnrollId,R.ReqId ORDER BY C.CampDescrip, PV.PrgVerDescrip, T.StartDate, T.EndDate, T.TermId, T.TermDescrip, R.ReqId, R.Descrip, GCT.SysComponentTypeId, GCT.Descrip ) AS rownumber
                                                                                   ,S.FirstName AS FirstName
                                                                                   ,S.LastName AS LastName
                                                                                   ,S.MiddleName
                                                                                   ,SYRES.ResourceID
                                                                          FROM      -- MOdified by Balaji
                                                                                    arStuEnrollments SE
                                                                          INNER JOIN (
                                                                                       SELECT   StudentId
                                                                                               ,FirstName
                                                                                               ,LastName
                                                                                               ,MiddleName
                                                                                       FROM     arStudent
                                                                                     ) S ON S.StudentId = SE.StudentId
                                                                          INNER JOIN arResults RES ON RES.StuEnrollId = SE.StuEnrollId -- RES.TestId = CS.ClsSectionId -- and RES.StuEnrollId = GBR.StuEnrollId
                                                                          INNER JOIN arClassSections CS ON CS.ClsSectionId = RES.TestId
                                                                          INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                                                                          INNER JOIN arTerm T ON CS.TermId = T.TermId
                                                                          INNER JOIN arReqs R ON CS.ReqId = R.ReqId
                                                                          LEFT OUTER JOIN arGrdBkWgtDetails GBWD ON GBWD.InstrGrdBkWgtId = CS.InstrGrdBkWgtId --.InstrGrdBkWgtDetailId = CS.InstrGrdBkWgtDetailId
                                                                          INNER JOIN arGrdComponentTypes GCT ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
                                                                          INNER JOIN (
                                                                                       SELECT   Resource
                                                                                               ,ResourceID
                                                                                       FROM     syResources
                                                                                       WHERE    ResourceTypeID = 10
                                                                                     ) SYRES ON SYRES.ResourceID = GCT.SysComponentTypeId
                                                                          INNER JOIN syCampuses C ON SE.CampusId = C.CampusId
                                                                          WHERE     SE.StuEnrollId = @StuEnrollId
                                                                                    AND T.TermId = @TermId
                                                                                    AND R.ReqId = @reqid
                                                                                    AND (
                                                                                          @sysComponentTypeId IS NULL
                                                                                          OR GCT.SysComponentTypeId IN (
                                                                                          SELECT    Val
                                                                                          FROM      MultipleValuesForReportParameters(@sysComponentTypeId,',',
                                                                                                                                        1) )
                                                                                        )
                                                                          UNION
                                                                          SELECT    4 AS Tag
                                                            ,3
                                                                                   ,PV.PrgVerId
                                                                                   ,PV.PrgVerDescrip
                                                                                   ,NULL
                                                                                   ,T.TermId
                                                                                   ,T.TermDescrip
                                                                                   ,T.StartDate AS termStartdate
                                                                                   ,T.EndDate AS TermEndDate
                                                                                   ,GBCR.ReqId
                                                                                   ,R.Descrip AS CourseDescrip
                                                                                   ,'(' + R.Code + ' ) ' + R.Descrip AS CourseCodeDescrip
                                                                                   ,NULL
                                                                                   ,NULL
                                                                                   ,NULL
                                                                                   ,NULL
                                                                                   ,GBCR.ConversionResultId AS GrdBkResultId
                                                                                   ,CASE WHEN LOWER(@SetGradeBookAt) = 'instructorlevel'
                                                                                         THEN RTRIM(GBWD.Descrip)
                                                                                         ELSE GCT.Descrip
                                                                                    END AS GradeBookDescription
                                                                                   ,GBCR.Score AS GradeBookResult
                                                                                   ,NULL
                                                                                   ,NULL
                                                                                   ,NULL
                                                                                   ,NULL
                                                                                   ,NULL
                                                                                   ,GCT.SysComponentTypeId
                                                                                   ,NULL
                                                                                   ,NULL
                                                                                   ,SE.StuEnrollId
                                                                                   ,GBCR.MinResult
                                                                                   ,SYRES.Resource -- Student data  
                                                                                   ,NULL AS CreditsAttempted
                                                                                   ,NULL AS CreditsEarned
                                                                                   ,NULL AS Completed
                                                                                   ,NULL AS CurrentScore
                                                                                   ,NULL AS CurrentGrade
                                                                                   ,NULL AS FinalScore
                                                                                   ,NULL AS FinalGrade
                                                                      ,NULL AS WeightedAverage_GPA
                                                                                   ,NULL AS SimpleAverage_GPA
                                                                                   ,NULL
                                                                                   ,NULL
                                                                                   ,C.CampusId
                                                                                   ,C.CampDescrip
                                                                                   ,ROW_NUMBER() OVER ( PARTITION BY SE.StuEnrollId,R.ReqId ORDER BY C.CampDescrip, PV.PrgVerDescrip, T.StartDate, T.EndDate, T.TermId, T.TermDescrip, R.ReqId, R.Descrip, GCT.SysComponentTypeId, GCT.Descrip ) AS rownumber
                                                                                   ,S.FirstName AS FirstName
                                                                                   ,S.LastName AS LastName
                                                                                   ,S.MiddleName
                                                                                   ,SYRES.ResourceID
                                                                          FROM      arGrdBkConversionResults GBCR
                                                                          INNER JOIN arStuEnrollments SE ON GBCR.StuEnrollId = SE.StuEnrollId
                                                                          INNER JOIN (
                                                                                       SELECT   StudentId
                                                                                               ,FirstName
                                                                                               ,LastName
                                                                                               ,MiddleName
                                                                                       FROM     arStudent
                                                                                     ) S ON S.StudentId = SE.StudentId
                                                                          INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                                                                          INNER JOIN arTerm T ON GBCR.TermId = T.TermId
                                                                          INNER JOIN arReqs R ON GBCR.ReqId = R.ReqId
                                                                          INNER JOIN arGrdComponentTypes GCT ON GCT.GrdComponentTypeId = GBCR.GrdComponentTypeId
                                                                          INNER JOIN arGrdBkWgtDetails GBWD ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
                                                                                                               AND GBWD.GrdComponentTypeId = GBCR.GrdComponentTypeId
                                                                          INNER JOIN arGrdBkWeights GBW ON GBWD.InstrGrdBkWgtId = GBW.InstrGrdBkWgtId
                                                                                                           AND GBCR.ReqId = GBW.ReqId
                                                                          INNER JOIN (
                                                                                       SELECT   ReqId
                                                                                               ,MAX(EffectiveDate) AS EffectiveDate
                                                                                       FROM     arGrdBkWeights
                                                                                       GROUP BY ReqId
                                                                               ) AS MaxEffectiveDatesByCourse ON GBCR.ReqId = MaxEffectiveDatesByCourse.ReqId
                                                                          INNER JOIN (
                                                                                       SELECT   Resource
                                                                                               ,ResourceID
                                                                                       FROM     syResources
                                                                                       WHERE    ResourceTypeID = 10
                                                                                     ) SYRES ON SYRES.ResourceID = GCT.SysComponentTypeId
                                                                          INNER JOIN syCampuses C ON SE.CampusId = C.CampusId
                                                                          WHERE     MaxEffectiveDatesByCourse.EffectiveDate <= T.StartDate
                                                                                    AND SE.StuEnrollId = @StuEnrollId
                                                                                    AND T.TermId = @TermId
                                                                                    AND R.ReqId = @reqid
                                                                                    AND (
                                                                                          @sysComponentTypeId IS NULL
                                                                                          OR GCT.SysComponentTypeId IN (
                                                                                          SELECT    Val
                                                                                          FROM      MultipleValuesForReportParameters(@sysComponentTypeId,',',
                                                                                                                                        1) )
                                                                                        )
                                                                        ) dt
                                                               );
                END;
            ELSE
                BEGIN
                    SET @CourseComponentsThatNeedsToBeScored = (
                                                                 SELECT COUNT(*)
                                                                 FROM   (
                                                                          SELECT DISTINCT
                                                                                    GradeBookDescription
                                                                                   ,GradeBookScore
                                                                                   ,MinResult
                                                                                   ,GradeBookSysComponentTypeId
                                                                                   ,GradeComponentDescription
                                                                                   ,CampDescrip
                                                                                   ,FirstName
                                                                                   ,LastName
                                                                                   ,MiddleName
                                                                                   ,GrdBkResultId
                                                                                   ,TermStartDate
                                                                                   ,TermEndDate
                                                               ,TermDescription
                                                                                   ,CourseDescription
                                                                                   ,PrgVerDescrip
                                                                                   ,StuEnrollId
                                                                                   ,CourseId
                                                                                   ,ResourceID
                                                                                   ,Required
                                                                          FROM      (
                                                                                      SELECT  DISTINCT
                                                                                                4 AS Tag
                                                                                               ,3 AS Parent
                                                                                               ,PV.PrgVerId
                                                                                               ,PV.PrgVerDescrip
                                                                                               ,NULL AS ProgramCredits
                                                                                               ,T.TermId
                                                                                               ,T.TermDescrip AS TermDescription
                                                                                               ,T.StartDate AS TermStartDate
                                                                                               ,T.EndDate AS TermEndDate
                                                                                               ,R.ReqId AS CourseId
                                                                                               ,R.Descrip AS CourseDescription
                                                                                               ,'(' + R.Code + ' ) ' + R.Descrip AS CourseCodeDescrip
                                                                                               ,NULL AS CourseCredits
                                                                                               ,NULL AS CourseFinAidCredits
                                                                                               ,NULL AS CoursePassingGrade
                                                                                               ,NULL AS CourseScore
                                                                                               ,(
                                                                                                  SELECT TOP 1
                                                                                                            GrdBkResultId
                                                                                                  FROM      arGrdBkResults
                                                                                                  WHERE     StuEnrollId = SE.StuEnrollId
                                                                                                            AND InstrGrdBkWgtDetailId = b.InstrGrdBkWgtDetailId
                                                                                                            AND ClsSectionId = CS.ClsSectionId
                                                                                                ) AS GrdBkResultId
                                                                                               ,RTRIM(GCT.Descrip) + ( CASE WHEN a.ResNum IN ( 0,1 ) THEN ''
                                                                             ELSE CAST(a.ResNum AS CHAR)
                                                                                                                       END ) AS GradeBookDescription
                                                                                               ,( CASE GCT.SysComponentTypeId
                                                                                                    WHEN 544 THEN (
                                                                                                                    SELECT  SUM(HoursAttended)
                                                                                                                    FROM    arExternshipAttendance
                                                                                                                    WHERE   StuEnrollId = SE.StuEnrollId
                                                                                                                  )
                                                                                                    ELSE 
--								(select Top 1 Score from arGrdBkResults where StuEnrollId=SE.StuEnrollId and 
--								InstrGrdBkWgtDetailId=b.InstrGrdBkWgtDetailId and
--								ClsSectionId=CS.ClsSectionId order by moddate desc) 
                                                                                                         a.Score
                                                                                                  END ) AS GradeBookScore
                                                                                               ,NULL AS GradeBookPostDate
                                                                                               ,NULL AS GradeBookPassingGrade
                                                                                               ,NULL AS GradeBookWeight
                                                                                               ,NULL AS GradeBookRequired
                                                                                               ,NULL AS GradeBookMustPass
                                                                                               ,GCT.SysComponentTypeId AS GradeBookSysComponentTypeId
                                                                                               ,NULL AS GradeBookHoursRequired
                                                                                               ,NULL AS GradeBookHoursCompleted
                                                                                               ,SE.StuEnrollId
                                                                                               ,( CASE WHEN GCT.SysComponentTypeId IN ( 500,503,504,544 )
                                                                                                       THEN b.Number
                                                                                                       ELSE (
                                                                                                              SELECT    MIN(MinVal)
                                                                                                              FROM      arGradeScaleDetails GSD
                                                                                                                       ,arGradeSystemDetails GSS
                                                                                                              WHERE     GSD.GrdSysDetailId = GSS.GrdSysDetailId
                                                                                                                        AND GSS.IsPass = 1
                                                                                                                        AND GSD.GrdScaleId = CS.GrdScaleId
                                                                                                            )
                                                                                                  END ) AS MinResult
                                                                                               ,SYRES.Resource AS GradeComponentDescription
                                                                                               ,NULL AS CreditsAttempted
                                                                                               ,NULL AS CreditsEarned
                                                                                               ,NULL AS Completed
                                                                                               ,NULL AS CurrentScore
                                                                                               ,NULL AS CurrentGrade
                                                                                               ,GCT.SysComponentTypeId AS FinalScore
                                                                                               ,NULL AS FinalGrade
                                                                                               ,NULL AS WeightedAverage_GPA
                                                                                               ,NULL AS SimpleAverage_GPA
                                                                                               ,NULL AS WeightedAverage_CumGPA
                                                                                               ,NULL AS SimpleAverage_CumGPA
                                                                                               ,C1.CampusId
                                                                                               ,C1.CampDescrip
                                                                                               , 
						--ROW_NUMBER() OVER (partition by SE.StuEnrollId,R.ReqId Order By C1.CampDescrip,PV.PrgVerDescrip,T.StartDate,T.EndDate,T.TermId,T.TermDescrip,R.ReqId,R.Descrip,GCT.SysComponentTypeId,GCT.Descrip) as rownumber,
                                                                                                c.FirstName AS FirstName
                                                                                               ,c.LastName AS LastName
                                                                                               ,c.MiddleName
                                                                                               ,SYRES.ResourceID
                                                                                               ,b.Required
                                                                                      FROM      arGrdBkResults a
                                                                                               ,arGrdBkWgtDetails b
                                                                                               ,arStudent c
                                                                                               ,arStuEnrollments SE
                                                                                               ,arResults e
                                                                                               ,arClassSections CS
                                                                                               ,arReqs R
                                                                                               ,arTerm T
                                                                                               ,arGrdComponentTypes GCT
                                                                                               ,(
                                                             SELECT    Resource
                                                                                                           ,ResourceID
                                                                                                  FROM      syResources
                                                                                                  WHERE     ResourceTypeID = 10
                                                                                                ) SYRES
                                                                                               ,syCampuses C1
                                                                                               ,arPrgVersions PV
                                                                                      WHERE     a.InstrGrdBkWgtDetailId = b.InstrGrdBkWgtDetailId
                                                                                                AND a.StuEnrollId = @StuEnrollId
                                                                                                AND a.StuEnrollId = SE.StuEnrollId
                                                                                                AND SE.StudentId = c.StudentId
                                                                                                AND a.StuEnrollId = e.StuEnrollId
                                                                                                AND a.ClsSectionId = e.TestId
                                                                                                AND e.TestId = CS.ClsSectionId
                                                                                                AND CS.ReqId = R.ReqId
                                                                                                AND CS.TermId = T.TermId
                                                                                                AND GCT.GrdComponentTypeId = b.GrdComponentTypeId
                                                                                                AND SYRES.ResourceID = GCT.SysComponentTypeId
                                                                                                AND SE.CampusId = C1.CampusId
                                                                                                AND SE.PrgVerId = PV.PrgVerId 
						--and a.ClsSectionId = 'CD64FA81-7E91-4E22-A323-34E7B5695E2E' 
                                                                                                AND R.ReqId = @reqid
                                                                                                AND T.TermId = @TermId
                                                                                                AND (
                                                                                                      @sysComponentTypeId IS NULL
                                                                                                      OR GCT.SysComponentTypeId IN (
                                                                                                      SELECT    Val
                                                                                                      FROM      MultipleValuesForReportParameters(@sysComponentTypeId,
                                                                                                                                                ',',1) )
                                                                                                    )
                                                                                                AND a.ResNum >= 1
                                                                                      UNION
   SELECT  DISTINCT
                                                                                                4 AS Tag
                                                                                               ,3 AS Parent
                                                                                               ,PV.PrgVerId
                                                                                               ,PV.PrgVerDescrip
                                                                                               ,NULL AS ProgramCredits
                                                                                               ,T.TermId
                                                                                               ,T.TermDescrip AS TermDescription
                                                                                               ,T.StartDate AS TermStartDate
                                                                                               ,T.EndDate AS TermEndDate
                                                                                               ,R.ReqId AS CourseId
                                                                                               ,R.Descrip AS CourseDescription
                                                                                               ,'(' + R.Code + ' ) ' + R.Descrip AS CourseCodeDescrip
                                                                                               ,NULL AS CourseCredits
                                                                                               ,NULL AS CourseFinAidCredits
                                                                                               ,NULL AS CoursePassingGrade
                                                                                               ,NULL AS CourseScore
                                                                                               ,(
                                                                                                  SELECT TOP 1
                                                                                                            GrdBkResultId
                                                                                                  FROM      arGrdBkResults
                                                                                                  WHERE     StuEnrollId = SE.StuEnrollId
                                                                                                            AND InstrGrdBkWgtDetailId = b.InstrGrdBkWgtDetailId
                                                                                                            AND ClsSectionId = CS.ClsSectionId
                                                                                                ) AS GrdBkResultId
                                                                                               ,RTRIM(GCT.Descrip) + ( CASE WHEN a.ResNum IN ( 0,1 ) THEN ''
                                                                                                                            ELSE CAST(a.ResNum AS CHAR)
                                                                                                                       END ) AS GradeBookDescription
                                                                                               ,( CASE GCT.SysComponentTypeId
                                                                                                    WHEN 544 THEN (
                                                                                                                    SELECT  SUM(HoursAttended)
                                                                                                                    FROM    arExternshipAttendance
                                                                          WHERE   StuEnrollId = SE.StuEnrollId
                                                                                                                  )
                                                                                                    ELSE 
--								(select Top 1 Score from arGrdBkResults where StuEnrollId=SE.StuEnrollId and 
--								InstrGrdBkWgtDetailId=b.InstrGrdBkWgtDetailId and
--								ClsSectionId=CS.ClsSectionId order by moddate desc) 
                                                                                                         a.Score
                                                                                                  END ) AS GradeBookScore
                                                                                               ,NULL AS GradeBookPostDate
                                                                                               ,NULL AS GradeBookPassingGrade
                                                                                               ,NULL AS GradeBookWeight
                                                                                               ,NULL AS GradeBookRequired
                                                                                               ,NULL AS GradeBookMustPass
                                                                                               ,GCT.SysComponentTypeId AS GradeBookSysComponentTypeId
                                                                                               ,NULL AS GradeBookHoursRequired
                                                                                               ,NULL AS GradeBookHoursCompleted
                                                                                               ,SE.StuEnrollId
                                                                                               ,( CASE WHEN GCT.SysComponentTypeId IN ( 500,503,504,544 )
                                                                                                       THEN b.Number
                                                                                                       ELSE (
                                                                                                              SELECT    MIN(MinVal)
                                                                                                              FROM      arGradeScaleDetails GSD
                                                                                                                       ,arGradeSystemDetails GSS
                                                                                                              WHERE     GSD.GrdSysDetailId = GSS.GrdSysDetailId
                                                                                                                        AND GSS.IsPass = 1
                                                                                                                        AND GSD.GrdScaleId = CS.GrdScaleId
                                                                                                            )
                                                                                                  END ) AS MinResult
                                                                                               ,SYRES.Resource AS GradeComponentDescription
                                                                                               ,NULL AS CreditsAttempted
                                                                                               ,NULL AS CreditsEarned
                                                                                               ,NULL AS Completed
          ,NULL AS CurrentScore
                                                                                               ,NULL AS CurrentGrade
                                                                                               ,GCT.SysComponentTypeId AS FinalScore
                                                                                               ,NULL AS FinalGrade
                                                                                               ,NULL AS WeightedAverage_GPA
                                                                                               ,NULL AS SimpleAverage_GPA
                                                                                               ,NULL AS WeightedAverage_CumGPA
                                                                                               ,NULL AS SimpleAverage_CumGPA
                                                                                               ,C1.CampusId
                                                                                               ,C1.CampDescrip
                                                                                               , 
						--ROW_NUMBER() OVER (partition by SE.StuEnrollId,R.ReqId Order By C1.CampDescrip,PV.PrgVerDescrip,T.StartDate,T.EndDate,T.TermId,T.TermDescrip,R.ReqId,R.Descrip,GCT.SysComponentTypeId,GCT.Descrip) as rownumber,
                                                                                                c.FirstName AS FirstName
                                                                                               ,c.LastName AS LastName
                                                                                               ,c.MiddleName
                                                                                               ,SYRES.ResourceID
                                                                                               ,b.Required
                                                                                      FROM      arGrdBkResults a
                                                                                               ,arGrdBkWgtDetails b
                                                                                               ,arStudent c
                                                                                               ,arStuEnrollments SE
                                                                                               ,arResults e
                                                                                               ,arClassSections CS
                                                                                               ,arReqs R
                                                                                               ,arTerm T
                                                                                               ,arGrdComponentTypes GCT
                                                                                               ,(
                                                                                                  SELECT    Resource
                                                                                                           ,ResourceID
                                                                                                  FROM      syResources
                                                                                                  WHERE     ResourceTypeID = 10
                                                                                                ) SYRES
                                                                                               ,syCampuses C1
                                                                                               ,arPrgVersions PV
                                 WHERE     a.InstrGrdBkWgtDetailId = b.InstrGrdBkWgtDetailId
                                                                                                AND a.StuEnrollId = @StuEnrollId
                                                                                                AND a.StuEnrollId = SE.StuEnrollId
                                                                                                AND SE.StudentId = c.StudentId
                                                                                                AND a.StuEnrollId = e.StuEnrollId
                                                                                                AND a.ClsSectionId = e.TestId
                                                                                                AND e.TestId = CS.ClsSectionId
                                                                                                AND CS.ReqId = R.ReqId
                                                                                                AND CS.TermId = T.TermId
                                                                                                AND GCT.GrdComponentTypeId = b.GrdComponentTypeId
                                                                                                AND SYRES.ResourceID = GCT.SysComponentTypeId
                                                                                                AND SE.CampusId = C1.CampusId
                                                                                                AND SE.PrgVerId = PV.PrgVerId 
						--and a.ClsSectionId = 'CD64FA81-7E91-4E22-A323-34E7B5695E2E' 
                                                                                                AND R.ReqId = @reqid
                                                                                                AND T.TermId = @TermId
                                                                                                AND (
                                                                                                      @sysComponentTypeId IS NULL
                                                                                                      OR GCT.SysComponentTypeId IN (
                                                                                                      SELECT    Val
                                                                                                      FROM      MultipleValuesForReportParameters(@sysComponentTypeId,
                                                                                                                                                ',',1) )
                                                                                                    )
                                                                                                AND a.ResNum = 0
                                                                                                AND a.GrdBkResultId NOT IN (
                                                                                                SELECT DISTINCT
                                                                                                        GrdBkResultId
                                                                                                FROM    arGrdBkResults a
                                                                                                       ,arGrdBkWgtDetails b
                                                                                                       ,arStudent c
                                                                                                       ,arStuEnrollments SE
                                                                                                       ,arResults e
                                                        ,arClassSections CS
                                                                                                       ,arReqs R
                                                                                                       ,arTerm T
                                                                                                       ,arGrdComponentTypes GCT
                                                                                                       ,(
                                                                                                          SELECT    Resource
                                                                                                                   ,ResourceID
                                                                                                          FROM      syResources
                                                                                                          WHERE     ResourceTypeID = 10
                                                                                                        ) SYRES
                                                                                                       ,syCampuses C1
                                                                                                       ,arPrgVersions PV
                                                                                                WHERE   a.InstrGrdBkWgtDetailId = b.InstrGrdBkWgtDetailId
                                                                                                        AND a.StuEnrollId = @StuEnrollId
                                                                                                        AND a.StuEnrollId = SE.StuEnrollId
                                                                                                        AND SE.StudentId = c.StudentId
                                                                                                        AND a.StuEnrollId = e.StuEnrollId
                                                                                                        AND a.ClsSectionId = e.TestId
                                                                                                        AND e.TestId = CS.ClsSectionId
                                                                                                        AND CS.ReqId = R.ReqId
                                                                                                        AND CS.TermId = T.TermId
                                                                                                        AND GCT.GrdComponentTypeId = b.GrdComponentTypeId
                                                                                                        AND SYRES.ResourceID = GCT.SysComponentTypeId
                                                                                                        AND SE.CampusId = C1.CampusId
                                                                                                        AND SE.PrgVerId = PV.PrgVerId
                                                                                                        AND R.ReqId = @reqid
                                                                                                        AND T.TermId = @TermId
                                                                                                        AND (
                                                                                                              @sysComponentTypeId IS NULL
                                                                                                              OR GCT.SysComponentTypeId IN (
                                                                                                              SELECT    Val
                                                                                                             FROM      MultipleValuesForReportParameters(@sysComponentTypeId,
                                                                                                                                                ',',1) )
                                                                                                            )
                                                                                                        AND a.ResNum >= 1 )
                                                                                      UNION
                                                                                      SELECT    *
                                                                                      FROM      (
                                                                                                  SELECT  DISTINCT
                                                                                                            4 AS Tag
                                                                                                           ,3 AS Parent
                                                                                                           ,PV.PrgVerId
                                                                                                           ,PV.PrgVerDescrip
                                                                                                           ,NULL AS ProgramCredits
                                                                                                           ,T.TermId
                                                                                                           ,T.TermDescrip AS TermDescription
                                                                                                           ,T.StartDate AS TermStartDate
                                                                                                           ,T.EndDate AS TermEndDate
                                                                                                           ,R.ReqId AS CourseId
                                                                                                           ,R.Descrip AS CourseDescription
                                                                                                           ,'(' + R.Code + ' ) ' + R.Descrip AS CourseCodeDescrip
                                                                                                           ,NULL AS CourseCredits
                                                                                                           ,NULL AS CourseFinAidCredits
                                                                                                           ,NULL AS CoursePassingGrade
                                                                                                           ,NULL AS CourseScore
                                                                                                           ,(
                                                                                                              SELECT TOP 1
                                                                                                                        GrdBkResultId
                                                                                                              FROM      arGrdBkResults
                                                                                                              WHERE     StuEnrollId = SE.StuEnrollId
                                                                                                                        AND InstrGrdBkWgtDetailId = b.InstrGrdBkWgtDetailId
                                    AND ClsSectionId = CS.ClsSectionId
                                                                                                            ) AS GrdBkResultId
                                                                                                           ,RTRIM(GCT.Descrip) AS GradeBookDescription
                                                                                                           ,( CASE GCT.SysComponentTypeId
                                                                                                                WHEN 544
                                                                                                                THEN (
                                                                                                                       SELECT   SUM(HoursAttended)
                                                                                                                       FROM     arExternshipAttendance
                                                                                                                       WHERE    StuEnrollId = SE.StuEnrollId
                                                                                                                     )
                                                                                                                ELSE 
--								(select Top 1 Score from arGrdBkResults where StuEnrollId=SE.StuEnrollId and 
--								InstrGrdBkWgtDetailId=b.InstrGrdBkWgtDetailId and
--								ClsSectionId=CS.ClsSectionId order by moddate desc) 
                                                                                                                     NULL
                                                                                                              END ) AS GradeBookScore
                                                                                                           ,NULL AS GradeBookPostDate
                                                                                                           ,NULL AS GradeBookPassingGrade
                                                                                                           ,NULL AS GradeBookWeight
                                                                                                           ,NULL AS GradeBookRequired
                                                                                                           ,NULL AS GradeBookMustPass
                                                                                                           ,GCT.SysComponentTypeId AS GradeBookSysComponentTypeId
                                                                                                           ,NULL AS GradeBookHoursRequired
                                                                                                           ,NULL AS GradeBookHoursCompleted
                                                                                                           ,SE.StuEnrollId
                                                                                                           ,( CASE WHEN GCT.SysComponentTypeId IN ( 500,503,504,
                                                                                                                                                544 )
                                                                                                                   THEN b.Number
                                                                                                                   ELSE (
                                                                                                                          SELECT    MIN(MinVal)
                                                                                                                          FROM      arGradeScaleDetails GSD
                                                                                                                                   ,arGradeSystemDetails GSS
                                                                                                                          WHERE     GSD.GrdSysDetailId = GSS.GrdSysDetailId
                                                                                                                                    AND GSS.IsPass = 1
                                                                                                                                    AND GSD.GrdScaleId = CS.GrdScaleId
                                                                                                                        )
                                                                                                              END ) AS MinResult
                                                                                                           ,SYRES.Resource AS GradeComponentDescription
                                                                                                           ,NULL AS CreditsAttempted
                                                                                                           ,NULL AS CreditsEarned
                                                                                                           ,NULL AS Completed
                                                                                                           ,NULL AS CurrentScore
                                                                                                           ,NULL AS CurrentGrade
                                                                                                           ,GCT.SysComponentTypeId AS FinalScore
                                                                                                           ,NULL AS FinalGrade
                                                                                                           ,NULL AS WeightedAverage_GPA
                                                                                                           ,NULL AS SimpleAverage_GPA
                                                                                                           ,NULL AS WeightedAverage_CumGPA
                                                                                                           ,NULL AS SimpleAverage_CumGPA
                                                                                                           ,C1.CampusId
                                                                                                           ,C1.CampDescrip
                                                                                                           , 
						--ROW_NUMBER() OVER (partition by SE.StuEnrollId,R.ReqId Order By C1.CampDescrip,PV.PrgVerDescrip,T.StartDate,T.EndDate,T.TermId,T.TermDescrip,R.ReqId,R.Descrip,GCT.SysComponentTypeId,GCT.Descrip) as rownumber,
                                                                                                            c.FirstName AS FirstName
                                                                                                           ,c.LastName AS LastName
                                                                                                           ,c.MiddleName
                                                                                                           ,SYRES.ResourceID
                                                                                                           ,b.Required
                                                                                                  FROM      arResults e
                                                                                                           ,arStuEnrollments SE
                                                                                                           ,arStudent c
                                                                                                           ,arClassSections CS
                                                                                                           ,arReqs R
                                                                                                           ,arTerm T
                                                                                                           ,arGrdComponentTypes GCT
                                                                                                           ,(
                                                                                                              SELECT    Resource
                                                                                                                       ,ResourceID
                                                                                                              FROM      syResources
                                                                                                              WHERE     ResourceTypeID = 10
                                                                                                            ) SYRES
                                                                                                           ,syCampuses C1
                                                                                                           ,arPrgVersions PV
                                                                                                           ,(
                                                                                                              SELECT DISTINCT TOP 1
                                                                                                                        A.InstrGrdBkWgtId
                                                                                                                       ,A.EffectiveDate
                                                                                                                       ,b.GrdScaleId
                                                                                                                       ,b.ReqId
                                                                                                              FROM      arGrdBkWeights A
                                                                                                                       ,arClassSections b
                                                                                                              WHERE     A.ReqId = b.ReqId
                                                                                                                        AND A.EffectiveDate <= b.StartDate
                                                                                                                        AND b.TermId = @TermId
                                                                                                                        AND b.ReqId = @reqid
                                                                                                              ORDER BY  EffectiveDate DESC
                                                                                                            ) a
                                                                                                           ,arGrdBkWgtDetails b
                                                                                                  WHERE     e.StuEnrollId = SE.StuEnrollId
                                                                                                            AND SE.StudentId = c.StudentId
                                                AND e.TestId = CS.ClsSectionId
                                                                                                            AND CS.ReqId = R.ReqId
                                                                                                            AND CS.TermId = T.TermId
                                                                                                            AND a.InstrGrdBkWgtId = b.InstrGrdBkWgtId
                                                                                                            AND a.ReqId = R.ReqId
                                                                                                            AND GCT.GrdComponentTypeId = b.GrdComponentTypeId
                                                                                                            AND SYRES.ResourceID = GCT.SysComponentTypeId
                                                                                                            AND SE.CampusId = C1.CampusId
                                                                                                            AND SE.PrgVerId = PV.PrgVerId
                                                                                                            AND SE.StuEnrollId = @StuEnrollId
                                                                                                            AND (
                                                                                                                  @sysComponentTypeId IS NULL
                                                                                                                  OR GCT.SysComponentTypeId IN (
                                                                                                                  SELECT    Val
                                                                                                                  FROM      MultipleValuesForReportParameters(@sysComponentTypeId,
                                                                                                                                                ',',1) )
                                                                                                                )
                                                                                                ) dt5
                                                                                      WHERE     GrdBkResultId IS NULL
                                                                                      UNION
                                                                                      SELECT  DISTINCT
                                                                                                4 AS Tag
                                                                                               ,3 AS Parent
                                                                                               ,PV.PrgVerId
                                                                                               ,PV.PrgVerDescrip
                                                                                               ,NULL AS ProgramCredits
                                                                                               ,T.TermId
                                                                                               ,T.TermDescrip AS TermDescription
                                                                                               ,T.StartDate AS TermStartDate
                                                                                               ,T.EndDate AS TermEndDate
                                                                                               ,R.ReqId AS CourseId
                  ,R.Descrip AS CourseDescription
                                                                                               ,'(' + R.Code + ' ) ' + R.Descrip AS CourseCodeDescrip
                                                                                               ,NULL AS CourseCredits
                                                                                               ,NULL AS CourseFinAidCredits
                                                                                               ,NULL AS CoursePassingGrade
                                                                                               ,NULL AS CourseScore
                                                                                               ,(
                                                                                                  SELECT TOP 1
                                                                                                            GrdBkResultId
                                                                                                  FROM      arGrdBkResults
                                                                                                  WHERE     StuEnrollId = SE.StuEnrollId
                                                                                                            AND InstrGrdBkWgtDetailId = b.InstrGrdBkWgtDetailId
                                                                                                            AND ClsSectionId = CS.ClsSectionId
                                                                                                ) AS GrdBkResultId
                                                                                               ,RTRIM(GCT.Descrip) AS GradeBookDescription
                                                                                               ,( CASE GCT.SysComponentTypeId
                                                                                                    WHEN 544 THEN (
                                                                                                                    SELECT  SUM(HoursAttended)
                                                                                                                    FROM    arExternshipAttendance
                                                                                                                    WHERE   StuEnrollId = SE.StuEnrollId
                                                                                                                  )
                                                                                                    ELSE 
--								(select Top 1 Score from arGrdBkResults where StuEnrollId=SE.StuEnrollId and 
--								InstrGrdBkWgtDetailId=b.InstrGrdBkWgtDetailId and
--								ClsSectionId=CS.ClsSectionId order by moddate desc) 
                                                                                                         NULL
                                                                                                  END ) AS GradeBookScore
                                                                                               ,NULL AS GradeBookPostDate
                                                                                               ,NULL AS GradeBookPassingGrade
                                                                                               ,NULL AS GradeBookWeight
                                                                                               ,NULL AS GradeBookRequired
                                                                                               ,NULL AS GradeBookMustPass
                                                                                               ,GCT.SysComponentTypeId AS GradeBookSysComponentTypeId
                                                                     ,NULL AS GradeBookHoursRequired
                                                                                               ,NULL AS GradeBookHoursCompleted
                                                                                               ,SE.StuEnrollId
                                                                                               ,( CASE WHEN GCT.SysComponentTypeId IN ( 500,503,504,544 )
                                                                                                       THEN b.Number
                                                                                                       ELSE (
                                                                                                              SELECT    MIN(MinVal)
                                                                                                              FROM      arGradeScaleDetails GSD
                                                                                                                       ,arGradeSystemDetails GSS
                                                                                                              WHERE     GSD.GrdSysDetailId = GSS.GrdSysDetailId
                                                                                                                        AND GSS.IsPass = 1
                                                                                                                        AND GSD.GrdScaleId = CS.GrdScaleId
                                                                                                            )
                                                                                                  END ) AS MinResult
                                                                                               ,SYRES.Resource AS GradeComponentDescription
                                                                                               ,NULL AS CreditsAttempted
                                                                                               ,NULL AS CreditsEarned
                                                                                               ,NULL AS Completed
                                                                                               ,NULL AS CurrentScore
                                                                                               ,NULL AS CurrentGrade
                                                                                               ,GCT.SysComponentTypeId AS FinalScore
                                                                                               ,NULL AS FinalGrade
                                                                                               ,NULL AS WeightedAverage_GPA
                                                                                               ,NULL AS SimpleAverage_GPA
                                                                                               ,NULL AS WeightedAverage_CumGPA
                                                                                               ,NULL AS SimpleAverage_CumGPA
                                                                                               ,C1.CampusId
                                                                                               ,C1.CampDescrip
                                                                                               , 
						--ROW_NUMBER() OVER (partition by SE.StuEnrollId,R.ReqId Order By C1.CampDescrip,PV.PrgVerDescrip,T.StartDate,T.EndDate,T.TermId,T.TermDescrip,R.ReqId,R.Descrip,GCT.SysComponentTypeId,GCT.Descrip) as rownumber,
                                                                                                c.FirstName AS FirstName
                                                                             ,c.LastName AS LastName
                                                                                               ,c.MiddleName
                                                                                               ,SYRES.ResourceID
                                                                                               ,b.Required
                                                                                      FROM      arResults e
                                                                                               ,arStuEnrollments SE
                                                                                               ,arStudent c
                                                                                               ,arClassSections CS
                                                                                               ,arReqs R
                                                                                               ,arTerm T
                                                                                               ,arGrdComponentTypes GCT
                                                                                               ,(
                                                                                                  SELECT    Resource
                                                                                                           ,ResourceID
                                                                                                  FROM      syResources
                                                                                                  WHERE     ResourceTypeID = 10
                                                                                                ) SYRES
                                                                                               ,syCampuses C1
                                                                                               ,arPrgVersions PV
                                                                                               ,(
                                                                                                  SELECT DISTINCT TOP 1
                                                                                                            A.InstrGrdBkWgtId
                                                                                                           ,A.EffectiveDate
                                                                                                           ,b.GrdScaleId
                                                                                                           ,b.ReqId
                                                                                                  FROM      arGrdBkWeights A
                                                                                                           ,arClassSections b
                                                                                                  WHERE     A.ReqId = b.ReqId
                                                                                                            AND A.EffectiveDate <= b.StartDate
                                                                                                            AND b.TermId = @TermId
                                                                                                            AND b.ReqId = @reqid
                                                                                                  ORDER BY  A.EffectiveDate DESC
                                                                                                ) a
                                                                                               ,arGrdBkWgtDetails b
                                                                  WHERE     e.StuEnrollId = SE.StuEnrollId
                                                                                                AND SE.StudentId = c.StudentId
                                                                                                AND e.TestId = CS.ClsSectionId
                                                                                                AND CS.ReqId = R.ReqId
                                                                                                AND CS.TermId = T.TermId
                                                                                                AND a.InstrGrdBkWgtId = b.InstrGrdBkWgtId
                                                                                                AND a.ReqId = R.ReqId
                                                                                                AND GCT.GrdComponentTypeId = b.GrdComponentTypeId
                                                                                                AND SYRES.ResourceID = GCT.SysComponentTypeId
                                                                                                AND SE.CampusId = C1.CampusId
                                                                                                AND SE.PrgVerId = PV.PrgVerId
                                                                                                AND SE.StuEnrollId = @StuEnrollId
                                                                                                AND R.ReqId = @reqid
                                                                                                AND T.TermId = @TermId
                                                                                                AND (
                                                                                                      @sysComponentTypeId IS NULL
                                                                                                      OR GCT.SysComponentTypeId IN (
                                                                                                      SELECT    Val
                                                                                                      FROM      MultipleValuesForReportParameters(@sysComponentTypeId,
                                                                                                                                                ',',1) )
                                                                                                    )
                                                                                                AND e.TestId NOT IN ( SELECT    ClsSectionId
                                                                                                                      FROM      arGrdBkResults
                                                                                                                      WHERE     StuEnrollId = e.StuEnrollId
                                                                                                                                AND ClsSectionId = e.TestId )
                                                                                      UNION
                                                                                      SELECT    4 AS Tag
                                                                                               ,3
                                                                                               ,PV.PrgVerId
                                                                                               ,PV.PrgVerDescrip
                                                                                               ,NULL
       ,T.TermId
                                                                                               ,T.TermDescrip
                                                                                               ,T.StartDate AS termStartdate
                                                                                               ,T.EndDate AS TermEndDate
                                                                                               ,GBCR.ReqId
                                                                                               ,R.Descrip AS CourseDescrip
                                                                                               ,'(' + R.Code + ' ) ' + R.Descrip AS CourseCodeDescrip
                                                                                               ,NULL
                                                                                               ,NULL
                                                                                               ,NULL
                                                                                               ,NULL
                                                                                               ,GBCR.ConversionResultId AS GrdBkResultId
                                                                                               ,GBCR.Comments AS GradeBookDescription
                                                                                               ,GBCR.Score AS GradeBookResult
                                                                                               ,NULL
                                                                                               ,NULL
                                                                                               ,NULL
                                                                                               ,NULL
                                                                                               ,NULL
                                                                                               ,GCT.SysComponentTypeId
                                                                                               ,NULL
                                                                                               ,NULL
                                                                                               ,SE.StuEnrollId
                                                                                               ,GBCR.MinResult
                                                                                               ,SYRES.Resource -- Student data  
                                                                                               ,NULL AS CreditsAttempted
                                                                                               ,NULL AS CreditsEarned
                                                                                               ,NULL AS Completed
                                                                                               ,NULL AS CurrentScore
                                                                                               ,NULL AS CurrentGrade
                                                                                               ,NULL AS FinalScore
                                                                                               ,NULL AS FinalGrade
                                                                                               ,NULL AS WeightedAverage_GPA
                                                                                               ,NULL AS SimpleAverage_GPA
                                                                                               ,NULL
                         ,NULL
                                                                                               ,C.CampusId
                                                                                               ,C.CampDescrip
                                                                                               ,
						--ROW_NUMBER() OVER (partition by SE.StuEnrollId,R.ReqId Order By C.CampDescrip,PV.PrgVerDescrip,T.StartDate,T.EndDate,T.TermId,T.TermDescrip,R.ReqId,R.Descrip,GCT.SysComponentTypeId,GCT.Descrip) as rownumber,
                                                                                                S.FirstName AS FirstName
                                                                                               ,S.LastName AS LastName
                                                                                               ,S.MiddleName
                                                                                               ,SYRES.ResourceID
                                                                                               ,GBWD.Required
                                                                                      FROM      arGrdBkConversionResults GBCR
                                                                                      INNER JOIN arStuEnrollments SE ON GBCR.StuEnrollId = SE.StuEnrollId
                                                                                      INNER JOIN (
                                                                                                   SELECT   StudentId
                                                                                                           ,FirstName
                                                                                                           ,LastName
                                                                                                           ,MiddleName
                                                                                                   FROM     arStudent
                                                                                                 ) S ON S.StudentId = SE.StudentId
                                                                                      INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                                                                                      INNER JOIN arTerm T ON GBCR.TermId = T.TermId
                                                                                      INNER JOIN arReqs R ON GBCR.ReqId = R.ReqId
                                                                                      INNER JOIN arGrdComponentTypes GCT ON GCT.GrdComponentTypeId = GBCR.GrdComponentTypeId
                                                                                      INNER JOIN arGrdBkWgtDetails GBWD ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
                                                                                                                           AND GBWD.GrdComponentTypeId = GBCR.GrdComponentTypeId
                                                                                      INNER JOIN arGrdBkWeights GBW ON GBWD.InstrGrdBkWgtId = GBW.InstrGrdBkWgtId
                                                                                                                       AND GBCR.ReqId = GBW.ReqId
                                                                                      INNER JOIN (
                                                                                                   SELECT   Resource
                                                                                                           ,ResourceID
                                                                                                   FROM     syResources
                                                     WHERE    ResourceTypeID = 10
                                                                                                 ) SYRES ON SYRES.ResourceID = GCT.SysComponentTypeId
                                                                                      INNER JOIN syCampuses C ON SE.CampusId = C.CampusId
                                                                                      WHERE     --MaxEffectiveDatesByCourse.EffectiveDate <= T.StartDate and
                                                                                                SE.StuEnrollId = @StuEnrollId
                                                                                                AND T.TermId = @TermId
                                                                                                AND R.ReqId = @reqid
                                                                                                AND (
                                                                                                      @sysComponentTypeId IS NULL
                                                                                                      OR GCT.SysComponentTypeId IN (
                                                                                                      SELECT    Val
                                                                                                      FROM      MultipleValuesForReportParameters(@sysComponentTypeId,
                                                                                                                                                ',',1) )
                                                                                                    )
                                                                                    ) dt
                                                                        ) dt1
                                                                 WHERE  Required = 1
                                                               );	
                END;
			
		
            DECLARE @hasallscoresbeenpostedforrequiredcomponents BIT;
            IF @CountComponentsThatHasScores >= @CourseComponentsThatNeedsToBeScored
                BEGIN
                    SET @hasallscoresbeenpostedforrequiredcomponents = 1;
                END;
            ELSE
                BEGIN
                    SET @hasallscoresbeenpostedforrequiredcomponents = 0;
                END;

				/************************************************* Changes for Build 2816 *********************/
			-- Rally case DE 738 KeyBoarding Courses
			--declare @GradesFormat varchar(50)
            SET @GradesFormat = (
                                  SELECT    dbo.GetAppSettingValue(47,@CampusId)
                                ); -- 47 refers to grades format
				-- This condition is met only for numeric grade schools
            IF (
                 @IsGradeBookNotSatisified = 0
                 AND @IsWeighted = 0
                 AND @FinalScore IS NULL
                 AND @FinalGradeDesc IS NULL
                 AND LOWER(LTRIM(RTRIM(@GradesFormat))) <> 'letter'
               )
                AND @CountComponentsThatHasScores >= 1
                BEGIN
								-- Balaji Comments : Keyboarding components do not have any weights set (@IsWeighted will be zero)
								-- For non key boarding components, weights will be set (@IsWeighted>0)
								-- For key boarding courses, if all grade books are satisfied set credits earned, attempted and completed
                    SET @CreditsAttempted = (
                                              SELECT    Credits
                                              FROM      arReqs
                                              WHERE     ReqId = @reqid
                                            );	
                    IF @hasallscoresbeenpostedforrequiredcomponents = 1
                        BEGIN
                            SET @FinAidCredits = (
                                                   SELECT   FinAidCredits
                                                   FROM     arReqs
                                                   WHERE    ReqId = @reqid
                                                 );	
                            SET @CreditsEarned = (
                                                   SELECT   Credits
                                                   FROM     arReqs
                                                   WHERE    ReqId = @reqid
                                                 );
                            SET @Completed = 1;
                        END;
                END;
			
			-- DE748 Name: ROSS: Completed field should also check for the Must Pass property of the work unit. 
            IF LOWER(LTRIM(RTRIM(@GradesFormat))) <> 'letter'
                AND @IsGradeBookNotSatisified >= 1
                BEGIN
                    SET @Completed = 0;
                    SET @CreditsEarned = 0;
                    SET @FinAidCreditsEarned = 0;
                END;
			
			--DE738 Name: ROSS: Progress Report not taking care of courses that are not weighted. 

			-- Print @TermDescrip
			-- Print @CourseCodeDescrip
			-- Print @Completed
--			-- Print LOWER(LTRIM(RTRIM(@GradesFormat)))
--			-- Print @FinalScore
--			-- Print @FinalGradedesc
--			-- Print '@IsWeighted='
--			-- Print @IsWeighted
			
            IF (
                 LOWER(LTRIM(RTRIM(@GradesFormat))) <> 'letter'
                 AND @Completed = 1
                 AND @FinalScore IS NULL
                 AND @FinalGradeDesc IS NULL
               )
                BEGIN
                    SET @CreditsAttempted = @CreditsAttempted; 
                    SET @CreditsEarned = @CreditsAttempted; 
                    SET @FinAidCreditsEarned = @FinAidCredits;
                END;

					-- In Ross Example : Externship, the student may not have completed the course but once he attempts a work unit
					-- we need to take the credits as attempted
            IF (
                 LOWER(LTRIM(RTRIM(@GradesFormat))) <> 'letter'
                 AND @Completed = 0
                 AND @FinalScore IS NULL
                 AND @FinalGradeDesc IS NULL
               )
                BEGIN
                    DECLARE @rowcount4 INT;
                    SET @rowcount4 = (
                                       SELECT   COUNT(*)
                                       FROM     arGrdBkResults
                                       WHERE    StuEnrollId = @StuEnrollId
                                                AND ClsSectionId = @ClsSectionId
                                                AND Score IS NOT NULL
                                     );
                    IF @rowcount4 >= 1
                        BEGIN
										-- Print 'Gets in to if'
                            SET @CreditsAttempted = (
                                                      SELECT    Credits
                                                      FROM      arReqs
                                                      WHERE     ReqId = @reqid
                                                    );
                            SET @CreditsEarned = 0;
                            SET @FinAidCreditsEarned = 0;
										-- Print @CreditsAttempted
                        END;
                    ELSE
                        BEGIN
                            SET @rowcount4 = (
                                               SELECT   COUNT(*)
                                               FROM     arGrdBkConversionResults
                                               WHERE    StuEnrollId = @StuEnrollId
                                                        AND ReqId = @reqid
                      AND TermId = @TermId
                                                        AND Score IS NOT NULL
                                             );
                            IF @rowcount4 >= 1
                                BEGIN
                                    SET @CreditsAttempted = (
                                                              SELECT    Credits
                                                              FROM      arReqs
                                                              WHERE     ReqId = @reqid
                                                            );
                                    SET @CreditsEarned = 0;
                                    SET @FinAidCreditsEarned = 0;
                                END;
                        END;

							--For Externship Attendance						
                    IF @sysComponentTypeId = 544
                        BEGIN
                            SET @rowcount4 = (
                                               SELECT   COUNT(*)
                                               FROM     arExternshipAttendance
                                               WHERE    StuEnrollId = @StuEnrollId
                                                        AND HoursAttended >= 1
                                             );
                            IF @rowcount4 >= 1
                                BEGIN
                                    SET @CreditsAttempted = (
                                                              SELECT    Credits
                                                              FROM      arReqs
                                                              WHERE     ReqId = @reqid
                                                            );
                                    SET @CreditsEarned = 0;
                                    SET @FinAidCreditsEarned = 0;
                                END;

                        END;
				
                END;
			/************************************************* Changes for Build 2816 *********************/
			
			-- If the final grade is not null the final grade will over ride current grade 
            IF @FinalGradeDesc IS NOT NULL
                BEGIN
                    SET @CurrentGrade = @FinalGradeDesc; 
				
                END;

			/************************* Case added for brownson starts here ******************/
			-- For Letter Grade Schools, if any one of work unit is attempted and if no final grade is posted then 
			-- set credit attempted
            DECLARE @IsScorePostedForAnyWorkUnit INT; -- If value>=1 then score was posted
            SET @IsScorePostedForAnyWorkUnit = (
                                                 SELECT COUNT(*)
                                                 FROM   arGrdBkResults
                                                 WHERE  StuEnrollId = @StuEnrollId
                                                        AND ClsSectionId = @ClsSectionId
                                                        AND Score IS NOT NULL
                                               );
            IF (
                 LOWER(LTRIM(RTRIM(@GradesFormat))) = 'letter'
                 AND @FinalGradeDesc IS NULL
                 AND @IsScorePostedForAnyWorkUnit >= 1
               )
                BEGIN
                    SET @Completed = 0;
                    SET @CreditsAttempted = (
                                              SELECT    Credits
                                              FROM      arReqs
                                              WHERE     ReqId = @reqid
                                            );
                    SET @CreditsEarned = 0;
                    SET @FinAidCreditsEarned = 0;				
                END;
			/************************* Case added for brownson ends here ******************/


				-- DE 996 Transfer Grades has completed set to no even when the credits were earned.
			-- If Credits was earned set completed to yes
            IF @IsCreditsEarned = 1
                BEGIN
                    IF (
                         @IsGradeBookNotSatisified = 0
                         AND @IsWeighted > 0
                       ) -- If all Grade books are satisfied and all courses are weighted
                        BEGIN
                            SET @Completed = 1;
                        END;
                    ELSE
                        BEGIN
                            SET @Completed = 0;
                        END;
                END;
		
				
			-- For Letter grade schools no need to check for grade books satisifed condition
            IF LOWER(LTRIM(RTRIM(@GradesFormat))) = 'letter'
                BEGIN
                    IF @IsCreditsEarned = 1
                        BEGIN
                            SET @Completed = 1;
                        END;
                END;

			-- numeric and non ross schools
			-- no need to check if student satisfied grade book and weights
			--declare @ShowROSSOnlyTabsForStudent_Value bit
			--set @ShowROSSOnlyTabsForStudent_Value = (select value from syConfigAppSetValues where settingId=68)
            IF LOWER(LTRIM(RTRIM(@GradesFormat))) <> 'letter'
                AND @ShowROSSOnlyTabsForStudent_Value = 0
                BEGIN
                    IF @IsCreditsEarned = 1
                        OR (
                             @FinalScore IS NOT NULL
                             AND @IsPass = 1
                           )
                        BEGIN
                            SET @Completed = 1;		
                        END;
                END;
				
			-- This condition does not apply for key boarding courses, as no final score or grade is posted and
			-- isCreditsEarned will always be NULL
            IF @IsCreditsEarned IS NULL
                BEGIN
                    SET @Completed = 0;
					-- Only for Key boarding courses
                    IF (
                         @IsGradeBookNotSatisified = 0
                         AND @IsWeighted = 0
                         AND @FinalScore IS NULL
                         AND @FinalGradeDesc IS NULL
                         AND LOWER(LTRIM(RTRIM(@GradesFormat))) <> 'letter'
                       )
                        AND @CountComponentsThatHasScores >= 1
                        BEGIN
								-- Balaji Comments : Keyboarding components do not have any weights set (@IsWeighted will be zero)
								-- For non key boarding components, weights will be set (@IsWeighted>0)
								-- For key boarding courses, if all grade books are satisfied set credits earned, attempted and completed
                            SET @CreditsAttempted = (
                                                      SELECT    Credits
                                                      FROM      arReqs
                                                      WHERE     ReqId = @reqid
                                                    );	
									
                            IF @hasallscoresbeenpostedforrequiredcomponents = 1
                                BEGIN
                                    SET @FinAidCredits = (
                                                           SELECT   FinAidCredits
                                                           FROM     arReqs
                                                           WHERE    ReqId = @reqid
                                                         );	
                                    SET @CreditsEarned = (
                                                           SELECT   Credits
                                                           FROM     arReqs
                                                           WHERE    ReqId = @reqid
                                                         );
                            SET @Completed = 1;
                                END;
                        END;
                END;
			
			-- Unitek : If the externship component was not satisfied then set completed to no
			--IF @sysComponentTypeId = 544 
			--		BEGIN
			--			IF @IsGradeBookNotSatisified>=1 -- work unit comp not satisfied
			--				BEGIN
			--					SET @Completed = 0
			--				END
			--			ELSE
			--				BEGIN
			--					SET @Completed = 1
			--				end
			--		end
				
			-- DE1148
            IF @Completed = 1
                AND @IsCreditsEarned = 1
                BEGIN
                    SET @CreditsEarned = (
                                           SELECT   Credits
                                           FROM     arReqs
                                           WHERE    ReqId = @reqid
                                         );
                    SET @FinAidCreditsEarned = @FinAidCredits;
                END;

            DECLARE @varGradeRounding VARCHAR(3);
            DECLARE @roundfinalscore DECIMAL(18,4);
            SET @varGradeRounding = (
                                      SELECT    dbo.GetAppSettingValue(45,@CampusId)
                                    );
			-- If rounding is set to yes, then round the scores to next available score or ignore rounding
            IF ( LOWER(@varGradeRounding) = 'yes' )
                BEGIN
                    IF @FinalScore IS NOT NULL
                        BEGIN
                            SET @FinalScore = ROUND(@FinalScore,0);
                            SET @CurrentScore = @FinalScore;
                        END;
                    IF @FinalScore IS NULL
                        AND @CurrentScore IS NOT NULL
                        BEGIN
                            SET @CurrentScore = ROUND(@CurrentScore,0);
                        END;
                END;
            ELSE
                BEGIN
                    IF @FinalScore IS NOT NULL
                        BEGIN
                            SET @CurrentScore = @FinalScore;
                        END;	
                    IF @FinalScore IS NULL
                        AND @CurrentScore IS NOT NULL
                        BEGIN
                            SET @CurrentScore = ROUND(@CurrentScore,0);
                        END;
                END;
				
				
            IF @CourseComponentsThatNeedsToBeScored >= 1
                AND @CountComponentsThatHasScores = 0
                BEGIN
                    SET @CreditsAttempted = 0;
                END;

            IF @CountComponentsThatHasScores >= 1
                BEGIN
                    SET @CreditsAttempted = (
                                              SELECT    Credits
                                              FROM      arReqs
                                              WHERE     ReqId = @reqid
                                            );
                END;
			
            IF LOWER(LTRIM(RTRIM(@GradesFormat))) = 'letter'
                AND @FinalGrade IS NOT NULL
                AND @CourseComponentsThatNeedsToBeScored = 0
                AND @IsCreditsAttempted = 1
                BEGIN
                    SET @CreditsAttempted = (
                                              SELECT    Credits
                                              FROM      arReqs
                                              WHERE     ReqId = @reqid
                                            );
                END;
			
            IF LOWER(LTRIM(RTRIM(@GradesFormat))) <> 'letter'
                AND @FinalScore IS NOT NULL
                AND @CourseComponentsThatNeedsToBeScored = 0
                AND @IsCreditsAttempted = 1
                BEGIN
                    SET @CreditsAttempted = (
                                              SELECT    Credits
                                              FROM      arReqs
                          WHERE     ReqId = @reqid
                                            );
                END;
				
			-- Check if student passed the course
            IF LOWER(LTRIM(RTRIM(@GradesFormat))) = 'letter'
                AND @FinalGrade IS NOT NULL
                AND @IsPass = 0
                BEGIN
                    SET @Completed = 0;
                    IF @IsCreditsEarned IS NULL
                        OR @IsCreditsEarned = 0
                        BEGIN
                            SET @CreditsEarned = 0;
                        END;
                    IF @IsCreditsAttempted IS NULL
                        OR @IsCreditsAttempted = 0
                        BEGIN
                            SET @CreditsAttempted = 0;
                        END;
                END;
				
			---- Unitek/Ross : If the externship component was not satisfied then set completed to no
			--	-- Unitek : If the externship component was not satisfied then set completed to no
            IF @sysComponentTypeId = 544
                OR @sysComponentTypeId = 500
                OR @sysComponentTypeId = 503
                BEGIN
                    IF @IsGradeBookNotSatisified >= 1 -- work unit comp not satisfied
                        BEGIN
                            SET @Completed = 0;
                        END;
                    ELSE
                        BEGIN
                            SET @Completed = 1;
                        END;
                END;
			
			--PRINT @CourseCodeDescrip 		
			--PRINT @Completed
			
	--				DECLARE @CountWorkUnitsNotSatisfied_500503544 int
	---- Work unit not satisfied		
	--if  LOWER(LTRIM(RTRIM(@SetGradeBookAt))) = 'courselevel'
	--begin
		
	--	Create table #Temp1(Id uniqueidentifier,StuEnrollId uniqueidentifier,
	--	TermId uniqueidentifier,GradeBookDescription varchar(50),Number int,
	--	GradeBookSysComponentTypeId int,GradeBookScore decimal(18,2),MinResult decimal(18,2),
	--	GradeComponentDescription varchar(50),RowNumber int,ClsSectionId uniqueidentifier)
	--	 Declare @Id uniqueidentifier,@Descrip varchar(50),@Number int,@GrdComponentTypeId int,@Counter int,@times int
	--	 Declare @MinResult decimal(18,2),@GrdComponentDescription varchar(50)
		
	--	set @Counter = 0
		
	--	Declare @TermStartDate1 datetime
	--	Set @TermStartDate1 = (select StartDate from arTerm where TermId=@TermId)
		
	--		Create table #temp2(ReqId uniqueidentifier,EffectiveDate datetime)
	--		insert into #temp2
	--		select ReqId,Max(EffectiveDate) as EffectiveDate from arGrdBkWeights where ReqId=@ReqId
	--		and EffectiveDate<=@TermStartDate1
	--		Group By ReqId
		
	--    declare getUsers_Cursor cursor for
	--    Select *,ROW_NUMBER() OVER (partition by @StuEnrollId,@TermId,SysComponentTypeId 
	--			Order By SysComponentTypeId,Descrip) as rownumber 
	--    from
	--    (
	--	 select Distinct
	--		isnull(GD.InstrGrdBkWgtDetailId,newid()) as ID, 
	--			GC.Descrip, 
	--			GD.Number, 
	--			GC.SysComponentTypeId,
	--			(CASE WHEN GC.SysComponentTypeId  in (500,503,504,544) THEN GD.Number 
	--						   ELSE (SELECT MIN(MinVal) 
	--									FROM arGradeScaleDetails GSD, arGradeSystemDetails GSS 
	--									WHERE GSD.GrdSysDetailId=GSS.GrdSysDetailId 
	--									AND GSS.IsPass=1 and GSD.GrdScaleId=CS.GrdScaleId) 
	--							END 
	--						) AS MinResult,
	--			S.Resource as GradeComponentDescription,CS.ClsSectionId
	--			--,MaxEffectiveDatesByCourse.ReqId 
	-- from 
	--	  arGrdComponentTypes GC, 
	--	  (
	--		Select * from arGrdBkWgtDetails where InstrGrdBkWgtId in
	--		(Select t1.InstrGrdBkWgtId from arGrdBkWeights t1,#temp2 t2 
	--		where t1.ReqId=t2.ReqId and t1.EffectiveDate=t2.EffectiveDate)
	--	   ) GD, arGrdBkWeights GW, 
	--	  arReqs R, arClassSections CS, syResources S, arResults RES,arTerm T
	-- where 
	--	  GC.GrdComponentTypeId = GD.GrdComponentTypeId and 
	--	  GD.InstrGrdBkWgtId=GW.InstrGrdBkWgtId and 
	--	  GW.ReqId=R.ReqId and R.ReqId=Cs.ReqId and CS.TermId=@TermId 
	--	  and RES.TestId = CS.ClsSectionId and RES.StuEnrollId=@StuEnrollId
	--	  and GD.Number > 0 and GC.SysComponentTypeId=S.ResourceId and
	--	  CS.TermId=T.TermId and R.ReqId=@ReqId
	--		) dt
	--		order by 
	--		   SysComponentTypeId,RowNumber
	--	open getUsers_Cursor
	--	fetch next from getUsers_Cursor
	--	into 
	--		@ID,@Descrip,@Number,@GrdComponentTypeId,@MinResult,@GrdComponentDescription,@ClsSectionId,@rownumber
	--	set @Counter = 0
	--	declare @Score decimal(18,2),@GrdCompDescrip varchar(50)
	--	while @@FETCH_STATUS = 0
	--	begin
	--		Print @number
	--		set @times = 1
			
	--		--if (@GrdComponentTypeId = 500 or @GrdComponentTypeId=503 or @GrdComponentTypeId=544)
	--		--	begin
	--		--		set @GrdCompDescrip = @Descrip
	--		--			set @Score = (select Top 1 Score from arGrdBkResults where StuEnrollId=@StuEnrollId and InstrGrdBkWgtDetailId=@Id and ResNum=@times and ClsSectionId=@ClsSectionId) 
	--		--			if @Score is NULL
	--		--				begin
	--		--					set @Score = (select Top 1 Score from arGrdBkResults where StuEnrollId=@StuEnrollId and InstrGrdBkWgtDetailId=@Id and ResNum=(@times-1) and ClsSectionId=@ClsSectionId) 	
	--		--				end
	--		--			insert into #temp1 values(@Id,@StuEnrollId,@TermId,
	--		--			@GrdCompDescrip,@Number,@GrdComponentTypeId,@Score,@MinResult,@GrdComponentDescription,@rownumber,@ClsSectionId)
	--		--	end
	--		if (@GrdComponentTypeId = 500 or @GrdComponentTypeId=503 or @GrdComponentTypeId=544)
	--			begin
	--				set @GrdCompDescrip = @Descrip
	--				if (@GrdComponentTypeId = 500 or @GrdComponentTypeId=503)
	--					begin
	--						set @Score = (select SUM(Score) from arGrdBkResults where StuEnrollId=@StuEnrollId and InstrGrdBkWgtDetailId=@Id  
	--									and ClsSectionId=@ClsSectionId) 
	--					end
	--				if (@GrdComponentTypeId=544)
	--					begin
	--						set @Score = (select SUM(HoursAttended) from arExternshipAttendance where StuEnrollId=@StuEnrollId) 
	--					end
	--					insert into #temp1 values(@Id,@StuEnrollId,@TermId,
	--					@GrdCompDescrip,@Number,@GrdComponentTypeId,@Score,@MinResult,@GrdComponentDescription,@rownumber,@ClsSectionId)
	--			end
	--		else
	--			begin
	--				while @times <= @number
	--					begin
	--						Print @times
							
	--						if @Number>1 
	--							begin
	--								set @GrdCompDescrip = 	@Descrip+cast(@times as char)
	--								set @Score = (select Score from arGrdBkResults where StuEnrollID=@StuEnrollId and 
	--											  InstrGrdBkWgtDetailId=@Id and resnum=@times and ClsSectionId=@ClsSectionId)
												  
																	
	--								set @rownumber = @times
	--							end
	--						else
	--							begin
	--								set @GrdCompDescrip = @Descrip
	--								set @Score = (select Top 1 Score from arGrdBkResults where StuEnrollId=@StuEnrollId and InstrGrdBkWgtDetailId=@Id and ResNum=@times and ClsSectionId=@ClsSectionId) 
	--								if @Score is NULL
	--									begin
	--										set @Score = (select Top 1 Score from arGrdBkResults where StuEnrollId=@StuEnrollId and InstrGrdBkWgtDetailId=@Id and ResNum=(@times-1) and ClsSectionId=@ClsSectionId) 	
	--									end
	--							end
	--						insert into #temp1 values(@Id,@StuEnrollId,@TermId,
	--						@GrdCompDescrip,@Number,@GrdComponentTypeId,@Score,@MinResult,@GrdComponentDescription,@rownumber,@ClsSectionId)
							
	--						set @times = @times + 1
	--					end
	--			end
	--		fetch next from getUsers_Cursor
	--		into
	--		 @ID,@Descrip,@Number,@GrdComponentTypeId,@MinResult,@GrdComponentDescription,@ClsSectionId,@rownumber
	--	end
	--	close getUsers_Cursor
	--	deallocate getUsers_Cursor
		
	--	Create table #Temp3(GetCountOfWorkUnit_500503544_NotSatisfied INT)
	--	INSERT INTO #Temp3 
	--	SELECT COUNT(*) AS RowNumber FROM 
	--	(		
	--	select * from #temp1 --where GradeBookSysComponentTypeId=501
	--	--order by 
	--	--	GradeBookSysComponentTypeId,GradeBookDescription,RowNumber
	--	union
	--	select		
	--				GBWD.InstrGrdBkWgtDetailId,SE.StuEnrollId,
	--				T.TermId,
	--				GCT.Descrip as GradeBookDescription,
	--				GBWD.Number, GCT.SysComponentTypeId,
	--				GBCR.Score,GBCR.MinResult,SYRES.Resource,
	--				ROW_NUMBER() OVER (partition by SE.StuEnrollId,T.TermId,GCT.SysComponentTypeId 
	--				Order By GCT.SysComponentTypeId,GCT.Descrip) as rownumber,
	--				(select Top 1 ClsSectionId from arClassSections where TermId=T.TermId and ReqId=R.ReqId) as ClsSectionId 
				
							
	--					from	arGrdBkConversionResults GBCR INNER JOIN arStuEnrollments SE ON GBCR.StuEnrollId = SE.StuEnrollId
	--							 INNER JOIN (Select StudentId,FirstName,LastName,MiddleName from arStudent) S ON S.StudentId = SE.StudentId
	--							INNER JOIN arPrgVersions PV  ON SE.prgVerId = PV.PrgVerId
	--							INNER JOIN arTerm T ON GBCR.TermId = T.TermId
	--							INNER JOIN arReqs R ON GBCR.ReqId = R.ReqId
	--							INNER JOIN arGrdComponentTypes GCT ON GCT.GrdComponentTypeId=GBCR.GrdComponentTypeId
	--							INNER JOIN arGrdBkWgtDetails GBWD ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId AND GBWD.GrdComponentTypeId=GBCR.GrdComponentTypeId
	--							INNER JOIN arGrdBkWeights GBW ON GBWD.InstrGrdBkWgtId=GBW.InstrGrdBkWgtId AND GBCR.ReqId=GBW.ReqId
	--							INNER JOIN (select ReqId,Max(EffectiveDate) as EffectiveDate from arGrdBkWeights group by ReqId) as MaxEffectiveDatesByCourse ON GBCR.ReqId = MaxEffectiveDatesByCourse.ReqId
	--							INNER JOIN (Select Resource,ResourceId from syResources where ResourceTypeId=10) SYRES ON SYRES.ResourceId=GCT.SysComponentTypeId
	--							INNER JOIN syCampuses C ON SE.CampusId = C.CampusId
	--					where  
	--							MaxEffectiveDatesByCourse.EffectiveDate <= T.StartDate and
	--							SE.StuEnrollId = @StuEnrollId and T.TermId = @TermId and --R.ReqId = @ReqId and 
	--							(@SysComponentTypeId is null or GCT.SysComponentTypeId in (Select Val from [MultipleValuesForReportParameters](@SysComponentTypeId,'','',1)))
	--							--and GCT.SysComponentTypeId=501
	--		) derT1 WHERE GradeBookSysComponentTypeId IN (500,503,544) AND (GradeBookScore IS NULL OR MinResult>GradeBookScore)
	--	--order by 
	--	--	GradeBookSysComponentTypeId,GradeBookDescription,RowNumber
		
	--	Set @CountWorkUnitsNotSatisfied_500503544 = (SELECT TOP 1 * FROM #temp3)
	--	DROP TABLE #temp3
	--	drop table #temp2
	--	drop table #temp1
	--end

					
			
			-- Unitek/Ross : If the externship component was not satisfied then set completed to no
			-- Unitek : If the externship component was not satisfied then set completed to no
			--if  LOWER(LTRIM(RTRIM(@SetGradeBookAt))) = 'courselevel'
			--BEGIN 
			--	IF @CountWorkUnitsNotSatisfied_500503544>=1 OR @IsGradeBookNotSatisified>=1 
			--		BEGIN
			--			SET @Completed = 0
			--		END
			--	ELSE
			--		BEGIN
			--			SET @Completed = 1
			--			IF (@FinalScore IS NOT NULL OR @FinalGradeDesc IS NOT NULL) AND @isPass=0
			--			begin
			--				SET @Completed = 0
			--			END
			--				IF (@FinalScore IS NULL OR @FinalGradeDesc IS NULL) and LOWER(LTRIM(RTRIM(@GradesFormat))) <> 'letter' AND @CourseComponentsThatNeedsToBeScored>=1 and @CountComponentsThatHasScores=0
			--				BEGIN
			--					SET @Completed=0
			--				end
			--		END
			--END
			
			
			
            DELETE  FROM syCreditSummary
            WHERE   StuEnrollId = @StuEnrollId
                    AND TermId = @TermId
                    AND ReqId = @reqid
                    AND ClsSectionId = @ClsSectionId;

            INSERT  INTO syCreditSummary
            VALUES  ( @StuEnrollId,@TermId,@TermDescrip,@reqid,@CourseCodeDescrip,@ClsSectionId,@CreditsEarned,@CreditsAttempted,@CurrentScore,@CurrentGrade,
                      @FinalScore,@FinalGradeDesc,@Completed,@FinalGPA,@Product_WeightedAverage_Credits_GPA,@Count_WeightedAverage_Credits,
                      @Product_SimpleAverage_Credits_GPA,@Count_SimpleAverage_Credits,'sa',GETDATE(),@ComputedSimpleGPA,@ComputedWeightedGPA,@CourseCredits,NULL,
                      NULL,@FinAidCreditsEarned,NULL,NULL,@TermStartDate );

		
							 
							 
            DECLARE @wCourseCredits DECIMAL(18,2)
               ,@wWeighted_GPA_Credits DECIMAL(18,2)
               ,@sCourseCredits DECIMAL(18,2)
               ,@sSimple_GPA_Credits DECIMAL(18,2);
			-- For weighted average
            SET @ComputedWeightedGPA = 0;
            SET @ComputedSimpleGPA = 0;
            SET @wCourseCredits = (
                                    SELECT  SUM(coursecredits)
                                    FROM    syCreditSummary
                                    WHERE   StuEnrollId = @StuEnrollId
                                            AND TermId = @TermId
                                            AND FinalGPA IS NOT NULL
                                  );
            SET @wWeighted_GPA_Credits = (
                                           SELECT   SUM(coursecredits * FinalGPA)
                                           FROM     syCreditSummary
                                           WHERE    StuEnrollId = @StuEnrollId
                                                    AND TermId = @TermId
                                                    AND FinalGPA IS NOT NULL
                                         );
            IF @wCourseCredits >= 1
                BEGIN
                    SET @ComputedWeightedGPA = @wWeighted_GPA_Credits / @wCourseCredits;
                END;
			--For Simple Average
            SET @sCourseCredits = (
                                    SELECT  COUNT(*)
                                    FROM    syCreditSummary
                                    WHERE   StuEnrollId = @StuEnrollId
                                            AND TermId = @TermId
                                            AND FinalGPA IS NOT NULL
                                  );
            SET @sSimple_GPA_Credits = (
                                         SELECT SUM(FinalGPA)
                                         FROM   syCreditSummary
                                         WHERE  StuEnrollId = @StuEnrollId
                                                AND TermId = @TermId
                                                AND FinalGPA IS NOT NULL
                                       );
            IF @sCourseCredits >= 1
                BEGIN
                    SET @ComputedSimpleGPA = @sSimple_GPA_Credits / @sCourseCredits;
                END; 
			--CumulativeGPA
            DECLARE @cumCourseCredits DECIMAL(18,2)
               ,@cumWeighted_GPA_Credits DECIMAL(18,2)
               ,@cumWeightedGPA DECIMAL(18,2);
            SET @cumWeightedGPA = 0;
            SET @cumCourseCredits = (
                                      SELECT    SUM(coursecredits)
                                      FROM      syCreditSummary
                                      WHERE     StuEnrollId = @StuEnrollId
                                                AND FinalGPA IS NOT NULL
                                    );
            SET @cumWeighted_GPA_Credits = (
                                             SELECT SUM(coursecredits * FinalGPA)
                                             FROM   syCreditSummary
                                             WHERE  StuEnrollId = @StuEnrollId
                                                    AND FinalGPA IS NOT NULL
                                           );
			
            IF @cumCourseCredits >= 1
                BEGIN
                    SET @cumWeightedGPA = @cumWeighted_GPA_Credits / @cumCourseCredits;
                END; 
			--CumulativeSimpleGPA
            DECLARE @cumSimpleCourseCredits DECIMAL(18,2)
               ,@cumSimple_GPA_Credits DECIMAL(18,2)
               ,@cumSimpleGPA DECIMAL(18,2);
            SET @cumSimpleGPA = 0;
            SET @cumSimpleCourseCredits = (
                                            SELECT  COUNT(coursecredits)
                                            FROM    syCreditSummary
                                            WHERE   StuEnrollId = @StuEnrollId
                                                    AND FinalGPA IS NOT NULL
                                          );
            SET @cumSimple_GPA_Credits = (
                                           SELECT   SUM(FinalGPA)
                                           FROM     syCreditSummary
                                           WHERE    StuEnrollId = @StuEnrollId
                                                    AND FinalGPA IS NOT NULL
                                         );
            IF @cumSimpleCourseCredits >= 1
                BEGIN
                    SET @cumSimpleGPA = @cumSimple_GPA_Credits / @cumSimpleCourseCredits;
                END; 
			--Average calculation
            DECLARE @termAverageSum DECIMAL(18,2)
               ,@CumAverage DECIMAL(18,2)
               ,@cumAverageSum DECIMAL(18,2)
               ,@cumAveragecount INT;
			-- Term Average
            SET @TermAverageCount = (
                                      SELECT    COUNT(*)
                                      FROM      syCreditSummary
                                      WHERE     StuEnrollId = @StuEnrollId 
									--and Completed=1 
                                                AND TermId = @TermId
                                                AND FinalScore IS NOT NULL
                                    );
            SET @termAverageSum = (
                                    SELECT  SUM(FinalScore)
                                    FROM    syCreditSummary
                                    WHERE   StuEnrollId = @StuEnrollId 
									--and Completed=1 
                                            AND TermId = @TermId
                                            AND FinalScore IS NOT NULL
                                  );
            SET @TermAverage = @termAverageSum / @TermAverageCount; 
			-- Cumulative Average
            SET @cumAveragecount = (
                                     SELECT COUNT(*)
                                     FROM   syCreditSummary
                                     WHERE  StuEnrollId = @StuEnrollId 
									--and Completed=1 
                                            AND FinalScore IS NOT NULL
                                   );
            SET @cumAverageSum = (
                                   SELECT   SUM(FinalScore)
                                   FROM     syCreditSummary
                                   WHERE    StuEnrollId = @StuEnrollId
                                            AND 
									--Completed=1 and 
                                            FinalScore IS NOT NULL
                                 );
            SET @CumAverage = @cumAverageSum / @cumAveragecount; 
			
			
            UPDATE  syCreditSummary
            SET     TermGPA_Simple = @ComputedSimpleGPA
                   ,TermGPA_Weighted = @ComputedWeightedGPA
                   ,Average = @TermAverage
            WHERE   StuEnrollId = @StuEnrollId
                    AND TermId = @TermId; 
			
			--Update Cumulative GPA
            UPDATE  syCreditSummary
            SET     CumulativeGPA = @cumWeightedGPA
                   ,CumulativeGPA_Simple = @cumSimpleGPA
                   ,CumAverage = @CumAverage
            WHERE   StuEnrollId = @StuEnrollId;
			

            SET @PrevStuEnrollId = @StuEnrollId; 
            SET @PrevTermId = @TermId; 
            SET @PrevReqId = @reqid;

            FETCH NEXT FROM GetExternshipAttendance_Cursor INTO @StuEnrollId,@TermId,@TermDescrip,@TermStartDate,@reqid,@CourseCodeDescrip,@FinalScore,
                @FinalGrade,@sysComponentTypeId,@CreditsAttempted,@ClsSectionId,@Grade,@IsPass,@IsCreditsAttempted,@IsCreditsEarned,@PrgVerId,@IsInGPA,
                @FinAidCredits; 
        END;
    CLOSE GetExternshipAttendance_Cursor;
    DEALLOCATE GetExternshipAttendance_Cursor;

--==========================================================================================
-- END TRIGGER TR_ExternshipAttendance  -- AFTER UPDATE  
--==========================================================================================


    SET NOCOUNT OFF;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO

PRINT N'Refreshing [dbo].[VIEW_LeadUserTask]';
GO
IF OBJECT_ID(N'[dbo].[VIEW_LeadUserTask]', 'V') IS NOT NULL
    EXEC sp_refreshview N'[dbo].[VIEW_LeadUserTask]';
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Refreshing [dbo].[View_Messages]';
GO
IF OBJECT_ID(N'[dbo].[View_Messages]', 'V') IS NOT NULL
    EXEC sp_refreshview N'[dbo].[View_Messages]';
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Refreshing [dbo].[VIEW_SySDFModuleValue_Lead]';
GO
IF OBJECT_ID(N'[dbo].[VIEW_SySDFModuleValue_Lead]', 'V') IS NOT NULL
    EXEC sp_refreshview N'[dbo].[VIEW_SySDFModuleValue_Lead]';
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Refreshing [dbo].[View_TaskNotes]';
GO
IF OBJECT_ID(N'[dbo].[View_TaskNotes]', 'V') IS NOT NULL
    EXEC sp_refreshview N'[dbo].[View_TaskNotes]';
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'If Exists sp USP_GetPrgVersionsByCampusId, delete it '
GO
IF EXISTS ( SELECT  1 
            FROM    sys.objects 
            WHERE   object_id = OBJECT_ID(N'[dbo].[USP_GetPrgVersionsByCampusId]') 
                    AND type IN ( N'P',N'PC' ) ) 
    BEGIN 
		PRINT N'    Dropping SP USP_GetPrgVersionsByCampusId'; 
        DROP PROCEDURE USP_GetPrgVersionsByCampusId; 
    END; 
GO 
IF @@ERROR <> 0 
    SET NOEXEC ON; 
GO 
PRINT N'Creating USP_GetPrgVersionsByCampusId'; 
GO 

--=================================================================================================
-- USP_GetPrgVersionsByCampusId
--=================================================================================================
-- =============================================
-- Author:		JAGG
-- Create date: 06/06/2017
-- Description: Get List of active Program Versions for CampusId 
-- EXECUTE  [USP_GetPrgVersionsByCampusId   @CampusId = N'F335D044-F05B-4F4F-8590-39552C6FDC3F'              
--                 
-- =============================================
CREATE PROCEDURE [dbo].[USP_GetPrgVersionsByCampusId]
    @CampusId NVARCHAR(50)
AS
    BEGIN
        SELECT   DISTINCT APV.PrgVerId
                ,APV.PrgVerDescrip AS PrgVerDescrip
        FROM     arPrgVersions AS APV
        INNER JOIN syStatuses AS SS ON APV.StatusId = SS.StatusId
        INNER JOIN dbo.syCampGrps AS GG ON GG.CampGrpId = APV.CampGrpId
        LEFT JOIN dbo.syCampuses AS CC ON CC.CampusId = GG.CampusId
        WHERE    SS.StatusCode = 'A'
                 AND (
                         CC.CampusId = @CampusId
                         OR GG.CampGrpCode = 'All'
                     )
        ORDER BY APV.PrgVerDescrip;
    END;
--=================================================================================================
-- END  --  USP_GetPrgVersionsByCampusId
--=================================================================================================
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'If exist VIEW GlTransView1, delete it';
GO
--=================================================================================================
-- CREATE VIEW GlTransView1
--=================================================================================================
IF EXISTS (
              SELECT 1
              FROM   sys.views AS V
              WHERE  object_id = OBJECT_ID(N'[dbo].[GlTransView1]')
                     AND type IN ( N'V' )
          )
    BEGIN
        PRINT '    Dropping view GlTransView1';
        DROP VIEW GlTransView1;
    END;

GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating VIEW GlTransView1';
GO
CREATE VIEW [dbo].[GlTransView1]
AS
    SELECT  T.TransTypeId
           ,T.TransCodeId
           ,TCGLA.TypeId
           ,T.TransAmount
           ,TCGLA.TransCodeGLAccountId
           ,Percentage
           ,T.TransactionId
           ,T.TransDate AS Date
           ,T.TransDescrip + '-' + (
                                     SELECT FirstName + ' ' + LastName
                                     FROM   arStudent
                                     WHERE  StudentId = (
                                                          SELECT    StudentId
                                                          FROM      arStuEnrollments
                                                          WHERE     StuEnrollId = T.StuEnrollId
                                                        )
                                   ) AS TransCodeDescrip
           ,(
              SELECT    TransCodeDescrip
              FROM      saTransCodes
              WHERE     TransCodeId = T.TransCodeId
            ) TransCodeDescripSummary
           ,TCGLA.GLAccount
           ,(
              SELECT    CASE WHEN TypeId = 0
                                  OR TypeId = 2 THEN T.TransAmount * ( Percentage / 100 )
                             WHEN TypeId = 1
                                  OR TypeId = 3 THEN T.TransAmount * ( Percentage / 100 ) * ( -1 )
                        END
            ) AS Amount
           ,T.CampusId
           ,TC.TransCodeDescrip AS TransCode
    FROM    saTransactions T
           ,dbo.saTransCodeGLAccounts TCGLA
           ,saTransCodes TC
    WHERE   TCGLA.TransCodeId = T.TransCodeId
            AND T.Voided = 0
            AND TypeId IN ( 0,1 )
            AND T.TransCodeId = TC.TransCodeId
    UNION ALL
    SELECT  T.TransTypeId
           ,T.TransCodeId
           ,TCGLA.TypeId
           ,T.TransAmount
           ,TCGLA.TransCodeGLAccountId
           ,Percentage
           ,T.TransactionId
           ,D.DefRevenueDate AS Date
           ,T.TransDescrip + '-' + (
                                     SELECT FirstName + ' ' + LastName
                                     FROM   arStudent
                                     WHERE  StudentId = (
                                                          SELECT    StudentId
                                                          FROM      arStuEnrollments
                                                          WHERE     StuEnrollId = T.StuEnrollId
                                                        )
                                   ) AS TransCodeDescrip
           ,(
              SELECT    TransCodeDescrip
              FROM      saTransCodes
              WHERE     TransCodeId = T.TransCodeId
            ) TransCodeDescripSummary
           ,TCGLA.GLAccount
           ,(
              SELECT    CASE WHEN TypeId = 0
                                  OR TypeId = 2 THEN D.Amount
                             WHEN TypeId = 1
                                  OR TypeId = 3 THEN D.Amount * ( -1 )
                        END
            ) AS Amount
           ,T.CampusId
           ,TC.TransCodeDescrip AS TransCode
    FROM    dbo.saDeferredRevenues D
           ,saTransactions T
           ,dbo.saTransCodeGLAccounts TCGLA
           ,saTransCodes TC
    WHERE   T.TransactionId = D.TransactionId
            AND TCGLA.TransCodeId = T.TransCodeId
            AND T.Voided = 0
            AND TypeId IN ( 2,3 )
            AND T.TransCodeId = TC.TransCodeId
            AND TC.TransCodeId = TCGLA.TransCodeId
    UNION ALL
    SELECT  T.TransTypeId
           ,T.PaymentCodeId
           ,TCGLA.TypeId
           ,T.TransAmount
           ,TCGLA.TransCodeGLAccountId
           ,Percentage
           ,T.TransactionId
           ,T.TransDate AS Date
           ,T.TransDescrip + '-' + (
                                     SELECT FirstName + ' ' + LastName
                                     FROM   arStudent
                                     WHERE  StudentId = (
                                                          SELECT    StudentId
                                                          FROM      arStuEnrollments
                                                          WHERE     StuEnrollId = T.StuEnrollId
                                                        )
                                   ) AS TransCodeDescrip
           ,(
              SELECT    TransCodeDescrip
              FROM      saTransCodes
              WHERE     TransCodeId = T.TransCodeId
            ) TransCodeDescripSummary
           ,TCGLA.GLAccount
           ,(
              SELECT    CASE WHEN TypeId = 0
                                  OR TypeId = 2 THEN T.TransAmount * ( Percentage / 100 )
                             WHEN TypeId = 1
                                  OR TypeId = 3 THEN T.TransAmount * ( Percentage / 100 ) * ( -1 )
                        END
            ) AS Amount
           ,T.CampusId
           ,TC.TransCodeDescrip AS TransCode
    FROM    saTransactions T
           ,dbo.saTransCodeGLAccounts TCGLA
           ,saTransCodes TC
    WHERE   TCGLA.TransCodeId = T.PaymentCodeId
            AND T.Voided = 0
            AND TypeId IN ( 0,1 )
            AND T.TransCodeId = TC.TransCodeId; 




GO



IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'If exist VIEW GlTransView2, delete it';
GO
--=================================================================================================
-- CREATE VIEW GlTransView2
--=================================================================================================
IF EXISTS (
              SELECT 1
              FROM   sys.views AS V
              WHERE  object_id = OBJECT_ID(N'[dbo].[GlTransView2]')
                     AND type IN ( N'V' )
          )
    BEGIN
        PRINT '    Dropping view GlTransView2';
        DROP VIEW GlTransView2;
    END;

GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating VIEW GlTransView2';
GO
CREATE VIEW [dbo].[GlTransView2]
AS
    SELECT
 DISTINCT   TransTypeId
           ,TransCodeId
           ,TypeId
           ,TransAmount
           ,TransCodeGLAccountId
           ,Percentage
           ,TransactionId
           ,Date
           ,TransCodeDescrip
           ,TransCodeDescripSummary
           ,GLAccount
           ,CampusId
           ,Amount
           ,(
              SELECT    CASE WHEN TransTypeId = 0
                                  AND Amount < 0 THEN Amount * ( -1 )
                             WHEN TransTypeId = 2
                                  AND (
                                        TypeId = 1
                                        OR TypeId = 3
                                      ) THEN Amount
                             WHEN TransTypeId = 1
                                  AND (
                                        TypeId = 1
                                        OR TypeId = 3
                                      )
                                  AND Amount < 0 THEN Amount * ( -1 )
                             WHEN TransTypeId = 1
                                  AND (
                                        TypeId = 1
                                        OR TypeId = 3
                                      )
                                  AND Amount > 0 THEN Amount
                        END
            ) AS CreditAmount
           ,(
              SELECT    CASE WHEN TransTypeId = 0
                                  AND Amount > 0 THEN Amount
                             WHEN TransTypeId = 2
                                  AND (
                                        TypeId = 0
                                        OR TypeId = 2
                                      ) THEN Amount * ( -1 )
                             WHEN TransTypeId = 1
                                  AND (
                                        TypeId = 0
                                        OR TypeId = 2
                                      )
                                  AND Amount < 0 THEN Amount * ( -1 )
                             WHEN TransTypeId = 1
                                  AND (
                                        TypeId = 0
                                        OR TypeId = 2
                                      )
                                  AND Amount > 0 THEN Amount
                        END
            ) AS DebitAmount
           ,TransCode
    FROM    GlTransView1;
 



GO



IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'If exist VIEW GradeView, delete it';
GO
--=================================================================================================
-- CREATE VIEW GradeView
--=================================================================================================
IF EXISTS (
              SELECT 1
              FROM   sys.views AS V
              WHERE  object_id = OBJECT_ID(N'[dbo].[GradeView]')
                     AND type IN ( N'V' )
          )
    BEGIN
        PRINT '    Dropping view GradeView';
        DROP VIEW GradeView;
    END;

GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating VIEW GradeView';
GO
CREATE VIEW [dbo].[GradeView]
AS
    SELECT  dbo.arGradeScales.GrdScaleId
           ,dbo.arGradeScales.InstructorId
           ,dbo.arGradeScales.Descrip
           ,dbo.arGradeScaleDetails.MinVal
           ,dbo.arGradeScaleDetails.MaxVal
           ,dbo.arGradeScaleDetails.GrdSysDetailId
           ,dbo.arGradeSystemDetails.Grade
    FROM    dbo.arGradeScaleDetails
    INNER JOIN dbo.arGradeScales ON dbo.arGradeScaleDetails.GrdScaleId = dbo.arGradeScales.GrdScaleId
    INNER JOIN dbo.arGradeSystemDetails ON dbo.arGradeScaleDetails.GrdSysDetailId = dbo.arGradeSystemDetails.GrdSysDetailId;



GO



IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'If exist VIEW GradeWeightsView, delete it';
GO
--=================================================================================================
-- CREATE VIEW GradeWeightsView
--=================================================================================================
IF EXISTS (
              SELECT 1
              FROM   sys.views AS V
              WHERE  object_id = OBJECT_ID(N'[dbo].[GradeWeightsView]')
                     AND type IN ( N'V' )
          )
    BEGIN
        PRINT '    Dropping view GradeWeightsView';
        DROP VIEW GradeWeightsView;
    END;

GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating VIEW GradeWeightsView';
GO
CREATE VIEW [dbo].[GradeWeightsView]
AS
    SELECT  dbo.arGrdBkWeights.InstrGrdBkWgtId
           ,dbo.arGrdBkWgtDetails.Code
           ,dbo.arGrdBkWgtDetails.Descrip
           ,dbo.arGrdBkWgtDetails.Weight
           ,dbo.arGrdBkWgtDetails.Seq
           ,dbo.arGrdBkWeights.InstructorId
           ,dbo.arGrdBkWeights.Descrip AS Expr1
    FROM    dbo.arGrdBkWeights
    INNER JOIN dbo.arGrdBkWgtDetails ON dbo.arGrdBkWeights.InstrGrdBkWgtId = dbo.arGrdBkWgtDetails.InstrGrdBkWgtId;



GO



IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'If exist VIEW postfinalgrades, delete it';
GO
--=================================================================================================
-- CREATE VIEW postfinalgrades
--=================================================================================================
IF EXISTS (
              SELECT 1
              FROM   sys.views AS V
              WHERE  object_id = OBJECT_ID(N'[dbo].[postfinalgrades]')
                     AND type IN ( N'V' )
          )
    BEGIN
        PRINT '    Dropping view postfinalgrades';
        DROP VIEW postfinalgrades;
    END;

GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating VIEW postfinalgrades';
GO
CREATE VIEW [dbo].[postfinalgrades]
AS
    SELECT  e.Grade
           ,e.GrdSysDetailId
    FROM    dbo.arClassSections a
    INNER JOIN dbo.arGradeScales b ON a.GrdScaleId = b.GrdScaleId
    INNER JOIN dbo.arGradeScaleDetails c ON b.GrdScaleId = c.GrdScaleId
    INNER JOIN dbo.arGradeSystems d ON b.GrdSystemId = d.GrdSystemId
    INNER JOIN dbo.arGradeSystemDetails e ON c.GrdSysDetailId = e.GrdSysDetailId
    WHERE   ( a.ClsSectionId = '1E872B0C-A440-448D-A867-64CBA2326A27' );



GO



IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'If exist VIEW SettingStdScoreLimit, delete it';
GO
--=================================================================================================
-- CREATE VIEW SettingStdScoreLimit
--=================================================================================================
IF EXISTS (
              SELECT 1
              FROM   sys.views AS V
              WHERE  object_id = OBJECT_ID(N'[dbo].[SettingStdScoreLimit]')
                     AND type IN ( N'V' )
          )
    BEGIN
        PRINT '    Dropping view SettingStdScoreLimit';
        DROP VIEW SettingStdScoreLimit;
    END;

GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating VIEW SettingStdScoreLimit';
GO
CREATE VIEW [dbo].[SettingStdScoreLimit]
AS
    SELECT  dbo.arGradeScaleDetails.GrdScaleId
           ,MIN(dbo.arGradeScaleDetails.MinVal) AS Expr1
           ,MAX(dbo.arGradeScaleDetails.MaxVal) AS Expr2
    FROM    dbo.arPrgVersions
    INNER JOIN dbo.arStuEnrollments ON dbo.arPrgVersions.PrgVerId = dbo.arStuEnrollments.PrgVerId
    INNER JOIN dbo.arGradeScaleDetails
    INNER JOIN dbo.arGradeScales ON dbo.arGradeScaleDetails.GrdScaleId = dbo.arGradeScales.GrdScaleId ON dbo.arPrgVersions.ThGrdScaleId = dbo.arGradeScales.GrdScaleId
    WHERE   ( dbo.arStuEnrollments.StuEnrollId = '85DDDF78-F3CA-4783-8E42-35DD949FF36E' )
    GROUP BY dbo.arGradeScaleDetails.GrdScaleId;



GO



IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'If exist VIEW UnscheduledDates, delete it';
GO
--=================================================================================================
-- CREATE VIEW UnscheduledDates
--=================================================================================================
IF EXISTS (
              SELECT 1
              FROM   sys.views AS V
              WHERE  object_id = OBJECT_ID(N'[dbo].[UnscheduledDates]')
                     AND type IN ( N'V' )
          )
    BEGIN
        PRINT '    Dropping view UnscheduledDates';
        DROP VIEW UnscheduledDates;
    END;

GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating VIEW UnscheduledDates';
GO
CREATE VIEW [dbo].[UnscheduledDates]
AS
    SELECT  dbo.arUnschedClosures.StartDate
           ,dbo.arUnschedClosures.EndDate
           ,dbo.arUnschedClosures.UnschedClosureId
    FROM    dbo.arUnschedClosures
    INNER JOIN dbo.arClassSections ON dbo.arUnschedClosures.ClsSectionId = dbo.arClassSections.ClsSectionId
    WHERE   ( dbo.arClassSections.TermId = '1693EA60-1114-4F1C-BE15-BA9AF5DE9DEF' );



GO



IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'If exist VIEW VIEW2, delete it';
GO
--=================================================================================================
-- CREATE VIEW VIEW2
--=================================================================================================
IF EXISTS (
              SELECT 1
              FROM   sys.views AS V
              WHERE  object_id = OBJECT_ID(N'[dbo].[VIEW2]')
                     AND type IN ( N'V' )
          )
    BEGIN
        PRINT '    Dropping view VIEW2';
        DROP VIEW VIEW2;
    END;

GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating VIEW VIEW2';
GO
CREATE VIEW [dbo].[VIEW2]
AS
    SELECT  dbo.syResTblFlds.ResDefId
           ,dbo.syTblFlds.TblFldsId
           ,dbo.syResTblFlds.ResourceId
           ,dbo.syFields.FldName
           ,dbo.syFields.FldId
           ,dbo.syDDLS.DDLName
           ,dbo.syTables.TblName
           ,dbo.syDDLS.ValFldId
           ,dbo.syDDLS.DispFldId
    FROM    dbo.syTables
    INNER JOIN dbo.syDDLS ON dbo.syTables.TblId = dbo.syDDLS.TblId
    INNER JOIN dbo.syResTblFlds
    INNER JOIN dbo.syTblFlds ON dbo.syResTblFlds.TblFldsId = dbo.syTblFlds.TblFldsId
    INNER JOIN dbo.syFields ON dbo.syTblFlds.FldId = dbo.syFields.FldId ON dbo.syDDLS.DDLId = dbo.syFields.DDLId
    WHERE   ( dbo.syResTblFlds.ResourceId = 88 );



GO



IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'If exist VIEW VIEW_SySDFModuleValue_Lead, delete it';
GO
--=================================================================================================
-- CREATE VIEW VIEW_SySDFModuleValue_Lead
--=================================================================================================
IF EXISTS (
              SELECT 1
              FROM   sys.views AS V
              WHERE  object_id = OBJECT_ID(N'[dbo].[VIEW_SySDFModuleValue_Lead]')
                     AND type IN ( N'V' )
          )
    BEGIN
        PRINT '    Dropping view VIEW_SySDFModuleValue_Lead';
        DROP VIEW VIEW_SySDFModuleValue_Lead;
    END;

GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating VIEW VIEW_SySDFModuleValue_Lead';
GO
CREATE VIEW [dbo].[VIEW_SySDFModuleValue_Lead]
AS
    SELECT  mod.SDFPKID
           ,mod.PgPKID AS LeadId
           ,mod.SDFID
           ,mod.SDFValue
           ,mod.ModUser
           ,mod.ModDate
    FROM    dbo.sySDFModuleValue mod
    JOIN    adLeads lead ON lead.LeadId = mod.PgPKID; 
 
 
 
--SELECT  mod.SDFPKID, mod.PgPKID AS LeadId, mod.SDFID, mod.SDFValue, mod.ModUser, mod.ModDate 
--FROM    dbo.sySDFModuleValue mod 
--JOIN  adLeads lead ON lead.LeadId = mod.PgPKID 
 

GO


IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'If exist SP usp_AdvPortalCampuses, delete it';
GO
--=================================================================================================
-- usp_AdvPortalCampuses
--=================================================================================================
IF EXISTS (
              SELECT 1
              FROM   sys.objects
              WHERE  object_id = OBJECT_ID(N'[dbo].[usp_AdvPortalCampuses]')
                     AND type IN ( N'P', N'PC' )
          )
    BEGIN
		PRINT '    Dropping sp usp_AdvPortalCampuses';	
        DROP PROCEDURE usp_AdvPortalCampuses;
    END;
GO
PRINT N'Creating sp usp_AdvPortalCampuses';
GO
CREATE PROCEDURE [dbo].[usp_AdvPortalCampuses]
AS
    BEGIN

        DECLARE @PortalCampuses TABLE
            (
             SerialNum INT
            ,CampusId VARCHAR(255)
            );

        INSERT  INTO @PortalCampuses
                SELECT	DISTINCT
                        ROW_NUMBER() OVER ( ORDER BY CampusId )
                       ,AppSettingValue.CampusId
                FROM    syConfigAppSettings Appsetting
                INNER JOIN dbo.syConfigAppSetValues AppSettingValue ON Appsetting.SettingId = AppSettingValue.SettingId
                WHERE   KeyName = 'PortalCustomer'
                        AND AppSettingValue.Value = 'Yes';

        DECLARE @PortalCampusDescrip TABLE
            (
             CampusId VARCHAR(255)
            ,CampusDescrip VARCHAR(255)
            );

        DECLARE @i INT;
        SET @i = 1;

        WHILE ( @i <= (
                        SELECT  MAX(SerialNum)
                        FROM    @PortalCampuses
                      ) )
            BEGIN

                IF (
                     SELECT CampusId
                     FROM   @PortalCampuses
                     WHERE  SerialNum = @i
                   ) IS NULL
                    BEGIN

                        INSERT  INTO @PortalCampusDescrip
                                SELECT DISTINCT
                                        CampusId
                                       ,CampDescrip
                                FROM    dbo.syCampuses Campus
                                INNER JOIN dbo.syStatuses Statuses ON Campus.StatusId = Statuses.StatusId
                                WHERE   Statuses.Status = 'Active'
                                        AND Campus.LeadStatusId IS NOT NULL
                                        AND Campus.AdmRepId IS NOT NULL;
                        SET @i = @i + 1;
                    END; 
                ELSE
                    IF (
                         SELECT CampusId
                         FROM   @PortalCampuses
                         WHERE  SerialNum = @i
                       ) IS NOT NULL
                        BEGIN

                            INSERT  INTO @PortalCampusDescrip
                                    SELECT DISTINCT
                                            PC.CampusId
                                           ,Camp.CampDescrip
                                    FROM    @PortalCampuses PC
                                    INNER JOIN dbo.syCampuses Camp ON PC.CampusId = Camp.CampusId
                                    INNER JOIN dbo.syStatuses Statuses ON Camp.StatusId = Statuses.StatusId
                                    WHERE   Statuses.Status = 'Active'
                                            AND Camp.LeadStatusId IS NOT NULL
                                            AND Camp.AdmRepId IS NOT NULL;
                            SET @i = @i + 1;
                        END; 
            END;

        SELECT DISTINCT
                *
        FROM    @PortalCampusDescrip
        ORDER BY CampusDescrip;

    END;




GO



IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'If exist SP usp_AdvPortalInsertLead, delete it';
GO
--=================================================================================================
-- usp_AdvPortalInsertLead
--=================================================================================================
IF EXISTS (
              SELECT 1
              FROM   sys.objects
              WHERE  object_id = OBJECT_ID(N'[dbo].[usp_AdvPortalInsertLead]')
                     AND type IN ( N'P', N'PC' )
          )
    BEGIN
		PRINT '    Dropping sp usp_AdvPortalInsertLead';	
        DROP PROCEDURE usp_AdvPortalInsertLead;
    END;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating sp usp_AdvPortalInsertLead';
GO
CREATE PROCEDURE [dbo].[usp_AdvPortalInsertLead]
    @CampusId UNIQUEIDENTIFIER
   ,@ProgId UNIQUEIDENTIFIER
   ,@SourceTypeId VARCHAR(50) = NULL
   ,@PrevEduLvlId VARCHAR(50) = NULL
   ,@FirstName VARCHAR(50)
   ,@MiddleName VARCHAR(50) = NULL
   ,@LastName VARCHAR(50)
   ,@BirthDate DATETIME = NULL
   ,@GenderId VARCHAR(50) = NULL
   ,@PhoneNumber VARCHAR(50) = NULL
   ,@ForeignPhone BIT = 0
   ,@PhoneNumber2 VARCHAR(50) = NULL
   ,@ForeignPhone2 BIT = 0
   ,@Email VARCHAR(50)
   ,@AlternativeEmail VARCHAR(50) = NULL
   ,@Address1 VARCHAR(50) = NULL
   ,@Address2 VARCHAR(50) = NULL
   ,@City VARCHAR(50) = NULL
   ,@StateId VARCHAR(50) = NULL
   ,@OtherState VARCHAR(50) = NULL
   ,@Zip VARCHAR(50) = NULL
   ,@ForeignZip BIT = 0
   ,@CountyId VARCHAR(50) = NULL
   ,@CountryId VARCHAR(50) = NULL
   ,@RaceId VARCHAR(50) = NULL
   ,@NationalityId VARCHAR(50) = NULL
   ,@Citizen VARCHAR(50) = NULL
   ,@MaritalStatus VARCHAR(50) = NULL
   ,@FamilyIncome VARCHAR(50) = NULL
   ,@Children VARCHAR(50) = NULL
   ,@DrivLicStateID VARCHAR(50) = NULL
   ,@DrivLicNumber VARCHAR(50) = NULL
   ,@Comments VARCHAR(250) = NULL
   ,@LeadEntryMsg VARCHAR(150) OUTPUT
AS
    BEGIN
        SET NOCOUNT ON;

		--/* Check if lead is duplicate */

        SET @LeadEntryMsg = '';

        IF EXISTS ( SELECT  *
                    FROM    dbo.adLeads
                    WHERE   FirstName = @FirstName
                            AND LastName = @LastName )
            BEGIN
                SET @LeadEntryMsg = 'Your first and last name already exists in our system. Please contact school associate for further information.';
            END;
        ELSE
            IF EXISTS ( SELECT  *
                        FROM    dbo.adLeads
                        WHERE   FirstName = @FirstName
                                AND (
                                      HomeEmail = @Email
                                      OR WorkEmail = @Email
                                    ) )
                BEGIN
                    SET @LeadEntryMsg = 'Your first name and email address already exists in our system. Please contact school associate for futher information.';
                END;
            ELSE 
			-- ==========================================================
			-- Use transaction to do this. Two tables must be modified
			-- ==========================================================
                BEGIN TRY
                    BEGIN TRANSACTION T1;

					-- INSERT new lead into Leads table 
                    BEGIN 
                        INSERT  INTO dbo.adLeads
                                (
                                 FirstName
                                ,MiddleName
                                ,LastName
                                ,BirthDate
                                ,Gender
                                ,ModUser
                                ,ModDate
                                ,Phone
                                ,ForeignPhone
                                ,Phone2
                                ,ForeignPhone2
                                ,HomeEmail
                                ,WorkEmail
                                ,Address1
                                ,Address2
                                ,City
                                ,StateId
                                ,OtherState
                                ,Zip
                                ,ForeignZip
                                ,County
                                ,LeadStatus
                                ,AdmissionsRep
                                ,AssignedDate
                                ,SourceCategoryID
                                ,SourceTypeID
                                ,SourceDate
                                ,ProgramID
                                ,AreaID
                                ,Comments
                                ,CampusId
                                ,Country
                                ,PreviousEducation
                                ,Race
                                ,Nationality
                                ,Citizen
                                ,MaritalStatus
                                ,FamilyIncome
                                ,Children
                                ,DrivLicStateID
                                ,DrivLicNumber
							    )
                        VALUES  (
                                 @FirstName
                                ,@MiddleName
                                ,@LastName
                                ,@BirthDate
                                ,@GenderId
                                ,'sa'
                                ,GETDATE()
                                ,CASE WHEN ( @PhoneNumber <> '' ) THEN @PhoneNumber
                                      ELSE NULL
                                 END
                                ,@ForeignPhone
                                ,CASE WHEN ( @PhoneNumber2 <> '' ) THEN @PhoneNumber2
                                      ELSE NULL
                                 END
                                ,@ForeignPhone2
                                ,@Email
                                ,CASE WHEN ( @AlternativeEmail <> '' ) THEN @AlternativeEmail
                                      ELSE NULL
                                 END
                                ,CASE WHEN ( @Address1 <> '' ) THEN @Address1
                                      ELSE NULL
                                 END
                                ,CASE WHEN ( @Address2 <> '' ) THEN @Address2
                                      ELSE NULL
                                 END
                                ,CASE WHEN ( @City <> '' ) THEN @City
                                      ELSE NULL
                                 END
                                ,CASE WHEN ( @StateId = '' ) THEN NULL
                                      ELSE @StateId
                                 END
                                ,@OtherState
                                ,CASE WHEN ( @Zip <> '' ) THEN @Zip
                                      ELSE NULL
                                 END
                                ,@ForeignZip
                                ,CASE WHEN ( @CountyId <> '' ) THEN @CountyId
                                      ELSE NULL
                                 END
                                ,(
                                   SELECT   UPPER(LeadStatusId)
                                   FROM     dbo.syCampuses
                                   WHERE    CampusId = @CampusId
                                 )
                                ,(
                                   SELECT   UPPER(AdmRepId)
                                   FROM     dbo.syCampuses
                                   WHERE    CampusId = @CampusId
                                 )
                                ,GETDATE()
                                ,CASE WHEN ( @SourceTypeId = '' ) THEN NULL
                                      ELSE (
                                             SELECT SourceCatagoryId
                                             FROM   dbo.adSourceType
                                             WHERE  SourceTypeId = @SourceTypeId
                                           )
                                 END
                                ,CASE WHEN ( @SourceTypeId = '' ) THEN NULL
                                      ELSE @SourceTypeId
                                 END
                                ,GETDATE()
                                ,UPPER(@ProgId)
                                ,(
                                   SELECT   PrgGrpId
                                   FROM     dbo.arPrgGrp
                                   WHERE    PrgGrpId IN ( SELECT TOP 1
                                                                    PrgGrpId
                                                          FROM      dbo.arPrgVersions
                                                          WHERE     ProgId = @ProgId )
                                 )
                                ,@Comments
                                ,UPPER(@CampusId)
                                ,CASE WHEN ( @CountryId = '' ) THEN NULL
                                      ELSE UPPER(@CountryId)
                                 END
                                ,CASE WHEN ( @PrevEduLvlId = '' ) THEN NULL
                                      ELSE UPPER(@PrevEduLvlId)
                                 END
                                ,CASE WHEN ( @RaceId <> '' ) THEN @RaceId
                                      ELSE NULL
                                 END
                                ,CASE WHEN ( @NationalityId <> '' ) THEN @NationalityId
                                      ELSE NULL
                                 END
                                ,CASE WHEN ( @Citizen <> '' ) THEN @Citizen
                                      ELSE NULL
                                 END
                                ,CASE WHEN ( @MaritalStatus <> '' ) THEN @MaritalStatus
                                      ELSE NULL
                                 END
                                ,CASE WHEN ( @FamilyIncome <> '' ) THEN @FamilyIncome
                                      ELSE NULL
                                 END
                                ,CASE WHEN ( @Children <> '' ) THEN @Children
                                      ELSE NULL
                                 END
                                ,CASE WHEN ( @DrivLicStateID <> '' ) THEN @DrivLicStateID
                                      ELSE NULL
                                 END
                                ,CASE WHEN ( @DrivLicNumber <> '' ) THEN @DrivLicNumber
                                      ELSE NULL
                                 END
                                );

					-- INSERT new prospect status into LeadStatusesChanges table 

                        INSERT  INTO syLeadStatusesChanges
                                (
                                 StatusChangeId
                                ,LeadId
                                ,OrigStatusId
                                ,NewStatusId
                                ,ModUser
                                ,ModDate
							    )
                        VALUES  (
                                 NEWID()
                                ,(
                                   SELECT   LeadId
                                   FROM     adLeads
                                   WHERE    FirstName = @FirstName
                                            AND LastName = @LastName
                                            AND HomeEmail = @Email
                                            AND ProgramID = UPPER(@ProgId)
                                 )
                                ,NULL
                                ,(
                                   SELECT   UPPER(LeadStatusId)
                                   FROM     dbo.syCampuses
                                   WHERE    CampusId = @CampusId
                                 )
                                ,'sa'
                                ,GETDATE()
                                );
                        SET @LeadEntryMsg = 'Your request for ''' + (
                                                                      SELECT    ProgDescrip
                                                                      FROM      arPrograms
                                                                      WHERE     ProgId = @ProgId
                                                                    ) + ''' program is received. You will receive a confirmation email shortly.';
                    END;

                    COMMIT TRANSACTION T1;
                END TRY
                BEGIN CATCH
                    IF @@TRANCOUNT > 0
                        ROLLBACK TRANSACTION T1;
                    SET @LeadEntryMsg = ERROR_MESSAGE();

                END CATCH;

        SELECT  @LeadEntryMsg AS LeadEntryMsg; 
    END;



GO


IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'If exist SP USP_getOnTimeGraduationDetails, delete it';
GO
--=================================================================================================
-- USP_getOnTimeGraduationDetails
--=================================================================================================
IF EXISTS (
              SELECT 1
              FROM   sys.objects
              WHERE  object_id = OBJECT_ID(N'[dbo].[USP_getOnTimeGraduationDetails]')
                     AND type IN ( N'P', N'PC' )
          )
    BEGIN
		PRINT '    Dropping sp USP_getOnTimeGraduationDetails';	
        DROP PROCEDURE USP_getOnTimeGraduationDetails;
    END;

GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating sp USP_getOnTimeGraduationDetails';
GO
--=================================================================================================
-- USP_getOnTimeGraduationDetails
--=================================================================================================
CREATE PROCEDURE [dbo].[USP_getOnTimeGraduationDetails]
    (
     @StartDate AS DATETIME
    ,@EndDate AS DATETIME
    ,@RevGradDateType AS VARCHAR(50)
    ,@CampusId AS VARCHAR(50) = NULL
    ,@ProgramIdList AS VARCHAR(MAX) = NULL
    )
AS
    BEGIN
        IF ( @RevGradDateType = 'fromprogram' )
            BEGIN
                EXEC dbo.USP_getOnTimeGraduationDetails_ProgramWeeks @StartDate,@EndDate,@CampusId,@ProgramIdList;
            END;
        ELSE
            BEGIN
                EXEC dbo.USP_getOnTimeGraduationDetails_RevisedGradDate @StartDate,@EndDate,@CampusId,@ProgramIdList;
            END;
    
    END;
--=================================================================================================
-- END  --  USP_getOnTimeGraduationDetails
--=================================================================================================    


GO


IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'If exist SP USP_SA_LeadPayments_PrintReceipt, delete it';
GO
--=================================================================================================
-- USP_SA_LeadPayments_PrintReceipt
--=================================================================================================
IF EXISTS (
              SELECT 1
              FROM   sys.objects
              WHERE  object_id = OBJECT_ID(N'[dbo].[USP_SA_LeadPayments_PrintReceipt]')
                     AND type IN ( N'P', N'PC' )
          )
    BEGIN
		PRINT '    Dropping sp USP_SA_LeadPayments_PrintReceipt';	
        DROP PROCEDURE USP_SA_LeadPayments_PrintReceipt;
    END;

GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating sp USP_SA_LeadPayments_PrintReceipt';
GO
CREATE PROCEDURE [dbo].[USP_SA_LeadPayments_PrintReceipt]
    @CampusId UNIQUEIDENTIFIER
   ,@TransactionId UNIQUEIDENTIFIER
AS
    SET NOCOUNT ON;
    BEGIN
	
        DECLARE @LeadReceiptData TABLE
            (
             SchoolName VARCHAR(100) NOT NULL
            ,SchoolAddress1 VARCHAR(200) NOT NULL
            ,SchoolAddress2 VARCHAR(200) NULL
            ,SchoolCity VARCHAR(50) NOT NULL
            ,SchoolState VARCHAR(50) NOT NULL
            ,SchoolZip VARCHAR(20) NOT NULL
            ,SchoolCountry VARCHAR(50) NULL
            ,LeadName VARCHAR(100) NOT NULL
            ,LeadAddress1 VARCHAR(200) NULL
            ,LeadAddress2 VARCHAR(200) NULL
            ,LeadCity VARCHAR(50) NULL
            ,LeadState VARCHAR(50) NULL
            ,LeadZip VARCHAR(20) NULL
            ,LeadCountry VARCHAR(50) NULL
            ,TransDate VARCHAR(10) NOT NULL
            ,TransDesc VARCHAR(50) NULL
            ,PaymentTypeDesc VARCHAR(50) NULL
            ,CheckNumber VARCHAR(50) NULL
            ,TransAmount DECIMAL(19,4) NOT NULL
            ,Voided BIT NULL
            );
		
        DECLARE @SchoolData TABLE
            (
             SchoolName VARCHAR(100) NOT NULL
            ,SchoolAddress1 VARCHAR(200) NOT NULL
            ,SchoolAddress2 VARCHAR(200) NULL
            ,SchoolCity VARCHAR(50) NOT NULL
            ,SchoolState VARCHAR(50) NULL
            ,SchoolZip VARCHAR(20) NOT NULL
            ,SchoolCountry VARCHAR(50) NULL
            );
		
        DECLARE @LeadData TABLE
            (
             LeadName VARCHAR(100) NOT NULL
            ,LeadAddress1 VARCHAR(200) NULL
            ,LeadAddress2 VARCHAR(200) NULL
            ,LeadCity VARCHAR(50) NULL
            ,LeadState VARCHAR(50) NULL
            ,LeadZip VARCHAR(20) NULL
            ,LeadCountry VARCHAR(50) NULL
            );
		
				
		--Get School Address
        DECLARE @AddressToBePrintedInReceipts VARCHAR(50);
        SET @AddressToBePrintedInReceipts = (
                                              SELECT    Value
                                              FROM      syConfigAppSetValues v
                                              INNER JOIN syConfigAppSettings s ON s.SettingId = v.SettingId
                                              WHERE     KeyName = 'AddressToBePrintedInReceipts'
                                            );
		
        BEGIN
            IF @AddressToBePrintedInReceipts = 'CorporateAddress'
                BEGIN
                    INSERT  INTO @SchoolData
                            (
                             SchoolName
                            ,SchoolAddress1
                            ,SchoolAddress2
                            ,SchoolCity
                            ,SchoolState
                            ,SchoolZip
                            ,SchoolCountry 				
				            )
                            SELECT  (
                                      SELECT    Value
                                      FROM      dbo.syConfigAppSetValues v
                                      INNER JOIN dbo.syConfigAppSettings s ON s.SettingId = v.SettingId
                                      WHERE     KeyName LIKE 'CorporateName'
                                    ) AS SchoolName
                                   ,(
                                      SELECT    Value
                                      FROM      dbo.syConfigAppSetValues v
                                      INNER JOIN dbo.syConfigAppSettings s ON s.SettingId = v.SettingId
                                      WHERE     KeyName LIKE 'CorporateAddress1'
                                    ) AS SchoolAddress1
                                   ,(
                                      SELECT    Value
                                      FROM      dbo.syConfigAppSetValues v
                                      INNER JOIN dbo.syConfigAppSettings s ON s.SettingId = v.SettingId
                                      WHERE     KeyName LIKE 'CorporateAddress2'
                                    ) AS SchoolAddress2
                                   ,(
                                      SELECT    Value
                                      FROM      dbo.syConfigAppSetValues v
                                      INNER JOIN dbo.syConfigAppSettings s ON s.SettingId = v.SettingId
                                      WHERE     KeyName LIKE 'CorporateCity'
                                    ) AS SchoolCity
                                   ,(
                                      SELECT    Value
                                      FROM      dbo.syConfigAppSetValues v
                                      INNER JOIN dbo.syConfigAppSettings s ON s.SettingId = v.SettingId
                                      WHERE     KeyName LIKE 'CorporateState'
                                    ) AS SchoolState
                                   ,(
                                      SELECT    Value
                                      FROM      dbo.syConfigAppSetValues v
                                      INNER JOIN dbo.syConfigAppSettings s ON s.SettingId = v.SettingId
                                      WHERE     KeyName LIKE 'CorporateZip'
                                    ) AS SchoolZip
                                   ,(
                                      SELECT    Value
                                      FROM      dbo.syConfigAppSetValues v
                                      INNER JOIN dbo.syConfigAppSettings s ON s.SettingId = v.SettingId
                                      WHERE     KeyName LIKE 'CorporateCountry'
                                    ) AS SchoolCountry;
                END;
				
            IF @AddressToBePrintedInReceipts = 'CampusAddress'
                BEGIN			
                    INSERT  INTO @SchoolData
                            (
                             SchoolName
                            ,SchoolAddress1
                            ,SchoolAddress2
                            ,SchoolCity
                            ,SchoolState
                            ,SchoolZip
                            ,SchoolCountry 	
				            )
                            SELECT  (
                                      SELECT    CampDescrip
                                      FROM      dbo.syCampuses
                                      WHERE     CampusId = @CampusId
                                    ) AS SchoolName
                                   ,(
                                      SELECT    Address1
                                      FROM      dbo.syCampuses
                                      WHERE     CampusId = @CampusId
                                    ) AS SchoolAddress1
                                   ,(
                                      SELECT    Address2
                                      FROM      dbo.syCampuses
                                      WHERE     CampusId = @CampusId
                                    ) AS SchoolAddress2
                                   ,(
                                      SELECT    City
                                      FROM      dbo.syCampuses
                                      WHERE     CampusId = @CampusId
                                    ) AS SchoolCity
                                   ,(
                                      SELECT    StateDescrip
                                      FROM      syStates s
                                      INNER JOIN syCampuses c ON s.StateId = c.StateId
                                      WHERE     CampusId = @CampusId
                                    ) AS SchoolState
                                   ,(
                                      SELECT    Zip
                                      FROM      dbo.syCampuses
                                      WHERE     CampusId = @CampusId
                                    ) AS SchoolZip
                                   ,(
                                      SELECT    CountryDescrip
                                      FROM      dbo.adCountries s
                                      INNER JOIN syCampuses c ON s.CountryId = c.CountryId
                                      WHERE     CampusId = @CampusId
                                    ) AS SchoolCountry;
                END;	
		
        END;		
		
		--Get Lead Address
        BEGIN		
            INSERT  INTO @LeadData
                    (
                     LeadName
                    ,LeadAddress1
                    ,LeadAddress2
                    ,LeadCity
                    ,LeadState
                    ,LeadZip
                    ,LeadCountry 		
			        )
                    SELECT  ( LastName + ', ' + FirstName ) AS LeadName
                           ,Address1
                           ,Address2
                           ,City
                           ,(
                              SELECT    StateDescrip
                              FROM      syStates
                              WHERE     StateId = AL.StateId
                            ) AS LeadState
                           ,Zip
                           ,(
                              SELECT    CountryDescrip
                              FROM      adCountries
                              WHERE     CountryId = AL.Country
                            ) AS LeadCountry
                    FROM    adLeads AL
                    WHERE   LeadId = (
                                       SELECT   LeadId
                                       FROM     dbo.adLeadTransactions
                                       WHERE    TransactionId = @TransactionId
                                     );
            BEGIN
                INSERT  INTO @LeadReceiptData
                        (
                         SchoolName
                        ,SchoolAddress1
                        ,SchoolAddress2
                        ,SchoolCity
                        ,SchoolState
                        ,SchoolZip
                        ,SchoolCountry
                        ,LeadName
                        ,LeadAddress1
                        ,LeadAddress2
                        ,LeadCity
                        ,LeadState
                        ,LeadZip
                        ,LeadCountry
                        ,TransDate
                        ,TransDesc
                        ,PaymentTypeDesc
                        ,CheckNumber
                        ,TransAmount
                        ,Voided 			
			            )
                        SELECT  SchoolName AS SchoolName
                               ,SchoolAddress1 AS SchoolAddress1
                               ,SchoolAddress2 AS SchoolAddress2
                               ,SchoolCity AS SchoolCity
                               ,SchoolState AS SchoolState
                               ,SchoolZip AS SchoolZip
                               ,SchoolCountry AS SchoolCountry
                               ,LeadName AS LeadName
                               ,LeadAddress1 AS LeadAddress1
                               ,LeadAddress2 AS LeadAddress2
                               ,LeadCity AS LeadtCity
                               ,LeadState AS LeadState
                               ,LeadZip AS LeadZip
                               ,LeadCountry AS LeadCountry
                               ,CONVERT(VARCHAR(10),(
                                                      SELECT    CreatedDate
                                                      FROM      dbo.adLeadTransactions
                                                      WHERE     TransactionId = @TransactionId
                                                    ),101) AS TransDate
                               ,(
                                  SELECT    TransCodeDescrip
                                  FROM      dbo.saTransCodes
                                  WHERE     TransCodeId = (
                                                            SELECT  TransCodeId
                                                            FROM    dbo.adLeadTransactions
                                                            WHERE   TransactionId = @TransactionId
                                                          )
                                ) AS TransDesc
                               ,(
                                  SELECT    Description
                                  FROM      dbo.saPaymentTypes PT
                                  INNER JOIN dbo.adLeadPayments P ON PT.PaymentTypeId = P.PaymentTypeId
                                  WHERE     P.TransactionId = @TransactionId
                                ) AS PaymentTypeDesc
                               ,(
                                  SELECT    CheckNumber
                                  FROM      dbo.adLeadPayments
                                  WHERE     TransactionId = @TransactionId
                                ) AS CheckNumber
                               ,(
                                  SELECT    TransAmount
                                  FROM      dbo.adLeadTransactions
                                  WHERE     TransactionId = @TransactionId
                                ) AS TransAmount
                               ,(
                                  SELECT    Voided
                                  FROM      dbo.adLeadTransactions
                                  WHERE     TransactionId = @TransactionId
                                ) AS Voided
			-- T.TransAmount AS TransAmount
			-- FROM @SchoolData, @LeadData, dbo.saTransactions T, dbo.saPayments P
			-- WHERE T.TransactionId = @TransactionId AND T.Voided = 0
			--   AND P.TransactionId = @TransactionId
                        FROM    @SchoolData
                               ,@LeadData;
            END;
		
            SELECT  SchoolName
                   ,SchoolAddress1
                   ,SchoolAddress2
                   ,SchoolCity
                   ,SchoolState
                   ,SchoolZip
                   ,SchoolCountry
                   ,LeadName
                   ,LeadAddress1
                   ,LeadAddress2
                   ,LeadCity
                   ,LeadState
                   ,LeadZip
                   ,LeadCountry
                   ,TransDate
                   ,TransDesc
                   ,PaymentTypeDesc
                   ,CheckNumber
                   ,ABS(CONVERT(DECIMAL(18,2),ROUND(TransAmount,2))) AS TransAmount
                   ,Voided
				--ABS([TransAmount]) AS TransAmount 	
            FROM    @LeadReceiptData;
		
        END;
    END;



GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'If exist SP Usp_TR_Sub6_Courses, delete it';
GO
--=================================================================================================
-- Usp_TR_Sub6_Courses
--=================================================================================================
IF EXISTS (
              SELECT 1
              FROM   sys.objects
              WHERE  object_id = OBJECT_ID(N'[dbo].[Usp_TR_Sub6_Courses]')
                     AND type IN ( N'P', N'PC' )
          )
    BEGIN
		PRINT '    Dropping sp Usp_TR_Sub6_Courses';
        DROP PROCEDURE Usp_TR_Sub6_Courses;
    END;

GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating sp Usp_TR_Sub6_Courses';
GO
-- ========================================================================================================= 
-- Usp_TR_Sub6_Courses  
-- ========================================================================================================= 
CREATE PROCEDURE [dbo].[Usp_TR_Sub6_Courses]
    @StuEnrollIdList VARCHAR(MAX)
   ,@TermId VARCHAR(50) = NULL
AS --''''''''''''''''''''''''''''' This section of the stored proc calculates the GPA ''''''''''''''''''''''''''''''' 
--''''''''''''''''''''''''''''' Term GPA and Program version GPA ''''''''''''''''''''''''''''''''''''''''''''''''' 
    DECLARE @CumulativeGPA_Range DECIMAL(18,2)
       ,@GradeCourseRepetitionsMethod VARCHAR(50); 
    DECLARE @cumSimpleCourseCredits DECIMAL(18,2)
       ,@cumSimple_GPA_Credits DECIMAL(18,2)
       ,@cumSimpleGPA DECIMAL(18,2)
       ,@cumSimpleCourseCredits_OverAll DECIMAL(18,2)
       ,@cumSimple_GPA_Credits_OverAll DECIMAL(18,2)
       ,@cumSimpleGPA_OverAll DECIMAL(18,2); 
    DECLARE @IsMakingSAP BIT; 
    DECLARE @times INT
       ,@counter INT
       ,@StudentId VARCHAR(50)
       ,@StuEnrollId VARCHAR(50); 
    DECLARE @cumCourseCredits DECIMAL(18,2)
       ,@cumWeighted_GPA_Credits DECIMAL(18,2)
       ,@cumWeightedGPA DECIMAL(18,2); 
    DECLARE @EquivCourse_SA_CC INT
       ,@EquivCourse_SA_GPA DECIMAL(18,2)
       ,@EquivCourse_SA_CC_OverAll INT
       ,@EquivCourse_SA_GPA_OverAll DECIMAL(18,2); 
 
	--DROP TABLE #CoursesNotRepeated 
    CREATE TABLE #CoursesNotRepeated
        (
         StuEnrollId UNIQUEIDENTIFIER
        ,TermId UNIQUEIDENTIFIER
        ,TermDescrip VARCHAR(100)
        ,ReqId UNIQUEIDENTIFIER
        ,ReqDescrip VARCHAR(100)
        ,ClsSectionId VARCHAR(50)
        ,CreditsEarned DECIMAL(18,2)
        ,CreditsAttempted DECIMAL(18,2)
        ,CurrentScore DECIMAL(18,2)
        ,CurrentGrade VARCHAR(10)
        ,FinalScore DECIMAL(18,2)
        ,FinalGrade VARCHAR(10)
        ,Completed BIT
        ,FinalGPA DECIMAL(18,2)
        ,Product_WeightedAverage_Credits_GPA DECIMAL(18,2)
        ,Count_WeightedAverage_Credits DECIMAL(18,2)
        ,Product_SimpleAverage_Credits_GPA DECIMAL(18,2)
        ,Count_SimpleAverage_Credits DECIMAL(18,2)
        ,ModUser VARCHAR(50)
        ,ModDate DATETIME
        ,TermGPA_Simple DECIMAL(18,2)
        ,TermGPA_Weighted DECIMAL(18,2)
        ,coursecredits DECIMAL(18,2)
        ,CumulativeGPA DECIMAL(18,2)
        ,CumulativeGPA_Simple DECIMAL(18,2)
        ,FACreditsEarned DECIMAL(18,2)
        ,Average DECIMAL(18,2)
        ,CumAverage DECIMAL(18,2)
        ,TermStartDate DATETIME
        ,rownumber INT NULL
        ,StudentId UNIQUEIDENTIFIER
        ); 
 
    SET @GradeCourseRepetitionsMethod = (
                                          SELECT    Value
                                          FROM      dbo.syConfigAppSetValues t1
                                          INNER JOIN dbo.syConfigAppSettings t2 ON t1.SettingId = t2.SettingId
                                                                                   AND t2.KeyName = 'GradeCourseRepetitionsMethod'
                                        ); 
 
    SET @cumSimpleGPA = 0; 
 
 
		  --SELECT  * 
    --                   ,0 AS row-number 
    --                   ,( SELECT TOP 1 
    --                                StudentId 
    --                      FROM      arStuEnrollments 
    --                      WHERE     StuEnrollId = dbo.syCreditSummary.StuEnrollId 
    --                    ) AS StudentId 
    --            FROM    syCreditSummary 
    --            WHERE   StuEnrollId IN (Select Val from [MultipleValuesForReportParameters](@StuEnrollIdList,',',1)) 
				--		AND (FinalScore IS NOT NULL OR FinalGrade IS NOT NULL) 
    --                    AND ReqId IN ( SELECT   ReqId 
    --                                   FROM     ( SELECT    ReqId 
--                                                       ,ReqDescrip 
    --                                                       --,TermId 
    --                                                       ,StuEnrollId 
    --                                                       ,COUNT(*) AS counter 
    --                                              FROM      syCreditSummary 
    --                                              WHERE     StuEnrollId IN (Select Val from [MultipleValuesForReportParameters](@StuEnrollIdList,',',1)) 
    --                                              GROUP BY  ReqId 
    --                                                       ,ReqDescrip 
    --                                                       --,TermId 
    --                                                       ,StuEnrollId 
    --                                              HAVING    COUNT(*) = 1 
    --                                            ) dt ); 
 
 
		 
 
    CREATE TABLE #getStudentGPAbyTerms
        (
         StuEnrollId UNIQUEIDENTIFIER
        ,TermId UNIQUEIDENTIFIER
        ,TermDescrip VARCHAR(50)
        ,TermCredits DECIMAL(18,2) NULL
        ,TermSimpleGPA DECIMAL(18,2) NULL
        ,TermWeightedGPA DECIMAL(18,2) NULL
        ,TermStartDate DATETIME
        ); 
 
    INSERT  INTO #getStudentGPAbyTerms
            SELECT DISTINCT
                    StuEnrollId
                   ,T.TermId
                   ,T.TermDescrip
                   ,NULL
                   ,NULL
                   ,NULL
                   ,T.StartDate
            FROM    arResults R
            INNER JOIN arClassSections CS ON R.TestId = CS.ClsSectionId
            INNER JOIN arTerm T ON CS.TermId = T.TermId
            WHERE   R.StuEnrollId IN ( SELECT   Val
                                       FROM     MultipleValuesForReportParameters(@StuEnrollIdList,',',1) )
            UNION
            SELECT DISTINCT
                    StuEnrollId
                   ,T.TermId
                   ,T.TermDescrip
                   ,NULL
                   ,NULL
                   ,NULL
                   ,T.StartDate
            FROM    arTransferGrades TG
            INNER JOIN arTerm T ON TG.TermId = T.TermId
            WHERE   TG.StuEnrollId IN ( SELECT  Val
                                        FROM    MultipleValuesForReportParameters(@StuEnrollIdList,',',1) ); 
 
		--SELECT * FROM #getStudentGPAbyTerms WHERE StuEnrollID='95094539-B06E-4323-9F1C-AC1EF0B22F2F' 
 
    CREATE TABLE #getStudentGPAByProgramVersion
        (
         LastName VARCHAR(50)
        ,FirstName VARCHAR(50)
        ,StudentId UNIQUEIDENTIFIER
        ,StudentNumber VARCHAR(50)
        ,StuEnrollID UNIQUEIDENTIFIER
        ,PrgVerId UNIQUEIDENTIFIER
        ,ProgramVersion VARCHAR(50)
        ,AcademicType NVARCHAR(50)
        ,ProgramVersion_SimpleGPA DECIMAL(18,2)
        ,ProgramVersion_WeightedGPA DECIMAL(18,2)
        ,OverAll_SimpleGPA DECIMAL(18,2)
        ,OverAll_WeightedGPA DECIMAL(18,2)
        ,IsMakingSAP BIT
        ,SSN VARCHAR(11)
        ,EnrollmentID VARCHAR(50)
        ,MiddleName VARCHAR(50)
        ,TermId UNIQUEIDENTIFIER
        ,TermDescription VARCHAR(50)
        ,rowNumber INT
        ); 
 
    INSERT  INTO #getStudentGPAByProgramVersion
            SELECT DISTINCT
                    S.LastName
                   ,S.FirstName
                   ,S.StudentId
                   ,S.StudentNumber
                   ,SE.StuEnrollId
                   ,APV.PrgVerId
                   ,APV.PrgVerDescrip
                   ,CASE WHEN (
                                APV.Credits > 0.0
                                AND APV.Hours > 0
                              )
                              OR ( APV.IsContinuingEd = 1 ) THEN 'Credits-ClockHours'
                         WHEN APV.Credits > 0.0 THEN 'Credits'
                         WHEN APV.Hours > 0.0 THEN 'ClockHours'
                         ELSE ''
                    END AcademicType
                   ,NULL AS ProgramVersion_SimpleGPA
                   ,NULL AS ProgramVersion_WeightedGPA
                   ,NULL AS OverAll_SimpleGPA
                   ,NULL AS OverAll_WeightedGPA
                   ,0 AS IsMakingSAP
                   ,S.SSN
                   ,SE.EnrollmentId
                   ,S.MiddleName
                   ,GST.TermId
                   ,GST.TermDescrip
                   ,ROW_NUMBER() OVER ( PARTITION BY SE.StuEnrollId ORDER BY GST.TermStartDate DESC ) AS RowNumber
            FROM    arStudent S
            INNER JOIN arStuEnrollments SE ON S.StudentId = SE.StudentId
            INNER JOIN dbo.arPrgVersions APV ON SE.PrgVerId = APV.PrgVerId
            INNER JOIN #getStudentGPAbyTerms GST ON GST.StuEnrollId = SE.StuEnrollId
            LEFT OUTER JOIN dbo.adLeadByLeadGroups LLG ON SE.StuEnrollId = LLG.StuEnrollId
            WHERE   SE.StuEnrollId IN ( SELECT  Val
                                        FROM    MultipleValuesForReportParameters(@StuEnrollIdList,',',1) ); 
						 
 
    INSERT  INTO #CoursesNotRepeated
            SELECT  StuEnrollId
                   ,TermId
                   ,TermDescrip
                   ,ReqId
                   ,ReqDescrip
                   ,ClsSectionId
                   ,CreditsEarned
                   ,CreditsAttempted
                   ,CurrentScore
                   ,CurrentGrade
                   ,FinalScore
                   ,FinalGrade
                   ,Completed
                   ,FinalGPA
                   ,Product_WeightedAverage_Credits_GPA
                   ,Count_WeightedAverage_Credits
                   ,Product_SimpleAverage_Credits_GPA
                   ,Count_SimpleAverage_Credits
                   ,ModUser
                   ,ModDate
                   ,TermGPA_Simple
                   ,TermGPA_Weighted
                   ,coursecredits
                   ,CumulativeGPA
                   ,CumulativeGPA_Simple
                   ,FACreditsEarned
                   ,Average
                   ,CumAverage
                   ,TermStartDate
                   ,0 AS rownumber
                   ,(
                      SELECT TOP 1
                                StudentId
                      FROM      arStuEnrollments
                      WHERE     StuEnrollId = dbo.syCreditSummary.StuEnrollId
                    ) AS StudentId
            FROM    syCreditSummary
            WHERE   StuEnrollId IN ( SELECT DISTINCT
                                            StuEnrollID
                                     FROM   #getStudentGPAByProgramVersion )
                    AND (
                          FinalScore IS NOT NULL
                          OR FinalGrade IS NOT NULL
                        )
                    AND ReqId IN ( SELECT   ReqId
                                   FROM     (
                                              SELECT    ReqId
                                                       ,ReqDescrip 
                                                           --,TermId 
                                                       ,StuEnrollId
                                                       ,COUNT(*) AS counter
                                              FROM      syCreditSummary
                                              WHERE     StuEnrollId IN ( SELECT DISTINCT
                                                                                StuEnrollID
                                                                         FROM   #getStudentGPAByProgramVersion )
                                              GROUP BY  ReqId
                                                       ,ReqDescrip 
                                                           --,TermId 
                                                       ,StuEnrollId
                                              HAVING    COUNT(*) = 1
                                            ) dt ); 
 
		--Select * from #CoursesNotRepeated where ReqId='485FBB54-202D-4A46-A353-3F9C76A275E4' 
		--Select * from syCreditSummary 
 
    IF LOWER(@GradeCourseRepetitionsMethod) = 'latest'
        BEGIN 
            INSERT  INTO #CoursesNotRepeated
                    SELECT  dt2.StuEnrollId
                           ,dt2.TermId
                           ,dt2.TermDescrip
                           ,dt2.ReqId
                           ,dt2.ReqDescrip
                           ,dt2.ClsSectionId
                           ,dt2.CreditsEarned
                           ,dt2.CreditsAttempted
                           ,dt2.CurrentScore
                           ,dt2.CurrentGrade
                           ,dt2.FinalScore
                           ,dt2.FinalGrade
                           ,dt2.Completed
                           ,dt2.FinalGPA
                           ,dt2.Product_WeightedAverage_Credits_GPA
                           ,dt2.Count_WeightedAverage_Credits
                           ,dt2.Product_SimpleAverage_Credits_GPA
                           ,dt2.Count_SimpleAverage_Credits
                           ,dt2.ModUser
                           ,dt2.ModDate
                           ,dt2.TermGPA_Simple
                           ,dt2.TermGPA_Weighted
                           ,dt2.coursecredits
                           ,dt2.CumulativeGPA
                           ,dt2.CumulativeGPA_Simple
                           ,dt2.FACreditsEarned
                           ,dt2.Average
                           ,dt2.CumAverage
                           ,dt2.TermStartDate
                           ,dt2.RowNumber
                           ,dt2.StudentId
                    FROM    (
                              SELECT    StuEnrollId
                                       ,TermId
                                       ,TermDescrip
                                       ,ReqId
                                       ,ReqDescrip
                                       ,ClsSectionId
                                       ,CreditsEarned
                                       ,CreditsAttempted
                                       ,CurrentScore
                                       ,CurrentGrade
                                       ,FinalScore
                                       ,FinalGrade
                                       ,Completed
                                       ,FinalGPA
                                       ,Product_WeightedAverage_Credits_GPA
                                       ,Count_WeightedAverage_Credits
                                       ,Product_SimpleAverage_Credits_GPA
                                       ,Count_SimpleAverage_Credits
                                       ,ModUser
                                       ,ModDate
                                       ,TermGPA_Simple
                                       ,TermGPA_Weighted
                                       ,coursecredits
                                       ,CumulativeGPA
                                       ,CumulativeGPA_Simple
                                       ,FACreditsEarned
                                       ,Average
                                       ,CumAverage
                                       ,TermStartDate
                                       ,ROW_NUMBER() OVER ( PARTITION BY ReqId ORDER BY TermStartDate DESC ) AS RowNumber
                                       ,(
                                          SELECT TOP 1
                                                    StudentId
                                          FROM      arStuEnrollments
                                          WHERE     StuEnrollId = dbo.syCreditSummary.StuEnrollId
                                        ) AS StudentId
                              FROM      syCreditSummary
                              WHERE     StuEnrollId IN ( SELECT StuEnrollID
                                                         FROM   #getStudentGPAByProgramVersion )
                                        AND (
                                              FinalScore IS NOT NULL
                                              OR FinalGrade IS NOT NULL
                                            )
                                        AND ReqId IN ( SELECT   ReqId
                                                       FROM     (
                                                                  SELECT    ReqId
                                                                           ,ReqDescrip 
                                                               --,TermId 
                                                                           ,StuEnrollId
                                                                           ,COUNT(*) AS Counter
                                                                  FROM      syCreditSummary
                                                                  WHERE     StuEnrollId IN ( SELECT StuEnrollID
                                                                                             FROM   #getStudentGPAByProgramVersion )
                                                                  GROUP BY  ReqId
                                                                           ,ReqDescrip 
                                                               --,TermId 
                                                                           ,StuEnrollId
                                                                  HAVING    COUNT(*) > 1
                                                                ) dt )
                            ) dt2
                    WHERE   RowNumber = 1
                    ORDER BY TermStartDate DESC; 
        END; 
 
    IF LOWER(@GradeCourseRepetitionsMethod) = 'best'
        BEGIN 
            INSERT  INTO #CoursesNotRepeated
                    SELECT  dt2.StuEnrollId
                           ,dt2.TermId
                           ,dt2.TermDescrip
                           ,dt2.ReqId
                           ,dt2.ReqDescrip
                           ,dt2.ClsSectionId
                           ,dt2.CreditsEarned
                           ,dt2.CreditsAttempted
                           ,dt2.CurrentScore
                           ,dt2.CurrentGrade
                           ,dt2.FinalScore
                           ,dt2.FinalGrade
                           ,dt2.Completed
                           ,dt2.FinalGPA
                           ,dt2.Product_WeightedAverage_Credits_GPA
                           ,dt2.Count_WeightedAverage_Credits
                           ,dt2.Product_SimpleAverage_Credits_GPA
                           ,dt2.Count_SimpleAverage_Credits
                           ,dt2.ModUser
                           ,dt2.ModDate
                           ,dt2.TermGPA_Simple
                           ,dt2.TermGPA_Weighted
                           ,dt2.coursecredits
                           ,dt2.CumulativeGPA
                           ,dt2.CumulativeGPA_Simple
                           ,dt2.FACreditsEarned
                           ,dt2.Average
                           ,dt2.CumAverage
                           ,dt2.TermStartDate
                           ,dt2.RowNumber
                           ,dt2.StudentId
                    FROM    (
                              SELECT    StuEnrollId
                                       ,TermId
                                       ,TermDescrip
                                       ,ReqId
                                       ,ReqDescrip
                                       ,ClsSectionId
                                       ,CreditsEarned
                                       ,CreditsAttempted
                                       ,CurrentScore
                                       ,CurrentGrade
                                       ,FinalScore
                                       ,FinalGrade
                                       ,Completed
                                       ,FinalGPA
                                       ,Product_WeightedAverage_Credits_GPA
                                       ,Count_WeightedAverage_Credits
                                       ,Product_SimpleAverage_Credits_GPA
                                       ,Count_SimpleAverage_Credits
                                       ,ModUser
                                       ,ModDate
                                       ,TermGPA_Simple
                                       ,TermGPA_Weighted
                                       ,coursecredits
                                       ,CumulativeGPA
                                       ,CumulativeGPA_Simple
                                       ,FACreditsEarned
                                       ,Average
                                       ,CumAverage
                                       ,TermStartDate
                                       ,ROW_NUMBER() OVER ( PARTITION BY ReqId ORDER BY FinalGPA DESC ) AS RowNumber
                                       ,(
                                          SELECT TOP 1
                                                    StudentId
                                          FROM      arStuEnrollments
                                          WHERE     StuEnrollId = dbo.syCreditSummary.StuEnrollId
                                        ) AS StudentId
                              FROM      syCreditSummary
                              WHERE     StuEnrollId IN ( SELECT StuEnrollID
                                                         FROM   #getStudentGPAByProgramVersion )
                                        AND (
                                              FinalScore IS NOT NULL
                                              OR FinalGrade IS NOT NULL
                                            )
                                        AND ReqId IN ( SELECT   ReqId
                                                       FROM     (
                                                                  SELECT    ReqId
                                                                           ,ReqDescrip 
                                                               --,TermId 
                                                                           ,StuEnrollId
                                                                           ,COUNT(*) AS Counter
                                                                  FROM      syCreditSummary
                                                                  WHERE     StuEnrollId IN ( SELECT StuEnrollID
                                                                                             FROM   #getStudentGPAByProgramVersion )
                                                                  GROUP BY  ReqId
                                                                           ,ReqDescrip 
                                                               --,TermId 
                                                                           ,StuEnrollId
                                                                  HAVING    COUNT(*) > 1
                                                                ) dt )
                            ) dt2
                    WHERE   RowNumber = 1;  
        END; 
 
        --Select * from #CoursesNotRepeated where ReqId='485FBB54-202D-4A46-A353-3F9C76A275E4' 
 
    IF LOWER(@GradeCourseRepetitionsMethod) = 'average'
        BEGIN 
            INSERT  INTO #CoursesNotRepeated
                    SELECT  dt2.StuEnrollId
                           ,dt2.TermId
                           ,dt2.TermDescrip
                           ,dt2.ReqId
                           ,dt2.ReqDescrip
                           ,dt2.ClsSectionId
                           ,dt2.CreditsEarned
                           ,dt2.CreditsAttempted
                           ,dt2.CurrentScore
                           ,dt2.CurrentGrade
                           ,dt2.FinalScore
                           ,dt2.FinalGrade
                           ,dt2.Completed
                           ,dt2.FinalGPA
                           ,dt2.Product_WeightedAverage_Credits_GPA
                           ,dt2.Count_WeightedAverage_Credits
                           ,dt2.Product_SimpleAverage_Credits_GPA
                           ,dt2.Count_SimpleAverage_Credits
                           ,dt2.ModUser
                           ,dt2.ModDate
                           ,dt2.TermGPA_Simple
                           ,dt2.TermGPA_Weighted
                           ,dt2.coursecredits
                           ,dt2.CumulativeGPA
                           ,dt2.CumulativeGPA_Simple
                           ,dt2.FACreditsEarned
                           ,dt2.Average
                           ,dt2.CumAverage
                           ,dt2.TermStartDate
                           ,dt2.RowNumber
                           ,dt2.StudentId
                    FROM    (
                              SELECT    StuEnrollId
                                       ,TermId
                                       ,TermDescrip
                                       ,ReqId
                                       ,ReqDescrip
                                       ,ClsSectionId
                                       ,CreditsEarned
                                       ,CreditsAttempted
                                       ,CurrentScore
                                       ,CurrentGrade
                                       ,FinalScore
                                       ,FinalGrade
                                       ,Completed
                                       ,FinalGPA
                                       ,Product_WeightedAverage_Credits_GPA
                                       ,Count_WeightedAverage_Credits
                                       ,Product_SimpleAverage_Credits_GPA
                                       ,Count_SimpleAverage_Credits
                                       ,ModUser
                                       ,ModDate
                                       ,TermGPA_Simple
                                       ,TermGPA_Weighted
                                       ,coursecredits
                                       ,CumulativeGPA
                                       ,CumulativeGPA_Simple
                                       ,FACreditsEarned
                                       ,Average
                                       ,CumAverage
                                       ,TermStartDate
                                       ,ROW_NUMBER() OVER ( PARTITION BY ReqId ORDER BY FinalGPA DESC ) AS RowNumber
                                       ,(
                                          SELECT TOP 1
                                                    StudentId
                                          FROM      arStuEnrollments
                                          WHERE     StuEnrollId = dbo.syCreditSummary.StuEnrollId
                                        ) AS StudentId
                              FROM      syCreditSummary
                              WHERE     StuEnrollId IN ( SELECT StuEnrollID
                                                         FROM   #getStudentGPAByProgramVersion )
                                        AND (
                                              FinalScore IS NOT NULL
                                              OR FinalGrade IS NOT NULL
                                            )
                                        AND ReqId IN ( SELECT   ReqId
                                                       FROM     (
                                                                  SELECT    ReqId
                                                                           ,ReqDescrip 
                                                               --,TermId 
                                                                           ,StuEnrollId
                                                                           ,COUNT(*) AS Counter
                                                                  FROM      syCreditSummary
                                                                  WHERE     StuEnrollId IN ( SELECT StuEnrollID
                                                                                             FROM   #getStudentGPAByProgramVersion )
                                                                  GROUP BY  ReqId
                                                                           ,ReqDescrip 
                                                               --,TermId 
                                                                           ,StuEnrollId
                                                                  HAVING    COUNT(*) > 1
                                                                ) dt )
                            ) dt2;  
        END; 
--- ******************* Equivalent Courses in Term Temp Table Starts Here *************************	 
-- Get Equivalent courses for all courses that student is currently registered in 
    SELECT  dt.EquivReqId
           ,dt.StuEnrollId
    INTO    #tmpEquivalentCoursesTakenByStudentInTerm
    FROM    -- Get the list of Equivalent courses taken by student enrollment 
            (
              SELECT DISTINCT
                        CE.EquivReqId
                       ,R.StuEnrollId
                       ,CS.TermId
              FROM      arResults R
              INNER JOIN dbo.arClassSections CS ON CS.ClsSectionId = R.TestId
              INNER JOIN syCreditSummary CSS ON CSS.StuEnrollId = R.StuEnrollId
                                                AND CSS.ReqId = CS.ReqId
              INNER JOIN arReqs R1 ON R1.ReqId = CS.ReqId
              INNER JOIN arCourseEquivalent CE ON R1.ReqId = CE.ReqId
              WHERE     R.StuEnrollId IN ( SELECT   Val
                                           FROM     MultipleValuesForReportParameters(@StuEnrollIdList,',',1) )
                        AND R.GrdSysDetailId IS NOT NULL
                        AND CSS.FinalGPA IS NOT NULL
              UNION
              SELECT DISTINCT
                        CE.EquivReqId
                       ,TR.StuEnrollId
                       ,TR.TermId
              FROM      dbo.arTransferGrades TR
              INNER JOIN syCreditSummary CSS ON CSS.StuEnrollId = TR.StuEnrollId
                                                AND CSS.ReqId = TR.ReqId
              INNER JOIN arReqs R1 ON R1.ReqId = TR.ReqId
              INNER JOIN arCourseEquivalent CE ON R1.ReqId = CE.ReqId
              WHERE     TR.StuEnrollId IN ( SELECT  Val
                                            FROM    MultipleValuesForReportParameters(@StuEnrollIdList,',',1) )
                        AND TR.GrdSysDetailId IS NOT NULL
                        AND CSS.FinalGPA IS NOT NULL
            ) dt; 
 
-- Check if student has got a grade on any of the Equivalent courses 
    SELECT DISTINCT
            ECS.EquivReqId
           ,R.StuEnrollId
           ,CS.TermId
    INTO    #tmpStudentsWhoHasGradedEquivalentCourseInTerm
    FROM    arResults R
    INNER JOIN dbo.arClassSections CS ON CS.ClsSectionId = R.TestId
    INNER JOIN syCreditSummary CSS ON CSS.StuEnrollId = R.StuEnrollId
                                      AND CSS.ReqId = CS.ReqId
    INNER JOIN #tmpEquivalentCoursesTakenByStudentInTerm ECS ON R.StuEnrollId = ECS.StuEnrollId
                                                                AND ECS.EquivReqId = CS.ReqId
    WHERE   R.StuEnrollId IN ( SELECT   Val
                               FROM     MultipleValuesForReportParameters(@StuEnrollIdList,',',1) )
            AND R.GrdSysDetailId IS NOT NULL
            AND CSS.FinalGPA IS NOT NULL
    UNION
    SELECT DISTINCT
            ECS.EquivReqId
           ,TR.StuEnrollId
           ,TR.TermId
    FROM    dbo.arTransferGrades TR
    INNER JOIN dbo.arClassSections CS ON CS.ClsSectionId = TR.ReqId
    INNER JOIN syCreditSummary CSS ON CSS.StuEnrollId = TR.StuEnrollId
                                      AND CSS.ReqId = CS.ReqId
    INNER JOIN #tmpEquivalentCoursesTakenByStudentInTerm ECS ON TR.StuEnrollId = ECS.StuEnrollId
                                                                AND ECS.EquivReqId = CS.ReqId
    WHERE   TR.StuEnrollId IN ( SELECT  Val
                                FROM    MultipleValuesForReportParameters(@StuEnrollIdList,',',1) )
            AND TR.GrdSysDetailId IS NOT NULL
            AND CSS.FinalGPA IS NOT NULL; 
--- ******************* Equivalent Courses Temp Table Ends Here *************************	 
 
    DECLARE @curTermId UNIQUEIDENTIFIER; 
    DECLARE getNodes_Cursor CURSOR FAST_FORWARD FORWARD_ONLY
    FOR
        SELECT  DISTINCT
                StuEnrollId
               ,TermId
        FROM    #CoursesNotRepeated; 
	   
 
    OPEN getNodes_Cursor; 
    FETCH NEXT FROM getNodes_Cursor 
				INTO @StuEnrollId,@curTermId; 
    WHILE @@FETCH_STATUS = 0
        BEGIN 
				----- Program Version GPA Starts Here----------------------------------------------------------------------- 
            SET @cumSimpleCourseCredits = (
                                            SELECT  COUNT(coursecredits)
                                            FROM    #CoursesNotRepeated
                                            WHERE   StuEnrollId IN ( @StuEnrollId )
                                                    AND TermId IN ( @curTermId )
                                                    AND FinalGPA IS NOT NULL
                                          ); 
 
 
            SET @cumSimple_GPA_Credits = (
                                           SELECT   SUM(FinalGPA)
                                           FROM     #CoursesNotRepeated
                                           WHERE    StuEnrollId IN ( @StuEnrollId )
                                                    AND TermId IN ( @curTermId )
                                                    AND FinalGPA IS NOT NULL
                                         ); 
 
            SET @EquivCourse_SA_CC = (
                                       --SELECT    ISNULL(COUNT(Credits), 0) AS Credits 
                                      --FROM      arReqs 
                                      --WHERE     ReqId IN ( 
                                      --          SELECT  CE.EquivReqId 
                                      --          FROM    arCourseEquivalent CE 
                                      --                  INNER JOIN dbo.arClassSections CS ON CE.ReqId = CS.ReqId 
                                      --                  INNER JOIN arResults R ON CS.ClsSectionId = R.TestId 
                                      --                  INNER JOIN syCreditSummary CSS ON CSS.StuEnrollId = R.StuEnrollId 
                                      --                        AND CSS.ReqId = CS.ReqId 
                                      --          WHERE   R.StuEnrollId IN ( 
                                      --                  @StuEnrollId) 
                                      --                  AND CS.TermId IN ( 
                                      --                  @curTermId) 
                                      --                  AND R.GrdSysDetailId IS NOT NULL 
                                      --                  AND CSS.FinalGPA IS NOT NULL) 
                                       SELECT   ISNULL(COUNT(Credits),0) AS Credits
                                       FROM     arReqs
                                       WHERE    ReqId IN ( SELECT   CE.EquivReqId
                                                           FROM     #tmpStudentsWhoHasGradedEquivalentCourseInTerm CE
                                                           WHERE    CE.StuEnrollId IN ( @StuEnrollId )
                                                                    AND CE.TermId IN ( @curTermId ) )
                                     ); 
 
            SET @EquivCourse_SA_GPA = (
                                        SELECT  SUM(ISNULL(GSD.GPA,0.00))
                                        FROM    #tmpStudentsWhoHasGradedEquivalentCourseInTerm GEC
                                        INNER JOIN arResults R ON GEC.StuEnrollId = R.StuEnrollId
                                        INNER JOIN arGradeSystemDetails GSD ON R.GrdSysDetailId = GSD.GrdSysDetailId
                                        INNER JOIN syCreditSummary CSS ON CSS.StuEnrollId = R.StuEnrollId
                                                                          AND CSS.ReqId = GEC.EquivReqId
                                        WHERE   R.StuEnrollId IN ( @StuEnrollId )
                                                AND GEC.TermId IN ( @curTermId )
                                      ); 
            --                           SELECT   SUM(ISNULL(GSD.GPA, 0.00)) 
            --                           FROM     arCourseEquivalent CE 
            --                                    INNER JOIN dbo.arClassSections CS ON CE.ReqId = CS.ReqId 
            --                                    INNER JOIN arResults R ON CS.ClsSectionId = R.TestId 
            --                                    INNER JOIN arGradeSystemDetails GSD ON R.GrdSysDetailId = GSD.GrdSysDetailId 
            --INNER JOIN syCreditSummary CSS ON CSS.StuEnrollId = R.StuEnrollId 
            --                                                  AND CSS.ReqId = CS.ReqId 
            --                           WHERE    R.StuEnrollId IN (@StuEnrollId) 
            --                                    AND CS.TermId IN (@curTermId) 
            --                                    AND R.GrdSysDetailId IS NOT NULL 
            --                                    AND CSS.FinalGPA IS NOT NULL 
                                     
 
 
            SET @cumSimpleCourseCredits = @cumSimpleCourseCredits;                                            --  + ISNULL(@EquivCourse_SA_CC, 0.00); 
            SET @cumSimple_GPA_Credits = @cumSimple_GPA_Credits;                                              --  + ISNULL(@EquivCourse_SA_GPA, 0.00); 
 
	 
            IF @cumSimpleCourseCredits >= 1
                BEGIN 
                    SET @cumSimpleGPA = @cumSimple_GPA_Credits / @cumSimpleCourseCredits; 
                END;  
 
            SET @cumWeightedGPA = 0; 
            SET @cumCourseCredits = (
                                      SELECT    SUM(coursecredits)
                                      FROM      #CoursesNotRepeated
                                      WHERE     StuEnrollId IN ( @StuEnrollId )
                                                AND TermId IN ( @curTermId )
                                                AND FinalGPA IS NOT NULL
                                    ); 
            SET @cumWeighted_GPA_Credits = (
                                             SELECT SUM(coursecredits * FinalGPA)
                                             FROM   #CoursesNotRepeated
                                             WHERE  StuEnrollId IN ( @StuEnrollId )
                                                    AND TermId IN ( @curTermId )
                                                    AND FinalGPA IS NOT NULL
                                           ); 
 
            DECLARE @doesThisCourseHaveAEquivalentCourse INT
               ,@EquivCourse_WGPA_CC1 INT
               ,@EquivCourse_WGPA_GPA1 DECIMAL(18,2); 
	 
            SET @EquivCourse_WGPA_CC1 = (
                                          SELECT    SUM(ISNULL(Credits,0)) AS Credits
                                          FROM      arReqs
                                          WHERE     ReqId IN ( SELECT   CE.EquivReqId
                                                               FROM     #tmpStudentsWhoHasGradedEquivalentCourseInTerm CE
                                                               WHERE    CE.StuEnrollId IN ( @StuEnrollId )
                                                                        AND CE.TermId IN ( @TermId ) )
                                        ); 
                                         --SELECT SUM(ISNULL(Credits, 0)) AS Credits 
                                         --FROM   arReqs 
                                         --WHERE  ReqId IN ( 
                                         --       SELECT  CE.EquivReqId 
                                         --       FROM    arCourseEquivalent CE 
                                         --               INNER JOIN dbo.arClassSections CS ON CE.EquivReqId = CS.ReqId 
                                         --               INNER JOIN arResults R ON CS.ClsSectionId = R.TestId 
                                         --               INNER JOIN syCreditSummary CSS ON CSS.StuEnrollId = R.StuEnrollId 
                                         --                     AND CSS.ReqId = CE.EquivReqId 
                                         --       WHERE   R.StuEnrollId IN ( 
                                         --               @StuEnrollId) 
                                         --               AND R.GrdSysDetailId IS NOT NULL 
                                         --               AND CS.TermId IN ( 
                                         --               @curTermId) 
                                         --               AND CSS.FinalGPA IS NOT NULL) 
                                         
		 
            SET @EquivCourse_WGPA_GPA1 = (
                                           SELECT   SUM(GSD.GPA * R1.Credits)
                                           FROM     #tmpStudentsWhoHasGradedEquivalentCourseInTerm GEC
                                           INNER JOIN arResults R ON GEC.StuEnrollId = R.StuEnrollId
                                           INNER JOIN arGradeSystemDetails GSD ON R.GrdSysDetailId = GSD.GrdSysDetailId
                                           INNER JOIN syCreditSummary CSS ON CSS.StuEnrollId = R.StuEnrollId
                                                                             AND CSS.ReqId = GEC.EquivReqId
                                           INNER JOIN arReqs R1 ON R1.ReqId = GEC.EquivReqId
                                           WHERE    R.StuEnrollId IN ( @StuEnrollId )
                                                    AND GEC.TermId IN ( @curTermId ) 
 
 
                           --               SELECT    SUM(GSD.GPA * R1.Credits) 
                           --               FROM      arCourseEquivalent CE 
                           --                         INNER JOIN dbo.arClassSections CS ON CE.ReqId = CS.ReqId 
                           --                         INNER JOIN arResults R ON CS.ClsSectionId = R.TestId 
                           --INNER JOIN arGradeSystemDetails GSD ON R.GrdSysDetailId = GSD.GrdSysDetailId 
                           --                         INNER JOIN arReqs R1 ON R1.ReqId = CE.ReqId 
                           --                         INNER JOIN syCreditSummary CSS ON CSS.StuEnrollId = R.StuEnrollId 
                           --                                   AND CSS.ReqId = CS.ReqId 
                           --               WHERE     R.StuEnrollId IN ( 
                           --                         @StuEnrollId) 
                           --                         AND R.GrdSysDetailId IS NOT NULL 
                           --                         AND CS.TermId IN ( 
                           --                         @curTermId) 
                           --                         AND CSS.FinalGPA IS NOT NULL 
                                         ); 
 
		 
            SET @cumCourseCredits = @cumCourseCredits;                                                        --  + ISNULL(@EquivCourse_WGPA_CC1, 0.00); 
            SET @cumWeighted_GPA_Credits = @cumWeighted_GPA_Credits;                                          --  + ISNULL(@EquivCourse_WGPA_GPA1, 0.00); 
 
            IF @cumCourseCredits >= 1
                BEGIN 
                    SET @cumWeightedGPA = @cumWeighted_GPA_Credits / @cumCourseCredits; 
                END; 	 
 
            DECLARE @TermCredits DECIMAL(18,2); 
            SET @TermCredits = (
                                 SELECT SUM(CreditsEarned)
                                 FROM   #CoursesNotRepeated
                                 WHERE  StuEnrollId IN ( @StuEnrollId )
                                        AND TermId IN ( @curTermId )
                                        AND FinalGPA IS NOT NULL
                               ); 
 
            UPDATE  #getStudentGPAbyTerms
            SET     TermCredits = @TermCredits
                   ,TermSimpleGPA = @cumSimpleGPA
                   ,TermWeightedGPA = @cumWeightedGPA
            WHERE   StuEnrollId = @StuEnrollId
                    AND TermId = @curTermId; 
 
						  
            FETCH NEXT FROM getNodes_Cursor 
				INTO @StuEnrollId,@curTermId; 
        END; 
    CLOSE getNodes_Cursor; 
    DEALLOCATE getNodes_Cursor; 
 
 
	-- Get Equivalent courses for all courses that student is currently registered in 
    SELECT  dt.EquivReqId
           ,dt.StuEnrollId
    INTO    #tmpEquivalentCoursesTakenByStudent
    FROM    -- Get the list of Equivalent courses taken by student enrollment 
            (
              SELECT DISTINCT
                        CE.EquivReqId
                       ,R.StuEnrollId
              FROM      arResults R
              INNER JOIN dbo.arClassSections CS ON CS.ClsSectionId = R.TestId
              INNER JOIN syCreditSummary CSS ON CSS.StuEnrollId = R.StuEnrollId
                                                AND CSS.ReqId = CS.ReqId
              INNER JOIN arReqs R1 ON R1.ReqId = CS.ReqId
              INNER JOIN arCourseEquivalent CE ON R1.ReqId = CE.ReqId
              WHERE     R.StuEnrollId IN ( SELECT   Val
                                           FROM     MultipleValuesForReportParameters(@StuEnrollIdList,',',1) )
                        AND R.GrdSysDetailId IS NOT NULL
                        AND CSS.FinalGPA IS NOT NULL
              UNION
              SELECT DISTINCT
                        CE.EquivReqId
                       ,TR.StuEnrollId
              FROM      dbo.arTransferGrades TR
              INNER JOIN syCreditSummary CSS ON CSS.StuEnrollId = TR.StuEnrollId
                                                AND CSS.ReqId = TR.ReqId
              INNER JOIN arReqs R1 ON R1.ReqId = TR.ReqId
              INNER JOIN arCourseEquivalent CE ON R1.ReqId = CE.ReqId
              WHERE     TR.StuEnrollId IN ( SELECT  Val
                                            FROM    MultipleValuesForReportParameters(@StuEnrollIdList,',',1) )
                        AND TR.GrdSysDetailId IS NOT NULL
                        AND CSS.FinalGPA IS NOT NULL
            ) dt; 
 
-- Check if student has got a grade on any of the Equivalent courses 
    SELECT DISTINCT
            ECS.EquivReqId
           ,R.StuEnrollId
    INTO    #tmpStudentsWhoHasGradedEquivalentCourse
    FROM    arResults R
    INNER JOIN dbo.arClassSections CS ON CS.ClsSectionId = R.TestId
    INNER JOIN syCreditSummary CSS ON CSS.StuEnrollId = R.StuEnrollId
                                      AND CSS.ReqId = CS.ReqId
    INNER JOIN #tmpEquivalentCoursesTakenByStudent ECS ON R.StuEnrollId = ECS.StuEnrollId
                                                          AND ECS.EquivReqId = CS.ReqId
    WHERE   R.StuEnrollId IN ( SELECT   Val
                               FROM     MultipleValuesForReportParameters(@StuEnrollIdList,',',1) )
            AND R.GrdSysDetailId IS NOT NULL
            AND CSS.FinalGPA IS NOT NULL
    UNION
    SELECT DISTINCT
            ECS.EquivReqId
           ,TR.StuEnrollId
    FROM    dbo.arTransferGrades TR
    INNER JOIN dbo.arClassSections CS ON CS.ClsSectionId = TR.ReqId
    INNER JOIN syCreditSummary CSS ON CSS.StuEnrollId = TR.StuEnrollId
                                      AND CSS.ReqId = CS.ReqId
    INNER JOIN #tmpEquivalentCoursesTakenByStudent ECS ON TR.StuEnrollId = ECS.StuEnrollId
                                                          AND ECS.EquivReqId = CS.ReqId
    WHERE   TR.StuEnrollId IN ( SELECT  Val
                                FROM    MultipleValuesForReportParameters(@StuEnrollIdList,',',1) )
            AND TR.GrdSysDetailId IS NOT NULL
            AND CSS.FinalGPA IS NOT NULL; 
 
    DECLARE getNodes_Cursor CURSOR FAST_FORWARD FORWARD_ONLY
    FOR
        SELECT  StuEnrollID
        FROM    #getStudentGPAByProgramVersion; 
	   
 
    OPEN getNodes_Cursor; 
    FETCH NEXT FROM getNodes_Cursor 
				INTO @StuEnrollId; 
    WHILE @@FETCH_STATUS = 0
        BEGIN 
				----- Program Version GPA Starts Here----------------------------------------------------------------------- 
            SET @cumSimpleCourseCredits = (
                                            SELECT  COUNT(coursecredits)
                                            FROM    #CoursesNotRepeated
                                            WHERE   StuEnrollId IN ( @StuEnrollId )
                                                    AND FinalGPA IS NOT NULL
                                          ); 
 
			--Print '@cumSimpleCourseCredits' 
			--Print @cumSimpleCourseCredits 
 
            SET @cumSimpleCourseCredits_OverAll = (
                                                    SELECT  COUNT(coursecredits)
                                                    FROM    #CoursesNotRepeated
                                                    WHERE   StudentId IN ( SELECT   StudentId
                                                                           FROM     arStuEnrollments
                                                                           WHERE    StuEnrollId = @StuEnrollId )
                                                            AND FinalGPA IS NOT NULL
                                                  ); 
 
 
            SET @cumSimple_GPA_Credits = (
                                           SELECT   SUM(FinalGPA)
                                           FROM     #CoursesNotRepeated
                                           WHERE    StuEnrollId IN ( @StuEnrollId )
                                                    AND FinalGPA IS NOT NULL
                                         ); 
 
            SET @cumSimple_GPA_Credits_OverAll = (
                                                   SELECT   SUM(FinalGPA)
                                                   FROM     #CoursesNotRepeated
                                                   WHERE    StudentId IN ( SELECT   StudentId
                                                                           FROM     arStuEnrollments
                                                                           WHERE    StuEnrollId = @StuEnrollId )
                                                            AND FinalGPA IS NOT NULL
                                                 ); 
 
			 
	 
            SET @EquivCourse_SA_CC = (
                                       SELECT   ISNULL(COUNT(Credits),0) AS Credits
                                       FROM     arReqs
                                       WHERE    ReqId IN ( SELECT   CE.EquivReqId
                                                           FROM     #tmpStudentsWhoHasGradedEquivalentCourse CE
                                                           WHERE    CE.StuEnrollId IN ( @StuEnrollId ) )
                                     ); 
 
			 
														--SELECT  CE.EquivReqId 
														--FROM    arCourseEquivalent CE 
														--        INNER JOIN dbo.arClassSections CS ON CE.ReqId = CS.ReqId 
														--        INNER JOIN arResults R ON CS.ClsSectionId = R.TestId 
														--        INNER JOIN syCreditSummary CSS ON CSS.StuEnrollId = R.StuEnrollId 
														--              AND CSS.ReqId = CS.ReqId 
														--WHERE   R.StuEnrollId IN ( 
														--        @StuEnrollId) 
														--        AND R.GrdSysDetailId IS NOT NULL 
														--        AND CSS.FinalGPA IS NOT NULL 
												 
												 
            SET @EquivCourse_SA_CC_OverAll = (
                                               SELECT   ISNULL(COUNT(Credits),0) AS Credits
                                               FROM     arReqs
                                               WHERE    ReqId IN ( SELECT DISTINCT
                                                                            CE.EquivReqId
                                                                   FROM     #tmpStudentsWhoHasGradedEquivalentCourse CE
                                                                   INNER JOIN arStuEnrollments SE ON CE.StuEnrollId = SE.StuEnrollId
                                                                   INNER JOIN arStudent S ON SE.StudentId = S.StudentId
                                                                   WHERE    S.StudentId IN ( SELECT DISTINCT
                                                                                                    StudentId
                                                                                             FROM   dbo.arStuEnrollments SE1
                                                                                             WHERE  SE1.StuEnrollId = @StuEnrollId ) )
                                             ); 
 
            PRINT 'Test Equiv Course='; 
            PRINT @EquivCourse_SA_CC; 
            PRINT @EquivCourse_SA_CC_OverAll; 
 
                                      --                  SELECT 
                                      --                        CE.EquivReqId 
                                      --                  FROM  arCourseEquivalent CE 
                                      --                        INNER JOIN dbo.arClassSections CS ON CE.ReqId = CS.ReqId 
                                      --                        INNER JOIN arResults R ON CS.ClsSectionId = R.TestId 
                                      --                        INNER JOIN dbo.arStuEnrollments SE ON R.StuEnrollId = SE.StuEnrollId 
                                      --                        INNER JOIN syCreditSummary CSS ON CSS.StuEnrollId = R.StuEnrollId 
                                      --                        AND CSS.ReqId = CS.ReqId 
                                      --                  WHERE SE.StudentId IN ( 
                                      --                        SELECT 
                                      --                        StudentId 
                                      --FROM 
                                      --                        arStuEnrollments 
                                      --                        WHERE 
                                      --                        StuEnrollId = @StuEnrollId) 
                                      --                        AND R.GrdSysDetailId IS NOT NULL 
                                      --                        AND CSS.FinalGPA IS NOT NULL) 
                                            
		 
            SET @EquivCourse_SA_GPA = (
                                        SELECT  SUM(ISNULL(GSD.GPA,0.00))
                                        FROM    #tmpStudentsWhoHasGradedEquivalentCourse GEC
                                        INNER JOIN arResults R ON GEC.StuEnrollId = R.StuEnrollId
                                        INNER JOIN arGradeSystemDetails GSD ON R.GrdSysDetailId = GSD.GrdSysDetailId
                                        INNER JOIN syCreditSummary CSS ON CSS.StuEnrollId = R.StuEnrollId
                                                                          AND CSS.ReqId = GEC.EquivReqId
                                        WHERE   R.StuEnrollId IN ( @StuEnrollId )
                                      ); 
 
			 
                                       --SELECT   SUM(ISNULL(GSD.GPA, 0.00)) 
                                       --FROM     arCourseEquivalent CE 
                                       --         INNER JOIN dbo.arClassSections CS ON CE.ReqId = CS.ReqId 
                                       --         INNER JOIN arResults R ON CS.ClsSectionId = R.TestId 
                                       --         INNER JOIN arGradeSystemDetails GSD ON R.GrdSysDetailId = GSD.GrdSysDetailId 
                                       --         INNER JOIN syCreditSummary CSS ON CSS.StuEnrollId = R.StuEnrollId 
                                       --                       AND CSS.ReqId = CS.ReqId 
                                       --WHERE    R.StuEnrollId IN (@StuEnrollId) 
                                       --         AND R.GrdSysDetailId IS NOT NULL 
                                       --         AND CSS.FinalGPA IS NOT NULL 
                                       
 
            SET @EquivCourse_SA_GPA_OverAll = (
                                                SELECT  SUM(ISNULL(GSD.GPA,0.00))
                                                FROM    #tmpStudentsWhoHasGradedEquivalentCourse GEC
                                                INNER JOIN arResults R ON GEC.StuEnrollId = R.StuEnrollId
                                                INNER JOIN arGradeSystemDetails GSD ON R.GrdSysDetailId = GSD.GrdSysDetailId
                                                INNER JOIN syCreditSummary CSS ON CSS.StuEnrollId = R.StuEnrollId
                                                                                  AND CSS.ReqId = GEC.EquivReqId
                                                INNER JOIN dbo.arStuEnrollments SE2 ON SE2.StuEnrollId = R.StuEnrollId
                                                INNER JOIN arStudent S ON SE2.StudentId = S.StudentId
                                                WHERE   S.StudentId IN ( SELECT DISTINCT
                                                                                StudentId
                                                                         FROM   dbo.arStuEnrollments SE1
                                                                         WHERE  SE1.StuEnrollId = @StuEnrollId )
                                              ); 
 
             
            PRINT 'Test Equiv Course GPA='; 
            PRINT @EquivCourse_SA_GPA; 
            PRINT @EquivCourse_SA_GPA_OverAll; 
			                                   --SELECT   SUM(ISNULL(GSD.GPA, 
                                               --               0.00)) 
                                               --FROM     arCourseEquivalent CE 
                                               --         INNER JOIN dbo.arClassSections CS ON CE.ReqId = CS.ReqId 
                                               --         INNER JOIN arResults R ON CS.ClsSectionId = R.TestId 
                                               --         INNER JOIN arGradeSystemDetails GSD ON R.GrdSysDetailId = GSD.GrdSysDetailId 
                                               --         INNER JOIN dbo.arStuEnrollments SE ON R.StuEnrollId = SE.StuEnrollId 
                                               --         INNER JOIN syCreditSummary CSS ON CSS.StuEnrollId = R.StuEnrollId 
                                               --               AND CSS.ReqId = CS.ReqId 
                                               --WHERE    StudentId IN ( 
                                               --         SELECT 
                                               --               StudentId 
                                               --         FROM  arStuEnrollments 
                                               --         WHERE StuEnrollId = @StuEnrollId) 
                                               --         AND R.GrdSysDetailId IS NOT NULL 
                                               --         AND CSS.FinalGPA IS NOT NULL 
                                           
			 
 
            SET @cumSimpleCourseCredits = @cumSimpleCourseCredits;                                            --  + ISNULL(@EquivCourse_SA_CC, 0.00); 
            SET @cumSimple_GPA_Credits = @cumSimple_GPA_Credits;                                              --  + ISNULL(@EquivCourse_SA_GPA, 0.00); 
 
            SET @cumSimpleCourseCredits_OverAll = @cumSimpleCourseCredits_OverAll;                            --  + ISNULL(@EquivCourse_SA_CC_OverAll, 0.00); 
            SET @cumSimple_GPA_Credits_OverAll = @cumSimple_GPA_Credits_OverAll;                              --  + ISNULL(@EquivCourse_SA_GPA_OverAll, 0.00); 
 
            PRINT 'Cum Values='; 
            PRINT @cumSimple_GPA_Credits; 
			 
            IF @cumSimpleCourseCredits >= 1
                BEGIN 
                    SET @cumSimpleGPA = @cumSimple_GPA_Credits / @cumSimpleCourseCredits; 
                    SET @cumSimpleGPA_OverAll = @cumSimple_GPA_Credits_OverAll / @cumSimpleCourseCredits_OverAll; 
                END;  
			 
            PRINT 'NWH New';	 
            PRINT @cumSimpleCourseCredits; 
            PRINT @cumSimple_GPA_Credits; 
            PRINT @EquivCourse_SA_CC; 
            PRINT @EquivCourse_SA_GPA; 
--			print 'beforePPPRP'	 
--			PRINT        @cumSimpleCourseCredits 
--Print @cumSimple_GPA_Credits 
--			Print '@cumSimpleGPA=' 
--			Print @cumSimpleGPA 
 
			 
            SET @cumWeightedGPA = 0; 
            SET @cumCourseCredits = (
                                      SELECT    SUM(coursecredits)
                                      FROM      #CoursesNotRepeated
                                      WHERE     StuEnrollId IN ( @StuEnrollId )
                                                AND FinalGPA IS NOT NULL
                                    ); 
            SET @cumWeighted_GPA_Credits = (
                                             SELECT SUM(coursecredits * FinalGPA)
                                             FROM   #CoursesNotRepeated
                                             WHERE  StuEnrollId IN ( @StuEnrollId )
                                                    AND FinalGPA IS NOT NULL
                                           ); 
			 
 
            DECLARE @cumCourseCredits_OverAll DECIMAL(18,2)
               ,@cumWeighted_GPA_Credits_OverAll DECIMAL(18,2); 
 
            SET @cumCourseCredits_OverAll = (
                                              SELECT    SUM(coursecredits)
                                              FROM      #CoursesNotRepeated
                                              WHERE     StudentId IN ( SELECT   StudentId
                                                                       FROM     arStuEnrollments
                                                                       WHERE    StuEnrollId = @StuEnrollId )
                                                        AND FinalGPA IS NOT NULL
                                            ); 
            PRINT 'New values='; 
            PRINT @cumWeighted_GPA_Credits; 
            PRINT @cumCourseCredits_OverAll; 
 
            SET @cumWeighted_GPA_Credits_OverAll = (
                                                     SELECT SUM(coursecredits * FinalGPA)
                                                     FROM   #CoursesNotRepeated
                                                     WHERE  StudentId IN ( SELECT   StudentId
                                                                           FROM     arStuEnrollments
                                                                           WHERE    StuEnrollId = @StuEnrollId )
                                                            AND FinalGPA IS NOT NULL
                                                   ); 
 
 
            DECLARE @EquivCourse_WGPA_CC1_OverAll DECIMAL(18,2)
               ,@EquivCourse_WGPA_GPA1_OverAll DECIMAL(18,2)
               ,@cumWeightedGPA_OverAll DECIMAL(18,2); 
		 
            SET @EquivCourse_WGPA_CC1 = (
                                          --      SELECT SUM(ISNULL(Credits, 0)) AS Credits 
                                   --      FROM   arReqs 
                                   --      WHERE  ReqId IN ( 
                                   --             SELECT  CE.EquivReqId 
                                   --             FROM    arCourseEquivalent CE 
                                   --                     INNER JOIN dbo.arClassSections CS ON CE.ReqId = CS.ReqId 
                                   --                     INNER JOIN arResults R ON CS.ClsSectionId = R.TestId 
                                   --                     INNER JOIN syCreditSummary CSS ON CSS.StuEnrollId = R.StuEnrollId 
                                   --AND CSS.ReqId = CS.ReqId 
                                   --             WHERE   R.StuEnrollId IN ( 
                                   --                     @StuEnrollId) 
                                   --                     AND R.GrdSysDetailId IS NOT NULL 
                                   --                     AND CSS.FinalGPA IS NOT NULL) 
                                          SELECT    SUM(ISNULL(Credits,0)) AS Credits
                                          FROM      arReqs
                                          WHERE     ReqId IN ( SELECT   CE.EquivReqId
                                                               FROM     #tmpStudentsWhoHasGradedEquivalentCourse CE
                                                               WHERE    CE.StuEnrollId IN ( @StuEnrollId ) )
                                        ); 
		 
 
            SET @EquivCourse_WGPA_GPA1 = (
                                           --SELECT    SUM(GSD.GPA * R1.Credits) 
                                          --FROM      arCourseEquivalent CE 
                                          --          INNER JOIN dbo.arClassSections CS ON CE.ReqId = CS.ReqId 
                                          --          INNER JOIN arResults R ON CS.ClsSectionId = R.TestId 
                                          --          INNER JOIN arGradeSystemDetails GSD ON R.GrdSysDetailId = GSD.GrdSysDetailId 
                                          --          INNER JOIN arReqs R1 ON R1.ReqId = CE.ReqId 
                                          --          INNER JOIN syCreditSummary CSS ON CSS.StuEnrollId = R.StuEnrollId 
                                          --                    AND CSS.ReqId = CS.ReqId 
                                          --WHERE     R.StuEnrollId IN ( 
                                          --          @StuEnrollId) 
                                          --          AND R.GrdSysDetailId IS NOT NULL 
                                          --          AND CSS.FinalGPA IS NOT NULL 
                                           SELECT   SUM(GSD.GPA * R1.Credits)
                                           FROM     #tmpStudentsWhoHasGradedEquivalentCourse GEC
                                           INNER JOIN arResults R ON GEC.StuEnrollId = R.StuEnrollId
                                           INNER JOIN arGradeSystemDetails GSD ON R.GrdSysDetailId = GSD.GrdSysDetailId
                                           INNER JOIN syCreditSummary CSS ON CSS.StuEnrollId = R.StuEnrollId
                                                                             AND CSS.ReqId = GEC.EquivReqId
                                           INNER JOIN arReqs R1 ON R1.ReqId = GEC.EquivReqId
                                           WHERE    R.StuEnrollId IN ( @StuEnrollId )
                                         ); 
 
            SET @EquivCourse_WGPA_CC1_OverAll = (
                                                  SELECT    SUM(ISNULL(Credits,0)) AS Credits
                                                  FROM      arReqs
                                                  WHERE     ReqId IN ( SELECT DISTINCT
                                                                                CE.EquivReqId
                                                                       FROM     #tmpStudentsWhoHasGradedEquivalentCourse CE
                                                                       INNER JOIN arStuEnrollments SE ON CE.StuEnrollId = SE.StuEnrollId
                                                                       INNER JOIN arStudent S ON SE.StudentId = S.StudentId
                                                                       WHERE    S.StudentId IN ( SELECT DISTINCT
                                                                                                        StudentId
                                                                                                 FROM   dbo.arStuEnrollments SE1
                                                                                                 WHERE  SE1.StuEnrollId = @StuEnrollId ) ) 
 
                                                 --SELECT SUM(ISNULL(Credits, 0)) AS Credits 
                                                 --FROM   arReqs 
                                                 --WHERE  ReqId IN ( 
                                                 --       SELECT 
                                                 --             CE.EquivReqId 
                                                 --       FROM  arCourseEquivalent CE 
                                                 --             INNER JOIN dbo.arClassSections CS ON CE.ReqId = CS.ReqId 
                                                 --             INNER JOIN arResults R ON CS.ClsSectionId = R.TestId 
                                                 --             INNER JOIN dbo.arStuEnrollments SE ON R.StuEnrollId = SE.StuEnrollId 
                                                 --             INNER JOIN syCreditSummary CSS ON CSS.StuEnrollId = R.StuEnrollId 
                                                 --             AND CSS.ReqId = CS.ReqId 
                                                 --       WHERE StudentId IN ( 
                                                 --             SELECT 
                                                 --             StudentId 
                                                 --             FROM 
                                                 --             arStuEnrollments 
                                                 --             WHERE 
                                                 --             StuEnrollId = @StuEnrollId) 
                                                 --             AND R.GrdSysDetailId IS NOT NULL 
                                                 --             AND CSS.FinalGPA IS NOT NULL) 
                                                ); 
 
            SET @EquivCourse_WGPA_GPA1_OverAll = (
                                                   SELECT   SUM(GSD.GPA * R1.Credits)
                                                   FROM     #tmpStudentsWhoHasGradedEquivalentCourse GEC
                                                   INNER JOIN arResults R ON GEC.StuEnrollId = R.StuEnrollId
                                                   INNER JOIN arGradeSystemDetails GSD ON R.GrdSysDetailId = GSD.GrdSysDetailId
                                                   INNER JOIN syCreditSummary CSS ON CSS.StuEnrollId = R.StuEnrollId
                                                                                     AND CSS.ReqId = GEC.EquivReqId
                                                   INNER JOIN arReqs R1 ON R1.ReqId = GEC.EquivReqId
                                                   INNER JOIN dbo.arStuEnrollments SE2 ON SE2.StuEnrollId = R.StuEnrollId
                                                   INNER JOIN arStudent S ON SE2.StudentId = S.StudentId
                                                   WHERE    S.StudentId IN ( SELECT DISTINCT
                                                                                    StudentId
                                                                             FROM   dbo.arStuEnrollments SE1
                                                                             WHERE  SE1.StuEnrollId = @StuEnrollId )
                                                 );  
                                                  --SELECT    SUM(GSD.GPA 
                                                  --            * R1.Credits) 
                                                  --FROM      arCourseEquivalent CE 
                                                  --          INNER JOIN dbo.arClassSections CS ON CE.ReqId = CS.ReqId 
                                                  --          INNER JOIN arResults R ON CS.ClsSectionId = R.TestId 
                                                  --          INNER JOIN arGradeSystemDetails GSD ON R.GrdSysDetailId = GSD.GrdSysDetailId 
                                                  --          INNER JOIN arReqs R1 ON R1.ReqId = CE.ReqId 
                                                  --          INNER JOIN dbo.arStuEnrollments SE ON R.StuEnrollId = SE.StuEnrollId 
                                                  --          INNER JOIN syCreditSummary CSS ON CSS.StuEnrollId = R.StuEnrollId 
                                                  --            AND CSS.ReqId = CS.ReqId 
                                                  --WHERE     StudentId IN ( 
                                                  --          SELECT 
                                                  --            StudentId 
                                                  --          FROM 
                                                  --            arStuEnrollments 
                                                  --          WHERE 
                                                  --            StuEnrollId = @StuEnrollId) 
                                                  --          AND R.GrdSysDetailId IS NOT NULL 
                                                  --          AND CSS.FinalGPA IS NOT NULL 
                                                 
 
            PRINT 'B'; 
            PRINT @cumCourseCredits; 
			--PRINT ISNULL(@EquivCourse_WGPA_CC1, 0.00) 
            PRINT @EquivCourse_WGPA_GPA1; 
 
            SET @cumCourseCredits = @cumCourseCredits;                                                        --  + ISNULL(@EquivCourse_WGPA_CC1, 0.00); 
            SET @cumWeighted_GPA_Credits = @cumWeighted_GPA_Credits;                                          --  + ISNULL(@EquivCourse_WGPA_GPA1, 0.00); 
 
            SET @cumCourseCredits_OverAll = @cumCourseCredits_OverAll;                                        --  + ISNULL(@EquivCourse_WGPA_CC1_OverAll, 0.00); 
            SET @cumWeighted_GPA_Credits_OverAll = @cumWeighted_GPA_Credits_OverAll;                          --  + ISNULL(@EquivCourse_WGPA_GPA1_OverAll, 0.00); 
 
 
 
            PRINT 'Before calc'; 
            PRINT @cumWeighted_GPA_Credits; 
            PRINT @cumCourseCredits; 
 
            IF @cumCourseCredits >= 1
                BEGIN 
                    SET @cumWeightedGPA = @cumWeighted_GPA_Credits / @cumCourseCredits; 
                    SET @cumWeightedGPA_OverAll = @cumWeighted_GPA_Credits_OverAll / @cumCourseCredits_OverAll; 
                END; 	 
 
 
            PRINT @cumWeighted_GPA_Credits; 
 
            --********************* Making SAP ******************************************************* 
            SELECT  @IsMakingSAP = ASCR1.IsMakingSAP
            FROM    (
                      SELECT    ROW_NUMBER() OVER ( PARTITION BY ASCR.StuEnrollId ORDER BY ASCR.DatePerformed DESC ) AS N
                               ,ISNULL(ASCR.IsMakingSAP,0) AS IsMakingSAP
                      FROM      arSAPChkResults AS ASCR
                      WHERE     ASCR.StuEnrollId = @StuEnrollId
                    ) AS ASCR1
            WHERE   ASCR1.N = 1; 
 
             --********************* Update #getStudentGPAByProgramVersion ******************************************************* 
 
            UPDATE  #getStudentGPAByProgramVersion
            SET     ProgramVersion_SimpleGPA = @cumSimpleGPA
                   ,ProgramVersion_WeightedGPA = @cumWeightedGPA
                   ,OverAll_SimpleGPA = @cumSimpleGPA_OverAll
                   ,OverAll_WeightedGPA = @cumWeightedGPA_OverAll
                   ,IsMakingSAP = ISNULL(@IsMakingSAP,0)
            WHERE   StuEnrollID = @StuEnrollId; 
 
            FETCH NEXT FROM getNodes_Cursor 
				INTO @StuEnrollId; 
        END; 
    CLOSE getNodes_Cursor; 
    DEALLOCATE getNodes_Cursor; 
 
	--SELECT * FROM #CoursesNotRepeated 
 
 
 
    CREATE TABLE #GPASummary
        (
         StuEnrollId UNIQUEIDENTIFIER
        ,TermId UNIQUEIDENTIFIER
        ,TermDescrip VARCHAR(100)
        ,TermSimpleGPA DECIMAL(18,2)
        ,TermWeightedGPA DECIMAL(18,2)
        ,ProgramVersionSimpleGPA DECIMAL(18,2)
        ,ProgramVersionWeightedGPA DECIMAL(18,2)
        ,StudentSimpleGPA DECIMAL(18,2)
        ,StudentWeightedGPA DECIMAL(18,2)
        ,IsMakingSAP BIT
        ,AcademicType NVARCHAR(50)
        ); 
 
	 
    INSERT  INTO #GPASummary
            SELECT DISTINCT
                    SGT.StuEnrollId
                   ,SGT.TermId
                   ,TermDescrip
                   ,TermSimpleGPA
                   ,TermWeightedGPA
                   ,ProgramVersion_SimpleGPA AS ProgramVersionSimpleGPA
                   ,ProgramVersion_WeightedGPA AS ProgramVersionWeightedGPA
                   ,OverAll_SimpleGPA AS StudentSimpleGPA
                   ,OverAll_WeightedGPA AS StudentWeightedGPA
                   ,SGPV.IsMakingSAP AS IsMakingSAP
                   ,SGPV.AcademicType AS AcademicType
            FROM    #getStudentGPAbyTerms SGT
            INNER JOIN #getStudentGPAByProgramVersion SGPV ON SGT.StuEnrollId = SGPV.StuEnrollID; 
 
    DROP TABLE #CoursesNotRepeated; 
    DROP TABLE #getStudentGPAbyTerms; 
    DROP TABLE #getStudentGPAByProgramVersion; 
    DROP TABLE #tmpEquivalentCoursesTakenByStudent; 
    DROP TABLE #tmpStudentsWhoHasGradedEquivalentCourse; 
    DROP TABLE #tmpEquivalentCoursesTakenByStudentInTerm; 
    DROP TABLE #tmpStudentsWhoHasGradedEquivalentCourseInTerm; 
 
--''''''''''''''''''''''''''''' End: This section of the stored proc calculates the GPA ''''''''''''''''''''''''''''''' 
 
    DECLARE @GradesFormat AS VARCHAR(50); 
    DECLARE @GPAMethod AS VARCHAR(50); 
    DECLARE @GradeBookAt AS VARCHAR(50); 
    DECLARE @StuEnrollCampusId AS UNIQUEIDENTIFIER; 
    DECLARE @TermStartDate1 AS DATETIME;  
    DECLARE @Score AS DECIMAL(18,2); 
    DECLARE @GrdCompDescrip AS VARCHAR(50); 
 
    DECLARE @curId AS UNIQUEIDENTIFIER; 
    DECLARE @curReqId AS UNIQUEIDENTIFIER; 
    DECLARE @curStuEnrollId AS UNIQUEIDENTIFIER; 
    DECLARE @curDescrip AS VARCHAR(50); 
    DECLARE @curNumber AS INT; 
    DECLARE @curGrdComponentTypeId AS INT; 
    DECLARE @curMinResult AS DECIMAL(18,2);  
    DECLARE @curGrdComponentDescription AS VARCHAR(50);    
    DECLARE @curClsSectionId AS UNIQUEIDENTIFIER; 
    DECLARE @curRownumber AS INT; 
			 
    SET @StuEnrollCampusId = COALESCE((
                                        SELECT TOP 1
                                                ASE.CampusId
                                        FROM    arStuEnrollments AS ASE
                                        WHERE   ASE.StuEnrollId IN ( SELECT Val
                                                                     FROM   MultipleValuesForReportParameters(@StuEnrollIdList,',',1) )
                                      ),NULL); 
    SET @GradesFormat = dbo.GetAppSettingValueByKeyName('GradesFormat',@StuEnrollCampusId); 
    IF ( @GradesFormat IS NOT NULL )
        BEGIN 
            SET @GradesFormat = LOWER(LTRIM(RTRIM(@GradesFormat))); 
        END; 
    SET @GPAMethod = dbo.GetAppSettingValueByKeyName('GPAMethod',@StuEnrollCampusId); 
    IF ( @GPAMethod IS NOT NULL )
        BEGIN 
            SET @GPAMethod = LOWER(LTRIM(RTRIM(@GPAMethod))); 
        END; 
    SET @GradeBookAt = dbo.GetAppSettingValueByKeyName('GradeBookWeightingLevel',@StuEnrollCampusId); 
    IF ( @GradeBookAt IS NOT NULL )
        BEGIN 
            SET @GradeBookAt = LOWER(LTRIM(RTRIM(@GradeBookAt))); 
        END; 
 
  
 
    SELECT  dt1.PrgVerId
           ,dt1.PrgVerDescrip
           ,dt1.PrgVersionTrackCredits
           ,dt1.TermId
           ,dt1.TermDescription
           ,dt1.TermStartDate
           ,dt1.TermEndDate
           ,dt1.CourseId
           ,dt1.CouseStartDate
           ,dt1.CourseCode
           ,dt1.CourseDescription
           ,dt1.CourseCodeDescription
           ,dt1.CourseCredits
           ,dt1.CourseFinAidCredits
           ,dt1.MinVal
           ,dt1.CourseScore
           ,dt1.StuEnrollId
           ,dt1.CreditsAttempted
           ,dt1.CreditsEarned
           ,dt1.Completed
           ,dt1.CurrentScore
           ,dt1.CurrentGrade
           ,dt1.FinalScore
           ,dt1.FinalGrade
           ,dt1.FinalGPA
           ,dt1.ScheduleDays
           ,dt1.ActualDay 
		--, dt1.CampusId 
		-- dt1.CampDescrip 
           ,dt1.GrdBkWgtDetailsCount
           ,dt1.ClockHourProgram
           ,dt1.GradesFormat
           ,dt1.GPAMethod
           ,GS.TermSimpleGPA
           ,GS.TermWeightedGPA
           ,GS.ProgramVersionSimpleGPA
           ,GS.ProgramVersionWeightedGPA
           ,GS.StudentSimpleGPA
           ,GS.StudentWeightedGPA
           ,GS.IsMakingSAP
           ,GS.AcademicType
           ,dt1.RowNumber
           ,dt1.RowNumber_RepeatedCourse
    FROM    (
              SELECT    dt.PrgVerId
                       ,dt.PrgVerDescrip
                       ,dt.PrgVersionTrackCredits
                       ,dt.TermId
                       ,dt.TermDescription
                       ,dt.TermStartDate
                       ,dt.TermEndDate
                       ,dt.CourseId
                       ,dt.CouseStartDate
                       ,dt.CourseCode
                       ,dt.CourseDescription
                       ,dt.CourseCodeDescription
                       ,dt.CourseCredits
                       ,dt.CourseFinAidCredits
                       ,dt.MinVal
                       ,dt.CourseScore
                       ,dt.StuEnrollId
                       ,dt.CreditsAttempted
                       ,dt.CreditsEarned
                       ,dt.Completed
                       ,dt.CurrentScore
                       ,dt.CurrentGrade
                       ,dt.FinalScore
                       ,dt.FinalGrade
                       ,dt.FinalGPA
                       ,dt.ScheduleDays
                       ,dt.ActualDay
                       ,dt.GrdBkWgtDetailsCount
                       ,dt.ClockHourProgram
                       ,@GradesFormat AS GradesFormat
                       ,@GPAMethod AS GPAMethod
                       ,ROW_NUMBER() OVER ( PARTITION BY TermId,CourseId ORDER BY TermStartDate, TermEndDate, TermDescription, CourseDescription ) AS RowNumber 
                        -- In ordert filere repeated courses by double enrollment 
                       ,ROW_NUMBER() OVER ( PARTITION BY CourseId ORDER BY TermStartDate, TermEndDate, TermDescription, CourseDescription ) AS RowNumber_RepeatedCourse
              FROM      (
                          SELECT DISTINCT
                                    PV.PrgVerId AS PrgVerId
                                   ,PV.PrgVerDescrip AS PrgVerDescrip
                                   ,CASE WHEN ( PV.Credits > 0.0 ) THEN 1
                                         ELSE 0
                                    END AS PrgVersionTrackCredits
                                   ,T.TermId AS TermId
                                   ,T.TermDescrip AS TermDescription
                                   ,T.StartDate AS TermStartDate
                                   ,T.EndDate AS TermEndDate
                                   ,CS.ReqId AS CourseId
                                   ,CS.StartDate AS CouseStartDate
                                   ,R.Code AS CourseCode
                                   ,R.Descrip AS CourseDescription
                                   ,'(' + R.Code + ') ' + R.Descrip AS CourseCodeDescription
                                   ,R.Credits AS CourseCredits
                                   ,R.FinAidCredits AS CourseFinAidCredits
                                   ,(
                                      SELECT    MIN(GCD.MinVal)
                                      FROM      arGradeScaleDetails GCD
                                      INNER JOIN arGradeSystemDetails GSD ON GSD.GrdSysDetailId = GCD.GrdSysDetailId
                                      WHERE     GSD.IsPass = 1
                                                AND GCD.GrdScaleId = CS.GrdScaleId
                                    ) AS MinVal
                                   ,RES.Score AS CourseScore
                                   ,SE.StuEnrollId AS StuEnrollId
                                   ,ISNULL(SCS.CreditsAttempted,0) AS CreditsAttempted
                                   ,ISNULL(SCS.CreditsEarned,0) AS CreditsEarned
                                   ,SCS.Completed AS Completed
                                   ,SCS.CurrentScore AS CurrentScore
                                   ,SCS.CurrentGrade AS CurrentGrade
                                   ,SCS.FinalScore AS FinalScore
                                   ,SCS.FinalGrade AS FinalGrade
                                   ,SCS.FinalGPA AS FinalGPA 
						  --, SSAS.ScheduledDays AS ScheduleDays 
						  --, SSAS.ActualDays AS ActualDay 
                                   ,R.Hours AS ScheduleDays
                                   ,CASE WHEN (
                                                ISNULL(SCS.CreditsEarned,0) > 0
                                                OR (
                                                     SELECT GSD.IsCreditsEarned
                                                     FROM   dbo.arGradeSystemDetails GSD
                                                     INNER JOIN dbo.arGradeSystems GS ON GSD.GrdSystemId = GS.GrdSystemId
                                                     INNER JOIN arPrgVersions PV1 ON PV1.GrdSystemId = GS.GrdSystemId
                                                     WHERE  PV1.PrgVerId = PV.PrgVerId
                                                            AND GSD.Grade = SCS.FinalGrade
                                                   ) = 1
                                              ) THEN R.Hours
                                         ELSE 0
                                    END AS ActualDay
                                   ,(
                                      SELECT    COUNT(*) AS GrdBkWgtDetailsCount
                                      FROM      arGrdBkResults
                                      WHERE     StuEnrollId = SE.StuEnrollId
                                                AND ClsSectionId = RES.TestId
                                    ) AS GrdBkWgtDetailsCount
                                   ,CASE WHEN P.ACId = 5 THEN 'True'
                                         ELSE 'False'
                                    END AS ClockHourProgram
                          FROM      arClassSections CS
                          INNER JOIN arResults RES ON RES.TestId = CS.ClsSectionId
                          INNER JOIN arStuEnrollments SE ON RES.StuEnrollId = SE.StuEnrollId
                          INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                          INNER JOIN arPrograms P ON P.ProgId = PV.ProgId
                          INNER JOIN arTerm T ON CS.TermId = T.TermId
                          INNER JOIN arReqs R ON CS.ReqId = R.ReqId
                          LEFT JOIN syCreditSummary SCS ON SE.StuEnrollId = SCS.StuEnrollId
                                                           AND SCS.TermId = T.TermId
                                                           AND SCS.ReqId = R.ReqId
                                                           AND SCS.ClsSectionId = CS.ClsSectionId
                          LEFT JOIN syStudentAttendanceSummary AS SSAS ON SSAS.ClsSectionId = CS.ClsSectionId
                                                                          AND SSAS.StuEnrollId = SE.StuEnrollId
                          WHERE     SE.StuEnrollId IN ( SELECT  Val
                                                        FROM    MultipleValuesForReportParameters(@StuEnrollIdList,',',1) )
                                    AND (
                                          @TermId IS NULL
                                          OR T.TermId = @TermId
                                        ) 
							--AND R.IsAttendanceOnly = 0 
                            -- Nota: 2016-01-05 JT: As per DE12275 Case 986512 Do not check for Completed but check for FinalsGPA is not null 
                            --AND SCS.Completed = 1   
                            --AND SCS.FinalGPA IS NOT NULL 
							-- Show the courses only if it has a grade   Letter or numenic always have GPAFinal not null  
							--AND (ISNULL(SCS.FinalScore, 0) > 0 OR ISNULL(SCS.FinalGrade, 0) 
    --                                AND SCS.CurrentScore IS NOT NULL  -- for dual enrollment courses 
                                    AND (
                                          RES.GrdSysDetailId IS NOT NULL
                                          OR RES.Score IS NOT NULL
                                        )
                          UNION
                          SELECT DISTINCT
                                    PV.PrgVerId AS PrgVerId
                                   ,PV.PrgVerDescrip AS PrgVerDescrip
                                   ,CASE WHEN ( PV.Credits > 0.0 ) THEN 1
                                         ELSE 0
                                    END AS PrgVersionTrackCredits
                                   ,T.TermId AS TermId
                                   ,T.TermDescrip AS TermDescription
                                   ,T.StartDate AS TermStartDate
                                   ,T.EndDate AS TermEndDate
                                   ,GBCR.ReqId AS CourseId
                                   ,T.StartDate AS CouseStartDate   -- tranfered 
                                   ,R.Code AS CourseCode
                                   ,R.Descrip AS CourseDescrip
                                   ,'(' + R.Code + ') ' + R.Descrip AS CourseCodeDescrip
                                   ,R.Credits AS CourseCredits
                                   ,R.FinAidCredits AS CourseFinAidCredits
                                   ,(
                                      SELECT    MIN(GCD.MinVal)
                                      FROM      arGradeScaleDetails GCD
                                      INNER JOIN arGradeSystemDetails GSD ON GSD.GrdSysDetailId = GCD.GrdSysDetailId
                                      WHERE     GSD.IsPass = 1
                                    ) AS MinVal
                                   ,ISNULL(GBCR.Score,0)
                                   ,SE.StuEnrollId
                                   ,ISNULL(SCS.CreditsAttempted,0) AS CreditsAttempted
                                   ,ISNULL(SCS.CreditsEarned,0) AS CreditsEarned
                                   ,SCS.Completed AS Completed
                                   ,SCS.CurrentScore AS CurrentScore
                                   ,SCS.CurrentGrade AS CurrentGrade
                                   ,SCS.FinalScore AS FinalScore
                                   ,SCS.FinalGrade AS FinalGrade
                                   ,SCS.FinalGPA AS FinalGPA
                                   ,R.Hours AS ScheduleDays
                                   ,CASE WHEN (
                                                ISNULL(SCS.CreditsEarned,0) > 0
                                                OR (
                                                     SELECT GSD.IsCreditsEarned
                                                     FROM   dbo.arGradeSystemDetails GSD
                                                     INNER JOIN dbo.arGradeSystems GS ON GSD.GrdSystemId = GS.GrdSystemId
                                                     INNER JOIN arPrgVersions PV1 ON PV1.GrdSystemId = GS.GrdSystemId
                                                     WHERE  PV1.PrgVerId = PV.PrgVerId
                                                            AND GSD.Grade = SCS.FinalGrade
                                                   ) = 1
                                              ) THEN R.Hours
                                         ELSE 0
                                    END AS ActualDay 
						  --, NULL AS ScheduleDays -- SSAS.ScheduledDays AS ScheduleDays 
						  --, NULL AS ActualDays   --SSAS.ActualDays    AS ActualDays 
                                   ,(
                                      SELECT    COUNT(*) AS GrdBkWgtDetailsCount
                                      FROM      arGrdBkConversionResults
                                      WHERE     StuEnrollId = SE.StuEnrollId
                                                AND TermId = GBCR.TermId
                                                AND ReqId = GBCR.ReqId
                                    ) AS GrdBkWgtDetailsCount
                                   ,CASE WHEN P.ACId = 5 THEN 'True'
                                         ELSE 'False'
                                    END AS ClockHourProgram
                          FROM      arTransferGrades GBCR
                          INNER JOIN arStuEnrollments SE ON GBCR.StuEnrollId = SE.StuEnrollId
                          INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                          INNER JOIN arPrograms P ON P.ProgId = PV.ProgId
                          INNER JOIN arTerm T ON GBCR.TermId = T.TermId
                          INNER JOIN arReqs R ON GBCR.ReqId = R.ReqId
                          LEFT JOIN syCreditSummary SCS ON SE.StuEnrollId = SCS.StuEnrollId
                                                           AND T.TermId = SCS.TermId
                                                           AND R.ReqId = SCS.ReqId
                          WHERE     SE.StuEnrollId IN ( SELECT  Val
                                                        FROM    MultipleValuesForReportParameters(@StuEnrollIdList,',',1) )
                                    AND (
                                          @TermId IS NULL
                                          OR T.TermId = @TermId
                                        ) 
							--AND R.IsAttendanceOnly = 0 
                            -- Nota: 2016-01-05 JT: As per DE12275 Case 986512 Do not check for Completed but check for FinalsGPA is not null 
                            --AND SCS.Completed = 1   
                            --AND SCS.FinalGPA IS NOT NULL 
							--AND SCS.Completed = 1 
							-- Show the courses only if it has a grade   Letter or numenic always have GPAFinal not null  
							--AND (ISNULL(SCS.FinalScore, 0) > 0 OR ISNULL(SCS.FinalGrade, 0) 
                                    AND ( GBCR.GrdSysDetailId IS NOT NULL )
                        ) dt
            ) dt1
    INNER JOIN #GPASummary GS ON dt1.StuEnrollId = GS.StuEnrollId
                                 AND dt1.TermId = GS.TermId
    WHERE   dt1.RowNumber_RepeatedCourse = 1   -- In ordert filere repeated courses by double enrollment 
    ORDER BY CASE WHEN @TermId IS NOT NULL THEN ( RANK() OVER ( ORDER BY dt1.TermStartDate, dt1.TermDescription, dt1.CourseDescription ) )
                  ELSE ( RANK() OVER ( ORDER BY dt1.CouseStartDate, dt1.CourseDescription ) )
             END; 
 
--Drop table #CoursesNotRepeated 
--	Drop table #getStudentGPAbyTerms 
--	Drop table #getStudentGPAByProgramVersion 
    DROP TABLE #GPASummary; 
 
-- ========================================================================================================= 
-- END  --  Usp_TR_Sub6_Courses 
-- ========================================================================================================= 
 
 

GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'If not exist table syVersionHistory, Create it ';
GO
IF NOT EXISTS (
                  SELECT 1
                  FROM   sys.objects
                  WHERE  object_id = OBJECT_ID(N'[dbo].[syVersionHistory]')
                         AND type IN ( N'U' )
              )
    BEGIN
        PRINT N'   Creating [dbo].[syVersionHistory]';
        CREATE TABLE dbo.syVersionHistory
            (
                Id INT NOT NULL IDENTITY(1, 1)
               ,Major INT NULL
               ,Minor INT NULL
               ,Build INT NULL
               ,Revision INT NULL
               ,VersionBegin DATETIME NULL
               ,VersionEnd DATETIME NULL
               ,Description VARCHAR(100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
               ,ModUser VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
            ) ON [PRIMARY];
    END;
IF @@ERROR <> 0
    SET NOEXEC ON;
GO

-- =========================================================================================================
-- Rename Primary Keys to fit A1-Naming Conventions.
-- =========================================================================================================
-- =========================================================================================================
-- Change PK from syVersionHistory for PK_syVersionHistory_Id
-- =========================================================================================================
BEGIN
    DECLARE @PKName AS NVARCHAR(500);
    DECLARE @SQL AS NVARCHAR(MAX);
    SET @PKName = NULL;

    -- select the current PK name for syVerionHistory
    SELECT @PKName = I.name
    FROM   sys.tables AS T
    INNER JOIN sys.indexes AS I ON I.object_id = T.object_id
    INNER JOIN sys.schemas AS S ON S.schema_id = T.schema_id
    WHERE  I.is_primary_key = 1
           AND S.name = 'dbo'
           AND T.name = 'syVersionHistory';

    -- If @PKname is not null go Drop it
    IF @PKName IS NOT NULL
        BEGIN
            PRINT N'    Drpp primary key PK_syVersionHistory_' + @PKName + ' on syVersionHistory';
            SET @SQL = 'ALTER TABLE syVersionHistory DROP CONSTRAINT ' + @PKName + '; ';
            EXEC sp_executesql @SQL;
        END;
END;
GO
-- ADD PK to syVersionHistory table
IF EXISTS (
              SELECT 1
              FROM   sys.objects
              WHERE  object_id = OBJECT_ID(N'[dbo].[syVersionHistory]')
                     AND type IN ( N'U', N'PC' )
          )
    BEGIN
        PRINT N'If not exist primary key PK_syVersionHistory_Id on syVersionHistory, create it';
        IF NOT EXISTS (
                          SELECT 1
                          FROM   sys.objects AS O
                          WHERE  object_id = OBJECT_ID(N'[dbo].[PK_syVersionHistory_Id]')
                                 AND type IN ( N'PK' )
                      )
            BEGIN
                PRINT N'    Creating primary key PK_syVersionHistory_Id onsyVersionHistory ';
                ALTER TABLE syVersionHistory
                ADD CONSTRAINT PK_syVersionHistory_Id
                    PRIMARY KEY CLUSTERED ( Id ASC ) ON [PRIMARY];
            END;
    END;
GO
-- =========================================================================================================
-- END  --  -- Change PK from syVersionHistory for PK_syVersionHistory_Id
-- =========================================================================================================
-- =========================================================================================================
-- END  --  Rename Indexes Keys to fit A1-Naming Conventions.
-- =========================================================================================================
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Create constraints DF_syKlassAppConfigurationSetting_KlassAppId, if not exists';
GO
IF EXISTS (
              SELECT 1
              FROM   sys.objects
              WHERE  object_id = OBJECT_ID(N'[dbo].[syKlassAppConfigurationSetting]')
                     AND type IN ( N'U' )
          )
    BEGIN
	    PRINT N'   Table exists syKlassAppConfigurationSetting'; 
        IF NOT EXISTS (
                          SELECT 1
                          FROM   sys.default_constraints AS DC
                          INNER JOIN sys.tables AS T ON T.object_id = DC.parent_object_id
                          INNER JOIN sys.columns AS C ON C.object_id = T.object_id
                                                         AND C.column_id = DC.parent_column_id
                          WHERE  DC.name = 'DF_syKlassAppConfigurationSetting_KlassAppId'
                                 AND T.name = 'syKlassAppConfigurationSetting'
                      )
            BEGIN
                PRINT N'   Adding constraints to [dbo].[syKlassAppConfigurationSetting]';
                ALTER TABLE dbo.syKlassAppConfigurationSetting
                ADD CONSTRAINT DF_syKlassAppConfigurationSetting_KlassAppId
                DEFAULT (( 0 )) FOR KlassAppId;
            END;
    END;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'If Exists a adLeads.HighSchoolId (been not Null) and not exists in syInstitutions.HSId, set adLeads.HighSchoolId to Null ';
IF EXISTS (
			SELECT 1 FROM adLeads AS AL    
			LEFT JOIN syInstitutions AS SI ON SI.HSId = AL.HighSchoolId
			WHERE Al.HighSchoolId IS NOT NULL
			AND  SI.HSId is NULL
          )
    BEGIN
        PRINT N'    Updating HighSchoolId to Null';
		UPDATE AL
			SET AL.HighSchoolId = NULL
		FROM adLeads AS AL    
		LEFT JOIN syInstitutions AS SI ON SI.HSId = AL.HighSchoolId
		WHERE Al.HighSchoolId IS NOT NULL
		AND  SI.HSId is NULL
    END;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'If do not exist [FK_adLeads_SyInstitutions_HighSchoolId_HSId], ADD it';
GO
IF NOT EXISTS (
                  SELECT 1
                  FROM   sys.foreign_keys
                  WHERE  object_id = OBJECT_ID(N'[dbo].[FK_adLeads_SyInstitutions_HighSchoolId_HSId]', 'F')
                         AND parent_object_id = OBJECT_ID(N'[dbo].[adLeads]', 'U')
              )
    BEGIN
        PRINT N'    Creating FK  FK_adLeads_SyInstitutions_HighSchoolId_HSId';
        ALTER TABLE adLeads
        ADD CONSTRAINT FK_adLeads_SyInstitutions_HighSchoolId_HSId
            FOREIGN KEY ( HighSchoolId )
            REFERENCES syInstitutions ( HSId );
    END;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'If Exists FK_adLeads_SyInstitutions_HighSchoolId_HSId, Check constraint ';
GO
IF EXISTS (
            SELECT 1
            FROM   sys.foreign_keys
            WHERE  object_id = OBJECT_ID(N'[dbo].[FK_adLeads_SyInstitutions_HighSchoolId_HSId]', 'F')
                    AND parent_object_id = OBJECT_ID(N'[dbo].[adLeads]', 'U')
          )
    BEGIN
        PRINT N'    Checking constraint FK_adLeads_SyInstitutions_HighSchoolId_HSId';
        ALTER TABLE dbo.adLeads CHECK CONSTRAINT FK_adLeads_SyInstitutions_HighSchoolId_HSId;
    END;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'If exist SP USP_IPEDS_MissingDataReportGained, delete it';
--=================================================================================================
-- USP_IPEDS_MissingDataReportGained
--=================================================================================================
IF EXISTS (
              SELECT 1
              FROM   sys.objects
              WHERE  object_id = OBJECT_ID(N'[dbo].[USP_IPEDS_MissingDataReportGained]')
                     AND type IN ( N'P', N'PC' )
          )
    BEGIN
		PRINT '    Dropping sp USP_IPEDS_MissingDataReportGained';
        DROP PROCEDURE USP_IPEDS_MissingDataReportGained;
    END;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating SP USP_IPEDS_MissingDataReportGained';
GO
--=================================================================================================
-- USP_IPEDS_MissingDataReportGained
--=================================================================================================
CREATE PROCEDURE dbo.USP_IPEDS_MissingDataReportGained
    @campusId AS VARCHAR(50)
   ,@ProgId AS VARCHAR(8000) = NULL
   --'112AE539-EF1F-4513-A795-04D360C254C3,C1B7EB2C-9E8D-490E-A04A-50544FCBA85D,F8DB1537-7F52-49B4-805F-6F2603BF44BD',  
   ,@OrderBY AS INT = 1
AS
    BEGIN
        /************************************************************************************************************************************  
   Business Rules for Missing Data Report  

   1. Exclude students whose current enrollment status is  
     b. No Start  

   2. If student�s current status is Dropped, Expelled or Terminated  
     a. Check if drop date is less than or equal to Official Reporting date (or date range)  
       i. If above condition is met and if student�s LDA falls before the �Exclude� date,   
       then exclude student.   

   3. If student is currently in Grad status,  
     a. Check if expected grad date is less than or equal to Official Reporting date (or date range)  
       i. If above condition is met and if student�s expected Grad Date falls   
       before the �Exclude� date, then exclude student.   
 **************************************************************************************************************************************/
        -- Resolve All to NULL and all are listed....  
        IF @ProgId = 'E711CC07-3806-4E01-AC15-8788BBDC0E4F'
            BEGIN
                SET @ProgId = NULL;
            END;

        SELECT   t.*
                ,(
                     SELECT CASE WHEN t.StudentIdentifier = 'X' THEN 1
                                 ELSE 0
                            END
                 ) AS StudentIdentifierCount
                ,(
                     SELECT CASE WHEN t.BirthDate = 'X' THEN 1
                                 ELSE 0
                            END
                 ) AS BirthDateCount
                ,(
                     SELECT CASE WHEN t.OPEID = 'X' THEN 1
                                 ELSE 0
                            END
                 ) AS OPEIDCount
                --(select case when t.ProgramName ='X' then 1 else 0 end) as ProgramNameCount,  
                ,(
                     SELECT CASE WHEN t.CIPCode = 'X' THEN 1
                                 ELSE 0
                            END
                 ) AS CIPCodeCount
                ,(
                     SELECT CASE WHEN t.CredentialLevel = 'X' THEN 1
                                 ELSE 0
                            END
                 ) AS CredentialLevelCount
                --(SELECT CASE WHEN t.InstitutionName ='X' then 1 else 0 end) as InstitutionNameCount,  
                ,(
                     SELECT CASE WHEN t.ProgramAttendanceBeginDate = 'X' THEN 1
                                 ELSE 0
                            END
                 ) AS ProgramAttendanceBeginDateCount
                ,(
                     SELECT CASE WHEN t.ProgramAttendanceStatusDate = 'X' THEN 1
                                 ELSE 0
                            END
                 ) AS ProgramAttendanceStatusDateCount
                ,(
                     SELECT CASE WHEN t.LengthOfGEProgram = 'X' THEN 1
                                 ELSE 0
                            END
                 ) AS LengthOfGEProgramCount
                ,(
                     SELECT CASE WHEN t.StudentEnrollmentStatusAsOfThe1stDayOfEnrollmentInProgram = 'X' THEN 1
                                 ELSE 0
                            END
                 ) AS StudentEnrollmentStatusAsOfThe1stDayOfEnrollmentInProgramCount
        FROM     (
                     SELECT *
                     FROM   (
                                SELECT
                                    --Select case @ShowSSN when 0 then S.StudentNumber else case when S.SSN IS null then '' else substring(S.SSN,1,3)+'-'+substring(S.SSN,4,2)+'-' + substring(S.SSN,6,4) end end AS StudentIdentifier,   
                                      S.LastName
                                     ,S.FirstName
                                     ,CASE ( ISNULL(S.SSN, ''))
                                           WHEN '' THEN 'X'
                                           ELSE ''
                                      END AS StudentIdentifier
                                     ,CASE ( ISNULL(DOB, ''))
                                           WHEN '' THEN 'X'
                                           ELSE ''
                                      END AS BirthDate
                                     ,CASE ( ISNULL(syCampuses.OPEID, ''))
                                           WHEN '' THEN 'X'
                                           ELSE ''
                                      END AS OPEID
                                     --CASE (isnull(syCampuses.SchoolName,'')) When '' then 'X' else '' end as InstitutionName,  
                                     --CASE (isnull(PV.PrgVerDescrip,'')) When '' then 'X' else '' end as ProgramName,  
                                     ,CASE ( ISNULL(P.CIPCode, ''))
                                           WHEN '' THEN 'X'
                                           ELSE ''
                                      END AS CIPCode
                                     --P.CIPCode AS CIPCodex,  
                                     ,CASE ( ISNULL(CONVERT(NVARCHAR(50), P.CredentialLvlId), ''))
                                           WHEN '' THEN 'X'
                                           ELSE ''
                                      END AS CredentialLevel
                                     ,CASE ( ISNULL(summ.StudentAttendedDate, ''))
                                           WHEN '' THEN ( CASE LOWER(UT.UnitTypeDescrip)
                                                               WHEN 'none' THEN ''
                                                               ELSE 'X'
                                                          END
                                                        )
                                           ELSE ''
                                      END AS ProgramAttendanceBeginDate
                                     ,CASE ( ISNULL(   CASE WHEN T7.SysStatusDescrip = 'Future start'
                                                                 AND (
                                                                         SE.LDA <> NULL
                                                                         OR SE.StartDate <> NULL
                                                                     ) THEN NULL
                                                            WHEN T7.GEProgramStatus = 'C'
                                                                 OR T7.GEProgramStatus = 'G' THEN SE.ExpGradDate
                                                            WHEN T7.GEProgramStatus = 'W' THEN SE.DateDetermined
                                                            ELSE NULL
                                                       END
                                                      ,''
                                                   )
                                           )
                                           WHEN '' THEN 'X'
                                           ELSE ''
                                      END AS ProgramAttendanceStatusDate
                                     ,CASE ( ISNULL(PV.Weeks, ''))
                                           WHEN '' THEN 'X'
                                           ELSE ''
                                      END AS LengthOfGEProgram
                                     ,CASE ( ISNULL(T10.GERptAgencyFldValId, ''))
                                           WHEN '' THEN 'X'
                                           ELSE ''
                                      END AS StudentEnrollmentStatusAsOfThe1stDayOfEnrollmentInProgram
                                FROM  arStudent S
                                LEFT OUTER JOIN arStuEnrollments SE ON S.StudentId = SE.StudentId
                                INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                                INNER JOIN arPrograms P ON P.ProgId = PV.ProgId
                                INNER JOIN syStatusCodes SC ON SE.StatusCodeId = SC.StatusCodeId
                                JOIN  dbo.syCampuses ON syCampuses.CampusId = SE.CampusId
                                LEFT JOIN dbo.syStudentAttendanceSummary summ ON summ.StuEnrollId = SE.StuEnrollId
                                JOIN  dbo.sySysStatus T7 ON T7.SysStatusId = SC.SysStatusId
                                LEFT JOIN dbo.arAttendTypes T10 ON T10.AttendTypeId = SE.attendtypeid
                                INNER JOIN dbo.arAttUnitType UT ON UT.UnitTypeId = PV.UnitTypeId
                                --      left JOIN syRptAgencyFldValues T11 ON T10.gerptagencyfldvalid = T11.RptAgencyFldValId  
                                WHERE (
                                          @ProgId IS NULL
                                          OR P.ProgId IN (
                                                             SELECT strval
                                                             FROM   dbo.SPLIT(@ProgId)
                                                         )
                                      )
                                      AND SE.CampusId = @campusId
                                      AND ( SC.SysStatusId <> 8 )
                                      --AND  SE.StartDate<=@OffRepDate   
                                      AND SC.SysStatusId IN ( 12, 14 )
                                      AND P.IsGEProgram = 1
                            ) t1
                     --  WHERE    
                     --ExcludeDate>@ExcludeStuBefThisDate     
                     UNION
                     SELECT
                         --case @ShowSSN when 0 then S.StudentNumber else case when S.SSN IS null then '' else substring(S.SSN,1,3)+'-'+substring(S.SSN,4,2)+'-' + substring(S.SSN,6,4) end end AS StudentIdentifier,   
                           S.LastName
                          ,S.FirstName
                          ,CASE ( ISNULL(S.SSN, ''))
                                WHEN '' THEN 'X'
                                ELSE ''
                           END AS StudentIdentifier
                          ,CASE ( ISNULL(DOB, ''))
                                WHEN '' THEN 'X'
                                ELSE ''
                           END AS BirthDate
                          ,CASE ( ISNULL(syCampuses.OPEID, ''))
                                WHEN '' THEN 'X'
                                ELSE ''
                           END AS OPEID
                          --CASE(ISNULL (syCampuses.SchoolName,'')) When '' then 'X' else '' end as InstitutionName,  
                          --CASE(ISNULL (PV.PrgVerDescrip,'')) When '' then 'X' else '' end as ProgramName,  
                          ,CASE ( ISNULL(P.CIPCode, ''))
                                WHEN '' THEN 'X'
                                ELSE ''
                           END AS CIPCode
                          --P.CIPCode AS CIPCodex,  
                          ,CASE ( ISNULL(CONVERT(NVARCHAR(50), P.CredentialLvlId), ''))
                                WHEN '' THEN 'X'
                                ELSE ''
                           END AS CredentialLevel
                          ,CASE ( ISNULL(summ.StudentAttendedDate, ''))
                                WHEN '' THEN ( CASE LOWER(UT.UnitTypeDescrip)
                                                    WHEN 'none' THEN ''
                                                    ELSE 'X'
                                               END
                                             )
                                ELSE ''
                           END AS ProgramAttendanceBeginDate
                          ,CASE ( ISNULL(   CASE WHEN T7.SysStatusDescrip = 'Future start'
                                                      AND (
                                                              SE.LDA <> NULL
                                                              OR SE.StartDate <> NULL
                                                          ) THEN NULL
                                                 WHEN T7.GEProgramStatus = 'C'
                                                      OR T7.GEProgramStatus = 'G' THEN SE.ExpGradDate
                                                 WHEN T7.GEProgramStatus = 'W' THEN SE.DateDetermined
                                                 ELSE NULL
                                            END
                                           ,''
                                        )
                                )
                                WHEN '' THEN 'X'
                                ELSE ''
                           END AS ProgramAttendanceStatusDate
                          ,CASE ( ISNULL(PV.Weeks, ''))
                                WHEN '' THEN 'X'
                                ELSE ''
                           END AS LengthOfGEProgram
                          ,CASE ( ISNULL(T10.GERptAgencyFldValId, ''))
                                WHEN '' THEN 'X'
                                ELSE ''
                           END AS StudentEnrollmentStatusAsOfThe1stDayOfEnrollmentInProgram
                     FROM  arStudent S
                     LEFT OUTER JOIN arStuEnrollments SE ON S.StudentId = SE.StudentId
                     INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                     INNER JOIN arPrograms P ON P.ProgId = PV.ProgId
                     INNER JOIN syStatusCodes SC ON SE.StatusCodeId = SC.StatusCodeId
                     JOIN  dbo.syCampuses ON syCampuses.CampusId = SE.CampusId
                     LEFT JOIN dbo.syStudentAttendanceSummary summ ON summ.StuEnrollId = SE.StuEnrollId
                     JOIN  dbo.sySysStatus T7 ON T7.SysStatusId = SC.SysStatusId
                     LEFT JOIN dbo.arAttendTypes T10 ON T10.AttendTypeId = SE.attendtypeid
                     INNER JOIN dbo.arAttUnitType UT ON UT.UnitTypeId = PV.UnitTypeId
                     --LEFT JOIN syRptAgencyFldValues T11 ON T10.gerptagencyfldvalid = T11.RptAgencyFldValId  
                     WHERE (
                               @ProgId IS NULL
                               OR P.ProgId IN (
                                                  SELECT strval
                                                  FROM   dbo.SPLIT(@ProgId)
                                              )
                           )
                           AND SE.CampusId = @campusId
                           AND ( SC.SysStatusId <> 8 )
                           --AND  SE.StartDate<=@OffRepDate   
                           AND SC.SysStatusId NOT IN ( 12, 14 )
                           AND P.IsGEProgram = 1
                 ) t
        WHERE    (
                     StudentIdentifier = 'X'
                     OR BirthDate = 'X'
                     OR OPEID = 'X'
                     --OR InstitutionName='X'   
                     --OR ProgramName='X'   
                     OR CIPCode = 'X'
                     OR CredentialLevel = 'X'
                     OR ProgramAttendanceBeginDate = 'X'
                     OR ProgramAttendanceStatusDate = 'X'
                     OR LengthOfGEProgram = 'X'
                     OR StudentEnrollmentStatusAsOfThe1stDayOfEnrollmentInProgram = 'X'
                 )
        GROUP BY StudentIdentifier
                ,OPEID
                --, InstitutionName  
                ,LastName
                ,FirstName
                --, ProgramName  
                --,CIPCodex  
                ,ProgramAttendanceBeginDate
                ,ProgramAttendanceStatusDate
                ,LengthOfGEProgram
                ,StudentEnrollmentStatusAsOfThe1stDayOfEnrollmentInProgram
                ,BirthDate
                ,CIPCode
                ,CredentialLevel
        ORDER BY
            --case @OrderBY when 1 then t.StudentIdentifier else t.LastName end   
                 t.LastName;


    END;
--=================================================================================================
-- END  --  USP_IPEDS_MissingDataReportGained
--=================================================================================================
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'If exist SP USP_STUDENTGPA_GETDETAILS, delete it';
--=================================================================================================
-- USP_STUDENTGPA_GETDETAILS
--=================================================================================================
IF EXISTS (
              SELECT 1
              FROM   sys.objects
              WHERE  object_id = OBJECT_ID(N'[dbo].[USP_STUDENTGPA_GETDETAILS]')
                     AND type IN ( N'P', N'PC' )
          )
    BEGIN
		PRINT '    Dropping sp USP_STUDENTGPA_GETDETAILS';
        DROP PROCEDURE USP_STUDENTGPA_GETDETAILS;
    END;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'Creating SP USP_STUDENTGPA_GETDETAILS';
GO
--=================================================================================================
-- USP_STUDENTGPA_GETDETAILS
--=================================================================================================
CREATE PROCEDURE [dbo].[USP_STUDENTGPA_GETDETAILS]
    @CampusId UNIQUEIDENTIFIER
   ,@PrgVerId VARCHAR(8000) = NULL
   ,@pTermId VARCHAR(8000) = NULL
   ,@EnrollmentStatusId VARCHAR(8000) = NULL
   ,@StudentGrpId VARCHAR(8000) = NULL
   ,@CumulativeGPAGT DECIMAL(18, 2) = NULL
   ,@CumulativeGPALT DECIMAL(18, 2) = NULL
   ,@termGPAGT DECIMAL(18, 2) = NULL
   ,@termGPALT DECIMAL(18, 2) = NULL
   ,@termCreditsRangeGT DECIMAL(18, 2) = NULL
   ,@termCreditsRangeLT DECIMAL(18, 2) = NULL

/*
-- Note:
-- Review Average Process. already was updated in other SP 
-- Pending changes

*/
AS
    BEGIN

        DECLARE @CumulativeGPA_Range DECIMAL(18, 2);
        DECLARE @GradeCourseRepetitionsMethod VARCHAR(50);
        DECLARE @cumSimpleCourses INTEGER;
        DECLARE @cumSimpleCourseCredits DECIMAL(18, 2);
        DECLARE @cumSimple_GPA_Credits DECIMAL(18, 2);
        DECLARE @cumSimpleGPA DECIMAL(18, 2);
        DECLARE @cumSimpleCourses_OverAll INTEGER;
        DECLARE @cumSimple_GPA_Credits_OverAll DECIMAL(18, 2);
        DECLARE @cumSimpleGPA_OverAll DECIMAL(18, 2);
        DECLARE @times INT;
        DECLARE @counter INT;
        DECLARE @StudentId VARCHAR(50);
        DECLARE @StuEnrollId VARCHAR(50);
        DECLARE @cumCourseCredits DECIMAL(18, 2);
        DECLARE @cumWeighted_GPA_Credits DECIMAL(18, 2);
        DECLARE @cumWeightedGPA DECIMAL(18, 2);
        DECLARE @EquivCourses_SA_CC INT;
        DECLARE @EquivCourse_SA_GPA DECIMAL(18, 2);
        DECLARE @EquivCourses_SA_CC_OverAll INT;
        DECLARE @EquivCourse_SA_GPA_OverAll DECIMAL(18, 2);
        DECLARE @EquivCourse_WGPA_CC1 INT;
        DECLARE @EquivCourse_WGPA_GPA1 DECIMAL(18, 2);
        DECLARE @GPAMethod AS VARCHAR(1000);
        DECLARE @GradesFormat AS VARCHAR(1000);

        SET @GPAMethod = dbo.GetAppSettingValueByKeyName('GPAMethod', @CampusId);
        IF ( @GPAMethod IS NOT NULL )
            BEGIN
                SET @GPAMethod = LOWER(LTRIM(RTRIM(@GPAMethod)));
            END;
        SET @GradesFormat = dbo.GetAppSettingValueByKeyName('GradesFormat', @CampusId);
        IF ( @GradesFormat IS NOT NULL )
            BEGIN
                SET @GradesFormat = LOWER(LTRIM(RTRIM(@GradesFormat)));
            END;

        IF EXISTS (
                  SELECT 1
                  FROM   tempdb.dbo.sysobjects AS O
                  WHERE  O.xtype IN ( 'U' )
                         AND O.id = OBJECT_ID(N'tempdb..#PrgVersionsList')
                  )
            BEGIN
                DROP TABLE #PrgVersionsList;
            END;
        CREATE TABLE #PrgVersionsList
            (
                PrgVerId UNIQUEIDENTIFIER NOT NULL
               ,CONSTRAINT PK_#PrgVersionsList
                    PRIMARY KEY CLUSTERED ( PrgVerId ASC ) ON [PRIMARY]
            );
        IF EXISTS (
                  SELECT 1
                  FROM   tempdb.dbo.sysobjects AS O
                  WHERE  O.xtype IN ( 'U' )
                         AND O.id = OBJECT_ID(N'tempdb..#EnrollmentStatusList')
                  )
            BEGIN
                DROP TABLE #EnrollmentStatusList;
            END;
        CREATE TABLE #EnrollmentStatusList
            (
                StatusCodeId UNIQUEIDENTIFIER NOT NULL
               ,CONSTRAINT PK_#EnrollmentStatusList
                    PRIMARY KEY CLUSTERED ( StatusCodeId ASC ) ON [PRIMARY]
            );
        IF EXISTS (
                  SELECT 1
                  FROM   tempdb.dbo.sysobjects AS O
                  WHERE  O.xtype IN ( 'U' )
                         AND O.id = OBJECT_ID(N'tempdb..#LeadGroupsList')
                  )
            BEGIN
                DROP TABLE #LeadGroupsList;
            END;
        CREATE TABLE #LeadGroupsList
            (
                LeadGrpId UNIQUEIDENTIFIER NOT NULL
               ,CONSTRAINT PK_#LeadGroupsList
                    PRIMARY KEY CLUSTERED ( LeadGrpId ASC ) ON [PRIMARY]
            );
        IF EXISTS (
                  SELECT 1
                  FROM   tempdb.dbo.sysobjects AS O
                  WHERE  O.xtype IN ( 'U' )
                         AND O.id = OBJECT_ID(N'tempdb..#TermIdList')
                  )
            BEGIN
                DROP TABLE #TermIdList;
            END;
        CREATE TABLE #TermIdList
            (
                TermId UNIQUEIDENTIFIER NOT NULL
               ,CONSTRAINT PK_#TermIdList
                    PRIMARY KEY CLUSTERED ( TermId ASC ) ON [PRIMARY]
            );
        IF EXISTS (
                  SELECT 1
                  FROM   tempdb.dbo.sysobjects AS O
                  WHERE  O.xtype IN ( 'U' )
                         AND O.id = OBJECT_ID(N'tempdb..#CoursesNotRepeated')
                  )
            BEGIN
                DROP TABLE #CoursesNotRepeated;
            END;
        CREATE TABLE #CoursesNotRepeated
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,TermId UNIQUEIDENTIFIER
               ,TermDescrip VARCHAR(100)
               ,ReqId UNIQUEIDENTIFIER
               ,ReqDescrip VARCHAR(100)
               ,ClsSectionId VARCHAR(50)
               ,CreditsEarned DECIMAL(18, 2)
               ,CreditsAttempted DECIMAL(18, 2)
               ,CurrentScore DECIMAL(18, 2)
               ,CurrentGrade VARCHAR(10)
               ,FinalScore DECIMAL(18, 2)
               ,FinalGrade VARCHAR(10)
               ,Completed BIT
               ,FinalGPA DECIMAL(18, 2)
               ,Product_WeightedAverage_Credits_GPA DECIMAL(18, 2)
               ,Count_WeightedAverage_Credits DECIMAL(18, 2)
               ,Product_SimpleAverage_Credits_GPA DECIMAL(18, 2)
               ,Count_SimpleAverage_Credits DECIMAL(18, 2)
               ,ModUser VARCHAR(50)
               ,ModDate DATETIME
               ,TermGPA_Simple DECIMAL(18, 2)
               ,TermGPA_Weighted DECIMAL(18, 2)
               ,coursecredits DECIMAL(18, 2)
               ,CumulativeGPA DECIMAL(18, 2)
               ,CumulativeGPA_Simple DECIMAL(18, 2)
               ,FACreditsEarned DECIMAL(18, 2)
               ,Average DECIMAL(18, 2)
               ,CumAverage DECIMAL(18, 2)
               ,TermStartDate DATETIME
               ,rownumber INT NULL
               ,StudentId UNIQUEIDENTIFIER
            );
        IF EXISTS (
                  SELECT 1
                  FROM   tempdb.dbo.sysobjects AS O
                  WHERE  O.xtype IN ( 'U' )
                         AND O.id = OBJECT_ID(N'tempdb..#tmpCoursesNotRepeatedOnlyIds')
                  )
            BEGIN
                -- ALTER TABLE tempdb..#tmpCoursesNotRepeatedOnlyIds 
                --     DROP CONSTRAINT PK_#tmpCoursesNotRepeatedOnlyIds 
                DROP TABLE #tmpCoursesNotRepeatedOnlyIds;
            END;
        CREATE TABLE #tmpCoursesNotRepeatedOnlyIds
            (
                StuEnrollId UNIQUEIDENTIFIER NOT NULL
               ,TermId UNIQUEIDENTIFIER NOT NULL
               ,CONSTRAINT PK_#tmpCoursesNotRepeatedOnlyIds
                    PRIMARY KEY CLUSTERED
                        (
                        StuEnrollId ASC
                       ,TermId ASC
                        ) ON [PRIMARY]
            );
        IF EXISTS (
                  SELECT 1
                  FROM   tempdb.dbo.sysobjects AS O
                  WHERE  O.xtype IN ( 'U' )
                         AND O.id = OBJECT_ID(N'tempdb..#getStudentTerms')
                  )
            BEGIN
                DROP TABLE #getStudentTerms;
            END;
        CREATE TABLE #getStudentTerms
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,TermId UNIQUEIDENTIFIER
               ,TermDescrip VARCHAR(50)
               ,TermCredits DECIMAL(18, 2) NULL
               ,TermSimpleCourses INTEGER NULL
               ,TermSimple_GPA_Credits DECIMAL(18, 2) NULL
               ,TermEquivCourse_SA_CC INTEGER NULL
               ,TermEquivCourse_SA_GPA DECIMAL(18, 2) NULL
               ,TermSimpleGPA DECIMAL(18, 2) NULL
               ,TermCourseCredits DECIMAL(18, 2) NULL
               ,TermWeighted_GPA_Credits DECIMAL(18, 2) NULL
               ,TermEquivCourse_WGPA_CC1 DECIMAL(18, 2) NULL
               ,TermEquivCourse_WGPA_GPA1 DECIMAL(18, 2) NULL
               ,TermWeightedGPA DECIMAL(18, 2) NULL
               ,TermAverageCount INTEGER NULL
               ,TermAverageSum DECIMAL(18, 2) NULL
               ,TermAverage DECIMAL(18, 2) NULL
               ,TermStartDate DATETIME
            );
        IF EXISTS (
                  SELECT 1
                  FROM   tempdb.dbo.sysobjects AS O
                  WHERE  O.xtype IN ( 'U' )
                         AND O.id = OBJECT_ID(N'tempdb..#getStudentEnrollments')
                  )
            BEGIN
                DROP TABLE #getStudentEnrollments;
            END;
        CREATE TABLE #getStudentEnrollments
            (
                LastName VARCHAR(50)
               ,FirstName VARCHAR(50)
               ,StudentId UNIQUEIDENTIFIER
               ,StudentNumber VARCHAR(50)
               ,StuEnrollId UNIQUEIDENTIFIER
               ,PrgVerId UNIQUEIDENTIFIER
               ,ProgramVersion VARCHAR(50)
               ,PrgVersionTrackCredits BIT
               ,PrgVers_EquivCourses_SA_CC INTEGER           --@EquivCourses_SA_CC
               ,PrgVers_SimpleCourseCredits INTEGER          --@cumSimpleCourseCredits
               ,PrgVers_EquivCourse_SA_GPA DECIMAL(18, 2)    --@EquivCourse_SA_GPA
               ,PrgVers_Simple_GPA_Credits DECIMAL(18, 2)    --@cumSimple_GPA_Credits
               ,PrgVers_SimpleGPA DECIMAL(18, 2)             --@cumSimpleGPA
               ,PrgVers_EquivCourse_WGPA_CC1 DECIMAL(18, 2)  --@EquivCourse_WGPA_CC1
               ,PrgVers_CourseCredits DECIMAL(18, 2)         --@cumCourseCredits
               ,PrgVers_EquivCourse_WGPA_GPA1 DECIMAL(18, 2) --@EquivCourse_WGPA_GPA1
               ,PrgVers_Weighted_GPA_Credits DECIMAL(18, 2)  --@cumWeighted_GPA_Credits
               ,PrgVers_WeightedGPA DECIMAL(18, 2)           --@cumWeightedGPA
               ,PrgVers_AverageCount INTEGER
               ,PrgVers_AverageSum DECIMAL(18, 2)
               ,PrgVers_Average DECIMAL(18, 2)
               ,OverAll_EquivCourses_SA_CC INTEGER           --@EquivCourses_SA_CC_OverAll
               ,OverAll_SimpleCourses INTEGER                --@cumSimpleCourses_OverAll
               ,OverAll_EquivCourse_SA_GPA DECIMAL(18, 2)    --@EquivCourse_SA_GPA_OverAll
               ,OverAll_Simple_GPA_Credits DECIMAL(18, 2)    --@cumSimple_GPA_Credits_OverAll
               ,OverAll_SimpleGPA DECIMAL(18, 2)             --@cumSimpleGPA_OverAll
               ,OverAll_EquivCourse_WGPA_CC1 DECIMAL(18, 2)  --@EquivCourse_WGPA_CC1_OverAll
               ,OverAll_CourseCredits DECIMAL(18, 2)         --@cumCourseCredits_OverAll
               ,OverAll_EquivCourse_WGPA_GPA1 DECIMAL(18, 2) --@EquivCourse_WGPA_GPA1_OverAll
               ,OverAll_Weighted_GPA_Credits DECIMAL(18, 2)  --@cumWeighted_GPA_Credits_OverAll
               ,OverAll_WeightedGPA DECIMAL(18, 2)           --@cumWeightedGPA_OverAll
               ,OverAll_AverageCount INTEGER
               ,OverAll_AverageSum DECIMAL(18, 2)
               ,OverAll_Average DECIMAL(18, 2)
               ,SSN VARCHAR(11)
               ,EnrollmentID VARCHAR(50)
               ,MiddleName VARCHAR(50)
               ,TermId UNIQUEIDENTIFIER
               ,TermDescription VARCHAR(50)
               ,TermStartDate DATETIME
               ,rowNumber INT
            );
        IF EXISTS (
                  SELECT 1
                  FROM   tempdb.dbo.sysobjects AS O
                  WHERE  O.xtype IN ( 'U' )
                         AND O.id = OBJECT_ID(N'tempdb..#tmpStudentEnrollmentsOnlyIds')
                  )
            BEGIN
                DROP TABLE #tmpStudentEnrollmentsOnlyIds;
            END;
        CREATE TABLE #tmpStudentEnrollmentsOnlyIds
            (
                StudentId UNIQUEIDENTIFIER NOT NULL
               ,StuEnrollId UNIQUEIDENTIFIER NOT NULL
               ,CONSTRAINT PK_#tmpStudentEnrollmentsOnlyIds
                    PRIMARY KEY CLUSTERED ( StuEnrollId ASC ) ON [PRIMARY]
            );
        IF EXISTS (
                  SELECT 1
                  FROM   tempdb.dbo.sysobjects AS O
                  WHERE  O.xtype IN ( 'U' )
                         AND O.id = OBJECT_ID(N'tempdb..#tmpEquivalentCoursesTakenByStudentInTerm')
                  )
            BEGIN
                DROP TABLE #tmpEquivalentCoursesTakenByStudentInTerm;
            END;
        CREATE TABLE #tmpEquivalentCoursesTakenByStudentInTerm
            (
                EquivReqId UNIQUEIDENTIFIER NOT NULL
               ,StuEnrollId UNIQUEIDENTIFIER NOT NULL
            );
        IF EXISTS (
                  SELECT 1
                  FROM   tempdb.dbo.sysobjects AS O
                  WHERE  O.xtype IN ( 'U' )
                         AND O.id = OBJECT_ID(N'tempdb..#tmpStudentsWhoHasGradedEquivalentCourseInTerm')
                  )
            BEGIN
                DROP TABLE #tmpStudentsWhoHasGradedEquivalentCourseInTerm;
            END;
        CREATE TABLE #tmpStudentsWhoHasGradedEquivalentCourseInTerm
            (
                EquivReqId UNIQUEIDENTIFIER NOT NULL
               ,StuEnrollId UNIQUEIDENTIFIER NOT NULL
               ,TermId UNIQUEIDENTIFIER NOT NULL
            );
        IF EXISTS (
                  SELECT 1
                  FROM   tempdb.dbo.sysobjects AS O
                  WHERE  O.xtype IN ( 'U' )
                         AND O.id = OBJECT_ID(N'tempdb..#tmpEquivalentCoursesTakenByStudent')
                  )
            BEGIN
                DROP TABLE #tmpEquivalentCoursesTakenByStudent;
            END;
        CREATE TABLE #tmpEquivalentCoursesTakenByStudent
            (
                EquivReqId UNIQUEIDENTIFIER NOT NULL
               ,StuEnrollId UNIQUEIDENTIFIER NOT NULL
            );
        IF EXISTS (
                  SELECT 1
                  FROM   tempdb.dbo.sysobjects AS O
                  WHERE  O.xtype IN ( 'U' )
                         AND O.id = OBJECT_ID(N'tempdb..#tmpStudentsWhoHasGradedEquivalentCourse')
                  )
            BEGIN
                DROP TABLE #tmpStudentsWhoHasGradedEquivalentCourse;
            END;
        CREATE TABLE #tmpStudentsWhoHasGradedEquivalentCourse
            (
                EquivReqId UNIQUEIDENTIFIER NOT NULL
               ,StuEnrollId UNIQUEIDENTIFIER NOT NULL
            );
        IF EXISTS (
                  SELECT 1
                  FROM   tempdb.dbo.sysobjects AS O
                  WHERE  O.xtype IN ( 'U' )
                         AND O.id = OBJECT_ID(N'tempdb..#tmpCreditSummary')
                  )
            BEGIN
                DROP TABLE #tmpCreditSummary;
            END;
        CREATE TABLE #tmpCreditSummary
            (
                ReqId UNIQUEIDENTIFIER
               ,StuEnrollId UNIQUEIDENTIFIER
               ,Counted INTEGER
            );

        IF @PrgVerId IS NULL
           OR @PrgVerId = ''
            BEGIN
                INSERT INTO #PrgVersionsList
                            SELECT APV.PrgVerId
                            FROM   arPrgVersions AS APV;
            END;
        ELSE
            BEGIN
                INSERT INTO #PrgVersionsList
                            SELECT Val
                            FROM   MultipleValuesForReportParameters(@PrgVerId, ',', 1);
            END;
        IF @EnrollmentStatusId IS NULL
           OR @EnrollmentStatusId = ''
            BEGIN
                INSERT INTO #EnrollmentStatusList
                            SELECT SSC.StatusCodeId
                            FROM   syStatusCodes AS SSC;
            END;
        ELSE
            BEGIN
                INSERT INTO #EnrollmentStatusList
                            SELECT Val
                            FROM   MultipleValuesForReportParameters(@EnrollmentStatusId, ',', 1);
            END;
        IF @StudentGrpId IS NULL
           OR @StudentGrpId = ''
            BEGIN
                INSERT INTO #LeadGroupsList
                            SELECT ALG.LeadGrpId
                            FROM   adLeadGroups AS ALG;
            END;
        ELSE
            BEGIN
                INSERT INTO #LeadGroupsList
                            SELECT Val
                            FROM   MultipleValuesForReportParameters(@StudentGrpId, ',', 1);
            END;
        -- When term is null --> means Only Show information of Lastest term for the student
        IF (
           @pTermId IS NULL
           OR @pTermId = ''
           )
            BEGIN
                INSERT INTO #TermIdList
                            SELECT AT.TermId
                            FROM   arTerm AS AT;
            END;
        ELSE
            BEGIN
                INSERT INTO #TermIdList
                            SELECT Val
                            FROM   MultipleValuesForReportParameters(@pTermId, ',', 1);
            END;

        SET @GradeCourseRepetitionsMethod = (
                                            SELECT TOP 1 Value
                                            FROM   dbo.syConfigAppSetValues t1
                                            INNER JOIN dbo.syConfigAppSettings t2 ON t1.SettingId = t2.SettingId
                                                                                     AND t2.KeyName = 'GradeCourseRepetitionsMethod'
                                            );
        SET @GradeCourseRepetitionsMethod = LOWER(LTRIM(RTRIM(@GradeCourseRepetitionsMethod)));

        INSERT INTO #getStudentTerms
                    SELECT R.StuEnrollId
                          ,T.TermId
                          ,T.TermDescrip
                          ,NULL
                          ,NULL
                          ,NULL
                          ,NULL
                          ,NULL
                          ,NULL
                          ,NULL
                          ,NULL
                          ,NULL
                          ,NULL
                          ,NULL
                          ,NULL
                          ,NULL
                          ,NULL
                          ,T.StartDate
                    FROM   arResults R
                    INNER JOIN arClassSections CS ON R.TestId = CS.ClsSectionId
                    INNER JOIN arTerm T ON T.TermId = CS.TermId
                    INNER JOIN arStuEnrollments SE ON SE.StuEnrollId = R.StuEnrollId
                    INNER JOIN #PrgVersionsList AS PVL ON PVL.PrgVerId = SE.PrgVerId
                    INNER JOIN dbo.adLeadByLeadGroups LLG ON LLG.StuEnrollId = SE.StuEnrollId
                    INNER JOIN #LeadGroupsList AS LGL ON LGL.LeadGrpId = LLG.LeadGrpId
                    INNER JOIN #EnrollmentStatusList AS ESL ON ESL.StatusCodeId = SE.StatusCodeId
                    WHERE  SE.CampusId = @CampusId
                    UNION
                    SELECT TG.StuEnrollId
                          ,T.TermId
                          ,T.TermDescrip
                          ,NULL
                          ,NULL
                          ,NULL
                          ,NULL
                          ,NULL
                          ,NULL
                          ,NULL
                          ,NULL
                          ,NULL
                          ,NULL
                          ,NULL
                          ,NULL
                          ,NULL
                          ,NULL
                          ,T.StartDate
                    FROM   arTransferGrades TG
                    INNER JOIN arTerm T ON T.TermId = TG.TermId
                    INNER JOIN arStuEnrollments SE ON SE.StuEnrollId = TG.StuEnrollId
                    INNER JOIN dbo.adLeadByLeadGroups LLG ON LLG.StuEnrollId = SE.StuEnrollId
                    INNER JOIN #PrgVersionsList AS PVL2 ON PVL2.PrgVerId = SE.PrgVerId
                    INNER JOIN #EnrollmentStatusList AS ESL ON ESL.StatusCodeId = SE.StatusCodeId
                    INNER JOIN #LeadGroupsList AS LGL ON LGL.LeadGrpId = LLG.LeadGrpId
                    WHERE  SE.CampusId = @CampusId;

        INSERT INTO #getStudentEnrollments
                    SELECT S.LastName
                          ,S.FirstName
                          ,S.StudentId
                          ,S.StudentNumber
                          ,SE.StuEnrollId
                          ,APV.PrgVerId
                          ,APV.PrgVerDescrip
                          ,CASE WHEN ( APV.Credits > 0.0 ) THEN 1
                                ELSE 0
                           END AS PrgVersionTrackCredits
                          ,NULL AS PrgVers_EquivCourses_SA_CC
                          ,NULL AS PrgVers_SimpleCourseCredits
                          ,NULL AS PrgVers_EquivCourse_SA_GPA
                          ,NULL AS PrgVers_Simple_GPA_Credits
                          ,NULL AS PrgVers_SimpleGPA
                          ,NULL AS PrgVers_EquivCourse_WGPA_CC1
                          ,NULL AS PrgVers_CourseCredits
                          ,NULL AS PrgVers_EquivCourse_WGPA_GPA1
                          ,NULL AS PrgVers_Weighted_GPA_Credits
                          ,NULL AS PrgVers_WeightedGPA
                          ,NULL AS PrgVers_AverageCount
                          ,NULL AS PrgVers_AverageSum
                          ,NULL AS PrgVers_Average
                          ,NULL AS OverAll_EquivCourses_SA_CC
                          ,NULL AS OverAll_SimpleCourses
                          ,NULL AS OverAll_EquivCourse_SA_GPA
                          ,NULL AS OverAll_Simple_GPA_Credits
                          ,NULL AS OverAll_SimpleGPA
                          ,NULL AS OverAll_EquivCourse_WGPA_CC1
                          ,NULL AS OverAll_CourseCredits
                          ,NULL AS OverAll_EquivCourse_WGPA_GPA1
                          ,NULL AS OverAll_Weighted_GPA_Credits
                          ,NULL AS OverAll_WeightedGPA
                          ,NULL AS OverAll_AverageCount
                          ,NULL AS OverAll_AverageSum
                          ,NULL AS OverAll_Average
                          ,S.SSN
                          ,SE.EnrollmentId
                          ,S.MiddleName
                          ,GST.TermId
                          ,GST.TermDescrip
                          ,GST.TermStartDate
                          ,ROW_NUMBER() OVER ( PARTITION BY SE.StuEnrollId
                                               ORDER BY GST.TermStartDate DESC
                                             ) AS RowNumber
                    FROM   arStudent S
                    INNER JOIN arStuEnrollments SE ON S.StudentId = SE.StudentId
                    INNER JOIN #EnrollmentStatusList AS ESL ON ESL.StatusCodeId = SE.StatusCodeId
                    INNER JOIN dbo.arPrgVersions APV ON SE.PrgVerId = APV.PrgVerId
                    INNER JOIN #getStudentTerms GST ON GST.StuEnrollId = SE.StuEnrollId
                    INNER JOIN #PrgVersionsList AS PVL ON PVL.PrgVerId = SE.PrgVerId
                    LEFT OUTER JOIN adLeadByLeadGroups LLG ON SE.StuEnrollId = LLG.StuEnrollId
                    LEFT OUTER JOIN #LeadGroupsList AS LGL ON LGL.LeadGrpId = LLG.LeadGrpId
                    WHERE  SE.CampusId = @CampusId;

        INSERT INTO #tmpStudentEnrollmentsOnlyIds
                    SELECT DISTINCT GSE.StudentId
                          ,GSE.StuEnrollId
                    FROM   #getStudentEnrollments AS GSE;

        INSERT INTO #tmpCreditSummary
                    SELECT   SCS.ReqId
                            ,SCS.StuEnrollId
                            ,COUNT(*) AS Counted
                    FROM     syCreditSummary AS SCS
                    INNER JOIN #tmpStudentEnrollmentsOnlyIds AS GSE ON GSE.StuEnrollId = SCS.StuEnrollId
                    GROUP BY SCS.StuEnrollId
                            ,SCS.ReqId
                    ORDER BY SCS.StuEnrollId
                            ,SCS.ReqId;



        INSERT INTO #CoursesNotRepeated
                    SELECT SCS.StuEnrollId
                          ,SCS.TermId
                          ,SCS.TermDescrip
                          ,SCS.ReqId
                          ,SCS.ReqDescrip
                          ,SCS.ClsSectionId
                          ,SCS.CreditsEarned
                          ,SCS.CreditsAttempted
                          ,SCS.CurrentScore
                          ,SCS.CurrentGrade
                          ,SCS.FinalScore
                          ,SCS.FinalGrade
                          ,SCS.Completed
                          ,SCS.FinalGPA
                          ,SCS.Product_WeightedAverage_Credits_GPA
                          ,SCS.Count_WeightedAverage_Credits
                          ,SCS.Product_SimpleAverage_Credits_GPA
                          ,SCS.Count_SimpleAverage_Credits
                          ,SCS.ModUser
                          ,SCS.ModDate
                          ,SCS.TermGPA_Simple
                          ,SCS.TermGPA_Weighted
                          ,SCS.coursecredits
                          ,SCS.CumulativeGPA
                          ,SCS.CumulativeGPA_Simple
                          ,SCS.FACreditsEarned
                          ,SCS.Average
                          ,SCS.CumAverage
                          ,SCS.TermStartDate
                          ,0 AS rownumber
                          ,GSE.StudentId
                    FROM   syCreditSummary AS SCS
                    INNER JOIN #tmpCreditSummary AS TCS ON TCS.ReqId = SCS.ReqId
                                                           AND TCS.StuEnrollId = SCS.StuEnrollId
                    INNER JOIN #tmpStudentEnrollmentsOnlyIds AS GSE ON GSE.StuEnrollId = TCS.StuEnrollId
                                                                       AND TCS.Counted = 1
                                                                       AND (
                                                                           FinalScore IS NOT NULL
                                                                           OR FinalGrade IS NOT NULL
                                                                           );

        IF @GradeCourseRepetitionsMethod = 'latest'
            BEGIN
                INSERT INTO #CoursesNotRepeated
                            SELECT dt2.StuEnrollId
                                  ,dt2.TermId
                                  ,dt2.TermDescrip
                                  ,dt2.ReqId
                                  ,dt2.ReqDescrip
                                  ,dt2.ClsSectionId
                                  ,dt2.CreditsEarned
                                  ,dt2.CreditsAttempted
                                  ,dt2.CurrentScore
                                  ,dt2.CurrentGrade
                                  ,dt2.FinalScore
                                  ,dt2.FinalGrade
                                  ,dt2.Completed
                                  ,dt2.FinalGPA
                                  ,dt2.Product_WeightedAverage_Credits_GPA
                                  ,dt2.Count_WeightedAverage_Credits
                                  ,dt2.Product_SimpleAverage_Credits_GPA
                                  ,dt2.Count_SimpleAverage_Credits
                                  ,dt2.ModUser
                                  ,dt2.ModDate
                                  ,dt2.TermGPA_Simple
                                  ,dt2.TermGPA_Weighted
                                  ,dt2.coursecredits
                                  ,dt2.CumulativeGPA
                                  ,dt2.CumulativeGPA_Simple
                                  ,dt2.FACreditsEarned
                                  ,dt2.Average
                                  ,dt2.CumAverage
                                  ,dt2.TermStartDate
                                  ,dt2.RowNumber
                                  ,dt2.StudentId
                            FROM   (
                                   SELECT SCS.StuEnrollId
                                         ,SCS.TermId
                                         ,SCS.TermDescrip
                                         ,SCS.ReqId
                                         ,SCS.ReqDescrip
                                         ,SCS.ClsSectionId
                                         ,SCS.CreditsEarned
                                         ,SCS.CreditsAttempted
                                         ,SCS.CurrentScore
                                         ,SCS.CurrentGrade
                                         ,SCS.FinalScore
                                         ,SCS.FinalGrade
                                         ,SCS.Completed
                                         ,SCS.FinalGPA
                                         ,SCS.Product_WeightedAverage_Credits_GPA
                                         ,SCS.Count_WeightedAverage_Credits
                                         ,SCS.Product_SimpleAverage_Credits_GPA
                                         ,SCS.Count_SimpleAverage_Credits
                                         ,SCS.ModUser
                                         ,SCS.ModDate
                                         ,SCS.TermGPA_Simple
                                         ,SCS.TermGPA_Weighted
                                         ,SCS.coursecredits
                                         ,SCS.CumulativeGPA
                                         ,SCS.CumulativeGPA_Simple
                                         ,SCS.FACreditsEarned
                                         ,SCS.Average
                                         ,SCS.CumAverage
                                         ,SCS.TermStartDate
                                         ,ROW_NUMBER() OVER ( PARTITION BY SCS.StuEnrollId
                                                                          ,SCS.ReqId
                                                              ORDER BY SCS.TermStartDate DESC
                                                            ) AS RowNumber
                                         ,GSE.StudentId
                                   FROM   syCreditSummary AS SCS
                                   INNER JOIN #tmpCreditSummary AS TCS ON TCS.ReqId = SCS.ReqId
                                                                          AND TCS.StuEnrollId = SCS.StuEnrollId
                                   INNER JOIN #tmpStudentEnrollmentsOnlyIds AS GSE ON GSE.StuEnrollId = SCS.StuEnrollId
                                   WHERE  (
                                          SCS.FinalScore IS NOT NULL
                                          OR SCS.FinalGrade IS NOT NULL
                                          )
                                          AND TCS.Counted > 1
                                   ) AS dt2
                            WHERE  dt2.RowNumber = 1;
            END;

        IF @GradeCourseRepetitionsMethod = 'best'
            BEGIN
                INSERT INTO #CoursesNotRepeated
                            SELECT dt2.StuEnrollId
                                  ,dt2.TermId
                                  ,dt2.TermDescrip
                                  ,dt2.ReqId
                                  ,dt2.ReqDescrip
                                  ,dt2.ClsSectionId
                                  ,dt2.CreditsEarned
                                  ,dt2.CreditsAttempted
                                  ,dt2.CurrentScore
                                  ,dt2.CurrentGrade
                                  ,dt2.FinalScore
                                  ,dt2.FinalGrade
                                  ,dt2.Completed
                                  ,dt2.FinalGPA
                                  ,dt2.Product_WeightedAverage_Credits_GPA
                                  ,dt2.Count_WeightedAverage_Credits
                                  ,dt2.Product_SimpleAverage_Credits_GPA
                                  ,dt2.Count_SimpleAverage_Credits
                                  ,dt2.ModUser
                                  ,dt2.ModDate
                                  ,dt2.TermGPA_Simple
                                  ,dt2.TermGPA_Weighted
                                  ,dt2.coursecredits
                                  ,dt2.CumulativeGPA
                                  ,dt2.CumulativeGPA_Simple
                                  ,dt2.FACreditsEarned
                                  ,dt2.Average
                                  ,dt2.CumAverage
                                  ,dt2.TermStartDate
                                  ,dt2.RowNumber
                                  ,dt2.StudentId
                            FROM   (
                                   SELECT SCS.StuEnrollId
                                         ,SCS.TermId
                                         ,SCS.TermDescrip
                                         ,SCS.ReqId
                                         ,SCS.ReqDescrip
                                         ,SCS.ClsSectionId
                                         ,SCS.CreditsEarned
                                         ,SCS.CreditsAttempted
                                         ,SCS.CurrentScore
                                         ,SCS.CurrentGrade
                                         ,SCS.FinalScore
                                         ,SCS.FinalGrade
                                         ,SCS.Completed
                                         ,SCS.FinalGPA
                                         ,SCS.Product_WeightedAverage_Credits_GPA
                                         ,SCS.Count_WeightedAverage_Credits
                                         ,SCS.Product_SimpleAverage_Credits_GPA
                                         ,SCS.Count_SimpleAverage_Credits
                                         ,SCS.ModUser
                                         ,SCS.ModDate
                                         ,SCS.TermGPA_Simple
                                         ,SCS.TermGPA_Weighted
                                         ,SCS.coursecredits
                                         ,SCS.CumulativeGPA
                                         ,SCS.CumulativeGPA_Simple
                                         ,SCS.FACreditsEarned
                                         ,SCS.Average
                                         ,SCS.CumAverage
                                         ,SCS.TermStartDate
                                         --,ROW_NUMBER() OVER ( PARTITION BY SCS.StuEnrollId, ReqId ORDER BY FinalGPA DESC ) AS RowNumber
                                         ,ROW_NUMBER() OVER ( PARTITION BY SCS.StuEnrollId
                                                                          ,SCS.ReqId
                                                              ORDER BY Completed DESC
                                                                      ,CreditsEarned DESC
                                                                      ,FinalGPA DESC
                                                            ) AS RowNumber
                                         ,GSE.StudentId AS StudentId
                                   FROM   syCreditSummary AS SCS
                                   INNER JOIN #tmpCreditSummary AS TCS ON TCS.ReqId = SCS.ReqId
                                                                          AND TCS.StuEnrollId = SCS.StuEnrollId
                                   INNER JOIN #tmpStudentEnrollmentsOnlyIds AS GSE ON GSE.StuEnrollId = SCS.StuEnrollId
                                   WHERE  (
                                          SCS.FinalScore IS NOT NULL
                                          OR SCS.FinalGrade IS NOT NULL
                                          )
                                          AND TCS.Counted > 1
                                   ) dt2
                            WHERE  RowNumber = 1;
            END;

        /*
-- Note:
-- Review Average Process. already was updated in other SP 
-- Pending changes
*/

        IF @GradeCourseRepetitionsMethod = 'average'
            BEGIN
                INSERT INTO #CoursesNotRepeated
                            SELECT dt2.StuEnrollId
                                  ,dt2.TermId
                                  ,dt2.TermDescrip
                                  ,dt2.ReqId
                                  ,dt2.ReqDescrip
                                  ,dt2.ClsSectionId
                                  ,dt2.CreditsEarned
                                  ,dt2.CreditsAttempted
                                  ,dt2.CurrentScore
                                  ,dt2.CurrentGrade
                                  ,dt2.FinalScore
                                  ,dt2.FinalGrade
                                  ,dt2.Completed
                                  ,dt2.FinalGPA
                                  ,dt2.Product_WeightedAverage_Credits_GPA
                                  ,dt2.Count_WeightedAverage_Credits
                                  ,dt2.Product_SimpleAverage_Credits_GPA
                                  ,dt2.Count_SimpleAverage_Credits
                                  ,dt2.ModUser
                                  ,dt2.ModDate
                                  ,dt2.TermGPA_Simple
                                  ,dt2.TermGPA_Weighted
                                  ,dt2.coursecredits
                                  ,dt2.CumulativeGPA
                                  ,dt2.CumulativeGPA_Simple
                                  ,dt2.FACreditsEarned
                                  ,dt2.Average
                                  ,dt2.CumAverage
                                  ,dt2.TermStartDate
                                  ,dt2.RowNumber
                                  ,dt2.StudentId
                            FROM   (
                                   SELECT SCS.StuEnrollId
                                         ,SCS.TermId
                                         ,SCS.TermDescrip
                                         ,SCS.ReqId
                                         ,SCS.ReqDescrip
                                         ,SCS.ClsSectionId
                                         ,SCS.CreditsEarned
                                         ,SCS.CreditsAttempted
                                         ,SCS.CurrentScore
                                         ,SCS.CurrentGrade
                                         ,SCS.FinalScore
                                         ,SCS.FinalGrade
                                         ,SCS.Completed
                                         ,SCS.FinalGPA
                                         ,SCS.Product_WeightedAverage_Credits_GPA
                                         ,SCS.Count_WeightedAverage_Credits
                                         ,SCS.Product_SimpleAverage_Credits_GPA
                                         ,SCS.Count_SimpleAverage_Credits
                                         ,SCS.ModUser
                                         ,SCS.ModDate
                                         ,SCS.TermGPA_Simple
                                         ,SCS.TermGPA_Weighted
                                         ,SCS.coursecredits
                                         ,SCS.CumulativeGPA
                                         ,SCS.CumulativeGPA_Simple
                                         ,SCS.FACreditsEarned
                                         ,SCS.Average
                                         ,SCS.CumAverage
                                         ,SCS.TermStartDate
                                         ,ROW_NUMBER() OVER ( PARTITION BY SCS.StuEnrollId
                                                                          ,SCS.ReqId
                                                              ORDER BY SCS.FinalGPA DESC
                                                            ) AS RowNumber
                                         ,GSE.StudentId AS StudentId
                                   FROM   syCreditSummary AS SCS
                                   INNER JOIN #tmpCreditSummary AS TCS ON TCS.ReqId = SCS.ReqId
                                                                          AND TCS.StuEnrollId = SCS.StuEnrollId
                                   INNER JOIN #tmpStudentEnrollmentsOnlyIds AS GSE ON GSE.StuEnrollId = SCS.StuEnrollId
                                   WHERE  (
                                          SCS.FinalScore IS NOT NULL
                                          OR SCS.FinalGrade IS NOT NULL
                                          )
                                          AND TCS.Counted > 1
                                   ) dt2;
            END;


        --- ******************* Equivalent Courses in Term Temp Table Starts Here *************************	
        -- Get Equivalent courses for all courses that student is currently registered in
        INSERT INTO #tmpEquivalentCoursesTakenByStudentInTerm
                    SELECT dt.EquivReqId
                          ,dt.StuEnrollId
                    FROM -- Get the list of Equivalent courses taken by student enrollment
                           (
                           SELECT DISTINCT CE.EquivReqId
                                 ,R.StuEnrollId
                                 ,CS.TermId
                           FROM   arResults R
                           INNER JOIN dbo.arClassSections CS ON CS.ClsSectionId = R.TestId
                           INNER JOIN syCreditSummary CSS ON CSS.StuEnrollId = R.StuEnrollId
                                                             AND CSS.ReqId = CS.ReqId
                           INNER JOIN arReqs R1 ON R1.ReqId = CS.ReqId
                           INNER JOIN arCourseEquivalent CE ON R1.ReqId = CE.ReqId
                           INNER JOIN #tmpStudentEnrollmentsOnlyIds AS GSE ON GSE.StuEnrollId = R.StuEnrollId
                           WHERE  R.GrdSysDetailId IS NOT NULL
                                  AND (
                                      (
                                      CSS.FinalGPA IS NOT NULL
                                      AND @GradesFormat = 'letter'
                                      )
                                      OR (
                                         CSS.FinalScore IS NOT NULL
                                         AND @GradesFormat = 'numeric'
                                         )
                                      )
                           UNION
                           SELECT DISTINCT CE.EquivReqId
                                 ,TR.StuEnrollId
                                 ,TR.TermId
                           FROM   dbo.arTransferGrades TR
                           INNER JOIN syCreditSummary CSS ON CSS.StuEnrollId = TR.StuEnrollId
                                                             AND CSS.ReqId = TR.ReqId
                           INNER JOIN arReqs R1 ON R1.ReqId = TR.ReqId
                           INNER JOIN arCourseEquivalent CE ON R1.ReqId = CE.ReqId
                           INNER JOIN #tmpStudentEnrollmentsOnlyIds AS GSE ON GSE.StuEnrollId = TR.StuEnrollId
                           WHERE  TR.GrdSysDetailId IS NOT NULL
                                  AND (
                                      (
                                      CSS.FinalGPA IS NOT NULL
                                      AND @GradesFormat = 'letter'
                                      )
                                      OR (
                                         CSS.FinalScore IS NOT NULL
                                         AND @GradesFormat = 'numeric'
                                         )
                                      )
                           ) dt;

        -- Check if student has got a grade on any of the Equivalent courses
        INSERT INTO #tmpStudentsWhoHasGradedEquivalentCourseInTerm
                    SELECT DISTINCT ECS.EquivReqId
                          ,R.StuEnrollId
                          ,CS.TermId
                    FROM   arResults R
                    INNER JOIN dbo.arClassSections CS ON CS.ClsSectionId = R.TestId
                    INNER JOIN syCreditSummary CSS ON CSS.StuEnrollId = R.StuEnrollId
                                                      AND CSS.ReqId = CS.ReqId
                    INNER JOIN #tmpEquivalentCoursesTakenByStudentInTerm AS ECS ON ECS.StuEnrollId = R.StuEnrollId
                                                                                   AND ECS.EquivReqId = CS.ReqId
                    WHERE  R.GrdSysDetailId IS NOT NULL
                           AND (
                               (
                               CSS.FinalGPA IS NOT NULL
                               AND @GradesFormat = 'letter'
                               )
                               OR (
                                  CSS.FinalScore IS NOT NULL
                                  AND @GradesFormat = 'numeric'
                                  )
                               )
                    UNION
                    SELECT DISTINCT ECS.EquivReqId
                          ,TR.StuEnrollId
                          ,TR.TermId
                    FROM   dbo.arTransferGrades TR
                    INNER JOIN dbo.arClassSections CS ON CS.ClsSectionId = TR.ReqId
                    INNER JOIN syCreditSummary CSS ON CSS.StuEnrollId = TR.StuEnrollId
                                                      AND CSS.ReqId = CS.ReqId
                    INNER JOIN #tmpEquivalentCoursesTakenByStudentInTerm ECS ON TR.StuEnrollId = ECS.StuEnrollId
                                                                                AND ECS.EquivReqId = CS.ReqId
                    WHERE  TR.GrdSysDetailId IS NOT NULL
                           AND (
                               (
                               CSS.FinalGPA IS NOT NULL
                               AND @GradesFormat = 'letter'
                               )
                               OR (
                                  CSS.FinalScore IS NOT NULL
                                  AND @GradesFormat = 'numeric'
                                  )
                               );


        --- ******************* Equivalent Courses Temp Table Ends Here *************************

        INSERT INTO #tmpCoursesNotRepeatedOnlyIds (
                                                  StuEnrollId
                                                 ,TermId
                                                  )
                    SELECT DISTINCT CNR.StuEnrollId
                          ,CNR.TermId
                    FROM   #CoursesNotRepeated AS CNR;

        IF @GradesFormat = 'letter'
            BEGIN
                UPDATE GST
                SET    GST.TermSimpleCourses = T.TermSimpleCourses
                      ,GST.TermSimple_GPA_Credits = T.TermSimple_GPA_Credits
                      ,GST.TermCourseCredits = T.TermCourseCredits
                      ,GST.TermWeighted_GPA_Credits = T.TermWeighted_GPA_Credits
                FROM   #getStudentTerms AS GST
                INNER JOIN (
                           SELECT   CNR.StuEnrollId
                                   ,CNR.TermId
                                   ,COUNT(ISNULL(CNR.coursecredits, 0)) AS TermSimpleCourses
                                   ,SUM(ISNULL(CNR.FinalGPA, 0.0)) AS TermSimple_GPA_Credits
                                   ,SUM(ISNULL(CNR.coursecredits, 0.0)) AS TermCourseCredits
                                   ,SUM(ISNULL(CNR.coursecredits * CNR.FinalGPA, 0.0)) AS TermWeighted_GPA_Credits
                           FROM     #CoursesNotRepeated AS CNR
                           WHERE    CNR.FinalGPA IS NOT NULL
                           GROUP BY CNR.StuEnrollId
                                   ,CNR.TermId
                           ) AS T ON T.StuEnrollId = GST.StuEnrollId
                                     AND T.TermId = GST.TermId;
                UPDATE GST
                SET    GST.TermCredits = T.TermCredits
                FROM   #getStudentTerms AS GST
                INNER JOIN (
                           SELECT   CNR.StuEnrollId
                                   ,CNR.TermId
                                   ,SUM(ISNULL(CNR.CreditsEarned, 0)) AS TermCredits
                           FROM     #CoursesNotRepeated AS CNR
                           --WHERE   CNR.FinalGPA IS NOT NULL    --AND FinalGPA IS NOT NULL 'Commented by Balaji on 2/24 as this variable just reports credits earned
                           GROUP BY CNR.StuEnrollId
                                   ,CNR.TermId
                           ) AS T ON T.StuEnrollId = GST.StuEnrollId
                                     AND T.TermId = GST.TermId;
                UPDATE GST
                SET    GST.TermEquivCourse_SA_CC = T.TermEquivCourse_SA_CC
                FROM   #getStudentTerms AS GST
                INNER JOIN (
                           SELECT   CE.StuEnrollId
                                   ,CE.TermId
                                   ,COUNT(ISNULL(AR.Credits, 0)) AS TermEquivCourse_SA_CC
                           FROM     arReqs AS AR
                           INNER JOIN #tmpStudentsWhoHasGradedEquivalentCourseInTerm AS CE ON CE.EquivReqId = AR.ReqId
                           INNER JOIN #tmpCoursesNotRepeatedOnlyIds AS TCN ON TCN.StuEnrollId = CE.StuEnrollId
                                                                              AND TCN.TermId = CE.TermId
                           GROUP BY CE.StuEnrollId
                                   ,CE.TermId
                           ) AS T ON T.StuEnrollId = GST.StuEnrollId
                                     AND T.TermId = GST.TermId;
                UPDATE GST
                SET    GST.TermEquivCourse_SA_GPA = T.TermEquivCourse_SA_GPA
                FROM   #getStudentTerms AS GST
                INNER JOIN (
                           SELECT   CE.StuEnrollId
                                   ,CE.TermId
                                   ,SUM(ISNULL(AGSD.GPA, 0.00)) AS TermEquivCourse_SA_GPA
                           FROM     #tmpStudentsWhoHasGradedEquivalentCourseInTerm AS CE
                           INNER JOIN arResults AS AR ON AR.StuEnrollId = CE.StuEnrollId
                           INNER JOIN arGradeSystemDetails AS AGSD ON AGSD.GrdSysDetailId = AR.GrdSysDetailId
                           INNER JOIN syCreditSummary AS SCS ON SCS.StuEnrollId = AR.StuEnrollId
                                                                AND SCS.ReqId = CE.EquivReqId
                           INNER JOIN #tmpCoursesNotRepeatedOnlyIds AS TCN ON TCN.StuEnrollId = CE.StuEnrollId
                                                                              AND TCN.TermId = CE.TermId
                           GROUP BY CE.StuEnrollId
                                   ,CE.TermId
                           ) AS T ON T.StuEnrollId = GST.StuEnrollId
                                     AND T.TermId = GST.TermId;
                UPDATE GST
                SET    GST.TermSimpleGPA = CASE WHEN ( ISNULL(GST.TermSimpleCourses, 0) + ISNULL(GST.TermEquivCourse_SA_CC, 0) > 0 ) THEN
                    ( ISNULL(GST.TermSimple_GPA_Credits, 0.0) + ISNULL(GST.TermEquivCourse_SA_GPA, 0.0))
                    / ( ISNULL(GST.TermSimpleCourses, 0) + ISNULL(GST.TermEquivCourse_SA_CC, 0))
                                                ELSE 0.0
                                           END
                      ,GST.TermWeightedGPA = CASE WHEN ( ISNULL(GST.TermCourseCredits, 0.0) + ISNULL(GST.TermEquivCourse_WGPA_CC1, 0.0) > 0.00 ) THEN
                      ( ISNULL(GST.TermWeighted_GPA_Credits, 0.0) + ISNULL(GST.TermEquivCourse_SA_GPA, 0.0))
                      / ( ISNULL(GST.TermCourseCredits, 0.0) + ISNULL(GST.TermEquivCourse_WGPA_CC1, 0.0))
                                                  ELSE 0.0
                                             END
                      ,GST.TermAverage = CASE WHEN ( ISNULL(GST.TermAverageCount, 0) > 0 ) THEN
                      ( ISNULL(GST.TermAverageSum, 0.0) / ( ISNULL(GST.TermAverageCount, 0)))
                                              ELSE 0.0
                                         END
                FROM   #getStudentTerms AS GST;
            END;
        ELSE
            BEGIN
                UPDATE GST
                SET    GST.TermAverageCount = T.TermAverageCount
                      ,GST.TermAverageSum = T.TermAverageSum
                FROM   #getStudentTerms AS GST
                INNER JOIN (
                           SELECT   CNR.StuEnrollId
                                   ,CNR.TermId
                                   ,COUNT(ISNULL(CNR.coursecredits, 0)) AS TermAverageCount
                                   ,SUM(ISNULL(CNR.FinalScore, 0.0)) AS TermAverageSum
                           FROM     #CoursesNotRepeated AS CNR
                           WHERE    CNR.FinalScore IS NOT NULL
                           GROUP BY CNR.StuEnrollId
                                   ,CNR.TermId
                           ) AS T ON T.StuEnrollId = GST.StuEnrollId
                                     AND T.TermId = GST.TermId;
                UPDATE GST
                SET    GST.TermCredits = T.TermCredits
                FROM   #getStudentTerms AS GST
                INNER JOIN (
                           SELECT   CNR.StuEnrollId
                                   ,CNR.TermId
                                   ,SUM(ISNULL(CNR.CreditsEarned, 0)) AS TermCredits
                           FROM     #CoursesNotRepeated AS CNR
                           --WHERE   CNR.FinalGPA IS NOT NULL    --AND FinalGPA IS NOT NULL 'Commented by Balaji on 2/24 as this variable just reports credits earned
                           GROUP BY CNR.StuEnrollId
                                   ,CNR.TermId
                           ) AS T ON T.StuEnrollId = GST.StuEnrollId
                                     AND T.TermId = GST.TermId;
                UPDATE GST
                SET    GST.TermAverage = CASE WHEN ( ISNULL(GST.TermAverageCount, 0) > 0 ) THEN
                    ( ISNULL(GST.TermAverageSum, 0.0) / ( ISNULL(GST.TermAverageCount, 0)))
                                              ELSE 0.0
                                         END
                FROM   #getStudentTerms AS GST;
            END;

        INSERT INTO #tmpEquivalentCoursesTakenByStudent
                    SELECT dt.EquivReqId
                          ,dt.StuEnrollId
                    FROM -- Get the list of Equivalent courses taken by student enrollment
                           (
                           SELECT DISTINCT CE.EquivReqId
                                 ,R.StuEnrollId
                           FROM   arResults R
                           INNER JOIN dbo.arClassSections CS ON CS.ClsSectionId = R.TestId
                           INNER JOIN syCreditSummary CSS ON CSS.StuEnrollId = R.StuEnrollId
                                                             AND CSS.ReqId = CS.ReqId
                           INNER JOIN arReqs R1 ON R1.ReqId = CS.ReqId
                           INNER JOIN arCourseEquivalent CE ON R1.ReqId = CE.ReqId
                           INNER JOIN #tmpStudentEnrollmentsOnlyIds AS GSE ON GSE.StuEnrollId = R.StuEnrollId
                           WHERE  R.GrdSysDetailId IS NOT NULL
                                  AND (
                                      (
                                      CSS.FinalGPA IS NOT NULL
                                      AND @GradesFormat = 'letter'
                                      )
                                      OR (
                                         CSS.FinalScore IS NOT NULL
                                         AND @GradesFormat = 'numeric'
                                         )
                                      )
                           UNION
                           SELECT DISTINCT CE.EquivReqId
                                 ,TR.StuEnrollId
                           FROM   dbo.arTransferGrades TR
                           INNER JOIN syCreditSummary CSS ON CSS.StuEnrollId = TR.StuEnrollId
                                                             AND CSS.ReqId = TR.ReqId
                           INNER JOIN arReqs R1 ON R1.ReqId = TR.ReqId
                           INNER JOIN arCourseEquivalent CE ON R1.ReqId = CE.ReqId
                           INNER JOIN #tmpStudentEnrollmentsOnlyIds AS GSE ON GSE.StuEnrollId = TR.StuEnrollId
                           WHERE  TR.GrdSysDetailId IS NOT NULL
                                  AND (
                                      (
                                      CSS.FinalGPA IS NOT NULL
                                      AND @GradesFormat = 'letter'
                                      )
                                      OR (
                                         CSS.FinalScore IS NOT NULL
                                         AND @GradesFormat = 'numeric'
                                         )
                                      )
                           ) dt;
        -- Check if student has got a grade on any of the Equivalent courses
        INSERT INTO #tmpStudentsWhoHasGradedEquivalentCourse
                    SELECT DISTINCT ECS.EquivReqId
                          ,R.StuEnrollId
                    FROM   arResults R
                    INNER JOIN dbo.arClassSections CS ON CS.ClsSectionId = R.TestId
                    INNER JOIN syCreditSummary CSS ON CSS.StuEnrollId = R.StuEnrollId
                                                      AND CSS.ReqId = CS.ReqId
                    INNER JOIN #tmpEquivalentCoursesTakenByStudent ECS ON R.StuEnrollId = ECS.StuEnrollId
                                                                          AND ECS.EquivReqId = CS.ReqId
                    WHERE  R.GrdSysDetailId IS NOT NULL
                           AND (
                               (
                               CSS.FinalGPA IS NOT NULL
                               AND @GradesFormat = 'letter'
                               )
                               OR (
                                  CSS.FinalScore IS NOT NULL
                                  AND @GradesFormat = 'numeric'
                                  )
                               )
                    UNION
                    SELECT DISTINCT ECS.EquivReqId
                          ,TR.StuEnrollId
                    FROM   dbo.arTransferGrades TR
                    INNER JOIN dbo.arClassSections CS ON CS.ClsSectionId = TR.ReqId
                    INNER JOIN syCreditSummary CSS ON CSS.StuEnrollId = TR.StuEnrollId
                                                      AND CSS.ReqId = CS.ReqId
                    INNER JOIN #tmpEquivalentCoursesTakenByStudent ECS ON TR.StuEnrollId = ECS.StuEnrollId
                                                                          AND ECS.EquivReqId = CS.ReqId
                    WHERE  TR.GrdSysDetailId IS NOT NULL
                           AND (
                               (
                               CSS.FinalGPA IS NOT NULL
                               AND @GradesFormat = 'letter'
                               )
                               OR (
                                  CSS.FinalScore IS NOT NULL
                                  AND @GradesFormat = 'numeric'
                                  )
                               );


        IF @GradesFormat = 'letter'
            BEGIN
                UPDATE GST
                SET    GST.PrgVers_SimpleCourseCredits = T.PrgVers_SimpleCourseCredits
                      ,GST.PrgVers_Simple_GPA_Credits = T.PrgVers_Simple_GPA_Credits
                      ,GST.PrgVers_CourseCredits = T.PrgVers_CourseCredits
                      ,GST.PrgVers_Weighted_GPA_Credits = T.PrgVers_Weighted_GPA_Credits
                FROM   #getStudentEnrollments AS GST
                INNER JOIN (
                           SELECT   CNR.StuEnrollId
                                   ,COUNT(ISNULL(CNR.coursecredits, 0)) AS PrgVers_SimpleCourseCredits
                                   ,SUM(ISNULL(CNR.FinalGPA, 0.0)) AS PrgVers_Simple_GPA_Credits
                                   ,SUM(ISNULL(CNR.coursecredits, 0.0)) AS PrgVers_CourseCredits
                                   ,SUM(ISNULL(CNR.coursecredits, 0.0) * ISNULL(CNR.FinalGPA, 0.0)) AS PrgVers_Weighted_GPA_Credits
                           FROM     #CoursesNotRepeated AS CNR
                           WHERE    CNR.FinalGPA IS NOT NULL
                           GROUP BY CNR.StuEnrollId
                           ) AS T ON T.StuEnrollId = GST.StuEnrollId;
                UPDATE GST
                SET    GST.OverAll_SimpleCourses = T.OverAll_SimpleCourses
                      ,GST.OverAll_Simple_GPA_Credits = T.OverAll_Simple_GPA_Credits
                      ,GST.OverAll_Weighted_GPA_Credits = T.OverAll_Weighted_GPA_Credits
                      ,GST.OverAll_CourseCredits = T.OverAll_CourseCredits
                FROM   #getStudentEnrollments AS GST
                INNER JOIN (
                           SELECT TSE.StuEnrollId
                                 ,ST.OverAll_SimpleCourses
                                 ,ST.OverAll_Simple_GPA_Credits
                                 ,ST.OverAll_Weighted_GPA_Credits
                                 ,ST.OverAll_CourseCredits
                           FROM   #tmpStudentEnrollmentsOnlyIds AS TSE
                           LEFT JOIN (
                                     SELECT   CNR.StudentId
                                             ,COUNT(ISNULL(CNR.coursecredits, 0)) AS OverAll_SimpleCourses
                                             ,SUM(ISNULL(CNR.FinalGPA, 0.0)) AS OverAll_Simple_GPA_Credits
                                             ,SUM(ISNULL(CNR.coursecredits, 0.0)) AS OverAll_CourseCredits
                                             ,SUM(ISNULL(CNR.coursecredits, 0.0) * ISNULL(CNR.FinalGPA, 0.0)) AS OverAll_Weighted_GPA_Credits
                                     FROM     #CoursesNotRepeated AS CNR
                                     WHERE    CNR.FinalGPA IS NOT NULL
                                     GROUP BY CNR.StudentId
                                     ) AS ST ON ST.StudentId = TSE.StudentId
                           ) AS T ON T.StuEnrollId = GST.StuEnrollId;
                UPDATE GST
                SET    GST.PrgVers_EquivCourses_SA_CC = T.PrgVers_EquivCourses_SA_CC
                      ,GST.PrgVers_EquivCourse_WGPA_CC1 = T.PrgVers_EquivCourse_WGPA_CC1
                FROM   #getStudentEnrollments AS GST
                INNER JOIN (
                           SELECT   CE.StuEnrollId
                                   ,COUNT(ISNULL(AR.Credits, 0)) AS PrgVers_EquivCourses_SA_CC
                                   ,SUM(ISNULL(AR.Credits, 0.0)) AS PrgVers_EquivCourse_WGPA_CC1
                           FROM     arReqs AS AR
                           INNER JOIN #tmpStudentsWhoHasGradedEquivalentCourse AS CE ON CE.EquivReqId = AR.ReqId
                           INNER JOIN #tmpStudentEnrollmentsOnlyIds AS TSE ON TSE.StuEnrollId = CE.StuEnrollId
                           GROUP BY CE.StuEnrollId
                           ) AS T ON T.StuEnrollId = GST.StuEnrollId;
                UPDATE GST
                SET    GST.OverAll_EquivCourses_SA_CC = T.OverAll_EquivCourses_SA_CC
                      ,GST.OverAll_EquivCourse_WGPA_CC1 = T.OverAll_EquivCourse_WGPA_CC1
                FROM   #getStudentEnrollments AS GST
                INNER JOIN (
                           SELECT TSE.StuEnrollId
                                 ,ST.OverAll_EquivCourses_SA_CC
                                 ,ST.OverAll_EquivCourse_WGPA_CC1
                           FROM   #tmpStudentEnrollmentsOnlyIds AS TSE
                           LEFT JOIN (
                                     SELECT   TSE1.StudentId
                                             ,COUNT(ISNULL(AR.Credits, 0)) AS OverAll_EquivCourses_SA_CC
                                             ,SUM(ISNULL(Credits, 0)) AS OverAll_EquivCourse_WGPA_CC1
                                     FROM     arReqs AS AR
                                     INNER JOIN #tmpStudentsWhoHasGradedEquivalentCourse AS CE ON CE.EquivReqId = AR.ReqId
                                     INNER JOIN #tmpStudentEnrollmentsOnlyIds AS TSE1 ON TSE1.StuEnrollId = CE.StuEnrollId
                                     GROUP BY TSE1.StudentId
                                     ) AS ST ON ST.StudentId = TSE.StudentId
                           ) AS T ON T.StuEnrollId = GST.StuEnrollId;
                UPDATE GST
                SET    GST.PrgVers_EquivCourse_SA_GPA = T.PrgVers_EquivCourse_SA_GPA
                FROM   #getStudentEnrollments AS GST
                INNER JOIN (
                           SELECT   CE.StuEnrollId
                                   ,SUM(ISNULL(AGSD.GPA, 0.00)) AS PrgVers_EquivCourse_SA_GPA
                           FROM     arGradeSystemDetails AS AGSD
                           INNER JOIN arResults AS AR ON AR.GrdSysDetailId = AGSD.GrdSysDetailId
                           INNER JOIN #tmpStudentsWhoHasGradedEquivalentCourse AS CE ON CE.StuEnrollId = AR.StuEnrollId
                           INNER JOIN syCreditSummary AS SCS ON SCS.StuEnrollId = AR.StuEnrollId
                                                                AND SCS.ReqId = CE.EquivReqId
                           INNER JOIN #tmpStudentEnrollmentsOnlyIds AS TSE ON TSE.StuEnrollId = CE.StuEnrollId
                           GROUP BY CE.StuEnrollId
                           ) AS T ON T.StuEnrollId = GST.StuEnrollId;
                UPDATE GST
                SET    GST.OverAll_EquivCourse_SA_GPA = T.OverAll_EquivCourse_SA_GPA
                FROM   #getStudentEnrollments AS GST
                INNER JOIN (
                           SELECT TSE.StuEnrollId
                                 ,ST.OverAll_EquivCourse_SA_GPA
                           FROM   #tmpStudentEnrollmentsOnlyIds AS TSE
                           LEFT JOIN (
                                     SELECT   TSE1.StudentId
                                             ,SUM(ISNULL(AGSD.GPA, 0.00)) AS OverAll_EquivCourse_SA_GPA
                                     FROM     arGradeSystemDetails AS AGSD
                                     INNER JOIN arResults AS AR ON AR.GrdSysDetailId = AGSD.GrdSysDetailId
                                     INNER JOIN #tmpStudentsWhoHasGradedEquivalentCourse AS CE ON CE.StuEnrollId = AR.StuEnrollId
                                     INNER JOIN syCreditSummary AS SCS ON SCS.StuEnrollId = AR.StuEnrollId
                                                                          AND SCS.ReqId = CE.EquivReqId
                                     INNER JOIN #tmpStudentEnrollmentsOnlyIds AS TSE1 ON TSE1.StuEnrollId = CE.StuEnrollId
                                     GROUP BY TSE1.StudentId
                                     ) AS ST ON ST.StudentId = TSE.StudentId
                           ) AS T ON T.StuEnrollId = GST.StuEnrollId;
                UPDATE GST
                SET    GST.PrgVers_EquivCourse_WGPA_GPA1 = T.PrgVers_EquivCourse_WGPA_GPA1
                FROM   #getStudentEnrollments AS GST
                INNER JOIN (
                           SELECT   CE.StuEnrollId
                                   ,SUM(ISNULL(AGSD.GPA, 0.0) * ISNULL(AR2.Credits, 0.0)) AS PrgVers_EquivCourse_WGPA_GPA1
                           FROM     #tmpStudentsWhoHasGradedEquivalentCourse AS CE
                           INNER JOIN arResults AS AR ON AR.StuEnrollId = CE.StuEnrollId
                           INNER JOIN arGradeSystemDetails AS AGSD ON AGSD.GrdSysDetailId = AR.GrdSysDetailId
                           INNER JOIN syCreditSummary AS SCS ON SCS.StuEnrollId = AR.StuEnrollId
                                                                AND SCS.ReqId = CE.EquivReqId
                           INNER JOIN arReqs AS AR2 ON AR2.ReqId = CE.EquivReqId
                           INNER JOIN #tmpStudentEnrollmentsOnlyIds AS TSE ON TSE.StuEnrollId = CE.StuEnrollId
                           GROUP BY CE.StuEnrollId
                           ) AS T ON T.StuEnrollId = GST.StuEnrollId;
                UPDATE GST
                SET    GST.OverAll_EquivCourse_WGPA_GPA1 = T.OverAll_EquivCourse_WGPA_GPA1
                FROM   #getStudentEnrollments AS GST
                INNER JOIN (
                           SELECT TSE.StuEnrollId
                                 ,ST.OverAll_EquivCourse_WGPA_GPA1
                           FROM   #tmpStudentEnrollmentsOnlyIds AS TSE
                           LEFT JOIN (
                                     SELECT   TSE1.StudentId
                                             ,SUM(ISNULL(AGSD.GPA, 0.0) * ISNULL(AR2.Credits, 0.0)) AS OverAll_EquivCourse_WGPA_GPA1
                                     FROM     arGradeSystemDetails AS AGSD
                                     INNER JOIN arResults AS AR ON AR.GrdSysDetailId = AGSD.GrdSysDetailId
                                     INNER JOIN #tmpStudentsWhoHasGradedEquivalentCourse AS CE ON CE.StuEnrollId = AR.StuEnrollId
                                     INNER JOIN syCreditSummary AS SCS ON SCS.StuEnrollId = AR.StuEnrollId
                                                                          AND SCS.ReqId = CE.EquivReqId
                                     INNER JOIN arReqs AR2 ON AR2.ReqId = CE.EquivReqId
                                     INNER JOIN #tmpStudentEnrollmentsOnlyIds AS TSE1 ON TSE1.StuEnrollId = CE.StuEnrollId
                                     GROUP BY TSE1.StudentId
                                     ) AS ST ON ST.StudentId = TSE.StudentId
                           ) AS T ON T.StuEnrollId = GST.StuEnrollId;
                UPDATE GST
                SET    GST.PrgVers_SimpleGPA = CASE WHEN ( ISNULL(GST.PrgVers_SimpleCourseCredits, 0) + ISNULL(GST.PrgVers_EquivCourses_SA_CC, 0) > 0 ) THEN
                    ( ISNULL(GST.PrgVers_Simple_GPA_Credits, 0.0) + ISNULL(GST.PrgVers_EquivCourse_SA_GPA, 0.0))
                    / ( ISNULL(GST.PrgVers_SimpleCourseCredits, 0) + ISNULL(GST.PrgVers_EquivCourses_SA_CC, 0))
                                                    ELSE 0.0
                                               END
                      ,GST.PrgVers_WeightedGPA = CASE WHEN ( ISNULL(GST.PrgVers_CourseCredits, 0) + ISNULL(GST.PrgVers_EquivCourse_WGPA_CC1, 0) > 0.00 ) THEN
                      ( ISNULL(GST.PrgVers_Weighted_GPA_Credits, 0.0) + ISNULL(GST.PrgVers_EquivCourse_WGPA_GPA1, 0.0))
                      / ( ISNULL(GST.PrgVers_CourseCredits, 0) + ISNULL(GST.PrgVers_EquivCourse_WGPA_CC1, 0))
                                                      ELSE 0.0
                                                 END
                      ,GST.OverAll_SimpleGPA = CASE WHEN ( ISNULL(GST.OverAll_SimpleCourses, 0) + ISNULL(GST.OverAll_EquivCourse_SA_GPA, 0) > 0 ) THEN
                      ( ISNULL(GST.OverAll_Simple_GPA_Credits, 0.0) + ISNULL(GST.OverAll_EquivCourse_SA_GPA, 0.0))
                      / ( ISNULL(GST.OverAll_SimpleCourses, 0) + ISNULL(GST.OverAll_EquivCourse_SA_GPA, 0))
                                                    ELSE 0.0
                                               END
                      ,GST.OverAll_WeightedGPA = CASE WHEN ( ISNULL(GST.OverAll_CourseCredits, 0) + ISNULL(GST.OverAll_EquivCourse_WGPA_CC1, 0) > 0.00 ) THEN
                      ( ISNULL(GST.OverAll_Weighted_GPA_Credits, 0.0) + ISNULL(GST.OverAll_EquivCourse_WGPA_GPA1, 0.0))
                      / ( ISNULL(GST.OverAll_CourseCredits, 0) + ISNULL(GST.OverAll_EquivCourse_WGPA_CC1, 0))
                                                      ELSE 0.0
                                                 END
                FROM   #getStudentEnrollments AS GST;
            END;
        ELSE
            BEGIN
                UPDATE GST
                SET    GST.PrgVers_AverageCount = T.PrgVers_AverageCount
                      ,GST.PrgVers_AverageSum = T.PrgVers_AverageSum
                FROM   #getStudentEnrollments AS GST
                INNER JOIN (
                           SELECT   CNR.StuEnrollId
                                   ,COUNT(ISNULL(CNR.coursecredits, 0)) AS PrgVers_AverageCount
                                   ,SUM(ISNULL(CNR.FinalScore, 0.0)) AS PrgVers_AverageSum
                           FROM     #CoursesNotRepeated AS CNR
                           WHERE    CNR.FinalScore IS NOT NULL
                           GROUP BY CNR.StuEnrollId
                           ) AS T ON T.StuEnrollId = GST.StuEnrollId;
                UPDATE GST
                SET    GST.OverAll_AverageCount = T.OverAll_AverageCount
                      ,GST.OverAll_AverageSum = T.OverAll_AverageSum
                FROM   #getStudentEnrollments AS GST
                INNER JOIN (
                           SELECT TSE.StuEnrollId
                                 ,ST.OverAll_AverageCount
                                 ,ST.OverAll_AverageSum
                           FROM   #tmpStudentEnrollmentsOnlyIds AS TSE
                           LEFT JOIN (
                                     SELECT   CNR.StudentId
                                             ,COUNT(ISNULL(CNR.coursecredits, 0)) AS OverAll_AverageCount
                                             ,SUM(ISNULL(CNR.FinalScore, 0.0)) AS OverAll_AverageSum
                                     FROM     #CoursesNotRepeated AS CNR
                                     WHERE    CNR.FinalScore IS NOT NULL
                                     GROUP BY CNR.StudentId
                                     ) AS ST ON ST.StudentId = TSE.StudentId
                           ) AS T ON T.StuEnrollId = GST.StuEnrollId;
                UPDATE GST
                SET    GST.PrgVers_Average = CASE WHEN ( ISNULL(GST.PrgVers_AverageCount, 0) > 0 ) THEN
                    ( ISNULL(GST.PrgVers_AverageSum, 0.0) / ( ISNULL(GST.PrgVers_AverageCount, 0)))
                                                  ELSE 0.0
                                             END
                      ,GST.OverAll_Average = CASE WHEN ( ISNULL(GST.OverAll_AverageCount, 0) > 0 ) THEN
                      ( ISNULL(GST.OverAll_AverageSum, 0.0) / ( ISNULL(GST.OverAll_AverageCount, 0)))
                                                  ELSE 0.0
                                             END
                FROM   #getStudentEnrollments AS GST;
            END;


        SELECT   DT.LastName
                ,DT.FirstName
                ,DT.StudentId
                ,DT.StudentNumber
                ,DT.StuEnrollId
                ,DT.PrgVerId
                ,DT.ProgramVersion
                ,DT.PrgVersionTrackCredits
                ,DT.PrgVers_SimpleGPA
                ,DT.PrgVers_WeightedGPA
                ,DT.PrgVers_Average
                ,DT.OverAll_SimpleGPA
                ,DT.OverAll_WeightedGPA
                ,DT.OverAll_Average
                ,DT.SSN
                ,DT.EnrollmentID
                ,DT.MiddleName
                ,DT.TermId
                ,DT.TermDescription
                ,DT.TermStartDate
                ,DT.rowNumber
                ,DT.TermDescrip
                ,DT.TermCredits
                ,DT.TermSimpleGPA
                ,DT.TermWeightedGPA
                ,DT.TermAverage
                ,DT.ProgramCredits
                ,@GPAMethod AS GPAMethod
                ,@GradesFormat AS GradesFormat
        FROM     (
                 SELECT DISTINCT GSE.LastName
                       ,GSE.FirstName
                       ,GSE.StudentId
                       ,GSE.StudentNumber
                       ,GSE.StuEnrollId
                       ,GSE.PrgVerId
                       ,GSE.ProgramVersion
                       ,GSE.PrgVersionTrackCredits
                       ,GSE.PrgVers_SimpleGPA
                       ,GSE.PrgVers_WeightedGPA
                       ,GSE.PrgVers_Average
                       ,GSE.OverAll_SimpleGPA
                       ,GSE.OverAll_WeightedGPA
                       ,GSE.OverAll_Average
                       ,GSE.SSN
                       ,GSE.EnrollmentID
                       ,GSE.MiddleName
                       ,GSE.TermId
                       ,GSE.TermDescription
                       ,GSE.TermStartDate
                       ,GSE.rowNumber
                       ,GST.TermDescrip
                       ,GST.TermCredits
                       ,GST.TermSimpleGPA
                       ,GST.TermWeightedGPA
                       ,GST.TermAverage
                       ,(
                        SELECT SUM(ISNULL(TermCredits, 0))
                        FROM   #getStudentTerms
                        WHERE  StuEnrollId = GSE.StuEnrollId
                        ) AS ProgramCredits
                       ,ROW_NUMBER() OVER ( PARTITION BY GSE.StuEnrollId
                                                        ,GSE.PrgVerId
                                            ORDER BY GSE.TermStartDate DESC
                                          ) AS RowNumber2
                 FROM   #getStudentEnrollments GSE
                 INNER JOIN #getStudentTerms GST ON GSE.StuEnrollId = GST.StuEnrollId
                                                    AND GSE.TermId = GST.TermId
                 INNER JOIN #TermIdList AS TIL ON TIL.TermId = GST.TermId
                 WHERE  CASE WHEN @GradesFormat = 'letter' THEN CASE WHEN (
                                                                          (
                                                                          ( @GPAMethod = 'WeightedAVG' )
                                                                          AND (
                                                                              @CumulativeGPAGT IS NULL
                                                                              OR PrgVers_WeightedGPA >= @CumulativeGPAGT
                                                                              )
                                                                          AND (
                                                                              @CumulativeGPALT IS NULL
                                                                              OR PrgVers_WeightedGPA <= @CumulativeGPALT
                                                                              )
                                                                          AND (
                                                                              @termGPAGT IS NULL
                                                                              OR GST.TermWeightedGPA >= @termGPAGT
                                                                              )
                                                                          AND (
                                                                              @termGPALT IS NULL
                                                                              OR GST.TermWeightedGPA <= @termGPALT
                                                                              )
                                                                          )
                                                                          OR (
                                                                             ( @GPAMethod != 'WeightedAVG' )
                                                                             AND (
                                                                                 @CumulativeGPAGT IS NULL
                                                                                 OR PrgVers_SimpleGPA >= @CumulativeGPAGT
                                                                                 )
                                                                             AND (
                                                                                 @CumulativeGPALT IS NULL
                                                                                 OR PrgVers_SimpleGPA <= @CumulativeGPALT
                                                                                 )
                                                                             AND (
                                                                                 @termGPAGT IS NULL
                                                                                 OR GST.TermSimpleGPA >= @termGPAGT
                                                                                 )
                                                                             AND (
                                                                                 @termGPALT IS NULL
                                                                                 OR GST.TermSimpleGPA <= @termGPALT
                                                                                 )
                                                                             )
                                                                          )
                                                                          AND (
                                                                              @termCreditsRangeGT IS NULL
                                                                              OR GST.TermCredits >= @termCreditsRangeGT
                                                                              )
                                                                          AND (
                                                                              @termCreditsRangeLT IS NULL
                                                                              OR GST.TermCredits <= @termCreditsRangeLT
                                                                              ) THEN 1
                                                                     ELSE 0
                                                                END
                             WHEN @GradesFormat = 'numeric' THEN CASE WHEN (
                                                                           @CumulativeGPAGT IS NULL
                                                                           OR GSE.PrgVers_Average >= @CumulativeGPAGT
                                                                           )
                                                                           AND (
                                                                               @CumulativeGPALT IS NULL
                                                                               OR GSE.PrgVers_Average <= @CumulativeGPALT
                                                                               )
                                                                           AND (
                                                                               @termGPAGT IS NULL
                                                                               OR GST.TermAverage >= @termGPAGT
                                                                               )
                                                                           AND (
                                                                               @termGPALT IS NULL
                                                                               OR GST.TermAverage <= @termGPALT
                                                                               )
                                                                           AND (
                                                                               @termCreditsRangeGT IS NULL
                                                                               OR GST.TermCredits >= @termCreditsRangeGT
                                                                               )
                                                                           AND (
                                                                               @termCreditsRangeLT IS NULL
                                                                               OR GST.TermCredits <= @termCreditsRangeLT
                                                                               ) THEN 1
                                                                      ELSE 0
                                                                 END
                        END = 1
                 ) AS DT
        WHERE    (
                 (
                 (
                 @pTermId IS NULL
                 OR @pTermId = ''
                 )
                 AND DT.RowNumber2 = 1
                 )
                 OR (
                    (
                    @pTermId IS NOT NULL
                    AND @pTermId <> ''
                    )
                    AND DT.RowNumber2 = 1
                    )
                 )
        ORDER BY CASE WHEN (
                           @pTermId IS NULL
                           OR @pTermId = ''
                           ) THEN ( RANK() OVER ( ORDER BY DT.LastName ASC
                                                          ,DT.FirstName ASC
                                                          ,DT.TermStartDate DESC
                                                )
                                  )
                      ELSE ( RANK() OVER ( ORDER BY DT.LastName ASC
                                                   ,DT.FirstName ASC
                                                   ,DT.TermStartDate ASC
                                         )
                           )
                 END;

        IF EXISTS (
                  SELECT 1
                  FROM   tempdb.dbo.sysobjects AS O
                  WHERE  O.xtype IN ( 'U' )
                         AND O.id = OBJECT_ID(N'tempdb..#PrgVersionsList')
                  )
            BEGIN
                DROP TABLE #PrgVersionsList;
            END;
        IF EXISTS (
                  SELECT 1
                  FROM   tempdb.dbo.sysobjects AS O
                  WHERE  O.xtype IN ( 'U' )
                         AND O.id = OBJECT_ID(N'tempdb..#EnrollmentStatusList')
                  )
            BEGIN
                DROP TABLE #EnrollmentStatusList;
            END;
        IF EXISTS (
                  SELECT 1
                  FROM   tempdb.dbo.sysobjects AS O
                  WHERE  O.xtype IN ( 'U' )
                         AND O.id = OBJECT_ID(N'tempdb..#LeadGroupsList')
                  )
            BEGIN
                DROP TABLE #LeadGroupsList;
            END;
        IF EXISTS (
                  SELECT 1
                  FROM   tempdb.dbo.sysobjects AS O
                  WHERE  O.xtype IN ( 'U' )
                         AND O.id = OBJECT_ID(N'tempdb..#TermIdList')
                  )
            BEGIN
                DROP TABLE #TermIdList;
            END;
        IF EXISTS (
                  SELECT 1
                  FROM   tempdb.dbo.sysobjects AS O
                  WHERE  O.xtype IN ( 'U' )
                         AND O.id = OBJECT_ID(N'tempdb..#CoursesNotRepeated')
                  )
            BEGIN
                DROP TABLE #CoursesNotRepeated;
            END;
        IF EXISTS (
                  SELECT 1
                  FROM   tempdb.dbo.sysobjects AS O
                  WHERE  O.xtype IN ( 'U' )
                         AND O.id = OBJECT_ID(N'tempdb..#tmpCoursesNotRepeatedOnlyIds')
                  )
            BEGIN
                -- ALTER TABLE tempdb..#tmpCoursesNotRepeatedOnlyIds 
                --     DROP CONSTRAINT PK_#tmpCoursesNotRepeatedOnlyIds 
                DROP TABLE #tmpCoursesNotRepeatedOnlyIds;
            END;
        IF EXISTS (
                  SELECT 1
                  FROM   tempdb.dbo.sysobjects AS O
                  WHERE  O.xtype IN ( 'U' )
                         AND O.id = OBJECT_ID(N'tempdb..#getStudentTerms')
                  )
            BEGIN
                DROP TABLE #getStudentTerms;
            END;
        IF EXISTS (
                  SELECT 1
                  FROM   tempdb.dbo.sysobjects AS O
                  WHERE  O.xtype IN ( 'U' )
                         AND O.id = OBJECT_ID(N'tempdb..#getStudentEnrollments')
                  )
            BEGIN
                DROP TABLE #getStudentEnrollments;
            END;
        IF EXISTS (
                  SELECT 1
                  FROM   tempdb.dbo.sysobjects AS O
                  WHERE  O.xtype IN ( 'U' )
                         AND O.id = OBJECT_ID(N'tempdb..#tmpStudentEnrollmentsOnlyIds')
                  )
            BEGIN
                DROP TABLE #tmpStudentEnrollmentsOnlyIds;
            END;
        IF EXISTS (
                  SELECT 1
                  FROM   tempdb.dbo.sysobjects AS O
                  WHERE  O.xtype IN ( 'U' )
                         AND O.id = OBJECT_ID(N'tempdb..#tmpEquivalentCoursesTakenByStudentInTerm')
                  )
            BEGIN
                DROP TABLE #tmpEquivalentCoursesTakenByStudentInTerm;
            END;
        IF EXISTS (
                  SELECT 1
                  FROM   tempdb.dbo.sysobjects AS O
                  WHERE  O.xtype IN ( 'U' )
                         AND O.id = OBJECT_ID(N'tempdb..#tmpStudentsWhoHasGradedEquivalentCourseInTerm')
                  )
            BEGIN
                DROP TABLE #tmpStudentsWhoHasGradedEquivalentCourseInTerm;
            END;
        IF EXISTS (
                  SELECT 1
                  FROM   tempdb.dbo.sysobjects AS O
                  WHERE  O.xtype IN ( 'U' )
                         AND O.id = OBJECT_ID(N'tempdb..#tmpEquivalentCoursesTakenByStudent')
                  )
            BEGIN
                DROP TABLE #tmpEquivalentCoursesTakenByStudent;
            END;
        IF EXISTS (
                  SELECT 1
                  FROM   tempdb.dbo.sysobjects AS O
                  WHERE  O.xtype IN ( 'U' )
                         AND O.id = OBJECT_ID(N'tempdb..#tmpStudentsWhoHasGradedEquivalentCourse')
                  )
            BEGIN
                DROP TABLE #tmpStudentsWhoHasGradedEquivalentCourse;
            END;
        IF EXISTS (
                  SELECT 1
                  FROM   tempdb.dbo.sysobjects AS O
                  WHERE  O.xtype IN ( 'U' )
                         AND O.id = OBJECT_ID(N'tempdb..#tmpCreditSummary')
                  )
            BEGIN
                DROP TABLE #tmpCreditSummary;
            END;

    END;
--=================================================================================================
-- END  --  USP_STUDENTGPA_GETDETAILS
--=================================================================================================
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
--=================================================================================================
-- CREATE VIEW adLeadsView
--=================================================================================================
PRINT N'If exist view adLeadsView, delete it';
GO
IF EXISTS (
          SELECT 1
          FROM   sys.views AS V
          WHERE  object_id = OBJECT_ID(N'[dbo].[adLeadsView]')
                 AND type IN ( N'V' )
          )
    BEGIN
        PRINT '    Droppin view adLeadsView';
        DROP VIEW adLeadsView;
    END;

GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT 'Creating view adLeadsView';
GO
--=================================================================================================
-- CREATE VIEW adLeadsView
--=================================================================================================
CREATE VIEW [dbo].[adLeadsView]
AS
    SELECT AL.LeadId
          ,(
           SELECT   TOP 1 ALE.EMail
           FROM     AdLeadEmail AS ALE
           WHERE    ALE.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' -- Status = 'Active'
                    AND ALE.IsPreferred = 1
                    AND ALE.LeadId = AL.LeadId
           ORDER BY ALE.ModDate DESC
           ) AS Email_Best
          ,(
           SELECT   TOP 1 ALE.EMail
           FROM     AdLeadEmail AS ALE
           WHERE    ALE.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' -- Status = 'Active'
                    AND ALE.IsPreferred = 0
                    AND ALE.LeadId = AL.LeadId
           ORDER BY ALE.ModDate DESC
           ) AS Email
          ,(
           SELECT   TOP 1 ALP.IsForeignPhone
           FROM     adLeadPhone AS ALP
           WHERE    ALP.IsBest = 1
                    AND ALP.Position = 1
                    AND ALP.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' -- Status = 'Active'
                    AND ALP.LeadId = AL.LeadId
           ORDER BY ALP.ModDate DESC
           ) AS IsForeignPhone
          ,(
           SELECT   TOP 1 ALP.Phone
           FROM     adLeadPhone AS ALP
           WHERE    ALP.IsBest = 1
                    AND ALP.Position = 1
                    AND ALP.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' -- Status = 'Active'
                    AND ALP.LeadId = AL.LeadId
           ORDER BY ALP.ModDate DESC
           ) AS Phone_Best
          ,(
           SELECT   TOP 1 st.PhoneTypeDescrip
           FROM     adLeadPhone AS ALP
           INNER JOIN syPhoneType AS st ON st.PhoneTypeId = ALP.PhoneTypeId
           WHERE    ALP.IsBest = 1
                    AND ALP.Position = 1
                    AND ALP.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' -- Status = 'Active'
                    AND ALP.LeadId = AL.LeadId
           ORDER BY ALP.ModDate DESC
           ) AS PhoneTypeId
          ,(
           SELECT   TOP 1 ALP.PhoneTypeId
           FROM     adLeadPhone AS ALP
           WHERE    ALP.IsBest = 1
                    AND ALP.Position = 1
                    AND ALP.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' -- Status = 'Active'
                    AND ALP.LeadId = AL.LeadId
           ORDER BY ALP.ModDate DESC
           ) AS PhoneType
          ,(
           SELECT   TOP 1 ALA.AddressTypeId
           FROM     adLeadAddresses AS ALA
           WHERE    ALA.IsShowOnLeadPage = 1
                    AND ALA.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' -- Status = 'Active'
                    AND ALA.LeadId = AL.LeadId
           ORDER BY ALA.ModDate DESC
           ) AS AddressTypeId
          ,(
           SELECT   TOP 1 ALA.Address1
           FROM     adLeadAddresses AS ALA
           WHERE    ALA.IsShowOnLeadPage = 1
                    AND ALA.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' -- Status = 'Active'
                    AND ALA.LeadId = AL.LeadId
           ORDER BY ALA.ModDate DESC
           ) AS Address1
          ,(
           SELECT   TOP 1 ALA.Address2
           FROM     adLeadAddresses AS ALA
           WHERE    ALA.IsShowOnLeadPage = 1
                    AND ALA.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' -- Status = 'Active'
                    AND ALA.LeadId = AL.LeadId
           ORDER BY ALA.ModDate DESC
           ) AS Address2
          ,(
           SELECT   TOP 1 ALA.City
           FROM     adLeadAddresses AS ALA
           WHERE    ALA.IsShowOnLeadPage = 1
                    AND ALA.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' -- Status = 'Active'
                    AND ALA.LeadId = AL.LeadId
           ORDER BY ALA.ModDate DESC
           ) AS City
          ,(
           SELECT   TOP 1 ALA.StateId
           FROM     adLeadAddresses AS ALA
           WHERE    ALA.IsShowOnLeadPage = 1
                    AND ALA.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' -- Status = 'Active'
                    AND ALA.LeadId = AL.LeadId
           ORDER BY ALA.ModDate DESC
           ) AS StateId
          ,(
           SELECT   TOP 1 ALA.ZipCode
           FROM     adLeadAddresses AS ALA
           WHERE    ALA.IsShowOnLeadPage = 1
                    AND ALA.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' -- Status = 'Active'
                    AND ALA.LeadId = AL.LeadId
           ORDER BY ALA.ModDate DESC
           ) AS ZipCode
          ,(
           SELECT   TOP 1 ALA.CountryId
           FROM     adLeadAddresses AS ALA
           WHERE    ALA.IsShowOnLeadPage = 1
                    AND ALA.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' -- Status = 'Active'
                    AND ALA.LeadId = AL.LeadId
           ORDER BY ALA.ModDate DESC
           ) AS CountryId
          ,(
           SELECT   TOP 1 ALA.State
           FROM     adLeadAddresses AS ALA
           WHERE    ALA.IsShowOnLeadPage = 1
                    AND ALA.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' -- Status = 'Active'
                    AND ALA.LeadId = AL.LeadId
           ORDER BY ALA.ModDate DESC
           ) AS State
          ,(
           SELECT   TOP 1 ALA.IsInternational
           FROM     adLeadAddresses AS ALA
           WHERE    ALA.IsShowOnLeadPage = 1
                    AND ALA.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' -- Status = 'Active'
                    AND ALA.LeadId = AL.LeadId
           ORDER BY ALA.ModDate DESC
           ) AS IsInternational
          ,(
           SELECT   TOP 1 ALA.CountyId
           FROM     adLeadAddresses AS ALA
           WHERE    ALA.IsShowOnLeadPage = 1
                    AND ALA.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' -- Status = 'Active'
                    AND ALA.LeadId = AL.LeadId
           ORDER BY ALA.ModDate DESC
           ) AS CountyId
    FROM   adLeads AS AL;
--=================================================================================================
-- END  --  CREATE VIEW adLeadsView
--=================================================================================================
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'If exists IX_adLeadAddresses_LeadId Index, Drop it.';
GO
IF EXISTS (
          SELECT 1
          FROM   sys.indexes AS I
          JOIN   sys.objects AS O ON I.object_id = O.object_id
          WHERE  O.name = 'adLeadAddresses'
                 AND I.name = 'IX_adLeadAddresses_LeadId'
          )
    BEGIN
        PRINT N'    Dropping Index IX_adLeadAddresses_LeadId';
        DROP INDEX IX_adLeadAddresses_LeadId
            ON adLeadAddresses;
    END;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'If exists IX_adLeadAddresses_LeadId_StatusId_IsShowOnLeadPage Index, Drop it.';
GO
IF EXISTS (
          SELECT 1
          FROM   sys.indexes AS I
          JOIN   sys.objects AS O ON I.object_id = O.object_id
          WHERE  O.name = 'adLeadAddresses'
                 AND I.name = 'IX_adLeadAddresses_LeadId_StatusId_IsShowOnLeadPage'
          )
    BEGIN
        PRINT N'    Dropping Index IX_adLeadAddresses_LeadId_StatusId_IsShowOnLeadPage';
        DROP INDEX IX_adLeadAddresses_LeadId_StatusId_IsShowOnLeadPage
            ON adLeadAddresses;
    END;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'If not exists IX_adLeadAddresses_LeadId_StatusId_IsShowOnLeadPage Index, Create it.';
GO
IF NOT EXISTS (
              SELECT 1
              FROM   sys.indexes AS I
              JOIN   sys.objects AS O ON I.object_id = O.object_id
              WHERE  O.name = 'adLeadAddresses'
                     AND I.name = 'IX_adLeadAddresses_LeadId_StatusId_IsShowOnLeadPage'
              )
    BEGIN
        PRINT N'    Creating Index IX_adLeadEntranceTest_LeadId_EntrTestId';
        CREATE NONCLUSTERED INDEX IX_adLeadAddresses_LeadId_StatusId_IsShowOnLeadPage
            ON adLeadAddresses
            (
            LeadId ASC
           ,StatusId ASC
           ,IsShowOnLeadPage ASC
            );
    END;
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'If exists IX_adLeadEntranceTest_LeadId_EntrTestId Index, Drop it.';
GO
IF EXISTS (
          SELECT 1
          FROM   sys.indexes AS I
          JOIN   sys.objects AS O ON I.object_id = O.object_id
          WHERE  O.name = 'adLeadEntranceTest'
                 AND I.name = 'IX_adLeadEntranceTest_LeadId_EntrTestId'
          )
    BEGIN
        PRINT N'    Dropping Index IX_adLeadEntranceTest_LeadId_EntrTestId';
        DROP INDEX IX_adLeadEntranceTest_LeadId_EntrTestId
            ON adLeadEntranceTest;
    END;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'If not exists IX_adLeadEntranceTest_LeadId_EntrTestId Index, Create it.';
GO
IF NOT EXISTS (
              SELECT 1
              FROM   sys.indexes AS I
              JOIN   sys.objects AS O ON I.object_id = O.object_id
              WHERE  O.name = 'adLeadEntranceTest'
                     AND I.name = 'IX_adLeadEntranceTest_LeadId_EntrTestId'
              )
    BEGIN
        PRINT N'    Creating Index IX_adLeadEntranceTest_LeadId_EntrTestId';
        CREATE NONCLUSTERED INDEX IX_adLeadEntranceTest_LeadId_EntrTestId
            ON adLeadEntranceTest
            (
            LeadId ASC
           ,EntrTestId ASC
            );
    END;
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'If exists IX_adEntrTestOverRide_LeadId Index, Drop it.';
GO
IF EXISTS (
          SELECT 1
          FROM   sys.indexes AS I
          JOIN   sys.objects AS O ON I.object_id = O.object_id
          WHERE  O.name = 'adEntrTestOverRide'
                 AND I.name = 'IX_adEntrTestOverRide_LeadId'
          )
    BEGIN
        PRINT N'    Dropping Index IX_adEntrTestOverRide_LeadId';
        DROP INDEX IX_adEntrTestOverRide_LeadId
            ON adEntrTestOverRide;
    END;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'If not exists IX_adEntrTestOverRide_LeadId Index, Create it.';
GO
IF NOT EXISTS (
              SELECT 1
              FROM   sys.indexes AS I
              JOIN   sys.objects AS O ON I.object_id = O.object_id
              WHERE  O.name = 'adEntrTestOverRide'
                     AND I.name = 'IX_adEntrTestOverRide_LeadId'
              )
    BEGIN
        PRINT N'    Creating Index IX_adEntrTestOverRide_LeadId';
        CREATE NONCLUSTERED INDEX IX_adEntrTestOverRide_LeadId
            ON adEntrTestOverRide ( LeadId ASC );
    END;
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'If exists IX_syStudentAttendanceSummary_StuEnrollId_TermId_TermStartDate, Drop it.';
GO
IF EXISTS (
          SELECT 1
          FROM   sys.indexes AS I
          JOIN   sys.objects AS O ON I.object_id = O.object_id
          WHERE  O.name = 'syStudentAttendanceSummary'
                 AND I.name = 'IX_syStudentAttendanceSummary_StuEnrollId_TermId_TermStartDate'
          )
    BEGIN
        PRINT N'    Dropping Index IX_syStudentAttendanceSummary_StuEnrollId_TermId_TermStartDate';
        DROP INDEX IX_syStudentAttendanceSummary_StuEnrollId_TermId_TermStartDate
            ON syStudentAttendanceSummary;
    END;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'If not exists column atClsSectAttendance.ModDate, add it.';
GO
IF NOT EXISTS (
              SELECT 1
              FROM   sys.columns AS C
              JOIN   sys.objects AS O ON O.object_id = C.object_id
              WHERE  O.name = 'atClsSectAttendance'
                     AND C.name = 'ModDate'
              )
    BEGIN
        PRINT N'    Adding atClsSectAttendance.ModDate default Getdate()';
        ALTER TABLE dbo.atClsSectAttendance
        ADD ModDate DATETIME NOT NULL CONSTRAINT DF_atClsSectAttendance_ModDate
                DEFAULT ( GETDATE());
    END;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'If not exists column atClsSectAttendance.ModUser, add it.';
GO
IF NOT EXISTS (
              SELECT 1
              FROM   sys.columns AS C
              JOIN   sys.objects AS O ON O.object_id = C.object_id
              WHERE  O.name = 'atClsSectAttendance'
                     AND C.name = 'ModUser'
              )
    BEGIN
        PRINT N'    Adding atClsSectAttendance.ModUser default "" ';
        ALTER TABLE dbo.atClsSectAttendance
        ADD ModUser VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT DF_atClsSectAttendance_ModUser
                DEFAULT ( '' );
    END;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'If not exists column atConversionAttendance.ModDate, add it.';
GO
IF NOT EXISTS (
              SELECT 1
              FROM   sys.columns AS C
              JOIN   sys.objects AS O ON O.object_id = C.object_id
              WHERE  O.name = 'atConversionAttendance'
                     AND C.name = 'ModDate'
              )
    BEGIN
        PRINT N'    Adding atConversionAttendance.ModDate default Getdate()';
        ALTER TABLE dbo.atConversionAttendance
        ADD ModDate DATETIME NOT NULL CONSTRAINT DF_atConversionAttendance_ModDate
                DEFAULT ( GETDATE());
    END;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'If not exists column atConversionAttendance.ModUser, add it.';
GO
IF NOT EXISTS (
              SELECT 1
              FROM   sys.columns AS C
              JOIN   sys.objects AS O ON O.object_id = C.object_id
              WHERE  O.name = 'atConversionAttendance'
                     AND C.name = 'ModUser'
              )
    BEGIN
        PRINT N'    Adding atConversionAttendance.ModUser default "" ';
        ALTER TABLE dbo.atConversionAttendance
        ADD ModUser VARCHAR(50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL CONSTRAINT DF_atConversionAttendance_ModUser
                DEFAULT ( '' );
    END;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'If not exists column syStudentAttendanceSummary.StudentAttendanceSummaryId, add it.';
GO
IF NOT EXISTS (
              SELECT 1
              FROM   sys.columns AS C
              JOIN   sys.objects AS O ON O.object_id = C.object_id
              WHERE  O.name = 'syStudentAttendanceSummary'
                     AND C.name = 'StudentAttendanceSummaryId'
              )
    BEGIN
        PRINT N'    Adding syStudentAttendanceSummary.StudentAttendanceSummaryId INTEGER NOT NULL IDENTITY(1, 1) ';
        ALTER TABLE dbo.syStudentAttendanceSummary
        ADD StudentAttendanceSummaryId INTEGER NOT NULL IDENTITY(1, 1);

    END;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
PRINT N'If not exist primary key PK_syStudentAttendanceSummary_StudentAttendanceSummaryId on syStudentAttendanceSummary, create it';
GO
IF NOT EXISTS (
              SELECT 1
              FROM   sys.objects AS O
              WHERE  object_id = OBJECT_ID(N'[dbo].[PK_syStudentAttendanceSummary_StudentAttendanceSummaryId]')
                     AND type IN ( N'PK' )
              )
    BEGIN
        PRINT N'    Creating primary key PK_syStudentAttendanceSummary_StudentAttendanceSummaryId on syStudentAttendanceSummary ';
        ALTER TABLE dbo.syStudentAttendanceSummary
        ADD CONSTRAINT PK_syStudentAttendanceSummary_StudentAttendanceSummaryId
            PRIMARY KEY CLUSTERED ( StudentAttendanceSummaryId ) ON [PRIMARY];
    END;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO


COMMIT TRANSACTION;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
DECLARE @Success AS BIT;
SET @Success = 1;
SET NOEXEC OFF;
IF ( @Success = 1 )
    PRINT 'The database update succeeded';
ELSE
    BEGIN
        IF @@TRANCOUNT > 0
            ROLLBACK TRANSACTION;
        PRINT 'The database update failed';
    END;
GO
