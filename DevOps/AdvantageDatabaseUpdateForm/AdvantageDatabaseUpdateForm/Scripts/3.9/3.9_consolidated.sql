-- ===============================================================================================
-- Consolidated Script Version 3.9
-- Data Changes Zone
-- Please do not deploy your schema changes in this area
-- Please use SQL Prompt format before insert here please
-- ===============================================================================================

-- =================================================================================================
-- DE13868 Adhoc report columns export blank data in excel
-- =================================================================================================
--  1) Update field Phone - Best
DECLARE @FldId AS INTEGER;
SET @FldId = 0;

SELECT @FldId = SF.FldId
FROM   syFields AS SF
WHERE  SF.FldName = '[Phone - Best]';

-- if Exists SF.FldName = '[Phone - Best]', update it
IF @FldId <> 0
    BEGIN
        UPDATE SF
        SET    SF.FldName = 'Phone_Best'
        FROM   syFields AS SF
        WHERE  SF.FldId = @FldId;

        UPDATE SFC
        SET    SFC.CalculationSql = '(SELECT TOP 1 ALP.Phone FROM adLeadPhone AS ALP INNER JOIN syStatuses AS SS ON SS.StatusId = ALP.StatusId WHERE   ALP.IsBest = 1 AND ALP.Position = 1 AND SS.StatusCode = ''A'' AND ALP.LeadId = adLeads.LeadId ORDER BY ALP.ModDate DESC ) AS Phone_Best'
        FROM   syFieldCalculation AS SFC
        WHERE  SFC.FldId = @FldId;
    END;
GO

-- 2) Update field Email - Best
DECLARE @FldId AS INTEGER;
SET @FldId = 0;

SELECT @FldId = SF.FldId
FROM   syFields AS SF
WHERE  SF.FldName = '[Email - Best]';

-- if Exists SF.FldName = '[Email - Best]', update it
IF @FldId <> 0
    BEGIN
        UPDATE SF
        SET    SF.FldName = 'Email_Best'
        FROM   syFields AS SF
        WHERE  SF.FldId = @FldId;

        UPDATE SFC
        SET    SFC.CalculationSql = '(SELECT TOP 1 ALE.EMail FROM AdLeadEmail AS ALE INNER JOIN syStatuses AS SS ON SS.StatusId = ALE.StatusId WHERE SS.Status = ''ACTIVE'' AND ALE.EMailTypeId = (SELECT EMailTypeId FROM syEmailType AS SETY WHERE SETY.EMailTypeCode = ''Home'') AND ALE.LeadId = adleads.LeadId ORDER BY ALE.ModDate DESC ) AS Email_Best'
        FROM   syFieldCalculation AS SFC
        WHERE  SFC.FldId = @FldId;
    END;
GO
-- =================================================================================================
-- END  --  DE13868 Adhoc report columns export blank data in excel
-- =================================================================================================

-- =================================================================================================
-- DE13818 QA US10537Central State Beauty-Maintenance  Student Group Type options are not displayed in the Student Hold Groups page.
-- =================================================================================================
--  Insert record in adStuGrpTypes if not exists.

IF NOT EXISTS (
              SELECT 1
              FROM   adStuGrpTypes AS ASGT
              WHERE  ASGT.Descrip = 'MANUAL'
              )
    BEGIN
        INSERT adStuGrpTypes (
                             GrpTypeID
                            ,Descrip
                             )
        VALUES ( NEWID()  -- GrpTypeID - uniqueidentifier
                ,'MANUAL' -- Descrip - varchar(50)
               );
    END;
-- =================================================================================================
-- END  --  DE13818 QA US10537Central State Beauty-Maintenance  Student Group Type options are not displayed in the Student Hold Groups page.
-- =================================================================================================

-- =================================================================================================
-- Eliminate duplicate column in syWapiBridgeExternalCompanyAllowedServices
-- =================================================================================================


DELETE FROM f
FROM syWapiBridgeExternalCompanyAllowedServices AS f
INNER JOIN syWapiBridgeExternalCompanyAllowedServices AS g ON g.IdExternalCompanies = f.IdExternalCompanies
                                                              AND g.IdAllowedServices = f.IdAllowedServices
                                                              AND f.IdWapiBridgeAllowedService < g.IdWapiBridgeAllowedService;

-- =================================================================================================
-- END  --  Eliminate duplicate column in syWapiBridgeExternalCompanyAllowedServices
-- =================================================================================================

-- =================================================================================================
-- Convert parameter of Klass App from 0/1 to Yes/No
-- Also repeat descriptions
-- =================================================================================================

-- Get the setting
DECLARE @settingId INT = (
                         SELECT TOP 1 SettingId
                         FROM   dbo.syConfigAppSettings
                         WHERE  KeyName = 'KlassApp_CampusToConsider'
                         );
IF EXISTS (
          SELECT *
          FROM   dbo.syConfigAppSetValues
          WHERE  SettingId = @settingId
          )
    BEGIN
        UPDATE dbo.syConfigAppSetValues
        SET    Value = ( CASE WHEN Value = '1'
                                   OR Value = 'Yes' THEN 'Yes'
                              ELSE 'No'
                         END
                       )
        WHERE  SettingId = @settingId;

        UPDATE dbo.syConfigAppSettings
        SET    Description = 'Yes/No Value Determine if the Campus student must be send to KLASS APP'
        WHERE  SettingId = @settingId;

        UPDATE dbo.syConfigAppSettings
        SET    Description = 'Klass APP URL for Custom Fields operation'
        WHERE  KeyName = 'KlassApp_UrlConfigCustomFields';

        UPDATE dbo.syConfigAppSettings
        SET    Description = 'Klass APP URL for Key Values operation'
        WHERE  KeyName = 'KlassApp_UrlConfigKeyValuePairs';

        UPDATE dbo.syConfigAppSettings
        SET    Description = 'Klass APP URL for Locations (Campus) operation'
        WHERE  KeyName = 'KlassApp_UrlConfigLocations';

    END;
GO
-- Create the Combo Box Yes/No for the settings
DECLARE @settingId INT = (
                         SELECT SettingId
                         FROM   dbo.syConfigAppSettings
                         WHERE  KeyName = 'KlassApp_CampusToConsider'
                         );


IF NOT EXISTS (
              SELECT *
              FROM   dbo.syConfigAppSet_Lookup
              WHERE  SettingId = @settingId
              )
    BEGIN
        BEGIN TRANSACTION Lookup;


        INSERT INTO dbo.syConfigAppSet_Lookup (
                                              LookUpId
                                             ,SettingId
                                             ,ValueOptions
                                             ,ModUser
                                             ,ModDate
                                              )
        VALUES ( NEWID()    -- LookUpId - uniqueidentifier
                ,@settingId -- SettingId - int
                ,'Yes'      -- ValueOptions - varchar(50)
                ,'SUPPORT'  -- ModUser - varchar(50)
                ,GETDATE()  -- ModDate - datetime
               );

        INSERT INTO dbo.syConfigAppSet_Lookup (
                                              LookUpId
                                             ,SettingId
                                             ,ValueOptions
                                             ,ModUser
                                             ,ModDate
                                              )
        VALUES ( NEWID()    -- LookUpId - uniqueidentifier
                ,@settingId -- SettingId - int
                ,'No'       -- ValueOptions - varchar(50)
                ,'SUPPORT'  -- ModUser - varchar(50)
                ,GETDATE()  -- ModDate - datetime
               );

        COMMIT TRANSACTION Lookup;
    END;
GO

-- =================================================================================================
-- Create default user if not exists
-- Create Operation type KLASS_APP_STUDENT
-- Create pre-populate Klass-App Operation
-- =================================================================================================
-- Create a default user if not exists...
IF NOT EXISTS (
              SELECT *
              FROM   dbo.syWapiExternalCompanies
              )
    BEGIN
        INSERT INTO dbo.syWapiExternalCompanies (
                                                Code
                                               ,Description
                                               ,IsActive
                                                )
        VALUES ( 'TOKEN_DEFAULT_PROVISIONAL'                                           -- Code - varchar(50)
                ,'Default provisional token, should be change in the final deployment' -- Description - varchar(100)
                ,1                                                                     -- IsActive - bit
               );
    END;
GO

-- Create the operation Type for KlassApp if does not exists
IF NOT EXISTS (
              SELECT *
              FROM   syWapiExternalOperationMode
              WHERE  Code = 'KLASS_APP_SERVICE'
              )
    BEGIN
        INSERT INTO dbo.syWapiExternalOperationMode (
                                                    Code
                                                   ,Description
                                                   ,IsActive
                                                    )
        VALUES ( 'KLASS_APP_SERVICE'           -- Code - varchar(50)
                ,'Communication with KlassApp' -- Description - varchar(100)
                ,1                             -- IsActive - bit
               );
    END;
GO

DECLARE @ExternalOperationId INT = (
                                   SELECT Id
                                   FROM   syWapiExternalOperationMode
                                   WHERE  Code = 'KLASS_APP_SERVICE'
                                   );
DECLARE @FirstOperationId INT = (
                                SELECT Id
                                FROM   dbo.syWapiAllowedServices
                                WHERE  Code = 'KLASS_GET_CONFIGURATION_STATUS'
                                );
DECLARE @SecondOperationId INT = (
                                 SELECT Id
                                 FROM   dbo.syWapiAllowedServices
                                 WHERE  Code = 'KLASS_GET_STUDENT_INFORMATION'
                                 );
DECLARE @CompanyCodeId INT = (
                             SELECT TOP 1 Id
                             FROM   dbo.syWapiExternalCompanies
                             WHERE  IsActive = 1
                                    AND Code LIKE '%DEFAULT%'
                             );

-- Create Basic configuration for Klass App Student Service
IF NOT EXISTS (
              SELECT *
              FROM   dbo.syWapiSettings
              WHERE  CodeOperation = 'KLASS_APP_STUDENT'
              )
    BEGIN
        INSERT INTO dbo.syWapiSettings (
                                       CodeOperation
                                      ,ExternalUrl
                                      ,IdExtCompany
                                      ,IdExtOperation
                                      ,IdAllowedService
                                      ,IdSecondAllowedService
                                      ,FirstAllowedServiceQueryString
                                      ,SecondAllowedServiceQueryString
                                      ,ConsumerKey
                                      ,PrivateKey
                                      ,OperationSecondTimeInterval
                                      ,PollSecondForOnDemandOperation
                                      ,FlagOnDemandOperation
                                      ,FlagRefreshConfiguration
                                      ,IsActive
                                      ,DateMod
                                      ,DateLastExecution
                                      ,UserMod
                                       )
        VALUES ( 'KLASS_APP_STUDENT'         -- CodeOperation - varchar(50)
                ,'https://api.klassapp.com/' -- ExternalUrl - varchar(2048)
                ,@CompanyCodeId              -- IdExtCompany - int
                ,@ExternalOperationId        -- IdExtOperation - int
                ,@FirstOperationId           -- IdAllowedService - int
                ,@SecondOperationId          -- IdSecondAllowedService - int
                ,''                          -- FirstAllowedServiceQueryString - varchar(200)
                ,''                          -- SecondAllowedServiceQueryString - varchar(200)
                ,''                          -- ConsumerKey - varchar(100)
                ,''                          -- PrivateKey - varchar(100)
                ,3600                        -- OperationSecondTimeInterval - int
                ,10                          -- PollSecondForOnDemandOperation - int
                ,0                           -- FlagOnDemandOperation - bit
                ,0                           -- FlagRefreshConfiguration - bit
                ,0                           -- IsActive - bit
                ,GETDATE()                   -- DateMod - datetime2(7)
                ,GETDATE()                   -- DateLastExecution - datetime2(7)
                ,'SUPPORT'                   -- UserMod - varchar(50)
               );

    END;
GO

-- ----------------------------------------------------------------------
-- Create Link for Fast Report: Klass-App Students Not Exported
-- JAGG US10815: Klass-App: Report to show students not synced
-- ----------------------------------------------------------------------

DELETE FROM dbo.syRlsResLvls
WHERE ResourceID = 840;
DELETE FROM dbo.syNavigationNodes
WHERE ResourceId = 840;
DELETE FROM dbo.syMenuItems
WHERE ResourceId = 840;
DELETE FROM dbo.syResources
WHERE ResourceID = 840;

IF NOT EXISTS (
              SELECT *
              FROM   syResources
              WHERE  ResourceID = 840
                     AND Resource = 'Fast Report Page'
              )
    BEGIN
        INSERT INTO syResources (
                                ResourceID
                               ,Resource
                               ,ResourceTypeID
                               ,ResourceURL
                               ,ModDate
                               ,ModUser
                               ,AllowSchlReqFlds
                               ,MRUTypeId
                               ,UsedIn
                                )
        VALUES ( 840
                -- Here your resource ID
                ,'Custom Reports'
                -- Resource Description
                ,5
                -- Resource type 5 is Report
                ,'~/sy/FastReport.aspx'
                -- The page where should appear the item
                ,GETDATE()
                ,'sa'
                -- Who create this records
                ,0
                ,1
                ,975
               ); -- page resource where is used (this is Report page)	
    END;
GO

IF NOT EXISTS (
              SELECT *
              FROM   syResources
              WHERE  ResourceID = 841
              )
    BEGIN
        INSERT INTO syResources (
                                ResourceID
                               ,Resource
                               ,ResourceTypeID
                               ,ResourceURL
                               ,ModDate
                               ,ModUser
                               ,AllowSchlReqFlds
                               ,MRUTypeId
                               ,UsedIn
                                )
        VALUES ( 841
                -- Here your resource ID
                ,'Mobility App Exceptions'
                -- Resource Description
                ,5
                -- Resource type 5 is Report
                ,'~/sy/FastReport.aspx'
                -- The page where should appear the item
                ,GETDATE()
                ,'SUPPORT'
                -- Who create this records
                ,0
                ,1
                ,975
               ); -- page resource where is used (this is Report page)	
    END;
GO
-- Enter 842 Custom Reports  in resources ID
IF NOT EXISTS (
              SELECT *
              FROM   syResources
              WHERE  ResourceID = 842
                     AND Resource = 'Custom Reports'
              )
    BEGIN

        INSERT INTO syResources (
                                ResourceID
                               ,Resource
                               ,ResourceTypeID
                               ,ResourceURL
                               ,ModDate
                               ,ModUser
                               ,AllowSchlReqFlds
                               ,MRUTypeId
                               ,UsedIn
                                )
        VALUES ( 842
                -- Here your resource ID
                ,'Custom Reports'
                -- Resource Description
                ,5
                -- Resource type 5 is Report
                ,'~/sy/FastReport.aspx'
                -- The page where should appear the item
                ,GETDATE()
                ,'SUPPORT'
                -- Who create this records
                ,0
                ,1
                ,975
               );
    -- page resource where is used (this is Report page)
    END;
GO

-- Link to Klass-App Students Not Exported

IF NOT EXISTS (
              SELECT *
              FROM   syMenuItems
              WHERE  ResourceId = 841
                     AND MenuName = 'Mobility App Exceptions'
              )
    BEGIN

        DECLARE @ParentId INT = (
                                SELECT s1.MenuItemId
                                FROM   syMenuItems s1
                                JOIN   dbo.syMenuItems s2 ON s2.MenuItemId = s1.ParentId
                                WHERE  s1.MenuName = 'General Reports'
                                       AND s1.ResourceId = 717
                                );

        INSERT INTO syMenuItems (
                                MenuName
                               ,DisplayName
                               ,Url
                               ,MenuItemTypeId
                               ,ParentId
                               ,DisplayOrder
                               ,IsPopup
                               ,ModDate
                               ,ModUser
                               ,IsActive
                               ,ResourceId
                               ,HierarchyId
                               ,ModuleCode
                               ,MRUType
                               ,HideStatusBar
                                )
        VALUES ( 'Mobility App Exceptions'
                ,'Mobility App Exceptions' -- Report or Initial folder
                ,'/SY/FastReport.aspx'
                                           -- Page URL where point the meu item created
                ,4
                                           -- Menu Item Type
                ,@ParentId
                                           -- The MenuItemId of the menu item under you want to put the created menu.
                ,300
                                           -- Relative position of the item in the existent list in the page.
                ,0
                ,GETDATE()
                ,'support'
                ,1
                ,841
                                           -- Resource ID
                ,NULL
                                           -- no more used
                ,NULL
                ,NULL
                ,NULL
               );
    END;

GO
-- Enter Custom Reports to Menu Items 842

IF NOT EXISTS (
              SELECT *
              FROM   syMenuItems
              WHERE  (
                     ResourceId = 842
                     AND MenuName = 'Custom Reports'
                     )
              )
    BEGIN



        DECLARE @ParentId INT = (
                                SELECT s1.MenuItemId
                                FROM   syMenuItems s1
                                JOIN   dbo.syMenuItems s2 ON s2.MenuItemId = s1.ParentId
                                WHERE  s1.MenuName = 'General Reports'
                                       AND s1.ResourceId = 717
                                );

        INSERT INTO syMenuItems (
                                MenuName
                               ,DisplayName
                               ,Url
                               ,MenuItemTypeId
                               ,ParentId
                               ,DisplayOrder
                               ,IsPopup
                               ,ModDate
                               ,ModUser
                               ,IsActive
                               ,ResourceId
                               ,HierarchyId
                               ,ModuleCode
                               ,MRUType
                               ,HideStatusBar
                                )
        VALUES ( 'Custom Reports'
                ,'Custom Reports' -- Initial directory (folder name)
                ,'/SY/FastReport.aspx'
                                  -- Page URL where point the menu item created
                ,4
                                  -- Menu Item Type
                ,@ParentId
                                  -- The MenuItemId of the menu item under you want to put the created menu.
                ,400
                                  -- Relative position of the item in the existent list in the page.
                ,0
                ,GETDATE()
                ,'support'
                ,1
                ,842
                                  -- Resource ID
                ,NULL
                                  -- no more used
                ,NULL
                ,NULL
                ,NULL
               );
    END;
GO
-- ===========================================================================
-- Enter Security for the links Custom Reports and Mobility App Exceptions
-- ===========================================================================
DECLARE @ParentId UNIQUEIDENTIFIER;
SET @ParentId = (
                SELECT HierarchyId
                FROM   syNavigationNodes
                WHERE  ResourceId = 717
                );
IF NOT EXISTS (
              SELECT *
              FROM   dbo.syNavigationNodes
              WHERE  ResourceId = 841
              )
    BEGIN
        INSERT INTO dbo.syNavigationNodes (
                                          HierarchyId
                                         ,HierarchyIndex
                                         ,ResourceId
                                         ,ParentId
                                         ,ModUser
                                         ,ModDate
                                         ,IsPopupWindow
                                         ,IsShipped
                                          )
        VALUES ( NEWID()   -- HierarchyId - uniqueidentifier
                ,2         -- HierarchyIndex - smallint
                ,841       -- ResourceId - smallint
                ,@ParentId -- ParentId - uniqueidentifier
                ,'SUPPORT' -- ModUser - varchar(50)
                ,GETDATE() -- ModDate - datetime
                ,0         -- IsPopupWindow - bit
                ,1         -- IsShipped - bit
               );
    END;
IF NOT EXISTS (
              SELECT *
              FROM   dbo.syNavigationNodes
              WHERE  ResourceId = 842
              )
    BEGIN
        INSERT INTO dbo.syNavigationNodes (
                                          HierarchyId
                                         ,HierarchyIndex
                                         ,ResourceId
                                         ,ParentId
                                         ,ModUser
                                         ,ModDate
                                         ,IsPopupWindow
                                         ,IsShipped
                                          )
        VALUES ( NEWID()   -- HierarchyId - uniqueidentifier
                ,2         -- HierarchyIndex - smallint
                ,842       -- ResourceId - smallint
                ,@ParentId -- ParentId - uniqueidentifier
                ,'SUPPORT' -- ModUser - varchar(50)
                ,GETDATE() -- ModDate - datetime
                ,0         -- IsPopupWindow - bit
                ,1         -- IsShipped - bit
               );
    END;

GO
-- ===========================================================================
-- Start US 10829: Hide Invalid page options in Add Custom Fields to a Page
-- ===========================================================================
BEGIN TRANSACTION deletePageOption;
BEGIN TRY
    DECLARE @Error AS INTEGER;
    SET @Error = 0;
    IF EXISTS (
              SELECT 1
              FROM   syAdvantageResourceRelations
              WHERE  syAdvantageResourceRelations.RelatedResourceId = 159
                     AND ResourceId = 394
              )
        BEGIN
            DELETE FROM syAdvantageResourceRelations
            WHERE syAdvantageResourceRelations.RelatedResourceId = 159
                  AND ResourceId = 394;
        END;
    IF EXISTS (
              SELECT 1
              FROM   syAdvantageResourceRelations
              WHERE  syAdvantageResourceRelations.RelatedResourceId = 213
                     AND ResourceId = 394
              )
        BEGIN
            DELETE FROM syAdvantageResourceRelations
            WHERE syAdvantageResourceRelations.RelatedResourceId = 213
                  AND ResourceId = 394;
        END;
    IF EXISTS (
              SELECT 1
              FROM   syAdvantageResourceRelations
              WHERE  syAdvantageResourceRelations.RelatedResourceId = 155
                     AND ResourceId = 394
              )
        BEGIN
            DELETE FROM syAdvantageResourceRelations
            WHERE syAdvantageResourceRelations.RelatedResourceId = 155
                  AND ResourceId = 394;
        END;
    IF ( @@ERROR > 0 )
        BEGIN
            SET @Error = 1;
        END;
    ELSE
        BEGIN
            SET @Error = 0;
        END;

END TRY
BEGIN CATCH
    ROLLBACK TRANSACTION deletePageOption;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE()
          ,@severity = ERROR_SEVERITY()
          ,@state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @Error > 0
    BEGIN
        ROLLBACK TRANSACTION deletePageOption;
        PRINT 'Update fail';

    END;
ELSE
    BEGIN
        COMMIT TRANSACTION deletePageOption;
        PRINT 'Update successful';
    END;
GO
-- ===========================================================================
-- End US 10829: Hide Invalid page options in Add Custom Fields to a Page
-- ===========================================================================

----------------------------------------------------------------------------------------------------------------------------------------------------
--Start DE13751:Central State Beauty/Winter - Student Financial Aid Reports-Missing "c" letter in Financial
-----------------------------------------------------------------------------------------------------------------------------------------------------
UPDATE dbo.syMenuItems
SET    MenuName = 'Winter - Student Financial Aid Reports'
      ,DisplayName = 'Winter - Student Financial Aid Reports'
WHERE  MenuName = 'Winter - Student Finanial Aid Reports';
GO
----------------------------------------------------------------------------------------------------------------------------------------------------
--End DE13751:Central State Beauty/Winter - Student Financial Aid Reports-Missing "c" letter in Financial
-----------------------------------------------------------------------------------------------------------------------------------------------------

--====================================================================================
-- START DE14029 - Normal - Advantage - Rosedale - Fields missing from lead AdHoc
--====================================================================================
----------------------------------------------------------------------------------
--DE14029 - Normal - Advantage - Rosedale - Fields missing from lead AdHoc
--------------------------------------------------------------------------------------
BEGIN TRANSACTION trAdhoc;
BEGIN TRY
    DECLARE @Error AS INTEGER;
    SET @Error = 0;
    DECLARE @fldId INT
           ,@leadTblId INT;
    SET @fldId = (
                 SELECT FldId
                 FROM   syFields
                 WHERE  FldName = 'HighSchool'
                 );
    SET @leadTblId = (
                     SELECT TblId
                     FROM   syTables
                     WHERE  TblName = 'adLeads'
                     );
    -- start undo the changes already made in the	consolidated script
    IF EXISTS (
              SELECT 1
              FROM   syTblFlds
              WHERE  FldId = @fldId
                     AND TblId = @leadTblId
              )
        BEGIN
            IF (   (
                   SELECT CategoryId
                   FROM   syTblFlds
                   WHERE  FldId = @fldId
                          AND TblId = @leadTblId
                   ) IS NOT NULL
               )
                BEGIN

                    UPDATE syTblFlds
                    SET    CategoryId = NULL --23950
                    WHERE  FldId = @fldId
                           AND TblId = @leadTblId;
                END;
            IF ( @@ERROR > 0 )
                BEGIN
                    SET @Error = 1;
                END;
            ELSE
                BEGIN
                    SET @Error = 0;
                END;
            IF (   (
                   SELECT FKColDescrip
                   FROM   syTblFlds
                   WHERE  FldId = @fldId
                          AND TblId = @leadTblId
                   ) IS NOT NULL
               )
                BEGIN
                    UPDATE syTblFlds
                    SET    FKColDescrip = NULL --'HSName'
                    WHERE  FldId = @fldId
                           AND TblId = @leadTblId;
                END;
            IF ( @@ERROR > 0 )
                BEGIN
                    SET @Error = 1;
                END;
            ELSE
                BEGIN
                    SET @Error = 0;
                END;
        END;
    -- end undo the changes already made in the	consolidated script
    -- start do the changes for HighSchoolGradDate
    SET @fldId = (
                 SELECT FldId
                 FROM   syFields
                 WHERE  FldName = 'HighSchoolGradDate'
                 );
    IF EXISTS (
              SELECT 1
              FROM   syTblFlds
              WHERE  FldId = @fldId
                     AND TblId = @leadTblId
              )
        BEGIN

            IF (   (
                   SELECT CategoryId
                   FROM   syTblFlds
                   WHERE  FldId = @fldId
                          AND TblId = @leadTblId
                   ) IS NULL
               )
                BEGIN

                    UPDATE syTblFlds
                    SET    CategoryId = (
                                        SELECT CategoryId
                                        FROM   syFldCategories
                                        WHERE  EntityId = 395
                                               AND Descrip = 'General Information'
                                        )
                    WHERE  FldId = @fldId
                           AND TblId = @leadTblId;
                END;
            IF ( @@ERROR > 0 )
                BEGIN
                    SET @Error = 1;
                END;
            ELSE
                BEGIN
                    SET @Error = 0;
                END;

        END;
    --GO
    -- end do the changes for HighSchoolGradDate
    -- start the changes of HighSchoolId as that is the column in adleads.
    -- step1 add the table syInstitutions in syTables
    IF NOT EXISTS (
                  SELECT 1
                  FROM   syTables
                  WHERE  TblName = 'syInstitutions'
                  )
        BEGIN
            INSERT INTO syTables (
                                 TblId
                                ,TblName
                                ,TblDescrip
                                ,TblPK
                                 )
            VALUES (((
                     SELECT MAX(TblId) AS TblId
                     FROM   syTables
                     ) + 1
                    )                                                                               -- TblId - int
                   ,'syInstitutions'                                                                -- TblName - varchar(50)
                   ,'the table to have information of colleges, high school and other institutions' -- TblDescrip - varchar(100)
                   ,((
                     SELECT MAX(TblPK)
                     FROM   syTables
                     ) + 1
                    )                                                                               -- TblPK - int
                   );
            IF ( @@ERROR > 0 )
                BEGIN
                    SET @Error = 1;
                END;
            ELSE
                BEGIN
                    SET @Error = 0;
                END;
        END;
    --GO
    --step2: replace the adHighSchools with syInstitutions for highschools in syDDLS
    DECLARE @HsTblId INT
           ,@InstTblId INT;
    SET @HsTblId = (
                   SELECT TblId
                   FROM   syTables
                   WHERE  TblName = 'adHighSchools'
                   );
    SET @InstTblId = (
                     SELECT TblId
                     FROM   syTables
                     WHERE  TblName = 'syInstitutions'
                     );
    UPDATE syDDLS
    SET    TblId = @InstTblId
    WHERE  TblId = @HsTblId;
    --step3: add create HighSchoolId in syfields
    IF NOT EXISTS (
                  SELECT 1
                  FROM   syFields
                  WHERE  FldName = 'HighSchoolId'
                  )
        BEGIN
            DECLARE --@leadTblId INT
                @ddlId INT
               ,@HighSchoolIdFlId INT;
            SET @ddlId = (
                         SELECT DDLId
                         FROM   syDDLS
                         WHERE  DDLName = 'HighSchools'
                         );

            INSERT INTO syFields (
                                 FldId
                                ,FldName
                                ,FldTypeId
                                ,FldLen
                                ,DDLId
                                ,DerivedFld
                                ,SchlReq
                                ,LogChanges
                                ,Mask
                                 )
            VALUES (((
                     SELECT MAX(FldId)
                     FROM   syFields
                     ) + 1
                    )              -- FldId - int
                   ,'HighSchoolId' -- FldName - varchar(200)
                   ,72             -- FldTypeId - int
                   ,36             -- FldLen - int
                   ,@ddlId         -- DDLId - int
                   ,NULL           -- DerivedFld - bit
                   ,NULL           -- SchlReq - bit
                   ,1              -- LogChanges - bit
                   ,NULL           -- Mask - varchar(50)
                   );
            IF ( @@ERROR > 0 )
                BEGIN
                    SET @Error = 1;
                END;
            ELSE
                BEGIN
                    SET @Error = 0;
                END;
            SET @HighSchoolIdFlId = (
                                    SELECT FldId
                                    FROM   syFields
                                    WHERE  FldName = 'HighSchoolId'
                                    );
            INSERT INTO syTblFlds (
                                  TblFldsId
                                 ,TblId
                                 ,FldId
                                 ,CategoryId
                                 ,FKColDescrip
                                  )
            VALUES (((
                     SELECT MAX(TblFldsId)
                     FROM   syTblFlds
                     ) + 1
                    )                 -- TblFldsId - int
                   ,@leadTblId        -- TblId - int
                   ,@HighSchoolIdFlId -- FldId - int
                   ,(
                    SELECT CategoryId
                    FROM   syFldCategories
                    WHERE  EntityId = 395
                           AND Descrip = 'General Information'
                    )                 -- CategoryId - int
                   ,'HSName'          -- FKColDescrip - varchar(50)
                   );
            IF ( @@ERROR > 0 )
                BEGIN
                    SET @Error = 1;
                END;
            ELSE
                BEGIN
                    SET @Error = 0;
                END;
            INSERT INTO syFldCaptions (
                                      FldCapId
                                     ,FldId
                                     ,LangId
                                     ,Caption
                                      )
            VALUES ((
                    SELECT MAX(FldCapId)
                    FROM   syFldCaptions
                    ) + 1
                   ,@HighSchoolIdFlId
                   ,1
                   ,'High School'
                   );
            IF ( @@ERROR > 0 )
                BEGIN
                    SET @Error = 1;
                END;
            ELSE
                BEGIN
                    SET @Error = 0;
                END;
        END;
END TRY
BEGIN CATCH
    ROLLBACK TRANSACTION trAdhoc;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE()
          ,@severity = ERROR_SEVERITY()
          ,@state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @Error > 0
    BEGIN
        ROLLBACK TRANSACTION trAdhoc;
        PRINT 'Update fail';

    END;
ELSE
    BEGIN
        COMMIT TRANSACTION trAdhoc;
        PRINT 'Update successful';
    END;
GO
--====================================================================================
-- END DE14029 - Normal - Advantage - Rosedale - Fields missing from lead AdHoc
--====================================================================================
--====================================================================================
-- START US10753 - Update Requirement Overrides Correctly
--====================================================================================
--insert into adEntrTestOverRide
BEGIN TRANSACTION insertRows;
BEGIN TRY
    DECLARE @error AS INT;
    SET @error = 0;
    DECLARE @tempLeads AS TABLE
        (
            LeadId UNIQUEIDENTIFIER --,
        --studentId UNIQUEIDENTIFIER
        );
    INSERT INTO @tempLeads (
                           LeadId
                           --,studentId
                           )
                (SELECT LeadId
                 FROM   adLeads
                 WHERE  StudentId = '00000000-0000-0000-0000-000000000000'
                        AND StudentStatusId = '1AF592A6-8790-48EC-9916-5412C25EF49F'
                        AND LeadStatus IN (
                                          SELECT StatusCodeId
                                          FROM   syStatusCodes
                                          WHERE  SysStatusId < 6
                                          ));

    DECLARE leadCur CURSOR FOR
        SELECT LeadId
        FROM   @tempLeads;
    DECLARE @leadId UNIQUEIDENTIFIER;
    OPEN leadCur;
    FETCH NEXT FROM leadCur
    INTO @leadId;
    WHILE @@FETCH_STATUS = 0
        BEGIN
            -- documents
            IF EXISTS (
                      SELECT 1
                      FROM   adLeadDocsReceived
                      WHERE  LeadId = @leadId
                      )
                BEGIN
                    IF NOT EXISTS (
                                  SELECT 1
                                  FROM   adEntrTestOverRide
                                  WHERE  LeadId = @leadId
                                         AND EntrTestId IN (
                                                           SELECT DocumentId
                                                           FROM   adLeadDocsReceived
                                                           WHERE  LeadId = @leadId
                                                           )
                                  )
                        BEGIN
                            INSERT INTO adEntrTestOverRide (
                                                           EntrTestOverRideId
                                                          ,LeadId
                                                          ,EntrTestId
                                                          ,ModUser
                                                          ,ModDate
                                                          ,OverRide
                                                          ,StudentId
                                                           )
                                        SELECT NEWID()
                                              ,LeadId
                                              ,DocumentId
                                              ,ModUser
                                              ,ModDate
                                              ,Override
                                              ,NULL
                                        FROM   adLeadDocsReceived
                                        WHERE  LeadId = @leadId;
                        END;
                END;
            -- test
            IF EXISTS (
                      SELECT 1
                      FROM   adLeadEntranceTest
                      WHERE  LeadId = @leadId
                      )
                BEGIN
                    IF NOT EXISTS (
                                  SELECT 1
                                  FROM   adEntrTestOverRide
                                  WHERE  LeadId = @leadId
                                         AND EntrTestId IN (
                                                           SELECT EntrTestId
                                                           FROM   adLeadEntranceTest
                                                           WHERE  LeadId = @leadId
                                                           )
                                  )
                        BEGIN
                            INSERT INTO adEntrTestOverRide (
                                                           EntrTestOverRideId
                                                          ,LeadId
                                                          ,EntrTestId
                                                          ,ModUser
                                                          ,ModDate
                                                          ,OverRide
                                                          ,StudentId
                                                           )
                                        SELECT NEWID()
                                              ,LeadId
                                              ,EntrTestId
                                              ,ModUser
                                              ,ModDate
                                              ,OverRide
                                              ,NULL
                                        FROM   adLeadEntranceTest
                                        WHERE  LeadId = @leadId;
                        END;
                END;
            -- tours,event,interview
            IF EXISTS (
                      SELECT 1
                      FROM   AdLeadReqsReceived
                      WHERE  LeadId = @leadId
                      )
                BEGIN
                    IF NOT EXISTS (
                                  SELECT 1
                                  FROM   adEntrTestOverRide
                                  WHERE  LeadId = @leadId
                                         AND EntrTestId IN (
                                                           SELECT DocumentId
                                                           FROM   AdLeadReqsReceived
                                                           WHERE  LeadId = @leadId
                                                           )
                                  )
                        BEGIN
                            INSERT INTO adEntrTestOverRide (
                                                           EntrTestOverRideId
                                                          ,LeadId
                                                          ,EntrTestId
                                                          ,ModUser
                                                          ,ModDate
                                                          ,OverRide
                                                          ,StudentId
                                                           )
                                        SELECT NEWID()
                                              ,LeadId
                                              ,DocumentId
                                              ,ModUser
                                              ,ModDate
                                              ,Override
                                              ,NULL
                                        FROM   AdLeadReqsReceived
                                        WHERE  LeadId = @leadId;
                        END;
                END;
            -- fee
            IF EXISTS (
                      SELECT 1
                      FROM   adLeadTranReceived
                      WHERE  LeadId = @leadId
                      )
                BEGIN
                    IF NOT EXISTS (
                                  SELECT 1
                                  FROM   adEntrTestOverRide
                                  WHERE  LeadId = @leadId
                                         AND EntrTestId IN (
                                                           SELECT DocumentId
                                                           FROM   adLeadTranReceived
                                                           WHERE  LeadId = @leadId
                                                           )
                                  )
                        BEGIN
                            INSERT INTO adEntrTestOverRide (
                                                           EntrTestOverRideId
                                                          ,LeadId
                                                          ,EntrTestId
                                                          ,ModUser
                                                          ,ModDate
                                                          ,OverRide
                                                          ,StudentId
                                                           )
                                        SELECT NEWID()
                                              ,LeadId
                                              ,DocumentId
                                              ,ModUser
                                              ,ModDate
                                              ,Override
                                              ,NULL
                                        FROM   adLeadTranReceived
                                        WHERE  LeadId = @leadId;
                        END;
                END;
            FETCH NEXT FROM leadCur
            INTO @leadId;
        END;
    IF ( @error = 0 )
        BEGIN
            COMMIT TRANSACTION insertRows;
        END;
    ELSE
        BEGIN
            ROLLBACK TRANSACTION insertRows;
        END;
END TRY
BEGIN CATCH
    ROLLBACK TRANSACTION insertRows;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    RAISERROR(@msg, @severity, @state);
END CATCH;
GO
--====================================================================================
-- END US10753 - Update Requirement Overrides Correctly
--====================================================================================
--============================================================================================================
-- START DE13929 - Normal - Advantage - Rosedale - Students missing from "High School Leads-MS" AdHoc report
--============================================================================================================
-- step 1 create the view that contains all the address, phone and email related info in adhoc report for the lead
-- step 2 insert into syTables for the new view created
-- step 3 add the required relation for adleads and adleadsView in syRptAdhocRelations
-- step 4 change the links in syTblFlds to point to view instead of the adlead_address/adLeads_Phone/Adlead_email table for the adhoc report 
-- step 5 update the existing adhoc reports to point to newly generated tblfldId so that existing report point to new change
-- step 6 add the required relation for the ddl in syRptAdhocRelations
BEGIN TRANSACTION trAdhoc;
BEGIN TRY
    DECLARE @Error AS INTEGER;
    SET @Error = 0;

    -- insert into syTables for the view
    IF NOT EXISTS (
                  SELECT 1
                  FROM   syTables
                  WHERE  TblName = 'adLeadsView'
                  )
        BEGIN
            INSERT INTO syTables (
                                 TblId
                                ,TblName
                                ,TblDescrip
                                ,TblPK
                                 )
            VALUES (((
                     SELECT MAX(TblId)
                     FROM   syTables
                     ) + 1
                    )                                        -- TblId - int
                   ,'adLeadsView'                            -- TblName - varchar(50)
                   ,'View of AdLeads table for Adhoc Report' -- TblDescrip - varchar(100)
                   ,((
                     SELECT MAX(TblPK)
                     FROM   syTables
                     ) + 1
                    )                                        -- TblPK - int
                   );
            IF ( @@ERROR > 0 )
                BEGIN
                    SET @Error = 1;
                END;
            ELSE
                BEGIN
                    SET @Error = 0;
                END;
        END;

    -- add the required relation for adleads and adleadsView in syRptAdhocRelations
    IF NOT EXISTS (
                  SELECT 1
                  FROM   syRptAdhocRelations
                  WHERE  FkTable = 'adLeadsView'
                         AND FkColumn = 'LeadId'
                         AND PkTable = 'adLeads'
                         AND PkColumn = 'LeadId'
                  )
        BEGIN
            INSERT INTO syRptAdhocRelations (
                                            FkTable
                                           ,FkColumn
                                           ,PkTable
                                           ,PkColumn
                                            )
            VALUES ( N'adLeadsView' -- FkTable - nvarchar(100)
                    ,N'LeadId'      -- FkColumn - nvarchar(100)
                    ,N'adLeads'     -- PkTable - nvarchar(100)
                    ,N'LeadId'      -- PkColumn - nvarchar(100)
                   );
            IF ( @@ERROR > 0 )
                BEGIN
                    SET @Error = 1;
                END;
            ELSE
                BEGIN
                    SET @Error = 0;
                END;
        END;
    -- change the links in syTblFlds to point to view instead of the adlead_address/adLeads_Phone/Adlead_email table for the adhoc report 
    DECLARE @oldTblId AS INT
           ,@newTblId AS INT
           ,@fldId AS INT
           ,@categoryId AS INT
           ,@newTblFldId AS INT
           ,@oldTblFldId AS INT
           ,@fkColDes AS VARCHAR(50);
    SET @categoryId = (
                      SELECT CategoryId
                      FROM   syFldCategories
                      WHERE  EntityId = 395
                             AND Descrip = 'General Information'
                      );
    SET @newTblId = (
                    SELECT TblId
                    FROM   syTables
                    WHERE  TblName = 'adLeadsView'
                    );
    -- start AdLeadEmail
    -- email best
    -- Print 'sytblFld';
    -- Print 'Email_Best';
    SET @oldTblId = (
                    SELECT TblId
                    FROM   syTables
                    WHERE  TblName = 'AdLeadEmail'
                    );
    SET @fldId = (
                 SELECT FldId
                 FROM   syFields
                 WHERE  FldName = 'Email_Best'
                 );
    IF EXISTS (
              SELECT 1
              FROM   syTblFlds
              WHERE  TblId = @oldTblId
                     AND FldId = @fldId
              )
        BEGIN
            SET @oldTblFldId = (
                               SELECT TblFldsId
                               FROM   syTblFlds
                               WHERE  TblId = @oldTblId
                                      AND FldId = @fldId
                               );
            IF NOT EXISTS (
                          SELECT 1
                          FROM   syTblFlds
                          WHERE  TblId = @newTblId
                                 AND FldId = @fldId
                          )
                BEGIN
                    SET @newTblFldId = ((
                                        SELECT MAX(TblFldsId)
                                        FROM   syTblFlds
                                        ) + 1
                                       );
                    INSERT INTO syTblFlds (
                                          TblFldsId
                                         ,TblId
                                         ,FldId
                                         ,CategoryId
                                         ,FKColDescrip
                                          )
                    VALUES ( @newTblFldId -- TblFldsId - int
                            ,@newTblId    -- TblId - int
                            ,@fldId       -- FldId - int
                            ,@categoryId  -- CategoryId - int
                            ,NULL         -- FKColDescrip - varchar(50)
                           );
                    -- update the existing adhoc reports
                    IF EXISTS (
                              SELECT 1
                              FROM   syRptAdHocFields
                              WHERE  TblFldsId = @oldTblFldId
                              )
                        BEGIN
                            UPDATE syRptAdHocFields
                            SET    TblFldsId = @newTblFldId
                            WHERE  TblFldsId = @oldTblFldId;
                        END;
                END;
            IF (   (
                   SELECT CategoryId
                   FROM   syTblFlds
                   WHERE  TblFldsId = @oldTblFldId
                   ) IS NOT NULL
               )
                BEGIN
                    UPDATE syTblFlds
                    SET    CategoryId = NULL
                    WHERE  TblFldsId = @oldTblFldId;
                END;

        END;
    -- email
    --SET @oldTblId = (SELECT TblId FROM syTables WHERE TblName = 'AdLeadEmail')
    -- Print '[Email]';
    SET @fldId = (
                 SELECT FldId
                 FROM   syFields
                 WHERE  FldName = '[Email]'
                 );
    IF EXISTS (
              SELECT 1
              FROM   syTblFlds
              WHERE  TblId = @oldTblId
                     AND FldId = @fldId
              )
        BEGIN
            SET @oldTblFldId = (
                               SELECT TblFldsId
                               FROM   syTblFlds
                               WHERE  TblId = @oldTblId
                                      AND FldId = @fldId
                               );
            IF NOT EXISTS (
                          SELECT 1
                          FROM   syTblFlds
                          WHERE  TblId = @newTblId
                                 AND FldId = @fldId
                          )
                BEGIN
                    SET @newTblFldId = ((
                                        SELECT MAX(TblFldsId)
                                        FROM   syTblFlds
                                        ) + 1
                                       );
                    INSERT INTO syTblFlds (
                                          TblFldsId
                                         ,TblId
                                         ,FldId
                                         ,CategoryId
                                         ,FKColDescrip
                                          )
                    VALUES ( @newTblFldId -- TblFldsId - int
                            ,@newTblId    -- TblId - int
                            ,@fldId       -- FldId - int
                            ,@categoryId  -- CategoryId - int
                            ,NULL         -- FKColDescrip - varchar(50)
                           );
                    -- update the existing adhoc reports
                    IF EXISTS (
                              SELECT 1
                              FROM   syRptAdHocFields
                              WHERE  TblFldsId = @oldTblFldId
                              )
                        BEGIN
                            UPDATE syRptAdHocFields
                            SET    TblFldsId = @newTblFldId
                            WHERE  TblFldsId = @oldTblFldId;
                        END;
                END;
            IF (   (
                   SELECT CategoryId
                   FROM   syTblFlds
                   WHERE  TblFldsId = @oldTblFldId
                   ) IS NOT NULL
               )
                BEGIN
                    UPDATE syTblFlds
                    SET    CategoryId = NULL
                    WHERE  TblFldsId = @oldTblFldId;
                END;

        END;
    -- end AdLeadEmail
    -- start phone table
    -- Int'l
    -- Print 'IsForeignPhone';
    SET @oldTblId = (
                    SELECT TblId
                    FROM   syTables
                    WHERE  TblName = 'adLeadPhone'
                    );
    SET @fldId = (
                 SELECT FldId
                 FROM   syFields
                 WHERE  FldName = 'IsForeignPhone'
                 );
    IF EXISTS (
              SELECT 1
              FROM   syTblFlds
              WHERE  TblId = @oldTblId
                     AND FldId = @fldId
              )
        BEGIN
            SET @oldTblFldId = (
                               SELECT TblFldsId
                               FROM   syTblFlds
                               WHERE  TblId = @oldTblId
                                      AND FldId = @fldId
                               );
            IF NOT EXISTS (
                          SELECT 1
                          FROM   syTblFlds
                          WHERE  TblId = @newTblId
                                 AND FldId = @fldId
                          )
                BEGIN
                    SET @newTblFldId = ((
                                        SELECT MAX(TblFldsId)
                                        FROM   syTblFlds
                                        ) + 1
                                       );
                    INSERT INTO syTblFlds (
                                          TblFldsId
                                         ,TblId
                                         ,FldId
                                         ,CategoryId
                                         ,FKColDescrip
                                          )
                    VALUES ( @newTblFldId -- TblFldsId - int
                            ,@newTblId    -- TblId - int
                            ,@fldId       -- FldId - int
                            ,@categoryId  -- CategoryId - int
                            ,NULL         -- FKColDescrip - varchar(50)
                           );
                    -- update the existing adhoc reports
                    IF EXISTS (
                              SELECT 1
                              FROM   syRptAdHocFields
                              WHERE  TblFldsId = @oldTblFldId
                              )
                        BEGIN
                            UPDATE syRptAdHocFields
                            SET    TblFldsId = @newTblFldId
                            WHERE  TblFldsId = @oldTblFldId;
                        END;
                END;
            IF (   (
                   SELECT CategoryId
                   FROM   syTblFlds
                   WHERE  TblFldsId = @oldTblFldId
                   ) IS NOT NULL
               )
                BEGIN
                    UPDATE syTblFlds
                    SET    CategoryId = NULL
                    WHERE  TblFldsId = @oldTblFldId;
                END;

        END;
    -- Phone - Best
    -- Print 'Phone_Best';
    SET @fldId = (
                 SELECT FldId
                 FROM   syFields
                 WHERE  FldName = 'Phone_Best'
                 );
    IF EXISTS (
              SELECT 1
              FROM   syTblFlds
              WHERE  TblId = @oldTblId
                     AND FldId = @fldId
              )
        BEGIN
            SET @oldTblFldId = (
                               SELECT TblFldsId
                               FROM   syTblFlds
                               WHERE  TblId = @oldTblId
                                      AND FldId = @fldId
                               );
            IF NOT EXISTS (
                          SELECT 1
                          FROM   syTblFlds
                          WHERE  TblId = @newTblId
                                 AND FldId = @fldId
                          )
                BEGIN
                    SET @newTblFldId = ((
                                        SELECT MAX(TblFldsId)
                                        FROM   syTblFlds
                                        ) + 1
                                       );
                    INSERT INTO syTblFlds (
                                          TblFldsId
                                         ,TblId
                                         ,FldId
                                         ,CategoryId
                                         ,FKColDescrip
                                          )
                    VALUES ( @newTblFldId -- TblFldsId - int
                            ,@newTblId    -- TblId - int
                            ,@fldId       -- FldId - int
                            ,@categoryId  -- CategoryId - int
                            ,NULL         -- FKColDescrip - varchar(50)
                           );
                    -- update the existing adhoc reports
                    IF EXISTS (
                              SELECT 1
                              FROM   syRptAdHocFields
                              WHERE  TblFldsId = @oldTblFldId
                              )
                        BEGIN
                            UPDATE syRptAdHocFields
                            SET    TblFldsId = @newTblFldId
                            WHERE  TblFldsId = @oldTblFldId;
                        END;
                END;
            IF (   (
                   SELECT CategoryId
                   FROM   syTblFlds
                   WHERE  TblFldsId = @oldTblFldId
                   ) IS NOT NULL
               )
                BEGIN
                    UPDATE syTblFlds
                    SET    CategoryId = NULL
                    WHERE  TblFldsId = @oldTblFldId;
                END;

        END;
    -- Phone Type
    -- Print 'PhoneTypeId';
    SET @fldId = (
                 SELECT FldId
                 FROM   syFields
                 WHERE  FldName = 'PhoneTypeId'
                 );
    IF EXISTS (
              SELECT 1
              FROM   syTblFlds
              WHERE  TblId = @oldTblId
                     AND FldId = @fldId
              )
        BEGIN
            SET @oldTblFldId = (
                               SELECT TblFldsId
                               FROM   syTblFlds
                               WHERE  TblId = @oldTblId
                                      AND FldId = @fldId
                               );
            IF NOT EXISTS (
                          SELECT 1
                          FROM   syTblFlds
                          WHERE  TblId = @newTblId
                                 AND FldId = @fldId
                          )
                BEGIN
                    SET @newTblFldId = ((
                                        SELECT MAX(TblFldsId)
                                        FROM   syTblFlds
                                        ) + 1
                                       );
                    INSERT INTO syTblFlds (
                                          TblFldsId
                                         ,TblId
                                         ,FldId
                                         ,CategoryId
                                         ,FKColDescrip
                                          )
                    VALUES ( @newTblFldId -- TblFldsId - int
                            ,@newTblId    -- TblId - int
                            ,@fldId       -- FldId - int
                            ,@categoryId  -- CategoryId - int
                            ,NULL         -- FKColDescrip - varchar(50)
                           );
                    -- update the existing adhoc reports
                    IF EXISTS (
                              SELECT 1
                              FROM   syRptAdHocFields
                              WHERE  TblFldsId = @oldTblFldId
                              )
                        BEGIN
                            UPDATE syRptAdHocFields
                            SET    TblFldsId = @newTblFldId
                            WHERE  TblFldsId = @oldTblFldId;
                        END;
                END;
            IF (   (
                   SELECT CategoryId
                   FROM   syTblFlds
                   WHERE  TblFldsId = @oldTblFldId
                   ) IS NOT NULL
               )
                BEGIN
                    UPDATE syTblFlds
                    SET    CategoryId = NULL
                    WHERE  TblFldsId = @oldTblFldId;
                END;

        END;
    -- type
    -- Print 'PhoneType';
    SET @oldTblId = (
                    SELECT TblId
                    FROM   syTables
                    WHERE  TblName = 'adLeads'
                    );
    SET @fldId = (
                 SELECT FldId
                 FROM   syFields
                 WHERE  FldName = 'PhoneType'
                 );
    IF EXISTS (
              SELECT 1
              FROM   syTblFlds
              WHERE  TblId = @oldTblId
                     AND FldId = @fldId
              )
        BEGIN
            SET @oldTblFldId = (
                               SELECT TblFldsId
                               FROM   syTblFlds
                               WHERE  TblId = @oldTblId
                                      AND FldId = @fldId
                               );
            SET @fkColDes = (
                            SELECT FKColDescrip
                            FROM   syTblFlds
                            WHERE  TblFldsId = @oldTblFldId
                            );
            IF NOT EXISTS (
                          SELECT 1
                          FROM   syTblFlds
                          WHERE  TblId = @newTblId
                                 AND FldId = @fldId
                          )
                BEGIN
                    SET @newTblFldId = ((
                                        SELECT MAX(TblFldsId)
                                        FROM   syTblFlds
                                        ) + 1
                                       );
                    INSERT INTO syTblFlds (
                                          TblFldsId
                                         ,TblId
                                         ,FldId
                                         ,CategoryId
                                         ,FKColDescrip
                                          )
                    VALUES ( @newTblFldId -- TblFldsId - int
                            ,@newTblId    -- TblId - int
                            ,@fldId       -- FldId - int
                            ,@categoryId  -- CategoryId - int
                            ,@fkColDes    -- FKColDescrip - varchar(50)
                           );
                    -- update the existing adhoc reports
                    IF EXISTS (
                              SELECT 1
                              FROM   syRptAdHocFields
                              WHERE  TblFldsId = @oldTblFldId
                              )
                        BEGIN
                            UPDATE syRptAdHocFields
                            SET    TblFldsId = @newTblFldId
                            WHERE  TblFldsId = @oldTblFldId;
                        END;
                END;
            IF (   (
                   SELECT CategoryId
                   FROM   syTblFlds
                   WHERE  TblFldsId = @oldTblFldId
                   ) IS NOT NULL
               )
                BEGIN
                    UPDATE syTblFlds
                    SET    CategoryId = NULL
                          ,FKColDescrip = NULL
                    WHERE  TblFldsId = @oldTblFldId;
                END;

        END;
    -- end phone table
    -- start address table
    -- Address Type
    -- Print 'AddressTypeId';
    SET @oldTblId = (
                    SELECT TblId
                    FROM   syTables
                    WHERE  TblName = 'adLeadAddresses'
                    );
    SET @fldId = (
                 SELECT FldId
                 FROM   syFields
                 WHERE  FldName = 'AddressTypeId'
                 );
    IF EXISTS (
              SELECT 1
              FROM   syTblFlds
              WHERE  TblId = @oldTblId
                     AND FldId = @fldId
              )
        BEGIN
            SET @oldTblFldId = (
                               SELECT TblFldsId
                               FROM   syTblFlds
                               WHERE  TblId = @oldTblId
                                      AND FldId = @fldId
                               );
            SET @fkColDes = (
                            SELECT FKColDescrip
                            FROM   syTblFlds
                            WHERE  TblFldsId = @oldTblFldId
                            );
            IF NOT EXISTS (
                          SELECT 1
                          FROM   syTblFlds
                          WHERE  TblId = @newTblId
                                 AND FldId = @fldId
                          )
                BEGIN
                    SET @newTblFldId = ((
                                        SELECT MAX(TblFldsId)
                                        FROM   syTblFlds
                                        ) + 1
                                       );
                    INSERT INTO syTblFlds (
                                          TblFldsId
                                         ,TblId
                                         ,FldId
                                         ,CategoryId
                                         ,FKColDescrip
                                          )
                    VALUES ( @newTblFldId -- TblFldsId - int
                            ,@newTblId    -- TblId - int
                            ,@fldId       -- FldId - int
                            ,@categoryId  -- CategoryId - int
                            ,@fkColDes    -- FKColDescrip - varchar(50)
                           );
                    -- update the existing adhoc reports
                    IF EXISTS (
                              SELECT 1
                              FROM   syRptAdHocFields
                              WHERE  TblFldsId = @oldTblFldId
                              )
                        BEGIN
                            UPDATE syRptAdHocFields
                            SET    TblFldsId = @newTblFldId
                            WHERE  TblFldsId = @oldTblFldId;
                        END;
                END;
            IF (   (
                   SELECT CategoryId
                   FROM   syTblFlds
                   WHERE  TblFldsId = @oldTblFldId
                   ) IS NOT NULL
               )
                BEGIN
                    UPDATE syTblFlds
                    SET    CategoryId = NULL
                          ,FKColDescrip = NULL
                    WHERE  TblFldsId = @oldTblFldId;
                END;
        END;
    -- Address 1
    -- Print 'Address1';
    SET @fldId = (
                 SELECT FldId
                 FROM   syFields
                 WHERE  FldName = 'Address1'
                 );
    IF EXISTS (
              SELECT 1
              FROM   syTblFlds
              WHERE  TblId = @oldTblId
                     AND FldId = @fldId
              )
        BEGIN
            SET @oldTblFldId = (
                               SELECT TblFldsId
                               FROM   syTblFlds
                               WHERE  TblId = @oldTblId
                                      AND FldId = @fldId
                               );
            IF NOT EXISTS (
                          SELECT 1
                          FROM   syTblFlds
                          WHERE  TblId = @newTblId
                                 AND FldId = @fldId
                          )
                BEGIN
                    SET @newTblFldId = ((
                                        SELECT MAX(TblFldsId)
                                        FROM   syTblFlds
                                        ) + 1
                                       );
                    INSERT INTO syTblFlds (
                                          TblFldsId
                                         ,TblId
                                         ,FldId
                                         ,CategoryId
                                         ,FKColDescrip
                                          )
                    VALUES ( @newTblFldId -- TblFldsId - int
                            ,@newTblId    -- TblId - int
                            ,@fldId       -- FldId - int
                            ,@categoryId  -- CategoryId - int
                            ,NULL         -- FKColDescrip - varchar(50)
                           );
                    -- update the existing adhoc reports
                    IF EXISTS (
                              SELECT 1
                              FROM   syRptAdHocFields
                              WHERE  TblFldsId = @oldTblFldId
                              )
                        BEGIN
                            UPDATE syRptAdHocFields
                            SET    TblFldsId = @newTblFldId
                            WHERE  TblFldsId = @oldTblFldId;
                        END;
                END;
            IF (   (
                   SELECT CategoryId
                   FROM   syTblFlds
                   WHERE  TblFldsId = @oldTblFldId
                   ) IS NOT NULL
               )
                BEGIN
                    UPDATE syTblFlds
                    SET    CategoryId = NULL
                    WHERE  TblFldsId = @oldTblFldId;
                END;
        END;
    -- Address 2
    -- Print 'Address2';
    SET @fldId = (
                 SELECT FldId
                 FROM   syFields
                 WHERE  FldName = 'Address2'
                 );
    IF EXISTS (
              SELECT 1
              FROM   syTblFlds
              WHERE  TblId = @oldTblId
                     AND FldId = @fldId
              )
        BEGIN
            SET @oldTblFldId = (
                               SELECT TblFldsId
                               FROM   syTblFlds
                               WHERE  TblId = @oldTblId
                                      AND FldId = @fldId
                               );
            IF NOT EXISTS (
                          SELECT 1
                          FROM   syTblFlds
                          WHERE  TblId = @newTblId
                                 AND FldId = @fldId
                          )
                BEGIN
                    SET @newTblFldId = ((
                                        SELECT MAX(TblFldsId)
                                        FROM   syTblFlds
                                        ) + 1
                                       );
                    INSERT INTO syTblFlds (
                                          TblFldsId
                                         ,TblId
                                         ,FldId
                                         ,CategoryId
                                         ,FKColDescrip
                                          )
                    VALUES ( @newTblFldId -- TblFldsId - int
                            ,@newTblId    -- TblId - int
                            ,@fldId       -- FldId - int
                            ,@categoryId  -- CategoryId - int
                            ,NULL         -- FKColDescrip - varchar(50)
                           );
                    -- update the existing adhoc reports
                    IF EXISTS (
                              SELECT 1
                              FROM   syRptAdHocFields
                              WHERE  TblFldsId = @oldTblFldId
                              )
                        BEGIN
                            UPDATE syRptAdHocFields
                            SET    TblFldsId = @newTblFldId
                            WHERE  TblFldsId = @oldTblFldId;
                        END;
                END;
            IF (   (
                   SELECT CategoryId
                   FROM   syTblFlds
                   WHERE  TblFldsId = @oldTblFldId
                   ) IS NOT NULL
               )
                BEGIN
                    UPDATE syTblFlds
                    SET    CategoryId = NULL
                    WHERE  TblFldsId = @oldTblFldId;
                END;
        END;
    -- City
    -- Print 'City';
    SET @fldId = (
                 SELECT FldId
                 FROM   syFields
                 WHERE  FldName = 'City'
                 );
    IF EXISTS (
              SELECT 1
              FROM   syTblFlds
              WHERE  TblId = @oldTblId
                     AND FldId = @fldId
              )
        BEGIN
            SET @oldTblFldId = (
                               SELECT TblFldsId
                               FROM   syTblFlds
                               WHERE  TblId = @oldTblId
                                      AND FldId = @fldId
                               );
            IF NOT EXISTS (
                          SELECT 1
                          FROM   syTblFlds
                          WHERE  TblId = @newTblId
                                 AND FldId = @fldId
                          )
                BEGIN
                    SET @newTblFldId = ((
                                        SELECT MAX(TblFldsId)
                                        FROM   syTblFlds
                                        ) + 1
                                       );
                    INSERT INTO syTblFlds (
                                          TblFldsId
                                         ,TblId
                                         ,FldId
                                         ,CategoryId
                                         ,FKColDescrip
                                          )
                    VALUES ( @newTblFldId -- TblFldsId - int
                            ,@newTblId    -- TblId - int
                            ,@fldId       -- FldId - int
                            ,@categoryId  -- CategoryId - int
                            ,NULL         -- FKColDescrip - varchar(50)
                           );
                    -- update the existing adhoc reports
                    IF EXISTS (
                              SELECT 1
                              FROM   syRptAdHocFields
                              WHERE  TblFldsId = @oldTblFldId
                              )
                        BEGIN
                            UPDATE syRptAdHocFields
                            SET    TblFldsId = @newTblFldId
                            WHERE  TblFldsId = @oldTblFldId;
                        END;
                END;
            IF (   (
                   SELECT CategoryId
                   FROM   syTblFlds
                   WHERE  TblFldsId = @oldTblFldId
                   ) IS NOT NULL
               )
                BEGIN
                    UPDATE syTblFlds
                    SET    CategoryId = NULL
                    WHERE  TblFldsId = @oldTblFldId;
                END;
        END;
    -- stateId
    -- Print 'StateId';
    SET @fldId = (
                 SELECT FldId
                 FROM   syFields
                 WHERE  FldName = 'StateId'
                 );
    IF EXISTS (
              SELECT 1
              FROM   syTblFlds
              WHERE  TblId = @oldTblId
                     AND FldId = @fldId
              )
        BEGIN
            SET @oldTblFldId = (
                               SELECT TblFldsId
                               FROM   syTblFlds
                               WHERE  TblId = @oldTblId
                                      AND FldId = @fldId
                               );
            SET @fkColDes = (
                            SELECT FKColDescrip
                            FROM   syTblFlds
                            WHERE  TblFldsId = @oldTblFldId
                            );
            IF NOT EXISTS (
                          SELECT 1
                          FROM   syTblFlds
                          WHERE  TblId = @newTblId
                                 AND FldId = @fldId
                          )
                BEGIN
                    SET @newTblFldId = ((
                                        SELECT MAX(TblFldsId)
                                        FROM   syTblFlds
                                        ) + 1
                                       );
                    INSERT INTO syTblFlds (
                                          TblFldsId
                                         ,TblId
                                         ,FldId
                                         ,CategoryId
                                         ,FKColDescrip
                                          )
                    VALUES ( @newTblFldId -- TblFldsId - int
                            ,@newTblId    -- TblId - int
                            ,@fldId       -- FldId - int
                            ,@categoryId  -- CategoryId - int
                            ,@fkColDes    -- FKColDescrip - varchar(50)
                           );
                    -- update the existing adhoc reports
                    IF EXISTS (
                              SELECT 1
                              FROM   syRptAdHocFields
                              WHERE  TblFldsId = @oldTblFldId
                              )
                        BEGIN
                            UPDATE syRptAdHocFields
                            SET    TblFldsId = @newTblFldId
                            WHERE  TblFldsId = @oldTblFldId;
                        END;
                END;
            IF (   (
                   SELECT CategoryId
                   FROM   syTblFlds
                   WHERE  TblFldsId = @oldTblFldId
                   ) IS NOT NULL
               )
                BEGIN
                    UPDATE syTblFlds
                    SET    CategoryId = NULL
                          ,FKColDescrip = NULL
                    WHERE  TblFldsId = @oldTblFldId;
                END;
        END;
    --ZipCode
    -- Print 'ZipCode';
    SET @fldId = (
                 SELECT FldId
                 FROM   syFields
                 WHERE  FldName = 'ZipCode'
                 );
    IF EXISTS (
              SELECT 1
              FROM   syTblFlds
              WHERE  TblId = @oldTblId
                     AND FldId = @fldId
              )
        BEGIN
            SET @oldTblFldId = (
                               SELECT TblFldsId
                               FROM   syTblFlds
                               WHERE  TblId = @oldTblId
                                      AND FldId = @fldId
                               );
            IF NOT EXISTS (
                          SELECT 1
                          FROM   syTblFlds
                          WHERE  TblId = @newTblId
                                 AND FldId = @fldId
                          )
                BEGIN
                    SET @newTblFldId = ((
                                        SELECT MAX(TblFldsId)
                                        FROM   syTblFlds
                                        ) + 1
                                       );
                    INSERT INTO syTblFlds (
                                          TblFldsId
                                         ,TblId
                                         ,FldId
                                         ,CategoryId
                                         ,FKColDescrip
                                          )
                    VALUES ( @newTblFldId -- TblFldsId - int
                            ,@newTblId    -- TblId - int
                            ,@fldId       -- FldId - int
                            ,@categoryId  -- CategoryId - int
                            ,NULL         -- FKColDescrip - varchar(50)
                           );
                    -- update the existing adhoc reports
                    IF EXISTS (
                              SELECT 1
                              FROM   syRptAdHocFields
                              WHERE  TblFldsId = @oldTblFldId
                              )
                        BEGIN
                            UPDATE syRptAdHocFields
                            SET    TblFldsId = @newTblFldId
                            WHERE  TblFldsId = @oldTblFldId;
                        END;

                END;
            IF (   (
                   SELECT CategoryId
                   FROM   syTblFlds
                   WHERE  TblFldsId = @oldTblFldId
                   ) IS NOT NULL
               )
                BEGIN
                    UPDATE syTblFlds
                    SET    CategoryId = NULL
                    WHERE  TblFldsId = @oldTblFldId;
                END;
        END;
    --CountryId
    -- Print 'CountryId';
    SET @fldId = (
                 SELECT FldId
                 FROM   syFields
                 WHERE  FldName = 'CountryId'
                 );
    IF EXISTS (
              SELECT 1
              FROM   syTblFlds
              WHERE  TblId = @oldTblId
                     AND FldId = @fldId
              )
        BEGIN
            SET @oldTblFldId = (
                               SELECT TblFldsId
                               FROM   syTblFlds
                               WHERE  TblId = @oldTblId
                                      AND FldId = @fldId
                               );
            SET @fkColDes = (
                            SELECT FKColDescrip
                            FROM   syTblFlds
                            WHERE  TblFldsId = @oldTblFldId
                            );
            IF NOT EXISTS (
                          SELECT 1
                          FROM   syTblFlds
                          WHERE  TblId = @newTblId
                                 AND FldId = @fldId
                          )
                BEGIN
                    SET @newTblFldId = ((
                                        SELECT MAX(TblFldsId)
                                        FROM   syTblFlds
                                        ) + 1
                                       );
                    INSERT INTO syTblFlds (
                                          TblFldsId
                                         ,TblId
                                         ,FldId
                                         ,CategoryId
                                         ,FKColDescrip
                                          )
                    VALUES ( @newTblFldId -- TblFldsId - int
                            ,@newTblId    -- TblId - int
                            ,@fldId       -- FldId - int
                            ,@categoryId  -- CategoryId - int
                            ,@fkColDes    -- FKColDescrip - varchar(50)
                           );
                    -- update the existing adhoc reports
                    IF EXISTS (
                              SELECT 1
                              FROM   syRptAdHocFields
                              WHERE  TblFldsId = @oldTblFldId
                              )
                        BEGIN
                            UPDATE syRptAdHocFields
                            SET    TblFldsId = @newTblFldId
                            WHERE  TblFldsId = @oldTblFldId;
                        END;
                END;
            IF (   (
                   SELECT CategoryId
                   FROM   syTblFlds
                   WHERE  TblFldsId = @oldTblFldId
                   ) IS NOT NULL
               )
                BEGIN
                    UPDATE syTblFlds
                    SET    CategoryId = NULL
                          ,FKColDescrip = NULL
                    WHERE  TblFldsId = @oldTblFldId;
                END;
        END;
    --State - other state not listed
    -- Print 'State';
    SET @fldId = (
                 SELECT FldId
                 FROM   syFields
                 WHERE  FldName = 'State'
                        AND FldTypeId = 200
                 );
    IF EXISTS (
              SELECT 1
              FROM   syTblFlds
              WHERE  TblId = @oldTblId
                     AND FldId = @fldId
              )
        BEGIN
            SET @oldTblFldId = (
                               SELECT TblFldsId
                               FROM   syTblFlds
                               WHERE  TblId = @oldTblId
                                      AND FldId = @fldId
                               );
            IF NOT EXISTS (
                          SELECT 1
                          FROM   syTblFlds
                          WHERE  TblId = @newTblId
                                 AND FldId = @fldId
                          )
                BEGIN
                    SET @newTblFldId = ((
                                        SELECT MAX(TblFldsId)
                                        FROM   syTblFlds
                                        ) + 1
                                       );
                    INSERT INTO syTblFlds (
                                          TblFldsId
                                         ,TblId
                                         ,FldId
                                         ,CategoryId
                                         ,FKColDescrip
                                          )
                    VALUES ( @newTblFldId -- TblFldsId - int
                            ,@newTblId    -- TblId - int
                            ,@fldId       -- FldId - int
                            ,@categoryId  -- CategoryId - int
                            ,NULL         -- FKColDescrip - varchar(50)
                           );
                    -- update the existing adhoc reports
                    IF EXISTS (
                              SELECT 1
                              FROM   syRptAdHocFields
                              WHERE  TblFldsId = @oldTblFldId
                              )
                        BEGIN
                            UPDATE syRptAdHocFields
                            SET    TblFldsId = @newTblFldId
                            WHERE  TblFldsId = @oldTblFldId;
                        END;
                END;
            IF (   (
                   SELECT CategoryId
                   FROM   syTblFlds
                   WHERE  TblFldsId = @oldTblFldId
                   ) IS NOT NULL
               )
                BEGIN
                    UPDATE syTblFlds
                    SET    CategoryId = NULL
                    WHERE  TblFldsId = @oldTblFldId;
                END;
        END;
    -- IsInternational
    -- Print 'IsInternational';
    SET @fldId = (
                 SELECT FldId
                 FROM   syFields
                 WHERE  FldName = 'IsInternational'
                 );
    IF EXISTS (
              SELECT 1
              FROM   syTblFlds
              WHERE  TblId = @oldTblId
                     AND FldId = @fldId
              )
        BEGIN
            SET @oldTblFldId = (
                               SELECT TblFldsId
                               FROM   syTblFlds
                               WHERE  TblId = @oldTblId
                                      AND FldId = @fldId
                               );
            IF NOT EXISTS (
                          SELECT 1
                          FROM   syTblFlds
                          WHERE  TblId = @newTblId
                                 AND FldId = @fldId
                          )
                BEGIN
                    SET @newTblFldId = ((
                                        SELECT MAX(TblFldsId)
                                        FROM   syTblFlds
                                        ) + 1
                                       );
                    INSERT INTO syTblFlds (
                                          TblFldsId
                                         ,TblId
                                         ,FldId
                                         ,CategoryId
                                         ,FKColDescrip
                                          )
                    VALUES ( @newTblFldId -- TblFldsId - int
                            ,@newTblId    -- TblId - int
                            ,@fldId       -- FldId - int
                            ,@categoryId  -- CategoryId - int
                            ,NULL         -- FKColDescrip - varchar(50)
                           );
                    -- update the existing adhoc reports
                    IF EXISTS (
                              SELECT 1
                              FROM   syRptAdHocFields
                              WHERE  TblFldsId = @oldTblFldId
                              )
                        BEGIN
                            UPDATE syRptAdHocFields
                            SET    TblFldsId = @newTblFldId
                            WHERE  TblFldsId = @oldTblFldId;
                        END;
                END;
            IF (   (
                   SELECT CategoryId
                   FROM   syTblFlds
                   WHERE  TblFldsId = @oldTblFldId
                   ) IS NOT NULL
               )
                BEGIN
                    UPDATE syTblFlds
                    SET    CategoryId = NULL
                    WHERE  TblFldsId = @oldTblFldId;
                END;
        END;
    ---- CountyId
    -- Print 'CountyId';
    SET @fldId = (
                 SELECT FldId
                 FROM   syFields
                 WHERE  FldName = 'CountyId'
                 );
    IF EXISTS (
              SELECT 1
              FROM   syTblFlds
              WHERE  TblId = @oldTblId
                     AND FldId = @fldId
              )
        BEGIN
            SET @oldTblFldId = (
                               SELECT TblFldsId
                               FROM   syTblFlds
                               WHERE  TblId = @oldTblId
                                      AND FldId = @fldId
                               );
            SET @fkColDes = (
                            SELECT FKColDescrip
                            FROM   syTblFlds
                            WHERE  TblFldsId = @oldTblFldId
                            );
            IF NOT EXISTS (
                          SELECT 1
                          FROM   syTblFlds
                          WHERE  TblId = @newTblId
                                 AND FldId = @fldId
                          )
                BEGIN
                    SET @newTblFldId = ((
                                        SELECT MAX(TblFldsId)
                                        FROM   syTblFlds
                                        ) + 1
                                       );
                    INSERT INTO syTblFlds (
                                          TblFldsId
                                         ,TblId
                                         ,FldId
                                         ,CategoryId
                                         ,FKColDescrip
                                          )
                    VALUES ( @newTblFldId -- TblFldsId - int
                            ,@newTblId    -- TblId - int
                            ,@fldId       -- FldId - int
                            ,@categoryId  -- CategoryId - int
                            ,@fkColDes    -- FKColDescrip - varchar(50)
                           );
                    -- update the existing adhoc reports
                    IF EXISTS (
                              SELECT 1
                              FROM   syRptAdHocFields
                              WHERE  TblFldsId = @oldTblFldId
                              )
                        BEGIN
                            UPDATE syRptAdHocFields
                            SET    TblFldsId = @newTblFldId
                            WHERE  TblFldsId = @oldTblFldId;
                        END;
                END;
            IF (   (
                   SELECT CategoryId
                   FROM   syTblFlds
                   WHERE  TblFldsId = @oldTblFldId
                   ) IS NOT NULL
               )
                BEGIN
                    UPDATE syTblFlds
                    SET    CategoryId = NULL
                          ,FKColDescrip = NULL
                    WHERE  TblFldsId = @oldTblFldId;
                END;
        END;
    -- end lead address table
    -- relations of the columns with view
    -- view with state
    -- Print 'relations';
    -- Print 'StateId'
    IF NOT EXISTS (
                  SELECT 1
                  FROM   syRptAdhocRelations
                  WHERE  FkTable = 'adLeadsView'
                         AND FkColumn = 'StateId'
                         AND PkTable = 'syStates'
                         AND PkColumn = 'StateId'
                  )
        BEGIN
            INSERT INTO syRptAdhocRelations (
                                            FkTable
                                           ,FkColumn
                                           ,PkTable
                                           ,PkColumn
                                            )
            VALUES ( N'adLeadsView' -- FkTable - nvarchar(100)
                    ,N'StateId'     -- FkColumn - nvarchar(100)
                    ,N'syStates'    -- PkTable - nvarchar(100)
                    ,N'StateId'     -- PkColumn - nvarchar(100)
                   );
        END;
    -- view with PhoneType
    -- Print 'PhoneType'
    IF NOT EXISTS (
                  SELECT 1
                  FROM   syRptAdhocRelations
                  WHERE  FkTable = 'adLeadsView'
                         AND FkColumn = 'PhoneType'
                         AND PkTable = 'syPhoneType'
                         AND PkColumn = 'PhoneTypeId'
                  )
        BEGIN
            INSERT INTO syRptAdhocRelations (
                                            FkTable
                                           ,FkColumn
                                           ,PkTable
                                           ,PkColumn
                                            )
            VALUES ( N'adLeadsView' -- FkTable - nvarchar(100)
                    ,N'PhoneType'   -- FkColumn - nvarchar(100)
                    ,N'syPhoneType' -- PkTable - nvarchar(100)
                    ,N'PhoneTypeId' -- PkColumn - nvarchar(100)
                   );
        END;
    -- view with AddressTypeId   
    -- Print 'AddressTypeId'
    IF NOT EXISTS (
                  SELECT 1
                  FROM   syRptAdhocRelations
                  WHERE  FkTable = 'adLeadsView'
                         AND FkColumn = 'AddressTypeId'
                         AND PkTable = 'plAddressTypes'
                         AND PkColumn = 'AddressTypeId'
                  )
        BEGIN
            INSERT INTO syRptAdhocRelations (
                                            FkTable
                                           ,FkColumn
                                           ,PkTable
                                           ,PkColumn
                                            )
            VALUES ( N'adLeadsView'    -- FkTable - nvarchar(100)
                    ,N'AddressTypeId'  -- FkColumn - nvarchar(100)
                    ,N'plAddressTypes' -- PkTable - nvarchar(100)
                    ,N'AddressTypeId'  -- PkColumn - nvarchar(100)
                   );
        END;
    -- view with CountryId   
    -- Print 'CountryId'
    IF NOT EXISTS (
                  SELECT 1
                  FROM   syRptAdhocRelations
                  WHERE  FkTable = 'adLeadsView'
                         AND FkColumn = 'CountryId'
                         AND PkTable = 'adCountries'
                         AND PkColumn = 'CountryId'
                  )
        BEGIN
            INSERT INTO syRptAdhocRelations (
                                            FkTable
                                           ,FkColumn
                                           ,PkTable
                                           ,PkColumn
                                            )
            VALUES ( N'adLeadsView' -- FkTable - nvarchar(100)
                    ,N'CountryId'   -- FkColumn - nvarchar(100)
                    ,N'adCountries' -- PkTable - nvarchar(100)
                    ,N'CountryId'   -- PkColumn - nvarchar(100)
                   );
        END;
    -- view with CountyId   
    -- Print 'CountyId'
    IF NOT EXISTS (
                  SELECT 1
                  FROM   syRptAdhocRelations
                  WHERE  FkTable = 'adLeadsView'
                         AND FkColumn = 'CountyId'
                         AND PkTable = 'adCounties'
                         AND PkColumn = 'CountyId'
                  )
        BEGIN
            INSERT INTO syRptAdhocRelations (
                                            FkTable
                                           ,FkColumn
                                           ,PkTable
                                           ,PkColumn
                                            )
            VALUES ( N'adLeadsView' -- FkTable - nvarchar(100)
                    ,N'CountyId'    -- FkColumn - nvarchar(100)
                    ,N'adCounties'  -- PkTable - nvarchar(100)
                    ,N'CountyId'    -- PkColumn - nvarchar(100)
                   );
        END;
END TRY
BEGIN CATCH
    ROLLBACK TRANSACTION trAdhoc;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE()
          ,@severity = ERROR_SEVERITY()
          ,@state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @Error > 0
    BEGIN
        ROLLBACK TRANSACTION trAdhoc;
        PRINT 'Update fail';

    END;
ELSE
    BEGIN
        COMMIT TRANSACTION trAdhoc;
        PRINT 'Update successful';
    END;
GO
--============================================================================================================
-- END DE13929 - Normal - Advantage - Rosedale - Students missing from "High School Leads-MS" AdHoc report
--============================================================================================================
--============================================================================================================
--  US10900 -Correction of attendance tables to be used by Klass App
--============================================================================================================
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;
GO
BEGIN TRANSACTION UPDATE_Attendance;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
DISABLE TRIGGER TR_InsertAttendanceSummary_ByClass_Minutes ON atClsSectAttendance;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
DISABLE TRIGGER TR_InsertAttendanceSummary_ByClass_PA ON atClsSectAttendance;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
DISABLE TRIGGER TR_InsertClsSectionAttendance_Class_ClockHour ON atClsSectAttendance;
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
UPDATE ACSA
SET    ACSA.ModDate = ACSA.MeetDate
      ,ACSA.ModUser = 'support'
FROM   atClsSectAttendance AS ACSA
WHERE  ACSA.ModUser = '';
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
ENABLE TRIGGER TR_InsertAttendanceSummary_ByClass_Minutes ON atClsSectAttendance;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
ENABLE TRIGGER TR_InsertAttendanceSummary_ByClass_PA ON atClsSectAttendance;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
ENABLE TRIGGER TR_InsertClsSectionAttendance_Class_ClockHour ON atClsSectAttendance;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
UPDATE ACA
SET    ACA.ModDate = ACA.MeetDate
      ,ACA.ModUser = 'support'
FROM   atConversionAttendance AS ACA
WHERE  ACA.ModUser = '';
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
COMMIT TRANSACTION UPDATE_Attendance;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
DECLARE @Success AS BIT;
SET @Success = 1;
SET NOEXEC OFF;
IF ( @Success = 1 )
    PRINT 'The attendance table update succeeded';
ELSE
    BEGIN
        IF @@TRANCOUNT > 0
            ROLLBACK TRANSACTION UPDATE_Attendance;
        PRINT 'The attendance table update failed';
    END;
GO
--============================================================================================================
-- END  --  US10900 -Correction of attendance tables to be used by Klass App
--============================================================================================================

-- =================================================================================================
-- DE14045 QA: Leaving Blank "Best" email at Student info page and save,Creates an empty record at the student contact page.
-- Changes Made By: HC on 20170823
-- =================================================================================================
BEGIN TRANSACTION CleanEmtpyEmails;
BEGIN TRY

    DELETE FROM AdLeadEmail
    WHERE LEN(EMail) = 0;

END TRY
BEGIN CATCH
    SELECT ERROR_NUMBER() AS ErrorNumber
          ,ERROR_SEVERITY() AS ErrorSeverity
          ,ERROR_STATE() AS ErrorState
          ,ERROR_PROCEDURE() AS ErrorProcedure
          ,ERROR_LINE() AS ErrorLine
          ,ERROR_MESSAGE() AS ErrorMessage;

    IF @@TRANCOUNT > 0
        BEGIN
            ROLLBACK TRANSACTION CleanEmtpyEmails;
        END;
END CATCH;

IF @@TRANCOUNT > 0
    BEGIN
        COMMIT TRANSACTION CleanEmtpyEmails;
    END;

GO

-- =================================================================================================
-- END  --  DE14045 QA: Leaving Blank "Best" email at Student info page and save,Creates an empty record at the student contact page.
-- =================================================================================================
--=================================================================================================
-- US10971 REGRESSION: SAP Policy field need to be made as non required at the Enrollment page.
--=================================================================================================
BEGIN TRANSACTION deleteRow;
BEGIN TRY
    DECLARE @error AS INT;
    SET @error = 0;
    DECLARE @fldId INT;
    SET @fldId = (
                 SELECT FldId
                 FROM   syFields
                 WHERE  FldName = 'SAPId'
                 );
    IF EXISTS (
              SELECT t1.TblFldsId
                    ,t2.FldId
              FROM   syInstResFldsReq t1
                    ,syTblFlds t2
              WHERE  t1.TblFldsId = t2.TblFldsId
                     AND ResourceId = 169
                     AND FldId = @fldId
              )
        BEGIN
            DELETE FROM syInstResFldsReq
            WHERE ResReqFldId = (
                                SELECT ResReqFldId
                                FROM   syInstResFldsReq t1
                                      ,syTblFlds t2
                                WHERE  t1.TblFldsId = t2.TblFldsId
                                       AND ResourceId = 169
                                       AND FldId = @fldId
                                );
        END;
    IF ( @@ERROR > 0 )
        BEGIN
            SET @error = 1;
        END;
    ELSE
        BEGIN
            SET @error = 0;
        END;

END TRY
BEGIN CATCH
    ROLLBACK TRANSACTION deleteRow;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE()
          ,@severity = ERROR_SEVERITY()
          ,@state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
    BEGIN
        ROLLBACK TRANSACTION deleteRow;
        PRINT 'Update fail';

    END;
ELSE
    BEGIN
        COMMIT TRANSACTION deleteRow;
        PRINT 'Update successful';
    END;
GO
--=================================================================================================
-- END --  US10971 REGRESSION: SAP Policy field need to be made as non required at the Enrollment page.
--=================================================================================================

--=================================================================================================
-- DE14136 REGRESSION: SAP Check issues
--=================================================================================================
--Script requested by Andrei to set the SAPUsesCreditRanges config item to false
--This came up from testing DE14136:REGRESSION: SAP Check issues
-------------------------------------------------------------------------------------------------------------------------------------------------
BEGIN

    DECLARE @Error AS INTEGER;
    SET @Error = 0;

    BEGIN TRANSACTION UpdateConfigSetting;
    BEGIN TRY
        IF @Error = 0
            BEGIN
                UPDATE v
                SET    v.Value = 'False'
                FROM   dbo.syConfigAppSettings c
                INNER JOIN dbo.syConfigAppSetValues v ON c.SettingId = v.SettingId
                WHERE  KeyName = 'SAPUsesCreditRanges';

                IF ( @@ERROR > 0 )
                    BEGIN
                        SET @Error = @@ERROR;
                    END;
            END;

    END TRY
    BEGIN CATCH
        DECLARE @msg NVARCHAR(MAX);
        DECLARE @severity INT;
        DECLARE @state INT;
        SELECT @msg = ERROR_MESSAGE()
              ,@severity = ERROR_SEVERITY()
              ,@state = ERROR_STATE();
        RAISERROR(@msg, @severity, @state);
        SET @Error = 1;
    END CATCH;

    IF ( @Error = 0 )
        BEGIN
            COMMIT TRANSACTION UpdateConfigSetting;
        END;
    ELSE
        BEGIN
            ROLLBACK TRANSACTION UpdateConfigSetting;
        END;
END;
GO
--=================================================================================================
-- END  --  DE14136 REGRESSION: SAP Check issues
--=================================================================================================

-- ===============================================================================================
-- END  --   Consolidated Script Version 3.9
-- ===============================================================================================