﻿namespace AdvantageDatabaseUpdateForm
{
    partial class InstallStarterDatabaseForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TfDatabaseName = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel1 = new MaterialSkin.Controls.MaterialLabel();
            this.TfDatabaseDataLocation = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel2 = new MaterialSkin.Controls.MaterialLabel();
            this.TfDatabaseLogLocation = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel3 = new MaterialSkin.Controls.MaterialLabel();
            this.MrbRestoreDatabase = new MaterialSkin.Controls.MaterialRaisedButton();
            this.MrbOpenBackupLocation = new MaterialSkin.Controls.MaterialRaisedButton();
            this.MfbDataLocation = new MaterialSkin.Controls.MaterialFlatButton();
            this.MfbDatabaseLogLocation = new MaterialSkin.Controls.MaterialFlatButton();
            this.materialLabel4 = new MaterialSkin.Controls.MaterialLabel();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.materialLabel5 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel6 = new MaterialSkin.Controls.MaterialLabel();
            this.CbEnvironmentName = new System.Windows.Forms.ComboBox();
            this.materialLabel7 = new MaterialSkin.Controls.MaterialLabel();
            this.TfDatabaseNameTenant = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel8 = new MaterialSkin.Controls.MaterialLabel();
            this.MrbRestoreDatabaseTenant = new MaterialSkin.Controls.MaterialRaisedButton();
            this.TfTenantServerInstance = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel9 = new MaterialSkin.Controls.MaterialLabel();
            this.TfTenantUser = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel10 = new MaterialSkin.Controls.MaterialLabel();
            this.TfTenantPassword = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel11 = new MaterialSkin.Controls.MaterialLabel();
            this.MrbSetupTenantConnectionString = new MaterialSkin.Controls.MaterialRaisedButton();
            this.TfApiUrl = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel12 = new MaterialSkin.Controls.MaterialLabel();
            this.TfServiceUrl = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel13 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel14 = new MaterialSkin.Controls.MaterialLabel();
            this.MrbSafeUrl = new MaterialSkin.Controls.MaterialRaisedButton();
            this.TfSiteUrl = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel15 = new MaterialSkin.Controls.MaterialLabel();
            this.SuspendLayout();
            // 
            // TfDatabaseName
            // 
            this.TfDatabaseName.BackColor = System.Drawing.Color.White;
            this.TfDatabaseName.Depth = 0;
            this.TfDatabaseName.Hint = "";
            this.TfDatabaseName.Location = new System.Drawing.Point(264, 144);
            this.TfDatabaseName.MouseState = MaterialSkin.MouseState.HOVER;
            this.TfDatabaseName.Name = "TfDatabaseName";
            this.TfDatabaseName.PasswordChar = '\0';
            this.TfDatabaseName.SelectedText = "";
            this.TfDatabaseName.SelectionLength = 0;
            this.TfDatabaseName.SelectionStart = 0;
            this.TfDatabaseName.Size = new System.Drawing.Size(233, 23);
            this.TfDatabaseName.TabIndex = 3;
            this.TfDatabaseName.Text = "Advantage";
            this.TfDatabaseName.UseSystemPasswordChar = false;
            // 
            // materialLabel1
            // 
            this.materialLabel1.AutoSize = true;
            this.materialLabel1.BackColor = System.Drawing.Color.White;
            this.materialLabel1.Depth = 0;
            this.materialLabel1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.materialLabel1.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel1.Location = new System.Drawing.Point(12, 148);
            this.materialLabel1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel1.Name = "materialLabel1";
            this.materialLabel1.Size = new System.Drawing.Size(237, 19);
            this.materialLabel1.TabIndex = 2;
            this.materialLabel1.Text = "Advantage Client Database Name:";
            // 
            // TfDatabaseDataLocation
            // 
            this.TfDatabaseDataLocation.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TfDatabaseDataLocation.BackColor = System.Drawing.Color.White;
            this.TfDatabaseDataLocation.Depth = 0;
            this.TfDatabaseDataLocation.Enabled = false;
            this.TfDatabaseDataLocation.Hint = "";
            this.TfDatabaseDataLocation.Location = new System.Drawing.Point(203, 336);
            this.TfDatabaseDataLocation.MouseState = MaterialSkin.MouseState.HOVER;
            this.TfDatabaseDataLocation.Name = "TfDatabaseDataLocation";
            this.TfDatabaseDataLocation.PasswordChar = '\0';
            this.TfDatabaseDataLocation.SelectedText = "";
            this.TfDatabaseDataLocation.SelectionLength = 0;
            this.TfDatabaseDataLocation.SelectionStart = 0;
            this.TfDatabaseDataLocation.Size = new System.Drawing.Size(710, 23);
            this.TfDatabaseDataLocation.TabIndex = 5;
            this.TfDatabaseDataLocation.UseSystemPasswordChar = false;
            // 
            // materialLabel2
            // 
            this.materialLabel2.AutoSize = true;
            this.materialLabel2.BackColor = System.Drawing.Color.White;
            this.materialLabel2.Depth = 0;
            this.materialLabel2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.materialLabel2.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel2.Location = new System.Drawing.Point(21, 340);
            this.materialLabel2.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel2.Name = "materialLabel2";
            this.materialLabel2.Size = new System.Drawing.Size(174, 19);
            this.materialLabel2.TabIndex = 4;
            this.materialLabel2.Text = "Database Data Location:";
            // 
            // TfDatabaseLogLocation
            // 
            this.TfDatabaseLogLocation.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TfDatabaseLogLocation.BackColor = System.Drawing.Color.White;
            this.TfDatabaseLogLocation.Depth = 0;
            this.TfDatabaseLogLocation.Enabled = false;
            this.TfDatabaseLogLocation.Hint = "";
            this.TfDatabaseLogLocation.Location = new System.Drawing.Point(203, 425);
            this.TfDatabaseLogLocation.MouseState = MaterialSkin.MouseState.HOVER;
            this.TfDatabaseLogLocation.Name = "TfDatabaseLogLocation";
            this.TfDatabaseLogLocation.PasswordChar = '\0';
            this.TfDatabaseLogLocation.SelectedText = "";
            this.TfDatabaseLogLocation.SelectionLength = 0;
            this.TfDatabaseLogLocation.SelectionStart = 0;
            this.TfDatabaseLogLocation.Size = new System.Drawing.Size(710, 23);
            this.TfDatabaseLogLocation.TabIndex = 7;
            this.TfDatabaseLogLocation.UseSystemPasswordChar = false;
            // 
            // materialLabel3
            // 
            this.materialLabel3.AutoSize = true;
            this.materialLabel3.BackColor = System.Drawing.Color.White;
            this.materialLabel3.Depth = 0;
            this.materialLabel3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.materialLabel3.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel3.Location = new System.Drawing.Point(21, 429);
            this.materialLabel3.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel3.Name = "materialLabel3";
            this.materialLabel3.Size = new System.Drawing.Size(168, 19);
            this.materialLabel3.TabIndex = 6;
            this.materialLabel3.Text = "Database Log Location:";
            // 
            // MrbRestoreDatabase
            // 
            this.MrbRestoreDatabase.Depth = 0;
            this.MrbRestoreDatabase.Location = new System.Drawing.Point(18, 503);
            this.MrbRestoreDatabase.MouseState = MaterialSkin.MouseState.HOVER;
            this.MrbRestoreDatabase.Name = "MrbRestoreDatabase";
            this.MrbRestoreDatabase.Primary = true;
            this.MrbRestoreDatabase.Size = new System.Drawing.Size(191, 40);
            this.MrbRestoreDatabase.TabIndex = 12;
            this.MrbRestoreDatabase.Text = "1. Install Advantage Database ";
            this.MrbRestoreDatabase.UseVisualStyleBackColor = true;
            this.MrbRestoreDatabase.Click += new System.EventHandler(this.MrbRestoreDatabase_Click);
            // 
            // MrbOpenBackupLocation
            // 
            this.MrbOpenBackupLocation.Depth = 0;
            this.MrbOpenBackupLocation.Location = new System.Drawing.Point(878, 503);
            this.MrbOpenBackupLocation.MouseState = MaterialSkin.MouseState.HOVER;
            this.MrbOpenBackupLocation.Name = "MrbOpenBackupLocation";
            this.MrbOpenBackupLocation.Primary = true;
            this.MrbOpenBackupLocation.Size = new System.Drawing.Size(126, 40);
            this.MrbOpenBackupLocation.TabIndex = 13;
            this.MrbOpenBackupLocation.Text = "Open Backup Location";
            this.MrbOpenBackupLocation.UseVisualStyleBackColor = true;
            this.MrbOpenBackupLocation.Click += new System.EventHandler(this.MrbOpenBackupLocation_Click);
            // 
            // MfbDataLocation
            // 
            this.MfbDataLocation.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.MfbDataLocation.AutoSize = true;
            this.MfbDataLocation.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.MfbDataLocation.Depth = 0;
            this.MfbDataLocation.Location = new System.Drawing.Point(920, 323);
            this.MfbDataLocation.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.MfbDataLocation.MouseState = MaterialSkin.MouseState.HOVER;
            this.MfbDataLocation.Name = "MfbDataLocation";
            this.MfbDataLocation.Primary = false;
            this.MfbDataLocation.Size = new System.Drawing.Size(23, 36);
            this.MfbDataLocation.TabIndex = 38;
            this.MfbDataLocation.Text = "...";
            this.MfbDataLocation.UseVisualStyleBackColor = true;
            this.MfbDataLocation.Click += new System.EventHandler(this.MfbDataLocation_Click);
            // 
            // MfbDatabaseLogLocation
            // 
            this.MfbDatabaseLogLocation.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.MfbDatabaseLogLocation.AutoSize = true;
            this.MfbDatabaseLogLocation.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.MfbDatabaseLogLocation.Depth = 0;
            this.MfbDatabaseLogLocation.Location = new System.Drawing.Point(920, 413);
            this.MfbDatabaseLogLocation.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.MfbDatabaseLogLocation.MouseState = MaterialSkin.MouseState.HOVER;
            this.MfbDatabaseLogLocation.Name = "MfbDatabaseLogLocation";
            this.MfbDatabaseLogLocation.Primary = false;
            this.MfbDatabaseLogLocation.Size = new System.Drawing.Size(23, 36);
            this.MfbDatabaseLogLocation.TabIndex = 39;
            this.MfbDatabaseLogLocation.Text = "...";
            this.MfbDatabaseLogLocation.UseVisualStyleBackColor = true;
            this.MfbDatabaseLogLocation.Click += new System.EventHandler(this.MfbDatabaseLogLocation_Click);
            // 
            // materialLabel4
            // 
            this.materialLabel4.AutoSize = true;
            this.materialLabel4.BackColor = System.Drawing.Color.White;
            this.materialLabel4.Depth = 0;
            this.materialLabel4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.materialLabel4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold);
            this.materialLabel4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel4.Location = new System.Drawing.Point(12, 280);
            this.materialLabel4.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel4.Name = "materialLabel4";
            this.materialLabel4.Size = new System.Drawing.Size(650, 18);
            this.materialLabel4.TabIndex = 40;
            this.materialLabel4.Text = "Specify where the Database files will be restored. Specify the folder for Data an" +
    "d Log:";
            // 
            // folderBrowserDialog1
            // 
            this.folderBrowserDialog1.SelectedPath = "C:\\Program Files\\Microsoft SQL Server";
            // 
            // materialLabel5
            // 
            this.materialLabel5.AutoSize = true;
            this.materialLabel5.BackColor = System.Drawing.Color.White;
            this.materialLabel5.Depth = 0;
            this.materialLabel5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.materialLabel5.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel5.Location = new System.Drawing.Point(205, 365);
            this.materialLabel5.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel5.Name = "materialLabel5";
            this.materialLabel5.Size = new System.Drawing.Size(555, 19);
            this.materialLabel5.TabIndex = 41;
            this.materialLabel5.Text = "Ej: C:\\Program Files\\Microsoft SQL Server\\MSSQL14.SQLDEV2016\\MSSQL\\DATA";
            // 
            // materialLabel6
            // 
            this.materialLabel6.AutoSize = true;
            this.materialLabel6.BackColor = System.Drawing.Color.White;
            this.materialLabel6.Depth = 0;
            this.materialLabel6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.materialLabel6.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel6.Location = new System.Drawing.Point(205, 455);
            this.materialLabel6.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel6.Name = "materialLabel6";
            this.materialLabel6.Size = new System.Drawing.Size(544, 19);
            this.materialLabel6.TabIndex = 42;
            this.materialLabel6.Text = "Ej: C:\\Program Files\\Microsoft SQL Server\\MSSQL14.SQLDEV2016\\MSSQL\\LOG";
            // 
            // CbEnvironmentName
            // 
            this.CbEnvironmentName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CbEnvironmentName.FormattingEnabled = true;
            this.CbEnvironmentName.Location = new System.Drawing.Point(164, 88);
            this.CbEnvironmentName.Name = "CbEnvironmentName";
            this.CbEnvironmentName.Size = new System.Drawing.Size(233, 21);
            this.CbEnvironmentName.TabIndex = 44;
            this.CbEnvironmentName.SelectedIndexChanged += new System.EventHandler(this.CbEnvironmentName_SelectedIndexChanged);
            // 
            // materialLabel7
            // 
            this.materialLabel7.AutoSize = true;
            this.materialLabel7.BackColor = System.Drawing.Color.White;
            this.materialLabel7.Depth = 0;
            this.materialLabel7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.materialLabel7.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold);
            this.materialLabel7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel7.Location = new System.Drawing.Point(5, 90);
            this.materialLabel7.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel7.Name = "materialLabel7";
            this.materialLabel7.Size = new System.Drawing.Size(159, 18);
            this.materialLabel7.TabIndex = 43;
            this.materialLabel7.Text = "Select Environment:";
            // 
            // TfDatabaseNameTenant
            // 
            this.TfDatabaseNameTenant.BackColor = System.Drawing.Color.White;
            this.TfDatabaseNameTenant.Depth = 0;
            this.TfDatabaseNameTenant.Hint = "";
            this.TfDatabaseNameTenant.Location = new System.Drawing.Point(264, 188);
            this.TfDatabaseNameTenant.MouseState = MaterialSkin.MouseState.HOVER;
            this.TfDatabaseNameTenant.Name = "TfDatabaseNameTenant";
            this.TfDatabaseNameTenant.PasswordChar = '\0';
            this.TfDatabaseNameTenant.SelectedText = "";
            this.TfDatabaseNameTenant.SelectionLength = 0;
            this.TfDatabaseNameTenant.SelectionStart = 0;
            this.TfDatabaseNameTenant.Size = new System.Drawing.Size(233, 23);
            this.TfDatabaseNameTenant.TabIndex = 46;
            this.TfDatabaseNameTenant.Text = "TenantAuthDb";
            this.TfDatabaseNameTenant.UseSystemPasswordChar = false;
            // 
            // materialLabel8
            // 
            this.materialLabel8.AutoSize = true;
            this.materialLabel8.BackColor = System.Drawing.Color.White;
            this.materialLabel8.Depth = 0;
            this.materialLabel8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.materialLabel8.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel8.Location = new System.Drawing.Point(12, 192);
            this.materialLabel8.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel8.Name = "materialLabel8";
            this.materialLabel8.Size = new System.Drawing.Size(205, 19);
            this.materialLabel8.TabIndex = 45;
            this.materialLabel8.Text = "Tenant Auth Database Name:";
            // 
            // MrbRestoreDatabaseTenant
            // 
            this.MrbRestoreDatabaseTenant.Depth = 0;
            this.MrbRestoreDatabaseTenant.Location = new System.Drawing.Point(236, 503);
            this.MrbRestoreDatabaseTenant.MouseState = MaterialSkin.MouseState.HOVER;
            this.MrbRestoreDatabaseTenant.Name = "MrbRestoreDatabaseTenant";
            this.MrbRestoreDatabaseTenant.Primary = true;
            this.MrbRestoreDatabaseTenant.Size = new System.Drawing.Size(191, 40);
            this.MrbRestoreDatabaseTenant.TabIndex = 47;
            this.MrbRestoreDatabaseTenant.Text = "2. Install TenantAuthDb Database ";
            this.MrbRestoreDatabaseTenant.UseVisualStyleBackColor = true;
            this.MrbRestoreDatabaseTenant.Click += new System.EventHandler(this.MrbRestoreDatabaseTenant_Click);
            // 
            // TfTenantServerInstance
            // 
            this.TfTenantServerInstance.BackColor = System.Drawing.Color.White;
            this.TfTenantServerInstance.Depth = 0;
            this.TfTenantServerInstance.Hint = "";
            this.TfTenantServerInstance.Location = new System.Drawing.Point(771, 144);
            this.TfTenantServerInstance.MouseState = MaterialSkin.MouseState.HOVER;
            this.TfTenantServerInstance.Name = "TfTenantServerInstance";
            this.TfTenantServerInstance.PasswordChar = '\0';
            this.TfTenantServerInstance.SelectedText = "";
            this.TfTenantServerInstance.SelectionLength = 0;
            this.TfTenantServerInstance.SelectionStart = 0;
            this.TfTenantServerInstance.Size = new System.Drawing.Size(233, 23);
            this.TfTenantServerInstance.TabIndex = 49;
            this.TfTenantServerInstance.UseSystemPasswordChar = false;
            // 
            // materialLabel9
            // 
            this.materialLabel9.AutoSize = true;
            this.materialLabel9.BackColor = System.Drawing.Color.White;
            this.materialLabel9.Depth = 0;
            this.materialLabel9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.materialLabel9.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel9.Location = new System.Drawing.Point(519, 148);
            this.materialLabel9.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel9.Name = "materialLabel9";
            this.materialLabel9.Size = new System.Drawing.Size(166, 19);
            this.materialLabel9.TabIndex = 48;
            this.materialLabel9.Text = "Tenant Server Instance:";
            // 
            // TfTenantUser
            // 
            this.TfTenantUser.BackColor = System.Drawing.Color.White;
            this.TfTenantUser.Depth = 0;
            this.TfTenantUser.Hint = "";
            this.TfTenantUser.Location = new System.Drawing.Point(771, 189);
            this.TfTenantUser.MouseState = MaterialSkin.MouseState.HOVER;
            this.TfTenantUser.Name = "TfTenantUser";
            this.TfTenantUser.PasswordChar = '\0';
            this.TfTenantUser.SelectedText = "";
            this.TfTenantUser.SelectionLength = 0;
            this.TfTenantUser.SelectionStart = 0;
            this.TfTenantUser.Size = new System.Drawing.Size(233, 23);
            this.TfTenantUser.TabIndex = 51;
            this.TfTenantUser.Text = "AdvUser";
            this.TfTenantUser.UseSystemPasswordChar = false;
            // 
            // materialLabel10
            // 
            this.materialLabel10.AutoSize = true;
            this.materialLabel10.BackColor = System.Drawing.Color.White;
            this.materialLabel10.Depth = 0;
            this.materialLabel10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.materialLabel10.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel10.Location = new System.Drawing.Point(519, 193);
            this.materialLabel10.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel10.Name = "materialLabel10";
            this.materialLabel10.Size = new System.Drawing.Size(94, 19);
            this.materialLabel10.TabIndex = 50;
            this.materialLabel10.Text = "Tenant User:";
            // 
            // TfTenantPassword
            // 
            this.TfTenantPassword.BackColor = System.Drawing.Color.White;
            this.TfTenantPassword.Depth = 0;
            this.TfTenantPassword.Hint = "";
            this.TfTenantPassword.Location = new System.Drawing.Point(771, 231);
            this.TfTenantPassword.MouseState = MaterialSkin.MouseState.HOVER;
            this.TfTenantPassword.Name = "TfTenantPassword";
            this.TfTenantPassword.PasswordChar = '\0';
            this.TfTenantPassword.SelectedText = "";
            this.TfTenantPassword.SelectionLength = 0;
            this.TfTenantPassword.SelectionStart = 0;
            this.TfTenantPassword.Size = new System.Drawing.Size(233, 23);
            this.TfTenantPassword.TabIndex = 53;
            this.TfTenantPassword.UseSystemPasswordChar = false;
            // 
            // materialLabel11
            // 
            this.materialLabel11.AutoSize = true;
            this.materialLabel11.BackColor = System.Drawing.Color.White;
            this.materialLabel11.Depth = 0;
            this.materialLabel11.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.materialLabel11.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel11.Location = new System.Drawing.Point(519, 235);
            this.materialLabel11.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel11.Name = "materialLabel11";
            this.materialLabel11.Size = new System.Drawing.Size(129, 19);
            this.materialLabel11.TabIndex = 52;
            this.materialLabel11.Text = "Tenant Password:";
            // 
            // MrbSetupTenantConnectionString
            // 
            this.MrbSetupTenantConnectionString.Depth = 0;
            this.MrbSetupTenantConnectionString.Location = new System.Drawing.Point(444, 503);
            this.MrbSetupTenantConnectionString.MouseState = MaterialSkin.MouseState.HOVER;
            this.MrbSetupTenantConnectionString.Name = "MrbSetupTenantConnectionString";
            this.MrbSetupTenantConnectionString.Primary = true;
            this.MrbSetupTenantConnectionString.Size = new System.Drawing.Size(191, 40);
            this.MrbSetupTenantConnectionString.TabIndex = 54;
            this.MrbSetupTenantConnectionString.Text = "3. Setup Tenant Starter Connection String";
            this.MrbSetupTenantConnectionString.UseVisualStyleBackColor = true;
            this.MrbSetupTenantConnectionString.Click += new System.EventHandler(this.MrbSetupTenantConnectionString_Click);
            // 
            // TfApiUrl
            // 
            this.TfApiUrl.BackColor = System.Drawing.Color.White;
            this.TfApiUrl.Depth = 0;
            this.TfApiUrl.Hint = "";
            this.TfApiUrl.Location = new System.Drawing.Point(209, 622);
            this.TfApiUrl.MouseState = MaterialSkin.MouseState.HOVER;
            this.TfApiUrl.Name = "TfApiUrl";
            this.TfApiUrl.PasswordChar = '\0';
            this.TfApiUrl.SelectedText = "";
            this.TfApiUrl.SelectionLength = 0;
            this.TfApiUrl.SelectionStart = 0;
            this.TfApiUrl.Size = new System.Drawing.Size(622, 23);
            this.TfApiUrl.TabIndex = 56;
            this.TfApiUrl.Text = "http://localhost/advantage/current/API/API";
            this.TfApiUrl.UseSystemPasswordChar = false;
            // 
            // materialLabel12
            // 
            this.materialLabel12.AutoSize = true;
            this.materialLabel12.BackColor = System.Drawing.Color.White;
            this.materialLabel12.Depth = 0;
            this.materialLabel12.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.materialLabel12.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel12.Location = new System.Drawing.Point(7, 622);
            this.materialLabel12.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel12.Name = "materialLabel12";
            this.materialLabel12.Size = new System.Drawing.Size(137, 19);
            this.materialLabel12.TabIndex = 55;
            this.materialLabel12.Text = "Advantage API URL";
            // 
            // TfServiceUrl
            // 
            this.TfServiceUrl.BackColor = System.Drawing.Color.White;
            this.TfServiceUrl.Depth = 0;
            this.TfServiceUrl.Hint = "";
            this.TfServiceUrl.Location = new System.Drawing.Point(209, 661);
            this.TfServiceUrl.MouseState = MaterialSkin.MouseState.HOVER;
            this.TfServiceUrl.Name = "TfServiceUrl";
            this.TfServiceUrl.PasswordChar = '\0';
            this.TfServiceUrl.SelectedText = "";
            this.TfServiceUrl.SelectionLength = 0;
            this.TfServiceUrl.SelectionStart = 0;
            this.TfServiceUrl.Size = new System.Drawing.Size(622, 23);
            this.TfServiceUrl.TabIndex = 58;
            this.TfServiceUrl.Text = "http://localhost/advantage/current/Services/";
            this.TfServiceUrl.UseSystemPasswordChar = false;
            // 
            // materialLabel13
            // 
            this.materialLabel13.AutoSize = true;
            this.materialLabel13.BackColor = System.Drawing.Color.White;
            this.materialLabel13.Depth = 0;
            this.materialLabel13.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.materialLabel13.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel13.Location = new System.Drawing.Point(7, 665);
            this.materialLabel13.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel13.Name = "materialLabel13";
            this.materialLabel13.Size = new System.Drawing.Size(196, 19);
            this.materialLabel13.TabIndex = 57;
            this.materialLabel13.Text = "Advantage Web Service URL";
            // 
            // materialLabel14
            // 
            this.materialLabel14.AutoSize = true;
            this.materialLabel14.BackColor = System.Drawing.Color.White;
            this.materialLabel14.Depth = 0;
            this.materialLabel14.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.materialLabel14.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold);
            this.materialLabel14.ForeColor = System.Drawing.Color.DodgerBlue;
            this.materialLabel14.Location = new System.Drawing.Point(11, 582);
            this.materialLabel14.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel14.Name = "materialLabel14";
            this.materialLabel14.Size = new System.Drawing.Size(471, 18);
            this.materialLabel14.TabIndex = 59;
            this.materialLabel14.Text = "Update the Configuration App Keys for API and Service URLs:";
            // 
            // MrbSafeUrl
            // 
            this.MrbSafeUrl.Depth = 0;
            this.MrbSafeUrl.Location = new System.Drawing.Point(858, 737);
            this.MrbSafeUrl.MouseState = MaterialSkin.MouseState.HOVER;
            this.MrbSafeUrl.Name = "MrbSafeUrl";
            this.MrbSafeUrl.Primary = true;
            this.MrbSafeUrl.Size = new System.Drawing.Size(146, 40);
            this.MrbSafeUrl.TabIndex = 60;
            this.MrbSafeUrl.Text = "4. Save URLs";
            this.MrbSafeUrl.UseVisualStyleBackColor = true;
            this.MrbSafeUrl.Click += new System.EventHandler(this.MrbSafeUrl_Click);
            // 
            // TfSiteUrl
            // 
            this.TfSiteUrl.BackColor = System.Drawing.Color.White;
            this.TfSiteUrl.Depth = 0;
            this.TfSiteUrl.Hint = "";
            this.TfSiteUrl.Location = new System.Drawing.Point(209, 701);
            this.TfSiteUrl.MouseState = MaterialSkin.MouseState.HOVER;
            this.TfSiteUrl.Name = "TfSiteUrl";
            this.TfSiteUrl.PasswordChar = '\0';
            this.TfSiteUrl.SelectedText = "";
            this.TfSiteUrl.SelectionLength = 0;
            this.TfSiteUrl.SelectionStart = 0;
            this.TfSiteUrl.Size = new System.Drawing.Size(622, 23);
            this.TfSiteUrl.TabIndex = 62;
            this.TfSiteUrl.Text = "http://localhost/advantage/current/Site/";
            this.TfSiteUrl.UseSystemPasswordChar = false;
            // 
            // materialLabel15
            // 
            this.materialLabel15.AutoSize = true;
            this.materialLabel15.BackColor = System.Drawing.Color.White;
            this.materialLabel15.Depth = 0;
            this.materialLabel15.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.materialLabel15.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel15.Location = new System.Drawing.Point(7, 701);
            this.materialLabel15.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel15.Name = "materialLabel15";
            this.materialLabel15.Size = new System.Drawing.Size(173, 19);
            this.materialLabel15.TabIndex = 61;
            this.materialLabel15.Text = "Advantage Web Site URL";
            // 
            // InstallStarterDatabaseForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1042, 802);
            this.Controls.Add(this.TfSiteUrl);
            this.Controls.Add(this.materialLabel15);
            this.Controls.Add(this.MrbSafeUrl);
            this.Controls.Add(this.materialLabel14);
            this.Controls.Add(this.TfServiceUrl);
            this.Controls.Add(this.materialLabel13);
            this.Controls.Add(this.TfApiUrl);
            this.Controls.Add(this.materialLabel12);
            this.Controls.Add(this.MrbSetupTenantConnectionString);
            this.Controls.Add(this.TfTenantPassword);
            this.Controls.Add(this.materialLabel11);
            this.Controls.Add(this.TfTenantUser);
            this.Controls.Add(this.materialLabel10);
            this.Controls.Add(this.TfTenantServerInstance);
            this.Controls.Add(this.materialLabel9);
            this.Controls.Add(this.MrbRestoreDatabaseTenant);
            this.Controls.Add(this.TfDatabaseNameTenant);
            this.Controls.Add(this.materialLabel8);
            this.Controls.Add(this.CbEnvironmentName);
            this.Controls.Add(this.materialLabel7);
            this.Controls.Add(this.materialLabel6);
            this.Controls.Add(this.materialLabel5);
            this.Controls.Add(this.materialLabel4);
            this.Controls.Add(this.MfbDatabaseLogLocation);
            this.Controls.Add(this.MfbDataLocation);
            this.Controls.Add(this.MrbOpenBackupLocation);
            this.Controls.Add(this.MrbRestoreDatabase);
            this.Controls.Add(this.TfDatabaseLogLocation);
            this.Controls.Add(this.materialLabel3);
            this.Controls.Add(this.TfDatabaseDataLocation);
            this.Controls.Add(this.materialLabel2);
            this.Controls.Add(this.TfDatabaseName);
            this.Controls.Add(this.materialLabel1);
            this.Name = "InstallStarterDatabaseForm";
            this.Text = "Install the Starter Advantage Database";
            this.Load += new System.EventHandler(this.InstallStarterDatabaseForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MaterialSkin.Controls.MaterialSingleLineTextField TfDatabaseName;
        private MaterialSkin.Controls.MaterialLabel materialLabel1;
        private MaterialSkin.Controls.MaterialSingleLineTextField TfDatabaseDataLocation;
        private MaterialSkin.Controls.MaterialLabel materialLabel2;
        private MaterialSkin.Controls.MaterialSingleLineTextField TfDatabaseLogLocation;
        private MaterialSkin.Controls.MaterialLabel materialLabel3;
        private MaterialSkin.Controls.MaterialRaisedButton MrbRestoreDatabase;
        private MaterialSkin.Controls.MaterialRaisedButton MrbOpenBackupLocation;
        private MaterialSkin.Controls.MaterialFlatButton MfbDataLocation;
        private MaterialSkin.Controls.MaterialFlatButton MfbDatabaseLogLocation;
        private MaterialSkin.Controls.MaterialLabel materialLabel4;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private MaterialSkin.Controls.MaterialLabel materialLabel5;
        private MaterialSkin.Controls.MaterialLabel materialLabel6;
        private System.Windows.Forms.ComboBox CbEnvironmentName;
        private MaterialSkin.Controls.MaterialLabel materialLabel7;
        private MaterialSkin.Controls.MaterialSingleLineTextField TfDatabaseNameTenant;
        private MaterialSkin.Controls.MaterialLabel materialLabel8;
        private MaterialSkin.Controls.MaterialRaisedButton MrbRestoreDatabaseTenant;
        private MaterialSkin.Controls.MaterialSingleLineTextField TfTenantServerInstance;
        private MaterialSkin.Controls.MaterialLabel materialLabel9;
        private MaterialSkin.Controls.MaterialSingleLineTextField TfTenantUser;
        private MaterialSkin.Controls.MaterialLabel materialLabel10;
        private MaterialSkin.Controls.MaterialSingleLineTextField TfTenantPassword;
        private MaterialSkin.Controls.MaterialLabel materialLabel11;
        private MaterialSkin.Controls.MaterialRaisedButton MrbSetupTenantConnectionString;
        private MaterialSkin.Controls.MaterialSingleLineTextField TfApiUrl;
        private MaterialSkin.Controls.MaterialLabel materialLabel12;
        private MaterialSkin.Controls.MaterialSingleLineTextField TfServiceUrl;
        private MaterialSkin.Controls.MaterialLabel materialLabel13;
        private MaterialSkin.Controls.MaterialLabel materialLabel14;
        private MaterialSkin.Controls.MaterialRaisedButton MrbSafeUrl;
        private MaterialSkin.Controls.MaterialSingleLineTextField TfSiteUrl;
        private MaterialSkin.Controls.MaterialLabel materialLabel15;
    }
}