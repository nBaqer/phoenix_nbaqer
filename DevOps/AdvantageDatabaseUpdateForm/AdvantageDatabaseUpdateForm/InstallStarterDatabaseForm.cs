﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="InstallStarterDatabaseForm.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The install starter database form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AdvantageDatabaseUpdateForm
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Windows.Forms;

    using AdvantageDatabaseUpdateForm.Common;
    using AdvantageDatabaseUpdateForm.Data;

    using Newtonsoft.Json;

    /// <summary>
    /// The install starter database form.
    /// </summary>
    public partial class InstallStarterDatabaseForm : MaterialSkin.Controls.MaterialForm
    {
        private List<Models.Environment> environments;

        /// <summary>
        /// Initializes a new instance of the <see cref="InstallStarterDatabaseForm"/> class.
        /// </summary>
        public InstallStarterDatabaseForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// The open backup location_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void MrbOpenBackupLocation_Click(object sender, EventArgs e)
        {
            Executable.Execute("explorer.exe ", Executable.GetCurrentPath() + "\\Resources\\StarterDatabase\\");
        }

        /// <summary>
        /// The restore database_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void MrbRestoreDatabase_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(this.TfDatabaseName.Text)
                    || string.IsNullOrEmpty(this.TfDatabaseDataLocation.Text)
                    || string.IsNullOrEmpty(this.TfDatabaseLogLocation.Text))
                {
                    MessageBox.Show(
                        "Database Data Location and Database Log Location are required.",
                        "Missing Required parameter",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                }
                else if (this.CbEnvironmentName.SelectedItem == null)
                {
                    MessageBox.Show(
                        "An environment is required .",
                        "Missing Required parameter",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                }
                else
                {
                    var environment = this.environments.FirstOrDefault(
                        x => x.Name == this.CbEnvironmentName.SelectedItem.ToString());
                    if (MessageBox.Show(
                            "Are you sure you want to restore the backup? This action may override the existing "
                            + this.TfDatabaseName.Text + " on the server : "
                            + environment.DatabaseConfiguration.TenantAuthServerName,
                            "Confirm action!",
                            MessageBoxButtons.YesNoCancel) == DialogResult.Yes)
                    {

                        if (!this.TfDatabaseDataLocation.Text.EndsWith("\\"))
                        {
                            this.TfDatabaseDataLocation.Text += "\\";
                        }

                        if (!this.TfDatabaseLogLocation.Text.EndsWith("\\"))
                        {
                            this.TfDatabaseLogLocation.Text += "\\";
                        }

                        StringBuilder restoreScript = new StringBuilder();
                        restoreScript.AppendLine("USE [master]");
                        restoreScript.AppendLine($"RESTORE DATABASE [{this.TfDatabaseName.Text.Trim()}] FROM  ");
                        restoreScript.AppendLine(
                            "DISK = N'" + Executable.GetCurrentPath()
                                        + "\\Resources\\StarterDatabase\\AdvantageStarterDB.bak" + "' ");
                        restoreScript.AppendLine("WITH  FILE = 1, ");
                        restoreScript.AppendLine(
                            "MOVE N'AdvantageV1_Data' TO N'" + this.TfDatabaseDataLocation.Text
                                                             + this.TfDatabaseName.Text.Trim() + ".mdf',  ");
                        restoreScript.AppendLine(
                            "MOVE N'DocumentData' TO N'" + this.TfDatabaseDataLocation.Text
                                                         + this.TfDatabaseName.Text.Trim() + ".ndf',  ");
                        restoreScript.AppendLine(
                            "MOVE N'AdvantageV1_Log' TO N'" + this.TfDatabaseLogLocation.Text
                                                            + this.TfDatabaseName.Text.Trim() + ".ldf',  ");
                        restoreScript.AppendLine("NOUNLOAD,  STATS = 5 ");

                        if (environment != null)
                        {
                            string masterConnectionString = DatabaseScriptRunner.GetConnectionString(
                                "master",
                                environment.DatabaseConfiguration.TenantAuthUserName,
                                environment.DatabaseConfiguration.TenantAuthDatabasePassword,
                                environment.DatabaseConfiguration.TenantAuthServerName);

                            DatabaseScriptRunner databaseScriptRunner = new DatabaseScriptRunner();
                            databaseScriptRunner.RunScript(restoreScript.ToString(), masterConnectionString, false);
                            MessageBox.Show(
                                $"Database {this.TfDatabaseName.Text.Trim()} has been restored on the server {environment.DatabaseConfiguration.TenantAuthServerName}",
                                "Confirmation!",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Information);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(
                    exception.Message,
                    "An error has occurred!",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }

        }

        /// <summary>
        /// The install starter database form load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void InstallStarterDatabaseForm_Load(object sender, EventArgs e)
        {

            this.LoadEnvironments();
        }

        /// <summary>
        /// The data location_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void MfbDataLocation_Click(object sender, EventArgs e)
        {
            if (this.folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                this.TfDatabaseDataLocation.Text = this.folderBrowserDialog1.SelectedPath;
            }
        }

        /// <summary>
        /// The database log location_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void MfbDatabaseLogLocation_Click(object sender, EventArgs e)
        {
            if (this.folderBrowserDialog1.ShowDialog() == DialogResult.OK)
            {
                this.TfDatabaseLogLocation.Text = this.folderBrowserDialog1.SelectedPath;
            }
        }

        /// <summary>
        /// The load environments.
        /// </summary>
        private void LoadEnvironments()
        {
            this.environments = JsonConvert.DeserializeObject<List<Models.Environment>>(
                File.ReadAllText(Executable.GetCurrentPath() + @"\EnvironmentConfiguration.json"));
            if (environments != null)
            {
                this.CbEnvironmentName.Items.Clear();

                foreach (var environment in environments)
                {
                    this.CbEnvironmentName.Items.Add(environment.Name);
                }
            }
        }

        private void MrbRestoreDatabaseTenant_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(this.TfDatabaseNameTenant.Text)
                    || string.IsNullOrEmpty(this.TfDatabaseDataLocation.Text)
                    || string.IsNullOrEmpty(this.TfDatabaseLogLocation.Text))
                {
                    MessageBox.Show(
                        "Database Data Location and Database Log Location are required.",
                        "Missing Required parameter",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                }
                else if (this.CbEnvironmentName.SelectedItem == null)
                {
                    MessageBox.Show(
                        "An environment is required .",
                        "Missing Required parameter",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                }
                else
                {
                    var environment = this.environments.FirstOrDefault(
                        x => x.Name == this.CbEnvironmentName.SelectedItem.ToString());
                    if (MessageBox.Show(
                            "Are you sure you want to restore the backup? This action may override the existing "
                            + this.TfDatabaseNameTenant.Text + " on the server : "
                            + environment.DatabaseConfiguration.TenantAuthServerName,
                            "Confirm action!",
                            MessageBoxButtons.YesNoCancel) == DialogResult.Yes)
                    {

                        if (!this.TfDatabaseDataLocation.Text.EndsWith("\\"))
                        {
                            this.TfDatabaseDataLocation.Text += "\\";
                        }

                        if (!this.TfDatabaseLogLocation.Text.EndsWith("\\"))
                        {
                            this.TfDatabaseLogLocation.Text += "\\";
                        }

                        StringBuilder restoreScript = new StringBuilder();
                        restoreScript.AppendLine("USE [master]");
                        restoreScript.AppendLine($"RESTORE DATABASE [{this.TfDatabaseNameTenant.Text.Trim()}] FROM  ");
                        restoreScript.AppendLine(
                            "DISK = N'" + Executable.GetCurrentPath()
                                        + "\\Resources\\StarterDatabase\\TenantAuthDb_Blank.bak" + "' ");
                        restoreScript.AppendLine("WITH  FILE = 1, ");
                        restoreScript.AppendLine(
                            "MOVE N'AspNetServicesDB' TO N'" + this.TfDatabaseDataLocation.Text
                                                             + this.TfDatabaseNameTenant.Text.Trim() + ".mdf',  ");
                        restoreScript.AppendLine(
                            "MOVE N'AspNetServicesDB_log' TO N'" + this.TfDatabaseLogLocation.Text
                                                                 + this.TfDatabaseNameTenant.Text.Trim() + ".ldf',  ");
                        restoreScript.AppendLine("NOUNLOAD,  STATS = 5 ");

                        if (environment != null)
                        {
                            string masterConnectionString = DatabaseScriptRunner.GetConnectionString(
                                "master",
                                environment.DatabaseConfiguration.TenantAuthUserName,
                                environment.DatabaseConfiguration.TenantAuthDatabasePassword,
                                environment.DatabaseConfiguration.TenantAuthServerName);

                            DatabaseScriptRunner databaseScriptRunner = new DatabaseScriptRunner();
                            databaseScriptRunner.RunScript(restoreScript.ToString(), masterConnectionString, false);
                            MessageBox.Show(
                                $"Database {this.TfDatabaseNameTenant.Text.Trim()} has been restored on the server {environment.DatabaseConfiguration.TenantAuthServerName}",
                                "Confirmation!",
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Information);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(
                    exception.Message,
                    "An error has occurred!",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        private void MrbSetupTenantConnectionString_Click(object sender, EventArgs e)
        {
            if (this.CbEnvironmentName.SelectedItem == null)
            {
                MessageBox.Show(
                    "Select an environment",
                    "Required Field Validation",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
            }
            else if (string.IsNullOrEmpty(this.TfDatabaseName.Text))
            {
                MessageBox.Show(
                    "Advantage Client Database Name is required",
                    "Required Field Validation",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
            }
            else if (string.IsNullOrEmpty(this.TfDatabaseNameTenant.Text))
            {
                MessageBox.Show(
                    "Tenant Auth Database Name is required",
                    "Required Field Validation",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
            }
            else if (string.IsNullOrEmpty(this.TfTenantServerInstance.Text))
            {
                MessageBox.Show(
                    "Tenant Server Instance is required",
                    "Required Field Validation",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
            }
            else if (string.IsNullOrEmpty(this.TfTenantUser.Text))
            {
                MessageBox.Show(
                    "Tenant User is required",
                    "Required Field Validation",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
            }
            else if (string.IsNullOrEmpty(this.TfTenantPassword.Text))
            {
                MessageBox.Show(
                    "Tenant Password is required",
                    "Required Field Validation",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
            }
            else
            {
                SqlConnectionStringBuilder connectionString = new SqlConnectionStringBuilder();
                connectionString.DataSource = this.TfTenantServerInstance.Text;
                connectionString.InitialCatalog = this.TfDatabaseName.Text;
                connectionString.UserID = this.TfTenantUser.Text;
                connectionString.Password = this.TfTenantPassword.Text;
                connectionString.ConnectTimeout = 10;

                bool isConnectionValid = true;

                SqlConnection connection = new SqlConnection(connectionString.ConnectionString);
                try
                {
                    connection.Open();
                }
                catch (Exception exception)
                {
                    isConnectionValid = false;
                    MessageBox.Show(
                        "Unable to connect to the " + this.TfDatabaseName.Text + " on Server"
                        + this.TfTenantServerInstance.Text + " with User " + this.TfTenantUser.Text
                        + " due to the following exception: " + exception.Message,
                        "Invalid Connection",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Information);

                }
                finally
                {
                    connection.Close();
                }

                if (isConnectionValid)
                {
                    connectionString.InitialCatalog = TfDatabaseNameTenant.Text;
                    connection.ConnectionString = connection.ConnectionString;

                    StringBuilder tenantConnectionUpdate = new StringBuilder();

                    tenantConnectionUpdate.AppendLine(
                        "IF EXISTS(SELECT * FROM dbo.Tenant WHERE  DatabaseName = '' AND ServerName = '' AND UserName = '' AND Password = '' AND TenantName = 'Advantage')");
                    tenantConnectionUpdate.AppendLine(" Begin ");
                    tenantConnectionUpdate.AppendLine(
                        "UPDATE dbo.Tenant" + " SET TenantName = '" + TfDatabaseName.Text + "',ServerName = '"
                        + TfTenantServerInstance.Text + "', DatabaseName = '" + TfDatabaseName.Text + "' , UserName = '"
                        + TfTenantUser.Text + "', Password = '" + this.TfTenantPassword.Text + "' "
                        + " WHERE TenantName = 'Advantage' ");

                    tenantConnectionUpdate.AppendLine(
                        " UPDATE dbo.DataConnection " + " SET ServerName = '" + TfTenantServerInstance.Text
                        + "', DatabaseName = '" + TfDatabaseName.Text + "', UserName = '" + TfTenantUser.Text
                        + "', Password = '" + this.TfTenantPassword.Text + "' " + " WHERE DatabaseName = 'Advantage' ");

                    tenantConnectionUpdate.AppendLine(
                        " DECLARE @ServiceKey UNIQUEIDENTIFIER = NEWID() "
                        + " INSERT INTO dbo.ApiAuthenticationKey (TenantId,[Key],CreatedDate) "
                        + " SELECT TenantId, @ServiceKey, GETDATE() FROM dbo.Tenant WHERE TenantName = '"
                        + TfDatabaseName.Text + "' ");

                    tenantConnectionUpdate.AppendLine(
                        "  UPDATE " + TfDatabaseName.Text + "..[syConfigAppSetValues] "
                        + " SET    Value = @ServiceKey WHERE  SettingId = ( SELECT val.[SettingId] FROM   "
                        + TfDatabaseName.Text + "..[syConfigAppSetValues] val " + " JOIN   " + TfDatabaseName.Text
                        + "..[syConfigAppSettings] settings ON settings.SettingId = val.SettingId "
                        + " WHERE  KeyName = 'AdvantageServiceAuthenticationKey' ); ");

                    tenantConnectionUpdate.AppendLine(" End ");
                    try
                    {
                        SqlCommand command = new SqlCommand(tenantConnectionUpdate.ToString(), connection);
                        command.CommandType = CommandType.Text;

                        connection.Open();

                        command.ExecuteNonQuery();

                        MessageBox.Show(
                            "Tenant Connection have been Updated!",
                            "Confirmation!",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Information);
                    }
                    catch (Exception exception)
                    {
                        MessageBox.Show(
                            "Unable to set up the tenant due to the following exception: " + exception.Message,
                            "Invalid Connection",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Information);
                    }
                    finally
                    {
                        connection.Close();

                    }
                }
            }
        }

        private void CbEnvironmentName_SelectedIndexChanged(object sender, EventArgs e)
        {
            var environment =
                this.environments.FirstOrDefault(x => x.Name == this.CbEnvironmentName.SelectedItem.ToString());
            if (environment != null)
            {
                this.TfTenantServerInstance.Text = environment.DatabaseConfiguration.TenantAuthServerName;
            }
        }

        private void MrbSafeUrl_Click(object sender, EventArgs e)
        {
            if (this.CbEnvironmentName.SelectedItem == null)
            {
                MessageBox.Show(
                    "Select an environment",
                    "Required Field Validation",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
            }
            else if (string.IsNullOrEmpty(this.TfDatabaseName.Text))
            {
                MessageBox.Show(
                    "Advantage Client Database Name is required",
                    "Required Field Validation",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
            }
            else if (string.IsNullOrEmpty(this.TfDatabaseNameTenant.Text))
            {
                MessageBox.Show(
                    "Tenant Auth Database Name is required",
                    "Required Field Validation",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
            }
            else if (string.IsNullOrEmpty(this.TfTenantServerInstance.Text))
            {
                MessageBox.Show(
                    "Tenant Server Instance is required",
                    "Required Field Validation",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
            }
            else if (string.IsNullOrEmpty(this.TfTenantUser.Text))
            {
                MessageBox.Show(
                    "Tenant User is required",
                    "Required Field Validation",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
            }
            else if (string.IsNullOrEmpty(this.TfTenantPassword.Text))
            {
                MessageBox.Show(
                    "Tenant Password is required",
                    "Required Field Validation",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
            }

            else if (string.IsNullOrEmpty(this.TfApiUrl.Text))
            {
                MessageBox.Show(
                    "API Url required",
                    "Required Field Validation",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
            }
            else if (this.TfApiUrl.Text[this.TfApiUrl.Text.Length - 1] == '/')
            {
                MessageBox.Show(
                    "API URL should NOT end in /",
                    "Required Field Validation",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
            }
            else if (this.TfSiteUrl.Text[this.TfSiteUrl.Text.Length - 1] != '/')
            {
                MessageBox.Show(
                    "Site Url should have a / at the end",
                    "Required Field Validation",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
            }
            else if (this.TfServiceUrl.Text[this.TfServiceUrl.Text.Length - 1] != '/')
            {
                MessageBox.Show(
                    "Service Url should have a / at the end",
                    "Required Field Validation",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
            }
            else
            {
                SqlConnectionStringBuilder connectionString = new SqlConnectionStringBuilder();
                connectionString.DataSource = this.TfTenantServerInstance.Text;
                connectionString.InitialCatalog = this.TfDatabaseName.Text;
                connectionString.UserID = this.TfTenantUser.Text;
                connectionString.Password = this.TfTenantPassword.Text;
                connectionString.ConnectTimeout = 10;

                bool isConnectionValid = true;

                SqlConnection connection = new SqlConnection(connectionString.ConnectionString);
                try
                {
                    connection.Open();

                    StringBuilder updateAdvantageUrl = new StringBuilder();
                    updateAdvantageUrl.AppendLine("UPDATE     [value] ");
                    updateAdvantageUrl.AppendLine("SET        [value].[Value] = '"+this.TfSiteUrl.Text+"'");
                    updateAdvantageUrl.AppendLine("FROM       syConfigAppSettings setting");
                    updateAdvantageUrl.AppendLine("INNER JOIN syConfigAppSetValues [value] ON [value].SettingId = setting.SettingId");
                    updateAdvantageUrl.AppendLine("WHERE      setting.KeyName = 'AdvantageSiteUri';");
                    updateAdvantageUrl.AppendLine("");
                    updateAdvantageUrl.AppendLine("UPDATE     [value]");
                    updateAdvantageUrl.AppendLine("SET        [value].[Value] = '"+this.TfServiceUrl.Text+"'");
                    updateAdvantageUrl.AppendLine("FROM       syConfigAppSettings setting");
                    updateAdvantageUrl.AppendLine("INNER JOIN syConfigAppSetValues [value] ON [value].SettingId = setting.SettingId");
                    updateAdvantageUrl.AppendLine("WHERE      setting.KeyName = 'AdvantageServiceUri';");
                    updateAdvantageUrl.AppendLine("");
                    updateAdvantageUrl.AppendLine("UPDATE     [value]");
                    updateAdvantageUrl.AppendLine("SET        [value].[Value] = '"+this.TfApiUrl.Text+"'");
                    updateAdvantageUrl.AppendLine("FROM       syConfigAppSettings setting");
                    updateAdvantageUrl.AppendLine("INNER JOIN syConfigAppSetValues [value] ON [value].SettingId = setting.SettingId");
                    updateAdvantageUrl.AppendLine("WHERE      setting.KeyName = 'AdvantageApiURL';");
                    updateAdvantageUrl.AppendLine("");

                    SqlCommand command = new SqlCommand(updateAdvantageUrl.ToString(), connection);
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();

                    MessageBox.Show(
                        "Advantage URL's have been Updated!",
                        "Confirmation!",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Information);

                }
                catch (Exception exception)
                {
                    isConnectionValid = false;
                    MessageBox.Show(
                        "Unable to connect to the " + this.TfDatabaseName.Text + " on Server"
                        + this.TfTenantServerInstance.Text + " with User " + this.TfTenantUser.Text
                        + " due to the following exception: " + exception.Message,
                        "Invalid Connection",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Information);

                }
                finally
                {
                    connection.Close();
                }

                if (isConnectionValid)
                {
                }
            }
        }
    }
}
