﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ConnectionConfiguration.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The connection configuration.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AdvantageDatabaseUpdateForm.Common.Database
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Forms;

    /// <summary>
    /// The connection type.
    /// </summary>
    public enum ConnectionType
    {
        /// <summary>
        /// The windows authentication.
        /// </summary>
        WindowsAuthentication = 0,

        /// <summary>
        /// The sql server authentication.
        /// </summary>
        SqlServerAuthentication = 1
    }

    /// <summary>
    /// The connection configuration.
    /// </summary>
    public class ConnectionConfiguration : IValidate
    {
        /// <summary>
        /// Gets or sets the connection type.
        /// </summary>
        public ConnectionType ConnectionType { get; set; }

        /// <summary>
        /// Gets or sets the server name.
        /// </summary>
        public string ServerName { get; set; }

        /// <summary>
        /// Gets or sets the user name.
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Gets or sets the password.
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// The validate.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool Validate()
        {
            bool isValid = true;
            StringBuilder messageStringBuilder = new StringBuilder();
            messageStringBuilder.AppendLine("Unable to execute your action due to the following reason(s):");

            if (string.IsNullOrWhiteSpace(this.ServerName))
            {
                isValid = false;
                messageStringBuilder.AppendLine(@"Server name is required");
            }

            if (string.IsNullOrWhiteSpace(this.UserName))
            {
                isValid = false;
                messageStringBuilder.AppendLine(@"User Name is required");
            }

            if (this.ConnectionType == ConnectionType.SqlServerAuthentication)
            {
                if (string.IsNullOrWhiteSpace(this.Password))
                {
                    isValid = false;
                    messageStringBuilder.AppendLine(@"Password is required");
                }
            }


            if (!isValid)
            {
                MessageBox.Show(messageStringBuilder.ToString(), @"Validation alert!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

            return isValid;
        }

        /// <summary>
        /// The test connection.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool TestConnection()
        {
            SqlConnectionStringBuilder stringBuilder = new SqlConnectionStringBuilder();

            if (this.ConnectionType == ConnectionType.WindowsAuthentication)
            {
                stringBuilder.IntegratedSecurity = true;
            }
            else
            {
                stringBuilder.UserID = this.UserName;
                stringBuilder.Password = this.Password;
                stringBuilder.InitialCatalog = "master";
            }

            stringBuilder.DataSource = this.ServerName;
            stringBuilder.ConnectTimeout = 60;

            SqlConnection connection = new SqlConnection(stringBuilder.ToString());
            try
            {
                connection.Open();
                return true;
            }
            catch (Exception e)
            {
                MessageBox.Show(
                    @"Unable to connect to the server. " + e.Message,
                    @"An error occurred connecting to the master database, user must have be able to connect to master database and have owner permissions on the server or databases to work.",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }

            return false;
        }
    }
}
