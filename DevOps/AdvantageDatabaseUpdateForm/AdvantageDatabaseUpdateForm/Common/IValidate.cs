﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdvantageDatabaseUpdateForm.Common
{
    public interface IValidate
    {
        bool Validate();
    }
}
