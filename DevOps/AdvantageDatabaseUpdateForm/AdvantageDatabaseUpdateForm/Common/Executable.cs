﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Executable.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the Executable type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AdvantageDatabaseUpdateForm.Common
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// The executable.
    /// </summary>
    public static class Executable
    {
        /// <summary>
        /// The execute exe.
        /// </summary>
        /// <param name="path">
        /// The path.
        /// </param>
        public static void
            Execute(string path)
        {
            System.Diagnostics.Process process = new System.Diagnostics.Process();
            System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
            startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            startInfo.FileName = path;
            process.StartInfo = startInfo;
            process.Start();
        }

        /// <summary>
        /// The execute.
        /// </summary>
        /// <param name="processName">
        /// The process name.
        /// </param>
        /// <param name="path">
        /// The path.
        /// </param>
        public static void Execute(string processName, string path)
        {
            Process.Start(processName, path);
        }

        /// <summary>
        /// The get current path.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string GetCurrentPath()
        {
            return System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
        }
    }
}
