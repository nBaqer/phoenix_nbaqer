﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Execute.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The execute.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AdvantageDatabaseUpdateForm.Common.CLI
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;

    using AdvantageDatabaseUpdateForm.Tabs;

    using Newtonsoft.Json;

    using Environment = System.Environment;

    /// <summary>
    /// The execute.
    /// </summary>
    public class Execute
    {
        /// <summary>
        /// The arguments.
        /// </summary>
        private readonly string[] arguments;

        /// <summary>
        /// Initializes a new instance of the <see cref="Execute"/> class.
        /// </summary>
        /// <param name="arguments">
        /// The arguments.
        /// </param>
        public Execute(string[] arguments)
        {
            this.arguments = arguments;
        }

        /// <summary>
        /// The run by command line.
        /// </summary>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        public int RunByCommandLine()
        {
            Console.WriteLine("Arguments Received");

            Dictionary<string, string> argumentsDictionary = new Dictionary<string, string>();

            foreach (var argument in this.arguments)
            {
                var keyValuePair = argument.Split(':');
                if (keyValuePair.Length == 2)
                {
                    argumentsDictionary.Add(keyValuePair[0], keyValuePair[1]);
                }
            }

            if (argumentsDictionary.Count > 0)
            {
                if (!argumentsDictionary.ContainsKey("environment"))
                {
                    throw new Exception("Missing Environment argument.");
                }
                if (!argumentsDictionary.ContainsKey("tenant"))
                {
                    throw new Exception("Missing Tenant argument.");
                }

                List<Models.Environment> environments =
                    JsonConvert.DeserializeObject<List<Models.Environment>>(File.ReadAllText(
                        Executable.GetCurrentPath() + @"\EnvironmentConfiguration.json"));

                Models.DatabaseVersionManifest databaseVersionManifest =
                    JsonConvert.DeserializeObject<Models.DatabaseVersionManifest>(
                        File.ReadAllText(Executable.GetCurrentPath() + @"\DatabaseVersionManifest.json"));

                DatabaseManagementTab databaseManagementTab = new DatabaseManagementTab(environments, databaseVersionManifest);

                var environment =
                    environments.FirstOrDefault(x => x.Name == argumentsDictionary["environment"].ToString());

                if (environment == null)
                {
                    throw new Exception("Unable to find the environment provided as an argument on the environment manifest");
                }

                var tenants = databaseManagementTab.GetTenantList(environment);

                if (tenants.Count == 0)
                {
                    throw new Exception("Unable to find tenants for the environment provided");
                }

                var tenant = tenants.FirstOrDefault(x => x.Name == argumentsDictionary["tenant"].ToString());
                if (tenant == null)
                {
                    throw new Exception("Tenant was not found");
                }

                databaseManagementTab.GetCurrentHistory(tenant);
                var didCompleted = databaseManagementTab.UpdateDatabase(tenant, environment, false);
                if (!didCompleted)
                {
                    return 1;
                }
            }

            return 0;
        }
    }
}
