﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AdvantageApiTab.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The advantage api tab.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AdvantageDatabaseUpdateForm.Tabs
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Policy;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Forms;

    using AdvantageDatabaseUpdateForm.Common;
    using AdvantageDatabaseUpdateForm.Common.Database;
    using AdvantageDatabaseUpdateForm.Data;
    using AdvantageDatabaseUpdateForm.Models;

    using MaterialSkin.Controls;

    using Environment = System.Environment;
    using Version = System.Version;

    /// <summary>
    /// The advantage api tab.
    /// </summary>
    public class AdvantageApiTab
    {

        private ConnectionConfiguration connectionConfiguration { get; set; }
        private List<Models.Environment> environments { get; set; }
        private DatabaseVersionManifest versionManifest { get; set; }

        private MaterialRaisedButton installCoreRuntimeButton;

        private MaterialRaisedButton instalCoreWebHosting;

        private MaterialRaisedButton viewConfigurationSteps;

        private MaterialSingleLineTextField advantageApiUrlTextField;

        private MaterialRaisedButton saveAdvantageApiUrlButton;

        private MaterialRaisedButton launchAdvantageApiUrl;

        private ComboBox environmentApiTabComboBox;

        private ComboBox tenantAdvantageApiComboBox;

        private Tenant selectedTenant;

        private IList<Tenant> tenantList;

        public AdvantageApiTab(ConnectionConfiguration connectionConfiguration, List<Models.Environment> environments, DatabaseVersionManifest versionManifest)
        {
            this.connectionConfiguration = connectionConfiguration;
            this.environments = environments;
            this.versionManifest = versionManifest;
        }

        /// <summary>
        /// The initialize tab.
        /// </summary>
        /// <param name="environmentApiTabComboBox">
        /// The environment api tab combo box
        /// </param>
        /// <param name="tenantAdvantageApiComboBox">
        /// The tenant api tab combo box
        /// </param>
        /// <param name="installCoreRuntimeButton">
        /// The install core runtime button.
        /// </param>
        /// <param name="instalCoreWebHosting">
        /// The instal core web hosting.
        /// </param>
        /// <param name="advantageApiUrlTextField">
        /// The advantage api url text field.
        /// </param>
        /// <param name="saveAdvantageApiUrlButton">
        /// The save advantage api url button.
        /// </param>
        /// <param name="launchAdvantageApiUrl">
        /// The launch advantage api url.
        /// </param>
        public void InitializeTab(ComboBox environmentApiTabComboBox, ComboBox tenantAdvantageApiComboBox, MaterialRaisedButton installCoreRuntimeButton, MaterialRaisedButton instalCoreWebHosting, MaterialSingleLineTextField advantageApiUrlTextField, MaterialRaisedButton saveAdvantageApiUrlButton, MaterialRaisedButton launchAdvantageApiUrl)
        {
            this.instalCoreWebHosting = instalCoreWebHosting;
            this.installCoreRuntimeButton = installCoreRuntimeButton;
            this.advantageApiUrlTextField = advantageApiUrlTextField;
            this.saveAdvantageApiUrlButton = saveAdvantageApiUrlButton;
            this.launchAdvantageApiUrl = launchAdvantageApiUrl;
            this.environmentApiTabComboBox = environmentApiTabComboBox;
            this.tenantAdvantageApiComboBox = tenantAdvantageApiComboBox;

            this.instalCoreWebHosting.Click += this.InstalCoreWebHostingClick;
            this.installCoreRuntimeButton.Click += this.InstallCoreRuntimeButtonClick;
            this.saveAdvantageApiUrlButton.Click += this.SaveAdvantageApiUrlButtonClick;
            this.launchAdvantageApiUrl.Click += this.LaunchAdvantageApiUrlClick;
            this.environmentApiTabComboBox.SelectedIndexChanged += this.EnvironmentApiTabComboBoxSelectedIndexChanged;
            this.tenantAdvantageApiComboBox.SelectedIndexChanged += this.TenantAdvantageApiComboBoxSelectedIndexChanged;

            this.LoadEnvironments();
        }

        /// <summary>
        /// The load environments.
        /// </summary>
        public void LoadEnvironments()
        {
            this.environmentApiTabComboBox.Items.Clear();
            this.tenantAdvantageApiComboBox.Items.Clear();
            this.advantageApiUrlTextField.Text = @"HTTPS://";
            if (this.environments != null)
            {
                foreach (var environment in this.environments)
                {
                    this.environmentApiTabComboBox.Items.Add(environment.Name);
                }
            }
        }

        /// <summary>
        /// The tenant advantage api combo box selected index changed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void TenantAdvantageApiComboBoxSelectedIndexChanged(object sender, System.EventArgs e)
        {
            this.selectedTenant = this.tenantList.FirstOrDefault(x => x.Name == this.tenantAdvantageApiComboBox.SelectedItem.ToString());
            ConfigureApplicationSettings configureApplicationSettings = new ConfigureApplicationSettings(DatabaseScriptRunner.GetConnectionString(this.selectedTenant.DatabaseName, this.selectedTenant.UserName, this.selectedTenant.Password, this.selectedTenant.ServerName));
            this.advantageApiUrlTextField.Text = configureApplicationSettings.GetApplicationSettingValue<string>("AdvantageApiURL");
        }

        /// <summary>
        /// The environment api tab combo box selected index changed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void EnvironmentApiTabComboBoxSelectedIndexChanged(object sender, System.EventArgs e)
        {
            try
            {
                this.tenantAdvantageApiComboBox.Items.Clear();

                foreach (var tenant in this.GetTenantList(
                    this.environments.FirstOrDefault(x => x.Name == this.environmentApiTabComboBox.SelectedItem.ToString())))
                {
                    this.tenantAdvantageApiComboBox.Items.Add(tenant.Name);
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(@"An un-handle exception has occurred: " + exception.Message, @"An un-handle exception has occurred:", MessageBoxButtons.OK);
            }
        }

        /// <summary>
        /// The launch advantage api url click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e
        /// </param>
        private void LaunchAdvantageApiUrlClick(object sender, System.EventArgs e)
        {
            if (string.IsNullOrEmpty(this.advantageApiUrlTextField.Text.Trim()) || this.advantageApiUrlTextField.Text.Trim().ToUpper().Equals("https://"))
            {
                MessageBox.Show(
                    @"The Advantage API URL have not been configured.",
                    @"Validation was not successful!",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation);
            }
            else if (!this.advantageApiUrlTextField.Text.Trim().ToUpper().EndsWith("/API/API"))
            {
                MessageBox.Show(
                    @"The Advantage API URL must end in /API/API",
                    @"Validation was not successful!",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation);
            }
            else
            {
                Executable.Execute(this.advantageApiUrlTextField.Text.ToUpper().Replace("/API/API", string.Empty) + "/API/Swagger");
            }
        }

        /// <summary>
        /// The save advantage api url button click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void SaveAdvantageApiUrlButtonClick(object sender, System.EventArgs e)
        {
            if (this.selectedTenant == null)
            {
                MessageBox.Show(
                    @"An environment and tenant are required to be selected",
                    @"Required field validation!",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation);
            }
            else
            if (string.IsNullOrWhiteSpace(this.advantageApiUrlTextField.Text))
            {
                MessageBox.Show(
                    @"The API URL is required",
                    @"Required field validation!",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation);
            }
            else
            {
                if (Uri.TryCreate(this.advantageApiUrlTextField.Text, UriKind.RelativeOrAbsolute, out var apiUrl) && (apiUrl.Scheme == Uri.UriSchemeHttp || apiUrl.Scheme == Uri.UriSchemeHttps))
                {
                    if (apiUrl.Scheme == Uri.UriSchemeHttp)
                    {
                        if (MessageBox.Show(
                                @"The API URL you have entered is not a secured url. The Advantage API URL should be used under a HTTPS protocol. Press 'Yes' if the school is aware of the risk that enabling the API usage without a security can cause. Or Press 'Yes' if the advantage installation is not used publicly or available on the internet. Otherwise press 'No' and provide a HTTPS url.",
                                @"Security Risk, Action Required",
                                MessageBoxButtons.YesNo,
                                MessageBoxIcon.Exclamation) == DialogResult.Yes)
                        {
                            this.SaveAdvantageApiUrl();
                        }
                    }
                    else
                    {
                        this.SaveAdvantageApiUrl();
                    }
                }
                else
                {
                    MessageBox.Show(
                        @"The API URL is in an invalid format",
                        @"Required field validation!",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Exclamation);
                }
            }
        }

        /// <summary>
        /// The install core runtime button click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void InstallCoreRuntimeButtonClick(object sender, System.EventArgs e)
        {
            if (System.Environment.Is64BitOperatingSystem)
            {
                Executable.Execute(Executable.GetCurrentPath() + "\\Prerequisites\\dotnet-runtime-2.0.5-win-x64.exe");
            }
            else
            {
                Executable.Execute(Executable.GetCurrentPath() + "\\Prerequisites\\dotnet-runtime-2.0.5-win-x86.exe");
            }
        }

        /// <summary>
        /// The instal core web hosting click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void InstalCoreWebHostingClick(object sender, System.EventArgs e)
        {
            Executable.Execute(Executable.GetCurrentPath() + "\\Prerequisites\\DotNetCore.2.0.5-WindowsHosting.exe");
        }


        /// <summary>
        /// The get tenant list.
        /// </summary>
        /// <param name="environment">
        /// The environment.
        /// </param>
        /// <returns>
        /// The <see cref="IList"/>.
        /// </returns>
        private IList<Tenant> GetTenantList(Models.Environment environment)
        {
            Tenants tenants = new Tenants();

            this.tenantList = tenants.GetTenants(
                DatabaseScriptRunner.GetConnectionString(
                    environment.DatabaseConfiguration.TenantAuthDatabaseName,
                    environment.DatabaseConfiguration.TenantAuthUserName,
                    environment.DatabaseConfiguration.TenantAuthDatabasePassword,
                    environment.DatabaseConfiguration.TenantAuthServerName,
                    1000),
                environment.DatabaseConfiguration.TenantAuthDatabaseName);
            return this.tenantList;
        }

        /// <summary>
        /// The save advantage api url.
        /// </summary>
        private void SaveAdvantageApiUrl()
        {
            ConfigureApplicationSettings configureApplicationSettings = new ConfigureApplicationSettings(DatabaseScriptRunner.GetConnectionString(this.selectedTenant.DatabaseName, this.selectedTenant.UserName, this.selectedTenant.Password, this.selectedTenant.ServerName));
            configureApplicationSettings.UpdateApplicationSetting("AdvantageApiURL", this.advantageApiUrlTextField.Text.Trim());
            MessageBox.Show(@"API Url have been updated! You can proceed to next step to launch the API URl", @"Information was saved!", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}
