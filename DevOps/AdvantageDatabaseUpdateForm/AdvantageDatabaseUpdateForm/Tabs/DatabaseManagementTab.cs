﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DatabaseManagementTab.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the DatabaseManagementTab type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AdvantageDatabaseUpdateForm.Tabs
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Windows.Forms;

    using AdvantageDatabaseUpdateForm.Common.Database;
    using AdvantageDatabaseUpdateForm.Data;
    using AdvantageDatabaseUpdateForm.Models;

    using MaterialSkin.Controls;

    using Environment = AdvantageDatabaseUpdateForm.Models.Environment;
    using Version = AdvantageDatabaseUpdateForm.Models.Version;

    /// <summary>
    /// The database management tab.
    /// </summary>
    public class DatabaseManagementTab
    {
        /// <summary>
        /// The environments.
        /// </summary>
        private readonly List<Models.Environment> environments;

        /// <summary>
        /// The version manifest.
        /// </summary>
        private readonly DatabaseVersionManifest versionManifest;

        /// <summary>
        /// The environment name combo box.
        /// </summary>
        private ComboBox environmentNameComboBox;

        /// <summary>
        /// The tenant name combo box.
        /// </summary>
        private ComboBox tenantNameComboBox;

        /// <summary>
        /// The current version material text field.
        /// </summary>
        private MaterialSingleLineTextField currentVersionMaterialTextField;

        /// <summary>
        /// The newer version material text field.
        /// </summary>
        private MaterialSingleLineTextField newerVersionMaterialTextField;

        /// <summary>
        /// The version history data grid view.
        /// </summary>
        private DataGridView versionHistoryDataGridView;

        /// <summary>
        /// The update database button.
        /// </summary>
        private MaterialRaisedButton updateDatabaseButton;

        /// <summary>
        /// The install SQL jobs button.
        /// </summary>
        private MaterialRaisedButton installSqlJobsButton;

        /// <summary>
        /// The tenants.
        /// </summary>
        private IList<Tenant> tenants;

        /// <summary>
        /// The pending versions.
        /// </summary>
        private List<Version> pendingVersions;

        /// <summary>
        /// The sorted versions.
        /// </summary>
        private List<Version> sortedVersions;

        /// <summary>
        /// The current advantage database id.
        /// </summary>
        private Guid currentAdvantageDatabaseId;

        /// <summary>
        /// The update database thread.
        /// </summary>
        private Thread updateDatabaseThread;

        /// <summary>
        /// The install sql jobs thread.
        /// </summary>
        private Thread installSqlJobsThread;

        /// <summary>
        /// The status label.
        /// </summary>
        private MaterialLabel statusLabel;

        /// <summary>
        /// The previous version number.
        /// </summary>
        private int previousVersionNumber;

        /// <summary>
        /// The newer version number.
        /// </summary>
        private int newerVersionNumber;

        /// <summary>
        /// Initializes a new instance of the <see cref="DatabaseManagementTab"/> class.
        /// </summary>
        /// <param name="environments">
        /// The environments.
        /// </param>
        /// <param name="versionManifest">
        /// The version manifest.
        /// </param>
        public DatabaseManagementTab(
            List<Models.Environment> environments,
            DatabaseVersionManifest versionManifest)
        {
            this.environments = environments;
            this.versionManifest = versionManifest;
            this.pendingVersions = new List<Version>();
        }

        /// <summary>
        /// Gets the previous version.
        /// </summary>
        public string PreviousVersion { get; private set; }

        /// <summary>
        /// Gets the newer version.
        /// </summary>
        public string NewerVersion { get; private set; }

        /// <summary>
        /// The initialize tab.
        /// </summary>
        /// <param name="environmentName">
        /// The environment name combo box.
        /// </param>
        /// <param name="tenantName">
        /// The tenant name combo box.
        /// </param>
        /// <param name="currentVersionMaterial">
        /// The current version material text field.
        /// </param>
        /// <param name="newerVersionMaterial">
        /// The newer version material text field.
        /// </param>
        /// <param name="versionHistory">
        /// The version history data grid view.
        /// </param>
        /// <param name="updateDatabase">
        /// The update Database.
        /// </param>
        /// <param name="installSqlJobs">
        /// The install Sql Jobs.
        /// </param>
        /// <param name="status">
        /// The status.
        /// </param>
        public void InitializeTab(
            ComboBox environmentName,
            ComboBox tenantName,
            MaterialSingleLineTextField currentVersionMaterial,
            MaterialSingleLineTextField newerVersionMaterial,
            DataGridView versionHistory,
            MaterialRaisedButton updateDatabase,
            MaterialRaisedButton installSqlJobs,
            MaterialLabel status)
        {
            this.environmentNameComboBox = environmentName;
            this.tenantNameComboBox = tenantName;
            this.currentVersionMaterialTextField = currentVersionMaterial;
            this.newerVersionMaterialTextField = newerVersionMaterial;
            this.versionHistoryDataGridView = versionHistory;
            this.updateDatabaseButton = updateDatabase;
            this.installSqlJobsButton = installSqlJobs;
            this.statusLabel = status;

            this.LoadEnvironments();

            this.environmentNameComboBox.SelectedIndexChanged += this.EnvironmentNameComboBoxSelectedIndexChanged;
            this.tenantNameComboBox.SelectedIndexChanged += this.TenantNameComboBoxSelectedIndexChanged;
        }



        /// <summary>
        /// The get tenant list.
        /// </summary>
        /// <param name="environment">
        /// The environment.
        /// </param>
        /// <returns>
        /// The <see cref="IList"/>.
        /// </returns>
        public IList<Tenant> GetTenantList(Models.Environment environment)
        {
            Tenants tenants = new Tenants();

            return tenants.GetTenants(
                DatabaseScriptRunner.GetConnectionString(
                    environment.DatabaseConfiguration.TenantAuthDatabaseName,
                    environment.DatabaseConfiguration.TenantAuthUserName,
                    environment.DatabaseConfiguration.TenantAuthDatabasePassword,
                    environment.DatabaseConfiguration.TenantAuthServerName,
                    100),
                environment.DatabaseConfiguration.TenantAuthDatabaseName);
        }


        /// <summary>
        /// The update database to latest from the UI forms on a separate thread
        /// </summary>
        public void UpdateDatabaseToLatest()
        {
            var rgx = new Regex(@"(\.\d(?!\d))");

            int previousVersion = int.Parse(rgx.Replace(this.PreviousVersion, grp => "0" + grp.Value).Replace(".", string.Empty));
            int newerVersion = int.Parse(rgx.Replace(this.NewerVersion, grp => "0" + grp.Value).Replace(".", string.Empty));

            if (previousVersion < newerVersion)
            {
                var tenant =
                    this.tenants.FirstOrDefault(x => x.Name == this.tenantNameComboBox.SelectedItem.ToString());
                var environment = this.environments.FirstOrDefault(
                    x => x.Name == this.environmentNameComboBox.SelectedItem.ToString());
                this.UpdateDatabaseThread(tenant, environment, true);
            }
        }

        /// <summary>
        /// The install SQL jobs.
        /// </summary>
        public void InstallSqlJobs()
        {
            this.InstallSqlJobsThread();
        }

        /// <summary>
        /// The get current history.
        /// </summary>
        /// <param name="tenant">
        /// The tenant.
        /// </param>
        public bool GetCurrentHistory(Tenant tenant)
        {
            this.pendingVersions = new List<Version>();

            DatabaseVersionHistory versionHistory = new DatabaseVersionHistory();

            string advantageConnectionString = DatabaseScriptRunner.GetConnectionString(
                tenant.DatabaseName,
                tenant.UserName,
                tenant.Password,
                tenant.ServerName,
                1000);

            IList<Models.Version> versions = versionHistory.GetHistory(advantageConnectionString, tenant.DatabaseName);

            foreach (var version in versions)
            {
                version.Status = Version.EScriptStatus.Executed;
            }

            if (versions.Any())
            {
                var currentDatabaseVersion = versions.OrderByDescending(x => x.Major).ThenByDescending(x => x.Minor)
                    .ThenByDescending(x => x.Build).ThenByDescending(x => x.Revision).First();

                var firstVersionInstalledOnHistory = versions.OrderBy(x => x.Major).ThenBy(x => x.Minor)
                    .ThenBy(x => x.Build).ThenBy(x => x.Revision).First();

                var newerDatabaseVersion = this.versionManifest.Versions.OrderByDescending(x => x.Major)
                    .ThenByDescending(x => x.Minor).ThenByDescending(x => x.Build).ThenByDescending(x => x.Revision)
                    .First();

                this.currentAdvantageDatabaseId = versionHistory.GetAdvantageDatabaseId(
                    advantageConnectionString,
                    tenant.DatabaseName);

                this.PreviousVersion = string.Format(
                    @"{0}.{1}.{2}.{3}",
                    currentDatabaseVersion.Major,
                    currentDatabaseVersion.Minor,
                    currentDatabaseVersion.Build,
                    currentDatabaseVersion.Revision);

                this.NewerVersion = string.Format(
                    @"{0}.{1}.{2}.{3}",
                    newerDatabaseVersion.Major,
                    newerDatabaseVersion.Minor,
                    newerDatabaseVersion.Build,
                    newerDatabaseVersion.Revision);

                this.pendingVersions = this.versionManifest.Versions.Where(
                    x => !versions.Any(
                             y => y.Major == x.Major && y.Minor == x.Minor && y.Build == x.Build
                                  && y.Revision == x.Revision)).ToList();

                this.pendingVersions = this.pendingVersions.Where(
                    x => this.GetBuildNumberFromVersion(x)
                         > this.GetBuildNumberFromVersion(firstVersionInstalledOnHistory)).ToList();

                foreach (var version in this.pendingVersions)
                {
                    version.Status = Version.EScriptStatus.Pending;
                    versions.Add(version);
                }

                this.sortedVersions = versions.OrderByDescending(x => x.Major).ThenByDescending(x => x.Minor)
                    .ThenByDescending(x => x.Build).ThenByDescending(x => x.Revision).ToList();

                var rgx = new Regex(@"(\.\d(?!\d))");
                this.previousVersionNumber = int.Parse(rgx.Replace(this.PreviousVersion, grp => "0" + grp.Value).Replace(".", string.Empty));
                this.newerVersionNumber = int.Parse(rgx.Replace(this.NewerVersion, grp => "0" + grp.Value).Replace(".", string.Empty));

                return false;
            }

            return true;
        }


        /// <summary>
        /// The refresh selected tenant.
        /// </summary>
        public void RefreshSelectedTenant()
        {
            try
            {
                var status = this.GetCurrentHistory(
                    this.tenants.FirstOrDefault(x => x.Name == this.tenantNameComboBox.SelectedItem.ToString()));
                this.BindCurrentHistory();
                if (status)
                {
                    this.updateDatabaseButton.Visible = false;
                    this.statusLabel.Text = @"Database is up to date!";
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(
                    @"An un-handle exception has occurred: " + exception.Message,
                    @"An un-handle exception has occurred:",
                    MessageBoxButtons.OK);
            }
        }

        /// <summary>
        /// The update database.
        /// </summary>
        /// <param name="tenant">
        /// The tenant.
        /// </param>
        /// <param name="environment">
        /// The environment.
        /// </param>
        /// <param name="shouldUpdateUi">
        /// The should update ui.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool UpdateDatabase(
            Tenant tenant,
            Environment environment,
            bool shouldUpdateUi)
        {

            bool completed = true;
            string tenantDatabaseConnectionString = DatabaseScriptRunner.GetConnectionString(
                environment.DatabaseConfiguration.TenantAuthDatabaseName,
                environment.DatabaseConfiguration.TenantAuthUserName,
                environment.DatabaseConfiguration.TenantAuthDatabasePassword,
                environment.DatabaseConfiguration.TenantAuthServerName);

            string advantageConnectionString = DatabaseScriptRunner.GetConnectionString(
                tenant.DatabaseName,
                environment.DatabaseConfiguration.TenantAuthUserName,
                environment.DatabaseConfiguration.TenantAuthDatabasePassword,
                tenant.ServerName);

            Assembly executingAssembly = Assembly.GetExecutingAssembly();
            foreach (var version in this.sortedVersions.Where(x => x.Status == Version.EScriptStatus.Pending)
                .OrderBy(x => x.Major).ThenBy(x => x.Minor).ThenBy(x => x.Build).ThenBy(x => x.Revision))
            {
                version.Status = Version.EScriptStatus.Executing;

                if (shouldUpdateUi)
                {
                    this.RefreshHistoryGrid("Executing... " + version.Description);
                }

                var ver = $@"{version.Major}.{version.Minor}.{version.Build}.{version.Revision}";
                var rgx = new Regex(@"(\.0{1})(?!$)");
                ver = rgx.Replace(ver, "00", 1).Replace(".", string.Empty);

                Type migrationStepType = executingAssembly.GetType(
                    string.Format(
                        @"AdvantageDatabaseUpdateForm.MigrationSteps.Version{0}{1}.UpdateTo{2}",
                        version.Major,
                        version.Minor,
                        ver));

                if (migrationStepType != null)
                {
                    ConstructorInfo constructor = migrationStepType.GetConstructor(Type.EmptyTypes);
                    if (constructor != null)
                    {
                        object constructorObject = constructor.Invoke(new object[] { });

                        MethodInfo updateMethodInfo = migrationStepType.GetMethod("Update");
                        if (updateMethodInfo != null)
                        {
                            try
                            {
                                if (version.Database == Version.EDatabase.TenantAuthDatabase)
                                {
                                    updateMethodInfo.Invoke(
                                        constructorObject,
                                        new object[]
                                            {
                                                tenantDatabaseConnectionString,
                                                environment.DatabaseConfiguration.TenantAuthDatabaseName, tenant.DatabaseName,
                                                version, this.currentAdvantageDatabaseId
                                            });
                                }
                                else if (version.Database == Version.EDatabase.AdvantageDatabase)
                                {
                                    updateMethodInfo.Invoke(
                                        constructorObject,
                                        new object[]
                                            {
                                                advantageConnectionString, tenant.DatabaseName, tenant.DatabaseName, version,
                                                this.currentAdvantageDatabaseId
                                            });
                                }

                                version.Status = Version.EScriptStatus.Executed;
                                var sortedVersionUpdate = this.sortedVersions.FirstOrDefault(
                                    x => x.Major == version.Major && x.Minor == version.Minor && x.Build == version.Build
                                         && x.Revision == version.Revision);

                                if (sortedVersionUpdate != null)
                                {
                                    sortedVersionUpdate.VersionEnd = DateTime.Now;
                                }

                                if (shouldUpdateUi)
                                {
                                    this.RefreshHistoryGrid("Executed " + version.Description);
                                }
                            }
                            catch (Exception e)
                            {
                                completed = false;
                                if (shouldUpdateUi)
                                {
                                    MessageBox.Show(
                                        e.InnerException != null ? e.InnerException.Message : e.Message,
                                        @"An error has occurred!",
                                        MessageBoxButtons.OK,
                                        MessageBoxIcon.Error);
                                    version.Status = Version.EScriptStatus.Pending;
                                    this.updateDatabaseButton.Invoke(
                                        (MethodInvoker)delegate { this.updateDatabaseButton.Visible = true; });
                                    this.RefreshHistoryGrid(
                                        "An error has occurred. "
                                        + (e.InnerException != null ? e.InnerException.Message : e.Message));
                                }
                                else
                                {
                                    throw new Exception(e.InnerException != null ? e.InnerException.Message : e.Message);
                                }

                                break;
                            }
                        }
                    }
                }
            }

            if (completed)
            {
                if (shouldUpdateUi)
                {
                    this.currentVersionMaterialTextField.Invoke(
                        (MethodInvoker)delegate
                        {
                            this.currentVersionMaterialTextField.Text = this.PreviousVersion = this.NewerVersion;
                        });
                    MessageBox.Show(
                        @"Database Updated Successfully",
                        @"Database update completed!",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Information);
                    this.RefreshHistoryGrid("Database Updated Successfully!");
                }
            }

            return completed;
        }


        /// <summary>
        /// The update database thread.
        /// </summary>
        /// <param name="tenant">
        /// The tenant.
        /// </param>
        /// <param name="environment">
        /// The environment.
        /// </param>
        /// <param name="shouldUpdateUi">
        /// The should update UI.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private bool UpdateDatabaseThread(Tenant tenant, Models.Environment environment, bool shouldUpdateUi)
        {
            bool completed = true;

            try
            {
                if (environment != null && tenant != null)
                {
                    this.updateDatabaseThread = new Thread(
                        () =>
                            {
                                completed = this.UpdateDatabase(tenant, environment, shouldUpdateUi);
                            });

                    this.updateDatabaseThread.Start();
                }
            }
            catch (Exception e)
            {
                if (shouldUpdateUi)
                {
                    this.updateDatabaseButton.Invoke(
                        (MethodInvoker)delegate { this.updateDatabaseButton.Visible = true; });
                    MessageBox.Show(
                        e.InnerException != null ? e.InnerException.Message : e.Message,
                        @"An error has occurred!",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                }
                else
                {
                    throw new Exception(e.InnerException != null ? e.InnerException.Message : e.Message);
                }
            }

            return completed;
        }


        /// <summary>
        /// The bind current history.
        /// </summary>
        private void BindCurrentHistory()
        {
            this.currentVersionMaterialTextField.Text = this.PreviousVersion;
            this.newerVersionMaterialTextField.Text = this.NewerVersion;

            this.versionHistoryDataGridView.AutoGenerateColumns = false;
            this.versionHistoryDataGridView.DataSource = this.sortedVersions;
            this.versionHistoryDataGridView.Refresh();

            if (this.previousVersionNumber < this.newerVersionNumber)
            {
                this.updateDatabaseButton.Visible = true;
                this.statusLabel.Text = string.Empty;
            }
            else
            {
                this.updateDatabaseButton.Visible = false;
                this.statusLabel.Text = @"Database is up to date!";
            }
        }

        /// <summary>
        /// The get build number from version.
        /// </summary>
        /// <param name="version">
        /// The version.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        private int GetBuildNumberFromVersion(Version version)
        {
            var versionBuild = version.Build <= 9 ? "0" + version.Build : version.Build.ToString();
            var versionRevision = version.Revision <= 9 ? "0" + version.Revision : version.Revision.ToString();
            var buildNumber = int.Parse($@"{version.Major}{version.Minor}{versionBuild}{versionRevision}");
            return buildNumber;
        }

        /// <summary>
        /// The refresh history grid.
        /// </summary>
        /// <param name="status">
        /// The status.
        /// </param>
        private void RefreshHistoryGrid(string status)
        {
            this.versionHistoryDataGridView.Invoke(
                (MethodInvoker)delegate
                    {
                        this.versionHistoryDataGridView.DataSource = this.sortedVersions;
                        this.versionHistoryDataGridView.Refresh();
                    });
            this.statusLabel.Invoke((MethodInvoker)delegate { this.statusLabel.Text = status; });
        }


        /// <summary>
        /// The environment name combo box selected index changed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void EnvironmentNameComboBoxSelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.tenants = this.GetTenantList(
                    this.environments.FirstOrDefault(
                        x => x.Name == this.environmentNameComboBox.SelectedItem.ToString()));

                this.tenantNameComboBox.Items.Clear();

                foreach (var tenant in this.tenants)
                {
                    this.tenantNameComboBox.Items.Add(tenant.Name);
                }
            }
            catch (Exception exception)
            {
                MessageBox.Show(
                    @"An un-handle exception has occurred: " + exception.Message,
                    @"An un-handle exception has occurred:",
                    MessageBoxButtons.OK);
            }
        }

        /// <summary>
        /// The tenant name combo box selected index changed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void TenantNameComboBoxSelectedIndexChanged(object sender, EventArgs e)
        {
            this.RefreshSelectedTenant();
        }


        /// <summary>
        /// The load environments.
        /// </summary>
        private void LoadEnvironments()
        {
            if (this.environments != null)
            {
                this.environmentNameComboBox.Items.Clear();

                foreach (var environment in this.environments)
                {
                    this.environmentNameComboBox.Items.Add(environment.Name);
                }
            }
        }

        /// <summary>
        /// The install SQL jobs thread.
        /// </summary>
        private void InstallSqlJobsThread()
        {
            if (this.tenantNameComboBox.SelectedItem != null && this.environmentNameComboBox.SelectedItem != null)
            {
                var tenant = this.tenants.FirstOrDefault(x => x.Name == this.tenantNameComboBox.SelectedItem.ToString());
                var environment =
                    this.environments.FirstOrDefault(x => x.Name == this.environmentNameComboBox.SelectedItem.ToString());
                if (environment != null && tenant != null)
                {
                    string advantageConnectionString = DatabaseScriptRunner.GetConnectionString(
                        tenant.DatabaseName,
                        environment.DatabaseConfiguration.TenantAuthUserName,
                        environment.DatabaseConfiguration.TenantAuthDatabasePassword,
                        tenant.ServerName);


                    var currentVersion = "";

                    if (this.PreviousVersion != null)
                    {
                        currentVersion = this.PreviousVersion.Substring(
                           0,
                           this.PreviousVersion.IndexOf('.', this.PreviousVersion.IndexOf('.') + 1));
                        this.installSqlJobsThread = new Thread(
                            () => this.InstallSqlJobsWorker(tenant.DatabaseName, advantageConnectionString, currentVersion));

                        this.installSqlJobsThread.Start();
                    }
                    else
                    {
                        var confirmation = new ConfirmationBox("The version of the selected tenant could not be determined. Would you like to install the sql jobs using the latest version defined in the version manifest?");
                        confirmation.Show();
                        confirmation.confirmButtonClicked += (s, e) =>
                            {

                                var newerDatabaseVersion = this.versionManifest.Versions.OrderByDescending(x => x.Major)
                                    .ThenByDescending(x => x.Minor).ThenByDescending(x => x.Build).ThenByDescending(x => x.Revision)
                                    .First();
                                currentVersion = newerDatabaseVersion.Major + "." + newerDatabaseVersion.Minor;
                                this.installSqlJobsThread = new Thread(
                                    () => this.InstallSqlJobsWorker(tenant.DatabaseName, advantageConnectionString, currentVersion));

                                this.installSqlJobsThread.Start();
                            };
                    }

                }
            }
            else
            {
                MessageBox.Show(
                    @"The environment and tenant needs to be selected to run this operation.",
                    @"An error has occurred!",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                this.installSqlJobsButton.Invoke(
                    (MethodInvoker)delegate { this.installSqlJobsButton.Visible = true; });

            }
        }

        /// <summary>
        /// The install SQL jobs worker function.
        /// </summary>
        /// <param name="databaseName">
        /// The database Name.
        /// </param>
        /// <param name="connectionString">
        /// The connection string.
        /// </param>
        /// <param name="version">
        /// The version.
        /// </param>
        private void InstallSqlJobsWorker(string databaseName, string connectionString, string version)
        {
            DatabaseScriptRunner sr = new DatabaseScriptRunner();

            try
            {
                sr.InstallJobs(databaseName, connectionString, version);
            }
            catch (Exception e)
            {
                MessageBox.Show(
                    e.InnerException != null ? e.InnerException.Message : e.Message,
                    @"An error has occurred!",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
                this.installSqlJobsButton.Invoke(
                    (MethodInvoker)delegate { this.installSqlJobsButton.Visible = true; });
                this.RefreshHistoryGrid(
                    "An error has occurred. "
                    + (e.InnerException != null ? e.InnerException.Message : e.Message));
            }

            MessageBox.Show(
                $@"All SQL jobs have been installed ",
                @"Job Installation Completed!",
                MessageBoxButtons.OK,
                MessageBoxIcon.Information);

            this.installSqlJobsButton.Invoke(
                (MethodInvoker)delegate { this.installSqlJobsButton.Visible = true; });
        }
    }
}
