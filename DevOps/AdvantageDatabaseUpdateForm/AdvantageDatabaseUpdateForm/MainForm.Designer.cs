﻿namespace AdvantageDatabaseUpdateForm
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.tabControl = new MaterialSkin.Controls.MaterialTabControl();
            this.databaseManagmentTab = new System.Windows.Forms.TabPage();
            this.mrbInstallStarterDatabase = new MaterialSkin.Controls.MaterialRaisedButton();
            this.mrbInstallSqlJobs = new MaterialSkin.Controls.MaterialRaisedButton();
            this.mtlStatus = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel38 = new MaterialSkin.Controls.MaterialLabel();
            this.mrbUpdateDatabase = new MaterialSkin.Controls.MaterialRaisedButton();
            this.mrbInstallNewEnviroment = new MaterialSkin.Controls.MaterialRaisedButton();
            this.mrbInstallNewTenant = new MaterialSkin.Controls.MaterialRaisedButton();
            this.dgvVersionHistory = new System.Windows.Forms.DataGridView();
            this.Status = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Description = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Major = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Minor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Build = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Revision = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DateOfExecution = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mtfNewerVersion = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel4 = new MaterialSkin.Controls.MaterialLabel();
            this.mtfCurrentVersion = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel3 = new MaterialSkin.Controls.MaterialLabel();
            this.mrbBackupDatabases = new MaterialSkin.Controls.MaterialRaisedButton();
            this.cbTenantName = new System.Windows.Forms.ComboBox();
            this.materialLabel1 = new MaterialSkin.Controls.MaterialLabel();
            this.cbEnvironmentName = new System.Windows.Forms.ComboBox();
            this.materialLabel2 = new MaterialSkin.Controls.MaterialLabel();
            this.advantageAPITab = new System.Windows.Forms.TabPage();
            this.materialLabel47 = new MaterialSkin.Controls.MaterialLabel();
            this.TenantAdvantageApiComboBox = new System.Windows.Forms.ComboBox();
            this.materialLabel45 = new MaterialSkin.Controls.MaterialLabel();
            this.EnvironmentApiTabComboBox = new System.Windows.Forms.ComboBox();
            this.materialLabel46 = new MaterialSkin.Controls.MaterialLabel();
            this.LaunchApiButton = new MaterialSkin.Controls.MaterialRaisedButton();
            this.materialLabel44 = new MaterialSkin.Controls.MaterialLabel();
            this.SaveAdvantageApiUrlButton = new MaterialSkin.Controls.MaterialRaisedButton();
            this.mtfAdvantageApiUrl = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel43 = new MaterialSkin.Controls.MaterialLabel();
            this.InstalCoreWebHostingButton = new MaterialSkin.Controls.MaterialRaisedButton();
            this.materialLabel41 = new MaterialSkin.Controls.MaterialLabel();
            this.InstallCoreRuntimeButton = new MaterialSkin.Controls.MaterialRaisedButton();
            this.materialLabel40 = new MaterialSkin.Controls.MaterialLabel();
            this.materialLabel39 = new MaterialSkin.Controls.MaterialLabel();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.MrbInstalCrystal64Bit = new MaterialSkin.Controls.MaterialRaisedButton();
            this.materialLabel48 = new MaterialSkin.Controls.MaterialLabel();
            this.MrbInstalCrystal32Bit = new MaterialSkin.Controls.MaterialRaisedButton();
            this.materialLabel42 = new MaterialSkin.Controls.MaterialLabel();
            this.configurationTab = new System.Windows.Forms.TabPage();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.mcbApiAppPath = new MaterialSkin.Controls.MaterialCheckBox();
            this.mcbWapiAppPath = new MaterialSkin.Controls.MaterialCheckBox();
            this.mcbWebServiceAppPath = new MaterialSkin.Controls.MaterialCheckBox();
            this.mcbHostApplicationPath = new MaterialSkin.Controls.MaterialCheckBox();
            this.mcbSiteApplicationPath = new MaterialSkin.Controls.MaterialCheckBox();
            this.mfbApiAppPath = new MaterialSkin.Controls.MaterialFlatButton();
            this.mtfApiAppPath = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel36 = new MaterialSkin.Controls.MaterialLabel();
            this.mtfApiAppName = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel37 = new MaterialSkin.Controls.MaterialLabel();
            this.mfbWapiApPath = new MaterialSkin.Controls.MaterialFlatButton();
            this.mtfWapiAppPath = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel34 = new MaterialSkin.Controls.MaterialLabel();
            this.mtfWapiAppName = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel35 = new MaterialSkin.Controls.MaterialLabel();
            this.mfbWebServiceAppPath = new MaterialSkin.Controls.MaterialFlatButton();
            this.mtfWebServiceAppPath = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel32 = new MaterialSkin.Controls.MaterialLabel();
            this.mtfWebServiceAppName = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel33 = new MaterialSkin.Controls.MaterialLabel();
            this.mfbHostApplicationPath = new MaterialSkin.Controls.MaterialFlatButton();
            this.mtfHostApplicationPath = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel30 = new MaterialSkin.Controls.MaterialLabel();
            this.mtfHostApplicationName = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel31 = new MaterialSkin.Controls.MaterialLabel();
            this.mfbSiteApplicationPath = new MaterialSkin.Controls.MaterialFlatButton();
            this.mtfSiteApplicationPath = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel29 = new MaterialSkin.Controls.MaterialLabel();
            this.mtfSiteApplicationName = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel28 = new MaterialSkin.Controls.MaterialLabel();
            this.mrbSaveEnviroment = new MaterialSkin.Controls.MaterialRaisedButton();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.mtfReportServerFolder = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel24 = new MaterialSkin.Controls.MaterialLabel();
            this.mtfReportServicesUrl = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel25 = new MaterialSkin.Controls.MaterialLabel();
            this.mtfReportExecutionUrl = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel26 = new MaterialSkin.Controls.MaterialLabel();
            this.mtfReportServerUrl = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel27 = new MaterialSkin.Controls.MaterialLabel();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.mfbReleasePackagePath = new MaterialSkin.Controls.MaterialFlatButton();
            this.mfbDatabaseLogInstallation = new MaterialSkin.Controls.MaterialFlatButton();
            this.mfbDatabaseInstallationLocation = new MaterialSkin.Controls.MaterialFlatButton();
            this.mfbSiteBackupLocation = new MaterialSkin.Controls.MaterialFlatButton();
            this.mfbDatabaseBackupLocation = new MaterialSkin.Controls.MaterialFlatButton();
            this.mtfReleasePackagePath = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel23 = new MaterialSkin.Controls.MaterialLabel();
            this.mtfDatabaseLogPath = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel20 = new MaterialSkin.Controls.MaterialLabel();
            this.mtfDatabaseInstallationPath = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel19 = new MaterialSkin.Controls.MaterialLabel();
            this.mtfSiteBackupLocation = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel21 = new MaterialSkin.Controls.MaterialLabel();
            this.mtfDatabaseBackupLocation = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel22 = new MaterialSkin.Controls.MaterialLabel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.mcbVersionVirtualDirectoryPath = new MaterialSkin.Controls.MaterialCheckBox();
            this.mcbEnvironmentVirtualDirectoryPath = new MaterialSkin.Controls.MaterialCheckBox();
            this.mcbProductVirtualDirectoryPath = new MaterialSkin.Controls.MaterialCheckBox();
            this.mfbVersionVirtualDirectoryPath = new MaterialSkin.Controls.MaterialFlatButton();
            this.mfbEnvironmentVirtualDirectoryPath = new MaterialSkin.Controls.MaterialFlatButton();
            this.mfbProductVirtualDirectoryPath = new MaterialSkin.Controls.MaterialFlatButton();
            this.mfbDefaultWebSitePath = new MaterialSkin.Controls.MaterialFlatButton();
            this.mtfVeresionVirtualDirectoryPath = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel17 = new MaterialSkin.Controls.MaterialLabel();
            this.mtfVersionVirtualDirectoryName = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel18 = new MaterialSkin.Controls.MaterialLabel();
            this.mtfEnvironmentVirtualDirectoryPath = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel15 = new MaterialSkin.Controls.MaterialLabel();
            this.mtfEnviromentVirtualDirectoryName = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel16 = new MaterialSkin.Controls.MaterialLabel();
            this.mtfProductVirtualDirectoryPath = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel11 = new MaterialSkin.Controls.MaterialLabel();
            this.mtfProductVirtualDirectoryName = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel12 = new MaterialSkin.Controls.MaterialLabel();
            this.mtfDefaultWebsitePath = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel13 = new MaterialSkin.Controls.MaterialLabel();
            this.mtfDefaultWebSiteName = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel14 = new MaterialSkin.Controls.MaterialLabel();
            this.mtfEnviromentName = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel6 = new MaterialSkin.Controls.MaterialLabel();
            this.mrbAddEnviroment = new MaterialSkin.Controls.MaterialRaisedButton();
            this.cbEnviroment = new System.Windows.Forms.ComboBox();
            this.materialLabel5 = new MaterialSkin.Controls.MaterialLabel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.mtfTenantPassword = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel10 = new MaterialSkin.Controls.MaterialLabel();
            this.mtfTenantUserName = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel9 = new MaterialSkin.Controls.MaterialLabel();
            this.mtfTenantServerName = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel8 = new MaterialSkin.Controls.MaterialLabel();
            this.mtfTenantAuthDbName = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel7 = new MaterialSkin.Controls.MaterialLabel();
            this.deployTab = new System.Windows.Forms.TabPage();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.tabSelector = new MaterialSkin.Controls.MaterialTabSelector();
            this.mrbRefresh = new MaterialSkin.Controls.MaterialRaisedButton();
            this.tabControl.SuspendLayout();
            this.databaseManagmentTab.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVersionHistory)).BeginInit();
            this.advantageAPITab.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.configurationTab.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl.Controls.Add(this.databaseManagmentTab);
            this.tabControl.Controls.Add(this.advantageAPITab);
            this.tabControl.Controls.Add(this.tabPage1);
            this.tabControl.Depth = 0;
            this.tabControl.Location = new System.Drawing.Point(0, 110);
            this.tabControl.MouseState = MaterialSkin.MouseState.HOVER;
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(1372, 898);
            this.tabControl.TabIndex = 0;
            this.tabControl.SelectedIndexChanged += new System.EventHandler(this.tabControl_SelectedIndexChanged);
            // 
            // databaseManagmentTab
            // 
            this.databaseManagmentTab.AutoScroll = true;
            this.databaseManagmentTab.AutoScrollMinSize = new System.Drawing.Size(0, -50);
            this.databaseManagmentTab.BackColor = System.Drawing.SystemColors.Control;
            this.databaseManagmentTab.Controls.Add(this.mrbRefresh);
            this.databaseManagmentTab.Controls.Add(this.mrbInstallStarterDatabase);
            this.databaseManagmentTab.Controls.Add(this.mrbInstallSqlJobs);
            this.databaseManagmentTab.Controls.Add(this.mtlStatus);
            this.databaseManagmentTab.Controls.Add(this.materialLabel38);
            this.databaseManagmentTab.Controls.Add(this.mrbUpdateDatabase);
            this.databaseManagmentTab.Controls.Add(this.mrbInstallNewEnviroment);
            this.databaseManagmentTab.Controls.Add(this.mrbInstallNewTenant);
            this.databaseManagmentTab.Controls.Add(this.dgvVersionHistory);
            this.databaseManagmentTab.Controls.Add(this.mtfNewerVersion);
            this.databaseManagmentTab.Controls.Add(this.materialLabel4);
            this.databaseManagmentTab.Controls.Add(this.mtfCurrentVersion);
            this.databaseManagmentTab.Controls.Add(this.materialLabel3);
            this.databaseManagmentTab.Controls.Add(this.mrbBackupDatabases);
            this.databaseManagmentTab.Controls.Add(this.cbTenantName);
            this.databaseManagmentTab.Controls.Add(this.materialLabel1);
            this.databaseManagmentTab.Controls.Add(this.cbEnvironmentName);
            this.databaseManagmentTab.Controls.Add(this.materialLabel2);
            this.databaseManagmentTab.Location = new System.Drawing.Point(4, 22);
            this.databaseManagmentTab.Name = "databaseManagmentTab";
            this.databaseManagmentTab.Padding = new System.Windows.Forms.Padding(3);
            this.databaseManagmentTab.Size = new System.Drawing.Size(1364, 872);
            this.databaseManagmentTab.TabIndex = 0;
            this.databaseManagmentTab.Text = "Database Managment";
            // 
            // mrbInstallStarterDatabase
            // 
            this.mrbInstallStarterDatabase.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.mrbInstallStarterDatabase.Depth = 0;
            this.mrbInstallStarterDatabase.Location = new System.Drawing.Point(200, 691);
            this.mrbInstallStarterDatabase.MouseState = MaterialSkin.MouseState.HOVER;
            this.mrbInstallStarterDatabase.Name = "mrbInstallStarterDatabase";
            this.mrbInstallStarterDatabase.Primary = true;
            this.mrbInstallStarterDatabase.Size = new System.Drawing.Size(169, 40);
            this.mrbInstallStarterDatabase.TabIndex = 24;
            this.mrbInstallStarterDatabase.Text = "Install Starter Database";
            this.mrbInstallStarterDatabase.UseVisualStyleBackColor = true;
            this.mrbInstallStarterDatabase.Click += new System.EventHandler(this.MrbInstallStarterDatabaseClick);
            // 
            // mrbInstallSqlJobs
            // 
            this.mrbInstallSqlJobs.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.mrbInstallSqlJobs.Depth = 0;
            this.mrbInstallSqlJobs.Location = new System.Drawing.Point(25, 691);
            this.mrbInstallSqlJobs.MouseState = MaterialSkin.MouseState.HOVER;
            this.mrbInstallSqlJobs.Name = "mrbInstallSqlJobs";
            this.mrbInstallSqlJobs.Primary = true;
            this.mrbInstallSqlJobs.Size = new System.Drawing.Size(169, 40);
            this.mrbInstallSqlJobs.TabIndex = 23;
            this.mrbInstallSqlJobs.Text = "Install Sql Jobs";
            this.mrbInstallSqlJobs.UseVisualStyleBackColor = true;
            this.mrbInstallSqlJobs.Click += new System.EventHandler(this.mrbInstallSqlJobs_Click);
            // 
            // mtlStatus
            // 
            this.mtlStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.mtlStatus.AutoSize = true;
            this.mtlStatus.Depth = 0;
            this.mtlStatus.Font = new System.Drawing.Font("Roboto", 11F);
            this.mtlStatus.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.mtlStatus.Location = new System.Drawing.Point(86, 786);
            this.mtlStatus.MouseState = MaterialSkin.MouseState.HOVER;
            this.mtlStatus.Name = "mtlStatus";
            this.mtlStatus.Size = new System.Drawing.Size(0, 19);
            this.mtlStatus.TabIndex = 22;
            // 
            // materialLabel38
            // 
            this.materialLabel38.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.materialLabel38.AutoSize = true;
            this.materialLabel38.Depth = 0;
            this.materialLabel38.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel38.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel38.Location = new System.Drawing.Point(24, 786);
            this.materialLabel38.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel38.Name = "materialLabel38";
            this.materialLabel38.Size = new System.Drawing.Size(56, 19);
            this.materialLabel38.TabIndex = 21;
            this.materialLabel38.Text = "Status:";
            // 
            // mrbUpdateDatabase
            // 
            this.mrbUpdateDatabase.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.mrbUpdateDatabase.Depth = 0;
            this.mrbUpdateDatabase.Location = new System.Drawing.Point(1173, 691);
            this.mrbUpdateDatabase.MouseState = MaterialSkin.MouseState.HOVER;
            this.mrbUpdateDatabase.Name = "mrbUpdateDatabase";
            this.mrbUpdateDatabase.Primary = true;
            this.mrbUpdateDatabase.Size = new System.Drawing.Size(169, 40);
            this.mrbUpdateDatabase.TabIndex = 20;
            this.mrbUpdateDatabase.Text = "Update Database to Latest Version";
            this.mrbUpdateDatabase.UseVisualStyleBackColor = true;
            this.mrbUpdateDatabase.Click += new System.EventHandler(this.UpdateDatabaseButton_Click);
            // 
            // mrbInstallNewEnviroment
            // 
            this.mrbInstallNewEnviroment.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.mrbInstallNewEnviroment.Depth = 0;
            this.mrbInstallNewEnviroment.Enabled = false;
            this.mrbInstallNewEnviroment.Location = new System.Drawing.Point(158, 816);
            this.mrbInstallNewEnviroment.MouseState = MaterialSkin.MouseState.HOVER;
            this.mrbInstallNewEnviroment.Name = "mrbInstallNewEnviroment";
            this.mrbInstallNewEnviroment.Primary = true;
            this.mrbInstallNewEnviroment.Size = new System.Drawing.Size(126, 40);
            this.mrbInstallNewEnviroment.TabIndex = 19;
            this.mrbInstallNewEnviroment.Text = "Install New Environment";
            this.mrbInstallNewEnviroment.UseVisualStyleBackColor = true;
            this.mrbInstallNewEnviroment.Visible = false;
            // 
            // mrbInstallNewTenant
            // 
            this.mrbInstallNewTenant.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.mrbInstallNewTenant.Depth = 0;
            this.mrbInstallNewTenant.Enabled = false;
            this.mrbInstallNewTenant.Location = new System.Drawing.Point(26, 816);
            this.mrbInstallNewTenant.MouseState = MaterialSkin.MouseState.HOVER;
            this.mrbInstallNewTenant.Name = "mrbInstallNewTenant";
            this.mrbInstallNewTenant.Primary = true;
            this.mrbInstallNewTenant.Size = new System.Drawing.Size(126, 40);
            this.mrbInstallNewTenant.TabIndex = 18;
            this.mrbInstallNewTenant.Text = "Install new Tenant";
            this.mrbInstallNewTenant.UseVisualStyleBackColor = true;
            this.mrbInstallNewTenant.Visible = false;
            // 
            // dgvVersionHistory
            // 
            this.dgvVersionHistory.AllowUserToAddRows = false;
            this.dgvVersionHistory.AllowUserToDeleteRows = false;
            this.dgvVersionHistory.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvVersionHistory.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvVersionHistory.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Status,
            this.Description,
            this.Major,
            this.Minor,
            this.Build,
            this.Revision,
            this.DateOfExecution});
            this.dgvVersionHistory.Location = new System.Drawing.Point(26, 119);
            this.dgvVersionHistory.Name = "dgvVersionHistory";
            this.dgvVersionHistory.ReadOnly = true;
            this.dgvVersionHistory.Size = new System.Drawing.Size(1316, 569);
            this.dgvVersionHistory.TabIndex = 17;
            // 
            // Status
            // 
            this.Status.DataPropertyName = "Status";
            this.Status.HeaderText = "Status";
            this.Status.Name = "Status";
            this.Status.ReadOnly = true;
            // 
            // Description
            // 
            this.Description.DataPropertyName = "Description";
            this.Description.HeaderText = "Description";
            this.Description.Name = "Description";
            this.Description.ReadOnly = true;
            this.Description.Width = 300;
            // 
            // Major
            // 
            this.Major.DataPropertyName = "Major";
            this.Major.HeaderText = "Major";
            this.Major.Name = "Major";
            this.Major.ReadOnly = true;
            // 
            // Minor
            // 
            this.Minor.DataPropertyName = "Minor";
            this.Minor.HeaderText = "Minor";
            this.Minor.Name = "Minor";
            this.Minor.ReadOnly = true;
            // 
            // Build
            // 
            this.Build.DataPropertyName = "Build";
            this.Build.HeaderText = "Build";
            this.Build.Name = "Build";
            this.Build.ReadOnly = true;
            // 
            // Revision
            // 
            this.Revision.DataPropertyName = "Revision";
            this.Revision.HeaderText = "Revision";
            this.Revision.Name = "Revision";
            this.Revision.ReadOnly = true;
            // 
            // DateOfExecution
            // 
            this.DateOfExecution.DataPropertyName = "VersionEnd";
            this.DateOfExecution.HeaderText = "Date and Time of Execution";
            this.DateOfExecution.Name = "DateOfExecution";
            this.DateOfExecution.ReadOnly = true;
            this.DateOfExecution.Width = 170;
            // 
            // mtfNewerVersion
            // 
            this.mtfNewerVersion.BackColor = System.Drawing.Color.White;
            this.mtfNewerVersion.Depth = 0;
            this.mtfNewerVersion.Enabled = false;
            this.mtfNewerVersion.Hint = "";
            this.mtfNewerVersion.Location = new System.Drawing.Point(520, 60);
            this.mtfNewerVersion.MouseState = MaterialSkin.MouseState.HOVER;
            this.mtfNewerVersion.Name = "mtfNewerVersion";
            this.mtfNewerVersion.PasswordChar = '\0';
            this.mtfNewerVersion.SelectedText = "";
            this.mtfNewerVersion.SelectionLength = 0;
            this.mtfNewerVersion.SelectionStart = 0;
            this.mtfNewerVersion.Size = new System.Drawing.Size(233, 23);
            this.mtfNewerVersion.TabIndex = 16;
            this.mtfNewerVersion.UseSystemPasswordChar = false;
            // 
            // materialLabel4
            // 
            this.materialLabel4.AutoSize = true;
            this.materialLabel4.BackColor = System.Drawing.Color.White;
            this.materialLabel4.Depth = 0;
            this.materialLabel4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.materialLabel4.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel4.Location = new System.Drawing.Point(402, 64);
            this.materialLabel4.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel4.Name = "materialLabel4";
            this.materialLabel4.Size = new System.Drawing.Size(112, 19);
            this.materialLabel4.TabIndex = 15;
            this.materialLabel4.Text = "Newer Version:";
            // 
            // mtfCurrentVersion
            // 
            this.mtfCurrentVersion.BackColor = System.Drawing.Color.White;
            this.mtfCurrentVersion.Depth = 0;
            this.mtfCurrentVersion.Enabled = false;
            this.mtfCurrentVersion.Hint = "";
            this.mtfCurrentVersion.Location = new System.Drawing.Point(148, 60);
            this.mtfCurrentVersion.MouseState = MaterialSkin.MouseState.HOVER;
            this.mtfCurrentVersion.Name = "mtfCurrentVersion";
            this.mtfCurrentVersion.PasswordChar = '\0';
            this.mtfCurrentVersion.SelectedText = "";
            this.mtfCurrentVersion.SelectionLength = 0;
            this.mtfCurrentVersion.SelectionStart = 0;
            this.mtfCurrentVersion.Size = new System.Drawing.Size(233, 23);
            this.mtfCurrentVersion.TabIndex = 14;
            this.mtfCurrentVersion.UseSystemPasswordChar = false;
            // 
            // materialLabel3
            // 
            this.materialLabel3.AutoSize = true;
            this.materialLabel3.BackColor = System.Drawing.Color.White;
            this.materialLabel3.Depth = 0;
            this.materialLabel3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.materialLabel3.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel3.Location = new System.Drawing.Point(24, 64);
            this.materialLabel3.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel3.Name = "materialLabel3";
            this.materialLabel3.Size = new System.Drawing.Size(118, 19);
            this.materialLabel3.TabIndex = 13;
            this.materialLabel3.Text = "Current Version:";
            // 
            // mrbBackupDatabases
            // 
            this.mrbBackupDatabases.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.mrbBackupDatabases.Depth = 0;
            this.mrbBackupDatabases.Enabled = false;
            this.mrbBackupDatabases.Location = new System.Drawing.Point(290, 816);
            this.mrbBackupDatabases.MouseState = MaterialSkin.MouseState.HOVER;
            this.mrbBackupDatabases.Name = "mrbBackupDatabases";
            this.mrbBackupDatabases.Primary = true;
            this.mrbBackupDatabases.Size = new System.Drawing.Size(126, 40);
            this.mrbBackupDatabases.TabIndex = 12;
            this.mrbBackupDatabases.Text = "Backup the Databases";
            this.mrbBackupDatabases.UseVisualStyleBackColor = true;
            this.mrbBackupDatabases.Visible = false;
            // 
            // cbTenantName
            // 
            this.cbTenantName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbTenantName.FormattingEnabled = true;
            this.cbTenantName.Location = new System.Drawing.Point(557, 22);
            this.cbTenantName.Name = "cbTenantName";
            this.cbTenantName.Size = new System.Drawing.Size(233, 21);
            this.cbTenantName.TabIndex = 8;
            // 
            // materialLabel1
            // 
            this.materialLabel1.AutoSize = true;
            this.materialLabel1.BackColor = System.Drawing.Color.White;
            this.materialLabel1.Depth = 0;
            this.materialLabel1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.materialLabel1.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel1.Location = new System.Drawing.Point(446, 24);
            this.materialLabel1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel1.Name = "materialLabel1";
            this.materialLabel1.Size = new System.Drawing.Size(105, 19);
            this.materialLabel1.TabIndex = 7;
            this.materialLabel1.Text = "Select Tenant:";
            // 
            // cbEnvironmentName
            // 
            this.cbEnvironmentName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbEnvironmentName.FormattingEnabled = true;
            this.cbEnvironmentName.Location = new System.Drawing.Point(181, 22);
            this.cbEnvironmentName.Name = "cbEnvironmentName";
            this.cbEnvironmentName.Size = new System.Drawing.Size(233, 21);
            this.cbEnvironmentName.TabIndex = 6;
            // 
            // materialLabel2
            // 
            this.materialLabel2.AutoSize = true;
            this.materialLabel2.BackColor = System.Drawing.Color.White;
            this.materialLabel2.Depth = 0;
            this.materialLabel2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.materialLabel2.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel2.Location = new System.Drawing.Point(22, 24);
            this.materialLabel2.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel2.Name = "materialLabel2";
            this.materialLabel2.Size = new System.Drawing.Size(143, 19);
            this.materialLabel2.TabIndex = 5;
            this.materialLabel2.Text = "Select Environment:";
            // 
            // advantageAPITab
            // 
            this.advantageAPITab.BackColor = System.Drawing.SystemColors.Control;
            this.advantageAPITab.Controls.Add(this.materialLabel47);
            this.advantageAPITab.Controls.Add(this.TenantAdvantageApiComboBox);
            this.advantageAPITab.Controls.Add(this.materialLabel45);
            this.advantageAPITab.Controls.Add(this.EnvironmentApiTabComboBox);
            this.advantageAPITab.Controls.Add(this.materialLabel46);
            this.advantageAPITab.Controls.Add(this.LaunchApiButton);
            this.advantageAPITab.Controls.Add(this.materialLabel44);
            this.advantageAPITab.Controls.Add(this.SaveAdvantageApiUrlButton);
            this.advantageAPITab.Controls.Add(this.mtfAdvantageApiUrl);
            this.advantageAPITab.Controls.Add(this.materialLabel43);
            this.advantageAPITab.Controls.Add(this.InstalCoreWebHostingButton);
            this.advantageAPITab.Controls.Add(this.materialLabel41);
            this.advantageAPITab.Controls.Add(this.InstallCoreRuntimeButton);
            this.advantageAPITab.Controls.Add(this.materialLabel40);
            this.advantageAPITab.Controls.Add(this.materialLabel39);
            this.advantageAPITab.Location = new System.Drawing.Point(4, 22);
            this.advantageAPITab.Name = "advantageAPITab";
            this.advantageAPITab.Padding = new System.Windows.Forms.Padding(3);
            this.advantageAPITab.Size = new System.Drawing.Size(1364, 872);
            this.advantageAPITab.TabIndex = 1;
            this.advantageAPITab.Text = "Advantage API";
            // 
            // materialLabel47
            // 
            this.materialLabel47.AutoSize = true;
            this.materialLabel47.Depth = 0;
            this.materialLabel47.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel47.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel47.Location = new System.Drawing.Point(83, 227);
            this.materialLabel47.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel47.Name = "materialLabel47";
            this.materialLabel47.Size = new System.Drawing.Size(732, 19);
            this.materialLabel47.TabIndex = 28;
            this.materialLabel47.Text = "3. Stop Here, Go back to the Release Notes for 3.10 to read more about How to Con" +
    "figure the IIS For the API";
            // 
            // TenantAdvantageApiComboBox
            // 
            this.TenantAdvantageApiComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.TenantAdvantageApiComboBox.FormattingEnabled = true;
            this.TenantAdvantageApiComboBox.Location = new System.Drawing.Point(860, 288);
            this.TenantAdvantageApiComboBox.Name = "TenantAdvantageApiComboBox";
            this.TenantAdvantageApiComboBox.Size = new System.Drawing.Size(233, 21);
            this.TenantAdvantageApiComboBox.TabIndex = 27;
            // 
            // materialLabel45
            // 
            this.materialLabel45.AutoSize = true;
            this.materialLabel45.BackColor = System.Drawing.Color.White;
            this.materialLabel45.Depth = 0;
            this.materialLabel45.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.materialLabel45.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel45.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel45.Location = new System.Drawing.Point(749, 290);
            this.materialLabel45.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel45.Name = "materialLabel45";
            this.materialLabel45.Size = new System.Drawing.Size(105, 19);
            this.materialLabel45.TabIndex = 26;
            this.materialLabel45.Text = "Select Tenant:";
            // 
            // EnvironmentApiTabComboBox
            // 
            this.EnvironmentApiTabComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.EnvironmentApiTabComboBox.FormattingEnabled = true;
            this.EnvironmentApiTabComboBox.Location = new System.Drawing.Point(484, 288);
            this.EnvironmentApiTabComboBox.Name = "EnvironmentApiTabComboBox";
            this.EnvironmentApiTabComboBox.Size = new System.Drawing.Size(233, 21);
            this.EnvironmentApiTabComboBox.TabIndex = 25;
            // 
            // materialLabel46
            // 
            this.materialLabel46.AutoSize = true;
            this.materialLabel46.BackColor = System.Drawing.Color.White;
            this.materialLabel46.Depth = 0;
            this.materialLabel46.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.materialLabel46.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel46.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel46.Location = new System.Drawing.Point(325, 290);
            this.materialLabel46.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel46.Name = "materialLabel46";
            this.materialLabel46.Size = new System.Drawing.Size(143, 19);
            this.materialLabel46.TabIndex = 24;
            this.materialLabel46.Text = "Select Environment:";
            // 
            // LaunchApiButton
            // 
            this.LaunchApiButton.Depth = 0;
            this.LaunchApiButton.Location = new System.Drawing.Point(221, 391);
            this.LaunchApiButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.LaunchApiButton.Name = "LaunchApiButton";
            this.LaunchApiButton.Primary = true;
            this.LaunchApiButton.Size = new System.Drawing.Size(94, 37);
            this.LaunchApiButton.TabIndex = 23;
            this.LaunchApiButton.Text = "Launch";
            this.LaunchApiButton.UseVisualStyleBackColor = true;
            // 
            // materialLabel44
            // 
            this.materialLabel44.AutoSize = true;
            this.materialLabel44.Depth = 0;
            this.materialLabel44.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel44.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel44.Location = new System.Drawing.Point(82, 399);
            this.materialLabel44.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel44.Name = "materialLabel44";
            this.materialLabel44.Size = new System.Drawing.Size(129, 19);
            this.materialLabel44.TabIndex = 22;
            this.materialLabel44.Text = "5. Launch the API:";
            // 
            // SaveAdvantageApiUrlButton
            // 
            this.SaveAdvantageApiUrlButton.Depth = 0;
            this.SaveAdvantageApiUrlButton.Location = new System.Drawing.Point(989, 336);
            this.SaveAdvantageApiUrlButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.SaveAdvantageApiUrlButton.Name = "SaveAdvantageApiUrlButton";
            this.SaveAdvantageApiUrlButton.Primary = true;
            this.SaveAdvantageApiUrlButton.Size = new System.Drawing.Size(94, 37);
            this.SaveAdvantageApiUrlButton.TabIndex = 21;
            this.SaveAdvantageApiUrlButton.Text = "Save";
            this.SaveAdvantageApiUrlButton.UseVisualStyleBackColor = true;
            // 
            // mtfAdvantageApiUrl
            // 
            this.mtfAdvantageApiUrl.Depth = 0;
            this.mtfAdvantageApiUrl.Hint = "";
            this.mtfAdvantageApiUrl.Location = new System.Drawing.Point(326, 340);
            this.mtfAdvantageApiUrl.MouseState = MaterialSkin.MouseState.HOVER;
            this.mtfAdvantageApiUrl.Name = "mtfAdvantageApiUrl";
            this.mtfAdvantageApiUrl.PasswordChar = '\0';
            this.mtfAdvantageApiUrl.SelectedText = "";
            this.mtfAdvantageApiUrl.SelectionLength = 0;
            this.mtfAdvantageApiUrl.SelectionStart = 0;
            this.mtfAdvantageApiUrl.Size = new System.Drawing.Size(639, 23);
            this.mtfAdvantageApiUrl.TabIndex = 20;
            this.mtfAdvantageApiUrl.Text = "https://";
            this.mtfAdvantageApiUrl.UseSystemPasswordChar = false;
            // 
            // materialLabel43
            // 
            this.materialLabel43.AutoSize = true;
            this.materialLabel43.Depth = 0;
            this.materialLabel43.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel43.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel43.Location = new System.Drawing.Point(83, 282);
            this.materialLabel43.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel43.Name = "materialLabel43";
            this.materialLabel43.Size = new System.Drawing.Size(208, 19);
            this.materialLabel43.TabIndex = 7;
            this.materialLabel43.Text = "4. Set the Advantage API URL:";
            // 
            // InstalCoreWebHostingButton
            // 
            this.InstalCoreWebHostingButton.Depth = 0;
            this.InstalCoreWebHostingButton.Location = new System.Drawing.Point(364, 144);
            this.InstalCoreWebHostingButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.InstalCoreWebHostingButton.Name = "InstalCoreWebHostingButton";
            this.InstalCoreWebHostingButton.Primary = true;
            this.InstalCoreWebHostingButton.Size = new System.Drawing.Size(81, 37);
            this.InstalCoreWebHostingButton.TabIndex = 4;
            this.InstalCoreWebHostingButton.Text = "Install";
            this.InstalCoreWebHostingButton.UseVisualStyleBackColor = true;
            // 
            // materialLabel41
            // 
            this.materialLabel41.AutoSize = true;
            this.materialLabel41.Depth = 0;
            this.materialLabel41.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel41.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel41.Location = new System.Drawing.Point(83, 152);
            this.materialLabel41.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel41.Name = "materialLabel41";
            this.materialLabel41.Size = new System.Drawing.Size(275, 19);
            this.materialLabel41.TabIndex = 3;
            this.materialLabel41.Text = "2. Install the .NET Core IIS Web Hosting";
            // 
            // InstallCoreRuntimeButton
            // 
            this.InstallCoreRuntimeButton.Depth = 0;
            this.InstallCoreRuntimeButton.Location = new System.Drawing.Point(364, 64);
            this.InstallCoreRuntimeButton.MouseState = MaterialSkin.MouseState.HOVER;
            this.InstallCoreRuntimeButton.Name = "InstallCoreRuntimeButton";
            this.InstallCoreRuntimeButton.Primary = true;
            this.InstallCoreRuntimeButton.Size = new System.Drawing.Size(81, 37);
            this.InstallCoreRuntimeButton.TabIndex = 2;
            this.InstallCoreRuntimeButton.Text = "Install";
            this.InstallCoreRuntimeButton.UseVisualStyleBackColor = true;
            // 
            // materialLabel40
            // 
            this.materialLabel40.AutoSize = true;
            this.materialLabel40.Depth = 0;
            this.materialLabel40.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel40.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel40.Location = new System.Drawing.Point(83, 72);
            this.materialLabel40.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel40.Name = "materialLabel40";
            this.materialLabel40.Size = new System.Drawing.Size(223, 19);
            this.materialLabel40.TabIndex = 1;
            this.materialLabel40.Text = "1. Install the .NET Core Runtime";
            // 
            // materialLabel39
            // 
            this.materialLabel39.AutoSize = true;
            this.materialLabel39.Depth = 0;
            this.materialLabel39.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel39.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel39.Location = new System.Drawing.Point(394, 19);
            this.materialLabel39.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel39.Name = "materialLabel39";
            this.materialLabel39.Size = new System.Drawing.Size(433, 19);
            this.materialLabel39.TabIndex = 0;
            this.materialLabel39.Text = "To install and configure the Advantge API follow the next steps:";
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.MrbInstalCrystal64Bit);
            this.tabPage1.Controls.Add(this.materialLabel48);
            this.tabPage1.Controls.Add(this.MrbInstalCrystal32Bit);
            this.tabPage1.Controls.Add(this.materialLabel42);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1364, 872);
            this.tabPage1.TabIndex = 2;
            this.tabPage1.Text = "Dependencies";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // MrbInstalCrystal64Bit
            // 
            this.MrbInstalCrystal64Bit.Depth = 0;
            this.MrbInstalCrystal64Bit.Location = new System.Drawing.Point(428, 117);
            this.MrbInstalCrystal64Bit.MouseState = MaterialSkin.MouseState.HOVER;
            this.MrbInstalCrystal64Bit.Name = "MrbInstalCrystal64Bit";
            this.MrbInstalCrystal64Bit.Primary = true;
            this.MrbInstalCrystal64Bit.Size = new System.Drawing.Size(81, 37);
            this.MrbInstalCrystal64Bit.TabIndex = 6;
            this.MrbInstalCrystal64Bit.Text = "Install";
            this.MrbInstalCrystal64Bit.UseVisualStyleBackColor = true;
            this.MrbInstalCrystal64Bit.Click += new System.EventHandler(this.MrbInstalCrystal64Bit_Click);
            // 
            // materialLabel48
            // 
            this.materialLabel48.AutoSize = true;
            this.materialLabel48.Depth = 0;
            this.materialLabel48.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel48.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel48.Location = new System.Drawing.Point(18, 125);
            this.materialLabel48.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel48.Name = "materialLabel48";
            this.materialLabel48.Size = new System.Drawing.Size(342, 19);
            this.materialLabel48.TabIndex = 5;
            this.materialLabel48.Text = "2. Install Crystal Report Runtime 64bit v13.0 SP22 ";
            // 
            // MrbInstalCrystal32Bit
            // 
            this.MrbInstalCrystal32Bit.Depth = 0;
            this.MrbInstalCrystal32Bit.Location = new System.Drawing.Point(428, 39);
            this.MrbInstalCrystal32Bit.MouseState = MaterialSkin.MouseState.HOVER;
            this.MrbInstalCrystal32Bit.Name = "MrbInstalCrystal32Bit";
            this.MrbInstalCrystal32Bit.Primary = true;
            this.MrbInstalCrystal32Bit.Size = new System.Drawing.Size(81, 37);
            this.MrbInstalCrystal32Bit.TabIndex = 4;
            this.MrbInstalCrystal32Bit.Text = "Install";
            this.MrbInstalCrystal32Bit.UseVisualStyleBackColor = true;
            this.MrbInstalCrystal32Bit.Click += new System.EventHandler(this.MrbInstalCrystal32Bit_Click);
            // 
            // materialLabel42
            // 
            this.materialLabel42.AutoSize = true;
            this.materialLabel42.Depth = 0;
            this.materialLabel42.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel42.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel42.Location = new System.Drawing.Point(18, 47);
            this.materialLabel42.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel42.Name = "materialLabel42";
            this.materialLabel42.Size = new System.Drawing.Size(342, 19);
            this.materialLabel42.TabIndex = 3;
            this.materialLabel42.Text = "1. Install Crystal Report Runtime 32bit v13.0 SP22 ";
            // 
            // configurationTab
            // 
            this.configurationTab.AutoScroll = true;
            this.configurationTab.BackColor = System.Drawing.SystemColors.Control;
            this.configurationTab.Controls.Add(this.groupBox5);
            this.configurationTab.Controls.Add(this.mrbSaveEnviroment);
            this.configurationTab.Controls.Add(this.groupBox4);
            this.configurationTab.Controls.Add(this.groupBox3);
            this.configurationTab.Controls.Add(this.groupBox2);
            this.configurationTab.Controls.Add(this.mtfEnviromentName);
            this.configurationTab.Controls.Add(this.materialLabel6);
            this.configurationTab.Controls.Add(this.mrbAddEnviroment);
            this.configurationTab.Controls.Add(this.cbEnviroment);
            this.configurationTab.Controls.Add(this.materialLabel5);
            this.configurationTab.Controls.Add(this.groupBox1);
            this.configurationTab.Location = new System.Drawing.Point(4, 22);
            this.configurationTab.Name = "configurationTab";
            this.configurationTab.Size = new System.Drawing.Size(1364, 872);
            this.configurationTab.TabIndex = 3;
            this.configurationTab.Text = "Configuration";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.mcbApiAppPath);
            this.groupBox5.Controls.Add(this.mcbWapiAppPath);
            this.groupBox5.Controls.Add(this.mcbWebServiceAppPath);
            this.groupBox5.Controls.Add(this.mcbHostApplicationPath);
            this.groupBox5.Controls.Add(this.mcbSiteApplicationPath);
            this.groupBox5.Controls.Add(this.mfbApiAppPath);
            this.groupBox5.Controls.Add(this.mtfApiAppPath);
            this.groupBox5.Controls.Add(this.materialLabel36);
            this.groupBox5.Controls.Add(this.mtfApiAppName);
            this.groupBox5.Controls.Add(this.materialLabel37);
            this.groupBox5.Controls.Add(this.mfbWapiApPath);
            this.groupBox5.Controls.Add(this.mtfWapiAppPath);
            this.groupBox5.Controls.Add(this.materialLabel34);
            this.groupBox5.Controls.Add(this.mtfWapiAppName);
            this.groupBox5.Controls.Add(this.materialLabel35);
            this.groupBox5.Controls.Add(this.mfbWebServiceAppPath);
            this.groupBox5.Controls.Add(this.mtfWebServiceAppPath);
            this.groupBox5.Controls.Add(this.materialLabel32);
            this.groupBox5.Controls.Add(this.mtfWebServiceAppName);
            this.groupBox5.Controls.Add(this.materialLabel33);
            this.groupBox5.Controls.Add(this.mfbHostApplicationPath);
            this.groupBox5.Controls.Add(this.mtfHostApplicationPath);
            this.groupBox5.Controls.Add(this.materialLabel30);
            this.groupBox5.Controls.Add(this.mtfHostApplicationName);
            this.groupBox5.Controls.Add(this.materialLabel31);
            this.groupBox5.Controls.Add(this.mfbSiteApplicationPath);
            this.groupBox5.Controls.Add(this.mtfSiteApplicationPath);
            this.groupBox5.Controls.Add(this.materialLabel29);
            this.groupBox5.Controls.Add(this.mtfSiteApplicationName);
            this.groupBox5.Controls.Add(this.materialLabel28);
            this.groupBox5.Location = new System.Drawing.Point(625, 536);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(693, 444);
            this.groupBox5.TabIndex = 32;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "IIS Application Configurations";
            // 
            // mcbApiAppPath
            // 
            this.mcbApiAppPath.AutoSize = true;
            this.mcbApiAppPath.Checked = true;
            this.mcbApiAppPath.CheckState = System.Windows.Forms.CheckState.Checked;
            this.mcbApiAppPath.Depth = 0;
            this.mcbApiAppPath.Font = new System.Drawing.Font("Roboto", 10F);
            this.mcbApiAppPath.Location = new System.Drawing.Point(543, 395);
            this.mcbApiAppPath.Margin = new System.Windows.Forms.Padding(0);
            this.mcbApiAppPath.MouseLocation = new System.Drawing.Point(-1, -1);
            this.mcbApiAppPath.MouseState = MaterialSkin.MouseState.HOVER;
            this.mcbApiAppPath.Name = "mcbApiAppPath";
            this.mcbApiAppPath.Ripple = true;
            this.mcbApiAppPath.Size = new System.Drawing.Size(118, 30);
            this.mcbApiAppPath.TabIndex = 71;
            this.mcbApiAppPath.Text = "Relative Path?";
            this.mcbApiAppPath.UseVisualStyleBackColor = true;
            // 
            // mcbWapiAppPath
            // 
            this.mcbWapiAppPath.AutoSize = true;
            this.mcbWapiAppPath.Checked = true;
            this.mcbWapiAppPath.CheckState = System.Windows.Forms.CheckState.Checked;
            this.mcbWapiAppPath.Depth = 0;
            this.mcbWapiAppPath.Font = new System.Drawing.Font("Roboto", 10F);
            this.mcbWapiAppPath.Location = new System.Drawing.Point(543, 313);
            this.mcbWapiAppPath.Margin = new System.Windows.Forms.Padding(0);
            this.mcbWapiAppPath.MouseLocation = new System.Drawing.Point(-1, -1);
            this.mcbWapiAppPath.MouseState = MaterialSkin.MouseState.HOVER;
            this.mcbWapiAppPath.Name = "mcbWapiAppPath";
            this.mcbWapiAppPath.Ripple = true;
            this.mcbWapiAppPath.Size = new System.Drawing.Size(118, 30);
            this.mcbWapiAppPath.TabIndex = 70;
            this.mcbWapiAppPath.Text = "Relative Path?";
            this.mcbWapiAppPath.UseVisualStyleBackColor = true;
            // 
            // mcbWebServiceAppPath
            // 
            this.mcbWebServiceAppPath.AutoSize = true;
            this.mcbWebServiceAppPath.Checked = true;
            this.mcbWebServiceAppPath.CheckState = System.Windows.Forms.CheckState.Checked;
            this.mcbWebServiceAppPath.Depth = 0;
            this.mcbWebServiceAppPath.Font = new System.Drawing.Font("Roboto", 10F);
            this.mcbWebServiceAppPath.Location = new System.Drawing.Point(543, 224);
            this.mcbWebServiceAppPath.Margin = new System.Windows.Forms.Padding(0);
            this.mcbWebServiceAppPath.MouseLocation = new System.Drawing.Point(-1, -1);
            this.mcbWebServiceAppPath.MouseState = MaterialSkin.MouseState.HOVER;
            this.mcbWebServiceAppPath.Name = "mcbWebServiceAppPath";
            this.mcbWebServiceAppPath.Ripple = true;
            this.mcbWebServiceAppPath.Size = new System.Drawing.Size(118, 30);
            this.mcbWebServiceAppPath.TabIndex = 69;
            this.mcbWebServiceAppPath.Text = "Relative Path?";
            this.mcbWebServiceAppPath.UseVisualStyleBackColor = true;
            // 
            // mcbHostApplicationPath
            // 
            this.mcbHostApplicationPath.AutoSize = true;
            this.mcbHostApplicationPath.Checked = true;
            this.mcbHostApplicationPath.CheckState = System.Windows.Forms.CheckState.Checked;
            this.mcbHostApplicationPath.Depth = 0;
            this.mcbHostApplicationPath.Font = new System.Drawing.Font("Roboto", 10F);
            this.mcbHostApplicationPath.Location = new System.Drawing.Point(543, 140);
            this.mcbHostApplicationPath.Margin = new System.Windows.Forms.Padding(0);
            this.mcbHostApplicationPath.MouseLocation = new System.Drawing.Point(-1, -1);
            this.mcbHostApplicationPath.MouseState = MaterialSkin.MouseState.HOVER;
            this.mcbHostApplicationPath.Name = "mcbHostApplicationPath";
            this.mcbHostApplicationPath.Ripple = true;
            this.mcbHostApplicationPath.Size = new System.Drawing.Size(118, 30);
            this.mcbHostApplicationPath.TabIndex = 68;
            this.mcbHostApplicationPath.Text = "Relative Path?";
            this.mcbHostApplicationPath.UseVisualStyleBackColor = true;
            // 
            // mcbSiteApplicationPath
            // 
            this.mcbSiteApplicationPath.AutoSize = true;
            this.mcbSiteApplicationPath.Checked = true;
            this.mcbSiteApplicationPath.CheckState = System.Windows.Forms.CheckState.Checked;
            this.mcbSiteApplicationPath.Depth = 0;
            this.mcbSiteApplicationPath.Font = new System.Drawing.Font("Roboto", 10F);
            this.mcbSiteApplicationPath.Location = new System.Drawing.Point(543, 62);
            this.mcbSiteApplicationPath.Margin = new System.Windows.Forms.Padding(0);
            this.mcbSiteApplicationPath.MouseLocation = new System.Drawing.Point(-1, -1);
            this.mcbSiteApplicationPath.MouseState = MaterialSkin.MouseState.HOVER;
            this.mcbSiteApplicationPath.Name = "mcbSiteApplicationPath";
            this.mcbSiteApplicationPath.Ripple = true;
            this.mcbSiteApplicationPath.Size = new System.Drawing.Size(118, 30);
            this.mcbSiteApplicationPath.TabIndex = 44;
            this.mcbSiteApplicationPath.Text = "Relative Path?";
            this.mcbSiteApplicationPath.UseVisualStyleBackColor = true;
            // 
            // mfbApiAppPath
            // 
            this.mfbApiAppPath.AutoSize = true;
            this.mfbApiAppPath.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.mfbApiAppPath.Depth = 0;
            this.mfbApiAppPath.Location = new System.Drawing.Point(422, 395);
            this.mfbApiAppPath.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.mfbApiAppPath.MouseState = MaterialSkin.MouseState.HOVER;
            this.mfbApiAppPath.Name = "mfbApiAppPath";
            this.mfbApiAppPath.Primary = false;
            this.mfbApiAppPath.Size = new System.Drawing.Size(23, 36);
            this.mfbApiAppPath.TabIndex = 63;
            this.mfbApiAppPath.Text = "...";
            this.mfbApiAppPath.UseVisualStyleBackColor = true;
            // 
            // mtfApiAppPath
            // 
            this.mtfApiAppPath.BackColor = System.Drawing.Color.White;
            this.mtfApiAppPath.Depth = 0;
            this.mtfApiAppPath.Enabled = false;
            this.mtfApiAppPath.Hint = "";
            this.mtfApiAppPath.Location = new System.Drawing.Point(182, 403);
            this.mtfApiAppPath.MouseState = MaterialSkin.MouseState.HOVER;
            this.mtfApiAppPath.Name = "mtfApiAppPath";
            this.mtfApiAppPath.PasswordChar = '\0';
            this.mtfApiAppPath.SelectedText = "";
            this.mtfApiAppPath.SelectionLength = 0;
            this.mtfApiAppPath.SelectionStart = 0;
            this.mtfApiAppPath.Size = new System.Drawing.Size(233, 23);
            this.mtfApiAppPath.TabIndex = 67;
            this.mtfApiAppPath.Text = "API";
            this.mtfApiAppPath.UseSystemPasswordChar = false;
            // 
            // materialLabel36
            // 
            this.materialLabel36.AutoSize = true;
            this.materialLabel36.BackColor = System.Drawing.Color.White;
            this.materialLabel36.Depth = 0;
            this.materialLabel36.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.materialLabel36.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel36.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel36.Location = new System.Drawing.Point(6, 406);
            this.materialLabel36.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel36.Name = "materialLabel36";
            this.materialLabel36.Size = new System.Drawing.Size(100, 19);
            this.materialLabel36.TabIndex = 66;
            this.materialLabel36.Text = "API App Path:";
            // 
            // mtfApiAppName
            // 
            this.mtfApiAppName.BackColor = System.Drawing.Color.White;
            this.mtfApiAppName.Depth = 0;
            this.mtfApiAppName.Enabled = false;
            this.mtfApiAppName.Hint = "";
            this.mtfApiAppName.Location = new System.Drawing.Point(182, 364);
            this.mtfApiAppName.MouseState = MaterialSkin.MouseState.HOVER;
            this.mtfApiAppName.Name = "mtfApiAppName";
            this.mtfApiAppName.PasswordChar = '\0';
            this.mtfApiAppName.SelectedText = "";
            this.mtfApiAppName.SelectionLength = 0;
            this.mtfApiAppName.SelectionStart = 0;
            this.mtfApiAppName.Size = new System.Drawing.Size(233, 23);
            this.mtfApiAppName.TabIndex = 65;
            this.mtfApiAppName.Text = "API";
            this.mtfApiAppName.UseSystemPasswordChar = false;
            // 
            // materialLabel37
            // 
            this.materialLabel37.AutoSize = true;
            this.materialLabel37.BackColor = System.Drawing.Color.White;
            this.materialLabel37.Depth = 0;
            this.materialLabel37.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.materialLabel37.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel37.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel37.Location = new System.Drawing.Point(6, 367);
            this.materialLabel37.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel37.Name = "materialLabel37";
            this.materialLabel37.Size = new System.Drawing.Size(110, 19);
            this.materialLabel37.TabIndex = 64;
            this.materialLabel37.Text = "API App Name:";
            // 
            // mfbWapiApPath
            // 
            this.mfbWapiApPath.AutoSize = true;
            this.mfbWapiApPath.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.mfbWapiApPath.Depth = 0;
            this.mfbWapiApPath.Location = new System.Drawing.Point(422, 313);
            this.mfbWapiApPath.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.mfbWapiApPath.MouseState = MaterialSkin.MouseState.HOVER;
            this.mfbWapiApPath.Name = "mfbWapiApPath";
            this.mfbWapiApPath.Primary = false;
            this.mfbWapiApPath.Size = new System.Drawing.Size(23, 36);
            this.mfbWapiApPath.TabIndex = 58;
            this.mfbWapiApPath.Text = "...";
            this.mfbWapiApPath.UseVisualStyleBackColor = true;
            // 
            // mtfWapiAppPath
            // 
            this.mtfWapiAppPath.BackColor = System.Drawing.Color.White;
            this.mtfWapiAppPath.Depth = 0;
            this.mtfWapiAppPath.Enabled = false;
            this.mtfWapiAppPath.Hint = "";
            this.mtfWapiAppPath.Location = new System.Drawing.Point(182, 321);
            this.mtfWapiAppPath.MouseState = MaterialSkin.MouseState.HOVER;
            this.mtfWapiAppPath.Name = "mtfWapiAppPath";
            this.mtfWapiAppPath.PasswordChar = '\0';
            this.mtfWapiAppPath.SelectedText = "";
            this.mtfWapiAppPath.SelectionLength = 0;
            this.mtfWapiAppPath.SelectionStart = 0;
            this.mtfWapiAppPath.Size = new System.Drawing.Size(233, 23);
            this.mtfWapiAppPath.TabIndex = 62;
            this.mtfWapiAppPath.Text = "WapiServices";
            this.mtfWapiAppPath.UseSystemPasswordChar = false;
            // 
            // materialLabel34
            // 
            this.materialLabel34.AutoSize = true;
            this.materialLabel34.BackColor = System.Drawing.Color.White;
            this.materialLabel34.Depth = 0;
            this.materialLabel34.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.materialLabel34.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel34.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel34.Location = new System.Drawing.Point(6, 324);
            this.materialLabel34.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel34.Name = "materialLabel34";
            this.materialLabel34.Size = new System.Drawing.Size(110, 19);
            this.materialLabel34.TabIndex = 61;
            this.materialLabel34.Text = "Wapi App Path:";
            // 
            // mtfWapiAppName
            // 
            this.mtfWapiAppName.BackColor = System.Drawing.Color.White;
            this.mtfWapiAppName.Depth = 0;
            this.mtfWapiAppName.Enabled = false;
            this.mtfWapiAppName.Hint = "";
            this.mtfWapiAppName.Location = new System.Drawing.Point(182, 282);
            this.mtfWapiAppName.MouseState = MaterialSkin.MouseState.HOVER;
            this.mtfWapiAppName.Name = "mtfWapiAppName";
            this.mtfWapiAppName.PasswordChar = '\0';
            this.mtfWapiAppName.SelectedText = "";
            this.mtfWapiAppName.SelectionLength = 0;
            this.mtfWapiAppName.SelectionStart = 0;
            this.mtfWapiAppName.Size = new System.Drawing.Size(233, 23);
            this.mtfWapiAppName.TabIndex = 60;
            this.mtfWapiAppName.Text = "WapiServices";
            this.mtfWapiAppName.UseSystemPasswordChar = false;
            // 
            // materialLabel35
            // 
            this.materialLabel35.AutoSize = true;
            this.materialLabel35.BackColor = System.Drawing.Color.White;
            this.materialLabel35.Depth = 0;
            this.materialLabel35.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.materialLabel35.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel35.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel35.Location = new System.Drawing.Point(6, 285);
            this.materialLabel35.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel35.Name = "materialLabel35";
            this.materialLabel35.Size = new System.Drawing.Size(120, 19);
            this.materialLabel35.TabIndex = 59;
            this.materialLabel35.Text = "Wapi App Name:";
            // 
            // mfbWebServiceAppPath
            // 
            this.mfbWebServiceAppPath.AutoSize = true;
            this.mfbWebServiceAppPath.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.mfbWebServiceAppPath.Depth = 0;
            this.mfbWebServiceAppPath.Location = new System.Drawing.Point(422, 229);
            this.mfbWebServiceAppPath.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.mfbWebServiceAppPath.MouseState = MaterialSkin.MouseState.HOVER;
            this.mfbWebServiceAppPath.Name = "mfbWebServiceAppPath";
            this.mfbWebServiceAppPath.Primary = false;
            this.mfbWebServiceAppPath.Size = new System.Drawing.Size(23, 36);
            this.mfbWebServiceAppPath.TabIndex = 53;
            this.mfbWebServiceAppPath.Text = "...";
            this.mfbWebServiceAppPath.UseVisualStyleBackColor = true;
            // 
            // mtfWebServiceAppPath
            // 
            this.mtfWebServiceAppPath.BackColor = System.Drawing.Color.White;
            this.mtfWebServiceAppPath.Depth = 0;
            this.mtfWebServiceAppPath.Enabled = false;
            this.mtfWebServiceAppPath.Hint = "";
            this.mtfWebServiceAppPath.Location = new System.Drawing.Point(182, 237);
            this.mtfWebServiceAppPath.MouseState = MaterialSkin.MouseState.HOVER;
            this.mtfWebServiceAppPath.Name = "mtfWebServiceAppPath";
            this.mtfWebServiceAppPath.PasswordChar = '\0';
            this.mtfWebServiceAppPath.SelectedText = "";
            this.mtfWebServiceAppPath.SelectionLength = 0;
            this.mtfWebServiceAppPath.SelectionStart = 0;
            this.mtfWebServiceAppPath.Size = new System.Drawing.Size(233, 23);
            this.mtfWebServiceAppPath.TabIndex = 57;
            this.mtfWebServiceAppPath.Text = "Services";
            this.mtfWebServiceAppPath.UseSystemPasswordChar = false;
            // 
            // materialLabel32
            // 
            this.materialLabel32.AutoSize = true;
            this.materialLabel32.BackColor = System.Drawing.Color.White;
            this.materialLabel32.Depth = 0;
            this.materialLabel32.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.materialLabel32.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel32.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel32.Location = new System.Drawing.Point(6, 237);
            this.materialLabel32.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel32.Name = "materialLabel32";
            this.materialLabel32.Size = new System.Drawing.Size(159, 19);
            this.materialLabel32.TabIndex = 56;
            this.materialLabel32.Text = "Web Service App Path:";
            // 
            // mtfWebServiceAppName
            // 
            this.mtfWebServiceAppName.BackColor = System.Drawing.Color.White;
            this.mtfWebServiceAppName.Depth = 0;
            this.mtfWebServiceAppName.Enabled = false;
            this.mtfWebServiceAppName.Hint = "";
            this.mtfWebServiceAppName.Location = new System.Drawing.Point(182, 192);
            this.mtfWebServiceAppName.MouseState = MaterialSkin.MouseState.HOVER;
            this.mtfWebServiceAppName.Name = "mtfWebServiceAppName";
            this.mtfWebServiceAppName.PasswordChar = '\0';
            this.mtfWebServiceAppName.SelectedText = "";
            this.mtfWebServiceAppName.SelectionLength = 0;
            this.mtfWebServiceAppName.SelectionStart = 0;
            this.mtfWebServiceAppName.Size = new System.Drawing.Size(233, 23);
            this.mtfWebServiceAppName.TabIndex = 55;
            this.mtfWebServiceAppName.Text = "Services";
            this.mtfWebServiceAppName.UseSystemPasswordChar = false;
            // 
            // materialLabel33
            // 
            this.materialLabel33.AutoSize = true;
            this.materialLabel33.BackColor = System.Drawing.Color.White;
            this.materialLabel33.Depth = 0;
            this.materialLabel33.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.materialLabel33.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel33.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel33.Location = new System.Drawing.Point(6, 192);
            this.materialLabel33.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel33.Name = "materialLabel33";
            this.materialLabel33.Size = new System.Drawing.Size(169, 19);
            this.materialLabel33.TabIndex = 54;
            this.materialLabel33.Text = "Web Service App Name:";
            // 
            // mfbHostApplicationPath
            // 
            this.mfbHostApplicationPath.AutoSize = true;
            this.mfbHostApplicationPath.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.mfbHostApplicationPath.Depth = 0;
            this.mfbHostApplicationPath.Location = new System.Drawing.Point(422, 139);
            this.mfbHostApplicationPath.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.mfbHostApplicationPath.MouseState = MaterialSkin.MouseState.HOVER;
            this.mfbHostApplicationPath.Name = "mfbHostApplicationPath";
            this.mfbHostApplicationPath.Primary = false;
            this.mfbHostApplicationPath.Size = new System.Drawing.Size(23, 36);
            this.mfbHostApplicationPath.TabIndex = 48;
            this.mfbHostApplicationPath.Text = "...";
            this.mfbHostApplicationPath.UseVisualStyleBackColor = true;
            // 
            // mtfHostApplicationPath
            // 
            this.mtfHostApplicationPath.BackColor = System.Drawing.Color.White;
            this.mtfHostApplicationPath.Depth = 0;
            this.mtfHostApplicationPath.Enabled = false;
            this.mtfHostApplicationPath.Hint = "";
            this.mtfHostApplicationPath.Location = new System.Drawing.Point(182, 147);
            this.mtfHostApplicationPath.MouseState = MaterialSkin.MouseState.HOVER;
            this.mtfHostApplicationPath.Name = "mtfHostApplicationPath";
            this.mtfHostApplicationPath.PasswordChar = '\0';
            this.mtfHostApplicationPath.SelectedText = "";
            this.mtfHostApplicationPath.SelectionLength = 0;
            this.mtfHostApplicationPath.SelectionStart = 0;
            this.mtfHostApplicationPath.Size = new System.Drawing.Size(233, 23);
            this.mtfHostApplicationPath.TabIndex = 52;
            this.mtfHostApplicationPath.Text = "Host";
            this.mtfHostApplicationPath.UseSystemPasswordChar = false;
            // 
            // materialLabel30
            // 
            this.materialLabel30.AutoSize = true;
            this.materialLabel30.BackColor = System.Drawing.Color.White;
            this.materialLabel30.Depth = 0;
            this.materialLabel30.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.materialLabel30.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel30.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel30.Location = new System.Drawing.Point(6, 147);
            this.materialLabel30.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel30.Name = "materialLabel30";
            this.materialLabel30.Size = new System.Drawing.Size(160, 19);
            this.materialLabel30.TabIndex = 51;
            this.materialLabel30.Text = "Host Application Path:";
            // 
            // mtfHostApplicationName
            // 
            this.mtfHostApplicationName.BackColor = System.Drawing.Color.White;
            this.mtfHostApplicationName.Depth = 0;
            this.mtfHostApplicationName.Enabled = false;
            this.mtfHostApplicationName.Hint = "";
            this.mtfHostApplicationName.Location = new System.Drawing.Point(182, 103);
            this.mtfHostApplicationName.MouseState = MaterialSkin.MouseState.HOVER;
            this.mtfHostApplicationName.Name = "mtfHostApplicationName";
            this.mtfHostApplicationName.PasswordChar = '\0';
            this.mtfHostApplicationName.SelectedText = "";
            this.mtfHostApplicationName.SelectionLength = 0;
            this.mtfHostApplicationName.SelectionStart = 0;
            this.mtfHostApplicationName.Size = new System.Drawing.Size(233, 23);
            this.mtfHostApplicationName.TabIndex = 50;
            this.mtfHostApplicationName.Text = "Host";
            this.mtfHostApplicationName.UseSystemPasswordChar = false;
            // 
            // materialLabel31
            // 
            this.materialLabel31.AutoSize = true;
            this.materialLabel31.BackColor = System.Drawing.Color.White;
            this.materialLabel31.Depth = 0;
            this.materialLabel31.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.materialLabel31.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel31.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel31.Location = new System.Drawing.Point(6, 103);
            this.materialLabel31.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel31.Name = "materialLabel31";
            this.materialLabel31.Size = new System.Drawing.Size(170, 19);
            this.materialLabel31.TabIndex = 49;
            this.materialLabel31.Text = "Host Application Name:";
            // 
            // mfbSiteApplicationPath
            // 
            this.mfbSiteApplicationPath.AutoSize = true;
            this.mfbSiteApplicationPath.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.mfbSiteApplicationPath.Depth = 0;
            this.mfbSiteApplicationPath.Location = new System.Drawing.Point(422, 56);
            this.mfbSiteApplicationPath.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.mfbSiteApplicationPath.MouseState = MaterialSkin.MouseState.HOVER;
            this.mfbSiteApplicationPath.Name = "mfbSiteApplicationPath";
            this.mfbSiteApplicationPath.Primary = false;
            this.mfbSiteApplicationPath.Size = new System.Drawing.Size(23, 36);
            this.mfbSiteApplicationPath.TabIndex = 44;
            this.mfbSiteApplicationPath.Text = "...";
            this.mfbSiteApplicationPath.UseVisualStyleBackColor = true;
            // 
            // mtfSiteApplicationPath
            // 
            this.mtfSiteApplicationPath.BackColor = System.Drawing.Color.White;
            this.mtfSiteApplicationPath.Depth = 0;
            this.mtfSiteApplicationPath.Enabled = false;
            this.mtfSiteApplicationPath.Hint = "";
            this.mtfSiteApplicationPath.Location = new System.Drawing.Point(182, 64);
            this.mtfSiteApplicationPath.MouseState = MaterialSkin.MouseState.HOVER;
            this.mtfSiteApplicationPath.Name = "mtfSiteApplicationPath";
            this.mtfSiteApplicationPath.PasswordChar = '\0';
            this.mtfSiteApplicationPath.SelectedText = "";
            this.mtfSiteApplicationPath.SelectionLength = 0;
            this.mtfSiteApplicationPath.SelectionStart = 0;
            this.mtfSiteApplicationPath.Size = new System.Drawing.Size(233, 23);
            this.mtfSiteApplicationPath.TabIndex = 47;
            this.mtfSiteApplicationPath.Text = "Site";
            this.mtfSiteApplicationPath.UseSystemPasswordChar = false;
            // 
            // materialLabel29
            // 
            this.materialLabel29.AutoSize = true;
            this.materialLabel29.BackColor = System.Drawing.Color.White;
            this.materialLabel29.Depth = 0;
            this.materialLabel29.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.materialLabel29.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel29.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel29.Location = new System.Drawing.Point(6, 64);
            this.materialLabel29.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel29.Name = "materialLabel29";
            this.materialLabel29.Size = new System.Drawing.Size(153, 19);
            this.materialLabel29.TabIndex = 46;
            this.materialLabel29.Text = "Site Application Path:";
            // 
            // mtfSiteApplicationName
            // 
            this.mtfSiteApplicationName.BackColor = System.Drawing.Color.White;
            this.mtfSiteApplicationName.Depth = 0;
            this.mtfSiteApplicationName.Enabled = false;
            this.mtfSiteApplicationName.Hint = "";
            this.mtfSiteApplicationName.Location = new System.Drawing.Point(182, 24);
            this.mtfSiteApplicationName.MouseState = MaterialSkin.MouseState.HOVER;
            this.mtfSiteApplicationName.Name = "mtfSiteApplicationName";
            this.mtfSiteApplicationName.PasswordChar = '\0';
            this.mtfSiteApplicationName.SelectedText = "";
            this.mtfSiteApplicationName.SelectionLength = 0;
            this.mtfSiteApplicationName.SelectionStart = 0;
            this.mtfSiteApplicationName.Size = new System.Drawing.Size(233, 23);
            this.mtfSiteApplicationName.TabIndex = 45;
            this.mtfSiteApplicationName.Text = "Site";
            this.mtfSiteApplicationName.UseSystemPasswordChar = false;
            // 
            // materialLabel28
            // 
            this.materialLabel28.AutoSize = true;
            this.materialLabel28.BackColor = System.Drawing.Color.White;
            this.materialLabel28.Depth = 0;
            this.materialLabel28.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.materialLabel28.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel28.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel28.Location = new System.Drawing.Point(6, 24);
            this.materialLabel28.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel28.Name = "materialLabel28";
            this.materialLabel28.Size = new System.Drawing.Size(163, 19);
            this.materialLabel28.TabIndex = 44;
            this.materialLabel28.Text = "Site Application Name:";
            // 
            // mrbSaveEnviroment
            // 
            this.mrbSaveEnviroment.Depth = 0;
            this.mrbSaveEnviroment.Enabled = false;
            this.mrbSaveEnviroment.Location = new System.Drawing.Point(1149, 7);
            this.mrbSaveEnviroment.MouseState = MaterialSkin.MouseState.HOVER;
            this.mrbSaveEnviroment.Name = "mrbSaveEnviroment";
            this.mrbSaveEnviroment.Primary = true;
            this.mrbSaveEnviroment.Size = new System.Drawing.Size(169, 40);
            this.mrbSaveEnviroment.TabIndex = 31;
            this.mrbSaveEnviroment.Text = "Save";
            this.mrbSaveEnviroment.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.mtfReportServerFolder);
            this.groupBox4.Controls.Add(this.materialLabel24);
            this.groupBox4.Controls.Add(this.mtfReportServicesUrl);
            this.groupBox4.Controls.Add(this.materialLabel25);
            this.groupBox4.Controls.Add(this.mtfReportExecutionUrl);
            this.groupBox4.Controls.Add(this.materialLabel26);
            this.groupBox4.Controls.Add(this.mtfReportServerUrl);
            this.groupBox4.Controls.Add(this.materialLabel27);
            this.groupBox4.Location = new System.Drawing.Point(22, 612);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(558, 206);
            this.groupBox4.TabIndex = 30;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "SQL Reporting Services Configuration";
            // 
            // mtfReportServerFolder
            // 
            this.mtfReportServerFolder.BackColor = System.Drawing.Color.White;
            this.mtfReportServerFolder.Depth = 0;
            this.mtfReportServerFolder.Enabled = false;
            this.mtfReportServerFolder.Hint = "";
            this.mtfReportServerFolder.Location = new System.Drawing.Point(231, 148);
            this.mtfReportServerFolder.MouseState = MaterialSkin.MouseState.HOVER;
            this.mtfReportServerFolder.Name = "mtfReportServerFolder";
            this.mtfReportServerFolder.PasswordChar = '\0';
            this.mtfReportServerFolder.SelectedText = "";
            this.mtfReportServerFolder.SelectionLength = 0;
            this.mtfReportServerFolder.SelectionStart = 0;
            this.mtfReportServerFolder.Size = new System.Drawing.Size(233, 23);
            this.mtfReportServerFolder.TabIndex = 29;
            this.mtfReportServerFolder.UseSystemPasswordChar = false;
            // 
            // materialLabel24
            // 
            this.materialLabel24.AutoSize = true;
            this.materialLabel24.BackColor = System.Drawing.Color.White;
            this.materialLabel24.Depth = 0;
            this.materialLabel24.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.materialLabel24.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel24.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel24.Location = new System.Drawing.Point(6, 159);
            this.materialLabel24.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel24.Name = "materialLabel24";
            this.materialLabel24.Size = new System.Drawing.Size(149, 19);
            this.materialLabel24.TabIndex = 28;
            this.materialLabel24.Text = "Report Server Folder:";
            // 
            // mtfReportServicesUrl
            // 
            this.mtfReportServicesUrl.BackColor = System.Drawing.Color.White;
            this.mtfReportServicesUrl.Depth = 0;
            this.mtfReportServicesUrl.Enabled = false;
            this.mtfReportServicesUrl.Hint = "";
            this.mtfReportServicesUrl.Location = new System.Drawing.Point(231, 106);
            this.mtfReportServicesUrl.MouseState = MaterialSkin.MouseState.HOVER;
            this.mtfReportServicesUrl.Name = "mtfReportServicesUrl";
            this.mtfReportServicesUrl.PasswordChar = '\0';
            this.mtfReportServicesUrl.SelectedText = "";
            this.mtfReportServicesUrl.SelectionLength = 0;
            this.mtfReportServicesUrl.SelectionStart = 0;
            this.mtfReportServicesUrl.Size = new System.Drawing.Size(233, 23);
            this.mtfReportServicesUrl.TabIndex = 27;
            this.mtfReportServicesUrl.UseSystemPasswordChar = false;
            // 
            // materialLabel25
            // 
            this.materialLabel25.AutoSize = true;
            this.materialLabel25.BackColor = System.Drawing.Color.White;
            this.materialLabel25.Depth = 0;
            this.materialLabel25.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.materialLabel25.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel25.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel25.Location = new System.Drawing.Point(6, 114);
            this.materialLabel25.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel25.Name = "materialLabel25";
            this.materialLabel25.Size = new System.Drawing.Size(149, 19);
            this.materialLabel25.TabIndex = 26;
            this.materialLabel25.Text = "Report Services URL:";
            // 
            // mtfReportExecutionUrl
            // 
            this.mtfReportExecutionUrl.BackColor = System.Drawing.Color.White;
            this.mtfReportExecutionUrl.Depth = 0;
            this.mtfReportExecutionUrl.Enabled = false;
            this.mtfReportExecutionUrl.Hint = "";
            this.mtfReportExecutionUrl.Location = new System.Drawing.Point(231, 62);
            this.mtfReportExecutionUrl.MouseState = MaterialSkin.MouseState.HOVER;
            this.mtfReportExecutionUrl.Name = "mtfReportExecutionUrl";
            this.mtfReportExecutionUrl.PasswordChar = '\0';
            this.mtfReportExecutionUrl.SelectedText = "";
            this.mtfReportExecutionUrl.SelectionLength = 0;
            this.mtfReportExecutionUrl.SelectionStart = 0;
            this.mtfReportExecutionUrl.Size = new System.Drawing.Size(233, 23);
            this.mtfReportExecutionUrl.TabIndex = 25;
            this.mtfReportExecutionUrl.UseSystemPasswordChar = false;
            // 
            // materialLabel26
            // 
            this.materialLabel26.AutoSize = true;
            this.materialLabel26.BackColor = System.Drawing.Color.White;
            this.materialLabel26.Depth = 0;
            this.materialLabel26.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.materialLabel26.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel26.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel26.Location = new System.Drawing.Point(6, 70);
            this.materialLabel26.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel26.Name = "materialLabel26";
            this.materialLabel26.Size = new System.Drawing.Size(158, 19);
            this.materialLabel26.TabIndex = 24;
            this.materialLabel26.Text = "Report Execution URL:";
            // 
            // mtfReportServerUrl
            // 
            this.mtfReportServerUrl.BackColor = System.Drawing.Color.White;
            this.mtfReportServerUrl.Depth = 0;
            this.mtfReportServerUrl.Enabled = false;
            this.mtfReportServerUrl.Hint = "";
            this.mtfReportServerUrl.Location = new System.Drawing.Point(231, 23);
            this.mtfReportServerUrl.MouseState = MaterialSkin.MouseState.HOVER;
            this.mtfReportServerUrl.Name = "mtfReportServerUrl";
            this.mtfReportServerUrl.PasswordChar = '\0';
            this.mtfReportServerUrl.SelectedText = "";
            this.mtfReportServerUrl.SelectionLength = 0;
            this.mtfReportServerUrl.SelectionStart = 0;
            this.mtfReportServerUrl.Size = new System.Drawing.Size(233, 23);
            this.mtfReportServerUrl.TabIndex = 23;
            this.mtfReportServerUrl.UseSystemPasswordChar = false;
            // 
            // materialLabel27
            // 
            this.materialLabel27.AutoSize = true;
            this.materialLabel27.BackColor = System.Drawing.Color.White;
            this.materialLabel27.Depth = 0;
            this.materialLabel27.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.materialLabel27.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel27.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel27.Location = new System.Drawing.Point(6, 27);
            this.materialLabel27.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel27.Name = "materialLabel27";
            this.materialLabel27.Size = new System.Drawing.Size(134, 19);
            this.materialLabel27.TabIndex = 22;
            this.materialLabel27.Text = "Report Server URL:";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.mfbReleasePackagePath);
            this.groupBox3.Controls.Add(this.mfbDatabaseLogInstallation);
            this.groupBox3.Controls.Add(this.mfbDatabaseInstallationLocation);
            this.groupBox3.Controls.Add(this.mfbSiteBackupLocation);
            this.groupBox3.Controls.Add(this.mfbDatabaseBackupLocation);
            this.groupBox3.Controls.Add(this.mtfReleasePackagePath);
            this.groupBox3.Controls.Add(this.materialLabel23);
            this.groupBox3.Controls.Add(this.mtfDatabaseLogPath);
            this.groupBox3.Controls.Add(this.materialLabel20);
            this.groupBox3.Controls.Add(this.mtfDatabaseInstallationPath);
            this.groupBox3.Controls.Add(this.materialLabel19);
            this.groupBox3.Controls.Add(this.mtfSiteBackupLocation);
            this.groupBox3.Controls.Add(this.materialLabel21);
            this.groupBox3.Controls.Add(this.mtfDatabaseBackupLocation);
            this.groupBox3.Controls.Add(this.materialLabel22);
            this.groupBox3.Location = new System.Drawing.Point(22, 335);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(558, 258);
            this.groupBox3.TabIndex = 30;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Installation and Backup Directories";
            // 
            // mfbReleasePackagePath
            // 
            this.mfbReleasePackagePath.AutoSize = true;
            this.mfbReleasePackagePath.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.mfbReleasePackagePath.Depth = 0;
            this.mfbReleasePackagePath.Location = new System.Drawing.Point(471, 208);
            this.mfbReleasePackagePath.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.mfbReleasePackagePath.MouseState = MaterialSkin.MouseState.HOVER;
            this.mfbReleasePackagePath.Name = "mfbReleasePackagePath";
            this.mfbReleasePackagePath.Primary = false;
            this.mfbReleasePackagePath.Size = new System.Drawing.Size(23, 36);
            this.mfbReleasePackagePath.TabIndex = 36;
            this.mfbReleasePackagePath.Text = "...";
            this.mfbReleasePackagePath.UseVisualStyleBackColor = true;
            // 
            // mfbDatabaseLogInstallation
            // 
            this.mfbDatabaseLogInstallation.AutoSize = true;
            this.mfbDatabaseLogInstallation.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.mfbDatabaseLogInstallation.Depth = 0;
            this.mfbDatabaseLogInstallation.Location = new System.Drawing.Point(471, 158);
            this.mfbDatabaseLogInstallation.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.mfbDatabaseLogInstallation.MouseState = MaterialSkin.MouseState.HOVER;
            this.mfbDatabaseLogInstallation.Name = "mfbDatabaseLogInstallation";
            this.mfbDatabaseLogInstallation.Primary = false;
            this.mfbDatabaseLogInstallation.Size = new System.Drawing.Size(23, 36);
            this.mfbDatabaseLogInstallation.TabIndex = 35;
            this.mfbDatabaseLogInstallation.Text = "...";
            this.mfbDatabaseLogInstallation.UseVisualStyleBackColor = true;
            // 
            // mfbDatabaseInstallationLocation
            // 
            this.mfbDatabaseInstallationLocation.AutoSize = true;
            this.mfbDatabaseInstallationLocation.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.mfbDatabaseInstallationLocation.Depth = 0;
            this.mfbDatabaseInstallationLocation.Location = new System.Drawing.Point(471, 115);
            this.mfbDatabaseInstallationLocation.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.mfbDatabaseInstallationLocation.MouseState = MaterialSkin.MouseState.HOVER;
            this.mfbDatabaseInstallationLocation.Name = "mfbDatabaseInstallationLocation";
            this.mfbDatabaseInstallationLocation.Primary = false;
            this.mfbDatabaseInstallationLocation.Size = new System.Drawing.Size(23, 36);
            this.mfbDatabaseInstallationLocation.TabIndex = 34;
            this.mfbDatabaseInstallationLocation.Text = "...";
            this.mfbDatabaseInstallationLocation.UseVisualStyleBackColor = true;
            // 
            // mfbSiteBackupLocation
            // 
            this.mfbSiteBackupLocation.AutoSize = true;
            this.mfbSiteBackupLocation.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.mfbSiteBackupLocation.Depth = 0;
            this.mfbSiteBackupLocation.Location = new System.Drawing.Point(471, 60);
            this.mfbSiteBackupLocation.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.mfbSiteBackupLocation.MouseState = MaterialSkin.MouseState.HOVER;
            this.mfbSiteBackupLocation.Name = "mfbSiteBackupLocation";
            this.mfbSiteBackupLocation.Primary = false;
            this.mfbSiteBackupLocation.Size = new System.Drawing.Size(23, 36);
            this.mfbSiteBackupLocation.TabIndex = 33;
            this.mfbSiteBackupLocation.Text = "...";
            this.mfbSiteBackupLocation.UseVisualStyleBackColor = true;
            // 
            // mfbDatabaseBackupLocation
            // 
            this.mfbDatabaseBackupLocation.AutoSize = true;
            this.mfbDatabaseBackupLocation.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.mfbDatabaseBackupLocation.Depth = 0;
            this.mfbDatabaseBackupLocation.Location = new System.Drawing.Point(471, 18);
            this.mfbDatabaseBackupLocation.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.mfbDatabaseBackupLocation.MouseState = MaterialSkin.MouseState.HOVER;
            this.mfbDatabaseBackupLocation.Name = "mfbDatabaseBackupLocation";
            this.mfbDatabaseBackupLocation.Primary = false;
            this.mfbDatabaseBackupLocation.Size = new System.Drawing.Size(23, 36);
            this.mfbDatabaseBackupLocation.TabIndex = 32;
            this.mfbDatabaseBackupLocation.Text = "...";
            this.mfbDatabaseBackupLocation.UseVisualStyleBackColor = true;
            // 
            // mtfReleasePackagePath
            // 
            this.mtfReleasePackagePath.BackColor = System.Drawing.Color.White;
            this.mtfReleasePackagePath.Depth = 0;
            this.mtfReleasePackagePath.Enabled = false;
            this.mtfReleasePackagePath.Hint = "";
            this.mtfReleasePackagePath.Location = new System.Drawing.Point(231, 208);
            this.mtfReleasePackagePath.MouseState = MaterialSkin.MouseState.HOVER;
            this.mtfReleasePackagePath.Name = "mtfReleasePackagePath";
            this.mtfReleasePackagePath.PasswordChar = '\0';
            this.mtfReleasePackagePath.SelectedText = "";
            this.mtfReleasePackagePath.SelectionLength = 0;
            this.mtfReleasePackagePath.SelectionStart = 0;
            this.mtfReleasePackagePath.Size = new System.Drawing.Size(233, 23);
            this.mtfReleasePackagePath.TabIndex = 31;
            this.mtfReleasePackagePath.UseSystemPasswordChar = false;
            // 
            // materialLabel23
            // 
            this.materialLabel23.AutoSize = true;
            this.materialLabel23.BackColor = System.Drawing.Color.White;
            this.materialLabel23.Depth = 0;
            this.materialLabel23.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.materialLabel23.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel23.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel23.Location = new System.Drawing.Point(6, 215);
            this.materialLabel23.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel23.Name = "materialLabel23";
            this.materialLabel23.Size = new System.Drawing.Size(186, 19);
            this.materialLabel23.TabIndex = 30;
            this.materialLabel23.Text = "Release Package location:";
            // 
            // mtfDatabaseLogPath
            // 
            this.mtfDatabaseLogPath.BackColor = System.Drawing.Color.White;
            this.mtfDatabaseLogPath.Depth = 0;
            this.mtfDatabaseLogPath.Enabled = false;
            this.mtfDatabaseLogPath.Hint = "";
            this.mtfDatabaseLogPath.Location = new System.Drawing.Point(231, 158);
            this.mtfDatabaseLogPath.MouseState = MaterialSkin.MouseState.HOVER;
            this.mtfDatabaseLogPath.Name = "mtfDatabaseLogPath";
            this.mtfDatabaseLogPath.PasswordChar = '\0';
            this.mtfDatabaseLogPath.SelectedText = "";
            this.mtfDatabaseLogPath.SelectionLength = 0;
            this.mtfDatabaseLogPath.SelectionStart = 0;
            this.mtfDatabaseLogPath.Size = new System.Drawing.Size(233, 23);
            this.mtfDatabaseLogPath.TabIndex = 29;
            this.mtfDatabaseLogPath.UseSystemPasswordChar = false;
            // 
            // materialLabel20
            // 
            this.materialLabel20.AutoSize = true;
            this.materialLabel20.BackColor = System.Drawing.Color.White;
            this.materialLabel20.Depth = 0;
            this.materialLabel20.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.materialLabel20.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel20.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel20.Location = new System.Drawing.Point(6, 166);
            this.materialLabel20.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel20.Name = "materialLabel20";
            this.materialLabel20.Size = new System.Drawing.Size(213, 19);
            this.materialLabel20.TabIndex = 28;
            this.materialLabel20.Text = "Database log installation path:";
            // 
            // mtfDatabaseInstallationPath
            // 
            this.mtfDatabaseInstallationPath.BackColor = System.Drawing.Color.White;
            this.mtfDatabaseInstallationPath.Depth = 0;
            this.mtfDatabaseInstallationPath.Enabled = false;
            this.mtfDatabaseInstallationPath.Hint = "";
            this.mtfDatabaseInstallationPath.Location = new System.Drawing.Point(231, 114);
            this.mtfDatabaseInstallationPath.MouseState = MaterialSkin.MouseState.HOVER;
            this.mtfDatabaseInstallationPath.Name = "mtfDatabaseInstallationPath";
            this.mtfDatabaseInstallationPath.PasswordChar = '\0';
            this.mtfDatabaseInstallationPath.SelectedText = "";
            this.mtfDatabaseInstallationPath.SelectionLength = 0;
            this.mtfDatabaseInstallationPath.SelectionStart = 0;
            this.mtfDatabaseInstallationPath.Size = new System.Drawing.Size(233, 23);
            this.mtfDatabaseInstallationPath.TabIndex = 27;
            this.mtfDatabaseInstallationPath.UseSystemPasswordChar = false;
            // 
            // materialLabel19
            // 
            this.materialLabel19.AutoSize = true;
            this.materialLabel19.BackColor = System.Drawing.Color.White;
            this.materialLabel19.Depth = 0;
            this.materialLabel19.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.materialLabel19.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel19.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel19.Location = new System.Drawing.Point(6, 122);
            this.materialLabel19.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel19.Name = "materialLabel19";
            this.materialLabel19.Size = new System.Drawing.Size(188, 19);
            this.materialLabel19.TabIndex = 26;
            this.materialLabel19.Text = "Database installation path:";
            // 
            // mtfSiteBackupLocation
            // 
            this.mtfSiteBackupLocation.BackColor = System.Drawing.Color.White;
            this.mtfSiteBackupLocation.Depth = 0;
            this.mtfSiteBackupLocation.Enabled = false;
            this.mtfSiteBackupLocation.Hint = "";
            this.mtfSiteBackupLocation.Location = new System.Drawing.Point(231, 62);
            this.mtfSiteBackupLocation.MouseState = MaterialSkin.MouseState.HOVER;
            this.mtfSiteBackupLocation.Name = "mtfSiteBackupLocation";
            this.mtfSiteBackupLocation.PasswordChar = '\0';
            this.mtfSiteBackupLocation.SelectedText = "";
            this.mtfSiteBackupLocation.SelectionLength = 0;
            this.mtfSiteBackupLocation.SelectionStart = 0;
            this.mtfSiteBackupLocation.Size = new System.Drawing.Size(233, 23);
            this.mtfSiteBackupLocation.TabIndex = 25;
            this.mtfSiteBackupLocation.UseSystemPasswordChar = false;
            // 
            // materialLabel21
            // 
            this.materialLabel21.AutoSize = true;
            this.materialLabel21.BackColor = System.Drawing.Color.White;
            this.materialLabel21.Depth = 0;
            this.materialLabel21.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.materialLabel21.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel21.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel21.Location = new System.Drawing.Point(6, 70);
            this.materialLabel21.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel21.Name = "materialLabel21";
            this.materialLabel21.Size = new System.Drawing.Size(150, 19);
            this.materialLabel21.TabIndex = 24;
            this.materialLabel21.Text = "Site backup location:";
            // 
            // mtfDatabaseBackupLocation
            // 
            this.mtfDatabaseBackupLocation.BackColor = System.Drawing.Color.White;
            this.mtfDatabaseBackupLocation.Depth = 0;
            this.mtfDatabaseBackupLocation.Enabled = false;
            this.mtfDatabaseBackupLocation.Hint = "";
            this.mtfDatabaseBackupLocation.Location = new System.Drawing.Point(231, 23);
            this.mtfDatabaseBackupLocation.MouseState = MaterialSkin.MouseState.HOVER;
            this.mtfDatabaseBackupLocation.Name = "mtfDatabaseBackupLocation";
            this.mtfDatabaseBackupLocation.PasswordChar = '\0';
            this.mtfDatabaseBackupLocation.SelectedText = "";
            this.mtfDatabaseBackupLocation.SelectionLength = 0;
            this.mtfDatabaseBackupLocation.SelectionStart = 0;
            this.mtfDatabaseBackupLocation.Size = new System.Drawing.Size(233, 23);
            this.mtfDatabaseBackupLocation.TabIndex = 23;
            this.mtfDatabaseBackupLocation.UseSystemPasswordChar = false;
            // 
            // materialLabel22
            // 
            this.materialLabel22.AutoSize = true;
            this.materialLabel22.BackColor = System.Drawing.Color.White;
            this.materialLabel22.Depth = 0;
            this.materialLabel22.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.materialLabel22.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel22.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel22.Location = new System.Drawing.Point(6, 27);
            this.materialLabel22.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel22.Name = "materialLabel22";
            this.materialLabel22.Size = new System.Drawing.Size(192, 19);
            this.materialLabel22.TabIndex = 22;
            this.materialLabel22.Text = "Database Backup Location:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.mcbVersionVirtualDirectoryPath);
            this.groupBox2.Controls.Add(this.mcbEnvironmentVirtualDirectoryPath);
            this.groupBox2.Controls.Add(this.mcbProductVirtualDirectoryPath);
            this.groupBox2.Controls.Add(this.mfbVersionVirtualDirectoryPath);
            this.groupBox2.Controls.Add(this.mfbEnvironmentVirtualDirectoryPath);
            this.groupBox2.Controls.Add(this.mfbProductVirtualDirectoryPath);
            this.groupBox2.Controls.Add(this.mfbDefaultWebSitePath);
            this.groupBox2.Controls.Add(this.mtfVeresionVirtualDirectoryPath);
            this.groupBox2.Controls.Add(this.materialLabel17);
            this.groupBox2.Controls.Add(this.mtfVersionVirtualDirectoryName);
            this.groupBox2.Controls.Add(this.materialLabel18);
            this.groupBox2.Controls.Add(this.mtfEnvironmentVirtualDirectoryPath);
            this.groupBox2.Controls.Add(this.materialLabel15);
            this.groupBox2.Controls.Add(this.mtfEnviromentVirtualDirectoryName);
            this.groupBox2.Controls.Add(this.materialLabel16);
            this.groupBox2.Controls.Add(this.mtfProductVirtualDirectoryPath);
            this.groupBox2.Controls.Add(this.materialLabel11);
            this.groupBox2.Controls.Add(this.mtfProductVirtualDirectoryName);
            this.groupBox2.Controls.Add(this.materialLabel12);
            this.groupBox2.Controls.Add(this.mtfDefaultWebsitePath);
            this.groupBox2.Controls.Add(this.materialLabel13);
            this.groupBox2.Controls.Add(this.mtfDefaultWebSiteName);
            this.groupBox2.Controls.Add(this.materialLabel14);
            this.groupBox2.Location = new System.Drawing.Point(625, 105);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(693, 411);
            this.groupBox2.TabIndex = 30;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "IIS Configuration";
            // 
            // mcbVersionVirtualDirectoryPath
            // 
            this.mcbVersionVirtualDirectoryPath.AutoSize = true;
            this.mcbVersionVirtualDirectoryPath.Checked = true;
            this.mcbVersionVirtualDirectoryPath.CheckState = System.Windows.Forms.CheckState.Checked;
            this.mcbVersionVirtualDirectoryPath.Depth = 0;
            this.mcbVersionVirtualDirectoryPath.Font = new System.Drawing.Font("Roboto", 10F);
            this.mcbVersionVirtualDirectoryPath.Location = new System.Drawing.Point(543, 349);
            this.mcbVersionVirtualDirectoryPath.Margin = new System.Windows.Forms.Padding(0);
            this.mcbVersionVirtualDirectoryPath.MouseLocation = new System.Drawing.Point(-1, -1);
            this.mcbVersionVirtualDirectoryPath.MouseState = MaterialSkin.MouseState.HOVER;
            this.mcbVersionVirtualDirectoryPath.Name = "mcbVersionVirtualDirectoryPath";
            this.mcbVersionVirtualDirectoryPath.Ripple = true;
            this.mcbVersionVirtualDirectoryPath.Size = new System.Drawing.Size(118, 30);
            this.mcbVersionVirtualDirectoryPath.TabIndex = 43;
            this.mcbVersionVirtualDirectoryPath.Text = "Relative Path?";
            this.mcbVersionVirtualDirectoryPath.UseVisualStyleBackColor = true;
            // 
            // mcbEnvironmentVirtualDirectoryPath
            // 
            this.mcbEnvironmentVirtualDirectoryPath.AutoSize = true;
            this.mcbEnvironmentVirtualDirectoryPath.Checked = true;
            this.mcbEnvironmentVirtualDirectoryPath.CheckState = System.Windows.Forms.CheckState.Checked;
            this.mcbEnvironmentVirtualDirectoryPath.Depth = 0;
            this.mcbEnvironmentVirtualDirectoryPath.Font = new System.Drawing.Font("Roboto", 10F);
            this.mcbEnvironmentVirtualDirectoryPath.Location = new System.Drawing.Point(543, 246);
            this.mcbEnvironmentVirtualDirectoryPath.Margin = new System.Windows.Forms.Padding(0);
            this.mcbEnvironmentVirtualDirectoryPath.MouseLocation = new System.Drawing.Point(-1, -1);
            this.mcbEnvironmentVirtualDirectoryPath.MouseState = MaterialSkin.MouseState.HOVER;
            this.mcbEnvironmentVirtualDirectoryPath.Name = "mcbEnvironmentVirtualDirectoryPath";
            this.mcbEnvironmentVirtualDirectoryPath.Ripple = true;
            this.mcbEnvironmentVirtualDirectoryPath.Size = new System.Drawing.Size(118, 30);
            this.mcbEnvironmentVirtualDirectoryPath.TabIndex = 42;
            this.mcbEnvironmentVirtualDirectoryPath.Text = "Relative Path?";
            this.mcbEnvironmentVirtualDirectoryPath.UseVisualStyleBackColor = true;
            // 
            // mcbProductVirtualDirectoryPath
            // 
            this.mcbProductVirtualDirectoryPath.AutoSize = true;
            this.mcbProductVirtualDirectoryPath.Checked = true;
            this.mcbProductVirtualDirectoryPath.CheckState = System.Windows.Forms.CheckState.Checked;
            this.mcbProductVirtualDirectoryPath.Depth = 0;
            this.mcbProductVirtualDirectoryPath.Font = new System.Drawing.Font("Roboto", 10F);
            this.mcbProductVirtualDirectoryPath.Location = new System.Drawing.Point(543, 153);
            this.mcbProductVirtualDirectoryPath.Margin = new System.Windows.Forms.Padding(0);
            this.mcbProductVirtualDirectoryPath.MouseLocation = new System.Drawing.Point(-1, -1);
            this.mcbProductVirtualDirectoryPath.MouseState = MaterialSkin.MouseState.HOVER;
            this.mcbProductVirtualDirectoryPath.Name = "mcbProductVirtualDirectoryPath";
            this.mcbProductVirtualDirectoryPath.Ripple = true;
            this.mcbProductVirtualDirectoryPath.Size = new System.Drawing.Size(118, 30);
            this.mcbProductVirtualDirectoryPath.TabIndex = 41;
            this.mcbProductVirtualDirectoryPath.Text = "Relative Path?";
            this.mcbProductVirtualDirectoryPath.UseVisualStyleBackColor = true;
            // 
            // mfbVersionVirtualDirectoryPath
            // 
            this.mfbVersionVirtualDirectoryPath.AutoSize = true;
            this.mfbVersionVirtualDirectoryPath.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.mfbVersionVirtualDirectoryPath.Depth = 0;
            this.mfbVersionVirtualDirectoryPath.Location = new System.Drawing.Point(479, 347);
            this.mfbVersionVirtualDirectoryPath.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.mfbVersionVirtualDirectoryPath.MouseState = MaterialSkin.MouseState.HOVER;
            this.mfbVersionVirtualDirectoryPath.Name = "mfbVersionVirtualDirectoryPath";
            this.mfbVersionVirtualDirectoryPath.Primary = false;
            this.mfbVersionVirtualDirectoryPath.Size = new System.Drawing.Size(23, 36);
            this.mfbVersionVirtualDirectoryPath.TabIndex = 40;
            this.mfbVersionVirtualDirectoryPath.Text = "...";
            this.mfbVersionVirtualDirectoryPath.UseVisualStyleBackColor = true;
            // 
            // mfbEnvironmentVirtualDirectoryPath
            // 
            this.mfbEnvironmentVirtualDirectoryPath.AutoSize = true;
            this.mfbEnvironmentVirtualDirectoryPath.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.mfbEnvironmentVirtualDirectoryPath.Depth = 0;
            this.mfbEnvironmentVirtualDirectoryPath.Location = new System.Drawing.Point(506, 246);
            this.mfbEnvironmentVirtualDirectoryPath.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.mfbEnvironmentVirtualDirectoryPath.MouseState = MaterialSkin.MouseState.HOVER;
            this.mfbEnvironmentVirtualDirectoryPath.Name = "mfbEnvironmentVirtualDirectoryPath";
            this.mfbEnvironmentVirtualDirectoryPath.Primary = false;
            this.mfbEnvironmentVirtualDirectoryPath.Size = new System.Drawing.Size(23, 36);
            this.mfbEnvironmentVirtualDirectoryPath.TabIndex = 39;
            this.mfbEnvironmentVirtualDirectoryPath.Text = "...";
            this.mfbEnvironmentVirtualDirectoryPath.UseVisualStyleBackColor = true;
            // 
            // mfbProductVirtualDirectoryPath
            // 
            this.mfbProductVirtualDirectoryPath.AutoSize = true;
            this.mfbProductVirtualDirectoryPath.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.mfbProductVirtualDirectoryPath.Depth = 0;
            this.mfbProductVirtualDirectoryPath.Location = new System.Drawing.Point(476, 149);
            this.mfbProductVirtualDirectoryPath.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.mfbProductVirtualDirectoryPath.MouseState = MaterialSkin.MouseState.HOVER;
            this.mfbProductVirtualDirectoryPath.Name = "mfbProductVirtualDirectoryPath";
            this.mfbProductVirtualDirectoryPath.Primary = false;
            this.mfbProductVirtualDirectoryPath.Size = new System.Drawing.Size(23, 36);
            this.mfbProductVirtualDirectoryPath.TabIndex = 38;
            this.mfbProductVirtualDirectoryPath.Text = "...";
            this.mfbProductVirtualDirectoryPath.UseVisualStyleBackColor = true;
            // 
            // mfbDefaultWebSitePath
            // 
            this.mfbDefaultWebSitePath.AutoSize = true;
            this.mfbDefaultWebSitePath.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.mfbDefaultWebSitePath.Depth = 0;
            this.mfbDefaultWebSitePath.Location = new System.Drawing.Point(476, 53);
            this.mfbDefaultWebSitePath.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.mfbDefaultWebSitePath.MouseState = MaterialSkin.MouseState.HOVER;
            this.mfbDefaultWebSitePath.Name = "mfbDefaultWebSitePath";
            this.mfbDefaultWebSitePath.Primary = false;
            this.mfbDefaultWebSitePath.Size = new System.Drawing.Size(23, 36);
            this.mfbDefaultWebSitePath.TabIndex = 37;
            this.mfbDefaultWebSitePath.Text = "...";
            this.mfbDefaultWebSitePath.UseVisualStyleBackColor = true;
            // 
            // mtfVeresionVirtualDirectoryPath
            // 
            this.mtfVeresionVirtualDirectoryPath.BackColor = System.Drawing.Color.White;
            this.mtfVeresionVirtualDirectoryPath.Depth = 0;
            this.mtfVeresionVirtualDirectoryPath.Enabled = false;
            this.mtfVeresionVirtualDirectoryPath.Hint = "";
            this.mtfVeresionVirtualDirectoryPath.Location = new System.Drawing.Point(237, 357);
            this.mtfVeresionVirtualDirectoryPath.MouseState = MaterialSkin.MouseState.HOVER;
            this.mtfVeresionVirtualDirectoryPath.Name = "mtfVeresionVirtualDirectoryPath";
            this.mtfVeresionVirtualDirectoryPath.PasswordChar = '\0';
            this.mtfVeresionVirtualDirectoryPath.SelectedText = "";
            this.mtfVeresionVirtualDirectoryPath.SelectionLength = 0;
            this.mtfVeresionVirtualDirectoryPath.SelectionStart = 0;
            this.mtfVeresionVirtualDirectoryPath.Size = new System.Drawing.Size(233, 23);
            this.mtfVeresionVirtualDirectoryPath.TabIndex = 37;
            this.mtfVeresionVirtualDirectoryPath.Text = "Current";
            this.mtfVeresionVirtualDirectoryPath.UseSystemPasswordChar = false;
            // 
            // materialLabel17
            // 
            this.materialLabel17.AutoSize = true;
            this.materialLabel17.BackColor = System.Drawing.Color.White;
            this.materialLabel17.Depth = 0;
            this.materialLabel17.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.materialLabel17.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel17.Location = new System.Drawing.Point(9, 360);
            this.materialLabel17.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel17.Name = "materialLabel17";
            this.materialLabel17.Size = new System.Drawing.Size(212, 19);
            this.materialLabel17.TabIndex = 36;
            this.materialLabel17.Text = "Version Virtual Directory Path:";
            // 
            // mtfVersionVirtualDirectoryName
            // 
            this.mtfVersionVirtualDirectoryName.BackColor = System.Drawing.Color.White;
            this.mtfVersionVirtualDirectoryName.Depth = 0;
            this.mtfVersionVirtualDirectoryName.Enabled = false;
            this.mtfVersionVirtualDirectoryName.Hint = "";
            this.mtfVersionVirtualDirectoryName.Location = new System.Drawing.Point(237, 310);
            this.mtfVersionVirtualDirectoryName.MouseState = MaterialSkin.MouseState.HOVER;
            this.mtfVersionVirtualDirectoryName.Name = "mtfVersionVirtualDirectoryName";
            this.mtfVersionVirtualDirectoryName.PasswordChar = '\0';
            this.mtfVersionVirtualDirectoryName.SelectedText = "";
            this.mtfVersionVirtualDirectoryName.SelectionLength = 0;
            this.mtfVersionVirtualDirectoryName.SelectionStart = 0;
            this.mtfVersionVirtualDirectoryName.Size = new System.Drawing.Size(233, 23);
            this.mtfVersionVirtualDirectoryName.TabIndex = 35;
            this.mtfVersionVirtualDirectoryName.Text = "Current";
            this.mtfVersionVirtualDirectoryName.UseSystemPasswordChar = false;
            // 
            // materialLabel18
            // 
            this.materialLabel18.AutoSize = true;
            this.materialLabel18.BackColor = System.Drawing.Color.White;
            this.materialLabel18.Depth = 0;
            this.materialLabel18.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.materialLabel18.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel18.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel18.Location = new System.Drawing.Point(9, 310);
            this.materialLabel18.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel18.Name = "materialLabel18";
            this.materialLabel18.Size = new System.Drawing.Size(222, 19);
            this.materialLabel18.TabIndex = 34;
            this.materialLabel18.Text = "Version Virtual Directory Name:";
            // 
            // mtfEnvironmentVirtualDirectoryPath
            // 
            this.mtfEnvironmentVirtualDirectoryPath.BackColor = System.Drawing.Color.White;
            this.mtfEnvironmentVirtualDirectoryPath.Depth = 0;
            this.mtfEnvironmentVirtualDirectoryPath.Enabled = false;
            this.mtfEnvironmentVirtualDirectoryPath.Hint = "";
            this.mtfEnvironmentVirtualDirectoryPath.Location = new System.Drawing.Point(266, 258);
            this.mtfEnvironmentVirtualDirectoryPath.MouseState = MaterialSkin.MouseState.HOVER;
            this.mtfEnvironmentVirtualDirectoryPath.Name = "mtfEnvironmentVirtualDirectoryPath";
            this.mtfEnvironmentVirtualDirectoryPath.PasswordChar = '\0';
            this.mtfEnvironmentVirtualDirectoryPath.SelectedText = "";
            this.mtfEnvironmentVirtualDirectoryPath.SelectionLength = 0;
            this.mtfEnvironmentVirtualDirectoryPath.SelectionStart = 0;
            this.mtfEnvironmentVirtualDirectoryPath.Size = new System.Drawing.Size(233, 23);
            this.mtfEnvironmentVirtualDirectoryPath.TabIndex = 33;
            this.mtfEnvironmentVirtualDirectoryPath.Text = "Live";
            this.mtfEnvironmentVirtualDirectoryPath.UseSystemPasswordChar = false;
            // 
            // materialLabel15
            // 
            this.materialLabel15.AutoSize = true;
            this.materialLabel15.BackColor = System.Drawing.Color.White;
            this.materialLabel15.Depth = 0;
            this.materialLabel15.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.materialLabel15.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel15.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel15.Location = new System.Drawing.Point(6, 262);
            this.materialLabel15.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel15.Name = "materialLabel15";
            this.materialLabel15.Size = new System.Drawing.Size(244, 19);
            this.materialLabel15.TabIndex = 32;
            this.materialLabel15.Text = "Environment Virtual Directory Path:";
            // 
            // mtfEnviromentVirtualDirectoryName
            // 
            this.mtfEnviromentVirtualDirectoryName.BackColor = System.Drawing.Color.White;
            this.mtfEnviromentVirtualDirectoryName.Depth = 0;
            this.mtfEnviromentVirtualDirectoryName.Enabled = false;
            this.mtfEnviromentVirtualDirectoryName.Hint = "";
            this.mtfEnviromentVirtualDirectoryName.Location = new System.Drawing.Point(266, 214);
            this.mtfEnviromentVirtualDirectoryName.MouseState = MaterialSkin.MouseState.HOVER;
            this.mtfEnviromentVirtualDirectoryName.Name = "mtfEnviromentVirtualDirectoryName";
            this.mtfEnviromentVirtualDirectoryName.PasswordChar = '\0';
            this.mtfEnviromentVirtualDirectoryName.SelectedText = "";
            this.mtfEnviromentVirtualDirectoryName.SelectionLength = 0;
            this.mtfEnviromentVirtualDirectoryName.SelectionStart = 0;
            this.mtfEnviromentVirtualDirectoryName.Size = new System.Drawing.Size(233, 23);
            this.mtfEnviromentVirtualDirectoryName.TabIndex = 31;
            this.mtfEnviromentVirtualDirectoryName.Text = "Live";
            this.mtfEnviromentVirtualDirectoryName.UseSystemPasswordChar = false;
            // 
            // materialLabel16
            // 
            this.materialLabel16.AutoSize = true;
            this.materialLabel16.BackColor = System.Drawing.Color.White;
            this.materialLabel16.Depth = 0;
            this.materialLabel16.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.materialLabel16.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel16.Location = new System.Drawing.Point(6, 214);
            this.materialLabel16.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel16.Name = "materialLabel16";
            this.materialLabel16.Size = new System.Drawing.Size(254, 19);
            this.materialLabel16.TabIndex = 30;
            this.materialLabel16.Text = "Environment Virtual Directory Name:";
            // 
            // mtfProductVirtualDirectoryPath
            // 
            this.mtfProductVirtualDirectoryPath.BackColor = System.Drawing.Color.White;
            this.mtfProductVirtualDirectoryPath.Depth = 0;
            this.mtfProductVirtualDirectoryPath.Enabled = false;
            this.mtfProductVirtualDirectoryPath.Hint = "";
            this.mtfProductVirtualDirectoryPath.Location = new System.Drawing.Point(234, 163);
            this.mtfProductVirtualDirectoryPath.MouseState = MaterialSkin.MouseState.HOVER;
            this.mtfProductVirtualDirectoryPath.Name = "mtfProductVirtualDirectoryPath";
            this.mtfProductVirtualDirectoryPath.PasswordChar = '\0';
            this.mtfProductVirtualDirectoryPath.SelectedText = "";
            this.mtfProductVirtualDirectoryPath.SelectionLength = 0;
            this.mtfProductVirtualDirectoryPath.SelectionStart = 0;
            this.mtfProductVirtualDirectoryPath.Size = new System.Drawing.Size(233, 23);
            this.mtfProductVirtualDirectoryPath.TabIndex = 29;
            this.mtfProductVirtualDirectoryPath.Text = "Advantage";
            this.mtfProductVirtualDirectoryPath.UseSystemPasswordChar = false;
            // 
            // materialLabel11
            // 
            this.materialLabel11.AutoSize = true;
            this.materialLabel11.BackColor = System.Drawing.Color.White;
            this.materialLabel11.Depth = 0;
            this.materialLabel11.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.materialLabel11.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel11.Location = new System.Drawing.Point(6, 166);
            this.materialLabel11.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel11.Name = "materialLabel11";
            this.materialLabel11.Size = new System.Drawing.Size(212, 19);
            this.materialLabel11.TabIndex = 28;
            this.materialLabel11.Text = "Product Virtual Directory Path:";
            // 
            // mtfProductVirtualDirectoryName
            // 
            this.mtfProductVirtualDirectoryName.AccessibleDescription = "";
            this.mtfProductVirtualDirectoryName.BackColor = System.Drawing.Color.White;
            this.mtfProductVirtualDirectoryName.Depth = 0;
            this.mtfProductVirtualDirectoryName.Enabled = false;
            this.mtfProductVirtualDirectoryName.Hint = "";
            this.mtfProductVirtualDirectoryName.Location = new System.Drawing.Point(234, 114);
            this.mtfProductVirtualDirectoryName.MouseState = MaterialSkin.MouseState.HOVER;
            this.mtfProductVirtualDirectoryName.Name = "mtfProductVirtualDirectoryName";
            this.mtfProductVirtualDirectoryName.PasswordChar = '\0';
            this.mtfProductVirtualDirectoryName.SelectedText = "";
            this.mtfProductVirtualDirectoryName.SelectionLength = 0;
            this.mtfProductVirtualDirectoryName.SelectionStart = 0;
            this.mtfProductVirtualDirectoryName.Size = new System.Drawing.Size(233, 23);
            this.mtfProductVirtualDirectoryName.TabIndex = 27;
            this.mtfProductVirtualDirectoryName.Text = "Advantage";
            this.mtfProductVirtualDirectoryName.UseSystemPasswordChar = false;
            // 
            // materialLabel12
            // 
            this.materialLabel12.AutoSize = true;
            this.materialLabel12.BackColor = System.Drawing.Color.White;
            this.materialLabel12.Depth = 0;
            this.materialLabel12.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.materialLabel12.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel12.Location = new System.Drawing.Point(6, 114);
            this.materialLabel12.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel12.Name = "materialLabel12";
            this.materialLabel12.Size = new System.Drawing.Size(222, 19);
            this.materialLabel12.TabIndex = 26;
            this.materialLabel12.Text = "Product Virtual Directory Name:";
            // 
            // mtfDefaultWebsitePath
            // 
            this.mtfDefaultWebsitePath.BackColor = System.Drawing.Color.White;
            this.mtfDefaultWebsitePath.Depth = 0;
            this.mtfDefaultWebsitePath.Enabled = false;
            this.mtfDefaultWebsitePath.Hint = "";
            this.mtfDefaultWebsitePath.Location = new System.Drawing.Point(231, 62);
            this.mtfDefaultWebsitePath.MouseState = MaterialSkin.MouseState.HOVER;
            this.mtfDefaultWebsitePath.Name = "mtfDefaultWebsitePath";
            this.mtfDefaultWebsitePath.PasswordChar = '\0';
            this.mtfDefaultWebsitePath.SelectedText = "";
            this.mtfDefaultWebsitePath.SelectionLength = 0;
            this.mtfDefaultWebsitePath.SelectionStart = 0;
            this.mtfDefaultWebsitePath.Size = new System.Drawing.Size(233, 23);
            this.mtfDefaultWebsitePath.TabIndex = 25;
            this.mtfDefaultWebsitePath.Text = "C:\\inetpub\\wwwroot";
            this.mtfDefaultWebsitePath.UseSystemPasswordChar = false;
            // 
            // materialLabel13
            // 
            this.materialLabel13.AutoSize = true;
            this.materialLabel13.BackColor = System.Drawing.Color.White;
            this.materialLabel13.Depth = 0;
            this.materialLabel13.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.materialLabel13.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel13.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel13.Location = new System.Drawing.Point(6, 70);
            this.materialLabel13.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel13.Name = "materialLabel13";
            this.materialLabel13.Size = new System.Drawing.Size(154, 19);
            this.materialLabel13.TabIndex = 24;
            this.materialLabel13.Text = "Defaut Web Site Path:";
            // 
            // mtfDefaultWebSiteName
            // 
            this.mtfDefaultWebSiteName.BackColor = System.Drawing.Color.White;
            this.mtfDefaultWebSiteName.Depth = 0;
            this.mtfDefaultWebSiteName.Enabled = false;
            this.mtfDefaultWebSiteName.Hint = "";
            this.mtfDefaultWebSiteName.Location = new System.Drawing.Point(231, 23);
            this.mtfDefaultWebSiteName.MouseState = MaterialSkin.MouseState.HOVER;
            this.mtfDefaultWebSiteName.Name = "mtfDefaultWebSiteName";
            this.mtfDefaultWebSiteName.PasswordChar = '\0';
            this.mtfDefaultWebSiteName.SelectedText = "";
            this.mtfDefaultWebSiteName.SelectionLength = 0;
            this.mtfDefaultWebSiteName.SelectionStart = 0;
            this.mtfDefaultWebSiteName.Size = new System.Drawing.Size(233, 23);
            this.mtfDefaultWebSiteName.TabIndex = 23;
            this.mtfDefaultWebSiteName.Text = "Default Web Site";
            this.mtfDefaultWebSiteName.UseSystemPasswordChar = false;
            // 
            // materialLabel14
            // 
            this.materialLabel14.AutoSize = true;
            this.materialLabel14.BackColor = System.Drawing.Color.White;
            this.materialLabel14.Depth = 0;
            this.materialLabel14.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.materialLabel14.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel14.Location = new System.Drawing.Point(6, 27);
            this.materialLabel14.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel14.Name = "materialLabel14";
            this.materialLabel14.Size = new System.Drawing.Size(164, 19);
            this.materialLabel14.TabIndex = 22;
            this.materialLabel14.Text = "Defaut Web Site Name:";
            // 
            // mtfEnviromentName
            // 
            this.mtfEnviromentName.BackColor = System.Drawing.Color.White;
            this.mtfEnviromentName.Depth = 0;
            this.mtfEnviromentName.Enabled = false;
            this.mtfEnviromentName.Hint = "";
            this.mtfEnviromentName.Location = new System.Drawing.Point(159, 63);
            this.mtfEnviromentName.MouseState = MaterialSkin.MouseState.HOVER;
            this.mtfEnviromentName.Name = "mtfEnviromentName";
            this.mtfEnviromentName.PasswordChar = '\0';
            this.mtfEnviromentName.SelectedText = "";
            this.mtfEnviromentName.SelectionLength = 0;
            this.mtfEnviromentName.SelectionStart = 0;
            this.mtfEnviromentName.Size = new System.Drawing.Size(233, 23);
            this.mtfEnviromentName.TabIndex = 21;
            this.mtfEnviromentName.UseSystemPasswordChar = false;
            // 
            // materialLabel6
            // 
            this.materialLabel6.AutoSize = true;
            this.materialLabel6.BackColor = System.Drawing.Color.White;
            this.materialLabel6.Depth = 0;
            this.materialLabel6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.materialLabel6.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel6.Location = new System.Drawing.Point(20, 63);
            this.materialLabel6.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel6.Name = "materialLabel6";
            this.materialLabel6.Size = new System.Drawing.Size(141, 19);
            this.materialLabel6.TabIndex = 20;
            this.materialLabel6.Text = "Environment Name:";
            // 
            // mrbAddEnviroment
            // 
            this.mrbAddEnviroment.Depth = 0;
            this.mrbAddEnviroment.Enabled = false;
            this.mrbAddEnviroment.Location = new System.Drawing.Point(776, 17);
            this.mrbAddEnviroment.MouseState = MaterialSkin.MouseState.HOVER;
            this.mrbAddEnviroment.Name = "mrbAddEnviroment";
            this.mrbAddEnviroment.Primary = true;
            this.mrbAddEnviroment.Size = new System.Drawing.Size(92, 21);
            this.mrbAddEnviroment.TabIndex = 19;
            this.mrbAddEnviroment.Text = "Add";
            this.mrbAddEnviroment.UseVisualStyleBackColor = true;
            // 
            // cbEnviroment
            // 
            this.cbEnviroment.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbEnviroment.FormattingEnabled = true;
            this.cbEnviroment.Items.AddRange(new object[] {
            "Windows Authentication",
            "SQL Server Authentication"});
            this.cbEnviroment.Location = new System.Drawing.Point(523, 17);
            this.cbEnviroment.Name = "cbEnviroment";
            this.cbEnviroment.Size = new System.Drawing.Size(233, 21);
            this.cbEnviroment.TabIndex = 8;
            // 
            // materialLabel5
            // 
            this.materialLabel5.AutoSize = true;
            this.materialLabel5.BackColor = System.Drawing.Color.White;
            this.materialLabel5.Depth = 0;
            this.materialLabel5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.materialLabel5.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel5.Location = new System.Drawing.Point(364, 19);
            this.materialLabel5.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel5.Name = "materialLabel5";
            this.materialLabel5.Size = new System.Drawing.Size(143, 19);
            this.materialLabel5.TabIndex = 7;
            this.materialLabel5.Text = "Select Environment:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.mtfTenantPassword);
            this.groupBox1.Controls.Add(this.materialLabel10);
            this.groupBox1.Controls.Add(this.mtfTenantUserName);
            this.groupBox1.Controls.Add(this.materialLabel9);
            this.groupBox1.Controls.Add(this.mtfTenantServerName);
            this.groupBox1.Controls.Add(this.materialLabel8);
            this.groupBox1.Controls.Add(this.mtfTenantAuthDbName);
            this.groupBox1.Controls.Add(this.materialLabel7);
            this.groupBox1.Location = new System.Drawing.Point(22, 105);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(558, 206);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Database Configuration";
            // 
            // mtfTenantPassword
            // 
            this.mtfTenantPassword.BackColor = System.Drawing.Color.White;
            this.mtfTenantPassword.Depth = 0;
            this.mtfTenantPassword.Enabled = false;
            this.mtfTenantPassword.Hint = "";
            this.mtfTenantPassword.Location = new System.Drawing.Point(231, 148);
            this.mtfTenantPassword.MouseState = MaterialSkin.MouseState.HOVER;
            this.mtfTenantPassword.Name = "mtfTenantPassword";
            this.mtfTenantPassword.PasswordChar = '*';
            this.mtfTenantPassword.SelectedText = "";
            this.mtfTenantPassword.SelectionLength = 0;
            this.mtfTenantPassword.SelectionStart = 0;
            this.mtfTenantPassword.Size = new System.Drawing.Size(233, 23);
            this.mtfTenantPassword.TabIndex = 29;
            this.mtfTenantPassword.UseSystemPasswordChar = false;
            // 
            // materialLabel10
            // 
            this.materialLabel10.AutoSize = true;
            this.materialLabel10.BackColor = System.Drawing.Color.White;
            this.materialLabel10.Depth = 0;
            this.materialLabel10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.materialLabel10.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel10.Location = new System.Drawing.Point(6, 159);
            this.materialLabel10.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel10.Name = "materialLabel10";
            this.materialLabel10.Size = new System.Drawing.Size(174, 19);
            this.materialLabel10.TabIndex = 28;
            this.materialLabel10.Text = "TenantAuthDb Password";
            // 
            // mtfTenantUserName
            // 
            this.mtfTenantUserName.BackColor = System.Drawing.Color.White;
            this.mtfTenantUserName.Depth = 0;
            this.mtfTenantUserName.Enabled = false;
            this.mtfTenantUserName.Hint = "";
            this.mtfTenantUserName.Location = new System.Drawing.Point(231, 106);
            this.mtfTenantUserName.MouseState = MaterialSkin.MouseState.HOVER;
            this.mtfTenantUserName.Name = "mtfTenantUserName";
            this.mtfTenantUserName.PasswordChar = '\0';
            this.mtfTenantUserName.SelectedText = "";
            this.mtfTenantUserName.SelectionLength = 0;
            this.mtfTenantUserName.SelectionStart = 0;
            this.mtfTenantUserName.Size = new System.Drawing.Size(233, 23);
            this.mtfTenantUserName.TabIndex = 27;
            this.mtfTenantUserName.UseSystemPasswordChar = false;
            // 
            // materialLabel9
            // 
            this.materialLabel9.AutoSize = true;
            this.materialLabel9.BackColor = System.Drawing.Color.White;
            this.materialLabel9.Depth = 0;
            this.materialLabel9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.materialLabel9.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel9.Location = new System.Drawing.Point(6, 114);
            this.materialLabel9.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel9.Name = "materialLabel9";
            this.materialLabel9.Size = new System.Drawing.Size(187, 19);
            this.materialLabel9.TabIndex = 26;
            this.materialLabel9.Text = "TenantAuthDb User Name:";
            // 
            // mtfTenantServerName
            // 
            this.mtfTenantServerName.BackColor = System.Drawing.Color.White;
            this.mtfTenantServerName.Depth = 0;
            this.mtfTenantServerName.Enabled = false;
            this.mtfTenantServerName.Hint = "";
            this.mtfTenantServerName.Location = new System.Drawing.Point(231, 62);
            this.mtfTenantServerName.MouseState = MaterialSkin.MouseState.HOVER;
            this.mtfTenantServerName.Name = "mtfTenantServerName";
            this.mtfTenantServerName.PasswordChar = '\0';
            this.mtfTenantServerName.SelectedText = "";
            this.mtfTenantServerName.SelectionLength = 0;
            this.mtfTenantServerName.SelectionStart = 0;
            this.mtfTenantServerName.Size = new System.Drawing.Size(233, 23);
            this.mtfTenantServerName.TabIndex = 25;
            this.mtfTenantServerName.UseSystemPasswordChar = false;
            // 
            // materialLabel8
            // 
            this.materialLabel8.AutoSize = true;
            this.materialLabel8.BackColor = System.Drawing.Color.White;
            this.materialLabel8.Depth = 0;
            this.materialLabel8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.materialLabel8.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel8.Location = new System.Drawing.Point(6, 70);
            this.materialLabel8.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel8.Name = "materialLabel8";
            this.materialLabel8.Size = new System.Drawing.Size(198, 19);
            this.materialLabel8.TabIndex = 24;
            this.materialLabel8.Text = "TenantAuthDb Server Name:";
            // 
            // mtfTenantAuthDbName
            // 
            this.mtfTenantAuthDbName.BackColor = System.Drawing.Color.White;
            this.mtfTenantAuthDbName.Depth = 0;
            this.mtfTenantAuthDbName.Enabled = false;
            this.mtfTenantAuthDbName.Hint = "";
            this.mtfTenantAuthDbName.Location = new System.Drawing.Point(231, 23);
            this.mtfTenantAuthDbName.MouseState = MaterialSkin.MouseState.HOVER;
            this.mtfTenantAuthDbName.Name = "mtfTenantAuthDbName";
            this.mtfTenantAuthDbName.PasswordChar = '\0';
            this.mtfTenantAuthDbName.SelectedText = "";
            this.mtfTenantAuthDbName.SelectionLength = 0;
            this.mtfTenantAuthDbName.SelectionStart = 0;
            this.mtfTenantAuthDbName.Size = new System.Drawing.Size(233, 23);
            this.mtfTenantAuthDbName.TabIndex = 23;
            this.mtfTenantAuthDbName.UseSystemPasswordChar = false;
            // 
            // materialLabel7
            // 
            this.materialLabel7.AutoSize = true;
            this.materialLabel7.BackColor = System.Drawing.Color.White;
            this.materialLabel7.Depth = 0;
            this.materialLabel7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.materialLabel7.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel7.Location = new System.Drawing.Point(6, 27);
            this.materialLabel7.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel7.Name = "materialLabel7";
            this.materialLabel7.Size = new System.Drawing.Size(219, 19);
            this.materialLabel7.TabIndex = 22;
            this.materialLabel7.Text = "TenantAuthDb Database Name:";
            // 
            // deployTab
            // 
            this.deployTab.Location = new System.Drawing.Point(4, 22);
            this.deployTab.Name = "deployTab";
            this.deployTab.Padding = new System.Windows.Forms.Padding(3);
            this.deployTab.Size = new System.Drawing.Size(1364, 1002);
            this.deployTab.TabIndex = 2;
            this.deployTab.Text = "Deployment";
            this.deployTab.UseVisualStyleBackColor = true;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(1246, 28);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(119, 36);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 19;
            this.pictureBox1.TabStop = false;
            // 
            // tabSelector
            // 
            this.tabSelector.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabSelector.BaseTabControl = this.tabControl;
            this.tabSelector.Depth = 0;
            this.tabSelector.Location = new System.Drawing.Point(0, 63);
            this.tabSelector.MouseState = MaterialSkin.MouseState.HOVER;
            this.tabSelector.Name = "tabSelector";
            this.tabSelector.Size = new System.Drawing.Size(1377, 48);
            this.tabSelector.TabIndex = 18;
            this.tabSelector.Text = "materialTabSelector1";
            // 
            // mrbRefresh
            // 
            this.mrbRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.mrbRefresh.Depth = 0;
            this.mrbRefresh.Location = new System.Drawing.Point(803, 22);
            this.mrbRefresh.MouseState = MaterialSkin.MouseState.HOVER;
            this.mrbRefresh.Name = "mrbRefresh";
            this.mrbRefresh.Primary = true;
            this.mrbRefresh.Size = new System.Drawing.Size(77, 23);
            this.mrbRefresh.TabIndex = 25;
            this.mrbRefresh.Text = "Refresh";
            this.mrbRefresh.UseVisualStyleBackColor = true;
            this.mrbRefresh.Click += new System.EventHandler(this.mrbRefresh_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1377, 895);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.tabSelector);
            this.Controls.Add(this.tabControl);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.Text = "Advantage Installer and Maintenance Tool";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainForm_FormClosed);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.tabControl.ResumeLayout(false);
            this.databaseManagmentTab.ResumeLayout(false);
            this.databaseManagmentTab.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVersionHistory)).EndInit();
            this.advantageAPITab.ResumeLayout(false);
            this.advantageAPITab.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.configurationTab.ResumeLayout(false);
            this.configurationTab.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MaterialSkin.Controls.MaterialTabControl tabControl;
        private System.Windows.Forms.TabPage databaseManagmentTab;
        private System.Windows.Forms.TabPage advantageAPITab;
        private System.Windows.Forms.TabPage deployTab;
        private System.Windows.Forms.TabPage configurationTab;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.ComboBox cbEnvironmentName;
        private MaterialSkin.Controls.MaterialLabel materialLabel2;
        private System.Windows.Forms.ComboBox cbTenantName;
        private MaterialSkin.Controls.MaterialLabel materialLabel1;
        private MaterialSkin.Controls.MaterialRaisedButton mrbBackupDatabases;
        private MaterialSkin.Controls.MaterialSingleLineTextField mtfNewerVersion;
        private MaterialSkin.Controls.MaterialLabel materialLabel4;
        private MaterialSkin.Controls.MaterialSingleLineTextField mtfCurrentVersion;
        private MaterialSkin.Controls.MaterialLabel materialLabel3;
        private System.Windows.Forms.DataGridView dgvVersionHistory;
        private MaterialSkin.Controls.MaterialRaisedButton mrbUpdateDatabase;
        private MaterialSkin.Controls.MaterialRaisedButton mrbInstallNewEnviroment;
        private MaterialSkin.Controls.MaterialRaisedButton mrbInstallNewTenant;
        private MaterialSkin.Controls.MaterialRaisedButton mrbSaveEnviroment;
        private System.Windows.Forms.GroupBox groupBox4;
        private MaterialSkin.Controls.MaterialSingleLineTextField mtfReportServerFolder;
        private MaterialSkin.Controls.MaterialLabel materialLabel24;
        private MaterialSkin.Controls.MaterialSingleLineTextField mtfReportServicesUrl;
        private MaterialSkin.Controls.MaterialLabel materialLabel25;
        private MaterialSkin.Controls.MaterialSingleLineTextField mtfReportExecutionUrl;
        private MaterialSkin.Controls.MaterialLabel materialLabel26;
        private MaterialSkin.Controls.MaterialSingleLineTextField mtfReportServerUrl;
        private MaterialSkin.Controls.MaterialLabel materialLabel27;
        private System.Windows.Forms.GroupBox groupBox3;
        private MaterialSkin.Controls.MaterialSingleLineTextField mtfReleasePackagePath;
        private MaterialSkin.Controls.MaterialLabel materialLabel23;
        private MaterialSkin.Controls.MaterialSingleLineTextField mtfDatabaseLogPath;
        private MaterialSkin.Controls.MaterialLabel materialLabel20;
        private MaterialSkin.Controls.MaterialSingleLineTextField mtfDatabaseInstallationPath;
        private MaterialSkin.Controls.MaterialLabel materialLabel19;
        private MaterialSkin.Controls.MaterialSingleLineTextField mtfSiteBackupLocation;
        private MaterialSkin.Controls.MaterialLabel materialLabel21;
        private MaterialSkin.Controls.MaterialSingleLineTextField mtfDatabaseBackupLocation;
        private MaterialSkin.Controls.MaterialLabel materialLabel22;
        private System.Windows.Forms.GroupBox groupBox2;
        private MaterialSkin.Controls.MaterialSingleLineTextField mtfVeresionVirtualDirectoryPath;
        private MaterialSkin.Controls.MaterialLabel materialLabel17;
        private MaterialSkin.Controls.MaterialSingleLineTextField mtfVersionVirtualDirectoryName;
        private MaterialSkin.Controls.MaterialLabel materialLabel18;
        private MaterialSkin.Controls.MaterialSingleLineTextField mtfEnvironmentVirtualDirectoryPath;
        private MaterialSkin.Controls.MaterialLabel materialLabel15;
        private MaterialSkin.Controls.MaterialSingleLineTextField mtfEnviromentVirtualDirectoryName;
        private MaterialSkin.Controls.MaterialLabel materialLabel16;
        private MaterialSkin.Controls.MaterialSingleLineTextField mtfProductVirtualDirectoryPath;
        private MaterialSkin.Controls.MaterialLabel materialLabel11;
        private MaterialSkin.Controls.MaterialSingleLineTextField mtfProductVirtualDirectoryName;
        private MaterialSkin.Controls.MaterialLabel materialLabel12;
        private MaterialSkin.Controls.MaterialSingleLineTextField mtfDefaultWebsitePath;
        private MaterialSkin.Controls.MaterialLabel materialLabel13;
        private MaterialSkin.Controls.MaterialSingleLineTextField mtfDefaultWebSiteName;
        private MaterialSkin.Controls.MaterialLabel materialLabel14;
        private MaterialSkin.Controls.MaterialSingleLineTextField mtfEnviromentName;
        private MaterialSkin.Controls.MaterialLabel materialLabel6;
        private MaterialSkin.Controls.MaterialRaisedButton mrbAddEnviroment;
        private System.Windows.Forms.ComboBox cbEnviroment;
        private MaterialSkin.Controls.MaterialLabel materialLabel5;
        private System.Windows.Forms.GroupBox groupBox1;
        private MaterialSkin.Controls.MaterialSingleLineTextField mtfTenantPassword;
        private MaterialSkin.Controls.MaterialLabel materialLabel10;
        private MaterialSkin.Controls.MaterialSingleLineTextField mtfTenantUserName;
        private MaterialSkin.Controls.MaterialLabel materialLabel9;
        private MaterialSkin.Controls.MaterialSingleLineTextField mtfTenantServerName;
        private MaterialSkin.Controls.MaterialLabel materialLabel8;
        private MaterialSkin.Controls.MaterialSingleLineTextField mtfTenantAuthDbName;
        private MaterialSkin.Controls.MaterialLabel materialLabel7;
        private System.Windows.Forms.GroupBox groupBox5;
        private MaterialSkin.Controls.MaterialCheckBox mcbApiAppPath;
        private MaterialSkin.Controls.MaterialCheckBox mcbWapiAppPath;
        private MaterialSkin.Controls.MaterialCheckBox mcbWebServiceAppPath;
        private MaterialSkin.Controls.MaterialCheckBox mcbHostApplicationPath;
        private MaterialSkin.Controls.MaterialCheckBox mcbSiteApplicationPath;
        private MaterialSkin.Controls.MaterialFlatButton mfbApiAppPath;
        private MaterialSkin.Controls.MaterialSingleLineTextField mtfApiAppPath;
        private MaterialSkin.Controls.MaterialLabel materialLabel36;
        private MaterialSkin.Controls.MaterialSingleLineTextField mtfApiAppName;
        private MaterialSkin.Controls.MaterialLabel materialLabel37;
        private MaterialSkin.Controls.MaterialFlatButton mfbWapiApPath;
        private MaterialSkin.Controls.MaterialSingleLineTextField mtfWapiAppPath;
        private MaterialSkin.Controls.MaterialLabel materialLabel34;
        private MaterialSkin.Controls.MaterialSingleLineTextField mtfWapiAppName;
        private MaterialSkin.Controls.MaterialLabel materialLabel35;
        private MaterialSkin.Controls.MaterialFlatButton mfbWebServiceAppPath;
        private MaterialSkin.Controls.MaterialSingleLineTextField mtfWebServiceAppPath;
        private MaterialSkin.Controls.MaterialLabel materialLabel32;
        private MaterialSkin.Controls.MaterialSingleLineTextField mtfWebServiceAppName;
        private MaterialSkin.Controls.MaterialLabel materialLabel33;
        private MaterialSkin.Controls.MaterialFlatButton mfbHostApplicationPath;
        private MaterialSkin.Controls.MaterialSingleLineTextField mtfHostApplicationPath;
        private MaterialSkin.Controls.MaterialLabel materialLabel30;
        private MaterialSkin.Controls.MaterialSingleLineTextField mtfHostApplicationName;
        private MaterialSkin.Controls.MaterialLabel materialLabel31;
        private MaterialSkin.Controls.MaterialFlatButton mfbSiteApplicationPath;
        private MaterialSkin.Controls.MaterialSingleLineTextField mtfSiteApplicationPath;
        private MaterialSkin.Controls.MaterialLabel materialLabel29;
        private MaterialSkin.Controls.MaterialSingleLineTextField mtfSiteApplicationName;
        private MaterialSkin.Controls.MaterialLabel materialLabel28;
        private MaterialSkin.Controls.MaterialFlatButton mfbReleasePackagePath;
        private MaterialSkin.Controls.MaterialFlatButton mfbDatabaseLogInstallation;
        private MaterialSkin.Controls.MaterialFlatButton mfbDatabaseInstallationLocation;
        private MaterialSkin.Controls.MaterialFlatButton mfbSiteBackupLocation;
        private MaterialSkin.Controls.MaterialFlatButton mfbDatabaseBackupLocation;
        private MaterialSkin.Controls.MaterialCheckBox mcbVersionVirtualDirectoryPath;
        private MaterialSkin.Controls.MaterialCheckBox mcbEnvironmentVirtualDirectoryPath;
        private MaterialSkin.Controls.MaterialCheckBox mcbProductVirtualDirectoryPath;
        private MaterialSkin.Controls.MaterialFlatButton mfbVersionVirtualDirectoryPath;
        private MaterialSkin.Controls.MaterialFlatButton mfbEnvironmentVirtualDirectoryPath;
        private MaterialSkin.Controls.MaterialFlatButton mfbProductVirtualDirectoryPath;
        private MaterialSkin.Controls.MaterialFlatButton mfbDefaultWebSitePath;
        private MaterialSkin.Controls.MaterialLabel mtlStatus;
        private MaterialSkin.Controls.MaterialLabel materialLabel38;
        private MaterialSkin.Controls.MaterialLabel materialLabel43;
        private MaterialSkin.Controls.MaterialRaisedButton InstalCoreWebHostingButton;
        private MaterialSkin.Controls.MaterialLabel materialLabel41;
        private MaterialSkin.Controls.MaterialRaisedButton InstallCoreRuntimeButton;
        private MaterialSkin.Controls.MaterialLabel materialLabel40;
        private MaterialSkin.Controls.MaterialLabel materialLabel39;
        private MaterialSkin.Controls.MaterialRaisedButton LaunchApiButton;
        private MaterialSkin.Controls.MaterialLabel materialLabel44;
        private MaterialSkin.Controls.MaterialRaisedButton SaveAdvantageApiUrlButton;
        private MaterialSkin.Controls.MaterialSingleLineTextField mtfAdvantageApiUrl;
        private System.Windows.Forms.ComboBox TenantAdvantageApiComboBox;
        private MaterialSkin.Controls.MaterialLabel materialLabel45;
        private System.Windows.Forms.ComboBox EnvironmentApiTabComboBox;
        private MaterialSkin.Controls.MaterialLabel materialLabel46;
        private MaterialSkin.Controls.MaterialLabel materialLabel47;
        private MaterialSkin.Controls.MaterialTabSelector tabSelector;
        private System.Windows.Forms.DataGridViewTextBoxColumn Status;
        private System.Windows.Forms.DataGridViewTextBoxColumn Description;
        private System.Windows.Forms.DataGridViewTextBoxColumn Major;
        private System.Windows.Forms.DataGridViewTextBoxColumn Minor;
        private System.Windows.Forms.DataGridViewTextBoxColumn Build;
        private System.Windows.Forms.DataGridViewTextBoxColumn Revision;
        private System.Windows.Forms.DataGridViewTextBoxColumn DateOfExecution;
        private MaterialSkin.Controls.MaterialRaisedButton mrbInstallSqlJobs;
        private System.Windows.Forms.TabPage tabPage1;
        private MaterialSkin.Controls.MaterialRaisedButton MrbInstalCrystal64Bit;
        private MaterialSkin.Controls.MaterialLabel materialLabel48;
        private MaterialSkin.Controls.MaterialRaisedButton MrbInstalCrystal32Bit;
        private MaterialSkin.Controls.MaterialLabel materialLabel42;
        private MaterialSkin.Controls.MaterialRaisedButton mrbInstallStarterDatabase;
        private MaterialSkin.Controls.MaterialRaisedButton mrbRefresh;
    }
}