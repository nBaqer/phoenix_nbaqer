﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MainForm.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The main form.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AdvantageDatabaseUpdateForm
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Diagnostics.CodeAnalysis;
    using System.Drawing;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Forms;

    using AdvantageDatabaseUpdateForm.Common;
    using AdvantageDatabaseUpdateForm.Common.Database;
    using AdvantageDatabaseUpdateForm.Tabs;

    using MaterialSkin;

    using Newtonsoft.Json;

    /// <summary>
    /// The main form.
    /// </summary>
    public partial class MainForm : MaterialSkin.Controls.MaterialForm
    {
        /// <summary>
        /// The connection configuration.
        /// </summary>
        private readonly ConnectionConfiguration connectionConfiguration;

        /// <summary>
        /// The backup confirmation box.
        /// </summary>
        private readonly BackupConfirmationBox backupConfirmationBox;

        /// <summary>
        /// The confirmation box.
        /// </summary>
        private readonly ConfirmationBox confirmationBox;

        /// <summary>
        /// The database management tab.
        /// </summary>
        private DatabaseManagementTab databaseManagementTab;

        /// <summary>
        /// The advantage api tab.
        /// </summary>
        private AdvantageApiTab advantageApiTab;

        private InstallStarterDatabaseForm installStarterDatabaseForm;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainForm"/> class.
        /// </summary>
        /// <param name="connectionConfiguration">
        /// The connection configuration.
        /// </param>
        public MainForm(ConnectionConfiguration connectionConfiguration)
        {
            this.connectionConfiguration = connectionConfiguration;
            this.InitializeComponent();
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Blue800, Primary.Blue900, Primary.Blue500, Accent.Blue200, TextShade.WHITE);
            this.backupConfirmationBox = new BackupConfirmationBox();
            this.confirmationBox = new ConfirmationBox("You are about to install the sql jobs for the selected tenant. Click confirm to continue.");
            this.confirmationBox.confirmButtonClicked += (s, e) => this.ConfirmedInstallSqlJobs();
            this.MrbInstalCrystal64Bit.Enabled = System.Environment.Is64BitOperatingSystem;
        }

        /// <summary>
        /// The main form_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        // ReSharper disable once InconsistentNaming
        private void MainForm_Load(object sender, EventArgs e)
        {
            if (!File.Exists(Executable.GetCurrentPath() + @"\EnvironmentConfiguration.json"))
            {
                this.tabControl.SelectedIndex = 3;
                MessageBox.Show(
                    @"Missing Environment Configuration. Configure an environment to get started!",
                    @"Missing Environment Configuration.",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Warning);
            }
            else
            {
                List<Models.Environment> environments =
                    JsonConvert.DeserializeObject<List<Models.Environment>>(File.ReadAllText(
                        Executable.GetCurrentPath() + @"\EnvironmentConfiguration.json"));

                Models.DatabaseVersionManifest databaseVersionManifest =
                    JsonConvert.DeserializeObject<Models.DatabaseVersionManifest>(
                        File.ReadAllText(Executable.GetCurrentPath() + @"\DatabaseVersionManifest.json"));

                this.databaseManagementTab = new DatabaseManagementTab(
                    environments,
                    databaseVersionManifest);

                this.databaseManagementTab.InitializeTab(this.cbEnvironmentName, this.cbTenantName, this.mtfCurrentVersion, this.mtfNewerVersion, this.dgvVersionHistory, this.mrbUpdateDatabase, this.mrbInstallSqlJobs, this.mtlStatus);

                this.advantageApiTab = new AdvantageApiTab(this.connectionConfiguration, environments, databaseVersionManifest);
                this.advantageApiTab.InitializeTab(this.EnvironmentApiTabComboBox, this.TenantAdvantageApiComboBox, this.InstallCoreRuntimeButton, this.InstalCoreWebHostingButton, this.mtfAdvantageApiUrl, this.SaveAdvantageApiUrlButton, this.LaunchApiButton);
            }
        }


        /// <summary>
        /// The update database button_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        // ReSharper disable once InconsistentNaming
        private void UpdateDatabaseButton_Click(object sender, EventArgs e)
        {
            if (this.backupConfirmationBox.IsConfirmed())
            {
                this.mrbUpdateDatabase.Visible = false;
                this.databaseManagementTab.UpdateDatabaseToLatest();
                this.backupConfirmationBox.ResetConfirmation();
            }
            else
            {
                this.backupConfirmationBox.Show(this);
            }
        }

        /// <summary>
        /// The main form_ form closed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        // ReSharper disable once InconsistentNaming
        private void MainForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        /// <summary>
        /// The tab control_ selected index changed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        [SuppressMessage("StyleCop.CSharp.NamingRules", "SA1300:ElementMustBeginWithUpperCaseLetter", Justification = "Reviewed. Suppression is OK here.")]
        // ReSharper disable once InconsistentNaming
        private void tabControl_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.tabControl.SelectedIndex == 0)
            {
                this.advantageApiTab.LoadEnvironments();
            }
            else if (this.tabControl.SelectedIndex == 1)
            {
                if ((this.databaseManagementTab.PreviousVersion != this.databaseManagementTab.NewerVersion) || string.IsNullOrEmpty(this.databaseManagementTab.PreviousVersion) || string.IsNullOrEmpty(this.databaseManagementTab.NewerVersion))
                {
                    this.EnvironmentApiTabComboBox.Enabled = false;
                    this.TenantAdvantageApiComboBox.Enabled = false;
                    this.mtfAdvantageApiUrl.Enabled = false;
                    this.SaveAdvantageApiUrlButton.Enabled = false;
                    this.LaunchApiButton.Enabled = false;
                    MessageBox.Show(
                        @"The database is not up to date or the version was not obtained. Go back to the Database Management Tab to update the tenant to the latest version",
                        @"Database Verification Failed!",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Exclamation);
                }
                else
                {
                    this.EnvironmentApiTabComboBox.Enabled = true;
                    this.TenantAdvantageApiComboBox.Enabled = true;
                    this.mtfAdvantageApiUrl.Enabled = true;
                    this.SaveAdvantageApiUrlButton.Enabled = true;
                    this.LaunchApiButton.Enabled = true;
                }
            }
        }

        /// <summary>
        /// The mrb install SQL jobs_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void mrbInstallSqlJobs_Click(object sender, EventArgs e)
        {
            this.confirmationBox.Show(this);
        }

        /// <summary>
        /// The confirmed install SQL jobs.
        /// </summary>
        private void ConfirmedInstallSqlJobs()
        {
            this.mrbInstallSqlJobs.Visible = false;
            this.databaseManagementTab.InstallSqlJobs();
            this.backupConfirmationBox.ResetConfirmation();
        }

        /// <summary>
        /// The instal crystal 32 bit_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void MrbInstalCrystal32Bit_Click(object sender, EventArgs e)
        {
            Executable.Execute(Executable.GetCurrentPath() + "\\Prerequisites\\CRRuntime_32bit_13_0_22.msi");
        }

        /// <summary>
        /// The instal crystal 64 bit_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void MrbInstalCrystal64Bit_Click(object sender, EventArgs e)
        {
            Executable.Execute(Executable.GetCurrentPath() + "\\Prerequisites\\CRRuntime_64bit_13_0_22.msi");
        }

        /// <summary>
        /// The mrb install starter database_ click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void MrbInstallStarterDatabaseClick(object sender, EventArgs e)
        {
            this.installStarterDatabaseForm = new InstallStarterDatabaseForm();
            this.installStarterDatabaseForm.Show();
        }

        /// <summary>
        /// The refresh tenant click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void mrbRefresh_Click(object sender, EventArgs e)
        {
            if (this.databaseManagementTab != null)
            {
                this.databaseManagementTab.RefreshSelectedTenant();
            }
        }
    }
}
