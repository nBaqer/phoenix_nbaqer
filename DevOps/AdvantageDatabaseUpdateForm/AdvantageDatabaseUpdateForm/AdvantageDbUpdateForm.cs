﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AdvantageDbUpdateForm.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the AdvantageDbUpdateForm type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AdvantageDatabaseUpdateForm
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Drawing;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Forms;

    /// <summary>
    /// The advantage db update form.
    /// </summary>
    public partial class AdvantageDbUpdateForm : Form
    {
        /// <summary>
        /// The back backup confirmation box.
        /// </summary>
        private BackupConfirmationBox backBackupConfirmationBox;

        /// <summary>
        /// The advantage db script installer.
        /// </summary>
        private AdvantageDbScriptInstaller advantageDbScriptInstaller;

        /// <summary>
        /// Initializes a new instance of the <see cref="AdvantageDbUpdateForm"/> class.
        /// </summary>
        public AdvantageDbUpdateForm()
        {
            this.backBackupConfirmationBox = new BackupConfirmationBox();

            this.InitializeComponent();

            //this.txtServerName.Text = "CORDERO\\SQLDEV2016";
            //this.txtUserName.Text = "sa";
            //this.txtPassword.Text = "Fame.Fame4321";
            //this.txtTenantDatabaseName.Text = @"TenantAuthDB";

            //this.txtAdvantageDatabaseName.Text = "Landsdale38SP1";
        }

        /// <summary>
        /// The btn test credentials click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void BtnTestCredentialsClick(object sender, EventArgs e)
        {
            try
            {
                this.advantageDbScriptInstaller = new AdvantageDbScriptInstaller(
                    this.txtServerName.Text,
                    this.txtUserName.Text,
                    this.txtPassword.Text,
                    this.txtTenantDatabaseName.Text,
                    this.txtAdvantageDatabaseName.Text,
                    this.txtScriptFolder.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(
                    ex.Message,
                    @"Invalid parameter",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }

            this.advantageDbScriptInstaller?.TestConnection();
        }

        /// <summary>
        /// The btn verify current version click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void BtnVerifyCurrentVersionClick(object sender, EventArgs e)
        {
            try
            {
                this.advantageDbScriptInstaller = new AdvantageDbScriptInstaller(
                    this.txtServerName.Text,
                    this.txtUserName.Text,
                    this.txtPassword.Text,
                    this.txtTenantDatabaseName.Text,
                    this.txtAdvantageDatabaseName.Text,
                    this.txtScriptFolder.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(
                    ex.Message,
                    @"Invalid parameter",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }

            this.advantageDbScriptInstaller?.VerifyCurrentDatabaseVersion();
        }

        /// <summary>
        /// The btn update database click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void BtnUpdateDatabaseClick(object sender, EventArgs e)
        {
            if (this.backBackupConfirmationBox.IsConfirmed())
            {
                try
                {
                    this.advantageDbScriptInstaller = new AdvantageDbScriptInstaller(
                        this.txtServerName.Text,
                        this.txtUserName.Text,
                        this.txtPassword.Text,
                        this.txtTenantDatabaseName.Text,
                        this.txtAdvantageDatabaseName.Text,
                        this.txtScriptFolder.Text);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(
                        ex.Message,
                        @"Invalid parameter",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Error);
                }

                this.advantageDbScriptInstaller?.UpdateDatabaseToLatest();
                this.backBackupConfirmationBox.Close();
                this.backBackupConfirmationBox = new BackupConfirmationBox();
            }
            else
            {
                this.backBackupConfirmationBox.Show(this);
            }
        }
    }
}
