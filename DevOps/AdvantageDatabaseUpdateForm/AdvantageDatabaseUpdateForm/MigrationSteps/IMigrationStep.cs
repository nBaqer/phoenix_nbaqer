﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IMigrationStep.cs" company="">
//   
// </copyright>
// <summary>
//   Defines the IMigrationStep type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AdvantageDatabaseUpdateForm.MigrationSteps
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    using Version = AdvantageDatabaseUpdateForm.Models.Version;

    /// <summary>
    /// The MigrationStep interface.
    /// </summary>
    public interface IMigrationStep
    {
        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="connectionString">
        /// The connection string.
        /// </param>
        /// <param name="databaseName">
        /// The database name.
        /// </param>
        /// <param name="advantageHistoryDatabaseName">
        /// The advantage database that will be used to store the history version record.
        /// </param>
        /// <param name="version">
        /// The version details from the database version manifest
        /// </param>
        /// <param name="databaseClientId">
        /// The current database client id
        /// </param>
        void Update(string connectionString, string databaseName, string advantageHistoryDatabaseName, Version version, Guid? databaseClientId);
    }
}
