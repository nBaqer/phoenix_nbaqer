﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UpdateTo3913.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the UpdateTo3913 type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AdvantageDatabaseUpdateForm.MigrationSteps.Version39
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    using AdvantageDatabaseUpdateForm.Data;

    using Version = AdvantageDatabaseUpdateForm.Models.Version;

    /// <summary>
    /// The update to 3913.
    /// </summary>
    public class UpdateTo3913 : DatabaseScriptRunner, IMigrationStep
    {
        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="connectionString">
        /// The connection string.
        /// </param>
        /// <param name="databaseName">
        /// The database name.
        /// </param>
        /// <param name="advantageHistoryDatabaseName">
        /// The advantage database that will be used to store the history version record.
        /// </param>
        /// <param name="version">
        /// The version.
        /// </param>
        /// <param name="databaseClientId">
        /// The current database client id
        /// </param>
        public void Update(
            string connectionString,
            string databaseName,
            string advantageHistoryDatabaseName,
            Version version,
            Guid? databaseClientId = null)
        {
            try
            {
                string script = File.ReadAllText(
                    this.ScriptLocation + "3.9.1\\RepareKlassAppAddress3.9SP1_3.sql");

                this.RunScript(script, connectionString);

                DatabaseVersionHistory databaseVersionHistory = new DatabaseVersionHistory();
                databaseVersionHistory.InsertIntoHistory(connectionString, advantageHistoryDatabaseName, version.Major, version.Minor, version.Build, version.Revision, version.Description);
            }
            catch (Exception e)
            {
                throw new Exception(
                    @"Unable to execute UpdateTo3913 due to: " + e.Message + "; " + (e.InnerException?.Message ?? string.Empty));
            }
        }
    }
}
