﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UpdateTo310266.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the UpdateTo31005 type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AdvantageDatabaseUpdateForm.MigrationSteps.Version310
{
    using System;
    using System.IO;

    using AdvantageDatabaseUpdateForm.Data;
    using AdvantageDatabaseUpdateForm.Models;

    using Version = AdvantageDatabaseUpdateForm.Models.Version;

    /// <summary>
    /// The update to 3.10.26.6.
    /// </summary>
    public class UpdateTo310266 : DatabaseScriptRunner, IMigrationStep
    {
        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="connectionString">
        /// The connection string.
        /// </param>
        /// <param name="databaseName">
        /// The database name.
        /// </param>
        /// <param name="advantageHistoryDatabaseName">
        /// The advantage database that will be used to store the history version record.
        /// </param>
        /// <param name="version">
        /// The version.
        /// </param>
        /// <param name="databaseClientId">
        /// The current database client id
        /// </param>
        public void Update(
            string connectionString,
            string databaseName,
            string advantageHistoryDatabaseName,
            Version version,
            Guid? databaseClientId = null)
        {
            try
            {
                string script = File.ReadAllText(
                    this.ScriptLocation + "3.10\\JOB_PaymentPeriodAutoPostJob.sql");

                string strippedDbName = "'" + databaseName.Replace(".", string.Empty).Replace(" ", string.Empty) + "'";

                script = script.Replace("@DatabaseName", strippedDbName);
                script = script.Replace("@JobStringName", "'_PaymentPeriodAutoPostJob'");

                this.RunScript(script, connectionString, false);

                DatabaseVersionHistory databaseVersionHistory = new DatabaseVersionHistory();
                databaseVersionHistory.InsertIntoHistory(connectionString, advantageHistoryDatabaseName, version.Major, version.Minor, version.Build, version.Revision, version.Description);
            }
            catch (Exception e)
            {
                throw new Exception(
                    @"Unable to execute UpdateTo310266 due to: " + e.Message + "; " + (e.InnerException?.Message ?? string.Empty));
            }
        }
    }
}
