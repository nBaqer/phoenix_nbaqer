﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UpdateTo40002.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the UpdateTo40002 type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AdvantageDatabaseUpdateForm.MigrationSteps.Version40
{
    using System;
    using System.IO;

    using AdvantageDatabaseUpdateForm.Data;

    using Version = AdvantageDatabaseUpdateForm.Models.Version;

    /// <summary>
    /// The update to 40002.
    /// </summary>
    public class UpdateTo40002 : DatabaseScriptRunner, IMigrationStep
    {
        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="connectionString">
        /// The connection string.
        /// </param>
        /// <param name="databaseName">
        /// The database name.
        /// </param>
        /// <param name="advantageHistoryDatabaseName">
        /// The advantage history database name.
        /// </param>
        /// <param name="version">
        /// The version.
        /// </param>
        /// <param name="databaseClientId">
        /// The database client id.
        /// </param>
        public void Update(
            string connectionString,
            string databaseName,
            string advantageHistoryDatabaseName,
            Version version,
            Guid? databaseClientId)
        {
            try
            {
                string script = File.ReadAllText(
                    this.ScriptLocation + "4.0\\Update_4.0_To_4.0.2.sql");

                this.RunScript(script, connectionString);

                DatabaseVersionHistory databaseVersionHistory = new DatabaseVersionHistory();
                databaseVersionHistory.InsertIntoHistory(connectionString, advantageHistoryDatabaseName, version.Major, version.Minor, version.Build, version.Revision, version.Description);
            }
            catch (Exception e)
            {
                throw new Exception(
                    @"Unable to execute UpdateTo40002 due to: " + e.Message + "; " + (e.InnerException?.Message ?? string.Empty));
            }
        }
    }
}
