﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UpdateTo311000.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the UpdateTo3100 type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AdvantageDatabaseUpdateForm.MigrationSteps.Version311
{
    using System;
    using System.IO;
    using System.Linq;

    using AdvantageDatabaseUpdateForm.Data;

    using Version = AdvantageDatabaseUpdateForm.Models.Version;

    /// <summary>
    /// The update to 3.11.0.0.
    /// </summary>
    public class UpdateTo311000 : DatabaseScriptRunner, IMigrationStep
    {
        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="connectionString">
        /// The connection string.
        /// </param>
        /// <param name="databaseName">
        /// The database name.
        /// </param>
        /// <param name="advantageHistoryDatabaseName">
        /// The advantage database that will be used to store the history version record.
        /// </param>
        /// <param name="version">
        /// The version.
        /// </param>
        /// <param name="databaseClientId">
        /// The current database client id
        /// </param>
        public void Update(
            string connectionString,
            string databaseName,
            string advantageHistoryDatabaseName,
            Version version,
            Guid? databaseClientId = null)
        {
            try
            {
                string script;

                if (version.ClientId != null && version.ClientId.Value != Guid.Empty)
                {
                    script = File.ReadAllText(this.ScriptLocation + "3.11\\" + version.ClientId + ".sql");
                }
                else if (version.ClientSpecificVersions != null && version.ClientSpecificVersions.Any(x => x.ClientId == databaseClientId))
                {
                    script = File.ReadAllText(this.ScriptLocation + "3.11\\" + databaseClientId + ".sql");
                }
                else
                {
                    script = File.ReadAllText(
                        this.ScriptLocation + "3.11\\Synchronize_3.10_with_3.11.sql");
                }

                this.RunScript(script, connectionString);

                DatabaseVersionHistory databaseVersionHistory = new DatabaseVersionHistory();
                databaseVersionHistory.InsertIntoHistory(connectionString, advantageHistoryDatabaseName, version.Major, version.Minor, version.Build, version.Revision, version.Description);
            }
            catch (Exception e)
            {
                throw new Exception(
                    @"Unable to execute UpdateTo311000 due to: " + e.Message + "; " + (e.InnerException?.Message ?? string.Empty));
            }
        }
    }
}
