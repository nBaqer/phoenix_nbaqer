﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UpdateTo4121.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the UpdateTo4121 type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AdvantageDatabaseUpdateForm.MigrationSteps.Version41
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    using AdvantageDatabaseUpdateForm.Data;

    using Version = AdvantageDatabaseUpdateForm.Models.Version;

    /// <summary>
    /// The update to 4121.
    /// </summary>
    public class UpdateTo4121 : DatabaseScriptRunner, IMigrationStep
    {
        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="connectionString">
        /// The connection string.
        /// </param>
        /// <param name="databaseName">
        /// The database name.
        /// </param>
        /// <param name="advantageHistoryDatabaseName">
        /// The advantage history database name.
        /// </param>
        /// <param name="version">
        /// The version.
        /// </param>
        /// <param name="databaseClientId">
        /// The database client id.
        /// </param>
        public void Update(
            string connectionString,
            string databaseName,
            string advantageHistoryDatabaseName,
            Version version,
            Guid? databaseClientId)
        {
            try
            {
                string script;

                if (version.ClientId != null && version.ClientId.Value != Guid.Empty)
                {
                    script = File.ReadAllText(this.ScriptLocation + "4.1SP1\\" + version.ClientId + ".sql");
                }
                else if (version.ClientSpecificVersions != null && version.ClientSpecificVersions.Any(x => x.ClientId == databaseClientId))
                {
                    script = File.ReadAllText(this.ScriptLocation + "4.1SP1\\" + databaseClientId + ".sql");
                }
                else
                {
                    script = File.ReadAllText(
                        this.ScriptLocation + "4.1SP1\\Update_4.1SP1_rev_v18.sql");
                }

                this.RunScript(script, connectionString);

                DatabaseVersionHistory databaseVersionHistory = new DatabaseVersionHistory();
                databaseVersionHistory.InsertIntoHistory(connectionString, advantageHistoryDatabaseName, version.Major, version.Minor, version.Build, version.Revision, version.Description);
            }
            catch (Exception e)
            {
                throw new Exception(
                    @"Unable to execute UpdateTo4121 due to: " + e.Message + "; " + (e.InnerException?.Message ?? string.Empty));
            }
        }
    }
}
