﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="UpdateTo43001.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the UpdateTo43001 type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AdvantageDatabaseUpdateForm.MigrationSteps.Version43
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    using AdvantageDatabaseUpdateForm.Data;

    using Version = AdvantageDatabaseUpdateForm.Models.Version;

    /// <summary>
    /// The update to UpdateTo43001.
    /// </summary>
    public class UpdateTo43001 : DatabaseScriptRunner, IMigrationStep
    {
        /// <summary>
        /// The update.
        /// </summary>
        /// <param name="connectionString">
        /// The connection string.
        /// </param>
        /// <param name="databaseName">
        /// The database name.
        /// </param>
        /// <param name="advantageHistoryDatabaseName">
        /// The advantage history database name.
        /// </param>
        /// <param name="version">
        /// The version.
        /// </param>
        /// <param name="databaseClientId">
        /// The database client id.
        /// </param>
        public void Update(
            string connectionString,
            string databaseName,
            string advantageHistoryDatabaseName,
            Version version,
            Guid? databaseClientId)
        {
            try
            {
                string script;

                if (version.ClientId != null && version.ClientId.Value != Guid.Empty)
                {
                    script = File.ReadAllText(this.ScriptLocation + "4.1SP3\\" + version.ClientId + ".sql");
                }
                else if (version.ClientSpecificVersions != null && version.ClientSpecificVersions.Any(x => x.ClientId == databaseClientId))
                {
                    script = File.ReadAllText(this.ScriptLocation + "4.1SP3\\" + databaseClientId + ".sql");
                }
                else
                {
                    script = File.ReadAllText(
                        this.ScriptLocation + "4.1SP3\\02_DataChanges.sql");
                }

                this.RunScript(script, connectionString);

                DatabaseVersionHistory databaseVersionHistory = new DatabaseVersionHistory();
                databaseVersionHistory.InsertIntoHistory(connectionString, advantageHistoryDatabaseName, version.Major, version.Minor, version.Build, version.Revision, version.Description);
            }
            catch (Exception e)
            {
                throw new Exception(
                    @"Unable to execute UpdateTo43001 due to: " + e.Message + "; " + (e.InnerException?.Message ?? string.Empty));
            }
        }
    }
}
