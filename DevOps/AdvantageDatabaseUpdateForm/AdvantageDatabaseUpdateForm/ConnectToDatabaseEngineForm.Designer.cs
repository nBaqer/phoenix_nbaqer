﻿namespace AdvantageDatabaseUpdateForm
{
    partial class ConnectToDatabaseEngineForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConnectToDatabaseEngineForm));
            this.materialLabel1 = new MaterialSkin.Controls.MaterialLabel();
            this.mtbServerName = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.materialLabel2 = new MaterialSkin.Controls.MaterialLabel();
            this.cbAuthenticationType = new System.Windows.Forms.ComboBox();
            this.mtbUserName = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel3 = new MaterialSkin.Controls.MaterialLabel();
            this.mtbPassword = new MaterialSkin.Controls.MaterialSingleLineTextField();
            this.materialLabel4 = new MaterialSkin.Controls.MaterialLabel();
            this.mrbConnect = new MaterialSkin.Controls.MaterialRaisedButton();
            this.materialLabel5 = new MaterialSkin.Controls.MaterialLabel();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // materialLabel1
            // 
            this.materialLabel1.AutoSize = true;
            this.materialLabel1.BackColor = System.Drawing.Color.White;
            this.materialLabel1.Depth = 0;
            this.materialLabel1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.materialLabel1.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel1.Location = new System.Drawing.Point(121, 208);
            this.materialLabel1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel1.Name = "materialLabel1";
            this.materialLabel1.Size = new System.Drawing.Size(94, 19);
            this.materialLabel1.TabIndex = 0;
            this.materialLabel1.Text = "Serve Name:";
            // 
            // mtbServerName
            // 
            this.mtbServerName.BackColor = System.Drawing.Color.White;
            this.mtbServerName.Depth = 0;
            this.mtbServerName.Enabled = false;
            this.mtbServerName.Hint = "";
            this.mtbServerName.Location = new System.Drawing.Point(221, 206);
            this.mtbServerName.MouseState = MaterialSkin.MouseState.HOVER;
            this.mtbServerName.Name = "mtbServerName";
            this.mtbServerName.PasswordChar = '\0';
            this.mtbServerName.SelectedText = "";
            this.mtbServerName.SelectionLength = 0;
            this.mtbServerName.SelectionStart = 0;
            this.mtbServerName.Size = new System.Drawing.Size(233, 23);
            this.mtbServerName.TabIndex = 1;
            this.mtbServerName.UseSystemPasswordChar = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(500, 26);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(119, 36);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // materialLabel2
            // 
            this.materialLabel2.AutoSize = true;
            this.materialLabel2.BackColor = System.Drawing.Color.White;
            this.materialLabel2.Depth = 0;
            this.materialLabel2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.materialLabel2.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel2.Location = new System.Drawing.Point(62, 169);
            this.materialLabel2.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel2.Name = "materialLabel2";
            this.materialLabel2.Size = new System.Drawing.Size(147, 19);
            this.materialLabel2.TabIndex = 3;
            this.materialLabel2.Text = "Authentication Type:";
            // 
            // cbAuthenticationType
            // 
            this.cbAuthenticationType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbAuthenticationType.FormattingEnabled = true;
            this.cbAuthenticationType.Items.AddRange(new object[] {
            "Windows Authentication",
            "SQL Server Authentication"});
            this.cbAuthenticationType.Location = new System.Drawing.Point(221, 167);
            this.cbAuthenticationType.Name = "cbAuthenticationType";
            this.cbAuthenticationType.Size = new System.Drawing.Size(233, 21);
            this.cbAuthenticationType.TabIndex = 4;
            this.cbAuthenticationType.SelectedIndexChanged += new System.EventHandler(this.CbAuthenticationTypeSelectedIndexChanged);
            // 
            // mtbUserName
            // 
            this.mtbUserName.BackColor = System.Drawing.Color.White;
            this.mtbUserName.Depth = 0;
            this.mtbUserName.Enabled = false;
            this.mtbUserName.Hint = "";
            this.mtbUserName.Location = new System.Drawing.Point(221, 252);
            this.mtbUserName.MouseState = MaterialSkin.MouseState.HOVER;
            this.mtbUserName.Name = "mtbUserName";
            this.mtbUserName.PasswordChar = '\0';
            this.mtbUserName.SelectedText = "";
            this.mtbUserName.SelectionLength = 0;
            this.mtbUserName.SelectionStart = 0;
            this.mtbUserName.Size = new System.Drawing.Size(233, 23);
            this.mtbUserName.TabIndex = 6;
            this.mtbUserName.UseSystemPasswordChar = false;
            // 
            // materialLabel3
            // 
            this.materialLabel3.AutoSize = true;
            this.materialLabel3.BackColor = System.Drawing.Color.White;
            this.materialLabel3.Depth = 0;
            this.materialLabel3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.materialLabel3.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel3.Location = new System.Drawing.Point(121, 254);
            this.materialLabel3.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel3.Name = "materialLabel3";
            this.materialLabel3.Size = new System.Drawing.Size(88, 19);
            this.materialLabel3.TabIndex = 5;
            this.materialLabel3.Text = "User Name:";
            // 
            // mtbPassword
            // 
            this.mtbPassword.BackColor = System.Drawing.Color.White;
            this.mtbPassword.Depth = 0;
            this.mtbPassword.Enabled = false;
            this.mtbPassword.Hint = "";
            this.mtbPassword.Location = new System.Drawing.Point(221, 296);
            this.mtbPassword.MouseState = MaterialSkin.MouseState.HOVER;
            this.mtbPassword.Name = "mtbPassword";
            this.mtbPassword.PasswordChar = '*';
            this.mtbPassword.SelectedText = "";
            this.mtbPassword.SelectionLength = 0;
            this.mtbPassword.SelectionStart = 0;
            this.mtbPassword.Size = new System.Drawing.Size(233, 23);
            this.mtbPassword.TabIndex = 8;
            this.mtbPassword.UseSystemPasswordChar = false;
            // 
            // materialLabel4
            // 
            this.materialLabel4.AutoSize = true;
            this.materialLabel4.BackColor = System.Drawing.Color.White;
            this.materialLabel4.Depth = 0;
            this.materialLabel4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.materialLabel4.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel4.Location = new System.Drawing.Point(130, 298);
            this.materialLabel4.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel4.Name = "materialLabel4";
            this.materialLabel4.Size = new System.Drawing.Size(79, 19);
            this.materialLabel4.TabIndex = 7;
            this.materialLabel4.Text = "Password:";
            // 
            // mrbConnect
            // 
            this.mrbConnect.Depth = 0;
            this.mrbConnect.Enabled = false;
            this.mrbConnect.Location = new System.Drawing.Point(494, 351);
            this.mrbConnect.MouseState = MaterialSkin.MouseState.HOVER;
            this.mrbConnect.Name = "mrbConnect";
            this.mrbConnect.Primary = true;
            this.mrbConnect.Size = new System.Drawing.Size(126, 40);
            this.mrbConnect.TabIndex = 11;
            this.mrbConnect.Text = "Connect";
            this.mrbConnect.UseVisualStyleBackColor = true;
            this.mrbConnect.Click += new System.EventHandler(this.ConnectButtonOnClick);
            // 
            // materialLabel5
            // 
            this.materialLabel5.AutoSize = true;
            this.materialLabel5.BackColor = System.Drawing.Color.White;
            this.materialLabel5.Depth = 0;
            this.materialLabel5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.materialLabel5.Font = new System.Drawing.Font("Roboto", 11F);
            this.materialLabel5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.materialLabel5.Location = new System.Drawing.Point(207, 102);
            this.materialLabel5.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialLabel5.Name = "materialLabel5";
            this.materialLabel5.Size = new System.Drawing.Size(199, 19);
            this.materialLabel5.TabIndex = 12;
            this.materialLabel5.Text = "Connect to Database Engine";
            // 
            // ConnectToDatabaseEngineForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(631, 440);
            this.Controls.Add(this.materialLabel5);
            this.Controls.Add(this.mrbConnect);
            this.Controls.Add(this.mtbPassword);
            this.Controls.Add(this.materialLabel4);
            this.Controls.Add(this.mtbUserName);
            this.Controls.Add(this.materialLabel3);
            this.Controls.Add(this.cbAuthenticationType);
            this.Controls.Add(this.materialLabel2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.mtbServerName);
            this.Controls.Add(this.materialLabel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "ConnectToDatabaseEngineForm";
            this.Sizable = false;
            this.Text = "Advantage Installer and Maintenance Tool";
            this.Load += new System.EventHandler(this.ConnectToDatabaseEngineFormLoad);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private MaterialSkin.Controls.MaterialLabel materialLabel1;
        private MaterialSkin.Controls.MaterialSingleLineTextField mtbServerName;
        private System.Windows.Forms.PictureBox pictureBox1;
        private MaterialSkin.Controls.MaterialLabel materialLabel2;
        private System.Windows.Forms.ComboBox cbAuthenticationType;
        private MaterialSkin.Controls.MaterialSingleLineTextField mtbUserName;
        private MaterialSkin.Controls.MaterialLabel materialLabel3;
        private MaterialSkin.Controls.MaterialSingleLineTextField mtbPassword;
        private MaterialSkin.Controls.MaterialLabel materialLabel4;
        private MaterialSkin.Controls.MaterialRaisedButton mrbConnect;
        private MaterialSkin.Controls.MaterialLabel materialLabel5;
    }
}