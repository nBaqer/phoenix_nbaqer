﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BackupConfirmationBox.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the BackupConfirmationBox type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AdvantageDatabaseUpdateForm
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Drawing;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Forms;

    using MaterialSkin;
    using MaterialSkin.Controls;

    /// <summary>
    /// The backup confirmation box.
    /// </summary>
    public partial class ConfirmationBox : MaterialSkin.Controls.MaterialForm
    {
        /// <summary>
        /// The is confirmed.
        /// </summary>
        private bool isConfirmed = false;

        /// <summary>
        /// The parent form.
        /// </summary>
        private MaterialForm parentForm;

        /// <summary>
        /// The confirm button clicked.
        /// </summary>
        public event EventHandler confirmButtonClicked;

        /// <summary>
        /// Initializes a new instance of the <see cref="ConfirmationBox"/> class.
        /// </summary>
        public ConfirmationBox(string text)
        {
            this.InitializeComponent();
            this.isConfirmed = false;
            this.ControlBox = false;
            this.UpdateText(text);
        }

        /// <summary>
        /// The update text.
        /// </summary>
        /// <param name="text">
        /// The text.
        /// </param>
        public void UpdateText(string text)
        {
            this.label1.Text = text;
        }

        /// <summary>
        /// The show.
        /// </summary>
        /// <param name="owner">
        /// The owner.
        /// </param>
        public void Show(MaterialForm owner)
        {
            this.Visible = true;
            this.parentForm = owner;
            base.Show();
            this.isConfirmed = false;
            MaterialSkinManager materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.RemoveFormToManage(owner);
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Grey800, Primary.Grey900, Primary.Grey500, Accent.Teal200, TextShade.WHITE);
        }

        /// <summary>
        /// The is confirmed.
        /// </summary>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool IsConfirmed()
        {
            return this.isConfirmed;
        }

        /// <summary>
        /// The reset confirmation.
        /// </summary>
        public void ResetConfirmation()
        {
            this.isConfirmed = false;
        }

        /// <summary>
        /// The btn confirm click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void BtnConfirmClick(object sender, EventArgs e)
        {
            this.isConfirmed = true;
            this.Hide();
            this.confirmButtonClicked?.Invoke(this, e);
        }

        /// <summary>
        /// The btn cancel click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void BtnCancelClick(object sender, EventArgs e)
        {
            this.isConfirmed = false;
            this.Hide();
            this.parentForm.Focus();
        }

        /// <summary>
        /// The backup confirmation box_ visible changed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        // ReSharper disable once InconsistentNaming
        private void BackupConfirmationBox_VisibleChanged(object sender, EventArgs e)
        {
            if (!this.Visible)
            {
                MaterialSkinManager materialSkinManager = MaterialSkinManager.Instance;
                materialSkinManager.RemoveFormToManage(this);
                if (this.parentForm != null)
                {
                    materialSkinManager.AddFormToManage(this.parentForm);
                }
                materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
                materialSkinManager.ColorScheme = new ColorScheme(Primary.Blue800, Primary.Blue900, Primary.Blue500, Accent.Blue200, TextShade.WHITE);
            }
        }
    }
}
