﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ConnectToDatabaseEngineForm.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the ConnectToDatabaseEngineForm type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AdvantageDatabaseUpdateForm
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Drawing;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Forms;

    using AdvantageDatabaseUpdateForm.Common;
    using AdvantageDatabaseUpdateForm.Common.Database;

    using MaterialSkin;

    /// <summary>
    /// The connect to database engine form.
    /// </summary>
    public partial class ConnectToDatabaseEngineForm : MaterialSkin.Controls.MaterialForm
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ConnectToDatabaseEngineForm"/> class.
        /// </summary>
        public ConnectToDatabaseEngineForm()
        {
            this.InitializeComponent();
            var materialSkinManager = MaterialSkinManager.Instance;
            materialSkinManager.AddFormToManage(this);
            materialSkinManager.Theme = MaterialSkinManager.Themes.LIGHT;
            materialSkinManager.ColorScheme = new ColorScheme(Primary.Blue800, Primary.Blue900, Primary.Blue500, Accent.LightBlue200, TextShade.WHITE);
        }

        /// <summary>
        /// The connect to database engine form_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void ConnectToDatabaseEngineFormLoad(object sender, EventArgs e)
        {
#if DEBUG
            this.mtbServerName.Text = @"localhost\SqlDev2016";
#endif
        }

        /// <summary>
        /// The cb authentication type_ selected index changed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void CbAuthenticationTypeSelectedIndexChanged(object sender, EventArgs e)
        {
            this.mtbPassword.Text = string.Empty;
            this.mrbConnect.Enabled = true;
            if (this.cbAuthenticationType.SelectedIndex == 0)
            {
                /*
                 * Windows Authentication
                 */
                this.mtbServerName.Enabled = true;
                this.mtbPassword.Enabled = false;
                this.mtbUserName.Enabled = true;
                this.mtbUserName.Text = Environment.UserDomainName + @"\" + Environment.UserName;
            }
            else if (this.cbAuthenticationType.SelectedIndex == 1)
            {
                /*
                 * SQL Server Authentication
                 */
                this.mtbServerName.Enabled = true;
                this.mtbPassword.Enabled = true;
                this.mtbUserName.Enabled = true;
                this.mtbUserName.Text = string.Empty;
            }
        }

        /// <summary>
        /// The connect button on click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void ConnectButtonOnClick(object sender, EventArgs e)
        {
            ConnectionConfiguration connectionConfiguration = new ConnectionConfiguration();
            connectionConfiguration.UserName = this.mtbUserName.Text;
            connectionConfiguration.ConnectionType = (ConnectionType)this.cbAuthenticationType.SelectedIndex;
            connectionConfiguration.Password = this.mtbPassword.Text;
            connectionConfiguration.ServerName = this.mtbServerName.Text;
            if (connectionConfiguration.Validate())
            {
                if (connectionConfiguration.TestConnection())
                {
                    MainForm mainForm = new MainForm(connectionConfiguration);
                    mainForm.Show();
                    this.Hide();
                }
            }
        }
    }
}
