﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Program.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the Program type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AdvantageDatabaseUpdateForm
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Windows.Forms;

    using AdvantageDatabaseUpdateForm.Common.CLI;
    using AdvantageDatabaseUpdateForm.Models;

    using Newtonsoft.Json;

    using Version = AdvantageDatabaseUpdateForm.Models.Version;

    /// <summary>
    /// The program.
    /// </summary>
    public static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        /// <param name="args">
        /// The args.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        [STAThread]
        public static int Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            if (args != null && args.Length > 0)
            {
                //Debugger.Launch();
                Execute cliExecute = new Execute(args);
                return cliExecute.RunByCommandLine();
            }
            else
            {
                Application.Run(new ConnectToDatabaseEngineForm());
            }

            return 0;
        }
    }
}
