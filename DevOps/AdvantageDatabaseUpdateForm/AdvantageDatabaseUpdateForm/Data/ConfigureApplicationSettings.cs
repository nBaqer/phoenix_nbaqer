﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ConfigureApplicationSettings.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the ConfigureApplicationSettings type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AdvantageDatabaseUpdateForm.Data
{
    using System.Data;
    using System.Data.SqlClient;

    /// <summary>
    /// The configure application settings.
    /// </summary>
    public class ConfigureApplicationSettings
    {
        /// <summary>
        /// The advantage connection string.
        /// </summary>
        private readonly string advantageConnectionString;

        /// <summary>
        /// Initializes a new instance of the <see cref="ConfigureApplicationSettings"/> class.
        /// </summary>
        /// <param name="advantageConnectionString">
        /// The advantage connection string.
        /// </param>
        public ConfigureApplicationSettings(string advantageConnectionString)
        {
            this.advantageConnectionString = advantageConnectionString;
        }

        /// <summary>
        /// The update application setting.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <param name="value">
        /// The value.
        /// </param>
        public void UpdateApplicationSetting(string key, string value)
        {
            DatabaseScriptRunner scriptRunner = new DatabaseScriptRunner();

            string script = "UPDATE SettingValue SET SettingValue.Value = @Value FROM dbo.syConfigAppSetValues SettingValue INNER JOIN dbo.syConfigAppSettings Setting ON Setting.SettingId = SettingValue.SettingId WHERE Setting.KeyName = @KeyName";
            SqlCommand command = new SqlCommand(script);
            command.CommandType = CommandType.Text;
            command.Parameters.AddWithValue("@Value", value);
            command.Parameters.AddWithValue("@KeyName", key);
            scriptRunner.RunCommand(command, this.advantageConnectionString);
        }

        /// <summary>
        /// The get application setting value.
        /// </summary>
        /// <param name="key">
        /// The key.
        /// </param>
        /// <typeparam name="T">
        /// The type of value 
        /// </typeparam>
        /// <returns>
        /// The <see cref="T"/>.
        /// </returns>
        public T GetApplicationSettingValue<T>(string key)
        {
            DatabaseScriptRunner scriptRunner = new DatabaseScriptRunner();

            string script = "SELECT SettingValue.Value FROM dbo.syConfigAppSetValues SettingValue INNER JOIN dbo.syConfigAppSettings Setting ON Setting.SettingId = SettingValue.SettingId WHERE Setting.KeyName = '" + key + "'";
            DataTable dataTable = scriptRunner.GetDataTable(script, this.advantageConnectionString);

            if (dataTable?.Rows.Count == 1)
            {
                return (T)dataTable.Rows[0][0];
            }

            return default(T);
        }
    }
}
