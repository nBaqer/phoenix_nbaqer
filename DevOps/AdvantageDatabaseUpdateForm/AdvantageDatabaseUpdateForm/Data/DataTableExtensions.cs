﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DataTableExtensions.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the DataTableExtensions type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AdvantageDatabaseUpdateForm.Data
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Reflection;

    /// <summary>
    /// The data table extensions.
    /// </summary>
    public static class DataTableExtensions
    {
        /// <summary>
        /// The type dictionary.
        /// </summary>
        private static readonly Dictionary<Type, IList<PropertyInfo>> TypeDictionary = new Dictionary<Type, IList<PropertyInfo>>();

        /// <summary>
        /// The get properties for type.
        /// </summary>
        /// <typeparam name="T">
        /// The Entity Type
        /// </typeparam>
        /// <returns>
        /// The <see cref="IList"/>.
        /// </returns>
        public static IList<PropertyInfo> GetPropertiesForType<T>()
        {
            var type = typeof(T);

            if (!TypeDictionary.ContainsKey(typeof(T)))
            {
                TypeDictionary.Add(type, type.GetProperties().ToList());
            }

            return TypeDictionary[type];
        }

        /// <summary>
        /// The to list.
        /// </summary>
        /// <param name="table">
        /// The table.
        /// </param>
        /// <typeparam name="T">
        /// The entity type
        /// </typeparam>
        /// <returns>
        /// The <see cref="IList"/>.
        /// </returns>
        public static IList<T> ToList<T>(this DataTable table) where T : new()
        {
            IList<PropertyInfo> properties = GetPropertiesForType<T>();
            IList<T> result = new List<T>();

            foreach (var row in table.Rows)
            {
                result.Add(CreateItemFromRow<T>((DataRow)row, properties));
            }

            return result;
        }

        /// <summary>
        /// The create item from row.
        /// </summary>
        /// <param name="row">
        /// The row.
        /// </param>
        /// <param name="properties">
        /// The properties.
        /// </param>
        /// <typeparam name="T">
        /// The entity type
        /// </typeparam>
        /// <returns>
        /// The <see cref="T"/>.
        /// </returns>
        private static T CreateItemFromRow<T>(DataRow row, IList<PropertyInfo> properties) where T : new()
        {
            T item = new T();

            foreach (var property in properties)
            {
                // Only load those attributes NOT tagged with the Ignore Attribute
                var atr = property.GetCustomAttribute(typeof(IgnoreAttribute));
                if (atr == null)
                {
                    if (row[property.Name] != DBNull.Value)
                    {
                        property.SetValue(item, row[property.Name], null);
                    }
                }
            }

            return item;
        }


        /// <summary>
        /// The ignore attribute.
        /// </summary>
        [AttributeUsage(AttributeTargets.Property, Inherited = false, AllowMultiple = true)]
        public sealed class IgnoreAttribute : Attribute
        {
        }
    }
}
