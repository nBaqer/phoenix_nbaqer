﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DatabaseScriptRunner.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The database script runner.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AdvantageDatabaseUpdateForm.Data
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.IO;
    using System.Windows.Forms;

    using AdvantageDatabaseUpdateForm.Common;

    /// <summary>
    /// The database script runner.
    /// </summary>
    public class DatabaseScriptRunner
    {

        /// <summary>
        /// Gets the script location.
        /// </summary>
        public string ScriptLocation
        {
            get
            {
                return Executable.GetCurrentPath() + "\\Scripts\\";
            }
        }


        /// <summary>
        /// The get connection string.
        /// </summary>
        /// <param name="databaseName">
        /// The database name.
        /// </param>
        /// <param name="sqlUserName">
        /// The sql user name.
        /// </param>
        /// <param name="sqlPassword">
        /// The sql password.
        /// </param>
        /// <param name="sqlServerName">
        /// The sql server name.
        /// </param>
        /// <param name="timeOut">
        /// The time out.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public static string GetConnectionString(string databaseName, string sqlUserName, string sqlPassword, string sqlServerName, int timeOut = int.MaxValue)
        {
            SqlConnectionStringBuilder stringBuilder = new SqlConnectionStringBuilder();
            stringBuilder.UserID = sqlUserName;
            stringBuilder.Password = sqlPassword;
            stringBuilder.DataSource = sqlServerName;
            stringBuilder.InitialCatalog = databaseName;
            stringBuilder.ConnectTimeout = timeOut;
            return stringBuilder.ConnectionString;
        }

        /// <summary>
        /// The get data.
        /// </summary>
        /// <param name="script">
        /// The script.
        /// </param>
        /// <param name="connectionString">
        /// The connection string.
        /// </param>
        /// <typeparam name="T">
        /// The model type entity
        /// </typeparam>
        /// <returns>
        /// The <see cref="IList"/>.
        /// </returns>
        /// <exception cref="Exception">
        /// SQL Connection or Script exception
        /// </exception>
        public IList<T> GetData<T>(string script, string connectionString) where T : new()
        {
            return this.GetDataTable(script, connectionString).ToList<T>();
        }

        /// <summary>
        /// The get data table.
        /// </summary>
        /// <param name="script">
        /// The script.
        /// </param>
        /// <param name="connectionString">
        /// The connection string.
        /// </param>
        /// <returns>
        /// The <see cref="DataTable"/>.
        /// </returns>
        public DataTable GetDataTable(string script, string connectionString)
        {
            SqlConnection connection = new SqlConnection(connectionString);
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand(script, connection);
                SqlDataAdapter adapter = new SqlDataAdapter(command);
                DataTable data = new DataTable();
                adapter.Fill(data);
                return data;
            }
            catch (Exception e)
            {
                MessageBox.Show(
                    @"An un-handle exception has occurred. " + e.Message,
                    @"An error occurred connecting executing query",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }

            return null;
        }

        /// <summary>
        /// The run script.
        /// </summary>
        /// <param name="script">
        /// The script.
        /// </param>
        /// <param name="connectionString">
        /// The connection string.
        /// </param>
        /// <param name="shouldStripGoStatement">
        /// The should strip go statement.
        /// </param>
        /// <exception cref="Exception">
        /// SQL Connection exception or Script exception
        /// </exception>
        public void RunScript(string script, string connectionString, bool shouldStripGoStatement = true)
        {
            int blockId = 0;
            SqlConnection connection = new SqlConnection(connectionString);
            try
            {
                connection.Open();
                SqlCommand command;

                if (shouldStripGoStatement)
                {
                    script = script.Replace("--GO", "--").Replace("-- GO", "-- ").Replace("/* GO", "/*")
                        .Replace("/*GO", "/*");

                    string[] scripts = script.Split(new string[] { "GO\r\n", "GO ", "GO\t", "GO\n" }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (var splitScript in scripts)
                    {
                        command = new SqlCommand(
                            splitScript,
                            connection);
                        command.CommandTimeout = int.MaxValue;
                        command.ExecuteNonQuery();
                        blockId += 1;
                    }
                }
                else
                {
                    command = new SqlCommand(
                        script,
                        connection);
                    command.CommandTimeout = int.MaxValue;
                    command.ExecuteNonQuery();
                }

            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
        }

        /// <summary>
        /// The run command.
        /// </summary>
        /// <param name="command">
        /// The command.
        /// </param>
        /// <param name="connectionString">
        /// The connection string.
        /// </param>
        public void RunCommand(SqlCommand command, string connectionString)
        {
            int blockId = 0;
            SqlConnection connection = new SqlConnection(connectionString);
            try
            {
                connection.Open();
                command.Connection = connection;
                command.CommandTimeout = int.MaxValue;
                command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
        }

        /// <summary>
        /// The install jobs.
        /// </summary>
        /// <param name="databaseName">
        /// The database name.
        /// </param>
        /// <param name="connectionString">
        /// The connection string.
        /// </param>
        /// <param name="version">
        /// The version.
        /// </param>
        public void InstallJobs(string databaseName, string connectionString, string version)
        {
            DirectoryInfo taskDirectory = new DirectoryInfo($"{this.ScriptLocation}{version}\\");
            FileInfo[] taskFiles = taskDirectory.GetFiles("JOB_*.sql");
            string strippedDbName = "'" + databaseName.Replace(".", string.Empty).Replace(" ", string.Empty) + "'";

            foreach (FileInfo file in taskFiles)
            {

                var filePath = file.FullName;
                var fileName = file.Name;
                var jobName = Path.GetFileNameWithoutExtension(fileName).Substring(fileName.IndexOf('_') + 1);
                string script = File.ReadAllText(filePath);
                script = script.Replace("@DatabaseName", strippedDbName);
                script = script.Replace("@JobStringName", "'_" + jobName + "'");

                this.RunScript(script, connectionString, false);

            }
        }
    }
}
