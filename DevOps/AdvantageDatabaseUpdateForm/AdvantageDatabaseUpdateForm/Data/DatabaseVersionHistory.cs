﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DatabaseVersionHistory.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The database version history.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AdvantageDatabaseUpdateForm.Data
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;

    using Version = AdvantageDatabaseUpdateForm.Models.Version;

    /// <summary>
    /// The database version history.
    /// </summary>
    public class DatabaseVersionHistory
    {
        /// <summary>
        /// The database script runner.
        /// </summary>
        private readonly DatabaseScriptRunner databaseScriptRunner = new DatabaseScriptRunner();

        /// <summary>
        /// The insert into history.
        /// </summary>
        /// <param name="connectionString">
        /// The connection string.
        /// </param>
        /// <param name="advantageDatabase">
        /// The advantage database name
        /// </param>
        /// <param name="major">
        /// The major.
        /// </param>
        /// <param name="minor">
        /// The minor.
        /// </param>
        /// <param name="build">
        /// The build.
        /// </param>
        /// <param name="revision">
        /// The revision.
        /// </param>
        /// <param name="description">
        /// The description.
        /// </param>
        public void InsertIntoHistory(
            string connectionString,
            string advantageDatabase,
            int major,
            int minor,
            int build,
            int revision,
            string description)
        {
            string script = string.Format(
                @"INSERT INTO {0}.dbo.syVersionHistory (Major,Minor,Build,Revision,VersionBegin,VersionEnd,Description,ModUser) "
                + "VALUES ( {1},{2},{3},{4}, GETDATE() ,GETDATE() ,'{5}' ,'SUPPORT');",
                advantageDatabase,
                major,
                minor,
                build,
                revision,
                description);

            this.databaseScriptRunner.RunScript(script, connectionString, false);
        }

        /// <summary>
        /// The get history.
        /// </summary>
        /// <param name="connectionString">
        /// The connection string.
        /// </param>
        /// <param name="advantageDatabase">
        /// The advantage database.
        /// </param>
        /// <returns>
        /// The <see cref="IList"/>.
        /// </returns>
        public IList<Version> GetHistory(string connectionString, string advantageDatabase)
        {
            string script = string.Format("SELECT * FROM {0}.dbo.syVersionHistory", advantageDatabase);
            return this.databaseScriptRunner.GetData<Version>(script, connectionString);
        }

        /// <summary>
        /// The get advantage database id.
        /// </summary>
        /// <param name="connectionString">
        /// The connection String.
        /// </param>
        /// <param name="advantageDatabase">
        /// The advantage Database.
        /// </param>
        /// <returns>
        /// The <see cref="Guid"/>.
        /// </returns>
        public Guid GetAdvantageDatabaseId(string connectionString, string advantageDatabase)
        {
            string script = string.Format("SELECT value FROM dbo.syConfigAppSetValues WHERE SettingId = (SELECT SettingId FROM dbo.syConfigAppSettings WHERE KeyName like \'%AdvantageServiceAuthenticationKey%\')", advantageDatabase);
            DataTable value = this.databaseScriptRunner.GetDataTable(script, connectionString);

            if (value?.Rows.Count > 0)
            {
                return Guid.Parse(value.Rows[0]["value"].ToString());
            }

            return Guid.Empty;
        }
    }
}
