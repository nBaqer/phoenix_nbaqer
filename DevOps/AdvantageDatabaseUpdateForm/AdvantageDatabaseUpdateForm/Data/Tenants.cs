﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Tenants.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the Tenants type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AdvantageDatabaseUpdateForm.Data
{
    using System;
    using System.Collections.Generic;

    using AdvantageDatabaseUpdateForm.Models;

    /// <summary>
    /// The tenants.
    /// </summary>
    public class Tenants
    {
        /// <summary>
        /// The database script runner.
        /// </summary>
        private readonly DatabaseScriptRunner databaseScriptRunner = new DatabaseScriptRunner();

        /// <summary>
        /// The get history.
        /// </summary>
        /// <param name="connectionString">
        /// The connection string.
        /// </param>
        /// <param name="tenantAuthDatabaseName">
        /// The tenant database name.
        /// </param>
        /// <returns>
        /// The <see cref="IList"/>.
        /// </returns>
        public IList<Tenant> GetTenants(string connectionString, string tenantAuthDatabaseName)
        {
            string script = string.Format(
                @"SELECT TenantId AS Id, TenantName AS Name, ServerName, DatabaseName, UserName, Password, Environment.VersionNumber AS VersionNumber  FROM {0}.dbo.Tenant AS Tenant "
                + "INNER JOIN  dbo.Environment AS Environment ON Environment.EnvironmentId = Tenant.EnvironmentID",
                tenantAuthDatabaseName);

            return this.databaseScriptRunner.GetData<Tenant>(script, connectionString);
        }
    }
}
