﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AdvantageDbScriptInstaller.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the AdvantageDbScriptInstaller type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AdvantageDatabaseUpdateForm
{
    using System;
    using System.Data;
    using System.Data.SqlClient;
    using System.IO;
    using System.Runtime.InteropServices;
    using System.Windows.Forms;

    /// <summary>
    /// The advantage db script installer.
    /// </summary>
    public class AdvantageDbScriptInstaller
    {
        /// <summary>
        /// The sql server name.
        /// </summary>
        private readonly string sqlServerName;

        /// <summary>
        /// The sql user name.
        /// </summary>
        private readonly string sqlUserName;

        /// <summary>
        /// The sql password.
        /// </summary>
        private readonly string sqlPassword;

        /// <summary>
        /// The tenant db name.
        /// </summary>
        private readonly string tenantDbName;

        /// <summary>
        /// The advantage db name.
        /// </summary>
        private readonly string advantageDbName;

        /// <summary>
        /// The script location.
        /// </summary>
        private readonly string scriptLocation;

        /// <summary>
        /// The latest version is.
        /// </summary>
        private readonly int latestVersionIs = 3931;

        /// <summary>
        /// The latest version code is.
        /// </summary>
        private string latestVersionCodeIs = "3.9.3.1";

        /// <summary>
        /// Initializes a new instance of the <see cref="AdvantageDbScriptInstaller"/> class.
        /// </summary>
        /// <param name="sqlServerName">
        /// The sql server name.
        /// </param>
        /// <param name="sqlUserName">
        /// The sql user name.
        /// </param>
        /// <param name="sqlPassword">
        /// The sql password.
        /// </param>
        /// <param name="tenantDbName">
        /// The tenant db name.
        /// </param>
        /// <param name="advantageDbName">
        /// The advantage db name.
        /// </param>
        /// <param name="scriptLocation">
        /// The script location.
        /// </param>
        /// <exception cref="Exception">
        /// One of the parameters on the constructor is required.
        /// </exception>
        public AdvantageDbScriptInstaller(string sqlServerName, string sqlUserName, string sqlPassword, string tenantDbName, string advantageDbName, string scriptLocation)
        {
            this.sqlPassword = sqlPassword.Trim();
            this.sqlServerName = sqlServerName.Trim();
            this.sqlUserName = sqlUserName.Trim();
            this.tenantDbName = tenantDbName.Trim();
            this.advantageDbName = advantageDbName.Trim();
            this.scriptLocation = scriptLocation.Trim();

            if (string.IsNullOrEmpty(this.sqlServerName))
            {
                throw new Exception("SQL Server Name is Required");
            }

            if (string.IsNullOrEmpty(this.sqlPassword))
            {
                throw new Exception("SQL Server User Password is Required");
            }

            if (string.IsNullOrEmpty(this.sqlUserName))
            {
                throw new Exception("SQL Server User Name is Required");
            }

            if (string.IsNullOrEmpty(this.tenantDbName))
            {
                throw new Exception("Tenant Database Name is Required");
            }

            if (string.IsNullOrEmpty(this.advantageDbName))
            {
                throw new Exception("Advantage Database Name is Required");
            }

            if (string.IsNullOrEmpty(this.scriptLocation))
            {
                throw new Exception("Advantage Database Scripts Location is Required");
            }

            if (!this.scriptLocation.EndsWith("\\"))
            {
                this.scriptLocation = scriptLocation + "\\";
            }
        }


        /// <summary>
        /// The test connection.
        /// </summary>
        public void TestConnection()
        {
            this.TestConnection(this.tenantDbName);
            this.TestConnection(this.advantageDbName);
        }

        /// <summary>
        /// The verify current database version.
        /// </summary>
        public void VerifyCurrentDatabaseVersion()
        {
            SqlConnection connection = new SqlConnection(this.GetConnectionString(this.advantageDbName));
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand(
                    "SELECT * FROM syVersionHistory ORDER BY major DESC, minor DESC, build DESC, revision DESC",
                    connection);
                command.CommandType = CommandType.Text;
                SqlDataAdapter adapter = new SqlDataAdapter(command);
                DataTable dataTable = new DataTable();
                adapter.Fill(dataTable);
                if (dataTable.Rows.Count > 0)
                {
                    string versionCode = dataTable.Rows[0]["Major"].ToString()
                                                                + "." + dataTable.Rows[0]["Minor"].ToString()
                                                                + "." + dataTable.Rows[0]["Build"].ToString()
                                                                + "." + dataTable.Rows[0]["Revision"].ToString();

                    int versionNumber = Convert.ToInt32(dataTable.Rows[0]["Major"].ToString()
                                        + dataTable.Rows[0]["Minor"].ToString()
                                        + dataTable.Rows[0]["Build"].ToString()
                                        + dataTable.Rows[0]["Revision"].ToString());

                    string message = string.Empty;
                    if (versionNumber == this.latestVersionIs)
                    {
                        message = "The database is up to date with the latest version supported by this tool. The database version is: "
                                  + versionCode;
                        MessageBox.Show(message, @"Database up to date!", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        if (versionNumber < this.latestVersionIs)
                        {
                            message = "The database is not up to date. The database is on version: " + versionCode + " and can be updated to the latest version :" + this.latestVersionCodeIs;
                        }
                        else if (versionNumber > this.latestVersionIs)
                        {
                            message = "The database version (v" + versionCode + ") is more recent thant the version supported by this tool (v" + this.latestVersionCodeIs + ")";
                        }

                        MessageBox.Show(message, @"Database or this tool is not up to date!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(
                    e.Message,
                    @"An error occurred executing the action",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
        }

        /// <summary>
        /// The get database version.
        /// </summary>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetDatabaseVersion()
        {
            SqlConnection connection = new SqlConnection(this.GetConnectionString(this.advantageDbName));
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand(
                    "SELECT * FROM syVersionHistory ORDER BY major DESC, minor DESC, build DESC, revision DESC",
                    connection);
                command.CommandType = CommandType.Text;
                SqlDataAdapter adapter = new SqlDataAdapter(command);
                DataTable dataTable = new DataTable();
                adapter.Fill(dataTable);
                if (dataTable.Rows.Count > 0)
                {
                    string version = dataTable.Rows[0]["Major"].ToString() + dataTable.Rows[0]["Minor"].ToString() + dataTable.Rows[0]["Build"].ToString() + dataTable.Rows[0]["Revision"].ToString();
                    return version;
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(
                    e.Message,
                    @"An error occurred executing the action",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }

            return null;
        }

        /// <summary>
        /// The update database to latest.
        /// </summary>
        /// <exception cref="Exception">
        /// Unable to get current database version or failure to execute one of the updates.
        /// </exception>
        public void UpdateDatabaseToLatest()
        {
            try
            {
                string version = this.GetDatabaseVersion();
                if (string.IsNullOrEmpty(version))
                {
                    throw new Exception("Unable to get current database version");
                }

                switch (version)
                {
                    case "3811":
                        {
                            this.UpdateTo38150();
                            this.UpdateTo38151();
                            this.UpdateTo38152();
                            this.UpdateTo3900();
                            this.UpdateTo3901();
                            this.UpdateTo3910();
                            this.UpdateTo3911();
                            this.UpdateTo3912();
                            this.UpdateTo3913();
                            this.UpdateTo3920();
                            this.UpdateTo3921();
                            this.UpdateTo3930();
                            this.UpdateTo3931();
                            break;
                        }

                    case "38150":
                        {
                            this.UpdateTo38151();
                            this.UpdateTo38152();
                            this.UpdateTo3900();
                            this.UpdateTo3901();
                            this.UpdateTo3910();
                            this.UpdateTo3911();
                            this.UpdateTo3912();
                            this.UpdateTo3913();
                            this.UpdateTo3920();
                            this.UpdateTo3921();
                            this.UpdateTo3930();
                            this.UpdateTo3931();
                            break;
                        }

                    case "38151":
                        {
                            this.UpdateTo38152();
                            this.UpdateTo3900();
                            this.UpdateTo3901();
                            this.UpdateTo3910();
                            this.UpdateTo3911();
                            this.UpdateTo3912();
                            this.UpdateTo3913();
                            this.UpdateTo3920();
                            this.UpdateTo3921();
                            this.UpdateTo3930();
                            this.UpdateTo3931();
                            break;
                        }

                    case "38152":
                        {
                            this.UpdateTo3900();
                            this.UpdateTo3901();
                            this.UpdateTo3910();
                            this.UpdateTo3911();
                            this.UpdateTo3912();
                            this.UpdateTo3913();
                            this.UpdateTo3920();
                            this.UpdateTo3921();
                            this.UpdateTo3930();
                            this.UpdateTo3931();
                            break;
                        }
                    case "3900":
                        {
                            this.UpdateTo3901();
                            this.UpdateTo3910();
                            this.UpdateTo3911();
                            this.UpdateTo3912();
                            this.UpdateTo3913();
                            this.UpdateTo3920();
                            this.UpdateTo3921();
                            this.UpdateTo3930();
                            this.UpdateTo3931();
                            break;
                        }

                    case "3901":
                        {
                            this.UpdateTo3910();
                            this.UpdateTo3911();
                            this.UpdateTo3912();
                            this.UpdateTo3913();
                            this.UpdateTo3920();
                            this.UpdateTo3921();
                            this.UpdateTo3930();
                            this.UpdateTo3931();
                            break;
                        }

                    case "3910":
                        {
                            this.UpdateTo3911();
                            this.UpdateTo3912();
                            this.UpdateTo3913();
                            this.UpdateTo3920();
                            this.UpdateTo3921();
                            this.UpdateTo3930();
                            this.UpdateTo3931();
                            break;
                        }
                    case "3911":
                        {
                            this.UpdateTo3912();
                            this.UpdateTo3913();
                            this.UpdateTo3920();
                            this.UpdateTo3921();
                            this.UpdateTo3930();
                            this.UpdateTo3931();
                            break;
                        }

                    case "3912":
                        {
                            this.UpdateTo3913();
                            this.UpdateTo3920();
                            this.UpdateTo3921();
                            this.UpdateTo3930();
                            this.UpdateTo3931();
                            break;
                        }

                    case "3913":
                        {
                            this.UpdateTo3920();
                            this.UpdateTo3921();
                            this.UpdateTo3930();
                            this.UpdateTo3931();
                            break;
                        }
                    case "3920":
                        {
                            this.UpdateTo3921();
                            this.UpdateTo3930();
                            this.UpdateTo3931();
                            break;
                        }
                    case "3921":
                        {
                            this.UpdateTo3930();
                            this.UpdateTo3931();
                            break;
                        }

                    case "3930":
                        {
                            this.UpdateTo3931();
                            break;
                        }
                }

                MessageBox.Show(
                    @"Database updated successfully!",
                    @"Update completed.",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
            }
            catch (Exception e)
            {
                MessageBox.Show(
                    e.Message,
                    @"An error occurred updating the database",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// The get connection string.
        /// </summary>
        /// <param name="databaseName">
        /// The database name.
        /// </param>
        /// <param name="timeOut">
        /// The time Out.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetConnectionString(string databaseName, int timeOut = int.MaxValue)
        {
            SqlConnectionStringBuilder stringBuilder = new SqlConnectionStringBuilder();
            stringBuilder.UserID = this.sqlUserName;
            stringBuilder.Password = this.sqlPassword;
            stringBuilder.DataSource = this.sqlServerName;
            stringBuilder.InitialCatalog = databaseName;
            stringBuilder.ConnectTimeout = timeOut;
            return stringBuilder.ConnectionString;
        }

        /// <summary>
        /// The test connection.
        /// </summary>
        /// <param name="databaseName">
        /// The database name.
        /// </param>
        private void TestConnection(string databaseName)
        {
            SqlConnection connection = new SqlConnection(this.GetConnectionString(databaseName, 3000));
            try
            {
                connection.Open();
                MessageBox.Show(@"Successfully Connected to: " + databaseName);
            }
            catch (Exception e)
            {
                MessageBox.Show(
                    @"Unable to connect to " + databaseName + ". " + e.Message,
                    @"An error occurred connecting to the database",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
        }

        /// <summary>
        /// The run script.
        /// </summary>
        /// <param name="databaseName">
        /// The database name.
        /// </param>
        /// <param name="script">
        /// The script.
        /// </param>
        /// <param name="major">
        /// The major.
        /// </param>
        /// <param name="minor">
        /// The minor.
        /// </param>
        /// <param name="build">
        /// The build.
        /// </param>
        /// <param name="revision">
        /// The revision.
        /// </param>
        /// <param name="description">
        /// The description.
        /// </param>
        /// <param name="shouldStripGoStatement">
        /// The should strip go statement.
        /// </param>
        /// <exception cref="Exception">
        /// SQL Exception running the script or connecting to the database
        /// </exception>
        private void RunScript(
            string databaseName,
            string script,
            int major,
            int minor,
            int build,
            int revision,
            string description,
            bool shouldStripGoStatement = true)
        {
            int blockId = 0;
            SqlConnection connection = new SqlConnection(this.GetConnectionString(databaseName));
            try
            {
                connection.Open();
                SqlCommand command;

                if (shouldStripGoStatement)
                {
                    script = script.Replace("--GO", "--").Replace("-- GO", "-- ").Replace("/* GO", "/*")
                        .Replace("/*GO", "/*");

                    string[] scripts = script.Split(new string[] { "GO\r\n", "GO ", "GO\t" }, StringSplitOptions.RemoveEmptyEntries);
                    foreach (var splitScript in scripts)
                    {
                        command = new SqlCommand(
                            splitScript,
                            connection);
                        command.ExecuteNonQuery();
                        blockId += 1;
                    }
                }
                else
                {
                    command = new SqlCommand(
                        script,
                        connection);
                    command.ExecuteNonQuery();
                }


                command = new SqlCommand("INSERT INTO " + this.advantageDbName + ".dbo.syVersionHistory (Major,Minor,Build,Revision,VersionBegin,VersionEnd,Description,ModUser)VALUES ( @Major,@Minor,@Build,@Revision,GETDATE(),GETDATE(),@Description ,'SUPPORT');", connection);
                command.Parameters.AddWithValue("@Major", major);
                command.Parameters.AddWithValue("@Minor", minor);
                command.Parameters.AddWithValue("@Build", build);
                command.Parameters.AddWithValue("@Revision", revision);
                command.Parameters.AddWithValue("@Description", description);

                command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
        }

        /// <summary>
        /// The update to 38150.
        /// </summary>
        /// <exception cref="Exception">
        /// Unable to execute the script.
        /// </exception>
        private void UpdateTo38150()
        {
            try
            {
                string script = File.ReadAllText(
                    this.scriptLocation + "Step6.SP1\\Converting_DB_3.8GE_Patch_to_3.8SP1.sql");

                this.RunScript(this.advantageDbName, script, 3, 8, 1, 50, "SP1 Schema Changes Script");
            }
            catch (Exception e)
            {
                throw new Exception(
                    @"Unable to execute UpdateTo38150 due to: " + e.Message + "; " + (e.InnerException?.Message ?? string.Empty));
            }
        }

        /// <summary>
        /// The update to 38151.
        /// </summary>
        /// <exception cref="Exception">
        /// Unable to execute the script.
        /// </exception>
        private void UpdateTo38151()
        {
            try
            {
                string script = File.ReadAllText(
                    this.scriptLocation + "Step6.SP1\\3.8SP1_consolidated.sql");

                this.RunScript(this.advantageDbName, script, 3, 8, 1, 51, "SP1 Consolidated Script");
            }
            catch (Exception e)
            {
                throw new Exception("Unable to execute UpdateTo38151 due to: " + e.Message + "; " + (e.InnerException?.Message ?? string.Empty));
            }
        }

        /// <summary>
        /// The update to 38152.
        /// </summary>
        /// <exception cref="Exception">
        /// Unable to execute the script.
        /// </exception>
        private void UpdateTo38152()
        {
            try
            {
                string script = File.ReadAllText(
                    this.scriptLocation + "Step6.SP1\\JOB_AttendanceJob.sql");

                string strippedDbName = "'" + this.advantageDbName.Replace(".", string.Empty).Replace(" ", string.Empty) + "'";

                script = script.Replace("@DatabaseName", strippedDbName);
                script = script.Replace("@JobStringName", "'_AttendanceJob'");

                this.RunScript(this.advantageDbName, script, 3, 8, 1, 52, "SP1 AttendanceJob Script", false);
            }
            catch (Exception e)
            {
                throw new Exception("Unable to execute UpdateTo38152 due to: " + e.Message + "; " + (e.InnerException?.Message ?? string.Empty));
            }
        }

        /// <summary>
        /// The update to 3900.
        /// </summary>
        /// <exception cref="Exception">
        /// Unable to execute the script.
        /// </exception>
        private void UpdateTo3900()
        {
            try
            {
                string script = File.ReadAllText(
                    this.scriptLocation + "3.9\\Synchronize_3.8SP1_with_3.9.sql");

                this.RunScript(this.advantageDbName, script, 3, 9, 0, 0, "Schema changes in 3.9");
            }
            catch (Exception e)
            {
                throw new Exception("Unable to execute UpdateTo3900 due to: " + e.Message + "; " + (e.InnerException?.Message ?? string.Empty));
            }
        }

        /// <summary>
        /// The update to 3901.
        /// </summary>
        /// <exception cref="Exception">
        /// Unable to execute the script.
        /// </exception>
        private void UpdateTo3901()
        {
            try
            {
                string script = File.ReadAllText(
                    this.scriptLocation + "3.9\\3.9_consolidated.sql");

                this.RunScript(this.advantageDbName, script, 3, 9, 0, 1, "Data Changes Consolidated Scrip 3.9");
            }
            catch (Exception e)
            {
                throw new Exception("Unable to execute UpdateTo3901 due to: " + e.Message + "; " + (e.InnerException?.Message ?? string.Empty));
            }
        }

        /// <summary>
        /// The update to 3910.
        /// </summary>
        /// <exception cref="Exception">
        /// Unable to execute the script.
        /// </exception>
        private void UpdateTo3910()
        {
            try
            {
                string script = File.ReadAllText(
                    this.scriptLocation + "3.9.1\\Synchronize_3.9_with_3.9SP1.sql");

                this.RunScript(this.advantageDbName, script, 3, 9, 1, 0, "Schema changes in 3.9.1");
            }
            catch (Exception e)
            {
                throw new Exception("Unable to execute UpdateTo3910 due to: " + e.Message + "; " + (e.InnerException?.Message ?? string.Empty));
            }
        }

        /// <summary>
        /// The update to 3911.
        /// </summary>
        /// <exception cref="Exception">
        /// Unable to execute the script.
        /// </exception>
        private void UpdateTo3911()
        {
            try
            {
                string script = File.ReadAllText(
                    this.scriptLocation + "3.9.1\\3.9SP1_consolidated.sql");

                this.RunScript(this.advantageDbName, script, 3, 9, 1, 1, "Data Changes Consolidated Scrip 3.9.1");
            }
            catch (Exception e)
            {
                throw new Exception("Unable to execute UpdateTo3911 due to: " + e.Message + "; " + (e.InnerException?.Message ?? string.Empty));
            }
        }

        /// <summary>
        /// The update to 3912.
        /// </summary>
        /// <exception cref="Exception">
        /// Unable to execute the script.
        /// </exception>
        private void UpdateTo3912()
        {
            try
            {
                string script = File.ReadAllText(
                    this.scriptLocation + "3.9.1\\HOST_MultiTenantHost.sql");

                script = script.Replace("USE [TenantAuthDB];", string.Empty);

                this.RunScript(this.tenantDbName, script, 3, 9, 1, 2, "Changes to MultiTenant DB for Voyant and Klass-App");
            }
            catch (Exception e)
            {
                throw new Exception("Unable to execute UpdateTo3912 due to: " + e.Message + "; " + (e.InnerException?.Message ?? string.Empty));
            }
        }

        /// <summary>
        /// The update to 3913.
        /// </summary>
        /// <exception cref="Exception">
        /// Unable to execute the script.
        /// </exception>
        private void UpdateTo3913()
        {
            try
            {
                string script = File.ReadAllText(
                    this.scriptLocation + "3.9.1\\RepareKlassAppAddress3.9SP1_3.sql");

                this.RunScript(this.advantageDbName, script, 3, 9, 1, 3, "Change Address to Configurate KlassApp from HTTP to HTTPS");
            }
            catch (Exception e)
            {
                throw new Exception("Unable to execute UpdateTo3913 due to: " + e.Message + "; " + (e.InnerException?.Message ?? string.Empty));
            }
        }

        /// <summary>
        /// The update to 3920.
        /// </summary>
        /// <exception cref="Exception">
        /// Unable to execute the script.
        /// </exception>
        private void UpdateTo3920()
        {
            try
            {
                string script = File.ReadAllText(
                    this.scriptLocation + "3.9.2\\Synchronize_3.9SP1_with_3.9SP2.sql");

                this.RunScript(this.advantageDbName, script, 3, 9, 2, 0, "Synchronize Schema for 3.9.2");
            }
            catch (Exception e)
            {
                throw new Exception("Unable to execute UpdateTo3920 due to: " + e.Message + "; " + (e.InnerException?.Message ?? string.Empty));
            }
        }

        /// <summary>
        /// The update to 3921.
        /// </summary>
        /// <exception cref="Exception">
        /// Unable to execute the script.
        /// </exception>
        private void UpdateTo3921()
        {
            try
            {
                string script = File.ReadAllText(
                    this.scriptLocation + "3.9.2\\3.9SP2_consolidated.sql");

                this.RunScript(this.advantageDbName, script, 3, 9, 2, 1, "Consolidated Script for 3.9.2");
            }
            catch (Exception e)
            {
                throw new Exception("Unable to execute UpdateTo3921 due to: " + e.Message + "; " + (e.InnerException?.Message ?? string.Empty));
            }
        }

        /// <summary>
        /// The update to 3930.
        /// </summary>
        /// <exception cref="Exception">
        /// Unable to execute the script.
        /// </exception>
        private void UpdateTo3930()
        {
            try
            {
                string script = File.ReadAllText(
                    this.scriptLocation + "3.9.3\\Synchronize_3.9SP2_with_3.9SP3.sql");

                this.RunScript(this.advantageDbName, script, 3, 9, 3, 0, "Synchronize Schema for 3.9.3");
            }
            catch (Exception e)
            {
                throw new Exception("Unable to execute UpdateTo3930 due to: " + e.Message + "; " + (e.InnerException?.Message ?? string.Empty));
            }
        }

        /// <summary>
        /// The update to 3931.
        /// </summary>
        /// <exception cref="Exception">
        /// Unable to execute the script.
        /// </exception>
        private void UpdateTo3931()
        {
            try
            {
                string script = File.ReadAllText(
                    this.scriptLocation + "3.9.3\\3.9SP3_consolidated.sql");

                this.RunScript(this.advantageDbName, script, 3, 9, 3, 1, "Consolidated Script for 3.9.3");
            }
            catch (Exception e)
            {
                throw new Exception("Unable to execute UpdateTo3931 due to: " + e.Message + "; " + (e.InnerException?.Message ?? string.Empty));
            }
        }
    }
}
