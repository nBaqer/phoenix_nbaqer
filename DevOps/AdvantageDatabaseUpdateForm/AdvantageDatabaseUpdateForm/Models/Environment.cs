﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Environment.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the Environment type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AdvantageDatabaseUpdateForm.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// The environment.
    /// </summary>
    public class Environment
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Environment"/> class.
        /// </summary>
        public Environment()
        {
            this.DatabaseConfiguration = new DatabaseConfiguration();
            this.SqlReportingConfiguration = new SqlReportingConfiguration();
            this.Installation = new Installation();
            this.IISConfiguration = new IISConfiguration();
        }

        /// <summary>
        /// Gets or sets the environment name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the database configuration.
        /// </summary>
        public DatabaseConfiguration DatabaseConfiguration { get; set; }

        /// <summary>
        /// Gets or sets the sql reporting configuration.
        /// </summary>
        public SqlReportingConfiguration SqlReportingConfiguration { get; set; }

        /// <summary>
        /// Gets or sets the installation.
        /// </summary>
        public Installation Installation { get; set; }

        /// <summary>
        /// Gets or sets the iis configuration.
        /// </summary>
        // ReSharper disable once InconsistentNaming
        public IISConfiguration IISConfiguration { get; set; }
    }
}
