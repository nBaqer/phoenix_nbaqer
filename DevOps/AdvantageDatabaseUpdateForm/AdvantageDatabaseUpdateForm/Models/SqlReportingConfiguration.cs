﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SqlReportingConfiguration.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the SqlReportingConfiguration type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AdvantageDatabaseUpdateForm.Models
{
    /// <summary>
    /// The sql reporting configuration.
    /// </summary>
    public class SqlReportingConfiguration
    {
        /// <summary>
        /// Gets or sets the report server url.
        /// </summary>
        public string ReportServerUrl { get; set; }

        /// <summary>
        /// Gets or sets the report service url.
        /// </summary>
        public string ReportServiceUrl { get; set; }

        /// <summary>
        /// Gets or sets the report execution service url.
        /// </summary>
        public string ReportExecutionServiceUrl { get; set; }

        /// <summary>
        /// Gets or sets the report server folder.
        /// </summary>
        public string ReportServerFolder { get; set; }
    }
}
