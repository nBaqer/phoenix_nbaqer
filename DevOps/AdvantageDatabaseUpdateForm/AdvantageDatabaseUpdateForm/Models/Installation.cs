﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Installation.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The installation.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AdvantageDatabaseUpdateForm.Models
{
    /// <summary>
    /// The installation.
    /// </summary>
    public class Installation
    {
        /// <summary>
        /// Gets or sets the database backup location.
        /// </summary>
        public string DatabaseBackupLocation { get; set; }

        /// <summary>
        /// Gets or sets the site backup location.
        /// </summary>
        public string SiteBackupLocation { get; set; }

        /// <summary>
        /// Gets or sets the database installation path.
        /// </summary>
        public string DatabaseInstallationPath { get; set; }

        /// <summary>
        /// Gets or sets the database log path.
        /// </summary>
        public string DatabaseLogPath { get; set; }

        /// <summary>
        /// Gets or sets the release package location.
        /// </summary>
        public string ReleasePackageLocation { get; set; }
    }
}
