﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="VersionHistory.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the VersionHistory type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AdvantageDatabaseUpdateForm.Models
{
    using System;
    using System.Collections.Generic;

    using AdvantageDatabaseUpdateForm.Data;

    /// <summary>
    /// The version history.
    /// </summary>
    public class Version
    {
        /// <summary>
        /// The script status.
        /// </summary>
        public enum EScriptStatus
        {
            /// <summary>
            /// The executed.
            /// </summary>
            Executed = 1,

            /// <summary>
            /// The pending.
            /// </summary>
            Pending = 0,

            /// <summary>
            /// The executing.
            /// </summary>
            Executing = 2
        }

        /// <summary>
        /// The e database.
        /// </summary>
        public enum EDatabase
        {
            /// <summary>
            /// The tenant auth database.
            /// </summary>
            TenantAuthDatabase = 0,

            /// <summary>
            /// The advantage database.
            /// </summary>
            AdvantageDatabase = 1
        }

        /// <summary>
        /// Gets or sets the status.
        /// </summary>
        [DataTableExtensions.IgnoreAttribute]
        public EScriptStatus Status { get; set; }

        /// <summary>
        /// Gets or sets the database.
        /// The database in which the script will be executed. 1 for the TenantAuthDb or 2 for the Advantage Database.
        /// </summary>
        [DataTableExtensions.IgnoreAttribute]
        public EDatabase Database { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the major.
        /// </summary>
        public int Major { get; set; }

        /// <summary>
        /// Gets or sets the minor.
        /// </summary>
        public int Minor { get; set; }

        /// <summary>
        /// Gets or sets the build.
        /// </summary>
        public int Build { get; set; }

        /// <summary>
        /// Gets or sets the revision.
        /// </summary>
        public int Revision { get; set; }

        /// <summary>
        /// Gets or sets the client id.
        /// The Advantage API Key that's unique per tenant and exist inside the app syConfigAppSettingsValues where the App Setting Key Name is AdvantageServiceAuthenticationKey
        /// If this is provided in the Database Version Manifest, the script will only be run on the database that contains that key.
        /// </summary>
        [DataTableExtensions.IgnoreAttribute]
        public Guid? ClientId { get; set; }

        /// <summary>
        /// Gets or sets the client specific versions.
        /// </summary>
        [DataTableExtensions.IgnoreAttribute]
        public List<Version> ClientSpecificVersions { get; set; }

        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the version begin.
        /// </summary>
        public DateTime? VersionBegin { get; set; }

        /// <summary>
        /// Gets or sets the version end.
        /// </summary>
        public DateTime? VersionEnd { get; set; }
    }
}
