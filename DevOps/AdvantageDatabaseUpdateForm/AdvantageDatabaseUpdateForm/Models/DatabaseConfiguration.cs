﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DatabaseConfiguration.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The database configuration.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AdvantageDatabaseUpdateForm.Models
{
    /// <summary>
    /// The database configuration.
    /// </summary>
    public class DatabaseConfiguration
    {
        /// <summary>
        /// Gets or sets the tenant auth database name.
        /// </summary>
        public string TenantAuthDatabaseName { get; set; }

        /// <summary>
        /// Gets or sets the tenant auth server name.
        /// </summary>
        public string TenantAuthServerName { get; set; }

        /// <summary>
        /// Gets or sets the tenant auth user name.
        /// </summary>
        public string TenantAuthUserName { get; set; }

        /// <summary>
        /// Gets or sets the tenant auth database password.
        /// </summary>
        public string TenantAuthDatabasePassword { get; set; }
    }
}
