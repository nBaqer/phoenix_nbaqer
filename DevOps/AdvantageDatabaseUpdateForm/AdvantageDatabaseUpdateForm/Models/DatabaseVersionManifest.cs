﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DatabaseVersionManifest.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the VersionHistory type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AdvantageDatabaseUpdateForm.Models
{
    using System.Collections.Generic;

    /// <summary>
    /// The version history.
    /// </summary>
    public class DatabaseVersionManifest
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DatabaseVersionManifest"/> class.
        /// </summary>
        public DatabaseVersionManifest()
        {
            this.Versions = new List<Version>();
        }

        /// <summary>
        /// Gets or sets the versions.
        /// </summary>
        public List<Version> Versions { get; set; }
    }
}
