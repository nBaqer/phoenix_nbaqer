﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IISApplicationConfiguration.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the IISApplicationConfiguration type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AdvantageDatabaseUpdateForm.Models
{
    /// <summary>
    /// The iis application configuration.
    /// </summary>
    // ReSharper disable once InconsistentNaming
    public class IISApplicationConfiguration
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the path.
        /// </summary>
        public string Path { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is relative.
        /// </summary>
        public bool IsRelative { get; set; }
    }
}
