﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IISConfiguration.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The iis configuration.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace AdvantageDatabaseUpdateForm.Models
{
    using System.Collections.Generic;

    /// <summary>
    /// The iis configuration.
    /// </summary>
    // ReSharper disable once InconsistentNaming
    public class IISConfiguration
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="IISConfiguration"/> class.
        /// </summary>
        public IISConfiguration()
        {
            this.ApplicationConfigurations = new List<IISApplicationConfiguration>();
        }

        /// <summary>
        /// Gets or sets the application configurations.
        /// </summary>
        public List<IISApplicationConfiguration> ApplicationConfigurations { get; set; }

        /// <summary>
        /// Gets or sets the default website name.
        /// </summary>
        public string DefaultWebsiteName { get; set; }

        /// <summary>
        /// Gets or sets the default website path.
        /// </summary>
        public string DefaultWebsitePath { get; set; }

        /// <summary>
        /// Gets or sets the product virtual directory name.
        /// </summary>
        public string ProductVirtualDirectoryName { get; set; }

        /// <summary>
        /// Gets or sets the product virtual directory path.
        /// </summary>
        public string ProductVirtualDirectoryPath { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is product virtual directory path relative.
        /// </summary>
        public bool IsProductVirtualDirectoryPathRelative { get; set; }

        /// <summary>
        /// Gets or sets the environment virtual directory name.
        /// </summary>
        public string EnvironmentVirtualDirectoryName { get; set; }

        /// <summary>
        /// Gets or sets the environment virtual directory path.
        /// </summary>
        public string EnvironmentVirtualDirectoryPath { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is environment virtual directory path relative.
        /// </summary>
        public bool IsEnvironmentVirtualDirectoryPathRelative { get; set; }

        /// <summary>
        /// Gets or sets the version virtual directory name.
        /// </summary>
        public string VersionVirtualDirectoryName { get; set; }

        /// <summary>
        /// Gets or sets the version virtual directory path.
        /// </summary>
        public string VersionVirtualDirectoryPath { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether is version virtual directory path relative.
        /// </summary>
        public bool IsVersionVirtualDirectoryPathRelative { get; set; }
    }
}
