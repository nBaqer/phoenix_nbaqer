const path = require("path");
const HtmlWebPackPlugin = require("html-webpack-plugin");
// const cssVariablesPlugin = require('postcss-css-variables');
//  const cssVariables = require('./src/app/style/cssVariables');

const htmlPlugin = new HtmlWebPackPlugin({
  template: "./src/index.html",
  filename: "./index.html"
});

const config = {
  target: "electron-renderer",
  devtool: "source-map",
  entry: "./src/app/App.tsx",
  output: {
    filename: "renderer.js",
    path: path.resolve(__dirname, "dist")
  },
  externals:{
    electron: "require('electron')",
    child_process: "require('child_process')",
    fs: "require('fs')",
    path: "require('path')",
    mssql:"require('mssql')",
    tedious:"require('tedious')",
    mkdirp:"require('mkdirp')"
    // msnodesqlv8:"require('mssql/msnodesqlv8')"
  },
  module: {
    rules: [
      {
        test: /\.(ts|tsx)$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader"
        }
      },
      {
        test: /\.css$/,
        use: ['style-loader', {loader:'typings-for-css-modules-loader',options:{
          modules:true, 
          namedExport: true,
          camelCase: true,
          sourceMap: true
        }}],
      }
      // {
      //   test: /\.css$/,
      //   // include: path.join(__dirname, 'src/components'),
      //   exclude: /node_modules/,
      //   use: [
      //     'style-loader!css-loader',
      //     {
      //       loader: 'typings-for-css-modules-loader',
      //       options: {
      //         modules: true,
      //         namedExport: true
      //       }
      //     }
      //   ]
      // }
      // ,npm
      // {
      //   loader: require.resolve('postcss-loader'),
      //   options: {
      //     ident: 'postcss',
      //     plugins: () => [
      //       cssVariablesPlugin({
      //         variables: cssVariables
      //       })
      //     ],
      //   },
      // }
    ]
  },
  resolve: {
    extensions: [".ts", ".tsx", ".js", ".css"]
  },
  node: {
    __dirname: true,
    __filename: false
  },
  plugins: [htmlPlugin]
};

module.exports = (env, argv) => {
return config;
};