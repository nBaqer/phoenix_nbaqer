const path = require("path");

const config = {
  target: "electron-main",
  devtool: "source-map",
  entry: "./src/main.ts",
  output: {
    filename: "main.js",
    path: path.resolve(__dirname, "dist")
  },
  
  externals:{
    electron: "require('electron')",
    child_process: "require('child_process')",
    fs: "require('fs')",
    path: "require('path')",
    mssql:"require('mssql')",
    tedious:"require('tedious')",
    mkdirp:"require('mkdirp')"    
    // msnodesqlv8:"require('mssql/msnodesqlv8')"
  },
  module: {
    rules: [
      {
        test: /\.(ts|tsx)$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader"
        }
      }
      ,
      {
        test: /\.css$/,
        use: ['style-loader', {loader:'typings-for-css-modules-loader',options:{
          modules:true, 
          namedExport: true,
          camelCase: true,
          sourceMap: true
        }}],
      }
      // {
      //   test: /\.css$/,
      //   // include: path.join(__dirname, '/'),
      //   exclude: /node_modules/,
      //   use: [
      //     'style-loader!css-loader',
      //     {
      //       loader: 'typings-for-css-modules-loader',
      //       options: {
      //         modules: true,
      //         namedExport: true
      //       }
      //     }
      //   ]
      // }
    ]
  },
  resolve: {
    extensions: [".ts", ".tsx", ".js", ".css"]
  },
  node: {
    __dirname: false,
    __filename: false
  }
};

module.exports = (env, argv) => {
  
  return config;
};