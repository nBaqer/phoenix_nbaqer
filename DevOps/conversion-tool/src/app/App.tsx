import * as ReactDOM from 'react-dom';
import * as React from 'react';
import Dashboard from "./components/pages/Dashboard";

ReactDOM.render(<Dashboard />, document.getElementById('app'));