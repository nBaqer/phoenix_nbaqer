import * as React from "react"
import { AppConnection } from "./models/AppConnection";

export interface WindowInterface extends Window{
    isConnectedToBridge: boolean,
    appConnection: AppConnection,
    conversionStructureFolder: string
}

export const PrepopulatedValuesFileName: string = "prepopulatedValues.local.first.json";