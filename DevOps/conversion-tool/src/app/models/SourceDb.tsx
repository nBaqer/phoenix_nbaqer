
export class SourceDb {
    client: string;
    campus: string;
    freedomDataLocation: string;
    freedomBackupName: string;
    schoolCode: number;
    freedomDbName: string;
    advantageDbName: string;
    isInitialCampus: boolean;
    useFreedomAcademicProbation: boolean;
    advantageBackupLocation: string;
    conversionStructureFolder: string;
}
