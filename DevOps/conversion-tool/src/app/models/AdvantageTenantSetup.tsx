export class AdvantageTenatSetup{
    database: string;
    server: string;
    instance: string;
    username:string;
    password: string;
    environment: number;
    site: string;
    api: string;
    service: string;
    reportService:string;
    reportExecution: string;
}