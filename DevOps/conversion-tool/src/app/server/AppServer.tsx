import { Connection, Request, TYPES } from 'tedious'
import { WindowInterface } from '../global';
import { AppConnection } from '../models/AppConnection'
import { SourceDb } from '../models/SourceDb'
import { AdvantageTenatSetup } from '../models/AdvantageTenantSetup'
import { resolve } from 'url';
import { threadId } from 'worker_threads';
// import { ConnectionPool, Request } from 'mssql/msnodesqlv8'




export default class AppServer {

    connectionString: string;
    enviroment: any;
    tempAppConnection: AppConnection;
    generalErrorMessage:string= "Oops! Something went wrong!!! To get more details on error press Ctrl + Shift + I";

    constructor(appConnection?: AppConnection) {
        if (appConnection === undefined && (window as WindowInterface).appConnection) {
            appConnection = (window as WindowInterface).appConnection;
        }
        else {
            //throw error ... can't use server if have not previously set up server or trying to set up connection first time
        }
        this.tempAppConnection = appConnection
        this.connectionString = "server=" + appConnection.host + ";Database=" + appConnection.database + ";Trusted_Connection=Yes;Driver=msnodesqlv8";
        this.enviroment = {
            server: appConnection.host,
            authentication: { type: "default", options: { userName: appConnection.user, password: appConnection.password } },
            options: {
                database: appConnection.database
            }
        }

        if (appConnection.instance !== "") {
            this.enviroment.options.instanceName = appConnection.instance;
        }

    }

    delay(ms: number) {
        return new Promise( resolve => setTimeout(resolve, ms) );
    }

    testConnection =  async ():Promise<string> => {
        const connection = await new Connection(this.enviroment);
        connection.on('debug', function (err) { console.log('debug:', err); });
        var that = this;   

        let errorCon : any;

        connection.on('error', function(err){
            errorCon = err;
        })

        connection.on('connect', function (err) {
            if (err) {
                console.log(err);
                errorCon = err;
                (window as WindowInterface).isConnectedToBridge = false;
                (window as WindowInterface).appConnection = null;
            }
            else {

                (window as WindowInterface).isConnectedToBridge = true;
                (window as WindowInterface).appConnection = that.tempAppConnection;
            }
        });
        Promise.resolve(connection)
        await this.delay(500);

        if (connection.loggedIn) return "Connection Successful!";
        if ( errorCon) return "Error " + errorCon.code + ": " + errorCon.message;

        return this.generalErrorMessage;
    }


    addReportRole(user: string, database: string) {
        this.enviroment.options.database = database;
        const connection = new Connection(this.enviroment);
        connection.on('debug', function (err) { console.log('debug:', err); });

        connection.on('connect', function (err) {
            if (err) {
                console.log(err);
                (window as WindowInterface).isConnectedToBridge = false;
            }
            else {

                (window as WindowInterface).isConnectedToBridge = true;
            }
            let upsertProcedure: string = "CreateAdvantageDbPermission";
            var request = new Request(upsertProcedure, function (err) {
                if (err) {
                    console.log(err);
                }
            });
            request.addParameter('User', TYPES.VarChar, user);
            connection.callProcedure(request);
        })
    }


    setupAdvantage(advSetup: AdvantageTenatSetup) {
        this.enviroment.options.database = advSetup.database
        const connection = new Connection(this.enviroment);
        connection.on('debug', function (err) { console.log('debug:', err); });

        connection.on('connect', function (err) {
            if (err) {
                console.log(err);
                (window as WindowInterface).isConnectedToBridge = false;
            }
            else {

                (window as WindowInterface).isConnectedToBridge = true;
            }
            let procedure: string = "SetupAdvantageTenant";
            var request = new Request(procedure, function (err) {
                if (err) {
                    console.log(err);
                }
            });
            request.addParameter('ServerName', TYPES.VarChar, advSetup.server + (advSetup.instance !== "" ? '\\' + advSetup.instance : ""));
            request.addParameter('DBUser', TYPES.VarChar, advSetup.username);
            request.addParameter('DBPassword', TYPES.VarChar, advSetup.password);
            request.addParameter('Environment', TYPES.Int, advSetup.environment);
            request.addParameter('SiteURL', TYPES.VarChar, advSetup.site);
            request.addParameter('AdvantageApiURL', TYPES.VarChar, advSetup.api);
            request.addParameter('ServiceURL', TYPES.VarChar, advSetup.service);
            request.addParameter('DBOUser', TYPES.VarChar, advSetup.username);
            request.addParameter('ReportExecutionServices', TYPES.VarChar, advSetup.reportExecution);
            request.addParameter('ReportServices', TYPES.VarChar, advSetup.reportService);
            connection.callProcedure(request);
        })
    }

    removeTenant(databaseName: string) {
        this.enviroment.options.database = 'TenantAuthDb'
        const connection = new Connection(this.enviroment);
        connection.on('debug', function (err) { console.log('debug:', err); });

        connection.on('connect', function (err) {
            if (err) {
                console.log(err);
                (window as WindowInterface).isConnectedToBridge = false;
            }
            else {

                (window as WindowInterface).isConnectedToBridge = true;
            }
            let procedure: string = "removeTenantbyName";
            var request = new Request(procedure, function (err) {
                if (err) {
                    console.log(err);
                }
            });

            request.addParameter('TenantName', TYPES.VarChar, databaseName);
            connection.callProcedure(request);
        })
    }

    upsertSourceDbRecord = async (sourceDb: SourceDb) => {
        this.enviroment.options.database = 'FreedomBridge'
        const connection = new Connection(this.enviroment);
        connection.on('debug', function (err) { console.log('debug:', err); });

        connection.on('connect', function (err) {
            if (err) {
                console.log(err);
                (window as WindowInterface).isConnectedToBridge = false;
            }
            else {

                (window as WindowInterface).isConnectedToBridge = true;
            }
            let upsertProcedure: string = "bridge.usp_UpsertSchoolInformation";
            var request = new Request(upsertProcedure, function (err) {
                if (err) {
                    console.log(err);
                }
            });

            request.addParameter('InitialCampus', TYPES.Bit, sourceDb.isInitialCampus);
            request.addParameter('FreedomDbName', TYPES.VarChar, sourceDb.freedomDbName);
            request.addParameter('BasePath', TYPES.VarChar, sourceDb.freedomDataLocation);
            request.addParameter('Backupname', TYPES.VarChar, sourceDb.freedomBackupName);
            request.addParameter('SchoolName', TYPES.VarChar, sourceDb.client);
            request.addParameter('CampusName', TYPES.VarChar, sourceDb.campus);
            request.addParameter('SchoolCode', TYPES.Int, sourceDb.schoolCode);
            request.addParameter('AdvantageDbName', TYPES.VarChar, sourceDb.advantageDbName);
            request.addParameter('UseAcademicProbationFromFreedom', TYPES.Bit, sourceDb.useFreedomAcademicProbation);
            request.addParameter('AdvantageBackupLocation', TYPES.VarChar, sourceDb.advantageBackupLocation);
            request.addParameter('ConversionStructureFolder', TYPES.VarChar, sourceDb.conversionStructureFolder);
            connection.callProcedure(request);
        })


    }

    restoreBackUp = async (path: string, dbName: string):Promise<string> => {
        this.enviroment.options.database = 'master'
        const connection = new Connection(this.enviroment);
        connection.on('debug', function (err) { console.log('debug:', err); });
        let errorReq : any;
        connection.on('connect', function (err) {
            if (err) {
                console.log(err);
                (window as WindowInterface).isConnectedToBridge = false;
            }
            else {

                (window as WindowInterface).isConnectedToBridge = true;
            }

            let restoreDbQuery: string = "IF EXISTS ( SELECT TOP 1 1 FROM sys.sysdatabases WHERE name = '" + dbName + "')" +
                " BEGIN " +
                " ALTER DATABASE " + dbName +
                " SET SINGLE_USER" +
                "  WITH ROLLBACK IMMEDIATE;" +
                " END " +
                " RESTORE DATABASE " + dbName +
                " FROM DISK = N'" + path + "' " +
                "  WITH FILE = 1 " +
                " ,MOVE N'" + dbName + "' " +
                " TO N'" + (window as WindowInterface).conversionStructureFolder + '\\' + dbName + ".mdf'" +
                " ,MOVE N'" + dbName + "_log' " +
                " TO N'" + (window as WindowInterface).conversionStructureFolder + '\\' + dbName + ".ldf'" +
                "  ,NOUNLOAD" +
                "  ,REPLACE" +
                "  ,STATS = 5;" +
                "  ALTER DATABASE " + dbName +
                " SET MULTI_USER;"
            "  GO       ";


            var request = new Request(restoreDbQuery, function (err) {
                if (err) {
                    console.log(restoreDbQuery)
                    console.log(err);
                    errorReq = err;
                }
            });

            connection.execSql(request);
        })
        Promise.resolve(connection)
        await this.delay(500);

        if (connection.loggedIn && errorReq === undefined) return "Restore Successful!";
        if ( errorReq) return "Error " + errorReq.code + ": " + errorReq.message;

        return this.generalErrorMessage;
    }

    createBackup = async (path: string, dbName: string) => {
        const connection = new Connection(this.enviroment);
        connection.on('debug', function (err) { console.log('debug:', err); });

        connection.on('connect', function (err) {
            if (err) {
                console.log(err);
                (window as WindowInterface).isConnectedToBridge = false;
            }
            else {

                (window as WindowInterface).isConnectedToBridge = true;
            }
            let now: Date = new Date();
            let finalPath: string = path + dbName + "_" + (now.getMonth() + 1) + "_" + (now.getDate()) + "_" + now.getHours() + "_" + now.getMinutes() + "_" + now.getSeconds() + ".bak";

            let restoreDbQuery: string = "BACKUP DATABASE " + dbName +
                " TO  DISK = N'" + finalPath + "'" +
                " WITH NOFORMAT" +
                " ,INIT" +
                " ,NAME = N'" + dbName + "'" +
                " ,SKIP" +
                " ,NOREWIND" +
                " ,NOUNLOAD" +
                " ,STATS = 10;" +
                "         ";


            var request = new Request(restoreDbQuery, function (err) {
                if (err) {
                    console.log(err);
                }
            });

            connection.execSql(request);
        });


    }
}

