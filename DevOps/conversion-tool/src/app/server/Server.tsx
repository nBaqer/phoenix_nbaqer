import { Connection, Request, TYPES } from 'tedious'
import { WindowInterface } from '../global';
import { AppConnection } from '../models/AppConnection'
import {SourceDb} from '../models/SourceDb'
// import { ConnectionPool, Request } from 'mssql/msnodesqlv8'




export default class Server {
    connectionString: string;
    enviroment: any;



    constructor(appConnection?: AppConnection) {

        if (appConnection === undefined && (window as WindowInterface).appConnection ){
            appConnection  = (window as WindowInterface).appConnection;
        }
        else{
            //throw error ... can't use server if have not previously set up server or trying to set up connection first time
        }
        this.connectionString = "server=" + appConnection.host + ";Database=" + appConnection.database + ";Trusted_Connection=Yes;Driver=msnodesqlv8";
            this.enviroment = {
                server: appConnection.host,
                authentication: { type: "default", options: { userName: appConnection.user, password: appConnection.password } },
                options: {
                    database: appConnection.database,
                    instanceName: appConnection.instance,
                }
            }
    }


    testConnection = async () => {
        const connection = new Connection(this.enviroment);
        connection.on('debug', function (err) { console.log('debug:', err); });

        connection.on('connect', function (err) {
            if (err) {
                console.log(err);
                (window as WindowInterface).isConnectedToBridge = false;
                (window as WindowInterface).appConnection = null;
            }
            else {

                (window as WindowInterface).isConnectedToBridge = true;
                (window as WindowInterface).appConnection = null;
            }

            // console.log((window as WindowInterface).isConnectedToBridge)
            // // If no error, then good to proceed.  
            // console.log("Connected");
            // var request = new Request("SELECT * FROM stage.PreConversionResults ", function (err) {
            //     if (err) {
            //         console.log(err);
            //     }
            // });

            // var result = "";
            // request.on('row', function (columns) {
            //     columns.forEach(function (column) {
            //         if (column.value === null) {
            //             console.log('NULL');
            //         } else {
            //             result += column.value + " ";
            //         }
            //     });
            //     console.log(result);
            //     result = "";
            // });

            // request.on('done', function (rowCount, more) {
            //     console.log(rowCount + ' rows returned');
            // });
            // connection.execSql(request);

        });
        // await pool.connect();

        // const result = await pool.request().query("SELECT * FROM stage.PreConversionResults ")
        // console.log(result);
        // pool.close();
    }

    upsertSourceDbRecord = async (sourceDb: SourceDb) => {

    }

    createBackup = async (path: string, dbName: string) => {
        const connection = new Connection(this.enviroment);
        connection.on('debug', function (err) { console.log('debug:', err); });

        connection.on('connect', function (err) {
            if (err) {
                console.log(err);
                (window as WindowInterface).isConnectedToBridge = false;
            }
            else {

                (window as WindowInterface).isConnectedToBridge = true;
            }
            let now: Date = new Date();
            let finalPath: string = path + dbName + "_" + (now.getMonth() + 1) + "_" + (now.getDate()) + "_" + now.getHours() + "_" + now.getMinutes() + "_" + now.getSeconds() + ".bak";

            let restoreDbQuery: string = "BACKUP DATABASE " + dbName +
                " TO  DISK = N'" + finalPath + "'" +
                " WITH NOFORMAT" +
                " ,INIT" +
                " ,NAME = N'" + dbName + "'" +
                " ,SKIP" +
                " ,NOREWIND" +
                " ,NOUNLOAD" +
                " ,STATS = 10;" +
                "         ";


            var request = new Request(restoreDbQuery, function (err) {
                if (err) {
                    console.log(err);
                }
            });

            connection.execSql(request);
        });


    }
}

