import React, { Component } from 'react'
import PropTypes, { string } from 'prop-types'
import * as InputLabelStyle from './InputLabel.css'

interface InputLabelProps{
    text: string;
}

export default class InputLabel extends Component<InputLabelProps> {

    static propTypes = {
        text: string
    }
   
    render() {
        return (
            <div className={InputLabelStyle.inputLabelDiv}>
                <label className={InputLabelStyle.inputLabel}>{this.props.text}</label>
            </div>
        )
    }
}
