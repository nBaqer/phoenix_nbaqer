import React, { Component } from 'react'
import { InputProps } from './pages/interface/InputProps';

export default class InputCheckbox extends Component<InputProps> {
    render() {
        return (
            <input type="checkbox" value={this.props.value} checked={this.props.boolValue} onChange={this.props.onChange}/>
        )
    }
}
