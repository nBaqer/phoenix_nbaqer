import React, { Component } from 'react'
import PropTypes, { string, any, func } from 'prop-types'
import * as GlobalStyle from './pages/global.css'
import { InputProps } from './pages/interface/InputProps';



export default class InputText extends Component<InputProps> {
    static propTypes = {
        text: string,
        id: string,
        onChange: func
    }

    state = {
        focus: false
    }

    render() {
        return (
            <input className={GlobalStyle.inputText} type="text" value={this.props.value} id={this.props.id} onChange={this.props.onChange}   />
        )
    }
}


