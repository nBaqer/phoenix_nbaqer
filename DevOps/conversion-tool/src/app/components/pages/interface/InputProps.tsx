

export interface InputProps {
    value?: string;
    boolValue?: boolean;
    label?: string;
    id: string;
    readonly?: boolean;
    onChange(e): void
}
