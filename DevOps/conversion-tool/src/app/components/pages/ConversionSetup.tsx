import React, { Component } from 'react'
import { DashboardProps } from './interface/DashboardProps';
import { rootPath } from 'electron-root-path'
import isDev from 'electron-is-dev'
import * as GlobalStyle from './global.css'
import AppServer from '../../server/AppServer';
import fs from 'fs'
import mkdirp from 'mkdirp'
import InputLabel from '../InputLabel'
import InputText from '../InputText'
import { WindowInterface, PrepopulatedValuesFileName } from '../../global';
import axios from 'axios'
import { exec } from 'child_process';
import DialogBox from '../DialogBox';

export default class ConversionSetup extends Component<DashboardProps> {

    _permissionModalPopup : React.RefObject<DialogBox> = React.createRef();
    _structureModalPopup : React.RefObject<DialogBox> = React.createRef();
    _ssisConfigInitialModalPopup : React.RefObject<DialogBox> = React.createRef();
    _ssisConfigConnectionModalPopup : React.RefObject<DialogBox> = React.createRef();
    _restoreBridgeModalPopup : React.RefObject<DialogBox> = React.createRef();
    constructor(props){
        super(props);
        this._permissionModalPopup = React.createRef();
        this._structureModalPopup = React.createRef();
        this._ssisConfigInitialModalPopup = React.createRef();
        this._ssisConfigConnectionModalPopup = React.createRef();
        this._restoreBridgeModalPopup = React.createRef();
    }

    state = {

        conversionStructureFolderId: "conversionStructureFolder",
        conversionStructureFolderLabel: "Conversion Structure Path",
        conversionStructureFolderInput: "C:\\SSIS\\DB\\FreedomBridge",

        serverId: "server",
        serverLabel: "Server",
        serverInput: "LocalHost\\sqldev2016",
    }

    getVisibilityStyle = () => {

        if (this.props.hide === true)
            return {
                display: "none"
            }
        else
            return {
                display: "block"
            }
    }


    componentDidMount() {
        if (isDev === false) {
            console.log('reading values from json')
            let jsonPath: string = rootPath + "\\resources\\app\\extraResources\\" + PrepopulatedValuesFileName;
            axios.get(jsonPath).then(response => {
                this.setState({
                    conversionStructureFolderInput: response.data.conversionSetup.conversionStructureFolder,
                    serverInput: response.data.conversionSetup.server
                })
            })

        }
    }

    inputOnChange = (event) => {
        let stateInputProp: string = event.target.id + 'Input';
        this.setState({
            [stateInputProp]: event.target.value
        })
    }

    setPermissions = () => {
        let that = this;
        var ps = require('child_process')
        var spawn = ps.spawn;
        var ls = exec("C:\\Windows\\System32\\Icacls " + rootPath + " /grant \"authenticated users\":F", function (error, stdout, stderro) {

            if (error){
                console.log(error)
                that._permissionModalPopup.current.openModal("Error giving permissions: "  + error);
            }
            console.log(stdout);
            that._permissionModalPopup.current.openModal("Permissions Given: " + stdout);
        })


    }

    restoreFreedomBridgeDatabase = () => {
        let middlePath: string = isDev ? "\\extraResources\\" : "\\resources\\app\\extraResources\\";
        let advantageBackupLocation: string = rootPath + middlePath + "FreedomBridge.bak"
let that = this;
        let server: AppServer = new AppServer()
        server.restoreBackUp(advantageBackupLocation, 'FreedomBridge').then((message) => {
            that._restoreBridgeModalPopup.current.openModal(message);
        });
    }

    copySSISConfigurations = () => {
        let that = this;
        let destinationPath = this.state.conversionStructureFolderInput.replace("\\DB\\FreedomBridge", "\\Freedom\\Conf");
        let middlePath: string = rootPath + (isDev ? "\\extraResources\\" : "\\resources\\app\\extraResources\\");
        let connectionFrom: string = middlePath + '\\connection.dtsConfig';
        let initialFrom: string = middlePath + "\\initial.dtsConfig";
        let connectionTo: string = destinationPath + '\\connection.dtsConfig';
        let initialTo: string = destinationPath + "\\initial.dtsConfig";
        fs.copyFile(connectionFrom, connectionTo, (err) => {
            if (err) {
                console.log('error copying Connection config')
                that._ssisConfigConnectionModalPopup.current.openModal("Error copying "+connectionTo+": "  + err);
            }
            console.log('Connection config copied')
            var data = fs.readFileSync(connectionTo).toString();
            data = data.replace(/localhost\\sqldev2016/g, this.state.serverInput);
            fs.writeFile(connectionTo, data, function (err) {
                if (err) {
                    console.log('Error copying Connection config')
                    that._ssisConfigConnectionModalPopup.current.openModal("Error replacing values on  connection.dtsConfig: "  + err);
                }
                console.log('Connection config modified')
                that._ssisConfigConnectionModalPopup.current.openModal("Connection.dtsConfig copied and modified");
            })
        })


        fs.copyFile(initialFrom, initialTo, (err) => {
            if (err) {
                console.log('error copying initial config')
                that._ssisConfigInitialModalPopup.current.openModal("Error copying "+initialTo+": "  + err);
            }
            console.log('Inital config copied')
            console.log(this.state.serverInput);
            var data = fs.readFileSync(initialTo).toString();

            data = data.replace(/localhost\\sqldev2016/g, this.state.serverInput);
            console.log(data);
            fs.writeFile(initialTo, data, function (err) {
                if (err) {
                    console.log('Error copying inital config')
                    that._ssisConfigInitialModalPopup.current.openModal("Error replacing values on  initial.dtsConfig: "  + err);
                }
                console.log('Initial config modified')
                that._ssisConfigInitialModalPopup.current.openModal("Initial.dtsConfig copied and modified");
            })
        })
    }

    createConversionStructure = () => {
        let that = this;
        mkdirp(this.state.conversionStructureFolderInput, function (err) {
            if (err) {
                console.log(err)
                that._structureModalPopup.current.openModal("Error creating structure: "  + err);
            }
            (window as WindowInterface).conversionStructureFolder = that.state.conversionStructureFolderInput
            that._structureModalPopup.current.openModal("Structure created");
        });

        mkdirp(this.state.conversionStructureFolderInput.replace("\\DB\\FreedomBridge", "\\Freedom\\Conf"), function (err) {
            if (err) {
                console.log(err)
                that._structureModalPopup.current.openModal("Error creating structure: "  + err);
            }
            (window as WindowInterface).conversionStructureFolder = that.state.conversionStructureFolderInput
            that._structureModalPopup.current.openModal("Structure created");
        });
    }

    render() {
        return (
            <div style={this.getVisibilityStyle()}>
                <React.Fragment>
                    <form action="">
                        <div className={GlobalStyle.formRow}>
                            <input className={GlobalStyle.submitButton} type="button" value="Set Permissions" onClick={this.setPermissions} />
                            <DialogBox ref={this._permissionModalPopup} />
                        </div>

                        <div className={GlobalStyle.formRow}>
                            <InputLabel text={this.state.conversionStructureFolderLabel} ></InputLabel>
                            <InputText value={this.state.conversionStructureFolderInput} id={this.state.conversionStructureFolderId} onChange={this.inputOnChange} />
                           
                        </div>
                        <div className={GlobalStyle.formRow}>
                            <input className={GlobalStyle.submitButton} type="button" value="Create Structure" onClick={this.createConversionStructure} />
                            <DialogBox ref={this._structureModalPopup} />
                        </div>
                        <div className={GlobalStyle.formRow}>
                            <InputLabel text={this.state.serverLabel} ></InputLabel>
                            <InputText value={this.state.serverInput} id={this.state.serverId} onChange={this.inputOnChange} />
                        </div>
                        <div className={GlobalStyle.formRow}>
                            <input className={GlobalStyle.submitButton} type="button" value="Copy SSIS Configuration" onClick={this.copySSISConfigurations} />
                            <DialogBox ref={this._ssisConfigConnectionModalPopup} />
                            <DialogBox ref={this._ssisConfigInitialModalPopup} />
                        </div>

                        <div className={GlobalStyle.formRow}>
                            <input className={GlobalStyle.submitButton} type="button" value="Restore Freedom Bridge" onClick={this.restoreFreedomBridgeDatabase} />
                            <DialogBox ref={this._restoreBridgeModalPopup} />
                        </div>
                    </form>
                </React.Fragment>
            </div>
        )
    }
}
