import React, { Component } from 'react'
import { DashboardProps } from './interface/DashboardProps';
import InputLabel from '../InputLabel'
import InputText from '../InputText'
import InputCheckbox from '../InputCheckbox'
import * as GlobalStyle from './global.css'
import { SourceDb } from '../../models/SourceDb'
import AppServer from '../../server/AppServer'
import {rootPath} from 'electron-root-path'
import isDev from 'electron-is-dev'
import { WindowInterface, PrepopulatedValuesFileName } from '../../global'
import axios from 'axios'

export default class SchoolSetup extends Component<DashboardProps> {

   

    state = {
        clientNameId: "clientName",
        clientNameLabel: "Client",
        clientNameInput: "Paul Mitchel",

        campusNameId: "campusName",
        campusNameLabel: "Campus",
        campusNameInput: "PM_1",

        freedomDataLocationId: "freedomDataLocation",
        freedomDataLocationLabel: "Freedom Data Location",
        freedomDataLocationInput: "C:\\SSIS\\DataToConvert\\5657-Paul Michell the School-Normal",

        freeedomBackupId: "freedomBackup",
        freedomBackupLabel: "Freedom Backup File",
        freedomBackupInput: "FR_5657 _PM_724.bak",

        schoolCodeId: "schoolCode",
        schoolCodeLabel: "School Code",
        schoolCodeInput: 5657,

        isInitialCampusId: "isInitialCampus",
        isInitialCampusLabel: "Is Initial Campus?",
        isIntialCampusInput: true,

        useFreedomAcademicProbationId: "useAcademicProbation",
        useFreedomAcademicProbationLabel: "Uses Freedom Academic Probation?",
        useFreedomAcademicProbationInput: true,

        freedomDbNameId: "freedomDbName",
        freedomDbNameLabel: "Freedom Database",
        freedomDbNameInput: "PM_1",

        advantageDbNameId: "advantageDbName",
        advantageDbNameLabel: "Advantage Database",
        advantageDbNameInput: "FreedomAdvantage",

    }

    componentDidMount(){
        if ( isDev === false){
            console.log('reading values from json')
            let jsonPath : string = rootPath + "\\resources\\app\\extraResources\\" + PrepopulatedValuesFileName;
            axios.get(jsonPath).then( response => {
                this.setState({
                    clientNameInput : response.data.schoolSetup.clientName,
                    campusNameInput :response.data.schoolSetup.campus,
                    freedomDataLocationInput:response.data.schoolSetup.freedomDataLocation,
                    freedomBackupInput:response.data.schoolSetup.freedomBackup,
                    schoolCodeInput:response.data.schoolSetup.schoolCode,
                    isIntialCampusInput:response.data.schoolSetup.isInitialCampus,
                    useFreedomAcademicProbationInput:response.data.schoolSetup.useAcademicProbation,
                    freedomDbNameInput:response.data.schoolSetup.freedomDbName,
                    advantageDbNameInput:response.data.schoolSetup.advantageDbName
                })
            })
            
        }
    }

    getVisibilityStyle = () => {

        if (this.props.hide === true)
            return {
                display: "none"
            }
        else
            return {
                display: "block"
            }
    }

    inputOnChange = (event) => {
        let stateInputProp: string = event.target.id + 'Input';
        this.setState({
            [stateInputProp]: event.target.value
        })
    }

    save = () => {
        let middlePath: string = isDev ? "\\extraResources\\" : "\\resources\\app\\extraResources\\";
        let advantageBackupLocation: string = rootPath + middlePath + "AdvantageStarter.bak"
        console.log("reading adv location at: " + advantageBackupLocation);
        let sourceDb: SourceDb = {
            campus: this.state.campusNameInput,
            advantageDbName: this.state.advantageDbNameInput,
            client: this.state.clientNameInput,
            freedomBackupName: this.state.freedomBackupInput,
            freedomDbName: this.state.freedomDbNameInput,
            isInitialCampus: this.state.isIntialCampusInput,
            useFreedomAcademicProbation: this.state.useFreedomAcademicProbationInput,
            freedomDataLocation: this.state.freedomDataLocationInput,
            schoolCode: this.state.schoolCodeInput,
            advantageBackupLocation: advantageBackupLocation,
            conversionStructureFolder : (window as WindowInterface).conversionStructureFolder
        }

        let server: AppServer = new AppServer()
        server.upsertSourceDbRecord(sourceDb);
    }


    render() {
        return (
            <div style={this.getVisibilityStyle()}>
                <React.Fragment>
                    <form action="">
                        <div className={GlobalStyle.formRow}>
                            <InputLabel text={this.state.clientNameLabel} ></InputLabel>
                            <InputText value={this.state.clientNameInput} id={this.state.clientNameId} onChange={this.inputOnChange} />
                        </div>
                        <div className={GlobalStyle.formRow}>
                            <InputLabel text={this.state.campusNameLabel} ></InputLabel>
                            <InputText value={this.state.campusNameInput} id={this.state.campusNameId} onChange={this.inputOnChange} />
                        </div>
                        <div className={GlobalStyle.formRow}>
                            <InputLabel text={this.state.freedomDataLocationLabel} ></InputLabel>
                            <InputText value={this.state.freedomDataLocationInput} id={this.state.freedomDataLocationId} onChange={this.inputOnChange} />
                        </div>
                        <div className={GlobalStyle.formRow}>
                            <InputLabel text={this.state.freedomBackupLabel} ></InputLabel>
                            <InputText value={this.state.freedomBackupInput} id={this.state.freeedomBackupId} onChange={this.inputOnChange} />
                        </div>
                        <div className={GlobalStyle.formRow}>
                            <InputLabel text={this.state.schoolCodeLabel} ></InputLabel>
                            <InputText value={this.state.schoolCodeInput.toString()} id={this.state.schoolCodeId} onChange={this.inputOnChange} />
                        </div>
                        <div className={GlobalStyle.formRow}>
                            <InputLabel text={this.state.freedomDbNameLabel} ></InputLabel>
                            <InputText value={this.state.freedomDbNameInput} id={this.state.freedomDbNameId} onChange={this.inputOnChange} />
                        </div>
                        <div className={GlobalStyle.formRow}>
                            <InputCheckbox boolValue={this.state.useFreedomAcademicProbationInput} id={this.state.useFreedomAcademicProbationId} onChange={this.inputOnChange} value={this.state.useFreedomAcademicProbationInput.toString()}/>
                            <InputLabel text={this.state.useFreedomAcademicProbationLabel} ></InputLabel>
                        </div>
                        <div className={GlobalStyle.formRow}>
                            <InputCheckbox boolValue={this.state.isIntialCampusInput} id={this.state.isInitialCampusId} onChange={this.inputOnChange} value={this.state.isIntialCampusInput.toString()}/>
                            <InputLabel text={this.state.isInitialCampusLabel} ></InputLabel>
                        </div>
                        <div className={GlobalStyle.formRow}>
                            <InputLabel text={this.state.advantageDbNameLabel} ></InputLabel>
                            <InputText value={this.state.advantageDbNameInput} id={this.state.advantageDbNameId} onChange={this.inputOnChange} />
                        </div>
                        <div className={GlobalStyle.formRow}>
                            <input className={GlobalStyle.submitButton} type="button" value="Save" onClick={this.save} />
                        </div>
                    </form>
                </React.Fragment>
            </div>
        )
    }
}
