import React, { Component } from 'react'
import { DashboardProps } from './interface/DashboardProps';
import InputLabel from '../InputLabel'
import InputText from '../InputText'
import InputTextArea from '../InputTextArea'
import * as GlobalStyle from './global.css'
import { rootPath } from 'electron-root-path'
import isDev from 'electron-is-dev'
import axios from 'axios'
import { PrepopulatedValuesFileName } from '../../global';

export default class Execute extends Component<DashboardProps> {


  state = {
    ssisPathExecutableId: "ssisPathExecutable",
    ssisPathExecutableLabel: "SSIS Executable",
    ssisPathExecutableInput: "C:\\Program Files\\Microsoft SQL Server\\140\\DTS\\Binn\\DTExec.exe",

    executionOutputId: "executionOutput",
    executionOutputLabel: "Execution Output",
    executionOutputInput: "",
    executionOutputReadonly: true
  }

  componentDidMount() {
    if (isDev === false) {
      console.log('reading values from json')
      let jsonPath: string = rootPath + "\\resources\\app\\extraResources\\" + PrepopulatedValuesFileName;
      axios.get(jsonPath).then(response => {
        this.setState({
          ssisPathExecutableInput: response.data.execute.ssisPathExecutable
        })
      })

    }
  }

  executeButtonOnClick = (e) => {
    let that = this;
    var ps = require('child_process')
    var spawn = ps.spawn;
    let middlePath: string = isDev ? "\\extraResources\\" : "\\resources\\app\\extraResources\\";
    let isPackDirectory: string = rootPath + middlePath + "FreedomBridge.ispac"
    var parameters = ["/proj \"" + isPackDirectory + "\" /pack \"Initial.dtsx\" "];
    var ls = spawn(this.state.ssisPathExecutableInput, parameters)
    ls.stdout.on('data', function (data) {

      that.setState({ executionOutputInput: data.toString() })
      console.log('stdout: ' + data.toString());
    });

    ls.stderr.on('data', function (data) {
      console.log('stderr: ' + data.toString());
    });

    ls.on('exit', function (code) {
      console.log('child process exited with code ' + code.toString());
    });
  }

  getVisibilityStyle = () => {

    if (this.props.hide === true)
      return {
        display: "none"
      }
    else
      return {
        display: "block"

      }
  }

  inputOnChange = (event) => {
    let stateInputProp: string = event.target.id + 'Input';
    this.setState({
      [stateInputProp]: event.target.value
    })
  }


  render() {
    return (
      <div style={this.getVisibilityStyle()}>
        <React.Fragment>
          <section>
            <div className={GlobalStyle.formRow}>
              <InputLabel text={this.state.ssisPathExecutableLabel}></InputLabel>
              <InputText value={this.state.ssisPathExecutableInput} id={this.state.ssisPathExecutableId} onChange={this.inputOnChange} />
            </div>
            <div className={GlobalStyle.formRow}>
              <input className={GlobalStyle.submitButton} type="button" onClick={this.executeButtonOnClick} value="Execute" />
            </div>
            <div className={GlobalStyle.formRow}>
              <InputTextArea value={this.state.executionOutputInput} onChange={this.inputOnChange} id={this.state.executionOutputId} readonly={this.state.executionOutputReadonly} />
            </div>
          </section>
        </React.Fragment>
      </div>
    )
  }
}