export const formRow: string;
export const submitButton: string;
export const inputText: string;
export const inputTextArea: string;
