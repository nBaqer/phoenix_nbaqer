import React, { Component } from 'react'
import { DashboardProps } from './interface/DashboardProps';

export default class Welcome extends Component<DashboardProps> {

    getVisibilityStyle = () => {

        if (this.props.hide === true )
        return {
          display:"none"
        }
        else
        return {
          display:"block"
        }
      }

    render() {
        return (
            <div style={this.getVisibilityStyle()}>

            <React.Fragment>
            <section>
                <h1>Conversion Summary</h1>
                <div>
                    <p>Summary For Conversion</p>
                </div>
            </section>
            </React.Fragment>
            </div>
        )
    }
}

