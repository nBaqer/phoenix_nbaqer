import React, { Component } from 'react'
import { DashboardProps } from './interface/DashboardProps';

import InputLabel from '../InputLabel'
import InputText from '../InputText'
import * as GlobalStyle from './global.css'
import AppServer from '../../server/AppServer'
import isDev from 'electron-is-dev'
import axios from 'axios'
import {rootPath} from 'electron-root-path'
import  {AdvantageTenatSetup}  from '../../models/AdvantageTenantSetup'
import { PrepopulatedValuesFileName } from '../../global';
import DialogBox from '../DialogBox';
import fs from 'fs'

export default class AdvantageSetup extends Component<DashboardProps> {

    _ModalPopup : React.RefObject<DialogBox> = React.createRef();
    constructor(props){
        super(props);
        this._ModalPopup = React.createRef();
    }
    state = {
        databaseNameId: "databaseName",
        databaseNameLabel: "Database",
        databaseNameInputText: "FreedomAdvantage",

        serverNameId: "serverName",
        serverNameLabel: "Server",
        serverNameInputText: "Localhost",

        instanceId: "instanceName",
        instanceNameLabel: "Instance",
        instanceNameInputText: "SQLDEV2016",

        usernameId: "username",
        usernameLabel: "Username",
        usernameInputText: "testmtiuser",

        passwordId: "password",
        passwordLabel: "Password",
        passwordInputText: "Emphasys2015",

        environmentId: "environment",
        environmentLabel: "Environment",
        environmentInputText: 11018,

        siteUrlId: "siteUrl",
        siteUrlLabel: "Site Url",
        siteUrlInputText: "http://localhost/advantage/current/site/",

        apiUrlId: "apiUrl",
        apiUrlLabel: "API Url",
        apiUrlInputText: "http://localhost/advantage/current/API/API",

        serviceUrlId: "serviceUrl",
        serviceUrlLabel: "Service Url",
        serviceUrlInputText: "http://localhost/advantage/current/services/",

        reportExecutionId: "reportExecution",
        reportExecutionLabel: "Report Execution Services",
        reportExecutionInputText: "http://localhost/Reportserver_SQLDEV2016/ReportService2005.asmx",

        reportServiceUrlId: "reportServiceUrl",
        reportServiceUrlLabel: "Report Services",
        reportServiceUrlInputText: "http://localhost/Reportserver_SQLDEV2016/ReportExecution2005.asmx",

        userRoleReportId: "userRoleReport",
        userRoleReportLabel: "User Role Report",
        userRoleReportInputText: "NT AUTHORITY\\NETWORK SERVICE",

    }

    componentDidMount(){
        if ( isDev === false){
            console.log('reading values from json')
            let jsonPath : string = rootPath + "\\resources\\app\\extraResources\\" + PrepopulatedValuesFileName;
            axios.get(jsonPath).then( response => {
                this.setState({
                    databaseNameInputText : response.data.advantageSetup.databaseName,
                    serverNameInputText :response.data.advantageSetup.serverName,
                    usernameInputText:response.data.advantageSetup.username,
                    passwordInputText:response.data.advantageSetup.password,
                    instanceNameInputText:response.data.advantageSetup.instanceName,
                    environmentInputText : response.data.advantageSetup.environment,
                    siteUrlInputText :response.data.advantageSetup.siteUrl,
                    apiUrlInputText:response.data.advantageSetup.apiUrl,
                    serviceUrlInputText:response.data.advantageSetup.serviceUrl,
                    reportExecutionInputText:response.data.advantageSetup.reportExecution,
                    reportServiceUrlInputText:response.data.advantageSetup.reportServiceUrl,
                    userRoleReportInputText:response.data.advantageSetup.userRoleReport
                })
            })
            
        }
    }

    getVisibilityStyle = () => {

        if (this.props.hide === true)
            return {
                display: "none"
            }
        else
            return {
                display: "block"
            }
    }

    inputOnChange = (event) => {
        let stateInputProp: string = event.target.id + 'InputText';
        this.setState({
            [stateInputProp]: event.target.value
        })
    }

    removeTenant = () => {   
        let server: AppServer = new AppServer()
        server.removeTenant(this.state.databaseNameInputText);  
    }

    setupAdvantage = () => { 
        let server: AppServer = new AppServer()
        let setup: AdvantageTenatSetup = new AdvantageTenatSetup()
        setup.database = this.state.databaseNameInputText
        setup.server = this.state.serverNameInputText
        setup.instance = this.state.instanceNameInputText
        setup.username = this.state.usernameInputText
        setup.password = this.state.passwordInputText
        setup.environment = this.state.environmentInputText
        setup.site = this.state.siteUrlInputText
        setup.api = this.state.apiUrlInputText
        setup.service = this.state.serviceUrlInputText
        setup.reportExecution = this.state.reportExecutionInputText
        setup.reportService = this.state.reportServiceUrlInputText


        
        server.setupAdvantage(setup);    
    }

    addReportRole = () => {  
        let server: AppServer = new AppServer()
        server.addReportRole( this.state.userRoleReportInputText, this.state.databaseNameInputText);   
    }

    resetLocalIIS = () => {
        let that = this;
        var ps = require('child_process')
        var spawn = ps.spawn;
        var ls = ps.exec("C:\\Windows\\System32\\iisreset /restart", function (error, stdout, stderro) {

            if (error){
                console.log(error)
                that._ModalPopup.current.openModal("Error resetting IIS: "  + error);
            }
            console.log(stdout);
            that._ModalPopup.current.openModal("IIS reseted locally: " + stdout);
        })
    }


    render() {
        return (
            <div style={this.getVisibilityStyle()}>
                <React.Fragment>
                    <section>
                    <div className={GlobalStyle.formRow}>
                                <InputLabel text={this.state.databaseNameLabel} ></InputLabel>
                                <InputText value={this.state.databaseNameInputText} id={this.state.databaseNameId} onChange={this.inputOnChange} />
                            </div>
                            <div className={GlobalStyle.formRow}>
                                <InputLabel text={this.state.serverNameLabel} ></InputLabel>
                                <InputText value={this.state.serverNameInputText} id={this.state.serverNameId} onChange={this.inputOnChange} />
                            </div>
                            <div className={GlobalStyle.formRow}>
                                <InputLabel text={this.state.instanceNameLabel} ></InputLabel>
                                <InputText value={this.state.instanceNameInputText} id={this.state.instanceId} onChange={this.inputOnChange} />
                            </div>
                            <div className={GlobalStyle.formRow}>
                                <InputLabel text={this.state.usernameLabel} ></InputLabel>
                                <InputText value={this.state.usernameInputText} id={this.state.usernameId} onChange={this.inputOnChange} />
                            </div>
                            <div className={GlobalStyle.formRow}>
                                <InputLabel text={this.state.passwordLabel} ></InputLabel>
                                <InputText value={this.state.passwordInputText} id={this.state.passwordId} onChange={this.inputOnChange} />
                            </div>
                            <div className={GlobalStyle.formRow}>
                                <InputLabel text={this.state.environmentLabel} ></InputLabel>
                                <InputText value={this.state.environmentInputText.toString()} id={this.state.environmentId} onChange={this.inputOnChange} />
                            </div>
                            <div className={GlobalStyle.formRow}>
                                <InputLabel text={this.state.siteUrlLabel} ></InputLabel>
                                <InputText value={this.state.siteUrlInputText} id={this.state.siteUrlId} onChange={this.inputOnChange} />
                            </div>
                            <div className={GlobalStyle.formRow}>
                                <InputLabel text={this.state.apiUrlLabel} ></InputLabel>
                                <InputText value={this.state.apiUrlInputText} id={this.state.apiUrlId} onChange={this.inputOnChange} />
                            </div>
                            <div className={GlobalStyle.formRow}>
                                <InputLabel text={this.state.serverNameLabel} ></InputLabel>
                                <InputText value={this.state.serviceUrlInputText} id={this.state.serviceUrlId} onChange={this.inputOnChange} />
                            </div>
                            <div className={GlobalStyle.formRow}>
                                <InputLabel text={this.state.reportExecutionLabel} ></InputLabel>
                                <InputText value={this.state.reportExecutionInputText} id={this.state.reportExecutionId} onChange={this.inputOnChange} />
                            </div>                            
                            <div className={GlobalStyle.formRow}>
                                <InputLabel text={this.state.reportServiceUrlLabel} ></InputLabel>
                                <InputText value={this.state.reportServiceUrlInputText} id={this.state.reportServiceUrlId} onChange={this.inputOnChange} />
                            </div>
                            <div className={GlobalStyle.formRow}>
                                <InputLabel text={this.state.userRoleReportLabel} ></InputLabel>
                                <InputText value={this.state.userRoleReportInputText} id={this.state.userRoleReportId} onChange={this.inputOnChange} />
                            </div>
                            <div className={GlobalStyle.formRow}>
                            <input className={GlobalStyle.submitButton} type="button" value="Remove Tenant" onClick={this.removeTenant} />
                            <input className={GlobalStyle.submitButton} type="button" value="Set Up" onClick={this.setupAdvantage} />
                            <input className={GlobalStyle.submitButton} type="button" value="Add Report Role" onClick={this.addReportRole} />
                            <input className={GlobalStyle.submitButton} type="button" value="Reset IIS(local)" onClick={this.resetLocalIIS} />
                            <DialogBox ref={this._ModalPopup} />
                      </div>
                    </section>
                    
                </React.Fragment>
            </div>
        )
    }
}
