export const dashboardSection: string;
export const leftSection: string;
export const rightSection: string;
export const leftContentSectionWrapper: string;
export const rightContentSectionWrapper: string;
