import React, { Component, ReactPropTypes } from 'react'
import PropTypes from 'prop-types';
import Dashboard from './Dashboard';
import { DashboardProps } from './interface/DashboardProps';
import { rootPath } from 'electron-root-path'
import InputLabel from '../InputLabel'
import InputText from '../InputText'
import * as GlobalStyle from './global.css'
import isDev from 'electron-is-dev'
import InputTextArea from '../InputTextArea';


export default class ConversionCheck extends Component<DashboardProps>{


    state = {
        ssisPathExecutableId: "ssisPathExecutable",
        ssisPathExecutableLabel: "SSIS Executable",
        ssisPathExecutableInput: "C:\\Program Files\\Microsoft SQL Server\\140\\DTS\\Binn\\DTExec.exe",

        executionOutputId: "executionOutput",
        executionOutputLabel: "Execution Output",
        executionOutputInput: "",
        executionOutputReadonly: true
    }

    conversionPrecheckButtonOnClick = (e) => {
        let that = this;
        var ps = require('child_process')
        var spawn = ps.spawn;
        let middlePath: string = isDev ? "\\extraResources\\" : "\\resources\\app\\extraResources\\";
        let isPackDirectory: string = rootPath + middlePath + "FreedomSanityCheck.ispac"
        var parameters = ["/proj \"" + isPackDirectory + "\" /pack \"SanityCheck.dtsx\" "];

        var ls = spawn(this.state.ssisPathExecutableInput, parameters)
        ls.stdout.on('data', function (data) {

            that.setState({ executionOutputInput: data.toString() })
            console.log('stdout: ' + data.toString());
        });

        ls.stderr.on('data', function (data) {
            console.log('stderr: ' + data.toString());
        });

        ls.on('exit', function (code) {
            console.log('child process exited with code ' + code.toString());
        });
    }

    inputOnChange = (event) => {
        let stateInputProp: string = event.target.id + 'Input';
        this.setState({
            [stateInputProp]: event.target.value
        })
    }

    getVisibilityStyle = () => {

        if (this.props.hide === true)
            return {
                display: "none"
            }
        else
            return {
                display: "block"
            }
    }

    render() {
        return (
            <div style={this.getVisibilityStyle()}>
                <React.Fragment>
                    <section>
                        <div className={GlobalStyle.formRow}>
                            <InputLabel text={this.state.ssisPathExecutableLabel}></InputLabel>
                            <InputText value={this.state.ssisPathExecutableInput} id={this.state.ssisPathExecutableId} onChange={this.inputOnChange} />
                        </div>
                        <div className={GlobalStyle.formRow}>
                            <input className={GlobalStyle.submitButton} type="button" onClick={this.conversionPrecheckButtonOnClick} value="Execute Pre Check" />
                        </div>
                        <div className={GlobalStyle.formRow}>
                            <InputTextArea value={this.state.executionOutputInput} onChange={this.inputOnChange} id={this.state.executionOutputId} readonly={this.state.executionOutputReadonly} />
                        </div>
                    </section>
                </React.Fragment>
            </div>
        )
    }
}



