import * as React from 'react';
import Welcome from './Welcome'
import ServerAccess from './ServerAccess'
import Exectute from './Execute'
import ConversionCheck from './ConversionCheck'
import { WindowInterface } from '../../global';
import SchoolSetup from './SchoolSetup';
import * as DashboardStyle from './Dashboard.css'
import ConversionSetup from './ConversionSetup';
import AdvantageSetup from './AdvantageSetup';

export default class Dashboard extends React.Component {

    state = {
        WelcomeHidden: false,
        ServerAccessHidden: true,
        ConversionCheckHidden: true,
        ExecuteHidden: true,
        SchoolSetupHidden: true,
        ConversionSetupHidden: true,
        AdvantageSetupHidden: true,
        WelcomeContent: "Welcome",
        ServerAccessContent: "ServerAccess",
        SchoolSetupContent: "SchoolSetup",
        ConversionSetupContent: "ConversionSetup",
        ConversionCheckContent: " ConversionCheck",
        ExecuteContent: "Execute",
        AdvantageSetupContent: "AdvantageSetup",
        ActiveLink: "Welcome"
    }

    getSectionStyle = (e) => {
        if (this.state.ActiveLink === e) {
            return aActiveStyle
        }
        var bridgeAccess = (window as WindowInterface).isConnectedToBridge;
        if (bridgeAccess === undefined && (e === this.state.ConversionCheckContent || e === this.state.ExecuteContent)) {
            return aDisabledStyle;
        }

        return aStyle
    }


    toggleContent = (e) => {
        console.log(e)
        console.log(this.state[e + "Content"])
        //sets all menus to false
        this.setState({
            WelcomeHidden: true,
            ServerAccessHidden: true,
            ConversionCheckHidden: true,
            ExecuteHidden: true,
            SchoolSetupHidden: true,
            ConversionSetupHidden: true,
            AdvantageSetupHidden: true,
            
        })
        //will set the chosen menu visible
        this.setState({
            ActiveLink: e,
            [e + "Hidden"]: e === this.state[e + "Content"] ? false : true
        })
    }


    render() {
        return (<div>
            <section id="leftsidebar" className={[DashboardStyle.dashboardSection, DashboardStyle.leftSection].join(" ")}>
                <div className={DashboardStyle.leftContentSectionWrapper} style={innerStyle}>
                    <nav>
                        <ul style={ulStyle}>
                            <li style={liStyle}>
                                <a style={this.getSectionStyle(this.state.WelcomeContent)} href="#Welcome" onClick={this.toggleContent.bind(null, this.state.WelcomeContent)} data-content={this.state.WelcomeContent}>Conversion</a>
                            </li>
                            <li style={liStyle}>
                                <a style={this.getSectionStyle(this.state.ServerAccessContent)} href="#ServerAccess" onClick={this.toggleContent.bind(null, this.state.ServerAccessContent)} data-content={this.state.ServerAccessContent}>Set up Connection</a>
                            </li>
                            <li style={liStyle}>
                                <a style={this.getSectionStyle(this.state.ConversionSetupContent)} href="#ConversionSetup" onClick={this.toggleContent.bind(null, this.state.ConversionSetupContent)} data-content={this.state.ConversionSetupContent}>Conversion Set up</a>
                            </li>
                            <li style={liStyle}>
                                <a style={this.getSectionStyle(this.state.SchoolSetupContent)} href="#SchoolSetup" onClick={this.toggleContent.bind(null, this.state.SchoolSetupContent)} data-content={this.state.SchoolSetupContent}>School Set up</a>
                            </li>
                            <li style={liStyle}>
                                <a style={this.getSectionStyle(this.state.ConversionCheckContent)} href="#ConversionCheck" onClick={this.toggleContent.bind(null, this.state.ConversionCheckContent)} data-content={this.state.ConversionCheckContent}>Conversion Check</a>
                            </li>
                            <li style={liStyle}>
                                <a style={this.getSectionStyle(this.state.ExecuteContent)} href="#Execute" onClick={this.toggleContent.bind(null, this.state.ExecuteContent)} data-content={this.state.ExecuteContent}>Execute</a>
                            </li>
                            <li style={liStyle}>
                                <a style={this.getSectionStyle(this.state.AdvantageSetupContent)} href="#AdvantageSetup" onClick={this.toggleContent.bind(null, this.state.AdvantageSetupContent)} data-content={this.state.AdvantageSetupContent}>Setup Advantage</a>
                            </li>
                        </ul>
                    </nav>
                </div>
            </section>
            <section id="rightsidebar" className={[DashboardStyle.rightSection, DashboardStyle.dashboardSection].join(" ")}>

                <div id="Wrapper" className={DashboardStyle.rightContentSectionWrapper}>
                    <Welcome hide={this.state.WelcomeHidden} />
                    <ServerAccess hide={this.state.ServerAccessHidden} />
                    <ConversionSetup hide={this.state.ConversionSetupHidden} />
                    <SchoolSetup hide={this.state.SchoolSetupHidden} />
                    <ConversionCheck hide={this.state.ConversionCheckHidden} />
                    <Exectute hide={this.state.ExecuteHidden} />
                    <AdvantageSetup hide={this.state.AdvantageSetupHidden} />
                </div>
            </section>
        </div>)
    }
};


const wrapperStyle: React.CSSProperties = {
    padding: '20px',
    background: 'rgb(190, 191, 197)',
    width: '460px',
    float: 'left',
    display: 'inline',
    height: '600px'
}

const innerStyle: React.CSSProperties = {
    alignItems: 'center',
    justifyContent: 'center'
}

const ulStyle: React.CSSProperties = {
    listStyle: 'none',
}

const liStyle: React.CSSProperties = {
    height: '30px'
}

const aStyle: React.CSSProperties = {
    color: 'rgb(190, 191, 197)',
    textDecoration: 'none',
    fontSize: '18px',
    fontWeight: 300

}

const aActiveStyle: React.CSSProperties = {
    color: 'rgb(255, 255, 255)',
    fontSize: '20px',
    textDecoration: 'underline',
    fontWeight: 600

}

const aDisabledStyle: React.CSSProperties = {
    color: 'rgb(255, 0, 0)',
    fontSize: '18px',
    fontWeight: 300
}