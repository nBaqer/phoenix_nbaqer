import React, { Component } from 'react'
import { DashboardProps } from './interface/DashboardProps';
import InputLabel from '../InputLabel'
import InputText from '../InputText'
import * as GlobalStyle from './global.css'
import AppServer from '../../server/AppServer'
import { AppConnection } from '../../models/AppConnection';
import {rootPath} from 'electron-root-path'
import isDev from 'electron-is-dev'
import axios from 'axios'
import {PrepopulatedValuesFileName} from '../../global'
import DialogBox from '../DialogBox';


export default class ServerAccess extends Component<DashboardProps> {

    _modalPopup : React.RefObject<DialogBox> = React.createRef();
    constructor(props){
        super(props);
        this._modalPopup = React.createRef();
    }

    state = {
        databaseNameId: "databaseName",
        databaseNameLabel: "Database",
        databaseNameInputText: "TenantAuthDb",

        serverNameId: "serverName",
        serverNameLabel: "Server",
        serverNameInputText: "Localhost",

        usernameId: "username",
        usernameLabel: "Username",
        usernameInputText: "sa",

        passwordId: "password",
        passwordLabel: "Password",
        passwordInputText: "Fame.Fame4321",

        instanceId: "instanceName",
        instanceNameLabel: "Instance",
        instanceNameInputText: "SQLDEV2016",

        backupDbId: "backupDb",
        backupDbLabel: "Backup DB",
        backupDbInputText: "FreedomAdvantage",

        backupPathId: "backupPath",
        backupPathLabel: "Backup Path",
        backupPathInputText: "C:\\SSIS\\Freedom\\",

        popupOpen: false



    }

    getVisibilityStyle = () => {

        if (this.props.hide === true)
            return {
                display: "none"
            }
        else
            return {
                display: "block"
            }
    }

    componentDidMount(){
        if ( isDev === false){
            console.log('reading values from json')
            let jsonPath : string = rootPath + "\\resources\\app\\extraResources\\" + PrepopulatedValuesFileName;
            axios.get(jsonPath).then( response => {
                this.setState({
                    databaseNameInputText : response.data.serverAccess.databaseName,
                    serverNameInputText :response.data.serverAccess.serverName,
                    usernameInputText:response.data.serverAccess.username,
                    passwordInputText:response.data.serverAccess.password,
                    instanceNameInputText:response.data.serverAccess.instanceName,
                    backupDbInputText:response.data.serverAccess.backupDb,
                    backupPathInputText:response.data.serverAccess.backupPath
                })
            })
            
        }
    }

    getConnectionProperties = ():AppConnection => {
        let appConnection: AppConnection = {
            database: this.state.databaseNameInputText,
            host: this.state.serverNameInputText,
            instance: this.state.instanceNameInputText,
            password: this.state.passwordInputText,
            user: this.state.usernameInputText
        }
        return appConnection;
    }

    testConnection = () => {       
        console.log(rootPath) 
        var test = new AppServer(this.getConnectionProperties())
       test.testConnection().then((message) => {
            this._modalPopup.current.openModal(message);
        });
        
    }

    getBackup = () => {
        var test = new AppServer(this.getConnectionProperties())
        test.createBackup(this.state.backupPathInputText, this.state.backupDbInputText);
    }

    inputOnChange = (event) => {
        let stateInputProp: string = event.target.id + 'InputText';
        this.setState({
            [stateInputProp]: event.target.value
        })
    }

  

    render() {
        return (
            <div style={this.getVisibilityStyle()}>
                <React.Fragment>
                    <section>
                        <form action="">
                            <div className={GlobalStyle.formRow}>
                                <InputLabel text={this.state.serverNameLabel} ></InputLabel>
                                <InputText value={this.state.serverNameInputText} id={this.state.serverNameId} onChange={this.inputOnChange} />
                            </div>
                            <div className={GlobalStyle.formRow}>
                                <InputLabel text={this.state.instanceNameLabel} ></InputLabel>
                                <InputText value={this.state.instanceNameInputText} id={this.state.instanceId} onChange={this.inputOnChange} />
                            </div>
                            <div className={GlobalStyle.formRow}>
                                <InputLabel text={this.state.databaseNameLabel} ></InputLabel>
                                <InputText value={this.state.databaseNameInputText} id={this.state.databaseNameId} onChange={this.inputOnChange} />
                            </div>
                            <div className={GlobalStyle.formRow}>
                                <InputLabel text={this.state.usernameLabel} ></InputLabel>
                                <InputText value={this.state.usernameInputText} id={this.state.usernameId} onChange={this.inputOnChange} />
                            </div>
                            <div className={GlobalStyle.formRow}>
                                <InputLabel text={this.state.passwordLabel} ></InputLabel>
                                <InputText value={this.state.passwordInputText} id={this.state.passwordId} onChange={this.inputOnChange} />
                            </div>
                            <div className={GlobalStyle.formRow}>
                            <input className={GlobalStyle.submitButton} type="button" value="Connect" onClick={this.testConnection} />
                            <DialogBox ref={this._modalPopup} />
                      </div>
                        </form>

                    </section>
                    <section>
                        <form>
                            <div className={GlobalStyle.formRow}>
                                <InputLabel text={this.state.backupDbLabel} ></InputLabel>
                                <InputText value={this.state.backupDbInputText} id={this.state.backupDbId} onChange={this.inputOnChange} />
                            </div>
                            <div className={GlobalStyle.formRow}>
                                <InputLabel text={this.state.backupPathLabel} ></InputLabel>
                                <InputText value={this.state.backupPathInputText} id={this.state.backupPathId} onChange={this.inputOnChange} />
                            </div>
                            <div className={GlobalStyle.formRow}>
                            <input className={GlobalStyle.submitButton} type="button" value="Get Backup" onClick={this.getBackup} />
                            </div>
                        </form>
                    </section>
                </React.Fragment>
            </div>
        )
    }
}
