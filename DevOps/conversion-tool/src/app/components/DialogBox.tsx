import React, { Component } from 'react'
import Popup from 'reactjs-popup'
import { PopupProps } from './pages/interface/PopupProps';
import PopupStlye from './DialogBox.css'

export default class DialogBox extends Component<PopupProps> {

    constructor(props) {
        super(props);
        // this.state = { open: false };
        this.openModal = this.openModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
    }

    state = {
        open: false,
        content: 'Test',
        header: 'Message'
    }

    openModal(message: string) {
        this.setState({ open: true, 
            content:   message });
    }
    closeModal() {
        this.setState({ open: false });
    }

    getHeaderColor = (): React.CSSProperties => {
        if (this.state.content === undefined || this.state.content.toLowerCase().includes("err") ) {
            return errHeaderStyle;
        }
        return successHeaderStyle
    }

    render() {
        return (
            <Popup open={this.state.open} closeOnDocumentClick onClose={this.closeModal} contentStyle={PopupContentStyle} position={"center center"}>
                <div className={PopupStlye.Modal}>
                    <a className={PopupStlye.Close} onClick={this.closeModal}>
                        &times;
                    </a>
                    <div className={PopupStlye.Header} style={this.getHeaderColor()}>{this.state.header}</div>
                    <div className={PopupStlye.Content}>
                        {this.state.content}
                    </div>

                </div>
            </Popup>
        )
    }
}

const errHeaderStyle: React.CSSProperties = {
    backgroundColor: "#ff0000"
}

const successHeaderStyle: React.CSSProperties = {
    backgroundColor: "#4CAF50"
}

const PopupContentStyle: React.CSSProperties = {
    width: "40%",
    height: "40%"

}
