import React, { Component } from 'react'
import PropTypes, { string, any, func } from 'prop-types'
import * as GlobalStyle from './pages/global.css'
import { InputProps } from './pages/interface/InputProps';

export default class InputTextArea extends Component<InputProps> {

   
    
    render() {
        return (
            <textarea className={GlobalStyle.inputTextArea} value={this.props.value} id={this.props.id} onChange={this.props.onChange}   readOnly={this.props.readonly}/>
        )
    }
}
