@ECHO OFF
SET LoopCounter=0
SET LoopCounterLimit=30
SET TargetServerName=dev-com-web1.dev.fameinc.com
SET LogFileName=%~dp0\Log.log
SET TargetServerUserName="Internal\TFSBuild"

SETLOCAL
GOTO Start

:whileIISIsStopping 
   for /F "tokens=3 delims=: " %%M in ('sc \\%TargetServerName% query "W3SVC" ^| findstr "        STATE"') do (
		if /I "%%M" == "STOP_PENDING" (
			IF %LoopCounter% LSS %LoopCounterLimit% (
				goto :whileIISIsStopping
			) ELSE (
				SET LoopCounter=0
			)
		)
		SET LoopCounter=%LoopCounter%+1;
   )
	IF ERRORLEVEL 1 SET ExitCode=1
	EXIT /B %ExitCode%
	

:waitWhileIISIsStarted
	for /F "tokens=3 delims=: " %%M in ('sc \\%TargetServerName% query "W3SVC" ^| findstr "        STATE"') do (
		if /I "%%M" == "START_PENDING" (
			IF %LoopCounter% LSS %LoopCounterLimit% (
				goto :waitWhileIISIsStarted
			) ELSE (
				SET LoopCounter=0
			)
		) else (
			if /I "%%M" == "STOPPED" (
				ECHO "Unable to start the IIS automatically. Check Service configuratoin" %RedirLog%
				SET ExitCode=2
			)
		)
		SET LoopCounter=%LoopCounter%+1;
	)
	IF ERRORLEVEL 1 SET ExitCode=1
	EXIT /B %ExitCode%
	
:Start
	
IF EXIST \\%TargetServerName% (
	ECHO Disconnecting previous connection %TargetServerName%  %LogFileName%
	net use /delete \\%TargetServerName%
)
ECHO Connecting to Target server %TargetServerName% ...  %LogFileName%

net use \\%TargetServerName% "TFS.Build" /user:%TargetServerUserName%

ECHO Stopping w3svc Service on Target server %TargetServerName% ...  %LogFileName%

SC \\%TargetServerName% stop w3svc

ECHO Waitting for w3svc Service on Target server %TargetServerName% to stop ...  %LogFileName%
CALL :whileIISIsStopping

ECHO Starting w3svc Service on  Target server %TargetServerName% ...  %LogFileName%

sc \\%TargetServerName% start w3svc

CALL :waitWhileIISIsStarted

IF EXIST \\%TargetServerName% (
	ECHO Disconnecting previous connection %TargetServerName%  %LogFileName%
	net use /delete \\%TargetServerName%
)
GOTO :END


:End
EXIT /B %ExitCode%

