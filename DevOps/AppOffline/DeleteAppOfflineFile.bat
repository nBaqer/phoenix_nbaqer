@ECHO OFF & cls

SETLOCAL 
GOTO Start

:Usage
ECHO.
ECHO Copy offline htm file:
ECHO 	This script copies the offline template to app_offline.htm
ECHO.
GOTO End



:start
SET Path=%~1

IF "%Path%"=="" GOTO :Usage

ECHO %Path%
IF "%Path:~1%" neq "\" (
	SET Path=%Path%\
)
IF EXIST %Path%app_offline.htm DEL %Path%app_offline.htm

ECHO.
PAUSE
CLS

:End
EXIT /B %ExitCode%