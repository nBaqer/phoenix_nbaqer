@ECHO OFF & cls

SETLOCAL 
GOTO Start

:Usage
ECHO.
ECHO Copy offline htm file:
ECHO 	This script copies the offline template to app_offline.htm
ECHO Usage
ECHO 	SourceControlPath DropLocation
ECHO.
GOTO End




:start
SET SourceControlPath=%~1
SET DropLocation=%~2

IF "%SourceControlPath%"=="" GOTO :Usage
IF "%DropLocation%"=="" GOTO :Usage

ECHO %SourceControlPath%
IF "%SourceControlPath:~1%" neq "\" (
	SET SourceControlPath=%SourceControlPath%\
)
ECHO %SourceControlPath%
IF "%DropLocation:~1%" neq "\" (
	SET DropLocation= %DropLocation%\
)


COPY %SourceControlPath%template.htm %DropLocation%app_offline.htm

ECHO.
PAUSE
CLS

:End
EXIT /B %ExitCode%