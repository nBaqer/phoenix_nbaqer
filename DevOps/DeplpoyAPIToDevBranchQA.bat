@ECHO OFF
SET LoopCounter=0
SET LoopCounterLimit=30
SET TargetServerName=dev-com-web1.dev.fameinc.com
SET LogFileName=%~dp0Log.log
SET TargetServerUserName="Internal\TFSBuild"
SET LocalFolder="C:\inetpub\wwwroot\advantage_api"
SET UncTargetFolder=\\dev-com-web1.dev.fameinc.com\c$\inetpub\wwwroot\Advantage\QA\develop\3.10.0\API

SETLOCAL
GOTO Start

:Start
	
IF EXIST "%UncTargetFolder%" (
	ECHO Disconnecting previous connection %TargetServerName%  %LogFileName%
	net use "%UncTargetFolder%" /delete 
)
ECHO Connecting to Target server %TargetServerName% ...  %LogFileName%

net use "%UncTargetFolder%" "TFS.Build" /user:%TargetServerUserName%

ECHO Copying files

XCOPY %LocalFolder% "%UncTargetFolder%" /s/e/i/q/r/y

IF ERRORLEVEL 0 (
	ECHO "Files were copied sucessfully"
)

ECHO Swapping configs

RENAME "%UncTargetFolder%\appsettings.json" "appsettings.local.json"
RENAME "%UncTargetFolder%\appsettings.QA.json" "appsettings.json"

IF EXIST "%UncTargetFolder%" (
	ECHO Disconnecting previous connection %TargetServerName%  %LogFileName%
	net use "%UncTargetFolder%" /delete 
)
ECHO "Command completed"
GOTO :END


:End
EXIT /B %ExitCode%

