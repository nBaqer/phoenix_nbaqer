@ECHO off & cls

REM TeamMember			Bitbucket User				Advantage Repository Name

REM Himaraj 			himaraj						advantage_hrao
REM Satyanarayana  		SatyaIndecomm 			 	advantage_sg
REM Dheeraj  			dkumarindecomm				advantage_dkumar
REM Nisha 				IndecommNisha 				advantage_npeeyoosh
REM Aniket  			AniketIndecomm				advantage_aniketb

REM Zev 				zlesnik						advantage_zlesnik
REM Troy 				Phantom1795					advantage_trichards
REM Torres 				JoseITorres					advantage_jtorres
REM Guirado 			jguirado					advantage_jguirado
REM Sweta 				thakkarsweta  				advantage_sthakkar
REM Hector 				hcordero_at_fame 			advantage_hcordero

SET DropFolder=%~dp0

for %%a in ("%DropFolder%") do set "workingFolder=%%~dpa"
echo %workingFolder%

for %%a in (%workingFolder:~0,-1%) do set "repositoryFolder=%%~dpa"
echo %repositoryFolder%

for %%a in (%repositoryFolder:~0,-1%) do set "repositoryName=%%~na"
echo %repositoryName%


SET ForkRepository=advantage_trichards
SET UserName=Phantom1795
SET Alias=Advantage_Troy

IF /I NOT "%repositoryName%"=="%ForkRepository%" (
	git remote add %Alias% https://advreviewer@bitbucket.org/%UserName%/%ForkRepository%.git
	git fetch %Alias%
)



SET ForkRepository=advantage_sthakkar
SET UserName=thakkarsweta
SET Alias=Advantage_Sweta

IF /I NOT "%repositoryName%"=="%ForkRepository%" (
	git remote add %Alias% https://advreviewer@bitbucket.org/%UserName%/%ForkRepository%.git
	git fetch %Alias%
)


SET ForkRepository=advantage_hcordero
SET UserName=hcordero_at_fame
SET Alias=Advantage_Hector

IF /I NOT "%repositoryName%"=="%ForkRepository%" (
	git remote add %Alias% https://advreviewer@bitbucket.org/%UserName%/%ForkRepository%.git
	git fetch %Alias%
)


SET ForkRepository=advantage_esosa
SET UserName=easosa
SET Alias=Advantage_Edwin

IF /I NOT "%repositoryName%"=="%ForkRepository%" (
	git remote add %Alias% https://advreviewer@bitbucket.org/%UserName%/%ForkRepository%.git
	git fetch %Alias%
)


SET ForkRepository=advantage_jastudillo
SET UserName=jastudillo-fame
SET Alias=Advantage_JoseAstudillo

IF /I NOT "%repositoryName%"=="%ForkRepository%" (
	git remote add %Alias% https://advreviewer@bitbucket.org/%UserName%/%ForkRepository%.git
	git fetch %Alias%
)

SET ForkRepository=advantage_kdbefera
SET UserName=KDbefera
SET Alias=Advantage_kimberly

IF /I NOT "%repositoryName%"=="%ForkRepository%" (
	git remote add %Alias% https://advreviewer@bitbucket.org/%UserName%/%ForkRepository%.git
	git fetch %Alias%
)

SET ForkRepository=advantage_jgonzalez
SET UserName=jgonzalez2
SET Alias=Advantage_Joseph

IF /I NOT "%repositoryName%"=="%ForkRepository%" (
	git remote add %Alias% https://advreviewer@bitbucket.org/%UserName%/%ForkRepository%.git
	git fetch %Alias%
)
