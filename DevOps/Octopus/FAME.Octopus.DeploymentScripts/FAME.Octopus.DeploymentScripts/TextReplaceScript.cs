﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace FAME.Octopus.DeploymentScripts
{
    //Example
    //FAME.Octopus.DeploymentScripts.exe TextReplace "C:\_git\advantage\Web.config" "STATIC_FILES_BUILD_NUMBER" "412"
    public class TextReplaceScript
    {
        public static void Run()
        {
            string filesToScan = Octopus.Parameters["filesToScan"];
            string textToFind = Octopus.Parameters["textToFind"];
            string textToReplaceWith = Octopus.Parameters["textToReplaceWith"];

            if (string.IsNullOrEmpty(filesToScan))
            {
                Octopus.FailStep("FilesToScan parameter is required!");
            }

            if (string.IsNullOrEmpty(textToFind))
            {
                Octopus.FailStep("TextToFind parameter is required!");
            }

            List<string> fileList = new List<string>();
            fileList = filesToScan.Split(
                ';').ToList();

            if (fileList.Count == 0)
            {
                Octopus.FailStep("FilesToScan parameter can not be empty!");
            }

            foreach (var filePath in fileList)
            {
                if (!System.IO.File.Exists(Path.GetFullPath(filePath)))
                {
                    Octopus.FailStep("Config file not found at location: " + filePath);
                    break;
                }

                var fileContents = System.IO.File.ReadAllText(filePath);
                System.IO.File.WriteAllText(filePath, fileContents.Replace(textToFind, textToReplaceWith));
            }

        }
    }
}