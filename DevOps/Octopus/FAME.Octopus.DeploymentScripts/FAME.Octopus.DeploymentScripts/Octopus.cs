﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FAME.Octopus.DeploymentScripts
{
    public class Octopus
    {
        public static Dictionary<string, string> Parameters = new Dictionary<string, string>();

        public static bool DebugMode { get; set; }

        public static void FailStep(string message)
        {

            if (Octopus.DebugMode)
            {
                Console.WriteLine(message);
            }
            else
            {
                throw new Exception(message);
            }
        }

        public static void WriteHighlight(string message)
        {
            Console.WriteLine(message);
        }


        public static void SimulateConnectionString()
        {
            Octopus.Parameters.Add("ServerName", "CORDERO\\SQLDEV2016");
            Octopus.Parameters.Add("UserName", "sa");
            Octopus.Parameters.Add("Password", "Fame.Fame1");
            Octopus.Parameters.Add("DatabaseName", "TenantAuthDb");
            Octopus.Parameters.Add("ConnectionTimeOut", string.Empty);
            Octopus.Parameters.Add("ConnectionStringFiles", @"C:\inetpub\wwwroot\FAME\Advantage\Host\App_Data\Configs\connectionStrings.config");
            Octopus.Parameters.Add("ConnectionStringName", "TenantAuthDBConnection");
            Octopus.DebugMode = true;
        }

        public static void SimulateSessionState()
        {
            Parameters = new Dictionary<string, string>();
            Octopus.Parameters.Add("Mode", "SQL");
            Octopus.Parameters.Add("ServerName", "CORDERO\\SQLDEV2016");
            Octopus.Parameters.Add("UserName", "sa");
            Octopus.Parameters.Add("Password", "Fame.Fame1");
            Octopus.Parameters.Add("ApplicationName", "Advantage");
            Octopus.Parameters.Add("ConnectionTimeOut", "20");
            Octopus.Parameters.Add("Cookieless", "false");
            Octopus.Parameters.Add("SessionStateFiles", @"C:\inetpub\wwwroot\FAME\Advantage\Site\App_Data\Configs\sessionState.config");
            Octopus.DebugMode = true;
        }


        public static void SimulateAppSettings()
        {
            Parameters = new Dictionary<string, string>();
            Octopus.Parameters.Add("AppSettings", "Reports.ReportServerURL=http://CORDERO/ReportServer_SQLDEV2016 \nReports.ReportsFolder=/Advantage/develop/4.1.0/ \nInstrumentationKey=26e3ae79-3584-4716-9c70-c2164486fc54");
            Octopus.Parameters.Add("AppSettingFiles", @"C:\inetpub\wwwroot\FAME\Advantage\Site\App_Data\Configs\appSettings.config");
            Octopus.DebugMode = true;
        }

        public static void SimulateServicesAppSettings()
        {
            Parameters = new Dictionary<string, string>();
            Octopus.Parameters.Add("AppSettings", "WapiServiceUsername=wapiUser\nWapiServicePassword=s!sfame1234");
            Octopus.Parameters.Add("AppSettingFiles", @"C:\inetpub\wwwroot\FAME\Advantage\Services\App_Data\Configs\appSettings.config");
            Octopus.DebugMode = true;
        }

        public static void SimulateJsonAppSettings()
        {
            Parameters = new Dictionary<string, string>();
            Octopus.Parameters.Add("AppSettings", "ReportServer.ReportServerUrl=http://CORDERO/ReportServer_SQLDEV2016 \nReportServer.ReportServerFolder=/Advantage/develop/4.1.0/ \nApplicationInsights.InstrumentationKey=26e3ae79-3584-4716-9c70-c2164486fc54\nReportServer.Credentials.IsImpersonationEnabled=false");
            Octopus.Parameters.Add("AppSettingFiles", @"C:\inetpub\wwwroot\FAME\Advantage\API\appsettings.json");
            Octopus.DebugMode = true;
        }

        public static void SimulateVdProjetReplace()
        {
            Parameters = new Dictionary<string, string>();
            Octopus.Parameters.Add("VdprojFile", @"C:\_Git\Advantage\DevOps\AdvantageDatabaseUpdateForm\AdvantageInstallerMaintenanceTool\AdvantageInstallerMaintenanceTool.vdproj");
            Octopus.Parameters.Add("VersionCode", @"4.0.172");
            Octopus.DebugMode = true;
        }


        public static void SimulateSmtpWebConfigSettingScript()
        {
            Parameters = new Dictionary<string, string>();
            
            Octopus.Parameters.Add("AppSettingFiles", @"C:\_Git\Advantage\Advantage\Source\FAME.Advantage.MultiTenantHost\Web.config");
            Octopus.Parameters.Add("SMTPServer", "server_Name");
            Octopus.Parameters.Add("SMTPEmailFrom", "email_from");
            Octopus.Parameters.Add("SMTPLogin", "login");
            Octopus.Parameters.Add("SMTPLoginPassword", "login_password");
            Octopus.Parameters.Add("SMTPPort", "port");
            Octopus.DebugMode = true;
        }

    }
}
