﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FAME.Octopus.DeploymentScripts
{
    public class VdprojVersionReplacer
    {
        public static void Run()
        {
            string vdprojFile = Octopus.Parameters["VdprojFile"];
            string versionCode = Octopus.Parameters["VersionCode"];

            if (string.IsNullOrEmpty(vdprojFile))
            {
                Octopus.FailStep("vdproj File parameter is required!");
            }

            if (string.IsNullOrEmpty(versionCode))
            {
                Octopus.FailStep("Version Code parameter is required!");
            }

            if (!System.IO.File.Exists(vdprojFile))
            {
                Octopus.FailStep("vdproj file was not found at location: " + vdprojFile);
            }

            string vdProjRaw = System.IO.File.ReadAllText(vdprojFile);

            if (string.IsNullOrEmpty(vdProjRaw))
            {
                Octopus.FailStep("vdproj is empty!");
            }

            int productCodeIndexOf = vdProjRaw.IndexOf("\"ProductCode\" = \"8:{", StringComparison.Ordinal);
            string productCodeLine = vdProjRaw.Substring(productCodeIndexOf, 58);
            string existingProductCodeGuid = vdProjRaw.Substring(productCodeIndexOf + 20, 36);

            vdProjRaw = vdProjRaw.Replace(productCodeLine,
                productCodeLine.Replace(existingProductCodeGuid, Guid.NewGuid().ToString().ToUpper()));


            int productVersionIndexOf = vdProjRaw.IndexOf("\"ProductVersion\" = \"8:", StringComparison.Ordinal);
            string productVersionLine = vdProjRaw.Substring(productVersionIndexOf, 30);
            string existingProductVersion = productVersionLine.Substring(productVersionLine.IndexOf(":", StringComparison.Ordinal) + 1, productVersionLine.Length - 1 - (productVersionLine.IndexOf(":", StringComparison.Ordinal) + 1));

            vdProjRaw = vdProjRaw.Replace(productVersionLine, productVersionLine.Replace(existingProductVersion, versionCode));

            System.IO.File.WriteAllText(vdprojFile, vdProjRaw);
        }
    }
}
