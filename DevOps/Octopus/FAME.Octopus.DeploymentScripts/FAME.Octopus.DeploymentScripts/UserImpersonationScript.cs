﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FAME.Octopus.DeploymentScripts
{
    public class UserImpersonationScript
    {
        public static void Run()
        {
            string isImpersonationEnabled = Octopus.Parameters["IsImpersonationEnabled"];
            string userName = Octopus.Parameters["UserName"];
            string password = Octopus.Parameters["Password"];
            string configFiles = Octopus.Parameters["ImpersonationConfigFiles"];

            if (string.IsNullOrEmpty(isImpersonationEnabled))
            {
                Octopus.FailStep("Is Impersonation Enabled parameter is required!");
            }

            if (string.IsNullOrEmpty(configFiles))
            {
                Octopus.FailStep("Impersonation Config Files parameter is required!");
            }


            bool isImpersonate = false;
            if (bool.TryParse(isImpersonationEnabled, out isImpersonate))
            {
                if (string.IsNullOrEmpty(userName))
                {
                    Octopus.FailStep("User Name parameter is required!");
                }

                if (string.IsNullOrEmpty(password))
                {
                    Octopus.FailStep("Password parameter is required!");
                }
            }

            List<string> configList = new List<string>();
            configList = configFiles.Split(';').ToList();

            if (configList.Count == 0)
            {
                Octopus.FailStep("Config File parameter can not be empty!");
            }

            foreach (var config in configList)
            {
                if (!System.IO.File.Exists(config))
                {
                    Octopus.FailStep("Config file not found at location: " + config);
                    break;
                }
                else
                {
                    if (isImpersonate)
                    {
                        string setting = "<identity impersonate=\"true\" userName=\"" + userName + "\" password=\""
                                        + password + "\" />";
                        System.IO.File.WriteAllText(config, setting);
                    }
                    else
                    {
                        string setting = "<identity impersonate=\"false\" />";
                        System.IO.File.WriteAllText(config, setting);
                    }
                }
            }
        }
    }
}
