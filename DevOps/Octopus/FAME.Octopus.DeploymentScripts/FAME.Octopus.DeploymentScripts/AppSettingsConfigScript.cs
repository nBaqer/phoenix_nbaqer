﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FAME.Octopus.DeploymentScripts
{
    using System.Xml;

    public class AppSettingsConfigScript
    {
        public static void Run()
        {
            string configFiles = Octopus.Parameters["AppSettingFiles"];
            string appSettings = Octopus.Parameters["AppSettings"];

            if (string.IsNullOrEmpty(configFiles))
            {
                Octopus.FailStep("Config File parameter is required!");
            }

            if (string.IsNullOrEmpty(appSettings))
            {
                Octopus.FailStep("App Settings parameter is required!");
            }

            List<string> configList = new List<string>();
            configList = configFiles.Split(
                ';').ToList();

            if (configList.Count == 0)
            {
                Octopus.FailStep("Config File parameter can not be empty!");
            }

            Dictionary<string, string> appSettingDictionary = new Dictionary<string, string>();

            foreach (var setting in appSettings.Split('\n'))
            {
                var key = setting.Split('=')[0];
                var value = setting.Split('=')[1];

                appSettingDictionary.Add(key, value);
            }

            foreach (var config in configList)
            {
                if (!System.IO.File.Exists(config))
                {
                    Octopus.FailStep("Config file not found at location: " + config);
                    break;
                }
                else
                {
                    System.Xml.XmlDocument configDoc = new System.Xml.XmlDocument();
                    configDoc.Load(config);
                    System.Xml.XmlNode rootNode = configDoc.SelectSingleNode("appSettings");
                    if (rootNode != null)
                    {
                        foreach (System.Xml.XmlNode appSetting in rootNode.ChildNodes)
                        {
                            if (appSetting.Attributes != null)
                            {
                                if (appSettingDictionary.ContainsKey(appSetting.Attributes["key"].Value))
                                {
                                    appSetting.Attributes["value"].Value =
                                        appSettingDictionary[appSetting.Attributes["key"].Value].Trim();
                                }
                            }
                        }
                    }

                    configDoc.Save(config);
                }
            }

        }
    }
}
