﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FAME.Octopus.DeploymentScripts
{
    using System.Data;
    using System.Data.SqlClient;
    using System.Web.UI.WebControls;

    public class VariablesCheck
    {
        public static void Run()
        {
            bool isValid = true;

            /*
                General Application Level Paths and Project level configuration
            */
            var appApiPath = Octopus.Parameters["Application.API.Path"];
            var appHostPath = Octopus.Parameters["Application.Host.Path"];
            var appServicePath = Octopus.Parameters["Application.Services.Path"];
            var appSitePath = Octopus.Parameters["Application.Site.Path"];
            var appWapiServicePath = Octopus.Parameters["Application.WapiServices.Path"];
            var appWapiWindowsServicePath = Octopus.Parameters["Application.WapiWindowsService.Path"];
            var dotnetFrameworkLocation = Octopus.Parameters["DotNetFrameworkV4Location"];
            var projectBranchName = Octopus.Parameters["ProjectBranchName"];
            var projectName = Octopus.Parameters["ProjectName"];

            if (string.IsNullOrEmpty(projectName))
            {
                isValid = false;
                Console.Error.WriteLine("ProjectName is a required variable");
            }
            else
            {
                Console.WriteLine("Project Name Is valid: " + projectName);
            }

            if (string.IsNullOrEmpty(projectBranchName))
            {
                isValid = false;
                Console.Error.WriteLine("ProjectBranchName is a required variable");
            }
            else
            {
                Console.WriteLine("Project Branch Name Is valid: " + projectName);
            }

            if (string.IsNullOrEmpty(appApiPath))
            {
                isValid = false;
                Console.Error.WriteLine("Application.API.Path is a required variable");
            }
            else
            {
                if (!appApiPath.EndsWith("\\"))
                {
                    isValid = false;
                    Console.Error.WriteLine("Application.API.Path must end in \\");
                }
                else
                {
                    Console.WriteLine("Application.API.Path is valid.");
                }
            }


            if (string.IsNullOrEmpty(appHostPath))
            {
                isValid = false;
                Console.Error.WriteLine("Application.Host.Path is a required variable");
            }
            else
            {
                if (!appHostPath.EndsWith("\\"))
                {
                    isValid = false;
                    Console.Error.WriteLine("Application.Host.Path must end in \\");
                }
                else
                {
                    Console.WriteLine("Application.Host.Path is valid.");
                }
            }

            if (string.IsNullOrEmpty(appServicePath))
            {
                isValid = false;
                Console.Error.WriteLine("Application.Services.Path is a required variable");
            }
            else
            {
                if (!appServicePath.EndsWith("\\"))
                {
                    isValid = false;
                    Console.Error.WriteLine("Application.Services.Path must end in \\");
                }
                else
                {
                    Console.WriteLine("Application.Services.Path is valid.");
                }
            }

            if (string.IsNullOrEmpty(appSitePath))
            {
                isValid = false;
                Console.Error.WriteLine("Application.Site.Path is a required variable");
            }
            else
            {
                if (!appSitePath.EndsWith("\\"))
                {
                    isValid = false;
                    Console.Error.WriteLine("Application.Site.Path must end in \\");
                }
                else
                {
                    Console.WriteLine("Application.Site.Path is valid.");
                }
            }

            if (string.IsNullOrEmpty(appWapiServicePath))
            {
                isValid = false;
                Console.Error.WriteLine("Application.WapiServices.Path is a required variable");
            }
            else
            {
                if (!appWapiServicePath.EndsWith("\\"))
                {
                    isValid = false;
                    Console.Error.WriteLine("Application.WapiServices.Path must end in \\");
                }
                else
                {
                    Console.WriteLine("Application.WapiServices.Path is valid.");
                }
            }

            if (string.IsNullOrEmpty(appWapiWindowsServicePath))
            {
                isValid = false;
                Console.Error.WriteLine("Application.WapiWindowsService.Path is a required variable");
            }
            else
            {
                if (!appWapiWindowsServicePath.EndsWith("\\"))
                {
                    isValid = false;
                    Console.Error.WriteLine("Application.WapiWindowsService.Path must end in \\");
                }
                else
                {
                    Console.WriteLine("Application.WapiWindowsService.Path is valid.");
                }
            }

            if (string.IsNullOrEmpty(dotnetFrameworkLocation))
            {
                isValid = false;
                Console.Error.WriteLine("DotNetFrameworkV4Location is a required variable");
            }
            else
            {
                if (!dotnetFrameworkLocation.EndsWith("\\"))
                {
                    isValid = false;
                    Console.Error.WriteLine("DotNetFrameworkV4Location must end in \\");
                }
                else
                {
                    if (!System.IO.Directory.Exists(dotnetFrameworkLocation))
                    {
                        isValid = false;
                        Console.WriteLine("DotNetFrameworkV4Location directory doesn't exist. Directory Location: " + dotnetFrameworkLocation);
                    }
                    else
                    {
                        Console.WriteLine("DotNetFrameworkV4Location is valid.");
                    }
                }
            }



            /*
                SQL Server Reporting Service variables
            */
            var ssrsReportServerUrl = Octopus.Parameters["Reports.ReportServerURL"];
            var ssrsReportServerFolder = Octopus.Parameters["Reports.ReportsFolder"];

            if (string.IsNullOrEmpty(ssrsReportServerUrl))
            {
                isValid = false;
                Console.Error.WriteLine("Reports.ReportServerURL is a required variable");
            }
            else
            {
                if (!System.Uri.IsWellFormedUriString(ssrsReportServerUrl, UriKind.Absolute))
                {
                    isValid = false;
                    Console.Error.WriteLine(
                        "Reports.ReportServerURL is not a valid formatted url. The URL must be formed as protocol://domainName/PathToReportServer, example: http://localhost/ReportServer_SQLDEV2016 . The given URL is: "
                        + ssrsReportServerUrl);
                }
                else
                {
                    Console.WriteLine("Reports.ReportServerURL is a valid URL.");
                }

            }

            if (string.IsNullOrEmpty(ssrsReportServerFolder))
            {
                isValid = false;
                Console.Error.WriteLine("Reports.ReportsFolder is a required variable");
            }
            else
            {
                if (!ssrsReportServerFolder.StartsWith("/"))
                {
                    isValid = false;
                    Console.Error.WriteLine("Reports.ReportsFolder variable must contain a forward slash(/) at the beginning.");
                }
                else if (!ssrsReportServerFolder.EndsWith("/"))
                {
                    isValid = false;
                    Console.Error.WriteLine("Reports.ReportsFolder variable must contain a forward slash(/) at the end.");
                }
                else
                {
                    Console.WriteLine("Reports.ReportsFolder is a valid folder");
                }
            }

            /*
                Advantage DB Tool variables
            */
            var tenantDbInstallationPath = Octopus.Parameters["Tenant.AdvantageDbInstallationPath"];
            var tenantDbToolConfigPath = Octopus.Parameters["Tenant.AdvantageDbToolConfigPath"];
            var tenantDbToolExeLocation = Octopus.Parameters["Tenant.AdvantageDbToolExe"];


            if (string.IsNullOrEmpty(tenantDbInstallationPath))
            {
                isValid = false;
                Console.Error.WriteLine("Tenant.AdvantageDbInstallationPath is a required variable");
            }
            else
            {
                if (!System.IO.Directory.Exists(tenantDbInstallationPath))
                {
                    isValid = false;
                    Console.Error.WriteLine(
                        "Tenant.AdvantageDbInstallationPath " + tenantDbInstallationPath + " directory does not exist. Check the path is valid and that the DbTool have been installed manually on the web server.");
                }
                if (!tenantDbInstallationPath.EndsWith("\\"))
                {
                    isValid = false;
                    Console.Error.WriteLine("Tenant.AdvantageDbInstallationPath is not valid, path must end with backslash \"\\\"");
                }
                else
                {
                    Console.WriteLine("Tenant.AdvantageDbInstallationPath is a valid path.");
                }
            }


            if (string.IsNullOrEmpty(tenantDbToolConfigPath))
            {
                isValid = false;
                Console.Error.WriteLine("Tenant.AdvantageDbInstallationPath is a required variable");
            }
            else
            {
                if (!System.IO.File.Exists(tenantDbToolConfigPath))
                {
                    isValid = false;
                    Console.Error.WriteLine(
                        "Tenant.AdvantageDbToolConfigPath " + tenantDbToolConfigPath + " file does not exist. Check the file path is valid and that the DbTool have been installed manually on the web server.");
                }
                if (!tenantDbToolConfigPath.EndsWith(".json"))
                {
                    isValid = false;
                    Console.Error.WriteLine(@"Tenant.AdvantageDbToolConfigPath is not valid, file path must be the full path to the EnvironmentConfiguration.json. Example: C:\Program Files (x86)\FAME Inc\AdvantageInstallerMaintenanceTool\EnvironmentConfiguration.json ");
                }
                else
                {
                    Console.WriteLine("Tenant.AdvantageDbToolConfigPath is a valid file path.");
                }
            }


            if (string.IsNullOrEmpty(tenantDbToolExeLocation))
            {
                isValid = false;
                Console.Error.WriteLine("Tenant.AdvantageDbToolExe is a required variable");
            }
            else
            {
                if (!System.IO.File.Exists(tenantDbToolExeLocation))
                {
                    isValid = false;
                    Console.Error.WriteLine(
                        "Tenant.AdvantageDbToolExe " + tenantDbToolExeLocation + " file does not exist. Check the file path is valid and that the DbTool have been installed manually on the web server.");
                }
                if (!tenantDbToolExeLocation.EndsWith(".exe"))
                {
                    isValid = false;
                    Console.Error.WriteLine(@"Tenant.AdvantageDbToolExe is not valid, file path must be the full path to the AdvantageInstallerMaintenanceTool.exe. Example: C:\Program Files (x86)\FAME Inc\AdvantageInstallerMaintenanceTool\AdvantageInstallerMaintenanceTool.exe ");
                }
                else
                {
                    Console.WriteLine("Tenant.AdvantageDbToolExe is a valid file path.");
                }
            }


            /*
                Tenant application specific paths.
            */
            var tenantAppInstallationPath = Octopus.Parameters["Tenant.ApplicationInstallationPath"];
            var tenantDbBackupLocation = Octopus.Parameters["Tenant.DatabaseBackupLocation"];
            var tenantDeploymentBackupLocation = Octopus.Parameters["Tenant.DeploymentBackupLocation"];
            var tenantWorkingDirectory = Octopus.Parameters["Tenant.WorkingDirectory"];

            if (string.IsNullOrEmpty(tenantAppInstallationPath))
            {
                isValid = false;
                Console.Error.WriteLine("Tenant.TenantAppInstallationPath is a required variable");
            }
            else
            {
                if (!System.IO.Directory.Exists(tenantAppInstallationPath))
                {
                    isValid = false;
                    Console.Error.WriteLine(
                        "Tenant.TenantAppInstallationPath " + tenantAppInstallationPath + " directory does not exist");
                }

                if (!tenantAppInstallationPath.EndsWith("\\"))
                {
                    isValid = false;
                    Console.Error.WriteLine(
                        "Tenant.TenantAppInstallationPath path need's to end in on backslash (\\)");
                }

                if (!System.IO.Directory.Exists(tenantAppInstallationPath + "Host"))
                {
                    isValid = false;
                    Console.Error.WriteLine(
                        "Tenant.TenantAppInstallationPath " + tenantAppInstallationPath + "HOST Application does not exist");
                }
                if (!System.IO.Directory.Exists(tenantAppInstallationPath + "Site"))
                {
                    isValid = false;
                    Console.Error.WriteLine(
                        "Tenant.TenantAppInstallationPath " + tenantAppInstallationPath + "Site Application does not exist");
                }
                if (!System.IO.Directory.Exists(tenantAppInstallationPath + "Api"))
                {
                    isValid = false;
                    Console.Error.WriteLine(
                        "Tenant.TenantAppInstallationPath " + tenantAppInstallationPath + "Site Application does not exist");
                }
                if (!System.IO.Directory.Exists(tenantAppInstallationPath + "Services"))
                {
                    isValid = false;
                    Console.Error.WriteLine(
                        "Tenant.TenantAppInstallationPath " + tenantAppInstallationPath + "Services Application does not exist");
                }
                if (!System.IO.Directory.Exists(tenantAppInstallationPath + "WapiServices"))
                {
                    isValid = false;
                    Console.Error.WriteLine(
                        "Tenant.TenantAppInstallationPath " + tenantAppInstallationPath + "WapiServices Application does not exist");
                }
                if (!System.IO.Directory.Exists(tenantAppInstallationPath + "WapiWindowsService"))
                {
                    isValid = false;
                    Console.Error.WriteLine(
                        "Tenant.TenantAppInstallationPath " + tenantAppInstallationPath + "WapiWindowsService Application does not exist");
                }
            }

            if (string.IsNullOrEmpty(tenantDbBackupLocation))
            {
                isValid = false;
                Console.Error.WriteLine("Tenant.DatabaseBackupLocation is a required variable");
            }

            if (!System.IO.Directory.Exists(tenantDbBackupLocation))
            {
                try
                {
                    System.IO.Directory.CreateDirectory(tenantDbBackupLocation);
                    Console.WriteLine("Directory created: " + tenantDbBackupLocation);

                }
                catch (Exception e)
                {
                    isValid = false;
                    Console.Error.WriteLine(e.Message + ":\n" + e.StackTrace);
                }
            }

            if (string.IsNullOrEmpty(tenantDeploymentBackupLocation))
            {
                isValid = false;
                Console.Error.WriteLine("Tenant.DeploymentBackupLocation is a required variable");
            }

            if (!System.IO.Directory.Exists(tenantDeploymentBackupLocation))
            {
                try
                {
                    System.IO.Directory.CreateDirectory(tenantDeploymentBackupLocation);
                    Console.WriteLine("Directory created: " + tenantDeploymentBackupLocation);

                }
                catch (Exception e)
                {
                    isValid = false;
                    Console.Error.WriteLine(e.Message + ":\n" + e.StackTrace);
                }
            }


            if (string.IsNullOrEmpty(tenantWorkingDirectory))
            {
                isValid = false;
                Console.Error.WriteLine("Tenant.WorkingDirectory is a required variable");
            }


            if (!string.IsNullOrEmpty(tenantWorkingDirectory))
            {
                if (!tenantWorkingDirectory.EndsWith("\\"))
                {
                    isValid = false;
                    Console.Error.WriteLine("Tenant.WorkingDirectory path needs to end in backlash \\");
                }
            }


            if (!System.IO.Directory.Exists(tenantWorkingDirectory))
            {
                try
                {
                    System.IO.Directory.CreateDirectory(tenantWorkingDirectory);
                    Console.WriteLine("Directory created: " + tenantWorkingDirectory);
                }
                catch (Exception e)
                {
                    isValid = false;
                    Console.Error.WriteLine(e.Message + ":\n" + e.StackTrace);
                }
            }

            /*
                Databases Names and connection string.
            */
            var tenantAuthDbName = Octopus.Parameters["Tenant.TenantAuthDbName"];
            var tenantAuthDbConnectionString = Octopus.Parameters["Tenant.ConnectionString"];
            var tenantName = Octopus.Parameters["Tenant.Name"];

            var tenantAdvDbName = Octopus.Parameters["Tenant.AdvantageDbName"];


            try
            {
                if (string.IsNullOrEmpty(tenantAuthDbConnectionString))
                {
                    isValid = false;
                    Console.Error.WriteLine("Tenant.ConnectionString is a required variable");
                }
                if (string.IsNullOrEmpty(tenantAuthDbName))
                {
                    isValid = false;
                    Console.Error.WriteLine("Tenant.TenantAuthDbName is a required variable");
                }
                if (string.IsNullOrEmpty(tenantName))
                {
                    isValid = false;
                    Console.Error.WriteLine("Tenant.Name is a required variable");
                }
                if (string.IsNullOrEmpty(tenantAdvDbName))
                {
                    isValid = false;
                    Console.Error.WriteLine("Tenant.AdvantageDbName is a required variable");
                }

                if (!string.IsNullOrEmpty(tenantAuthDbConnectionString))
                {
                    System.Data.SqlClient.SqlConnection sqlConnection = new System.Data.SqlClient.SqlConnection();
                    sqlConnection.ConnectionString = tenantAuthDbConnectionString;

                    try
                    {
                        sqlConnection.Open();
                    }
                    catch (Exception e)
                    {
                        isValid = false;
                        Console.Error.WriteLine(
                            "Unable to connect to the TenantAuthDb. Invalid Tenant.ConnectionString. Review the Server/DataSource, UserName, Password and Database are valid.");
                        Console.Error.WriteLine(e.Message);
                        Console.Error.WriteLine(e.StackTrace);
                    }
                    finally
                    {
                        sqlConnection.Close();
                    }
                }

                if (!string.IsNullOrEmpty(tenantAuthDbConnectionString) && !string.IsNullOrEmpty(tenantName))
                {
                    System.Data.SqlClient.SqlConnection sqlConnection = new System.Data.SqlClient.SqlConnection();
                    System.Data.SqlClient.SqlConnectionStringBuilder connectionStringBuilder = new System.Data.SqlClient.SqlConnectionStringBuilder(tenantAuthDbConnectionString);
                    connectionStringBuilder.InitialCatalog = tenantName;
                    sqlConnection.ConnectionString = connectionStringBuilder.ConnectionString;

                    try
                    {
                        sqlConnection.Open();
                    }
                    catch (Exception e)
                    {
                        isValid = false;
                        Console.Error.WriteLine(
                            "Unable to connect to the " + tenantName + " database using the following connection string" + connectionStringBuilder.ConnectionString);
                        Console.Error.WriteLine(e.Message);
                        Console.Error.WriteLine(e.StackTrace);
                    }
                    finally
                    {
                        sqlConnection.Close();
                    }
                }

                if (!string.IsNullOrEmpty(tenantAuthDbConnectionString) && !string.IsNullOrEmpty(tenantAdvDbName))
                {
                    System.Data.SqlClient.SqlConnection sqlConnection = new System.Data.SqlClient.SqlConnection();
                    System.Data.SqlClient.SqlConnectionStringBuilder connectionStringBuilder = new System.Data.SqlClient.SqlConnectionStringBuilder(tenantAuthDbConnectionString);
                    connectionStringBuilder.InitialCatalog = tenantAdvDbName;
                    sqlConnection.ConnectionString = connectionStringBuilder.ConnectionString;

                    try
                    {
                        sqlConnection.Open();
                    }
                    catch (Exception e)
                    {
                        isValid = false;
                        Console.Error.WriteLine(
                            "Unable to connect to the " + tenantAdvDbName + " database using the following connection string" + connectionStringBuilder.ConnectionString);
                        Console.Error.WriteLine(e.Message);
                        Console.Error.WriteLine(e.StackTrace);
                    }
                    finally
                    {
                        sqlConnection.Close();
                    }
                }

            }
            catch (Exception e)
            {
                isValid = false;
                Console.Error.WriteLine("Unable to connect to the database server and/or the databases due to: ");
                Console.Error.WriteLine(e.Message);
                Console.Error.WriteLine(e.StackTrace);
            }

            var tenantDbServerName = Octopus.Parameters["Tenant.DatabaseServerName"];
            var tenantDbSqLUserLogin = Octopus.Parameters["Tenant.DatabaseSQLLogin"];
            var tenantDbSqlPassword = Octopus.Parameters["Tenant.DatabaseSQLPassword"];

            if (string.IsNullOrEmpty(tenantDbServerName))
            {
                isValid = false;
                Console.Error.WriteLine("Tenant.DatabaseServerName is a required variable");
            }
            if (string.IsNullOrEmpty(tenantDbSqLUserLogin))
            {
                isValid = false;
                Console.Error.WriteLine("Tenant.DatabaseSQLLogin is a required variable");
            }
            if (string.IsNullOrEmpty(tenantDbSqlPassword))
            {
                isValid = false;
                Console.Error.WriteLine("Tenant.DatabaseSQLPassword is a required variable");
            }

            try
            {
                System.Data.SqlClient.SqlConnection sqlConnection = new System.Data.SqlClient.SqlConnection();
                System.Data.SqlClient.SqlConnectionStringBuilder connectionStringBuilder = new System.Data.SqlClient.SqlConnectionStringBuilder(tenantAuthDbConnectionString);
                connectionStringBuilder.InitialCatalog = "master";
                connectionStringBuilder.DataSource = tenantDbServerName;
                connectionStringBuilder.UserID = tenantDbSqLUserLogin;
                connectionStringBuilder.Password = tenantDbSqlPassword;

                sqlConnection.ConnectionString = connectionStringBuilder.ConnectionString;

                sqlConnection.Open();

                System.Data.SqlClient.SqlCommand testPermCommand = new System.Data.SqlClient.SqlCommand("SELECT IS_SRVROLEMEMBER('sysadmin') as IsSysAdmin", sqlConnection);
                testPermCommand.CommandType = System.Data.CommandType.Text;
                System.Data.SqlClient.SqlDataAdapter adapter = new System.Data.SqlClient.SqlDataAdapter(testPermCommand);
                System.Data.DataTable dataTable = new System.Data.DataTable();
                adapter.Fill(dataTable);

                if (dataTable != null)
                {
                    if (dataTable.Rows.Count > 0)
                    {
                        if (dataTable.Rows[0]["IsSysAdmin"].ToString() == "0")
                        {
                            isValid = false;
                            Console.Error.WriteLine(" User " + tenantDbSqLUserLogin + " doesn't have SysAdmin permissions");
                        }
                    }
                    else
                    {
                        isValid = false;
                        Console.Error.WriteLine("Unable check the User " + tenantDbSqLUserLogin + " permissions.");
                    }
                }
                else
                {
                    isValid = false;
                    Console.Error.WriteLine("Unable check the User " + tenantDbSqLUserLogin + " permissions.");
                }

            }
            catch (Exception e)
            {
                isValid = false;
                Console.Error.WriteLine("Unable check the User " + tenantDbSqLUserLogin + " permissions. Reason could be: unable to connect to the database server and/or the databases. Exception:");
                Console.Error.WriteLine(e.Message);
                Console.Error.WriteLine(e.StackTrace);
            }


            var tenantPackageId = Octopus.Parameters["Tenant.PackageId"];
            if (string.IsNullOrEmpty(tenantPackageId))
            {
                isValid = false;
                Console.Error.WriteLine("Tenant.PackageId is a required variable");
            }

            /*
                User for Impersonation
            */

            var tenantUsernameToImpersonate = Octopus.Parameters["Tenant.UserNameToImpersonate"];
            var tenantPasswordToImpersonate = Octopus.Parameters["Tenant.PasswordToImpersonate"];
            if (string.IsNullOrEmpty(tenantUsernameToImpersonate))
            {
                isValid = false;
                Console.Error.WriteLine("Tenant.UserNameToImpersonate is a required variable");
            }

            if (string.IsNullOrEmpty(tenantPasswordToImpersonate))
            {
                isValid = false;
                Console.Error.WriteLine("Tenant.PasswordToImpersonate is a required variable");
            }


            /*
                Flags to determine which tool to use to update the database. Only Db Tool supported at the moment.
            */

            var tenantUseDbTool = Octopus.Parameters["Tenant.UseDbToolToUpdateDatabase"];
            var tenantUseRedgate = Octopus.Parameters["Tenant.UseRedGateToUpdateDb"];

            if (string.IsNullOrEmpty(tenantUseDbTool) && string.IsNullOrEmpty(tenantUseRedgate))
            {
                isValid = false;
                Console.Error.WriteLine("Tenant.UseDbToolToUpdateDatabase and Tenant.UseRedGateToUpdateDb are empty or null, one of them must be set.");
            }

            /*
                Wapi Windows Service variables
            */

            var tenantWapiWindowsServiceName = Octopus.Parameters["Tenant.AdvantageWindowsServiceName"];
            var wapiWindowsServiceSecrectKey = Octopus.Parameters["WapiWindowsService.SecretCompanyKeys"];
            var wapiWindowsServiceWapiProxyUrl = Octopus.Parameters["WapiWindowsService.UrlWapiProxy"];


            if (string.IsNullOrEmpty(tenantWapiWindowsServiceName))
            {
                isValid = false;
                Console.Error.WriteLine("Tenant.AdvantageWindowsServiceName is a required variable");
            }
            if (string.IsNullOrEmpty(wapiWindowsServiceSecrectKey))
            {
                isValid = false;
                Console.Error.WriteLine("WapiWindowsService.SecretCompanyKeys is a required variable");
            }
            if (string.IsNullOrEmpty(wapiWindowsServiceWapiProxyUrl))
            {
                isValid = false;
                Console.Error.WriteLine("WapiWindowsService.UrlWapiProxy is a required variable");
            }

            /*
                Azure Application insights configuration key
            */
            var appInsightsKey = Octopus.Parameters["AppInsights.InstrumentationKey"];

            if (string.IsNullOrEmpty(appInsightsKey))
            {
                isValid = false;
                Console.Error.WriteLine("appInsightsKey is a required variable");
            }

            var smtpServer = Octopus.Parameters["Tenant.Host.SMTP.Server"];
            var smtpServerPort = Octopus.Parameters["Tenant.Host.SMTP.Port"];
            var smtpEmailFrom = Octopus.Parameters["Tenant.Host.SMTP.EmailFrom"];

            if (string.IsNullOrEmpty(smtpServer))
            {
                isValid = false;
                Console.Error.WriteLine("Tenant.Host.SMTP.Server is a required variable");
            }

            if (string.IsNullOrEmpty(smtpServerPort))
            {
                isValid = false;
                Console.Error.WriteLine("Tenant.Host.SMTP.Port is a required variable");
            }

            if (string.IsNullOrEmpty(smtpEmailFrom))
            {
                isValid = false;
                Console.Error.WriteLine("Tenant.Host.SMTP.EmailFrom is a required variable");
            }

            if (isValid == false)
            {
                Octopus.FailStep("The Variables Requirements have not been met, review the log of the exceptions before trying to deploy.");
            }
            else
            {
                Octopus.WriteHighlight("Variable Check passed!");
            }

        }
    }
}
