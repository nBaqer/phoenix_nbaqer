﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FAME.Octopus.DeploymentScripts
{
    using System.CodeDom;

    public class JsonAppSettingScript
    {
        public static void Run()
        {
            string configFiles = Octopus.Parameters["AppSettingFiles"];
            string appSettings = Octopus.Parameters["AppSettings"];

            if (string.IsNullOrEmpty(configFiles))
            {
                Octopus.FailStep("Config File parameter is required!");
            }

            if (string.IsNullOrEmpty(appSettings))
            {
                Octopus.FailStep("App Settings parameter is required!");
            }

            List<string> configList = new List<string>();
            configList = configFiles.Split(
                ';').ToList();

            if (configList.Count == 0)
            {
                Octopus.FailStep("Config File parameter can not be empty!");
            }

            Dictionary<string, string> appSettingDictionary = new Dictionary<string, string>();

            foreach (var setting in appSettings.Split('\n'))
            {
                var key = setting.Split('=')[0];
                var value = setting.Split('=')[1];

                appSettingDictionary.Add(key, value);
            }

            foreach (var config in configList)
            {
                if (!System.IO.File.Exists(config))
                {
                    Octopus.FailStep("Config file not found at location: " + config);
                    break;
                }
                else
                {
                    string json = System.IO.File.ReadAllText(config);
                    var serializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                    dynamic deserializedObject = serializer.Deserialize<dynamic>(json);
                    if (deserializedObject != null)
                    {
                        foreach (string setting in appSettingDictionary.Keys)
                        {
                            if (!setting.Contains("."))
                            {
                                deserializedObject[setting] = appSettingDictionary[setting];
                            }
                            else
                            {
                                string[] settingNavigation = setting.Split('.');
                                Dictionary<string, object> subItem = (Dictionary<string, object>)deserializedObject;
                                int count = 0;
                                while (count <= settingNavigation.Length - 1)
                                {
                                    if (subItem.ContainsKey(settingNavigation[count]))
                                    {
                                        var result = subItem[settingNavigation[count]];
                                        if (result.GetType() == typeof(Dictionary<string, object>))
                                        {
                                            subItem = (Dictionary<string, object>)result;
                                        }
                                    }

                                    count += 1;
                                }

                                Type type = subItem[settingNavigation.Last()].GetType();
                                if (type == typeof(bool))
                                {
                                    subItem[settingNavigation.Last()] = bool.Parse(appSettingDictionary[setting]);
                                }
                                else if (type == typeof(long))
                                {
                                    subItem[settingNavigation.Last()] = long.Parse(appSettingDictionary[setting]);
                                }
                                else if (type == typeof(double))
                                {
                                    subItem[settingNavigation.Last()] = double.Parse(appSettingDictionary[setting]);
                                }
                                else if (type == typeof(int))
                                {
                                    subItem[settingNavigation.Last()] = int.Parse(appSettingDictionary[setting]);
                                }
                                else if (type == typeof(decimal))
                                {
                                    subItem[settingNavigation.Last()] = decimal.Parse(appSettingDictionary[setting]);
                                }
                                else if (type == typeof(DateTime))
                                {
                                    subItem[settingNavigation.Last()] = DateTime.Parse(appSettingDictionary[setting]);
                                }
                                else if (type == typeof(Guid))
                                {
                                    subItem[settingNavigation.Last()] = Guid.Parse(appSettingDictionary[setting]);
                                }
                                else if (type == typeof(string))
                                {
                                    subItem[settingNavigation.Last()] = appSettingDictionary[setting];
                                }

                            }
                        }
                    }
                    string serializedJson = serializer.Serialize(deserializedObject);
                    if (!string.IsNullOrEmpty(serializedJson))
                    {
                        System.IO.File.WriteAllText(config, serializedJson);
                    }
                }
            }
        }
    }
}
