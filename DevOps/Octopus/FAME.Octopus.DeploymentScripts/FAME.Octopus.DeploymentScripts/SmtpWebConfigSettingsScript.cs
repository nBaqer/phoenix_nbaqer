﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FAME.Octopus.DeploymentScripts
{
    using System.Xml;

    public class SmtpWebConfigSettingsScript
    {
        public static void Run()
        {
            string configFiles = Octopus.Parameters["AppSettingFiles"];
            string smtpServer = Octopus.Parameters["SMTPServer"];
            string smtpEmailFrom = Octopus.Parameters["SMTPEmailFrom"];
            string smtpLogin = Octopus.Parameters["SMTPLogin"];
            string smtpLoginPassword = Octopus.Parameters["SMTPLoginPassword"];
            string smtpPort = Octopus.Parameters["SMTPPort"];

            if (string.IsNullOrEmpty(configFiles))
            {
                Octopus.FailStep("Config File parameter is required!");
            }

            List<string> configList = new List<string>();
            configList = configFiles.Split(
                ';').ToList();

            if (configList.Count == 0)
            {
                Octopus.FailStep("Config File parameter can not be empty!");
            }

            if (string.IsNullOrEmpty(smtpServer))
            {
                Octopus.FailStep("SMTP Server parameter is required!");
            }

            if (string.IsNullOrEmpty(smtpEmailFrom))
            {
                Octopus.FailStep("SMTP Email From parameter is required!");
            }

            if (!string.IsNullOrEmpty(smtpLogin))
            {
                if (string.IsNullOrEmpty(smtpLoginPassword))
                {
                    Octopus.FailStep("SMTP Login Password parameter is required!");
                }
            }

            if (string.IsNullOrEmpty(smtpPort))
            {
                Octopus.FailStep("SMTP Port parameter is required!");
            }

            foreach (var config in configList)
            {

                if (!System.IO.File.Exists(config))
                {
                    Octopus.FailStep("Config file not found at location: " + config);
                    break;
                }
                else
                {
                    System.Xml.XmlDocument configDoc = new System.Xml.XmlDocument();
                    configDoc.Load(config);
                    System.Xml.XmlNode rootNode = configDoc.SelectSingleNode("configuration");
                    if (rootNode != null)
                    {
                        System.Xml.XmlNode systemNetXmlNode = rootNode["system.net"];
                        if (systemNetXmlNode != null)
                        {
                            System.Xml.XmlNode mailSettingsXmlNode = systemNetXmlNode["mailSettings"];
                            if (mailSettingsXmlNode != null)
                            {
                                System.Xml.XmlNode smtpXmlNode = mailSettingsXmlNode["smtp"];
                                if (smtpXmlNode != null)
                                {
                                    if (smtpXmlNode.Attributes["from"] != null)
                                    {
                                        smtpXmlNode.Attributes["from"].Value = smtpEmailFrom;
                                    }

                                    System.Xml.XmlNode networkXmlElement = smtpXmlNode["network"];
                                    if (networkXmlElement != null)
                                    {
                                        if (networkXmlElement.Attributes["host"] != null)
                                        {
                                            networkXmlElement.Attributes["host"].Value = smtpServer;
                                        }
                                        if (networkXmlElement.Attributes["userName"] != null)
                                        {
                                            networkXmlElement.Attributes["userName"].Value = smtpLogin;
                                        }
                                        if (networkXmlElement.Attributes["password"] != null)
                                        {
                                            networkXmlElement.Attributes["password"].Value = smtpLoginPassword;
                                        }
                                        if (networkXmlElement.Attributes["port"] != null)
                                        {
                                            networkXmlElement.Attributes["port"].Value = smtpPort;
                                        }
                                    }
                                }
                                else
                                {
                                    Octopus.FailStep("Web Config " + config + " is missing element smtp");
                                }
                            }
                            else
                            {
                                Octopus.FailStep("Web Config " + config + " is missing element mailSettings");
                            }
                        }
                        else
                        {
                            Octopus.FailStep("Web Config " + config + " is missing element system.net");
                        }
                    }
                    configDoc.Save(config);

                }
            }

        }
    }
}
