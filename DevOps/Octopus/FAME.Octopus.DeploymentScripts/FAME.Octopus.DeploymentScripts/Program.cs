using System;
using System.Diagnostics;

namespace FAME.Octopus.DeploymentScripts
{
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Xml;

    class Program
    {
        static int Main(string[] args)
        {
            //Debugger.Launch();
            if (args.Length > 0)
            {
                if (args[0].Equals("TextReplace"))
                {
                    Octopus.Parameters = new Dictionary<string, string>();
                    Octopus.Parameters.Add("filesToScan", args[1]);
                    Octopus.Parameters.Add("textToFind", args[2]);
                    Octopus.Parameters.Add("textToReplaceWith", args[3]);
                    Octopus.DebugMode = false;
                    TextReplaceScript.Run();
                }
                else if (args[0].Equals("JsonReplacer"))
                {
                    Octopus.Parameters = new Dictionary<string, string>();
                    Octopus.Parameters.Add("AppSettingFiles", args[1]);
                    Octopus.Parameters.Add("AppSettings", args[2]);
                    Octopus.DebugMode = false;

                    JsonAppSettingScript.Run();

                    return 0;
                }
                else if (args[0].Equals("VdprojVersionReplacer"))
                {
                    Octopus.Parameters = new Dictionary<string, string>();
                    Octopus.Parameters.Add("VdprojFile", args[1]);
                    Octopus.Parameters.Add("VersionCode", args[2]);
                    Octopus.DebugMode = false;

                    VdprojVersionReplacer.Run();

                    return 0;
                }
            }
            else
            {
                Console.WriteLine("Hello Octopus Deploy!");


                //Octopus.SimulateVdProjetReplace();
                //VdprojVersionReplacer.Run();

                //Octopus.SimulateConnectionString();
                //ConnectionStringScript.Run();


                //Octopus.SimulateSessionState();
                //SessionStateScript.Run();

                //Octopus.SimulateAppSettings();
                //AppSettingsConfigScript.Run();

                //Octopus.SimulateServicesAppSettings();
                //AppSettingsConfigScript.Run();

                //Octopus.SimulateJsonAppSettings();
                //JsonAppSettingScript.Run();

                Octopus.SimulateSmtpWebConfigSettingScript();
                SmtpWebConfigSettingsScript.Run();

                Console.ReadKey();
                return 0;
            }

            return 0;
        }

    }

}

