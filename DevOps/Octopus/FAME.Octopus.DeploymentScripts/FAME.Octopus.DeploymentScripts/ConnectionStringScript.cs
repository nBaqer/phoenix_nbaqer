﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FAME.Octopus.DeploymentScripts
{
    public class ConnectionStringScript
    {
        public static void Run()
        {
            string serverName = Octopus.Parameters["ServerName"];
            string userName = Octopus.Parameters["UserName"];
            string password = Octopus.Parameters["Password"];
            string databaseName = Octopus.Parameters["DatabaseName"];
            string timeOut = Octopus.Parameters["ConnectionTimeOut"];
            string configFiles = Octopus.Parameters["ConnectionStringFiles"];
            string connectionStringName = Octopus.Parameters["ConnectionStringName"];
            int intTimeOut = 0;

            if (string.IsNullOrEmpty(serverName))
            {
                Octopus.FailStep("Server Name parameter is required!");
            }
            if (string.IsNullOrEmpty(userName))
            {
                Octopus.FailStep("User Name parameter is required!");
            }
            if (string.IsNullOrEmpty(password))
            {
                Octopus.FailStep("Password parameter is required!");
            }
            if (string.IsNullOrEmpty(databaseName))
            {
                Octopus.FailStep("Database Name parameter is required!");
            }

            if (!string.IsNullOrEmpty(timeOut))
            {

                if (!int.TryParse(timeOut, out intTimeOut))
                {
                    Octopus.FailStep("Time Out parameter must be a numeric value!");
                }
            }

            if (string.IsNullOrEmpty(configFiles))
            {
                Octopus.FailStep("Config File parameter is required!");
            }


            if (string.IsNullOrEmpty(connectionStringName))
            {
                Octopus.FailStep("Connection String Name parameter is required!");
            }

            List<string> configList = new List<string>();
            configList = configFiles.Split(
                ';').ToList();

            if (configList.Count == 0)
            {
                Octopus.FailStep("Config File parameter can not be empty!");
            }

            foreach (var config in configList)
            {
                if (!System.IO.File.Exists(config))
                {
                    Octopus.FailStep("Config file not found at location: " + config);
                    break;
                }
                else
                {

                    System.Data.SqlClient.SqlConnectionStringBuilder connectionString =
                        new System.Data.SqlClient.SqlConnectionStringBuilder();
                    if (intTimeOut > 0)
                    {
                        connectionString.ConnectTimeout = intTimeOut;
                    }

                    connectionString.InitialCatalog = databaseName;
                    connectionString.Password = password;
                    connectionString.IntegratedSecurity = false;
                    connectionString.UserID = userName;
                    connectionString.DataSource = serverName;

                    System.Xml.XmlDocument configDoc = new System.Xml.XmlDocument();
                    configDoc.Load(config);

                    string query = "add[@name='" + connectionStringName + "']";

                    System.Xml.XmlNode node = configDoc.SelectSingleNode("connectionStrings").SelectSingleNode(query);
                    if (node == null)
                    {
                        Octopus.FailStep("Connection string name was not found inside config file " + config);
                        break;
                    }

                    foreach (System.Xml.XmlAttribute attribute in node.Attributes)
                    {
                        if (attribute.Name.Equals("connectionString", StringComparison.CurrentCultureIgnoreCase))
                        {
                            attribute.Value = connectionString.ConnectionString;
                            configDoc.Save(config);
                        }
                    }
                }
            }
        }
    }
}
