﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FAME.Octopus.DeploymentScripts
{
    public class SessionStateScript
    {
        public static void Run()
        {
            string serverName = Octopus.Parameters["ServerName"];
            string userName = Octopus.Parameters["UserName"];
            string password = Octopus.Parameters["Password"];
            string applicationName = Octopus.Parameters["ApplicationName"];
            string timeOut = Octopus.Parameters["ConnectionTimeOut"];
            string configFiles = Octopus.Parameters["SessionStateFiles"];
            string mode = Octopus.Parameters["Mode"];
            string cookieless = Octopus.Parameters["Cookieless"];
            int intTimeOut = 20;

            if (string.IsNullOrEmpty(mode))
            {
                Octopus.FailStep("Mode parameter is required (Choose SQL or InProc)!");
            }

            if (mode.Equals("SQL", StringComparison.CurrentCultureIgnoreCase))
            {
                if (string.IsNullOrEmpty(serverName))
                {
                    Octopus.FailStep("Server Name parameter is required!");
                }

                if (string.IsNullOrEmpty(userName))
                {
                    Octopus.FailStep("User Name parameter is required!");
                }

                if (string.IsNullOrEmpty(password))
                {
                    Octopus.FailStep("Password parameter is required!");
                }

                if (string.IsNullOrEmpty(applicationName))
                {
                    Octopus.FailStep("Application Name parameter is required!");
                }
            }

            if (!string.IsNullOrEmpty(timeOut))
            {

                if (!int.TryParse(timeOut, out intTimeOut))
                {
                    Octopus.FailStep("Time Out parameter must be a numeric value!");
                }
            }

            if (string.IsNullOrEmpty(configFiles))
            {
                Octopus.FailStep("Config File parameter is required!");
            }


            if (string.IsNullOrEmpty(cookieless))
            {
                Octopus.FailStep("Cookieless parameter is required (Choose true or false)!");
            }

            List<string> configList = new List<string>();
            configList = configFiles.Split(';').ToList();

            if (configList.Count == 0)
            {
                Octopus.FailStep("Config File parameter can not be empty!");
            }

            foreach (var config in configList)
            {
                if (!System.IO.File.Exists(config))
                {
                    Octopus.FailStep("Config file not found at location: " + config);
                    break;
                }
                else
                {
                    if (mode.Equals("SQL", StringComparison.CurrentCultureIgnoreCase))
                    {
                        string connection = "Data Source=" + serverName + "; user id=" + userName + ";password=" + password + "; Application Name=" + applicationName;
                        string setting = "<sessionState  timeout=\"" + intTimeOut.ToString() + "\"  mode=\"SQLServer\" sqlConnectionString=\"" + connection + " cookieless=\"" + cookieless + "\"" + " />";
                        System.IO.File.WriteAllText(config, setting);
                    }
                    else if (mode.Equals("InProc", StringComparison.CurrentCultureIgnoreCase))
                    {
                        System.IO.File.WriteAllText(config, "<sessionState  timeout=\"" + intTimeOut.ToString() + "\" />");
                    }
                    else
                    {
                        Octopus.FailStep("Invalid Mode parameter. Only SQL or InProc are allowed");
                        break;

                    }
                }

            }
        }

    }

}