-- ===============================================================================================
-- Consolidated Script Version 3.10SP1
-- Data Changes Zone
-- Please do not deploy your schema changes in this area
-- Please use SQL Prompt format before insert here please
-- Please run after Schema changes....
-- ===============================================================================================

---------------------------------------------------------------------------------------------------------------------------------------------
--Dev Coding: AD-3308 - Adhoc Reporting: Update links for student phone numbers and addresses
---------------------------------------------------------------------------------------------------------------------------------------------
BEGIN

    DECLARE @Error AS INTEGER;
    DECLARE @LeadEntityId AS INTEGER;
    DECLARE @StudentEntityId AS INTEGER;

    SET @Error = 0;

    SET @LeadEntityId = (
                        SELECT ResourceID
                        FROM   dbo.syResources
                        WHERE  Resource = 'Lead'
                               AND ResourceTypeID = 8
                        );
    SET @StudentEntityId = (
                           SELECT ResourceID
                           FROM   dbo.syResources
                           WHERE  Resource = 'Student'
                                  AND ResourceTypeID = 8
                           );

    BEGIN TRANSACTION AdhocReports;
    BEGIN TRY
        ------------------------------------------------------------------------------------------------------
        --LEAD PHONES
        ------------------------------------------------------------------------------------------------------
        IF @Error = 0
            BEGIN
                --Create a new category for Lead Phones
                IF NOT EXISTS (
                              SELECT *
                              FROM   dbo.syFldCategories
                              WHERE  EntityId = @LeadEntityId
                                     AND Descrip = 'Lead Phones'
                              )
                    BEGIN
                        INSERT INTO dbo.syFldCategories (
                                                        EntityId
                                                       ,Descrip
                                                       ,CategoryId
                                                        )
                        VALUES ( @LeadEntityId -- EntityId - smallint
                                ,'Lead Phones' -- Descrip - varchar(50)
                                ,(
                                 SELECT MAX(CategoryId)
                                 FROM   dbo.syFldCategories
                                 ) + 1         -- CategoryId - int
                            );
                    END;

                --Remove the Phone_Best field from the metadata for the adLeadPhone table
                IF EXISTS (
                          SELECT     *
                          FROM       dbo.syTables t
                          INNER JOIN dbo.syTblFlds tf ON tf.TblId = t.TblId
                          INNER JOIN dbo.syFields f ON f.FldId = tf.FldId
                          WHERE      t.TblName = 'adLeadPhone'
                                     AND f.FldName = 'Phone_Best'
                          )
                    BEGIN
                        DELETE FROM dbo.syTblFlds
                        WHERE EXISTS (
                                     SELECT     *
                                     FROM       dbo.syTables t
                                     INNER JOIN dbo.syTblFlds tf ON tf.TblId = t.TblId
                                     INNER JOIN dbo.syFields f ON f.FldId = tf.FldId
                                     WHERE      t.TblName = 'adLeadPhone'
                                                AND f.FldName = 'Phone_Best'
                                                AND dbo.syTblFlds.TblFldsId = tf.TblFldsId
                                     );
                    END;


                --Add the LeadPhoneId if it is missing
                IF NOT EXISTS (
                              SELECT *
                              FROM   dbo.syFields
                              WHERE  FldName = 'LeadPhoneId'
                              )
                    BEGIN
                        INSERT INTO dbo.syFields (
                                                 FldId
                                                ,FldName
                                                ,FldTypeId
                                                ,FldLen
                                                ,DDLId
                                                ,DerivedFld
                                                ,SchlReq
                                                ,LogChanges
                                                ,Mask
                                                 )
                        VALUES ((
                                SELECT MAX(FldId)
                                FROM   dbo.syFields
                                ) + 1         -- FldId - int
                               ,'LeadPhoneId' -- FldName - varchar(200)
                               ,72            -- FldTypeId - int
                               ,16            -- FldLen - int
                               ,NULL          -- DDLId - int
                               ,0             -- DerivedFld - bit
                               ,0             -- SchlReq - bit
                               ,0             -- LogChanges - bit
                               ,NULL          -- Mask - varchar(50)
                            );
                    END;

                --Add the Position field if it is missing
                IF NOT EXISTS (
                              SELECT *
                              FROM   dbo.syFields
                              WHERE  FldName = 'Position'
                              )
                    BEGIN
                        INSERT INTO dbo.syFields (
                                                 FldId
                                                ,FldName
                                                ,FldTypeId
                                                ,FldLen
                                                ,DDLId
                                                ,DerivedFld
                                                ,SchlReq
                                                ,LogChanges
                                                ,Mask
                                                 )
                        VALUES ((
                                SELECT MAX(FldId)
                                FROM   dbo.syFields
                                ) + 1      -- FldId - int
                               ,'Position' -- FldName - varchar(200)
                               ,3          -- FldTypeId - int
                               ,50         -- FldLen - int
                               ,NULL       -- DDLId - int
                               ,0          -- DerivedFld - bit
                               ,0          -- SchlReq - bit
                               ,0          -- LogChanges - bit
                               ,NULL       -- Mask - varchar(50)
                            );
                    END;

                --Add the caption for the Position field
                IF NOT EXISTS (
                              SELECT     *
                              FROM       dbo.syFldCaptions fc
                              INNER JOIN dbo.syFields f ON f.FldId = fc.FldId
                              WHERE      f.FldName = 'Position'
                              )
                    INSERT INTO dbo.syFldCaptions (
                                                  FldCapId
                                                 ,FldId
                                                 ,LangId
                                                 ,Caption
                                                 ,FldDescrip
                                                  )
                    VALUES ((
                            SELECT MAX(FldCapId)
                            FROM   dbo.syFldCaptions
                            ) + 1      -- FldCapId - int
                           ,(
                            SELECT FldId
                            FROM   dbo.syFields
                            WHERE  FldName = 'Position'
                            )          -- FldId - int
                           ,1          -- LangId - tinyint
                           ,'Position' -- Caption - varchar(100)
                           ,NULL       -- FldDescrip - varchar(150)
                        );

                --Add the IsBest field if it is missing
                IF NOT EXISTS (
                              SELECT *
                              FROM   dbo.syFields
                              WHERE  FldName = 'IsBest'
                              )
                    BEGIN
                        INSERT INTO dbo.syFields (
                                                 FldId
                                                ,FldName
                                                ,FldTypeId
                                                ,FldLen
                                                ,DDLId
                                                ,DerivedFld
                                                ,SchlReq
                                                ,LogChanges
                                                ,Mask
                                                 )
                        VALUES ((
                                SELECT MAX(FldId)
                                FROM   dbo.syFields
                                ) + 1    -- FldId - int
                               ,'IsBest' -- FldName - varchar(200)
                               ,11       -- FldTypeId - int
                               ,1        -- FldLen - int
                               ,NULL     -- DDLId - int
                               ,0        -- DerivedFld - bit
                               ,0        -- SchlReq - bit
                               ,0        -- LogChanges - bit
                               ,NULL     -- Mask - varchar(50)
                            );
                    END;

                --Add the caption for the IsBest field
                IF NOT EXISTS (
                              SELECT     *
                              FROM       dbo.syFldCaptions fc
                              INNER JOIN dbo.syFields f ON f.FldId = fc.FldId
                              WHERE      f.FldName = 'IsBest'
                              )
                    INSERT INTO dbo.syFldCaptions (
                                                  FldCapId
                                                 ,FldId
                                                 ,LangId
                                                 ,Caption
                                                 ,FldDescrip
                                                  )
                    VALUES ((
                            SELECT MAX(FldCapId)
                            FROM   dbo.syFldCaptions
                            ) + 1        -- FldCapId - int
                           ,(
                            SELECT FldId
                            FROM   dbo.syFields
                            WHERE  FldName = 'IsBest'
                            )            -- FldId - int
                           ,1            -- LangId - tinyint
                           ,'Best Phone' -- Caption - varchar(100)
                           ,NULL         -- FldDescrip - varchar(150)
                        );

                --Add the IsShowOnLeadPage field if it is missing
                IF NOT EXISTS (
                              SELECT *
                              FROM   dbo.syFields
                              WHERE  FldName = 'IsShowOnLeadPage'
                              )
                    BEGIN
                        INSERT INTO dbo.syFields (
                                                 FldId
                                                ,FldName
                                                ,FldTypeId
                                                ,FldLen
                                                ,DDLId
                                                ,DerivedFld
                                                ,SchlReq
                                                ,LogChanges
                                                ,Mask
                                                 )
                        VALUES ((
                                SELECT MAX(FldId)
                                FROM   dbo.syFields
                                ) + 1              -- FldId - int
                               ,'IsShowOnLeadPage' -- FldName - varchar(200)
                               ,11                 -- FldTypeId - int
                               ,1                  -- FldLen - int
                               ,NULL               -- DDLId - int
                               ,0                  -- DerivedFld - bit
                               ,0                  -- SchlReq - bit
                               ,0                  -- LogChanges - bit
                               ,NULL               -- Mask - varchar(50)
                            );
                    END;

                --Add the caption for the IsShowOnLeadPage field
                IF NOT EXISTS (
                              SELECT     *
                              FROM       dbo.syFldCaptions fc
                              INNER JOIN dbo.syFields f ON f.FldId = fc.FldId
                              WHERE      f.FldName = 'IsShowOnLeadPage'
                              )
                    INSERT INTO dbo.syFldCaptions (
                                                  FldCapId
                                                 ,FldId
                                                 ,LangId
                                                 ,Caption
                                                 ,FldDescrip
                                                  )
                    VALUES ((
                            SELECT MAX(FldCapId)
                            FROM   dbo.syFldCaptions
                            ) + 1               -- FldCapId - int
                           ,(
                            SELECT FldId
                            FROM   dbo.syFields
                            WHERE  FldName = 'IsShowOnLeadPage'
                            )                   -- FldId - int
                           ,1                   -- LangId - tinyint
                           ,'Show on Lead Page' -- Caption - varchar(100)
                           ,NULL                -- FldDescrip - varchar(150)
                        );

                --Fix the caption for the Phone field. It is currently saying Phone - Best.
                --It should just say Phone
                UPDATE     fc
                SET        fc.Caption = 'Phone'
                FROM       dbo.syFldCaptions fc
                INNER JOIN dbo.syFields f ON f.FldId = fc.FldId
                WHERE      f.FldName = 'Phone';


                --Add the fields for Lead Phones
                ---------------------------------
                --LeadPhoneId
                IF NOT EXISTS (
                              SELECT     *
                              FROM       dbo.syTables t
                              INNER JOIN dbo.syTblFlds tf ON tf.TblId = t.TblId
                              INNER JOIN dbo.syFields f ON f.FldId = tf.FldId
                              WHERE      t.TblName = 'adLeadPhone'
                                         AND f.FldName = 'LeadPhoneId'
                              )
                    BEGIN
                        INSERT INTO dbo.syTblFlds (
                                                  TblFldsId
                                                 ,TblId
                                                 ,FldId
                                                 ,CategoryId
                                                 ,FKColDescrip
                                                  )
                        VALUES ((
                                SELECT MAX(TblFldsId)
                                FROM   dbo.syTblFlds
                                ) + 1 -- TblFldsId - int
                               ,(
                                SELECT TblId
                                FROM   dbo.syTables
                                WHERE  TblName = 'adLeadPhone'
                                )     -- TblId - int
                               ,(
                                SELECT FldId
                                FROM   dbo.syFields
                                WHERE  FldName = 'LeadPhoneId'
                                )     -- FldId - int
                               ,NULL  -- CategoryId - int
                               ,NULL  -- FKColDescrip - varchar(50)
                            );
                    END;

                --LeadId
                IF NOT EXISTS (
                              SELECT     *
                              FROM       dbo.syTables t
                              INNER JOIN dbo.syTblFlds tf ON tf.TblId = t.TblId
                              INNER JOIN dbo.syFields f ON f.FldId = tf.FldId
                              WHERE      t.TblName = 'adLeadPhone'
                                         AND f.FldName = 'LeadId'
                              )
                    BEGIN
                        INSERT INTO dbo.syTblFlds (
                                                  TblFldsId
                                                 ,TblId
                                                 ,FldId
                                                 ,CategoryId
                                                 ,FKColDescrip
                                                  )
                        VALUES ((
                                SELECT MAX(TblFldsId)
                                FROM   dbo.syTblFlds
                                ) + 1 -- TblFldsId - int
                               ,(
                                SELECT TblId
                                FROM   dbo.syTables
                                WHERE  TblName = 'adLeadPhone'
                                )     -- TblId - int
                               ,(
                                SELECT FldId
                                FROM   dbo.syFields
                                WHERE  FldName = 'LeadId'
                                )     -- FldId - int
                               ,NULL  -- CategoryId - int
                               ,NULL  -- FKColDescrip - varchar(50)
                            );
                    END;

                --PhoneTypeId
                IF NOT EXISTS (
                              SELECT     *
                              FROM       dbo.syTables t
                              INNER JOIN dbo.syTblFlds tf ON tf.TblId = t.TblId
                              INNER JOIN dbo.syFields f ON f.FldId = tf.FldId
                              WHERE      t.TblName = 'adLeadPhone'
                                         AND f.FldName = 'PhoneTypeId'
                              )
                    BEGIN
                        INSERT INTO dbo.syTblFlds (
                                                  TblFldsId
                                                 ,TblId
                                                 ,FldId
                                                 ,CategoryId
                                                 ,FKColDescrip
                                                  )
                        VALUES ((
                                SELECT MAX(TblFldsId)
                                FROM   dbo.syTblFlds
                                ) + 1              -- TblFldsId - int
                               ,(
                                SELECT TblId
                                FROM   dbo.syTables
                                WHERE  TblName = 'adLeadPhone'
                                )                  -- TblId - int
                               ,(
                                SELECT FldId
                                FROM   dbo.syFields
                                WHERE  FldName = 'PhoneTypeId'
                                )                  -- FldId - int
                               ,(
                                SELECT CategoryId
                                FROM   dbo.syFldCategories
                                WHERE  EntityId = @LeadEntityId
                                       AND Descrip = 'Lead Phones'
                                )                  -- CategoryId - int
                               ,'PhoneTypeDescrip' -- FKColDescrip - varchar(50)
                            );
                    END;

                --Phone
                IF NOT EXISTS (
                              SELECT     *
                              FROM       dbo.syTables t
                              INNER JOIN dbo.syTblFlds tf ON tf.TblId = t.TblId
                              INNER JOIN dbo.syFields f ON f.FldId = tf.FldId
                              WHERE      t.TblName = 'adLeadPhone'
                                         AND f.FldName = 'Phone'
                              )
                    BEGIN
                        INSERT INTO dbo.syTblFlds (
                                                  TblFldsId
                                                 ,TblId
                                                 ,FldId
                                                 ,CategoryId
                                                 ,FKColDescrip
                                                  )
                        VALUES ((
                                SELECT MAX(TblFldsId)
                                FROM   dbo.syTblFlds
                                ) + 1 -- TblFldsId - int
                               ,(
                                SELECT TblId
                                FROM   dbo.syTables
                                WHERE  TblName = 'adLeadPhone'
                                )     -- TblId - int
                               ,(
                                SELECT FldId
                                FROM   dbo.syFields
                                WHERE  FldName = 'Phone'
                                )     -- FldId - int
                               ,(
                                SELECT CategoryId
                                FROM   dbo.syFldCategories
                                WHERE  EntityId = @LeadEntityId
                                       AND Descrip = 'Lead Phones'
                                )     -- CategoryId - int
                               ,NULL  -- FKColDescrip - varchar(50)
                            );
                    END;

                --ModDate
                IF NOT EXISTS (
                              SELECT     *
                              FROM       dbo.syTables t
                              INNER JOIN dbo.syTblFlds tf ON tf.TblId = t.TblId
                              INNER JOIN dbo.syFields f ON f.FldId = tf.FldId
                              WHERE      t.TblName = 'adLeadPhone'
                                         AND f.FldName = 'ModDate'
                              )
                    BEGIN
                        INSERT INTO dbo.syTblFlds (
                                                  TblFldsId
                                                 ,TblId
                                                 ,FldId
                                                 ,CategoryId
                                                 ,FKColDescrip
                                                  )
                        VALUES ((
                                SELECT MAX(TblFldsId)
                                FROM   dbo.syTblFlds
                                ) + 1 -- TblFldsId - int
                               ,(
                                SELECT TblId
                                FROM   dbo.syTables
                                WHERE  TblName = 'adLeadPhone'
                                )     -- TblId - int
                               ,(
                                SELECT FldId
                                FROM   dbo.syFields
                                WHERE  FldName = 'ModDate'
                                )     -- FldId - int
                               ,NULL  -- CategoryId - int
                               ,NULL  -- FKColDescrip - varchar(50)
                            );
                    END;

                --ModUser
                IF NOT EXISTS (
                              SELECT     *
                              FROM       dbo.syTables t
                              INNER JOIN dbo.syTblFlds tf ON tf.TblId = t.TblId
                              INNER JOIN dbo.syFields f ON f.FldId = tf.FldId
                              WHERE      t.TblName = 'adLeadPhone'
                                         AND f.FldName = 'ModUser'
                              )
                    BEGIN
                        INSERT INTO dbo.syTblFlds (
                                                  TblFldsId
                                                 ,TblId
                                                 ,FldId
                                                 ,CategoryId
                                                 ,FKColDescrip
                                                  )
                        VALUES ((
                                SELECT MAX(TblFldsId)
                                FROM   dbo.syTblFlds
                                ) + 1 -- TblFldsId - int
                               ,(
                                SELECT TblId
                                FROM   dbo.syTables
                                WHERE  TblName = 'adLeadPhone'
                                )     -- TblId - int
                               ,(
                                SELECT FldId
                                FROM   dbo.syFields
                                WHERE  FldName = 'ModUser'
                                )     -- FldId - int
                               ,NULL  -- CategoryId - int
                               ,NULL  -- FKColDescrip - varchar(50)
                            );
                    END;

                --Position
                IF NOT EXISTS (
                              SELECT     *
                              FROM       dbo.syTables t
                              INNER JOIN dbo.syTblFlds tf ON tf.TblId = t.TblId
                              INNER JOIN dbo.syFields f ON f.FldId = tf.FldId
                              WHERE      t.TblName = 'adLeadPhone'
                                         AND f.FldName = 'Position'
                              )
                    BEGIN
                        INSERT INTO dbo.syTblFlds (
                                                  TblFldsId
                                                 ,TblId
                                                 ,FldId
                                                 ,CategoryId
                                                 ,FKColDescrip
                                                  )
                        VALUES ((
                                SELECT MAX(TblFldsId)
                                FROM   dbo.syTblFlds
                                ) + 1 -- TblFldsId - int
                               ,(
                                SELECT TblId
                                FROM   dbo.syTables
                                WHERE  TblName = 'adLeadPhone'
                                )     -- TblId - int
                               ,(
                                SELECT FldId
                                FROM   dbo.syFields
                                WHERE  FldName = 'Position'
                                )     -- FldId - int
                               ,(
                                SELECT CategoryId
                                FROM   dbo.syFldCategories
                                WHERE  EntityId = @LeadEntityId
                                       AND Descrip = 'Lead Phones'
                                )     -- CategoryId - int
                               ,NULL  -- FKColDescrip - varchar(50)
                            );
                    END;


                --Extension
                IF NOT EXISTS (
                              SELECT     *
                              FROM       dbo.syTables t
                              INNER JOIN dbo.syTblFlds tf ON tf.TblId = t.TblId
                              INNER JOIN dbo.syFields f ON f.FldId = tf.FldId
                              WHERE      t.TblName = 'adLeadPhone'
                                         AND f.FldName = 'Extension'
                              )
                    BEGIN
                        INSERT INTO dbo.syTblFlds (
                                                  TblFldsId
                                                 ,TblId
                                                 ,FldId
                                                 ,CategoryId
                                                 ,FKColDescrip
                                                  )
                        VALUES ((
                                SELECT MAX(TblFldsId)
                                FROM   dbo.syTblFlds
                                ) + 1 -- TblFldsId - int
                               ,(
                                SELECT TblId
                                FROM   dbo.syTables
                                WHERE  TblName = 'adLeadPhone'
                                )     -- TblId - int
                               ,(
                                SELECT FldId
                                FROM   dbo.syFields
                                WHERE  FldName = 'Extension'
                                )     -- FldId - int
                               ,(
                                SELECT CategoryId
                                FROM   dbo.syFldCategories
                                WHERE  EntityId = @LeadEntityId
                                       AND Descrip = 'Lead Phones'
                                )     -- CategoryId - int
                               ,NULL  -- FKColDescrip - varchar(50)
                            );
                    END;

                --IsForeignPhone
                IF NOT EXISTS (
                              SELECT     *
                              FROM       dbo.syTables t
                              INNER JOIN dbo.syTblFlds tf ON tf.TblId = t.TblId
                              INNER JOIN dbo.syFields f ON f.FldId = tf.FldId
                              WHERE      t.TblName = 'adLeadPhone'
                                         AND f.FldName = 'IsForeignPhone'
                              )
                    BEGIN
                        INSERT INTO dbo.syTblFlds (
                                                  TblFldsId
                                                 ,TblId
                                                 ,FldId
                                                 ,CategoryId
                                                 ,FKColDescrip
                                                  )
                        VALUES ((
                                SELECT MAX(TblFldsId)
                                FROM   dbo.syTblFlds
                                ) + 1 -- TblFldsId - int
                               ,(
                                SELECT TblId
                                FROM   dbo.syTables
                                WHERE  TblName = 'adLeadPhone'
                                )     -- TblId - int
                               ,(
                                SELECT FldId
                                FROM   dbo.syFields
                                WHERE  FldName = 'IsForeignPhone'
                                )     -- FldId - int
                               ,(
                                SELECT CategoryId
                                FROM   dbo.syFldCategories
                                WHERE  EntityId = @LeadEntityId
                                       AND Descrip = 'Lead Phones'
                                )     -- CategoryId - int
                               ,NULL  -- FKColDescrip - varchar(50)
                            );
                    END;

                --IsBest
                IF NOT EXISTS (
                              SELECT     *
                              FROM       dbo.syTables t
                              INNER JOIN dbo.syTblFlds tf ON tf.TblId = t.TblId
                              INNER JOIN dbo.syFields f ON f.FldId = tf.FldId
                              WHERE      t.TblName = 'adLeadPhone'
                                         AND f.FldName = 'IsBest'
                              )
                    BEGIN
                        INSERT INTO dbo.syTblFlds (
                                                  TblFldsId
                                                 ,TblId
                                                 ,FldId
                                                 ,CategoryId
                                                 ,FKColDescrip
                                                  )
                        VALUES ((
                                SELECT MAX(TblFldsId)
                                FROM   dbo.syTblFlds
                                ) + 1 -- TblFldsId - int
                               ,(
                                SELECT TblId
                                FROM   dbo.syTables
                                WHERE  TblName = 'adLeadPhone'
                                )     -- TblId - int
                               ,(
                                SELECT FldId
                                FROM   dbo.syFields
                                WHERE  FldName = 'IsBest'
                                )     -- FldId - int
                               ,(
                                SELECT CategoryId
                                FROM   dbo.syFldCategories
                                WHERE  EntityId = @LeadEntityId
                                       AND Descrip = 'Lead Phones'
                                )     -- CategoryId - int
                               ,NULL  -- FKColDescrip - varchar(50)
                            );
                    END;

                --IsShowOnLeadPage
                IF NOT EXISTS (
                              SELECT     *
                              FROM       dbo.syTables t
                              INNER JOIN dbo.syTblFlds tf ON tf.TblId = t.TblId
                              INNER JOIN dbo.syFields f ON f.FldId = tf.FldId
                              WHERE      t.TblName = 'adLeadPhone'
                                         AND f.FldName = 'IsShowOnLeadPage'
                              )
                    BEGIN
                        INSERT INTO dbo.syTblFlds (
                                                  TblFldsId
                                                 ,TblId
                                                 ,FldId
                                                 ,CategoryId
                                                 ,FKColDescrip
                                                  )
                        VALUES ((
                                SELECT MAX(TblFldsId)
                                FROM   dbo.syTblFlds
                                ) + 1 -- TblFldsId - int
                               ,(
                                SELECT TblId
                                FROM   dbo.syTables
                                WHERE  TblName = 'adLeadPhone'
                                )     -- TblId - int
                               ,(
                                SELECT FldId
                                FROM   dbo.syFields
                                WHERE  FldName = 'IsShowOnLeadPage'
                                )     -- FldId - int
                               ,(
                                SELECT CategoryId
                                FROM   dbo.syFldCategories
                                WHERE  EntityId = @LeadEntityId
                                       AND Descrip = 'Lead Phones'
                                )     -- CategoryId - int
                               ,NULL  -- FKColDescrip - varchar(50)
                            );
                    END;


                --StatusId
                IF NOT EXISTS (
                              SELECT     *
                              FROM       dbo.syTables t
                              INNER JOIN dbo.syTblFlds tf ON tf.TblId = t.TblId
                              INNER JOIN dbo.syFields f ON f.FldId = tf.FldId
                              WHERE      t.TblName = 'adLeadPhone'
                                         AND f.FldName = 'StatusId'
                              )
                    BEGIN
                        INSERT INTO dbo.syTblFlds (
                                                  TblFldsId
                                                 ,TblId
                                                 ,FldId
                                                 ,CategoryId
                                                 ,FKColDescrip
                                                  )
                        VALUES ((
                                SELECT MAX(TblFldsId)
                                FROM   dbo.syTblFlds
                                ) + 1    -- TblFldsId - int
                               ,(
                                SELECT TblId
                                FROM   dbo.syTables
                                WHERE  TblName = 'adLeadPhone'
                                )        -- TblId - int
                               ,(
                                SELECT FldId
                                FROM   dbo.syFields
                                WHERE  FldName = 'StatusId'
                                )        -- FldId - int
                               ,(
                                SELECT CategoryId
                                FROM   dbo.syFldCategories
                                WHERE  EntityId = @LeadEntityId
                                       AND Descrip = 'Lead Phones'
                                )        -- CategoryId - int
                               ,'Status' -- FKColDescrip - varchar(50)
                            );
                    END;


                --Update the CategoryId for the IsForeignPhone field on the adLeadPhone table
                UPDATE     tf
                SET        tf.CategoryId = (
                                           SELECT CategoryId
                                           FROM   dbo.syFldCategories
                                           WHERE  EntityId = @LeadEntityId
                                                  AND Descrip = 'Lead Phones'
                                           )
                FROM       dbo.syTables t
                INNER JOIN dbo.syTblFlds tf ON tf.TblId = t.TblId
                INNER JOIN dbo.syFields f ON f.FldId = tf.FldId
                WHERE      t.TblName = 'adLeadPhone'
                           AND f.FldName = 'IsForeignPhone';

                --Update the CategoryId and FKColDescrip for the PhoneTypeId on the adLeadPhone table
                UPDATE     tf
                SET        tf.CategoryId = (
                                           SELECT CategoryId
                                           FROM   dbo.syFldCategories
                                           WHERE  EntityId = @LeadEntityId
                                                  AND Descrip = 'Lead Phones'
                                           )
                          ,tf.FKColDescrip = 'PhoneTypeDescrip'
                FROM       dbo.syTables t
                INNER JOIN dbo.syTblFlds tf ON tf.TblId = t.TblId
                INNER JOIN dbo.syFields f ON f.FldId = tf.FldId
                WHERE      t.TblName = 'adLeadPhone'
                           AND f.FldName = 'PhoneTypeId';

                --Remove old category from the table
                UPDATE     tf
                SET        tf.CategoryId = NULL
                FROM       dbo.syTables t
                INNER JOIN dbo.syTblFlds tf ON tf.TblId = t.TblId
                INNER JOIN dbo.syFields f ON f.FldId = tf.FldId
                WHERE      t.TblName = 'adLeadPhone'
                           AND tf.CategoryId IS NOT NULL
                           AND tf.CategoryId <> (
                                                SELECT CategoryId
                                                FROM   dbo.syFldCategories
                                                WHERE  EntityId = @LeadEntityId
                                                       AND Descrip = 'Lead Phones'
                                                );


                IF ( @@ERROR > 0 )
                    BEGIN
                        SET @Error = @@ERROR;
                    END;
            END;


        -------------------------------------------------------------------------------------------------------
        --LEAD ADDRESSES
        -------------------------------------------------------------------------------------------------------
        IF @Error = 0
            BEGIN
                --Create a new category for LeadAddresses
                IF NOT EXISTS (
                              SELECT *
                              FROM   dbo.syFldCategories
                              WHERE  EntityId = @LeadEntityId
                                     AND Descrip = 'Lead Addresses'
                              )
                    BEGIN
                        INSERT INTO dbo.syFldCategories (
                                                        EntityId
                                                       ,Descrip
                                                       ,CategoryId
                                                        )
                        VALUES ( @LeadEntityId    -- EntityId - smallint
                                ,'Lead Addresses' -- Descrip - varchar(50)
                                ,(
                                 SELECT MAX(CategoryId)
                                 FROM   dbo.syFldCategories
                                 ) + 1            -- CategoryId - int
                            );
                    END;

                --Add the IsMailingAddress field if it is missing
                IF NOT EXISTS (
                              SELECT *
                              FROM   dbo.syFields
                              WHERE  FldName = 'IsMailingAddress'
                              )
                    BEGIN
                        INSERT INTO dbo.syFields (
                                                 FldId
                                                ,FldName
                                                ,FldTypeId
                                                ,FldLen
                                                ,DDLId
                                                ,DerivedFld
                                                ,SchlReq
                                                ,LogChanges
                                                ,Mask
                                                 )
                        VALUES ((
                                SELECT MAX(FldId)
                                FROM   dbo.syFields
                                ) + 1              -- FldId - int
                               ,'IsMailingAddress' -- FldName - varchar(200)
                               ,11                 -- FldTypeId - int
                               ,1                  -- FldLen - int
                               ,NULL               -- DDLId - int
                               ,0                  -- DerivedFld - bit
                               ,0                  -- SchlReq - bit
                               ,0                  -- LogChanges - bit
                               ,NULL               -- Mask - varchar(50)
                            );
                    END;

                --Add the caption for the IsMailingAddress field
                IF NOT EXISTS (
                              SELECT     *
                              FROM       dbo.syFldCaptions fc
                              INNER JOIN dbo.syFields f ON f.FldId = fc.FldId
                              WHERE      f.FldName = 'IsMailingAddress'
                              )
                    INSERT INTO dbo.syFldCaptions (
                                                  FldCapId
                                                 ,FldId
                                                 ,LangId
                                                 ,Caption
                                                 ,FldDescrip
                                                  )
                    VALUES ((
                            SELECT MAX(FldCapId)
                            FROM   dbo.syFldCaptions
                            ) + 1                 -- FldCapId - int
                           ,(
                            SELECT FldId
                            FROM   dbo.syFields
                            WHERE  FldName = 'IsMailingAddress'
                            )                     -- FldId - int
                           ,1                     -- LangId - tinyint
                           ,'Is Mailing Address?' -- Caption - varchar(100)
                           ,NULL                  -- FldDescrip - varchar(150)
                        );


                --Add new fields to Lead Addresses category
                -------------------------------------------
                --Add the IsMailingAddress field
                IF NOT EXISTS (
                              SELECT     *
                              FROM       dbo.syTables t
                              INNER JOIN dbo.syTblFlds tf ON tf.TblId = t.TblId
                              INNER JOIN dbo.syFields f ON f.FldId = tf.FldId
                              WHERE      t.TblName = 'adLeadAddresses'
                                         AND f.FldName = 'IsMailingAddress'
                              )
                    BEGIN
                        INSERT INTO dbo.syTblFlds (
                                                  TblFldsId
                                                 ,TblId
                                                 ,FldId
                                                 ,CategoryId
                                                 ,FKColDescrip
                                                  )
                        VALUES ((
                                SELECT MAX(TblFldsId)
                                FROM   dbo.syTblFlds
                                ) + 1 -- TblFldsId - int
                               ,(
                                SELECT TblId
                                FROM   dbo.syTables
                                WHERE  TblName = 'adLeadAddresses'
                                )     -- TblId - int
                               ,(
                                SELECT FldId
                                FROM   dbo.syFields
                                WHERE  FldName = 'IsMailingAddress'
                                )     -- FldId - int
                               ,(
                                SELECT CategoryId
                                FROM   dbo.syFldCategories
                                WHERE  EntityId = @LeadEntityId
                                       AND Descrip = 'Lead Addresses'
                                )     -- CategoryId - int
                               ,NULL  -- FKColDescrip - varchar(50)
                            );
                    END;

                --IsShowOnLeadPage
                IF NOT EXISTS (
                              SELECT     *
                              FROM       dbo.syTables t
                              INNER JOIN dbo.syTblFlds tf ON tf.TblId = t.TblId
                              INNER JOIN dbo.syFields f ON f.FldId = tf.FldId
                              WHERE      t.TblName = 'adLeadAddresses'
                                         AND f.FldName = 'IsShowOnLeadPage'
                              )
                    BEGIN
                        INSERT INTO dbo.syTblFlds (
                                                  TblFldsId
                                                 ,TblId
                                                 ,FldId
                                                 ,CategoryId
                                                 ,FKColDescrip
                                                  )
                        VALUES ((
                                SELECT MAX(TblFldsId)
                                FROM   dbo.syTblFlds
                                ) + 1 -- TblFldsId - int
                               ,(
                                SELECT TblId
                                FROM   dbo.syTables
                                WHERE  TblName = 'adLeadAddresses'
                                )     -- TblId - int
                               ,(
                                SELECT FldId
                                FROM   dbo.syFields
                                WHERE  FldName = 'IsShowOnLeadPage'
                                )     -- FldId - int
                               ,(
                                SELECT CategoryId
                                FROM   dbo.syFldCategories
                                WHERE  EntityId = @LeadEntityId
                                       AND Descrip = 'Lead Addresses'
                                )     -- CategoryId - int
                               ,NULL  -- FKColDescrip - varchar(50)
                            );
                    END;

                --All of the fields defined for the adLeadAddresses table metadata needs to be shown for the Lead Addresses category
                UPDATE     tf
                SET        tf.CategoryId = (
                                           SELECT CategoryId
                                           FROM   dbo.syFldCategories
                                           WHERE  EntityId = @LeadEntityId
                                                  AND Descrip = 'Lead Addresses'
                                           )
                FROM       dbo.syTables t
                INNER JOIN dbo.syTblFlds tf ON tf.TblId = t.TblId
                INNER JOIN dbo.syFields f ON f.FldId = tf.FldId
                WHERE      t.TblName = 'adLeadAddresses';

                --Update the FKColDescrip for the lookup fields
                UPDATE     tf
                SET        tf.FKColDescrip = 'StateDescrip'
                FROM       dbo.syTables t
                INNER JOIN dbo.syTblFlds tf ON tf.TblId = t.TblId
                INNER JOIN dbo.syFields f ON f.FldId = tf.FldId
                WHERE      t.TblName = 'adLeadAddresses'
                           AND f.FldName = 'StateId';

                UPDATE     tf
                SET        tf.FKColDescrip = 'CountryDescrip'
                FROM       dbo.syTables t
                INNER JOIN dbo.syTblFlds tf ON tf.TblId = t.TblId
                INNER JOIN dbo.syFields f ON f.FldId = tf.FldId
                WHERE      t.TblName = 'adLeadAddresses'
                           AND f.FldName = 'CountryId';

                UPDATE     tf
                SET        tf.FKColDescrip = 'CountyDescrip'
                FROM       dbo.syTables t
                INNER JOIN dbo.syTblFlds tf ON tf.TblId = t.TblId
                INNER JOIN dbo.syFields f ON f.FldId = tf.FldId
                WHERE      t.TblName = 'adLeadAddresses'
                           AND f.FldName = 'CountyId';

                UPDATE     tf
                SET        tf.FKColDescrip = 'AddressDescrip'
                FROM       dbo.syTables t
                INNER JOIN dbo.syTblFlds tf ON tf.TblId = t.TblId
                INNER JOIN dbo.syFields f ON f.FldId = tf.FldId
                WHERE      t.TblName = 'adLeadAddresses'
                           AND f.FldName = 'AddressTypeId';

                --Remove any old categories from the table
                UPDATE     tf
                SET        tf.CategoryId = NULL
                FROM       dbo.syTables t
                INNER JOIN dbo.syTblFlds tf ON tf.TblId = t.TblId
                INNER JOIN dbo.syFields f ON f.FldId = tf.FldId
                WHERE      t.TblName = 'adLeadAddresses'
                           AND tf.CategoryId IS NOT NULL
                           AND tf.CategoryId <> (
                                                SELECT CategoryId
                                                FROM   dbo.syFldCategories
                                                WHERE  EntityId = @LeadEntityId
                                                       AND Descrip = 'Lead Addresses'
                                                );

                IF ( @@ERROR > 0 )
                    BEGIN
                        SET @Error = @@ERROR;
                    END;
            END;


        -------------------------------------------------------------------------------------------------------
        --STUDENT PHONES
        -------------------------------------------------------------------------------------------------------
        IF @Error = 0
            BEGIN
                --Create a new category for Student Phones
                IF NOT EXISTS (
                              SELECT *
                              FROM   dbo.syFldCategories
                              WHERE  EntityId = @StudentEntityId
                                     AND Descrip = 'Student Phones'
                              )
                    BEGIN
                        INSERT INTO dbo.syFldCategories (
                                                        EntityId
                                                       ,Descrip
                                                       ,CategoryId
                                                        )
                        VALUES ( @StudentEntityId -- EntityId - smallint
                                ,'Student Phones' -- Descrip - varchar(50)
                                ,(
                                 SELECT MAX(CategoryId)
                                 FROM   dbo.syFldCategories
                                 ) + 1            -- CategoryId - int
                            );
                    END;

                --Create the metadata for the new fields for arStudentPhone
                -----------------------------------------------------------
                --Position
                IF NOT EXISTS (
                              SELECT     *
                              FROM       dbo.syTables t
                              INNER JOIN dbo.syTblFlds tf ON tf.TblId = t.TblId
                              INNER JOIN dbo.syFields f ON f.FldId = tf.FldId
                              WHERE      t.TblName = 'arStudentPhone'
                                         AND f.FldName = 'Position'
                              )
                    BEGIN
                        INSERT INTO dbo.syTblFlds (
                                                  TblFldsId
                                                 ,TblId
                                                 ,FldId
                                                 ,CategoryId
                                                 ,FKColDescrip
                                                  )
                        VALUES ((
                                SELECT MAX(TblFldsId)
                                FROM   dbo.syTblFlds
                                ) + 1 -- TblFldsId - int
                               ,(
                                SELECT TblId
                                FROM   dbo.syTables
                                WHERE  TblName = 'arStudentPhone'
                                )     -- TblId - int
                               ,(
                                SELECT FldId
                                FROM   dbo.syFields
                                WHERE  FldName = 'Position'
                                )     -- FldId - int
                               ,(
                                SELECT CategoryId
                                FROM   dbo.syFldCategories
                                WHERE  EntityId = @StudentEntityId
                                       AND Descrip = 'Student Phones'
                                )     -- CategoryId - int
                               ,NULL  -- FKColDescrip - varchar(50)
                            );
                    END;


                --Extension
                IF NOT EXISTS (
                              SELECT     *
                              FROM       dbo.syTables t
                              INNER JOIN dbo.syTblFlds tf ON tf.TblId = t.TblId
                              INNER JOIN dbo.syFields f ON f.FldId = tf.FldId
                              WHERE      t.TblName = 'arStudentPhone'
                                         AND f.FldName = 'Extension'
                              )
                    BEGIN
                        INSERT INTO dbo.syTblFlds (
                                                  TblFldsId
                                                 ,TblId
                                                 ,FldId
                                                 ,CategoryId
                                                 ,FKColDescrip
                                                  )
                        VALUES ((
                                SELECT MAX(TblFldsId)
                                FROM   dbo.syTblFlds
                                ) + 1 -- TblFldsId - int
                               ,(
                                SELECT TblId
                                FROM   dbo.syTables
                                WHERE  TblName = 'arStudentPhone'
                                )     -- TblId - int
                               ,(
                                SELECT FldId
                                FROM   dbo.syFields
                                WHERE  FldName = 'Extension'
                                )     -- FldId - int
                               ,(
                                SELECT CategoryId
                                FROM   dbo.syFldCategories
                                WHERE  EntityId = @StudentEntityId
                                       AND Descrip = 'Student Phones'
                                )     -- CategoryId - int
                               ,NULL  -- FKColDescrip - varchar(50)
                            );
                    END;


                --IsForeignPhone
                IF NOT EXISTS (
                              SELECT     *
                              FROM       dbo.syTables t
                              INNER JOIN dbo.syTblFlds tf ON tf.TblId = t.TblId
                              INNER JOIN dbo.syFields f ON f.FldId = tf.FldId
                              WHERE      t.TblName = 'arStudentPhone'
                                         AND f.FldName = 'IsForeignPhone'
                              )
                    BEGIN
                        INSERT INTO dbo.syTblFlds (
                                                  TblFldsId
                                                 ,TblId
                                                 ,FldId
                                                 ,CategoryId
                                                 ,FKColDescrip
                                                  )
                        VALUES ((
                                SELECT MAX(TblFldsId)
                                FROM   dbo.syTblFlds
                                ) + 1 -- TblFldsId - int
                               ,(
                                SELECT TblId
                                FROM   dbo.syTables
                                WHERE  TblName = 'arStudentPhone'
                                )     -- TblId - int
                               ,(
                                SELECT FldId
                                FROM   dbo.syFields
                                WHERE  FldName = 'IsForeignPhone'
                                )     -- FldId - int
                               ,(
                                SELECT CategoryId
                                FROM   dbo.syFldCategories
                                WHERE  EntityId = @StudentEntityId
                                       AND Descrip = 'Student Phones'
                                )     -- CategoryId - int
                               ,NULL  -- FKColDescrip - varchar(50)
                            );
                    END;

                --IsBest
                IF NOT EXISTS (
                              SELECT     *
                              FROM       dbo.syTables t
                              INNER JOIN dbo.syTblFlds tf ON tf.TblId = t.TblId
                              INNER JOIN dbo.syFields f ON f.FldId = tf.FldId
                              WHERE      t.TblName = 'arStudentPhone'
                                         AND f.FldName = 'IsBest'
                              )
                    BEGIN
                        INSERT INTO dbo.syTblFlds (
                                                  TblFldsId
                                                 ,TblId
                                                 ,FldId
                                                 ,CategoryId
                                                 ,FKColDescrip
                                                  )
                        VALUES ((
                                SELECT MAX(TblFldsId)
                                FROM   dbo.syTblFlds
                                ) + 1 -- TblFldsId - int
                               ,(
                                SELECT TblId
                                FROM   dbo.syTables
                                WHERE  TblName = 'arStudentPhone'
                                )     -- TblId - int
                               ,(
                                SELECT FldId
                                FROM   dbo.syFields
                                WHERE  FldName = 'IsBest'
                                )     -- FldId - int
                               ,(
                                SELECT CategoryId
                                FROM   dbo.syFldCategories
                                WHERE  EntityId = @StudentEntityId
                                       AND Descrip = 'Student Phones'
                                )     -- CategoryId - int
                               ,NULL  -- FKColDescrip - varchar(50)
                            );
                    END;


                --IsShowOnLeadPage
                IF NOT EXISTS (
                              SELECT     *
                              FROM       dbo.syTables t
                              INNER JOIN dbo.syTblFlds tf ON tf.TblId = t.TblId
                              INNER JOIN dbo.syFields f ON f.FldId = tf.FldId
                              WHERE      t.TblName = 'arStudentPhone'
                                         AND f.FldName = 'IsShowOnLeadPage'
                              )
                    BEGIN
                        INSERT INTO dbo.syTblFlds (
                                                  TblFldsId
                                                 ,TblId
                                                 ,FldId
                                                 ,CategoryId
                                                 ,FKColDescrip
                                                  )
                        VALUES ((
                                SELECT MAX(TblFldsId)
                                FROM   dbo.syTblFlds
                                ) + 1 -- TblFldsId - int
                               ,(
                                SELECT TblId
                                FROM   dbo.syTables
                                WHERE  TblName = 'arStudentPhone'
                                )     -- TblId - int
                               ,(
                                SELECT FldId
                                FROM   dbo.syFields
                                WHERE  FldName = 'IsShowOnLeadPage'
                                )     -- FldId - int
                               ,(
                                SELECT CategoryId
                                FROM   dbo.syFldCategories
                                WHERE  EntityId = @StudentEntityId
                                       AND Descrip = 'Student Phones'
                                )     -- CategoryId - int
                               ,NULL  -- FKColDescrip - varchar(50)
                            );
                    END;


                --Update the CategoryId for the PhoneTypeId field on the arStudentTable table
                UPDATE     tf
                SET        tf.CategoryId = (
                                           SELECT CategoryId
                                           FROM   dbo.syFldCategories
                                           WHERE  EntityId = @StudentEntityId
                                                  AND Descrip = 'Student Phones'
                                           )
                FROM       dbo.syTables t
                INNER JOIN dbo.syTblFlds tf ON tf.TblId = t.TblId
                INNER JOIN dbo.syFields f ON f.FldId = tf.FldId
                WHERE      t.TblName = 'arStudentPhone'
                           AND f.FldName = 'PhoneTypeId';


                --Update the CategoryId for the Phone field on the arStudentTable table
                UPDATE     tf
                SET        tf.CategoryId = (
                                           SELECT CategoryId
                                           FROM   dbo.syFldCategories
                                           WHERE  EntityId = @StudentEntityId
                                                  AND Descrip = 'Student Phones'
                                           )
                FROM       dbo.syTables t
                INNER JOIN dbo.syTblFlds tf ON tf.TblId = t.TblId
                INNER JOIN dbo.syFields f ON f.FldId = tf.FldId
                WHERE      t.TblName = 'arStudentPhone'
                           AND f.FldName = 'Phone';

                --Update the CategoryId for the StatusId field on the arStudentTable table
                UPDATE     tf
                SET        tf.CategoryId = (
                                           SELECT CategoryId
                                           FROM   dbo.syFldCategories
                                           WHERE  EntityId = @StudentEntityId
                                                  AND Descrip = 'Student Phones'
                                           )
                FROM       dbo.syTables t
                INNER JOIN dbo.syTblFlds tf ON tf.TblId = t.TblId
                INNER JOIN dbo.syFields f ON f.FldId = tf.FldId
                WHERE      t.TblName = 'arStudentPhone'
                           AND f.FldName = 'StatusId';

                --Remove any old categories from the table
                UPDATE     tf
                SET        tf.CategoryId = NULL
                FROM       dbo.syTables t
                INNER JOIN dbo.syTblFlds tf ON tf.TblId = t.TblId
                INNER JOIN dbo.syFields f ON f.FldId = tf.FldId
                WHERE      t.TblName = 'arStudentPhone'
                           AND tf.CategoryId IS NOT NULL
                           AND tf.CategoryId <> (
                                                SELECT CategoryId
                                                FROM   dbo.syFldCategories
                                                WHERE  EntityId = @StudentEntityId
                                                       AND Descrip = 'Student Phones'
                                                );


                IF ( @@ERROR > 0 )
                    BEGIN
                        SET @Error = @@ERROR;
                    END;
            END;


        -------------------------------------------------------------------------------------------------------
        --STUDENT ADDRESSES
        -------------------------------------------------------------------------------------------------------
        IF @Error = 0
            BEGIN
                --Create a new category for Student Addresses
                IF NOT EXISTS (
                              SELECT *
                              FROM   dbo.syFldCategories
                              WHERE  EntityId = @StudentEntityId
                                     AND Descrip = 'Student Addresses'
                              )
                    BEGIN
                        INSERT INTO dbo.syFldCategories (
                                                        EntityId
                                                       ,Descrip
                                                       ,CategoryId
                                                        )
                        VALUES ( @StudentEntityId    -- EntityId - smallint
                                ,'Student Addresses' -- Descrip - varchar(50)
                                ,(
                                 SELECT MAX(CategoryId)
                                 FROM   dbo.syFldCategories
                                 ) + 1               -- CategoryId - int
                            );
                    END;

                --Create the ForeignCountyStr field if it is missing
                IF NOT EXISTS (
                              SELECT *
                              FROM   dbo.syFields
                              WHERE  FldName = 'ForeignCountyStr'
                              )
                    BEGIN
                        INSERT INTO dbo.syFields (
                                                 FldId
                                                ,FldName
                                                ,FldTypeId
                                                ,FldLen
                                                ,DDLId
                                                ,DerivedFld
                                                ,SchlReq
                                                ,LogChanges
                                                ,Mask
                                                 )
                        VALUES ((
                                SELECT MAX(FldId)
                                FROM   syFields
                                ) + 1              -- FldId - int
                               ,'ForeignCountyStr' -- FldName - varchar(200)
                               ,200                -- FldTypeId - int
                               ,100                -- FldLen - int
                               ,NULL               -- DDLId - int
                               ,0                  -- DerivedFld - bit
                               ,0                  -- SchlReq - bit
                               ,0                  -- LogChanges - bit
                               ,NULL               -- Mask - varchar(50)
                            );
                    END;


                --Create the ForeignCountryStr field if it is missing
                IF NOT EXISTS (
                              SELECT *
                              FROM   dbo.syFields
                              WHERE  FldName = 'ForeignCountryStr'
                              )
                    BEGIN
                        INSERT INTO dbo.syFields (
                                                 FldId
                                                ,FldName
                                                ,FldTypeId
                                                ,FldLen
                                                ,DDLId
                                                ,DerivedFld
                                                ,SchlReq
                                                ,LogChanges
                                                ,Mask
                                                 )
                        VALUES ((
                                SELECT MAX(FldId)
                                FROM   syFields
                                ) + 1               -- FldId - int
                               ,'ForeignCountryStr' -- FldName - varchar(200)
                               ,200                 -- FldTypeId - int
                               ,100                 -- FldLen - int
                               ,NULL                -- DDLId - int
                               ,0                   -- DerivedFld - bit
                               ,0                   -- SchlReq - bit
                               ,0                   -- LogChanges - bit
                               ,NULL                -- Mask - varchar(50)
                            );
                    END;

                --Create the AddressApto field if it is missing
                IF NOT EXISTS (
                              SELECT *
                              FROM   dbo.syFields
                              WHERE  FldName = 'AddressApto'
                              )
                    BEGIN
                        INSERT INTO dbo.syFields (
                                                 FldId
                                                ,FldName
                                                ,FldTypeId
                                                ,FldLen
                                                ,DDLId
                                                ,DerivedFld
                                                ,SchlReq
                                                ,LogChanges
                                                ,Mask
                                                 )
                        VALUES ((
                                SELECT MAX(FldId)
                                FROM   syFields
                                ) + 1         -- FldId - int
                               ,'AddressApto' -- FldName - varchar(200)
                               ,200           -- FldTypeId - int
                               ,20            -- FldLen - int
                               ,NULL          -- DDLId - int
                               ,0             -- DerivedFld - bit
                               ,0             -- SchlReq - bit
                               ,0             -- LogChanges - bit
                               ,NULL          -- Mask - varchar(50)
                            );
                    END;

                --Create the metadata for the new fields for arStudAddresses
                -----------------------------------------------------------
                --IsMailingAddress
                IF NOT EXISTS (
                              SELECT     *
                              FROM       dbo.syTables t
                              INNER JOIN dbo.syTblFlds tf ON tf.TblId = t.TblId
                              INNER JOIN dbo.syFields f ON f.FldId = tf.FldId
                              WHERE      t.TblName = 'arStudAddresses'
                                         AND f.FldName = 'IsMailingAddress'
                              )
                    BEGIN
                        INSERT INTO dbo.syTblFlds (
                                                  TblFldsId
                                                 ,TblId
                                                 ,FldId
                                                 ,CategoryId
                                                 ,FKColDescrip
                                                  )
                        VALUES ((
                                SELECT MAX(TblFldsId)
                                FROM   dbo.syTblFlds
                                ) + 1 -- TblFldsId - int
                               ,(
                                SELECT TblId
                                FROM   dbo.syTables
                                WHERE  TblName = 'arStudAddresses'
                                )     -- TblId - int
                               ,(
                                SELECT FldId
                                FROM   dbo.syFields
                                WHERE  FldName = 'IsMailingAddress'
                                )     -- FldId - int
                               ,(
                                SELECT CategoryId
                                FROM   dbo.syFldCategories
                                WHERE  EntityId = @StudentEntityId
                                       AND Descrip = 'Student Addresses'
                                )     -- CategoryId - int
                               ,NULL  -- FKColDescrip - varchar(50)
                            );
                    END;


                --IsShowOnLeadPage
                IF NOT EXISTS (
                              SELECT     *
                              FROM       dbo.syTables t
                              INNER JOIN dbo.syTblFlds tf ON tf.TblId = t.TblId
                              INNER JOIN dbo.syFields f ON f.FldId = tf.FldId
                              WHERE      t.TblName = 'arStudAddresses'
                                         AND f.FldName = 'IsShowOnLeadPage'
                              )
                    BEGIN
                        INSERT INTO dbo.syTblFlds (
                                                  TblFldsId
                                                 ,TblId
                                                 ,FldId
                                                 ,CategoryId
                                                 ,FKColDescrip
                                                  )
                        VALUES ((
                                SELECT MAX(TblFldsId)
                                FROM   dbo.syTblFlds
                                ) + 1 -- TblFldsId - int
                               ,(
                                SELECT TblId
                                FROM   dbo.syTables
                                WHERE  TblName = 'arStudAddresses'
                                )     -- TblId - int
                               ,(
                                SELECT FldId
                                FROM   dbo.syFields
                                WHERE  FldName = 'IsShowOnLeadPage'
                                )     -- FldId - int
                               ,(
                                SELECT CategoryId
                                FROM   dbo.syFldCategories
                                WHERE  EntityId = @StudentEntityId
                                       AND Descrip = 'Student Addresses'
                                )     -- CategoryId - int
                               ,NULL  -- FKColDescrip - varchar(50)
                            );
                    END;


                --IsInternational
                IF NOT EXISTS (
                              SELECT     *
                              FROM       dbo.syTables t
                              INNER JOIN dbo.syTblFlds tf ON tf.TblId = t.TblId
                              INNER JOIN dbo.syFields f ON f.FldId = tf.FldId
                              WHERE      t.TblName = 'arStudAddresses'
                                         AND f.FldName = 'IsInternational'
                              )
                    BEGIN
                        INSERT INTO dbo.syTblFlds (
                                                  TblFldsId
                                                 ,TblId
                                                 ,FldId
                                                 ,CategoryId
                                                 ,FKColDescrip
                                                  )
                        VALUES ((
                                SELECT MAX(TblFldsId)
                                FROM   dbo.syTblFlds
                                ) + 1 -- TblFldsId - int
                               ,(
                                SELECT TblId
                                FROM   dbo.syTables
                                WHERE  TblName = 'arStudAddresses'
                                )     -- TblId - int
                               ,(
                                SELECT FldId
                                FROM   dbo.syFields
                                WHERE  FldName = 'IsInternational'
                                )     -- FldId - int
                               ,(
                                SELECT CategoryId
                                FROM   dbo.syFldCategories
                                WHERE  EntityId = @StudentEntityId
                                       AND Descrip = 'Student Addresses'
                                )     -- CategoryId - int
                               ,NULL  -- FKColDescrip - varchar(50)
                            );
                    END;


                --CountyId
                IF NOT EXISTS (
                              SELECT     *
                              FROM       dbo.syTables t
                              INNER JOIN dbo.syTblFlds tf ON tf.TblId = t.TblId
                              INNER JOIN dbo.syFields f ON f.FldId = tf.FldId
                              WHERE      t.TblName = 'arStudAddresses'
                                         AND f.FldName = 'CountyId'
                              )
                    BEGIN
                        INSERT INTO dbo.syTblFlds (
                                                  TblFldsId
                                                 ,TblId
                                                 ,FldId
                                                 ,CategoryId
                                                 ,FKColDescrip
                                                  )
                        VALUES ((
                                SELECT MAX(TblFldsId)
                                FROM   dbo.syTblFlds
                                ) + 1           -- TblFldsId - int
                               ,(
                                SELECT TblId
                                FROM   dbo.syTables
                                WHERE  TblName = 'arStudAddresses'
                                )               -- TblId - int
                               ,(
                                SELECT FldId
                                FROM   dbo.syFields
                                WHERE  FldName = 'CountyId'
                                )               -- FldId - int
                               ,(
                                SELECT CategoryId
                                FROM   dbo.syFldCategories
                                WHERE  EntityId = @StudentEntityId
                                       AND Descrip = 'Student Addresses'
                                )               -- CategoryId - int
                               ,'CountyDescrip' -- FKColDescrip - varchar(50)
                            );
                    END;


                --ForeignCountyStr
                IF NOT EXISTS (
                              SELECT     *
                              FROM       dbo.syTables t
                              INNER JOIN dbo.syTblFlds tf ON tf.TblId = t.TblId
                              INNER JOIN dbo.syFields f ON f.FldId = tf.FldId
                              WHERE      t.TblName = 'arStudAddresses'
                                         AND f.FldName = 'ForeignCountyStr'
                              )
                    BEGIN
                        INSERT INTO dbo.syTblFlds (
                                                  TblFldsId
                                                 ,TblId
                                                 ,FldId
                                                 ,CategoryId
                                                 ,FKColDescrip
                                                  )
                        VALUES ((
                                SELECT MAX(TblFldsId)
                                FROM   dbo.syTblFlds
                                ) + 1 -- TblFldsId - int
                               ,(
                                SELECT TblId
                                FROM   dbo.syTables
                                WHERE  TblName = 'arStudAddresses'
                                )     -- TblId - int
                               ,(
                                SELECT FldId
                                FROM   dbo.syFields
                                WHERE  FldName = 'ForeignCountyStr'
                                )     -- FldId - int
                               ,(
                                SELECT CategoryId
                                FROM   dbo.syFldCategories
                                WHERE  EntityId = @StudentEntityId
                                       AND Descrip = 'Student Addresses'
                                )     -- CategoryId - int
                               ,NULL  -- FKColDescrip - varchar(50)
                            );
                    END;

                --ForeignCountryStr
                IF NOT EXISTS (
                              SELECT     *
                              FROM       dbo.syTables t
                              INNER JOIN dbo.syTblFlds tf ON tf.TblId = t.TblId
                              INNER JOIN dbo.syFields f ON f.FldId = tf.FldId
                              WHERE      t.TblName = 'arStudAddresses'
                                         AND f.FldName = 'ForeignCountryStr'
                              )
                    BEGIN
                        INSERT INTO dbo.syTblFlds (
                                                  TblFldsId
                                                 ,TblId
                                                 ,FldId
                                                 ,CategoryId
                                                 ,FKColDescrip
                                                  )
                        VALUES ((
                                SELECT MAX(TblFldsId)
                                FROM   dbo.syTblFlds
                                ) + 1 -- TblFldsId - int
                               ,(
                                SELECT TblId
                                FROM   dbo.syTables
                                WHERE  TblName = 'arStudAddresses'
                                )     -- TblId - int
                               ,(
                                SELECT FldId
                                FROM   dbo.syFields
                                WHERE  FldName = 'ForeignCountryStr'
                                )     -- FldId - int
                               ,(
                                SELECT CategoryId
                                FROM   dbo.syFldCategories
                                WHERE  EntityId = @StudentEntityId
                                       AND Descrip = 'Student Addresses'
                                )     -- CategoryId - int
                               ,NULL  -- FKColDescrip - varchar(50)
                            );
                    END;

                --AddressApto
                IF NOT EXISTS (
                              SELECT     *
                              FROM       dbo.syTables t
                              INNER JOIN dbo.syTblFlds tf ON tf.TblId = t.TblId
                              INNER JOIN dbo.syFields f ON f.FldId = tf.FldId
                              WHERE      t.TblName = 'arStudAddresses'
                                         AND f.FldName = 'AddressApto'
                              )
                    BEGIN
                        INSERT INTO dbo.syTblFlds (
                                                  TblFldsId
                                                 ,TblId
                                                 ,FldId
                                                 ,CategoryId
                                                 ,FKColDescrip
                                                  )
                        VALUES ((
                                SELECT MAX(TblFldsId)
                                FROM   dbo.syTblFlds
                                ) + 1 -- TblFldsId - int
                               ,(
                                SELECT TblId
                                FROM   dbo.syTables
                                WHERE  TblName = 'arStudAddresses'
                                )     -- TblId - int
                               ,(
                                SELECT FldId
                                FROM   dbo.syFields
                                WHERE  FldName = 'AddressApto'
                                )     -- FldId - int
                               ,(
                                SELECT CategoryId
                                FROM   dbo.syFldCategories
                                WHERE  EntityId = @StudentEntityId
                                       AND Descrip = 'Student Addresses'
                                )     -- CategoryId - int
                               ,NULL  -- FKColDescrip - varchar(50)
                            );
                    END;


                --Update the CategoryId for the new fields on the arStudAddresses table
                UPDATE     tf
                SET        tf.CategoryId = (
                                           SELECT CategoryId
                                           FROM   dbo.syFldCategories
                                           WHERE  EntityId = @StudentEntityId
                                                  AND Descrip = 'Student Addresses'
                                           )
                FROM       dbo.syTables t
                INNER JOIN dbo.syTblFlds tf ON tf.TblId = t.TblId
                INNER JOIN dbo.syFields f ON f.FldId = tf.FldId
                WHERE      t.TblName = 'arStudAddresses'
                           AND f.FldName IN ( 'StatusId', 'Address1', 'Address2', 'City', 'StateId', 'Zip', 'CountryId', 'AddressTypeId', '' );


                --Create a record for the arStudAddresses table and CountyId field in syRptAdhocRelations
                --If this is not done the value for the CouuntyId field will be GUID instead of the CountyDescrip
                IF NOT EXISTS (
                              SELECT *
                              FROM   dbo.syRptAdhocRelations
                              WHERE  FkTable = 'arStudAddresses'
                                     AND FkColumn = 'CountyId'
                              )
                    BEGIN
                        INSERT INTO dbo.syRptAdhocRelations (
                                                            FkTable
                                                           ,FkColumn
                                                           ,PkTable
                                                           ,PkColumn
                                                            )
                        VALUES ( N'arStudAddresses' -- FkTable - nvarchar(100)
                                ,N'CountyId'        -- FkColumn - nvarchar(100)
                                ,N'adCounties'      -- PkTable - nvarchar(100)
                                ,N'CountyId'        -- PkColumn - nvarchar(100)
                            );

                    END;


                --Remove any old categories from the table
                UPDATE     tf
                SET        tf.CategoryId = NULL
                FROM       dbo.syTables t
                INNER JOIN dbo.syTblFlds tf ON tf.TblId = t.TblId
                INNER JOIN dbo.syFields f ON f.FldId = tf.FldId
                WHERE      t.TblName = 'arStudAddresses'
                           AND tf.CategoryId IS NOT NULL
                           AND tf.CategoryId <> (
                                                SELECT CategoryId
                                                FROM   dbo.syFldCategories
                                                WHERE  EntityId = @StudentEntityId
                                                       AND Descrip = 'Student Addresses'
                                                );

                IF ( @@ERROR > 0 )
                    BEGIN
                        SET @Error = @@ERROR;
                    END;
            END;


        -------------------------------------------------------------------------------------------------------
        --REMOVE THE OLD FIELDS FROM THE GENERAL INFORMATION CATEGORY FOR LEADS
        -------------------------------------------------------------------------------------------------------
        UPDATE     tf
        SET        tf.CategoryId = NULL
        FROM       dbo.syFldCategories fc
        INNER JOIN dbo.syTblFlds tf ON tf.CategoryId = fc.CategoryId
        INNER JOIN dbo.syTables t ON t.TblId = tf.TblId
        INNER JOIN dbo.syFields f ON f.FldId = tf.FldId
        WHERE      fc.EntityId = 395
                   AND fc.Descrip = 'General Information'
                   AND t.TblName = 'adLeadsView'
                   AND f.FldName NOT IN ( 'Email_Best', '[Email]' );




    END TRY
    BEGIN CATCH
        DECLARE @msg NVARCHAR(MAX);
        DECLARE @severity INT;
        DECLARE @state INT;
        SELECT @msg = ERROR_MESSAGE()
              ,@severity = ERROR_SEVERITY()
              ,@state = ERROR_STATE();
        RAISERROR(@msg, @severity, @state);
        SET @Error = 1;
    END CATCH;

    IF ( @Error = 0 )
        BEGIN
            COMMIT TRANSACTION AdhocReports;
        END;
    ELSE
        BEGIN
            ROLLBACK TRANSACTION AdhocReports;
        END;
END;
GO
---------------------------------------------------------------------------------------------------------------------------------------------
--END Dev Coding: AD-3308 - Adhoc Reporting: Update links for student phone numbers and addresses
---------------------------------------------------------------------------------------------------------------------------------------------

