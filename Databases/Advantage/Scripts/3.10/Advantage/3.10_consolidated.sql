-- ===============================================================================================
-- Consolidated Script Version 3.10
-- Data Changes Zone
-- Please do not deploy your schema changes in this area
-- Please use SQL Prompt format before insert here please
-- Please run after Schema changes....
-- ===============================================================================================

--=================================================================================================
-- US 11000: (JAGG) TECH Initial Consolidated Scrip Changes for Advantage API
--=================================================================================================
--=================================================================================================
-- Find all the system roles where RoleTypeId is null and set them to 1 in sySysRoles table
--=================================================================================================
UPDATE sySysRoles
SET    RoleTypeId = 1
WHERE  RoleTypeId IS NULL;
GO
--=================================================================================================
-- Insert the in sySysRoles table the record for "Admission Api" with RoleTypeId = 2 and SysRoleId = 17
--=================================================================================================
IF NOT EXISTS (
              SELECT 1
              FROM   dbo.sySysRoles
              WHERE  SysRoleId = 17
              )
    BEGIN
        INSERT INTO dbo.sySysRoles (
                                   SysRoleId
                                  ,Descrip
                                  ,StatusId
                                  ,ModUser
                                  ,ModDate
                                  ,RoleTypeId
                                  ,Permission
                                   )
        VALUES ( 17                                     -- SysRoleId - int
                ,'Admission Api'                        -- Descrip - varchar(80)
                ,'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' -- StatusId - uniqueidentifier
                ,'SUPPORT'                              -- ModUser - varchar(50)
                ,GETDATE()                              -- ModDate - datetime
                ,2
                ,NULL
               );
    END;
ELSE
    BEGIN
        UPDATE SSR
        SET    SSR.Descrip = 'Admission Api'
        FROM   sySysRoles AS SSR
        WHERE  SSR.Descrip <> 'Admission Api'
               AND SSR.SysRoleId = 17;
    END;
GO
--=================================================================================================
-- Insert a record to SyConfigSettings for the AdvantageApiURL
--=================================================================================================
DECLARE @KeyName VARCHAR(50) = 'AdvantageApiURL';

IF NOT EXISTS (
              SELECT 1
              FROM   syConfigAppSettings
              WHERE  KeyName = @KeyName
              )
    BEGIN
        BEGIN TRANSACTION TEMP1;
        DECLARE @MaxId INT = (
                             SELECT MAX(SettingId)
                             FROM   dbo.syConfigAppSettings
                             );
        SET @MaxId = @MaxId + 1;

        INSERT INTO dbo.syConfigAppSettings (
                                            SettingId
                                           ,KeyName
                                           ,Description
                                           ,ModUser
                                           ,ModDate
                                           ,CampusSpecific
                                           ,ExtraConfirmation
                                            )
        VALUES ( @MaxId              -- SettingId - int
                ,@KeyName            -- KeyName - varchar(200)
                ,'Advantage Api URL' -- Description - varchar(1000)
                ,'Support'           -- ModUser - varchar(50)
                ,GETDATE()           -- ModDate - datetime
                ,0                   -- CampusSpecific - bit
                ,0                   -- ExtraConfirmation - bit
               );

        INSERT INTO dbo.syConfigAppSetValues (
                                             ValueId
                                            ,SettingId
                                            ,CampusId
                                            ,Value
                                            ,ModUser
                                            ,ModDate
                                            ,Active
                                             )
        VALUES ( NEWID()                             -- ValueId - uniqueidentifier
                ,@MaxId                              -- SettingId - int
                ,NULL                                -- CampusId - uniqueidentifier
                ,'http://localhost/current/fame/advantage/api' -- Value - varchar(1000)
                ,'Support'                           -- ModUser - varchar(50)
                ,GETDATE()                           -- ModDate - datetime
                ,1                                   -- Active - bit
               );

        COMMIT TRANSACTION TEMP1;
    END;
GO
--=================================================================================================
-- Insert a record to SyConfigSettings for the TermsOfUseVersionNumber
--=================================================================================================
DECLARE @KeyName VARCHAR(50) = 'TermsOfUseVersionNumber';

IF NOT EXISTS (
              SELECT *
              FROM   syConfigAppSettings
              WHERE  KeyName = @KeyName
              )
    BEGIN
        BEGIN TRANSACTION TEMP2;
        DECLARE @MaxId INT = (
                             SELECT MAX(SettingId)
                             FROM   dbo.syConfigAppSettings
                             );
        SET @MaxId = @MaxId + 1;

        INSERT INTO dbo.syConfigAppSettings (
                                            SettingId
                                           ,KeyName
                                           ,Description
                                           ,ModUser
                                           ,ModDate
                                           ,CampusSpecific
                                           ,ExtraConfirmation
                                            )
        VALUES ( @MaxId                                          -- SettingId - int
                ,@KeyName                                        -- KeyName - varchar(200)
                ,'Terms Of Use Version Number: format vYYYYMMDD' -- Description - varchar(1000)
                ,'Support'                                       -- ModUser - varchar(50)
                ,GETDATE()                                       -- ModDate - datetime
                ,0                                               -- CampusSpecific - bit
                ,0                                               -- ExtraConfirmation - bit
               );

        INSERT INTO dbo.syConfigAppSetValues (
                                             ValueId
                                            ,SettingId
                                            ,CampusId
                                            ,Value
                                            ,ModUser
                                            ,ModDate
                                            ,Active
                                             )
        VALUES ( NEWID()     -- ValueId - uniqueidentifier
                ,@MaxId      -- SettingId - int
                ,NULL        -- CampusId - uniqueidentifier
                ,'v20170830' -- Value - varchar(1000)
                ,'Support'   -- ModUser - varchar(50)
                ,GETDATE()   -- ModDate - datetime
                ,1           -- Active - bit
               );

        COMMIT TRANSACTION TEMP2;
    END;
GO
-- ===============================================================================================
-- END US 11000: --   Consolidated Script Version 3.10
-- ===============================================================================================

--=================================================================================================
-- US11001 Create the update script for the permission for the existing users as per the role
--=================================================================================================
BEGIN TRANSACTION rolePermissions;
BEGIN TRY
    DECLARE @error AS INT;
    SET @error = 0;
    DECLARE @fldId INT;

    BEGIN
        UPDATE dbo.sySysRoles
        SET    Permission = N'{"modules": [
									{"name": "AR","level": "modify"}, {"name": "AD","level": "modify"}, 
									{"name": "FC","level": "modify"}, {"name": "FA","level": "modify"}, 
									{"name": "HR","level": "modify"}, {"name": "PL","level": "modify"}, 
									{"name": "SA","level": "modify"}, {"name": "SY","level": "delete"} ]}'-- Permission - nvarchar(max)
        WHERE  SysRoleId = 1
               AND Permission IS NULL; -- System Administrator

        UPDATE dbo.sySysRoles
        SET    Permission = N'{"modules": [
									{"name": "AR","level": "none"}, {"name": "AD","level": "none"}, 
									{"name": "FC","level": "modify"}, {"name": "FA","level": "none"}, 
									{"name": "HR","level": "none"}, {"name": "PL","level": "none"}, 
									{"name": "SA","level": "none"}, {"name": "SY","level": "none"} ]}'-- Permission - nvarchar(max)
        WHERE  SysRoleId = 2
               AND Permission IS NULL; -- Instructors


        UPDATE dbo.sySysRoles
        SET    Permission = N'{"modules": [
									{"name": "AR","level": "none"}, {"name": "AD","level": "modify"}, 
									{"name": "FC","level": "none"}, {"name": "FA","level": "none"}, 
									{"name": "HR","level": "none"}, {"name": "PL","level": "none"}, 
									{"name": "SA","level": "none"}, {"name": "SY","level": "none"} ]}'-- Permission - nvarchar(max)
        WHERE  SysRoleId = 3
               AND Permission IS NULL; -- Admission Reps


        UPDATE dbo.sySysRoles
        SET    Permission = N'{"modules": [
									{"name": "AR","level": "modify"}, {"name": "AD","level": "none"}, 
									{"name": "FC","level": "none"}, {"name": "FA","level": "none"}, 
									{"name": "HR","level": "none"}, {"name": "PL","level": "none"}, 
									{"name": "SA","level": "none"}, {"name": "SY","level": "none"} ]}'-- Permission - nvarchar(max)
        WHERE  SysRoleId = 4
               AND Permission IS NULL; -- Academic Advisors


        UPDATE dbo.sySysRoles
        SET    Permission = N'{"modules": [
									{"name": "AR","level": "modify"}, {"name": "AD","level": "modify"}, 
									{"name": "FC","level": "modify"}, {"name": "FA","level": "modify"}, 
									{"name": "HR","level": "modify"}, {"name": "PL","level": "modify"}, 
									{"name": "SA","level": "modify"}, {"name": "SY","level": "modify"} ]}'-- Permission - nvarchar(max)
        WHERE  SysRoleId = 5
               AND Permission IS NULL; -- Other


        UPDATE dbo.sySysRoles
        SET    Permission = N'{"modules": [
									{"name": "AR","level": "none"}, {"name": "AD","level": "none"}, 
									{"name": "FC","level": "none"}, {"name": "FA","level": "none"}, 
									{"name": "HR","level": "none"}, {"name": "PL","level": "modify"}, 
									{"name": "SA","level": "none"}, {"name": "SY","level": "none"} ]}'-- Permission - nvarchar(max)
        WHERE  SysRoleId = 6
               AND Permission IS NULL; -- Placement Reps


        UPDATE dbo.sySysRoles
        SET    Permission = N'{"modules": [
									{"name": "AR","level": "none"}, {"name": "AD","level": "none"}, 
									{"name": "FC","level": "none"}, {"name": "FA","level": "modify"}, 
									{"name": "HR","level": "none"}, {"name": "PL","level": "none"}, 
									{"name": "SA","level": "none"}, {"name": "SY","level": "none"} ]}'-- Permission - nvarchar(max)
        WHERE  SysRoleId = 7
               AND Permission IS NULL; -- Financial Aid Advisor


        UPDATE dbo.sySysRoles
        SET    Permission = N'{"modules": [
									{"name": "AR","level": "none"}, {"name": "AD","level": "delete"}, 
									{"name": "FC","level": "none"}, {"name": "FA","level": "none"}, 
									{"name": "HR","level": "none"}, {"name": "PL","level": "none"}, 
									{"name": "SA","level": "none"}, {"name": "SY","level": "none"} ]}'-- Permission - nvarchar(max)
        WHERE  SysRoleId = 8
               AND Permission IS NULL; -- Director Of Admissions


        UPDATE dbo.sySysRoles
        SET    Permission = N'{"modules": [
									{"name": "AR","level": "none"}, {"name": "AD","level": "none"}, 
									{"name": "FC","level": "none"}, {"name": "FA","level": "delete"}, 
									{"name": "HR","level": "none"}, {"name": "PL","level": "none"}, 
									{"name": "SA","level": "none"}, {"name": "SY","level": "none"} ]}'-- Permission - nvarchar(max)
        WHERE  SysRoleId = 9
               AND Permission IS NULL; -- Director of Financial Aid


        UPDATE dbo.sySysRoles
        SET    Permission = N'{"modules": [
									{"name": "AR","level": "none"}, {"name": "AD","level": "none"}, 
									{"name": "FC","level": "none"}, {"name": "FA","level": "none"}, 
									{"name": "HR","level": "none"}, {"name": "PL","level": "none"}, 
									{"name": "SA","level": "delete"}, {"name": "SY","level": "none"} ]}'-- Permission - nvarchar(max)
        WHERE  SysRoleId = 10
               AND Permission IS NULL; -- Director of Business Office


        UPDATE dbo.sySysRoles
        SET    Permission = N'{"modules": [
									{"name": "AR","level": "none"}, {"name": "AD","level": "none"}, 
									{"name": "FC","level": "none"}, {"name": "FA","level": "modify"}, 
									{"name": "HR","level": "none"}, {"name": "PL","level": "none"}, 
									{"name": "SA","level": "none"}, {"name": "SY","level": "none"} ]}'-- Permission - nvarchar(max)
        WHERE  SysRoleId = 11
               AND Permission IS NULL; -- Instructors Supervisor


        UPDATE dbo.sySysRoles
        SET    Permission = N'{"modules": [
									{"name": "AR","level": "modify"}, {"name": "AD","level": "modify"}, 
									{"name": "FC","level": "modify"}, {"name": "FA","level": "modify"}, 
									{"name": "HR","level": "none"}, {"name": "PL","level": "modify"}, 
									{"name": "SA","level": "modify"}, {"name": "SY","level": "none"} ]}'-- Permission - nvarchar(max)
        WHERE  SysRoleId = 12
               AND Permission IS NULL; -- Front Desk


        UPDATE dbo.sySysRoles
        SET    Permission = N'{"modules": [
									{"name": "AR","level": "delete"}, {"name": "AD","level": "none"}, 
									{"name": "FC","level": "none"}, {"name": "FA","level": "none"}, 
									{"name": "HR","level": "none"}, {"name": "PL","level": "none"}, 
									{"name": "SA","level": "none"}, {"name": "SY","level": "none"} ]}'-- Permission - nvarchar(max)
        WHERE  SysRoleId = 13
               AND Permission IS NULL; -- Director of Academics


        UPDATE dbo.sySysRoles
        SET    Permission = N'{"modules": [
									{"name": "AR","level": "modify"}, {"name": "AD","level": "modify"}, 
									{"name": "FC","level": "modify"}, {"name": "FA","level": "modify"}, 
									{"name": "HR","level": "modify"}, {"name": "PL","level": "modify"}, 
									{"name": "SA","level": "modify"}, {"name": "SY","level": "modify"} ]}'-- Permission - nvarchar(max)
        WHERE  SysRoleId = 14
               AND Permission IS NULL; -- Report Administrator


        UPDATE dbo.sySysRoles
        SET    Permission = N'{"modules": [
									{"name": "AR","level": "read"}, {"name": "AD","level": "read"}, 
									{"name": "FC","level": "read"}, {"name": "FA","level": "read"}, 
									{"name": "HR","level": "read"}, {"name": "PL","level": "read"}, 
									{"name": "SA","level": "read"}, {"name": "SY","level": "read"} ]}'-- Permission - nvarchar(max)
        WHERE  SysRoleId = 15
               AND Permission IS NULL; -- Dashboard


        UPDATE dbo.sySysRoles
        SET    Permission = N'{"modules": [
									{"name": "AR","level": "modify"}, {"name": "AD","level": "modify"}, 
									{"name": "FC","level": "modify"}, {"name": "FA","level": "modify"}, 
									{"name": "HR","level": "modify"}, {"name": "PL","level": "modify"}, 
									{"name": "SA","level": "modify"}, {"name": "SY","level": "modify"} ]}'-- Permission - nvarchar(max)
        WHERE  SysRoleId = 16
               AND Permission IS NULL; -- View Confidential


        UPDATE dbo.sySysRoles
        SET    Permission = N'{"modules": [
									{"name": "AR","level": "delete"}, {"name": "AD","level": "modify"}, 
									{"name": "FC","level": "none"}, {"name": "FA","level": "none"}, 
									{"name": "HR","level": "none"}, {"name": "PL","level": "none"}, 
									{"name": "SA","level": "none"}, {"name": "SY","level": "read"} ]}'-- Permission - nvarchar(max)
        WHERE  SysRoleId = 17
               AND Permission IS NULL; -- Admission API
    END;

    IF ( @@ERROR > 0 )
        BEGIN
            SET @error = 1;
        END;
    ELSE
        BEGIN
            SET @error = 0;
        END;

END TRY
BEGIN CATCH
    ROLLBACK TRANSACTION rolePermissions;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE()
          ,@severity = ERROR_SEVERITY()
          ,@state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
    BEGIN
        ROLLBACK TRANSACTION rolePermissions;
        PRINT 'Update fail';

    END;
ELSE
    BEGIN
        COMMIT TRANSACTION rolePermissions;
        PRINT 'Update successful';
    END;
GO
--=================================================================================================
-- END --   US11001 Create the update script for the permission for the existing users as per the role
--=================================================================================================

--=================================================================================================
-- US11053 New System Role - Student Termination
--=================================================================================================
BEGIN TRANSACTION STrole;
BEGIN TRY
    DECLARE @error AS INT;
    SET @error = 0;
    DECLARE @fldId INT;

    IF NOT EXISTS (
                  SELECT *
                  FROM   dbo.sySysRoles
                  WHERE  SysRoleId = 18
                  )
        BEGIN

            DECLARE @STpermission NVARCHAR(MAX) = N'{"modules": [
									{"name": "AR","level": "delete"}, {"name": "AD","level": "modify"}, 
									{"name": "FC","level": "none"}, {"name": "FA","level": "none"}, 
									{"name": "HR","level": "none"}, {"name": "PL","level": "none"}, 
									{"name": "SA","level": "none"}, {"name": "SY","level": "read"} ]}';

            DECLARE @ActiveStatus UNIQUEIDENTIFIER;

            SELECT @ActiveStatus = StatusId
            FROM   syStatuses
            WHERE  StatusCode = 'A';

            INSERT INTO dbo.sySysRoles (
                                       SysRoleId
                                      ,Descrip
                                      ,StatusId
                                      ,ModUser
                                      ,ModDate
                                      ,RoleTypeId
                                      ,Permission
                                       )
            VALUES ( 18, 'Student Termination', @ActiveStatus, 'sa', GETDATE(), 1, @STpermission );
        END;

    IF ( @@ERROR > 0 )
        BEGIN
            SET @error = 1;
        END;
    ELSE
        BEGIN
            SET @error = 0;
        END;

END TRY
BEGIN CATCH
    ROLLBACK TRANSACTION AArole;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE()
          ,@severity = ERROR_SEVERITY()
          ,@state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
    BEGIN
        ROLLBACK TRANSACTION STrole;
        PRINT 'Insert failed';

    END;
ELSE
    BEGIN
        COMMIT TRANSACTION STrole;
        PRINT 'Insert successful';
    END;
GO
--=================================================================================================
-- END --  US11053 New System Role - Student Termination
--=================================================================================================

--=================================================================================================
-- START
--	AD-590 Create the update script for the permission for the existing users as per the role
--	Insert the in SysRoleId table the record for "Admission Api" SysRoleId = 17 And Assign the new role to support user
-- START
--=================================================================================================
BEGIN TRANSACTION;
BEGIN TRY
    IF NOT EXISTS (
                  SELECT 1
                  FROM   dbo.syRoles
                  WHERE  SysRoleId = 17
                         AND Role = 'Admission Api'
                  )
        BEGIN
            INSERT INTO dbo.syRoles (
                                    SysRoleId
                                   ,Role
                                   ,Code
                                   ,StatusId
                                   ,ModUser
                                   ,ModDate
                                    )
            VALUES ( 17                                     -- SysRoleId - int
                    ,'Admission Api'                        -- Description
                    ,'AD-Api'                               -- Code - varchar(80)
                    ,'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' -- StatusId - uniqueidentifier
                    ,'SUPPORT'                              -- ModUser - varchar(50)
                    ,GETDATE()                              -- ModDate - datetime
                   );

            DECLARE @UserId UNIQUEIDENTIFIER;
            SET @UserId = (
                          SELECT UserId
                          FROM   syUsers
                          WHERE  UserName = 'support'
                                 AND IsAdvantageSuperUser = 1
                          );

            DECLARE @CampusGroupId UNIQUEIDENTIFIER;
            SET @CampusGroupId = (
                                 SELECT TOP 1 CampGrpId
                                 FROM   syCampGrps
                                 WHERE  IsAllCampusGrp = 1
                                 );

            DECLARE @ApiRoleId UNIQUEIDENTIFIER;
            SET @ApiRoleId = (
                             SELECT TOP 1 RoleId
                             FROM   syRoles
                             WHERE  Role = 'Admission Api'
                             );

            INSERT INTO syUsersRolesCampGrps
            VALUES ( NEWID(), @UserId, @ApiRoleId, @CampusGroupId, GETDATE(), 'support' );
        END;
END TRY
BEGIN CATCH
    SELECT ERROR_NUMBER() AS ErrorNumber
          ,ERROR_SEVERITY() AS ErrorSeverity
          ,ERROR_STATE() AS ErrorState
          ,ERROR_PROCEDURE() AS ErrorProcedure
          ,ERROR_LINE() AS ErrorLine
          ,ERROR_MESSAGE() AS ErrorMessage;

    IF @@TRANCOUNT > 0
        BEGIN
            ROLLBACK TRANSACTION;
            PRINT 'Exception Ocurred!';
        END;
END CATCH;

IF @@TRANCOUNT > 0
    BEGIN
        COMMIT TRANSACTION;
    END;

GO
--=================================================================================================
-- END
--	AD-590 Create the update script for the permission for the existing users as per the role
--	Insert the in SysRoleId table the record for "Admission Api" SysRoleId = 17 And Assign the new role to support user
-- END
--=================================================================================================
--=================================================================================================
-- START
-- AD-592 Tech: Allow Advantage to login in the API  
-- ADDING THE RECORD FOR 'AdvantageApiURL' IN THE 'syConfigAppSettings' TABLE. THE VALUE WILL BE DIFFERENT FOR DIFFERENT ENVIRONMENT.
-- NEED TO CONFIGURE THAT VALUE THROUGH MAINTAINANCE-->SET UP CONFIGURERATION.
-- START
--=================================================================================================
IF NOT EXISTS (
              SELECT 1
              FROM   syConfigAppSettings
              WHERE  KeyName LIKE 'AdvantageApiURL'
              )
    BEGIN
        DECLARE @settingId INT;
        SET @settingId = (
                         SELECT MAX(SettingId)
                         FROM   syConfigAppSettings
                         ) + 1;
        INSERT INTO syConfigAppSettings (
                                        SettingId
                                       ,KeyName
                                       ,Description
                                       ,ModUser
                                       ,ModDate
                                       ,CampusSpecific
                                       ,ExtraConfirmation
                                        )
        VALUES ( @settingId          -- SettingId - int
                ,'AdvantageApiURL'   -- KeyName - varchar(200)
                ,'Advantage Api URL' -- Description - varchar(1000)
                ,'Support'           -- ModUser - varchar(50)
                ,GETDATE()           -- ModDate - datetime
                ,0                   -- CampusSpecific - bit
                ,0                   -- ExtraConfirmation - bit
               );
    END;
GO
--=================================================================================================
-- END
--	AD-592 Tech: Allow Advantage to login in the API
-- END
--=================================================================================================
--=================================================================================================
--START
-- AD-290-Insert a record to SyConfigSettings for the New student termination workflow
--Insert a record to SyConfigSettings for the New student termination workflow
--START
--=================================================================================================

DECLARE @KeyName VARCHAR(50) = 'NewStudentTerminationWorkflow';
BEGIN TRANSACTION;
BEGIN TRY
    IF NOT EXISTS (
                  SELECT *
                  FROM   syConfigAppSettings
                  WHERE  KeyName = @KeyName
                  )
        BEGIN

            DECLARE @MaxId INT = (
                                 SELECT MAX(SettingId)
                                 FROM   dbo.syConfigAppSettings
                                 );
            SET @MaxId = @MaxId + 1;

            INSERT INTO dbo.syConfigAppSettings (
                                                SettingId
                                               ,KeyName
                                               ,Description
                                               ,ModUser
                                               ,ModDate
                                               ,CampusSpecific
                                               ,ExtraConfirmation
                                                )
            VALUES ( @MaxId                                                                            -- SettingId - int
                    ,@KeyName                                                                          -- KeyName - varchar(200)
                    ,'Enable the New student termination workflow. Values can be Enabled or Disabled.' -- Description - varchar(1000)
                    ,'Support'                                                                         -- ModUser - varchar(50)
                    ,GETDATE()                                                                         -- ModDate - datetime
                    ,0                                                                                 -- CampusSpecific - bit
                    ,0                                                                                 -- ExtraConfirmation - bit
                   );

            INSERT INTO dbo.syConfigAppSetValues (
                                                 ValueId
                                                ,SettingId
                                                ,CampusId
                                                ,Value
                                                ,ModUser
                                                ,ModDate
                                                ,Active
                                                 )
            VALUES ( NEWID()    -- ValueId - uniqueidentifier
                    ,@MaxId     -- SettingId - int
                    ,NULL       -- CampusId - uniqueidentifier
                    ,'Disabled' -- Value - varchar(1000)
                    ,'Support'  -- ModUser - varchar(50)
                    ,GETDATE()  -- ModDate - datetime
                    ,1          -- Active - bit
                   );

            INSERT INTO dbo.syConfigAppSet_Lookup (
                                                  LookUpId
                                                 ,SettingId
                                                 ,ValueOptions
                                                 ,ModUser
                                                 ,ModDate
                                                  )
            VALUES ( NEWID()    -- LookUpId - uniqueidentifier
                    ,@MaxId     -- SettingId - int
                    ,'Disabled' -- ValueOptions - varchar(50)
                    ,'Support'  -- ModUser - varchar(50)
                    ,GETDATE()  -- ModDate - datetime
                   );

            INSERT INTO dbo.syConfigAppSet_Lookup (
                                                  LookUpId
                                                 ,SettingId
                                                 ,ValueOptions
                                                 ,ModUser
                                                 ,ModDate
                                                  )
            VALUES ( NEWID()   -- LookUpId - uniqueidentifier
                    ,@MaxId    -- SettingId - int
                    ,'Enabled' -- ValueOptions - varchar(50)
                    ,'Support' -- ModUser - varchar(50)
                    ,GETDATE() -- ModDate - datetime
                   );


        END;
END TRY
BEGIN CATCH
    SELECT ERROR_NUMBER() AS ErrorNumber
          ,ERROR_SEVERITY() AS ErrorSeverity
          ,ERROR_STATE() AS ErrorState
          ,ERROR_PROCEDURE() AS ErrorProcedure
          ,ERROR_LINE() AS ErrorLine
          ,ERROR_MESSAGE() AS ErrorMessage;

    IF @@TRANCOUNT > 0
        BEGIN
            ROLLBACK TRANSACTION;
            PRINT 'Exception Ocurred!';
        END;
END CATCH;

IF @@TRANCOUNT > 0
    BEGIN
        COMMIT TRANSACTION;
    END;

GO

--=================================================================================================
-- END --  AD-290-Insert a record to SyConfigSettings for the New student termination workflow
--=================================================================================================
--=================================================================================================
-- AD-585 Push Credits Earned to Advantage Mobility App
--  JAGG Create entering 
--=================================================================================================
IF NOT EXISTS (
              SELECT KlassOperationTypeId
              FROM   dbo.syKlassOperationType
              WHERE  KlassOperationTypeId = 10
              )
    BEGIN
        INSERT INTO dbo.syKlassOperationType (
                                             KlassOperationTypeId
                                            ,Code
                                            ,Description
                                             )
        VALUES ( 10               -- Id - int
                ,'credEarned'     -- Code - varchar(20)
                ,'Credits Earned' -- Description - varchar(100)
               );
    END;

GO
--=================================================================================================
-- END --  AD-585 Push Credits Earned to Advantage Mobility App
--=================================================================================================
--=================================================================================================
-- AD-584 Push Credits Attemped to Advantage Mobility App
--  JAGG Create  
--=================================================================================================
IF NOT EXISTS (
              SELECT KlassOperationTypeId
              FROM   dbo.syKlassOperationType
              WHERE  KlassOperationTypeId = 11
              )
    BEGIN
        INSERT INTO dbo.syKlassOperationType (
                                             KlassOperationTypeId
                                            ,Code
                                            ,Description
                                             )
        VALUES ( 11                  -- Id - int
                ,'credAttemped'      -- Code - varchar(20)
                ,'Credits Attempted' -- Description - varchar(100)
               );
    END;

GO
--=================================================================================================
-- END --  AD-585 Push Credits Earned to Advantage Mobility App
--=================================================================================================


--=================================================================================================
--START
-- AD-289--Availability of Student Termination screen with the ability to calculate R2T4 on the basis of configuration value
--INSERT a record to syResources for the New Student Termination Url
--START
--=================================================================================================
DECLARE @ResourceURL VARCHAR(100) = '~/AR/StudentTermination.aspx';
DECLARE @DisplayName VARCHAR(200) = 'Terminate Student';
DECLARE @MaxID INT = 843;

BEGIN TRANSACTION;

BEGIN TRY

    IF NOT EXISTS (
                  SELECT *
                  FROM   syResources
                  WHERE  ResourceURL = @ResourceURL
                  )
        BEGIN

            INSERT INTO dbo.syResources (
                                        ResourceID
                                       ,Resource
                                       ,ResourceTypeID
                                       ,ResourceURL
                                       ,SummListId
                                       ,ChildTypeId
                                       ,ModDate
                                       ,ModUser
                                       ,AllowSchlReqFlds
                                       ,MRUTypeId
                                       ,UsedIn
                                       ,TblFldsId
                                       ,DisplayName
                                        )
            VALUES ( @MaxID       -- ResourceID - smallint
                    ,@DisplayName -- Resource - varchar(200)
                    ,3            -- ResourceTypeID - tinyint
                    ,@ResourceURL -- ResourceURL - varchar(100)
                    ,0            -- SummListId - smallint
                    ,0            -- ChildTypeId - tinyint
                    ,GETDATE()    -- ModDate - datetime
                    ,''           -- ModUser - varchar(50)
                    ,NULL         -- AllowSchlReqFlds - bit
                    ,0            -- MRUTypeId - smallint
                    ,0            -- UsedIn - int
                    ,0            -- TblFldsId - int
                    ,@DisplayName -- DisplayName - varchar(200)
                   );
        END;
END TRY
BEGIN CATCH
    SELECT ERROR_NUMBER() AS ErrorNumber;
    SELECT ERROR_SEVERITY() AS ErrorSeverity;
    SELECT ERROR_STATE() AS ErrorState;
    SELECT ERROR_PROCEDURE() AS ErrorProcedure;
    SELECT ERROR_LINE() AS ErrorLine;
    SELECT ERROR_MESSAGE() AS ErrorMessage;

    IF @@TRANCOUNT > 0
        BEGIN
            ROLLBACK TRANSACTION;
            PRINT 'Exception Ocurred!';
        END;
END CATCH;

IF @@TRANCOUNT > 0
    BEGIN
        COMMIT TRANSACTION;
    END;

GO

--=================================================================================================
--END
-- AD-289--Availability of Student Termination screen with the ability to calculate R2T4 on the basis of configuration value
--INSERT a record to syResources for the New Student Termination Url
--END
--=================================================================================================
--=================================================================================================
--START
--Dev Coding: AD-2580 - Unable to access 1098T upload
--INSERT records into syRlsResLvls to Give 1098T access to all roles that have access to Transaction Codes (as per  US9586) 
--START
--=================================================================================================
DECLARE @1098TResource VARCHAR(50) = '1098T Service';
DECLARE @TCResource VARCHAR(50) = 'Transaction Codes';
DECLARE @1098ResId SMALLINT;
DECLARE @TCResId SMALLINT;


BEGIN TRANSACTION;

BEGIN TRY

    SELECT @1098ResId = ResourceID
    FROM   dbo.syResources
    WHERE  Resource = @1098TResource;

    SELECT @TCResId = ResourceID
    FROM   dbo.syResources
    WHERE  Resource = @TCResource;

    IF NOT EXISTS (
                  SELECT 1
                  FROM   syRlsResLvls
                  WHERE  ResourceID = @1098ResId
                  )
        BEGIN

            INSERT INTO dbo.syRlsResLvls (
                                         RoleId
                                        ,ResourceID
                                        ,AccessLevel
                                        ,ModDate
                                        ,ModUser
                                        ,ParentId
                                         )
                        SELECT RoleId
                              ,@1098ResId
                              ,AccessLevel
                              ,GETDATE() ModDate
                              ,'Support' ModUser
                              ,NULL ParentId
                        FROM   syRlsResLvls
                        WHERE  ResourceID = @TCResId;

        END;
END TRY
BEGIN CATCH
    SELECT ERROR_NUMBER() AS ErrorNumber;
    SELECT ERROR_SEVERITY() AS ErrorSeverity;
    SELECT ERROR_STATE() AS ErrorState;
    SELECT ERROR_PROCEDURE() AS ErrorProcedure;
    SELECT ERROR_LINE() AS ErrorLine;
    SELECT ERROR_MESSAGE() AS ErrorMessage;

    IF @@TRANCOUNT > 0
        BEGIN
            ROLLBACK TRANSACTION;
            PRINT 'Exception Occurred!';
        END;
END CATCH;

IF @@TRANCOUNT > 0
    BEGIN
        COMMIT TRANSACTION;
    END;

GO

--=================================================================================================
--END
--Dev Coding: AD-2580 - Unable to access 1098T upload
--INSERT records into syRlsResLvls to Give 1098T access to all roles that have access to Transaction Codes (as per  US9586) 
--END
--=================================================================================================
--=================================================================================================
--START 
--AD-618: On/Off Switch for New Manage Users page in Manage Configuration Settings
--Insert a record to SyConfigSettings for the UsersPageEnabled
--START
--=================================================================================================
DECLARE @KeyName VARCHAR(50) = 'UsersPageEnabled';
BEGIN TRANSACTION usersPageConfig;
BEGIN TRY
    DECLARE @error AS INT;
    SET @error = 0;

    IF NOT EXISTS (
                  SELECT 1
                  FROM   syConfigAppSettings
                  WHERE  KeyName = @KeyName
                  )
        BEGIN
            DECLARE @MaxId INT = (
                                 SELECT MAX(SettingId)
                                 FROM   dbo.syConfigAppSettings
                                 );
            SET @MaxId = @MaxId + 1;

            INSERT INTO dbo.syConfigAppSettings (
                                                SettingId
                                               ,KeyName
                                               ,Description
                                               ,ModUser
                                               ,ModDate
                                               ,CampusSpecific
                                               ,ExtraConfirmation
                                                )
            VALUES ( @MaxId                                                       -- SettingId - int
                    ,@KeyName                                                     -- KeyName - varchar(200)
                    ,'Enable the New Manage Users page. Values can be Yes or No.' -- Description - varchar(1000)
                    ,'Support'                                                    -- ModUser - varchar(50)
                    ,GETDATE()                                                    -- ModDate - datetime
                    ,0                                                            -- CampusSpecific - bit
                    ,0                                                            -- ExtraConfirmation - bit
                   );

            INSERT INTO dbo.syConfigAppSetValues (
                                                 ValueId
                                                ,SettingId
                                                ,CampusId
                                                ,Value
                                                ,ModUser
                                                ,ModDate
                                                ,Active
                                                 )
            VALUES ( NEWID()   -- ValueId - uniqueidentifier
                    ,@MaxId    -- SettingId - int
                    ,NULL      -- CampusId - uniqueidentifier
                    ,'No'      -- Value - varchar(1000)
                    ,'Support' -- ModUser - varchar(50)
                    ,GETDATE() -- ModDate - datetime
                    ,1         -- Active - bit
                   );

            INSERT INTO dbo.syConfigAppSet_Lookup (
                                                  LookUpId
                                                 ,SettingId
                                                 ,ValueOptions
                                                 ,ModUser
                                                 ,ModDate
                                                  )
            VALUES ( NEWID()   -- LookUpId - uniqueidentifier
                    ,@MaxId    -- SettingId - int
                    ,'No'      -- ValueOptions - varchar(50)
                    ,'Support' -- ModUser - varchar(50)
                    ,GETDATE() -- ModDate - datetime
                   );

            INSERT INTO dbo.syConfigAppSet_Lookup (
                                                  LookUpId
                                                 ,SettingId
                                                 ,ValueOptions
                                                 ,ModUser
                                                 ,ModDate
                                                  )
            VALUES ( NEWID()   -- LookUpId - uniqueidentifier
                    ,@MaxId    -- SettingId - int
                    ,'Yes'     -- ValueOptions - varchar(50)
                    ,'Support' -- ModUser - varchar(50)
                    ,GETDATE() -- ModDate - datetime
                   );
        END;

    ELSE
        BEGIN
            DECLARE @settingId AS INT;
            SELECT @settingId = SettingId
            FROM   dbo.syConfigAppSettings
            WHERE  KeyName = @KeyName;
            IF NOT EXISTS (
                          SELECT 2
                          FROM   dbo.syConfigAppSet_Lookup
                          WHERE  SettingId = @settingId
                          )
                BEGIN
                    UPDATE dbo.syConfigAppSettings
                    SET    Description = 'Enable the New Manage Users page. Values can be Yes or No.'
                          ,ModDate = GETDATE()
                    WHERE  SettingId = @settingId;
                    UPDATE dbo.syConfigAppSetValues
                    SET    Value = 'No'
                          ,ModDate = GETDATE()
                    WHERE  SettingId = @settingId;

                    INSERT INTO dbo.syConfigAppSet_Lookup (
                                                          LookUpId
                                                         ,SettingId
                                                         ,ValueOptions
                                                         ,ModUser
                                                         ,ModDate
                                                          )
                    VALUES ( NEWID()    -- LookUpId - uniqueidentifier
                            ,@settingId -- SettingId - int
                            ,'No'       -- ValueOptions - varchar(50)
                            ,'Support'  -- ModUser - varchar(50)
                            ,GETDATE()  -- ModDate - datetime
                           );

                    INSERT INTO dbo.syConfigAppSet_Lookup (
                                                          LookUpId
                                                         ,SettingId
                                                         ,ValueOptions
                                                         ,ModUser
                                                         ,ModDate
                                                          )
                    VALUES ( NEWID()    -- LookUpId - uniqueidentifier
                            ,@settingId -- SettingId - int
                            ,'Yes'      -- ValueOptions - varchar(50)
                            ,'Support'  -- ModUser - varchar(50)
                            ,GETDATE()  -- ModDate - datetime
                           );

                END;
        END;
    IF ( @@ERROR > 0 )
        BEGIN
            SET @error = 1;
        END;
    ELSE
        BEGIN
            SET @error = 0;
        END;

END TRY
BEGIN CATCH
    ROLLBACK TRANSACTION usersPageConfig;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE()
          ,@severity = ERROR_SEVERITY()
          ,@state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
    BEGIN
        ROLLBACK TRANSACTION usersPageConfig;
        PRINT 'Update fail';

    END;
ELSE
    BEGIN
        COMMIT TRANSACTION usersPageConfig;
        PRINT 'Update successful';
    END;
GO

--=================================================================================================
--END 
--AD-618: On/Off Switch for New Manage Users page in Manage Configuration Settings
--Insert a record to SyConfigSettings for the UsersPageEnabled
--END
--=================================================================================================
--=================================================================================================
--START 
--AD-3719: QA: Chat310: Instead of yes/no it displays enabled in the "addcreditsbyservice"
--checking if the value is not Yes/No then will set the value as No in the config settings
--START
--=================================================================================================
BEGIN TRANSACTION addcreditsbyservice;
BEGIN TRY
    DECLARE @error AS INT;
    SET @error = 0;
    DECLARE @settingId AS INT;
    SET @settingId = (
                     SELECT syConfigAppSettings.SettingId
                     FROM   syConfigAppSetValues
                     INNER JOIN syConfigAppSettings ON syConfigAppSettings.SettingId = syConfigAppSetValues.SettingId
                     WHERE  KeyName = 'addcreditsbyservice'
                     );
    DECLARE @val VARCHAR(50);
    SET @val = (
               SELECT LOWER(Value)
               FROM   syConfigAppSetValues
               INNER JOIN syConfigAppSettings ON syConfigAppSettings.SettingId = syConfigAppSetValues.SettingId
               WHERE  KeyName = 'addcreditsbyservice'
               );
    IF @val NOT IN ( 'yes', 'no' )
        BEGIN
            UPDATE syConfigAppSetValues
            SET    Value = 'No'
            WHERE  SettingId = @settingId;
        END;
    IF ( @@ERROR > 0 )
        BEGIN
            SET @error = 1;
        END;
    ELSE
        BEGIN
            SET @error = 0;
        END;

END TRY
BEGIN CATCH
    ROLLBACK TRANSACTION addcreditsbyservice;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE()
          ,@severity = ERROR_SEVERITY()
          ,@state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
    BEGIN
        ROLLBACK TRANSACTION addcreditsbyservice;
        PRINT 'Update fail';

    END;
ELSE
    BEGIN
        COMMIT TRANSACTION addcreditsbyservice;
        PRINT 'Update successful';
    END;
GO
--=================================================================================================
--END 
--AD-618: On/Off Switch for New Manage Users page in Manage Configuration Settings
--checking if the value is not Yes/No then will set the value as No in the config settings
--END
--=================================================================================================

