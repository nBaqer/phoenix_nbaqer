/*
This migration script replaces uncommitted changes made to these objects:
adLeadImports

Use this script to make necessary schema and data changes for these objects only. Schema changes to any other objects won't be deployed.

Schema changes and migration scripts are deployed in the order they're committed.

Migration scripts must not reference static data. When you deploy migration scripts alongside static data 
changes, the migration scripts will run first. This can cause the deployment to fail. 
Read more at https://documentation.red-gate.com/display/SOC6/Static+data+and+migrations.
*/

--================================================================
-- START --  US AD-3554: DBA Recomentations
-- Edwin S 2-14-18
--================================================================

IF NOT ((
        SELECT DATA_TYPE
        FROM   INFORMATION_SCHEMA.COLUMNS
        WHERE  TABLE_NAME = 'adLeadImports'
               AND COLUMN_NAME = 'LeadImportId'
        ) = 'uniqueidentifier'
       )
    BEGIN
        PRINT N'Setting NOEXEC for Lead Import Id change on adLeadImports';
        SET NOEXEC ON;
    END;

SET NUMERIC_ROUNDABORT OFF;
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON;
GO
PRINT N'Dropping constraints from [dbo].[adLeadImports]';
GO
ALTER TABLE [dbo].[adLeadImports]
DROP CONSTRAINT [PK_adLeadImports_LeadImportId];
GO
PRINT N'Dropping constraints from [dbo].[adLeadImports]';
GO
ALTER TABLE [dbo].[adLeadImports]
DROP CONSTRAINT [DF_adLeadImports_LeadImportId];
GO
PRINT N'Rebuilding [dbo].[adLeadImports]';
GO
CREATE TABLE [dbo].[RG_Recovery_1_adLeadImports]
    (
        [RecNum] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[CurrentEnrollStatus] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[PrivTotalHrsAttended] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[PrivTotalHrsAbsent] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[TotalHrsMakeup] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[LastMonthTotalHrs] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[LastMonthAbsentHrs] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[LastMonthMakeupHrs] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[ScheduledHrsLDA] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[MonthEndPellPaidHrs] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[AttndSchdHrsMon] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[SchdDailyHrs_1] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[SchdDailyHrs_2] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[SchdDailyHrs_3] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[SchdDailyHrs_4] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[SchdDailyHrs_5] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[SchdDailyHrs_6] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[SchdDailyHrs_7] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[AttndSchdHrsTue] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[AttndSchdHrsWed] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[AttndSchdHrsThr] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[AttndSchdHrsFri] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[AttndSchdHrsSat] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[AttndSchdHrsSun] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[PrivTransferedInHrs] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[PrivOvtChargedAmt] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[FirstName] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[LastName] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[AddressLine1] [VARCHAR](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[AddressCity] [VARCHAR](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[AddressState] [VARCHAR](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[Zip] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[SSN] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[PhoneNumAreaCode1] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[PhoneNumPrefix1] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[PhoneNumBody1] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[CourseNumEnrolledIn] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[AbilityToBenefit] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[PrivTClockSchdRecKey] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[ClassIdNum] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[TClockBadgeNum] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[StartDate] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[EnrolledDate] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[ContractedGradDt] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[RevisedGradDate] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[LastDateAttended] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[EndLeaveDate] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[BeginLeaveDate] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[BeginProbationDate] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[PrevEnrollEndDate] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[ReEnrolledDate] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[EntranceinterviewDate] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[FullPartTimeROP] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[StateRegId] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[MiddleName] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[PaymentRecKey] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[GradRecKey] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[FinAidRecKey] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[PrivCurBalOwed] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[PrivCourseTuitCost] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[PrivCourseRegCost] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[PrivCourseKitCost] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[PrivCourseOthrCost] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[AccruedTuit] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[AccruedReg] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[AccruedKit] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[AccruedOthr] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[BirthDate] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[TermResKey] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[SubjectResKey] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[WorkUnitResKey] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[ResultTot] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[ResultCnt] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[LabCnt] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[AttendMonthKey] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[ProspectKey] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[PrivCourseRoomCost] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[StuTransKey] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[StuGroupKey] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[NotesKey] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[PhoneType1] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[PhoneType2] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[HomeEmail] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[XferSubjResKey] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[StateRegDate] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[PrevStudRecKey] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[NextStudRecKey] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[DegreeSeekingType] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[PrivStatusLogKey] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[PrivFirstTransDate] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[PrivLastTransDate] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[PrivZipPlus4] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[DropReasonCode] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[PhoneNumAreaCode2] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[PhoneNumPrefix2] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[PhoneNumBody2] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[TransHrsLnYr1] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[OhioTheoryHrs] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[OhioPracticalHrs] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[OhioDemoHrs] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[OhioClinicHrs] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[DefaultLtrRecKey] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[CreditHrs] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[TotHrsLastJune30th] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[SchdHrsLastJune30th] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[TranHrsUpFront] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[ATBscore] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[SAPInclPrevEnr] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[Ostudent] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[BirthDate1] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[Sex] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[UrbanType] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[Race] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[Dependency] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[EducationLevel] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[FullTimePartTime] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[IncomeLevel] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[Age] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[MilesFromSchool] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[Over50Miles] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[MaritalStatus] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[AllClinicInfo_1] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[AllClinicInfo_2] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[AllClinicInfo_3] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[AllClinicInfo_4] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[AllClinicInfo_5] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[AllClinicInfo_6] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[AllClinicInfo_7] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[AllClinicInfo_8] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[AllClinicInfo_9] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[AllClinicInfo_10] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[AllClinicInfo_11] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[AllClinicInfo_12] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[AllClinicInfo_13] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[AllClinicInfo_14] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[AllClinicInfo_15] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[AllClinicInfo_16] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[AllClinicInfo_17] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[AllClinicInfo_18] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[AllClinicInfo_19] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[AllClinicInfo_20] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[AllClinicInfo_21] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[AllClinicInfo_22] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[AllClinicInfo_23] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[AllClinicInfo_24] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[AllClinicInfo_25] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[AllClinicInfo_26] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[AllClinicInfo_27] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[AllClinicInfo_28] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[AllClinicInfo_29] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[AllClinicInfo_30] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[AllClinicInfo_31] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[AllClinicInfo_32] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[AllClinicInfo_33] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[AllClinicInfo_34] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[AllClinicInfo_35] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[AllClinicInfo_36] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[AllClinicInfo_37] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[AllClinicInfo_38] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[AllClinicInfo_39] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[AllClinicInfo_40] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[TotWrittenScores] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[TotPracticalScores] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[NumWrittenTaken] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[NumPracticalTaken] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[NumOfDeps] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[HeadHousehold] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[YearlyIncome] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[InHighSchool] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[CitizenType] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[HousingType] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[HighSchoolRecKey] [VARCHAR](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[HighSchGEDDate] [VARCHAR](8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
       ,[LeadImportId] [INT] NOT NULL IDENTITY(1, 1)
    ) ON [PRIMARY];
GO
INSERT INTO [dbo].[RG_Recovery_1_adLeadImports] (
                                                [RecNum]
                                               ,[CurrentEnrollStatus]
                                               ,[PrivTotalHrsAttended]
                                               ,[PrivTotalHrsAbsent]
                                               ,[TotalHrsMakeup]
                                               ,[LastMonthTotalHrs]
                                               ,[LastMonthAbsentHrs]
                                               ,[LastMonthMakeupHrs]
                                               ,[ScheduledHrsLDA]
                                               ,[MonthEndPellPaidHrs]
                                               ,[AttndSchdHrsMon]
                                               ,[SchdDailyHrs_1]
                                               ,[SchdDailyHrs_2]
                                               ,[SchdDailyHrs_3]
                                               ,[SchdDailyHrs_4]
                                               ,[SchdDailyHrs_5]
                                               ,[SchdDailyHrs_6]
                                               ,[SchdDailyHrs_7]
                                               ,[AttndSchdHrsTue]
                                               ,[AttndSchdHrsWed]
                                               ,[AttndSchdHrsThr]
                                               ,[AttndSchdHrsFri]
                                               ,[AttndSchdHrsSat]
                                               ,[AttndSchdHrsSun]
                                               ,[PrivTransferedInHrs]
                                               ,[PrivOvtChargedAmt]
                                               ,[FirstName]
                                               ,[LastName]
                                               ,[AddressLine1]
                                               ,[AddressCity]
                                               ,[AddressState]
                                               ,[Zip]
                                               ,[SSN]
                                               ,[PhoneNumAreaCode1]
                                               ,[PhoneNumPrefix1]
                                               ,[PhoneNumBody1]
                                               ,[CourseNumEnrolledIn]
                                               ,[AbilityToBenefit]
                                               ,[PrivTClockSchdRecKey]
                                               ,[ClassIdNum]
                                               ,[TClockBadgeNum]
                                               ,[StartDate]
                                               ,[EnrolledDate]
                                               ,[ContractedGradDt]
                                               ,[RevisedGradDate]
                                               ,[LastDateAttended]
                                               ,[EndLeaveDate]
                                               ,[BeginLeaveDate]
                                               ,[BeginProbationDate]
                                               ,[PrevEnrollEndDate]
                                               ,[ReEnrolledDate]
                                               ,[EntranceinterviewDate]
                                               ,[FullPartTimeROP]
                                               ,[StateRegId]
                                               ,[MiddleName]
                                               ,[PaymentRecKey]
                                               ,[GradRecKey]
                                               ,[FinAidRecKey]
                                               ,[PrivCurBalOwed]
                                               ,[PrivCourseTuitCost]
                                               ,[PrivCourseRegCost]
                                               ,[PrivCourseKitCost]
                                               ,[PrivCourseOthrCost]
                                               ,[AccruedTuit]
                                               ,[AccruedReg]
                                               ,[AccruedKit]
                                               ,[AccruedOthr]
                                               ,[BirthDate]
                                               ,[TermResKey]
                                               ,[SubjectResKey]
                                               ,[WorkUnitResKey]
                                               ,[ResultTot]
                                               ,[ResultCnt]
                                               ,[LabCnt]
                                               ,[AttendMonthKey]
                                               ,[ProspectKey]
                                               ,[PrivCourseRoomCost]
                                               ,[StuTransKey]
                                               ,[StuGroupKey]
                                               ,[NotesKey]
                                               ,[PhoneType1]
                                               ,[PhoneType2]
                                               ,[HomeEmail]
                                               ,[XferSubjResKey]
                                               ,[StateRegDate]
                                               ,[PrevStudRecKey]
                                               ,[NextStudRecKey]
                                               ,[DegreeSeekingType]
                                               ,[PrivStatusLogKey]
                                               ,[PrivFirstTransDate]
                                               ,[PrivLastTransDate]
                                               ,[PrivZipPlus4]
                                               ,[DropReasonCode]
                                               ,[PhoneNumAreaCode2]
                                               ,[PhoneNumPrefix2]
                                               ,[PhoneNumBody2]
                                               ,[TransHrsLnYr1]
                                               ,[OhioTheoryHrs]
                                               ,[OhioPracticalHrs]
                                               ,[OhioDemoHrs]
                                               ,[OhioClinicHrs]
                                               ,[DefaultLtrRecKey]
                                               ,[CreditHrs]
                                               ,[TotHrsLastJune30th]
                                               ,[SchdHrsLastJune30th]
                                               ,[TranHrsUpFront]
                                               ,[ATBscore]
                                               ,[SAPInclPrevEnr]
                                               ,[Ostudent]
                                               ,[BirthDate1]
                                               ,[Sex]
                                               ,[UrbanType]
                                               ,[Race]
                                               ,[Dependency]
                                               ,[EducationLevel]
                                               ,[FullTimePartTime]
                                               ,[IncomeLevel]
                                               ,[Age]
                                               ,[MilesFromSchool]
                                               ,[Over50Miles]
                                               ,[MaritalStatus]
                                               ,[AllClinicInfo_1]
                                               ,[AllClinicInfo_2]
                                               ,[AllClinicInfo_3]
                                               ,[AllClinicInfo_4]
                                               ,[AllClinicInfo_5]
                                               ,[AllClinicInfo_6]
                                               ,[AllClinicInfo_7]
                                               ,[AllClinicInfo_8]
                                               ,[AllClinicInfo_9]
                                               ,[AllClinicInfo_10]
                                               ,[AllClinicInfo_11]
                                               ,[AllClinicInfo_12]
                                               ,[AllClinicInfo_13]
                                               ,[AllClinicInfo_14]
                                               ,[AllClinicInfo_15]
                                               ,[AllClinicInfo_16]
                                               ,[AllClinicInfo_17]
                                               ,[AllClinicInfo_18]
                                               ,[AllClinicInfo_19]
                                               ,[AllClinicInfo_20]
                                               ,[AllClinicInfo_21]
                                               ,[AllClinicInfo_22]
                                               ,[AllClinicInfo_23]
                                               ,[AllClinicInfo_24]
                                               ,[AllClinicInfo_25]
                                               ,[AllClinicInfo_26]
                                               ,[AllClinicInfo_27]
                                               ,[AllClinicInfo_28]
                                               ,[AllClinicInfo_29]
                                               ,[AllClinicInfo_30]
                                               ,[AllClinicInfo_31]
                                               ,[AllClinicInfo_32]
                                               ,[AllClinicInfo_33]
                                               ,[AllClinicInfo_34]
                                               ,[AllClinicInfo_35]
                                               ,[AllClinicInfo_36]
                                               ,[AllClinicInfo_37]
                                               ,[AllClinicInfo_38]
                                               ,[AllClinicInfo_39]
                                               ,[AllClinicInfo_40]
                                               ,[TotWrittenScores]
                                               ,[TotPracticalScores]
                                               ,[NumWrittenTaken]
                                               ,[NumPracticalTaken]
                                               ,[NumOfDeps]
                                               ,[HeadHousehold]
                                               ,[YearlyIncome]
                                               ,[InHighSchool]
                                               ,[CitizenType]
                                               ,[HousingType]
                                               ,[HighSchoolRecKey]
                                               ,[HighSchGEDDate]
                                                )
            SELECT [RecNum]
                  ,[CurrentEnrollStatus]
                  ,[PrivTotalHrsAttended]
                  ,[PrivTotalHrsAbsent]
                  ,[TotalHrsMakeup]
                  ,[LastMonthTotalHrs]
                  ,[LastMonthAbsentHrs]
                  ,[LastMonthMakeupHrs]
                  ,[ScheduledHrsLDA]
                  ,[MonthEndPellPaidHrs]
                  ,[AttndSchdHrsMon]
                  ,[SchdDailyHrs_1]
                  ,[SchdDailyHrs_2]
                  ,[SchdDailyHrs_3]
                  ,[SchdDailyHrs_4]
                  ,[SchdDailyHrs_5]
                  ,[SchdDailyHrs_6]
                  ,[SchdDailyHrs_7]
                  ,[AttndSchdHrsTue]
                  ,[AttndSchdHrsWed]
                  ,[AttndSchdHrsThr]
                  ,[AttndSchdHrsFri]
                  ,[AttndSchdHrsSat]
                  ,[AttndSchdHrsSun]
                  ,[PrivTransferedInHrs]
                  ,[PrivOvtChargedAmt]
                  ,[FirstName]
                  ,[LastName]
                  ,[AddressLine1]
                  ,[AddressCity]
                  ,[AddressState]
                  ,[Zip]
                  ,[SSN]
                  ,[PhoneNumAreaCode1]
                  ,[PhoneNumPrefix1]
                  ,[PhoneNumBody1]
                  ,[CourseNumEnrolledIn]
                  ,[AbilityToBenefit]
                  ,[PrivTClockSchdRecKey]
                  ,[ClassIdNum]
                  ,[TClockBadgeNum]
                  ,[StartDate]
                  ,[EnrolledDate]
                  ,[ContractedGradDt]
                  ,[RevisedGradDate]
                  ,[LastDateAttended]
                  ,[EndLeaveDate]
                  ,[BeginLeaveDate]
                  ,[BeginProbationDate]
                  ,[PrevEnrollEndDate]
                  ,[ReEnrolledDate]
                  ,[EntranceinterviewDate]
                  ,[FullPartTimeROP]
                  ,[StateRegId]
                  ,[MiddleName]
                  ,[PaymentRecKey]
                  ,[GradRecKey]
                  ,[FinAidRecKey]
                  ,[PrivCurBalOwed]
                  ,[PrivCourseTuitCost]
                  ,[PrivCourseRegCost]
                  ,[PrivCourseKitCost]
                  ,[PrivCourseOthrCost]
                  ,[AccruedTuit]
                  ,[AccruedReg]
                  ,[AccruedKit]
                  ,[AccruedOthr]
                  ,[BirthDate]
                  ,[TermResKey]
                  ,[SubjectResKey]
                  ,[WorkUnitResKey]
                  ,[ResultTot]
                  ,[ResultCnt]
                  ,[LabCnt]
                  ,[AttendMonthKey]
                  ,[ProspectKey]
                  ,[PrivCourseRoomCost]
                  ,[StuTransKey]
                  ,[StuGroupKey]
                  ,[NotesKey]
                  ,[PhoneType1]
                  ,[PhoneType2]
                  ,[HomeEmail]
                  ,[XferSubjResKey]
                  ,[StateRegDate]
                  ,[PrevStudRecKey]
                  ,[NextStudRecKey]
                  ,[DegreeSeekingType]
                  ,[PrivStatusLogKey]
                  ,[PrivFirstTransDate]
                  ,[PrivLastTransDate]
                  ,[PrivZipPlus4]
                  ,[DropReasonCode]
                  ,[PhoneNumAreaCode2]
                  ,[PhoneNumPrefix2]
                  ,[PhoneNumBody2]
                  ,[TransHrsLnYr1]
                  ,[OhioTheoryHrs]
                  ,[OhioPracticalHrs]
                  ,[OhioDemoHrs]
                  ,[OhioClinicHrs]
                  ,[DefaultLtrRecKey]
                  ,[CreditHrs]
                  ,[TotHrsLastJune30th]
                  ,[SchdHrsLastJune30th]
                  ,[TranHrsUpFront]
                  ,[ATBscore]
                  ,[SAPInclPrevEnr]
                  ,[Ostudent]
                  ,[BirthDate1]
                  ,[Sex]
                  ,[UrbanType]
                  ,[Race]
                  ,[Dependency]
                  ,[EducationLevel]
                  ,[FullTimePartTime]
                  ,[IncomeLevel]
                  ,[Age]
                  ,[MilesFromSchool]
                  ,[Over50Miles]
                  ,[MaritalStatus]
                  ,[AllClinicInfo_1]
                  ,[AllClinicInfo_2]
                  ,[AllClinicInfo_3]
                  ,[AllClinicInfo_4]
                  ,[AllClinicInfo_5]
                  ,[AllClinicInfo_6]
                  ,[AllClinicInfo_7]
                  ,[AllClinicInfo_8]
                  ,[AllClinicInfo_9]
                  ,[AllClinicInfo_10]
                  ,[AllClinicInfo_11]
                  ,[AllClinicInfo_12]
                  ,[AllClinicInfo_13]
                  ,[AllClinicInfo_14]
                  ,[AllClinicInfo_15]
                  ,[AllClinicInfo_16]
                  ,[AllClinicInfo_17]
                  ,[AllClinicInfo_18]
                  ,[AllClinicInfo_19]
                  ,[AllClinicInfo_20]
                  ,[AllClinicInfo_21]
                  ,[AllClinicInfo_22]
                  ,[AllClinicInfo_23]
                  ,[AllClinicInfo_24]
                  ,[AllClinicInfo_25]
                  ,[AllClinicInfo_26]
                  ,[AllClinicInfo_27]
                  ,[AllClinicInfo_28]
                  ,[AllClinicInfo_29]
                  ,[AllClinicInfo_30]
                  ,[AllClinicInfo_31]
                  ,[AllClinicInfo_32]
                  ,[AllClinicInfo_33]
                  ,[AllClinicInfo_34]
                  ,[AllClinicInfo_35]
                  ,[AllClinicInfo_36]
                  ,[AllClinicInfo_37]
                  ,[AllClinicInfo_38]
                  ,[AllClinicInfo_39]
                  ,[AllClinicInfo_40]
                  ,[TotWrittenScores]
                  ,[TotPracticalScores]
                  ,[NumWrittenTaken]
                  ,[NumPracticalTaken]
                  ,[NumOfDeps]
                  ,[HeadHousehold]
                  ,[YearlyIncome]
                  ,[InHighSchool]
                  ,[CitizenType]
                  ,[HousingType]
                  ,[HighSchoolRecKey]
                  ,[HighSchGEDDate]
            FROM   [dbo].[adLeadImports];
GO
DROP TABLE [dbo].[adLeadImports];
GO
EXEC sp_rename N'[dbo].[RG_Recovery_1_adLeadImports]'
              ,N'adLeadImports'
              ,N'OBJECT';
GO
PRINT N'Creating primary key [PK_adLeadImports_LeadImportId] on [dbo].[adLeadImports]';
GO
ALTER TABLE [dbo].[adLeadImports]
ADD CONSTRAINT [PK_adLeadImports_LeadImportId]
    PRIMARY KEY CLUSTERED ( [LeadImportId] ) ON [PRIMARY];
GO

--Set NOEXEC off
SET NOEXEC OFF;

--================================================================
-- END --  US AD-3554: DBA Recomentations
--================================================================

/*
This migration script replaces uncommitted changes made to these objects:
CollegesList
References
SyTypeofRequirement
syStudentFormat

Use this script to make necessary schema and data changes for these objects only. Schema changes to any other objects won't be deployed.

Schema changes and migration scripts are deployed in the order they're committed.

Migration scripts must not reference static data. When you deploy migration scripts alongside static data 
changes, the migration scripts will run first. This can cause the deployment to fail. 
Read more at https://documentation.red-gate.com/display/SOC6/Static+data+and+migrations.
*/

--================================================================
-- START --  US AD-3354: DBA Recomentations
-- Jose A 2-14-18
--================================================================

SET NUMERIC_ROUNDABORT OFF;
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON;

IF EXISTS (
          SELECT OBJECT_NAME(object_id) AS NameofConstraint
                ,SCHEMA_NAME(schema_id) AS SchemaName
                ,OBJECT_NAME(parent_object_id) AS TableName
                ,type_desc AS ConstraintType
          FROM   sys.objects
          WHERE  type_desc LIKE '%CONSTRAINT'
                 AND OBJECT_NAME(object_id) = 'PK_CollegesList_id'
          )
    BEGIN
        PRINT N'Dropping constraints from [dbo].[CollegesList]';
        ALTER TABLE [dbo].[CollegesList]
        DROP CONSTRAINT [PK_CollegesList_id];

    END;

IF EXISTS (
          SELECT OBJECT_NAME(object_id) AS NameofConstraint
                ,SCHEMA_NAME(schema_id) AS SchemaName
                ,OBJECT_NAME(parent_object_id) AS TableName
                ,type_desc AS ConstraintType
          FROM   sys.objects
          WHERE  type_desc LIKE '%CONSTRAINT'
                 AND OBJECT_NAME(object_id) = 'PK_References_ReferenceId'
          )
    BEGIN
        PRINT N'Dropping constraints from [dbo].[References]';
        ALTER TABLE [dbo].[References]
        DROP CONSTRAINT [PK_References_ReferenceId];
    END;


IF EXISTS (
          SELECT OBJECT_NAME(object_id) AS NameofConstraint
                ,SCHEMA_NAME(schema_id) AS SchemaName
                ,OBJECT_NAME(parent_object_id) AS TableName
                ,type_desc AS ConstraintType
          FROM   sys.objects
          WHERE  type_desc LIKE '%CONSTRAINT'
                 AND OBJECT_NAME(object_id) = 'DF_References_StudentContactId'
          )
    BEGIN
        PRINT N'Dropping constraints from [dbo].[References]';
        ALTER TABLE [dbo].[References]
        DROP CONSTRAINT [DF_References_StudentContactId];
    END;


IF EXISTS (
          SELECT OBJECT_NAME(object_id) AS NameofConstraint
                ,SCHEMA_NAME(schema_id) AS SchemaName
                ,OBJECT_NAME(parent_object_id) AS TableName
                ,type_desc AS ConstraintType
          FROM   sys.objects
          WHERE  type_desc LIKE '%CONSTRAINT'
                 AND OBJECT_NAME(object_id) = 'DF_References_ReferenceId'
          )
    BEGIN
        PRINT N'Dropping constraints from [dbo].[References]';
        ALTER TABLE [dbo].[References]
        DROP CONSTRAINT [DF_References_ReferenceId];
    END;

IF EXISTS (
          SELECT OBJECT_NAME(object_id) AS NameofConstraint
                ,SCHEMA_NAME(schema_id) AS SchemaName
                ,OBJECT_NAME(parent_object_id) AS TableName
                ,type_desc AS ConstraintType
          FROM   sys.objects
          WHERE  type_desc LIKE '%CONSTRAINT'
                 AND OBJECT_NAME(object_id) = 'PK_SyTypeofRequirement_TypeOfRequirementId'
          )
    BEGIN
        PRINT N'Dropping constraints from [dbo].[SyTypeofRequirement]';
        ALTER TABLE [dbo].[SyTypeofRequirement]
        DROP CONSTRAINT [PK_SyTypeofRequirement_TypeOfRequirementId];
    END;

IF EXISTS (
          SELECT OBJECT_NAME(object_id) AS NameofConstraint
                ,SCHEMA_NAME(schema_id) AS SchemaName
                ,OBJECT_NAME(parent_object_id) AS TableName
                ,type_desc AS ConstraintType
          FROM   sys.objects
          WHERE  type_desc LIKE '%CONSTRAINT'
                 AND OBJECT_NAME(object_id) = 'DF_SyTypeofRequirement_TypeOfRequirementId'
          )
    BEGIN
        PRINT N'Dropping constraints from [dbo].[SyTypeofRequirement]';
        ALTER TABLE [dbo].[SyTypeofRequirement]
        DROP CONSTRAINT [DF_SyTypeofRequirement_TypeOfRequirementId];
    END;

IF EXISTS (
          SELECT OBJECT_NAME(object_id) AS NameofConstraint
                ,SCHEMA_NAME(schema_id) AS SchemaName
                ,OBJECT_NAME(parent_object_id) AS TableName
                ,type_desc AS ConstraintType
          FROM   sys.objects
          WHERE  type_desc LIKE '%CONSTRAINT'
                 AND OBJECT_NAME(object_id) = 'PK_syStudentFormat_StudentFormatId'
          )
    BEGIN
        PRINT N'Dropping constraints from [dbo].[syStudentFormat]';
        ALTER TABLE [dbo].[syStudentFormat]
        DROP CONSTRAINT [PK_syStudentFormat_StudentFormatId];
    END;

IF EXISTS (
          SELECT OBJECT_NAME(object_id) AS NameofConstraint
                ,SCHEMA_NAME(schema_id) AS SchemaName
                ,OBJECT_NAME(parent_object_id) AS TableName
                ,type_desc AS ConstraintType
          FROM   sys.objects
          WHERE  type_desc LIKE '%CONSTRAINT'
                 AND OBJECT_NAME(object_id) = 'DF_syStudentFormat_StudentFormatId'
          )
    BEGIN
        PRINT N'Dropping constraints from [dbo].[syStudentFormat]';
        ALTER TABLE [dbo].[syStudentFormat]
        DROP CONSTRAINT [DF_syStudentFormat_StudentFormatId];
    END;

IF EXISTS (
          SELECT *
          FROM   INFORMATION_SCHEMA.TABLES
          WHERE  TABLE_NAME = 'CollegesList'
          )
    BEGIN
        PRINT N'Dropping [dbo].[CollegesList]';
        DROP TABLE [dbo].[CollegesList];
    END;


IF EXISTS (
          SELECT *
          FROM   INFORMATION_SCHEMA.TABLES
          WHERE  TABLE_NAME = 'References'
          )
    BEGIN
        PRINT N'Dropping [dbo].[References]';
        DROP TABLE [dbo].[References];
    END;

IF NOT EXISTS (
              SELECT 1
              FROM   sys.columns
              WHERE  name = N'StudentFormatId'
                     AND object_id = OBJECT_ID(N'dbo.syStudentFormat')
              )
    BEGIN
        PRINT N'Altering [dbo].[syStudentFormat]';
        ALTER TABLE [dbo].[syStudentFormat]
        ADD [StudentFormatId] [INT] NOT NULL IDENTITY(1, 1);
    END;


IF EXISTS (
          SELECT 1
          FROM   sys.columns
          WHERE  name = N'Id'
                 AND object_id = OBJECT_ID(N'dbo.syStudentFormat')
          )
    BEGIN
        ALTER TABLE [dbo].[syStudentFormat]
        DROP COLUMN [Id];
    END;

IF NOT EXISTS (
              SELECT OBJECT_NAME(object_id) AS NameofConstraint
                    ,SCHEMA_NAME(schema_id) AS SchemaName
                    ,OBJECT_NAME(parent_object_id) AS TableName
                    ,type_desc AS ConstraintType
              FROM   sys.objects
              WHERE  type_desc LIKE '%CONSTRAINT'
                     AND OBJECT_NAME(object_id) = 'PK_syStudentFormat_StudentFormatId'
              )
    BEGIN
        PRINT N'Creating primary key [PK_syStudentFormat_StudentFormatId] on [dbo].[syStudentFormat]';
        ALTER TABLE [dbo].[syStudentFormat]
        ADD CONSTRAINT [PK_syStudentFormat_StudentFormatId]
            PRIMARY KEY CLUSTERED ( [StudentFormatId] ) ON [PRIMARY];
    END;


IF EXISTS (
          SELECT 1
          FROM   sys.columns
          WHERE  name = N'TypeOfRequirementId'
                 AND object_id = OBJECT_ID(N'dbo.TypeOfRequirementId')
          )
    BEGIN
        ALTER TABLE [dbo].[TypeOfRequirementId]
        DROP COLUMN TypeOfRequirementId;
    END;


IF NOT EXISTS (
              SELECT OBJECT_NAME(object_id) AS NameofConstraint
                    ,SCHEMA_NAME(schema_id) AS SchemaName
                    ,OBJECT_NAME(parent_object_id) AS TableName
                    ,type_desc AS ConstraintType
              FROM   sys.objects
              WHERE  type_desc LIKE '%CONSTRAINT'
                     AND OBJECT_NAME(object_id) = 'PK_SyTypeofRequirement_TypeofRequirementId'
              )
    BEGIN
        PRINT N'Creating primary key [PK_SyTypeofRequirement_TypeofRequirementId] on [dbo].[SyTypeofRequirement]';
        ALTER TABLE [dbo].[SyTypeofRequirement]
        ADD CONSTRAINT [PK_SyTypeofRequirement_TypeofRequirementId]
            PRIMARY KEY CLUSTERED ( [TypeOfRequirementId] ) ON [PRIMARY];
    END;

--================================================================
-- END --  US AD-3354: DBA Recomentations
--================================================================