-- ===============================================================================================
-- START Consolidated Script Version 4.1
-- Data Changes Zone
-- Please do not deploy your schema changes in this area
-- Please use SQL Prompt format before insert here please
-- Please run after Schema changes....
-- ===============================================================================================
--=================================================================================================
-- START AD-10769 Changes to the existing 'State Board Settings' page
--=================================================================================================
DECLARE @Error AS INT;
SET @Error = 0;
BEGIN TRANSACTION AddAgenciesStateBoardTrans;
BEGIN TRY

    UPDATE dbo.syMenuItems
    SET    MenuName = 'State Board/Accrediting Agency Settings'
          ,DisplayName = 'State Board/Accrediting Agency Settings'
    WHERE  ResourceId = 863
           AND DisplayName = 'State Board Settings';

    DECLARE @ActiveStatusId UNIQUEIDENTIFIER = (
                                               SELECT TOP 1 StatusId
                                               FROM   dbo.syStatuses
                                               WHERE  StatusCode = 'A'
                                               );
    IF NOT EXISTS (
                  SELECT TOP 1 *
                  FROM   dbo.syAccreditingAgencies
                  WHERE  Code = 'NACCAS'
                  )
        BEGIN
            INSERT INTO dbo.syAccreditingAgencies (
                                                  AccreditingAgencyId
                                                 ,Code
                                                 ,Description
                                                 ,ModUser
                                                 ,ModDate
                                                 ,StatusId
                                                  )
            VALUES ((
                    SELECT ISNULL(MAX(AccreditingAgencyId), 0) + 1
                    FROM   dbo.syAccreditingAgencies
                    )               -- AccreditingAgencyId - int
                   ,'NACCAS'        -- Code - varchar(80)
                   ,'NACCAS'        -- Description - varchar(200)
                   ,'sa'            -- ModUser - varchar(50)
                   ,GETDATE()       -- ModDate - datetime
                   ,@ActiveStatusId -- StatusId - uniqueidentifier
                );
        END;

    IF NOT EXISTS (
                  SELECT TOP 1 *
                  FROM   dbo.syAccreditingAgencies
                  WHERE  Code = 'COE'
                  )
        BEGIN
            INSERT INTO dbo.syAccreditingAgencies (
                                                  AccreditingAgencyId
                                                 ,Code
                                                 ,Description
                                                 ,ModUser
                                                 ,ModDate
                                                 ,StatusId
                                                  )
            VALUES ((
                    SELECT ISNULL(MAX(AccreditingAgencyId), 0) + 1
                    FROM   dbo.syAccreditingAgencies
                    )               -- AccreditingAgencyId - int
                   ,'COE'           -- Code - varchar(80)
                   ,'COE'           -- Description - varchar(200)
                   ,'sa'            -- ModUser - varchar(50)
                   ,GETDATE()       -- ModDate - datetime
                   ,@ActiveStatusId -- StatusId - uniqueidentifier
                );
        END;


END TRY
BEGIN CATCH
    SET @Error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE()
          ,@severity = ERROR_SEVERITY()
          ,@state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @Error > 0
    BEGIN
        ROLLBACK TRANSACTION AddAgenciesStateBoardTrans;
        PRINT 'Failed transcation AddAgenciesStateBoardTrans.';

    END;
ELSE
    BEGIN
        COMMIT TRANSACTION AddAgenciesStateBoardTrans;
        PRINT 'successful transaction AddAgenciesStateBoardTrans.';
    END;
GO
--=================================================================================================
-- END AD-10769 Changes to the existing 'State Board Settings' page
--=================================================================================================
--=================================================================================================
-- START AD-10776 NACCAS Preliminary and Fall Annual report generation parameters
--=================================================================================================
DECLARE @ErrorAddingNACCASPreliminaryAndFallAnnualReport AS INT;
SET @ErrorAddingNACCASPreliminaryAndFallAnnualReport = 0;
BEGIN TRANSACTION AddNACCASPrelAndFallAnnualReport;
BEGIN TRY
    DECLARE @ResourceId_NACCASPreliminaryAndFallAnnual INT = 846;
    DECLARE @ReportName_NACCASPreliminaryAndFallAnnual VARCHAR(30) = 'Preliminary and Fall Annual';
    DECLARE @ReportNameFull_NACCASPreliminaryAndFallAnnual VARCHAR(30) = @ReportName_NACCASPreliminaryAndFallAnnual + ' Report';
    DECLARE @ReportId_NACCASPreliminaryAndFallAnnual UNIQUEIDENTIFIER;
    DECLARE @ItemCaption_NACCASPreliminaryAndFallAnnual VARCHAR(MAX) = 'Preliminary and Fall Annual Options';
    DECLARE @AscsView_NACCASPreliminaryAndFallAnnual VARCHAR(MAX) = 'ParamNACCASPreliminaryAndFallAnnualReport';
    DECLARE @SetName_NACCASPreliminaryAndFallAnnual VARCHAR(30) = 'PreliminaryAndFallAnnualRerpotSet';
    DECLARE @SectionName_NACCASPreliminaryAndFallAnnual VARCHAR(30) = 'PreliminaryAndFallAnnualRerpotSection';


    DECLARE @ItemId_NACCASPreliminaryAndFallAnnual INT = 0;
    DECLARE @SetId_NACCASPreliminaryAndFallAnnual INT = 0;
    DECLARE @SectionId_NACCASPreliminaryAndFallAnnual INT = 0;

    EXEC USP_Report_CreateReportRecord @ResourceId = @ResourceId_NACCASPreliminaryAndFallAnnual
                                      ,@ReportName = @ReportName_NACCASPreliminaryAndFallAnnual
                                      ,@ReportTableLayout = 0
                                      ,@Return_ReportId = @ReportId_NACCASPreliminaryAndFallAnnual OUTPUT;



    EXEC @ItemId_NACCASPreliminaryAndFallAnnual = USP_Report_CreateParamItem @Caption = @ItemCaption_NACCASPreliminaryAndFallAnnual
                                                                            ,@ViewNameNoExtension = @AscsView_NACCASPreliminaryAndFallAnnual;



    EXEC @SetId_NACCASPreliminaryAndFallAnnual = USP_Report_CreateParamSet @SetName = @SetName_NACCASPreliminaryAndFallAnnual
                                                                          ,@ReportName = @ReportNameFull_NACCASPreliminaryAndFallAnnual
                                                                          ,@Type = 'Custom';


    EXEC @SectionId_NACCASPreliminaryAndFallAnnual = dbo.USP_Report_CreateParamSection @SectionName = @SectionName_NACCASPreliminaryAndFallAnnual -- varchar(max)
                                                                                      ,@Caption = 'Filter Options'                                -- varchar(max)
                                                                                      ,@SetId = @SetId_NACCASPreliminaryAndFallAnnual             -- int
                                                                                      ,@Sequence = 1                                              -- int
                                                                                      ,@Description = 'Filter Options';                           -- varchar(max)



    EXEC dbo.USP_Report_CreateParamDetail @ItemId = @ItemId_NACCASPreliminaryAndFallAnnual       -- int
                                         ,@SectionId = @SectionId_NACCASPreliminaryAndFallAnnual -- int
                                         ,@Sequence = 1;                                         -- int


    EXEC dbo.USP_Report_CreateReportTab @ReportId = @ReportId_NACCASPreliminaryAndFallAnnual      -- uniqueidentifier
                                       ,@FilterSetId = 0                                          -- int
                                       ,@OptionsSetId = @SetId_NACCASPreliminaryAndFallAnnual     -- int
                                       ,@SortSetId = 0                                            -- int
                                       ,@SetName = @SetName_NACCASPreliminaryAndFallAnnual        -- varchar(max)
                                       ,@ReportName = @ReportName_NACCASPreliminaryAndFallAnnual; -- varchar(max)




    ----------------------------------------------------------------------------------------------------------------			
    -----NACCAS REPORTS	RECORDS	
    ----------------------------------------------------------------------------------------------------------------	

    DECLARE @NaccasRootDirRDL VARCHAR(MAX) = 'NACCAS';

    DECLARE @Instructions_ResourceId INT = 849;
    DECLARE @EnrollmentCounts_ResourceId INT = 850;
    DECLARE @CohortGrid_ResourceId INT = 851;
    DECLARE @Summary_ResourceId INT = 852;

    EXEC USP_Report_CreateReportRecord @ResourceId = @Instructions_ResourceId
                                      ,@ReportName = 'Instructions'
                                      ,@ReportTableLayout = 0
                                      ,@RDLDir = @NaccasRootDirRDL
                                      ,@Return_ReportId = @ReportId_NACCASPreliminaryAndFallAnnual OUTPUT;

    EXEC USP_Report_CreateReportRecord @ResourceId = @EnrollmentCounts_ResourceId
                                      ,@ReportName = 'Enrollment Counts'
                                      ,@ReportTableLayout = 0
                                      ,@RDLDir = @NaccasRootDirRDL
                                      ,@Return_ReportId = @ReportId_NACCASPreliminaryAndFallAnnual OUTPUT;

    EXEC USP_Report_CreateReportRecord @ResourceId = @CohortGrid_ResourceId
                                      ,@ReportName = 'Cohort Grid'
                                      ,@ReportTableLayout = 0
                                      ,@RDLDir = @NaccasRootDirRDL
                                      ,@Return_ReportId = @ReportId_NACCASPreliminaryAndFallAnnual OUTPUT;

    EXEC USP_Report_CreateReportRecord @ResourceId = @Summary_ResourceId
                                      ,@ReportName = 'Summary'
                                      ,@ReportTableLayout = 0
                                      ,@RDLDir = @NaccasRootDirRDL
                                      ,@Return_ReportId = @ReportId_NACCASPreliminaryAndFallAnnual OUTPUT;



END TRY
BEGIN CATCH
    SET @ErrorAddingNACCASPreliminaryAndFallAnnualReport = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE()
          ,@severity = ERROR_SEVERITY()
          ,@state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @ErrorAddingNACCASPreliminaryAndFallAnnualReport > 0
    BEGIN
        ROLLBACK TRANSACTION AddNACCASPrelAndFallAnnualReport;
        PRINT 'Failed transcation AddNACCASPrelAndFallAnnualReport.';

    END;
ELSE
    BEGIN
        COMMIT TRANSACTION AddNACCASPrelAndFallAnnualReport;
        PRINT 'successful transaction AddNACCASPrelAndFallAnnualReport.';
    END;
GO
--=================================================================================================
-- END AD-10776 NACCAS Preliminary and Fall Annual report generation parameters
--=================================================================================================

--=================================================================================================
-- START AD-11517 Bug Fix for Summary Attendance Report
--=================================================================================================
DECLARE @ErrorSummaryAttendanceReportBugFix AS INT;
SET @ErrorSummaryAttendanceReportBugFix = 0;
BEGIN TRANSACTION SummaryAttendanceReportBugFix;
BEGIN TRY
    DECLARE @SummaryAttendanceReportResourceId INT = 877;
    UPDATE dbo.syReports
    SET    ReportDescription = 'Summary Attendance'
    WHERE  ResourceId = @SummaryAttendanceReportResourceId;
END TRY
BEGIN CATCH
    SET @ErrorSummaryAttendanceReportBugFix = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE()
          ,@severity = ERROR_SEVERITY()
          ,@state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @ErrorSummaryAttendanceReportBugFix > 0
    BEGIN
        ROLLBACK TRANSACTION SummaryAttendanceReportBugFix;
        PRINT 'Failed transcation SummaryAttendanceReportBugFix.';

    END;
ELSE
    BEGIN
        COMMIT TRANSACTION SummaryAttendanceReportBugFix;
        PRINT 'successful transaction SummaryAttendanceReportBugFix.';
    END;
GO
--=================================================================================================
-- END AD-11517 Bug Fix for Summary Attendance Report
--=================================================================================================
-- ===============================================================================================
-- START AD-8506 : Update NACCAS Report Menu in Advantage
-- ===============================================================================================
DECLARE @error INT = 0;
BEGIN TRANSACTION NaccasResIds;
BEGIN TRY
    -- delete resourceids 846,847,848,853,854,856,857,858,859
    IF EXISTS (
              SELECT 1
              FROM   syNavigationNodes
              WHERE  ResourceId IN ( 847, 848, 849, 850, 851, 852, 853, 854, 855, 856, 857, 858, 859 )
              )
        BEGIN
            DELETE FROM syNavigationNodes
            WHERE ResourceId IN ( 847, 848, 849, 850, 851, 852, 853, 854, 855, 856, 857, 858, 859 );
        END;
    IF ( @@ERROR > 1 )
        BEGIN
            SET @error = 1;
        END;
    IF EXISTS (
              SELECT 1
              FROM   syMenuItems
              WHERE  ResourceId IN ( 847, 848, 849, 850, 851, 852, 853, 854, 855, 856, 857, 858, 859 )
              )
        BEGIN
            DELETE FROM syMenuItems
            WHERE ResourceId IN ( 847, 848, 849, 850, 851, 852, 853, 854, 855, 856, 857, 858, 859 );
        END;
    IF ( @@ERROR > 1 )
        BEGIN
            SET @error = 1;
        END;
    IF EXISTS (
              SELECT 1
              FROM   syRlsResLvls
              WHERE  ResourceID IN ( 847, 848, 853, 854, 856, 857, 858, 859 )
              )
        BEGIN
            DELETE FROM syRlsResLvls
            WHERE ResourceID IN ( 847, 848, 853, 854, 856, 857, 858, 859 );
        END;
    IF ( @@ERROR > 1 )
        BEGIN
            SET @error = 1;
        END;

    IF EXISTS (
              SELECT 1
              FROM   syResources
              WHERE  ResourceID IN ( 847, 848, 853, 854, 856, 857, 858, 859 )
              )
        BEGIN
            DELETE FROM syResources
            WHERE ResourceID IN ( 847, 848, 853, 854, 856, 857, 858, 859 );
        END;
    IF ( @@ERROR > 1 )
        BEGIN
            SET @error = 1;
        END;
    -- update the resource Names and resource types and parent Id
    IF EXISTS (
              SELECT 1
              FROM   syResources
              WHERE  ResourceID = 845
                     AND Resource = 'Preliminary Annual Report'
              )
        BEGIN
            UPDATE syResources
            SET    Resource = 'NACCAS Reports'
            WHERE  ResourceID = 845;
        END;
    IF ( @@ERROR > 1 )
        BEGIN
            SET @error = 1;
        END;
    IF EXISTS (
              SELECT 1
              FROM   syMenuItems
              WHERE  ResourceId = 845
                     AND MenuName = 'Preliminary Annual Report'
              )
        BEGIN
            UPDATE syMenuItems
            SET    MenuName = 'NACCAS Reports'
                  ,DisplayName = 'NACCAS Reports'
            WHERE  ResourceId = 845;
        END;
    IF ( @@ERROR > 1 )
        BEGIN
            SET @error = 1;
        END;
    IF EXISTS (
              SELECT 1
              FROM   syResources
              WHERE  ResourceID = 846
                     AND Resource = 'Fall Annual Report'
              )
        BEGIN
            UPDATE syResources
            SET    Resource = 'NACCAS Preliminary and Fall Annual Reports'
                  ,ResourceTypeID = 5
                  ,ResourceURL = '~/sy/ParamReport.aspx'
            WHERE  ResourceID = 846;
        END;
    IF ( @@ERROR > 1 )
        BEGIN
            SET @error = 1;
        END;
    IF EXISTS (
              SELECT 1
              FROM   syMenuItems
              WHERE  ResourceId = 846
                     AND MenuName = 'Fall Annual Report'
              )
        BEGIN
            DECLARE @parentId INT;
            SELECT @parentId = (
                               SELECT MenuItemId
                               FROM   syMenuItems
                               WHERE  ResourceId = 845
                               );
            UPDATE syMenuItems
            SET    MenuName = 'NACCAS Preliminary and Fall Annual Reports'
                  ,Url = '/SY/ParamReport.aspx'
                  ,DisplayName = 'NACCAS Preliminary and Fall Annual Reports'
                  ,MenuItemTypeId = 4
                  ,ParentId = @parentId
            WHERE  ResourceId = 846;
        END;
    IF ( @@ERROR > 1 )
        BEGIN
            SET @error = 1;
        END;
    IF EXISTS (
              SELECT 1
              FROM   syResources
              WHERE  ResourceID = 850
                     AND Resource = 'Enrollment Count'
              )
        BEGIN
            UPDATE syResources
            SET    Resource = 'Enrollment Counts'
            WHERE  ResourceID = 850;
        END;
    IF ( @@ERROR > 1 )
        BEGIN
            SET @error = 1;
        END;
    IF EXISTS (
              SELECT 1
              FROM   syResources
              WHERE  ResourceID = 855
                     AND Resource = 'Cohort Grid-Exempted Student List Export'
              )
        BEGIN
            UPDATE syResources
            SET    Resource = 'Exempted Students List'
            WHERE  ResourceID = 855;
        END;
    IF ( @@ERROR > 1 )
        BEGIN
            SET @error = 1;
        END;
    -- changing the parent id in synavigation
    DECLARE @parentId1 UNIQUEIDENTIFIER;
    SET @parentId1 = (
                     SELECT HierarchyId
                     FROM   syNavigationNodes
                     WHERE  ResourceId = 844
                     );
    IF NOT EXISTS (
                  SELECT 1
                  FROM   syNavigationNodes
                  WHERE  ResourceId = 845
                         AND ParentId = @parentId1
                  )
        BEGIN
            UPDATE syNavigationNodes
            SET    ParentId = @parentId1
            WHERE  ResourceId = 845;
        END;
    IF ( @@ERROR > 1 )
        BEGIN
            SET @error = 1;
        END;
    DECLARE @parentIdN UNIQUEIDENTIFIER;
    SET @parentIdN = (
                     SELECT HierarchyId
                     FROM   syNavigationNodes
                     WHERE  ResourceId = 845
                     );
    IF NOT EXISTS (
                  SELECT 1
                  FROM   syNavigationNodes
                  WHERE  ResourceId = 846
                         AND ParentId = @parentIdN
                  )
        BEGIN
            UPDATE syNavigationNodes
            SET    ParentId = @parentIdN
                  ,HierarchyIndex = 3
            WHERE  ResourceId = 846;
        END;
    IF ( @@ERROR > 1 )
        BEGIN
            SET @error = 1;
        END;
END TRY
BEGIN CATCH
    SET @error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE()
          ,@severity = ERROR_SEVERITY()
          ,@state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
    BEGIN
        ROLLBACK TRANSACTION NaccasResIds;
        PRINT 'Failed to delete and update Naccas resourceIds.';

    END;
ELSE
    BEGIN
        COMMIT TRANSACTION NaccasResIds;
        PRINT 'Successfully deleted and updated Naccas resouceIds.';
    END;
GO
-- ===============================================================================================
-- END AD-8506 : Update NACCAS Report Menu in Advantage
-- ===============================================================================================
--=================================================================================================
-- START AD-10780 Ability to specify eligibility and reasons for ineligiblity for placement under Placements menu
--=================================================================================================
DECLARE @Error AS INT;
SET @Error = 0;
BEGIN TRANSACTION AddIneligibilityReasons;
BEGIN TRY

    IF ( EXISTS (
                SELECT *
                FROM   INFORMATION_SCHEMA.TABLES
                WHERE  TABLE_NAME = 'syIneligibilityReasons'
                )
       )
        BEGIN

            --/////////////////Deceased////////////////////////
            IF NOT EXISTS (
                          SELECT TOP 1 *
                          FROM   dbo.syIneligibilityReasons
                          WHERE  InelReasonName LIKE 'Deceased'
                          )
                BEGIN
                    INSERT INTO dbo.syIneligibilityReasons (
                                                           InelReasonId
                                                          ,InelReasonName
                                                          ,InelReasonCode
                                                           )
                    VALUES ( NEWID()    -- InelReasonId - uniqueidentifier
                            ,'Deceased' -- InelReasonName - varchar(255)
                            ,'DEC'      -- InelReasonCode - varchar(3)
                        );
                END;
            --/////////////////Permanently disabled////////////////////////
            IF NOT EXISTS (
                          SELECT TOP 1 *
                          FROM   dbo.syIneligibilityReasons
                          WHERE  InelReasonName LIKE 'Permanently disabled'
                          )
                BEGIN
                    INSERT INTO dbo.syIneligibilityReasons (
                                                           InelReasonId
                                                          ,InelReasonName
                                                          ,InelReasonCode
                                                           )
                    VALUES ( NEWID()                -- InelReasonId - uniqueidentifier
                            ,'Permanently disabled' -- InelReasonName - varchar(255)
                            ,'PD'                   -- InelReasonCode - varchar(3)
                        );
                END;
            --/////////////////Deployed for military service / duty ////////////////////////
            IF NOT EXISTS (
                          SELECT TOP 1 *
                          FROM   dbo.syIneligibilityReasons
                          WHERE  InelReasonName LIKE 'Deployed for military service / duty'
                          )
                BEGIN
                    INSERT INTO dbo.syIneligibilityReasons (
                                                           InelReasonId
                                                          ,InelReasonName
                                                          ,InelReasonCode
                                                           )
                    VALUES ( NEWID()                                -- InelReasonId - uniqueidentifier
                            ,'Deployed for military service / duty' -- InelReasonName - varchar(255)
                            ,'DM'                                   -- InelReasonCode - varchar(3)
                        );
                END;
            --/////////////////Studied under a student visa ////////////////////////
            IF NOT EXISTS (
                          SELECT TOP 1 *
                          FROM   dbo.syIneligibilityReasons
                          WHERE  InelReasonName LIKE 'Studied under a student visa'
                          )
                BEGIN
                    INSERT INTO dbo.syIneligibilityReasons (
                                                           InelReasonId
                                                          ,InelReasonName
                                                          ,InelReasonCode
                                                           )
                    VALUES ( NEWID()                        -- InelReasonId - uniqueidentifier
                            ,'Studied under a student visa' -- InelReasonName - varchar(255)
                            ,'SUV'                          -- InelReasonCode - varchar(3)
                        );
                END;
            --/////////////////Continuing education in any school under same ownership ////////////////////////
            IF NOT EXISTS (
                          SELECT TOP 1 *
                          FROM   dbo.syIneligibilityReasons
                          WHERE  InelReasonName LIKE 'Continuing education in any school under same ownership'
                          )
                BEGIN
                    INSERT INTO dbo.syIneligibilityReasons (
                                                           InelReasonId
                                                          ,InelReasonName
                                                          ,InelReasonCode
                                                           )
                    VALUES ( NEWID()                                                   -- InelReasonId - uniqueidentifier
                            ,'Continuing education in any school under same ownership' -- InelReasonName - varchar(255)
                            ,'CE'                                                      -- InelReasonCode - varchar(3)
                        );
                END;
        END;



END TRY
BEGIN CATCH
    SET @Error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE()
          ,@severity = ERROR_SEVERITY()
          ,@state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @Error > 0
    BEGIN
        ROLLBACK TRANSACTION AddIneligibilityReasons;
        PRINT 'Failed transcation AddIneligibilityReasons.';

    END;
ELSE
    BEGIN
        COMMIT TRANSACTION AddIneligibilityReasons;
        PRINT 'successful transaction AddIneligibilityReasons.';
    END;
GO
--=================================================================================================
-- END AD-10780 Ability to specify eligibility and reasons for ineligiblity for placement under Placements menu
--=================================================================================================
--=================================================================================================
-- START AD-10770 Advanatage drop reason mapping to NACCAS drop reasons section on 'State Board/Accrediting Agency Settings' page
--=================================================================================================
DECLARE @Error AS INT;
SET @Error = 0;
BEGIN TRANSACTION AddNACCASDropReasons;
BEGIN TRY

    IF ( EXISTS (
                SELECT *
                FROM   INFORMATION_SCHEMA.TABLES
                WHERE  TABLE_NAME = 'syNACCASDropReasons'
                )
       )
        BEGIN

            --/////////////////Deceased////////////////////////
            IF NOT EXISTS (
                          SELECT TOP 1 *
                          FROM   dbo.syNACCASDropReasons
                          WHERE  [Name] LIKE 'Deceased'
                          )
                BEGIN
                    INSERT INTO dbo.syNACCASDropReasons (
                                                        NACCASDropReasonId
                                                       ,[Name]
                                                       ,Code
                                                        )
                    VALUES ( NEWID()    -- NACCASDropReasonId - uniqueidentifier
                            ,'Deceased' -- NACCASDropReasonName - varchar(255)
                            ,'DEC'      -- NACCASDropReasonCode - varchar(3)
                        );

                END;
            --/////////////////Temporarily disabled////////////////////////
            IF NOT EXISTS (
                          SELECT TOP 1 *
                          FROM   dbo.syNACCASDropReasons
                          WHERE  [Name] LIKE 'Temporarily disabled'
                          )
                BEGIN
                    INSERT INTO dbo.syNACCASDropReasons (
                                                        NACCASDropReasonId
                                                       ,[Name]
                                                       ,Code
                                                        )
                    VALUES ( NEWID()                -- NACCASDropReasonId - uniqueidentifier
                            ,'Temporarily disabled' -- NACCASDropReasonName - varchar(255)
                            ,'TD'                   -- NACCASDropReasonCode - varchar(3)
                        );

                END;
            --///////////////// Permanently disabled////////////////////////
            IF NOT EXISTS (
                          SELECT TOP 1 *
                          FROM   dbo.syNACCASDropReasons
                          WHERE  [Name] LIKE 'Permanently disabled'
                          )
                BEGIN
                    INSERT INTO dbo.syNACCASDropReasons (
                                                        NACCASDropReasonId
                                                       ,[Name]
                                                       ,Code
                                                        )
                    VALUES ( NEWID()                -- NACCASDropReasonId - uniqueidentifier
                            ,'Permanently disabled' -- NACCASDropReasonName - varchar(255)
                            ,'PD'                   -- NACCASDropReasonCode - varchar(3)
                        );

                END;
            --///////////////// Health/medical problems////////////////////////
            IF NOT EXISTS (
                          SELECT TOP 1 *
                          FROM   dbo.syNACCASDropReasons
                          WHERE  [Name] LIKE 'Health/medical problems'
                          )
                BEGIN
                    INSERT INTO dbo.syNACCASDropReasons (
                                                        NACCASDropReasonId
                                                       ,[Name]
                                                       ,Code
                                                        )
                    VALUES ( NEWID()                   -- NACCASDropReasonId - uniqueidentifier
                            ,'Health/medical problems' -- NACCASDropReasonName - varchar(255)
                            ,'HM'                      -- NACCASDropReasonCode - varchar(3)
                        );

                END;
            --/////////////////Transferred to an equivalent program at another school with same accreditation////////////////////////
            IF NOT EXISTS (
                          SELECT TOP 1 *
                          FROM   dbo.syNACCASDropReasons
                          WHERE  [Name] LIKE 'Transferred to an equivalent program at another school with same accreditation'
                          )
                BEGIN
                    INSERT INTO dbo.syNACCASDropReasons (
                                                        NACCASDropReasonId
                                                       ,[Name]
                                                       ,Code
                                                        )
                    VALUES ( NEWID()                                                                          -- NACCASDropReasonId - uniqueidentifier
                            ,'Transferred to an equivalent program at another school with same accreditation' -- NACCASDropReasonName - varchar(255)
                            ,'T'                                                                              -- NACCASDropReasonCode - varchar(3)
                        );

                END;
            --/////////////////Military Transfer////////////////////////
            IF NOT EXISTS (
                          SELECT TOP 1 *
                          FROM   dbo.syNACCASDropReasons
                          WHERE  [Name] LIKE 'Military Transfer'
                          )
                BEGIN
                    INSERT INTO dbo.syNACCASDropReasons (
                                                        NACCASDropReasonId
                                                       ,[Name]
                                                       ,Code
                                                        )
                    VALUES ( NEWID()             -- NACCASDropReasonId - uniqueidentifier
                            ,'Military Transfer' -- NACCASDropReasonName - varchar(255)
                            ,'MT'                -- NACCASDropReasonCode - varchar(3)
                        );

                END;
            --/////////////////Call to Active Duty////////////////////////
            IF NOT EXISTS (
                          SELECT TOP 1 *
                          FROM   dbo.syNACCASDropReasons
                          WHERE  [Name] LIKE 'Call to Active Duty'
                          )
                BEGIN
                    INSERT INTO dbo.syNACCASDropReasons (
                                                        NACCASDropReasonId
                                                       ,[Name]
                                                       ,Code
                                                        )
                    VALUES ( NEWID()               -- NACCASDropReasonId - uniqueidentifier
                            ,'Call to Active Duty' -- NACCASDropReasonName - varchar(255)
                            ,'CAD'                 -- NACCASDropReasonCode - varchar(3)
                        );

                END;

        END;

END TRY
BEGIN CATCH
    SET @Error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE()
          ,@severity = ERROR_SEVERITY()
          ,@state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @Error > 0
    BEGIN
        ROLLBACK TRANSACTION AddNACCASDropReasons;
        PRINT 'Failed transcation AddNACCASDropReasons.';

    END;
ELSE
    BEGIN
        COMMIT TRANSACTION AddNACCASDropReasons;
        PRINT 'successful transaction AddNACCASDropReasons.';
    END;
GO
--=================================================================================================
-- END AD-10770 Advanatage drop reason mapping to NACCAS drop reasons section on 'State Board/Accrediting Agency Settings' page
--=================================================================================================
--=================================================================================================
-- START AD-10886 NACCAS Report - Run Report Page
--=================================================================================================
DECLARE @Error AS INT;
SET @Error = 0;
BEGIN TRANSACTION NacaasTrans10886;
BEGIN TRY
    UPDATE syReports
    SET    ReportTabLayout = 1
          ,AllowedExportTypes = 4 -- pdf, excel
    WHERE  ResourceId = 846;
END TRY
BEGIN CATCH
    SET @Error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE()
          ,@severity = ERROR_SEVERITY()
          ,@state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @Error > 0
    BEGIN
        ROLLBACK TRANSACTION NacaasTrans10886;
        PRINT 'Failed transaction NacaasTrans10886.';

    END;
ELSE
    BEGIN
        COMMIT TRANSACTION NacaasTrans10886;
        PRINT 'successful transaction NacaasTrans10886.';
    END;
GO
--=================================================================================================
-- END AD-10886 NACCAS Report - Run Report Page
--=================================================================================================
--=================================================================================================
-- START AD-11637 : Creating term without campus group is throwing a foreign key constraint error.
--=================================================================================================
DECLARE @Error AS INT;
SET @Error = 0;
BEGIN TRANSACTION campReqForTermPage;
BEGIN TRY
    IF NOT EXISTS (
                  SELECT 1
                  FROM   dbo.syResTblFlds
                  WHERE  ResourceId = 6
                         AND ControlName = 'ddlCampGrpId'
                         AND Required = 1
                  )
        UPDATE dbo.syResTblFlds
        SET    Required = 1
        WHERE  ResourceId = 6
               AND ControlName = 'ddlCampGrpId';
END TRY
BEGIN CATCH
    SET @Error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE()
          ,@severity = ERROR_SEVERITY()
          ,@state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @Error > 0
    BEGIN
        ROLLBACK TRANSACTION campReqForTermPage;
        PRINT 'Failed transaction campReqForTermPage.';

    END;
ELSE
    BEGIN
        COMMIT TRANSACTION campReqForTermPage;
        PRINT 'successful transaction campReqForTermPage.';
    END;
GO
--=================================================================================================
-- END AD-11637 : Creating term without campus group is throwing a foreign key constraint error.
--=================================================================================================
--=================================================================================================
-- START AD-12063 AD-10776: NACCAS missing in Report name, Annual missing for Preliminary Report type and program version displayed instead of program
--=================================================================================================
DECLARE @ErrorRenameNACCASReportPageTitleTransaction AS INT;
SET @ErrorRenameNACCASReportPageTitleTransaction = 0;
BEGIN TRANSACTION RenameNACCASReportTitleTrans;
BEGIN TRY
    DECLARE @ResourceId_NACCASPreliminaryAndFallAnnual INT = 846;

    UPDATE syReports
    SET    ReportDescription = 'NACCAS Preliminary and Fall Annual Reports'
    WHERE  ResourceId = @ResourceId_NACCASPreliminaryAndFallAnnual;
END TRY
BEGIN CATCH
    SET @ErrorRenameNACCASReportPageTitleTransaction = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE()
          ,@severity = ERROR_SEVERITY()
          ,@state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @ErrorRenameNACCASReportPageTitleTransaction > 0
    BEGIN
        ROLLBACK TRANSACTION RenameNACCASReportTitleTrans;
        PRINT 'Failed transcation RenameNACCASReportPageTitleTransaction.';

    END;
ELSE
    BEGIN
        COMMIT TRANSACTION RenameNACCASReportTitleTrans;
        PRINT 'successful transaction RenameNACCASReportPageTitleTransaction.';
    END;
GO
--=================================================================================================
-- END AD-12063 AD-10776: NACCAS missing in Report name, Annual missing for Preliminary Report type and program version displayed instead of program
--=================================================================================================
--=================================================================================================
-- START AD-6525 : Create API Role For LMS User
--=================================================================================================
DECLARE @ErrorApiRole AS INT;
SET @ErrorApiRole = 0;
BEGIN TRANSACTION createLMSRoleTrans;
BEGIN TRY
    IF NOT EXISTS (
                  SELECT 1
                  FROM   dbo.sySysRoles
                  WHERE  Descrip = 'LMS API'
                         AND RoleTypeId = 2
                  )
        BEGIN
            DECLARE @sysRoleId INT;
            SET @sysRoleId = (
                             SELECT MAX(SysRoleId)
                             FROM   dbo.sySysRoles
                             ) + 1;
            INSERT INTO dbo.sySysRoles (
                                       SysRoleId
                                      ,Descrip
                                      ,StatusId
                                      ,ModUser
                                      ,ModDate
                                      ,RoleTypeId
                                      ,Permission
                                       )
            VALUES ( @sysRoleId                                                                                                                                                                                                                                                                                                                      -- SysRoleId - int
                    ,'LMS API'                                                                                                                                                                                                                                                                                                                       -- Descrip - varchar(80)
                    ,'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'                                                                                                                                                                                                                                                                                          -- StatusId - uniqueidentifier
                    ,'Support'                                                                                                                                                                                                                                                                                                                       -- ModUser - varchar(50)
                    ,GETDATE()                                                                                                                                                                                                                                                                                                                       -- ModDate - datetime
                    ,2                                                                                                                                                                                                                                                                                                                               -- RoleTypeId - int
                    ,N'{"modules": [           {"name": "AR","level": "modify"}, {"name": "AD","level": "none"},            {"name": "FC","level": "none"}, {"name": "FA","level": "none"},            {"name": "HR","level": "none"}, {"name": "PL","level": "none"},            {"name": "SA","level": "none"}, {"name": "SY","level": "read"} ]}' -- Permission - nvarchar(max)
                );
            IF ( @@ERROR > 0 )
                BEGIN
                    SET @ErrorApiRole = 1;
                END;
            IF NOT EXISTS (
                          SELECT 1
                          FROM   syRoles
                          WHERE  SysRoleId = @sysRoleId
                          )
                BEGIN
                    INSERT INTO dbo.syRoles (
                                            RoleId
                                           ,Role
                                           ,Code
                                           ,StatusId
                                           ,SysRoleId
                                           ,ModDate
                                           ,ModUser
                                            )
                    VALUES ( NEWID()                                -- RoleId - uniqueidentifier
                            ,'LMS API'                              -- Role - varchar(60)
                            ,'LMS_API'                              -- Code - varchar(12)
                            ,'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' -- StatusId - uniqueidentifier
                            ,@sysRoleId                             -- SysRoleId - int
                            ,GETDATE()                              -- ModDate - datetime
                            ,'Support'                              -- ModUser - varchar(50)
                        );
                END;
            IF ( @@ERROR > 0 )
                BEGIN
                    SET @ErrorApiRole = 1;
                END;
        END;

END TRY
BEGIN CATCH
    SET @ErrorApiRole = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE()
          ,@severity = ERROR_SEVERITY()
          ,@state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @ErrorApiRole > 0
    BEGIN
        ROLLBACK TRANSACTION createLMSRoleTrans;
        PRINT 'Failed transcation createLMSRoleTrans.';

    END;
ELSE
    BEGIN
        COMMIT TRANSACTION createLMSRoleTrans;
        PRINT 'successful transaction createLMSRoleTrans.';
    END;
GO
--=================================================================================================
-- END AD-6525 : Create API Role For LMS User
--=================================================================================================
--=================================================================================================
-- START AD-10846 NACCAS Report-Exempted Students List
--=================================================================================================
DECLARE @Error AS INT;
SET @Error = 0;
BEGIN TRANSACTION ExemptedStudentReportTran;
BEGIN TRY
    IF NOT EXISTS (
                  SELECT TOP 1 ReportId
                  FROM   syReports
                  WHERE  ResourceId = '855'
                  )
        BEGIN
            INSERT INTO dbo.syReports (
                                      ReportId
                                     ,ResourceId
                                     ,ReportName
                                     ,ReportDescription
                                     ,RDLName
                                     ,AssemblyFilePath
                                     ,ReportClass
                                     ,CreationMethod
                                     ,WebServiceUrl
                                     ,RecordLimit
                                     ,AllowedExportTypes
                                     ,ReportTabLayout
                                     ,ShowPerformanceMsg
                                     ,ShowFilterMode
                                      )
            VALUES ( NEWID()                                           -- ReportId - uniqueidentifier
                    ,855                                               -- ResourceId - int
                    ,'Exempted Students'                               -- ReportName - varchar(50)
                    ,'Exempted Students'                               -- ReportDescription - varchar(500)
                    ,'NACCAS/ExemptedStudentsList'                     -- RDLName - varchar(200)
                    ,'~/Bin/Reporting.dll'                             -- AssemblyFilePath - varchar(200)
                    ,'FAME.Advantage.Reporting.Logic.ExemptedStudents' -- ReportClass - varchar(200)
                    ,'BuildReport'                                     -- CreationMethod - varchar(200)
                    ,'futurefield'                                     -- WebServiceUrl - varchar(200)
                    ,400                                               -- RecordLimit - bigint
                    ,0                                                 -- AllowedExportTypes - int
                    ,0                                                 -- ReportTabLayout - int
                    ,0                                                 -- ShowPerformanceMsg - bit
                    ,0                                                 -- ShowFilterMode - bit
                );
        END;
END TRY
BEGIN CATCH
    SET @Error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE()
          ,@severity = ERROR_SEVERITY()
          ,@state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @Error > 0
    BEGIN
        ROLLBACK TRANSACTION ExemptedStudentReportTran;
        PRINT 'Failed transaction ExemptedStudentReportTran.';
    END;
ELSE
    BEGIN
        COMMIT TRANSACTION ExemptedStudentReportTran;
        PRINT 'successful transaction ExemptedStudentReportTran.';
    END;
GO
--=================================================================================================
-- END AD-10846 NACCAS Report-Exempted Students List
--=================================================================================================
--=================================================================================================
-- START AD-11718 : Changing the name of 'ShowStateBoardReports' configuration key
--=================================================================================================
DECLARE @Error AS INT;
SET @Error = 0;
BEGIN TRANSACTION AppSetChange;
BEGIN TRY
    IF EXISTS (
              SELECT 1
              FROM   dbo.syConfigAppSettings
              WHERE  KeyName = 'ShowStateBoardReports'
              )
        BEGIN
            UPDATE dbo.syConfigAppSettings
            SET    KeyName = 'ShowStateBoardAccreditationAgencyReports'
            WHERE  KeyName = 'ShowStateBoardReports';
        END;
    IF ( @@ERROR > 1 )
        BEGIN
            SET @Error = 1;
        END;
    IF EXISTS (
              SELECT 1
              FROM   dbo.syResources
              WHERE  Resource = 'StateBoardSettings'
              )
        BEGIN
            UPDATE syResources
            SET    Resource = 'State Board/Accrediting Agency Settings'
            WHERE  Resource = 'StateBoardSettings';
        END;
    IF ( @@ERROR > 1 )
        BEGIN
            SET @Error = 1;
        END;
END TRY
BEGIN CATCH
    SET @Error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE()
          ,@severity = ERROR_SEVERITY()
          ,@state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @Error > 0
    BEGIN
        ROLLBACK TRANSACTION AppSetChange;
        PRINT 'Failed transaction AppSetChange.';
    END;
ELSE
    BEGIN
        COMMIT TRANSACTION AppSetChange;
        PRINT 'successful transaction AppSetChange.';
    END;
GO
--=================================================================================================
-- END AD-11718 : Changing the name of 'ShowStateBoardReports' configuration key
--=================================================================================================
--=================================================================================================
-- START AD-12633 Enable export to excel for Attendance Summary report.
--=================================================================================================
DECLARE @ErrorAllowExportTypeExcelForAttSummReport AS INT;
SET @ErrorAllowExportTypeExcelForAttSummReport = 0;
BEGIN TRANSACTION AllowExportExcelForAttSummRep;
BEGIN TRY
    UPDATE syReports
    SET    AllowedExportTypes = 4
    WHERE  ResourceId = 877;
END TRY
BEGIN CATCH
    SET @ErrorAllowExportTypeExcelForAttSummReport = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE()
          ,@severity = ERROR_SEVERITY()
          ,@state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @ErrorAllowExportTypeExcelForAttSummReport > 0
    BEGIN
        ROLLBACK TRANSACTION AllowExportExcelForAttSummRep;
        PRINT 'Failed transcation AllowExportTypeExcelForAttSummReport.';

    END;
ELSE
    BEGIN
        COMMIT TRANSACTION AllowExportExcelForAttSummRep;
        PRINT 'successful transaction AllowExportTypeExcelForAttSummReport.';
    END;
GO
--=================================================================================================
-- END AD-12633 Enable export to excel for Attendance Summary report.
--=================================================================================================
--=================================================================================================
-- START AD-13246 : Beta 4.0: Back Fill Script for Date Completed
--=================================================================================================
DECLARE @ErrorUpdate AS INT;
SET @ErrorUpdate = 0;
BEGIN TRANSACTION DateCompleteUpdate;
BEGIN TRY
    IF EXISTS (
              SELECT 1
              FROM   dbo.arResults
              WHERE  GrdSysDetailId IS NOT NULL
                     AND (
                         DateCompleted IS NULL
                         OR CAST(DateCompleted AS DATE) = '1/1/1900'
                         )
              )
        BEGIN

            UPDATE     r
            SET        r.DateCompleted = CASE WHEN r.ModDate IS NULL THEN e.ExpGradDate
                                              ELSE CASE WHEN r.ModDate
                                                             BETWEEN e.StartDate AND e.ExpGradDate THEN r.ModDate
                                                        ELSE e.ExpGradDate
                                                   END
                                         END
            FROM       dbo.arResults r
            INNER JOIN dbo.arStuEnrollments e ON e.StuEnrollId = r.StuEnrollId
            WHERE      GrdSysDetailId IS NOT NULL
                       AND (
                           DateCompleted IS NULL
                           OR CAST(DateCompleted AS DATE) = '1/1/1900'
                           );
        END;

END TRY
BEGIN CATCH
    SET @ErrorUpdate = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE()
          ,@severity = ERROR_SEVERITY()
          ,@state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @ErrorUpdate > 0
    BEGIN
        ROLLBACK TRANSACTION DateCompleteUpdate;
        PRINT 'Failed transcation DateCompleteUpdate.';

    END;
ELSE
    BEGIN
        COMMIT TRANSACTION DateCompleteUpdate;
        PRINT 'successful transaction DateCompleteUpdate.';
    END;
GO
--=================================================================================================
-- END AD-13246 : Beta 4.0: Back Fill Script for Date Completed
--=================================================================================================
--=================================================================================================
-- START AD-13507 Backfill Script for Historical Component Dates
--=================================================================================================
DECLARE @Error AS INT;
SET @Error = 0;
BEGIN TRANSACTION GrdBkResultsDateComp;
BEGIN TRY
    UPDATE dbo.arGrdBkResults
    SET    DateCompleted = CASE WHEN arGrdBkResults.ModDate < EndDate
                                     AND arGrdBkResults.ModDate < ExpGradDate THEN dbo.arGrdBkResults.ModDate
                                WHEN arGrdBkResults.ModDate > EndDate
                                     AND EndDate < ExpGradDate THEN EndDate
                                WHEN arGrdBkResults.ModDate > EndDate
                                     AND EndDate > ExpGradDate THEN ExpGradDate
                                ELSE ExpGradDate
                           END
    FROM   dbo.arGrdBkResults
    JOIN   dbo.arClassSections ON arClassSections.ClsSectionId = arGrdBkResults.ClsSectionId
    JOIN   dbo.arStuEnrollments ON arStuEnrollments.StuEnrollId = arGrdBkResults.StuEnrollId
    WHERE  DateCompleted IS NULL
           AND Score IS NOT NULL;
END TRY
BEGIN CATCH
    SET @Error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE()
          ,@severity = ERROR_SEVERITY()
          ,@state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @Error > 0
    BEGIN
        ROLLBACK TRANSACTION GrdBkResultsDateComp;
        PRINT 'Failed transaction GrdBkResultsDateComp.';
    END;
ELSE
    BEGIN
        COMMIT TRANSACTION GrdBkResultsDateComp;
        PRINT 'successful transaction GrdBkResultsDateComp.';
    END;
GO
--=================================================================================================
-- END AD-13507 Backfill Script for Historical Component Dates
--=================================================================================================
--=================================================================================================
-- START AD-13785 Exit Interview Date
--=================================================================================================
DECLARE @ErrorExitInterViewDateFill AS INT;
SET @ErrorExitInterViewDateFill = 0;
BEGIN TRANSACTION ExitInterViewDateFill;
BEGIN TRY
    --only update exit interview date when it is null.. set it with its current moddate
    UPDATE dbo.plExitInterview
    SET    ExitInterviewDate = ModDate
    WHERE  ExitInterviewDate IS NULL;
END TRY
BEGIN CATCH
    SET @ErrorExitInterViewDateFill = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE()
          ,@severity = ERROR_SEVERITY()
          ,@state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @ErrorExitInterViewDateFill > 0
    BEGIN
        ROLLBACK TRANSACTION ExitInterViewDateFill;
        PRINT 'Failed transcation ExitInterViewDateFill.';

    END;
ELSE
    BEGIN
        COMMIT TRANSACTION ExitInterViewDateFill;
        PRINT 'successful transaction ExitInterViewDateFill.';
    END;
GO
--=================================================================================================
-- END AD-13785 Exit Interview Date
--=================================================================================================
--=================================================================================================
-- START AD-14468 category items are listed twice in the drop down for Cincinati - both Lead info and QuickLead - for both campus
--=================================================================================================
DECLARE @Error AS INT;
SET @Error = 0;
BEGIN TRANSACTION QuickLeadMapTr;
BEGIN TRY
    UPDATE dbo.adQuickLeadMap
    SET    parentCtrlId = 'LeadAssignedToId'
    WHERE  parentCtrlId IS NULL
           AND fldName = 'SourceCategoryID';
    UPDATE dbo.adQuickLeadMap
    SET    parentCtrlId = 'LeadAssignedToId'
    WHERE  parentCtrlId IS NULL
           AND fldName = 'Gender';
    UPDATE dbo.adQuickLeadMap
    SET    parentCtrlId = 'LeadAssignedToId'
    WHERE  parentCtrlId IS NULL
           AND fldName = 'FamilyIncome';
    UPDATE dbo.adQuickLeadMap
    SET    parentCtrlId = 'LeadAssignedToId'
    WHERE  parentCtrlId IS NULL
           AND fldName = 'SourceAdvertisement';
    UPDATE dbo.adQuickLeadMap
    SET    parentCtrlId = 'LeadAssignedToId'
    WHERE  parentCtrlId IS NULL
           AND fldName = 'SourceTypeID';
    UPDATE dbo.syFields
    SET    FldTypeId = 72
    WHERE  FldName = 'SourceAdvertisement'
           AND FldTypeId = 200;
END TRY
BEGIN CATCH
    SET @Error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE()
          ,@severity = ERROR_SEVERITY()
          ,@state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @Error > 0
    BEGIN
        ROLLBACK TRANSACTION QuickLeadMapTr;
        PRINT 'Failed transcation QuickLeadMapTr.';

    END;
ELSE
    BEGIN
        COMMIT TRANSACTION QuickLeadMapTr;
        PRINT 'successful transaction QuickLeadMapTr.';
    END;
GO
--=================================================================================================
-- END AD-14468 category items are listed twice in the drop down for Cincinati - both Lead info and QuickLead - for both campus
--=================================================================================================
--=================================================================================================
-- START AD-15683 : Tricoci - Error Exporting adhoc Pending Grad report to Excel
-- changing the query of totalAidDue to match the projected amount in ledger
--=================================================================================================
DECLARE @Error AS INT;
SET @Error = 0;
BEGIN TRANSACTION TotalAidDueAdhoc;
BEGIN TRY
    DECLARE @tdFldId INT;
    SET @tdFldId = (
                   SELECT FldId
                   FROM   syFields
                   WHERE  FldName = 'TotalAidDue'
                   );
    UPDATE syFieldCalculation
    SET    CalculationSql = ' FORMAT(( SELECT COALESCE(SUM(Amount), 0)  FROM  faStudentAwards INNER JOIN faStudentAwardSchedule SAS ON SAS.StudentAwardId = faStudentAwards.StudentAwardId  WHERE   faStudentAwards.StuEnrollId = arStuEnrollments.StuEnrollId AND NOT EXISTS ( SELECT 1 FROM   saPmtDisbRel WHERE  AwardScheduleId = SAS.AwardScheduleId AND SAS.Amount <= ( SELECT   SUM(Amount) FROM     saPmtDisbRel WHERE    AwardScheduleId = SAS.AwardScheduleId GROUP BY AwardScheduleId )) ) - ( SELECT     COALESCE(SUM(SP.Amount), 0) FROM  faStudentAwardSchedule SAS INNER JOIN saPmtDisbRel SP ON SP.AwardScheduleId = SAS.AwardScheduleId INNER JOIN faStudentAwards  ON faStudentAwards.StudentAwardId = SAS.StudentAwardId INNER JOIN saTransactions TR ON TR.TransactionId = SP.TransactionId WHERE faStudentAwards.StuEnrollId = arStuEnrollments.StuEnrollId AND TR.Voided = 0 AND SAS.Amount > ( SELECT   SUM(Amount) FROM     saPmtDisbRel  WHERE    AwardScheduleId = SAS.AwardScheduleId  GROUP BY AwardScheduleId ) ),''C'',''en-us'') AS TotalAidDue '
    WHERE  FldId = @tdFldId;
    DECLARE @tdFldIdR INT;
    SET @tdFldIdR = (
                    SELECT FldId
                    FROM   syFields
                    WHERE  FldName = 'TotalAidReceived'
                    );
    UPDATE syFieldCalculation
    SET    CalculationSql = ' FORMAT(((   SELECT ( ISNULL(SUM(ISNULL(A.recAmt, 0)), 0) * -1 ) AS ReceivedAmt FROM   (   SELECT   ISNULL(SUM(tr.TransAmount), 0) AS recAmt FROM     dbo.faStudentAwards  INNER JOIN dbo.faStudentAwardSchedule ON faStudentAwardSchedule.StudentAwardId = faStudentAwards.StudentAwardId INNER JOIN dbo.saPmtDisbRel ON saPmtDisbRel.AwardScheduleId = faStudentAwardSchedule.AwardScheduleId  INNER JOIN dbo.saTransactions tr ON tr.TransactionId = dbo.saPmtDisbRel.TransactionId WHERE    dbo.faStudentAwards.StuEnrollId = arStuEnrollments.StuEnrollId GROUP BY faStudentAwards.StudentAwardId ) AS A )    - (   SELECT ISNULL(SUM(refAmt), 0)  FROM   (   SELECT   RefundAmount AS refAmt FROM     dbo.faStudentAwards  INNER JOIN dbo.faStudentAwardSchedule ON faStudentAwardSchedule.StudentAwardId = faStudentAwards.StudentAwardId   INNER JOIN dbo.saPmtDisbRel ON saPmtDisbRel.AwardScheduleId = faStudentAwardSchedule.AwardScheduleId INNER JOIN saRefunds ON saRefunds.AwardScheduleId = faStudentAwardSchedule.AwardScheduleId   WHERE    faStudentAwards.StuEnrollId = arStuEnrollments.StuEnrollId GROUP BY faStudentAwards.StudentAwardId ,  saRefunds.RefundAmount ) AS RefundMoney )) ,''C'' ,''en-us'') AS TotalAidReceived '
    WHERE  FldId = @tdFldIdR;
END TRY
BEGIN CATCH
    SET @Error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE()
          ,@severity = ERROR_SEVERITY()
          ,@state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @Error > 0
    BEGIN
        ROLLBACK TRANSACTION TotalAidDueAdhoc;
        PRINT 'Failed transcation TotalAidDueAdhoc.';

    END;
ELSE
    BEGIN
        COMMIT TRANSACTION TotalAidDueAdhoc;
        PRINT 'successful transaction TotalAidDueAdhoc.';
    END;
GO
--=================================================================================================
-- END AD-15683 : Tricoci - Error Exporting adhoc Pending Grad report to Excel
-- changing the query of totalAidDue to match the projected amount in ledger
--=================================================================================================
--=================================================================================================
-- START AD-15819 Post services - Support schools without scores
--=================================================================================================
DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION Trans15819;
BEGIN TRY
    IF NOT EXISTS (
                  SELECT 1
                  FROM   syConfigAppSettings
                  WHERE  KeyName = 'ClinicServicesScoresEnabled'
                  )
        BEGIN
            DECLARE @settingId INT;
            DECLARE @modDate DATETIME;
            SET @modDate = GETDATE();

            SET @settingId = (
                             SELECT MAX(SettingId) + 1
                             FROM   dbo.syConfigAppSetValues
                             );

            SET IDENTITY_INSERT dbo.syConfigAppSettings ON;

            INSERT INTO syConfigAppSettings (
                                            KeyName
                                           ,Description
                                           ,ModUser
                                           ,ModDate
                                           ,CampusSpecific
                                           ,ExtraConfirmation
                                           ,SettingId
                                            )
            VALUES ( 'ClinicServicesScoresEnabled'                                          -- KeyName - varchar(200)
                    ,'Yes/No campus specific value to enable clinic service score posting.' -- Description - varchar(1000)
                    ,'Support'                                                              -- ModUser - varchar(50)
                    ,@modDate                                                               -- ModDate - datetime
                    ,1                                                                      -- CampusSpecific - bit
                    ,0, @settingId                                                          -- ExtraConfirmation - bit
                );

            SET IDENTITY_INSERT dbo.syConfigAppSettings OFF;

            SET @settingId = (
                             SELECT SettingId
                             FROM   syConfigAppSettings
                             WHERE  KeyName = 'ClinicServicesScoresEnabled'
                             );

            INSERT INTO syConfigAppSet_Lookup (
                                              LookUpId
                                             ,SettingId
                                             ,ValueOptions
                                             ,ModUser
                                             ,ModDate
                                              )
            VALUES ( NEWID()    -- LookUpId - uniqueidentifier
                    ,@settingId -- SettingId - int
                    ,'No'       -- ValueOptions - varchar(50)
                    ,'Support'  -- ModUser - varchar(50)
                    ,@modDate   -- ModDate - datetime
                );
            INSERT INTO syConfigAppSet_Lookup (
                                              LookUpId
                                             ,SettingId
                                             ,ValueOptions
                                             ,ModUser
                                             ,ModDate
                                              )
            VALUES ( NEWID()    -- LookUpId - uniqueidentifier
                    ,@settingId -- SettingId - int
                    ,'Yes'      -- ValueOptions - varchar(50)
                    ,'Support'  -- ModUser - varchar(50)
                    ,@modDate   -- ModDate - datetime
                );
            INSERT INTO syConfigAppSetValues (
                                             ValueId
                                            ,SettingId
                                            ,CampusId
                                            ,Value
                                            ,ModUser
                                            ,ModDate
                                            ,Active
                                             )
            VALUES ( NEWID()    -- ValueId - uniqueidentifier
                    ,@settingId -- SettingId - int
                    ,NULL       -- CampusId - uniqueidentifier
                    ,'Yes'      -- Value - varchar(1000)
                    ,'Support'  -- ModUser - varchar(50)
                    ,@modDate   -- ModDate - datetime
                    ,1          -- Active - bit
                );
        END;
END TRY
BEGIN CATCH
    SET @error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE()
          ,@severity = ERROR_SEVERITY()
          ,@state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
    BEGIN
        ROLLBACK TRANSACTION Trans15819;
        PRINT 'Failed transcation Trans15819.';

    END;
ELSE
    BEGIN
        COMMIT TRANSACTION Trans15819;
        PRINT 'successful transaction Trans15819.';
    END;
GO

--=================================================================================================
-- END AD-15819 Post services - Support schools without scores
--=================================================================================================
--========================================================================================================================
-- AD-12878 : Allow AdHoc report to specify if a column is required (inner join) or not (left join) on a query.
--Insert a record to SyConfigSettings for adding 'ShowBlankFieldsOptionInAdhoc' key to Manage Configuration Settings page
--========================================================================================================================
UPDATE dbo.syUserResources
SET    useLeftJoin = 0;
DECLARE @KeyName VARCHAR(50) = 'ShowBlankFieldsOptionInAdhoc';
DECLARE @KeyDescription VARCHAR(300) = 'Values can be set to Yes or No. If value is set to Yes, it will display the checkBox in Set up adhoc Report page.';

IF NOT EXISTS (
              SELECT *
              FROM   syConfigAppSettings
              WHERE  KeyName = @KeyName
              )
    BEGIN TRY
        BEGIN
            BEGIN TRANSACTION ShowBlankFields;

            DECLARE @MaxId INT;

            INSERT INTO dbo.syConfigAppSettings (
                                                KeyName
                                               ,Description
                                               ,ModUser
                                               ,ModDate
                                               ,CampusSpecific
                                               ,ExtraConfirmation
                                                )
            VALUES ( @KeyName        -- KeyName - varchar(200)
                    ,@KeyDescription -- Description - varchar(1000)
                    ,'Support'       -- ModUser - varchar(50)
                    ,GETDATE()       -- ModDate - datetime
                    ,1               -- CampusSpecific - bit
                    ,0               -- ExtraConfirmation - bit
                );

            SET @MaxId = (
                         SELECT   TOP ( 1 ) SettingId
                         FROM     syConfigAppSettings
                         WHERE    KeyName = @KeyName
                         ORDER BY KeyName
                         );



            INSERT INTO dbo.syConfigAppSetValues (
                                                 ValueId
                                                ,SettingId
                                                ,CampusId
                                                ,Value
                                                ,ModUser
                                                ,ModDate
                                                ,Active
                                                 )
            VALUES ( NEWID()   -- ValueId - uniqueidentifier
                    ,@MaxId    -- SettingId - int
                    ,NULL      -- CampusId - uniqueidentifier
                    ,'No'      -- Value - varchar(1000)
                    ,'Support' -- ModUser - varchar(50)
                    ,GETDATE() -- ModDate - datetime
                    ,1         -- Active - bit
                );

            INSERT INTO dbo.syConfigAppSet_Lookup (
                                                  LookUpId
                                                 ,SettingId
                                                 ,ValueOptions
                                                 ,ModUser
                                                 ,ModDate
                                                  )
            VALUES ( NEWID()   -- LookUpId - uniqueidentifier
                    ,@MaxId    -- SettingId - int
                    ,'No'      -- ValueOptions - varchar(50)
                    ,'Support' -- ModUser - varchar(50)
                    ,GETDATE() -- ModDate - datetime
                );

            INSERT INTO dbo.syConfigAppSet_Lookup (
                                                  LookUpId
                                                 ,SettingId
                                                 ,ValueOptions
                                                 ,ModUser
                                                 ,ModDate
                                                  )
            VALUES ( NEWID()   -- LookUpId - uniqueidentifier
                    ,@MaxId    -- SettingId - int
                    ,'Yes'     -- ValueOptions - varchar(50)
                    ,'Support' -- ModUser - varchar(50)
                    ,GETDATE() -- ModDate - datetime
                );

        END;
    END TRY
    BEGIN CATCH
        SELECT ERROR_NUMBER() AS ErrorNumber;
        SELECT ERROR_SEVERITY() AS ErrorSeverity;
        SELECT ERROR_STATE() AS ErrorState;
        SELECT ERROR_PROCEDURE() AS ErrorProcedure;
        SELECT ERROR_LINE() AS ErrorLine;
        SELECT ERROR_MESSAGE() AS ErrorMessage;

        IF @@TRANCOUNT > 0
            BEGIN
                ROLLBACK TRANSACTION ShowBlankFields;
                PRINT 'Exception Ocurred!';
            END;
    END CATCH;

IF @@TRANCOUNT > 0
    BEGIN
        COMMIT TRANSACTION ShowBlankFields;
    END;
GO

--===============================================================================================================================
-- END --  AD-12878 : Allow AdHoc report to specify if a column is required (inner join) or not (left join) on a query.
--Insert a record to SyConfigSettings for adding 'ShowBlankFieldsOptionInAdhoc' key to Manage Configuration Settings page
--===============================================================================================================================
--=================================================================================================
-- START AbsentHours field to Adhoc AD-15849
--=================================================================================================

DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION AbsentHours;
BEGIN TRY
    DECLARE @fldId INT;
    SET @fldId =
    (
        SELECT MAX(FldId) FROM syFields
    ) + 1;
    DECLARE @fldTypeId INT;
    SET @fldTypeId =
    (
        SELECT FldTypeId FROM syFieldTypes WHERE FldType = 'Decimal'
    );
    IF NOT EXISTS (SELECT 1 FROM syFields WHERE FldName = 'AbsentHours')
    BEGIN
        INSERT INTO syFields
        (
            FldId,
            FldName,
            FldTypeId,
            FldLen,
            DDLId,
            DerivedFld,
            SchlReq,
            LogChanges,
            Mask
        )
        VALUES
        (   @fldId,        -- FldId - int
            'AbsentHours', -- FldName - varchar(200)
            @fldTypeId,    -- FldTypeId - int
            10,            -- FldLen - int
            NULL,          -- DDLId - int
            NULL,          -- DerivedFld - bit
            NULL,          -- SchlReq - bit
            NULL,          -- LogChanges - bit
            NULL           -- Mask - varchar(50)
            );
        DECLARE @categoryId INT;
        SET @categoryId =
        (
            SELECT CategoryId
            FROM syFldCategories
            WHERE Descrip = 'Enrollment'
                  AND EntityId = 394
        );
        DECLARE @TblFldsId INT;
        SET @TblFldsId =
        (
            SELECT MAX(TblFldsId) FROM syTblFlds
        ) + 1;
        DECLARE @tblId INT;
        SET @tblId =
        (
            SELECT TblId FROM syTables WHERE TblName = 'arStuEnrollments'
        );
        DECLARE @FldCapId INT;
        SET @FldCapId =
        (
            SELECT MAX(FldCapId) FROM syFldCaptions
        ) + 1;
        INSERT INTO syFldCaptions
        (
            FldCapId,
            FldId,
            LangId,
            Caption,
            FldDescrip
        )
        VALUES
        (   @FldCapId,                                  -- FldCapId - int
            @fldId,                                     -- FldId - int
            1,                                          -- LangId - tinyint
            'Absent Hours',                            -- Caption - varchar(100)
            'Total Absent Hours' -- FldDescrip - varchar(150)
            );
        INSERT INTO syTblFlds
        (
            TblFldsId,
            TblId,
            FldId,
            CategoryId,
            FKColDescrip
        )
        VALUES
        (   @TblFldsId,  -- TblFldsId - int
            @tblId,      -- TblId - int
            @fldId,      -- FldId - int
            @categoryId, -- CategoryId - int
            NULL         -- FKColDescrip - varchar(50)
            );
        INSERT INTO syFieldCalculation
        (
            FldId,
            CalculationSql
        )
        VALUES
        (   @fldId,                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           -- FldId - int
            '     (SELECT ISNULL((SUM(SchedHours) - sum(ActualHours)),0) AS AbsentHours FROM dbo.arStudentClockAttendance WHERE SchedHours NOT IN (9999.00) AND ActualHours NOT IN (9999.00) AND (ActualHours < SchedHours OR ActualHours = SchedHours ) AND  StuEnrollId = arStuEnrollments.StuEnrollId) AS AbsentHours  ' -- CalculationSql - varchar(8000)
            );
    END;
    ELSE
    BEGIN
        DECLARE @tdFldId INT;
        SET @tdFldId =
        (
            SELECT FldId FROM syFields WHERE FldName = 'AbsentHours'
        );
        UPDATE syFieldCalculation
        SET CalculationSql = '     (SELECT ISNULL((SUM(SchedHours) - sum(ActualHours)),0) AS AbsentHours FROM dbo.arStudentClockAttendance WHERE SchedHours NOT IN (9999.00) AND ActualHours NOT IN (9999.00) AND (ActualHours < SchedHours OR ActualHours = SchedHours ) AND  StuEnrollId = arStuEnrollments.StuEnrollId) AS AbsentHours   '
        WHERE FldId = @tdFldId;
    END;
END TRY
BEGIN CATCH
    SET @error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE(),
           @severity = ERROR_SEVERITY(),
           @state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
BEGIN
    ROLLBACK TRANSACTION AbsentHours;
    PRINT 'Failed to Add AbsentHours to Adhoc Report';

END;
ELSE
BEGIN
    COMMIT TRANSACTION AbsentHours;
    PRINT 'Added AbsentHours to Adhoc Report';
END;
GO
--=================================================================================================
-- END  Add AbsentHours field to Adhoc AD-15849
--=================================================================================================


--=================================================================================================
-- START MakeupHours field to Adhoc AD-15849
--=================================================================================================

DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION MakeupHours;
BEGIN TRY
    DECLARE @fldId INT;
    SET @fldId =
    (
        SELECT MAX(FldId) FROM syFields
    ) + 1;
    DECLARE @fldTypeId INT;
    SET @fldTypeId =
    (
        SELECT FldTypeId FROM syFieldTypes WHERE FldType = 'Decimal'
    );
    IF NOT EXISTS (SELECT 1 FROM syFields WHERE FldName = 'MakeupHours')
    BEGIN
        INSERT INTO syFields
        (
            FldId,
            FldName,
            FldTypeId,
            FldLen,
            DDLId,
            DerivedFld,
            SchlReq,
            LogChanges,
            Mask
        )
        VALUES
        (   @fldId,        -- FldId - int
            'MakeupHours', -- FldName - varchar(200)
            @fldTypeId,    -- FldTypeId - int
            10,            -- FldLen - int
            NULL,          -- DDLId - int
            NULL,          -- DerivedFld - bit
            NULL,          -- SchlReq - bit
            NULL,          -- LogChanges - bit
            NULL           -- Mask - varchar(50)
            );
        DECLARE @categoryId INT;
        SET @categoryId =
        (
            SELECT CategoryId
            FROM syFldCategories
            WHERE Descrip = 'Enrollment'
                  AND EntityId = 394
        );
        DECLARE @TblFldsId INT;
        SET @TblFldsId =
        (
            SELECT MAX(TblFldsId) FROM syTblFlds
        ) + 1;
        DECLARE @tblId INT;
        SET @tblId =
        (
            SELECT TblId FROM syTables WHERE TblName = 'arStuEnrollments'
        );
        DECLARE @FldCapId INT;
        SET @FldCapId =
        (
            SELECT MAX(FldCapId) FROM syFldCaptions
        ) + 1;
        INSERT INTO syFldCaptions
        (
            FldCapId,
            FldId,
            LangId,
            Caption,
            FldDescrip
        )
        VALUES
        (   @FldCapId,                                  -- FldCapId - int
            @fldId,                                     -- FldId - int
            1,                                          -- LangId - tinyint
            'Make-up Hours',                            -- Caption - varchar(100)
            'Total Make-up Hours' -- FldDescrip - varchar(150)
            );
        INSERT INTO syTblFlds
        (
            TblFldsId,
            TblId,
            FldId,
            CategoryId,
            FKColDescrip
        )
        VALUES
        (   @TblFldsId,  -- TblFldsId - int
            @tblId,      -- TblId - int
            @fldId,      -- FldId - int
            @categoryId, -- CategoryId - int
            NULL         -- FKColDescrip - varchar(50)
            );
        INSERT INTO syFieldCalculation
        (
            FldId,
            CalculationSql
        )
        VALUES
        (   @fldId,                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           -- FldId - int
            '     (SELECT ISNULL((SUM(ActualHours) - sum(SchedHours)),0) AS Makeup FROM dbo.arStudentClockAttendance WHERE SchedHours NOT IN (9999.00) AND ActualHours NOT IN (9999.00) AND ActualHours > SchedHours AND StuEnrollId =  arStuEnrollments.StuEnrollId) AS MakeupHours  ' -- CalculationSql - varchar(8000)
            );
    END;
    ELSE
    BEGIN
        DECLARE @tdFldId INT;
        SET @tdFldId =
        (
            SELECT FldId FROM syFields WHERE FldName = 'MakeupHours'
        );
        UPDATE syFieldCalculation
        SET CalculationSql = '     (SELECT ISNULL((SUM(ActualHours) - sum(SchedHours)),0) AS Makeup FROM dbo.arStudentClockAttendance WHERE SchedHours NOT IN (9999.00) AND ActualHours NOT IN (9999.00) AND ActualHours > SchedHours AND StuEnrollId =  arStuEnrollments.StuEnrollId) AS MakeupHours   '
        WHERE FldId = @tdFldId;
    END;
END TRY
BEGIN CATCH
    SET @error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE(),
           @severity = ERROR_SEVERITY(),
           @state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
BEGIN
    ROLLBACK TRANSACTION MakeupHours;
    PRINT 'Failed to Add MakeupHours to Adhoc Report';

END;
ELSE
BEGIN
    COMMIT TRANSACTION MakeupHours;
    PRINT 'Added MakeupHours to Adhoc Report';
END;
GO
--=================================================================================================
-- END  Add MakeupHours field to Adhoc AD-15849
--=================================================================================================
--=================================================================================================
-- START  AD-16533 : Save the changes when you are navigating off the leadInfo page
--=================================================================================================
DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION leadUrl;
BEGIN TRY
UPDATE dbo.syMenuItems SET Url = '/AD/AleadInfoPage.aspx' WHERE url = '/AD/aLeadInfoPage.aspx'
END TRY
BEGIN CATCH
    SET @error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE(),
           @severity = ERROR_SEVERITY(),
           @state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
BEGIN
    ROLLBACK TRANSACTION leadUrl;
    PRINT 'Failed to update leadUrl for LeadInfoPage';
END;
ELSE
BEGIN
    COMMIT TRANSACTION leadUrl;
    PRINT 'updated leadUrl for LeadInfoPage';
END;
GO
--=================================================================================================
-- END  AD-16533 : Save the changes when you are navigating off the leadInfo page
--=================================================================================================
-- ===============================================================================================
-- END Consolidated Script Version 4.1
-- ===============================================================================================