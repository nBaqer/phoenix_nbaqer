-- =========================================================================================================
-- Synchronize_3.9SP2_with_3.9SP3.sql
-- =========================================================================================================
/*                            Ver 003      2018-01-20
Run this script on:

 
to synchronize it with:

        3.9SP2  with  3.9SP3 (TFS)

You are recommended to back up your database before running this script


*/
SET NUMERIC_ROUNDABORT OFF;
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON;
GO
SET XACT_ABORT ON;
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;
GO
BEGIN TRANSACTION;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO

--=================================================================================================
-- isProgramUndergraduate 
--=================================================================================================
PRINT N'If Exists Function isProgramUndergraduate, Dropp it ';
GO
IF EXISTS (
          SELECT 1
          FROM   sys.objects
          WHERE  object_id = OBJECT_ID (N'[isProgramUndergraduate]')
                 AND type IN ( N'FN', N'TF', N'IF' )
          )
    BEGIN
		PRINT N'    Dropping Function isProgramUndergraduate';	
        DROP FUNCTION isProgramUndergraduate;
    END;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO

PRINT N'Create Function isProgramUndergraduate ';
GO
--=================================================================================================    
-- isProgramUndergraduate    
--=================================================================================================    
CREATE FUNCTION dbo.isProgramUndergraduate
    (
        @programId CHAR(36)
       ,@allDegree CHAR(1) = 'D' -- D is default for 2 yrs, B is bachelors, A is all grad(last column), 'O' is for others (B,A,O is for 4 yrs)
    )
RETURNS BIT
AS
    BEGIN
        DECLARE @returnValue BIT;
        DECLARE @ipedsvalue INT;

        SET @returnValue = 0;
        SET @ipedsvalue = (
                          SELECT IPEDSValue
                          FROM   arPrograms pg
                                ,dbo.arProgCredential pc
                          WHERE  pg.CredentialLvlId = pc.CredentialId
                                 AND pg.ProgId = @programId
                          );
        IF (@allDegree = 'D') -- this means the default behaivour   for 2 yrs and < 2 yrs 
            BEGIN

                IF (
                   @ipedsvalue = 154
                   OR @ipedsvalue = 155
                   OR @ipedsvalue = 156
                   OR @ipedsvalue = 157
                   )
                    BEGIN
                        SET @returnValue = 1;
                    END;
                ELSE
                    BEGIN
                        SET @returnValue = 0;
                    END;

            END;
        ELSE IF (@allDegree = 'O') -- this means the default behaivour    
                 BEGIN

                     IF (
                        @ipedsvalue = 154
                        OR @ipedsvalue = 155
                        OR @ipedsvalue = 156
                        )
                         BEGIN
                             SET @returnValue = 1;
                         END;
                     ELSE
                         BEGIN
                             SET @returnValue = 0;
                         END;

                 END;
        ELSE IF (@allDegree = 'B') -- this means that we have to consider only Bachelors   
                 BEGIN
                     IF (@ipedsvalue = 157)
                         BEGIN
                             SET @returnValue = 1;
                         END;
                     ELSE
                         BEGIN
                             SET @returnValue = 0;
                         END;
                 END;
        ELSE IF (@allDegree = 'A') -- this means that we have to consider for all the grades (done for 4 yr Pell report)    
                 BEGIN
                     IF (
                        @ipedsvalue = 160
                        OR @ipedsvalue = 157
                        OR @ipedsvalue = 158 -- including the calculation for Masters    
                        OR @ipedsvalue = 159 -- including the calculation for Doctoral    
                        )
                         BEGIN
                             SET @returnValue = 1;
                         END;
                     ELSE
                         BEGIN
                             SET @returnValue = 0;
                         END;
                 END;
        RETURN @returnValue;
    END;
--=================================================================================================    
-- END  --  isProgramUndergraduate    
--================================================================================================= 
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO

--=================================================================================================
-- CompletedProgramin150PercentofProgramLength 
--=================================================================================================
PRINT N'If Exists Function CompletedProgramin150PercentofProgramLength, Dropp it ';
GO
IF EXISTS (
          SELECT 1
          FROM   sys.objects
          WHERE  object_id = OBJECT_ID (N'[CompletedProgramin150PercentofProgramLength]')
                 AND type IN ( N'FN', N'TF', N'IF' )
          )
    BEGIN
		PRINT N'    Dropping Function CompletedProgramin150PercentofProgramLength';	
        DROP FUNCTION CompletedProgramin150PercentofProgramLength;
    END;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO

PRINT N'Create Function CompletedProgramin150PercentofProgramLength ';
GO
--=================================================================================================  
-- CompletedProgramin150PercentofProgramLength  
--=================================================================================================  
CREATE FUNCTION dbo.CompletedProgramin150PercentofProgramLength
    (
        @StuEnrollId CHAR(36)
       ,@allDegree CHAR(1) = 'D'
    )
RETURNS BIT
--RETURNS DECIMAL(18,2)    
AS
    BEGIN
        DECLARE @mtf DECIMAL(18, 2);
        DECLARE @progid UNIQUEIDENTIFIER;
        DECLARE @undergradprogram BIT;
        DECLARE @credithourprogram BIT;
        DECLARE @CreditsEarned DECIMAL(18, 2);
        DECLARE @HoursCompleted DECIMAL(18, 2);
        DECLARE @IsProgramComplete BIT;
        DECLARE @CreditsAttempted DECIMAL(18, 2);
        DECLARE @maxcredits DECIMAL(18, 2);
        DECLARE @percentagecredits DECIMAL(18, 2);
        DECLARE @percentageattempted DECIMAL(18, 2);

        SET @percentagecredits = 0.00;
        SET @percentageattempted = 0.00;

        SET @progid = (
                      SELECT ProgId
                      FROM   arPrgVersions pv
                            ,dbo.arStuEnrollments se
                      WHERE  pv.PrgVerId = se.PrgVerId
                             AND se.StuEnrollId = @StuEnrollId
                      );

        SET @undergradprogram = (
                                SELECT dbo.isProgramUndergraduate (@progid, @allDegree)
                                );
        SET @credithourprogram = (
                                 SELECT dbo.isProgramCreditHour (@progid)
                                 );

        SET @IsProgramComplete = 0;

        --Undergrad and credit hour    
        IF (
           @undergradprogram = 1
           AND @credithourprogram = 1
           )
            BEGIN

                SET @mtf = (
                           SELECT ISNULL (pv.Credits, 0.00)
                           FROM   arPrgVersions pv
                                 ,dbo.arStuEnrollments se
                           WHERE  pv.PrgVerId = se.PrgVerId
                                  AND se.StuEnrollId = @StuEnrollId
                           );

                SET @maxcredits = ISNULL (@mtf, 0) * 1.50;

                SET @percentagecredits = @mtf / @maxcredits;


                SET @CreditsEarned = (
                                     SELECT     ISNULL (SUM (CS.CreditsEarned), 0.00)
                                     FROM       dbo.arStuEnrollments se
                                     INNER JOIN dbo.syCreditSummary CS ON CS.StuEnrollId = se.StuEnrollId
                                     INNER JOIN syStatusCodes sc ON sc.StatusCodeId = se.StatusCodeId
                                     WHERE      se.StuEnrollId = @StuEnrollId
                                                AND sc.SysStatusId = 14
                                     );

                SET @CreditsAttempted = (
                                        SELECT     ISNULL (SUM (CS.CreditsAttempted), 0.00)
                                        FROM       dbo.arStuEnrollments se
                                        INNER JOIN dbo.syCreditSummary CS ON CS.StuEnrollId = se.StuEnrollId
                                        INNER JOIN syStatusCodes sc ON sc.StatusCodeId = se.StatusCodeId
                                        WHERE      se.StuEnrollId = @StuEnrollId
                                                   AND sc.SysStatusId = 14
                                        );



                IF (@CreditsAttempted <> 0.00)
                    BEGIN
                        SET @percentageattempted = @CreditsEarned / @CreditsAttempted;
                    END;

                IF (
                   @percentageattempted <> 0.00
                   AND @percentageattempted >= @percentagecredits
                   )
                    BEGIN
                        SET @IsProgramComplete = 1;
                    END;


            END;

        --undergrad and clock hour    
        IF (
           @undergradprogram = 1
           AND @credithourprogram = 0
           )
            BEGIN
                SET @mtf = (
                           SELECT ISNULL (pv.Hours, 0.00) * 1.50
                           FROM   arPrgVersions pv
                                 ,dbo.arStuEnrollments se
                           WHERE  pv.PrgVerId = se.PrgVerId
                                  AND se.StuEnrollId = @StuEnrollId
                           );

                SET @HoursCompleted = (
                                      SELECT     TOP 1 ISNULL (CS.AdjustedPresentDays, 0.00)
                                      FROM       arPrgVersions pv
                                      INNER JOIN dbo.arStuEnrollments se ON se.PrgVerId = pv.PrgVerId
                                      INNER JOIN dbo.syStudentAttendanceSummary CS ON CS.StuEnrollId = se.StuEnrollId
                                      INNER JOIN syStatusCodes sc ON sc.StatusCodeId = se.StatusCodeId
                                      WHERE      se.StuEnrollId = @StuEnrollId
                                                 AND sc.SysStatusId = 14
                                      ORDER BY   StudentAttendedDate DESC
                                      );

                IF (
                   @HoursCompleted <> 0.00
                   AND @HoursCompleted <= @mtf
                   )
                    BEGIN
                        SET @IsProgramComplete = 1;
                    END;
            END;


        RETURN @IsProgramComplete;
    END;
--=================================================================================================  
-- CompletedProgramin150PercentofProgramLength  
--=================================================================================================  
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO

--=================================================================================================
-- GetMaximumTimeFrame 
--=================================================================================================
PRINT N'If Exists Function GetMaximumTimeFrame, Dropp it ';
GO
IF EXISTS (
          SELECT 1
          FROM   sys.objects
          WHERE  object_id = OBJECT_ID (N'[GetMaximumTimeFrame]')
                 AND type IN ( N'FN', N'TF', N'IF' )
          )
    BEGIN
		PRINT N'    Dropping Function GetMaximumTimeFrame';	
        DROP FUNCTION GetMaximumTimeFrame;
    END;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO

PRINT N'Create Function GetMaximumTimeFrame ';
GO
--=================================================================================================  
-- GetMaximumTimeFrame  
--=================================================================================================  
CREATE FUNCTION dbo.GetMaximumTimeFrame
    (
        @StuEnrollId CHAR(36)
    )
RETURNS VARCHAR(100)
--RETURNS DECIMAL(18,2)    
AS
    BEGIN
        DECLARE @mtf DECIMAL(18, 2);
        DECLARE @progid UNIQUEIDENTIFIER;
        DECLARE @undergradprogram BIT;
        DECLARE @credithourprogram BIT;

        SET @progid = (
                      SELECT ProgId
                      FROM   arPrgVersions pv
                            ,dbo.arStuEnrollments se
                      WHERE  pv.PrgVerId = se.PrgVerId
                             AND se.StuEnrollId = @StuEnrollId
                      );

        SET @undergradprogram = (
                                SELECT dbo.isProgramUndergraduate (@progid, 'D')
                                );
        SET @credithourprogram = (
                                 SELECT dbo.isProgramCreditHour (@progid)
                                 );

        --Undergrad and credit hour    
        IF (
           @undergradprogram = 1
           AND @credithourprogram = 1
           )
            BEGIN
                SET @mtf = ((
                            SELECT pv.Credits
                            FROM   arPrgVersions pv
                                  ,dbo.arStuEnrollments se
                            WHERE  pv.PrgVerId = se.PrgVerId
                                   AND se.StuEnrollId = @StuEnrollId
                            ) * 1.50
                           );

            END;

        --undergrad and clock hour    
        IF (
           @undergradprogram = 1
           AND @credithourprogram = 0
           )
            BEGIN
                SET @mtf = ((
                            SELECT pv.Hours
                            FROM   arPrgVersions pv
                                  ,dbo.arStuEnrollments se
                            WHERE  pv.PrgVerId = se.PrgVerId
                                   AND se.StuEnrollId = @StuEnrollId
                            ) * 1.50
                           );
            END;


        RETURN @mtf;
    END;
--=================================================================================================  
-- END  --  GetMaximumTimeFrame  
--=================================================================================================  
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO

--=================================================================================================
-- USP_IPEDS_Winter_OutcomeMeasures 
--=================================================================================================
PRINT N'If Exists SP USP_IPEDS_Winter_OutcomeMeasures, Dropp it ';
GO
IF EXISTS (
          SELECT 1
          FROM   sys.objects
          WHERE  object_id = OBJECT_ID(N'[dbo].[USP_IPEDS_Winter_OutcomeMeasures]')
                 AND type IN ( N'P', N'PC' )
          )
    BEGIN
		PRINT N'    Dropping SP USP_IPEDS_Winter_OutcomeMeasures';	
        DROP PROCEDURE USP_IPEDS_Winter_OutcomeMeasures;
    END;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO

PRINT N'Create SP USP_IPEDS_Winter_OutcomeMeasures ';
GO
-- =========================================================================================================
-- USP_IPEDS_Winter_OutcomeMeasures 
-- =========================================================================================================
CREATE PROCEDURE dbo.USP_IPEDS_Winter_OutcomeMeasures
    @CampusId VARCHAR(50)
   ,@ProgIdList NVARCHAR(MAX) = NULL
   ,@CohortYear INTEGER
   ,@OrderBy VARCHAR(100)
AS
    BEGIN

        BEGIN --  01) Declare varialbles
            DECLARE @CohortStartDate DATETIME;
            DECLARE @CohortEndDate DATETIME;
            DECLARE @More5YearsGraduationDate DATETIME;
            DECLARE @More3YearsGraduationDate DATETIME;
            DECLARE @More1YearsGraduationDate DATETIME;
            DECLARE @MaxDate DATETIME;
            DECLARE @temp TABLE
                (
                    SSN NVARCHAR(50)
                   ,MaskedSSN NVARCHAR(20) NULL
                   ,StudentFullName NVARCHAR(250)
                   ,IsCohort INT
                   ,IsExclusion INT
                   ,IsCertificate INT
                   ,IsAssociates INT
                   ,IsBachelors INT
                   ,IsStillEnrolled INT
                   ,IsLaterEnrollment INT
                   ,TableGroup INT
                   ,IsFirstTimeInSchool INT
                   ,IsFullTime INT
                   ,IsPellGrantRecipient INT
                );

        END; --  End   01) Declare Varialbes

        BEGIN --   02) Define Periods.
            --   A) Cohort Range  -- Students who enter in Institution in this range
            SET @CohortStartDate = CONVERT (CHAR(4), @CohortYear) + '-07-01 00:00:00:000';
            SET @CohortEndDate = CONVERT (CHAR(4), (@CohortYear + 1)) + '-06-30 23:59:59:998';
            --  B) Graduation Range Dates
            SET @More5YearsGraduationDate = CONVERT (CHAR(4), (@CohortYear + 9 - 5)) + '-08-31 23:59:59:998';
            SET @More3YearsGraduationDate = CONVERT (CHAR(4), (@CohortYear + 9 - 3)) + '-08-31 23:59:59:998';
            SET @More1YearsGraduationDate = CONVERT (CHAR(4), (@CohortYear + 9 - 1)) + '-08-31 23:59:59:998';
            --  C) for student still on School GetDate or dic31
            SET @MaxDate = '2199/12/31';
        END; --   02) Define Periods.

        BEGIN -- 03) Select students enrolled in the Cohort Period
            INSERT INTO @temp (
                              SSN
                             ,MaskedSSN
                             ,StudentFullName
                             ,IsCohort
                             ,IsExclusion
                             ,IsCertificate
                             ,IsAssociates
                             ,IsBachelors
                             ,IsStillEnrolled
                             ,IsLaterEnrollment
                             ,TableGroup
                             ,IsFirstTimeInSchool
                             ,IsFullTime
                             ,IsPellGrantRecipient
                              )
                        SELECT T.SSN
                              ,'XXX-XX-' + SUBSTRING (ISNULL (T.SSN, 'XXX-XX-    '), ISNULL (LEN (T.SSN), 11) - 3, 4) AS MaskedSSN
                              ,T.StudentFullName
                              ,CASE WHEN IsExclusion = 1 THEN 0
                                    ELSE 1
                               END AS IsCohort
                              ,T.IsExclusion
                              ,CASE WHEN (
                                         T.SSSC_SysStatusId = 14 -- SysStaturId = 14 --> GraduatedT.CurrentSysStatusId = 14 -- SysStaturId = 14 --> Graduated
                                         AND T.CredentialIPEDSVal IN ( 154, 155 ) -- Less than 1 yr certificates, At least 1 but less than 4 yr certificates
                                         ) -- Credencial Code 01 --> Undergraduate Certificate 
                              THEN      1
                                    ELSE 0
                               END AS IsCertificate
                              ,CASE WHEN (
                                         T.SSSC_SysStatusId = 14 -- SysStaturId = 14 --> Graduated
                                         AND T.CredentialIPEDSVal = 156 --  Associate's degrees
                                         ) -- Credencial Code 02 --> IsAssociates
                              THEN      1
                                    ELSE 0
                               END AS IsAssociates
                              ,CASE WHEN (
                                         T.SSSC_SysStatusId = 14 -- SysStaturId = 14 --> Graduated
                                         AND T.CredentialIPEDSVal = 157 --  Bachelor's degrees
                                         ) -- Credencial Code 03 --> Bachelor's Degree
                              THEN      1
                                    ELSE 0
                               END AS IsBachelors
                              ,CASE WHEN (
                                         T.SSSC_SysStatusId NOT IN ( 12, 14 ) -- 7 9 10 11 20 22 24 24
                                         AND T.SSSC_InSchool = 1
                                         ) THEN 1
                                    WHEN (
                                         T.SSSC_SysStatusId IN ( 12, 14 ) -- SysStaturId = 12 -->Dropped, SysStaturId = 14 --> Graduated
                                         AND T.SSSC_InSchool = 0
                                         AND T.SSSC_DateOfChange > @More1YearsGraduationDate
                                         ) THEN 1
                                    ELSE 0
                               END AS IsStillEnrolled
                              ,0 AS IsLaterEnrollment
                              ,CASE WHEN T.SSSC_SysStatusId IN ( 12, 14 ) -- SysStaturId = 12 -->Dropped, SysStaturId = 14 --> Graduated
                              THEN      CASE WHEN (T.SSSC_DateOfChange <= @More5YearsGraduationDate) THEN 1
                                             WHEN (
                                                  T.SSSC_DateOfChange > @More5YearsGraduationDate
                                                  AND T.SSSC_DateOfChange <= @More3YearsGraduationDate
                                                  ) THEN 2
                                             WHEN (
                                                  T.SSSC_DateOfChange > @More3YearsGraduationDate
                                                  AND T.SSSC_DateOfChange <= @More1YearsGraduationDate
                                                  ) THEN 3
                                             ELSE 3
                                        END
                                    ELSE 3
                               END AS TableGroup
                              ,T.IsFirstTimeInSchool
                              ,T.IsFullTime
                              ,T.IsPellGrantRecipient
                        FROM   (
                               SELECT     dbo.UDF_FormatSSN (AL.SSN) AS SSN
                                         ,AL.LastName + ', ' + AL.FirstName + ' ' + ISNULL (LEFT(AL.MiddleName, 1), '') AS StudentFullName
                                         ,CASE WHEN ASE.IsFirstTimeInSchool = 1 THEN 1
                                               ELSE 0
                                          END AS IsFirstTimeInSchool
                                         ,SSS1.SysStatusId AS SSSC_SysStatusId
                                         ,SSS1.InSchool AS SSSC_InSchool
                                         ,SSSC.DateOfChange AS SSSC_DateOfChange
                                         ,APC.IPEDSValue AS CredentialIPEDSVal
                                         ,CASE WHEN AAT.IPEDSValue = 61 -- 61--> 'Full Time', 62--> 'PartTime'
                                         THEN      1
                                               ELSE 0
                                          END AS IsFullTime
                                         ,MAX (   CASE WHEN EXISTS (
                                                                   SELECT     1
                                                                   FROM       dbo.saTransactions AS ST
                                                                   INNER JOIN dbo.saFundSources AS SFS ON SFS.FundSourceId = ST.FundSourceId
                                                                   INNER JOIN dbo.syAdvFundSources AS SAFS ON SAFS.AdvFundSourceId = SFS.AdvFundSourceId
                                                                   WHERE      SAFS.AdvFundSourceId = 2 -- AdvFundSourceId =2 --> Descrip = 'PELL'
                                                                              AND ST.Voided <> 1																   
                                                                              AND ST.StuEnrollId = ASE.StuEnrollId
                                                                   ) THEN 1
                                                       ELSE 0
                                                  END
                                              ) OVER (PARTITION BY AL.StudentId
                                                     --,AP.ProgId
                                                     ) AS IsPellGrantRecipient
                                         ,CASE WHEN ( --if is dropped with IPEDS reasons ( 15,16,17,18,19 )
                                                    SSS.SysStatusId = 12
                                                    AND EXISTS (
                                                               SELECT 1
                                                               FROM   dbo.arDropReasons AS ADR
                                                               WHERE  ADR.DropReasonId = ASE.DropReasonId
                                                                      AND ADR.IPEDSValue IS NOT NULL
                                                               )
                                                    ) THEN 1
                                               ELSE 0
                                          END AS IsExclusion
                                         ,ROW_NUMBER () OVER (PARTITION BY ASE.StudentId
                                                              --,ASE.StuEnrollId
                                                              --,AP.ProgId
                                                              ORDER BY CASE WHEN SSS.SysStatusId = 14 --Graduated STATUS have priority
                                                                  THEN          0
                                                                            ELSE 1
                                                                       END
                                                                      ,SSSC.DateOfChange DESC
                                                                      ,SSSC.ModDate DESC
                                                             ) AS RowNumberDateOfChange
                               FROM       dbo.adLeads AS AL
                               INNER JOIN dbo.arStuEnrollments AS ASE ON ASE.StudentId = AL.StudentId
                               INNER JOIN dbo.syStatusCodes AS SSC ON SSC.StatusCodeId = ASE.StatusCodeId
                               INNER JOIN dbo.sySysStatus AS SSS ON SSS.SysStatusId = SSC.SysStatusId
                               INNER JOIN dbo.arPrgVersions AS APV ON APV.PrgVerId = ASE.PrgVerId
                               INNER JOIN dbo.arPrograms AS AP ON AP.ProgId = APV.ProgId
                               INNER JOIN dbo.arProgCredential AS APC ON APC.CredentialId = AP.CredentialLvlId
                               INNER JOIN dbo.syStatuses AS APC_SS ON APC_SS.StatusId = APC.StatusId
                               INNER JOIN dbo.arProgTypes AS APT ON APT.ProgTypId = APV.ProgTypId
                               INNER JOIN dbo.arAttendTypes AS AAT ON AAT.AttendTypeId = ASE.attendtypeid
                               INNER JOIN dbo.syStudentStatusChanges AS SSSC ON SSSC.StuEnrollId = ASE.StuEnrollId
                               INNER JOIN dbo.syStatusCodes AS SSC1 ON SSC1.StatusCodeId = SSSC.NewStatusId
                               INNER JOIN dbo.sySysStatus AS SSS1 ON SSS1.SysStatusId = SSC1.SysStatusId
                               WHERE -- leads only Enrolled
                                          AL.StudentId <> '00000000-0000-0000-0000-000000000000'
                                          AND -- StuEnrollId in Campus selected
                                   LTRIM (RTRIM (ASE.CampusId)) = LTRIM (RTRIM (@CampusId))
                                          AND -- Enrolled in Program Selected
                                          (
                                          @ProgIdList IS NULL
                                          OR EXISTS (
                                                    SELECT Val
                                                    FROM   dbo.MultipleValuesForReportParameters (@ProgIdList, ',', 1) AS L
                                                    WHERE  L.Val = AP.ProgId
                                                    )
                                          )
                                          AND -- StuEnrollment startDate between Cohort Year Range
                                   ASE.StartDate
                                          BETWEEN @CohortStartDate AND @CohortEndDate
                                          AND -- ( 154, 155 ) -- Less than 1 yr certificates, At least 1 but less than 4 yr certificates
                                   -- 156 --  Associate's degrees,  157 --  Bachelor's degrees
                                   APC.IPEDSValue IN ( 154, 155, 156, 157 )
                                          AND -- Certificate Valid for IPEDS -- Certificate should be Active
                                          (
                                          APC.IPEDSValue IS NOT NULL
                                          AND APC_SS.Status = 'Active'
                                          )
                                          -- do not include records with one of next condition
                                          AND NOT ( -- do not include records with one of next condition
                                                  -- current status in StuEnrollment Should not be 'No Start'  (It have to be filter because No Start)
                                                  (SSS.SysStatusId = 8
                                                  --         OR SSS1.SysStatusId = 8
                                                  )
                                                  OR -- current status in StuEnrollment Should not be (19) Transfer out
                                                  (SSS.SysStatusId = 19
                                                  --         OR SSS1.SysStatusId = 19
                                                  )
                                                  OR -- if is dropped without IPEDS reasons null  not in ( 15,16,17,18,19 )
                                                  (
                                                  SSS.SysStatusId = 12
                                                  AND EXISTS (
                                                             SELECT 1
                                                             FROM   dbo.arDropReasons AS ADR
                                                             WHERE  ADR.DropReasonId = ASE.DropReasonId
                                                                    AND ADR.IPEDSValue IS NULL
                                                             )
                                                  )
                                                  OR -- Program Version is not continuing Education
                                                  (APV.IsContinuingEd = 1)
                                                  OR -- Date Determined or ExpGradDate or LDA falls before End Cohor Year Range
                                                  (
                                                  ISNULL (ASE.DateDetermined, @MaxDate) < @CohortStartDate
                                                  OR ISNULL (ASE.ExpGradDate, @MaxDate) < @CohortStartDate
                                                  OR ISNULL (ASE.LDA, @MaxDate) < @CohortStartDate
                                                  )
                                                  OR -- Do not show students with missing SSN (SSN in Ceros, Blanks Or Nulls)
                                                  (
                                                  AL.SSN IS NULL
                                                  OR RTRIM (LTRIM (AL.SSN)) = ''
                                                  OR AL.SSN NOT LIKE '%[^0]%'
                                                  OR AL.SSN NOT LIKE '%[^1]%'
                                                  )
                                                  )
                               ) AS T
                        WHERE  T.RowNumberDateOfChange = 1;
        --) AS T1


        END; -- End  03) Select students enrolled in the Cohort Period

        BEGIN -- 04) Add Row for empty Tablles  and sort
            SELECT   T1.MaskedSSN
                    ,T1.StudentFullName
                    ,T1.IsCohort
                    ,T1.IsExclusion
                    ,T1.IsCertificate
                    ,T1.IsAssociates
                    ,T1.IsBachelors
                    ,T1.IsStillEnrolled
                    ,T1.IsLaterEnrollment
                    ,T1.TableGroup
                    ,T1.IsFirstTimeInSchool
                    ,T1.IsFullTime
                    ,T1.IsPellGrantRecipient
            FROM     (
                     SELECT TE0.SSN
                           ,TE0.MaskedSSN
                           ,TE0.StudentFullName
                           ,TE0.IsCohort
                           ,TE0.IsExclusion
                           ,TE0.IsCertificate
                           ,TE0.IsAssociates
                           ,TE0.IsBachelors
                           ,TE0.IsStillEnrolled
                           ,TE0.IsLaterEnrollment
                           ,TE0.TableGroup
                           ,TE0.IsFirstTimeInSchool
                           ,TE0.IsFullTime
                           ,TE0.IsPellGrantRecipient
                     FROM   @temp AS TE0
                     UNION
                     SELECT NULL AS SSN
                           ,NULL AS MaskedSSN
                           ,'Table 1 has no data' AS StudentFullName
                           ,NULL AS IsCohort
                           ,NULL AS IsExclusion
                           ,NULL AS IsCertificate
                           ,NULL AS IsAssociates
                           ,NULL AS IsBachelors
                           ,NULL AS IsStillEnrolled
                           ,NULL AS IsLaterEnrollment
                           ,1 AS TableGroup
                           ,NULL AS IsFirstTimeInSchool
                           ,NULL AS IsFullTime
                           ,NULL AS IsPellGrantRecipient
                     FROM   @temp AS TE1
                     WHERE  TE1.TableGroup = 1
                     HAVING COUNT (TE1.TableGroup) = 0
                     UNION
                     SELECT NULL AS SSN
                           ,NULL AS MaskedSSN
                           ,'Table 2 has no data' AS StudentFullName
                           ,NULL AS IsCohort
                           ,NULL AS IsExclusion
                           ,NULL AS IsCertificate
                           ,NULL AS IsAssociates
                           ,NULL AS IsBachelors
                           ,NULL AS IsStillEnrolled
                           ,NULL AS IsLaterEnrollment
                           ,2 AS TableGroup
                           ,NULL AS IsFirstTimeInSchool
                           ,NULL AS IsFullTime
                           ,NULL AS IsPellGrantRecipient
                     FROM   @temp AS TE2
                     WHERE  TE2.TableGroup = 2
                     HAVING COUNT (TE2.TableGroup) = 0
                     UNION
                     SELECT NULL AS SSN
                           ,NULL AS MaskedSSN
                           ,'Table 3 has no data' AS StudentFullName
                           ,NULL AS IsCohort
                           ,NULL AS IsExclusion
                           ,NULL AS IsCertificate
                           ,NULL AS IsAssociates
                           ,NULL AS IsBachelors
                           ,NULL AS IsStillEnrolled
                           ,NULL AS IsLaterEnrollment
                           ,3 AS TableGroup
                           ,NULL AS IsFirstTimeInSchool
                           ,NULL AS IsFullTime
                           ,NULL AS IsPellGrantRecipient
                     FROM   @temp AS TE3
                     WHERE  TE3.TableGroup = 3
                     HAVING COUNT (TE3.TableGroup) = 0
                     ) AS T1
            ORDER BY T1.TableGroup
                    ,T1.IsFirstTimeInSchool DESC
                    ,T1.IsFullTime DESC
                    ,T1.IsPellGrantRecipient DESC
                    ,CASE WHEN @OrderBy = 'SSN' THEN T1.SSN
                          ELSE T1.StudentFullName
                     END;
        END; -- END 04) Add Row for tables

    END;
-- =========================================================================================================
-- END  --  USP_IPEDS_Winter_OutcomeMeasures 
-- =========================================================================================================
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO

--=================================================================================================
-- USP_PELLRecipients_GetList 
--=================================================================================================
PRINT N'If Exists SP USP_PELLRecipients_GetList, Dropp it ';
GO
IF EXISTS (
          SELECT 1
          FROM   sys.objects
          WHERE  object_id = OBJECT_ID(N'[dbo].[USP_PELLRecipients_GetList]')
                 AND type IN ( N'P', N'PC' )
          )
    BEGIN
		PRINT N'    Dropping SP USP_PELLRecipients_GetList';	
        DROP PROCEDURE USP_PELLRecipients_GetList;
    END;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO

PRINT N'Create SP USP_PELLRecipients_GetList ';
GO
-- =========================================================================================================  
-- USP_PELLRecipients_GetList   
-- =========================================================================================================  
CREATE PROCEDURE dbo.USP_PELLRecipients_GetList
    @CampusId VARCHAR(50)
   ,@ProgId VARCHAR(4000) = NULL
   ,@CohortYear VARCHAR(10) = NULL
   ,@CohortPossible VARCHAR(20) = NULL
   ,@OrderBy VARCHAR(100)
   ,@StartDate DATETIME
   ,@EndDate DATETIME
AS --SET @CampusId='3F5E839A-589A-4B2A-B258-35A1A8B3B819'    
    --SET @ProgId=NULL    
    --SET @CohortYear='2015'    
    --SET @CohortPossible= 'full'    
    --SET @OrderBy = 'SSN'    
    --SET @StartDate='09/01/2015'    
    --SET @EndDate='08/31/2016'    

    BEGIN
        DECLARE @AcadInstFirstTimeStartDate DATETIME;
        DECLARE @ReturnValue VARCHAR(100)
               ,@Value VARCHAR(100);
        DECLARE @SSN VARCHAR(10)
               ,@FirstName VARCHAR(100)
               ,@LastName VARCHAR(100)
               ,@StudentNumber VARCHAR(50)
               ,@TransferredOut INT
               ,@Exclusions INT;
        DECLARE @CitizenShip_IPEDSValue INT
               ,@ProgramType_IPEDSValue INT
               ,@Gender_IPEDSValue INT
               ,@FullTimePartTime_IPEDSValue INT
               ,@StudentId UNIQUEIDENTIFIER;

        DECLARE @StatusDate DATETIME;

        SET @StatusDate = '08/31/' + CONVERT (CHAR(4), YEAR (GETDATE ()) - 1);

        -- Check if School tracks grades by letter or numeric       
        --SET @Value = (SELECT TOP 1 Value FROM dbo.syConfigAppSetValues WHERE SettingId=47)      
        -- 2/07/2013 - updated the @value     
        SET @Value = (
                     SELECT dbo.GetAppSettingValue (47, @CampusId)
                     );

        IF @ProgId IS NOT NULL
            BEGIN
                SELECT @ReturnValue = COALESCE (@ReturnValue, '') + ProgDescrip + ','
                FROM   arPrograms t1
                WHERE  t1.ProgId IN (
                                    SELECT Val
                                    FROM   MultipleValuesForReportParameters (@ProgId, ',', 1)
                                    );
                SET @ReturnValue = SUBSTRING (@ReturnValue, 1, LEN (@ReturnValue) - 1);
            END;
        ELSE
            BEGIN
                SELECT @ReturnValue = COALESCE (@ReturnValue, '') + ProgDescrip + ','
                FROM   arPrograms t1
                WHERE  t1.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
                       AND t1.CampGrpId IN (
                                           SELECT CampGrpId
                                           FROM   syCmpGrpCmps
                                           WHERE  CampusId = @CampusId
                                           );
                SET @ReturnValue = SUBSTRING (@ReturnValue, 1, LEN (@ReturnValue) - 1);
            END;

        -- Create a temp table to hold the final output of this stored proc      
        CREATE TABLE #GraduationRate
            (
                RowNumber UNIQUEIDENTIFIER
               ,SSN VARCHAR(50)
               ,StudentNumber VARCHAR(50)
               ,StudentName VARCHAR(100)
               ,Gender VARCHAR(50)
               ,Race VARCHAR(50)
               ,RevisedCohort VARCHAR(10)
               ,Exclusions VARCHAR(10)
               ,CopmPrg100Less2Yrs VARCHAR(10)
               ,CopmPrg150Less2Yrs VARCHAR(10)
               ,StillInProg150Percent VARCHAR(10)
               ,TransOut VARCHAR(10)
               ,RevisedCohortCount INT
               ,ExclusionsCount INT
               ,CopmPrg100Less2YrsCount INT
               ,CopmPrg150Less2YrsCount INT
               ,TransOutCount INT
               ,StillInProg150PercentCount INT
               ,GenderSequence INT
               ,RaceSequence INT
               ,StudentId UNIQUEIDENTIFIER
               ,StuEnrollId UNIQUEIDENTIFIER
               ,StartDate DATETIME
            );


        CREATE TABLE #StudentsList
            (
                StudentId UNIQUEIDENTIFIER
               ,StuENrollId UNIQUEIDENTIFIER
               ,SSN VARCHAR(10)
               ,FirstName VARCHAR(100)
               ,LastName VARCHAR(100)
               ,MiddleName VARCHAR(50)
               ,StudentNumber VARCHAR(50)
               ,TransferredOut INT
               ,Exclusions INT
               ,CitizenShip_IPEDSValue INT
               ,ProgramType_IPEDSValue INT
               ,Gender_IPEDSValue INT
               ,FullTimePartTime_IPEDSValue INT
               ,GenderId UNIQUEIDENTIFIER
               ,RaceId UNIQUEIDENTIFIER
               ,CitizenId UNIQUEIDENTIFIER
               ,GenderDescription VARCHAR(50)
               ,RaceDescription VARCHAR(50)
               ,StudentGraduatedStatusCount INT
               ,ClockHourProgramCount INT
            );

        -- Get the list of FullTime, FirstTime, UnderGraduate Students      
        -- Exclude students who are Transferred in to the institution     
        IF LOWER (@CohortPossible) = 'fall'
            BEGIN
                SET @AcadInstFirstTimeStartDate = DATEADD (YEAR, -1, @EndDate);
                INSERT INTO #StudentsList
                            SELECT     DISTINCT t1.StudentId
                                      ,t2.StuEnrollId
                                      ,t1.SSN
                                      ,t1.FirstName
                                      ,t1.LastName
                                      ,t1.MiddleName
                                      ,t1.StudentNumber
                                      ,(
                                       SELECT COUNT (*)
                                       FROM   arStuEnrollments SQ1
                                             ,syStatusCodes SQ2
                                             ,dbo.sySysStatus SQ3
                                       WHERE  SQ1.StatusCodeId = SQ2.StatusCodeId
                                              AND SQ2.SysStatusId = SQ3.SysStatusId
                                              AND SQ1.StuEnrollId = t2.StuEnrollId
                                              AND SQ3.SysStatusId = 19
                                              AND --SQ1.TransferDate<=@EndDate AND     
                                           SQ1.StuEnrollId NOT IN (
                                                                  SELECT DISTINCT StuEnrollId
                                                                  FROM   arTrackTransfer
                                                                  )
                                              AND SQ1.DateDetermined <= @StatusDate
                                       ) AS TransferredOut
                                      ,(
                                      -- Check if the student was either dropped and if the drop reason is either      
                                      -- deceased, active duty, foreign aid service, church mission      
                                      CASE WHEN (
                                                SELECT COUNT (*)
                                                FROM   arStuEnrollments SQ1
                                                      ,syStatusCodes SQ2
                                                      ,dbo.sySysStatus SQ3
                                                      ,arDropReasons SQ44
                                                WHERE  SQ1.StatusCodeId = SQ2.StatusCodeId
                                                       AND SQ2.SysStatusId = SQ3.SysStatusId
                                                       AND SQ1.DropReasonId = SQ44.DropReasonId
                                                       AND SQ1.StuEnrollId = t2.StuEnrollId
                                                       AND SQ3.SysStatusId IN ( 12 ) -- Dropped      
                                                       AND SQ1.DateDetermined <= @StatusDate
                                                       AND SQ44.IPEDSValue IN ( 15, 16, 17, 18, 19 )
                                                ) >= 1 THEN 1
                                           ELSE 0
                                      END
                                       ) AS Exclusions
                                      ,t11.IPEDSValue AS CitizenShip_IPEDSValue
                                      ,t9.IPEDSValue AS ProgramType_IPEDSValue
                                      ,t3.IPEDSValue AS Gender_IPEDSValue
                                      ,t10.IPEDSValue AS FullTimePartTime_IPEDSValue
                                      ,t1.Gender
                                      ,CASE WHEN (
                                                 t1.Race IS NULL
                                                 AND t11.IPEDSValue <> 65
                                                 ) THEN (
                                                        SELECT TOP 1 EthCodeId
                                                        FROM   adEthCodes
                                                        WHERE  EthCodeDescrip = 'Race/ethnicity unknown'
                                                        )
                                            ELSE t1.Race
                                       END AS Race
                                      ,t1.Citizen
                                      ,(
                                       SELECT DISTINCT AgencyDescrip
                                       FROM   syRptAgencyFldValues
                                       WHERE  RptAgencyFldValId = t3.IPEDSValue
                                       ) AS GenderDescription
                                      ,CASE WHEN (
                                                 t1.Race IS NULL
                                                 AND t11.IPEDSValue <> 65
                                                 ) THEN 'Race/ethnicity unknown'
                                            ELSE (
                                                 SELECT DISTINCT AgencyDescrip
                                                 FROM   syRptAgencyFldValues
                                                 WHERE  RptAgencyFldValId = t4.IPEDSValue
                                                 )
                                       END AS RaceDescription
                                      ,(
                                       SELECT COUNT (*)
                                       FROM   arStuEnrollments SEC
                                             ,syStatusCodes SC
                                       WHERE  SEC.StuEnrollId = t2.StuEnrollId
                                              AND SEC.StatusCodeId = SC.StatusCodeId
                                              AND SC.SysStatusId = 14
                                              AND ExpGradDate <= @StatusDate
                                       ) AS StudentGraduatedStatusCount
                                      ,(
                                       SELECT COUNT (*)
                                       FROM   arStuEnrollments SEC
                                             ,arPrograms P
                                             ,arPrgVersions PV
                                       WHERE  SEC.PrgVerId = PV.PrgVerId
                                              AND P.ProgId = PV.ProgId
                                              AND SEC.StuEnrollId = t2.StuEnrollId
                                              AND P.ACId = 5
                                       ) AS ClockHourProgramCount
                            FROM       adGenders t3
                            LEFT JOIN  arStudent t1 ON t3.GenderId = t1.Gender
                            LEFT JOIN  adEthCodes t4 ON t4.EthCodeId = t1.Race
                            INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                            INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                            INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                                         AND t6.SysStatusId NOT IN ( 8 ) -- Ignore "No Start" Students      
                            INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                            INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                            INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                            LEFT JOIN  arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                            LEFT JOIN  dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                            INNER JOIN adCitizenships t11 ON t1.Citizen = t11.CitizenshipId
                            INNER JOIN faStudentAwards SA ON SA.StuEnrollId = t2.StuEnrollId
                            INNER JOIN saFundSources FS ON FS.FundSourceId = SA.AwardTypeId
                            INNER JOIN saAwardTypes AT ON AT.AwardTypeId = FS.AwardTypeId
                            INNER JOIN syAdvFundSources AFS ON AFS.AdvFundSourceId = FS.AdvFundSourceId
                            WHERE      LTRIM (RTRIM (t2.CampusId)) = LTRIM (RTRIM (@CampusId))
                                       AND (
                                           @ProgId IS NULL
                                           OR t8.ProgId IN (
                                                           SELECT Val
                                                           FROM   MultipleValuesForReportParameters (@ProgId, ',', 1)
                                                           )
                                           )
                                       AND t10.IPEDSValue = 61 -- Full Time     
                                       AND t12.IPEDSValue = 11 -- First Time      
                                       AND t9.IPEDSValue = 58 -- Under Graduate    
                                       AND AFS.AdvFundSourceId = 2 -- PELL    
                                       AND AT.AwardTypeId = 1 --Grant    
                                       AND
                                       --2/07/2012 - DE9062 - Academic year options should have a cutoff start date For First Time     
                                       --t2.StartDate<=@EndDate -- Student Should Have Started Before the Report End Date    
                                       (
                                       t2.StartDate > @AcadInstFirstTimeStartDate
                                       AND t2.StartDate <= @EndDate
                                       )
                                       -- Exclude students who are Dropped out/Transferred/Graduated/No Start     
                                       -- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate    
                                       AND t2.StuEnrollId NOT IN (
                                                                 SELECT t1.StuEnrollId
                                                                 FROM   arStuEnrollments t1
                                                                       ,syStatusCodes t2
                                                                 WHERE  t1.StatusCodeId = t2.StatusCodeId
                                                                        AND StartDate <= @EndDate
                                                                        AND -- Student started before the end date range    
                                                                     LTRIM (RTRIM (t1.CampusId)) = LTRIM (RTRIM (@CampusId))
                                                                        AND (
                                                                            @ProgId IS NULL
                                                                            OR t8.ProgId IN (
                                                                                            SELECT Val
                                                                                            FROM   MultipleValuesForReportParameters (@ProgId, ',', 1)
                                                                                            )
                                                                            )
                                                                        AND t2.SysStatusId IN ( 12, 19, 8 ) -- Dropped out/Transferred/No Start    
                                                                        -- Date Determined or ExpGradDate or LDA falls before 08/31/2010    
                                                                        AND (
                                                                            t1.DateDetermined < @EndDate
                                                                            OR ExpGradDate < @EndDate
                                                                            OR LDA < @EndDate
                                                                            )
                                                                 )
                                       -- If Student is enrolled in only one program version and if that program version       
                                       -- happens to be a continuing ed program exclude the student      
                                       AND t2.StudentId NOT IN (
                                                               SELECT DISTINCT StudentId
                                                               FROM   (
                                                                      SELECT   StudentId
                                                                              ,COUNT (*) AS RowCounter
                                                                      FROM     arStuEnrollments
                                                                      WHERE    PrgVerId IN (
                                                                                           SELECT PrgVerId
                                                                                           FROM   arPrgVersions
                                                                                           WHERE  IsContinuingEd = 1
                                                                                           )
                                                                      GROUP BY StudentId
                                                                      HAVING   COUNT (*) = 1
                                                                      ) dtStudent_ContinuingEd
                                                               )
                                       -- Exclude students who were Transferred in to your institution       
                                       -- This was used in FALL Part B report and we can reuse it here      
                                       AND t2.StuEnrollId NOT IN (
                                                                 SELECT DISTINCT SQ1.StuEnrollId
                                                                 FROM   arStuEnrollments SQ1
                                                                       ,adDegCertSeeking SQ2
                                                                 WHERE  SQ1.StuEnrollId = t2.StuEnrollId
                                                                        AND
                                                                     -- To be considered for TransferIn, Student should be a First-Time Student      
                                                                     --SQ1.LeadId IS NOT NULL AND     
                                                                     SQ1.degcertseekingid = SQ2.DegCertSeekingId
                                                                        AND SQ2.IPEDSValue = 11
                                                                        AND (
                                                                            SQ1.TransferHours > 0
                                                                            OR SQ1.StuEnrollId = CASE WHEN @Value = 'letter' THEN
                                                                                                      (
                                                                                                      SELECT TOP 1 StuEnrollId
                                                                                                      FROM   arTransferGrades
                                                                                                      WHERE  StuEnrollId = SQ1.StuEnrollId
                                                                                                             AND GrdSysDetailId IN (
                                                                                                                                   SELECT GrdSysDetailId
                                                                                                                                   FROM   arGradeSystemDetails
                                                                                                                                   WHERE  IsTransferGrade = 1
                                                                                                                                   )
                                                                                                      )
                                                                                                      ELSE (
                                                                                                           SELECT TOP 1 StuEnrollId
                                                                                                           FROM   arTransferGrades
                                                                                                           WHERE  IsTransferred = 1
                                                                                                                  AND StuEnrollId = SQ1.StuEnrollId
                                                                                                           )
                                                                                                 END
                                                                            )
                                                                        AND SQ1.StartDate < @EndDate
                                                                        AND NOT EXISTS (
                                                                                       SELECT StuEnrollId
                                                                                       FROM   arStuEnrollments
                                                                                       WHERE  StudentId = SQ1.StudentId
                                                                                              AND StartDate < SQ1.StartDate
                                                                                       )
                                                                 )
                                       AND t2.StartDate IN (
                                                           SELECT     MIN (StartDate)
                                                           FROM       arStuEnrollments t2
                                                           INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                                                           INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                                                           INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                                                           LEFT JOIN  arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                                                           LEFT JOIN  dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                                                           WHERE      LTRIM (RTRIM (t2.CampusId)) = LTRIM (RTRIM (@CampusId))
                                                                      AND (
                                                                          @ProgId IS NULL
                                                                          OR t8.ProgId IN (
                                                                                          SELECT Val
                                                                                          FROM   MultipleValuesForReportParameters (@ProgId, ',', 1)
                                                                                          )
                                                                          )
                                                                      AND t10.IPEDSValue = 61
                                                                      AND -- Full Time      
                                                               t12.IPEDSValue = 11
                                                                      AND -- First Time      
                                                               t9.IPEDSValue = 58
                                                                      AND -- Under Graduate      
                                                               t2.StudentId = t1.StudentId
                                                                      AND
                                                                      --2/07/2012 - DE9062 - Academic year options should have a cutoff start date For First Time     
                                                                      --t2.StartDate<=@EndDate -- Student Should Have Started Before the Report End Date    
                                                                      (
                                                                      t2.StartDate > @AcadInstFirstTimeStartDate
                                                                      AND t2.StartDate <= @EndDate
                                                                      )
                                                           )
                                       -- If two enrollments fall on same start date pick any one     
                                       AND t2.StuEnrollId IN (
                                                             SELECT     TOP 1 StuEnrollId
                                                             FROM       arStuEnrollments t2
                                                             INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                                                             INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                                                             INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                                                             LEFT JOIN  arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                                                             LEFT JOIN  dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                                                             WHERE      LTRIM (RTRIM (t2.CampusId)) = LTRIM (RTRIM (@CampusId))
                                                                        AND (
                                                                            @ProgId IS NULL
                                                                            OR t8.ProgId IN (
                                                                                            SELECT Val
                                                                                            FROM   MultipleValuesForReportParameters (@ProgId, ',', 1)
                                                                                            )
                                                                            )
                                                                        AND t10.IPEDSValue = 61
                                                                        AND -- Full Time      
                                                                 t12.IPEDSValue = 11
                                                                        AND -- First Time      
                                                                 t9.IPEDSValue = 58
                                                                        AND -- Under Graduate      
                                                                 t2.StudentId = t1.StudentId
                                                                        AND
                                                                        --2/07/2012 - DE9062 - Academic year options should have a cutoff start date For First Time     
                                                                        --t2.StartDate<=@EndDate -- Student Should Have Started Before the Report End Date    
                                                                        (
                                                                        t2.StartDate > @AcadInstFirstTimeStartDate
                                                                        AND t2.StartDate <= @EndDate
                                                                        )
                                                                        AND t2.StartDate IN (
                                                                                            SELECT     MIN (StartDate)
                                                                                            FROM       arStuEnrollments t2
                                                                                            INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                                                                                            INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                                                                                            INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                                                                                            LEFT JOIN  arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                                                                                            LEFT JOIN  dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                                                                                            WHERE      LTRIM (RTRIM (t2.CampusId)) = LTRIM (RTRIM (@CampusId))
                                                                                                       AND (
                                                                                                           @ProgId IS NULL
                                                                                                           OR t8.ProgId IN (
                                                                                                                           SELECT Val
                                                                                                                           FROM   MultipleValuesForReportParameters (
                                                                                                                                                                        @ProgId
                                                                                                                                                                       ,',', 1
                                                                                                                                                                    )
                                                                                                                           )
                                                                                                           )
                                                                                                       AND t10.IPEDSValue = 61
                                                                                                       AND -- Full Time      
                                                                                                t12.IPEDSValue = 11
                                                                                                       AND -- First Time      
                                                                                                t9.IPEDSValue = 58
                                                                                                       AND -- Under Graduate      
                                                                                                t2.StudentId = t1.StudentId
                                                                                                       AND
                                                                                                       --2/07/2012 - DE9062 - Academic year options should have a cutoff start date For First Time     
                                                                                                       --t2.StartDate<=@EndDate -- Student Should Have Started Before the Report End Date    
                                                                                                       (
                                                                                                       t2.StartDate > @AcadInstFirstTimeStartDate
                                                                                                       AND t2.StartDate <= @EndDate
                                                                                                       )
                                                                                            )
                                                             );
            END;
        IF LOWER (@CohortPossible) = 'full'
            BEGIN
                INSERT INTO #StudentsList
                            SELECT     DISTINCT t1.StudentId
                                      ,t2.StuEnrollId
                                      ,t1.SSN
                                      ,t1.FirstName
                                      ,t1.LastName
                                      ,t1.MiddleName
                                      ,t1.StudentNumber
                                      ,(
                                       SELECT COUNT (*)
                                       FROM   arStuEnrollments SQ1
                                             ,syStatusCodes SQ2
                                             ,dbo.sySysStatus SQ3
                                       WHERE  SQ1.StatusCodeId = SQ2.StatusCodeId
                                              AND SQ2.SysStatusId = SQ3.SysStatusId
                                              AND SQ1.StuEnrollId = t2.StuEnrollId
                                              AND SQ3.SysStatusId = 19
                                              AND --SQ1.TransferDate<=@EndDate AND     
                                           SQ1.StuEnrollId NOT IN (
                                                                  SELECT DISTINCT StuEnrollId
                                                                  FROM   arTrackTransfer
                                                                  )
                                              AND SQ1.DateDetermined <= @StatusDate
                                       ) AS TransferredOut
                                      ,(
                                      -- Check if the student was either dropped and if the drop reason is either      
                                      -- deceased, active duty, foreign aid service, church mission      
                                      CASE WHEN (
                                                SELECT COUNT (*)
                                                FROM   arStuEnrollments SQ1
                                                      ,syStatusCodes SQ2
                                                      ,dbo.sySysStatus SQ3
                                                      ,arDropReasons SQ44
                                                WHERE  SQ1.StatusCodeId = SQ2.StatusCodeId
                                                       AND SQ2.SysStatusId = SQ3.SysStatusId
                                                       AND SQ1.DropReasonId = SQ44.DropReasonId
                                                       AND SQ1.StuEnrollId = t2.StuEnrollId
                                                       AND SQ3.SysStatusId IN ( 12 ) -- Dropped      
                                                       AND SQ1.DateDetermined <= @StatusDate
                                                       AND SQ44.IPEDSValue IN ( 15, 16, 17, 18, 19 )
                                                ) >= 1 THEN 1
                                           ELSE 0
                                      END
                                       ) AS Exclusions
                                      ,t11.IPEDSValue AS CitizenShip_IPEDSValue
                                      ,t9.IPEDSValue AS ProgramType_IPEDSValue
                                      ,t3.IPEDSValue AS Gender_IPEDSValue
                                      ,t10.IPEDSValue AS FullTimePartTime_IPEDSValue
                                      ,t1.Gender
                                      ,CASE WHEN (
                                                 t1.Race IS NULL
                                                 AND t11.IPEDSValue <> 65
                                                 ) THEN (
                                                        SELECT TOP 1 EthCodeId
                                                        FROM   adEthCodes
                                                        WHERE  EthCodeDescrip = 'Race/ethnicity unknown'
                                                        )
                                            ELSE t1.Race
                                       END AS Race
                                      ,t1.Citizen
                                      ,(
                                       SELECT DISTINCT AgencyDescrip
                                       FROM   syRptAgencyFldValues
                                       WHERE  RptAgencyFldValId = t3.IPEDSValue
                                       ) AS GenderDescription
                                      ,CASE WHEN (
                                                 t1.Race IS NULL
                                                 AND t11.IPEDSValue <> 65
                                                 ) THEN 'Race/ethnicity unknown'
                                            ELSE (
                                                 SELECT DISTINCT AgencyDescrip
                                                 FROM   syRptAgencyFldValues
                                                 WHERE  RptAgencyFldValId = t4.IPEDSValue
                                                 )
                                       END AS RaceDescription
                                      ,(
                                       SELECT COUNT (*)
                                       FROM   arStuEnrollments SEC
                                             ,syStatusCodes SC
                                       WHERE  SEC.StuEnrollId = t2.StuEnrollId
                                              AND SEC.StatusCodeId = SC.StatusCodeId
                                              AND SC.SysStatusId = 14
                                              AND ExpGradDate <= @StatusDate
                                       ) AS StudentGraduatedStatusCount
                                      ,(
                                       SELECT COUNT (*)
                                       FROM   arStuEnrollments SEC
                                             ,arPrograms P
                                             ,arPrgVersions PV
                                       WHERE  SEC.PrgVerId = PV.PrgVerId
                                              AND P.ProgId = PV.ProgId
                                              AND SEC.StuEnrollId = t2.StuEnrollId
                                              AND P.ACId = 5
                                       ) AS ClockHourProgramCount
                            FROM       adGenders t3
                            LEFT JOIN  arStudent t1 ON t3.GenderId = t1.Gender
                            LEFT JOIN  adEthCodes t4 ON t4.EthCodeId = t1.Race
                            INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                            INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                            INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                                         AND t6.SysStatusId NOT IN ( 8 ) -- Ignore "No Start" Students      
                            INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                            INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                            INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                            LEFT JOIN  arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                            LEFT JOIN  dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                            INNER JOIN adCitizenships t11 ON t1.Citizen = t11.CitizenshipId
                            INNER JOIN faStudentAwards SA ON SA.StuEnrollId = t2.StuEnrollId
                            INNER JOIN saFundSources FS ON FS.FundSourceId = SA.AwardTypeId
                            INNER JOIN saAwardTypes AT ON AT.AwardTypeId = FS.AwardTypeId
                            INNER JOIN syAdvFundSources AFS ON AFS.AdvFundSourceId = FS.AdvFundSourceId
                            WHERE      LTRIM (RTRIM (t2.CampusId)) = LTRIM (RTRIM (@CampusId))
                                       AND (
                                           @ProgId IS NULL
                                           OR t8.ProgId IN (
                                                           SELECT Val
                                                           FROM   MultipleValuesForReportParameters (@ProgId, ',', 1)
                                                           )
                                           )
                                       AND t10.IPEDSValue = 61 -- Full Time     
                                       AND t12.IPEDSValue = 11 -- First Time    
                                       AND t9.IPEDSValue = 58 -- Under Graduate      
                                       AND AFS.AdvFundSourceId = 2 -- PELL    
                                       AND AT.AwardTypeId = 1 --Grant    
                                       AND (
                                           t2.StartDate >= @StartDate
                                           AND t2.StartDate <= @EndDate
                                           )
                                       -- Exclude students who are Dropped out/Transferred/No Start     
                                       -- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate    
                                       AND t2.StuEnrollId NOT IN (
                                                                 SELECT t1.StuEnrollId
                                                                 FROM   arStuEnrollments t1
                                                                       ,syStatusCodes t2
                                                                 WHERE  t1.StatusCodeId = t2.StatusCodeId
                                                                        AND StartDate <= @EndDate
                                                                        AND -- Student started before the end date range    
                                                                     LTRIM (RTRIM (t1.CampusId)) = LTRIM (RTRIM (@CampusId))
                                                                        AND (
                                                                            @ProgId IS NULL
                                                                            OR t8.ProgId IN (
                                                                                            SELECT Val
                                                                                            FROM   MultipleValuesForReportParameters (@ProgId, ',', 1)
                                                                                            )
                                                                            )
                                                                        AND t2.SysStatusId IN ( 12, 19, 8 ) -- Dropped out/Transferred/No Start    
                                                                        -- Date Determined or ExpGradDate or LDA falls before 08/31/2010    
                                                                        AND (
                                                                            t1.DateDetermined < @StartDate
                                                                            OR LDA < @StartDate
                                                                            )
                                                                 )
                                       -- Student Should Have Started Before the Report End Date      
                                       -- If Student is enrolled in only one program version and     
                                       --if that program version       
                                       -- happens to be a continuing ed program exclude the student      
                                       AND t2.StudentId NOT IN (
                                                               SELECT DISTINCT StudentId
                                                               FROM   (
                                                                      SELECT   StudentId
                                                                              ,COUNT (*) AS RowCounter
                                                                      FROM     arStuEnrollments
                                                                      WHERE    PrgVerId IN (
                                                                                           SELECT PrgVerId
                                                                                           FROM   arPrgVersions
                                                                                           WHERE  IsContinuingEd = 1
                                                                                           )
                                                                      GROUP BY StudentId
                                                                      HAVING   COUNT (*) = 1
                                                                      ) dtStudent_ContinuingEd
                                                               )
                                       -- Exclude students who were Transferred in to your institution       
                                       -- This was used in FALL Part B report and we can reuse it here      
                                       AND t2.StuEnrollId NOT IN (
                                                                 SELECT DISTINCT SQ1.StuEnrollId
                                                                 FROM   arStuEnrollments SQ1
                                                                       ,adDegCertSeeking SQ2
                                                                 WHERE  SQ1.StuEnrollId = t2.StuEnrollId
                                                                        AND
                                                                     -- To be considered for TransferIn, Student should be a First-Time Student      
                                                                     --SQ1.LeadId IS NOT NULL AND     
                                                                     SQ1.degcertseekingid = SQ2.DegCertSeekingId
                                                                        AND SQ2.IPEDSValue = 11
                                                                        AND (
                                                                            SQ1.TransferHours > 0
                                                                            OR SQ1.StuEnrollId = CASE WHEN @Value = 'letter' THEN
                                                                                                      (
                                                                                                      SELECT TOP 1 StuEnrollId
                                                                                                      FROM   arTransferGrades
                                                                                                      WHERE  StuEnrollId = SQ1.StuEnrollId
                                                                                                             AND GrdSysDetailId IN (
                                                                                                                                   SELECT GrdSysDetailId
                                                                                                                                   FROM   arGradeSystemDetails
                                                                                                                                   WHERE  IsTransferGrade = 1
                                                                                                                                   )
                                                                                                      )
                                                                                                      ELSE (
                                                                                                           SELECT TOP 1 StuEnrollId
                                                                                                           FROM   arTransferGrades
                                                                                                           WHERE  IsTransferred = 1
                                                                                                                  AND StuEnrollId = SQ1.StuEnrollId
                                                                                                           )
                                                                                                 END
                                                                            )
                                                                        AND SQ1.StartDate < @EndDate
                                                                        AND NOT EXISTS (
                                                                                       SELECT StuEnrollId
                                                                                       FROM   arStuEnrollments
                                                                                       WHERE  StudentId = SQ1.StudentId
                                                                                              AND StartDate < SQ1.StartDate
                                                                                       )
                                                                 )
                                       AND t2.StartDate IN (
                                                           SELECT     MIN (StartDate)
                                                           FROM       arStuEnrollments t2
                                                           INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                                                           INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                                                           INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                                                           LEFT JOIN  arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                                                           LEFT JOIN  dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                                                           WHERE      LTRIM (RTRIM (t2.CampusId)) = LTRIM (RTRIM (@CampusId))
                                                                      AND (
                                                                          @ProgId IS NULL
                                                                          OR t8.ProgId IN (
                                                                                          SELECT Val
                                                                                          FROM   MultipleValuesForReportParameters (@ProgId, ',', 1)
                                                                                          )
                                                                          )
                                                                      AND t10.IPEDSValue = 61
                                                                      AND -- Full Time      
                                                               t12.IPEDSValue = 11
                                                                      AND -- First Time      
                                                               t9.IPEDSValue = 58
                                                                      AND -- Under Graduate      
                                                               t2.StudentId = t1.StudentId
                                                                      AND (
                                                                          t2.StartDate >= @StartDate
                                                                          AND t2.StartDate <= @EndDate
                                                                          )
                                                           )
                                       -- If two enrollments fall on same start date pick any one     
                                       AND t2.StuEnrollId IN (
                                                             SELECT     TOP 1 StuEnrollId
                                                             FROM       arStuEnrollments t2
                                                             INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                                                             INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                                                             INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                                                             LEFT JOIN  arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                                                             LEFT JOIN  dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                                                             WHERE      LTRIM (RTRIM (t2.CampusId)) = LTRIM (RTRIM (@CampusId))
                                                                        AND (
                                                                            @ProgId IS NULL
                                                                            OR t8.ProgId IN (
                                                                                            SELECT Val
                                                                                            FROM   MultipleValuesForReportParameters (@ProgId, ',', 1)
                                                                                            )
                                                                            )
                                                                        AND t10.IPEDSValue = 61
                                                                        AND -- Full Time      
                                                                 t12.IPEDSValue = 11
                                                                        AND -- First Time      
                                                                 t9.IPEDSValue = 58
                                                                        AND -- Under Graduate      
                                                                 t2.StudentId = t1.StudentId
                                                                        AND (
                                                                            t2.StartDate >= @StartDate
                                                                            AND t2.StartDate <= @EndDate
                                                                            )
                                                                        AND t2.StartDate IN (
                                                                                            SELECT     MIN (StartDate)
                                                                                            FROM       arStuEnrollments t2
                                                                                            INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                                                                                            INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                                                                                            INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                                                                                            LEFT JOIN  arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                                                                                            LEFT JOIN  dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                                                                                            WHERE      LTRIM (RTRIM (t2.CampusId)) = LTRIM (RTRIM (@CampusId))
                                                                                                       AND (
                                                                                                           @ProgId IS NULL
                                                                                                           OR t8.ProgId IN (
                                                                                                                           SELECT Val
                                                                                                                           FROM   MultipleValuesForReportParameters (
                                                                                                                                                                        @ProgId
                                                                                                                                                                       ,',', 1
                                                                                                                                                                    )
                                                                                                                           )
                                                                                                           )
                                                                                                       AND t10.IPEDSValue = 61
                                                                                                       AND -- Full Time      
                                                                                                t12.IPEDSValue = 11
                                                                                                       AND -- First Time      
                                                                                                t9.IPEDSValue = 58
                                                                                                       AND -- Under Graduate      
                                                                                                t2.StudentId = t1.StudentId
                                                                                                       AND (
                                                                                                           t2.StartDate >= @StartDate
                                                                                                           AND t2.StartDate <= @EndDate
                                                                                                           )
                                                                                            )
                                                             );
            END;


        SELECT   NEWID () AS RowNumber
                ,dbo.UDF_FormatSSN (SSN) AS SSN
                ,StudentNumber
                ,LastName + ', ' + FirstName + ' ' + ISNULL (MiddleName, '') AS StudentName
                ,1 AS RevisedCohort
                ,Exclusions
                ,CASE WHEN Exclusions >= 1 THEN 1
                      ELSE 0
                 END AS ExclusionsCount
                ,dbo.CompletedProgramin150PercentofProgramLength (StuENrollId, 'D') AS CompletedProgramin150PercentofProgramLength
                ,dbo.CompletedProgramin150PercentofProgramLength (StuENrollId, 'O') AS CompletedProgramin150PercentofProgramLengthForOthers
                ,dbo.CompletedProgramin150PercentofProgramLength (StuENrollId, 'A') AS CompletedProgramin150PercentofProgramLengthForAllDegree
                ,dbo.CompletedProgramin150PercentofProgramLength (StuENrollId, 'B') AS CompletedProgramin150PercentofProgramLengthForBachelors
        FROM     #StudentsList dt
        ORDER BY CASE WHEN @OrderBy = 'SSN' THEN dt.SSN
                 END
                ,CASE WHEN @OrderBy = 'LastName' THEN dt.LastName
                 END
                ,CASE WHEN @OrderBy = 'StudentNumber' THEN CONVERT (INT, dt.StudentNumber)
                 END;
        DROP TABLE #GraduationRate;
        DROP TABLE #StudentsList;
    END;
-- =========================================================================================================  
-- END  --  USP_PELLRecipients_GetList     
-- =========================================================================================================  
GO

GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO

--=================================================================================================
-- USP_GradRatesStafford_Detail_GetList 
--=================================================================================================
PRINT N'If Exists SP USP_GradRatesStafford_Detail_GetList, Dropp it ';
GO
IF EXISTS (
          SELECT 1
          FROM   sys.objects
          WHERE  object_id = OBJECT_ID (N'[usp_GradRatesStafford_Detail_GetList]')
                 AND type IN ( N'P', N'PC' )
          )
    BEGIN
	    PRINT N'    Dropping SP USP_GradRatesStafford_Detail_GetList';
        DROP PROCEDURE USP_GradRatesStafford_Detail_GetList;
    END;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO

PRINT N'Create SP USP_GradRatesStafford_Detail_GetList ';
GO
-- =========================================================================================================  
-- usp_GradRatesStafford_Detail_GetList   
-- =========================================================================================================  
CREATE PROCEDURE dbo.usp_GradRatesStafford_Detail_GetList
    @CampusId VARCHAR(50)
   ,@ProgId VARCHAR(4000) = NULL
   ,@CohortYear VARCHAR(10) = NULL
   ,@CohortPossible VARCHAR(20) = NULL
   ,@OrderBy VARCHAR(100)
   ,@StartDate DATETIME
   ,@EndDate DATETIME
AS --SET @CampusId='3F5E839A-589A-4B2A-B258-35A1A8B3B819'    
    --SET @ProgId=NULL    
    --SET @CohortYear='2013'    
    --SET @CohortPossible= 'full year'    
    --SET @OrderBy = 'SSN'    
    --SET @StartDate='09/01/2010'    
    --SET @EndDate='08/31/2011'    

    BEGIN
        DECLARE @AcadInstFirstTimeStartDate DATETIME;
        DECLARE @ReturnValue VARCHAR(100)
               ,@Value VARCHAR(100);
        DECLARE @SSN VARCHAR(10)
               ,@FirstName VARCHAR(100)
               ,@LastName VARCHAR(100)
               ,@StudentNumber VARCHAR(50)
               ,@TransferredOut INT
               ,@Exclusions INT;
        DECLARE @CitizenShip_IPEDSValue INT
               ,@ProgramType_IPEDSValue INT
               ,@Gender_IPEDSValue INT
               ,@FullTimePartTime_IPEDSValue INT
               ,@StudentId UNIQUEIDENTIFIER;

        DECLARE @StatusDate DATETIME;

        SET @StatusDate = '08/31/' + CONVERT (CHAR(4), YEAR (GETDATE ()) - 1);

        -- Check if School tracks grades by letter or numeric       
        --SET @Value = (SELECT TOP 1 Value FROM dbo.syConfigAppSetValues WHERE SettingId=47)      
        -- 2/07/2013 - updated the @value     
        SET @Value = (
                     SELECT dbo.GetAppSettingValue (47, @CampusId)
                     );

        IF @ProgId IS NOT NULL
            BEGIN
                SELECT @ReturnValue = COALESCE (@ReturnValue, '') + ProgDescrip + ','
                FROM   arPrograms t1
                WHERE  t1.ProgId IN (
                                    SELECT Val
                                    FROM   MultipleValuesForReportParameters (@ProgId, ',', 1)
                                    );
                SET @ReturnValue = SUBSTRING (@ReturnValue, 1, LEN (@ReturnValue) - 1);
            END;
        ELSE
            BEGIN
                SELECT @ReturnValue = COALESCE (@ReturnValue, '') + ProgDescrip + ','
                FROM   arPrograms t1
                WHERE  t1.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
                       AND t1.CampGrpId IN (
                                           SELECT CampGrpId
                                           FROM   syCmpGrpCmps
                                           WHERE  CampusId = @CampusId
                                           );
                SET @ReturnValue = SUBSTRING (@ReturnValue, 1, LEN (@ReturnValue) - 1);
            END;

        -- Create a temp table to hold the final output of this stored proc      
        CREATE TABLE #GraduationRate
            (
                RowNumber UNIQUEIDENTIFIER
               ,SSN VARCHAR(50)
               ,StudentNumber VARCHAR(50)
               ,StudentName VARCHAR(100)
               ,Gender VARCHAR(50)
               ,Race VARCHAR(50)
               ,RevisedCohort VARCHAR(10)
               ,Exclusions VARCHAR(10)
               ,CopmPrg100Less2Yrs VARCHAR(10)
               ,CopmPrg150Less2Yrs VARCHAR(10)
               ,StillInProg150Percent VARCHAR(10)
               ,TransOut VARCHAR(10)
               ,RevisedCohortCount INT
               ,ExclusionsCount INT
               ,CopmPrg100Less2YrsCount INT
               ,CopmPrg150Less2YrsCount INT
               ,TransOutCount INT
               ,StillInProg150PercentCount INT
               ,GenderSequence INT
               ,RaceSequence INT
               ,StudentId UNIQUEIDENTIFIER
               ,StuEnrollId UNIQUEIDENTIFIER
               ,StartDate DATETIME
            );


        CREATE TABLE #StudentsList
            (
                StudentId UNIQUEIDENTIFIER
               ,StuENrollId UNIQUEIDENTIFIER
               ,SSN VARCHAR(10)
               ,FirstName VARCHAR(100)
               ,LastName VARCHAR(100)
               ,MiddleName VARCHAR(50)
               ,StudentNumber VARCHAR(50)
               ,TransferredOut INT
               ,Exclusions INT
               ,CitizenShip_IPEDSValue INT
               ,ProgramType_IPEDSValue INT
               ,Gender_IPEDSValue INT
               ,FullTimePartTime_IPEDSValue INT
               ,GenderId UNIQUEIDENTIFIER
               ,RaceId UNIQUEIDENTIFIER
               ,CitizenId UNIQUEIDENTIFIER
               ,GenderDescription VARCHAR(50)
               ,RaceDescription VARCHAR(50)
               ,StudentGraduatedStatusCount INT
               ,ClockHourProgramCount INT
            );

        CREATE TABLE #StudentsListPELL
            (
                StudentId UNIQUEIDENTIFIER
               ,StuENrollId UNIQUEIDENTIFIER
               ,SSN VARCHAR(10)
               ,FirstName VARCHAR(100)
               ,LastName VARCHAR(100)
               ,MiddleName VARCHAR(50)
               ,StudentNumber VARCHAR(50)
               ,TransferredOut INT
               ,Exclusions INT
               ,CitizenShip_IPEDSValue INT
               ,ProgramType_IPEDSValue INT
               ,Gender_IPEDSValue INT
               ,FullTimePartTime_IPEDSValue INT
               ,GenderId UNIQUEIDENTIFIER
               ,RaceId UNIQUEIDENTIFIER
               ,CitizenId UNIQUEIDENTIFIER
               ,GenderDescription VARCHAR(50)
               ,RaceDescription VARCHAR(50)
               ,StudentGraduatedStatusCount INT
               ,ClockHourProgramCount INT
            );

        -- Get the list of FullTime, FirstTime, UnderGraduate Students      
        -- Exclude students who are Transferred in to the institution     
        IF LOWER (@CohortPossible) = 'fall'
            BEGIN
                SET @AcadInstFirstTimeStartDate = DATEADD (YEAR, -1, @EndDate);
                INSERT INTO #StudentsListPELL
                            SELECT     DISTINCT t1.StudentId
                                      ,t2.StuEnrollId
                                      ,t1.SSN
                                      ,t1.FirstName
                                      ,t1.LastName
                                      ,t1.MiddleName
                                      ,t1.StudentNumber
                                      ,(
                                       SELECT COUNT (*)
                                       FROM   arStuEnrollments SQ1
                                             ,syStatusCodes SQ2
                                             ,dbo.sySysStatus SQ3
                                       WHERE  SQ1.StatusCodeId = SQ2.StatusCodeId
                                              AND SQ2.SysStatusId = SQ3.SysStatusId
                                              AND SQ1.StuEnrollId = t2.StuEnrollId
                                              AND SQ3.SysStatusId = 19
                                              AND --SQ1.TransferDate<=@EndDate AND     
                                           SQ1.StuEnrollId NOT IN (
                                                                  SELECT DISTINCT StuEnrollId
                                                                  FROM   arTrackTransfer
                                                                  )
                                              AND SQ1.DateDetermined <= @StatusDate
                                       ) AS TransferredOut
                                      ,(
                                      -- Check if the student was either dropped and if the drop reason is either      
                                      -- deceased, active duty, foreign aid service, church mission      
                                      CASE WHEN (
                                                SELECT COUNT (*)
                                                FROM   arStuEnrollments SQ1
                                                      ,syStatusCodes SQ2
                                                      ,dbo.sySysStatus SQ3
                                                      ,arDropReasons SQ44
                                                WHERE  SQ1.StatusCodeId = SQ2.StatusCodeId
                                                       AND SQ2.SysStatusId = SQ3.SysStatusId
                                                       AND SQ1.DropReasonId = SQ44.DropReasonId
                                                       AND SQ1.StuEnrollId = t2.StuEnrollId
                                                       AND SQ3.SysStatusId IN ( 12 ) -- Dropped      
                                                       AND SQ1.DateDetermined <= @StatusDate
                                                       AND SQ44.IPEDSValue IN ( 15, 16, 17, 18, 19 )
                                                ) >= 1 THEN 1
                                           ELSE 0
                                      END
                                       ) AS Exclusions
                                      ,t11.IPEDSValue AS CitizenShip_IPEDSValue
                                      ,t9.IPEDSValue AS ProgramType_IPEDSValue
                                      ,t3.IPEDSValue AS Gender_IPEDSValue
                                      ,t10.IPEDSValue AS FullTimePartTime_IPEDSValue
                                      ,t1.Gender
                                      ,CASE WHEN (
                                                 t1.Race IS NULL
                                                 AND t11.IPEDSValue <> 65
                                                 ) THEN (
                                                        SELECT TOP 1 EthCodeId
                                                        FROM   adEthCodes
                                                        WHERE  EthCodeDescrip = 'Race/ethnicity unknown'
                                                        )
                                            ELSE t1.Race
                                       END AS Race
                                      ,t1.Citizen
                                      ,(
                                       SELECT DISTINCT AgencyDescrip
                                       FROM   syRptAgencyFldValues
                                       WHERE  RptAgencyFldValId = t3.IPEDSValue
                                       ) AS GenderDescription
                                      ,CASE WHEN (
                                                 t1.Race IS NULL
                                                 AND t11.IPEDSValue <> 65
                                                 ) THEN 'Race/ethnicity unknown'
                                            ELSE (
                                                 SELECT DISTINCT AgencyDescrip
                                                 FROM   syRptAgencyFldValues
                                                 WHERE  RptAgencyFldValId = t4.IPEDSValue
                                                 )
                                       END AS RaceDescription
                                      ,(
                                       SELECT COUNT (*)
                                       FROM   arStuEnrollments SEC
                                             ,syStatusCodes SC
                                       WHERE  SEC.StuEnrollId = t2.StuEnrollId
                                              AND SEC.StatusCodeId = SC.StatusCodeId
                                              AND SC.SysStatusId = 14
                                              AND ExpGradDate <= @StatusDate
                                       ) AS StudentGraduatedStatusCount
                                      ,(
                                       SELECT COUNT (*)
                                       FROM   arStuEnrollments SEC
                                             ,arPrograms P
                                             ,arPrgVersions PV
                                       WHERE  SEC.PrgVerId = PV.PrgVerId
                                              AND P.ProgId = PV.ProgId
                                              AND SEC.StuEnrollId = t2.StuEnrollId
                                              AND P.ACId = 5
                                       ) AS ClockHourProgramCount
                            FROM       adGenders t3
                            LEFT JOIN  arStudent t1 ON t3.GenderId = t1.Gender
                            LEFT JOIN  adEthCodes t4 ON t4.EthCodeId = t1.Race
                            INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                            INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                            INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                                         AND t6.SysStatusId NOT IN ( 8 ) -- Ignore "No Start" Students      
                            INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                            INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                            INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                            LEFT JOIN  arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                            LEFT JOIN  dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                            INNER JOIN adCitizenships t11 ON t1.Citizen = t11.CitizenshipId
                            INNER JOIN faStudentAwards SA ON SA.StuEnrollId = t2.StuEnrollId
                            INNER JOIN saFundSources FS ON FS.FundSourceId = SA.AwardTypeId
                            INNER JOIN saAwardTypes AT ON AT.AwardTypeId = FS.AwardTypeId
                            INNER JOIN syAdvFundSources AFS ON AFS.AdvFundSourceId = FS.AdvFundSourceId
                            WHERE      LTRIM (RTRIM (t2.CampusId)) = LTRIM (RTRIM (@CampusId))
                                       AND (
                                           @ProgId IS NULL
                                           OR t8.ProgId IN (
                                                           SELECT Val
                                                           FROM   MultipleValuesForReportParameters (@ProgId, ',', 1)
                                                           )
                                           )
                                       AND t10.IPEDSValue = 61 -- Full Time     
                                       AND t12.IPEDSValue = 11 -- First Time      
                                       AND t9.IPEDSValue = 58 -- Under Graduate    
                                       AND AFS.AdvFundSourceId = 2 -- DL Sub    
                                       AND AT.AwardTypeId = 1 --Loan    
                                       AND
                                       --2/07/2012 - DE9062 - Academic year options should have a cutoff start date For First Time     
                                       --t2.StartDate<=@EndDate -- Student Should Have Started Before the Report End Date    
                                       (
                                       t2.StartDate > @AcadInstFirstTimeStartDate
                                       AND t2.StartDate <= @EndDate
                                       )
                                       -- Exclude students who are Dropped out/Transferred/Graduated/No Start     
                                       -- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate    
                                       AND t2.StuEnrollId NOT IN (
                                                                 SELECT t1.StuEnrollId
                                                                 FROM   arStuEnrollments t1
                                                                       ,syStatusCodes t2
                                                                 WHERE  t1.StatusCodeId = t2.StatusCodeId
                                                                        AND StartDate <= @EndDate
                                                                        AND -- Student started before the end date range    
                                                                     LTRIM (RTRIM (t1.CampusId)) = LTRIM (RTRIM (@CampusId))
                                                                        AND (
                                                                            @ProgId IS NULL
                                                                            OR t8.ProgId IN (
                                                                                            SELECT Val
                                                                                            FROM   MultipleValuesForReportParameters (@ProgId, ',', 1)
                                                                                            )
                                                                            )
                                                                        AND t2.SysStatusId IN ( 12, 19, 8 ) -- Dropped out/Transferred/Graduated/No Start    
                                                                        -- Date Determined or ExpGradDate or LDA falls before 08/31/2010    
                                                                        AND (
                                                                            t1.DateDetermined < @EndDate
                                                                            OR ExpGradDate < @EndDate
                                                                            OR LDA < @EndDate
                                                                            )
                                                                 )
                                       -- If Student is enrolled in only one program version and if that program version       
                                       -- happens to be a continuing ed program exclude the student      
                                       AND t2.StudentId NOT IN (
                                                               SELECT DISTINCT StudentId
                                                               FROM   (
                                                                      SELECT   StudentId
                                                                              ,COUNT (*) AS RowCounter
                                                                      FROM     arStuEnrollments
                                                                      WHERE    PrgVerId IN (
                                                                                           SELECT PrgVerId
                                                                                           FROM   arPrgVersions
                                                                                           WHERE  IsContinuingEd = 1
                                                                                           )
                                                                      GROUP BY StudentId
                                                                      HAVING   COUNT (*) = 1
                                                                      ) dtStudent_ContinuingEd
                                                               )
                                       -- Exclude students who were Transferred in to your institution       
                                       -- This was used in FALL Part B report and we can reuse it here      
                                       AND t2.StuEnrollId NOT IN (
                                                                 SELECT DISTINCT SQ1.StuEnrollId
                                                                 FROM   arStuEnrollments SQ1
                                                                       ,adDegCertSeeking SQ2
                                                                 WHERE  SQ1.StuEnrollId = t2.StuEnrollId
                                                                        AND
                                                                     -- To be considered for TransferIn, Student should be a First-Time Student      
                                                                     --SQ1.LeadId IS NOT NULL AND     
                                                                     SQ1.degcertseekingid = SQ2.DegCertSeekingId
                                                                        AND SQ2.IPEDSValue = 11
                                                                        AND (
                                                                            SQ1.TransferHours > 0
                                                                            OR SQ1.StuEnrollId = CASE WHEN @Value = 'letter' THEN
                                                                                                      (
                                                                                                      SELECT TOP 1 StuEnrollId
                                                                                                      FROM   arTransferGrades
                                                                                                      WHERE  StuEnrollId = SQ1.StuEnrollId
                                                                                                             AND GrdSysDetailId IN (
                                                                                                                                   SELECT GrdSysDetailId
                                                                                                                                   FROM   arGradeSystemDetails
                                                                                                                                   WHERE  IsTransferGrade = 1
                                                                                                                                   )
                                                                                                      )
                                                                                                      ELSE (
                                                                                                           SELECT TOP 1 StuEnrollId
                                                                                                           FROM   arTransferGrades
                                                                                                           WHERE  IsTransferred = 1
                                                                                                                  AND StuEnrollId = SQ1.StuEnrollId
                                                                                                           )
                                                                                                 END
                                                                            )
                                                                        AND SQ1.StartDate < @EndDate
                                                                        AND NOT EXISTS (
                                                                                       SELECT StuEnrollId
                                                                                       FROM   arStuEnrollments
                                                                                       WHERE  StudentId = SQ1.StudentId
                                                                                              AND StartDate < SQ1.StartDate
                                                                                       )
                                                                 )
                                       AND t2.StartDate IN (
                                                           SELECT     MIN (StartDate)
                                                           FROM       arStuEnrollments t2
                                                           INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                                                           INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                                                           INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                                                           LEFT JOIN  arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                                                           LEFT JOIN  dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                                                           WHERE      LTRIM (RTRIM (t2.CampusId)) = LTRIM (RTRIM (@CampusId))
                                                                      AND (
                                                                          @ProgId IS NULL
                                                                          OR t8.ProgId IN (
                                                                                          SELECT Val
                                                                                          FROM   MultipleValuesForReportParameters (@ProgId, ',', 1)
                                                                                          )
                                                                          )
                                                                      AND t10.IPEDSValue = 61
                                                                      AND -- Full Time      
                                                               t12.IPEDSValue = 11
                                                                      AND -- First Time      
                                                               t9.IPEDSValue = 58
                                                                      AND -- Under Graduate      
                                                               t2.StudentId = t1.StudentId
                                                                      AND
                                                                      --2/07/2012 - DE9062 - Academic year options should have a cutoff start date For First Time     
                                                                      --t2.StartDate<=@EndDate -- Student Should Have Started Before the Report End Date    
                                                                      (
                                                                      t2.StartDate > @AcadInstFirstTimeStartDate
                                                                      AND t2.StartDate <= @EndDate
                                                                      )
                                                           )
                                       -- If two enrollments fall on same start date pick any one     
                                       AND t2.StuEnrollId IN (
                                                             SELECT     TOP 1 StuEnrollId
                                                             FROM       arStuEnrollments t2
                                                             INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                                                             INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                                                             INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                                                             LEFT JOIN  arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                                                             LEFT JOIN  dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                                                             WHERE      LTRIM (RTRIM (t2.CampusId)) = LTRIM (RTRIM (@CampusId))
                                                                        AND (
                                                                            @ProgId IS NULL
                                                                            OR t8.ProgId IN (
                                                                                            SELECT Val
                                                                                            FROM   MultipleValuesForReportParameters (@ProgId, ',', 1)
                                                                                            )
                                                                            )
                                                                        AND t10.IPEDSValue = 61
                                                                        AND -- Full Time      
                                                                 t12.IPEDSValue = 11
                                                                        AND -- First Time      
                                                                 t9.IPEDSValue = 58
                                                                        AND -- Under Graduate      
                                                                 t2.StudentId = t1.StudentId
                                                                        AND
                                                                        --2/07/2012 - DE9062 - Academic year options should have a cutoff start date For First Time     
                                                                        --t2.StartDate<=@EndDate -- Student Should Have Started Before the Report End Date    
                                                                        (
                                                                        t2.StartDate > @AcadInstFirstTimeStartDate
                                                                        AND t2.StartDate <= @EndDate
                                                                        )
                                                                        AND t2.StartDate IN (
                                                                                            SELECT     MIN (StartDate)
                                                                                            FROM       arStuEnrollments t2
                                                                                            INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                                                                                            INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                                                                                            INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                                                                                            LEFT JOIN  arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                                                                                            LEFT JOIN  dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                                                                                            WHERE      LTRIM (RTRIM (t2.CampusId)) = LTRIM (RTRIM (@CampusId))
                                                                                                       AND (
                                                                                                           @ProgId IS NULL
                                                                                                           OR t8.ProgId IN (
                                                                                                                           SELECT Val
                                                                                                                           FROM   MultipleValuesForReportParameters (
                                                                                                                                                                        @ProgId
                                                                                                                                                                       ,',', 1
                                                                                                                                                                    )
                                                                                                                           )
                                                                                                           )
                                                                                                       AND t10.IPEDSValue = 61
                                                                                                       AND -- Full Time      
                                                                                                t12.IPEDSValue = 11
                                                                                                       AND -- First Time      
                                                                                                t9.IPEDSValue = 58
                                                                                                       AND -- Under Graduate      
                                                                                                t2.StudentId = t1.StudentId
                                                                                                       AND
                                                                                                       --2/07/2012 - DE9062 - Academic year options should have a cutoff start date For First Time     
                                                                                                       --t2.StartDate<=@EndDate -- Student Should Have Started Before the Report End Date    
                                                                                                       (
                                                                                                       t2.StartDate > @AcadInstFirstTimeStartDate
                                                                                                       AND t2.StartDate <= @EndDate
                                                                                                       )
                                                                                            )
                                                             );
                INSERT INTO #StudentsList
                            SELECT     DISTINCT t1.StudentId
                                      ,t2.StuEnrollId
                                      ,t1.SSN
                                      ,t1.FirstName
                                      ,t1.LastName
                                      ,t1.MiddleName
                                      ,t1.StudentNumber
                                      ,(
                                       SELECT COUNT (*)
                                       FROM   arStuEnrollments SQ1
                                             ,syStatusCodes SQ2
                                             ,dbo.sySysStatus SQ3
                                       WHERE  SQ1.StatusCodeId = SQ2.StatusCodeId
                                              AND SQ2.SysStatusId = SQ3.SysStatusId
                                              AND SQ1.StuEnrollId = t2.StuEnrollId
                                              AND SQ3.SysStatusId = 19
                                              AND --SQ1.TransferDate<=@EndDate AND     
                                           SQ1.StuEnrollId NOT IN (
                                                                  SELECT DISTINCT StuEnrollId
                                                                  FROM   arTrackTransfer
                                                                  )
                                              AND SQ1.DateDetermined <= @StatusDate
                                       ) AS TransferredOut
                                      ,(
                                      -- Check if the student was either dropped and if the drop reason is either      
                                      -- deceased, active duty, foreign aid service, church mission      
                                      CASE WHEN (
                                                SELECT COUNT (*)
                                                FROM   arStuEnrollments SQ1
                                                      ,syStatusCodes SQ2
                                                      ,dbo.sySysStatus SQ3
                                                      ,arDropReasons SQ44
                                                WHERE  SQ1.StatusCodeId = SQ2.StatusCodeId
                                                       AND SQ2.SysStatusId = SQ3.SysStatusId
                                                       AND SQ1.DropReasonId = SQ44.DropReasonId
                                                       AND SQ1.StuEnrollId = t2.StuEnrollId
                                                       AND SQ3.SysStatusId IN ( 12 ) -- Dropped      
                                                       AND SQ1.DateDetermined <= @StatusDate
                                                       AND SQ44.IPEDSValue IN ( 15, 16, 17, 18, 19 )
                                                ) >= 1 THEN 1
                                           ELSE 0
                                      END
                                       ) AS Exclusions
                                      ,t11.IPEDSValue AS CitizenShip_IPEDSValue
                                      ,t9.IPEDSValue AS ProgramType_IPEDSValue
                                      ,t3.IPEDSValue AS Gender_IPEDSValue
                                      ,t10.IPEDSValue AS FullTimePartTime_IPEDSValue
                                      ,t1.Gender
                                      ,CASE WHEN (
                                                 t1.Race IS NULL
                                                 AND t11.IPEDSValue <> 65
                                                 ) THEN (
                                                        SELECT TOP 1 EthCodeId
                                                        FROM   adEthCodes
                                                        WHERE  EthCodeDescrip = 'Race/ethnicity unknown'
                                                        )
                                            ELSE t1.Race
                                       END AS Race
                                      ,t1.Citizen
                                      ,(
                                       SELECT DISTINCT AgencyDescrip
                                       FROM   syRptAgencyFldValues
                                       WHERE  RptAgencyFldValId = t3.IPEDSValue
                                       ) AS GenderDescription
                                      ,CASE WHEN (
                                                 t1.Race IS NULL
                                                 AND t11.IPEDSValue <> 65
                                                 ) THEN 'Race/ethnicity unknown'
                                            ELSE (
                                                 SELECT DISTINCT AgencyDescrip
                                                 FROM   syRptAgencyFldValues
                                                 WHERE  RptAgencyFldValId = t4.IPEDSValue
                                                 )
                                       END AS RaceDescription
                                      ,(
                                       SELECT COUNT (*)
                                       FROM   arStuEnrollments SEC
                                             ,syStatusCodes SC
                                       WHERE  SEC.StuEnrollId = t2.StuEnrollId
                                              AND SEC.StatusCodeId = SC.StatusCodeId
                                              AND SC.SysStatusId = 14
                                              AND ExpGradDate <= @StatusDate
                                       ) AS StudentGraduatedStatusCount
                                      ,(
                                       SELECT COUNT (*)
                                       FROM   arStuEnrollments SEC
                                             ,arPrograms P
                                             ,arPrgVersions PV
                                       WHERE  SEC.PrgVerId = PV.PrgVerId
                                              AND P.ProgId = PV.ProgId
                                              AND SEC.StuEnrollId = t2.StuEnrollId
                                              AND P.ACId = 5
                                       ) AS ClockHourProgramCount
                            FROM       adGenders t3
                            LEFT JOIN  arStudent t1 ON t3.GenderId = t1.Gender
                            LEFT JOIN  adEthCodes t4 ON t4.EthCodeId = t1.Race
                            INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                            INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                            INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                                         AND t6.SysStatusId NOT IN ( 8 ) -- Ignore "No Start" Students      
                            INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                            INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                            INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                            LEFT JOIN  arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                            LEFT JOIN  dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                            INNER JOIN adCitizenships t11 ON t1.Citizen = t11.CitizenshipId
                            INNER JOIN faStudentAwards SA ON SA.StuEnrollId = t2.StuEnrollId
                            INNER JOIN saFundSources FS ON FS.FundSourceId = SA.AwardTypeId
                            INNER JOIN saAwardTypes AT ON AT.AwardTypeId = FS.AwardTypeId
                            INNER JOIN syAdvFundSources AFS ON AFS.AdvFundSourceId = FS.AdvFundSourceId
                            WHERE      LTRIM (RTRIM (t2.CampusId)) = LTRIM (RTRIM (@CampusId))
                                       AND t2.StuEnrollId NOT IN (
                                                                 SELECT DISTINCT StuENrollId
                                                                 FROM   #StudentsListPELL
                                                                 )
                                       AND (
                                           @ProgId IS NULL
                                           OR t8.ProgId IN (
                                                           SELECT Val
                                                           FROM   MultipleValuesForReportParameters (@ProgId, ',', 1)
                                                           )
                                           )
                                       AND t10.IPEDSValue = 61 -- Full Time     
                                       AND t12.IPEDSValue = 11 -- First Time      
                                       AND t9.IPEDSValue = 58 -- Under Graduate    
                                       AND AFS.AdvFundSourceId = 7 -- DL Sub    
                                       AND AT.AwardTypeId = 2 --Loan    
                                       AND
                                       --2/07/2012 - DE9062 - Academic year options should have a cutoff start date For First Time     
                                       --t2.StartDate<=@EndDate -- Student Should Have Started Before the Report End Date    
                                       (
                                       t2.StartDate > @AcadInstFirstTimeStartDate
                                       AND t2.StartDate <= @EndDate
                                       )
                                       -- Exclude students who are Dropped out/Transferred/Graduated/No Start     
                                       -- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate    
                                       AND t2.StuEnrollId NOT IN (
                                                                 SELECT t1.StuEnrollId
                                                                 FROM   arStuEnrollments t1
                                                                       ,syStatusCodes t2
                                                                 WHERE  t1.StatusCodeId = t2.StatusCodeId
                                                                        AND StartDate <= @EndDate
                                                                        AND -- Student started before the end date range    
                                                                     LTRIM (RTRIM (t1.CampusId)) = LTRIM (RTRIM (@CampusId))
                                                                        AND (
                                                                            @ProgId IS NULL
                                                                            OR t8.ProgId IN (
                                                                                            SELECT Val
                                                                                            FROM   MultipleValuesForReportParameters (@ProgId, ',', 1)
                                                                                            )
                                                                            )
                                                                        AND t2.SysStatusId IN ( 12, 19, 8 ) -- Dropped out/Transferred/Graduated/No Start    
                                                                        -- Date Determined or ExpGradDate or LDA falls before 08/31/2010    
                                                                        AND (
                                                                            t1.DateDetermined < @EndDate
                                                                            OR ExpGradDate < @EndDate
                                                                            OR LDA < @EndDate
                                                                            )
                                                                 )
                                       -- If Student is enrolled in only one program version and if that program version       
                                       -- happens to be a continuing ed program exclude the student      
                                       AND t2.StudentId NOT IN (
                                                               SELECT DISTINCT StudentId
                                                               FROM   (
                                                                      SELECT   StudentId
                                                                              ,COUNT (*) AS RowCounter
                                                                      FROM     arStuEnrollments
                                                                      WHERE    PrgVerId IN (
                                                                                           SELECT PrgVerId
                                                                                           FROM   arPrgVersions
                                                                                           WHERE  IsContinuingEd = 1
                                                                                           )
                                                                      GROUP BY StudentId
                                                                      HAVING   COUNT (*) = 1
                                                                      ) dtStudent_ContinuingEd
                                                               )
                                       -- Exclude students who were Transferred in to your institution       
                                       -- This was used in FALL Part B report and we can reuse it here      
                                       AND t2.StuEnrollId NOT IN (
                                                                 SELECT DISTINCT SQ1.StuEnrollId
                                                                 FROM   arStuEnrollments SQ1
                                                                       ,adDegCertSeeking SQ2
                                                                 WHERE  SQ1.StuEnrollId = t2.StuEnrollId
                                                                        AND
                                                                     -- To be considered for TransferIn, Student should be a First-Time Student      
                                                                     --SQ1.LeadId IS NOT NULL AND     
                                                                     SQ1.degcertseekingid = SQ2.DegCertSeekingId
                                                                        AND SQ2.IPEDSValue = 11
                                                                        AND (
                                                                            SQ1.TransferHours > 0
                                                                            OR SQ1.StuEnrollId = CASE WHEN @Value = 'letter' THEN
                                                                                                      (
                                                                                                      SELECT TOP 1 StuEnrollId
                                                                                                      FROM   arTransferGrades
                                                                                                      WHERE  StuEnrollId = SQ1.StuEnrollId
                                                                                                             AND GrdSysDetailId IN (
                                                                                                                                   SELECT GrdSysDetailId
                                                                                                                                   FROM   arGradeSystemDetails
                                                                                                                                   WHERE  IsTransferGrade = 1
                                                                                                                                   )
                                                                                                      )
                                                                                                      ELSE (
                                                                                                           SELECT TOP 1 StuEnrollId
                                                                                                           FROM   arTransferGrades
                                                                                                           WHERE  IsTransferred = 1
                                                                                                                  AND StuEnrollId = SQ1.StuEnrollId
                                                                                                           )
                                                                                                 END
                                                                            )
                                                                        AND SQ1.StartDate < @EndDate
                                                                        AND NOT EXISTS (
                                                                                       SELECT StuEnrollId
                                                                                       FROM   arStuEnrollments
                                                                                       WHERE  StudentId = SQ1.StudentId
                                                                                              AND StartDate < SQ1.StartDate
                                                                                       )
                                                                 )
                                       AND t2.StartDate IN (
                                                           SELECT     MIN (StartDate)
                                                           FROM       arStuEnrollments t2
                                                           INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                                                           INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                                                           INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                                                           LEFT JOIN  arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                                                           LEFT JOIN  dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                                                           WHERE      LTRIM (RTRIM (t2.CampusId)) = LTRIM (RTRIM (@CampusId))
                                                                      AND (
                                                                          @ProgId IS NULL
                                                                          OR t8.ProgId IN (
                                                                                          SELECT Val
                                                                                          FROM   MultipleValuesForReportParameters (@ProgId, ',', 1)
                                                                                          )
                                                                          )
                                                                      AND t10.IPEDSValue = 61
                                                                      AND -- Full Time      
                                                               t12.IPEDSValue = 11
                                                                      AND -- First Time      
                                                               t9.IPEDSValue = 58
                                                                      AND -- Under Graduate      
                                                               t2.StudentId = t1.StudentId
                                                                      AND
                                                                      --2/07/2012 - DE9062 - Academic year options should have a cutoff start date For First Time     
                                                                      --t2.StartDate<=@EndDate -- Student Should Have Started Before the Report End Date    
                                                                      (
                                                                      t2.StartDate > @AcadInstFirstTimeStartDate
                                                                      AND t2.StartDate <= @EndDate
                                                                      )
                                                           )
                                       -- If two enrollments fall on same start date pick any one     
                                       AND t2.StuEnrollId IN (
                                                             SELECT     TOP 1 StuEnrollId
                                                             FROM       arStuEnrollments t2
                                                             INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                                                             INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                                                             INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                                                             LEFT JOIN  arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                                                             LEFT JOIN  dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                                                             WHERE      LTRIM (RTRIM (t2.CampusId)) = LTRIM (RTRIM (@CampusId))
                                                                        AND (
                                                                            @ProgId IS NULL
                                                                            OR t8.ProgId IN (
                                                                                            SELECT Val
                                                                                            FROM   MultipleValuesForReportParameters (@ProgId, ',', 1)
                                                                                            )
                                                                            )
                                                                        AND t10.IPEDSValue = 61
                                                                        AND -- Full Time      
                                                                 t12.IPEDSValue = 11
                                                                        AND -- First Time      
                                                                 t9.IPEDSValue = 58
                                                                        AND -- Under Graduate      
                                                                 t2.StudentId = t1.StudentId
                                                                        AND
                                                                        --2/07/2012 - DE9062 - Academic year options should have a cutoff start date For First Time     
                                                                        --t2.StartDate<=@EndDate -- Student Should Have Started Before the Report End Date    
                                                                        (
                                                                        t2.StartDate > @AcadInstFirstTimeStartDate
                                                                        AND t2.StartDate <= @EndDate
                                                                        )
                                                                        AND t2.StartDate IN (
                                                                                            SELECT     MIN (StartDate)
                                                                                            FROM       arStuEnrollments t2
                                                                                            INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                                                                                            INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                                                                                            INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                                                                                            LEFT JOIN  arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                                                                                            LEFT JOIN  dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                                                                                            WHERE      LTRIM (RTRIM (t2.CampusId)) = LTRIM (RTRIM (@CampusId))
                                                                                                       AND (
                                                                                                           @ProgId IS NULL
                                                                                                           OR t8.ProgId IN (
                                                                                                                           SELECT Val
                                                                                                                           FROM   MultipleValuesForReportParameters (
                                                                                                                                                                        @ProgId
                                                                                                                                                                       ,',', 1
                                                                                                                                                                    )
                                                                                                                           )
                                                                                                           )
                                                                                                       AND t10.IPEDSValue = 61
                                                                                                       AND -- Full Time      
                                                                                                t12.IPEDSValue = 11
                                                                                                       AND -- First Time      
                                                                                                t9.IPEDSValue = 58
                                                                                                       AND -- Under Graduate      
                                                                                                t2.StudentId = t1.StudentId
                                                                                                       AND
                                                                                                       --2/07/2012 - DE9062 - Academic year options should have a cutoff start date For First Time     
                                                                                                       --t2.StartDate<=@EndDate -- Student Should Have Started Before the Report End Date    
                                                                                                       (
                                                                                                       t2.StartDate > @AcadInstFirstTimeStartDate
                                                                                                       AND t2.StartDate <= @EndDate
                                                                                                       )
                                                                                            )
                                                             );
            END;
        IF LOWER (@CohortPossible) = 'full'
            BEGIN
                INSERT INTO #StudentsListPELL
                            SELECT     DISTINCT t1.StudentId
                                      ,t2.StuEnrollId
                                      ,t1.SSN
                                      ,t1.FirstName
                                      ,t1.LastName
                                      ,t1.MiddleName
                                      ,t1.StudentNumber
                                      ,(
                                       SELECT COUNT (*)
                                       FROM   arStuEnrollments SQ1
                                             ,syStatusCodes SQ2
                                             ,dbo.sySysStatus SQ3
                                       WHERE  SQ1.StatusCodeId = SQ2.StatusCodeId
                                              AND SQ2.SysStatusId = SQ3.SysStatusId
                                              AND SQ1.StuEnrollId = t2.StuEnrollId
                                              AND SQ3.SysStatusId = 19
                                              AND --SQ1.TransferDate<=@EndDate AND     
                                           SQ1.StuEnrollId NOT IN (
                                                                  SELECT DISTINCT StuEnrollId
                                                                  FROM   arTrackTransfer
                                                                  )
                                              AND SQ1.DateDetermined <= @StatusDate
                                       ) AS TransferredOut
                                      ,(
                                      -- Check if the student was either dropped and if the drop reason is either      
                                      -- deceased, active duty, foreign aid service, church mission      
                                      CASE WHEN (
                                                SELECT COUNT (*)
                                                FROM   arStuEnrollments SQ1
                                                      ,syStatusCodes SQ2
                                                      ,dbo.sySysStatus SQ3
                                                      ,arDropReasons SQ44
                                                WHERE  SQ1.StatusCodeId = SQ2.StatusCodeId
                                                       AND SQ2.SysStatusId = SQ3.SysStatusId
                                                       AND SQ1.DropReasonId = SQ44.DropReasonId
                                                       AND SQ1.StuEnrollId = t2.StuEnrollId
                                                       AND SQ3.SysStatusId IN ( 12 ) -- Dropped      
                                                       AND SQ1.DateDetermined <= @StatusDate
                                                       AND SQ44.IPEDSValue IN ( 15, 16, 17, 18, 19 )
                                                ) >= 1 THEN 1
                                           ELSE 0
                                      END
                                       ) AS Exclusions
                                      ,t11.IPEDSValue AS CitizenShip_IPEDSValue
                                      ,t9.IPEDSValue AS ProgramType_IPEDSValue
                                      ,t3.IPEDSValue AS Gender_IPEDSValue
                                      ,t10.IPEDSValue AS FullTimePartTime_IPEDSValue
                                      ,t1.Gender
                                      ,CASE WHEN (
                                                 t1.Race IS NULL
                                                 AND t11.IPEDSValue <> 65
                                                 ) THEN (
                                                        SELECT TOP 1 EthCodeId
                                                        FROM   adEthCodes
                                                        WHERE  EthCodeDescrip = 'Race/ethnicity unknown'
                                                        )
                                            ELSE t1.Race
                                       END AS Race
                                      ,t1.Citizen
                                      ,(
                                       SELECT DISTINCT AgencyDescrip
                                       FROM   syRptAgencyFldValues
                                       WHERE  RptAgencyFldValId = t3.IPEDSValue
                                       ) AS GenderDescription
                                      ,CASE WHEN (
                                                 t1.Race IS NULL
                                                 AND t11.IPEDSValue <> 65
                                                 ) THEN 'Race/ethnicity unknown'
                                            ELSE (
                                                 SELECT DISTINCT AgencyDescrip
                                                 FROM   syRptAgencyFldValues
                                                 WHERE  RptAgencyFldValId = t4.IPEDSValue
                                                 )
                                       END AS RaceDescription
                                      ,(
                                       SELECT COUNT (*)
                                       FROM   arStuEnrollments SEC
                                             ,syStatusCodes SC
                                       WHERE  SEC.StuEnrollId = t2.StuEnrollId
                                              AND SEC.StatusCodeId = SC.StatusCodeId
                                              AND SC.SysStatusId = 14
                                              AND ExpGradDate <= @StatusDate
                                       ) AS StudentGraduatedStatusCount
                                      ,(
                                       SELECT COUNT (*)
                                       FROM   arStuEnrollments SEC
                                             ,arPrograms P
                                             ,arPrgVersions PV
                                       WHERE  SEC.PrgVerId = PV.PrgVerId
                                              AND P.ProgId = PV.ProgId
                                              AND SEC.StuEnrollId = t2.StuEnrollId
                                              AND P.ACId = 5
                                       ) AS ClockHourProgramCount
                            FROM       adGenders t3
                            LEFT JOIN  arStudent t1 ON t3.GenderId = t1.Gender
                            LEFT JOIN  adEthCodes t4 ON t4.EthCodeId = t1.Race
                            INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                            INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                            INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                                         AND t6.SysStatusId NOT IN ( 8 ) -- Ignore "No Start" Students      
                            INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                            INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                            INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                            LEFT JOIN  arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                            LEFT JOIN  dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                            INNER JOIN adCitizenships t11 ON t1.Citizen = t11.CitizenshipId
                            INNER JOIN faStudentAwards SA ON SA.StuEnrollId = t2.StuEnrollId
                            INNER JOIN saFundSources FS ON FS.FundSourceId = SA.AwardTypeId
                            INNER JOIN saAwardTypes AT ON AT.AwardTypeId = FS.AwardTypeId
                            INNER JOIN syAdvFundSources AFS ON AFS.AdvFundSourceId = FS.AdvFundSourceId
                            WHERE      LTRIM (RTRIM (t2.CampusId)) = LTRIM (RTRIM (@CampusId))
                                       AND (
                                           @ProgId IS NULL
                                           OR t8.ProgId IN (
                                                           SELECT Val
                                                           FROM   MultipleValuesForReportParameters (@ProgId, ',', 1)
                                                           )
                                           )
                                       AND t10.IPEDSValue = 61 -- Full Time     
                                       AND t12.IPEDSValue = 11 -- First Time    
                                       AND t9.IPEDSValue = 58 -- Under Graduate      
                                       AND AFS.AdvFundSourceId = 2 -- PELL    
                                       AND AT.AwardTypeId = 1 --Grant    
                                       AND (
                                           t2.StartDate >= @StartDate
                                           AND t2.StartDate <= @EndDate
                                           )
                                       -- Exclude students who are Dropped out/Transferred/Graduated/No Start     
                                       -- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate    
                                       AND t2.StuEnrollId NOT IN (
                                                                 SELECT t1.StuEnrollId
                                                                 FROM   arStuEnrollments t1
                                                                       ,syStatusCodes t2
                                                                 WHERE  t1.StatusCodeId = t2.StatusCodeId
                                                                        AND StartDate <= @EndDate
                                                                        AND -- Student started before the end date range    
                                                                     LTRIM (RTRIM (t1.CampusId)) = LTRIM (RTRIM (@CampusId))
                                                                        AND (
                                                                            @ProgId IS NULL
                                                                            OR t8.ProgId IN (
                                                                                            SELECT Val
                                                                                            FROM   MultipleValuesForReportParameters (@ProgId, ',', 1)
                                                                                            )
                                                                            )
                                                                        AND t2.SysStatusId IN ( 12, 19, 8 ) -- Dropped out/Transferred/Graduated/No Start    
                                                                        -- Date Determined or ExpGradDate or LDA falls before 08/31/2010    
                                                                        AND (
                                                                            t1.DateDetermined < @StartDate
                                                                            OR ExpGradDate < @StartDate
                                                                            OR LDA < @StartDate
                                                                            )
                                                                 )
                                       -- Student Should Have Started Before the Report End Date      
                                       -- If Student is enrolled in only one program version and     
                                       --if that program version       
                                       -- happens to be a continuing ed program exclude the student      
                                       AND t2.StudentId NOT IN (
                                                               SELECT DISTINCT StudentId
                                                               FROM   (
                                                                      SELECT   StudentId
                                                                              ,COUNT (*) AS RowCounter
                                                                      FROM     arStuEnrollments
                                                                      WHERE    PrgVerId IN (
                                                                                           SELECT PrgVerId
                                                                                           FROM   arPrgVersions
                                                                                           WHERE  IsContinuingEd = 1
                                                                                           )
                                                                      GROUP BY StudentId
                                                                      HAVING   COUNT (*) = 1
                                                                      ) dtStudent_ContinuingEd
                                                               )
                                       -- Exclude students who were Transferred in to your institution       
                                       -- This was used in FALL Part B report and we can reuse it here      
                                       AND t2.StuEnrollId NOT IN (
                                                                 SELECT DISTINCT SQ1.StuEnrollId
                                                                 FROM   arStuEnrollments SQ1
                                                                       ,adDegCertSeeking SQ2
                                                                 WHERE  SQ1.StuEnrollId = t2.StuEnrollId
                                                                        AND
                                                                     -- To be considered for TransferIn, Student should be a First-Time Student      
                                                                     --SQ1.LeadId IS NOT NULL AND     
                                                                     SQ1.degcertseekingid = SQ2.DegCertSeekingId
                                                                        AND SQ2.IPEDSValue = 11
                                                                        AND (
                                                                            SQ1.TransferHours > 0
                                                                            OR SQ1.StuEnrollId = CASE WHEN @Value = 'letter' THEN
                                                                                                      (
                                                                                                      SELECT TOP 1 StuEnrollId
                                                                                                      FROM   arTransferGrades
                                                                                                      WHERE  StuEnrollId = SQ1.StuEnrollId
                                                                                                             AND GrdSysDetailId IN (
                                                                                                                                   SELECT GrdSysDetailId
                                                                                                                                   FROM   arGradeSystemDetails
                                                                                                                                   WHERE  IsTransferGrade = 1
                                                                                                                                   )
                                                                                                      )
                                                                                                      ELSE (
                                                                                                           SELECT TOP 1 StuEnrollId
                                                                                                           FROM   arTransferGrades
                                                                                                           WHERE  IsTransferred = 1
                                                                                                                  AND StuEnrollId = SQ1.StuEnrollId
                                                                                                           )
                                                                                                 END
                                                                            )
                                                                        AND SQ1.StartDate < @EndDate
                                                                        AND NOT EXISTS (
                                                                                       SELECT StuEnrollId
                                                                                       FROM   arStuEnrollments
                                                                                       WHERE  StudentId = SQ1.StudentId
                                                                                              AND StartDate < SQ1.StartDate
                                                                                       )
                                                                 )
                                       AND t2.StartDate IN (
                                                           SELECT     MIN (StartDate)
                                                           FROM       arStuEnrollments t2
                                                           INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                                                           INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                                                           INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                                                           LEFT JOIN  arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                                                           LEFT JOIN  dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                                                           WHERE      LTRIM (RTRIM (t2.CampusId)) = LTRIM (RTRIM (@CampusId))
                                                                      AND (
                                                                          @ProgId IS NULL
                                                                          OR t8.ProgId IN (
                                                                                          SELECT Val
                                                                                          FROM   MultipleValuesForReportParameters (@ProgId, ',', 1)
                                                                                          )
                                                                          )
                                                                      AND t10.IPEDSValue = 61
                                                                      AND -- Full Time      
                                                               t12.IPEDSValue = 11
                                                                      AND -- First Time      
                                                               t9.IPEDSValue = 58
                                                                      AND -- Under Graduate      
                                                               t2.StudentId = t1.StudentId
                                                                      AND (
                                                                          t2.StartDate >= @StartDate
                                                                          AND t2.StartDate <= @EndDate
                                                                          )
                                                           )
                                       -- If two enrollments fall on same start date pick any one     
                                       AND t2.StuEnrollId IN (
                                                             SELECT     TOP 1 StuEnrollId
                                                             FROM       arStuEnrollments t2
                                                             INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                                                             INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                                                             INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                                                             LEFT JOIN  arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                                                             LEFT JOIN  dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                                                             WHERE      LTRIM (RTRIM (t2.CampusId)) = LTRIM (RTRIM (@CampusId))
                                                                        AND (
                                                                            @ProgId IS NULL
                                                                            OR t8.ProgId IN (
                                                                                            SELECT Val
                                                                                            FROM   MultipleValuesForReportParameters (@ProgId, ',', 1)
                                                                                            )
                                                                            )
                                                                        AND t10.IPEDSValue = 61
                                                                        AND -- Full Time      
                                                                 t12.IPEDSValue = 11
                                                                        AND -- First Time      
                                                                 t9.IPEDSValue = 58
                                                                        AND -- Under Graduate      
                                                                 t2.StudentId = t1.StudentId
                                                                        AND (
                                                                            t2.StartDate >= @StartDate
                                                                            AND t2.StartDate <= @EndDate
                                                                            )
                                                                        AND t2.StartDate IN (
                                                                                            SELECT     MIN (StartDate)
                                                                                            FROM       arStuEnrollments t2
                                                                                            INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                                                                                            INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                                                                                            INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                                                                                            LEFT JOIN  arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                                                                                            LEFT JOIN  dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                                                                                            WHERE      LTRIM (RTRIM (t2.CampusId)) = LTRIM (RTRIM (@CampusId))
                                                                                                       AND (
                                                                                                           @ProgId IS NULL
                                                                                                           OR t8.ProgId IN (
                                                                                                                           SELECT Val
                                                                                                                           FROM   MultipleValuesForReportParameters (
                                                                                                                                                                        @ProgId
                                                                                                                                                                       ,',', 1
                                                                                                                                                                    )
                                                                                                                           )
                                                                                                           )
                                                                                                       AND t10.IPEDSValue = 61
                                                                                                       AND -- Full Time      
                                                                                                t12.IPEDSValue = 11
                                                                                                       AND -- First Time      
                                                                                                t9.IPEDSValue = 58
                                                                                                       AND -- Under Graduate      
                                                                                                t2.StudentId = t1.StudentId
                                                                                                       AND (
                                                                                                           t2.StartDate >= @StartDate
                                                                                                           AND t2.StartDate <= @EndDate
                                                                                                           )
                                                                                            )
                                                             );
                INSERT INTO #StudentsList
                            SELECT     DISTINCT t1.StudentId
                                      ,t2.StuEnrollId
                                      ,t1.SSN
                                      ,t1.FirstName
                                      ,t1.LastName
                                      ,t1.MiddleName
                                      ,t1.StudentNumber
                                      ,(
                                       SELECT COUNT (*)
                                       FROM   arStuEnrollments SQ1
                                             ,syStatusCodes SQ2
                                             ,dbo.sySysStatus SQ3
                                       WHERE  SQ1.StatusCodeId = SQ2.StatusCodeId
                                              AND SQ2.SysStatusId = SQ3.SysStatusId
                                              AND SQ1.StuEnrollId = t2.StuEnrollId
                                              AND SQ3.SysStatusId = 19
                                              AND --SQ1.TransferDate<=@EndDate AND     
                                           SQ1.StuEnrollId NOT IN (
                                                                  SELECT DISTINCT StuEnrollId
                                                                  FROM   arTrackTransfer
                                                                  )
                                              AND SQ1.DateDetermined <= @StatusDate
                                       ) AS TransferredOut
                                      ,(
                                      -- Check if the student was either dropped and if the drop reason is either      
                                      -- deceased, active duty, foreign aid service, church mission      
                                      CASE WHEN (
                                                SELECT COUNT (*)
                                                FROM   arStuEnrollments SQ1
                                                      ,syStatusCodes SQ2
                                                      ,dbo.sySysStatus SQ3
                                                      ,arDropReasons SQ44
                                                WHERE  SQ1.StatusCodeId = SQ2.StatusCodeId
                                                       AND SQ2.SysStatusId = SQ3.SysStatusId
                                                       AND SQ1.DropReasonId = SQ44.DropReasonId
                                                       AND SQ1.StuEnrollId = t2.StuEnrollId
                                                       AND SQ3.SysStatusId IN ( 12 ) -- Dropped      
                                                       AND SQ1.DateDetermined <= @StatusDate
                                                       AND SQ44.IPEDSValue IN ( 15, 16, 17, 18, 19 )
                                                ) >= 1 THEN 1
                                           ELSE 0
                                      END
                                       ) AS Exclusions
                                      ,t11.IPEDSValue AS CitizenShip_IPEDSValue
                                      ,t9.IPEDSValue AS ProgramType_IPEDSValue
                                      ,t3.IPEDSValue AS Gender_IPEDSValue
                                      ,t10.IPEDSValue AS FullTimePartTime_IPEDSValue
                                      ,t1.Gender
                                      ,CASE WHEN (
                                                 t1.Race IS NULL
                                                 AND t11.IPEDSValue <> 65
                                                 ) THEN (
                                                        SELECT TOP 1 EthCodeId
                                                        FROM   adEthCodes
                                                        WHERE  EthCodeDescrip = 'Race/ethnicity unknown'
                                                        )
                                            ELSE t1.Race
                                       END AS Race
                                      ,t1.Citizen
                                      ,(
                                       SELECT DISTINCT AgencyDescrip
                                       FROM   syRptAgencyFldValues
                                       WHERE  RptAgencyFldValId = t3.IPEDSValue
                                       ) AS GenderDescription
                                      ,CASE WHEN (
                                                 t1.Race IS NULL
                                                 AND t11.IPEDSValue <> 65
                                                 ) THEN 'Race/ethnicity unknown'
                                            ELSE (
                                                 SELECT DISTINCT AgencyDescrip
                                                 FROM   syRptAgencyFldValues
                                                 WHERE  RptAgencyFldValId = t4.IPEDSValue
                                                 )
                                       END AS RaceDescription
                                      ,(
                                       SELECT COUNT (*)
                                       FROM   arStuEnrollments SEC
                                             ,syStatusCodes SC
                                       WHERE  SEC.StuEnrollId = t2.StuEnrollId
                                              AND SEC.StatusCodeId = SC.StatusCodeId
                                              AND SC.SysStatusId = 14
                                              AND ExpGradDate <= @StatusDate
                                       ) AS StudentGraduatedStatusCount
                                      ,(
                                       SELECT COUNT (*)
                                       FROM   arStuEnrollments SEC
                                             ,arPrograms P
                                             ,arPrgVersions PV
                                       WHERE  SEC.PrgVerId = PV.PrgVerId
                                              AND P.ProgId = PV.ProgId
                                              AND SEC.StuEnrollId = t2.StuEnrollId
                                              AND P.ACId = 5
                                       ) AS ClockHourProgramCount
                            FROM       adGenders t3
                            LEFT JOIN  arStudent t1 ON t3.GenderId = t1.Gender
                            LEFT JOIN  adEthCodes t4 ON t4.EthCodeId = t1.Race
                            INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                            INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                            INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                                         AND t6.SysStatusId NOT IN ( 8 ) -- Ignore "No Start" Students      
                            INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                            INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                            INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                            LEFT JOIN  arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                            LEFT JOIN  dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                            INNER JOIN adCitizenships t11 ON t1.Citizen = t11.CitizenshipId
                            INNER JOIN faStudentAwards SA ON SA.StuEnrollId = t2.StuEnrollId
                            INNER JOIN saFundSources FS ON FS.FundSourceId = SA.AwardTypeId
                            INNER JOIN saAwardTypes AT ON AT.AwardTypeId = FS.AwardTypeId
                            INNER JOIN syAdvFundSources AFS ON AFS.AdvFundSourceId = FS.AdvFundSourceId
                            WHERE      LTRIM (RTRIM (t2.CampusId)) = LTRIM (RTRIM (@CampusId))
                                       AND t2.StuEnrollId NOT IN (
                                                                 SELECT DISTINCT StuENrollId
                                                                 FROM   #StudentsListPELL
                                                                 )
                                       AND (
                                           @ProgId IS NULL
                                           OR t8.ProgId IN (
                                                           SELECT Val
                                                           FROM   MultipleValuesForReportParameters (@ProgId, ',', 1)
                                                           )
                                           )
                                       AND t10.IPEDSValue = 61 -- Full Time     
                                       AND t12.IPEDSValue = 11 -- First Time    
                                       AND t9.IPEDSValue = 58 -- Under Graduate      
                                       AND AFS.AdvFundSourceId = 7 -- DL SUB    
                                       AND AT.AwardTypeId = 2 --LOAN    
                                       AND (
                                           t2.StartDate >= @StartDate
                                           AND t2.StartDate <= @EndDate
                                           )
                                       -- Exclude students who are Dropped out/Transferred/Graduated/No Start     
                                       -- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate    
                                       AND t2.StuEnrollId NOT IN (
                                                                 SELECT t1.StuEnrollId
                                                                 FROM   arStuEnrollments t1
                                                                       ,syStatusCodes t2
                                                                 WHERE  t1.StatusCodeId = t2.StatusCodeId
                                                                        AND StartDate <= @EndDate
                                                                        AND -- Student started before the end date range    
                                                                     LTRIM (RTRIM (t1.CampusId)) = LTRIM (RTRIM (@CampusId))
                                                                        AND (
                                                                            @ProgId IS NULL
                                                                            OR t8.ProgId IN (
                                                                                            SELECT Val
                                                                                            FROM   MultipleValuesForReportParameters (@ProgId, ',', 1)
                                                                                            )
                                                                            )
                                                                        AND t2.SysStatusId IN ( 12, 19, 8 ) -- Dropped out/Transferred/Graduated/No Start    
                                                                        -- Date Determined or ExpGradDate or LDA falls before 08/31/2010    
                                                                        AND (
                                                                            t1.DateDetermined < @StartDate
                                                                            OR ExpGradDate < @StartDate
                                                                            OR LDA < @StartDate
                                                                            )
                                                                 )
                                       -- Student Should Have Started Before the Report End Date      
                                       -- If Student is enrolled in only one program version and     
                                       --if that program version       
                                       -- happens to be a continuing ed program exclude the student      
                                       AND t2.StudentId NOT IN (
                                                               SELECT DISTINCT StudentId
                                                               FROM   (
                                                                      SELECT   StudentId
                                                                              ,COUNT (*) AS RowCounter
                                                                      FROM     arStuEnrollments
                                                                      WHERE    PrgVerId IN (
                                                                                           SELECT PrgVerId
                                                                                           FROM   arPrgVersions
                                                                                           WHERE  IsContinuingEd = 1
                                                                                           )
                                                                      GROUP BY StudentId
                                                                      HAVING   COUNT (*) = 1
                                                                      ) dtStudent_ContinuingEd
                                                               )
                                       -- Exclude students who were Transferred in to your institution       
                                       -- This was used in FALL Part B report and we can reuse it here      
                                       AND t2.StuEnrollId NOT IN (
                                                                 SELECT DISTINCT SQ1.StuEnrollId
                                                                 FROM   arStuEnrollments SQ1
                                                                       ,adDegCertSeeking SQ2
                                                                 WHERE  SQ1.StuEnrollId = t2.StuEnrollId
                                                                        AND
                                                                     -- To be considered for TransferIn, Student should be a First-Time Student      
                                                                     --SQ1.LeadId IS NOT NULL AND     
                                                                     SQ1.degcertseekingid = SQ2.DegCertSeekingId
                                                                        AND SQ2.IPEDSValue = 11
                                                                        AND (
                                                                            SQ1.TransferHours > 0
                                                                            OR SQ1.StuEnrollId = CASE WHEN @Value = 'letter' THEN
                                                                                                      (
                                                                                                      SELECT TOP 1 StuEnrollId
                                                                                                      FROM   arTransferGrades
                                                                                                      WHERE  StuEnrollId = SQ1.StuEnrollId
                                                                                                             AND GrdSysDetailId IN (
                                                                                                                                   SELECT GrdSysDetailId
                                                                                                                                   FROM   arGradeSystemDetails
                                                                                                                                   WHERE  IsTransferGrade = 1
                                                                                                                                   )
                                                                                                      )
                                                                                                      ELSE (
                                                                                                           SELECT TOP 1 StuEnrollId
                                                                                                           FROM   arTransferGrades
                                                                                                           WHERE  IsTransferred = 1
                                                                                                                  AND StuEnrollId = SQ1.StuEnrollId
                                                                                                           )
                                                                                                 END
                                                                            )
                                                                        AND SQ1.StartDate < @EndDate
                                                                        AND NOT EXISTS (
                                                                                       SELECT StuEnrollId
                                                                                       FROM   arStuEnrollments
                                                                                       WHERE  StudentId = SQ1.StudentId
                                                                                              AND StartDate < SQ1.StartDate
                                                                                       )
                                                                 )
                                       AND t2.StartDate IN (
                                                           SELECT     MIN (StartDate)
                                                           FROM       arStuEnrollments t2
                                                           INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                                                           INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                                                           INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                                                           LEFT JOIN  arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                                                           LEFT JOIN  dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                                                           WHERE      LTRIM (RTRIM (t2.CampusId)) = LTRIM (RTRIM (@CampusId))
                                                                      AND (
                                                                          @ProgId IS NULL
                                                                          OR t8.ProgId IN (
                                                                                          SELECT Val
                                                                                          FROM   MultipleValuesForReportParameters (@ProgId, ',', 1)
                                                                                          )
                                                                          )
                                                                      AND t10.IPEDSValue = 61
                                                                      AND -- Full Time      
                                                               t12.IPEDSValue = 11
                                                                      AND -- First Time      
                                                               t9.IPEDSValue = 58
                                                                      AND -- Under Graduate      
                                                               t2.StudentId = t1.StudentId
                                                                      AND (
                                                                          t2.StartDate >= @StartDate
                                                                          AND t2.StartDate <= @EndDate
                                                                          )
                                                           )
                                       -- If two enrollments fall on same start date pick any one     
                                       AND t2.StuEnrollId IN (
                                                             SELECT     TOP 1 StuEnrollId
                                                             FROM       arStuEnrollments t2
                                                             INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                                                             INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                                                             INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                                                             LEFT JOIN  arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                                                             LEFT JOIN  dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                                                             WHERE      LTRIM (RTRIM (t2.CampusId)) = LTRIM (RTRIM (@CampusId))
                                                                        AND (
                                                                            @ProgId IS NULL
                                                                            OR t8.ProgId IN (
                                                                                            SELECT Val
                                                                                            FROM   MultipleValuesForReportParameters (@ProgId, ',', 1)
                                                                                            )
                                                                            )
                                                                        AND t10.IPEDSValue = 61
                                                                        AND -- Full Time      
                                                                 t12.IPEDSValue = 11
                                                                        AND -- First Time      
                                                                 t9.IPEDSValue = 58
                                                                        AND -- Under Graduate      
                                                                 t2.StudentId = t1.StudentId
                                                                        AND (
                                                                            t2.StartDate >= @StartDate
                                                                            AND t2.StartDate <= @EndDate
                                                                            )
                                                                        AND t2.StartDate IN (
                                                                                            SELECT     MIN (StartDate)
                                                                                            FROM       arStuEnrollments t2
                                                                                            INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                                                                                            INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                                                                                            INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                                                                                            LEFT JOIN  arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                                                                                            LEFT JOIN  dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                                                                                            WHERE      LTRIM (RTRIM (t2.CampusId)) = LTRIM (RTRIM (@CampusId))
                                                                                                       AND (
                                                                                                           @ProgId IS NULL
                                                                                                           OR t8.ProgId IN (
                                                                                                                           SELECT Val
                                                                                                                           FROM   MultipleValuesForReportParameters (
                                                                                                                                                                        @ProgId
                                                                                                                                                                       ,',', 1
                                                                                                                                                                    )
                                                                                                                           )
                                                                                                           )
                                                                                                       AND t10.IPEDSValue = 61
                                                                                                       AND -- Full Time      
                                                                                                t12.IPEDSValue = 11
                                                                                                       AND -- First Time      
                                                                                                t9.IPEDSValue = 58
                                                                                                       AND -- Under Graduate      
                                                                                                t2.StudentId = t1.StudentId
                                                                                                       AND (
                                                                                                           t2.StartDate >= @StartDate
                                                                                                           AND t2.StartDate <= @EndDate
                                                                                                           )
                                                                                            )
                                                             );
            END;




        --SELECT * FROM #StudentsList  --WHERE lastname='Diognardi'    

        SELECT   NEWID () AS RowNumber
                ,dbo.UDF_FormatSSN (SSN) AS SSN
                ,StudentNumber
                ,LastName + ', ' + FirstName + ' ' + ISNULL (MiddleName, '') AS StudentName
                ,1 AS RevisedCohort
                ,Exclusions
                ,CASE WHEN Exclusions >= 1 THEN 1
                      ELSE 0
                 END AS ExclusionsCount
                ,dbo.CompletedProgramin150PercentofProgramLength (StuENrollId, 'D') AS CompletedProgramin150PercentofProgramLength
                ,dbo.CompletedProgramin150PercentofProgramLength (StuENrollId, 'O') AS CompletedProgramin150PercentofProgramLengthForOthers
                ,dbo.CompletedProgramin150PercentofProgramLength (StuENrollId, 'A') AS CompletedProgramin150PercentofProgramLengthForAllDegree
                ,dbo.CompletedProgramin150PercentofProgramLength (StuENrollId, 'B') AS CompletedProgramin150PercentofProgramLengthForBachelor
        FROM     #StudentsList dt
        ORDER BY CASE WHEN @OrderBy = 'SSN' THEN dt.SSN
                 END
                ,CASE WHEN @OrderBy = 'LastName' THEN dt.LastName
                 END
                ,CASE WHEN @OrderBy = 'StudentNumber' THEN CONVERT (INT, dt.StudentNumber)
                 END;




        DROP TABLE #GraduationRate;
        DROP TABLE #StudentsList;
    END;
-- =========================================================================================================  
-- usp_GradRatesStafford_Detail_GetList     
-- =========================================================================================================  

GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO






IF @@ERROR <> 0
    SET NOEXEC ON;
GO
COMMIT TRANSACTION;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
DECLARE @Success AS BIT;
SET @Success = 1;
SET NOEXEC OFF;
IF (@Success = 1)
    PRINT 'The database update succeeded';
ELSE
    BEGIN
        IF @@TRANCOUNT > 0
            ROLLBACK TRANSACTION;
        PRINT 'The database update failed';
    END;
GO
-- =========================================================================================================
-- END  --  Synchronize_3.9SP2_with_3.9SP3.sql
-- =========================================================================================================
