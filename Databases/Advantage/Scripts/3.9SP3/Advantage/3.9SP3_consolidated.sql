-- ===============================================================================================
-- Consolidated Script Version 3.9SP3
-- Data Changes Zone
-- Please do not deploy your schema changes in this area
-- Please use SQL Prompt format before insert here please
-- ===============================================================================================
-- ===============================================================================================
-- START  --   AD-2180 Update Agency Values for Ipeds Test
-- ===============================================================================================
BEGIN TRANSACTION;
BEGIN TRY
    IF EXISTS (
              SELECT 1
              FROM   syRptAgencyFldValues
              WHERE  RptAgencyFldId = 39
                     AND AgencyDescrip = 'SAT - Critical Reading'
              )
        BEGIN
            UPDATE syRptAgencyFldValues
            SET    AgencyDescrip = 'SAT - Evidence Based Reading and Writing'
            WHERE  RptAgencyFldId = 39
                   AND AgencyDescrip = 'SAT - Critical Reading';
        END;
    IF EXISTS (
              SELECT 1
              FROM   syRptAgencyFldValues
              WHERE  RptAgencyFldId = 39
                     AND AgencyDescrip = 'SAT - Writing'
              )
        BEGIN
            DELETE FROM syRptAgencyFldValues
            WHERE RptAgencyFldId = 39
                  AND AgencyDescrip = 'SAT - Writing';
        END;
    IF EXISTS (
              SELECT 1
              FROM   syRptAgencyFldValues
              WHERE  RptAgencyFldId = 39
                     AND AgencyDescrip = 'ACT - Writing'
              )
        BEGIN
            DELETE FROM syRptAgencyFldValues
            WHERE RptAgencyFldId = 39
                  AND AgencyDescrip = 'ACT - Writing';
        END;
END TRY
BEGIN CATCH
    DECLARE @ErrorMessage NVARCHAR(MAX)
           ,@ErrorSeverity INT
           ,@ErrorState INT;
    SELECT @ErrorMessage = ERROR_MESSAGE() + ' Line ' + CAST(ERROR_LINE() AS NVARCHAR(5))
          ,@ErrorSeverity = ERROR_SEVERITY()
          ,@ErrorState = ERROR_STATE();
    IF ( @@TRANCOUNT > 0 )
        BEGIN
            ROLLBACK TRANSACTION;
        END;
    RAISERROR(@ErrorMessage, @ErrorSeverity, @ErrorState);
END CATCH;
COMMIT TRANSACTION;
GO
-- ===============================================================================================
-- END  --   AD-2180 Update Agency Values for Ipeds Test
-- ===============================================================================================
-- ===============================================================================================
-- START AD-2483:QA: Test Scores Detail/Summary and Pell Recipients & Stafford Sub Recipients without Pell reports are not access for Report user.
-- ===============================================================================================
BEGIN TRANSACTION;
BEGIN TRY
    DECLARE @winterResourceId INT;
    DECLARE @testResourceId INT;
    DECLARE @hierarcyId UNIQUEIDENTIFIER;
    DECLARE @parentResourceID INT;
    SET @testResourceId = 0;
    SET @winterResourceId = 0;
    SET @winterResourceId = (
                            SELECT ResourceID
                            FROM   syResources
                            WHERE  Resource = 'Winter - Admissions'
                                   AND ResourceURL IS NULL
                            );
    SET @testResourceId = (
                          SELECT ResourceID
                          FROM   syResources
                          WHERE  Resource = 'All Inst - Test Scores - Detail'
                          );
    SET @hierarcyId = (
                      SELECT ParentId
                      FROM   syNavigationNodes
                      WHERE  ResourceId = @testResourceId
                      );
    IF @winterResourceId > 0
        BEGIN
            SET @parentResourceID = (
                                    SELECT ResourceId
                                    FROM   syNavigationNodes
                                    WHERE  HierarchyId = @hierarcyId
                                    );
            IF @parentResourceID <> @winterResourceId
                BEGIN
                    -- for both the test reports to show under winter admissions in manage security
                    UPDATE syNavigationNodes
                    SET    ResourceId = @winterResourceId
                    WHERE  HierarchyId = @hierarcyId;
                END;
        END;
    -- start adding the entry for Pell report
    DECLARE @PellResourceId INT;
    SET @PellResourceId = (
                          SELECT ResourceID
                          FROM   syResources
                          WHERE  Resource = 'Pell Recipients & Stafford Sub Recipients without Pell'
                          );
    IF NOT EXISTS (
                  SELECT 1
                  FROM   syNavigationNodes
                  WHERE  ResourceId = @PellResourceId
                  )
        BEGIN
            -- insert

            INSERT INTO syNavigationNodes (
                                          HierarchyId
                                         ,HierarchyIndex
                                         ,ResourceId
                                         ,ParentId
                                         ,ModUser
                                         ,ModDate
                                         ,IsPopupWindow
                                         ,IsShipped
                                          )
            VALUES ( NEWID()         -- HierarchyId - uniqueidentifier
                    ,4               -- HierarchyIndex - smallint
                    ,@PellResourceId -- ResourceId - smallint
                    ,@hierarcyId     -- ParentId - uniqueidentifier
                    ,'sa'            -- ModUser - varchar(50)
                    ,GETDATE()       -- ModDate - datetime
                    ,0               -- IsPopupWindow - bit
                    ,1               -- IsShipped - bit
                   );
        END;
    DECLARE @outcomeResourceId INT;
    DECLARE @parentId UNIQUEIDENTIFIER;
    DECLARE @WinterFinAidResourceId INT;
    SET @WinterFinAidResourceId = (
                                  SELECT ResourceID
                                  FROM   syResources
                                  WHERE  Resource = 'Winter - Student Financial Aid Reports'
                                  );
    SET @outcomeResourceId = (
                             SELECT ResourceID
                             FROM   syResources
                             WHERE  Resource = 'Outcome Measures'
                             );
    SET @parentId = (
                    SELECT HierarchyId
                    FROM   syNavigationNodes
                    WHERE  ResourceId = @WinterFinAidResourceId
                    );
    IF NOT EXISTS (
                  SELECT 1
                  FROM   syNavigationNodes
                  WHERE  ResourceId = @outcomeResourceId
                  )
        BEGIN
            INSERT INTO syNavigationNodes (
                                          HierarchyId
                                         ,HierarchyIndex
                                         ,ResourceId
                                         ,ParentId
                                         ,ModUser
                                         ,ModDate
                                         ,IsPopupWindow
                                         ,IsShipped
                                          )
            VALUES ( NEWID()            -- HierarchyId - uniqueidentifier
                    ,4                  -- HierarchyIndex - smallint
                    ,@outcomeResourceId -- ResourceId - smallint
                    ,@parentId          -- ParentId - uniqueidentifier
                    ,'sa'               -- ModUser - varchar(50)
                    ,GETDATE()          -- ModDate - datetime
                    ,0                  -- IsPopupWindow - bit
                    ,1                  -- IsShipped - bit
                   );
        END;
END TRY
BEGIN CATCH
    DECLARE @ErrorMessage NVARCHAR(MAX)
           ,@ErrorSeverity INT
           ,@ErrorState INT;
    SELECT @ErrorMessage = ERROR_MESSAGE() + ' Line ' + CAST(ERROR_LINE() AS NVARCHAR(5))
          ,@ErrorSeverity = ERROR_SEVERITY()
          ,@ErrorState = ERROR_STATE();
    IF ( @@TRANCOUNT > 0 )
        BEGIN
            ROLLBACK TRANSACTION;
        END;
    RAISERROR(@ErrorMessage, @ErrorSeverity, @ErrorState);
END CATCH;
COMMIT TRANSACTION;
GO
-- ===============================================================================================
-- END AD-2483:QA: Test Scores Detail/Summary and Pell Recipients & Stafford Sub Recipients without Pell reports are not access for Report user.
-- ===============================================================================================
-- ===============================================================================================
-- END  --   Consolidated Script Version 3.9SP3
-- ===============================================================================================