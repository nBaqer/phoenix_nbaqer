-- ===============================================================================================
-- START Consolidated Script Version 4.2
-- Data Changes Zone
-- Please do not deploy your schema changes in this area
-- Please use SQL Prompt format before insert here please
-- Please run after Schema changes....
-- ===============================================================================================
--=================================================================================================
-- START AD-8846 Restrict pay periods per academic year to 2 incase of Non-term and Clock hour Title IV programs	
--=================================================================================================
DECLARE @Error AS INT;
SET @Error = 0;
BEGIN TRANSACTION RestrictPayPeriods;
BEGIN TRY


    IF EXISTS (
              SELECT     pv.PrgVerId
              FROM       dbo.arPrgVersions pv
              INNER JOIN dbo.arPrograms pr ON pr.ProgId = pv.ProgId
              INNER JOIN dbo.arCampusPrgVersions cpv ON cpv.PrgVerId = pv.PrgVerId
              WHERE      (
                         pr.ACId = 6
                         OR pr.ACId = 5
                         )
                         AND cpv.IsTitleIV = 1
                         AND pv.PayPeriodPerAcYear > 2
              )
        BEGIN

            UPDATE dbo.arPrgVersions
            SET    PayPeriodPerAcYear = 2
            WHERE  PrgVerId IN (
                               SELECT     pv.PrgVerId
                               FROM       dbo.arPrgVersions pv
                               INNER JOIN dbo.arPrograms pr ON pr.ProgId = pv.ProgId
                               INNER JOIN dbo.arCampusPrgVersions cpv ON cpv.PrgVerId = pv.PrgVerId
                               WHERE      (
                                          pr.ACId = 6
                                          OR pr.ACId = 5
                                          )
                                          AND cpv.IsTitleIV = 1
                                          AND pv.PayPeriodPerAcYear > 2
                               );
        END;



END TRY
BEGIN CATCH
    SET @Error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE()
          ,@severity = ERROR_SEVERITY()
          ,@state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @Error > 0
    BEGIN
        ROLLBACK TRANSACTION RestrictPayPeriods;
        PRINT 'Failed transcation RestrictPayPeriods.';

    END;
ELSE
    BEGIN
        COMMIT TRANSACTION RestrictPayPeriods;
        PRINT 'successful transaction RestrictPayPeriods.';
    END;
GO
--=================================================================================================
-- END AD-8846 Restrict pay periods per academic year to 2 incase of Non-term and Clock hour Title IV programs
--=================================================================================================
-- ===============================================================================================
-- END Consolidated Script Version 4.2
-- ===============================================================================================
