-- ===============================================================================================
-- START Consolidated Script Version 4.1SP3
-- Data Changes Zone
-- Please do not deploy your schema changes in this area
-- Please use SQL Prompt format before insert here please
-- Please run after Schema changes....
-- ===============================================================================================
--=================================================================================================
-- START  AD-10019 : Charging Method by Payment Period - Allow more than one charge per period and choose transaction code for each charge.
--=================================================================================================
DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION TransCodeId;
BEGIN TRY
    DECLARE @transCodeId UNIQUEIDENTIFIER;
    SET @transCodeId = (
                       SELECT     TOP 1 t.TransCodeId
                       FROM       dbo.saTransactions t
                       INNER JOIN dbo.saPmtPeriods p ON p.PmtPeriodId = t.PmtPeriodId
                       WHERE      t.TransCodeId IS NOT NULL
                       );

    UPDATE dbo.saPmtPeriods
    SET    TransactionCodeId = @transCodeId
    WHERE  TransactionCodeId IS NULL;
END TRY
BEGIN CATCH
    SET @error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE()
          ,@severity = ERROR_SEVERITY()
          ,@state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
    BEGIN
        ROLLBACK TRANSACTION TransCodeId;
        PRINT 'Failed to update TransCodeId for saPmtPeriods';
    END;
ELSE
    BEGIN
        COMMIT TRANSACTION TransCodeId;
        PRINT 'updated TransCodeId for saPmtPeriods';
    END;
GO
--=================================================================================================
-- END  AD-10019 : Charging Method by Payment Period - Allow more than one charge per period and choose transaction code for each charge.
--=================================================================================================
--=================================================================================================
-- START  AD-16304 : Regulatory: Incorrect Calculation of Withdrawal Payment Period.
--=================================================================================================
DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION schDetails;
BEGIN TRY
    UPDATE dbo.arProgScheduleDetails
    SET    dw = 0
    WHERE  dw = 7;
END TRY
BEGIN CATCH
    SET @error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE()
          ,@severity = ERROR_SEVERITY()
          ,@state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
    BEGIN
        ROLLBACK TRANSACTION schDetails;
        PRINT 'Failed to update schDetails';
    END;
ELSE
    BEGIN
        COMMIT TRANSACTION schDetails;
        PRINT 'updated schDetails';
    END;
GO
--=================================================================================================
-- END  AD-16304 : Regulatory: Incorrect Calculation of Withdrawal Payment Period.
--=================================================================================================
--=================================================================================================
-- START AD-17410 : Chat - Missing Tennessee State Board Report
--=================================================================================================
DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION AD17410;
BEGIN TRY
    UPDATE dbo.syStates
    SET    IsStateBoard = 1
    WHERE  StateCode = 'TN';

    IF NOT EXISTS (
                  SELECT *
                  FROM   dbo.syStateReports
                  WHERE  StateId = (
                                   SELECT StateId
                                   FROM   dbo.syStates
                                   WHERE  StateCode = 'TN'
                                   )
                  )
        BEGIN
            INSERT INTO dbo.syStateReports (
                                           StateReportId
                                          ,StateId
                                          ,ReportId
                                          ,ModifiedUser
                                          ,ModifiedDate
                                           )
            VALUES ( NEWID()   -- StateReportId - uniqueidentifier
                    ,(
                     SELECT TOP 1 StateId
                     FROM   dbo.syStates
                     WHERE  StateCode = 'TN'
                     )         -- StateId - uniqueidentifier
                    ,(
                     SELECT ReportId
                     FROM   dbo.syReports
                     WHERE  RDLName = 'StateBoard/TennesseeStateBoard'
                     )         -- ReportId - uniqueidentifier
                    ,'sa'      -- ModifiedUser - varchar(50)
                    ,GETDATE() -- ModifiedDate - datetime
                );
        END;
END TRY
BEGIN CATCH
    SET @error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE()
          ,@severity = ERROR_SEVERITY()
          ,@state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
    BEGIN
        ROLLBACK TRANSACTION AD17410;
        PRINT 'Failed to update AD17410';
    END;
ELSE
    BEGIN
        COMMIT TRANSACTION AD17410;
        PRINT 'updated AD17410';
    END;
GO
--=================================================================================================
-- END AD-17410 : Chat - Missing Tennessee State Board Report
--=================================================================================================
--=================================================================================================
-- START AD-17374 logrocket
--=================================================================================================
DECLARE @AD17374 AS INT;
SET @AD17374 = 0;
BEGIN TRANSACTION AD17374;
BEGIN TRY

    DECLARE @settingKeyName VARCHAR(MAX) = 'EnableLogRocketRecording';
    IF NOT EXISTS (
                  SELECT 1
                  FROM   dbo.syConfigAppSettings
                  WHERE  KeyName = @settingKeyName
                  )
        BEGIN
            INSERT INTO dbo.syConfigAppSettings (
                                                KeyName
                                               ,Description
                                               ,ModUser
                                               ,ModDate
                                               ,CampusSpecific
                                               ,ExtraConfirmation
                                                )
            VALUES ( @settingKeyName                                                                               -- KeyName - varchar(200)
                    ,'Enable(Yes) or Disabled(No) the option to allow recording of user actions using LogRocket .' -- Description - varchar(1000)
                    ,'sa'                                                                                          -- ModUser - varchar(50)
                    ,GETDATE()                                                                                     -- ModDate - datetime
                    ,1                                                                                             -- CampusSpecific - bit
                    ,0                                                                                             -- ExtraConfirmation - bit
                );


            DECLARE @logRocketRecordingId INT = (
                                                SELECT TOP 1 SettingId
                                                FROM   dbo.syConfigAppSettings
                                                WHERE  KeyName = @settingKeyName
                                                );

            INSERT INTO syConfigAppSet_Lookup (
                                              LookUpId
                                             ,SettingId
                                             ,ValueOptions
                                             ,ModUser
                                             ,ModDate
                                              )
            VALUES ( NEWID()               -- LookUpId - uniqueidentifier
                    ,@logRocketRecordingId -- SettingId - int
                    ,'No'                  -- ValueOptions - varchar(50)
                    ,'Support'             -- ModUser - varchar(50)
                    ,GETDATE()             -- ModDate - datetime
                );
            INSERT INTO syConfigAppSet_Lookup (
                                              LookUpId
                                             ,SettingId
                                             ,ValueOptions
                                             ,ModUser
                                             ,ModDate
                                              )
            VALUES ( NEWID()               -- LookUpId - uniqueidentifier
                    ,@logRocketRecordingId -- SettingId - int
                    ,'Yes'                 -- ValueOptions - varchar(50)
                    ,'Support'             -- ModUser - varchar(50)
                    ,GETDATE()             -- ModDate - datetime
                );

            IF NOT EXISTS (
                          SELECT 1
                          FROM   dbo.syConfigAppSetValues
                          WHERE  SettingId = @logRocketRecordingId
                          )
                BEGIN
                    INSERT INTO dbo.syConfigAppSetValues (
                                                         ValueId
                                                        ,SettingId
                                                        ,CampusId
                                                        ,Value
                                                        ,ModUser
                                                        ,ModDate
                                                        ,Active
                                                         )
                    VALUES ( NEWID()               -- ValueId - uniqueidentifier
                            ,@logRocketRecordingId -- SettingId - int
                            ,NULL                  -- CampusId - uniqueidentifier
                            ,'No'                  -- Value - varchar(1000)
                            ,'SA'                  -- ModUser - varchar(50)
                            ,GETDATE()             -- ModDate - datetime
                            ,1                     -- Active - bit
                        );
                END;
        END;
END TRY
BEGIN CATCH
    SET @AD17374 = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE()
          ,@severity = ERROR_SEVERITY()
          ,@state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @AD17374 > 0
    BEGIN
        ROLLBACK TRANSACTION AD17374;
        PRINT 'Failed transcation log_rocketTran.';

    END;
ELSE
    BEGIN
        COMMIT TRANSACTION AD17374;
        PRINT 'successful transaction log_rocketTran.';
    END;
GO
--=================================================================================================
-- END AD-17374 logrocket
--=================================================================================================
--=================================================================================================
-- START tech: fixing completedHr and totalHour SQL for adhoc
--=================================================================================================
DECLARE @adhocError AS INT;
SET @adhocError = 0;
BEGIN TRANSACTION completedHrAdh;
BEGIN TRY

    DECLARE @completeHrsFldId INT;
    SET @completeHrsFldId = (
                            SELECT FldId
                            FROM   dbo.syFields
                            WHERE  FldName = 'CompletedHours'
                            );
    UPDATE dbo.syFieldCalculation
    SET    CalculationSql = ' ((  SELECT     TOP 1 ( CASE WHEN ( LOWER(LTRIM(RTRIM(dbo.GetAppSettingValueByKeyName(''TrackSapAttendance'', arStuEnrollments.CampusId)))) = ''byclass'' AND LOWER(LTRIM(RTRIM(dbo.GetAppSettingValueByKeyName( ''DisplayAttendanceUnitForProgressReportByClass''
    ,arStuEnrollments.CampusId  ) ) )) = ''hours'' AND AAUT1.UnitTypeDescrip = ''Present Absent'') THEN ( SELECT ISNULL(SUM(ActualPresentDays_ConvertTo_Hours),0) + ISNULL(SAS_ForTerm.MakeupDays,0) FROM   syStudentAttendanceSummary
   WHERE  StuEnrollId = arStuEnrollments.StuEnrollId ) ELSE CASE WHEN (  LOWER(LTRIM(RTRIM(dbo.GetAppSettingValueByKeyName(''TrackSapAttendance'', arStuEnrollments.CampusId)))) = ''byclass''    AND LOWER(LTRIM(RTRIM(dbo.GetAppSettingValueByKeyName(
    ''DisplayAttendanceUnitForProgressReportByClass'' ,arStuEnrollments.CampusId ) )  )  ) = ''hours''   AND AAUT1.UnitTypeDescrip = ''Minutes'') THEN CASE WHEN ( SELECT     TOP 1 EndDate  FROM       arTerm art  INNER JOIN arPrgVersions arp ON arp.PrgVerId = art.ProgramVersionId
   WHERE      PrgVerId = arStuEnrollments.PrgVerId ) IS NULL THEN ( ISNULL(SAS_NoTerm.AttendedDays,0) / 60 ) + ( ISNULL(SAS_NoTerm.MakeupDays,0) / 60 ) ELSE ( ISNULL(SAS_ForTerm.AttendedDays,0) / 60 ) + ( ISNULL(SAS_ForTerm.MakeupDays,0) / 60 )
    END  WHEN ( LOWER(LTRIM(RTRIM(dbo.GetAppSettingValueByKeyName(''TrackSapAttendance'', arStuEnrollments.CampusId)))) = ''byclass'' AND LOWER(LTRIM(RTRIM(dbo.GetAppSettingValueByKeyName(''DisplayAttendanceUnitForProgressReportByClass''
     ,arStuEnrollments.CampusId ) ) )) = ''hours'' AND AAUT1.UnitTypeDescrip = ''Clock Hours''  ) THEN CASE WHEN ( SELECT     TOP 1 EndDate FROM       arTerm art INNER JOIN arPrgVersions arp ON arp.PrgVerId = art.ProgramVersionId
   WHERE      PrgVerId = arStuEnrollments.PrgVerId  ) IS NULL THEN ( ISNULL(SAS_NoTerm.AttendedDays,0) / 60 ) + ( ISNULL(SAS_NoTerm.MakeupDays,0) / 60 ) ELSE ( ISNULL(SAS_ForTerm.AttendedDays,0) / 60 ) + ( ISNULL(SAS_ForTerm.MakeupDays,0) / 60 )
  END  WHEN ( LOWER(LTRIM(RTRIM(dbo.GetAppSettingValueByKeyName(''TrackSapAttendance'', arStuEnrollments.CampusId)))) = ''byclass'' AND LOWER(LTRIM(RTRIM(dbo.GetAppSettingValueByKeyName( ''DisplayAttendanceUnitForProgressReportByClass''
   ,arStuEnrollments.CampusId )  ) )  ) = ''presentabsent'' AND AAUT1.UnitTypeDescrip = ''Clock Hours'' ) THEN CASE WHEN ( SELECT     TOP 1 EndDate  FROM       arTerm art INNER JOIN arPrgVersions arp ON arp.PrgVerId = art.ProgramVersionId
   WHERE      PrgVerId = arStuEnrollments.PrgVerId ) IS NULL THEN ( ISNULL(SAS_NoTerm.AttendedDays,0) / 60 ) + ( ISNULL(SAS_NoTerm.MakeupDays,0) / 60 ) ELSE ( ISNULL(SAS_ForTerm.AttendedDays,0) / 60 ) + ( ISNULL(SAS_ForTerm.MakeupDays,0) / 60 )
  END WHEN (  LOWER(LTRIM(RTRIM(dbo.GetAppSettingValueByKeyName(''TrackSapAttendance'', arStuEnrollments.CampusId)))) = ''byclass'' AND LOWER(LTRIM(RTRIM(dbo.GetAppSettingValueByKeyName( ''DisplayAttendanceUnitForProgressReportByClass''
    ,arStuEnrollments.CampusId  ) ))) = ''presentabsent'' AND AAUT1.UnitTypeDescrip = ''Minutes'' ) THEN CASE WHEN ( SELECT     TOP 1 EndDate  FROM       arTerm art INNER JOIN arPrgVersions arp ON arp.PrgVerId = art.ProgramVersionId
  WHERE      PrgVerId = arStuEnrollments.PrgVerId ) IS NULL THEN ( ISNULL(SAS_NoTerm.AttendedDays,0) / 60 ) + ( ISNULL(SAS_NoTerm.MakeupDays,0) / 60 ) ELSE ( ISNULL(SAS_ForTerm.AttendedDays,0) / 60 ) + ( ISNULL(SAS_ForTerm.MakeupDays,0) / 60 )
    END ELSE  CASE WHEN ( SELECT     TOP 1 EndDate FROM       arTerm art INNER JOIN arPrgVersions arp ON arp.PrgVerId = art.ProgramVersionId WHERE      PrgVerId = arStuEnrollments.PrgVerId ) IS NULL THEN ( ISNULL(SAS_NoTerm.AttendedDays,0) ) + ( ISNULL(SAS_NoTerm.MakeupDays,0) )
   ELSE ( ISNULL(SAS_ForTerm.AttendedDays,0) ) + ( ISNULL(SAS_ForTerm.MakeupDays,0) ) END END END  ) FROM       arAttUnitType AAUT1 INNER JOIN arPrgVersions arp ON arp.UnitTypeId = AAUT1.UnitTypeId  LEFT JOIN  ( SELECT   StuEnrollId
,MAX(ActualRunningScheduledDays) AS ScheduledDays ,MAX(ActualRunningPresentDays) AS AttendedDays ,MAX(AdjustedAbsentDays) AS AbsentDays ,MAX(ActualRunningMakeupDays) AS MakeupDays,MAX(StudentAttendedDate) AS LDA  FROM     syStudentAttendanceSummary
  GROUP BY StuEnrollId ) SAS_NoTerm ON arStuEnrollments.StuEnrollId = SAS_NoTerm.StuEnrollId LEFT JOIN  ( SELECT   StuEnrollId ,MAX(ActualRunningScheduledDays) AS ScheduledDays ,MAX(ActualRunningPresentDays) AS AttendedDays
 ,MAX(AdjustedAbsentDays) AS AbsentDays ,MAX(ActualRunningMakeupDays) AS MakeupDays ,MAX(StudentAttendedDate) AS LDA  FROM     syStudentAttendanceSummary GROUP BY StuEnrollId  ) SAS_ForTerm ON arStuEnrollments.StuEnrollId = SAS_ForTerm.StuEnrollId )   ) AS CompletedHours '
    WHERE  FldId = @completeHrsFldId;


    DECLARE @totalHrsFldId INT;
    SET @totalHrsFldId = (
                         SELECT FldId
                         FROM   dbo.syFields
                         WHERE  FldName = 'TotalHours'
                         );
    UPDATE dbo.syFieldCalculation
    SET    CalculationSql = ' ( ISNULL((SELECT     TOP 1 ( CASE WHEN (LOWER(LTRIM(RTRIM(dbo.GetAppSettingValueByKeyName(''TrackSapAttendance'', arStuEnrollments.CampusId)))) = ''byclass''    AND LOWER(LTRIM(RTRIM(dbo.GetAppSettingValueByKeyName(
   ''DisplayAttendanceUnitForProgressReportByClass'' ,arStuEnrollments.CampusId  ) ) ) ) = ''hours'' AND AAUT1.UnitTypeDescrip = ''Present Absent''  ) THEN (  SELECT SUM(ActualPresentDays_ConvertTo_Hours) + SAS_ForTerm.MakeupDays
  FROM   syStudentAttendanceSummary WHERE  StuEnrollId = arStuEnrollments.StuEnrollId ) ELSE CASE WHEN ( LOWER(LTRIM(RTRIM(dbo.GetAppSettingValueByKeyName(''TrackSapAttendance'', arStuEnrollments.CampusId)))) = ''byclass''
  AND LOWER(LTRIM(RTRIM(dbo.GetAppSettingValueByKeyName( ''DisplayAttendanceUnitForProgressReportByClass'' ,arStuEnrollments.CampusId    )  )  )  ) = ''hours''  AND AAUT1.UnitTypeDescrip = ''Minutes''   ) THEN  CASE WHEN (
    SELECT     TOP 1 EndDate FROM       arTerm art INNER JOIN arPrgVersions arp ON arp.PrgVerId = art.ProgramVersionId WHERE      PrgVerId = arStuEnrollments.PrgVerId ) IS NULL THEN ( ISNULL(SAS_NoTerm.AttendedDays,0) / 60 ) + ( ISNULL(SAS_NoTerm.MakeupDays,0) / 60 )
   ELSE ( ISNULL(SAS_ForTerm.AttendedDays,0) / 60 ) + ( ISNULL(SAS_ForTerm.MakeupDays,0) / 60 )  END WHEN (  LOWER(LTRIM(RTRIM(dbo.GetAppSettingValueByKeyName(''TrackSapAttendance'', arStuEnrollments.CampusId)))) = ''byclass''
     AND LOWER(LTRIM(RTRIM(dbo.GetAppSettingValueByKeyName(''DisplayAttendanceUnitForProgressReportByClass'' ,arStuEnrollments.CampusId ) ) ) ) = ''hours'' AND AAUT1.UnitTypeDescrip = ''Clock Hours''  ) THEN  CASE WHEN ( SELECT     TOP 1 EndDate
   FROM       arTerm art INNER JOIN arPrgVersions arp ON arp.PrgVerId = art.ProgramVersionId   WHERE      PrgVerId = arStuEnrollments.PrgVerId ) IS NULL THEN ( ISNULL(SAS_NoTerm.AttendedDays,0) / 60 ) + ( ISNULL(SAS_NoTerm.MakeupDays,0) / 60 )
  ELSE ( ISNULL(SAS_ForTerm.AttendedDays,0) / 60 ) + ( ISNULL(SAS_ForTerm.MakeupDays,0) / 60 ) END WHEN ( LOWER(LTRIM(RTRIM(dbo.GetAppSettingValueByKeyName(''TrackSapAttendance'', arStuEnrollments.CampusId)))) = ''byclass''
  AND LOWER(LTRIM(RTRIM(dbo.GetAppSettingValueByKeyName(  ''DisplayAttendanceUnitForProgressReportByClass'' ,arStuEnrollments.CampusId ) ) ) ) = ''presentabsent'' AND AAUT1.UnitTypeDescrip = ''Clock Hours'' ) THEN
   CASE WHEN ( SELECT     TOP 1 EndDate FROM       arTerm art INNER JOIN arPrgVersions arp ON arp.PrgVerId = art.ProgramVersionId WHERE      PrgVerId = arStuEnrollments.PrgVerId ) IS NULL THEN ( ISNULL(SAS_NoTerm.AttendedDays,0) / 60 ) + ( ISNULL(SAS_NoTerm.MakeupDays,0) / 60 )
   ELSE ( ISNULL(SAS_ForTerm.AttendedDays,0) / 60 ) + ( ISNULL(SAS_ForTerm.MakeupDays,0) / 60 ) END WHEN (   LOWER(LTRIM(RTRIM(dbo.GetAppSettingValueByKeyName(''TrackSapAttendance'', arStuEnrollments.CampusId)))) = ''byclass''
       AND LOWER(LTRIM(RTRIM(dbo.GetAppSettingValueByKeyName( ''DisplayAttendanceUnitForProgressReportByClass'' ,arStuEnrollments.CampusId )) )  ) = ''presentabsent'' AND AAUT1.UnitTypeDescrip = ''Minutes'' ) THEN  CASE WHEN (
     SELECT     TOP 1 EndDate FROM       arTerm art INNER JOIN arPrgVersions arp ON arp.PrgVerId = art.ProgramVersionId WHERE      PrgVerId = arStuEnrollments.PrgVerId ) IS NULL THEN ( ISNULL(SAS_NoTerm.AttendedDays,0) / 60 ) + ( ISNULL(SAS_NoTerm.MakeupDays,0) / 60 )
   ELSE ( ISNULL(SAS_ForTerm.AttendedDays,0) / 60 ) + ( ISNULL(SAS_ForTerm.MakeupDays,0) / 60 ) END  ELSE  CASE WHEN ( SELECT     TOP 1 EndDate  FROM       arTerm art  INNER JOIN arPrgVersions arp ON arp.PrgVerId = art.ProgramVersionId
    WHERE      PrgVerId = arStuEnrollments.PrgVerId ) IS NULL THEN ( ISNULL(SAS_NoTerm.AttendedDays,0) ) + ( ISNULL(SAS_NoTerm.MakeupDays,0) ) ELSE ( ISNULL(SAS_ForTerm.AttendedDays,0) ) + ( ISNULL(SAS_ForTerm.MakeupDays,0) )
   END END END  ) FROM       arAttUnitType AAUT1 INNER JOIN arPrgVersions arp ON arp.UnitTypeId = AAUT1.UnitTypeId LEFT JOIN  (  SELECT   StuEnrollId ,MAX(ActualRunningScheduledDays) AS ScheduledDays,MAX(ActualRunningPresentDays) AS AttendedDays
  ,MAX(AdjustedAbsentDays) AS AbsentDays ,MAX(ActualRunningMakeupDays) AS MakeupDays,MAX(StudentAttendedDate) AS LDA FROM     syStudentAttendanceSummary GROUP BY StuEnrollId ) SAS_NoTerm ON arStuEnrollments.StuEnrollId = SAS_NoTerm.StuEnrollId
   LEFT JOIN  ( SELECT   StuEnrollId ,MAX(ActualRunningScheduledDays) AS ScheduledDays ,MAX(ActualRunningPresentDays) AS AttendedDays ,MAX(AdjustedAbsentDays) AS AbsentDays ,MAX(ActualRunningMakeupDays) AS MakeupDays,MAX(StudentAttendedDate) AS LDA
   FROM     syStudentAttendanceSummary GROUP BY StuEnrollId ) SAS_ForTerm ON arStuEnrollments.StuEnrollId = SAS_ForTerm.StuEnrollId ) ,0  ) + ISNULL(arStuEnrollments.TransferHours, 0) ) AS TotalHours '
    WHERE  FldId = @totalHrsFldId;


DECLARE @remainHrsFldId INT;
SET @remainHrsFldId = (
                      SELECT FldId
                      FROM   dbo.syFields
                      WHERE  FldName = 'RemainingHours'
                      );
UPDATE dbo.syFieldCalculation
SET    CalculationSql = ' (SELECT IIF(Remainin > 0, Remainin, ValueIfNegative) FROM   ( SELECT (( SELECT ISNULL(arPrgVersions.Hours, 0) FROM   arPrgVersions WHERE  arPrgVersions.PrgVerId = arStuEnrollments.PrgVerId  ) - (ISNULL(arStuEnrollments.TransferHours, 0)) - ( ISNULL( ( SELECT     TOP 1 ( CASE WHEN (LOWER(LTRIM(RTRIM(dbo.GetAppSettingValueByKeyName(''TrackSapAttendance'', arStuEnrollments.CampusId)))) = ''byclass''  AND LOWER(LTRIM(RTRIM(dbo.GetAppSettingValueByKeyName(
  ''DisplayAttendanceUnitForProgressReportByClass'',arStuEnrollments.CampusId ) ) ) ) = ''hours'' AND AAUT1.UnitTypeDescrip = ''Present Absent'' ) THEN ( SELECT SUM(ISNULL(ActualPresentDays_ConvertTo_Hours,0)) + ISNULL(SAS_ForTerm.MakeupDays,0)  FROM   syStudentAttendanceSummary WHERE  StuEnrollId = arStuEnrollments.StuEnrollId  ) ELSE CASE WHEN ( LOWER(LTRIM(RTRIM(dbo.GetAppSettingValueByKeyName(  ''TrackSapAttendance''  ,arStuEnrollments.CampusId  ) ) ) )
  = ''byclass'' AND LOWER(LTRIM(RTRIM(dbo.GetAppSettingValueByKeyName( ''DisplayAttendanceUnitForProgressReportByClass'' ,arStuEnrollments.CampusId    ) ) ) ) = ''hours'' AND AAUT1.UnitTypeDescrip = ''Minutes'' ) THEN CASE WHEN ( SELECT     TOP 1 EndDate FROM       arTerm art  INNER JOIN arPrgVersions arp ON arp.PrgVerId = art.ProgramVersionId   WHERE      PrgVerId = arStuEnrollments.PrgVerId ) IS NULL 
  THEN ( ISNULL(SAS_NoTerm.AttendedDays,0) / 60 ) + ( ISNULL(SAS_NoTerm.MakeupDays,0) / 60 ) ELSE ( ISNULL(SAS_ForTerm.AttendedDays,0) / 60 ) + ( ISNULL(SAS_ForTerm.MakeupDays,0) / 60 )
   END WHEN ( LOWER(LTRIM(RTRIM(dbo.GetAppSettingValueByKeyName( ''TrackSapAttendance''  ,arStuEnrollments.CampusId ) )  ) ) = ''byclass'' AND LOWER(LTRIM(RTRIM(dbo.GetAppSettingValueByKeyName(    ''DisplayAttendanceUnitForProgressReportByClass'' ,arStuEnrollments.CampusId ) ) ) ) = ''hours'' AND AAUT1.UnitTypeDescrip = ''Clock Hours''  ) THEN CASE WHEN ( SELECT     TOP 1 EndDate
 FROM       arTerm art INNER JOIN arPrgVersions arp ON arp.PrgVerId = art.ProgramVersionId WHERE      PrgVerId = arStuEnrollments.PrgVerId  ) IS NULL THEN ( ISNULL(SAS_NoTerm.AttendedDays,0) / 60 ) + ( ISNULL(SAS_NoTerm.MakeupDays,0) / 60 )   ELSE ( ISNULL(SAS_ForTerm.AttendedDays,0) / 60 ) + ( ISNULL(SAS_ForTerm.MakeupDays,0) / 60 ) END WHEN (  LOWER(LTRIM(RTRIM(dbo.GetAppSettingValueByKeyName( ''TrackSapAttendance''
  ,arStuEnrollments.CampusId ) ) )  ) = ''byclass'' AND LOWER(LTRIM(RTRIM(dbo.GetAppSettingValueByKeyName( ''DisplayAttendanceUnitForProgressReportByClass'' ,arStuEnrollments.CampusId    ) ) ) ) = ''presentabsent'' AND AAUT1.UnitTypeDescrip = ''Clock Hours'' ) THEN CASE WHEN (  SELECT     TOP 1 EndDate  FROM       arTerm art INNER JOIN arPrgVersions arp ON arp.PrgVerId = art.ProgramVersionId
  WHERE      PrgVerId = arStuEnrollments.PrgVerId ) IS NULL THEN ( ISNULL(SAS_NoTerm.AttendedDays,0) / 60 ) + ( ISNULL(SAS_NoTerm.MakeupDays,0) / 60 ) ELSE ( ISNULL(SAS_ForTerm.AttendedDays,0) / 60 ) + ( ISNULL(SAS_ForTerm.MakeupDays,0) / 60 )      END  WHEN ( LOWER(LTRIM(RTRIM(dbo.GetAppSettingValueByKeyName( ''TrackSapAttendance'' ,arStuEnrollments.CampusId ) ) ) ) = ''byclass'' AND LOWER(LTRIM(RTRIM(dbo.GetAppSettingValueByKeyName(
 ''DisplayAttendanceUnitForProgressReportByClass'' ,arStuEnrollments.CampusId ) ) ) ) = ''presentabsent'' AND AAUT1.UnitTypeDescrip = ''Minutes'' ) THEN  CASE WHEN ( SELECT     TOP 1 EndDate   FROM       arTerm art  INNER JOIN arPrgVersions arp ON arp.PrgVerId = art.ProgramVersionId WHERE      PrgVerId = arStuEnrollments.PrgVerId ) IS NULL THEN ( ISNULL(SAS_NoTerm.AttendedDays,0) / 60 ) + ( ISNULL(SAS_NoTerm.MakeupDays,0) / 60 )
  ELSE ( ISNULL(SAS_ForTerm.AttendedDays,0) / 60 ) + ( ISNULL(SAS_ForTerm.MakeupDays,0) / 60 )  END ELSE  CASE WHEN (  SELECT     TOP 1 EndDate FROM       arTerm art   INNER JOIN arPrgVersions arp ON arp.PrgVerId = art.ProgramVersionId WHERE      PrgVerId = arStuEnrollments.PrgVerId ) IS NULL THEN ( ISNULL(SAS_NoTerm.AttendedDays,0) ) + ( ISNULL(SAS_NoTerm.MakeupDays,0) )
  ELSE ( ISNULL(SAS_ForTerm.AttendedDays,0) ) + ( ISNULL(SAS_ForTerm.MakeupDays,0) ) END  END END  ) FROM       arAttUnitType AAUT1 INNER JOIN arPrgVersions arp ON arp.UnitTypeId = AAUT1.UnitTypeId    LEFT JOIN  ( SELECT   StuEnrollId ,MAX(ActualRunningScheduledDays) AS ScheduledDays ,MAX(ActualRunningPresentDays) AS AttendedDays ,MAX(AdjustedAbsentDays) AS AbsentDays
  ,MAX(ActualRunningMakeupDays) AS MakeupDays,MAX(StudentAttendedDate) AS LDA FROM     syStudentAttendanceSummary  GROUP BY StuEnrollId ) SAS_NoTerm ON arStuEnrollments.StuEnrollId = SAS_NoTerm.StuEnrollId   LEFT JOIN  ( SELECT   StuEnrollId ,MAX(ActualRunningScheduledDays) AS ScheduledDays ,MAX(ActualRunningPresentDays) AS AttendedDays,MAX(AdjustedAbsentDays) AS AbsentDays
 ,MAX(ActualRunningMakeupDays) AS MakeupDays,MAX(StudentAttendedDate) AS LDA FROM     syStudentAttendanceSummary  GROUP BY StuEnrollId ) SAS_ForTerm ON arStuEnrollments.StuEnrollId = SAS_ForTerm.StuEnrollId   ) ,0)  )  ) AS Remainin ,0 AS ValueIfNegative ) b ) AS RemainingHours '
WHERE  FldId = @remainHrsFldId;

END TRY
BEGIN CATCH
    SET @adhocError = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE()
          ,@severity = ERROR_SEVERITY()
          ,@state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @adhocError > 0
    BEGIN
        ROLLBACK TRANSACTION completedHrAdh;
        PRINT 'eroor in updating CompletedHr CalculatedSQL.';

    END;
ELSE
    BEGIN
        COMMIT TRANSACTION completedHrAdh;
        PRINT 'successful in updating CompletedHr CalculatedSQL.';
    END;
GO
--=================================================================================================
-- END tech: fixing completedHr and totalHour SQL for adhoc
--=================================================================================================
--=================================================================================================
-- START AD-16046 Lead Group Modifications
--=================================================================================================
DECLARE @ErrorAD_16046 AS INT;
SET @ErrorAD_16046 = 0;
BEGIN TRANSACTION AD_16046;
BEGIN TRY

    UPDATE dbo.syMenuItems
    SET    DisplayName = 'Set Up Groups',MenuName = 'Set Up Groups'	
    WHERE  ResourceId = 339
           AND DisplayName = 'Lead Groups';

		    UPDATE dbo.syResources
    SET    DisplayName = 'Set Up Groups', Resource = 'Set Up Groups'
    WHERE  ResourceId = 339
           AND Resource = 'Lead Groups';

END TRY
BEGIN CATCH
    SET @ErrorAD_16046 = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE()
          ,@severity = ERROR_SEVERITY()
          ,@state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @ErrorAD_16046 > 0
    BEGIN
        ROLLBACK TRANSACTION AD_16046;
        PRINT 'Failed transcation AD_16046.';

    END;
ELSE
    BEGIN
        COMMIT TRANSACTION AD_16046;
        PRINT 'successful transaction AD_16046.';
    END;
GO
--=================================================================================================
-- END AD-16046 Lead Group Modifications
--=================================================================================================
--=================================================================================================
-- START AD-17035 MultiColumn Progress and Transcript Report Enhancements
--=================================================================================================
DECLARE @ErrorAD17035 AS INT;
SET @ErrorAD17035 = 0;
BEGIN TRANSACTION AD_17035;
BEGIN TRY
    DECLARE @settingKeyName VARCHAR(MAX) = 'Transcript_Progress_header_type';
    IF NOT EXISTS (
                  SELECT 1
                  FROM   dbo.syConfigAppSettings
                  WHERE  KeyName = @settingKeyName
                  )
        BEGIN
            INSERT INTO dbo.syConfigAppSettings (
                                                KeyName
                                               ,Description
                                               ,ModUser
                                               ,ModDate
                                               ,CampusSpecific
                                               ,ExtraConfirmation
                                                )
            VALUES ( @settingKeyName                                                                               -- KeyName - varchar(200)
                    ,'Allows the option to select the header type to be used on Progress and Transcript Reports .' -- Description - varchar(1000)
                    ,'sa'                                                                                          -- ModUser - varchar(50)
                    ,GETDATE()                                                                                     -- ModDate - datetime
                    ,0                                                                                             -- CampusSpecific - bit
                    ,0                                                                                             -- ExtraConfirmation - bit
                );

            DECLARE @headerTypeProgTransSettingId INT = (
                                                        SELECT TOP 1 SettingId
                                                        FROM   dbo.syConfigAppSettings
                                                        WHERE  KeyName = @settingKeyName
                                                        );

            INSERT INTO syConfigAppSet_Lookup (
                                              LookUpId
                                             ,SettingId
                                             ,ValueOptions
                                             ,ModUser
                                             ,ModDate
                                              )
            VALUES ( NEWID()                       -- LookUpId - uniqueidentifier
                    ,@headerTypeProgTransSettingId -- SettingId - int
                    ,'Subject'                     -- ValueOptions - varchar(50)
                    ,'Support'                     -- ModUser - varchar(50)
                    ,GETDATE()                     -- ModDate - datetime
                );

            INSERT INTO syConfigAppSet_Lookup (
                                              LookUpId
                                             ,SettingId
                                             ,ValueOptions
                                             ,ModUser
                                             ,ModDate
                                              )
            VALUES ( NEWID()                       -- LookUpId - uniqueidentifier
                    ,@headerTypeProgTransSettingId -- SettingId - int
                    ,'Course'                      -- ValueOptions - varchar(50)
                    ,'Support'                     -- ModUser - varchar(50)
                    ,GETDATE()                     -- ModDate - datetime
                );

            IF NOT EXISTS (
                          SELECT 1
                          FROM   dbo.syConfigAppSetValues
                          WHERE  SettingId = @headerTypeProgTransSettingId
                          )
                BEGIN
                    INSERT INTO dbo.syConfigAppSetValues (
                                                         ValueId
                                                        ,SettingId
                                                        ,CampusId
                                                        ,Value
                                                        ,ModUser
                                                        ,ModDate
                                                        ,Active
                                                         )
                    VALUES ( NEWID()                       -- ValueId - uniqueidentifier
                            ,@headerTypeProgTransSettingId -- SettingId - int
                            ,NULL                          -- CampusId - uniqueidentifier
                            ,'Course'                      -- Value - varchar(1000)
                            ,'SA'                          -- ModUser - varchar(50)
                            ,GETDATE()                     -- ModDate - datetime
                            ,1                             -- Active - bit
                        );
                END;
        END;

END TRY
BEGIN CATCH
    SET @ErrorAD17035 = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE()
          ,@severity = ERROR_SEVERITY()
          ,@state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @ErrorAD17035 > 0
    BEGIN
        ROLLBACK TRANSACTION AD_17035;
        PRINT 'Failed transcation AD_17035.';

    END;
ELSE
    BEGIN
        COMMIT TRANSACTION AD_17035;
        PRINT 'successful transaction AD_17035.';
    END;
GO
--=================================================================================================
-- END AD-17035 MultiColumn Progress and Transcript Report Enhancements
--=================================================================================================

--=================================================================================================
-- START AD-16225 : New Grid and Search for Edit Time Clock Punches Page
--=================================================================================================
DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION ChangeEditPunchesLink;
BEGIN TRY
    DECLARE @resourceId INT = 587;
    UPDATE dbo.syResources
    SET    Resource = 'Edit Time Clock Punches'
          ,ResourceURL = '~/FA/EditTimeClockPunches.aspx'
    WHERE  ResourceID = @resourceId;

    UPDATE dbo.syMenuItems
    SET    MenuName = 'Edit Time Clock Punches'
          ,DisplayName = 'Edit Time Clock Punches'
          ,Url = '/FA/EditTimeClockPunches.aspx'
    WHERE  ResourceId = @resourceId;
END TRY
BEGIN CATCH
    SET @error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE()
          ,@severity = ERROR_SEVERITY()
          ,@state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
    BEGIN
        ROLLBACK TRANSACTION ChangeEditPunchesLink;
        PRINT 'Failed to change edit punches link';
    END;
ELSE
    BEGIN
        COMMIT TRANSACTION ChangeEditPunchesLink;
        PRINT 'updated edit punches link';
    END;
GO
--=================================================================================================
-- END AD-16225 : New Grid and Search for Edit Time Clock Punches Page
--=================================================================================================

-- ===============================================================================================
-- END Consolidated Script Version 4.1SP3
-- ===============================================================================================

