SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[CalculateHoursForStudentOnDate]
    (
        @StuEnrollId UNIQUEIDENTIFIER
       ,@Date DATETIME
       ,@AttemptedInsertedHours DECIMAL(16, 2)
    )
RETURNS DECIMAL(16, 2)
AS
    BEGIN
        DECLARE @CampusId UNIQUEIDENTIFIER = (
                                             SELECT TOP 1 CampusId
                                             FROM   dbo.arStuEnrollments
                                             WHERE  StuEnrollId = @StuEnrollId
                                             );

        DECLARE @IsTimeClock BIT = (
                                   SELECT TOP 1 dbo.arPrgVersions.UseTimeClock
                                   FROM   dbo.arStuEnrollments
                                   JOIN   dbo.arPrgVersions ON arPrgVersions.PrgVerId = arStuEnrollments.PrgVerId
                                   WHERE  StuEnrollId = @StuEnrollId
                                   );

        DECLARE @TrackSapAttendance VARCHAR(50) = LOWER(LTRIM(RTRIM(dbo.GetAppSettingValueByKeyName('TrackSapAttendance', @CampusId))));

        --no calculation needed, inserting these numbers as place holder or no attendance
        IF (
           @AttemptedInsertedHours = 0
           OR @AttemptedInsertedHours = 9999.0
           OR @AttemptedInsertedHours = 999.0
           OR @AttemptedInsertedHours = 99.0
           )
            BEGIN
                RETURN @AttemptedInsertedHours;
            END;

        --return attempted inserted hours if not by day or not time clock 
        IF (
           @TrackSapAttendance != 'byday'
           OR @IsTimeClock = 0
           )
            RETURN @AttemptedInsertedHours;

        --if by day and time clock, calculate hours based on punches, round based on campus settings
        DECLARE @MinimumHoursToBeConsideredPresent DECIMAL(16, 2) = (
                                                                    SELECT MinimumHoursToBePresent
                                                                    FROM   dbo.arStuEnrollments
                                                                    JOIN   dbo.arStudentSchedules ON arStudentSchedules.StuEnrollId = arStuEnrollments.StuEnrollId
                                                                    JOIN   dbo.arProgSchedules ON arProgSchedules.ScheduleId = arStudentSchedules.ScheduleId
                                                                    JOIN   dbo.arProgScheduleDetails ON arProgScheduleDetails.ScheduleId = arProgSchedules.ScheduleId
                                                                                                        AND dw = ( DATEPART(dw, @Date) - 1 )
                                                                                                        AND arStuEnrollments.StuEnrollId = @StuEnrollId
                                                                    );

        DECLARE @RoundingMethod VARCHAR(50) = dbo.GetAppSettingValueByKeyName('HoursRoundingMethod', @CampusId);

        IF @TrackSapAttendance = 'byday'
           AND @IsTimeClock = 1
            BEGIN
                DECLARE @CalculatedHoursForDayRounded DECIMAL(16, 2) = (
                                                                       SELECT   SUM(HoursForSingleInAndOut) AS TotalHoursForSingleDay
                                                                       FROM     (
                                                                                SELECT   dta.StuEnrollId
                                                                                        ,dta.PunchDate
                                                                                        ,CASE WHEN ( dta.rn % 2 ) = 0 THEN dta.rn - 1
                                                                                              ELSE dta.rn
                                                                                         END AS RelatedPunches
                                                                                        ,dbo.CalculateRoundingOnMinutes(
                                                                                                                           DATEDIFF(
                                                                                                                                       SECOND
                                                                                                                                      ,MIN(dta.PunchTime)
                                                                                                                                      ,MAX(dta.PunchTime)
                                                                                                                                   ) / 60.0
                                                                                                                          ,@RoundingMethod
                                                                                                                       ) AS HoursForSingleInAndOut
                                                                                FROM     (
                                                                                         SELECT BadgeId
                                                                                               ,StuEnrollId
                                                                                               ,PunchTime
                                                                                               ,CAST(PunchTime AS DATE) AS PunchDate
                                                                                               ,ROW_NUMBER() OVER ( ORDER BY StuEnrollId
                                                                                                                            ,PunchTime
                                                                                                                  ) rn
                                                                                         FROM   dbo.arStudentTimeClockPunches
                                                                                         WHERE  dbo.arStudentTimeClockPunches.Status = 1
                                                                                                AND StuEnrollId = @StuEnrollId
                                                                                                AND CAST(PunchTime AS DATE) = @Date
                                                                                         ) AS dta
                                                                                GROUP BY dta.StuEnrollId
                                                                                        ,dta.PunchDate
                                                                                        ,CASE WHEN ( dta.rn % 2 ) = 0 THEN dta.rn - 1
                                                                                              ELSE dta.rn
                                                                                         END
                                                                                HAVING   COUNT(dta.StuEnrollId) % 2 = 0
                                                                                ) AS Dta
                                                                       GROUP BY Dta.StuEnrollId
                                                                               ,Dta.PunchDate
                                                                       ) / 60.0;

                RETURN CASE WHEN @CalculatedHoursForDayRounded IS NULL THEN @AttemptedInsertedHours
                            WHEN ( @CalculatedHoursForDayRounded >= ISNULL(@MinimumHoursToBeConsideredPresent, 0)) THEN @CalculatedHoursForDayRounded
                            ELSE 0 --if student did not meet minimum hours to be considered present, return 0
                       END;
            END;

        RETURN @AttemptedInsertedHours;
    END;






GO
