SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------------------------
--Function to return the status of an enrollment at a specified date
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE FUNCTION [dbo].[UDF_GetEnrollmentStatusAtGivenDate]
(
    @StuEnrollId UNIQUEIDENTIFIER,
    @ReportDate DATETIME
)
RETURNS VARCHAR(50)
AS
BEGIN
    DECLARE @ReturnValue VARCHAR(50);
    DECLARE @CurrentStatus VARCHAR(50);
    DECLARE @StartDate DATETIME;
    DECLARE @LastDateOfChange DATETIME;
    DECLARE @LastNewStatus VARCHAR(50);

    SET @ReturnValue = '';

    SELECT @CurrentStatus = sc.StatusCodeDescrip,
           @StartDate = se.StartDate
    FROM dbo.arStuEnrollments se
        INNER JOIN dbo.syStatusCodes sc
            ON sc.StatusCodeId = se.StatusCodeId
    WHERE se.StuEnrollId = @StuEnrollId;

    SET @LastDateOfChange =
    (
        SELECT MAX(DateOfChange)
        FROM dbo.syStudentStatusChanges
        WHERE StuEnrollId = @StuEnrollId
    );

    SET @LastNewStatus =
    (
        SELECT TOP 1
               sc.StatusCodeDescrip
        FROM dbo.syStudentStatusChanges ssc
            INNER JOIN dbo.syStatusCodes sc
                ON sc.StatusCodeId = ssc.NewStatusId
        WHERE ssc.StuEnrollId = @StuEnrollId
        ORDER BY ssc.DateOfChange DESC,
                 ssc.ModDate DESC
    );


    --If there are no status changes for the enrollment and the given date is >= enrollment start date we can simply return the current status. 
    IF NOT EXISTS
    (
        SELECT 1
        FROM dbo.syStudentStatusChanges
        WHERE StuEnrollId = @StuEnrollId
    )
    BEGIN
        IF @ReportDate >= @StartDate
        BEGIN
            SET @ReturnValue = @CurrentStatus;
        END;
    END;
    ELSE
    BEGIN
        --If the report date > last date of change for the enrollment then we can simply return the last new status (should be same as current status)
        IF @ReportDate > @LastDateOfChange
           AND @ReportDate >= @StartDate
        BEGIN
            SET @ReturnValue = @LastNewStatus;
        END;
        ELSE
        BEGIN
            --If the report date is the same as a date of change then we can simply return the new status for that date of change.
            --Use the FORMAT function just in case the DateOfChange field in the database has a time component on it such as 2018-10-19 05:00:00.000
            IF EXISTS
            (
                SELECT 1
                FROM dbo.syStudentStatusChanges
                WHERE StuEnrollId = @StuEnrollId
                      AND CONVERT(VARCHAR, DateOfChange, 101) = CONVERT(VARCHAR, @ReportDate, 101)
            )
            BEGIN
                SET @ReturnValue =
                (
                    SELECT TOP 1
                           sc.StatusCodeDescrip
                    FROM dbo.syStudentStatusChanges ssc
                        INNER JOIN dbo.syStatusCodes sc
                            ON sc.StatusCodeId = ssc.NewStatusId
                    WHERE ssc.StuEnrollId = @StuEnrollId
                          AND CONVERT(VARCHAR, DateOfChange, 101) = CONVERT(VARCHAR, @ReportDate, 101)
                    ORDER BY ssc.DateOfChange DESC,
                             ssc.ModDate DESC
                );

            END;
            ELSE
            BEGIN
                --At this point we can get the last change record with DateOfChange that is less than the report date
                SET @ReturnValue =
                (
                    SELECT TOP 1
                           sc.StatusCodeDescrip
                    FROM dbo.syStudentStatusChanges ssc
                        INNER JOIN dbo.syStatusCodes sc
                            ON sc.StatusCodeId = ssc.NewStatusId
                    WHERE ssc.StuEnrollId = @StuEnrollId
                          AND ssc.DateOfChange < @ReportDate
                    ORDER BY ssc.DateOfChange DESC,
                             ssc.ModDate DESC
                );
            END;

        END;

    END;

    RETURN ISNULL(@ReturnValue, '');
END;



GO
