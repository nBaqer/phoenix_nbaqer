SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- **********************************************************************************************************************
-- Convert unformatted Phone Numbers (i.e., no embedded hyphens) to standard "xxx-xx-xxxx" Phone display format.
-- Example: 
--	PRINT dbo.udfConvertUnformattedPhoneToFormattedPhone('123456789') --returns '123-45-6789' 
-- ***********************************************************************************************************************
CREATE FUNCTION [dbo].[UDF_FormatPhone] ( @Phone CHAR(10) )
RETURNS CHAR(14)
AS
    BEGIN
        DECLARE @Mask VARCHAR(20)
           ,@MaskChar CHAR(1)
           ,@FormattedPhone VARCHAR(20)
           ,@intLoop INT;
--declare @Phone varchar(9)
        DECLARE @intInnerLoop INT;
        SET @Mask = (
                      SELECT    Mask
                      FROM      syInputMasks
                      WHERE     InputMaskId = 2
                    );
--set @Phone='123456789'
        SET @FormattedPhone = '';
        SET @intLoop = 0;
        SET @intInnerLoop = 0;
        WHILE ( @intLoop < LEN(@Mask) )
            BEGIN
                SET @intLoop = @intLoop + 1;
                SET @MaskChar = SUBSTRING(@Mask,@intLoop,1);
                IF LTRIM(RTRIM(@MaskChar)) = '#'
                    BEGIN
                        SET @intInnerLoop = @intInnerLoop + 1;
                        SET @FormattedPhone = @FormattedPhone + SUBSTRING(@Phone,@intInnerLoop,1); 
                    END;
                ELSE
                    BEGIN
                        IF @intLoop = 1
                            BEGIN
                                SET @FormattedPhone = @MaskChar; 
                            END;
                        ELSE
                            BEGIN
                                SET @FormattedPhone = @FormattedPhone + @MaskChar;
                            END;
                    END;
            END;
        RETURN @FormattedPhone;
    END; 

GO
