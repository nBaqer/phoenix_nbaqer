SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[GetUDFValue]
    (
     @SDFID UNIQUEIDENTIFIER
    ,@ID UNIQUEIDENTIFIER
	)
RETURNS VARCHAR(50)
AS
    BEGIN
        DECLARE @ReturnValue VARCHAR(50);
        SET @ReturnValue = (
                             SELECT TOP 1
                                    SDFValue
                             FROM   sySDFModuleValue V
                             WHERE  SDFID = @SDFID
                                    AND ( PgPKID = @ID )
                           );  
	
        RETURN @ReturnValue;
    END;

GO
