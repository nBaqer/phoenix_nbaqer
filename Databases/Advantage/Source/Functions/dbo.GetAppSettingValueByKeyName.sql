SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE FUNCTION [dbo].[GetAppSettingValueByKeyName]
    (
     @KeyName VARCHAR(200)
    ,@CampusId UNIQUEIDENTIFIER = NULL
	)
RETURNS VARCHAR(1000)
AS
    BEGIN    
        DECLARE @ReturnValue AS VARCHAR(1000);
        IF ( @CampusId IS NULL )
            BEGIN
                SET @ReturnValue = (
                                     SELECT SCASV.Value
                                     FROM   syConfigAppSettings AS SCAS
                                     INNER JOIN syConfigAppSetValues AS SCASV ON SCASV.SettingId = SCAS.SettingId
                                     WHERE  SCAS.KeyName = @KeyName
                                            AND SCASV.CampusId IS NULL
                                   );
            END;
        ELSE
            BEGIN
                SET @ReturnValue = (
                                     SELECT SCASV.Value
                                     FROM   syConfigAppSettings AS SCAS
                                     INNER JOIN syConfigAppSetValues AS SCASV ON SCASV.SettingId = SCAS.SettingId
                                     WHERE  SCAS.KeyName = @KeyName
                                            AND SCASV.CampusId = @CampusId
                                   );
                IF ( @ReturnValue IS NULL )
                    BEGIN
                        SET @ReturnValue = (
                                             SELECT SCASV.Value
                                             FROM   syConfigAppSettings AS SCAS
                                             INNER JOIN syConfigAppSetValues AS SCASV ON SCASV.SettingId = SCAS.SettingId
                                             WHERE  SCAS.KeyName = @KeyName
                                                    AND SCASV.CampusId IS NULL
                                           );
                    END;
            END;
        RETURN @ReturnValue;
    END;

GO
