SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[GetDeferredRevenue]
    (
     @TransactionId VARCHAR(36)
    ,@ReportDate DATETIME = NULL
	)
RETURNS DECIMAL(9,2)
AS
    BEGIN
-- get transaction amount 
        DECLARE @Amount DECIMAL(9,2);
        DECLARE @StuEnrollId VARCHAR(36);
        SELECT  @Amount = TransAmount
               ,@StuEnrollId = StuEnrollId
        FROM    saTransactions T
               ,saTransCodes TC
        WHERE   T.TransCodeId = TC.TransCodeId
                AND T.TransactionId = @TransactionId
                AND DefEarnings = 1;
-- if amount is null then return zero
        IF @Amount IS NULL
            RETURN 0.00;
-- default date is todays dateIf @ReportDate is null select @ReportDate=GETDATE()
-- get duration of the program and convert it to months
        DECLARE @TuitionEarningId VARCHAR(36);
        DECLARE @FullMonthBeforeDay SMALLINT;
        DECLARE @HalfMonthBeforeDay SMALLINT;
        DECLARE @TermDuration DECIMAL(9,4);
        DECLARE @Credits SMALLINT;
        DECLARE @RoundedElapsedTime DECIMAL(9,4);
        DECLARE @PercentToEarnIdx TINYINT;
        DECLARE @PercentageRangeBasisIdx TINYINT;
        SELECT  @TermDuration = Weeks * 12.00 / 52.00
               ,@TuitionEarningId = TuitionEarningId
               ,@Credits = Credits
        FROM    arPrgVersions
        WHERE   PrgVerId = (
                             SELECT PrgVerId
                             FROM   arStuEnrollments
                             WHERE  StuEnrollId = (
                                                    SELECT  StuEnrollId
                                                    FROM    saTransactions
                                                    WHERE   TransactionId = @TransactionId
                                                  )
                           );
        SELECT  @FullMonthBeforeDay = FullMonthBeforeDay
               ,@HalfMonthBeforeDay = HalfMonthBeforeDay
               ,@PercentToEarnIdx = PercentToEarnIdx
               ,@PercentageRangeBasisIdx = PercentageRangeBasisIdx
        FROM    saTuitionEarnings
        WHERE   TuitionEarningId = @TuitionEarningId;
-- get elapsed time from the begining of the program to the reportdate
        DECLARE @StartDate DATETIME;
        SELECT  @StartDate = StartDate
        FROM    arStuEnrollments
        WHERE   StuEnrollId = (
                                SELECT  StuEnrollId
                                FROM    saTransactions
                                WHERE   TransactionId = @TransactionId
                              );
        DECLARE @ElapsedTime AS DECIMAL(9,4);
        SELECT  @ElapsedTime = ( DATEDIFF(DAY,@StartDate,@ReportDate) + 1. ) / 7. * 12.0 / 52.0;
        SELECT  @RoundedElapsedTime = @ElapsedTime;
-- apply rounding rules
        IF DATEPART(DAY,@StartDate) <= @HalfMonthBeforeDay
            AND @HalfMonthBeforeDay > 0
            SELECT  @RoundedElapsedTime = ( ROUND(@ElapsedTime,0,1) * 2.0 + 1.0 ) / 2.0; 
        IF DATEPART(DAY,@StartDate) <= @FullMonthBeforeDay
            AND @FullMonthBeforeDay > 0
            SELECT  @RoundedElapsedTime = ROUND(@ElapsedTime + 0.99999,0,1); 
-- If Report Date is previous to StartDate return 0
        IF @ElapsedTime < 0
            SELECT  @RoundedElapsedTime = 0;
        IF @RoundedElapsedTime > @TermDuration
            SELECT  @RoundedElapsedTime = @TermDuration;
-- if the method is straightPercentage then apply percentage
        IF @PercentToEarnIdx = 0
            BEGIN
                IF @RoundedElapsedTime / @TermDuration > 1
                    RETURN @Amount;
                ELSE
                    RETURN @Amount*(@RoundedElapsedTime/@TermDuration);
            END;
        ELSE
	/* use a table to calculate the percentage result */
            BEGIN
		/* calculate percentage to be used as argument in search */
                DECLARE @PercentageArgument SMALLINT;
                SELECT  @PercentageArgument = CASE @PercentageRangeBasisIdx
                                                WHEN 0 THEN ROUND(( @RoundedElapsedTime / @TermDuration ) * 100.0,0)
                                                WHEN 1 THEN ROUND(( dbo.GetCreditsEarned(@StuEnrollId) * 1.00 / @Credits ) * 100.0,0)
                                                WHEN 2 THEN ROUND(( dbo.GetCreditsAttempted(@StuEnrollId) * 1.00 / @Credits ) * 100.0,0)
                                              END;
                DECLARE @PercentageResult SMALLINT;
                SELECT  @PercentageResult = EarnPercent
                FROM    saTuitionEarningsPercentageRanges
                WHERE   TuitionEarningId = @TuitionEarningId
                        AND UpTo = (
                                     SELECT MAX(UpTo)
                                     FROM   saTuitionEarningsPercentageRanges
                                     WHERE  TuitionEarningId = @TuitionEarningId
                                            AND UpTo <= @PercentageArgument
                                   );
                SELECT  @PercentageResult = COALESCE(@PercentageResult,0);
                SELECT  @Amount = @Amount * @PercentageResult / 100.00;
            END;
        RETURN @Amount;
    END;

GO
