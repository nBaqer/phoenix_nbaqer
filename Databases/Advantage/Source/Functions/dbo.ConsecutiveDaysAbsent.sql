SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[ConsecutiveDaysAbsent]
    (
     @StuEnrollId UNIQUEIDENTIFIER
    ,@ClsSectionId UNIQUEIDENTIFIER
    ,@CutOffDate DATETIME
    )
RETURNS INT
AS
    BEGIN
        DECLARE @Return INT;
        DECLARE @meetdate DATETIME;
        DECLARE @recordcounter INT;
        DECLARE @prevmeetdate DATETIME;
        DECLARE @consecutivecounter INT;
        DECLARE @prevconsecutivecounter INT;
        DECLARE @actual INT;
	
        SET @Return = 0;
        SET @recordcounter = 1;
        SET @consecutivecounter = 0;
        SET @prevconsecutivecounter = 0;
	
        DECLARE absentCursor CURSOR
        FOR
            SELECT  MeetDate
                   ,Actual
            FROM    dbo.atClsSectAttendance
            WHERE   StuEnrollId = @StuEnrollId
                    AND ClsSectionId = @ClsSectionId
                    AND Scheduled >= 1 -- any school that tracks attendance by PA and Clock Hour
                    AND MeetDate <= @CutOffDate
            ORDER BY MeetDate;
		
        OPEN absentCursor;
        FETCH NEXT FROM absentCursor
	INTO @meetdate,@actual;
	
        WHILE @@FETCH_STATUS = 0
            BEGIN
			
			--PRINT 'Record:' + CONVERT(VARCHAR(20),@recordcounter)
                IF @actual = 0
                    BEGIN
                        SET @consecutivecounter = @consecutivecounter + 1;
					--PRINT '@consecutivecounter:' + CONVERT(VARCHAR(20),@consecutivecounter)
                    END;
                ELSE
                    BEGIN
                        IF @consecutivecounter > @prevconsecutivecounter
                            BEGIN								
                                SET @prevconsecutivecounter = @consecutivecounter;
                                SET @consecutivecounter = 0;
                            END;
                    END;		
			
			
		
                SET @prevmeetdate = @meetdate;
                SET @recordcounter = @recordcounter + 1;
				
                FETCH NEXT FROM absentCursor
			INTO @meetdate,@actual;
            END;
		
        CLOSE absentCursor;
        DEALLOCATE absentCursor;	
	
        IF @consecutivecounter > @prevconsecutivecounter
            BEGIN								
                SET @prevconsecutivecounter = @consecutivecounter;							
            END;	
		 
        RETURN @prevconsecutivecounter;

    END;

GO
