SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE FUNCTION [dbo].[CompletedRequiredCourses]
    (
        @StuEnrollId UNIQUEIDENTIFIER
    )
RETURNS BIT
AS
    BEGIN
        DECLARE @ReturnValue BIT = 0;
        SET @ReturnValue = (
                           SELECT CASE WHEN EXISTS (
                                                   SELECT     *
                                                   FROM       dbo.arResults results
                                                   INNER JOIN dbo.arGradeSystemDetails gsd ON gsd.GrdSysDetailId = results.GrdSysDetailId
                                                   WHERE      results.StuEnrollId = @StuEnrollId
                                                              AND (
                                                                  gsd.IsPass = 0
                                                                  AND gsd.IsCreditsAttempted = 1
                                                                  )
                                                   ) THEN 0
                                       ELSE 1
                                  END
                           );
        RETURN @ReturnValue;
    END;



GO
