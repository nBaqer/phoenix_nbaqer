SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[MaxConsecutiveDaysAbsentForProgram]
    (
        @StuEnrollId UNIQUEIDENTIFIER
       ,@ClassMeetingDate DATETIME
       ,@ShowCalendarDays BIT = 0
       ,@ConsecutiveDays SMALLINT = 1
	   ,@ClsSectionId UNIQUEIDENTIFIER = '00000000-0000-0000-0000-000000000000'
    )
RETURNS INT
AS
    BEGIN
        DECLARE @Return INT;
        DECLARE @recordcounter INT;
        DECLARE @prevmeetdate DATE;
        DECLARE @consecutivecounter INT;
        DECLARE @prevconsecutivecounter INT;
        DECLARE @curMeetdate DATE;
        DECLARE @curActual INT;
        DECLARE @curDateNotParsed DATETIME2;
        DECLARE @LDA DATETIME2;
        DECLARE @EndDateRange DATETIME2;
        DECLARE @LDABeforeEndDateRange DATETIME2;

        SET @Return = 0;
        SET @recordcounter = 1;
        SET @consecutivecounter = 0;
        SET @prevconsecutivecounter = 0;

        SET @prevmeetdate = '1900-01-01';

        DECLARE @InfoDates AS TABLE
            (
                MeetDateWithoutTime DATETIME2
               ,Actual DECIMAL(18, 0)
            );

        INSERT INTO @InfoDates
                    SELECT ACSA.MeetDate AS MeetDateWithoutTime
                          ,ACSA.Actual
                    FROM   atClsSectAttendance AS ACSA
                    WHERE  ACSA.StuEnrollId = @StuEnrollId
					AND (ACSA.ClsSectionId = '00000000-0000-0000-0000-000000000000' OR ACSA.ClsSectionId = @ClsSectionId)
                           AND ACSA.Scheduled >= 1 -- any school that tracks attendance by PA and Clock Hour
                           AND ACSA.MeetDate <= @ClassMeetingDate
                    UNION
                    SELECT ACSA.RecordDate AS MeetDateWithoutTime
                          ,ACSA.ActualHours
                    FROM   arStudentClockAttendance AS ACSA
                    WHERE  ACSA.StuEnrollId = @StuEnrollId
                           AND ACSA.SchedHours >= 1 -- any school that tracks attendance by PA and Clock Hour
                           AND ACSA.RecordDate <= @ClassMeetingDate;

        SET @LDA = (
                   SELECT MAX(MeetDateWithoutTime)
                   FROM   @InfoDates
                   WHERE  Actual > 0  AND actual NOT IN (999,9999)
                   );

        SET @EndDateRange = DATEADD(DAY, @ConsecutiveDays * -1, @ClassMeetingDate);

        SET @LDABeforeEndDateRange = (
                                     SELECT MAX(MeetDateWithoutTime)
                                     FROM   @InfoDates
                                     WHERE  MeetDateWithoutTime <= @EndDateRange
                                            AND Actual > 0  AND actual NOT IN (999,9999)
                                     );
        SET @EndDateRange = @LDA;

IF @LDA > @EndDateRange
   AND @EndDateRange > @LDABeforeEndDateRange
    BEGIN
        SET @EndDateRange = @LDABeforeEndDateRange;
    END;


        IF @ShowCalendarDays = 1
            BEGIN
                DECLARE @MaxDate DATETIME2 = (
                                             SELECT MAX(MeetDateWithoutTime)
                                             FROM   @InfoDates
                                             );
                DECLARE @currentDay DATETIME;
                SELECT @currentDay = @EndDateRange;
                WHILE @currentDay <= @MaxDate
                    BEGIN

                        IF NOT EXISTS (
                                      SELECT TOP 1 D.MeetDateWithoutTime
                                      FROM   @InfoDates D
                                      WHERE  D.MeetDateWithoutTime = @currentDay
                                      )
                            BEGIN
                                INSERT INTO @InfoDates
                                VALUES ( @currentDay -- MeetDateWithoutTime - datetime2
                                        ,0           -- Actual - decimal(18, 0)
                                    );

                            END;
                        SELECT @currentDay = DATEADD(DAY, 1, @currentDay);


                    END;
            END;





        DECLARE absentCursor CURSOR FOR
            SELECT   CONVERT(VARCHAR(10), MeetDateWithoutTime, 101) AS MeetDateWithoutTime
                    ,MeetDateWithoutTime AS DateNotParsed
                    ,Actual AS Actual
            FROM     @InfoDates
            WHERE    MeetDateWithoutTime > @EndDateRange
            ORDER BY DateNotParsed DESC;

        OPEN absentCursor;
        FETCH NEXT FROM absentCursor
        INTO @curMeetdate
            ,@curDateNotParsed
            ,@curActual;

        WHILE @@FETCH_STATUS = 0
            BEGIN
                IF @curActual = 0
                    --If we are on a different date we can go ahead and increase the counter
                    BEGIN
                        IF @curMeetdate <> @prevmeetdate  AND NOT EXISTS (
                                  SELECT TOP 1 StuEnrollId
                                  FROM   dbo.arStudentLOAs
                                  WHERE  StartDate <= @curDateNotParsed
                                         AND EndDate >= @curDateNotParsed
                                         AND StuEnrollId = @StuEnrollId
                                  )
                            BEGIN
                                SET @consecutivecounter = @consecutivecounter + 1;
                            END;
                    END;
                ELSE
                    BEGIN
                        --If the student is present and the current date is the same as the previous date
                        --and the consecutive counter > 0 then we should set it to 0. For eg. if the student had
                        --three classes for a day and was absent for 2 but present for 1 then the student is present
                        --for that day and so the consecutive absences should be reset to 0.
                        IF @prevmeetdate = @curMeetdate
                            BEGIN
                                SET @consecutivecounter = 0;
                            END;
                        IF @consecutivecounter > @prevconsecutivecounter
                            BEGIN
                                SET @prevconsecutivecounter = @consecutivecounter;
                            END;
                        SET @consecutivecounter = 0;
                    END;

                SET @prevmeetdate = @curMeetdate;

                SET @recordcounter = @recordcounter + 1;
                FETCH NEXT FROM absentCursor
                INTO @curMeetdate
                    ,@curDateNotParsed
                    ,@curActual;
            END;

        CLOSE absentCursor;
        DEALLOCATE absentCursor;

        IF @consecutivecounter > @prevconsecutivecounter
            BEGIN
                SET @prevconsecutivecounter = @consecutivecounter;
            END;

        RETURN @prevconsecutivecounter;

    END;








GO
