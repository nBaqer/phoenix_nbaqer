SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[NumberofLOADaysBetweentheGivenPeriod]
    (
     @ReportDate DATE
    ,@StartDate DATE
    ,@StuEnrollId VARCHAR(50)
    )
RETURNS INTEGER
AS
    BEGIN
        DECLARE @ReturnValue INTEGER;
        DECLARE @intTotalNumberOfLOADays INTEGER;
	
        SET @ReturnValue = 0;
        SET @intTotalNumberOfLOADays = -1;

        SET @intTotalNumberOfLOADays = (
                                         SELECT DISTINCT
                                                COUNT(*) AS NumberOfLOADays
                                         FROM   arStudentLOAs
                                         WHERE  StartDate <= @StartDate
                                                AND ISNULL(LOAReturnDate,EndDate) >= @ReportDate
                                                AND StuEnrollId = @StuEnrollId
                                       );
	
        IF @intTotalNumberOfLOADays >= 1
            BEGIN
                SET @ReturnValue = DATEDIFF(DAYOFYEAR,@StartDate,@ReportDate) + 1;
            END;
        ELSE
            BEGIN
                IF (
                     SELECT SUM(NumberOfLOAdays) AS TotalNumberofLOAdays
                     FROM   (
                              --if LOA start date and end date falls with in the range
									SELECT DISTINCT
                                            StartDate
                                           ,ISNULL(LOAReturnDate,EndDate) AS LOAEndDate
                                           ,DATEDIFF(DAY,StartDate,ISNULL(LOAReturnDate,EndDate)) + 1 AS NumberOfLOAdays
                                    FROM    arStudentLOAs
                                    WHERE   StartDate >= @StartDate
                                            AND ISNULL(LOAReturnDate,EndDate) <= @ReportDate
                                            AND StuEnrollId = @StuEnrollId
                              UNION
									
									--if LOA start date is with in the range but holiday end date falls after report date
									--when enddate falls after report date
                              SELECT DISTINCT
                                        StartDate
                                       ,ISNULL(LOAReturnDate,EndDate) AS LOAEndDate
                                       ,DATEDIFF(DAY,StartDate,@ReportDate) + 1 AS NumberOfLOAdays
                              FROM      arStudentLOAs
                              WHERE     (
                                          StartDate >= @StartDate
                                          AND StartDate <= @ReportDate
                                        )
                                        AND ISNULL(LOAReturnDate,EndDate) >= @ReportDate
                                        AND StuEnrollId = @StuEnrollId
                              UNION
									
									--if holiday start date falls before student startdate and 
									--holiday end date is before the report date.
                              SELECT DISTINCT
                                        StartDate
                                       ,ISNULL(LOAReturnDate,EndDate) AS LOAEndDate
                                       ,DATEDIFF(DAY,@StartDate,ISNULL(LOAReturnDate,EndDate)) + 1 AS NumberOfLOAdays
                              FROM      arStudentLOAs
                              WHERE     StartDate < @StartDate
                                        AND ISNULL(LOAReturnDate,EndDate) <= @ReportDate
                                        AND DATEDIFF(DAY,@StartDate,ISNULL(LOAReturnDate,EndDate)) + 1 >= 1
                                        AND StuEnrollId = @StuEnrollId
                            ) LOAdays
                   ) IS NOT NULL
                    BEGIN
                        SET @ReturnValue = (
                                             SELECT SUM(NumberOfLOAdays) AS TotalNumberofLOAdays
                                             FROM   (
                                                      --if LOA start date and end date falls with in the range
									SELECT DISTINCT
                                            StartDate
                                           ,ISNULL(LOAReturnDate,EndDate) AS LOAEndDate
                                           ,DATEDIFF(DAY,StartDate,ISNULL(LOAReturnDate,EndDate)) + 1 AS NumberOfLOAdays
                                    FROM    arStudentLOAs
                                    WHERE   StartDate >= @StartDate
                                            AND ISNULL(LOAReturnDate,EndDate) <= @ReportDate
                                            AND StuEnrollId = @StuEnrollId
                                                      UNION
									
									--if LOA start date is with in the range but holiday end date falls after report date
									--when enddate falls after report date
                                                      SELECT DISTINCT
                                                                StartDate
                                                               ,ISNULL(LOAReturnDate,EndDate) AS LOAEndDate
                                                               ,DATEDIFF(DAY,StartDate,@ReportDate) + 1 AS NumberOfLOAdays
                                                      FROM      arStudentLOAs
                                                      WHERE     (
                                                                  StartDate >= @StartDate
                                                                  AND StartDate <= @ReportDate
                                                                )
                                                                AND ISNULL(LOAReturnDate,EndDate) >= @ReportDate
                                                                AND StuEnrollId = @StuEnrollId
                                                      UNION
									
									--if holiday start date falls before student startdate and 
									--holiday end date is before the report date.
                                                      SELECT DISTINCT
                                                                StartDate
                                                               ,ISNULL(LOAReturnDate,EndDate) AS LOAEndDate
                                                               ,DATEDIFF(DAY,@StartDate,ISNULL(LOAReturnDate,EndDate)) + 1 AS NumberOfLOAdays
                                                      FROM      arStudentLOAs
                                                      WHERE     StartDate < @StartDate
                                                                AND ISNULL(LOAReturnDate,EndDate) <= @ReportDate
                                                                AND DATEDIFF(DAY,@StartDate,ISNULL(LOAReturnDate,EndDate)) + 1 >= 1
                                                                AND StuEnrollId = @StuEnrollId
                                                    ) LOAdays
                                           );
				
				
                    END;
                ELSE
                    BEGIN
                        SET @ReturnValue = 0;
                    END;
		
			  
			
			
			
            END;
	
				


        RETURN @ReturnValue;
    END;

GO
