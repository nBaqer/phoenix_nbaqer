SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE FUNCTION [dbo].[udf_GetStudentClsSectMeetingfromStudentPunchinfo_moreClsSectExists]
    (
     @Punch DATETIME
    ,@PunchSpecialCode AS VARCHAR(5)
    ,@StuEnrollId AS VARCHAR(50)
    ,@CampusID AS VARCHAR(50)
    )
RETURNS VARCHAR(50)
AS
    BEGIN  


--DECLARE @StuEnrollId AS VARCHAR(50)   
--DECLARE @Punch AS DATETIME
--SET @Punch = '08/29/2011 12:50:00'
        DECLARE @punchDate AS DATETIME;
        DECLARE @PunchDay AS VARCHAR(5);
        SET @PunchDay = (
                          SELECT    dbo.udf_DayOfWeek(@Punch)
                        ); 
        SET @punchDate = CONVERT(VARCHAR,@Punch,101);
        DECLARE @punchTime AS DATETIME;

        SET @punchTime = CONVERT(VARCHAR,@Punch,108);
--DECLARE @PunchSpecialCode AS VARCHAR(5)
        DECLARE @ClsSectMeetingCount AS INTEGER;
        DECLARE @ClsSectMeetingID AS VARCHAR(50);
        DECLARE @ClsSectionCount AS INTEGER;
        SET @ClsSectMeetingID = 'Exception';
        
        
        DECLARE @PUnchType AS INTEGER;

        SET @PUnchType = (
                           SELECT   TCSPunchType
                           FROM     dbo.arTimeClockSpecialCode
                           WHERE    TCSpecialCode = @PunchSpecialCode
                                    AND CampusId = @CampusID
                         );

        IF @PUnchType = 1
            BEGIN


                SET @ClsSectMeetingCount = (
                                             SELECT COUNT(DISTINCT CSM.ClsSectMeetingId)
                                             FROM   dbo.arClsSectMeetings CSM
                                                   ,arTimeClockSpecialCode TCSP
                                                   ,dbo.arClassSections CS
                                                   ,arResults AR
                                                   ,dbo.syPeriods Sy
                                                   ,syPeriodsWorkDays SYWD
                                                   ,dbo.plWorkDays WD 
                                            --,
                                            --dbo.cmTimeInterval C1 ,
                                            --dbo.cmTimeInterval C2
                                             WHERE  CS.ClsSectionId = CSM.ClsSectionId
                                                    AND CSM.ClsSectionId = AR.TestId
                                                    AND StuEnrollId = @StuEnrollId
                                                    AND CS.StartDate <= @punchDate
                                                    AND CS.EndDate >= @punchDate
                                                    AND TCSP.InstructionTypeId = CSM.InstructionTypeID
                                                    AND CSM.StartDate <= @punchDate
                                                    AND CSM.EndDate >= @punchDate
                                                    AND Sy.PeriodId = CSM.PeriodId
                                            --AND C1.TimeIntervalId = Sy.StartTimeId
                                            --AND C2.TimeIntervalId = Sy.EndTimeId
                                                    AND Sy.PeriodId = SYWD.PeriodId
                                                    AND SYWD.WorkDayId = WD.WorkDaysId
                                            --AND @punchTime >= CONVERT(DATETIME, CONVERT(VARCHAR, DATEPART(hour,
                                            --                  DATEADD(minute,
                                            --                  -10,
                                            --                  CONVERT(DATETIME, c1.TimeIntervalDescrip, 108))))
                                            --+ ':'
                                            --+ CONVERT(VARCHAR, DATEPART(minute,
                                            --                  DATEADD(minute,
                                            --                  -10,
                                            --                  CONVERT(DATETIME, c1.TimeIntervalDescrip, 108)))), 1)
                                            --AND @punchTime <= CONVERT(DATETIME, CONVERT(VARCHAR, DATEPART(hour,
                                            --                  DATEADD(minute,
                                            --          10,
                                            --                  CONVERT(DATETIME, c1.TimeIntervalDescrip, 108))))
                                            --+ ':'
                                            --+ CONVERT(VARCHAR, DATEPART(minute,
                                            --                  DATEADD(minute,
                                            --                  10,
                                            --                  CONVERT(DATETIME, c1.TimeIntervalDescrip, 108)))), 1)
                                                    AND WD.WorkDaysDescrip = @PunchDay
                                                    AND TCSP.TCSpecialCode = @PunchSpecialCode
                                                    AND TCSP.CampusId = @CampusID
                                           );
                                   
            END;
        ELSE
            BEGIN

                SET @ClsSectMeetingCount = (
                                             SELECT COUNT(DISTINCT CSM.ClsSectMeetingId)
                                             FROM   dbo.arClsSectMeetings CSM
                                                   ,arTimeClockSpecialCode TCSP
                                                   ,dbo.arClassSections CS
                                                   ,arResults AR
                                                   ,dbo.syPeriods Sy
                                                   ,syPeriodsWorkDays SYWD
                                                   ,dbo.plWorkDays WD
                                            -- ,
                                            --dbo.cmTimeInterval C1 ,
                                            --dbo.cmTimeInterval C2
                                             WHERE  CS.ClsSectionId = CSM.ClsSectionId
                                                    AND CSM.ClsSectionId = AR.TestId
                                                    AND StuEnrollId = @StuEnrollId
                                                    AND CS.StartDate <= @punchDate
                                                    AND CS.EndDate >= @punchDate
                                                    AND TCSP.InstructionTypeId = CSM.InstructionTypeID
                                                    AND CSM.StartDate <= @punchDate
                                                    AND CSM.EndDate >= @punchDate
                                                    AND Sy.PeriodId = CSM.PeriodId
                                            --AND C1.TimeIntervalId = Sy.StartTimeId
                                            --AND C2.TimeIntervalId = Sy.EndTimeId
                                                    AND Sy.PeriodId = SYWD.PeriodId
                                                    AND SYWD.WorkDayId = WD.WorkDaysId
                                            --AND @punchTime >= CONVERT(DATETIME, CONVERT(VARCHAR, DATEPART(hour,
                                            --                  DATEADD(minute,
                                            --                  -10,
                                            --                  CONVERT(DATETIME, c2.TimeIntervalDescrip, 108))))
                                            --+ ':'
                                            --+ CONVERT(VARCHAR, DATEPART(minute,
                                            --                  DATEADD(minute,
                                            --                  -10,
                                            --                  CONVERT(DATETIME, c2.TimeIntervalDescrip, 108)))), 1)
                                            --AND @punchTime <= CONVERT(DATETIME, CONVERT(VARCHAR, DATEPART(hour,
                                            --                  DATEADD(minute,
                                            --                  10,
                                            --                  CONVERT(DATETIME, c2.TimeIntervalDescrip, 108))))
                                            --+ ':'
                                            --+ CONVERT(VARCHAR, DATEPART(minute,
                                            --             DATEADD(minute,
                                            --                  10,
                                            --                  CONVERT(DATETIME, c2.TimeIntervalDescrip, 108)))), 1)
                                                    AND WD.WorkDaysDescrip = @PunchDay
                                                    AND TCSP.TCSpecialCode = @PunchSpecialCode
                                                    AND TCSP.CampusId = @CampusID
                                           );
        
        
            END;
                                           
        
        IF @ClsSectMeetingCount = 0
            BEGIN
     --   SELECT  'No Meeting'
                SET @ClsSectionCount = (
                                         SELECT COUNT(DISTINCT CSM.ClsSectionId)
                                         FROM   dbo.arClsSectMeetings CSM
                                               ,arTimeClockSpecialCode TCSP
                                               ,dbo.arClassSections CS
                                               ,arResults AR
                                               ,dbo.syPeriods Sy
                                               ,syPeriodsWorkDays SYWD
                                               ,dbo.plWorkDays WD
                                                -- ,
                                                --dbo.cmTimeInterval C1 ,
                                                --dbo.cmTimeInterval C2
                                                -- ,
                                                --dbo.arClsSectionTimeClockPolicy CSTCP
                                         WHERE  CS.ClsSectionId = CSM.ClsSectionId
                                                AND CSM.ClsSectionId = AR.TestId
                                                AND StuEnrollId = @StuEnrollId
                                                AND CS.StartDate <= @punchDate
                                                AND CS.EndDate >= @punchDate
                                                AND TCSP.InstructionTypeId = CSM.InstructionTypeID
                                                AND CSM.StartDate <= @punchDate
                                                AND CSM.EndDate >= @punchDate
                                                AND Sy.PeriodId = CSM.PeriodId
                                                --AND C1.TimeIntervalId = Sy.StartTimeId
                                                --AND C2.TimeIntervalId = Sy.EndTimeId
                                                AND Sy.PeriodId = SYWD.PeriodId
                                                AND SYWD.WorkDayId = WD.WorkDaysId
                                                AND WD.WorkDaysDescrip = @PunchDay
                                                AND TCSP.TCSpecialCode = @PunchSpecialCode
                                              --  AND CStcp.ClsSectionId = CS.ClsSectionId
                                              --  AND CSTCP.AllowExtraHours = 1
                                                AND TCSP.CampusId = @CampusID
                                       );
    
    
                IF @ClsSectionCount = 0
                    BEGIN
                        SET @ClsSectMeetingID = 'Exception';
                    END;
    
                IF @ClsSectionCount = 1
                    BEGIN
                        SET @ClsSectMeetingID = (
                                                  SELECT TOP 1
                                                            CSM.ClsSectMeetingId
                                                  FROM      dbo.arClsSectMeetings CSM
                                                           ,arTimeClockSpecialCode TCSP
                                                           ,dbo.arClassSections CS
                                                           ,arResults AR
                                                           ,dbo.syPeriods Sy
                                                           ,syPeriodsWorkDays SYWD
                                                           ,dbo.plWorkDays WD
                                                            -- ,
                                                            --dbo.cmTimeInterval C1 ,
                                                            --dbo.cmTimeInterval C2 ,
                                                         --   dbo.arClsSectionTimeClockPolicy CSTCP
                                                  WHERE     CS.ClsSectionId = CSM.ClsSectionId
                                                            AND CSM.ClsSectionId = AR.TestId
                                                            AND StuEnrollId = @StuEnrollId
                                                            AND CS.StartDate <= @punchDate
                                                            AND CS.EndDate >= @punchDate
                                                            AND TCSP.InstructionTypeId = CSM.InstructionTypeID
                                                            AND CSM.StartDate <= @punchDate
                                                            AND CSM.EndDate >= @punchDate
                                                            AND Sy.PeriodId = CSM.PeriodId
                                                            --AND C1.TimeIntervalId = Sy.StartTimeId
                                                            --AND C2.TimeIntervalId = Sy.EndTimeId
                                                            AND Sy.PeriodId = SYWD.PeriodId
                                                            AND SYWD.WorkDayId = WD.WorkDaysId
                                                            AND WD.WorkDaysDescrip = @PunchDay
                                                            AND TCSP.TCSpecialCode = @PunchSpecialCode
                                                            --AND CStcp.ClsSectionId = CS.ClsSectionId
                                                            --AND CSTCP.AllowExtraHours = 1
                                                            AND TCSP.CampusId = @CampusID
                                                );
                    END;
    
                IF @ClsSectionCount > 1
                    BEGIN
                        SET @ClsSectMeetingID = 'Exception';
    
                    END;
    
            END;
     
        IF @ClsSectMeetingCount = 1
            BEGIN
     
                IF @PUnchType = 1
                    BEGIN
     
     
     
                        SET @ClsSectMeetingID = (
                                                  SELECT  DISTINCT
                                                            CSM.ClsSectMeetingId
                                                  FROM      dbo.arClsSectMeetings CSM
                                                           ,arTimeClockSpecialCode TCSP
                                                           ,dbo.arClassSections CS
                                                           ,arResults AR
                                                           ,dbo.syPeriods Sy
                                                           ,syPeriodsWorkDays SYWD
                                                           ,dbo.plWorkDays WD
                                                    -- ,
                                                    --dbo.cmTimeInterval C1 ,
                                                    --dbo.cmTimeInterval C2
                                                  WHERE     CS.ClsSectionId = CSM.ClsSectionId
                                                            AND CSM.ClsSectionId = AR.TestId
                                                            AND StuEnrollId = @StuEnrollId
                                                            AND CS.StartDate <= @punchDate
                                                            AND CS.EndDate >= @punchDate
                                                            AND TCSP.InstructionTypeId = CSM.InstructionTypeID
                                                            AND CSM.StartDate <= @punchDate
                                                            AND CSM.EndDate >= @punchDate
                                                            AND Sy.PeriodId = CSM.PeriodId
                                                    --AND C1.TimeIntervalId = Sy.StartTimeId
                                                    --AND C2.TimeIntervalId = Sy.EndTimeId
                                                            AND Sy.PeriodId = SYWD.PeriodId
                                                            AND SYWD.WorkDayId = WD.WorkDaysId
                                                    --AND @punchTime >= CONVERT(DATETIME, CONVERT(VARCHAR, DATEPART(hour,
                                                    --          DATEADD(minute,
                                                    --          -10,
                                                    --          CONVERT(DATETIME, c1.TimeIntervalDescrip, 108))))
                                                    --+ ':'
                                                    --+ CONVERT(VARCHAR, DATEPART(minute,
                                                    --          DATEADD(minute,
                                                    --          -10,
                                                    --          CONVERT(DATETIME, c1.TimeIntervalDescrip, 108)))), 1)
                                                    --AND @punchTime <= CONVERT(DATETIME, CONVERT(VARCHAR, DATEPART(hour,
                                                    --          DATEADD(minute,
                                                    --          10,
                                                    --          CONVERT(DATETIME, c1.TimeIntervalDescrip, 108))))
                                                    --+ ':'
                                                    --+ CONVERT(VARCHAR, DATEPART(minute,
                                                    --          DATEADD(minute,
                                                    --          10,
                                                    --          CONVERT(DATETIME, c1.TimeIntervalDescrip, 108)))), 1)
                                                            AND WD.WorkDaysDescrip = @PunchDay
                                                            AND TCSP.TCSpecialCode = @PunchSpecialCode
                                                            AND TCSP.CampusId = @CampusID
                                                );
                    END;
                ELSE
                    BEGIN
                        SET @ClsSectMeetingID = (
                                                  SELECT  DISTINCT
                                                            CSM.ClsSectMeetingId
                                                  FROM      dbo.arClsSectMeetings CSM
                                                           ,arTimeClockSpecialCode TCSP
                                                           ,dbo.arClassSections CS
                                                           ,arResults AR
                                                           ,dbo.syPeriods Sy
                                                           ,syPeriodsWorkDays SYWD
                                                           ,dbo.plWorkDays WD 
                                                    --,
                                                    --dbo.cmTimeInterval C1 ,
                                                    --dbo.cmTimeInterval C2
                                                  WHERE     CS.ClsSectionId = CSM.ClsSectionId
                                                            AND CSM.ClsSectionId = AR.TestId
                                                            AND StuEnrollId = @StuEnrollId
                                                            AND CS.StartDate <= @punchDate
                                                            AND CS.EndDate >= @punchDate
                                                            AND TCSP.InstructionTypeId = CSM.InstructionTypeID
                                                            AND CSM.StartDate <= @punchDate
                                                            AND CSM.EndDate >= @punchDate
                                                            AND Sy.PeriodId = CSM.PeriodId
                                                    --AND C1.TimeIntervalId = Sy.StartTimeId
                                                    --AND C2.TimeIntervalId = Sy.EndTimeId
                                                            AND Sy.PeriodId = SYWD.PeriodId
                                                            AND SYWD.WorkDayId = WD.WorkDaysId
                                                    --AND @punchTime >= CONVERT(DATETIME, CONVERT(VARCHAR, DATEPART(hour,
                                                    --          DATEADD(minute,
                                                    --          -10,
                                                    --          CONVERT(DATETIME, c2.TimeIntervalDescrip, 108))))
                                                    --+ ':'
                                                    --+ CONVERT(VARCHAR, DATEPART(minute,
                                                    --          DATEADD(minute,
                                                    --          -10,
                                                    --          CONVERT(DATETIME, c2.TimeIntervalDescrip, 108)))), 1)
                                                    --AND @punchTime <= CONVERT(DATETIME, CONVERT(VARCHAR, DATEPART(hour,
                                                    --          DATEADD(minute,
                                                    --          10,
                                                    --          CONVERT(DATETIME, c2.TimeIntervalDescrip, 108))))
                                                    --+ ':'
                                                    --+ CONVERT(VARCHAR, DATEPART(minute,
                                                    --          DATEADD(minute,
                                                    --          10,
                                                    --          CONVERT(DATETIME, c2.TimeIntervalDescrip, 108)))), 1)
                                                            AND WD.WorkDaysDescrip = @PunchDay
                                                            AND TCSP.TCSpecialCode = @PunchSpecialCode
                                                            AND TCSP.CampusId = @CampusID
                                                );
  
     
     
     
                    END;
     
         
   --  SELECT '1 Meeting'
            END;
     
        IF @ClsSectMeetingCount > 1
            BEGIN
                SET @ClsSectMeetingID = 'Exception';
   --  SELECT 'Exception'   
            END;
     
        RETURN @ClsSectMeetingID;  
    END;




GO
