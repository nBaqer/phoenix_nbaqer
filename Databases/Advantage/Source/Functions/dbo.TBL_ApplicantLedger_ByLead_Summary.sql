SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE FUNCTION [dbo].[TBL_ApplicantLedger_ByLead_Summary]
    (
     @CampusId UNIQUEIDENTIFIER
    ,@TransStartDate DATE
    ,@TransEndDate DATE
    )
RETURNS TABLE
AS
RETURN
    (
      SELECT    LT.TransactionId AS TransactionId
               ,LT.TransAmount AS TransactionAmount
               ,LT.LeadId
               ,(
                  SELECT    L.Firstname + ' ' + L.LastName
                ) AS LeadName
               ,CASE WHEN LT.Voided = 1 THEN 'Payment (Voided)'
                     ELSE CASE WHEN LT.TransTypeId IN ( 1,2 )
                                    AND (
                                          LT.ReversalReason IS NOT NULL
                                          OR LT.TransDescrip LIKE '%-Reversed'
                                        ) THEN 'Payment (Reversed)'
                               ELSE CASE WHEN LT.TransTypeId IN ( 1 )
                                              AND LT.TransAmount < 0
                                              AND LT.Voided = 0 THEN 'Charge (Adjusted)'
                                         ELSE CASE WHEN LT.TransTypeId IN ( 1 )
                                                        AND LT.TransAmount > 0
                                                        AND LT.Voided = 0 THEN 'Payment (Adjusted)'
                                                   ELSE 'Payment'
                                              END
                                    END
                          END
                END AS TransactionType
      FROM      adLeadTransactions LT
      INNER JOIN adLeads L ON LT.LeadId = L.LeadId
      WHERE     LT.TransTypeId <> 0
                AND L.CampusId = @CampusId
                AND CONVERT(NVARCHAR,LT.CreatedDate,101) >= @TransStartDate
                AND CONVERT(NVARCHAR,LT.CreatedDate,101) <= @TransEndDate
		---GROUP BY L.Firstname,L.LastName, LT.Voided, LT.TransTypeId, LT.ReversalReason, LT.TransAmount,LT.TransDescrip 
    );



GO
