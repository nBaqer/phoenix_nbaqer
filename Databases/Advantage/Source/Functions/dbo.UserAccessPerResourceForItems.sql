SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- ===========================================
-- Author:Kimberly Befera FAME Inc
-- Create date: 8/22/2019
-- Description: Returns the campus groups the user has access edit to for the given page for the Items  
CREATE   FUNCTION [dbo].[UserAccessPerResourceForItems]
(
    @UserId UNIQUEIDENTIFIER,
    @CampusId UNIQUEIDENTIFIER,
    @ResourceID VARCHAR(50)
)
RETURNS @Results TABLE
(
    CampusId UNIQUEIDENTIFIER,
    CampGrpId UNIQUEIDENTIFIER,
    CampGrpDescrip VARCHAR(50),
    AccessLevel INT
)
AS
BEGIN

    DECLARE @HasAllAccess INT;
    DECLARE @IsSupport INT;
    SET @IsSupport = 0;
    SET @HasAllAccess = 0;
    SET @IsSupport =
    (
        SELECT COUNT(UserName)
        FROM dbo.syUsers
        WHERE UserId = @UserId
              AND IsAdvantageSuperUser = 1
    );
    SET @HasAllAccess =
    (
        SELECT COUNT(*)
        FROM syUsersRolesCampGrps UTCG
            JOIN syCampGrps CG
                ON CG.CampGrpId = UTCG.CampGrpId
        WHERE IsAllCampusGrp = 1
              AND UserId = @UserId
    );


    IF (@IsSupport) > 0
    BEGIN
        INSERT @Results
        SELECT DISTINCT
               CmpGrp.CampusId,
               CG.CampGrpId,
               CG.CampGrpDescrip,
               15 AS AccessLevel
        FROM syCampGrps CG
            JOIN dbo.syCmpGrpCmps CmpGrp
                ON CmpGrp.CampGrpId = CG.CampGrpId
            JOIN dbo.syCampuses CAMP
                ON CAMP.CampusId = CmpGrp.CampusId
        WHERE CmpGrp.CampusId = @CampusId
        ORDER BY CG.CampGrpDescrip;

    END;
    ELSE
    BEGIN
        INSERT @Results
        SELECT DISTINCT
               CGC.CampusId,
               CG.CampGrpId,
               CG.CampGrpDescrip,
               MAX(RRL.AccessLevel) AS AccessLevel
        FROM syCmpGrpCmps CGC
            JOIN syUsersRolesCampGrps URCG
                ON URCG.CampGrpId = CGC.CampGrpId
            JOIN syCampGrps CG
                ON CG.CampGrpId = CGC.CampGrpId
            JOIN syRlsResLvls RRL
                ON RRL.RoleId = URCG.RoleId
            JOIN syResources R
                ON R.ResourceID = RRL.ResourceID
        WHERE URCG.UserId = @UserId
              AND CGC.CampusId = @CampusId
              AND RRL.ResourceID = @ResourceID
        GROUP BY CGC.CampusId,
                 CG.CampGrpId,
                 CG.CampGrpDescrip
        ORDER BY CG.CampGrpDescrip;
    END;

    RETURN;
END;

GO
