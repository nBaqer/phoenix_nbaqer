SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		FAME Inc.
-- Create date: 6/11/2019
-- Description:	Gets grade format based on campus student enrollment is tied to 
-- =============================================
CREATE   FUNCTION [dbo].[GetGradesFormatGivenStudentEnrollId]
    (
        @StuEnrollId VARCHAR(50)
    )
RETURNS VARCHAR(50)
AS
    BEGIN
        RETURN ( LOWER(LTRIM(RTRIM((
                                   SELECT dbo.GetAppSettingValueByKeyName('GradesFormat'
                                                                         ,(
                                                                          SELECT CampusId
                                                                          FROM   dbo.arStuEnrollments
                                                                          WHERE  StuEnrollId = @StuEnrollId
                                                                          )
                                                                         )
                                   )
                                  )
                            )
                      )
               );
    END;
GO
