SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE FUNCTION [dbo].[GetLDAforAGivenStuEnrollId]
    (
     @StuEnrollId UNIQUEIDENTIFIER
	)
RETURNS DATETIME
AS
    BEGIN
        DECLARE @ReturnValue DATETIME;
        SELECT  @ReturnValue = MAX(LDA)
        FROM    (
                  SELECT TOP 1
                            LDA
                  FROM      arStuEnrollments
                  WHERE     StuEnrollId = @StuEnrollId
                  UNION ALL
                  SELECT    MAX(AttendedDate) AS LDA
                  FROM      arExternshipAttendance
                  WHERE     StuEnrollId = @StuEnrollId
                  UNION ALL
                  SELECT    MAX(MeetDate) AS LDA
                  FROM      atClsSectAttendance t1
                           ,arClassSections t2
                  WHERE     t1.ClsSectionId = t2.ClsSectionId
                            AND StuEnrollId = @StuEnrollId
                            AND Actual > 0
                            AND Actual <> 99.00
                            AND Actual <> 999.00
                            AND Actual <> 9999.00
                  UNION ALL
                  SELECT    MAX(RecordDate) AS LDA
                  FROM      arStudentClockAttendance
                  WHERE     StuEnrollId = @StuEnrollId
                            AND (
                                  ActualHours > 0
                                  AND ActualHours <> 99.00
                                  AND ActualHours <> 999.00
                                  AND ActualHours <> 9999.00
                                )
                  UNION ALL
                  SELECT    MAX(MeetDate) AS LDA
                  FROM      atConversionAttendance
                  WHERE     StuEnrollId = @StuEnrollId
                            AND (
                                  Actual > 0
                                  AND Actual <> 99.00
                                  AND Actual <> 999.00
                                  AND Actual <> 9999.00
                                )
                ) AS TR;


        RETURN @ReturnValue;
    END;

GO
