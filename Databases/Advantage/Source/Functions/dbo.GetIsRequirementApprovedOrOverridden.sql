SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ginzo, John
-- Create date: 11/30/2015
-- Description:	Get Whether requirement is approved
-- =============================================
CREATE FUNCTION [dbo].[GetIsRequirementApprovedOrOverridden]
    (
     @CurReqId UNIQUEIDENTIFIER
    ,@CurReqType VARCHAR(50)
    ,@LeadId UNIQUEIDENTIFIER
    )
RETURNS BIT
AS
    BEGIN
        DECLARE @returnVal BIT;
        SET @returnVal = 0;

        IF @CurReqType = 'Document'
            BEGIN
                IF EXISTS ( SELECT TOP 1
                                    LeadDocId
                            FROM    dbo.adLeadDocsReceived ld
                            INNER JOIN dbo.syDocStatuses dc ON ld.DocStatusId = dc.DocStatusId
                            WHERE   leadid = @LeadId
                                    AND DocumentId = @CurReqId
                                    AND (
                                          ISNULL(dc.SysDocStatusId,0) = 1
                                          OR ISNULL(Override,0) = 1
                                        ) )
                    BEGIN
                        SET @returnVal = 1;
                    END;
            END;
        ELSE
            IF @CurReqType = 'Test'
                BEGIN
                    IF EXISTS ( SELECT TOP 1
                                        LeadEntrTestId
                                FROM    dbo.adLeadEntranceTest
                                WHERE   leadid = @LeadId
                                        AND EntrTestId = @CurReqId
                                        AND (
                                              ISNULL(Pass,0) = 1
                                              OR ISNULL(Override,0) = 1
                                            ) )
                        BEGIN
                            SET @returnVal = 1;
                        END;
                END;
            ELSE
                IF @CurReqType = 'Interview'
                    OR @CurReqType = 'Event'
                    OR @CurReqType = 'Tour'
                    BEGIN
                        IF EXISTS ( SELECT TOP 1
                                            LeadReqId
                                    FROM    dbo.AdLeadReqsReceived
                                    WHERE   leadid = @LeadId
                                            AND DocumentId = @CurReqId
                                            AND (
                                                  ISNULL(IsApproved,0) = 1
                                                  OR ISNULL(Override,0) = 1
                                                ) )
                            BEGIN
                                SET @returnVal = 1;
                            END;
                    END;
                ELSE
                    IF @CurReqType = 'Fee'
                        BEGIN
                            IF EXISTS ( SELECT TOP 1
                                                adLeadTranReceivedId
                                        FROM    dbo.adLeadTranReceived
                                        WHERE   leadid = @LeadId
                                                AND DocumentId = @CurReqId
                                                AND (
                                                      ISNULL(IsApproved,0) = 1
                                                      OR ISNULL(Override,0) = 1
                                                    ) )
                                BEGIN
                                    SET @returnVal = 1;
                                END;
                        END;

        RETURN @returnVal;

    END;
GO
