SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[MaxDate]
    (
     @date1 DATE
    ,@date2 DATE
    )
RETURNS DATE
AS
    BEGIN
        DECLARE @ReturnDate DATE;
	
        IF @date1 < @date2
            BEGIN
                SET @ReturnDate = @date2;
            END;
        ELSE
            BEGIN
                SET @ReturnDate = @date1;
            END;
		 
        RETURN @ReturnDate;

    END;

GO
