SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[GetStuEnrollment](
@FirstName varchar(MAX),
@LastName varchar(MAX))
RETURNs TABLE
as
RETURN (
	SELECT e.StuEnrollId FROM adleads L LEFT JOIN dbo.arStuEnrollments E ON E.LeadId = L.LeadId
	WHERE L.FirstName = @FirstName AND L.LastName = @LastName
	
)
GO
