SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[GetOverallGPA] ( @StuEnrollId CHAR(36) )
RETURNS DECIMAL(18,2)
AS
    BEGIN
        DECLARE @returnval DECIMAL(18,2);
        DECLARE @gparunningtotal DECIMAL(18,2);
        DECLARE @numcourses INT;
        DECLARE @reqid UNIQUEIDENTIFIER;
        DECLARE @isingpa BIT;
        DECLARE @gpa DECIMAL(18,2);
        DECLARE @betterrepeats INT;
        DECLARE @startdate DATETIME;

        SET @gparunningtotal = 0.0;
        SET @numcourses = 0;

        DECLARE resultsCursor CURSOR
        FOR
            SELECT  cs.ReqId
                   ,gsd.IsInGPA
                   ,gsd.GPA
                   ,cs.StartDate
            FROM    dbo.arResults rs
                   ,dbo.arClassSections cs
                   ,dbo.arGradeSystemDetails gsd
                   ,dbo.arGradeSystems gs
            WHERE   rs.StuEnrollId = @StuEnrollId
                    AND rs.TestId = cs.ClsSectionId
                    AND rs.GrdSysDetailId = gsd.GrdSysDetailId
                    AND gsd.GrdSystemId = gs.GrdSystemId
            UNION
            SELECT  tg.ReqId
                   ,gsd.IsInGPA
                   ,gsd.GPA
                   ,tm.StartDate
            FROM    dbo.arTransferGrades tg
                   ,dbo.arGradeSystemDetails gsd
                   ,dbo.arGradeSystems gs
                   ,arTerm tm
            WHERE   tg.StuEnrollId = @StuEnrollId
                    AND tg.GrdSysDetailId = gsd.GrdSysDetailId
                    AND gsd.GrdSystemId = gs.GrdSystemId
                    AND tg.TermId = tm.TermId
            ORDER BY StartDate;

        OPEN resultsCursor;
        FETCH NEXT FROM resultsCursor
		INTO @reqid,@isingpa,@gpa,@startdate;

        WHILE @@FETCH_STATUS = 0
            BEGIN
                IF @isingpa = 1
                    BEGIN
						--The GradeCourseRepetitionsMethod is set to Best. Before we add anything to the running totals
						--we need to check if the student has taken the course and has a better grade. If so, we will not
						--add this record.
						--The student might take the course twice and fail it both times. 
						--First check against the arResults table
                        SELECT  @betterrepeats = ISNULL(COUNT(*),0)
                        FROM    dbo.arResults rs2
                               ,dbo.arClassSections cs2
                               ,dbo.arGradeSystemDetails gsd2
                               ,dbo.arGradeSystems gs2
                        WHERE   rs2.StuEnrollId = @StuEnrollId
                                AND rs2.TestId = cs2.ClsSectionId
                                AND rs2.GrdSysDetailId = gsd2.GrdSysDetailId
                                AND gsd2.GrdSystemId = gs2.GrdSystemId
                                AND cs2.ReqId = @reqid
                                AND gsd2.GPA >= @gpa
                                AND cs2.StartDate > @startdate;

						--If nothing is found in the arResults table then check also against the arTransferGrades table
                        IF @betterrepeats = 0
                            BEGIN
                                SELECT  @betterrepeats = ISNULL(COUNT(*),0)
                                FROM    dbo.arTransferGrades tg
                                       ,dbo.arGradeSystemDetails gsd
                                       ,dbo.arGradeSystems gs
                                       ,arTerm tm
                                WHERE   tg.StuEnrollId = @StuEnrollId
                                        AND tg.GrdSysDetailId = gsd.GrdSysDetailId
                                        AND gsd.GrdSystemId = gs.GrdSystemId
                                        AND tg.TermId = tm.TermId
                                        AND tg.ReqId = @reqid
                                        AND gsd.GPA >= @gpa
                                        AND tm.StartDate > @startdate;

                            END;


                        IF @betterrepeats = 0
                            BEGIN
                                SET @numcourses += 1;
                                SET @gparunningtotal += @gpa;
                            END;

                    END;


                FETCH NEXT FROM resultsCursor
				INTO @reqid,@isingpa,@gpa,@startdate;

            END;

        IF @numcourses > 0
            BEGIN
                SET @returnval = @gparunningtotal / @numcourses;			
            END;
        ELSE
            BEGIN
                SET @returnval = NULL;
            END;


        CLOSE resultsCursor;
        DEALLOCATE resultsCursor;

        RETURN @returnval;                      
    END;

GO
