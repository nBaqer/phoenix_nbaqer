SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[DoesEnrollmentHavePendingBalance]
    (
        @StuEnrollId UNIQUEIDENTIFIER
    )
RETURNS INT
AS
    BEGIN
        DECLARE @ReturnValue INT = 0;


        DECLARE @AmountOwe DECIMAL(18, 2) = (
                                            SELECT SUM(TransAmount)
                                            FROM   dbo.saTransactions
                                            WHERE  StuEnrollId = @StuEnrollId
                                                   AND Voided = 0
                                            );
        IF ( @AmountOwe > 0 )
            BEGIN
                SET @ReturnValue = 1;
            END;
        RETURN @ReturnValue;
    END;

GO
