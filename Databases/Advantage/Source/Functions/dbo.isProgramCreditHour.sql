SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE  FUNCTION [dbo].[isProgramCreditHour] ( @programId CHAR(36) )
RETURNS BIT
AS
    BEGIN
        DECLARE @returnValue BIT;
        DECLARE @acid INT;

        SET @acid = (
                      SELECT    ACId
                      FROM      arPrograms
                      WHERE     ProgId = @programId
                    );

        IF ( @acid <> 5 )
            BEGIN
                SET @returnValue = 1;
            END;	
        ELSE
            BEGIN
                SET @returnValue = 0;
            END;
        
        RETURN @returnValue;
    END;
GO
