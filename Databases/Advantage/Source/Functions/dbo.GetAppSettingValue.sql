SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[GetAppSettingValue]
    (
     @SettingId INTEGER
    ,@CampusId UNIQUEIDENTIFIER
	)
RETURNS VARCHAR(1000)
AS
    BEGIN
        DECLARE @ReturnValue AS VARCHAR(1000);
		
        SET @ReturnValue = (
                             SELECT Value
                             FROM   syConfigAppSetValues
                             WHERE  SettingId = @SettingId
                                    AND CampusId = @CampusId
                           );

        IF ( @ReturnValue IS NULL )
            BEGIN
                SET @ReturnValue = (
                                     SELECT Value
                                     FROM   syConfigAppSetValues
                                     WHERE  SettingId = @SettingId
                                            AND CampusId IS NULL
                                   );
            END;	
        RETURN @ReturnValue;
    END;


GO
