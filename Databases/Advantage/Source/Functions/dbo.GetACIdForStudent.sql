SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		FAME Inc.
-- Create date: 6/11/2019
-- Description:	Given student enrollment id, returns the ACId for the program version student is enrolled in 
-- =============================================
CREATE   FUNCTION [dbo].[GetACIdForStudent]
    (
        @EnrollmentId VARCHAR(50)
    )
RETURNS INT
AS
    BEGIN
        RETURN (
               SELECT TOP 1 ACId
               FROM   dbo.arStuEnrollments
               JOIN   dbo.arPrgVersions ON arPrgVersions.PrgVerId = arStuEnrollments.PrgVerId
               JOIN   dbo.arPrograms ON arPrograms.ProgId = arPrgVersions.ProgId
               WHERE  dbo.arStuEnrollments.StuEnrollId = @EnrollmentId
               );
    END;
GO
