SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Sweta, Thakkar
-- Create date: 05/21/2019
-- Description:	Determine if enrollment has attendance
-- =============================================
CREATE FUNCTION [dbo].[EnrollmentHasAttendance]
    (
        @StuEnrollId UNIQUEIDENTIFIER
    )
RETURNS VARCHAR(3)
AS
    BEGIN

        DECLARE @retVal VARCHAR(3);
        IF (
           SELECT SUM(ActualDays)
           FROM   dbo.syStudentAttendanceSummary
           WHERE  StuEnrollId = @StuEnrollId
                  AND ActualDays != 9999.0
                  AND ActualDays != 999.0
           ) > 0
            SET @retVal = 'Yes';
        ELSE
            SET @retVal = 'No';

        RETURN @retVal;

    END;
	
GO
