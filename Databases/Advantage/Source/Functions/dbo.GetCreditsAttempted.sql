SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[GetCreditsAttempted] ( @StuEnrollId CHAR(36) )
RETURNS INTEGER
AS
    BEGIN
        DECLARE @CreditsAttempted INTEGER;
        SELECT  @CreditsAttempted = SUM(RQ.Credits)
        FROM    arResults R
               ,arGradeSystemDetails GSD
               ,arClassSections CS
               ,arReqs RQ
        WHERE   R.GrdSysDetailId = GSD.GrdSysDetailId
                AND R.TestId = CS.ClsSectionId
                AND CS.ReqId = RQ.ReqId
                AND GSD.IsCreditsAttempted = 1
                AND R.StuEnrollId = @StuEnrollId;
        RETURN  COALESCE(@CreditsAttempted, 0);
    END;


GO
