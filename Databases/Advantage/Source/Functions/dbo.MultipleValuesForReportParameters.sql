SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[MultipleValuesForReportParameters]
    (
     @String VARCHAR(MAX)
    , /* input string */@Delimeter CHAR(1)
    ,   /* delimiter */@TrimSpace BIT
    )      /* kill whitespace? */
RETURNS @Table TABLE ( Val VARCHAR(MAX) )
AS
    BEGIN
        DECLARE @Val VARCHAR(MAX);
        WHILE LEN(@String) > 0
            BEGIN
                SET @Val = LEFT(@String,ISNULL(NULLIF(CHARINDEX(@Delimeter,@String) - 1,-1),LEN(@String)));
                SET @String = SUBSTRING(@String,ISNULL(NULLIF(CHARINDEX(@Delimeter,@String),0),LEN(@String)) + 1,LEN(@String));
                IF @TrimSpace = 1
                    SET @Val = LTRIM(RTRIM(@Val));
                INSERT  INTO @Table
                        ( Val )
                VALUES  ( @Val );
            END;
        RETURN;
    END;

GO
