SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[GetEnrollmentHours]
    (
     @StuEnrollId UNIQUEIDENTIFIER
    ,@TermId UNIQUEIDENTIFIER
	)
RETURNS INTEGER
AS
    BEGIN
        DECLARE @Return_Value INTEGER;
        SELECT  @Return_Value = SUM(RQ.Hours)
        FROM    arResults R
               ,arClassSections CS
               ,arReqs RQ
        WHERE   R.TestId = CS.ClsSectionId
                AND CS.ReqId = RQ.ReqId
                AND R.StuEnrollId = @StuEnrollId
                AND CS.TermId = @TermId; 
        RETURN @Return_Value;
    END;

GO
