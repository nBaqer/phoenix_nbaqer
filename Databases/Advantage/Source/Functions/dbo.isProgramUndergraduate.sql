SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

--=================================================================================================    
-- isProgramUndergraduate    
--=================================================================================================    
CREATE FUNCTION [dbo].[isProgramUndergraduate]
    (
        @programId CHAR(36)
       ,@allDegree CHAR(1) = 'D' -- D is default for 2 yrs, B is bachelors, A is all grad(last column), 'O' is for others (B,A,O is for 4 yrs)
    )
RETURNS BIT
AS
    BEGIN
        DECLARE @returnValue BIT;
        DECLARE @ipedsvalue INT;

        SET @returnValue = 0;
        SET @ipedsvalue = (
                          SELECT IPEDSValue
                          FROM   arPrograms pg
                                ,dbo.arProgCredential pc
                          WHERE  pg.CredentialLvlId = pc.CredentialId
                                 AND pg.ProgId = @programId
                          );
        IF ( @allDegree = 'D' ) -- this means the default behaivour   for 2 yrs and < 2 yrs 
            BEGIN

                IF (
                   @ipedsvalue = 154
                   OR @ipedsvalue = 155
                   OR @ipedsvalue = 156
                   OR @ipedsvalue = 157
                   )
                    BEGIN
                        SET @returnValue = 1;
                    END;
                ELSE
                    BEGIN
                        SET @returnValue = 0;
                    END;

            END;
        ELSE IF ( @allDegree = 'O' ) -- this means the default behaivour    
                 BEGIN

                     IF (
                        @ipedsvalue = 154
                        OR @ipedsvalue = 155
                        OR @ipedsvalue = 156
                        )
                         BEGIN
                             SET @returnValue = 1;
                         END;
                     ELSE
                         BEGIN
                             SET @returnValue = 0;
                         END;

                 END;
        ELSE IF ( @allDegree = 'B' ) -- this means that we have to consider only Bachelors   
                 BEGIN
                     IF ( @ipedsvalue = 157 )
                         BEGIN
                             SET @returnValue = 1;
                         END;
                     ELSE
                         BEGIN
                             SET @returnValue = 0;
                         END;
                 END;
        ELSE IF ( @allDegree = 'A' ) -- this means that we have to consider for all the grades (done for 4 yr Pell report)    
                 BEGIN
                     IF (
                        @ipedsvalue = 160
                        OR @ipedsvalue = 157
                        OR @ipedsvalue = 158 -- including the calculation for Masters    
                        OR @ipedsvalue = 159 -- including the calculation for Doctoral    
                        )
                         BEGIN
                             SET @returnValue = 1;
                         END;
                     ELSE
                         BEGIN
                             SET @returnValue = 0;
                         END;
                 END;
        RETURN @returnValue;
    END;
--=================================================================================================    
-- END  --  isProgramUndergraduate    
--================================================================================================= 
GO
