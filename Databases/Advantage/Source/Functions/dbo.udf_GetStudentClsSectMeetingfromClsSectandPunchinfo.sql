SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE FUNCTION [dbo].[udf_GetStudentClsSectMeetingfromClsSectandPunchinfo]
    (
     @Punch DATETIME
    ,@PunchSpecialCode AS VARCHAR(5)
    ,@ClsSectionId AS VARCHAR(4000)
    ,@CampusID AS VARCHAR(50)
    )
RETURNS VARCHAR(50)
AS
    BEGIN    

--        SET @Punch = '08/29/2011 12:50:00'
        DECLARE @punchDate AS DATETIME;
        DECLARE @PunchDay AS VARCHAR(5);
        SET @PunchDay = (
                          SELECT    dbo.udf_DayOfWeek(@Punch)
                        ); 
        SET @punchDate = CONVERT(VARCHAR,@Punch,101);
        DECLARE @punchTime AS DATETIME;

        SET @punchTime = CONVERT(VARCHAR,@Punch,108);
	  --  DECLARE @PunchSpecialCode AS VARCHAR(5)

        DECLARE @ClsSectMeetingID AS VARCHAR(50);
        DECLARE @ClsSectmeetingsCount AS INTEGER;
        DECLARE @PUnchType AS INTEGER;

        SET @PUnchType = (
                           SELECT TOP 1
                                    TCSPunchType
                           FROM     dbo.arTimeClockSpecialCode
                           WHERE    TCSpecialCode = @PunchSpecialCode
                                    AND CampusId = @CampusID
                         );

        IF @PUnchType = 1
            BEGIN
	

                SET @ClsSectmeetingsCount = (
                                              SELECT    COUNT(DISTINCT CSM.ClsSectMeetingId)
                                              FROM      dbo.arClsSectMeetings CSM
                                                       ,arTimeClockSpecialCode TCSP
                                                       ,
				--dbo.arClassSections CS ,
				--arresults AR ,
                                                        dbo.syPeriods Sy
                                                       ,syPeriodsWorkDays SYWD
                                                       ,dbo.plWorkDays WD 
												--,
												--dbo.cmTimeInterval C1 ,
												--dbo.cmTimeInterval C2
                                              WHERE     CSM.ClsSectionId IN ( @ClsSectionId )
			  --  AND CSM.ClsSectionId = AR.TestId
			 --   AND StuEnrollId = '793A898B-634F-4CDB-8C8C-DD47788A1699'
				--AND CS.StartDate <= @PunchDate
				--AND CS.EndDate >= @PunchDate
                                                        AND TCSP.InstructionTypeId = CSM.InstructionTypeID
                                                        AND CSM.StartDate <= @punchDate
                                                        AND CSM.EndDate >= @punchDate
                                                        AND Sy.PeriodId = CSM.PeriodId
												--AND C1.TimeIntervalId = Sy.StartTimeId
												--AND C2.TimeIntervalId = Sy.EndTimeId
                                                        AND Sy.PeriodId = SYWD.PeriodId
                                                        AND SYWD.WorkDayId = WD.WorkDaysId
												--AND @punchTime >= CONVERT(DATETIME, CONVERT(VARCHAR, DATEPART(hour,
												--              DATEADD(minute,
												--              -10,
												--              CONVERT(DATETIME, c1.TimeIntervalDescrip, 108))))
												--+ ':'
												--+ CONVERT(VARCHAR, DATEPART(minute,
												--              DATEADD(minute,
												--              -10,
												--              CONVERT(DATETIME, c1.TimeIntervalDescrip, 108)))), 1)
												--AND @punchTime <= CONVERT(DATETIME, CONVERT(VARCHAR, DATEPART(hour,
												--              DATEADD(minute,
												--              10,
												--              CONVERT(DATETIME, c1.TimeIntervalDescrip, 108))))
												--+ ':'
												--+ CONVERT(VARCHAR, DATEPART(minute,
												--              DATEADD(minute,
												--              10,
												--              CONVERT(DATETIME, c1.TimeIntervalDescrip, 108)))), 1)
                                                        AND WD.WorkDaysDescrip = @PunchDay
                                                        AND TCSP.TCSpecialCode = @PunchSpecialCode
                                                        AND TCSP.CampusId = @CampusID
                                            );
				
            END;
        ELSE
            BEGIN
                SET @ClsSectmeetingsCount = (
                                              SELECT    COUNT(DISTINCT CSM.ClsSectMeetingId)
                                              FROM      dbo.arClsSectMeetings CSM
                                                       ,arTimeClockSpecialCode TCSP
                                                       ,
				--dbo.arClassSections CS ,
				--arresults AR ,
                                                        dbo.syPeriods Sy
                                                       ,syPeriodsWorkDays SYWD
                                                       ,dbo.plWorkDays WD 
												--,
												--dbo.cmTimeInterval C1 ,
												--dbo.cmTimeInterval C2
                                              WHERE     CSM.ClsSectionId IN ( @ClsSectionId )
			  --  AND CSM.ClsSectionId = AR.TestId
			 --   AND StuEnrollId = '793A898B-634F-4CDB-8C8C-DD47788A1699'
				--AND CS.StartDate <= @PunchDate
				--AND CS.EndDate >= @PunchDate
                                                        AND TCSP.InstructionTypeId = CSM.InstructionTypeID
                                                        AND CSM.StartDate <= @punchDate
                                                        AND CSM.EndDate >= @punchDate
                                                        AND Sy.PeriodId = CSM.PeriodId
												--AND C1.TimeIntervalId = Sy.StartTimeId
												--AND C2.TimeIntervalId = Sy.EndTimeId
                                                        AND Sy.PeriodId = SYWD.PeriodId
                                                        AND SYWD.WorkDayId = WD.WorkDaysId
												--AND @punchTime >= CONVERT(DATETIME, CONVERT(VARCHAR, DATEPART(hour,
												--              DATEADD(minute,
												--              -10,
												--              CONVERT(DATETIME, c2.TimeIntervalDescrip, 108))))
												--+ ':'
												--+ CONVERT(VARCHAR, DATEPART(minute,
												--              DATEADD(minute,
												--              -10,
												--              CONVERT(DATETIME, c2.TimeIntervalDescrip, 108)))), 1)
												--AND @punchTime <= CONVERT(DATETIME, CONVERT(VARCHAR, DATEPART(hour,
												--              DATEADD(minute,
												--              10,
												--              CONVERT(DATETIME, c2.TimeIntervalDescrip, 108))))
												--+ ':'
												--+ CONVERT(VARCHAR, DATEPART(minute,
												--              DATEADD(minute,
												--              10,
												--              CONVERT(DATETIME, c2.TimeIntervalDescrip, 108)))), 1)
                                                        AND WD.WorkDaysDescrip = @PunchDay
                                                        AND TCSP.TCSpecialCode = @PunchSpecialCode
                                                        AND TCSP.CampusId = @CampusID
                                            );
				
		
            END;
		
		
        IF @ClsSectmeetingsCount = 0
            BEGIN
                DECLARE @AllowExtraHours AS BIT;
				--SET @AllowExtraHours = ( SELECT AllowExtraHours
				--                         FROM   dbo.arClsSectionTimeClockPolicy
				--                         WHERE  ClsSectionId = @ClsSectionId
				--                       )
                SET @AllowExtraHours = 1;
			--  SELECT  'if Allow Extra Hours Then, Select Top 1 ClsSectmeeting else Exception'
                IF @AllowExtraHours = 1
                    BEGIN
                        SET @ClsSectmeetingsCount = (
                                                      SELECT    COUNT(DISTINCT ClsSectMeetingId) ClsSectmeetingID
                                                      FROM      dbo.arClsSectMeetings
                                                      WHERE     ClsSectionId = @ClsSectionId
                                                    );
						
                        IF @ClsSectmeetingsCount = 0
                            BEGIN
                                SET @ClsSectMeetingID = 'Exception';
                            END;
                        ELSE
                            BEGIN
                                SET @ClsSectMeetingID = (
                                                          SELECT TOP 1
                                                                    ( ClsSectMeetingId ) ClsSectmeetingID
                                                          FROM      dbo.arClsSectMeetings
                                                          WHERE     ClsSectionId = @ClsSectionId
                                                        );
						
                            END;
						
						
						
						
                    END;
                ELSE
                    BEGIN
                        SET @ClsSectMeetingID = ( 'Exception' );
                    END;
			 
            END;
        ELSE
            BEGIN
		
		
                IF @PUnchType = 1
                    BEGIN
		
		
                        SET @ClsSectMeetingID = (
                                                  SELECT TOP 1
                                                            CSM.ClsSectMeetingId
                                                  FROM      dbo.arClsSectMeetings CSM
                                                           ,arTimeClockSpecialCode TCSP
                                                           ,
				--dbo.arClassSections CS ,
				--arresults AR ,
                                                            dbo.syPeriods Sy
                                                           ,syPeriodsWorkDays SYWD
                                                           ,dbo.plWorkDays WD 
													--,
													--dbo.cmTimeInterval C1 ,
													--dbo.cmTimeInterval C2
                                                  WHERE     CSM.ClsSectionId IN ( @ClsSectionId )
			  --  AND CSM.ClsSectionId = AR.TestId
			 --   AND StuEnrollId = '793A898B-634F-4CDB-8C8C-DD47788A1699'
				--AND CS.StartDate <= @PunchDate
				--AND CS.EndDate >= @PunchDate
                                                            AND TCSP.InstructionTypeId = CSM.InstructionTypeID
                                                            AND CSM.StartDate <= @punchDate
                                                            AND CSM.EndDate >= @punchDate
                                                            AND Sy.PeriodId = CSM.PeriodId
													--AND C1.TimeIntervalId = Sy.StartTimeId
													--AND C2.TimeIntervalId = Sy.EndTimeId
                                                            AND Sy.PeriodId = SYWD.PeriodId
                                                            AND SYWD.WorkDayId = WD.WorkDaysId
													--AND @punchTime >= CONVERT(DATETIME, CONVERT(VARCHAR, DATEPART(hour,
													--          DATEADD(minute,
													--          -10,
													--          CONVERT(DATETIME, c1.TimeIntervalDescrip, 108))))
													--+ ':'
													--+ CONVERT(VARCHAR, DATEPART(minute,
													--          DATEADD(minute,
													--          -10,
													--          CONVERT(DATETIME, c1.TimeIntervalDescrip, 108)))), 1)
													--AND @punchTime <= CONVERT(DATETIME, CONVERT(VARCHAR, DATEPART(hour,
													--          DATEADD(minute,
													--          10,
													--          CONVERT(DATETIME, c1.TimeIntervalDescrip, 108))))
													--+ ':'
													--+ CONVERT(VARCHAR, DATEPART(minute,
													--          DATEADD(minute,
													--          10,
													--          CONVERT(DATETIME, c1.TimeIntervalDescrip, 108)))), 1)
                                                            AND WD.WorkDaysDescrip = @PunchDay
                                                            AND TCSP.TCSpecialCode = @PunchSpecialCode
                                                            AND TCSP.CampusId = @CampusID
                                                );
				
                    END;
                ELSE
                    BEGIN
				
				
                        SET @ClsSectMeetingID = (
                                                  SELECT TOP 1
                                                            CSM.ClsSectMeetingId
                                                  FROM      dbo.arClsSectMeetings CSM
                                                           ,arTimeClockSpecialCode TCSP
                                                           ,
				--dbo.arClassSections CS ,
				--arresults AR ,
                                                            dbo.syPeriods Sy
                                                           ,syPeriodsWorkDays SYWD
                                                           ,dbo.plWorkDays WD
													-- ,
													--dbo.cmTimeInterval C1 ,
													--dbo.cmTimeInterval C2
                                                  WHERE     CSM.ClsSectionId IN ( @ClsSectionId )
			  --  AND CSM.ClsSectionId = AR.TestId
			 --   AND StuEnrollId = '793A898B-634F-4CDB-8C8C-DD47788A1699'
				--AND CS.StartDate <= @PunchDate
				--AND CS.EndDate >= @PunchDate
                                                            AND TCSP.InstructionTypeId = CSM.InstructionTypeID
                                                            AND CSM.StartDate <= @punchDate
                                                            AND CSM.EndDate >= @punchDate
                                                            AND Sy.PeriodId = CSM.PeriodId
													--AND C1.TimeIntervalId = Sy.StartTimeId
													--AND C2.TimeIntervalId = Sy.EndTimeId
                                                            AND Sy.PeriodId = SYWD.PeriodId
                                                            AND SYWD.WorkDayId = WD.WorkDaysId
													--AND @punchTime >= CONVERT(DATETIME, CONVERT(VARCHAR, DATEPART(hour,
													--          DATEADD(minute,
													--          -10,
													--          CONVERT(DATETIME, c2.TimeIntervalDescrip, 108))))
													--+ ':'
													--+ CONVERT(VARCHAR, DATEPART(minute,
													--          DATEADD(minute,
													--          -10,
													--          CONVERT(DATETIME, c2.TimeIntervalDescrip, 108)))), 1)
													--AND @punchTime <= CONVERT(DATETIME, CONVERT(VARCHAR, DATEPART(hour,
													--          DATEADD(minute,
													--          10,
													--          CONVERT(DATETIME, c2.TimeIntervalDescrip, 108))))
													--+ ':'
													--+ CONVERT(VARCHAR, DATEPART(minute,
													--          DATEADD(minute,
													--          10,
													--          CONVERT(DATETIME, c2.TimeIntervalDescrip, 108)))), 1)
                                                            AND WD.WorkDaysDescrip = @PunchDay
                                                            AND TCSP.TCSpecialCode = @PunchSpecialCode
                                                            AND TCSP.CampusId = @CampusID
                                                );
				
				
				
                    END;
				
				
				
				
                RETURN @ClsSectMeetingID;
            END;   
        RETURN @ClsSectMeetingID;     
    END;
	






GO
