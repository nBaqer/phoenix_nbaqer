SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE FUNCTION [dbo].[GetGraduatedDate]
    (
        @StuEnrollId UNIQUEIDENTIFIER
    )
RETURNS DATETIME
AS
    BEGIN
        DECLARE @Graduated INT = 14;
        DECLARE @ReturnValue DATETIME = NULL;
        SET @ReturnValue = (
                           SELECT     TOP 1 DateOfChange
                           FROM       dbo.syStudentStatusChanges
                           INNER JOIN dbo.syStatusCodes ON syStatusCodes.StatusCodeId = syStudentStatusChanges.NewStatusId
                           WHERE      SysStatusId = @Graduated AND StuEnrollId = @StuEnrollId
                           ORDER BY   DateOfChange DESC
                           );
        RETURN @ReturnValue;
    END;



GO
