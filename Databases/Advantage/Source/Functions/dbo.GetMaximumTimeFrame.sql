SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

--=================================================================================================  
-- GetMaximumTimeFrame  
--=================================================================================================  
CREATE FUNCTION [dbo].[GetMaximumTimeFrame]
    (
        @StuEnrollId CHAR(36)
    )
RETURNS VARCHAR(100)
--RETURNS DECIMAL(18,2)    
AS
    BEGIN
        DECLARE @mtf DECIMAL(18, 2);
        DECLARE @progid UNIQUEIDENTIFIER;
        DECLARE @undergradprogram BIT;
        DECLARE @credithourprogram BIT;

        SET @progid = (
                      SELECT ProgId
                      FROM   arPrgVersions pv
                            ,dbo.arStuEnrollments se
                      WHERE  pv.PrgVerId = se.PrgVerId
                             AND se.StuEnrollId = @StuEnrollId
                      );

        SET @undergradprogram = (
                                SELECT dbo.isProgramUndergraduate(@progid, 'D')
                                );
        SET @credithourprogram = (
                                 SELECT dbo.isProgramCreditHour(@progid)
                                 );

        --Undergrad and credit hour    
        IF (
           @undergradprogram = 1
           AND @credithourprogram = 1
           )
            BEGIN
                SET @mtf = ((
                            SELECT pv.Credits
                            FROM   arPrgVersions pv
                                  ,dbo.arStuEnrollments se
                            WHERE  pv.PrgVerId = se.PrgVerId
                                   AND se.StuEnrollId = @StuEnrollId
                            ) * 1.50
                           );

            END;

        --undergrad and clock hour    
        IF (
           @undergradprogram = 1
           AND @credithourprogram = 0
           )
            BEGIN
                SET @mtf = ((
                            SELECT pv.Hours
                            FROM   arPrgVersions pv
                                  ,dbo.arStuEnrollments se
                            WHERE  pv.PrgVerId = se.PrgVerId
                                   AND se.StuEnrollId = @StuEnrollId
                            ) * 1.50
                           );
            END;


        RETURN @mtf;
    END;
--=================================================================================================  
-- END  --  GetMaximumTimeFrame  
--=================================================================================================  
GO
