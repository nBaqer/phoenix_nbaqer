SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

--==========================================================================================
-- UDF_GetDefaultStudentEnrollment
--==========================================================================================
CREATE FUNCTION [dbo].[UDF_GetDefaultStudentEnrollment]   
/* 
US4380 Multiple Enrollment Indicator on Student Enrollment Bar

CREATED: 
11/4/2013 WP

PURPOSE: 
Get student's most recent enrollment, prioritized by status, type (major, minor, concentration) and dates

MODIFIED:  prioritized All Status in School status in school, Not only currently Attending (9) 

*/  (
     @StudentId UNIQUEIDENTIFIER
    ,@CampusId UNIQUEIDENTIFIER
	)
RETURNS UNIQUEIDENTIFIER
AS
    BEGIN
        DECLARE @StuEnrollId UNIQUEIDENTIFIER;
		
        IF EXISTS ( SELECT  1
                    FROM    arStuEnrollments AS ASE
                    INNER JOIN syStatusCodes AS SSC ON SSC.StatusCodeId = ASE.StatusCodeId
                    INNER JOIN sySysStatus AS SSS ON SSS.SysStatusId = SSC.SysStatusId
                    WHERE   StudentId = @StudentId
                            AND CampusId = @CampusId
						--AND SSC.SysStatusId = 9 )             -- currently attending
                            AND SSS.InSchool = 1 -- in school
                  )
            BEGIN				
                SELECT TOP 1
                        @StuEnrollId = ASE.StuEnrollId
                FROM    arStuEnrollments AS ASE
                INNER JOIN syStatusCodes AS SSC ON SSC.StatusCodeId = ASE.StatusCodeId
                INNER JOIN sySysStatus AS SSS ON SSS.SysStatusId = SSC.SysStatusId
                WHERE   StudentId = @StudentId
                        AND CampusId = @CampusId
				    --AND SSC.SysStatusId = 9                   -- currently attending
                        AND SSS.InSchool = 1
                ORDER BY ASE.StartDate DESC
                       ,ASE.ExpGradDate DESC;	
            END;					
		
        ELSE
            IF EXISTS ( SELECT  1
                        FROM    arStuEnrollments AS ASE
                        INNER JOIN syStatusCodes AS SSC ON SSC.StatusCodeId = ASE.StatusCodeId
                        WHERE   StudentId = @StudentId
                                AND CampusId = @CampusId
                                AND SSC.SysStatusId = 14 )           -- graduated
                BEGIN				
                    SELECT TOP 1
                            @StuEnrollId = ASE.StuEnrollId
                    FROM    arStuEnrollments AS ASE
                    INNER JOIN syStatusCodes AS SSC ON SSC.StatusCodeId = ASE.StatusCodeId
                    WHERE   ASE.StudentId = @StudentId
                            AND CampusId = @CampusId
                            AND SSC.SysStatusId = 14                     -- graduated
                    ORDER BY ASE.StartDate DESC
                           ,ASE.ExpGradDate DESC;						
                END;

            ELSE
                BEGIN
                    SELECT TOP 1
                            @StuEnrollId = ASE.StuEnrollId
                    FROM    arStuEnrollments AS ASE
                    WHERE   ASE.StudentId = @StudentId
                            AND CampusId = @CampusId
                    ORDER BY ASE.StartDate DESC
                           ,ASE.ExpGradDate DESC;	
                END;					
	
        RETURN @StuEnrollId;
	
    END;
--==========================================================================================
-- END  --  UDF_GetDefaultStudentEnrollment
--==========================================================================================


GO
