SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[UDF_IsMakingSAP]
    (
     @StuEnrollId UNIQUEIDENTIFIER
	
    )
RETURNS VARCHAR(1)
AS
    BEGIN
        DECLARE @ReturnValue VARCHAR(1)
           ,@MakingSAP BIT;
        IF EXISTS ( SELECT  StuEnrollId
                    FROM    dbo.arSAPChkResults
                    WHERE   StuEnrollId = @StuEnrollId )
            BEGIN
                SET @MakingSAP = (
                                   SELECT TOP 1
                                            IsMakingSAP
                                   FROM     dbo.arSAPChkResults
                                   WHERE    StuEnrollId = @StuEnrollId
                                   ORDER BY Period DESC
                                 );
                IF ( @MakingSAP = 1 )
                    BEGIN
                        SET @ReturnValue = 'Y';
                    END;
                ELSE
                    BEGIN
                        SET @ReturnValue = 'N';
                    END;
					
            END;	
        ELSE
            BEGIN
                SET @ReturnValue = '';
            
            END; 	
				
        RETURN @ReturnValue;
    END;


GO
