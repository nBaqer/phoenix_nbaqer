SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[GetLDAFromAttendance]
    (
     @StuEnrollId UNIQUEIDENTIFIER
	)
RETURNS DATETIME
AS
    BEGIN
        DECLARE @ReturnValue DATETIME;
        SELECT  @ReturnValue = DATEADD(dd,0,DATEDIFF(dd,0,MAX(LDA)))
        FROM    (
                  SELECT    MAX(AttendedDate) AS LDA
                  FROM      arExternshipAttendance
                  WHERE     StuEnrollId = @StuEnrollId
                  UNION ALL
                  SELECT    MAX(MeetDate) AS LDA
                  FROM      atClsSectAttendance
                  WHERE     StuEnrollId = @StuEnrollId
                            AND Actual >= 1
                            AND (
                                  Actual <> 99.00
                                  AND Actual <> 999.00
                                  AND Actual <> 9999.00
                                )
                  UNION ALL
                  SELECT    MAX(AttendanceDate) AS LDA
                  FROM      atAttendance
                  WHERE     EnrollId = @StuEnrollId
                            AND Actual >= 1
                  UNION ALL
                  SELECT    MAX(RecordDate) AS LDA
                  FROM      arStudentClockAttendance
                  WHERE     StuEnrollId = @StuEnrollId
                            AND (
                                  ActualHours >= 1.00
                                  AND ActualHours <> 99.00
                                  AND ActualHours <> 999.00
                                  AND ActualHours <> 9999.00
                                )
                  UNION ALL
                  SELECT    MAX(MeetDate) AS LDA
                  FROM      atConversionAttendance
                  WHERE     StuEnrollId = @StuEnrollId
                            AND (
                                  Actual >= 1.00
                                  AND Actual <> 99.00
                                  AND Actual <> 999.00
                                  AND Actual <> 9999.00
                                )
                ) TR;
        RETURN @ReturnValue;
    END;

GO
