SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[AppSettings]
    (
     @KeyName VARCHAR(200)
    ,@CampusId VARCHAR(50)
    )
RETURNS VARCHAR(1000)
AS
    BEGIN
        DECLARE @return VARCHAR(1000);
	
        IF EXISTS ( SELECT  cv.Value
                    FROM    dbo.syConfigAppSettings cs
                           ,dbo.syConfigAppSetValues cv
                    WHERE   cs.SettingId = cv.SettingId
                            AND cs.KeyName = @KeyName
                            AND cv.CampusId = @CampusId )
            BEGIN
                SET @return = (
                                SELECT  cv.Value
                                FROM    dbo.syConfigAppSettings cs
                                       ,dbo.syConfigAppSetValues cv
                                WHERE   cs.SettingId = cv.SettingId
                                        AND cs.KeyName = @KeyName
                                        AND cv.CampusId = @CampusId
                              );		
            END;
        ELSE
            BEGIN
                SET @return = (
                                SELECT  cv.Value
                                FROM    dbo.syConfigAppSettings cs
                                       ,dbo.syConfigAppSetValues cv
                                WHERE   cs.SettingId = cv.SettingId
                                        AND cs.KeyName = @KeyName
                                        AND cv.CampusId IS NULL
                              );		
            END;
	
	
					
        RETURN @return;


    END;

GO
