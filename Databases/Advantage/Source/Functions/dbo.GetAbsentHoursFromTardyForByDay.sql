SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[GetAbsentHoursFromTardyForByDay]
    (
     @StuEnrollId UNIQUEIDENTIFIER
    ,@RecordDate DATETIME
	)
RETURNS DECIMAL(18,2)
AS
    BEGIN
        DECLARE @TardiesMakingAbsence INT;
        DECLARE @ReturnValue DECIMAL(18,2);
        SELECT  @ReturnValue = 0.00;
        SET @TardiesMakingAbsence = (
                                      SELECT    A.TardiesMakingAbsence
                                      FROM      dbo.arPrgVersions A
                                               ,dbo.arStuEnrollments B
                                      WHERE     A.PrgVerId = B.PrgVerId
                                                AND B.StuEnrollId = @StuEnrollId
                                    );
        IF @TardiesMakingAbsence = 0
            RETURN @ReturnValue;
		
        SELECT  @ReturnValue = ISNULL(SUM(SchedHours),0)
        FROM    (
                  SELECT    ROW_NUMBER() OVER ( ORDER BY RecordDate ASC ) AS ROWID
                           ,SchedHours
                  FROM      dbo.arStudentClockAttendance
                  WHERE     RecordDate <= @RecordDate
                            AND StuEnrollId = @StuEnrollId
                            AND isTardy = 1
                ) T
        WHERE   T.ROWID % @TardiesMakingAbsence = 0;
        RETURN @ReturnValue;
    END;


GO
