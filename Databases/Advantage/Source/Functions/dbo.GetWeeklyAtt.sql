SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[GetWeeklyAtt]
    (
     @StuEnrollId UNIQUEIDENTIFIER
    ,@RecordDate DATETIME
    ,@atDate DATETIME
	)
RETURNS DECIMAL(18,2)
AS
    BEGIN
        DECLARE @TardiesMakingAbsence INT;
        DECLARE @LocalValue BIT;
        DECLARE @ReturnValue DECIMAL(18,2);
        SELECT  @ReturnValue = 0.00;
		
		 
        SET @LocalValue = (
                            SELECT  COUNT(ActualHours)
                            FROM    arStudentClockAttendance
                            WHERE   StuEnrollId = @StuEnrollId
                                    AND CONVERT(DATE,RecordDate,101) = @atDate
                                    AND ActualHours <> 999.0
                                    AND ActualHours <> 9999.0
                          );
        IF @LocalValue = 0
            RETURN NULL;
        SET @TardiesMakingAbsence = (
                                      SELECT    A.TardiesMakingAbsence
                                      FROM      dbo.arPrgVersions A
                                               ,dbo.arStuEnrollments B
                                      WHERE     A.PrgVerId = B.PrgVerId
                                                AND B.StuEnrollId = @StuEnrollId
                                    );
									
        IF @TardiesMakingAbsence = 0
            BEGIN
                SELECT  @ReturnValue = ActualHours
                FROM    arStudentClockAttendance
                WHERE   StuEnrollId = @StuEnrollId
                        AND CONVERT(DATE,RecordDate,101) = @atDate
                        AND ActualHours <> 999.0
                        AND ActualHours <> 9999.0;
		
		
				
            END;
        ELSE
            BEGIN
                SELECT  @LocalValue = COUNT(RecordDate)
                FROM    (
                          SELECT    ROW_NUMBER() OVER ( ORDER BY RecordDate ASC ) AS ROWID
                                   ,RecordDate
                                   ,SchedHours
                          FROM      dbo.arStudentClockAttendance
                          WHERE     RecordDate <= @RecordDate
                                    AND StuEnrollId = @StuEnrollId
                                    AND isTardy = 1
                        ) T
                WHERE   T.ROWID % 3 = 0
                        AND T.RecordDate = @atDate;
						
                IF @LocalValue = 0
                    BEGIN
                        SELECT  @ReturnValue = ActualHours
                        FROM    arStudentClockAttendance
                        WHERE   StuEnrollId = @StuEnrollId
                                AND CONVERT(DATE,RecordDate,101) = @atDate
                                AND ActualHours <> 999.0
                                AND ActualHours <> 9999.0;
						
                    END;
									 
			   
            END;
        RETURN @ReturnValue;
    END;
	
  

GO
