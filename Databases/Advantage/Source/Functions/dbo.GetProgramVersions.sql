SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE FUNCTION [dbo].[GetProgramVersions]
    (
     @studentid UNIQUEIDENTIFIER
    ,@clssectionid UNIQUEIDENTIFIER
	)
RETURNS VARCHAR(1000)
AS
    BEGIN
        DECLARE @ReturnValue VARCHAR(1000)= '';
        DECLARE @prgverdescrip VARCHAR(200);

        DECLARE mycursor CURSOR
        FOR
            SELECT  PrgVerDescrip
            FROM    arPrgVersions pv
                   ,dbo.arStuEnrollments se
                   ,dbo.arStudent st
                   ,arResults rs
            WHERE   pv.PrgVerId = se.PrgVerId
                    AND se.StudentId = st.StudentId
                    AND se.StuEnrollId = rs.StuEnrollId
                    AND st.StudentId = @studentid
                    AND rs.TestId = @clssectionid;    
		
        OPEN mycursor;
		
        FETCH NEXT FROM mycursor
		INTO @prgverdescrip;
		
        WHILE @@FETCH_STATUS = 0
            BEGIN
                SET @ReturnValue += @prgverdescrip + ', ';

                FETCH NEXT FROM mycursor
				INTO @prgverdescrip;
            END;			

        CLOSE mycursor;
        DEALLOCATE mycursor;

		--Remove the last comma
        SET @ReturnValue = LEFT(RTRIM(@ReturnValue),LEN(RTRIM(@ReturnValue)) - 1);

        RETURN @ReturnValue;
		
    END;




GO
