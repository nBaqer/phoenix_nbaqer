SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
--========================================================================================== 
-- UDF_GetSyStatusesValue 
--========================================================================================== 
CREATE FUNCTION [dbo].[UDF_GetSyStatusesValue] ( @StatusCode VARCHAR(3) )
RETURNS UNIQUEIDENTIFIER
AS
    BEGIN 
        DECLARE @StatusId AS UNIQUEIDENTIFIER; 
        SET @StatusId = (
                          SELECT TOP 1
                                    SS.StatusId
                          FROM      syStatuses AS SS
                          WHERE     SS.StatusCode = @StatusCode
                        ); 
        RETURN @StatusId; 
    END; 
--========================================================================================== 
-- END  --  UDF_GetSyStatusesValue 
--========================================================================================== 
 
GO
