SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



-- =============================================
-- Author:		Ginzo, John
-- Create date: 08/10/2015
-- Description:	Determine if enrollment has grades
-- =============================================
CREATE FUNCTION [dbo].[EnrollmentHasGrades]
    (
        @StuEnrollId UNIQUEIDENTIFIER
    )
RETURNS VARCHAR(3)
AS
    BEGIN
        -- Declare the return variable here
        DECLARE @GrdBkresults INT;
        DECLARE @ArResults INT;

        SET @GrdBkresults = (   SELECT COUNT(*)
                                FROM   dbo.arGrdBkResults
                                WHERE  StuEnrollId = @StuEnrollId
                                       AND PostDate IS NOT NULL
                                       AND Score IS NOT NULL );

        SET @ArResults = (   SELECT COUNT(*)
                             FROM   dbo.arResults
                             WHERE  (   GrdSysDetailId IS NOT NULL
                                        OR Score IS NOT NULL )
                                    AND StuEnrollId = @StuEnrollId );

        DECLARE @retVal VARCHAR(3);
        IF @GrdBkresults > 0
           OR @ArResults > 0
            SET @retVal = 'Yes';
        ELSE
            SET @retVal = 'No';

        RETURN @retVal;


    END;




GO
