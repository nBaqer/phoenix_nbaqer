SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[ApplyRateSchedule]
    (
     @RateScheduleId UNIQUEIDENTIFIER
    ,@Credits INTEGER
    ,@Hours INTEGER
	)
RETURNS MONEY
AS
    BEGIN
        DECLARE @Return_Value MONEY;
        SELECT  @Return_Value = CASE RSD.FlatAmount
                                  WHEN 0.00 THEN CASE RSD.UnitId
                                                   WHEN 0 THEN Rate * @Credits
                                                   WHEN 1 THEN Rate * @Hours
                                                 END
                                  ELSE FlatAmount
                                END
        FROM    saRateSchedules RS
               ,saRateScheduleDetails RSD
        WHERE   RS.RateScheduleId = RSD.RateScheduleId
                AND RS.RateScheduleId = @RateScheduleId
                AND (
                      RSD.UnitId = 0
                      AND RSD.MinUnits = (
                                           SELECT   MAX(MinUnits)
                                           FROM     saRateScheduleDetails
                                           WHERE    MinUnits <= @Credits
                                         )
                    )
                OR (
                     RSD.UnitId = 1
                     AND RSD.MinUnits = (
                                          SELECT    MAX(MinUnits)
                                          FROM      saRateScheduleDetails
                                          WHERE     MinUnits <= @Hours
                                        )
                   );
        RETURN @Return_Value;
    END;

GO
