SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[udf_DayOfWeek] ( @dtDate DATETIME )
RETURNS VARCHAR(10)
AS
    BEGIN
        DECLARE @rtDayofWeek VARCHAR(10);
        SELECT  @rtDayofWeek = CASE DATEPART(WEEKDAY,@dtDate)
                                 WHEN 1 THEN 'Sun'
                                 WHEN 2 THEN 'Mon'
                                 WHEN 3 THEN 'Tue'
                                 WHEN 4 THEN 'Wed'
                                 WHEN 5 THEN 'Thu'
                                 WHEN 6 THEN 'Fri'
                                 WHEN 7 THEN 'Sat'
                               END;
        RETURN (@rtDayofWeek);
    END;


GO
