SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE FUNCTION [dbo].[NumberofHolidaysBetweentheGivenPeriod]
    (
     @ReportDate DATE
    ,@StartDate DATE
    ,@CampusId VARCHAR(50) = ''
    )
RETURNS INTEGER
AS
    BEGIN
        DECLARE @ReturnValue INTEGER;
        DECLARE @intTotalNumberOfHolidays AS INTEGER;
	
        SET @ReturnValue = 0;
        SET @intTotalNumberOfHolidays = -1;
	
	--Check to see if the holiday falls outside the range of Student StartDate (the StartDate parameter)
	--and the date deferred revenue is posted (reportDate parameter)
	--If holiday falls outside the range, then no need to calculate, return 0
        IF @CampusId = ''
            BEGIN
                SET @intTotalNumberOfHolidays = ISNULL((
                                                         SELECT DISTINCT
                                                                COUNT(*) AS NumberOfHolidays
                                                         FROM   syHolidays H
                                                         WHERE  H.HolidayStartDate <= @StartDate
                                                                AND H.HolidayEndDate >= @ReportDate
                                                       ),0);
            END;
        ELSE
            BEGIN
                SET @intTotalNumberOfHolidays = ISNULL((
                                                         SELECT DISTINCT
                                                                COUNT(*) AS NumberOfHolidays
                                                         FROM   syHolidays H
                                                         WHERE  H.HolidayStartDate <= @StartDate
                                                                AND H.HolidayEndDate >= @ReportDate
                                                                AND (
                                                                      H.CampGrpId IN ( SELECT   CampGrpId
                                                                                       FROM     syCmpGrpCmps
                                                                                       WHERE    CampusId = @CampusId
                                                                                                AND CampGrpId <> (
                                                                                                                   SELECT   CampGrpId
                                                                                                                   FROM     syCampGrps
                                                                                                                   WHERE    CampGrpDescrip = 'ALL'
                                                                                                                 ) )
                                                                      OR H.CampGrpId = (
                                                                                         SELECT CampGrpId
                                                                                         FROM   syCampGrps
                                                                                         WHERE  CampGrpDescrip = 'ALL'
                                                                                       )
                                                                    )
                                                       ),0);
										   
                IF @intTotalNumberOfHolidays >= 1
                    BEGIN
                        SET @ReturnValue = DATEDIFF(DAYOFYEAR,@StartDate,@ReportDate) + 1;
				
                    END;
                ELSE
                    BEGIN
                        IF @CampusId = ''
                            BEGIN
                                SET @ReturnValue = ISNULL((
                                                            --if Holiday start date and end date falls with in the range
                                                            SELECT  SUM(NumberOfHolidays) AS TotalNumberofHolidays
                                                            FROM    (
                                                                      SELECT DISTINCT
                                                                                H.HolidayStartDate
                                                                               ,H.HolidayEndDate
                                                                               ,DATEDIFF(DAY,H.HolidayStartDate,H.HolidayEndDate) + 1 AS NumberOfHolidays
                                                                      FROM      syHolidays H
                                                                      WHERE     H.HolidayStartDate >= @StartDate
                                                                                AND H.HolidayEndDate <= @ReportDate
                                                                      UNION
															
															--if Holiday start date is with in the range but holiday end date falls after report date
															--when enddate falls after report date
                                                                      SELECT DISTINCT
                                                                                H.HolidayStartDate
                                                                               ,H.HolidayEndDate
                                                                               ,DATEDIFF(DAY,H.HolidayStartDate,@ReportDate) + 1 AS NumberOfHolidays
                                                                      FROM      syHolidays H
                                                                      WHERE     (
                                                                                  H.HolidayStartDate >= @StartDate
                                                                                  AND H.HolidayStartDate <= @ReportDate
                                                                                )
                                                                                AND H.HolidayEndDate >= @ReportDate
                                                                      UNION
															
															--if holiday start date falls before student startdate and 
															--holiday end date is before the report date.
                                                                      SELECT DISTINCT
                                                                                H.HolidayStartDate
                                                                               ,H.HolidayEndDate
                                                                               ,DATEDIFF(DAY,@StartDate,H.HolidayEndDate) + 1 AS NumberOfHolidays
                                                                      FROM      syHolidays H
                                                                      WHERE     H.HolidayStartDate < @StartDate
                                                                                AND H.HolidayEndDate <= @ReportDate
                                                                                AND DATEDIFF(DAY,@StartDate,H.HolidayEndDate) + 1 >= 1
                                                                    ) Holidays
                                                          ),0);
						
						
                            END;
						
                        ELSE
                            BEGIN
                                SET @ReturnValue = ISNULL((
                                                            --if Holiday start date and end date falls with in the range
                                                            SELECT  SUM(NumberOfHolidays) AS TotalNumberofHolidays
                                                            FROM    (
                                                                      SELECT DISTINCT
                                                                                H.HolidayStartDate
                                                                               ,H.HolidayEndDate
                                                                               ,DATEDIFF(DAY,H.HolidayStartDate,H.HolidayEndDate) + 1 AS NumberOfHolidays
                                                                      FROM      syHolidays H
                                                                      WHERE     H.HolidayStartDate >= @StartDate
                                                                                AND H.HolidayEndDate <= @ReportDate
                                                                                AND (
                                                                                      H.CampGrpId IN (
                                                                                      SELECT    CampGrpId
                                                                                      FROM      syCmpGrpCmps
                                                                                      WHERE     CampusId = @CampusId
                                                                                                AND CampGrpId <> (
                                                                                                                   SELECT   CampGrpId
                                                                                                                   FROM     syCampGrps
                                                                                                                   WHERE    CampGrpDescrip = 'ALL'
                                                                                                                 ) )
                                                                                      OR H.CampGrpId = (
                                                                                                         SELECT CampGrpId
                                                                                                         FROM   syCampGrps
                                                                                                         WHERE  CampGrpDescrip = 'ALL'
                                                                                                       )
                                                                                    )
                                                                      UNION
															
															--if Holiday start date is with in the range but holiday end date falls after report date
															--when enddate falls after report date
                                                                      SELECT DISTINCT
                                                                                H.HolidayStartDate
                                                                               ,H.HolidayEndDate
                                                                               ,DATEDIFF(DAY,H.HolidayStartDate,@ReportDate) + 1 AS NumberOfHolidays
                                                                      FROM      syHolidays H
                                                                      WHERE     (
                                                                                  H.HolidayStartDate >= @StartDate
                                                                                  AND H.HolidayStartDate <= @ReportDate
                                                                                )
                                                                                AND H.HolidayEndDate >= @ReportDate
                                                                                AND (
                                                                                      H.CampGrpId IN (
                                                                                      SELECT    CampGrpId
                                                                                      FROM      syCmpGrpCmps
                                                                                      WHERE     CampusId = @CampusId
                                                                                                AND CampGrpId <> (
                                                                                                                   SELECT   CampGrpId
                                                                                                                   FROM     syCampGrps
                                                                                                                   WHERE    CampGrpDescrip = 'ALL'
                                                                                                                 ) )
                                                                                      OR H.CampGrpId = (
                                                                                                         SELECT CampGrpId
                                                                                                         FROM   syCampGrps
                                                                                                         WHERE  CampGrpDescrip = 'ALL'
                                                                                                       )
                                                                                    )
                                                                      UNION
															
															--if holiday start date falls before student startdate and 
															--holiday end date is before the report date.
                                                                      SELECT DISTINCT
                                                                                H.HolidayStartDate
                                                                               ,H.HolidayEndDate
                                                                               ,DATEDIFF(DAY,@StartDate,H.HolidayEndDate) + 1 AS NumberOfHolidays
                                                                      FROM      syHolidays H
                                                                      WHERE     H.HolidayStartDate < @StartDate
                                                                                AND H.HolidayEndDate <= @ReportDate
                                                                                AND DATEDIFF(DAY,@StartDate,H.HolidayEndDate) + 1 >= 1
                                                                                AND (
                                                                                      H.CampGrpId IN (
                                                                                      SELECT    CampGrpId
                                                                                      FROM      syCmpGrpCmps
                                                                                      WHERE     CampusId = @CampusId
                                                                                                AND CampGrpId <> (
                                                                                                                   SELECT   CampGrpId
                                                                                                                   FROM     syCampGrps
                                                                                                                   WHERE    CampGrpDescrip = 'ALL'
                                                                                                                 ) )
                                                                                      OR H.CampGrpId = (
                                                                                                         SELECT CampGrpId
                                                                                                         FROM   syCampGrps
                                                                                                         WHERE  CampGrpDescrip = 'ALL'
                                                                                                       )
                                                                                    )
                                                                    ) Holidays
                                                          ),0);
						
                            END;
					
					
				
				
                    END;
				
										   
										   
            END;
		
		
	
        RETURN @ReturnValue;
    END;


GO
