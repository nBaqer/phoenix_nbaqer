SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[GetModule] ( @URL VARCHAR(100) )
RETURNS VARCHAR(36)
AS
    BEGIN
        DECLARE @code VARCHAR(4);
        DECLARE @GUID VARCHAR(36);
        SELECT  @code = LEFT(@URL,4);
        SELECT  @GUID = CASE @code
                          WHEN '~/SY' THEN (
                                             SELECT HierarchyId
                                             FROM   syHierarchies
                                             WHERE  HierarchyName = 'System - Daily'
                                           )
                          WHEN '~/AR' THEN (
                                             SELECT HierarchyId
                                             FROM   syHierarchies
                                             WHERE  HierarchyName = 'Academic Records - Daily'
                                           )
                          WHEN '~/AD' THEN (
                                             SELECT HierarchyId
                                             FROM   syHierarchies
                                             WHERE  HierarchyName = 'Admissions - Daily'
                                           )
                          WHEN '~/BK' THEN (
                                             SELECT HierarchyId
                                             FROM   syHierarchies
                                             WHERE  HierarchyName = 'Bookstore - Daily'
                                           )
                          WHEN '~/CM' THEN (
                                             SELECT HierarchyId
                                             FROM   syHierarchies
                                             WHERE  HierarchyName = 'Contact Manager - Daily'
                                           )
                          WHEN '~/FA' THEN (
                                             SELECT HierarchyId
                                             FROM   syHierarchies
                                             WHERE  HierarchyName = 'Financial Aid - Daily'
                                           )
                          WHEN '~/PL' THEN (
                                             SELECT HierarchyId
                                             FROM   syHierarchies
                                             WHERE  HierarchyName = 'Placement - Daily - Students'
                                           )
                          WHEN '~/SA' THEN (
                                             SELECT HierarchyId
                                             FROM   syHierarchies
                                             WHERE  HierarchyName = 'Student Accounts - Daily'
                                           )
                          WHEN '~/HR' THEN (
                                             SELECT HierarchyId
                                             FROM   syHierarchies
                                             WHERE  HierarchyName = 'Human Resources - Daily'
                                           )
                        END;
        RETURN @GUID;
    END;

GO
