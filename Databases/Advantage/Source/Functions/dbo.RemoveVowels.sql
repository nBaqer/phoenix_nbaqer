SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ginzo, John
-- Create date: 05/09/2014
-- Description:	Return a string with no vowels
--				Used for checking first and last
--				last name to account for mispellings
-- =============================================
CREATE FUNCTION [dbo].[RemoveVowels]
    (
     @StringToModify VARCHAR(MAX)
    )
RETURNS VARCHAR(MAX)
AS
    BEGIN

        RETURN REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(@StringToModify, 'A', ''), 'E', ''), 'O', ''), 'U', ''), 'I', '');

    END;


GO
