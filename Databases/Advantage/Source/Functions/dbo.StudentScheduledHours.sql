SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		FAME Inc.
-- Create date: 5/23/2019
-- Description:	Student scheduled table valued function. Returns table student enrollment id , scheduled hours for day of the week , students must all be from same single campus
CREATE FUNCTION [dbo].[StudentScheduledHours]
    (
        @StuEnrollIdList VARCHAR(MAX) = NULL
       ,@Date DATETIME
    )
RETURNS TABLE
AS
    RETURN (
           SELECT ScheduledData.StuEnrollId
                 ,CASE WHEN ScheduledData.ScheduleDetailId IS NULL THEN 0
                       WHEN CAST(( ScheduledData.ScheduledMinutes - ScheduledData.LunchMinutes - ScheduledData.HolidayMinutes + LunchHolidayOverlapMinutes )
                                 / 60.0 AS DECIMAL(16, 2)) < 0 THEN 0
                       ELSE
                           CAST(( ScheduledData.ScheduledMinutes - ScheduledData.LunchMinutes - ScheduledData.HolidayMinutes + LunchHolidayOverlapMinutes )
                                / 60.0 AS DECIMAL(16, 2))
                  END AS CalculatedScheduledHours
                 ,ScheduledData.ScheduleId
           FROM   (
                  SELECT      arStudentSchedules.StuEnrollId
                             ,CampusId
                             ,total
                             ,timein
                             ,timeout
                             ,lunchin
                             ,lunchout
                             ,arProgSchedules.ScheduleId
                             ,dbo.arProgScheduleDetails.ScheduleDetailId
                             ,ISNULL(DATEDIFF(MINUTE, timein, timeout), 0) AS ScheduledMinutes
                             ,ISNULL(DATEDIFF(MINUTE, lunchout, lunchin), 0) AS LunchMinutes
                             ,ISNULL(
                                        CASE WHEN Holiday.AllDay = 1 THEN DATEDIFF(MINUTE, timein, timeout)
                                             WHEN (
                                                  CAST(Holiday.BeginTimeInteral AS TIME) <= CAST(timeout AS TIME)
                                                  AND CAST(ISNULL(Holiday.EndTimeInterval, timeout) AS TIME) >= CAST(timein AS TIME)
                                                  ) -- holiday falls entirely inside students schedule

                                        THEN     DATEDIFF(MINUTE, CAST(Holiday.BeginTimeInteral AS TIME), CAST(ISNULL(Holiday.EndTimeInterval, timeout) AS TIME))
                                             WHEN (
                                                  CAST(Holiday.BeginTimeInteral AS TIME) >= CAST(timein AS TIME)
                                                  AND CAST(Holiday.BeginTimeInteral AS TIME) <= CAST(timeout AS TIME)
                                                  ) -- holiday start time falls betweem time in and time out 
                                        THEN     DATEDIFF(MINUTE, CAST(Holiday.BeginTimeInteral AS TIME), CAST(timeout AS TIME))
                                             WHEN (
                                                  CAST(ISNULL(Holiday.EndTimeInterval, timeout) AS TIME) >= CAST(timein AS TIME)
                                                  AND CAST(ISNULL(Holiday.EndTimeInterval, timeout) AS TIME) <= CAST(timeout AS TIME)
                                                  ) -- holiday end time falls betweem time in and time out 
                                        THEN     DATEDIFF(MINUTE, CAST(Holiday.BeginTimeInteral AS TIME), CAST(ISNULL(Holiday.EndTimeInterval, timeout) AS TIME))
                                             ELSE 0
                                        END
                                       ,0
                                    ) AS HolidayMinutes
                             ,ISNULL(
                                        CASE WHEN Holiday.AllDay = 1 THEN DATEDIFF(MINUTE, lunchout, lunchin)
                                             WHEN (
                                                  CAST(Holiday.BeginTimeInteral AS TIME) <= CAST(lunchin AS TIME)
                                                  AND CAST(ISNULL(Holiday.EndTimeInterval, lunchin) AS TIME) >= CAST(lunchout AS TIME)
                                                  ) -- holiday falls entirely inside holiday schedule
                                        THEN     DATEDIFF(MINUTE, CAST(Holiday.BeginTimeInteral AS TIME), CAST(ISNULL(Holiday.EndTimeInterval, lunchin) AS TIME))
                                             WHEN (
                                                  CAST(Holiday.BeginTimeInteral AS TIME) >= CAST(lunchout AS TIME)
                                                  AND CAST(Holiday.BeginTimeInteral AS TIME) <= CAST(lunchin AS TIME)
                                                  ) -- holiday start time falls between lunch
                                        THEN     DATEDIFF(MINUTE, CAST(Holiday.BeginTimeInteral AS TIME), CAST(lunchin AS TIME))
                                             WHEN (
                                                  CAST(ISNULL(Holiday.EndTimeInterval, lunchin) AS TIME) >= CAST(lunchout AS TIME)
                                                  AND CAST(ISNULL(Holiday.EndTimeInterval, lunchin) AS TIME) <= CAST(lunchin AS TIME)
                                                  ) -- holiday end time falls between lunch 
                                        THEN     DATEDIFF(MINUTE, CAST(Holiday.BeginTimeInteral AS TIME), CAST(ISNULL(Holiday.EndTimeInterval, lunchin) AS TIME))
                                             ELSE 0
                                        END
                                       ,0
                                    ) AS LunchHolidayOverlapMinutes -- lunch and holiday may overlap, calculate this time and add it back to student scheduled hours bucket
                             ,Holiday.AllDay
                             ,Holiday.BeginTimeInteral
                             ,Holiday.EndTimeInterval
                             ,Holiday.HolidayDescrip
                  FROM        dbo.arStuEnrollments
                  JOIN        dbo.arStudentSchedules ON arStudentSchedules.StuEnrollId = arStuEnrollments.StuEnrollId
                  JOIN        dbo.arProgSchedules ON arProgSchedules.ScheduleId = arStudentSchedules.ScheduleId
                  LEFT JOIN   dbo.arProgScheduleDetails ON arProgScheduleDetails.ScheduleId = arStudentSchedules.ScheduleId
                  OUTER APPLY (
                              SELECT    TOP 1 HolidayDescrip
                                       ,HolidayStartDate
                                       ,HolidayEndDate
                                       ,AllDay
                                       ,a.TimeIntervalDescrip AS BeginTimeInteral
                                       ,b.TimeIntervalDescrip AS EndTimeInterval
                              FROM      dbo.syHolidays
                              LEFT JOIN dbo.cmTimeInterval a ON a.TimeIntervalId = syHolidays.StartTimeId
                              LEFT JOIN dbo.cmTimeInterval b ON b.TimeIntervalId = syHolidays.EndTimeId
                              WHERE     (
                                        CAST(@Date AS DATE) >= CAST(HolidayStartDate AS DATE)
                                        AND CAST(@Date AS DATE) <= CAST(HolidayEndDate AS DATE)
                                        )
                                        AND syHolidays.CampGrpId IN (
                                                                    SELECT CampGrpId
                                                                    FROM   dbo.syCmpGrpCmps
                                                                    WHERE  dbo.syCmpGrpCmps.CampusId = (
                                                                                                       SELECT CampusId
                                                                                                       FROM   dbo.arStuEnrollments
                                                                                                       WHERE  StuEnrollId = (
                                                                                                                            SELECT TOP 1 Val
                                                                                                                            FROM   MultipleValuesForReportParameters(
                                                                                                                                                                        @StuEnrollIdList
                                                                                                                                                                       ,','
                                                                                                                                                                       ,1
                                                                                                                                                                    )
                                                                                                                            )
                                                                                                       )
                                                                    )
                                        AND syHolidays.StatusId = (
                                                                  SELECT TOP 1 StatusId
                                                                  FROM   dbo.syStatuses
                                                                  WHERE  StatusCode = 'A'
                                                                  )
                              ) Holiday
                  WHERE       (
                              (
                              DATEPART(dw, @Date) - 1 = 0
                              AND dw = 7
                              )
                              OR dw = ( DATEPART(dw, @Date) - 1 )
                              )
                              AND arStuEnrollments.StuEnrollId IN (
                                                                  SELECT Val
                                                                  FROM   MultipleValuesForReportParameters(@StuEnrollIdList, ',', 1)
                                                                  )
                  ) ScheduledData
           );


GO
