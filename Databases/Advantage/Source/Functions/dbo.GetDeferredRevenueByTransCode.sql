SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[GetDeferredRevenueByTransCode]
    (
     @TransCodeId VARCHAR(36)
    ,@ReportDate DATETIME = NULL
	)
RETURNS DECIMAL(9,2)
AS
    BEGIN
        DECLARE @TotalAmount DECIMAL(9,2);
        SELECT  @TotalAmount = COALESCE(SUM(dbo.GetDeferredRevenue(TransactionId,@ReportDate)),0)
        FROM    saTransactions
        WHERE   TransCodeId = @TransCodeId;
        RETURN @TotalAmount;
    END;

GO
