SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE FUNCTION [dbo].[UDF_ShouldCourseBeChargedOnThisEnrollment]
    (
     @StuEnrollId UNIQUEIDENTIFIER
    ,@ReqId UNIQUEIDENTIFIER
	)
RETURNS INT
AS
    BEGIN
        DECLARE @ReturnValue INT;
        DECLARE @credentiallevel INT;
        DECLARE @startdate DATETIME;
        DECLARE @pvweeks INT;
        DECLARE @pvname VARCHAR(100); 
        DECLARE @StuEnrollId2 UNIQUEIDENTIFIER;
        DECLARE @credentiallevel2 INT;
        DECLARE @startdate2 DATETIME;
        DECLARE @pvweeks2 INT;
        DECLARE @pvname2 VARCHAR(100);      
			
        SET @ReturnValue = 0;

        SET @StuEnrollId2 = (
                              SELECT TOP 1
                                        rs.StuEnrollId
                              FROM      dbo.arTransferGrades rs
                                       ,dbo.arStuEnrollments se
                              WHERE     rs.StuEnrollId = se.StuEnrollId
                                        AND rs.ReqId = @ReqId
                                        AND rs.StuEnrollId <> @StuEnrollId
                                        AND se.StudentId = (
                                                             SELECT StudentId
                                                             FROM   dbo.arStuEnrollments
                                                             WHERE  StuEnrollId = @StuEnrollId
                                                           )
                            );
		
        IF @StuEnrollId2 IS NULL
			--If no other enrollment for this student has the same class then we can return 1	
            BEGIN
                SET @ReturnValue = 1;
            END;		
        ELSE
            BEGIN
				--Get the data for the enrollment passed in          
                SELECT  @credentiallevel = pc.IPEDSValue
                       ,@startdate = se.StartDate
                       ,@pvweeks = pv.Weeks
                       ,@pvname = pv.PrgVerDescrip
                FROM    dbo.arStuEnrollments se
                       ,arPrgVersions pv
                       ,arPrograms pg
                       ,dbo.arProgCredential pc
                WHERE   se.PrgVerId = pv.PrgVerId
                        AND pv.ProgId = pg.ProgId
                        AND pg.CredentialLvlId = pc.CredentialId
                        AND se.StuEnrollId = @StuEnrollId;

				--Get the data for the other enrollment
                SELECT  @credentiallevel2 = pc.IPEDSValue
                       ,@startdate2 = se.StartDate
                       ,@pvweeks2 = pv.Weeks
                       ,@pvname2 = pv.PrgVerDescrip
                FROM    dbo.arStuEnrollments se
                       ,arPrgVersions pv
                       ,arPrograms pg
                       ,dbo.arProgCredential pc
                WHERE   se.PrgVerId = pv.PrgVerId
                        AND pv.ProgId = pg.ProgId
                        AND pg.CredentialLvlId = pc.CredentialId
                        AND se.StuEnrollId = @StuEnrollId2;

				--Enrollment if it has a lowewr credential level
                IF @credentiallevel < @credentiallevel2
                    BEGIN
                        SET @ReturnValue = 1;
                    END;  
                ELSE
                    IF @credentiallevel = @credentiallevel2
                        BEGIN
							--Enrollment if it has an earlier start date
                            IF @startdate < @startdate2
                                BEGIN
                                    SET @ReturnValue = 1;
                                END; 
                            ELSE
                                IF @startdate = @startdate2
                                    BEGIN
										--Enrollment if it has less weeks
                                        IF @pvweeks < @pvweeks2
                                            BEGIN
                                                SET @ReturnValue = 1;
                                            END;
                                        ELSE
                                            IF @pvweeks = @pvweeks2
                                                BEGIN
													--Enrollment if it has lesser name
                                                    IF @pvname < @pvname2
                                                        BEGIN
                                                            SET @ReturnValue = 1;
                                                        END;                                                      
                                                END;                                        
                                    END;                             
                        END;             


            END;	 
  
  
  
		
		
        RETURN @ReturnValue;
    END;

GO
