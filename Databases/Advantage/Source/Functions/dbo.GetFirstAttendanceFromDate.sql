SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE FUNCTION [dbo].[GetFirstAttendanceFromDate]
    (
        @StuEnrollId UNIQUEIDENTIFIER
       ,@FromDate DATETIME
    )
RETURNS DATETIME
AS
    BEGIN
        DECLARE @ReturnValue DATETIME = NULL;
        SET @ReturnValue = (
                           SELECT   TOP 1 FDA
                           FROM     (
                                    SELECT   TOP 1 AttendedDate AS FDA
                                    FROM     arExternshipAttendance
                                    WHERE    StuEnrollId = @StuEnrollId
                                             AND AttendedDate > @FromDate
                                    ORDER BY AttendedDate ASC
                                    UNION ALL
                                    SELECT   TOP 1 MeetDate AS FDA
                                    FROM     atClsSectAttendance
                                    WHERE    StuEnrollId = @StuEnrollId
                                             AND MeetDate > @FromDate
                                             AND Actual >= 1
                                             AND (
                                                 Actual <> 99.00
                                                 AND Actual <> 999.00
                                                 AND Actual <> 9999.00
                                                 )
                                    ORDER BY MeetDate ASC
                                    UNION ALL
                                    SELECT   TOP 1 RecordDate AS FDA
                                    FROM     arStudentClockAttendance
                                    WHERE    StuEnrollId = @StuEnrollId
                                             AND RecordDate > @FromDate
                                             AND (
                                                 ActualHours >= 1.00
                                                 AND ActualHours <> 99.00
                                                 AND ActualHours <> 999.00
                                                 AND ActualHours <> 9999.00
                                                 )
                                    ORDER BY RecordDate ASC
                                    UNION ALL
                                    SELECT   TOP 1 MeetDate AS FDA
                                    FROM     atConversionAttendance
                                    WHERE    StuEnrollId = @StuEnrollId
                                             AND MeetDate > @FromDate
                                             AND (
                                                 Actual >= 1.00
                                                 AND Actual <> 99.00
                                                 AND Actual <> 999.00
                                                 AND Actual <> 9999.00
                                                 )
                                    ORDER BY MeetDate ASC
                                    ) TR
                           ORDER BY FDA ASC
                           );
        RETURN @ReturnValue;
    END;



GO
