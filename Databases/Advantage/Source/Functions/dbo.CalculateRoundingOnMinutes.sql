SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE   FUNCTION [dbo].[CalculateRoundingOnMinutes]
    (
        @UnroundedMinutes DECIMAL(16, 2)
       ,@RoundingMethod VARCHAR(50)
    )
RETURNS DECIMAL(16, 2)
AS
    BEGIN
        DECLARE @ReturnAmount DECIMAL(16, 2) = @UnroundedMinutes;
        DECLARE @Hour INT = 60;
        DECLARE @HalfHour INT = 30;
        DECLARE @QuarterHour INT = 15;
        DECLARE @TenMinutes INT = 10;
        DECLARE @FiveMinutes INT = 5;
        DECLARE @Minute INT = 1;

        DECLARE @SplitAmount INT = CASE WHEN @RoundingMethod = 'Hour' THEN @Hour
                                        WHEN @RoundingMethod = 'HalfHour' THEN @HalfHour
                                        WHEN @RoundingMethod = 'QuarterHour' THEN @QuarterHour
                                        WHEN @RoundingMethod = 'TenMinutes' THEN @TenMinutes
                                        WHEN @RoundingMethod = 'FiveMinutes' THEN @FiveMinutes
                                        WHEN @RoundingMethod = 'Minute' THEN @Minute
                                        ELSE 0 -- No Rounding
                                   END;

        SET @ReturnAmount = CASE WHEN @RoundingMethod = 'None' THEN @UnroundedMinutes
                                 WHEN @UnroundedMinutes % @SplitAmount >= ( @SplitAmount / 2.0 ) THEN
                                     @UnroundedMinutes - ( @UnroundedMinutes % @SplitAmount ) + @SplitAmount --round up
                                 ELSE @UnroundedMinutes - ( @UnroundedMinutes % @SplitAmount )               -- round down
                            END;

        RETURN @ReturnAmount;
    END;



GO
