SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[GetUDFValueForDate]
    (
     @SDFID UNIQUEIDENTIFIER
    ,@ID UNIQUEIDENTIFIER
	)
RETURNS DATETIME
AS
    BEGIN
        DECLARE @ReturnValue DATETIME;
        SET @ReturnValue = (
                             SELECT TOP 1
                                    CAST(SDFValue AS DATETIME)
                             FROM   sySDFModuleValue V
                             WHERE  SDFID = @SDFID
                                    AND SDFValue <> ''
                                    AND SDFValue IS NOT NULL
                                    AND ISDATE(SDFValue) = 1
                                    AND ( PgPKID = @ID )
                           );  
	
        RETURN @ReturnValue;
    END;


GO
