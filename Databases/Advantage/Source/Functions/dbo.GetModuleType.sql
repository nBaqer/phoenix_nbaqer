SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[GetModuleType]
    (
     @URL VARCHAR(100)
    ,@ResourceTypeId INT
	)
RETURNS VARCHAR(36)
AS
    BEGIN
        DECLARE @code VARCHAR(4);
        DECLARE @GUID VARCHAR(36);
        SELECT  @code = LEFT(@URL,4);
        IF @ResourceTypeId = 3
            BEGIN
                SELECT  @GUID = CASE @code
                                  WHEN '~/SY' THEN (
                                                     SELECT HierarchyId
                                                     FROM   syHierarchies
                                                     WHERE  HierarchyName = 'System - Daily'
                                                   )
                                  WHEN '~/AR' THEN (
                                                     SELECT HierarchyId
                                                     FROM   syHierarchies
                                                     WHERE  HierarchyName = 'Academic Records - Daily'
                                                   )
                                  WHEN '~/AD' THEN (
                                                     SELECT HierarchyId
                                                     FROM   syHierarchies
                                                     WHERE  HierarchyName = 'Admissions - Daily'
                                                   )
                                  WHEN '~/BK' THEN (
                                                     SELECT HierarchyId
                                                     FROM   syHierarchies
                                                     WHERE  HierarchyName = 'Bookstore - Daily'
                                                   )
                                  WHEN '~/CM' THEN (
                                                     SELECT HierarchyId
                                                     FROM   syHierarchies
                                                     WHERE  HierarchyName = 'Contact Manager - Daily'
                                                   )
                                  WHEN '~/FA' THEN (
                                                     SELECT HierarchyId
                                                     FROM   syHierarchies
                                                     WHERE  HierarchyName = 'Financial Aid - Daily'
                                                   )
                                  WHEN '~/PL' THEN (
                                                     SELECT HierarchyId
                                                     FROM   syHierarchies
                                                     WHERE  HierarchyName = 'Placement - Daily - Students'
                                                   )
                                  WHEN '~/SA' THEN (
                                                     SELECT HierarchyId
                                                     FROM   syHierarchies
                                                     WHERE  HierarchyName = 'Student Accounts - Daily'
                                                   )
                                  WHEN '~/HR' THEN (
                                                     SELECT HierarchyId
                                                     FROM   syHierarchies
                                                     WHERE  HierarchyName = 'Human Resources - Daily'
                                                   )
                                END;
                RETURN @GUID;
            END;
        IF @ResourceTypeId = 2
            BEGIN
                SELECT  @GUID = CASE @code
                                  WHEN '~/SY' THEN (
                                                     SELECT HierarchyId
                                                     FROM   syHierarchies
                                                     WHERE  HierarchyName = 'System - Lists'
                                                   )
                                  WHEN '~/AR' THEN (
                                                     SELECT HierarchyId
                                                     FROM   syHierarchies
                                                     WHERE  HierarchyName = 'Academic Records - Lists'
                                                   )
                                  WHEN '~/AD' THEN (
                                                     SELECT HierarchyId
                                                     FROM   syHierarchies
                                                     WHERE  HierarchyName = 'Admissions - Lists'
                                                   )
                                  WHEN '~/BK' THEN (
                                                     SELECT HierarchyId
                                                     FROM   syHierarchies
                                                     WHERE  HierarchyName = 'Bookstore - Lists'
                                                   )
                                  WHEN '~/CM' THEN (
                                                     SELECT HierarchyId
                                                     FROM   syHierarchies
                                                     WHERE  HierarchyName = 'Contact Manager - Lists'
                                                   )
                                  WHEN '~/FA' THEN (
                                                     SELECT HierarchyId
                                                     FROM   syHierarchies
                                                     WHERE  HierarchyName = 'Financial Aid - Lists'
                                                   )
                                  WHEN '~/PL' THEN (
                                                     SELECT HierarchyId
                                                     FROM   syHierarchies
                                                     WHERE  HierarchyName = 'Placement - Lists'
                                                   )
                                  WHEN '~/SA' THEN (
                                                     SELECT HierarchyId
                                                     FROM   syHierarchies
                                                     WHERE  HierarchyName = 'Student Accounts - Lists'
                                                   )
                                  WHEN '~/HR' THEN (
                                                     SELECT HierarchyId
                                                     FROM   syHierarchies
                                                     WHERE  HierarchyName = 'Human Resources - Lists'
                                                   )
                                END;
                RETURN @GUID;		
            END; 
        IF @ResourceTypeId = 4
            BEGIN
                SELECT  @GUID = CASE @code
                                  WHEN '~/SY' THEN (
                                                     SELECT HierarchyId
                                                     FROM   syHierarchies
                                                     WHERE  HierarchyName = 'System - Reports'
                                                   )
                                  WHEN '~/AR' THEN (
                                                     SELECT HierarchyId
                                                     FROM   syHierarchies
                                                     WHERE  HierarchyName = 'Academic Records - Reports'
                                                   )
                                  WHEN '~/AD' THEN (
                                                     SELECT HierarchyId
                                                     FROM   syHierarchies
                                                     WHERE  HierarchyName = 'Admissions - Reports'
                                                   )
                                  WHEN '~/BK' THEN (
                                                     SELECT HierarchyId
                                                     FROM   syHierarchies
                                                     WHERE  HierarchyName = 'Bookstore - Reports'
                                                   )
                                  WHEN '~/CM' THEN (
                                                     SELECT HierarchyId
                                                     FROM   syHierarchies
                                                     WHERE  HierarchyName = 'Contact Manager - Reports'
                                                   )
                                  WHEN '~/FA' THEN (
                                                     SELECT HierarchyId
                                                     FROM   syHierarchies
                                                     WHERE  HierarchyName = 'Financial Aid - Reports'
                                                   )
                                  WHEN '~/PL' THEN (
                                                     SELECT HierarchyId
                                                     FROM   syHierarchies
                                                     WHERE  HierarchyName = 'Placement - Reports'
                                                   )
                                  WHEN '~/SA' THEN (
                                                     SELECT HierarchyId
                                                     FROM   syHierarchies
                                                     WHERE  HierarchyName = 'Student Accounts - Reports'
                                                   )
                                  WHEN '~/HR' THEN (
                                                     SELECT HierarchyId
                                                     FROM   syHierarchies
                                                     WHERE  HierarchyName = 'Human Resources - Reports'
                                                   )
                                END;		
            END;
        RETURN @GUID;
    END;

GO
