SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[GetIPEDS_Spring_GetEnrollmentfromMultipleEnrollment]
    (
     @CampusId VARCHAR(50)
    ,@ProgId VARCHAR(4000) = NULL
    ,@EndDate DATETIME = NULL
    ,@DegreeType VARCHAR(20)
    ,@studentId UNIQUEIDENTIFIER
    )
RETURNS UNIQUEIDENTIFIER
AS
    BEGIN
        DECLARE @stuEnrollId UNIQUEIDENTIFIER; 
        SET @stuEnrollId = '00000000-0000-0000-0000-000000000000';
        SELECT TOP 1
                @stuEnrollId = StuEnrollId
        FROM    arStuEnrollments A1
               ,arPrgVersions A2
               ,arProgTypes A3
        WHERE   A1.PrgVerId = A2.PrgVerId
                AND A2.ProgTypId = A3.ProgTypId
                AND A1.StudentId = @studentId
                AND A1.CampusId = LTRIM(RTRIM(@CampusId))
                AND (
                      @ProgId IS NULL
                      OR A2.ProgId IN ( SELECT  Val
                                        FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                    )
                AND (
                      (
                        @DegreeType = 'undergraduate'
                        AND A3.IPEDSValue = 58
                      )
                      OR (
                           @DegreeType = 'graduate'
                           AND (
                                 A3.IPEDSValue = 59
                                 OR A3.IPEDSValue = 60
                               )
                         )
                    )
                AND A1.StartDate <= @EndDate
        ORDER BY StartDate DESC
               ,EnrollDate DESC;
        
        RETURN @stuEnrollId;
    
    END;   
    

GO
