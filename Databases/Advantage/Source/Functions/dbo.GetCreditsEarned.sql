SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[GetCreditsEarned] ( @StuEnrollId CHAR(36) )
RETURNS INTEGER
AS
    BEGIN
        DECLARE @CreditsEarned INTEGER;
        SELECT  @CreditsEarned = SUM(RQ.Credits)
        FROM    arResults R
               ,arGradeSystemDetails GSD
               ,arClassSections CS
               ,arReqs RQ
        WHERE   R.GrdSysDetailId = GSD.GrdSysDetailId
                AND R.TestId = CS.ClsSectionId
                AND CS.ReqId = RQ.ReqId
                AND GSD.IsPass = 1
                AND GSD.IsCreditsEarned = 1
                AND R.StuEnrollId = @StuEnrollId;
        RETURN  COALESCE(@CreditsEarned, 0);
    END;

GO
