SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- **********************************************************************************************************************
-- Convert unformatted Social-Security Numbers (i.e., no embedded hyphens) to standard "xxx-xx-xxxx" SSN display format.
-- Example: 
--	PRINT dbo.udfConvertUnformattedSSNToFormattedSSN('123456789') --returns '123-45-6789' 
-- ***********************************************************************************************************************
CREATE FUNCTION [dbo].[UDF_FormatSSN] ( @SSN CHAR(9) )
RETURNS CHAR(11)
AS
    BEGIN
        DECLARE @Mask VARCHAR(20)
           ,@MaskChar CHAR(1)
           ,@FormattedSSN VARCHAR(20)
           ,@intLoop INT;
--declare @SSN varchar(9)
        DECLARE @intInnerLoop INT;
        SET @Mask = (
                      SELECT    Mask
                      FROM      syInputMasks
                      WHERE     InputMaskId = 1
                    );
--set @SSN='123456789'
        SET @FormattedSSN = '';
        SET @intLoop = 0;
        SET @intInnerLoop = 0;
        WHILE ( @intLoop < LEN(@Mask) )
            BEGIN
                SET @intLoop = @intLoop + 1;
                SET @MaskChar = SUBSTRING(@Mask,@intLoop,1);
                IF LTRIM(RTRIM(@MaskChar)) = '#'
                    BEGIN
                        SET @intInnerLoop = @intInnerLoop + 1;
                        SET @FormattedSSN = @FormattedSSN + SUBSTRING(@SSN,@intInnerLoop,1); 
                    END;
                ELSE
                    BEGIN
                        IF @intLoop = 1
                            BEGIN
                                SET @FormattedSSN = @MaskChar; 
                            END;
                        ELSE
                            BEGIN
                                SET @FormattedSSN = @FormattedSSN + @MaskChar;
                            END;
                    END;
            END;
        RETURN @FormattedSSN;
    END; 

GO
