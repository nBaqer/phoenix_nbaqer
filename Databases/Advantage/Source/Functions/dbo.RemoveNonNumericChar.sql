SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [dbo].[RemoveNonNumericChar] ( @str VARCHAR(500) )
RETURNS VARCHAR(500)
    BEGIN  
        DECLARE @startingIndex INT;  
        SET @startingIndex = 0;  
        WHILE 1 = 1
            BEGIN  
                SET @startingIndex = PATINDEX('%[^0-9]%',@str);  
                IF @startingIndex <> 0
                    BEGIN  
                        SET @str = REPLACE(@str,SUBSTRING(@str,@startingIndex,1),'');  
                    END;  
                ELSE
                    BREAK;   
            END;  
        RETURN @str;  
    END;
GO
