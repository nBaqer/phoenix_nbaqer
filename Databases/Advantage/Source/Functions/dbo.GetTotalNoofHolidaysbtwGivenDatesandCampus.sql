SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE FUNCTION [dbo].[GetTotalNoofHolidaysbtwGivenDatesandCampus]
    (
     @StartDate AS DATETIME
    ,@EndDate AS DATETIME
    ,@CampusID AS UNIQUEIDENTIFIER
    )
RETURNS INTEGER
AS
    BEGIN
        DECLARE @Return_Value INTEGER;


 --DECLARE @StartDate AS DATETIME
 --DECLARE @EndDate AS DATETIME
 --DECLARE @CampusID UNIQUEIDENTIFIER
 
 --SET @StartDate='01/01/2010'
 --SET @EndDate='12/31/2010'
 --SET @CampusID='A78274B8-1CF0-4891-BDAC-867DF80BB6EC'
 
 
        SELECT  @Return_Value = ISNULL(SUM(NumberOfHolidays),0)
        FROM    (
                  -- ' if Holiday start date and end date falls with in the range
                  SELECT DISTINCT
                            H.HolidayStartDate
                           ,H.HolidayEndDate
                           ,DATEDIFF(DAY,H.HolidayStartDate,H.HolidayEndDate) + 1 AS NumberOfHolidays
                  FROM      syHolidays H
                  WHERE     H.HolidayStartDate >= @StartDate
                            AND H.HolidayEndDate <= @EndDate
                            AND (
                                  H.CampGrpId IN ( SELECT   CampGrpId
                                                   FROM     syCmpGrpCmps
                                                   WHERE    CampusId = @CampusID
                                                            AND CampGrpId <> (
                                                                               SELECT   CampGrpId
                                                                               FROM     syCampGrps
                                                                               WHERE    CampGrpDescrip = 'ALL'
                                                                             ) )
                                  OR H.CampGrpId = (
                                                     SELECT CampGrpId
                                                     FROM   syCampGrps
                                                     WHERE  CampGrpDescrip = 'ALL'
                                                   )
                                )
                  UNION 
           -- if Holiday start date is with in the range but holiday end date falls after report date
           -- when enddate falls after report date
                  SELECT DISTINCT
                            H.HolidayStartDate
                           ,H.HolidayEndDate
                           ,DATEDIFF(DAY,H.HolidayStartDate,@EndDate) + 1 AS NumberOfHolidays
                  FROM      syHolidays H
                  WHERE     (
                              H.HolidayStartDate >= @StartDate
                              AND H.HolidayStartDate <= @EndDate
                            )
                            AND H.HolidayEndDate >= @EndDate
                            AND (
                                  H.CampGrpId IN ( SELECT   CampGrpId
                                                   FROM     syCmpGrpCmps
                                                   WHERE    CampusId = @CampusID
                                                            AND CampGrpId <> (
                                                                               SELECT   CampGrpId
                                                                               FROM     syCampGrps
                                                                               WHERE    CampGrpDescrip = 'ALL'
                                                                             ) )
                                  OR H.CampGrpId = (
                                                     SELECT CampGrpId
                                                     FROM   syCampGrps
                                                     WHERE  CampGrpDescrip = 'ALL'
                                                   )
                                )
                  UNION 
          --  'if holiday start date falls before student startdate and 
          --  'holiday end date is before the report date.
                  SELECT DISTINCT
                            H.HolidayStartDate
                           ,H.HolidayEndDate
                           ,DATEDIFF(DAY,@StartDate,H.HolidayEndDate) + 1 AS NumberOfHolidays
                  FROM      syHolidays H
                  WHERE     H.HolidayStartDate < @StartDate
                            AND H.HolidayEndDate <= @EndDate
                            AND DATEDIFF(DAY,@StartDate,H.HolidayEndDate) + 1 >= 1
                            AND (
                                  H.CampGrpId IN ( SELECT   CampGrpId
                                                   FROM     syCmpGrpCmps
                                                   WHERE    CampusId = @CampusID
                                                            AND CampGrpId <> (
                                                                               SELECT   CampGrpId
                                                                               FROM     syCampGrps
                                                                               WHERE    CampGrpDescrip = 'ALL'
                                                                             ) )
                                  OR H.CampGrpId = (
                                                     SELECT CampGrpId
                                                     FROM   syCampGrps
                                                     WHERE  CampGrpDescrip = 'ALL'
                                                   )
                                )
                ) Holidays;  

        RETURN @Return_Value;
    END;


GO
