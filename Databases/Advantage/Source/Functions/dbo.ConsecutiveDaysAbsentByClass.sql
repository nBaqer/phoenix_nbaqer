SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[ConsecutiveDaysAbsentByClass]
    (
        @StuEnrollId UNIQUEIDENTIFIER
       ,@ClsSectionId UNIQUEIDENTIFIER
       ,@CutOffDate DATETIME
       ,@ShowCalendarDays BIT = 0
	   ,@ConsDays INT = 1
    )
RETURNS INT
AS
    BEGIN
        DECLARE @Return INT;
        DECLARE @meetdate DATETIME;
        DECLARE @recordcounter INT;
        DECLARE @prevmeetdate DATETIME;
        DECLARE @consecutivecounter INT;
        DECLARE @prevconsecutivecounter INT;
        DECLARE @actual INT;
        DECLARE @curDateNotParsed DATETIME2;

        SET @Return = 0;
        SET @recordcounter = 1;
        SET @consecutivecounter = 0;
        SET @prevconsecutivecounter = 0;

        DECLARE @InfoDates AS TABLE
            (
                MeetDate DATETIME2
               ,Actual DECIMAL(18, 0)
            );

        INSERT INTO @InfoDates
                    SELECT MeetDate
                          ,Actual
                    FROM   dbo.atClsSectAttendance
                    WHERE  StuEnrollId = @StuEnrollId
                           AND ClsSectionId = @ClsSectionId
                           AND Scheduled >= 1 -- any school that tracks attendance by PA and Clock Hour
                           AND MeetDate <= @CutOffDate;

        IF @ShowCalendarDays = 1
            BEGIN
                DECLARE @MaxDate DATETIME2 = (
                                             SELECT MAX(MeetDate)
                                             FROM   @InfoDates
                                             );
                DECLARE @MinDate DATETIME2 = (
                                             SELECT MIN(MeetDate)
                                             FROM   @InfoDates
                                             );
                DECLARE @currentDay DATETIME;
                SELECT @currentDay = @MinDate;
                WHILE @currentDay <= @MaxDate
                    BEGIN

                        IF NOT EXISTS (
                                      SELECT TOP 1 D.MeetDate
                                      FROM   @InfoDates D
                                      WHERE  D.MeetDate = @currentDay
                                      )
                            BEGIN
                                INSERT INTO @InfoDates
                                VALUES ( @currentDay -- MeetDateWithoutTime - datetime2
                                        ,0           -- Actual - decimal(18, 0)
                                    );

                            END;
                        SELECT @currentDay = DATEADD(DAY, 1, @currentDay);


                    END;
            END;

        DECLARE absentCursor CURSOR FOR
            SELECT   *
            FROM     @InfoDates
            ORDER BY MeetDate;

        OPEN absentCursor;
        FETCH NEXT FROM absentCursor
        INTO @meetdate
            ,@actual;

        WHILE @@FETCH_STATUS = 0
            BEGIN

                --PRINT 'Record:' + CONVERT(VARCHAR(20),@recordcounter)
                IF @actual = 0
                    BEGIN
                        SET @consecutivecounter = @consecutivecounter + 1;
                    --PRINT '@consecutivecounter:' + CONVERT(VARCHAR(20),@consecutivecounter)
                    END;
                ELSE
                    BEGIN
                        IF @consecutivecounter > @prevconsecutivecounter
                            BEGIN
                                SET @prevconsecutivecounter = @consecutivecounter;
                            END;
                        SET @consecutivecounter = 0;
                    END;
                SET @prevmeetdate = @meetdate;
                SET @recordcounter = @recordcounter + 1;

                FETCH NEXT FROM absentCursor
                INTO @meetdate
                    ,@actual;
            END;

        CLOSE absentCursor;
        DEALLOCATE absentCursor;

        IF @consecutivecounter > @prevconsecutivecounter
            BEGIN
                SET @prevconsecutivecounter = @consecutivecounter;
            END;

        RETURN @prevconsecutivecounter;

    END;




GO
