SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE FUNCTION [dbo].[GetStudentName]
    (
     @StuEnrollId UNIQUEIDENTIFIER
	)
RETURNS VARCHAR(50)
AS
    BEGIN
        DECLARE @ReturnValue VARCHAR(50);
        SELECT  @ReturnValue = LastName + ' ' + FirstName
        FROM    arStudent
        WHERE   StudentId = (
                              SELECT    StudentId
                              FROM      arStuEnrollments
                              WHERE     StuEnrollId = @StuEnrollId
                            );
        RETURN @ReturnValue;
    END;


GO
