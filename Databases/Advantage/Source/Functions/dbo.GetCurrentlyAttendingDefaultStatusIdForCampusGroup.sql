SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Jose Astudillo
-- Create date: 4/12/2018
-- Description:	This function get the currently attending default status for the given campus group if found.
--				If not found, then returns the currently attending default status for campus groups all.
--				If still not found then return the system created currently attending status
-- =============================================
CREATE FUNCTION [dbo].[GetCurrentlyAttendingDefaultStatusIdForCampusGroup]
    (
        @campusGroupId UNIQUEIDENTIFIER
    )
RETURNS UNIQUEIDENTIFIER
AS
    BEGIN
        -- Declare the return variable here
        DECLARE @currentlyAttendingDefaultStatusId UNIQUEIDENTIFIER;

        -- Add the T-SQL statements to compute the return value here
        SELECT @currentlyAttendingDefaultStatusId = CASE WHEN EXISTS (
                                                                     SELECT TOP 1 *
                                                                     FROM   syStatusCodes
                                                                     WHERE  SysStatusId = 9
                                                                            AND StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
                                                                            AND IsDefaultLeadStatus = 1
                                                                            AND CampGrpId = @campusGroupId
                                                                     ) THEN (
                                                                            SELECT TOP 1 StatusCodeId
                                                                            FROM   syStatusCodes
                                                                            WHERE  SysStatusId = 9
                                                                                   AND StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
                                                                                   AND IsDefaultLeadStatus = 1
                                                                                   AND CampGrpId = @campusGroupId
                                                                            )
                                                         --when currently attending default status does not exist for campus group then try with campus group ALL
                                                         WHEN NOT EXISTS (
                                                                         SELECT TOP 1 *
                                                                         FROM   syStatusCodes
                                                                         WHERE  SysStatusId = 9
                                                                                AND StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
                                                                                AND IsDefaultLeadStatus = 1
                                                                                AND CampGrpId = @campusGroupId
                                                                         )
                                                              AND EXISTS (
                                                                         SELECT TOP 1 *
                                                                         FROM   syStatusCodes
                                                                         WHERE  SysStatusId = 9
                                                                                AND StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
                                                                                AND IsDefaultLeadStatus = 1
                                                                                AND CampGrpId IN (
                                                                                                 SELECT CampGrpId
                                                                                                 FROM   dbo.syCampGrps
                                                                                                 WHERE  IsAllCampusGrp = 1
                                                                                                        AND StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
                                                                                                 )
                                                                         ) THEN
        (
        SELECT TOP 1 StatusCodeId
        FROM   syStatusCodes
        WHERE  SysStatusId = 9
               AND StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
               AND IsDefaultLeadStatus = 1
               AND CampGrpId IN (
                                SELECT CampGrpId
                                FROM   dbo.syCampGrps
                                WHERE  IsAllCampusGrp = 1
                                       AND StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
                                )
        )
                                                         ELSE (
                                                              SELECT TOP 1 StatusCodeId
                                                              FROM   syStatusCodes
                                                              WHERE  SysStatusId = 9
                                                                     AND StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
                                                                     AND CampGrpId IN (
                                                                                      SELECT CampGrpId
                                                                                      FROM   dbo.syCampGrps
                                                                                      WHERE  IsAllCampusGrp = 1
                                                                                             AND StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
                                                                                      )
                                                              )
                                                    END;

        -- Return the result of the function
        RETURN @currentlyAttendingDefaultStatusId;

    END;


GO
