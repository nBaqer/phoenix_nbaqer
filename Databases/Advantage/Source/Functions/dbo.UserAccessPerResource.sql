SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- ===========================================
-- Author:Kimberly Befera FAME Inc
-- Create date: 8/22/2019
-- Description: Returns the campus groups the user has access edit to for the given page  
CREATE   FUNCTION [dbo].[UserAccessPerResource]
(
    @UserId UNIQUEIDENTIFIER,
    @ResourceID VARCHAR(50)
)
RETURNS @Results TABLE
(
    CampGrpId UNIQUEIDENTIFIER,
    CampGrpDescrip VARCHAR(50),
    IsAllCampusGrp BIT,
    AccessLevel INT
)
AS
BEGIN
    DECLARE @AccessLevel TABLE
    (
        CampGrpId UNIQUEIDENTIFIER,
        CampGrpDescrip VARCHAR(50),
        IsAllCampusGrp BIT,
        AccessLevel INT
    );

    DECLARE @HasAllAccess INT;
    DECLARE @IsSupport INT;
    SET @IsSupport = 0;
    SET @HasAllAccess = 0;
    SET @IsSupport =
    (
        SELECT COUNT(UserName)
        FROM dbo.syUsers
        WHERE UserId = @UserId
              AND IsAdvantageSuperUser = 1
    );
    SET @HasAllAccess =
    (
        SELECT COUNT(*)
        FROM syUsersRolesCampGrps UTCG
            JOIN syCampGrps CG
                ON CG.CampGrpId = UTCG.CampGrpId
        WHERE IsAllCampusGrp = 1
              AND UserId = @UserId
    );


    IF (@IsSupport + @HasAllAccess) > 0
    BEGIN
        INSERT @Results
        SELECT DISTINCT
               CG.CampGrpId,
               CG.CampGrpDescrip,
               CG.IsAllCampusGrp,
               15 AS AccessLevel
        FROM syCampGrps CG
            JOIN dbo.syStatuses ST
                ON ST.StatusId = CG.StatusId
        WHERE ST.Status = 'Active'
        ORDER BY CG.IsAllCampusGrp DESC,
                 CG.CampGrpDescrip;

    END;
    ELSE
    BEGIN

        INSERT @Results
        SELECT DISTINCT
               URCG.CampGrpId,
               CG.CampGrpDescrip,
               CG.IsAllCampusGrp,
               MAX(RRL.AccessLevel) AS AccessLevel
        FROM syCampGrps CG
            JOIN syUsersRolesCampGrps URCG
                ON URCG.CampGrpId = CG.CampGrpId
            JOIN syRlsResLvls RRL
                ON RRL.RoleId = URCG.RoleId
            JOIN syResources R
                ON R.ResourceID = RRL.ResourceID
            JOIN dbo.syStatuses ST
                ON ST.StatusId = CG.StatusId
        WHERE ST.Status = 'Active'
              AND URCG.UserId = @UserId
              AND RRL.ResourceID = @ResourceID
        GROUP BY URCG.CampGrpId,
                 CG.CampGrpDescrip,
                 CG.IsAllCampusGrp
        ORDER BY CG.IsAllCampusGrp DESC,
                 CG.CampGrpDescrip;


    END;

    RETURN;
END;

GO
