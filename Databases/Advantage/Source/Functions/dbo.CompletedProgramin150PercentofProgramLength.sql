SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

--=================================================================================================  
-- CompletedProgramin150PercentofProgramLength  
--=================================================================================================  
CREATE FUNCTION [dbo].[CompletedProgramin150PercentofProgramLength]
    (
        @StuEnrollId CHAR(36)
       ,@allDegree CHAR(1) = 'D'
    )
RETURNS BIT
--RETURNS DECIMAL(18,2)    
AS
    BEGIN
        DECLARE @mtf DECIMAL(18, 2);
        DECLARE @progid UNIQUEIDENTIFIER;
        DECLARE @undergradprogram BIT;
        DECLARE @credithourprogram BIT;
        DECLARE @CreditsEarned DECIMAL(18, 2);
        DECLARE @HoursCompleted DECIMAL(18, 2);
        DECLARE @IsProgramComplete BIT;
        DECLARE @CreditsAttempted DECIMAL(18, 2);
        DECLARE @maxcredits DECIMAL(18, 2);
        DECLARE @percentagecredits DECIMAL(18, 2);
        DECLARE @percentageattempted DECIMAL(18, 2);

        SET @percentagecredits = 0.00;
        SET @percentageattempted = 0.00;

        SET @progid = (
                      SELECT ProgId
                      FROM   arPrgVersions pv
                            ,dbo.arStuEnrollments se
                      WHERE  pv.PrgVerId = se.PrgVerId
                             AND se.StuEnrollId = @StuEnrollId
                      );

        SET @undergradprogram = (
                                SELECT dbo.isProgramUndergraduate(@progid, @allDegree)
                                );
        SET @credithourprogram = (
                                 SELECT dbo.isProgramCreditHour(@progid)
                                 );

        SET @IsProgramComplete = 0;

        --Undergrad and credit hour    
        IF (
           @undergradprogram = 1
           AND @credithourprogram = 1
           )
            BEGIN

                SET @mtf = (
                           SELECT ISNULL(pv.Credits, 0.00)
                           FROM   arPrgVersions pv
                                 ,dbo.arStuEnrollments se
                           WHERE  pv.PrgVerId = se.PrgVerId
                                  AND se.StuEnrollId = @StuEnrollId
                           );

                SET @maxcredits = ISNULL(@mtf, 0) * 1.50;

                SET @percentagecredits = @mtf / @maxcredits;


                SET @CreditsEarned = (
                                     SELECT ISNULL(SUM(CS.CreditsEarned), 0.00)
                                     FROM   dbo.arStuEnrollments se
                                     INNER JOIN dbo.syCreditSummary CS ON CS.StuEnrollId = se.StuEnrollId
                                     INNER JOIN syStatusCodes sc ON sc.StatusCodeId = se.StatusCodeId
                                     WHERE  se.StuEnrollId = @StuEnrollId
                                            AND sc.SysStatusId = 14
                                     );

                SET @CreditsAttempted = (
                                        SELECT ISNULL(SUM(CS.CreditsAttempted), 0.00)
                                        FROM   dbo.arStuEnrollments se
                                        INNER JOIN dbo.syCreditSummary CS ON CS.StuEnrollId = se.StuEnrollId
                                        INNER JOIN syStatusCodes sc ON sc.StatusCodeId = se.StatusCodeId
                                        WHERE  se.StuEnrollId = @StuEnrollId
                                               AND sc.SysStatusId = 14
                                        );



                IF ( @CreditsAttempted <> 0.00 )
                    BEGIN
                        SET @percentageattempted = @CreditsEarned / @CreditsAttempted;
                    END;

                IF (
                   @percentageattempted <> 0.00
                   AND @percentageattempted >= @percentagecredits
                   )
                    BEGIN
                        SET @IsProgramComplete = 1;
                    END;


            END;

        --undergrad and clock hour    
        IF (
           @undergradprogram = 1
           AND @credithourprogram = 0
           )
            BEGIN
                SET @mtf = (
                           SELECT ISNULL(pv.Hours, 0.00) * 1.50
                           FROM   arPrgVersions pv
                                 ,dbo.arStuEnrollments se
                           WHERE  pv.PrgVerId = se.PrgVerId
                                  AND se.StuEnrollId = @StuEnrollId
                           );

                SET @HoursCompleted = (
                                      SELECT   TOP 1 ISNULL(CS.AdjustedPresentDays, 0.00)
                                      FROM     arPrgVersions pv
                                      INNER JOIN dbo.arStuEnrollments se ON se.PrgVerId = pv.PrgVerId
                                      INNER JOIN dbo.syStudentAttendanceSummary CS ON CS.StuEnrollId = se.StuEnrollId
                                      INNER JOIN syStatusCodes sc ON sc.StatusCodeId = se.StatusCodeId
                                      WHERE    se.StuEnrollId = @StuEnrollId
                                               AND sc.SysStatusId = 14
                                      ORDER BY StudentAttendedDate DESC
                                      );

                IF (
                   @HoursCompleted <> 0.00
                   AND @HoursCompleted <= @mtf
                   )
                    BEGIN
                        SET @IsProgramComplete = 1;
                    END;
            END;


        RETURN @IsProgramComplete;
    END;
--=================================================================================================  
-- CompletedProgramin150PercentofProgramLength  
--=================================================================================================  
GO
