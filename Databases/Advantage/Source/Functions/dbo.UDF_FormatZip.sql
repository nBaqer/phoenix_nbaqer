SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- **********************************************************************************************************************
-- Convert unformatted Zip Numbers (i.e., no embedded hyphens) to standard "xxx-xx-xxxx" Zip display format.
-- Example: 
--	PRINT dbo.udfConvertUnformattedZipToFormattedZip('123456789') --returns '123-45-6789' 
-- ***********************************************************************************************************************
CREATE FUNCTION [dbo].[UDF_FormatZip] ( @Zip CHAR(5) )
RETURNS CHAR(5)
AS
    BEGIN
        DECLARE @Mask VARCHAR(20)
           ,@MaskChar CHAR(1)
           ,@FormattedZip VARCHAR(20)
           ,@intLoop INT;
--declare @Zip varchar(9)
        DECLARE @intInnerLoop INT;
        SET @Mask = (
                      SELECT    Mask
                      FROM      syInputMasks
                      WHERE     InputMaskId = 3
                    );
--set @Zip='123456789'
        SET @FormattedZip = '';
        SET @intLoop = 0;
        SET @intInnerLoop = 0;
        WHILE ( @intLoop < LEN(@Mask) )
            BEGIN
                SET @intLoop = @intLoop + 1;
                SET @MaskChar = SUBSTRING(@Mask,@intLoop,1);
                IF LTRIM(RTRIM(@MaskChar)) = '#'
                    BEGIN
                        SET @intInnerLoop = @intInnerLoop + 1;
                        SET @FormattedZip = @FormattedZip + SUBSTRING(@Zip,@intInnerLoop,1); 
                    END;
                ELSE
                    BEGIN
                        IF @intLoop = 1
                            BEGIN
                                SET @FormattedZip = @MaskChar; 
                            END;
                        ELSE
                            BEGIN
                                SET @FormattedZip = @FormattedZip + @MaskChar;
                            END;
                    END;
            END;
        RETURN @FormattedZip;
    END; 

GO
