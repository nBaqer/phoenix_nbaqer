SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--=================================================================================================  
-- USP_PostClockAttendance_Insert  
--=================================================================================================  
CREATE PROCEDURE [dbo].[USP_PostClockAttendance_Insert]
    @AttValues NTEXT
AS
    BEGIN
        DECLARE @Error AS INTEGER;
        SET @Error = 0;

        BEGIN TRANSACTION AddMissingAttendanceRecords;
        BEGIN TRY

            IF @Error = 0
                BEGIN
                    DECLARE @hDoc INT;
                    DECLARE @modUser VARCHAR(50);

                    --Prepare input values as an XML document   
                    EXEC sp_xml_preparedocument @hDoc OUTPUT
                                               ,@AttValues;

                    -- temp table to find current default statuses per campus group  
                    DECLARE @CampusGroupStatuses TABLE
                        (
                            CampGrpId UNIQUEIDENTIFIER NOT NULL
                           ,StatusCodeId UNIQUEIDENTIFIER NOT NULL
                        );
                    INSERT INTO @CampusGroupStatuses
                                SELECT   pv.CampGrpId
                                        ,StatusCodeId = dbo.GetCurrentlyAttendingDefaultStatusIdForCampusGroup(pv.CampGrpId)
                                FROM     arStuEnrollments b
                                        ,dbo.arPrgVersions pv
                                WHERE    b.PrgVerId = pv.PrgVerId
                                         AND b.StuEnrollId IN (
                                                              SELECT StuEnrollId
                                                              FROM
                                                                     OPENXML(@hDoc, '/NewDataSet/InsertPostClockAttendance', 1)
                                                                         WITH (
                                                                              StuEnrollId VARCHAR(50)
                                                                              )
                                                              )
                                         AND b.StatusCodeId IN (
                                                               SELECT DISTINCT StatusCodeId
                                                               FROM   syStatusCodes
                                                               WHERE  SysStatusId = 7
                                                               )
                                GROUP BY pv.CampGrpId;

                    -- get Mod user from document  
                    SELECT @modUser = (
                                      SELECT TOP 1 ModUser
                                      FROM
                                             OPENXML(@hDoc, '/NewDataSet/InsertPostClockAttendance', 1)
                                                 WITH (
                                                      ModUser VARCHAR(50)
                                                      )
                                      );

                    -- Delete all records from arStudentClockAttendance table based on StuEnrollId,ScheduleId, RecordDate   
                    DELETE FROM arStudentClockAttendance
                    WHERE StuEnrollId IN (
                                         SELECT StuEnrollId
                                         FROM
                                                OPENXML(@hDoc, '/NewDataSet/InsertPostClockAttendance', 1)
                                                    WITH (
                                                         StuEnrollId VARCHAR(50)
                                                         )
                                         )
                          --AND ScheduleId IN (
                          --                  SELECT ScheduleId
                          --                  FROM
                          --                         OPENXML(@hDoc, '/NewDataSet/InsertPostClockAttendance', 1)
                          --                             WITH (
                          --                                  ScheduleId VARCHAR(50)
                          --                                  )
                          --                  )
                          AND RecordDate IN (
                                            SELECT RecordDate
                                            FROM
                                                   OPENXML(@hDoc, '/NewDataSet/InsertPostClockAttendance', 1)
                                                       WITH (
                                                            RecordDate VARCHAR(10)
                                                            )
                                            );


                    ---begin code by Balaji to insert into the summary table for progress reports   
                    DELETE FROM syStudentAttendanceSummary
                    WHERE StuEnrollId IN (
                                         SELECT StuEnrollId
                                         FROM
                                                OPENXML(@hDoc, '/NewDataSet/InsertPostClockAttendance', 1)
                                                    WITH (
                                                         StuEnrollId VARCHAR(50)
                                                         )
                                         )
                          AND StudentAttendedDate IN (
                                                     SELECT RecordDate
                                                     FROM
                                                            OPENXML(@hDoc, '/NewDataSet/InsertPostClockAttendance', 1)
                                                                WITH (
                                                                     RecordDate VARCHAR(10)
                                                                     )
                                                     );
                    IF EXISTS (
                              SELECT *
                              FROM   sysobjects
                              WHERE  type = 'TR'
                                     AND name = 'TR_InsertAttendanceSummary_ByDay_PA'
                              )
                        BEGIN
                            DISABLE TRIGGER TR_InsertAttendanceSummary_ByDay_PA ON arStudentClockAttendance;
                        END;
                    IF EXISTS (
                              SELECT *
                              FROM   sysobjects
                              WHERE  type = 'TR'
                                     AND name = 'TR_InsertAttendanceSummary_ByClockHour_Minutes'
                              )
                        BEGIN
                            DISABLE TRIGGER TR_InsertAttendanceSummary_ByClockHour_Minutes ON arStudentClockAttendance;
                        END;
                    ---end code by Balaji to insert into the summary table for progress reports  
                    -- Insert records into arStudentClockAttendance Table   

                    INSERT INTO arStudentClockAttendance (
                                                         StuEnrollId
                                                        ,ScheduleId
                                                        ,RecordDate
                                                        ,SchedHours
                                                        ,ActualHours
                                                        ,ModDate
                                                        ,ModUser
                                                        ,isTardy
                                                        ,PostByException
                                                        ,comments
                                                        ,TardyProcessed
                                                        ,Converted
                                                         )
                                SELECT StuEnrollId
                                      ,ScheduleId
                                      ,RecordDate
                                      ,SchedHours
                                      ,ActualHours
                                      ,ModDate
                                      ,ModUser
                                      ,IsTardy
                                      ,PostByException
                                      ,''
                                      ,TardyProcessed
                                      ,0
                                FROM
                                       OPENXML(@hDoc, '/NewDataSet/InsertPostClockAttendance', 1)
                                           WITH (
                                                StuEnrollId UNIQUEIDENTIFIER
                                               ,ScheduleId UNIQUEIDENTIFIER
                                               ,RecordDate VARCHAR(10)
                                               ,SchedHours DECIMAL(18, 2)
                                               ,ActualHours DECIMAL(18, 2)
                                               ,ModDate VARCHAR(10)
                                               ,ModUser VARCHAR(50)
                                               ,IsTardy BIT
                                               ,PostByException VARCHAR(10)
                                               ,comments VARCHAR(240)
                                               ,TardyProcessed BIT
                                               ,Converted BIT
                                                );


                    -- Insert into history table 4/11/2018  
                    INSERT INTO dbo.syStudentStatusChanges (
                                                           StuEnrollId
                                                          ,OrigStatusId
                                                          ,NewStatusId
                                                          ,CampusId
                                                          ,ModDate
                                                          ,ModUser
                                                          ,IsReversal
                                                          ,DropReasonId
                                                          ,DateOfChange
                                                          ,Lda
                                                          ,CaseNumber
                                                          ,RequestedBy
                                                          ,HaveBackup
                                                          ,HaveClientConfirmation
                                                           )
                                SELECT StuEnrollId
                                      ,e.StatusCodeId OrigStatusId
                                      ,cgs.StatusCodeId NewStatusId
                                      ,e.CampusId CampusId
                                      ,GETDATE() ModDate
                                      ,@modUser ModUser
                                      ,0 IsReversal
                                      ,NULL DropReasonId
                                      ,GETDATE() DateOfChange
                                      ,NULL Lda
                                      ,NULL CaseNumber
                                      ,NULL RequestedBy
                                      ,NULL HaveBackup
                                      ,NULL HaveClientConfirmation
                                FROM   dbo.arStuEnrollments e
                                JOIN   dbo.adLeads l ON e.StudentId = l.StudentId
                                JOIN   dbo.arPrgVersions pv ON e.PrgVerId = pv.PrgVerId
                                JOIN   @CampusGroupStatuses cgs ON pv.CampGrpId = cgs.CampGrpId
                                WHERE  e.StuEnrollId IN (
                                                        SELECT StuEnrollId
                                                        FROM
                                                               OPENXML(@hDoc, '/NewDataSet/InsertPostClockAttendance', 1)
                                                                   WITH (
                                                                        StuEnrollId VARCHAR(50)
                                                                        )
                                                        )
                                       AND e.StatusCodeId IN (
                                                             SELECT DISTINCT StatusCodeId
                                                             FROM   syStatusCodes
                                                             WHERE  SysStatusId = 7
                                                             );

                    --update b set StatusCodeId=(select Distinct StatusCodeId from syStatusCodes where StatusCodeDescrip='Currently Attending')   
                    --from arStudentClockAttendance a,arStuEnrollments b where a.StuEnrollId=b.StuEnrollId and a.ActualHours<>9999.00 and a.ActualHours<>999.00   
                    --and a.StuEnrollId in (select StuEnrollId from OPENXML(@hDoc,'/NewDataSet/InsertPostClockAttendance',1)   
                    --       with (StuEnrollId varchar(50)))   
                    --and b.StatusCodeId=(select Distinct StatusCodeId from syStatusCodes where StatusCodeDescrip='Future Start')   
                    --DE8240  Aug 15  
                    -- DE8192 OCt 05 2012  
                    --UPDATE  b  
                    --SET     StatusCodeId = (  
                    --                         SELECT TOP 1  
                    --                                StatusCodeId  
                    --                         FROM   syStatusCodes  
                    --                         WHERE  SysStatusId = 9  
                    --                                AND StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'  
                    --                       )  
                    --FROM    arStudentClockAttendance a  
                    --       ,arStuEnrollments b  
                    --WHERE   a.StuEnrollId = b.StuEnrollId  
                    --        AND a.ActualHours <> 9999.00  
                    --        AND a.ActualHours <> 999.00  
                    --        AND a.StuEnrollId IN ( SELECT   StuEnrollId  
                    --                               FROM     OPENXML(@hDoc,'/NewDataSet/InsertPostClockAttendance',1)   
                    --WITH (StuEnrollId VARCHAR(50)) )  
                    --        AND b.StatusCodeId IN ( SELECT DISTINCT  
                    --                                        StatusCodeId  
                    --                                FROM    syStatusCodes  
                    --                                WHERE   SysStatusId = 7 );   
                    UPDATE b
                    SET    StatusCodeId = cgs.StatusCodeId
                    FROM   arStudentClockAttendance a
                    JOIN   arStuEnrollments b ON a.StuEnrollId = b.StuEnrollId
                    JOIN   arPrgVersions pv ON pv.PrgVerId = b.PrgVerId
                    JOIN   @CampusGroupStatuses cgs ON cgs.CampGrpId = pv.CampGrpId
                    WHERE  a.ActualHours <> 9999.00
                           AND a.ActualHours <> 999.00
                           AND a.StuEnrollId IN (
                                                SELECT StuEnrollId
                                                FROM
                                                       OPENXML(@hDoc, '/NewDataSet/InsertPostClockAttendance', 1)
                                                           WITH (
                                                                StuEnrollId VARCHAR(50)
                                                                )
                                                )
                           AND b.StatusCodeId IN (
                                                 SELECT DISTINCT StatusCodeId
                                                 FROM   syStatusCodes
                                                 WHERE  SysStatusId = 7
                                                 );




                    --update arStuEnrollments start date with expected start date  
                    UPDATE c
                    SET    StartDate = ExpStartDate
                    FROM   arStudentClockAttendance a
                          ,arStuEnrollments c
                    WHERE  a.StuEnrollId = c.StuEnrollId
                           AND a.StuEnrollId IN (
                                                SELECT StuEnrollId
                                                FROM
                                                       OPENXML(@hDoc, '/NewDataSet/InsertPostClockAttendance', 1)
                                                           WITH (
                                                                StuEnrollId VARCHAR(50)
                                                                )
                                                )
                           AND c.StartDate IS NULL;



                    --Select * from arAttUnitType  

                    DECLARE @StuEnrollId UNIQUEIDENTIFIER
                           ,@MeetDate DATETIME
                           ,@WeekDay VARCHAR(15)
                           ,@StartDate DATETIME
                           ,@EndDate DATETIME;
                    DECLARE @PeriodDescrip VARCHAR(50)
                           ,@Actual DECIMAL(18, 2)
                           ,@Excused DECIMAL(18, 2)
                           ,@ClsSectionId UNIQUEIDENTIFIER;
                    DECLARE @Absent DECIMAL(18, 2)
                           ,@ScheduledMinutes DECIMAL(18, 2)
                           ,@TardyMinutes DECIMAL(18, 2);
                    DECLARE @tardy DECIMAL(18, 2)
                           ,@tracktardies INT
                           ,@tardiesMakingAbsence INT
                           ,@PrgVerId UNIQUEIDENTIFIER
                           ,@rownumber INT
                           ,@IsTardy BIT
                           ,@ActualRunningScheduledHours DECIMAL(18, 2);
                    DECLARE @ActualRunningPresentHours DECIMAL(18, 2)
                           ,@ActualRunningAbsentHours DECIMAL(18, 2)
                           ,@ActualRunningTardyHours DECIMAL(18, 2)
                           ,@ActualRunningMakeupHours DECIMAL(18, 2);
                    DECLARE @PrevStuEnrollId UNIQUEIDENTIFIER
                           ,@intTardyBreakPoint INT
                           ,@AdjustedRunningPresentHours DECIMAL(18, 2)
                           ,@AdjustedRunningAbsentHours DECIMAL(18, 2)
                           ,@ActualRunningScheduledDays DECIMAL(18, 2)
                           ,@MakeUpHours DECIMAL(18, 2);


                    DECLARE @StuEnrollCampusId UNIQUEIDENTIFIER
                           ,@trackattendanceby VARCHAR(50)
                           ,@UnitTypeId UNIQUEIDENTIFIER;
                    SET @StuEnrollCampusId = (
                                             SELECT TOP 1 CampusId
                                             FROM   arStuEnrollments
                                             WHERE  StuEnrollId IN (
                                                                   SELECT StuEnrollId
                                                                   FROM
                                                                          OPENXML(@hDoc, '/NewDataSet/InsertPostClockAttendance', 1)
                                                                              WITH (
                                                                                   StuEnrollId VARCHAR(50)
                                                                                   )
                                                                   )
                                             );

                    SET @trackattendanceby = (
                                             SELECT dbo.GetAppSettingValue(72, @StuEnrollCampusId)
                                             );

                    SET @UnitTypeId = (
                                      SELECT TOP 1 t1.UnitTypeId
                                      FROM   arPrgVersions t1
                                            ,arStuEnrollments t2
                                      WHERE  t1.PrgVerId = t2.PrgVerId
                                             AND t2.StuEnrollId IN (
                                                                   SELECT StuEnrollId
                                                                   FROM
                                                                          OPENXML(@hDoc, '/NewDataSet/InsertPostClockAttendance', 1)
                                                                              WITH (
                                                                                   StuEnrollId VARCHAR(50)
                                                                                   )
                                                                   )
                                      );

                    -- by Class and P/A  
                    IF ( @UnitTypeId = 'EF5535C2-142C-4223-AE3C-25A50A153CC6' )
                       AND LOWER(@trackattendanceby) = 'byclass'
                        BEGIN
                            PRINT 'Step 2';

                            DECLARE GetAttendance_Cursor CURSOR FORWARD_ONLY FAST_FORWARD FOR
                                SELECT   *
                                        ,ROW_NUMBER() OVER ( ORDER BY MeetDate ) AS RowNumber
                                FROM     (
                                         SELECT DISTINCT t1.StuEnrollId
                                               ,t1.ClsSectionId
                                               ,t1.MeetDate
                                               ,DATENAME(dw, t1.MeetDate) AS WeekDay
                                               ,t4.StartDate
                                               ,t4.EndDate
                                               ,t5.PeriodDescrip
                                               ,t1.Actual
                                               ,t1.Excused
                                               ,CASE WHEN (
                                                          t1.Actual = 0
                                                          AND t1.Excused = 0
                                                          ) THEN t1.Scheduled
                                                     ELSE CASE WHEN (
                                                                    t1.Actual <> 9999.00
                                                                    AND t1.Actual < t1.Scheduled
                                                                    AND t1.Excused <> 1
                                                                    ) THEN ( t1.Scheduled - t1.Actual )
                                                               ELSE 0
                                                          END
                                                END AS Absent
                                               ,t1.Scheduled AS ScheduledMinutes
                                               ,CASE WHEN (
                                                          t1.Actual > 0
                                                          AND t1.Actual < t1.Scheduled
                                                          ) THEN ( t1.Scheduled - t1.Actual )
                                                     ELSE 0
                                                END AS TardyMinutes
                                               ,t1.Tardy AS Tardy
                                               ,t3.TrackTardies
                                               ,t3.TardiesMakingAbsence
                                               ,t3.PrgVerId
                                         FROM   atClsSectAttendance t1
                                         INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                                         INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                                         INNER JOIN arClsSectMeetings t4 ON t1.ClsSectionId = t4.ClsSectionId
                                                                            AND (
                                                                                CONVERT(DATE, t1.MeetDate, 111) >= CONVERT(DATE, t4.StartDate, 111)
                                                                                AND CONVERT(DATE, t1.MeetDate, 111) <= CONVERT(DATE, t4.EndDate, 111)
                                                                                )
                                                                            AND t1.ClsSectMeetingId = t4.ClsSectMeetingId --DE8479 line added  
                                         INNER JOIN syPeriods t5 ON t4.PeriodId = t5.PeriodId
                                         INNER JOIN cmTimeInterval t6 ON t5.StartTimeId = t6.TimeIntervalId
                                         INNER JOIN cmTimeInterval t7 ON t5.EndTimeId = t7.TimeIntervalId
                                         INNER JOIN dbo.arClassSections t9 ON t9.ClsSectionId = t1.ClsSectionId
                                         INNER JOIN dbo.arReqs t10 ON t10.ReqId = t9.ReqId
                                         WHERE  t1.StuEnrollId IN (
                                                                  SELECT StuEnrollId
                                                                  FROM
                                                                         OPENXML(@hDoc, '/NewDataSet/InsertPostClockAttendance', 1)
                                                                             WITH (
                                                                                  StuEnrollId VARCHAR(50)
                                                                                  )
                                                                  )
                                                AND (
                                                    t10.UnitTypeId IN ( '2600592A-9739-4A13-BDCE-7A25FE4A7478', 'EF5535C2-142C-4223-AE3C-25A50A153CC6' )
                                                    OR t3.UnitTypeId IN ( '2600592A-9739-4A13-BDCE-7A25FE4A7478', 'EF5535C2-142C-4223-AE3C-25A50A153CC6' )
                                                    ) -- Minutes  
                                                AND t1.Actual <> 9999
                                         ) dt
                                ORDER BY StuEnrollId
                                        ,MeetDate;
                            DECLARE @boolReset BIT
                                   ,@AdjustedPresentDaysComputed DECIMAL(18, 2);
                            OPEN GetAttendance_Cursor;
                            FETCH NEXT FROM GetAttendance_Cursor
                            INTO @StuEnrollId
                                ,@ClsSectionId
                                ,@MeetDate
                                ,@WeekDay
                                ,@StartDate
                                ,@EndDate
                                ,@PeriodDescrip
                                ,@Actual
                                ,@Excused
                                ,@Absent
                                ,@ScheduledMinutes
                                ,@TardyMinutes
                                ,@tardy
                                ,@tracktardies
                                ,@tardiesMakingAbsence
                                ,@PrgVerId
                                ,@rownumber;
                            SET @ActualRunningPresentHours = 0;
                            SET @ActualRunningPresentHours = 0;
                            SET @ActualRunningAbsentHours = 0;
                            SET @ActualRunningTardyHours = 0;
                            SET @ActualRunningMakeupHours = 0;
                            SET @intTardyBreakPoint = 0;
                            SET @AdjustedRunningPresentHours = 0;
                            SET @AdjustedRunningAbsentHours = 0;
                            SET @ActualRunningScheduledDays = 0;
                            SET @boolReset = 0;
                            SET @MakeUpHours = 0;
                            WHILE @@FETCH_STATUS = 0
                                BEGIN

                                    IF @PrevStuEnrollId <> @StuEnrollId
                                        BEGIN
                                            SET @ActualRunningPresentHours = 0;
                                            SET @ActualRunningAbsentHours = 0;
                                            SET @intTardyBreakPoint = 0;
                                            SET @ActualRunningTardyHours = 0;
                                            SET @AdjustedRunningPresentHours = 0;
                                            SET @AdjustedRunningAbsentHours = 0;
                                            SET @ActualRunningScheduledDays = 0;
                                            SET @MakeUpHours = 0;
                                            SET @boolReset = 1;
                                        END;


                                    -- Scheduled and Actual: Calculate Students Actual and Adjusted Running Present Hours/Min/Day  
                                    IF @Actual <> 9999.00
                                        BEGIN
                                            SET @ActualRunningPresentHours = @ActualRunningPresentHours + @Actual + @Excused;
                                            SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours + @Actual + @Excused;

                                            -- If there are make up hrs deduct that otherwise it will be added again in progress report  
                                            IF (
                                               @Actual > 0
                                               AND @Actual > @ScheduledMinutes
                                               AND @Actual <> 9999.00
                                               )
                                                BEGIN
                                                    SET @ActualRunningPresentHours = @ActualRunningPresentHours - ( @Actual - @ScheduledMinutes );
                                                    SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - ( @Actual - @ScheduledMinutes );
                                                END;

                                            SET @ActualRunningScheduledDays = @ActualRunningScheduledDays + @ScheduledMinutes;
                                        END;

                                    -- Absent: Calculate Students Actual and Adjusted Running Absent Hours/Min/Day  
                                    SET @ActualRunningAbsentHours = @ActualRunningAbsentHours + @Absent;
                                    SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Absent;

                                    -- Tardy: Calculate Students Actual Running Tardy Hours/Min/Day     
                                    IF (
                                       @Actual > 0
                                       AND @Actual < @ScheduledMinutes
                                       AND @tardy = 1
                                       )
                                        BEGIN
                                            SET @ActualRunningTardyHours = @ActualRunningTardyHours + ( @ScheduledMinutes - @Actual );
                                        END;

                                    -- Track how many days student has been tardy only when   
                                    -- program version requires to track tardy  
                                    IF (
                                       @tracktardies = 1
                                       AND @tardy = 1
                                       )
                                        BEGIN
                                            SET @intTardyBreakPoint = @intTardyBreakPoint + 1;
                                        END;


                                    -- Tardy: If student is tardy multiple times and when the number of days student is tardy matches  
                                    -- the tardy rule set (for ex: 2 Tardies make 1 Absence). This rule indicates that  
                                    -- when student is tardy the second time, that second occurance will be considered as  
                                    -- absence  
                                    -- Variable @intTardyBreakpoint tracks how many times the student was tardy  
                                    -- Variable @tardiesMakingAbsence tracks the tardy rule  
                                    IF (
                                       @tracktardies = 1
                                       AND @intTardyBreakPoint = @tardiesMakingAbsence
                                       )
                                        BEGIN
                                            SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - @Actual - @Excused;
                                            SET @AdjustedRunningAbsentHours = ( @AdjustedRunningAbsentHours - @Absent ) + @ScheduledMinutes; --@TardyMinutes  
                                            SET @intTardyBreakPoint = 0;
                                        END;

                                    -- Makeup: Calculate Makeup Hours when Student Attended more than scheduled hours                    
                                    IF (
                                       @Actual > 0
                                       AND @Actual > @ScheduledMinutes
                                       AND @Actual <> 9999.00
                                       )
                                        BEGIN
                                            SET @MakeUpHours = @MakeUpHours + ( @Actual - @ScheduledMinutes );
                                        END;

                                    IF ( @tracktardies = 1 )
                                        BEGIN
                                            SET @AdjustedPresentDaysComputed = @AdjustedRunningPresentHours; --+IsNULL(@ActualRunningTardyHours,0)  
                                        END;
                                    ELSE
                                        BEGIN
                                            SET @AdjustedPresentDaysComputed = @AdjustedRunningPresentHours;
                                        END;

                                    DELETE FROM syStudentAttendanceSummary
                                    WHERE StuEnrollId = @StuEnrollId
                                          AND ClsSectionId = @ClsSectionId
                                          AND StudentAttendedDate = @MeetDate;
                                    INSERT INTO syStudentAttendanceSummary (
                                                                           StuEnrollId
                                                                          ,ClsSectionId
                                                                          ,StudentAttendedDate
                                                                          ,ScheduledDays
                                                                          ,ActualDays
                                                                          ,ActualRunningScheduledDays
                                                                          ,ActualRunningPresentDays
                                                                          ,ActualRunningAbsentDays
                                                                          ,ActualRunningMakeupDays
                                                                          ,ActualRunningTardyDays
                                                                          ,AdjustedPresentDays
                                                                          ,AdjustedAbsentDays
                                                                          ,AttendanceTrackType
                                                                          ,ModUser
                                                                          ,ModDate
                                                                          ,tardiesmakingabsence
                                                                           )
                                    VALUES ( @StuEnrollId
                                            ,@ClsSectionId
                                            ,@MeetDate
                                            ,@ScheduledMinutes
                                            ,@Actual
                                            ,@ActualRunningScheduledDays
                                            ,@ActualRunningPresentHours
                                            ,@ActualRunningAbsentHours
                                            ,ISNULL(@MakeUpHours, 0)
                                            ,@ActualRunningTardyHours
                                            ,@AdjustedPresentDaysComputed
                                            ,@AdjustedRunningAbsentHours
                                            ,'Post Attendance by Class Min'
                                            ,'sa'
                                            ,GETDATE()
                                            ,@tardiesMakingAbsence
                                           );

                                    --update syStudentAttendanceSummary set TardiesMakingAbsence=@TardiesMakingAbsence where StuEnrollId=@StuEnrollId  
                                    SET @PrevStuEnrollId = @StuEnrollId;

                                    FETCH NEXT FROM GetAttendance_Cursor
                                    INTO @StuEnrollId
                                        ,@ClsSectionId
                                        ,@MeetDate
                                        ,@WeekDay
                                        ,@StartDate
                                        ,@EndDate
                                        ,@PeriodDescrip
                                        ,@Actual
                                        ,@Excused
                                        ,@Absent
                                        ,@ScheduledMinutes
                                        ,@TardyMinutes
                                        ,@tardy
                                        ,@tracktardies
                                        ,@tardiesMakingAbsence
                                        ,@PrgVerId
                                        ,@rownumber;
                                END;
                            CLOSE GetAttendance_Cursor;
                            DEALLOCATE GetAttendance_Cursor;
                        END;
                    --select * from arAttUnitType  
                    --else if (@UnitTypeId='EF5535C2-142C-4223-AE3C-25A50A153CC6') and lower(@trackattendanceby)<>'byclass'  
                    -- begin  
                    --  DECLARE GetAttendance_Cursor CURSOR FORWARD_ONLY FAST_FORWARD FOR  
                    --  Select t1.StuEnrollId,t1.RecordDate,t1.ActualHours,t1.SchedHours,  
                    --    Case when ((t1.SchedHours >= 1 and t1.SchedHours not in (999,9999)) and t1.ActualHours=0)  Then t1.SchedHours else 0 end as Absent,  
                    --    t1.IsTardy,  
                    --      t3.TrackTardies,  
                    --    t3.tardiesMakingAbsence  
                    --  from  
                    --   arStudentClockAttendance t1 inner join arStuEnrollments t2 on t1.StuEnrollId=t2.StuEnrollId   
                    --   inner join arPrgVersions t3 on t2.PrgVerId=t3.PrgVerId   
                    --  where  
                    --    t1.StuEnrollId IN (  
                    --                                SELECT  StuEnrollId  
                    --                                FROM    OPENXML(@hDoc,'/NewDataSet/InsertPostClockAttendance',1)   
                    --         WITH (StuEnrollId VARCHAR(50))) and  
                    --   t3.UnitTypeId in ('ef5535c2-142c-4223-ae3c-25a50a153cc6') and t1.ActualHours <> 9999.00  
                    --  order by t1.StuEnrollId, t1.RecordDate  
                    --OPEN GetAttendance_Cursor  
                    --Declare @ActualHours decimal(18,2),@SchedHours decimal(18,2)  
                    --FETCH NEXT FROM GetAttendance_Cursor INTO    
                    --@StuEnrollId,@MeetDate,@Actual,@ScheduledMinutes,@Absent,  
                    --@IsTardy,  
                    --@TrackTardies,@tardiesMakingAbsence  
                    ----select * from arAttUnitType  
                    --set @ActualRunningPresentHours=0  
                    --set @ActualRunningPresentHours=0  
                    --set @ActualRunningAbsentHours=0  
                    --set @ActualRunningTardyHours=0  
                    --set @ActualRunningMakeupHours=0  
                    --set @intTardyBreakPoint=0  
                    --set @AdjustedRunningPresentHours=0  
                    --set @AdjustedRunningAbsentHours=0  
                    --set @ActualRunningScheduledDays=0  
                    --set @MakeupHours=0  
                    --WHILE @@FETCH_STATUS = 0  
                    --BEGIN  

                    --  if @PrevStuEnrollId <> @StuEnrollId   
                    --  begin  
                    --    set @ActualRunningPresentHours = 0  
                    --    set @ActualRunningAbsentHours = 0  
                    --    set @intTardyBreakPoint = 0  
                    --    set @ActualRunningTardyHours = 0  
                    --    set @AdjustedRunningPresentHours = 0  
                    --    set @AdjustedRunningAbsentHours = 0  
                    --    set @ActualRunningScheduledDays = 0  
                    --    set @MakeupHours=0  
                    --  end  

                    --  if (@Actual <> 9999 and @Actual <> 999)  
                    --  begin  
                    --   set @ActualRunningScheduledDays = IsNULL(@ActualRunningScheduledDays,0) +  @ScheduledMinutes  
                    --   set @ActualRunningPresentHours = isNULL(@ActualRunningPresentHours,0) + @Actual  
                    --   set @AdjustedRunningPresentHours = IsNULL(@AdjustedRunningPresentHours,0) + @Actual  
                    --  end  
                    --  set @ActualRunningAbsentHours = IsNULL(@ActualRunningAbsentHours,0) + @Absent  
                    --      if (@Actual=0 and @ScheduledMinutes >= 1 and @ScheduledMinutes not in (999,9999))   
                    --  begin  
                    --   set @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Absent  
                    --  end  
                    --  if (@tracktardies=1 and @IsTardy=1)  
                    --  begin  
                    --   set @ActualRunningTardyHours = @ActualRunningTardyHours + 1  
                    --  end  
                    --  --commented by balaji on 10/22/2012 as report (rdl) doesn't add days attended and make up days  
                    --   -- If there are make up hrs deduct that otherwise it will be added again in progress report  
                    --          IF (@Actual > 0 AND @Actual > @ScheduledMinutes and @Actual<>9999.00)   
                    --     BEGIN  
                    --      SET @ActualRunningPresentHours = @ActualRunningPresentHours - (@Actual - @ScheduledMinutes)  
                    --      SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - (@Actual - @ScheduledMinutes)  
                    --     END  
                    --  if @tracktardies=1 and (@TardyMinutes > 0 or @IsTardy=1)  
                    --  begin  
                    --   set @intTardyBreakPoint = @intTardyBreakPoint + 1  
                    --  end       

                    --  -- Makeup: Calculate Makeup Hours when Student Attended more than scheduled hours                    
                    --         IF (@Actual > 0 AND @Actual > @ScheduledMinutes and @Actual<>9999.00)   
                    --                BEGIN  
                    --                    SET @MakeupHours = @MakeupHours + (@Actual - @ScheduledMinutes)  
                    --                END  

                    --  if (@tracktardies=1 and @intTardyBreakPoint=@tardiesMakingAbsence)  
                    --  begin  
                    --   set @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - @Actual  
                    --   set @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @ScheduledMinutes --@TardyMinutes  
                    --   set @intTardyBreakPoint=0  
                    --  end  


                    --  Delete from syStudentAttendanceSummary where StuEnrollId=@StuEnrollId and StudentAttendedDate=@MeetDate  
                    --  insert into syStudentAttendanceSummary  
                    --     (StuEnrollId,ClsSectionId,StudentAttendedDate,ScheduledDays,ActualDays,  
                    --     ActualRunningScheduledDays,  
                    --     ActualRunningPresentDays,  
                    --     ActualRunningAbsentDays,  
                    --     ActualRunningMakeupDays,  
                    --     ActualRunningTardyDays,  
                    --     AdjustedPresentDays,  
                    --     AdjustedAbsentDays,AttendanceTrackType,  
                    --     ModUser,ModDate,TardiesMakingAbsence)  
                    --    values  
                    --     (@StuEnrollId,@ClsSectionId,@MeetDate,IsNULL(@ScheduledMinutes,0),@Actual,IsNULL(@ActualRunningScheduledDays,0),IsNULL(@ActualRunningPresentHours,0),  
                    --     IsNULL(@ActualRunningAbsentHours,0),IsNULL(@MakeupHours,0),IsNULL(@ActualRunningTardyHours,0),isNULL(@AdjustedRunningPresentHours,0),  
                    --     isNULL(@AdjustedRunningAbsentHours,0),'Post Attendance by Class','sa',getdate(),@TardiesMakingAbsence)  

                    --  --update syStudentAttendanceSummary set TardiesMakingAbsence=@TardiesMakingAbsence where StuEnrollId=@StuEnrollId  
                    --   --end  
                    --    set @PrevStuEnrollId=@StuEnrollId   
                    --FETCH NEXT FROM GetAttendance_Cursor INTO    
                    --@StuEnrollId,@MeetDate,@Actual,@ScheduledMinutes,@Absent,  
                    --@IsTardy,  
                    --@TrackTardies,@tardiesMakingAbsence  
                    --END  
                    --CLOSE GetAttendance_Cursor  
                    --DEALLOCATE GetAttendance_Cursor  

                    -- end  



                    -- by (minutes or clock hour) and P/A  
                    BEGIN
                        DECLARE GetAttendance_Cursor CURSOR FORWARD_ONLY FAST_FORWARD FOR
                            SELECT   *
                                    ,ROW_NUMBER() OVER ( ORDER BY MeetDate ) AS RowNumber
                            FROM     (
                                     SELECT DISTINCT t1.StuEnrollId
                                           ,t1.ClsSectionId
                                           ,t1.MeetDate
                                           ,DATENAME(dw, t1.MeetDate) AS WeekDay
                                           ,t4.StartDate
                                           ,t4.EndDate
                                           ,t5.PeriodDescrip
                                           ,t1.Actual
                                           ,t1.Excused
                                           ,CASE WHEN (
                                                      t1.Actual = 0
                                                      AND t1.Excused = 0
                                                      ) THEN t1.Scheduled
                                                 ELSE CASE WHEN (
                                                                t1.Actual <> 9999.00
                                                                AND t1.Actual < t1.Scheduled
                                                                ) THEN ( t1.Scheduled - t1.Actual )
                                                           ELSE 0
                                                      END
                                            END AS Absent
                                           ,t1.Scheduled AS ScheduledMinutes
                                           ,CASE WHEN (
                                                      t1.Actual > 0
                                                      AND t1.Actual < t1.Scheduled
                                                      ) THEN ( t1.Scheduled - t1.Actual )
                                                 ELSE 0
                                            END AS TardyMinutes
                                           ,t1.Tardy AS Tardy
                                           ,t3.TrackTardies
                                           ,t3.TardiesMakingAbsence
                                           ,t3.PrgVerId
                                     FROM   atClsSectAttendance t1
                                     INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                                     INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                                     INNER JOIN arClsSectMeetings t4 ON t1.ClsSectionId = t4.ClsSectionId
                                                                        AND (
                                                                            CONVERT(DATE, t1.MeetDate, 111) >= CONVERT(DATE, t4.StartDate, 111)
                                                                            AND CONVERT(DATE, t1.MeetDate, 111) <= CONVERT(DATE, t4.EndDate, 111)
                                                                            )
                                                                        AND t1.ClsSectMeetingId = t4.ClsSectMeetingId --DE8479 line added  
                                     INNER JOIN syPeriods t5 ON t4.PeriodId = t5.PeriodId
                                     INNER JOIN cmTimeInterval t6 ON t5.StartTimeId = t6.TimeIntervalId
                                     INNER JOIN cmTimeInterval t7 ON t5.EndTimeId = t7.TimeIntervalId
                                     INNER JOIN dbo.arClassSections t9 ON t9.ClsSectionId = t1.ClsSectionId
                                     INNER JOIN dbo.arReqs t10 ON t10.ReqId = t9.ReqId
                                     WHERE  t1.StuEnrollId IN (
                                                              SELECT StuEnrollId
                                                              FROM
                                                                     OPENXML(@hDoc, '/NewDataSet/InsertPostClockAttendance', 1)
                                                                         WITH (
                                                                              StuEnrollId VARCHAR(50)
                                                                              )
                                                              )
                                            AND (
                                                t10.UnitTypeId IN ( 'A1389C74-0BB9-4BBF-A47F-68428BE7FA4D', 'B937C92E-FD7A-455E-A731-527A9918C734' )
                                                OR t3.UnitTypeId IN ( 'A1389C74-0BB9-4BBF-A47F-68428BE7FA4D', 'B937C92E-FD7A-455E-A731-527A9918C734' )
                                                ) -- Minutes  
                                            AND t1.Actual <> 9999
                                     ) dt
                            ORDER BY StuEnrollId
                                    ,MeetDate;
                        OPEN GetAttendance_Cursor;
                        FETCH NEXT FROM GetAttendance_Cursor
                        INTO @StuEnrollId
                            ,@ClsSectionId
                            ,@MeetDate
                            ,@WeekDay
                            ,@StartDate
                            ,@EndDate
                            ,@PeriodDescrip
                            ,@Actual
                            ,@Excused
                            ,@Absent
                            ,@ScheduledMinutes
                            ,@TardyMinutes
                            ,@tardy
                            ,@tracktardies
                            ,@tardiesMakingAbsence
                            ,@PrgVerId
                            ,@rownumber;
                        SET @ActualRunningPresentHours = 0;
                        SET @ActualRunningPresentHours = 0;
                        SET @ActualRunningAbsentHours = 0;
                        SET @ActualRunningTardyHours = 0;
                        SET @ActualRunningMakeupHours = 0;
                        SET @intTardyBreakPoint = 0;
                        SET @AdjustedRunningPresentHours = 0;
                        SET @AdjustedRunningAbsentHours = 0;
                        SET @ActualRunningScheduledDays = 0;
                        SET @boolReset = 0;
                        SET @MakeUpHours = 0;
                        WHILE @@FETCH_STATUS = 0
                            BEGIN

                                IF @PrevStuEnrollId <> @StuEnrollId
                                    BEGIN
                                        SET @ActualRunningPresentHours = 0;
                                        SET @ActualRunningAbsentHours = 0;
                                        SET @intTardyBreakPoint = 0;
                                        SET @ActualRunningTardyHours = 0;
                                        SET @AdjustedRunningPresentHours = 0;
                                        SET @AdjustedRunningAbsentHours = 0;
                                        SET @ActualRunningScheduledDays = 0;
                                        SET @MakeUpHours = 0;
                                        SET @boolReset = 1;
                                    END;


                                -- Scheduled and Actual: Calculate Students Actual and Adjusted Running Present Hours/Min/Day  
                                IF @Actual <> 9999.00
                                    BEGIN
                                        SET @ActualRunningPresentHours = @ActualRunningPresentHours + @Actual;
                                        SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours + @Actual;

                                        -- If there are make up hrs deduct that otherwise it will be added again in progress report  
                                        IF (
                                           @Actual > 0
                                           AND @Actual > @ScheduledMinutes
                                           AND @Actual <> 9999.00
                                           )
                                            BEGIN
                                                SET @ActualRunningPresentHours = @ActualRunningPresentHours - ( @Actual - @ScheduledMinutes );
                                                SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - ( @Actual - @ScheduledMinutes );
                                            END;

                                        SET @ActualRunningScheduledDays = @ActualRunningScheduledDays + @ScheduledMinutes;
                                    END;

                                -- Absent: Calculate Students Actual and Adjusted Running Absent Hours/Min/Day  
                                SET @ActualRunningAbsentHours = @ActualRunningAbsentHours + @Absent;
                                SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Absent;

                                -- Tardy: Calculate Students Actual Running Tardy Hours/Min/Day     
                                IF (
                                   @Actual > 0
                                   AND @Actual < @ScheduledMinutes
                                   AND @tardy = 1
                                   )
                                    BEGIN
                                        SET @ActualRunningTardyHours = @ActualRunningTardyHours + ( @ScheduledMinutes - @Actual );
                                    END;

                                -- Track how many days student has been tardy only when   
                                -- program version requires to track tardy  
                                IF (
                                   @tracktardies = 1
                                   AND @tardy = 1
                                   )
                                    BEGIN
                                        SET @intTardyBreakPoint = @intTardyBreakPoint + 1;
                                    END;


                                -- Tardy: If student is tardy multiple times and when the number of days student is tardy matches  
                                -- the tardy rule set (for ex: 2 Tardies make 1 Absence). This rule indicates that  
                                -- when student is tardy the second time, that second occurance will be considered as  
                                -- absence  
                                -- Variable @intTardyBreakpoint tracks how many times the student was tardy  
                                -- Variable @tardiesMakingAbsence tracks the tardy rule  
                                IF (
                                   @tracktardies = 1
                                   AND @intTardyBreakPoint = @tardiesMakingAbsence
                                   )
                                    BEGIN
                                        SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - @Actual;
                                        SET @AdjustedRunningAbsentHours = ( @AdjustedRunningAbsentHours - @Absent ) + @ScheduledMinutes; --@TardyMinutes  
                                        SET @intTardyBreakPoint = 0;
                                    END;

                                -- Makeup: Calculate Makeup Hours when Student Attended more than scheduled hours                    
                                IF (
                                   @Actual > 0
                                   AND @Actual > @ScheduledMinutes
                                   AND @Actual <> 9999.00
                                   )
                                    BEGIN
                                        SET @MakeUpHours = @MakeUpHours + ( @Actual - @ScheduledMinutes );
                                    END;

                                IF ( @tracktardies = 1 )
                                    BEGIN
                                        SET @AdjustedPresentDaysComputed = @AdjustedRunningPresentHours; --+IsNULL(@ActualRunningTardyHours,0)  
                                    END;
                                ELSE
                                    BEGIN
                                        SET @AdjustedPresentDaysComputed = @AdjustedRunningPresentHours;
                                    END;

                                DELETE FROM syStudentAttendanceSummary
                                WHERE StuEnrollId = @StuEnrollId
                                      AND ClsSectionId = @ClsSectionId
                                      AND StudentAttendedDate = @MeetDate;
                                INSERT INTO syStudentAttendanceSummary (
                                                                       StuEnrollId
                                                                      ,ClsSectionId
                                                                      ,StudentAttendedDate
                                                                      ,ScheduledDays
                                                                      ,ActualDays
                                                                      ,ActualRunningScheduledDays
                                                                      ,ActualRunningPresentDays
                                                                      ,ActualRunningAbsentDays
                                                                      ,ActualRunningMakeupDays
                                                                      ,ActualRunningTardyDays
                                                                      ,AdjustedPresentDays
                                                                      ,AdjustedAbsentDays
                                                                      ,AttendanceTrackType
                                                                      ,ModUser
                                                                      ,ModDate
                                                                      ,tardiesmakingabsence
                                                                       )
                                VALUES ( @StuEnrollId
                                        ,@ClsSectionId
                                        ,@MeetDate
                                        ,@ScheduledMinutes
                                        ,@Actual
                                        ,@ActualRunningScheduledDays
                                        ,@ActualRunningPresentHours
                                        ,@ActualRunningAbsentHours
                                        ,ISNULL(@MakeUpHours, 0)
                                        ,@ActualRunningTardyHours
                                        ,@AdjustedPresentDaysComputed
                                        ,@AdjustedRunningAbsentHours
                                        ,'Post Attendance by Class Min'
                                        ,'sa'
                                        ,GETDATE()
                                        ,@tardiesMakingAbsence
                                       );

                                --update syStudentAttendanceSummary set TardiesMakingAbsence=@TardiesMakingAbsence where StuEnrollId=@StuEnrollId  
                                SET @PrevStuEnrollId = @StuEnrollId;

                                FETCH NEXT FROM GetAttendance_Cursor
                                INTO @StuEnrollId
                                    ,@ClsSectionId
                                    ,@MeetDate
                                    ,@WeekDay
                                    ,@StartDate
                                    ,@EndDate
                                    ,@PeriodDescrip
                                    ,@Actual
                                    ,@Excused
                                    ,@Absent
                                    ,@ScheduledMinutes
                                    ,@TardyMinutes
                                    ,@tardy
                                    ,@tracktardies
                                    ,@tardiesMakingAbsence
                                    ,@PrgVerId
                                    ,@rownumber;
                            END;
                        CLOSE GetAttendance_Cursor;
                        DEALLOCATE GetAttendance_Cursor;
                    END;

                    -- DECLARE GetAttendance_Cursor CURSOR  
                    -- FOR  
                    --     SELECT  t1.StuEnrollId ,  
                    --             NULL AS ClsSectionId ,  
                    --             t1.RecordDate AS MeetDate ,  
                    --             t1.ActualHours ,  
                    --             t1.SchedHours AS ScheduledMinutes ,  
                    --             CASE WHEN ( ( t1.SchedHours > 1  
                    --                           AND t1.SchedHours NOT IN ( 999, 9999 )  
                    --                         )  
                    --                         AND t1.ActualHours = 0  
                    --                       ) THEN t1.SchedHours  
                    --                  ELSE 0  
                    --             END AS Absent ,  
                    --             t1.IsTardy ,  
                    --             ( SELECT    SUM(SchedHours)  
                    --               FROM      arStudentClockAttendance  
                    --               WHERE     StuEnrollId = t1.StuEnrollId  
                    --                         AND RecordDate <= t1.RecordDate  
                    --                         AND ( t1.SchedHours > 1  
                    --                               AND t1.SchedHours NOT IN ( 999, 9999 )  
                    --                             )  
                    --             ) AS ActualRunningScheduledHours ,  
                    --             ( SELECT    SUM(ActualHours)  
                    --               FROM      arStudentClockAttendance  
                    --               WHERE     StuEnrollId = t1.StuEnrollId  
                    --                         AND RecordDate <= t1.RecordDate  
                    --                         AND ( t1.SchedHours > 1  
                    --                               AND t1.SchedHours NOT IN ( 999, 9999 )  
                    --                             )  
                    --                         AND ActualHours > 1  
                    --             ) AS ActualRunningPresentHours ,  
                    --             ( SELECT    COUNT(ActualHours)  
                    --               FROM      arStudentClockAttendance  
                    --               WHERE     StuEnrollId = t1.StuEnrollId  
                    --                         AND RecordDate <= t1.RecordDate  
                    --                         AND ( t1.SchedHours > 1  
                    --                               AND t1.SchedHours NOT IN ( 999, 9999 )  
                    --                             )  
                    --                         AND ActualHours = 0  
                    --             ) AS ActualRunningAbsentHours ,  
                    --             ( SELECT    SUM(ActualHours)  
                    --               FROM      arStudentClockAttendance  
                    --               WHERE     StuEnrollId = t1.StuEnrollId  
                    --                         AND RecordDate <= t1.RecordDate  
                    --                         AND SchedHours = 0  
                    --                         AND ActualHours > 1  
                    --             ) AS ActualRunningMakeupHours ,  
                    --             ( SELECT    SUM(SchedHours - ActualHours)  
                    --               FROM      arStudentClockAttendance  
                    --               WHERE     StuEnrollId = t1.StuEnrollId  
                    --                         AND RecordDate <= t1.RecordDate  
                    --                         AND ( ( t1.SchedHours > 1  
                    --                                 AND t1.SchedHours NOT IN ( 999, 9999 )  
                    --                               )  
                    --                               AND ActualHours > 1  
                    --                             )  
                    --             ) AS ActualRunningTardyHours ,  
                    --             t3.TrackTardies ,  
                    --             t3.tardiesMakingAbsence ,  
                    --             t3.PrgVerId ,  
                    --             ROW_NUMBER() OVER ( ORDER BY t1.RecordDate ) AS RowNumber  
                    --     FROM    arStudentClockAttendance t1  
                    --             INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId  
                    --             INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId  
                    --     WHERE   t1.StuEnrollId IN (  
                    --             SELECT  StuEnrollId  
                    --             FROM    OPENXML(@hDoc,'/NewDataSet/InsertPostClockAttendance',1)   
                    --      WITH (StuEnrollId VARCHAR(50)) )  
                    --             AND -- StuEnrollId goes in here  
                    --             t3.UnitTypeId IN ( 'A1389C74-0BB9-4BBF-A47F-68428BE7FA4D')  
                    --             AND t1.ActualHours <> 9999.00  
                    -- OPEN GetAttendance_Cursor  

                    -- FETCH NEXT FROM GetAttendance_Cursor INTO @StuEnrollId, @ClsSectionId,  
                    --     @MeetDate, @Actual, @ScheduledMinutes, @Absent, @IsTardy,  
                    --     @ActualRunningScheduledHours, @ActualRunningPresentHours,  
                    --     @ActualRunningAbsentHours, @ActualRunningMakeupHours,  
                    --     @ActualRunningTardyHours, @TrackTardies, @tardiesMakingAbsence,  
                    --     @PrgVerId, @RowNumber  

                    -- SET @ActualRunningPresentHours = 0  
                    -- SET @ActualRunningPresentHours = 0  
                    -- SET @ActualRunningAbsentHours = 0  
                    -- SET @ActualRunningTardyHours = 0  
                    -- SET @ActualRunningMakeupHours = 0  
                    -- SET @intTardyBreakPoint = 0  
                    -- SET @AdjustedRunningPresentHours = 0  
                    -- SET @AdjustedRunningAbsentHours = 0  
                    -- SET @ActualRunningScheduledDays = 0  
                    -- WHILE @@FETCH_STATUS = 0   
                    --     BEGIN  

                    --         IF @PrevStuEnrollId <> @StuEnrollId   
                    --             BEGIN  
                    --                 SET @ActualRunningPresentHours = 0  
                    --            SET @ActualRunningAbsentHours = 0  
                    --                 SET @intTardyBreakPoint = 0  
                    --                 SET @ActualRunningTardyHours = 0  
                    --                 SET @AdjustedRunningPresentHours = 0  
                    --                 SET @AdjustedRunningAbsentHours = 0  
                    --                 SET @ActualRunningScheduledDays = 0  
                    --             END  

                    --         SET @ActualRunningScheduledDays = @ActualRunningScheduledDays  
                    --             + @ScheduledMinutes  
                    --         SET @ActualRunningPresentHours = @ActualRunningPresentHours  
                    --             + @Actual  
                    --         SET @ActualRunningAbsentHours = @ActualRunningAbsentHours  
                    --             + @Absent  
                    --         SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours  
                    --             + @Actual  
                    --         SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours  
                    --             + @Absent   
                    --         IF ( @Actual > 0  
                    --              AND @Actual < @ScheduledMinutes  
                    --            )   
                    --             BEGIN  
                    --                 SET @ActualRunningTardyHours = @ActualRunningTardyHours  
                    --                     + ( @ScheduledMinutes - @Actual )  
                    --             END  

                    --         IF @tracktardies = 1  
                    --             AND @TardyMinutes > 0   
                    --             BEGIN  
                    --                 SET @intTardyBreakPoint = @intTardyBreakPoint + 1  
                    --             END   
                    --         IF ( @tracktardies = 1  
                    --              AND @intTardyBreakPoint = @tardiesMakingAbsence  
                    --            )   
                    --             BEGIN  
                    --                 SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours  
                    --                     - @Actual  
                    --                 SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours  
                    --                     + @ScheduledMinutes --@TardyMinutes  
                    --                 SET @intTardyBreakPoint = 0  
                    --             END  

                    --         INSERT  INTO syStudentAttendanceSummary  
                    --                 ( StuEnrollId ,  
                    --                   ClsSectionId ,  
                    --                   StudentAttendedDate ,  
                    --                   ScheduledDays ,  
                    --                   ActualDays ,  
                    --                   ActualRunningScheduledDays ,  
                    --                   ActualRunningPresentDays ,  
                    --                   ActualRunningAbsentDays ,  
                    --                   ActualRunningMakeupDays ,  
                    --                   ActualRunningTardyDays ,  
                    --                   AdjustedPresentDays ,  
                    --                   AdjustedAbsentDays ,  
                    --                   AttendanceTrackType ,  
                    --                   ModUser ,  
                    --                   ModDate  
                    --                 )  
                    --         VALUES  ( @StuEnrollId ,  
                    --                   @ClsSectionId ,  
                    --                   @MeetDate ,  
                    --                   @ScheduledMinutes ,  
                    --                   @Actual ,  
                    --                   @ActualRunningScheduledDays ,  
                    --                   @ActualRunningPresentHours ,  
                    --                   @ActualRunningAbsentHours ,  
                    --                   0 ,  
                    --                   @ActualRunningTardyHours ,  
                    --                   @AdjustedRunningPresentHours ,  
                    --                   @AdjustedRunningAbsentHours ,  
                    --                   'Post Attendance by Day' ,  
                    --                   'sa' ,  
                    --                   GETDATE()  
                    --                 )  
                    ----end  
                    --         SET @PrevStuEnrollId = @StuEnrollId   

                    --         FETCH NEXT FROM GetAttendance_Cursor INTO @StuEnrollId,  
                    --             @ClsSectionId, @MeetDate, @Actual, @ScheduledMinutes, @Absent,  
                    --             @IsTardy, @ActualRunningScheduledHours,  
                    --             @ActualRunningPresentHours, @ActualRunningAbsentHours,  
                    --             @ActualRunningMakeupHours, @ActualRunningTardyHours,  
                    --             @TrackTardies, @tardiesMakingAbsence, @PrgVerId, @RowNumber  
                    --     END  
                    -- CLOSE GetAttendance_Cursor  
                    -- DEALLOCATE GetAttendance_Cursor  

                    IF EXISTS (
                              SELECT *
                              FROM   sysobjects
                              WHERE  type = 'TR'
                                     AND name = 'TR_InsertAttendanceSummary_ByDay_PA'
                              )
                        BEGIN
                            ENABLE TRIGGER TR_InsertAttendanceSummary_ByDay_PA ON arStudentClockAttendance;
                        END;
                    IF EXISTS (
                              SELECT *
                              FROM   sysobjects
                              WHERE  type = 'TR'
                                     AND name = 'TR_InsertAttendanceSummary_ByClockHour_Minutes'
                              )
                        BEGIN
                            ENABLE TRIGGER TR_InsertAttendanceSummary_ByClockHour_Minutes ON arStudentClockAttendance;
                        END;
                    ---begin code by Balaji to insert into the summary table for progress reports  
                    EXEC sp_xml_removedocument @hDoc;
                    IF ( @@ERROR > 0 )
                        BEGIN
                            SET @Error = @@ERROR;
                        END;
                END;

        END TRY
        BEGIN CATCH
            DECLARE @msg NVARCHAR(MAX);
            DECLARE @severity INT;
            DECLARE @state INT;
            SELECT @msg = ERROR_MESSAGE()
                  ,@severity = ERROR_SEVERITY()
                  ,@state = ERROR_STATE();
            RAISERROR(@msg, @severity, @state);
            SET @Error = 1;
        END CATCH;

        IF ( @Error = 0 )
            BEGIN
                COMMIT TRANSACTION AddMissingAttendanceRecords;
            END;
        ELSE
            BEGIN
                ROLLBACK TRANSACTION AddMissingAttendanceRecords;
            END;
    END;
--=================================================================================================  
-- END  --  USP_PostClockAttendance_Insert  
--=================================================================================================  
GO
