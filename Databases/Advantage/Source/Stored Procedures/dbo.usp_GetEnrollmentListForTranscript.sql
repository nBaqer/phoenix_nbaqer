SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_GetEnrollmentListForTranscript]
    (
     @campGrpId VARCHAR(8000)
    ,@campusId UNIQUEIDENTIFIER
    ,@prgVerId VARCHAR(8000)
    ,@statusCodeId VARCHAR(8000) = NULL
    ,@cohortStartDate1 DATETIME
    ,@cohortStartDate2 DATETIME
    ,  
     /** New parameters added by kamalesh ahuja on march 08 2010 **/@StrTerm AS VARCHAR(1000)
    ,@strClassDates AS VARCHAR(100)
    ,@ShowClassDates AS INT
    ,@ShowDateIssues AS INT
    ,@TranscriptType AS VARCHAR(50)
    ,@ShowLegalDisc AS INT  
     /**/  
     
    )
AS
    SET NOCOUNT ON;  
    SELECT  arStuEnrollments.StuEnrollId
           ,A.LastName
           ,A.MiddleName
           ,A.FirstName
           ,A.SSN
           ,A.StudentNumber
           ,arStuEnrollments.EnrollmentId
           ,A.DOB
           ,arStuEnrollments.StartDate
           ,arStuEnrollments.ExpGradDate
           ,arStuEnrollments.PrgVerId
           ,arPrgVersions.PrgVerDescrip
           ,arPrograms.ProgDescrip AS ProgramDescrip
           ,   
        /** New code added by kamalesh ahuja on march 08 2010 **/arPrgVersions.IsContinuingEd AS IsContinuingEd
           ,  
        /**/(
              SELECT    dg.DegreeDescrip
              FROM      arDegrees dg
              WHERE     dg.DegreeId = arPrgVersions.DegreeId
            ) AS DegreeDescrip
           ,syCmpGrpCmps.CampGrpId
           ,syCampGrps.CampGrpDescrip
           ,arStuEnrollments.CampusId
           ,C.CampDescrip
           ,arStuEnrollments.StatusCodeId
           ,syStatusCodes.StatusCodeDescrip
           ,arStuEnrollments.EnrollmentId
           ,arStuEnrollments.LDA
           ,arStuEnrollments.DateDetermined
           ,syStatusCodes.SysStatusId
           ,(
              SELECT TOP 1
                        Address1
              FROM      arStudAddresses T
                       ,syStatuses Y
              WHERE     T.StudentId = A.StudentId
                        AND T.StatusId = Y.StatusId
                        AND Y.Status = 'Active'
              ORDER BY  T.Default1 DESC
            ) AS Address1
           ,(
              SELECT TOP 1
                        Address2
              FROM      arStudAddresses T
                       ,syStatuses Y
              WHERE     T.StudentId = A.StudentId
                        AND T.StatusId = Y.StatusId
                        AND Y.Status = 'Active'
              ORDER BY  T.Default1 DESC
            ) AS Address2
           ,(
              SELECT TOP 1
                        City
              FROM      arStudAddresses T
                       ,syStatuses Y
              WHERE     T.StudentId = A.StudentId
                        AND T.StatusId = Y.StatusId
                        AND Y.Status = 'Active'
              ORDER BY  T.Default1 DESC
            ) AS City
           ,(
              SELECT TOP 1
                        StateDescrip
              FROM      arStudAddresses T
                       ,syStates S
                       ,syStatuses Y
              WHERE     T.StudentId = A.StudentId
                        AND T.StateId = S.StateId
                        AND T.StatusId = Y.StatusId
                        AND Y.Status = 'Active'
              ORDER BY  T.Default1 DESC
            ) AS StateDescrip
           ,(
              SELECT TOP 1
                        Zip
              FROM      arStudAddresses T
                       ,syStatuses Y
              WHERE     T.StudentId = A.StudentId
                        AND T.StatusId = Y.StatusId
                        AND Y.Status = 'Active'
              ORDER BY  T.Default1 DESC
            ) AS Zip
           ,(
              SELECT TOP 1
                        ForeignZip
              FROM      arStudAddresses T
                       ,syStatuses Y
              WHERE     T.StudentId = A.StudentId
                        AND T.StatusId = Y.StatusId
                        AND Y.Status = 'Active'
              ORDER BY  T.Default1 DESC
            ) AS ForeignZip
           ,(
              SELECT TOP 1
                        OtherState
              FROM      arStudAddresses T
                       ,syStatuses Y
              WHERE     T.StudentId = A.StudentId
                        AND T.StatusId = Y.StatusId
                        AND Y.Status = 'Active'
              ORDER BY  T.Default1 DESC
            ) AS OtherState
           ,(
              SELECT TOP 1
                        CountryDescrip
              FROM      arStudAddresses T
                       ,syStatuses Y
                       ,adCountries C
              WHERE     T.StudentId = A.StudentId
                        AND T.StatusId = Y.StatusId
                        AND Y.Status = 'Active'
                        AND T.CountryId = C.CountryId
              ORDER BY  T.Default1 DESC
            ) AS CountryDescrip
           ,(
              SELECT TOP 1
                        Phone
              FROM      arStudentPhone T
                       ,syStatuses Y
              WHERE     T.StudentId = A.StudentId
                        AND T.StatusId = Y.StatusId
                        AND Y.Status = 'Active'
              ORDER BY  T.Default1 DESC
            ) AS Phone
           ,(
              SELECT TOP 1
                        ForeignPhone
              FROM      arStudentPhone T
                       ,syStatuses Y
              WHERE     T.StudentId = A.StudentId
                        AND T.StatusId = Y.StatusId
                        AND Y.Status = 'Active'
              ORDER BY  T.Default1 DESC
            ) AS ForeignPhone
           ,(
              SELECT    MAX(PostDate)
              FROM      arGrdBkResults
              WHERE     StuEnrollId = arStuEnrollments.stuEnrollId
            ) AS ExtDate
           ,dbo.GetLDA(arStuEnrollments.stuEnrollId) AS AttDate
           ,  
            /** New code added by kamalesh ahuja on march 08 2010 **/@StrTerm AS TermCond
           ,@strClassDates AS ClassCond
           ,@ShowDateIssues AS ShowDateIssue
           ,CASE WHEN LOWER(@TranscriptType) = 'traditional_b' THEN @ShowClassDates
                 ELSE '0'
            END AS ShowClassDate
           ,@ShowLegalDisc AS ShowLegalDisc  
            /**/
    FROM    arStuEnrollments
    INNER JOIN arStudent A ON arStuEnrollments.StudentId = A.StudentId
    INNER JOIN syCampuses C ON arStuEnrollments.CampusId = C.CampusId
    INNER JOIN syCmpGrpCmps ON syCmpGrpCmps.CampusId = arStuEnrollments.CampusId
                               AND syCmpGrpCmps.CampusId = arStuEnrollments.CampusId
    INNER JOIN syCampGrps ON syCampGrps.CampGrpId = syCmpGrpCmps.CampGrpId
                             AND syCampGrps.campgrpid IN ( SELECT   
                DISTINCT                                            t1.campgrpid
                                                           FROM     sycmpgrpcmps t1
                                                           WHERE    t1.campgrpid IN ( SELECT    strval
                                                                                      FROM      dbo.SPLIT(@campGrpId) ) )
    INNER JOIN arPrgVersions ON arPrgVersions.PrgVerId = arStuEnrollments.PrgVerId
    INNER JOIN arPrograms ON arPrograms.ProgId = arPrgVersions.ProgId
    INNER JOIN syStatusCodes ON syStatusCodes.StatusCodeId = arStuEnrollments.StatusCodeId
    WHERE   arStuEnrollments.CampusId = @campusId
            AND arStuEnrollments.prgverid IN ( SELECT   strval
                                               FROM     dbo.SPLIT(@prgVerId) )
            AND (
                  @statusCodeId IS NULL
                  OR arStuEnrollments.statuscodeid IN ( SELECT  strval
                                                        FROM    dbo.SPLIT(@statusCodeId) )
                )
            AND arStuEnrollments.cohortstartdate BETWEEN @cohortStartDate1 AND @cohortStartDate2;  
  



GO
