SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[USP_IL_CheckImportLeadsFoldersPaths] @CampusId VARCHAR(50)
AS
    SELECT  ILSourcePath
           ,ILArchivePath
           ,ILExceptionPath
           ,CampDescrip
    FROM    syCampuses
    WHERE   CampusId = @CampusId;
------------------------------------------------------------------------------------------------------------------------------
--Bruce S US3034 End
------------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------------
--Balaji  US3041 04/28/2012 Start
------------------------------------------------------------------------------------------------------------------------------
--Change from General Options to General Options
    UPDATE  syResources
    SET     Resource = 'General Options'
    WHERE   ResourceId IN ( 693,694,695,696,697,700,701,708,772 );



GO
