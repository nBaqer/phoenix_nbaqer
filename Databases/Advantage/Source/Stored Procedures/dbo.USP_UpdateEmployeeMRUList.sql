SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_UpdateEmployeeMRUList]
    @EntityId UNIQUEIDENTIFIER
   ,@UserId UNIQUEIDENTIFIER
   ,@CampusId UNIQUEIDENTIFIER
   ,@ModUser VARCHAR(50)
AS
    UPDATE  syMRUS
    SET     ModUser = @ModUser
           ,ModDate = GETDATE()
    WHERE   ChildId = @EntityId
            AND UserId = @UserId;   



GO
