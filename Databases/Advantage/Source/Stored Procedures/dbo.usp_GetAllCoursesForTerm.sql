SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_GetAllCoursesForTerm]
    (
        @campusId VARCHAR(50)
       ,@TermId VARCHAR(50)
    )
AS
    SET NOCOUNT ON;
    SELECT   ReqId
            ,+'(' + Code + ') ' + Descrip AS Descrip
            ,( CASE S.Status
                    WHEN 'Active' THEN 1
                    ELSE 0
               END
             ) AS Status
    FROM     arReqs T
            ,syStatuses S
    WHERE    T.StatusId = S.StatusId
             AND T.CampGrpId IN (
                                SELECT CampGrpId
                                FROM   syCmpGrpCmps
                                WHERE  CampusId = @campusId
                                )
             AND ReqTypeId = 1
    ORDER BY Descrip
            ,Code;


    SELECT StartDate
          ,EndDate
    FROM   arTerm
    WHERE  TermId = @TermId;
GO
