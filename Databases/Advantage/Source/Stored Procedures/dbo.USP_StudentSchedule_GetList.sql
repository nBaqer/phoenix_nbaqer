SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

--========================================================================================== 
-- USP_StudentSchedule_GetList
--==========================================================================================
CREATE PROCEDURE [dbo].[USP_StudentSchedule_GetList]
    @StuEnrollId UNIQUEIDENTIFIER
   ,@InstructorId UNIQUEIDENTIFIER = NULL
   ,@ReqId UNIQUEIDENTIFIER = NULL
   ,@TermId UNIQUEIDENTIFIER = NULL
   ,@StartDate DATETIME = NULL
   ,@EndDate DATETIME = NULL
AS
    BEGIN
        SELECT DISTINCT
                ACS.ClsSectionId
               ,AT.TermId
               ,AT.TermDescrip
               ,ACS.StartDate
               ,ACS.EndDate
               ,AR2.ReqId
               ,AR2.Descrip
               ,AR2.Code
               ,ACS.ClsSection
               ,AR2.Credits
               ,AR2.Hours
               ,SU.UserId
               ,SU.FullName
               ,AGSD.Grade
               ,AST.FirstName + ' ' + AST.LastName AS StudentName
               ,AST.StudentNumber AS StudentNumber
        FROM    arResults AS AR
        INNER JOIN arStuEnrollments AS ASE ON ASE.StuEnrollId = AR.StuEnrollId
        INNER JOIN arClassSections AS ACS ON ACS.ClsSectionId = AR.TestId
        INNER JOIN arTerm AS AT ON AT.TermId = ACS.TermId
        INNER JOIN arReqs AS AR2 ON AR2.ReqId = ACS.ReqId
        INNER JOIN syUsers AS SU ON SU.UserId = ACS.InstructorId
        INNER JOIN arStudent AS AST ON AST.StudentId = ASE.StudentId
        LEFT JOIN arGradeSystemDetails AS AGSD ON AGSD.GrdSysDetailId = AR.GrdSysDetailId
        WHERE   ASE.StuEnrollId = @StuEnrollId
                AND (
                      @InstructorId IS NULL
                      OR ACS.InstructorId = @InstructorId
                    )
                AND (
                      @ReqId IS NULL
                      OR AR2.ReqId = @ReqId
                    )
                AND (
                      @TermId IS NULL
                      OR AT.TermId = @TermId
                    )
                AND (
                      @StartDate IS NULL
                      OR ISNULL(ACS.StartDate,'1/1/1900') BETWEEN @StartDate AND ISNULL(ACS.EndDate,'12/31/2099')
                    )
                AND (
                      @EndDate IS NULL
                      OR ISNULL(ACS.EndDate,'12/31/2099') <= @EndDate
                    )
        ORDER BY ACS.StartDate DESC
               ,AR2.Descrip
               ,AGSD.Grade;


    END;


--========================================================================================== 
-- END  --  USP_StudentSchedule_GetList
--==========================================================================================


GO
