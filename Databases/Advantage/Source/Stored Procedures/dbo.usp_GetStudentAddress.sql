SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_GetStudentAddress]
    (
     @stuEnrollId UNIQUEIDENTIFIER
    )
AS
    SET NOCOUNT ON;
    SELECT TOP 1
            Address1
           ,Address2
           ,City
           ,(
              SELECT    StateDescrip
              FROM      syStates S
              WHERE     T.StateId = S.StateId
            ) AS State
           ,Zip
           ,ForeignZip
           ,OtherState
           ,(
              SELECT    CountryDescrip
              FROM      adCountries C
              WHERE     T.CountryId = C.CountryId
            ) AS Country
           ,(
              SELECT TOP 1
                        Phone
              FROM      arStudentPhone A
                       ,syStatuses Y
              WHERE     T.StudentId = A.StudentId
                        AND T.StatusId = Y.StatusId
                        AND Y.Status = 'Active	'
              ORDER BY  T.Default1 DESC
            ) AS Phone
           ,(
              SELECT TOP 1
                        ForeignPhone
              FROM      arStudentPhone A
                       ,syStatuses Y
              WHERE     T.StudentId = A.StudentId
                        AND T.StatusId = Y.StatusId
                        AND Y.Status = 'Active'
              ORDER BY  T.Default1 DESC
            ) AS ForeignPhone
    FROM    arStudAddresses T
           ,syStatuses Y
    WHERE   T.StudentId = (
                            SELECT  studentid
                            FROM    arStuEnrollments
                            WHERE   stuEnrollid = @stuEnrollId
                          )
            AND T.StatusId = Y.StatusId
            AND Y.Status = 'Active'
    ORDER BY T.Default1; 



GO
