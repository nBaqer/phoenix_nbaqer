SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_PhoneType_GetList]
    @PhoneTypeDescrip VARCHAR(50)
AS
    SELECT  PhoneTypeId
    FROM    syPhoneType
    WHERE   PhoneTypeDescrip LIKE @PhoneTypeDescrip + '%'; 



GO
