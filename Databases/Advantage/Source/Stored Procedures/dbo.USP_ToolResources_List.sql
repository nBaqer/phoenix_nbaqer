SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_ToolResources_List]
AS
    SELECT  771 AS ModuleResourceId
           ,NNChild.ResourceId AS ChildResourceId
           ,RChild.Resource AS ChildResource
           ,RChild.ResourceURL AS ChildResourceURL
           ,CASE WHEN (
                        NNParent.ResourceId IN ( 771 )
                        OR NNChild.ResourceId IN ( 772 )
                      ) THEN NULL
                 ELSE NNParent.ResourceId
            END AS ParentResourceId
           ,RParent.Resource AS ParentResource
           ,CASE WHEN NNChild.ResourceId = 398 THEN 1
                 ELSE CASE WHEN NNChild.ResourceId = 395 THEN 2
                           ELSE 3
                      END
            END AS FirstSortOrder
           ,CASE WHEN ( NNChild.ResourceId IN ( 772 ) ) THEN 1
                 ELSE 2
            END AS DisplaySequence
    FROM    syResources RChild
    INNER JOIN syNavigationNodes NNChild ON RChild.ResourceID = NNChild.ResourceId
    INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
    INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
    LEFT OUTER JOIN (
                      SELECT    *
                      FROM      syResources
                      WHERE     ResourceTypeID = 1
                    ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
    WHERE   RChild.ResourceTypeID IN ( 2,5,8,4,7 )
            AND (
                  RChild.ChildTypeId IS NULL
                  OR RChild.ChildTypeId = 5
                  OR RChild.ChildTypeId = 3
                ) 
		--AND RChild.ResourceId IN (283,331,141,186)
            AND ( RChild.ResourceID NOT IN ( 687 ) )
            AND (
			--NNParent.ParentId IN (SELECT HierarchyId FROM syNavigationNodes WHERE ResourceId=771)	OR
                  NNChild.ParentId IN ( SELECT  HierarchyId
                                        FROM    syNavigationNodes
                                        WHERE   ResourceId IN ( 771,772 ) ) );



GO
