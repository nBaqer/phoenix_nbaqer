SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[USP_AR_RegStd_GetCoursesFortheCourseGroupsfromProgId]
    (
     @ProgId UNIQUEIDENTIFIER
    )
AS /*----------------------------------------------------------------------------------------------------
	Author          :	Saraswathi Lakshmanan
    
    Create date		:	01/28/2010
    
	Procedure Name	:	USP_AR_RegStd_GetCoursesFortheCourseGroupsfromProgId

	Objective		:	get the courses and their Course Groups
	
	Parameters		:	Name			Type	Data Type			
						=====			====	=========			
						@progId			In		UniqueIdentifier	
							
	
	Output			:	Returns the ReqID 
	
	calling procedure : 		USP_AR_RegSt_GetAvailableStudentsfortheProgAndClsSection 		
						
*/-----------------------------------------------------------------------------------------------------
--Stored procedure created by Saraswathi Lakhsmanan on 1/25/10
--To fix issue 17548: ENH: All: Register Students Page is Taking Too Long to Populate Available Students 
/*
Create a variable table to store the ReqId and GrpId
The first part will find all the first level of ReqId and GrpIds for a given ProgramId
the second part- Which has the recursive insert  will get all the Reqids for the given Grpid.
i.e the Courses for the course groups are found. This is multilevel- a course group can have another crourse group and courses.
*/

    DECLARE @VariableReqtable TABLE
        (
         ReqId UNIQUEIDENTIFIER
        ,GrpID UNIQUEIDENTIFIER
        ,ID INT IDENTITY
        ); 
    INSERT  INTO @VariableReqtable
            SELECT  arReqGrpDef.Reqid
                   ,arReqGrpDef.GrpID
            FROM    arReqGrpDef
            INNER JOIN (
                         SELECT t3.ReqId
                         FROM   arReqs t3
                         INNER JOIN arProgVerDef t4 ON t3.ReqId = t4.ReqId
                         INNER JOIN arPrgVersions t6 ON t4.Prgverid = t6.PrgVerID
                         WHERE  t3.ReqTypeId = 2
                                AND t6.ProgId = @ProgId
                                AND t6.IsContinuingEd = 0
                       ) Table1 ON Table1.ReqID = arReqGrpDef.GrpId;
	
    DECLARE @MaxRowCounter INT
       ,@RowCounter INT;
    SET @MaxRowCounter = (
                           SELECT   MAX(ID)
                           FROM     @VariableReqtable
                         );
    SET @RowCounter = 1;
    WHILE ( @RowCounter <= @MaxRowCounter )
        BEGIN
            INSERT  INTO @VariableReqtable
                    SELECT  ReqId
                           ,GrpID
                    FROM    arReqGrpDef
                    WHERE   GrpID IN ( SELECT   ReqId
                                       FROM     @VariableReqtable
                                       WHERE    ID = @RowCounter ); 
   
            SET @RowCounter = @RowCounter + 1;
            SET @MaxRowCounter = (
                                   SELECT   MAX(ID)
                                   FROM     @VariableReqtable
                                 );
        END;

 
    SELECT  ReqId
           ,GrpID
    FROM    @VariableReqtable;



GO
