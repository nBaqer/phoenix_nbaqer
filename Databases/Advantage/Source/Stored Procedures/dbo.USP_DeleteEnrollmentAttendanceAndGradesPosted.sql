SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[USP_DeleteEnrollmentAttendanceAndGradesPosted]
    (
     @StuEnrollId UNIQUEIDENTIFIER
    )
AS
    BEGIN TRANSACTION DeleteAttendanceAndGradesPosted;
    BEGIN TRY
	
        DELETE  FROM arResults
        WHERE   StuEnrollId = @StuEnrollId;
	
        DELETE  FROM atClsSectAttendance
        WHERE   StuEnrollId = @StuEnrollId;

        DELETE  FROM arGrdBkResults
        WHERE   StuEnrollId = @StuEnrollId;

        DELETE  FROM syStudentAttendanceSummary
        WHERE   StuEnrollId = @StuEnrollId;
	
        DELETE  FROM arStudentClockAttendance
        WHERE   StuEnrollId = @StuEnrollId;

        DELETE  FROM arStudentSchedules
        WHERE   StuEnrollId = @StuEnrollId;
	
        DELETE  FROM syCreditSummary
        WHERE   StuEnrollId = @StuEnrollId;


    END TRY
    BEGIN CATCH
        SELECT  ERROR_NUMBER() AS ErrorNumber
               ,ERROR_SEVERITY() AS ErrorSeverity
               ,ERROR_STATE() AS ErrorState
               ,ERROR_PROCEDURE() AS ErrorProcedure
               ,ERROR_LINE() AS ErrorLine
               ,ERROR_MESSAGE() AS ErrorMessage;

        IF @@TRANCOUNT > 0
            BEGIN
                ROLLBACK TRANSACTION DeleteAttendanceAndGradesPosted;
            END;
	
        SELECT  0;
    END CATCH;

    IF @@TRANCOUNT > 0
        BEGIN
            COMMIT TRANSACTION DeleteAttendanceAndGradesPosted;
            SELECT  1;
        END;

GO
