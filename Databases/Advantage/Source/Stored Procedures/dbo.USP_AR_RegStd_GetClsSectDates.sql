SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[USP_AR_RegStd_GetClsSectDates]
    (
     @ClsSectId UNIQUEIDENTIFIER
    )
AS /*----------------------------------------------------------------------------------------------------
	Author          :	Saraswathi Lakshmanan
    
    Create date		:	02/04/2010
    
	Procedure Name	:	USP_AR_RegStd_GetClsSectDates

	Objective		:	get the Start and End Dates for a given Clssectuion
	
	Parameters		:	Name			Type	Data Type			Required? 	
						=====			====	=========			=========	
						@ClsSectID		In		Uniqueidentifier
	
	Output			:	Returns the dataset with dates		
						
*/-----------------------------------------------------------------------------------------------------
/*
Procedure created by Saraswathi Lakshmanan on Jan 29 2010
--To fix issue 17548: ENH: All: Register Students Page is Taking Too Long to Populate Available Students 
*/

    BEGIN
        SELECT  StartDate
               ,EndDate
        FROM    arClassSections
        WHERE   ClsSectionId = @ClsSectId; 
    END;




GO
