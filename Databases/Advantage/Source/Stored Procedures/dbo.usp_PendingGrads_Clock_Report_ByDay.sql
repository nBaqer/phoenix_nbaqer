SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[usp_PendingGrads_Clock_Report_ByDay]
    (
        @Amount DECIMAL
       ,@CmpGrpID VARCHAR(MAX) = NULL
       ,@PrgVerId AS VARCHAR(MAX) = NULL
    )
AS
    SET NOCOUNT ON;

    SELECT   LastName + ', ' + FirstName AS StudentName
            ,P.PrgVerDescrip
            ,SE.StartDate
            ,SE.ExpGradDate
            ,CumulativeGPA
            ,SAS.StuEnrollId
            ,P.Hours AS TotalValue
            ,CONVERT(DECIMAL(10, 2), ROUND(( AdjustedPresentDays ), 2)) AS SumValue
            ,C.CampDescrip
    FROM     dbo.syStudentAttendanceSummary SAS
            ,dbo.syCreditSummary CS
            ,arStuEnrollments SE
            ,dbo.arStudent S
            ,dbo.arProgVerDef PV
            ,dbo.arPrgVersions P
            ,dbo.syStatusCodes SSC
            ,dbo.syCampuses C
    WHERE    SE.StuEnrollId = SAS.StuEnrollId
             AND SAS.StuEnrollId = CS.StuEnrollId
             AND SAS.AdjustedPresentDays = (
                                           SELECT MAX(AdjustedPresentDays)
                                           FROM   syStudentAttendanceSummary
                                           WHERE  StuEnrollId = SAS.StuEnrollId
                                           )
             AND CS.ReqId = PV.ReqId
             AND SE.PrgVerId = PV.PrgVerId
             AND SE.StuEnrollId = CS.StuEnrollId
             AND SE.StudentId = S.StudentId
             AND P.PrgVerId = SE.PrgVerId
             AND SE.StatusCodeId = SSC.StatusCodeId
             AND SSC.SysStatusId IN ( 7, 9, 10, 11, 20, 21 )
             AND C.CampusId = SE.CampusId
             AND (
                 SE.CampusId IN (
                                SELECT DISTINCT CGC.CampusId
                                FROM   syCmpGrpCmps CGC
                                WHERE  CGC.CampGrpId IN (
                                                        SELECT strval
                                                        FROM   dbo.SPLIT(@CmpGrpID)
                                                        )
                                )
                 OR @CmpGrpID IS NULL
                 )
             AND (
                 P.PrgVerId IN (
                               SELECT strval
                               FROM   dbo.SPLIT(@PrgVerId)
                               )
                 OR @PrgVerId IS NULL
                 )
    GROUP BY SAS.StuEnrollId
            ,LastName
            ,FirstName
            ,P.PrgVerDescrip
            ,SE.StartDate
            ,SE.ExpGradDate
            ,CumulativeGPA
            ,P.Hours
            ,SAS.AdjustedPresentDays
            ,C.CampDescrip
    HAVING   (
             ( P.Hours - ( AdjustedPresentDays )) <= @Amount
             AND ( P.Hours - ( AdjustedPresentDays )) >= 0
             )
    ORDER BY LastName;
GO
