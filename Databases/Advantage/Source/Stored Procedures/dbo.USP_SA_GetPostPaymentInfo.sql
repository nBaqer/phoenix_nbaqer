SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[USP_SA_GetPostPaymentInfo]
    (
     @TransactionId UNIQUEIDENTIFIER
    )
AS /*---------------------------------------------------------------------------------------------------- 
 Author : Saraswathi Lakshmanan 
      
    Create date : 04/12/2010 
      
 Procedure Name : [USP_SA_GetPostPaymentInfo] 
  
 Objective : Get student Post Payment INformation 
   
 Parameters : Name Type Data Type Required? 
      ===== ==== ========= ========= 
      @TransactionId In Uniqueidentifier Required 
   
 Output : returns the post payment information 
        
*/----------------------------------------------------------------------------------------------------- 
  
    BEGIN 
  
        SELECT  SE.StudentId
               ,( LastName + ', ' + FirstName ) AS StudentName
               ,T.StuEnrollId
               ,PrgVerDescrip AS EnrollmentDescrip
               ,T.TransCodeId
               ,TransCodeDescrip
               ,T.TransDescrip
               ,T.TransDate
               ,T.TransAmount
               ,T.TransReference
               ,T.AcademicYearId
               ,(
                  SELECT    AcademicYearDescrip
                  FROM      saAcademicYears
                  WHERE     AcademicYearId = T.AcademicYearId
                ) AS AcademicYearDescrip
               ,T.TermId
               ,(
                  SELECT    TermDescrip
                  FROM      arTerm
                  WHERE     TermId = T.TermId
                ) AS TermDescrip
               ,(
                  SELECT    StartDate
                  FROM      arTerm
                  WHERE     TermId = T.TermId
                ) AS EarliestAllowedDate
               ,(
                  SELECT    EndDate
                  FROM      arTerm
                  WHERE     TermId = T.TermId
                ) AS LatestAllowedDate
               ,T.TransTypeId
               ,T.IsPosted
               ,P.PaymentTypeId
               ,P.CheckNumber
               ,P.ScheduledPayment
               ,P.BankAcctId
               ,(
                  SELECT    BankAcctDescrip
                  FROM      saBankAccounts
                  WHERE     BankAcctId = P.BankAcctId
                ) AS BankAcctDescrip
               ,T.ModUser
               ,T.ModDate
               ,P.ModUser AS ModUser1
               ,P.ModDate AS ModDate1
        FROM    saTransactions T
        INNER JOIN saPayments P ON T.TransactionId = P.TransactionId
        INNER JOIN arStuEnrollments SE ON SE.StuEnrollId = T.StuEnrollId
        INNER JOIN arStudent S ON SE.StudentId = S.StudentId
        INNER JOIN arPrgVersions PR ON PR.PrgVerId = SE.PrgVerId
        LEFT JOIN saTransCodes ST ON ST.TransCodeId = T.TransCodeId
        WHERE   T.TransactionId = @TransactionId
                AND T.Voided = 0; 
    END; 





GO
