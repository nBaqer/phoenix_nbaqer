SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
----------------------------------------------------------------------------------------
-- USP_Maintenance_Update
----------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[USP_Maintenance_Update]
    @pResourceId INT
   ,@field1 VARCHAR(50)
   ,@field2 VARCHAR(50)
   ,@field3 VARCHAR(50)
   ,@field4 VARCHAR(50)
   ,@field5 VARCHAR(50)
   ,@field6 INT = 0
   ,@username VARCHAR(50)
   ,@createddate DATETIME
AS
    DECLARE @ResourceId INT
       ,@SummListName VARCHAR(50)
       ,@TableName VARCHAR(50)
       ,@DisplayText VARCHAR(50)
       ,@DisplayValue VARCHAR(50);
    DECLARE @strSQL NVARCHAR(1000)
       ,@FilterStatusId UNIQUEIDENTIFIER
       ,@SortOrder INT; 
    DECLARE @ResDefId INT
       ,@TblFldsId INT
       ,@FldId INT
       ,@TblName VARCHAR(50)
       ,@Required INT;
    DECLARE @DDLId INT
       ,@FldLen INT
       ,@FldType VARCHAR(50)
       ,@TblPK INT
       ,@Caption VARCHAR(50)
       ,@OrderBy VARCHAR(50);
    DECLARE @FldName VARCHAR(500)
       ,@DynamicFieldName VARCHAR(1000)
       ,@DisplayValueField VARCHAR(50)
       ,@DynamicHeaderFields VARCHAR(1000);
    DECLARE @intLoop INT
       ,@DynamicFieldNameVariable VARCHAR(1000)
       ,@DataType VARCHAR(1000);
    DECLARE @FldCaption VARCHAR(50)
       ,@PrimaryKeyField VARCHAR(50);
    DECLARE @DegCertSeekingId UNIQUEIDENTIFIER
       ,@Code VARCHAR(50)
       ,@Descrip VARCHAR(50)
       ,@StatusId UNIQUEIDENTIFIER
       ,@CampGrpId UNIQUEIDENTIFIER;
    DECLARE @IPEDSVAL AS VARCHAR(50);
    DECLARE contact_cursor CURSOR
    FOR
        SELECT  ResDefId
               ,TblFldsId
               ,FldId
               ,TblName
               ,FldName
               ,Required
               ,DDLId
               ,FldLen
               ,FldType
               ,TblPK
               ,Caption
               ,
-- Comment : At times, keyword code is part of code field and description field
-- and sort order may have the same value for code and description. To avoid this
-- check for description column and if sort order is 2 then reset to 3.
-- Example: Table: adEthCodes. Fields : EthCode and EthCodeDescrip
                CASE WHEN CHARINDEX('desc',Caption) > 0
                          AND SortOrder = 2 THEN 3
                     ELSE SortOrder
                END AS SortOrder
        FROM    (
                  SELECT    t1.ResDefId
                           ,t2.TblFldsId
                           ,t2.FldId
                           ,t5.TblName
                           ,t3.FldName
                           ,t1.Required
                           ,t3.DDLId
                           ,t3.FldLen
                           ,t4.FldType
                           ,t5.TblPK
                           ,t6.Caption
                           ,CASE WHEN t2.FldId = t5.TblPK THEN 1 -- Need this as the order of fields in syResTblFlds is inconsistent between pages
                                 ELSE CASE WHEN CHARINDEX('code',t3.FldName) > 0 THEN 2 -- Code 
                                           ELSE CASE WHEN CHARINDEX('desc',t3.FldName) > 0
                                                          OR CHARINDEX('name',t3.FldName) > 0 THEN 3 -- Descrip 
                                                     ELSE CASE WHEN CHARINDEX('statusid',t3.FldName) > 0 THEN 4 -- StatusId 
                                                               ELSE CASE WHEN CHARINDEX('campgrpid',t3.FldName) > 0 THEN 5 -- CampGrpId 
                                                                         ELSE CASE WHEN CHARINDEX('status',t3.FldName) > 0 THEN 6 -- Status 
                                                                                   ELSE 7
                                                                              END
                                                                    END
                                                          END
                                                END
                                      END
                            END AS SortOrder
                  FROM      syResTblFlds t1
                           ,syTblFlds t2
                           ,syFields t3
                           ,syFieldTypes t4
                           ,syTables t5
                           ,syFldCaptions t6
                           ,syLangs t7
                  WHERE     t1.TblFldsId = t2.TblFldsId
                            AND t2.FldId = t3.FldId
                            AND t2.TblId = t5.TblId
                            AND t3.FldTypeId = t4.FldTypeId
                            AND t2.FldId = t6.FldId
                            AND t6.LangId = t7.LangId
                            AND t1.ResourceId = @pResourceId
					--AND t7.LangName=?
                ) t1
        ORDER BY SortOrder; 
    OPEN contact_cursor;

-- Perform the first fetch.
    SET @intLoop = 1;
    FETCH NEXT FROM contact_cursor INTO @ResDefId,@TblFldsId,@FldId,@TblName,@DisplayValueField,@Required,@DDLId,@FldLen,@FldType,@TblPK,@Caption,@SortOrder;

    SET @DynamicFieldName = '';
    SET @DynamicFieldNameVariable = '';
    SET @DataType = '';
-- Check @@FETCH_STATUS to see if there are any more rows to fetch.
    WHILE @@FETCH_STATUS = 0
        BEGIN
            IF @SortOrder = 1
                BEGIN
                    SET @FldCaption = (
                                        SELECT  Caption
                                        FROM    dbo.syFldCaptions
                                        WHERE   FldId = @FldId
                                      );
                    SET @PrimaryKeyField = @DisplayValueField;
                END;
	
	-- Build the field names
	
            DECLARE @fldValue VARCHAR(50);
	-- For update statement we don't need Primary Key Field in the 
	-- set statements needed only in where clause
            IF @SortOrder > 1
                AND @SortOrder <= 6
                BEGIN
                    IF @SortOrder = 2
                        BEGIN
                            SET @fldValue = @field2;
                        END; 
                    IF @SortOrder = 3
                        BEGIN
                            SET @fldValue = @field3;
                        END; 
                    IF @SortOrder = 4
                        BEGIN
                            SET @fldValue = @field4;
                        END; 
                    IF @SortOrder = 5
                        BEGIN
                            SET @fldValue = @field5;
                            PRINT @field5;
                        END; 
                    IF @SortOrder = 6
                        BEGIN
                            SET @fldValue = @field6;
                        END; 
								
                    SET @DynamicFieldName = @DynamicFieldName + @DisplayValueField + '=' + '''' + @fldValue + '''' + ',';
                END;
            FETCH NEXT FROM contact_cursor INTO @ResDefId,@TblFldsId,@FldId,@TblName,@DisplayValueField,@Required,@DDLId,@FldLen,@FldType,@TblPK,@Caption,
                @SortOrder;
            SET @intLoop = @intLoop + 1; 
        END;
--PRINT @DynamicFieldName
    SET @TableName = @TblName; -- Get the Table Name
    SET @FldName = SUBSTRING(@DynamicFieldName,0,LEN(@DynamicFieldName)); -- Truncate the last comma

-- Add IPEDS Fields
    IF EXISTS ( SELECT  name AS [Column Name]
                FROM    syscolumns
                WHERE   id = (
                               SELECT   id
                               FROM     sysobjects
                               WHERE    type = 'u'
                                        AND name = @TableName
                             )
                        AND name = 'IPEDSValue' )
        BEGIN 
		-- Add IPEDS VALUE
            IF @field6 = 0
                BEGIN 
                    IF EXISTS ( SELECT  name AS [Column Name]
                                FROM    syscolumns
                                WHERE   id = (
                                               SELECT   id
                                               FROM     sysobjects
                                               WHERE    type = 'u'
                                                        AND name = @TableName
                                             )
                                        AND name = 'IPEDSSequence' )
                        BEGIN
                            SET @IPEDSVAL = ',IPEDSValue=Null,IPEDSSequence=Null'; 
                        END;
                    ELSE
                        BEGIN
                            SET @IPEDSVAL = ',IPEDSValue=Null'; 
                        END;
                END;
            ELSE
                BEGIN
                    SET @IPEDSVAL = ',IPEDSValue=' + '''' + CONVERT(VARCHAR(10),@field6) + '''';
                END;
            SET @FldName = @FldName + @IPEDSVAL;
			
        END; 

    SET @strSQL = 'Update ' + @TableName + ' Set ' + @FldName + ',' + 'ModUser=' + '''' + @username + '''' + ',' + 'ModDate=' + ''''
        + CONVERT(CHAR(25),GETDATE()) + '''' + ' WHERE ' + @PrimaryKeyField + '=' + '''' + @field1 + '''';
	
--PRINT @strSQL
    EXEC (@strSQL);
    CLOSE contact_cursor;
    DEALLOCATE contact_cursor;



GO
