SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


---------------------------------------------------------------------------------------------------------------------------
--This section is for testing purposes only.
----------------------------------------------------------------------------------------------------------------------------
--declare @CampGrpId as varchar(8000)
--declare @PrgVerId as varchar(8000)
--declare @StartDate as datetime
--declare @EndDate as datetime

--set @CampGrpId='711DAE4C-9FA0-4814-B90C-7C8297596686' 
--set @PrgVerId = null
--set @StartDate='1/1/2010'
--set @EndDate='3/31/2010'
---------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[USP_MedianLoanDebt_GetDetail]
    @CampGrpId VARCHAR(8000) = '0F811F4D-FA5E-45A7-B9C4-3B52A8FE1238'
   ,@PrgVerId VARCHAR(8000) = NULL
   ,@StartDate DATETIME = '7/1/2010'
   ,@EndDate DATETIME = '6/30/2011'
AS
    BEGIN 
	
        SELECT  s.FirstName + ' ' + s.LastName + ' ' + ( ISNULL(s.MiddleName,'') ) AS StudentName
               ,se.PrgVerId
               ,pv.PrgVerDescrip
               ,pv.PrgVerCode
               ,se.StuEnrollId
               ,s.DOB
               ,(
                  SELECT    ISNULL(SUM(GrossAmount),0)
                  FROM      faStudentAwards tr
                           ,saFundSources fs
                  WHERE     tr.StuEnrollId = se.StuEnrollId
                            AND tr.AwardTypeId = fs.FundSourceId
                            AND fs.TitleIV = 1
                            AND fs.AwardTypeId = 2
                            AND fs.AdvFundSourceId NOT IN ( 4,6,9 )
                ) AS TitleIVLoanAmt
               ,(
                  SELECT    ISNULL(SUM(GrossAmount),0)
                  FROM      faStudentAwards tr
                           ,saFundSources fs
                  WHERE     tr.StuEnrollId = se.StuEnrollId
                            AND tr.AwardTypeId = fs.FundSourceId
                            AND fs.TitleIV = 0
                            AND fs.AwardTypeId = 2
                            AND fs.AdvFundSourceId NOT IN ( 4,6,9 )
                ) AS NonTitleIVLoanAmt
               ,(
                  SELECT    ISNULL(SUM(GrossAmount),0)
                  FROM      faStudentAwards tr
                           ,saFundSources fs
                  WHERE     tr.StuEnrollId = se.StuEnrollId
                            AND tr.AwardTypeId = fs.FundSourceId
                            AND fs.AwardTypeId = 2
                            AND fs.AdvFundSourceId NOT IN ( 4,6,9 )
                ) AS TotalLoanAmt
               ,(
                  SELECT    ISNULL(SUM(tr.TransAmount),0) - (
                                                              SELECT    ISNULL(SUM(Amount),0)
                                                              FROM      faStudentAwardSchedule sas
                                                                       ,faStudentAwards sa
                                                              WHERE     sas.StudentAwardId = sa.StudentAwardId
                                                                        AND sa.StuEnrollId = se.StuEnrollId
                                                                        AND NOT EXISTS ( SELECT pdr.AwardScheduleId
                                                                                         FROM   saPmtDisbRel pdr
                                                                                         WHERE  pdr.AwardScheduleId = sas.AwardScheduleId )
                                                            )
                  FROM      saTransactions tr
                  WHERE     tr.Voided = 0
                            AND tr.StuEnrollId = se.StuEnrollId
                ) AS InstitutionalOwed
               ,cm.CampDescrip
               ,(
                  SELECT    cg.CampGrpDescrip
                  FROM      syCmpGrpCmps cgc
                           ,syCampGrps cg
                  WHERE     cgc.CampGrpId = cg.CampGrpId
                            AND cgc.CampusId = se.CampusId
                            AND cgc.CampGrpId IN ( SELECT   strval
                                                   FROM     dbo.SPLIT(@CampGrpId) )
                ) AS CampGrpDescrip
        FROM    arStudent s
               ,arStuEnrollments se
               ,syStatusCodes sc
               ,arPrgVersions pv
               ,syCampuses cm
        WHERE   s.StudentId = se.StudentId
                AND se.StatusCodeId = sc.StatusCodeId
                AND se.PrgVerId = pv.PrgVerId
                AND se.CampusId = cm.CampusId
                AND sc.SysStatusId = 14 --Graduated System Status
                AND se.ExpGradDate BETWEEN @StartDate AND @EndDate
                AND (
                      @PrgVerId IS NULL
                      OR se.PrgVerId IN ( SELECT    strval
                                          FROM      SPLIT(@PrgVerId) )
                    )
                AND se.CampusId IN ( SELECT DISTINCT
                                            CGC.CampusId
                                     FROM   syCmpGrpCmps CGC
                                     WHERE  CGC.CampGrpId IN ( SELECT   strval
                                                               FROM     dbo.SPLIT(@CampGrpId) ) )	
	--and pv.PrgVerDescrip='Electrical Construction & Planning (D)'
        ORDER BY cm.CampDescrip
               ,pv.PrgVerDescrip
               ,StudentName;

    END;






GO
