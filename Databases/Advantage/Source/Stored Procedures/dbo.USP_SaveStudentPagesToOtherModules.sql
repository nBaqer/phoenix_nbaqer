SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/***************************************
Exec USP_SaveStudentPagesToOtherModules
'<NewDataSet>
  <StudentPageNavigation ModuleResourceId="300" SubMenuResourceId="740" PageResourceId="614" ModUser="sa" CheckboxState="1" />
  <StudentPageNavigation ModuleResourceId="191" SubMenuResourceId="740" PageResourceId="614" ModUser="sa" CheckboxState="0" />
</NewDataSet>'
go
Exec USP_SaveStudentPagesToOtherModules
'<NewDataSet>
  <StudentPageNavigation ModuleResourceId="300" SubMenuResourceId="737" PageResourceId="155" ModUser="sa" CheckboxState="false" />
  <StudentPageNavigation ModuleResourceId="300" SubMenuResourceId="737" PageResourceId="213" ModUser="sa" CheckboxState="false" />
  <StudentPageNavigation ModuleResourceId="191" SubMenuResourceId="737" PageResourceId="213" ModUser="sa" CheckboxState="false" />
  <StudentPageNavigation ModuleResourceId="193" SubMenuResourceId="737" PageResourceId="213" ModUser="sa" CheckboxState="false" />
  <StudentPageNavigation ModuleResourceId="194" SubMenuResourceId="737" PageResourceId="213" ModUser="sa" CheckboxState="false" />
  <StudentPageNavigation ModuleResourceId="300" SubMenuResourceId="737" PageResourceId="90" ModUser="sa" CheckboxState="false" />
  <StudentPageNavigation ModuleResourceId="191" SubMenuResourceId="737" PageResourceId="90" ModUser="sa" CheckboxState="false" />
  <StudentPageNavigation ModuleResourceId="193" SubMenuResourceId="737" PageResourceId="90" ModUser="sa" CheckboxState="false" />
  <StudentPageNavigation ModuleResourceId="300" SubMenuResourceId="737" PageResourceId="159" ModUser="sa" CheckboxState="false" />
  <StudentPageNavigation ModuleResourceId="300" SubMenuResourceId="738" PageResourceId="327" ModUser="sa" CheckboxState="false" />
  <StudentPageNavigation ModuleResourceId="300" SubMenuResourceId="738" PageResourceId="530" ModUser="sa" CheckboxState="false" />
  <StudentPageNavigation ModuleResourceId="300" SubMenuResourceId="738" PageResourceId="531" ModUser="sa" CheckboxState="false" />
</NewDataSet>'
go
exec USP_SaveStudentPagesToOtherModules
'<NewDataSet>
  <StudentPageNavigation ModuleResourceId="300" SubMenuResourceId="737" PageResourceId="155" ModUser="sa" CheckboxState="true" />
  <StudentPageNavigation ModuleResourceId="300" SubMenuResourceId="737" PageResourceId="213" ModUser="sa" CheckboxState="true" />
  <StudentPageNavigation ModuleResourceId="191" SubMenuResourceId="737" PageResourceId="213" ModUser="sa" CheckboxState="false" />
  <StudentPageNavigation ModuleResourceId="193" SubMenuResourceId="737" PageResourceId="213" ModUser="sa" CheckboxState="false" />
  <StudentPageNavigation ModuleResourceId="194" SubMenuResourceId="737" PageResourceId="213" ModUser="sa" CheckboxState="false" />
  <StudentPageNavigation ModuleResourceId="300" SubMenuResourceId="737" PageResourceId="90" ModUser="sa" CheckboxState="false" />
  <StudentPageNavigation ModuleResourceId="191" SubMenuResourceId="737" PageResourceId="90" ModUser="sa" CheckboxState="false" />
  <StudentPageNavigation ModuleResourceId="193" SubMenuResourceId="737" PageResourceId="90" ModUser="sa" CheckboxState="false" />
  <StudentPageNavigation ModuleResourceId="300" SubMenuResourceId="737" PageResourceId="159" ModUser="sa" CheckboxState="false" />
  <StudentPageNavigation ModuleResourceId="300" SubMenuResourceId="738" PageResourceId="327" ModUser="sa" CheckboxState="false" />
  <StudentPageNavigation ModuleResourceId="300" SubMenuResourceId="738" PageResourceId="530" ModUser="sa" CheckboxState="false" />
  <StudentPageNavigation ModuleResourceId="300" SubMenuResourceId="738" PageResourceId="531" ModUser="sa" CheckboxState="false" />
</NewDataSet>'
go
--Check page
select navPage.* from 
			syNavigationNodes navPage  INNER JOIN syNavigationNodes navSubMenu ON navPage.ParentId = navSubMenu.HierarchyId
			INNER JOIN syNavigationNodes navStudentModule ON navSubMenu.ParentId = navStudentModule.HierarchyId
			INNER JOIN syNavigationNodes navModule ON navStudentModule.ParentId = navModule.HierarchyId
			WHERE
				navPage.ResourceId=155 AND
				navSubMenu.ResourceId=737 AND 
				navStudentModule.ResourceId=394 AND
				navModule.ResourceId=300

--Check Submenu				
select navSubMenu.* from 
			syNavigationNodes navSubMenu INNER JOIN syNavigationNodes navStudentModule 
			ON navSubMenu.ParentId = navStudentModule.HierarchyId
			INNER JOIN syNavigationNodes navModule 
			ON navStudentModule.ParentId = navModule.HierarchyId
			WHERE
				navSubMenu.ResourceId=740 AND 
				navStudentModule.ResourceId=394 AND
				navModule.ResourceId=191
				
select count(*) from 
			syNavigationNodes navPage  INNER JOIN syNavigationNodes navSubMenu ON navPage.ParentId = navSubMenu.HierarchyId
			INNER JOIN syNavigationNodes navStudentModule ON navSubMenu.ParentId = navStudentModule.HierarchyId
			INNER JOIN syNavigationNodes navModule ON navStudentModule.ParentId = navModule.HierarchyId
			WHERE
				navPage.ResourceId=614 AND
				navSubMenu.ResourceId=740 AND 
				navStudentModule.ResourceId=394 AND
				navModule.ResourceId=191
*******************************************************************************/				
CREATE PROCEDURE [dbo].[USP_SaveStudentPagesToOtherModules] @RuleValues NTEXT
AS
    DECLARE @hDoc INT;
		--Prepare input values as an XML document
    EXEC sp_xml_preparedocument @hDoc OUTPUT,@RuleValues;
		
		
    DECLARE @ModuleResourceId INT
       ,@SubMenuResourceId INT
       ,@PageResourceId INT;
    DECLARE @hierarchyid UNIQUEIDENTIFIER
       ,@Parentid UNIQUEIDENTIFIER
       ,@state BIT;
		
    CREATE TABLE #tblModuleSubMenuPage
        (
         ModuleResourceId INT
        ,SubMenuResourceId INT
        ,PageResourceId INT
        ,CheckboxState BIT
        );
		
		
	  
	   
    INSERT  INTO #tblModuleSubMenuPage
            SELECT  ModuleResourceId
                   ,SubMenuResourceId
                   ,PageResourceId
                   ,CheckboxState
            FROM    OPENXML(@hDoc,'/NewDataSet/StudentPageNavigation',1) 
							WITH (ModuleResourceId INT,SubMenuResourceId INT,PageResourceId INT,CheckboxState BIT); 
 
    SELECT  *
    FROM    #tblModuleSubMenuPage;
	
	  
    DECLARE getModulesAndPages CURSOR
    FOR
        SELECT  ModuleResourceId
               ,SubMenuResourceId
               ,PageResourceId
               ,CheckboxState
        FROM    #tblModuleSubMenuPage;  

    OPEN getModulesAndPages;

    FETCH NEXT FROM getModulesAndPages
INTO @ModuleResourceId,@SubMenuResourceId,@PageResourceId,@state;

    WHILE @@FETCH_STATUS = 0
        BEGIN
		
	  --'''' Comment 1: Delete the menu and submenu based on state (coming from whether checkbox was selected or not)'''
		  
            IF (
                 SELECT COUNT(*)
                 FROM   syNavigationNodes navPage
                 INNER JOIN syNavigationNodes navSubMenu ON navPage.ParentId = navSubMenu.HierarchyId
                 INNER JOIN syNavigationNodes navStudentModule ON navSubMenu.ParentId = navStudentModule.HierarchyId
                 INNER JOIN syNavigationNodes navModule ON navStudentModule.ParentId = navModule.HierarchyId
                 WHERE  navPage.ResourceId = @PageResourceId
                        AND navSubMenu.ResourceId = @SubMenuResourceId
                        AND navStudentModule.ResourceId = 394
                        AND navModule.ResourceId = @ModuleResourceId
               ) >= 1
                AND @state = 0
                BEGIN
			-- if this item is the only item under the submenu
			-- first remove the page and then submenu
                    IF (
                         SELECT COUNT(*)
                         FROM   syNavigationNodes navPage
                         INNER JOIN syNavigationNodes navSubMenu ON navPage.ParentId = navSubMenu.HierarchyId
                         INNER JOIN syNavigationNodes navStudentModule ON navSubMenu.ParentId = navStudentModule.HierarchyId
                         INNER JOIN syNavigationNodes navModule ON navStudentModule.ParentId = navModule.HierarchyId
                         WHERE  navSubMenu.ResourceId = @SubMenuResourceId
                                AND navStudentModule.ResourceId = 394
                                AND navModule.ResourceId = @ModuleResourceId
                       ) = 1
                        AND @state = 0
                        BEGIN
					--Print 'IF'
					--Delete the page
                            DELETE  FROM syNavigationNodes
                            WHERE   ResourceId = @PageResourceId
                                    AND ParentId IN ( SELECT DISTINCT
                                                                navSubMenu.HierarchyId
                                                      FROM      syNavigationNodes navSubMenu
                                                      INNER JOIN syNavigationNodes navStudentModule ON navSubMenu.ParentId = navStudentModule.HierarchyId
                                                      INNER JOIN syNavigationNodes navModule ON navStudentModule.ParentId = navModule.HierarchyId
                                                      WHERE     navStudentModule.ResourceId = 394
                                                                AND navSubMenu.ResourceId = @SubMenuResourceId
                                                                AND navModule.ResourceId = @ModuleResourceId );
					
					-- Delete the sub menu
                            DELETE  FROM syNavigationNodes
                            WHERE   ResourceId = @SubMenuResourceId
                                    AND ParentId IN ( SELECT DISTINCT
                                                                navStudentModule.HierarchyId
                                                      FROM      syNavigationNodes navStudentModule
                                                      INNER JOIN syNavigationNodes navModule ON navStudentModule.ParentId = navModule.HierarchyId
                                                      WHERE     navStudentModule.ResourceId = 394
                                                                AND navModule.ResourceId = @ModuleResourceId );
						
                        END;	
                    ELSE
                        BEGIN
					--Print 'Comes to else part'
					--Delete the page and leave the sub menu
                            DELETE  FROM syNavigationNodes
                            WHERE   ResourceId = @PageResourceId
                                    AND ParentId IN ( SELECT DISTINCT
                                                                navSubMenu.HierarchyId
                                                      FROM      syNavigationNodes navSubMenu
                                                      INNER JOIN syNavigationNodes navStudentModule ON navSubMenu.ParentId = navStudentModule.HierarchyId
                                                      INNER JOIN syNavigationNodes navModule ON navStudentModule.ParentId = navModule.HierarchyId
                                                      WHERE     navStudentModule.ResourceId = 394
                                                                AND navSubMenu.ResourceId = @SubMenuResourceId
                                                                AND navModule.ResourceId = @ModuleResourceId );
                        END;
                END;
	  --'''' Comment 1 Ends Here'''
	
	 -- '''' Comment 2 Starts Here '''''''''''''''''''''''''''''''''
	 --''''' Insert the submenu and menu '''''''''''''''''''''''''''
            IF (
                 SELECT COUNT(*)
                 FROM   syNavigationNodes navPage
                 INNER JOIN syNavigationNodes navSubMenu ON navPage.ParentId = navSubMenu.HierarchyId
                 INNER JOIN syNavigationNodes navStudentModule ON navSubMenu.ParentId = navStudentModule.HierarchyId
                 INNER JOIN syNavigationNodes navModule ON navStudentModule.ParentId = navModule.HierarchyId
                 WHERE  navPage.ResourceId = @PageResourceId
                        AND navSubMenu.ResourceId = @SubMenuResourceId
                        AND navModule.ResourceId = @ModuleResourceId
               ) = 0
                AND @state = 1 -- If page is not part of this module and submenu
                BEGIN
                    IF (
                         SELECT COUNT(*)
                         FROM   syNavigationNodes navSubMenu
                         INNER JOIN syNavigationNodes navStudentModule ON navSubMenu.ParentId = navStudentModule.HierarchyId
                         INNER JOIN syNavigationNodes navModule ON navStudentModule.ParentId = navModule.HierarchyId
                         WHERE  navSubMenu.ResourceId = @SubMenuResourceId
                                AND navModule.ResourceId = @ModuleResourceId
                       ) = 0
                        BEGIN
						--Print 'Step1 - Start'
						--Print @SubMenuResourceId
						--Print @ModuleResourceId
						--Print @PageResourceId
						--Print 'Step1 - End'
						-- Submenu is not part of module, so it has to be added to module
						-- Step 1 : Now add the submenu first to the module
						-- Step 2: Now insert the page and establish relation with submenu
                            SET @hierarchyid = NEWID();
						--Get HierarchyId of ResourceId=394 and Module
                            SET @Parentid = (
                                              SELECT TOP 1
                                                        navStudentModule.HierarchyId
                                              FROM      syNavigationNodes navStudentModule
                                              INNER JOIN syNavigationNodes navModule ON navStudentModule.ParentId = navModule.HierarchyId
                                              WHERE     navStudentModule.ResourceId = 394
                                                        AND navModule.ResourceId = @ModuleResourceId
                                            ); 
						
						-- Step 1 : Now Insert the submenu first
                            INSERT  INTO syNavigationNodes
                                    (
                                     HierarchyId
                                    ,HierarchyIndex
                                    ,ResourceId
                                    ,ParentId
                                    ,ModUser
                                    ,ModDate
                                    ,IsPopupWindow
									)
                            VALUES  (
                                     @hierarchyid
                                    ,1
                                    ,@SubMenuResourceId
                                    ,@Parentid
                                    ,'sa'
                                    ,GETDATE()
                                    ,0
									);
						
						-- Step 2: Now insert the page and establish relation with submenu
                            SET @Parentid = @hierarchyid; -- now old hierarchyid becomes the new parent id for this page
                            SET @hierarchyid = NEWID();
                            INSERT  INTO syNavigationNodes
                                    (
                                     HierarchyId
                                    ,HierarchyIndex
                                    ,ResourceId
                                    ,ParentId
                                    ,ModUser
                                    ,ModDate
                                    ,IsPopupWindow
									)
                            VALUES  (
                                     @hierarchyid
                                    ,1
                                    ,@PageResourceId
                                    ,@Parentid
                                    ,'sa'
                                    ,GETDATE()
                                    ,0
									);
                        END;
                    ELSE
                        BEGIN
						--Print 'Step1 Else- Start'
						--Print @SubMenuResourceId
						--Print @ModuleResourceId
						--Print @PageResourceId
						--Print 'Step1 - End'
						-- Submenu is part of module, no need to add submenu, just add page
						-- Step 2: Now insert the page and establish relation with submenu
                            SET @hierarchyid = NEWID();
						--Get HierarchyId of ResourceId=394 and Module
                            SET @Parentid = (
                                              SELECT TOP 1
                                                        navSubMenu.HierarchyId
                                              FROM      syNavigationNodes navSubMenu
                                              INNER JOIN syNavigationNodes navStudentModule ON navSubMenu.ParentId = navStudentModule.HierarchyId
                                              INNER JOIN syNavigationNodes navModule ON navStudentModule.ParentId = navModule.HierarchyId
                                              WHERE     navSubMenu.ResourceId = @SubMenuResourceId
                                                        AND navStudentModule.ResourceId = 394
                                                        AND navModule.ResourceId = @ModuleResourceId
                                            ); 
						
						-- Step 1 : Now Insert the submenu first
                            INSERT  INTO syNavigationNodes
                                    (
                                     HierarchyId
                                    ,HierarchyIndex
                                    ,ResourceId
                                    ,ParentId
                                    ,ModUser
                                    ,ModDate
                                    ,IsPopupWindow
									)
                            VALUES  (
                                     @hierarchyid
                                    ,1
                                    ,@PageResourceId
                                    ,@Parentid
                                    ,'sa'
                                    ,GETDATE()
                                    ,0
									);
                        END;
                END;
		--'''''''''''''''''''''' Comment 2 ends here ''''''''''''''''''''''''''''''
            FETCH NEXT FROM getModulesAndPages
INTO @ModuleResourceId,@SubMenuResourceId,@PageResourceId,@state;
        END;
    CLOSE getModulesAndPages;
    DEALLOCATE getModulesAndPages;

    DROP TABLE #tblModuleSubMenuPage;                          
    EXEC sp_xml_removedocument @hDoc;



GO
