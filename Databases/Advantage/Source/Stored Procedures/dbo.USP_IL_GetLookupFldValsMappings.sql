SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[USP_IL_GetLookupFldValsMappings] @MappingId VARCHAR(50)
AS
    SELECT  lf.FldName
           ,lf.DDLName
           ,lf.Caption
           ,lm.FileValue
           ,lm.AdvValue
    FROM    adImportLeadsLookupValsMap lm
           ,adImportLeadsFields lf
    WHERE   lm.ILFieldId = lf.ILFieldId
            AND MappingId = @MappingId;





GO
