SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_InternationalStudents_List] @CampusId VARCHAR(50)
AS
    DECLARE @SettingId INT
       ,@SettingValue VARCHAR(50);
    SET @SettingId = (
                       SELECT   SettingId
                       FROM     dbo.syConfigAppSettings
                       WHERE    LOWER(KeyName) = 'studentidentifier'
                     );
    
    SET @SettingValue = (
                          SELECT TOP 1
                                    Value
                          FROM      dbo.syConfigAppSetValues
                          WHERE     SettingId = @SettingId
                        );

    SELECT DISTINCT
            CASE WHEN LOWER(@SettingValue) = 'ssn' THEN CASE WHEN LEN(S.SSN) = 9 THEN '***-**-' + SUBSTRING(S.SSN,6,4)
                                                             ELSE S.SSN
                                                        END
                 ELSE S.StudentNumber
            END AS StudentID
           ,S.LastName
           ,S.MiddleName
           ,S.FirstName
    FROM    arStudent S
    INNER JOIN dbo.arStuEnrollments SE ON S.StudentId = SE.StudentId
    INNER JOIN dbo.arPrgVersions PV ON PV.PrgVerId = SE.PrgVerId
    INNER JOIN dbo.arPrograms P ON P.ProgId = PV.ProgId
    INNER JOIN dbo.adLeadByLeadGroups LLG ON LLG.StuEnrollId = SE.StuEnrollId
    INNER JOIN dbo.adLeadGroups LG ON LG.LeadGrpId = LLG.LeadGrpId
    LEFT OUTER JOIN dbo.adCitizenships C ON S.Citizen = C.CitizenshipId
    WHERE   SE.CampusId = @CampusId
            AND P.Is1098T = 1
            --AND S.StudentStatus = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
            AND (
                  LG.Descrip LIKE 'Int%' -- International Lead Group
                  OR C.IPEDSValue = 65
                )  --Non-Citizen
    ORDER BY S.LastName;
GO
