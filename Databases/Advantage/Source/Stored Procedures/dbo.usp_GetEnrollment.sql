SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_GetEnrollment]
    (
     @stuEnrollId UNIQUEIDENTIFIER
    )
AS
    SET NOCOUNT ON;
    SELECT DISTINCT
            arStuEnrollments.StuEnrollId
           ,(
              SELECT    ProgDescrip
              FROM      arPrograms X
                       ,arPrgVersions Y
              WHERE     Y.PrgVerId = arStuEnrollments.PrgVerId
                        AND Y.ProgId = X.ProgId
            ) AS ProgramDescrip
           ,A.SSN
           ,arStuEnrollments.EnrollmentId
           ,A.StudentNumber
           ,arStuEnrollments.StatusCodeId
           ,(
              SELECT    StatusCodeDescrip
              FROM      syStatusCodes
              WHERE     StatusCodeId = arStuEnrollments.StatusCodeId
            ) AS StatusCodeDescrip
           ,(
              SELECT    SysStatusId
              FROM      syStatusCodes
              WHERE     StatusCodeId = arStuEnrollments.StatusCodeId
            ) AS SysStatusId
           ,arStuEnrollments.StartDate
           ,arStuEnrollments.ExpGradDate
           ,ISNULL((
                     SELECT A.LastName + ', ' + A.FirstName + ' ' + A.MiddleName
                   ),(
                       SELECT   A.LastName + ', ' + A.FirstName
                     )) AS StudentName
           , 
 --syCmpGrpCmps. CampGrpId,syCampGrps.CampGrpDescrip,
            (
              SELECT TOP 1
                        CampGrpId
              FROM      syCmpGrpCmps
              WHERE     campusid = C.Campusid
            ) AS CampGrpId
           ,(
              SELECT TOP 1
                        CampGrpDescrip
              FROM      syCampGrps
              WHERE     CampGrpId = syCampGrps.CampGrpId
            ) AS CampGrpDescrip
           ,C.CampDescrip
           ,(
              SELECT    MAX(StartDate)
              FROM      arStuProbWarnings SP
              WHERE     SP.StuEnrollId = arStuEnrollments.StuEnrollId
                        AND SP.ProbWarningTypeId = 1
            ) AS BeginProbationDate
           ,(
              SELECT    MAX(StartDate)
              FROM      arStudentLOAs SL
              WHERE     SL.StuEnrollId = arStuEnrollments.StuEnrollId
            ) AS BeginLeaveDate
           ,(
              SELECT    MAX(EndDate)
              FROM      arStudentLOAs SL
              WHERE     SL.StuEnrollId = arStuEnrollments.StuEnrollId
            ) AS EndLeaveDate
           ,arStuEnrollments.LDA
           ,arStuEnrollments.DateDetermined
           ,(
              SELECT    MAX(StartDate)
              FROM      arStdSuspensions SS
              WHERE     SS.StuEnrollId = arStuEnrollments.StuEnrollId
            ) AS BeginSuspensionDate
           ,(
              SELECT    MAX(EndDate)
              FROM      arStdSuspensions SS
              WHERE     SS.StuEnrollId = arStuEnrollments.StuEnrollId
            ) AS EndSuspensionDate
           ,(
              SELECT    UnitTypeDescrip
              FROM      arPrograms X
                       ,arPrgVersions Y
                       ,arAttUnitType Z
              WHERE     Y.PrgVerId = arStuEnrollments.PrgVerId
                        AND Y.ProgId = X.ProgId
                        AND Y.UnitTypeid = Z.UnitTypeid
            ) AS UnitTypeDescrip
           ,A.FirstName
           ,A.LastName
           ,A.MiddleName
           ,NULL AS ClsSectionid
           ,NULL AS ClsStartDate
           ,NULL AS ClsEndDate
           ,A.DOB
           ,(
              SELECT    dg.DegreeDescrip
              FROM      arPrgVersions pv
                       ,arDegrees dg
              WHERE     pv.PrgVerId = arStuEnrollments.PrgVerId
                        AND pv.DegreeId = dg.DegreeId
            ) AS DegreeDescrip
           ,ISNULL((
                     SELECT TOP 1
                            Phone
                     FROM   arStudentPhone
                     WHERE  studentid = A.studentid
                            AND default1 = 1
                   ),'') AS studentPhone
           ,(
              SELECT TOP 1
                        ForeignPhone
              FROM      arStudentPhone
              WHERE     studentid = A.studentid
                        AND default1 = 1
            ) AS ForeignPhone
    FROM    arStuEnrollments
    JOIN    arStudent A ON arStuEnrollments.StudentId = A.StudentId
    JOIN    syCampuses C ON arStuEnrollments.CampusId = C.CampusId
    JOIN    syCmpGrpCmps ON syCmpGrpCmps.CampusId = arStuEnrollments.CampusId
                            AND syCmpGrpCmps.CampusId = arStuEnrollments.CampusId
    JOIN    syCampGrps ON syCampGrps.CampGrpId = syCmpGrpCmps.CampGrpId
                          AND syCampGrps.CampGrpDescrip <> 'All'
                          AND arStuEnrollments.StuEnrollId = @stuEnrollId
            -- and syCampGrps.campusid is not null
            --New Code Added By Vijay Ramteke On June 23, 2010
                          AND arStuEnrollments.StuEnrollId NOT IN ( SELECT  SGS.StuEnrollId
                                                                    FROM    adStuGrpStudents SGS
                                                                           ,adStudentGroups SG
                                                                    WHERE   SGS.StuGrpId = SG.StuGrpId
                                                                            AND SG.IsTransHold = 1
                                                                            AND SGS.IsDeleted = 0
                                                                            AND SGS.StuEnrollId IS NOT NULL ); 
            --New Code Added By Vijay Ramteke On June 23, 2010




GO
