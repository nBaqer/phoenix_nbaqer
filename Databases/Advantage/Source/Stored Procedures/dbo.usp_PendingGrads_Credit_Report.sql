SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
------------------------------------------------------------------------------------
-- US6249 END - JG - ADD Indexes for syaudithistdetail if missing
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
-- Defect Pending Graduation Report not working
-------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[usp_PendingGrads_Credit_Report]
    (
     @Amount DECIMAL
    ,@CmpGrpID VARCHAR(MAX) = NULL
    ,@PrgVerId AS VARCHAR(MAX) = NULL
    )
AS
    BEGIN
        SET NOCOUNT ON;
        
        SELECT DISTINCT
                LastName + ', ' + FirstName AS StudentName
               ,LastName
               ,P.PrgVerDescrip
               ,SE.StartDate
               ,SE.ExpGradDate
               ,P.Credits AS TotalValue
               ,  --3
                CONVERT(DECIMAL(10,2),ROUND(SUM(CreditsEarned),2)) AS SumValue
               ,C.CampDescrip
        FROM    dbo.syCreditSummary CS
        JOIN    arStuEnrollments SE ON SE.StuEnrollId = CS.StuEnrollId
        INNER   JOIN dbo.arStudent S ON SE.StudentId = S.StudentId
        INNER  JOIN dbo.arPrgVersions P ON P.PrgVerId = SE.PrgVerId
        INNER  JOIN dbo.syStatusCodes SSC ON SE.StatusCodeId = SSC.StatusCodeId
        INNER JOIN dbo.syCampuses C ON C.CampusId = SE.CampusId
        WHERE   SSC.SysStatusId IN ( 7,9,10,11,20,21 )
                AND (
                      SE.CampusId IN ( SELECT DISTINCT
                                                CGC.CampusId
                                       FROM     syCmpGrpCmps CGC
                                       WHERE    CGC.CampGrpId IN ( SELECT   strval
                                                                   FROM     dbo.SPLIT(@CmpGrpID) ) )
                      OR @CmpGrpID IS NULL
                    )
                AND (
                      P.PrgVerId IN ( SELECT    strval
                                      FROM      dbo.SPLIT(@PrgVerId) )
                      OR @PrgVerId IS NULL
                    )
        GROUP BY CS.StuEnrollId
               ,LastName
               ,FirstName
               ,P.PrgVerDescrip
               ,SE.StartDate
               ,SE.ExpGradDate
               ,P.Credits
               ,C.CampDescrip
        HAVING  (
                  ( P.Credits - SUM(CreditsEarned) ) <= @Amount
                  AND ( P.Credits - SUM(CreditsEarned) ) >= 0
                )
        ORDER BY LastName;
    END; 



GO
