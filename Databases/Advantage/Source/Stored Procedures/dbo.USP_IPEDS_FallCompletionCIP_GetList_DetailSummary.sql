SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[USP_IPEDS_FallCompletionCIP_GetList_DetailSummary]
    @CampusId VARCHAR(50)
   ,@ProgId VARCHAR(4000) = NULL
   ,@StartDate DATETIME = NULL
   ,@EndDate DATETIME = NULL
   ,@DateRangeText VARCHAR(100) = NULL
   ,@OrderBy VARCHAR(100)
AS
    BEGIN
        SELECT  NEWID() AS RowNumber
               ,StudentId
               ,dbo.UDF_FormatSSN(SSN) AS SSN
               ,StudentNumber
               ,StudentName
               ,Men
               ,Women
               ,Race
               ,Gender
               ,GenderSequence
               ,RaceSequence
               ,CASE Men
                  WHEN 'X' THEN 1
                  ELSE 0
                END AS MenCount
               ,CASE Women
                  WHEN 'X' THEN 1
                  ELSE 0
                END AS WomenCount
               ,EnrollmentId
        FROM    (
                  SELECT    NULL AS StudentId
                           ,NULL AS SSN
                           ,NULL AS StudentNumber
                           ,NULL AS StudentName
                           ,'' AS Men
                           ,'' AS Women
                           ,'Nonresident Alien' AS Race
                           ,NULL AS Gender
                           ,1 AS GenderSequence
                           ,1 AS RaceSequence
                           ,NULL AS EnrollmentId
                  UNION
                  SELECT    NULL AS StudentId
                           ,NULL AS SSN
                           ,NULL AS StudentNumber
                           ,NULL AS StudentName
                           ,'' AS Men
                           ,'' AS Women
                           ,AgencyDescrip AS Race
                           ,NULL AS Gender
                           ,1 AS GenderSequence
                           ,CASE WHEN RptAgencyFldValId = 26 THEN 2
                                 WHEN RptAgencyFldValId = 27 THEN 3
                                 WHEN RptAgencyFldValId = 23 THEN 4
                                 WHEN RptAgencyFldValId = 24 THEN 5
                                 WHEN RptAgencyFldValId = 82 THEN 6
                                 WHEN RptAgencyFldValId = 25 THEN 7
                                 WHEN RptAgencyFldValId = 83 THEN 8
                                 WHEN RptAgencyFldValId = 28 THEN 9
                            END AS RaceSequence
                           ,NULL AS EnrollmentId
                  FROM      syRptAgencyFldValues
                  WHERE     RptAgencyFldId = 15
                            AND RptAgencyFldValId <> 29
                  UNION
                  SELECT    t1.StudentId
                           ,t1.SSN
                           ,t1.StudentNumber
                           ,t1.LastName + ', ' + t1.FirstName + ' ' + ISNULL(t1.MiddleName,'') AS StudentName
                           ,CASE WHEN t3.IPEDSValue = 30 THEN 'X'
                                 ELSE ''
                            END AS Men
                           ,CASE WHEN t3.IPEDSValue = 31 THEN 'X'
                                 ELSE ''
                            END AS Women
                           ,'Nonresident Alien' AS Race
                           ,t3.IPEDSValue AS Gender
                           ,t3.IPEDSSequence AS GenderSequence
                           ,1 AS RaceSequence
                           ,NULL AS EnrollmentId
                  FROM      adGenders t3
                  LEFT JOIN arStudent t1 ON t3.GenderId = t1.Gender
                  LEFT JOIN adEthCodes t4 ON t4.EthCodeId = t1.Race
                  INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                  INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                  INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                               AND t6.SysStatusId NOT IN ( 7,8 )
                  INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                  INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                  INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                  LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                  INNER JOIN adCitizenships t12 ON t1.Citizen = t12.CitizenshipId
                  WHERE     t2.CampusId = @CampusId
                            AND (
                                  (
                                    t8.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
                                    AND @ProgId IS NULL
                                  )
                                  OR t8.ProgId IN ( SELECT  Val
                                                    FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                )
                            AND t6.SysStatusId IN ( 14 )
                            AND (
                                  t3.IPEDSValue = 30
                                  OR t3.IPEDSValue = 31
                                )
                            AND t1.Race IS NOT NULL
                            AND t2.ExpGradDate >= @StartDate
                            AND t2.ExpGradDate <= @EndDate
                            AND t12.IPEDSValue = 65	
		-- and t4.IPEDSValue is not null
                  UNION
                  SELECT    t1.StudentId
                           ,t1.SSN
                           ,t1.StudentNumber
                           ,t1.LastName + ', ' + t1.FirstName + ' ' + ISNULL(t1.MiddleName,'') AS StudentName
                           ,CASE WHEN t3.IPEDSValue = 30 THEN 'X'
                                 ELSE ''
                            END AS Men
                           ,CASE WHEN t3.IPEDSValue = 31 THEN 'X'
                                 ELSE ''
                            END AS Women
                           ,CASE WHEN t4.IPEDSValue IS NULL THEN 'Race/ethnicity unknown'
                                 ELSE (
                                        SELECT DISTINCT
                                                AgencyDescrip
                                        FROM    syRptAgencyFldValues
                                        WHERE   RptAgencyFldValId = t4.IPEDSValue
                                      )
                            END AS Race
                           ,t3.IPEDSValue AS Gender
                           ,t3.IPEDSSequence AS GenderSequence
                           ,CASE WHEN t4.IPEDSValue IS NULL THEN 9
                                 ELSE t4.IPEDSSequence
                            END AS RaceSequence
                           ,t2.EnrollmentId
                  FROM      adGenders t3
                  LEFT JOIN arStudent t1 ON t3.GenderId = t1.Gender
                  LEFT JOIN adEthCodes t4 ON t4.EthCodeId = t1.Race
                  INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                  INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                  INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                               AND t6.SysStatusId NOT IN ( 7,8 )
                  INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                  INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                  INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                  LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                  INNER JOIN adCitizenships t12 ON t1.Citizen = t12.CitizenshipId
                  WHERE     t2.CampusId = @CampusId
                            AND (
                                  (
                                    t8.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
                                    AND @ProgId IS NULL
                                  )
                                  OR t8.ProgId IN ( SELECT  Val
                                                    FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                )
                            AND t6.SysStatusId IN ( 14 )
                            AND (
                                  t3.IPEDSValue = 30
                                  OR t3.IPEDSValue = 31
                                )
                            AND t1.Race IS NOT NULL
                            AND t2.ExpGradDate >= @StartDate
                            AND t2.ExpGradDate <= @EndDate
                            AND t12.IPEDSValue <> 65
                            AND t4.IPEDSValue IS NOT NULL
                ) dt
        ORDER BY RaceSequence
               ,CASE WHEN @OrderBy = 'SSN' THEN SSN
                END
               ,CASE WHEN @OrderBy = 'LastName' THEN StudentName
                END
               ,CASE WHEN @OrderBy = 'student number' THEN StudentNumber
                END
               ,CASE WHEN @OrderBy = 'enrollmentid' THEN EnrollmentId
                END;
    END;


GO
