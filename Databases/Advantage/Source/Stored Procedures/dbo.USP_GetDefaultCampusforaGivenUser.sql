SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[USP_GetDefaultCampusforaGivenUser]
	--Set up the parameters
    @UserID AS UNIQUEIDENTIFIER
AS
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;

--Get an Active default campus, if it is null then, try to get the top 1 active campus from the users roles 

        SELECT  ISNULL((
                         SELECT DISTINCT TOP 1
                                S.CampusID
                         FROM   sycampuses S
                         WHERE  S.CampusID = Syusers.CampusID
                                AND S.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
                                AND S.CampusID IN ( SELECT  SC.CampusId
                                                    FROM    dbo.syCmpGrpCmps SC
                                                           ,dbo.syUsersRolesCampGrps URC
                                                           ,dbo.syCampuses S
                                                    WHERE   URC.UserId = @UserID
                                                            AND SC.CampGrpId = URC.CampGrpId
                                                            AND S.CampusId = SC.CampusId
                                                            AND S.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' )
                       ),(
                           SELECT DISTINCT TOP 1
                                    SC.CampusId
                           FROM     dbo.syCmpGrpCmps SC
                                   ,dbo.syUsersRolesCampGrps URC
                                   ,dbo.syCampuses S
                           WHERE    URC.UserId = @UserID
                                    AND SC.CampGrpId = URC.CampGrpId
                                    AND S.CampusId = SC.CampusId
                                    AND S.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
                         )) CampusID
        FROM    dbo.syUsers
        WHERE   UserID = @UserID;

    END; 






GO
