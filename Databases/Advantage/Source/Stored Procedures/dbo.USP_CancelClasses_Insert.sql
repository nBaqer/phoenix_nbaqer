SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
 -----------------------------------------------------------------------------------------------------------
 --DE8278 QA: Copy class section not carrying over the periods. 
--Modified by Theresa G on 8/17/12 End
 -----------------------------------------------------------------------------------------------------------        
        
---------------------------------------------------------------------------------------------------------------------
--De8290  QA: Rescheduled class meeting inserts an empty record on the Enter class section with periods page.
--Aug-17 2012- The XML is case sensitive, the field names cases are matched according to the dataset
--Sara Start
--------------------------------------------------------------------------------------------------------------------------


CREATE PROCEDURE [dbo].[USP_CancelClasses_Insert] @RuleValues NTEXT
AS
    DECLARE @hDoc INT;
		 --Prepare input values as an XML document
    EXEC sp_xml_preparedocument @hDoc OUTPUT,@RuleValues;
 
	
		 -- Delete all records from bridge table based on GrdComponentTypeId
    BEGIN
        DELETE  FROM arUnschedClosures
        WHERE   ClsSectionId IN ( SELECT    ClsSectionId
                                  FROM      OPENXML(@hDoc,'/NewDataSet/SetupTestRules',1) 
							 WITH (ClsSectionId UNIQUEIDENTIFIER) )
                AND ClsMeetingId IN ( SELECT    ClsMeetingId
                                      FROM      OPENXML(@hDoc,'/NewDataSet/SetupTestRules',1) 
							 WITH (ClsMeetingId UNIQUEIDENTIFIER) )
                AND StartDate IN ( SELECT   StartDate
                                   FROM     OPENXML(@hDoc,'/NewDataSet/SetupTestRules',1) 
							 WITH (StartDate DATETIME) )
                AND EndDate IN ( SELECT EndDate
                                 FROM   OPENXML(@hDoc,'/NewDataSet/SetupTestRules',1) 
							 WITH (EndDate DATETIME) ); 
    END;
 
		
		-- Insert records into Bridge Table 
    INSERT  INTO arUnschedClosures
            SELECT DISTINCT
                    UnschedClosureId
                   ,StartDate
                   ,EndDate
                   ,ClsSectionId
                   ,ModUser
                   ,GETDATE() AS ModDate
                   ,ClsMeetingId
                   ,RescheduledMeetingId
                   ,ClassReSchduledFrom
                   ,ClassReSchduledTo
            FROM    OPENXML(@hDoc,'/NewDataSet/SetupTestRules',1) 
					 WITH (UnschedClosureId UNIQUEIDENTIFIER,StartDate DATETIME,EndDate DATETIME,ClsSectionId	UNIQUEIDENTIFIER,
					 ModUser VARCHAR(50),ModDate DATETIME,						
					 ClsMeetingId UNIQUEIDENTIFIER,RescheduledMeetingId UNIQUEIDENTIFIER,ClassReSchduledFrom DATETIME,ClassReSchduledTo DATETIME);
		   
    DECLARE @RoomId UNIQUEIDENTIFIER
       ,@PeriodId UNIQUEIDENTIFIER
       ,@RescheduledMeetingId UNIQUEIDENTIFIER;
    SET @RoomId = (
                    SELECT  COALESCE(RoomId,NULL)
                    FROM    OPENXML(@hDoc,'/NewDataSet/SetupTestRules',1) 
							 WITH (RoomId UNIQUEIDENTIFIER)
                  );
			
    SET @PeriodId = (
                      SELECT    COALESCE(PeriodId,NULL)
                      FROM      OPENXML(@hDoc,'/NewDataSet/SetupTestRules',1) 
							 WITH (PeriodId UNIQUEIDENTIFIER)
                    );
		
    SET @RescheduledMeetingId = (
                                  SELECT    RescheduledMeetingId
                                  FROM      OPENXML(@hDoc,'/NewDataSet/SetupTestRules',1) 
							 WITH (RescheduledMeetingId UNIQUEIDENTIFIER)
                                );
							  
		
		--Insert into arClsSectMeetings           
    IF @RescheduledMeetingId <> '00000000-0000-0000-0000-000000000000'
        BEGIN
            INSERT  INTO arClsSectMeetings
                    (
                     ClsSectMeetingId
                    ,RoomId
                    ,ClsSectionId
                    ,ModUser
                    ,ModDate
                    ,PeriodId
                    ,StartDate
                    ,EndDate
                    ,InstructionTypeID
                    ,IsMeetingRescheduled
					)
                    SELECT DISTINCT
                            RescheduledMeetingId AS ClsSectMeetingId
                           ,CASE WHEN ( @RoomId IS NULL ) THEN NULL
                                 ELSE RoomId
                            END AS RoomId
                           ,ClsSectionId
                           ,ModUser
                           ,GETDATE() AS ModDate
                           ,CASE WHEN ( @PeriodId IS NULL ) THEN NULL
                                 ELSE PeriodId
                            END AS PeriodId
                           ,reschedulestartdate AS StartDate
                           ,rescheduleenddate AS EndDate
                           ,instructiontypeid
                           ,ismeetingrescheduled
                    FROM    OPENXML(@hDoc,'/NewDataSet/SetupTestRules',1)   
					WITH (RescheduledMeetingId UNIQUEIDENTIFIER,
					RoomId VARCHAR(50),  
					ClsSectionId UNIQUEIDENTIFIER,ModUser VARCHAR(50),ModDate DATETIME,  
					PeriodId VARCHAR(50),
					reschedulestartdate DATETIME,rescheduleenddate DATETIME,
					instructiontypeid UNIQUEIDENTIFIER, ismeetingrescheduled BIT);
        END; 
    EXEC sp_xml_removedocument @hDoc;




GO
