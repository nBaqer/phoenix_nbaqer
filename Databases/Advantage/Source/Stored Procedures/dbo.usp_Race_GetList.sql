SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_Race_GetList]
    @EthCodeDescrip VARCHAR(50)
AS
    SELECT  EthCodeId
    FROM    adEthCodes
    WHERE   EthCodeDescrip LIKE @EthCodeDescrip + '%';



GO
