SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[Usp_TR_Sub11_ComponentsByCourse_GivenComponentTypeId]
--EXEC Usp_TR_Sub11_ComponentsByCourse_GivenComponentTypeId '2825E1F6-ADD1-4365-9E88-0028F7E9C567',NULL,NULL,'5C188D11-7C81-45E5-8280-7E1BE8D2CC12',NULL
    @StuEnrollIdList VARCHAR(MAX)
   ,@TermId VARCHAR(50) = NULL
   ,@SysComponentTypeIdList VARCHAR(50) = NULL
   ,@ReqId VARCHAR(50)
   ,@SysComponentTypeId VARCHAR(10) = NULL
AS
    DECLARE @StuEnrollCampusId UNIQUEIDENTIFIER;
    DECLARE @GradeBookAt VARCHAR(50);
    DECLARE @CharInt INT;
    DECLARE @Counter AS INT;  
    DECLARE @times AS INT;
	--DECLARE @TermStartDate1 AS DATETIME;     
    DECLARE @Score AS DECIMAL(18,2);
    DECLARE @GrdCompDescrip AS VARCHAR(50);


    DECLARE @curId AS UNIQUEIDENTIFIER;
    DECLARE @curReqId AS UNIQUEIDENTIFIER;
    DECLARE @curStuEnrollId AS UNIQUEIDENTIFIER;
    DECLARE @curDescrip AS VARCHAR(50);
    DECLARE @curNumber AS INT;
    DECLARE @curGrdComponentTypeId AS INT;
    DECLARE @curMinResult AS DECIMAL(18,2); 
    DECLARE @curGrdComponentDescription AS VARCHAR(50);   
    DECLARE @curClsSectionId AS UNIQUEIDENTIFIER;
    DECLARE @curTermId AS UNIQUEIDENTIFIER;
    DECLARE @curRownumber AS INT;
	 DECLARE @curRowSeq AS INT;
    DECLARE @DefaulSysComponentTypeIdList VARCHAR(MAX);
	
    SET @DefaulSysComponentTypeIdList = '501,544,502,499,503,500,533';
 
    CREATE TABLE #Temp1
        (
         Id UNIQUEIDENTIFIER
        ,TermId UNIQUEIDENTIFIER
        ,ReqId UNIQUEIDENTIFIER
        ,GradeBookDescription VARCHAR(50)
        ,Number INT
        ,GradeBookSysComponentTypeId INT
        ,GradeBookScore DECIMAL(18,2)
        ,MinResult DECIMAL(18,2)
        ,GradeComponentDescription VARCHAR(50)
        ,ClsSectionId UNIQUEIDENTIFIER
        ,StuEnrollId UNIQUEIDENTIFIER
        ,RowNumber INT
		,Seq int
        );
    CREATE TABLE #Temp2
        (
         ReqId UNIQUEIDENTIFIER
        ,EffectiveDate DATETIME
        );

    IF @SysComponentTypeIdList IS NULL
        BEGIN
            SET @SysComponentTypeIdList = @DefaulSysComponentTypeIdList;
        END;

    SET @StuEnrollCampusId = COALESCE((
                                        SELECT TOP 1
                                                ASE.CampusId
                                        FROM    arStuEnrollments AS ASE
                                        WHERE   ASE.StuEnrollId IN ( SELECT Val
                                                                     FROM   MultipleValuesForReportParameters(@StuEnrollIdList,',',1) )
                                      ),NULL);
    SET @GradeBookAt = ( dbo.GetAppSettingValueByKeyName('GradeBookWeightingLevel',@StuEnrollCampusId) );     

    IF ( @GradeBookAt IS NOT NULL )
        BEGIN
            SET @GradeBookAt = LOWER(LTRIM(RTRIM(@GradeBookAt)));
        END;

    SET @CharInt = (
                     SELECT CHARINDEX(@SysComponentTypeId,@SysComponentTypeIdList)
                   );

    
    IF @GradeBookAt = 'instructorlevel'
        BEGIN
            SELECT  CASE WHEN GradeBookDescription IS NULL THEN GradeComponentDescription
                         ELSE GradeBookDescription
                    END AS GradeBookDescription
                   ,MinResult
                   ,GradeBookScore
                   ,GradeComponentDescription
                   ,GradeBookSysComponentTypeId
                   ,StuEnrollId
                   ,rownumber
                   ,TermId
                   ,ReqId
            FROM    (
                      SELECT    AR2.ReqId
                               ,AT.TermId
                               ,CASE WHEN AGBWD.Descrip IS NULL THEN AGCT.Descrip
                                     ELSE AGBWD.Descrip
                                END AS GradeBookDescription
                               ,( CASE WHEN AGCT.SysComponentTypeId IN ( 500,503,544 ) THEN AGBWD.Number
                                       ELSE (
                                              SELECT    MIN(MinVal)
                                              FROM      arGradeScaleDetails GSD
                                                       ,arGradeSystemDetails GSS
                                              WHERE     GSD.GrdSysDetailId = GSS.GrdSysDetailId
                                                        AND GSS.IsPass = 1
                                                        AND GSD.GrdScaleId = ACS.GrdScaleId
                                            )
                                  END ) AS MinResult
                               ,AGBWD.Required
                               ,AGBWD.MustPass
                               ,ISNULL(AGCT.SysComponentTypeId,0) AS GradeBookSysComponentTypeId
                               ,AGBWD.Number
                               ,SR.Resource AS GradeComponentDescription
                               ,AGBWD.InstrGrdBkWgtDetailId
                               ,ASE.StuEnrollId
                               ,0 AS IsExternShip
                               ,CASE AGCT.SysComponentTypeId
                                  WHEN 544 THEN (
                                                  SELECT    SUM(ISNULL(AEA.HoursAttended,0.0))
                                                  FROM      arExternshipAttendance AS AEA
                                                  WHERE     AEA.StuEnrollId = ASE.StuEnrollId
                                                )
                                  ELSE (
                                         SELECT SUM(ISNULL(AGBR1.Score,0.0))
                                         FROM   arGrdBkResults AS AGBR1
                                         WHERE  AGBR1.StuEnrollId = ASE.StuEnrollId
                                                AND AGBR1.InstrGrdBkWgtDetailId = AGBWD.InstrGrdBkWgtDetailId
                                                AND AGBR1.ClsSectionId = ACS.ClsSectionId
                                       )
                                END AS GradeBookScore,
								AGBWD.Seq
                               ,ROW_NUMBER() OVER ( PARTITION BY ASE.StuEnrollId,AT.TermId,AR2.ReqId ORDER BY AGCT.SysComponentTypeId, AGBWD.Descrip ) AS rownumber
                      FROM      arGrdBkWgtDetails AS AGBWD
                      INNER JOIN arGrdBkResults AS AGBR ON AGBR.InstrGrdBkWgtDetailId = AGBWD.InstrGrdBkWgtDetailId 
							--INNER JOIN arGrdBkWeights AS AGBW ON AGBW.InstrGrdBkWgtId = AGBWD.InstrGrdBkWgtId
                      INNER JOIN arClassSections AS ACS ON ACS.ClsSectionId = AGBR.ClsSectionId
                      INNER JOIN arResults AS AR ON AR.TestId = ACS.ClsSectionId
                      INNER JOIN arGrdComponentTypes AS AGCT ON AGCT.GrdComponentTypeId = AGBWD.GrdComponentTypeId
                      INNER JOIN arStuEnrollments AS ASE ON ASE.StuEnrollId = AR.StuEnrollId
                      INNER JOIN arTerm AS AT ON AT.TermId = ACS.TermId
                      INNER JOIN arReqs AS AR2 ON AR2.ReqId = ACS.ReqId
                      INNER JOIN syResources AS SR ON SR.ResourceID = AGCT.SysComponentTypeId
                      WHERE     AR.StuEnrollId IN ( SELECT  Val
                                                    FROM    MultipleValuesForReportParameters(@StuEnrollIdList,',',1) )
                                AND AT.TermId = @TermId
                                AND AR2.ReqId = @ReqId
                                AND AGCT.SysComponentTypeId IN ( SELECT Val
                                                                 FROM   MultipleValuesForReportParameters(@SysComponentTypeIdList,',',1) )
                      UNION
                      SELECT    AR2.ReqId
                               ,AT.TermId
                               ,CASE WHEN AGBW.Descrip IS NULL THEN (
                                                                      SELECT    Resource
                                                                      FROM      syResources
                                                                      WHERE     ResourceID = AGCT.SysComponentTypeId
                                                                    )
                                     ELSE AGBW.Descrip
                                END AS GradeBookDescription
                               ,CASE WHEN AGCT.SysComponentTypeId IN ( 500,503,544 ) THEN AGBWD.Number
                                     ELSE (
                                            SELECT  MIN(MinVal)
                                            FROM    arGradeScaleDetails GSD
                                                   ,arGradeSystemDetails GSS
                                            WHERE   GSD.GrdSysDetailId = GSS.GrdSysDetailId
                                                    AND GSS.IsPass = 1
                                                    AND GSD.GrdScaleId = ACS.GrdScaleId
                                          )
                                END AS MinResult
                               ,AGBWD.Required
                               ,AGBWD.MustPass
                               ,ISNULL(AGCT.SysComponentTypeId,0) AS GradeBookSysComponentTypeId
                               ,AGBWD.Number
                               ,(
                                  SELECT    Resource
                                  FROM      syResources
                                  WHERE     ResourceID = AGCT.SysComponentTypeId
                                ) AS GradeComponentDescription
                               ,AGBWD.InstrGrdBkWgtDetailId
                               ,ASE.StuEnrollId
                               ,0 AS IsExternShip
                               ,CASE AGCT.SysComponentTypeId
                                  WHEN 544 THEN (
                                                  SELECT    SUM(ISNULL(AEA.HoursAttended,0.0))
                                                  FROM      arExternshipAttendance AS AEA
                                                  WHERE     AEA.StuEnrollId = ASE.StuEnrollId
                                                )
                                  ELSE (
                                         SELECT SUM(ISNULL(AGBR.Score,0.0))
                                         FROM   arGrdBkResults AS AGBR
                                         WHERE  AGBR.StuEnrollId = ASE.StuEnrollId
                                                AND AGBR.InstrGrdBkWgtDetailId = AGBWD.InstrGrdBkWgtDetailId
                                                AND AGBR.ClsSectionId = ACS.ClsSectionId
                                       )
                                END AS GradeBookScore
								, AGBWD.Seq
                               ,ROW_NUMBER() OVER ( PARTITION BY ASE.StuEnrollId,AT.TermId,AR2.ReqId ORDER BY AGCT.SysComponentTypeId, AGCT.Descrip ) AS rownumber
                      FROM      arGrdBkConversionResults GBCR
                      INNER JOIN arStuEnrollments AS ASE ON ASE.StuEnrollId = GBCR.StuEnrollId
                      INNER JOIN arStudent AS AST ON AST.StudentId = ASE.StudentId
                      INNER JOIN arPrgVersions AS APV ON APV.PrgVerId = ASE.PrgVerId
                      INNER JOIN arTerm AS AT ON AT.TermId = GBCR.TermId
                      INNER JOIN arReqs AS AR2 ON AR2.ReqId = GBCR.ReqId
                      INNER JOIN arGrdComponentTypes AS AGCT ON AGCT.GrdComponentTypeId = GBCR.GrdComponentTypeId
                      INNER JOIN arGrdBkWgtDetails AS AGBWD ON AGBWD.GrdComponentTypeId = AGCT.GrdComponentTypeId
                                                               AND AGBWD.GrdComponentTypeId = GBCR.GrdComponentTypeId
                      INNER JOIN arGrdBkWeights AS AGBW ON AGBW.InstrGrdBkWgtId = AGBWD.InstrGrdBkWgtId
                                                           AND AGBW.ReqId = GBCR.ReqId
                      INNER JOIN (
                                   SELECT   AGBW.ReqId
                                           ,MAX(AGBW.EffectiveDate) AS EffectiveDate
                                   FROM     arGrdBkWeights AS AGBW
                                   GROUP BY AGBW.ReqId
                                 ) AS MaxEffectiveDatesByCourse ON GBCR.ReqId = MaxEffectiveDatesByCourse.ReqId
                      INNER JOIN (
                                   SELECT   SR.Resource
                                           ,SR.ResourceID
                                   FROM     syResources AS SR
                                   WHERE    SR.ResourceTypeID = 10
                                 ) SYRES ON SYRES.ResourceID = AGCT.SysComponentTypeId
                      INNER JOIN syCampuses AS SC ON SC.CampusId = ASE.CampusId
                      INNER JOIN arClassSections AS ACS ON ACS.TermId = AT.TermId
                                                           AND ACS.ReqId = AR2.ReqId
                      LEFT JOIN arGrdBkResults AS AGBR ON AGBR.StuEnrollId = ASE.StuEnrollId
                                                          AND AGBR.InstrGrdBkWgtDetailId = AGBWD.InstrGrdBkWgtDetailId
                                                          AND AGBR.ClsSectionId = ACS.ClsSectionId
                      WHERE     MaxEffectiveDatesByCourse.EffectiveDate <= AT.StartDate
                                AND ASE.StuEnrollId IN ( SELECT Val
                                                         FROM   MultipleValuesForReportParameters(@StuEnrollIdList,',',1) )
                                AND AT.TermId = @TermId
                                AND AR2.ReqId = @ReqId
                                AND AGCT.SysComponentTypeId IN ( SELECT Val
                                                                 FROM   MultipleValuesForReportParameters(@SysComponentTypeIdList,',',1) )
                    ) dt
            WHERE   (
                      @SysComponentTypeId IS NULL
                      OR dt.GradeBookSysComponentTypeId = @SysComponentTypeId
                    )
            ORDER BY dt.Seq,
			 GradeComponentDescription
                   ,rownumber
                   ,GradeBookDescription; 
        END;
    ELSE
        BEGIN


            SELECT  AGBWD.InstrGrdBkWgtDetailId
                   ,AGBWD.InstrGrdBkWgtId
                   ,AGBWD.Code
                   ,AGBWD.Descrip
                   ,AGBWD.Weight
                   ,AGBWD.Seq
                   ,AGBWD.ModUser
                   ,AGBWD.ModDate
                   ,AGBWD.GrdComponentTypeId
                   ,AGBWD.Parameter
                   ,AGBWD.Number
                   ,AGBWD.GrdPolicyId
                   ,AGBWD.Required
                   ,AGBWD.MustPass
                   ,AGBWD.CreditsPerService
            INTO    #tmpInnerQueryTable
            FROM    arGrdBkWgtDetails AS AGBWD
            INNER JOIN arGrdBkResults AS AGBR ON AGBR.InstrGrdBkWgtDetailId = AGBWD.InstrGrdBkWgtDetailId
            INNER JOIN arClassSections AS ACS ON ACS.ClsSectionId = AGBR.ClsSectionId
            WHERE   AGBR.StuEnrollId IN ( SELECT    Val
                                          FROM      MultipleValuesForReportParameters(@StuEnrollIdList,',',1) )
                    AND ACS.ReqId = @ReqId;

            SELECT  ReqId
                   ,MAX(EffectiveDate) AS EffectiveDate
            INTO    #tmpGrdBkWeightsWithMaxEffectiveDates
            FROM    arGrdBkWeights
            GROUP BY ReqId;
                                        --HAVING ReqId=@ReqId

			--SET @TermStartDate1 = (
			--                       SELECT   AT.StartDate
			--                       FROM     arTerm AS AT
			--                       WHERE    AT.TermId = @TermId
			--                      );
            INSERT  INTO #Temp2
                    SELECT  AGBW.ReqId
                           ,MAX(AGBW.EffectiveDate) AS EffectiveDate
                    FROM    arGrdBkWeights AS AGBW
                    INNER JOIN syCreditSummary AS SCS ON SCS.ReqId = AGBW.ReqId
                    WHERE   SCS.StuEnrollId IN ( SELECT Val
                                                 FROM   MultipleValuesForReportParameters(@StuEnrollIdList,',',1) ) 
				   -- AND SCS.TermId = @TermId
				   -- AND AGBW.EffectiveDate <= @TermStartDate1
                            AND SCS.ReqId = @ReqId
                    GROUP BY AGBW.ReqId;

            DECLARE getUsers_Cursor CURSOR FAST_FORWARD FORWARD_ONLY
            FOR
                SELECT  dt.ID
                       ,dt.ReqId
                       ,dt.Descrip
                       ,dt.Number
                       ,dt.SysComponentTypeId
                       ,dt.MinResult
                       ,dt.GradeComponentDescription
                       ,dt.ClsSectionId
                       ,dt.StuEnrollId
                       ,dt.TermId
                       ,ROW_NUMBER() OVER ( PARTITION BY dt.StuEnrollId,dt.TermId,dt.SysComponentTypeId ORDER BY dt.SysComponentTypeId, dt.Descrip ) AS rownumber
					   ,dt.seq
                FROM    (
                          SELECT DISTINCT
                                    ISNULL(GD.InstrGrdBkWgtDetailId,NEWID()) AS ID
                                   ,AR.ReqId
                                   ,GC.Descrip
                                   ,GD.Number
                                   ,GC.SysComponentTypeId
                                   ,( CASE WHEN GC.SysComponentTypeId IN ( 500,503,544 ) THEN GD.Number
                                           ELSE (
                                                  SELECT    MIN(MinVal)
                                                  FROM      arGradeScaleDetails GSD
                                                           ,arGradeSystemDetails GSS
                                                  WHERE     GSD.GrdSysDetailId = GSS.GrdSysDetailId
                                                            AND GSS.IsPass = 1
                                                            AND GSD.GrdScaleId = CS.GrdScaleId
                                                )
                                      END ) AS MinResult
                                   ,S.Resource AS GradeComponentDescription
                                   ,CS.ClsSectionId
                                   ,RES.StuEnrollId
                                   ,T.TermId
								   ,GD.Seq
                          FROM      arGrdComponentTypes AS GC
                          INNER JOIN #tmpInnerQueryTable AS GD ON GD.GrdComponentTypeId = GC.GrdComponentTypeId
                          INNER JOIN arGrdBkWeights AS AGBW1 ON AGBW1.InstrGrdBkWgtId = GD.InstrGrdBkWgtId
                          INNER JOIN arReqs AS AR ON AR.ReqId = AGBW1.ReqId
                          INNER JOIN arClassSections AS CS ON CS.ReqId = AR.ReqId
                          INNER JOIN syResources AS S ON S.ResourceID = GC.SysComponentTypeId
                          INNER JOIN arResults AS RES ON RES.TestId = CS.ClsSectionId
                          INNER JOIN arTerm AS T ON T.TermId = CS.TermId
                          WHERE     RES.StuEnrollId IN ( SELECT Val
                                                         FROM   MultipleValuesForReportParameters(@StuEnrollIdList,',',1) )
                                    AND GD.Number > 0
                                    AND AR.ReqId = @ReqId
                        ) dt
                ORDER BY SysComponentTypeId
                       ,rownumber;
            OPEN getUsers_Cursor;
            FETCH NEXT FROM getUsers_Cursor
					INTO @curId,@curReqId,@curDescrip,@curNumber,@curGrdComponentTypeId,@curMinResult,@curGrdComponentDescription,@curClsSectionId,
                @curStuEnrollId,@curTermId,@curRownumber, @curRowSeq;
            SET @Counter = 0;
            WHILE @@FETCH_STATUS = 0
                BEGIN
					--PRINT @Number;
                    SET @times = 1;
                    WHILE @times <= @curNumber
                        BEGIN
							--PRINT @times;
                            IF @curNumber > 1
                                BEGIN
                                    SET @GrdCompDescrip = @curDescrip + CAST(@times AS CHAR);
                                    IF @curGrdComponentTypeId = 544
                                        BEGIN
                                            SET @Score = (
                                                           SELECT   SUM(ISNULL(AEA.HoursAttended,0.0))
                                                           FROM     arExternshipAttendance AS AEA
                                                           WHERE    AEA.StuEnrollId = @curStuEnrollId
                                                         );
                                        END;
                                    ELSE
                                        BEGIN
                                            SET @Score = (
                                                           SELECT   SUM(ISNULL(AGBR.Score,0.0))
                                                           FROM     arGrdBkResults AS AGBR
                                                           WHERE    AGBR.StuEnrollId = @curStuEnrollId
                                                                    AND AGBR.InstrGrdBkWgtDetailId = @curId
                                                                    AND AGBR.ResNum = @times
                                                                    AND AGBR.ClsSectionId = @curClsSectionId
                                                         );
                                        END;
                                    SET @curRownumber = @times;
                                END;
                            ELSE
                                BEGIN
                                    SET @GrdCompDescrip = @curDescrip;
                                    SET @Score = (
                                                   SELECT TOP 1
                                                            Score
                                                   FROM     arGrdBkResults
                                                   WHERE    StuEnrollId = @curStuEnrollId
                                                            AND InstrGrdBkWgtDetailId = @curId
                                                            AND ResNum = @times
                                                 );
                                    IF @Score IS NULL
                                        BEGIN
                                            SET @Score = (
                                                           SELECT TOP 1
                                                                    Score
                                                           FROM     arGrdBkResults
                                                           WHERE    StuEnrollId = @curStuEnrollId
                                                                    AND InstrGrdBkWgtDetailId = @curId
                                                                    AND ResNum = ( @times - 1 )
                                                         ); 
                                        END;
                                    IF @curGrdComponentTypeId = 544
                                        BEGIN
                                            SET @Score = (
                                                           SELECT   SUM(ISNULL(AEA.HoursAttended,0.0))
                                                           FROM     arExternshipAttendance AS AEA
                                                           WHERE    AEA.StuEnrollId = @curStuEnrollId
                                                         );
                                        END;
                                    ELSE
                                        BEGIN
                                            SET @Score = (
                                                           SELECT   SUM(ISNULL(AGBR.Score,0.0))
                                                           FROM     arGrdBkResults AS AGBR
                                                           WHERE    AGBR.StuEnrollId = @curStuEnrollId
                                                                    AND AGBR.InstrGrdBkWgtDetailId = @curId
                                                                    AND AGBR.ResNum = @times
                                                         );
                                        END;
                                END;
							--PRINT @Score;
                            INSERT  INTO #Temp1
                            VALUES  ( @curId,@curTermId,@curReqId,@GrdCompDescrip,@curNumber,@curGrdComponentTypeId,@Score,@curMinResult,
                                      @curGrdComponentDescription,@curClsSectionId,@curStuEnrollId,@curRownumber, @curRowSeq );
							
                            SET @times = @times + 1;
                        END;

                    FETCH NEXT FROM getUsers_Cursor
					INTO @curId,@curReqId,@curDescrip,@curNumber,@curGrdComponentTypeId,@curMinResult,@curGrdComponentDescription,@curClsSectionId,
                        @curStuEnrollId,@curTermId,@curRownumber, @curRowSeq;
                END;
            CLOSE getUsers_Cursor; 
            DEALLOCATE getUsers_Cursor; 

            SET @CharInt = (
                             SELECT CHARINDEX(@SysComponentTypeId,@SysComponentTypeIdList)
                           );
		
            IF ( @CharInt >= 1 )
                BEGIN
                    SELECT  GradeBookDescription
                           ,MinResult
                           ,GradeBookScore
                           ,GradeComponentDescription
                           ,GradeBookSysComponentTypeId
                           ,StuEnrollId
                           ,RowNumber
                           ,TermId
                           ,ReqId
						   ,seq
                          --   Id  , Number, ClsSectionId
                    FROM    #Temp1
                    WHERE   GradeBookSysComponentTypeId = @SysComponentTypeId
                    UNION
                    SELECT  CASE WHEN GBWD.Descrip IS NULL THEN GCT.Descrip
                                 ELSE GBWD.Descrip
                            END AS GradeBookDescription
                           ,GBCR.MinResult AS MinResult
                           ,GBCR.Score AS GradeBookScore
                           ,SYRES.Resource AS GradeComponentDescription
                           ,ISNULL(GCT.SysComponentTypeId,0) AS GradeBookSysComponentTypeId
                           ,SE.StuEnrollId
                           ,ROW_NUMBER() OVER ( PARTITION BY T.TermId,GCT.SysComponentTypeId ORDER BY GCT.SysComponentTypeId, GCT.Descrip ) AS rownumber
                           ,T.TermId
                           ,R.ReqId
						   ,GBWD.Seq
                         --, GBWD.InstrGrdBkWgtDetailId, GBWD.Number, SYRES.Resource
                          --, (
                          --   SELECT TOP 1
                          --          ClsSectionId
                          --   FROM   arClassSections
                          --   WHERE  TermId = T.TermId
                          --          AND ReqId = R.ReqId
                          --  ) AS ClsSectionId
                    FROM    arGrdBkConversionResults GBCR
                    INNER JOIN arStuEnrollments SE ON GBCR.StuEnrollId = SE.StuEnrollId
                    INNER JOIN arStudent S ON S.StudentId = SE.StudentId
                    INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                    INNER JOIN arTerm T ON GBCR.TermId = T.TermId
                    INNER JOIN arReqs R ON GBCR.ReqId = R.ReqId
                    INNER JOIN arGrdComponentTypes GCT ON GCT.GrdComponentTypeId = GBCR.GrdComponentTypeId
                    INNER JOIN arGrdBkWgtDetails GBWD ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
                                                         AND GBWD.GrdComponentTypeId = GBCR.GrdComponentTypeId
							--INNER JOIN arGrdBkWeights GBW ON GBWD.InstrGrdBkWgtId = GBW.InstrGrdBkWgtId
							--                                 AND GBCR.ReqId = GBW.ReqId
                    INNER JOIN #tmpGrdBkWeightsWithMaxEffectiveDates AS MaxEffectiveDatesByCourse ON GBCR.ReqId = MaxEffectiveDatesByCourse.ReqId
                    INNER JOIN (
                                 SELECT Resource
                                       ,ResourceID
                                 FROM   syResources
                                 WHERE  ResourceTypeID = 10
                               ) SYRES ON SYRES.ResourceID = GCT.SysComponentTypeId
                    INNER JOIN syCampuses C ON SE.CampusId = C.CampusId
                    WHERE   MaxEffectiveDatesByCourse.EffectiveDate <= T.StartDate
                            AND SE.StuEnrollId IN ( SELECT  Val
                                                    FROM    MultipleValuesForReportParameters(@StuEnrollIdList,',',1) )
                            AND T.TermId = @TermId
                            AND R.ReqId = @ReqId
                            AND GCT.SysComponentTypeId IN ( SELECT  Val
                                                            FROM    MultipleValuesForReportParameters(@SysComponentTypeIdList,',',1) )
                            AND (
                                  @SysComponentTypeId IS NULL
                                  OR GCT.SysComponentTypeId = @SysComponentTypeId
                                )
                    ORDER BY seq,
					GradeBookSysComponentTypeId
                           ,GradeBookDescription
                           ,rownumber; 
                END; 
            DROP TABLE #tmpInnerQueryTable;
        END;
    DROP TABLE #Temp2;
    DROP TABLE #Temp1;
	


GO
