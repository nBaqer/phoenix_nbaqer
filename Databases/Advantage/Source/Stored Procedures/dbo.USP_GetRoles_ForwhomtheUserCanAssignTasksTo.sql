SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[USP_GetRoles_ForwhomtheUserCanAssignTasksTo] ( @UserID AS VARCHAR(50) )
AS
    SET NOCOUNT ON;
    BEGIN 
        SELECT DISTINCT
                TM.RoleId
               ,Role
        FROM    dbo.tmPermissions TM
               ,syroles SR
        WHERE   TM.RoleId = SR.RoleId
                AND UserID = @UserID;

    END;




GO
