SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[USP_GetUserIdFromUserName] @UserName VARCHAR(50)
AS
    SELECT  UserID
    FROM    syusers
    WHERE   UserName = @UserName;




GO
