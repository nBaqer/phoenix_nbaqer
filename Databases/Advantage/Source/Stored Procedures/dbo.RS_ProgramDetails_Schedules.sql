SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RS_ProgramDetails_Schedules]
	-- Add the parameters for the stored procedure here
    @PrgVerId UNIQUEIDENTIFIER = '5FEF3CF7-5B12-4942-A1F1-8B87E01997C9' -- Electrolysis (historical)
    --,@CampusId UNIQUEIDENTIFIER = 'DC42A60A-5EB9-49B6-ADF6-535966F2E34A' -- Hieleach
AS
    BEGIN
        SET NOCOUNT ON;

 -- Select Schedules in program versions
        SELECT  Descrip AS Schedules
        FROM    arProgSchedules
        WHERE   PrgVerId = @PrgVerId
                AND Active = 1
        ORDER BY Descrip;
    END;



GO
