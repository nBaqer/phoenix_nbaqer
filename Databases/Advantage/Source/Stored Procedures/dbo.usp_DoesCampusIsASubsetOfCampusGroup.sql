SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_DoesCampusIsASubsetOfCampusGroup]
    (
     @campGrpId UNIQUEIDENTIFIER
    ,@campusId UNIQUEIDENTIFIER
    ,@prgVerId UNIQUEIDENTIFIER
    )
AS
    SET NOCOUNT ON;
    DECLARE @StudentCnt INTEGER
       ,@rtn BIT;

--SET @campGrpId= '2ac97fc1-bb9b-450a-aa84-7c503c90a1eb'
--SET @campusId='D4D42C06-8712-4C86-A8E1-9575C8F19186'
--SET @prgVerId='95C6CB09-29B4-4396-952A-09BB2B04CF65'
    SET @StudentCnt = (
                        SELECT  COUNT(*)
                        FROM    dbo.arStuEnrollments
                        WHERE   prgverid = @prgVerId
                      );
    SET @rtn = 1;
    IF @StudentCnt > 0
        BEGIN
            SELECT  @rtn = COUNT(*)
            FROM    syCmpGrpCmps t1
                   ,syCampGrps t2
            WHERE   t1.CampGrpId = t2.CampGrpId
                    AND t1.CampusId IN ( SELECT DISTINCT
                                                t3.CampusId
                                         FROM   syCmpGrpCmps t3
                                               ,syCampuses t4
                                         WHERE  t3.CampGrpId = @campGrpId
                                                AND t3.CampusId = t4.CampusId
                                                AND t4.StatusId = (
                                                                    SELECT  StatusId
                                                                    FROM    dbo.syStatuses
                                                                    WHERE   Status = 'Active'
                                                                  ) )
                    AND t1.CampusId = @campusId;
            SELECT  @rtn;
        END;
    ELSE
        SELECT  @rtn;



GO
