SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_dictationspeedtestreport_getlist]
    @termid VARCHAR(MAX) = NULL
   ,@testtype VARCHAR(MAX) = NULL
   ,@campgrpid VARCHAR(MAX) = NULL
   ,@prgverid VARCHAR(MAX) = NULL
   ,@statuscodeid VARCHAR(MAX) = NULL
   ,@studentid VARCHAR(8000) = NULL
   ,
--- Code changes made by Vijay Ramteke on 01/07/2010 to resolve mantis issue id 19264
    @stuenrollid VARCHAR(8000) = NULL
--------
AS
    DECLARE @hDocTerm INT
       ,@hDocgrdComponentTypeId INT
       ,@hDocCampGrpid INT
       ,@hDocPrgVerId INT
       ,@hDocStatusCodeId INT;
    
    IF ISNULL(@studentid,'') = ''
        BEGIN
    
    --Prepare input values as an XML document
            EXEC sp_xml_preparedocument @hDocTerm OUTPUT,@termid;
            EXEC sp_xml_preparedocument @hDocgrdComponentTypeId OUTPUT,@testtype;
            EXEC sp_xml_preparedocument @hDocCampGrpid OUTPUT,@campgrpid;
            EXEC sp_xml_preparedocument @hDocPrgVerId OUTPUT,@prgverid;
            EXEC sp_xml_preparedocument @hDocStatusCodeId OUTPUT,@statuscodeid;
            SELECT DISTINCT
                    t3.TermDescrip
                   ,t3.StartDate
                   ,t3.EndDate
                   ,t6.StudentId
                   ,t6.FirstName
                   ,t6.LastName
                   ,t6.MiddleName
                   ,t6.SSN
                   ,t6.StudentNumber
                   ,t2.EnrollmentId
                   ,t4.Descrip
                   ,t1.Speed
                   ,t1.Accuracy
                   ,t1.DatePassed
                   ,t5.FullName AS InstructorName
                   ,t7.PrgVerDescrip
                   ,t10.campdescrip AS campusdescrip
                   ,t9.campgrpdescrip
                   ,t6.FirstName + ' ' + t6.LastName AS StudentName
                   ,NULL AS StudentIdentifier
                   ,NULL AS StudentIdentifierCaption
                   ,(
                      SELECT TOP 1
                                Address1
                      FROM      arStudAddresses
                      WHERE     StudentId = t6.StudentId
                                AND default1 = 1
                    ) AS Address1
                   ,(
                      SELECT TOP 1
                                City
                      FROM      arStudAddresses
                      WHERE     StudentId = t6.StudentId
                                AND default1 = 1
                    ) AS City
                   ,(
                      SELECT TOP 1
                                StateDescrip
                      FROM      arStudAddresses
                               ,syStates
                      WHERE     arStudAddresses.StateId = syStates.StateId
                                AND arStudAddresses.StudentId = t6.StudentId
                    ) AS StateDescrip
                   ,(
                      SELECT TOP 1
                                Phone
                      FROM      arStudentPhone
                      WHERE     arStudentPhone.StudentId = t6.StudentId
                    ) AS PhoneNumber
                   ,(
                      SELECT TOP 1
                                Zip
                      FROM      arStudAddresses
                      WHERE     StudentId = t6.StudentId
                                AND default1 = 1
                    ) AS Zip
                   ,(
                      SELECT TOP 1
                                ForeignPhone
                      FROM      arStudentPhone
                      WHERE     arStudentPhone.StudentId = t6.StudentId
                    ) AS ForeignPhone
                   ,(
                      SELECT TOP 1
                                ForeignZip
                      FROM      arStudAddresses
                      WHERE     arStudAddresses.StudentId = t6.StudentId
                                AND default1 = 1
                    ) AS ForeignZip
                   ,(
                      SELECT TOP 1
                                Grade
                      FROM      arGradeSystemDetails
                      WHERE     GrdSysDetailId = t1.GrdSysDetailId
                    ) AS Grade
                   ,t4.GrdComponentTypeId
        -- Code changes by Vijay Ramteke on 28 May 2010 for mantis issue id 18848
                   ,t1.istestmentorproctored 
        ----
            FROM    arPostScoreDictationSpeedTest t1
            INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
            INNER JOIN arTerm t3 ON t1.TermId = t3.TermId
            INNER JOIN (
                         SELECT *
                         FROM   arGrdComponentTypes
                         WHERE  SysComponentTypeId = 612
                       ) t4 ON t1.GrdComponentTypeId = t4.GrdComponentTypeId
            INNER JOIN syUsers t5 ON t5.UserId = t1.InstructorId
            INNER JOIN arStudent t6 ON t2.StudentId = t6.StudentId
            INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
            INNER JOIN syCmpGrpCmps t8 ON t2.campusid = t8.campusid --and (t3.campgrpid=t8.campgrpid) -- or t3.CampGrpId = (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL'))
            INNER JOIN syCampGrps t9 ON t8.campgrpid = t9.campgrpid
            INNER JOIN sycampuses t10 ON t2.campusid = t10.campusid
            WHERE   (
                      @termid IS NULL
                      OR t1.TermId IN ( SELECT  termid
                                        FROM    OPENXML(@hDocTerm,'/NewDataSet/Terms',1) 
                                                WITH (termid VARCHAR(50)) )
                    )
                    AND (
                          @testtype IS NULL
                          OR t1.GrdComponentTypeId IN ( SELECT  grdComponentTypeId
                                                        FROM    OPENXML(@hDocgrdComponentTypeId,'/NewDataSet/GradeComponentTypes',1) 
                            WITH (grdComponentTypeId VARCHAR(50)) )
                        )
                    AND (
                          @campgrpid IS NULL
                          OR t8.campgrpid IN ( SELECT   campgrpid
                                               FROM     OPENXML(@hDocCampGrpid,'/NewDataSet/CampusGroups',1) 
                                                        WITH (campgrpid VARCHAR(50)) )
                        )
                    AND (
                          @prgverid IS NULL
                          OR t2.prgverid IN ( SELECT    prgverid
                                              FROM      OPENXML(@hDocPrgVerId,'/NewDataSet/ProgramVersion',1) 
                                                        WITH (prgverid VARCHAR(50)) )
                        )
                    AND (
                          @statuscodeid IS NULL
                          OR t2.statuscodeid IN ( SELECT    statuscodeid
                                                  FROM      OPENXML(@hDocStatusCodeId,'/NewDataSet/statuscodes',1) 
                                                        WITH (statuscodeid VARCHAR(50)) )
                        )
                    AND (
                          @studentid IS NULL
                          OR t6.StudentId = @studentid
                        )
        --- Code changes made by Vijay Ramteke on 01/07/2010 to resolve mantis issue id 19264 
                    AND (
                          @stuenrollid IS NULL
                          OR t2.StuEnrollId = @stuenrollid
                        )
        ---------------
            ORDER BY t3.StartDate
                   ,t3.termDescrip;

            EXEC sp_xml_removedocument @hDocTerm;
            EXEC sp_xml_removedocument @hDocgrdComponentTypeId;
            EXEC sp_xml_removedocument @hDocCampGrpid;
            EXEC sp_xml_removedocument @hDocPrgVerId;
            EXEC sp_xml_removedocument @hDocStatusCodeId;
        END;
    ELSE
        BEGIN
            SELECT DISTINCT
                    t3.TermDescrip
                   ,t3.StartDate
                   ,t3.EndDate
                   ,t6.StudentId
                   ,t6.FirstName
                   ,t6.LastName
                   ,t6.MiddleName
                   ,t6.SSN
                   ,t6.StudentNumber
                   ,t2.EnrollmentId
                   ,t4.Descrip
                   ,t1.Speed
                   ,t1.Accuracy
                   ,t1.DatePassed
                   ,t5.FullName AS InstructorName
                   ,t7.PrgVerDescrip
                   ,t10.campdescrip AS campusdescrip
                   ,t9.campgrpdescrip
                   ,t6.FirstName + ' ' + t6.LastName AS StudentName
                   ,NULL AS StudentIdentifier
                   ,NULL AS StudentIdentifierCaption
                   ,(
                      SELECT TOP 1
                                Address1
                      FROM      arStudAddresses
                      WHERE     StudentId = t6.StudentId
                                AND default1 = 1
                    ) AS Address1
                   ,(
                      SELECT TOP 1
                                City
                      FROM      arStudAddresses
                      WHERE     StudentId = t6.StudentId
                                AND default1 = 1
                    ) AS City
                   ,(
                      SELECT TOP 1
                                StateDescrip
                      FROM      arStudAddresses
                               ,syStates
                      WHERE     arStudAddresses.StateId = syStates.StateId
                                AND arStudAddresses.StudentId = t6.StudentId
                    ) AS StateDescrip
                   ,(
                      SELECT TOP 1
                                Phone
                      FROM      arStudentPhone
                      WHERE     arStudentPhone.StudentId = t6.StudentId
                    ) AS PhoneNumber
                   ,(
                      SELECT TOP 1
                                Zip
                      FROM      arStudAddresses
                      WHERE     StudentId = t6.StudentId
                                AND default1 = 1
                    ) AS Zip
                   ,(
                      SELECT TOP 1
                                ForeignPhone
                      FROM      arStudentPhone
                      WHERE     arStudentPhone.StudentId = t6.StudentId
                    ) AS ForeignPhone
                   ,(
                      SELECT TOP 1
                                ForeignZip
                      FROM      arStudAddresses
                      WHERE     arStudAddresses.StudentId = t6.StudentId
                                AND default1 = 1
                    ) AS ForeignZip
                   ,(
                      SELECT TOP 1
                                Grade
                      FROM      arGradeSystemDetails
                      WHERE     GrdSysDetailId = t1.GrdSysDetailId
                    ) AS Grade
                   ,t4.GrdComponentTypeId
            -- Code changes by Vijay Ramteke on 28 May 2010 for mantis issue id 18848
                   ,t1.istestmentorproctored 
        ----
            FROM    arPostScoreDictationSpeedTest t1
            INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
            INNER JOIN arTerm t3 ON t1.TermId = t3.TermId
            INNER JOIN (
                         SELECT *
                         FROM   arGrdComponentTypes
                         WHERE  SysComponentTypeId = 612
                       ) t4 ON t1.GrdComponentTypeId = t4.GrdComponentTypeId
            INNER JOIN syUsers t5 ON t5.UserId = t1.InstructorId
            INNER JOIN arStudent t6 ON t2.StudentId = t6.StudentId
            INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
            INNER JOIN syCmpGrpCmps t8 ON t2.campusid = t8.campusid --and (t3.campgrpid=t8.campgrpid) -- or t3.CampGrpId = (SELECT CampGrpId FROM syCampGrps WHERE CampGrpDescrip = 'ALL'))
            INNER JOIN syCampGrps t9 ON t8.campgrpid = t9.campgrpid
            INNER JOIN sycampuses t10 ON t2.campusid = t10.campusid
            WHERE   t6.StudentId = @studentid 
        --- Code changes made by Vijay Ramteke on 01/07/2010 to resolve mantis issue id 19264
                    AND (
                          @stuenrollid IS NULL
                          OR t2.StuEnrollId = @stuenrollid
                        )
                    AND t9.CampGrpDescrip <> 'All';
        ----------------
        END;



GO
