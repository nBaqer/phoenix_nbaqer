SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
---------------------------------------------------------------------------------------------------------------------------------------


---------------------------------------------------------------------------------------------------------------------------------------
-- Balaji - 10/27/2014 - modify sproc to resolve issue with dups
---------------------------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[USP_InsertMRU]
    @MRUTypeId INT
   ,@EntityId UNIQUEIDENTIFIER
   ,@UserId UNIQUEIDENTIFIER
   ,@CampusId UNIQUEIDENTIFIER
AS
    DECLARE @MRUID UNIQUEIDENTIFIER
       ,@Counter INT; 
    DECLARE @StudentFlag INT;

-- If User has already accessed this entity, update the ModDate
-- Else Insert into MRU Table
--IF EXISTS (SELECT * FROM syMRUs WHERE ChildId=@EntityId AND UserId=@UserId)
--		BEGIN
--				UPDATE syMRUs SET ModUser=@UserId,ModDate=GETDATE()
--				WHERE  ChildId=@EntityId AND UserId=@UserId 
--		END	
--Else
		--BEGIN
		   --***** Insert Student MRUS Starts Here ----
    IF @MRUTypeId = 1
        BEGIN
            SET @MRUID = NEWID();
            SET @Counter = (
                             SELECT MAX(Counter) + 1
                             FROM   syMRUS
                           );

            IF EXISTS ( SELECT  *
                        FROM    arStuEnrollments
                        WHERE   StudentId = @EntityId )
                BEGIN
                    SET @CampusId = (
                                      SELECT TOP 1
                                                CampusId
                                      FROM      arStuEnrollments
                                      WHERE     StudentId = @EntityId
                                    );
                END;
					
						-- Delete an existing student entry while accessing student
            DELETE  FROM syMRUS
            WHERE   MRUTypeId = 1
                    AND ChildId = @EntityId
                    AND UserId = @UserId;
					
						-- Insert Student MRU Entry
            INSERT  INTO dbo.syMRUS
                    (
                     MRUId
                    ,ChildId
                    ,MRUTypeId
                    ,UserId
                    ,CampusId
                    ,SortOrder
                    ,IsSticky
                    ,ModUser
                    ,ModDate
                    )
            VALUES  (
                     @MRUID
                    ,@EntityId
                    ,@MRUTypeId
                    ,@UserId
                    ,@CampusId
                    ,0
                    ,0
                    ,@UserId
                    ,GETDATE()
                    );
				
				
            DECLARE @LeadEntityId UNIQUEIDENTIFIER;
            SET @LeadEntityId = (
                                  SELECT TOP 1
                                            LeadId
                                  FROM      arStuEnrollments
                                  WHERE     StudentId = @EntityId
                                            AND LeadId IS NOT NULL
                                );
            IF LEN(@LeadEntityId) = 36
                BEGIN
									-- Delete an existing lead entry while accessing student
                    DELETE  FROM syMRUS
                    WHERE   MRUTypeId = 4
                            AND ChildId = @LeadEntityId
                            AND UserId = @UserId;
									
                    SET @MRUID = NEWID();
                    SET @Counter = (
                                     SELECT MAX(Counter) + 1
                                     FROM   syMRUS
                                   );
                    IF EXISTS ( SELECT  *
                                FROM    adLeads
                                WHERE   LeadId = @LeadEntityId )
                        BEGIN
                            SET @CampusId = (
                                              SELECT TOP 1
                                                        CampusId
                                              FROM      adLeads
                                              WHERE     LeadId = @LeadEntityId
                                            );
                        END;
									
									-- While Searching for student an entry needs to be added to lead MRU as well	
                    INSERT  INTO dbo.syMRUS
                            (
                             MRUId
                            ,ChildId
                            ,MRUTypeId
                            ,UserId
                            ,CampusId
                            ,SortOrder
                            ,IsSticky
                            ,ModUser
                            ,ModDate
                            )
                    VALUES  (
                             @MRUID
                            ,@LeadEntityId
                            ,4
                            ,@UserId
                            ,@CampusId
                            ,0
                            ,0
                            ,@UserId
                            ,GETDATE()
                            );		
                END; 
        END;
			-- If MRUTypeId = 1 Ends Here
		
				
			--****** Insert Employer MRUS Starts Here *****
    IF @MRUTypeId = 2 --Employer MRUS
        BEGIN
            SET @MRUID = NEWID();
            SET @Counter = (
                             SELECT MAX(Counter) + 1
                             FROM   syMRUS
                           );
						
						-- Delete an existing employer entry 
            DELETE  FROM syMRUS
            WHERE   MRUTypeId = @MRUTypeId
                    AND ChildId = @EntityId
                    AND UserId = @UserId;

            INSERT  INTO dbo.syMRUS
                    (
                     MRUId
                    ,ChildId
                    ,MRUTypeId
                    ,UserId
                    ,CampusId
                    ,SortOrder
                    ,IsSticky
                    ,ModUser
                    ,ModDate
                    )
            VALUES  (
                     @MRUID
                    ,@EntityId
                    ,@MRUTypeId
                    ,@UserId
                    ,@CampusId
                    ,0
                    ,0
                    ,@UserId
                    ,GETDATE()
                    );
        END;	
				--****** Insert Employer MRUS Ends Here *****
			
		
		--****** Insert Employee MRUS Starts Here *****
    IF @MRUTypeId = 3 --Employee MRUS
        BEGIN
            SET @MRUID = NEWID();
            SET @Counter = (
                             SELECT MAX(Counter) + 1
                             FROM   syMRUS
                           );
						
						-- Delete an existing employer entry 
            DELETE  FROM syMRUS
            WHERE   MRUTypeId = @MRUTypeId
                    AND ChildId = @EntityId
                    AND UserId = @UserId;

            INSERT  INTO dbo.syMRUS
                    (
                     MRUId
                    ,ChildId
                    ,MRUTypeId
                    ,UserId
                    ,CampusId
                    ,SortOrder
                    ,IsSticky
                    ,ModUser
                    ,ModDate
                    )
            VALUES  (
                     @MRUID
                    ,@EntityId
                    ,@MRUTypeId
                    ,@UserId
                    ,@CampusId
                    ,0
                    ,0
                    ,@UserId
                    ,GETDATE()
                    );
        END;	
		--****** Insert Employer MRUS Ends Here *****
		
		
		
		--****** Insert Lead MRUS Starts Here *****
    IF @MRUTypeId = 4
        BEGIN
				
            SET @MRUID = NEWID();
            SET @Counter = (
                             SELECT MAX(Counter) + 1
                             FROM   syMRUS
                           );
            IF EXISTS ( SELECT  *
                        FROM    adLeads
                        WHERE   LeadId = @EntityId )
                BEGIN
                    SET @CampusId = (
                                      SELECT TOP 1
                                                CampusId
                                      FROM      adLeads
                                      WHERE     LeadId = @EntityId
                                    );
                END;
					
					-- Delete an existing student entry while accessing student
            DELETE  FROM syMRUS
            WHERE   MRUTypeId = @MRUTypeId
                    AND ChildId = @EntityId
                    AND UserId = @UserId;
					
            INSERT  INTO dbo.syMRUS
                    (
                     MRUId
                    ,ChildId
                    ,MRUTypeId
                    ,UserId
                    ,CampusId
                    ,SortOrder
                    ,IsSticky
                    ,ModUser
                    ,ModDate
                    )
            VALUES  (
                     @MRUID
                    ,@EntityId
                    ,@MRUTypeId
                    ,@UserId
                    ,@CampusId
                    ,0
                    ,0
                    ,@UserId
                    ,GETDATE()
                    );
				
				
            DECLARE @StudentEntityId UNIQUEIDENTIFIER;
            SET @StudentEntityId = (
                                     SELECT TOP 1
                                            StudentId
                                     FROM   arStuEnrollments
                                     WHERE  LeadId = @EntityId
                                            AND LeadId IS NOT NULL
                                   );
            SET @CampusId = (
                              SELECT TOP 1
                                        CampusId
                              FROM      arStuEnrollments
                              WHERE     LeadId = @EntityId
                                        AND LeadId IS NOT NULL
                            );
            IF LEN(@StudentEntityId) = 36
                BEGIN
								-- Delete an existing student entry while accessing lead
                    DELETE  FROM syMRUS
                    WHERE   MRUTypeId = 1
                            AND ChildId = @StudentEntityId
                            AND UserId = @UserId;
						
                    IF EXISTS ( SELECT  *
                                FROM    syMRUS
                                WHERE   ChildId = @StudentEntityId
                                        AND UserId = @UserId
                                        AND CampusId = @CampusId )
                        BEGIN
                            UPDATE  syMRUS
                            SET     ModUser = @UserId
                                   ,ModDate = GETDATE()
                            WHERE   ChildId = @StudentEntityId
                                    AND UserId = @UserId;  
										--AND CampusId=@CampusId 
                        END;		
                    ELSE
                        BEGIN
                            SET @MRUID = NEWID();
                            SET @Counter = (
                                             SELECT MAX(Counter) + 1
                                             FROM   syMRUS
                                           );
										
                            INSERT  INTO dbo.syMRUS
                                    (
                                     MRUId
                                    ,ChildId
                                    ,MRUTypeId
                                    ,UserId
                                    ,CampusId
                                    ,SortOrder
                                    ,IsSticky
                                    ,ModUser
                                    ,ModDate
                                    )
                            VALUES  (
                                     @MRUID
                                    ,@StudentEntityId
                                    ,1
                                    ,@UserId
                                    ,@CampusId
                                    ,0
                                    ,0
                                    ,@UserId
                                    ,GETDATE()
                                    );
                        END;
                END; 
        END;
	--end

			--****** Insert Lead MRUS Ends Here *****



GO
