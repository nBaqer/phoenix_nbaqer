SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[USP_Fall05_12MonthEnrollment_ClockHourActivity_GetList]
    @CampusId VARCHAR(50)
   ,@ProgId VARCHAR(4000) = NULL
   ,@StartDate DATETIME = NULL
   ,@EndDate DATETIME = NULL
   ,@OrderBy VARCHAR(100)
AS
    SELECT *
    INTO   #TempClock
    FROM   (
           SELECT
               --StuEnrollId,
                dbo.UDF_FormatSSN(SSN) AS SSN
               ,StudentNumber
               ,StudentName
               ,dt2.StuEnrollId
               ,(
                SELECT TOP 1 EnrollmentId
                FROM   arStuEnrollments A5
                WHERE  A5.StuEnrollId = dt2.StuEnrollId
                ) AS EnrollmentId
               ,ProgCode
               ,ProgramName
               ,PrgVerCode
               ,PrgVerDescrip
               ,PrgVerId
               ,ProgLength
               ,CONVERT(VARCHAR, StartDate, 101) AS StartDate
               ,HrsPerWeek
               -- Field 20
               ,NumberofWeeks
               -- Field 21
               ,CASE WHEN WeeksInReportingPeriod > 0 THEN CONVERT(DECIMAL(18, 2), ROUND(WeeksInReportingPeriod, 2))
                     ELSE CASE WHEN WeeksInReportingPeriod < 0 THEN 0
                               ELSE 0.00
                          END
                END AS WeeksInReportingPeriod
               -- Field 26
               -- Commented by Balaji on 9/23/2010
               --Case When WeeksInReportingPeriod>=0 Then Convert(decimal(18,2),Round(HrsPerWeek*WeeksInReportingPeriod,2)) Else Convert(decimal(18,2),Round(HrsPerWeek*NumberofWeeks,2)) End as ContactHours
               ,CASE WHEN WeeksInReportingPeriod > 0 THEN CAST(ROUND(HrsPerWeek * WeeksInReportingPeriod, 2) AS DECIMAL(18, 2))
                     ELSE 0.00
                END AS ContactHours
               ,(
                SELECT TOP 1 UnitTypeId
                FROM   arPrgVersions PV
                WHERE  PV.PrgVerId = PrgVerId
                ) AS UnitTypeId
           FROM (
                SELECT StuEnrollId
                      ,SSN
                      ,StudentNumber
                      ,StudentName
                      ,ProgCode
                      ,ProgramName
                      ,ProgLength
                      ,CONVERT(VARCHAR, StartDate, 101) AS StartDate
                      ,HrsPerWeek
                      ,ISNULL(CAST(ROUND(NumberOfWeeks, 2) AS DECIMAL(18, 2)), 0) AS NumberofWeeks
                                    --Cond1 - If Student started before 09/01/2009 (year may vary) and the number of weeks from Student Start Date exceeds the Report End Date
                                    --Then take the number of weeks from the StartDate to EndDate i.e get the number of weeks between September 1 2009 and Aug 31 2010.
                                    --	Case When (DATEDIFF(day,StartDate,@StartDate)>=1 and DateDiff(day,DATEADD(ww,NumberOfweeks,StartDate),@EndDate)<0) Then  DATEDIFF(WW,@StartDate,@EndDate) 
                                    --		 ELSE 
                                    --				--Condition 2: If condition 1 fails, check if the student started before 09/01/2009 and then get the difference between
                                    --				--The student start date and 09/01/2009 and subtract the weeks from WeeksInProgram.
                                    --				-- If the ouput is negative show zero
                                    --				Case When DATEDIFF(day,StartDate,@StartDate)>=1 Then (NumberOfweeks-DATEDIFF(WW,StartDate,@StartDate))
                                    --				ELSE
                                    --					Case When DATEADD(ww,NumberofWeeks,StartDate)>=@EndDate Then (NumberofWeeks-DATEDIFF(ww,@EndDate,DateAdd(ww,NumberofWeeks,StartDate))) Else NumberofWeeks End			
                                    --				End
                                    --		 End 
                                    --	as
                                    --	WeeksInReportingPeriod,
                      ,CASE
                            -- If Student started after the report start date and graduated in the date range
                            WHEN (
                                 StartDate >= @StartDate
                                 AND StartDate <= @EndDate
                                 AND EndOfProgramDate <= @EndDate
                                 ) THEN ISNULL(NumberOfWeeks, 0.00)
                            -- If Student started before the report start date and graduated after report end date
                            WHEN (
                                 StartDate < @StartDate
                                 AND EndOfProgramDate > @EndDate
                                 ) THEN ISNULL(DATEDIFF(ww, @StartDate, @EndDate), 0.00)
                            -- If Student started before the report start date
                            WHEN ( StartDate < @StartDate ) THEN ISNULL(NumberOfWeeks - DATEDIFF(ww, StartDate, @StartDate), 0.00)
                            -- If Student graduated after report end date
                            WHEN ( EndOfProgramDate > @EndDate ) THEN
                                ISNULL(NumberOfWeeks - ( DATEDIFF(WEEK, @EndDate, DATEADD(WEEK, NumberOfWeeks, StartDate))), 0.00)
                            ELSE 0.00
                       END AS WeeksInReportingPeriod
                      ,PrgVerCode
                      ,PrgVerDescrip
                      ,PrgVerId
                      ,ContactHours --,Case When WeeksInReportingPeriod>0 Then WeeksInReportingPeriod Else 0 End as WeeksInReportingPeriod1
                FROM   (
                       SELECT StuEnrollId
                             ,SSN
                             ,StudentNumber
                             ,StudentName
                             ,ProgCode
                             ,ProgramName
                             ,ProgLength
                             ,CONVERT(VARCHAR, StartDate, 101) AS StartDate
                             ,HrsPerWeek
                             ,CASE WHEN HrsPerWeek > 0 THEN ProgLength / HrsPerWeek
                                   ELSE NULL
                              END AS NumberofWeeks
                             ,DATEADD(   WEEK
                                        ,( CASE WHEN HrsPerWeek > 0 THEN ProgLength / HrsPerWeek
                                                ELSE NULL
                                           END
                                         )
                                        ,CONVERT(VARCHAR, StartDate, 101)
                                     ) AS EndOfProgramDate
                             ,PrgVerDescrip
                             ,PrgVerCode
                             ,PrgVerId
                             ,CONVERT(VARCHAR, ExpGradDate, 101) AS ExpGradDate
                             ,ContactHours
                       FROM   (
                              SELECT
                                  --t1.StudentId, 
                                         t1.SSN
                                        ,t1.StudentNumber
                                        ,t1.LastName + ', ' + t1.FirstName + ' ' + ISNULL(t1.MiddleName, '') AS StudentName
                                        ,t8.ProgCode
                                        ,t8.ProgDescrip AS ProgramName
                                        ,t7.PrgVerDescrip
                                        ,t7.PrgVerCode
                                        ,t7.PrgVerId
                                        ,t7.Hours AS ProgLength
                                        ,t2.StartDate
                                        ,t2.ExpGradDate
                                        ,t2.StuEnrollId
                                        ,(
                                         SELECT SUM(PSD.total)
                                         FROM   arStudentSchedules SS
                                               ,arProgScheduleDetails PSD
                                         WHERE  StuEnrollId = t2.StuEnrollId
                                                AND SS.ScheduleId = PSD.ScheduleId
                                         ) AS HrsPerWeek
                                        --            (Select Top 1 AdjustedPresentDays from syStudentAttendanceSummary where StuEnrollId=t2.StuEnrollId
                                        --order by StudentAttendedDate Desc) as ContactHours
                                        ,(
                                         SELECT ISNULL(SUM(ActualDays), 0)
                                         FROM   dbo.syStudentAttendanceSummary sas
                                         WHERE  StuEnrollId = t2.StuEnrollId
                                                AND sas.StudentAttendedDate
                                                BETWEEN @StartDate AND @EndDate
                                         ) AS ContactHours
                              --ISNULL((Select SUM(PSD.total) as HrsPerWeek  from arProgSchedules PS, arProgScheduleDetails PSD, arStudentSchedules SS  Where t2.PrgVerId=PS.PrgVerId and t2.StuEnrollId =SS.StuEnrollId and 
                              --SS.ScheduleId =PS.ScheduleId And PS.ScheduleId=PSD.ScheduleId),0) as HrsPerWeek
                              FROM       dbo.adLeads t1
                              INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                              INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                              INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                                           AND t6.SysStatusId NOT IN ( 8 )
                              INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                              INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                              INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                              LEFT JOIN  arAttendTypes t10 ON t2.AttendTypeId = t10.AttendTypeId
                              WHERE      t2.CampusId = @CampusId
                                         AND (
                                             @ProgId IS NULL
                                             OR t8.ProgId IN (
                                                             SELECT Val
                                                             FROM   MultipleValuesForReportParameters(@ProgId, ',', 1)
                                                             )
                                             )
                                         AND t8.ACId = 5 -- Clock Hour Programs
                                         AND t2.StartDate <= @EndDate -- Check if Student started before the report end date
                                         AND StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
                                             -- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
                                             (
                                             SELECT t1.StuEnrollId
                                             FROM   arStuEnrollments t1
                                                   ,syStatusCodes t2
                                             WHERE  t1.StatusCodeId = t2.StatusCodeId
                                                    AND StartDate <= @EndDate
                                                    AND -- Student started before the end date range
                                                 LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                                    AND (
                                                        @ProgId IS NULL
                                                        OR t8.ProgId IN (
                                                                        SELECT Val
                                                                        FROM   MultipleValuesForReportParameters(@ProgId, ',', 1)
                                                                        )
                                                        )
                                                    AND t2.SysStatusId IN ( 12, 14, 19, 8 ) -- Dropped out/Transferred/Graduated/No Start
                                                    -- Date Determined or ExpGradDate or LDA falls before Report Start Date
                                                    AND (
                                                        t1.DateDetermined < @StartDate
                                                        OR ExpGradDate < @StartDate
                                                        OR LDA < @StartDate
                                                        )
                                             )
                                         -- If Student is enrolled in only one program version and if that program version 
                                         -- happens to be a continuing ed program exclude the student
                                         AND t2.StudentId NOT IN (
                                                                 SELECT DISTINCT StudentId
                                                                 FROM   (
                                                                        SELECT   StudentId
                                                                                ,COUNT(*) AS RowCounter
                                                                        FROM     arStuEnrollments
                                                                        WHERE    PrgVerId IN (
                                                                                             SELECT PrgVerId
                                                                                             FROM   arPrgVersions
                                                                                             WHERE  IsContinuingEd = 1
                                                                                             )
                                                                        GROUP BY StudentId
                                                                        HAVING   COUNT(*) = 1
                                                                        ) dtStudent_ContinuingEd
                                                                 )
                                         -- If student is enrolled in program versions that belong to same program
                                         -- then ignore the second enrollment

                                         -- If student is only enrolled in one program show the program
                                         AND ( t2.StuEnrollId IN (
                                                                 SELECT     TOP 1 StuEnrollId
                                                                 FROM       arStuEnrollments se
                                                                 INNER JOIN dbo.syStatusCodes sc ON sc.StatusCodeId = se.StatusCodeId
                                                                 INNER JOIN dbo.sySysStatus ss ON ss.SysStatusId = sc.SysStatusId
                                                                 WHERE      StudentId IN (
                                                                                         SELECT DISTINCT StudentId
                                                                                         FROM   (
                                                                                                SELECT   StudentId
                                                                                                        ,A3.ProgId
                                                                                                        ,COUNT(*) AS RowCounter
                                                                                                FROM     arStuEnrollments A1
                                                                                                        ,arPrgVersions A2
                                                                                                        ,arPrograms A3
                                                                                                WHERE    A1.PrgVerId = A2.PrgVerId
                                                                                                         AND A2.ProgId = A3.ProgId
                                                                                                         AND A1.StudentId = t1.StudentId
                                                                                                GROUP BY StudentId
                                                                                                        ,A3.ProgId
                                                                                                HAVING   COUNT(*) > 1
                                                                                                ) dtStudentInMultiplePrograms
                                                                                         )
                                                                            AND 1 = ( CASE WHEN ss.SysStatusId IN ( 8 ) THEN 0
                                                                                           WHEN ss.SysStatusId IN ( 12, 14, 19 )
                                                                                                -- Dropped out/Transferred/Graduated/No Start
                                                                                                -- Date Determined or ExpGradDate or LDA falls before Report Start Date
                                                                                                AND (
                                                                                                    se.DateDetermined < @StartDate
                                                                                                    OR se.ExpGradDate < @StartDate
                                                                                                    OR se.LDA < @StartDate
                                                                                                    ) THEN 0
                                                                                           ELSE 1
                                                                                      END
                                                                                    )
                                                                 ORDER BY   StartDate
                                                                           ,EnrollDate ASC
                                                                 UNION
                                                                 SELECT StuEnrollId
                                                                 FROM   arStuEnrollments
                                                                 WHERE  StudentId IN (
                                                                                     SELECT DISTINCT StudentId
                                                                                     FROM   (
                                                                                            SELECT   StudentId
                                                                                                    ,A3.ProgId
                                                                                                    ,COUNT(*) AS RowCounter
                                                                                            FROM     arStuEnrollments A1
                                                                                                    ,arPrgVersions A2
                                                                                                    ,arPrograms A3
                                                                                            WHERE    A1.PrgVerId = A2.PrgVerId
                                                                                                     AND A2.ProgId = A3.ProgId
                                                                                                     AND A1.StudentId = t1.StudentId
                                                                                            GROUP BY StudentId
                                                                                                    ,A3.ProgId
                                                                                            HAVING   COUNT(*) = 1
                                                                                            ) dtStudentInOnePrograms
                                                                                     )
                                                                 )
                                             )
                              ) dt
                       ) dt1
                ) dt2
           ) dt3;


    --DECLARE @GrandTotal DECIMAL(18,2)
    --   ,@PrgVerTotal DECIMAL(18,2);
    --SET @GrandTotal = ( SELECT  SUM(ContactHours) AS TotalContactHours
    --                    FROM    #TempClock
    --                  );

    SELECT   ROW_NUMBER() OVER ( ORDER BY ProgCode
                                         ,ProgramName
                                         ,PrgVerCode
                                         ,PrgVerDescrip
                                         ,CASE WHEN @OrderBy = 'SSN' THEN SSN
                                          END
                                         ,CASE WHEN @OrderBy = 'LastName' THEN StudentName
                                          END
                                         ,CASE WHEN @OrderBy = 'student number' THEN StudentNumber
                                          END
                                         ,CASE WHEN @OrderBy = 'enrollmentid' THEN EnrollmentId
                                          END
                               ) AS RowID
            ,t1.SSN
            ,t1.StudentNumber
            ,t1.StudentName
            ,t1.StuEnrollId
            ,t1.EnrollmentId
            ,t1.ProgCode
            ,t1.ProgramName
            ,t1.PrgVerCode
            ,t1.PrgVerDescrip
            ,t1.PrgVerId
            ,t1.ProgLength
            ,t1.StartDate
            ,t1.HrsPerWeek
            ,t1.NumberofWeeks
            ,t1.WeeksInReportingPeriod
            ,CAST(CASE WHEN ( t1.UnitTypeId = 'A1389C74-0BB9-4BBF-A47F-68428BE7FA4D'
                            --OR t1.UnitTypeId = 'B937C92E-FD7A-455E-A731-527A9918C734'
                            ) THEN --Minutes 
                           CASE WHEN t1.ContactHours > 0 THEN t1.ContactHours / 60
                                ELSE t1.ContactHours
                           END
                       ELSE t1.ContactHours
                  END AS DECIMAL(18, 2)) AS ContactHours
            ,(
             SELECT CASE WHEN ( t1.UnitTypeId = 'A1389C74-0BB9-4BBF-A47F-68428BE7FA4D'
                              --OR t1.UnitTypeId = 'B937C92E-FD7A-455E-A731-527A9918C734'
                              ) THEN SUM(TC.ContactHours / 60)
                         ELSE SUM(TC.ContactHours)
                    END
             FROM   #TempClock TC
             WHERE  PrgVerId = t1.PrgVerId
             ) AS PrgVersion_SubTotal
            ,(
             SELECT COUNT(StudentName)
             FROM   #TempClock
             WHERE  PrgVerId = t1.PrgVerId
             ) AS StudentCount_SubTotal
            ,(
             SELECT CASE WHEN ( t1.UnitTypeId = 'A1389C74-0BB9-4BBF-A47F-68428BE7FA4D'
                              --OR t1.UnitTypeId = 'B937C92E-FD7A-455E-A731-527A9918C734'
                              ) THEN SUM(TC.ContactHours / 60)
                         ELSE SUM(TC.ContactHours)
                    END
             FROM   #TempClock TC
             ) AS GrandTotal
    FROM     #TempClock t1
    ORDER BY ProgCode
            ,ProgramName
            ,PrgVerCode
            ,PrgVerDescrip
            ,CASE WHEN @OrderBy = 'SSN' THEN SSN
             END
            ,CASE WHEN @OrderBy = 'LastName' THEN StudentName
             END
            ,CASE WHEN @OrderBy = 'student number' THEN StudentNumber
             END
            ,CASE WHEN @OrderBy = 'enrollmentid' THEN EnrollmentId
             END;
    DROP TABLE #TempClock;



GO
