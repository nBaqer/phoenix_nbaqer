SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_Gender_GetList]
    @GenderDescrip VARCHAR(50)
AS
    SELECT  GenderId
    FROM    adGenders
    WHERE   GenderDescrip LIKE @GenderDescrip + '%'; 



GO
