SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_WinterIPEDS_FallPartB4_PTUG_GetList_Summary]
    @CampusId VARCHAR(50)
   ,@ProgId VARCHAR(4000) = NULL
   ,@StartDate DATETIME = NULL
   ,@EndDate DATETIME = NULL
   ,@DateRangeText VARCHAR(100) = NULL
   ,@OrderBy VARCHAR(100)
AS
    DECLARE @ReturnValue VARCHAR(100);
    DECLARE @ContinuingStartDate DATETIME;
    DECLARE @Value VARCHAR(50);
--set @Value='letter'
-- 2/07/2013 - updated the @value 
    SET @Value = (
                   SELECT   dbo.GetAppSettingValue(47,@CampusId)
                 );
    SET @ContinuingStartDate = @StartDate;
-- For Academic Year Reporter, the End Date should be 10/15/Cohort Year
-- and Start Date should be blank
		-- if day(@EndDate)=15 and month(@EndDate)=10 
    IF SUBSTRING(LOWER(@DateRangeText),1,10) = 'enrollment'
        BEGIN
            SET @EndDate = @StartDate; -- it will always be 10/15/cohort year example: for cohort year 2009, it is 10/15/2009
            SET @StartDate = @StartDate; -- it will always be 10/15/cohort year example: for cohort year 2009, it is 10/15/2009
        END;

    IF @ProgId IS NOT NULL
        BEGIN
            SELECT  @ReturnValue = COALESCE(@ReturnValue,'') + ProgDescrip + ','
            FROM    arPrograms t1
            WHERE   t1.ProgId IN ( SELECT   Val
                                   FROM     MultipleValuesForReportParameters(@ProgId,',',1) );
            SET @ReturnValue = SUBSTRING(@ReturnValue,1,LEN(@ReturnValue) - 1);
        END;
    ELSE
        BEGIN
            SELECT  @ReturnValue = COALESCE(@ReturnValue,'') + ProgDescrip + ','
            FROM    arPrograms t1
            WHERE   t1.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
                    AND t1.CampGrpId IN ( SELECT    CampGrpId
                                          FROM      syCmpGrpCmps
                                          WHERE     CampusId = @CampusId );
            SET @ReturnValue = SUBSTRING(@ReturnValue,1,LEN(@ReturnValue) - 1);
        END;

	
    CREATE TABLE #FallPartB4FTUG
        (
         RowNumber UNIQUEIDENTIFIER
        ,SSN VARCHAR(50)
        ,StudentNumber VARCHAR(50)
        ,StudentName VARCHAR(100)
        ,Gender VARCHAR(50)
        ,AgeSequence INT
        ,GenderSequence INT
        ,StudentId UNIQUEIDENTIFIER
        );  

    CREATE TABLE #AgeGroup
        (
         RowNumber UNIQUEIDENTIFIER
        ,AgeGroup VARCHAR(50)
        ,AgeSequence INT
        );

    INSERT  INTO #AgeGroup
    VALUES  ( NEWID(),'Under 18',1 );
    INSERT  INTO #AgeGroup
    VALUES  ( NEWID(),'18-19',2 );
    INSERT  INTO #AgeGroup
    VALUES  ( NEWID(),'20-21',3 );
    INSERT  INTO #AgeGroup
    VALUES  ( NEWID(),'22-24',4 );
    INSERT  INTO #AgeGroup
    VALUES  ( NEWID(),'25-29',5 );
    INSERT  INTO #AgeGroup
    VALUES  ( NEWID(),'30-34',6 );
    INSERT  INTO #AgeGroup
    VALUES  ( NEWID(),'35-39',7 );
    INSERT  INTO #AgeGroup
    VALUES  ( NEWID(),'40-49',8 );
    INSERT  INTO #AgeGroup
    VALUES  ( NEWID(),'50-64',9 );
    INSERT  INTO #AgeGroup
    VALUES  ( NEWID(),'65 and over',10 ); 

    INSERT  INTO #FallPartB4FTUG
            SELECT  NEWID() AS RowNumber
                   ,dbo.UDF_FormatSSN(SSN) AS SSN
                   ,StudentNumber
                   ,StudentName
                   ,Gender
                   ,CASE WHEN ( AgeDiff < 18 ) THEN 1
                         WHEN (
                                AgeDiff >= 18
                                AND AgeDiff < 20
                              ) THEN 2
                         WHEN (
                                AgeDiff >= 20
                                AND AgeDiff < 22
                              ) THEN 3
                         WHEN (
                                AgeDiff >= 22
                                AND AgeDiff < 25
                              ) THEN 4
                         WHEN (
                                AgeDiff >= 25
                                AND AgeDiff < 30
                              ) THEN 5
                         WHEN (
                                AgeDiff >= 30
                                AND AgeDiff < 35
                              ) THEN 6
                         WHEN (
                                AgeDiff >= 35
                                AND AgeDiff < 40
                              ) THEN 7
                         WHEN (
                                AgeDiff >= 40
                                AND AgeDiff < 50
                              ) THEN 8
                         WHEN (
                                AgeDiff >= 50
                                AND AgeDiff < 65
                              ) THEN 9
                         WHEN ( AgeDiff >= 65 ) THEN 10
                         ELSE 1
                    END AS AgeSequence
                   ,GenderSequence
                   ,StudentId
            FROM    (
                      SELECT    t1.SSN
                               ,t1.StudentNumber
                               ,t1.LastName + ', ' + t1.FirstName + ' ' + ISNULL(t1.MiddleName,'') AS StudentName
                               ,(
                                  SELECT DISTINCT
                                            AgencyDescrip
                                  FROM      syRptAgencyFldValues
                                  WHERE     RptAgencyFldValId = t3.IPEDSValue
                                ) AS Gender
                               ,CASE WHEN DAY(GETDATE()) < DAY(t1.DOB) THEN ( DATEDIFF(mm,t1.DOB,GETDATE()) - 1 ) / 12.000
                                     ELSE DATEDIFF(mm,t1.DOB,GETDATE()) / 12.000
                                END AS AgeDiff
                               ,t3.IPEDSSequence AS GenderSequence
                               ,t1.StudentId
                               ,t1.DOB AS BirthDate
                      FROM      adGenders t3
                      LEFT JOIN arStudent t1 ON t3.GenderId = t1.Gender
                      INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                      INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                      INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                                   AND t6.SysStatusId NOT IN ( 8 )
                      INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                      INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                      INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                      LEFT JOIN arAttendTypes t10 ON t2.AttendTypeId = t10.AttendTypeId
                      INNER JOIN adCitizenships t11 ON t1.Citizen = t11.CitizenshipId
                      WHERE     LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                                AND (
                                      @ProgId IS NULL
                                      OR t8.ProgId IN ( SELECT  Val
                                                        FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                    )
                                AND t9.IPEDSValue = 58
                                AND -- undergraduate
                                t10.IPEDSValue = 62
                                AND -- Part Time
                                t3.IPEDSValue = 30  -- Men
                                AND t2.StartDate <= @EndDate
                                AND StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
						( SELECT    t1.StuEnrollId
                          FROM      arStuEnrollments t1
                                   ,syStatusCodes t2
                          WHERE     t1.StatusCodeId = t2.StatusCodeId
                                    AND StartDate <= @EndDate
                                    AND -- Student started before the end date range
                                    LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                    AND (
                                          @ProgId IS NULL
                                          OR t8.ProgId IN ( SELECT  Val
                                                            FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                        )
                                    AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                    AND (
                                          t1.DateDetermined < @StartDate
                                          OR ExpGradDate < @StartDate
                                          OR LDA < @StartDate
                                        ) )
						  --DE9094
					    -- If Student is enrolled in only program version and if that program version 
						-- happens to be a continuing ed program exclude the student
                                AND t2.StudentId NOT IN ( SELECT DISTINCT
                                                                    StudentId
                                                          FROM      (
                                                                      SELECT    StudentId
                                                                               ,COUNT(*) AS RowCounter
                                                                      FROM      arStuEnrollments
                                                                      WHERE     PrgVerId IN ( SELECT    PrgVerId
                                                                                              FROM      arPrgVersions
                                                                                              WHERE     IsContinuingEd = 1 )
                                                                      GROUP BY  StudentId
                                                                      HAVING    COUNT(*) = 1
                                                                    ) dtStudent_ContinuingEd )
                        -- If Student is enrolled in two undergraduate programs or two graduate programs the student should be shown only once
                                AND t2.StuEnrollId IN (
                                --SELECT TOP 1
                                --        StuEnrollId
                                --FROM    arStuEnrollments A1 ,
                                --        arPrgVersions A2 ,
                                --        arProgTypes A3
                                --WHERE   A1.PrgVerId = A2.PrgVerId
                                --        AND A2.ProgTypId = A3.ProgTypId
                                --        AND A1.StudentId = t1.StudentId
                                --        AND A3.IPEDSValue = 58
                                --ORDER BY StartDate ,
                                --        EnrollDate ASC
                                SELECT  dbo.GetIPEDS_Spring_GetEnrollmentfromMultipleEnrollment(@CampusId,@ProgId,@EndDate,'undergraduate',t1.StudentId) )
                      UNION
                      SELECT    t1.SSN
                               ,t1.StudentNumber
                               ,t1.LastName + ', ' + t1.FirstName + ' ' + ISNULL(t1.MiddleName,'') AS StudentName
                               ,(
                                  SELECT DISTINCT
                                            AgencyDescrip
                                  FROM      syRptAgencyFldValues
                                  WHERE     RptAgencyFldValId = t3.IPEDSValue
                                ) AS Gender
                               ,CASE WHEN DAY(GETDATE()) < DAY(t1.DOB) THEN ( DATEDIFF(mm,t1.DOB,GETDATE()) - 1 ) / 12.000
                                     ELSE DATEDIFF(mm,t1.DOB,GETDATE()) / 12.000
                                END AS AgeDiff
                               ,t3.IPEDSSequence AS GenderSequence
                               ,t1.StudentId
                               ,t1.DOB AS BirthDate
                      FROM      adGenders t3
                      LEFT JOIN arStudent t1 ON t3.GenderId = t1.Gender
                      LEFT JOIN adEthCodes t4 ON t4.EthCodeId = t1.Race
                      INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                      INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                      INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                                   AND t6.SysStatusId NOT IN ( 8 )
                      INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                      INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                      INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                      LEFT JOIN arAttendTypes t10 ON t2.AttendTypeId = t10.AttendTypeId
                      INNER JOIN adCitizenships t11 ON t1.Citizen = t11.CitizenshipId
                      WHERE     LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                                AND (
                                      @ProgId IS NULL
                                      OR t8.ProgId IN ( SELECT  Val
                                                        FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                    )
                                AND t9.IPEDSValue = 58
                                AND -- undergraduate
                                t10.IPEDSValue = 62
                                AND -- Part Time
                                t3.IPEDSValue = 31  -- Women
                                AND t2.StartDate <= @EndDate
                                AND StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
						( SELECT    t1.StuEnrollId
                          FROM      arStuEnrollments t1
                                   ,syStatusCodes t2
                          WHERE     t1.StatusCodeId = t2.StatusCodeId
                                    AND StartDate <= @EndDate
                                    AND -- Student started before the end date range
                                    LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                    AND (
                                          @ProgId IS NULL
                                          OR t8.ProgId IN ( SELECT  Val
                                                            FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                        )
                                    AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                    AND (
                                          t1.DateDetermined < @StartDate
                                          OR ExpGradDate < @StartDate
                                          OR LDA < @StartDate
                                        ) )
						  --DE9094
					    -- If Student is enrolled in only program version and if that program version 
						-- happens to be a continuing ed program exclude the student
                                AND t2.StudentId NOT IN ( SELECT DISTINCT
                                                                    StudentId
                                                          FROM      (
                                                                      SELECT    StudentId
                                                                               ,COUNT(*) AS RowCounter
                                                                      FROM      arStuEnrollments
                                                                      WHERE     PrgVerId IN ( SELECT    PrgVerId
                                                                                              FROM      arPrgVersions
                                                                                              WHERE     IsContinuingEd = 1 )
                                                                      GROUP BY  StudentId
                                                                      HAVING    COUNT(*) = 1
                                                                    ) dtStudent_ContinuingEd )
                        -- If Student is enrolled in two undergraduate programs or two graduate programs the student should be shown only once
                                AND t2.StuEnrollId IN (
                                --SELECT TOP 1
                                --        StuEnrollId
                                --FROM    arStuEnrollments A1 ,
                                --        arPrgVersions A2 ,
                                --        arProgTypes A3
                                --WHERE   A1.PrgVerId = A2.PrgVerId
                                --        AND A2.ProgTypId = A3.ProgTypId
                                --        AND A1.StudentId = t1.StudentId
                                --        AND A3.IPEDSValue = 58
                                --ORDER BY StartDate ,
                                --        EnrollDate ASC 
                                SELECT  dbo.GetIPEDS_Spring_GetEnrollmentfromMultipleEnrollment(@CampusId,@ProgId,@EndDate,'undergraduate',t1.StudentId) )
                    ) dt
            WHERE   BirthDate IS NOT NULL; -- If BirthDate is null then student should not show up (DE 5148)

    SELECT  AgeGroup
           ,AgeSequence
           ,SUM(MenCount) AS MenCount
           ,SUM(WomenCount) AS WomenCount
    FROM    (
              SELECT    AG.AgeSequence
                       ,AG.AgeGroup
                       ,CASE ( FR.Gender )
                          WHEN 'Men' THEN 1
                          ELSE 0
                        END AS MenCount
                       ,CASE ( FR.Gender )
                          WHEN 'Women' THEN 1
                          ELSE 0
                        END AS WomenCount
              FROM      #AgeGroup AG
              LEFT OUTER JOIN #FallPartB4FTUG FR ON AG.AgeSequence = FR.AgeSequence
            ) dt
    GROUP BY AgeGroup
           ,AgeSequence
    ORDER BY AgeSequence;

    DROP TABLE #FallPartB4FTUG;
    DROP TABLE #AgeGroup;

GO
