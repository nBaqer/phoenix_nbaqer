SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[USP_CheckAttendanceInLDADates]
    (
     @StuEnrollId UNIQUEIDENTIFIER
    ,@StartDate DATETIME
    ,@EndDate DATETIME
     
    )
AS
    BEGIN
        SELECT  COUNT(*)
        FROM    (
                  SELECT    StuEnrollId
                  FROM      atClsSectAttendance
                  WHERE     StuEnrollId = @StuEnrollId
                            AND Actual > 0
                            AND (
                                  Actual <> 99.00
                                  AND Actual <> 999.00
                                  AND Actual <> 9999.00
                                )
                            AND MeetDate >= @StartDate
                            AND MeetDate < @EndDate
                  UNION
                  SELECT    StuEnrollId
                  FROM      arStudentClockAttendance
                  WHERE     StuEnrollId = @StuEnrollId
                            AND (
                                  ActualHours > 0
                                  AND ActualHours <> 99.00
                                  AND ActualHours <> 999.00
                                  AND ActualHours <> 9999.00
                                )
                            AND RecordDate >= @StartDate
                            AND RecordDate < @EndDate
                ) T;
    END;
    




GO
