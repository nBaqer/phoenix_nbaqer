SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[USP_GetUsertasks]
    (
     @CampusID AS VARCHAR(8000) = NULL
    ,@AssignedByID AS VARCHAR(8000) = NULL
    ,@OwnerID AS VARCHAR(8000) = NULL
    ,@REID AS VARCHAR(8000) = NULL
    ,@Priority AS VARCHAR(50) = NULL
    ,@Status AS INTEGER = NULL
    ,@EndDate AS DATETIME = NULL
    ,@StartDate AS DATETIME = NULL
    ,@OwnerIDForAssignedtoMe AS VARCHAR(8000) = NULL 
     
    )
AS
    SELECT 
        DISTINCT
            UT.UserTaskId
           ,UT.TaskId AS TaskID
           ,T.Descrip AS Descrip
           ,UT.StartDate
           ,UT.EndDate
           ,
      
   
--CASE 
--WHEN  EndDate IS NULL THEN  DATEADD(hour,1,UT.StartDate )
--ELSE  DATEADD(hour,1,UT.ENDDate )
--END 
--         AS EndDate ,
            UT.Priority AS Priority
           ,UT.Message
           ,UT.ReId
           ,(
              (
                SELECT  FirstName + ' ' + LastName
                FROM    arStudent S
                WHERE   S.StudentID = UT.ReId
              )
              UNION
              (
                SELECT  FirstName + ' ' + LastName
                FROM    adLeads
                WHERE   LeadId = UT.ReId
              )
              UNION
              (
                SELECT  FirstName + ' ' + LastName
                FROM    hrEmployees
                WHERE   EmpId = UT.ReId
              )
              UNION
              (
                SELECT  EmployerDescrip
                FROM    plEmployers
                WHERE   EmployerId = UT.ReId
              )
              UNION
              (
                SELECT  LenderDescrip
                FROM    faLenders
                WHERE   LenderId = UT.ReId
              )
            ) AS ReName
           ,(
              (
                SELECT TOP 1
                        C.CampDescrip
                FROM    arStudent S
                       ,arStuEnrollments SE
                       ,syCampuses C
                WHERE   S.StudentId = SE.StudentId
                        AND SE.CampusId = C.CampusId
                        AND S.StudentID = UT.ReId
              )
              UNION
              (
                SELECT TOP 1
                        C.CampDescrip
                FROM    adLeads L
                       ,syCampuses C
                WHERE   L.CampusId = C.CampusId
                        AND L.LeadId = UT.ReId
              )
              UNION
              (
                SELECT TOP 1
                        C.CampDescrip
                FROM    hrEmployees E
                       ,syCampuses C
                WHERE   E.CampusId = C.CampusId
                        AND E.EmpId = UT.ReId
              )
              UNION
              (
                SELECT TOP 1
                        C.CampDescrip
                FROM    plEmployers E
                       ,syCampuses C
                       ,syCmpGrpCmps CGC
                WHERE   E.CampGrpId = CGC.CampGrpId
                        AND C.CampusId = CGC.CampusId
                        AND E.EmployerId = UT.ReId
              )
              UNION
              (
                SELECT TOP 1
                        C.CampDescrip
                FROM    faLenders L
                       ,syCampuses C
                       ,syCmpGrpCmps CGC
                WHERE   L.CampGrpId = CGC.CampGrpId
                        AND C.CampusId = CGC.CampusId
                        AND L.LenderId = UT.ReId
              )
            ) AS CampusDescrip
           ,(
              (
                SELECT  Phone
                FROM    arStudentPhone S
                WHERE   S.StudentID = UT.ReId
                        AND default1 = 1
              )
              UNION
              (
                SELECT  Phone
                FROM    adLeads
                WHERE   LeadId = UT.ReId
              )
              UNION
              (
                SELECT  HomePhone
                FROM    hrEmpContactInfo
                WHERE   EmpId = UT.ReId
              )
              UNION
              (
                SELECT  Phone
                FROM    plEmployers
                WHERE   EmployerId = UT.ReId
              )
              UNION
              (
                SELECT  CustService
                FROM    faLenders
                WHERE   LenderId = UT.ReId
              )
            ) AS Phone
           ,(
              (
                SELECT  HomeEmail
                FROM    arStudent S
                WHERE   S.StudentID = UT.ReId
              )
              UNION
              (
                SELECT  HomeEmail
                FROM    adLeads
                WHERE   LeadId = UT.ReId
              )
              UNION
              (
                SELECT  HomeEmail
                FROM    hrEmpContactInfo
                WHERE   EmpId = UT.ReId
              )
              UNION
              (
                SELECT  Email
                FROM    plEmployers
                WHERE   EmployerId = UT.ReId
              )
              UNION
              (
                SELECT  Email
                FROM    faLenders
                WHERE   LenderId = UT.ReId
              )
            ) AS Email
           ,UT.OwnerId AS UserID
           ,(
              SELECT    username
              FROM      syUsers
              WHERE     UserId = UT.OwnerId
            ) AS UserName
           ,(
              SELECT    FullName
              FROM      syUsers
              WHERE     UserId = UT.OwnerId
            ) AS FullName
           ,UT.AssignedById
           ,(
              SELECT    FullName
              FROM      syUsers
              WHERE     UserId = UT.AssignedById
            ) AS AssignedByName
           ,UT.PrevUserTaskId
           ,(
              SELECT    T2.TaskId
              FROM      tmUserTasks UT2
                       ,tmTasks T2
              WHERE     UT2.TaskId = T2.TaskId
                        AND UT2.UserTaskId = UT.PrevUserTaskId
            ) AS PrevTaskId
           ,(
              SELECT    T2.Descrip
              FROM      tmUserTasks UT2
                       ,tmTasks T2
              WHERE     UT2.TaskId = T2.TaskId
                        AND UT2.UserTaskId = UT.PrevUserTaskId
            ) AS PrevTaskDescrip
           ,(
              SELECT    UT2.ResultId
              FROM      tmUserTasks UT2
                       ,tmTasks T2
              WHERE     UT2.TaskId = T2.TaskId
                        AND UT2.UserTaskId = UT.PrevUserTaskId
            ) AS PrevTaskResultId
           ,(
              SELECT    R.Descrip
              FROM      tmUserTasks UT2
                       ,tmTasks T2
                       ,tmResults R
              WHERE     UT2.TaskId = T2.TaskId
                        AND UT2.UserTaskId = UT.PrevUserTaskId
                        AND UT2.ResultId = R.ResultId
            ) AS PrevTaskResultDescrip
           ,UT.ResultId
           ,(
              SELECT    R.Descrip
              FROM      tmResults R
              WHERE     UT.ResultId = R.ResultId
            ) AS ResultDescrip
           ,T.CategoryId
           ,(
              SELECT    Descrip
              FROM      tmCategories
              WHERE     CategoryId = T.CategoryId
            ) AS CategoryDescrip
           ,T.ModuleEntityId
           ,M.ResourceId AS ModuleId
           ,M.Resource AS ModuleName
           ,E.ResourceId AS EntityId
           ,E.Resource AS EntityName
           ,UT.Status
           ,UT.ModDate
           ,UT.ModUser
           ,( CASE WHEN UT.StartDate IS NULL THEN 1
                   ELSE 0
              END )
    FROM    tmUserTasks UT
           ,tmTasks T
           ,syCmpGrpCmps CC
           ,syAdvantageResourceRelations R
           ,syResources E
           ,syResources M
    WHERE   UT.TaskId = T.TaskId
            AND CC.CampGrpId = T.CampGroupId
            AND R.ResourceId = E.ResourceId
            AND R.RelatedResourceId = M.ResourceId
            AND E.ResourceTypeId = 8
            AND M.ResourceTypeId = 1
            AND R.ResRelId = T.ModuleEntityId
            AND (
                  CC.CampusId = @CampusID
                  OR @CampusID IS NULL
                )
            AND (
                  UT.AssignedById = @AssignedByID
                  OR @AssignedByID IS NULL
                )
            AND (
                  @OwnerID IS NULL
                  OR UT.OwnerId IN ( SELECT strval
                                     FROM   dbo.SPLIT(@OwnerID) )
                )
            AND (
                  UT.ReID = @REID
                  OR @REID IS NULL
                )
            AND (
                  UT.Priority = @Priority
                  OR @Priority IS NULL
                )
            AND (
                  UT.StartDAte >= @StartDate
                  OR @StartDate IS NULL
                )
            AND (
                  UT.EndDAte <= @EndDate
                  OR @EndDate IS NULL
                )
            AND (
                  (
                    UT.OwnerId <> UT.AssignedById
                    AND UT.OwnerId <> @OwnerIDForAssignedtoMe
                  )
                  OR @OwnerIDForAssignedtoMe IS NULL
                )
    UNION
    SELECT 
        DISTINCT
            UT.UserTaskId
           ,UT.TaskId AS TaskID
           ,T.Descrip AS Descrip
           ,UT.StartDate
           ,UT.EndDate
           ,
      
   
--CASE 
--WHEN  EndDate IS NULL THEN  DATEADD(hour,1,UT.StartDate )
--ELSE  DATEADD(hour,1,UT.ENDDate )
--END 
--         AS EndDate ,
            UT.Priority AS Priority
           ,UT.Message
           ,UT.ReId
           ,(
              (
                SELECT  FirstName + ' ' + LastName
                FROM    arStudent S
                WHERE   S.StudentID = UT.ReId
              )
              UNION
              (
                SELECT  FirstName + ' ' + LastName
                FROM    adLeads
                WHERE   LeadId = UT.ReId
              )
              UNION
              (
                SELECT  FirstName + ' ' + LastName
                FROM    hrEmployees
                WHERE   EmpId = UT.ReId
              )
              UNION
              (
                SELECT  EmployerDescrip
                FROM    plEmployers
                WHERE   EmployerId = UT.ReId
              )
              UNION
              (
                SELECT  LenderDescrip
                FROM    faLenders
                WHERE   LenderId = UT.ReId
              )
            ) AS ReName
           ,(
              (
                SELECT TOP 1
                        C.CampDescrip
                FROM    arStudent S
                       ,arStuEnrollments SE
                       ,syCampuses C
                WHERE   S.StudentId = SE.StudentId
                        AND SE.CampusId = C.CampusId
                        AND S.StudentID = UT.ReId
              )
              UNION
              (
                SELECT TOP 1
                        C.CampDescrip
                FROM    adLeads L
                       ,syCampuses C
                WHERE   L.CampusId = C.CampusId
                        AND L.LeadId = UT.ReId
              )
              UNION
              (
                SELECT TOP 1
                        C.CampDescrip
                FROM    hrEmployees E
                       ,syCampuses C
                WHERE   E.CampusId = C.CampusId
                        AND E.EmpId = UT.ReId
              )
              UNION
              (
                SELECT TOP 1
                        C.CampDescrip
                FROM    plEmployers E
                       ,syCampuses C
                       ,syCmpGrpCmps CGC
                WHERE   E.CampGrpId = CGC.CampGrpId
                        AND C.CampusId = CGC.CampusId
                        AND E.EmployerId = UT.ReId
              )
              UNION
              (
                SELECT TOP 1
                        C.CampDescrip
                FROM    faLenders L
                       ,syCampuses C
                       ,syCmpGrpCmps CGC
                WHERE   L.CampGrpId = CGC.CampGrpId
                        AND C.CampusId = CGC.CampusId
                        AND L.LenderId = UT.ReId
              )
            ) AS CampusDescrip
           ,(
              (
                SELECT  Phone
                FROM    arStudentPhone S
                WHERE   S.StudentID = UT.ReId
                        AND default1 = 1
              )
              UNION
              (
                SELECT  Phone
                FROM    adLeads
                WHERE   LeadId = UT.ReId
              )
              UNION
              (
                SELECT  HomePhone
                FROM    hrEmpContactInfo
                WHERE   EmpId = UT.ReId
              )
              UNION
              (
                SELECT  Phone
                FROM    plEmployers
                WHERE   EmployerId = UT.ReId
              )
              UNION
              (
                SELECT  CustService
                FROM    faLenders
                WHERE   LenderId = UT.ReId
              )
            ) AS Phone
           ,(
              (
                SELECT  HomeEmail
                FROM    arStudent S
                WHERE   S.StudentID = UT.ReId
              )
              UNION
              (
                SELECT  HomeEmail
                FROM    adLeads
                WHERE   LeadId = UT.ReId
              )
              UNION
              (
                SELECT  HomeEmail
                FROM    hrEmpContactInfo
                WHERE   EmpId = UT.ReId
              )
              UNION
              (
                SELECT  Email
                FROM    plEmployers
                WHERE   EmployerId = UT.ReId
              )
              UNION
              (
                SELECT  Email
                FROM    faLenders
                WHERE   LenderId = UT.ReId
              )
            ) AS Email
           ,UT.OwnerId AS UserID
           ,(
              SELECT    username
              FROM      syUsers
              WHERE     UserId = UT.OwnerId
            ) AS UserName
           ,(
              SELECT    FullName
              FROM      syUsers
              WHERE     UserId = UT.OwnerId
            ) AS FullName
           ,UT.AssignedById
           ,(
              SELECT    FullName
              FROM      syUsers
              WHERE     UserId = UT.AssignedById
            ) AS AssignedByName
           ,UT.PrevUserTaskId
           ,(
              SELECT    T2.TaskId
              FROM      tmUserTasks UT2
                       ,tmTasks T2
              WHERE     UT2.TaskId = T2.TaskId
                        AND UT2.UserTaskId = UT.PrevUserTaskId
            ) AS PrevTaskId
           ,(
              SELECT    T2.Descrip
              FROM      tmUserTasks UT2
                       ,tmTasks T2
              WHERE     UT2.TaskId = T2.TaskId
                        AND UT2.UserTaskId = UT.PrevUserTaskId
            ) AS PrevTaskDescrip
           ,(
              SELECT    UT2.ResultId
              FROM      tmUserTasks UT2
                       ,tmTasks T2
              WHERE     UT2.TaskId = T2.TaskId
                        AND UT2.UserTaskId = UT.PrevUserTaskId
            ) AS PrevTaskResultId
           ,(
              SELECT    R.Descrip
              FROM      tmUserTasks UT2
                       ,tmTasks T2
                       ,tmResults R
              WHERE     UT2.TaskId = T2.TaskId
                        AND UT2.UserTaskId = UT.PrevUserTaskId
                        AND UT2.ResultId = R.ResultId
            ) AS PrevTaskResultDescrip
           ,UT.ResultId
           ,(
              SELECT    R.Descrip
              FROM      tmResults R
              WHERE     UT.ResultId = R.ResultId
            ) AS ResultDescrip
           ,T.CategoryId
           ,(
              SELECT    Descrip
              FROM      tmCategories
              WHERE     CategoryId = T.CategoryId
            ) AS CategoryDescrip
           ,T.ModuleEntityId
           ,M.ResourceId AS ModuleId
           ,M.Resource AS ModuleName
           ,E.ResourceId AS EntityId
           ,E.Resource AS EntityName
           ,UT.Status
           ,UT.ModDate
           ,UT.ModUser
           ,( CASE WHEN UT.StartDate IS NULL THEN 1
                   ELSE 0
              END )
    FROM    tmUserTasks UT
           ,tmTasks T
           ,syCmpGrpCmps CC
           ,syAdvantageResourceRelations R
           ,syResources E
           ,syResources M
    WHERE   UT.TaskId = T.TaskId
            AND CC.CampGrpId = T.CampGroupId
            AND R.ResourceId = E.ResourceId
            AND R.RelatedResourceId = M.ResourceId
            AND E.ResourceTypeId = 8
            AND M.ResourceTypeId = 1
            AND R.ResRelId = T.ModuleEntityId
            AND (
                  CC.CampusId = @CampusID
                  OR @CampusID IS NULL
                )
     --   AND (ut.AssignedById=@AssignedByID OR @AssignedByID IS NULL)
     --      And  ( @OwnerID IS NULL
      --                               OR ut.OwnerId IN (
      --                               SELECT strval
      --                               FROM   dbo.SPLIT(@OwnerID) ))
            AND UT.OwnerId = @OwnerIDForAssignedtoMe
            AND (
                  UT.ReID = @REID
                  OR @REID IS NULL
                )
            AND (
                  UT.Priority = @Priority
                  OR @Priority IS NULL
                )
            AND (
                  UT.StartDAte >= @StartDate
                  OR @StartDate IS NULL
                )
            AND (
                  UT.EndDAte <= @EndDate
                  OR @EndDate IS NULL
                );
  --   AND ((ut.OwnerId<>UT.AssignedById AND ut.OwnerId<>@OwnerIdForAssignedtoMe) OR @OwnerIDForAssignedtoMe IS NULL)
        




GO
