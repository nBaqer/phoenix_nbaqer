SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/* 
US4212 Transfer Partially Completed Components 

CREATED: 
7/8/2013 WMP

PURPOSE: 
Select classes available for a student to transfer partially completed components
	Requirements:
	- same course but different class section
	- current or future term
	- seats remaining
	- no student scheduling conflicts (handled in business rules, not this stored proc)

MODIFIED:
7/24/2013 WMP	DE9885	verify that student and class shifts match
7/26/2013 WMP	DE9908	update remaining spaces calculation
9/3/2013  WMP	US4366	renamed to add ByStudent
9/30/2013 WMP	DE10111 make campus specific

*/

CREATE PROCEDURE [dbo].[usp_AR_GetAvailableClassesForPartialTransfer_ByStudent]
    @ResultId UNIQUEIDENTIFIER = NULL
AS
    BEGIN

        DECLARE @StuEnrollId AS UNIQUEIDENTIFIER;
        DECLARE @ReqId AS UNIQUEIDENTIFIER;
        DECLARE @CurrentClsSectionId AS UNIQUEIDENTIFIER;
        DECLARE @StudentEnrollmentShiftId AS UNIQUEIDENTIFIER;
        DECLARE @BlankGUID UNIQUEIDENTIFIER = '00000000-0000-0000-0000-000000000000';
        DECLARE @CampusId UNIQUEIDENTIFIER;

        SELECT  @ReqId = c.ReqId
               ,@CurrentClsSectionId = r.TestId
               ,@CampusId = c.CampusId
               ,@StuEnrollId = r.StuEnrollId
               ,@StudentEnrollmentShiftId = e.ShiftId
        FROM    arResults r
        INNER JOIN arClassSections c ON c.ClsSectionId = r.TestId
        INNER JOIN arStuEnrollments e ON e.StuEnrollId = r.StuEnrollId
        WHERE   r.ResultId = @ResultId;
	
        SELECT  cs.ClsSectionId
               ,cs.ClsSection
               ,rq.Code AS CourseCode
               ,cs.MaxStud AS MaxStudents
               ,(
                  SELECT    COUNT(0)
                  FROM      arResults rs
                  WHERE     rs.TestId = cs.ClsSectionId
                ) AS Assigned
               ,cs.MaxStud - (
                               SELECT   COUNT(0)
                               FROM     arResults rs
                               WHERE    rs.TestId = cs.ClsSectionId
                             ) AS Remaining
               ,cs.StartDate AS ClsSectionStartDate
               ,cs.EndDate AS ClsSectionEndDate
               ,@StuEnrollId AS StuEnrollId
               ,t.TermCode
               ,t.TermDescrip
        FROM    arClassSections cs
        INNER JOIN arReqs rq ON rq.ReqId = cs.ReqId
        INNER JOIN arTerm t ON t.TermId = cs.TermId
        WHERE   cs.ReqId = @ReqId
                AND cs.CampusId = @CampusId
                AND cs.ClsSectionId <> @CurrentClsSectionId
                AND ISNULL(cs.ShiftId,@BlankGUID) = ISNULL(@StudentEnrollmentShiftId,@BlankGUID)
                AND cs.EndDate >= GETDATE()
                AND ( cs.MaxStud - (
                                     SELECT COUNT(0)
                                     FROM   arResults rs
                                     WHERE  rs.TestId = cs.ClsSectionId
                                   ) ) > 0
        ORDER BY cs.StartDate
               ,cs.ClsSection;

    END;



GO
