SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_GetAllActiveAndInActiveCampus] @campusId VARCHAR(50)
AS
    SET NOCOUNT ON;
    SELECT DISTINCT
            T.CampusId
           ,T.CampDescrip
           ,( CASE ST.Status
                WHEN 'Active' THEN 1
                ELSE 0
              END ) AS Status
    FROM    syCampuses T
           ,syStatuses ST
    WHERE   T.StatusId = ST.StatusId
            AND T.CampusId = @campusId
    ORDER BY Status
           ,T.CampDescrip;



GO
