SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[USP_FA_GetDetailedReportForAStudent9010]
    @StartDate DATETIME
   ,@EndDate DATETIME
   ,@StudentId AS VARCHAR(50)
   ,@StuEnrollId AS VARCHAR(50)
   ,@StudentIdentifier AS VARCHAR(50)
AS /*----------------------------------------------------------------------------------------------------
	Author : Vijay Ramteke
	
	Create date : 07/22/2010
	
	Procedure Name : USP_FA_GetDetailedReportForAStudent9010

	Objective : Get Revenue Ratio 90/10 ratio report for A Student
	
	Parameters : Name Type Data Type Required?
	
	Output : Returns the Revenue Ratio (90/10 ratio) report dataset for A Student
						
*/-----------------------------------------------------------------------------------------------------

    BEGIN

        SELECT  *
        INTO    #TempTrans
        FROM    satransactions
        WHERE   TransDate >= @StartDate
                AND TransDate <= @EndDate
                AND saTransactions.StuEnrollId IN ( SELECT  StuEnrollId
                                                    FROM    arStuEnrollments
                                                    WHERE   arStuEnrollments.StudentId = @StudentId
                                                            AND (
                                                                  @StuEnrollId IS NULL
                                                                  OR arStuEnrollments.StuEnrollId = @StuEnrollId
                                                                ) )
                AND TransTypeId IN ( 0,1,2 )
        ORDER BY TransDate;

        CREATE NONCLUSTERED INDEX INDX_Trans_TransId_DR ON #TempTrans (TransactionId);
        CREATE NONCLUSTERED INDEX INDX_Trans_StuEnrollment_DR ON #TempTrans (StuEnrollId);
        CREATE NONCLUSTERED INDEX INDX_Trans_TransAmount_DR ON #TempTrans (TransAmount);

        CREATE TABLE #tempRevinueRatio
            (
             RowId INT IDENTITY(1,1)
                       NOT NULL
            ,AGID UNIQUEIDENTIFIER
            ,LastName VARCHAR(50)
            ,FirstName VARCHAR(50)
            ,MiddleName VARCHAR(50)
            ,StudentIdentifier VARCHAR(50)
            ,CampusId UNIQUEIDENTIFIER
            ,CampDescrip VARCHAR(500)
            ,Numerator DECIMAL(18,4)
            ,Denominator DECIMAL(18,4)
            ,TitleIV INT
            ,RefundTypeId INT
            ,CashPayment DECIMAL(18,4)
            ,RefundApp DECIMAL(18,4)
            ,TransDate DATETIME
            ,TransDescrip VARCHAR(500)
            ,TransCode VARCHAR(500)
            ,TransType VARCHAR(50)
            ,Amount DECIMAL(18,4)
            ,PrgVerDescrip VARCHAR(500)
            ,TransactionId UNIQUEIDENTIFIER
            ,FinalNumerator DECIMAL(18,4) NULL
            ,FinalDenominator DECIMAL(18,4) NULL
            ,CreateDate DATETIME NULL
            ,ModDate DATETIME NULL
            ,DisbNum INT NULL
            );
  
        SELECT  *
        INTO    #tempRevinueRatio1
        FROM    (
                  --Title IV --
		
		-- 1. Payments --
                  SELECT    NEWID() AS AGID
                           ,S.LastName
                           ,S.FirstName
                           ,S.MiddleName
                           ,CASE @StudentIdentifier
                              WHEN 'StudentId' THEN S.StudentNumber
                              WHEN 'EnrollmentId' THEN SE.EnrollmentId
                              ELSE S.SSN
                            END AS StudentIdentifier
                           ,SE.CampusId
                           ,C.CampDescrip
                           ,CONVERT(DECIMAL(18,4),CONVERT(DECIMAL(18,4),0.00)) AS Numerator
                           ,CONVERT(DECIMAL(18,4),0.00) AS Denominator
                           ,FS.TitleIV
                           ,2 AS RefundTypeId
                           ,(
                              SELECT    ISNULL(( SUM(COALESCE(-1 * T2.TransAmount,CONVERT(DECIMAL(18,4),0.00))) ),CONVERT(DECIMAL(18,4),0.00))
                              FROM      #TempTrans T2
                                       ,dbo.saFundSources FS2
                              WHERE     T2.TransDate >= @StartDate
                                        AND T2.TransDate < T1.TransDate
                                        AND T2.TransTypeId IN ( 2 )
                                        AND T2.StuEnrollId = T1.StuEnrollId
                                        AND T2.FundSourceId = FS2.FundSourceId
                                        AND FS2.AdvFundSourceId IN ( 1,19,23 )
                            ) AS CashPayment
                           ,CONVERT(DECIMAL(18,4),0.00) AS RefundApp
                           ,T1.TransDate
                           ,T1.TransDescrip
                           ,(
                              SELECT    TransCodeDescrip
                              FROM      saTransCodes TCI
                              WHERE     T1.TransCodeId = TCI.TransCodeId
                            ) AS TransCode
                           ,(
                              SELECT    Description
                              FROM      saTransTypes TT
                              WHERE     T1.TransTypeId = TT.TransTypeId
                            ) AS TransType
                           ,T1.TransAmount AS Amount
                           ,(
                              SELECT    LTRIM(RTRIM(PrgVerDescrip)) + ' (' + LTRIM(RTRIM(PrgVerCode)) + ')'
                              FROM      arPrgVersions PV
                              WHERE     PV.PrgVerId = SE.PrgVerId
                            ) AS PrgVerDescrip
                           ,T1.TransactionId
                           ,T1.CreateDate
                           ,T1.ModDate
                           ,1 AS DisbNum
                  FROM      #TempTrans T1
                           ,saFundSources FS
                           ,arStuEnrollments SE
                           ,arStudent S
                           ,syCampuses C
                  WHERE     T1.voided = 0
                            AND T1.FundSourceId = FS.FundSourceId
                            AND FS.TitleIV = 1
                            AND T1.StuEnrollId = SE.StuEnrollId
                            AND SE.StudentId = S.StudentId
                            AND C.CampusId = SE.CampusId
                            AND (
                                  (
                                    SELECT  COUNT(TransCodeId)
                                    FROM    saTransCodes TC
                                    WHERE   TC.TransCodeId = T1.TransCodeId
                                            AND TC.IsInstCharge = 1
                                  ) > 0
                                  OR (
                                       SELECT   COUNT(T2.TransCodeId)
                                       FROM     saTransCodes TC
                                               ,saTransactions T2
                                               ,saAppliedPayments AP1
                                       WHERE    AP1.TransactionId = T1.TransactionId
                                                AND AP1.ApplyToTransId = T2.TransactionId
                                                AND TC.TransCodeId = T2.TransCodeId
                                                AND TC.IsInstCharge = 1
                                     ) > 0
                                )
                            AND T1.TransTypeId IN ( 1,2 )	  
		
		-- 1. Payments --
                  UNION
		
		-- 2. Refunds --
                  SELECT    NEWID() AS AGID
                           ,S.LastName
                           ,S.FirstName
                           ,S.MiddleName
                           ,CASE @StudentIdentifier
                              WHEN 'StudentId' THEN S.StudentNumber
                              WHEN 'EnrollmentId' THEN SE.EnrollmentId
                              ELSE S.SSN
                            END AS StudentIdentifier
                           ,SE.CampusId
                           ,C.CampDescrip
                           ,CONVERT(DECIMAL(18,4),0.00) AS Numerator
                           ,CONVERT(DECIMAL(18,4),0.00) AS Denominator
                           ,FS.TitleIV
                           ,CASE WHEN R.RefundTypeId > 0 THEN 1
                                 ELSE 0
                            END AS RefundTypeId
                           ,(
                              SELECT    ISNULL(( SUM(COALESCE(-1 * T2.TransAmount,CONVERT(DECIMAL(18,4),0.00))) ),CONVERT(DECIMAL(18,4),0.00))
                              FROM      #TempTrans T2
                                       ,dbo.saFundSources FS2
                              WHERE     T2.TransDate >= @StartDate
                                        AND T2.TransDate < T1.TransDate
                                        AND T2.TransTypeId IN ( 2 )
                                        AND T2.StuEnrollId = T1.StuEnrollId
                                        AND T2.FundSourceId = FS2.FundSourceId
                                        AND FS2.AdvFundSourceId IN ( 1,19,23 )
                            ) AS CashPayment
                           ,CONVERT(DECIMAL(18,4),0.00) AS RefundApp
                           ,T1.TransDate
                           ,T1.TransDescrip
                           ,(
                              SELECT    TransCodeDescrip
                              FROM      saTransCodes TCI
                              WHERE     T1.TransCodeId = TCI.TransCodeId
                            ) AS TransCode
                           ,(
                              SELECT    Description
                              FROM      saTransTypes TT
                              WHERE     T1.TransTypeId = TT.TransTypeId
                            ) AS TransType
                           ,-1 * COALESCE(-1 * T1.TransAmount,CONVERT(DECIMAL(18,4),0.00)) AS Amount
                           ,(
                              SELECT    LTRIM(RTRIM(PrgVerDescrip)) + ' (' + LTRIM(RTRIM(PrgVerCode)) + ')'
                              FROM      arPrgVersions PV
                              WHERE     PV.PrgVerId = SE.PrgVerId
                            ) AS PrgVerDescrip
                           ,T1.TransactionId
                           ,T1.CreateDate
                           ,T1.ModDate
                           ,0 AS DisbNum
                  FROM      #TempTrans T1
                           ,saRefunds R
                           ,saFundSources FS
                           ,arStuEnrollments SE
                           ,arStudent S
                           ,syCampuses C
                  WHERE     T1.voided = 0
                            AND T1.TransactionId = R.TransactionId
                            AND R.FundSourceId = FS.FundSourceId
                            AND FS.TitleIV = 1
                            AND T1.StuEnrollId = SE.StuEnrollId
                            AND SE.StudentId = S.StudentId
                            AND C.CampusId = SE.CampusId
                            AND ( (
                                    SELECT  COUNT(TransCodeId)
                                    FROM    saTransCodes TC
                                    WHERE   TC.TransCodeId = T1.TransCodeId
                                            AND TC.IsInstCharge = 1
                                  ) > 0 )
                            AND T1.TransTypeId IN ( 1,2 )
		
		-- 2. Refunds --

		 
		---Title IV---
                  UNION
		
		-- Non Title IV--
		
		-- 4. Payments --
                  SELECT    NEWID() AS AGID
                           ,S.LastName
                           ,S.FirstName
                           ,S.MiddleName
                           ,CASE @StudentIdentifier
                              WHEN 'StudentId' THEN S.StudentNumber
                              WHEN 'EnrollmentId' THEN SE.EnrollmentId
                              ELSE S.SSN
                            END AS StudentIdentifier
                           ,SE.CampusId
                           ,C.CampDescrip
                           ,CONVERT(DECIMAL(18,4),0.00) AS Numerator
                           ,CONVERT(DECIMAL(18,4),0.00) AS Denominator
                           ,0 AS TitleIV
                           ,2 AS RefundTypeId
                           ,(
                              SELECT    ISNULL(( SUM(COALESCE(-1 * T2.TransAmount,CONVERT(DECIMAL(18,4),0.00))) ),CONVERT(DECIMAL(18,4),0.00))
                              FROM      #TempTrans T2
                                       ,dbo.saFundSources FS2
                              WHERE     T2.TransDate >= @StartDate
                                        AND T2.TransDate < T1.TransDate
                                        AND T2.TransTypeId IN ( 2 )
                                        AND T2.StuEnrollId = T1.StuEnrollId
                                        AND T2.FundSourceId = FS2.FundSourceId
                                        AND FS2.AdvFundSourceId IN ( 1,19,23 )
                            ) AS CashPayment
                           ,CONVERT(DECIMAL(18,4),0.00) AS RefundApp
                           ,T1.TransDate
                           ,T1.TransDescrip
                           ,(
                              SELECT    TransCodeDescrip
                              FROM      saTransCodes TCI
                              WHERE     T1.TransCodeId = TCI.TransCodeId
                            ) AS TransCode
                           ,(
                              SELECT    Description
                              FROM      saTransTypes TT
                              WHERE     T1.TransTypeId = TT.TransTypeId
                            ) AS TransType
                           ,T1.TransAmount AS Amount
                           ,(
                              SELECT    LTRIM(RTRIM(PrgVerDescrip)) + ' (' + LTRIM(RTRIM(PrgVerCode)) + ')'
                              FROM      arPrgVersions PV
                              WHERE     PV.PrgVerId = SE.PrgVerId
                            ) AS PrgVerDescrip
                           ,T1.TransactionId
                           ,T1.CreateDate
                           ,T1.ModDate
                           ,1 AS DisbNum
                  FROM      #TempTrans T1
                           ,saFundSources FS
                           ,arStuEnrollments SE
                           ,arStudent S
                           ,syCampuses C
                  WHERE     T1.voided = 0
                            AND T1.FundSourceId = FS.FundSourceId
                            AND FS.TitleIV = 0
                            AND T1.StuEnrollId = SE.StuEnrollId
                            AND SE.StudentId = S.StudentId
                            AND C.CampusId = SE.CampusId
                            AND T1.Voided = 0
                            AND (
                                  (
                                    SELECT  COUNT(TransCodeId)
                                    FROM    saTransCodes TC
                                    WHERE   TC.TransCodeId = T1.TransCodeId
                                            AND TC.IsInstCharge = 1
                                  ) > 0
                                  OR (
                                       SELECT   COUNT(T2.TransCodeId)
                                       FROM     saTransCodes TC
                                               ,saTransactions T2
                                               ,saAppliedPayments AP1
                                       WHERE    AP1.TransactionId = T1.TransactionId
                                                AND AP1.ApplyToTransId = T2.TransactionId
                                                AND TC.TransCodeId = T2.TransCodeId
                                                AND TC.IsInstCharge = 1
                                     ) > 0
                                )
                            AND T1.TransTypeId IN ( 1,2 )	  
		
		-- 4. Payments --
                  UNION
		
		-- 5. Refunds --
                  SELECT    NEWID() AS AGID
                           ,S.LastName
                           ,S.FirstName
                           ,S.MiddleName
                           ,CASE @StudentIdentifier
                              WHEN 'StudentId' THEN S.StudentNumber
                              WHEN 'EnrollmentId' THEN SE.EnrollmentId
                              ELSE S.SSN
                            END AS StudentIdentifier
                           ,SE.CampusId
                           ,C.CampDescrip
                           ,CONVERT(DECIMAL(18,4),0.00) AS Numerator
                           ,CONVERT(DECIMAL(18,4),0.00) AS Denominator
                           ,0 AS TitleIV
                           ,CASE WHEN R.RefundTypeId > 0 THEN 1
                                 ELSE 0
                            END AS RefundTypeId
                           ,(
                              SELECT    ISNULL(( SUM(COALESCE(-1 * T2.TransAmount,CONVERT(DECIMAL(18,4),0.00))) ),CONVERT(DECIMAL(18,4),0.00))
                              FROM      #TempTrans T2
                                       ,dbo.saFundSources FS2
                              WHERE     T2.TransDate >= @StartDate
                                        AND T2.TransDate < T1.TransDate
                                        AND T2.TransTypeId IN ( 2 )
                                        AND T2.StuEnrollId = T1.StuEnrollId
                                        AND T2.FundSourceId = FS2.FundSourceId
                                        AND FS2.AdvFundSourceId IN ( 1,19,23 )
                            ) AS CashPayment
                           ,CONVERT(DECIMAL(18,4),0.00) AS RefundApp
                           ,T1.TransDate
                           ,T1.TransDescrip
                           ,(
                              SELECT    TransCodeDescrip
                              FROM      saTransCodes TCI
                              WHERE     T1.TransCodeId = TCI.TransCodeId
                            ) AS TransCode
                           ,(
                              SELECT    Description
                              FROM      saTransTypes TT
                              WHERE     T1.TransTypeId = TT.TransTypeId
                            ) AS TransType
                           ,-1 * COALESCE(-1 * T1.TransAmount,CONVERT(DECIMAL(18,4),0.00)) AS Amount
                           ,(
                              SELECT    LTRIM(RTRIM(PrgVerDescrip)) + ' (' + LTRIM(RTRIM(PrgVerCode)) + ')'
                              FROM      arPrgVersions PV
                              WHERE     PV.PrgVerId = SE.PrgVerId
                            ) AS PrgVerDescrip
                           ,T1.TransactionId
                           ,T1.CreateDate
                           ,T1.ModDate
                           ,1 AS DisbNum
                  FROM      #TempTrans T1
                           ,saRefunds R
                           ,saFundSources FS
                           ,arStuEnrollments SE
                           ,arStudent S
                           ,syCampuses C
                  WHERE     T1.voided = 0
                            AND T1.TransactionId = R.TransactionId
                            AND R.FundSourceId = FS.FundSourceId
                            AND FS.TitleIV = 0
                            AND T1.StuEnrollId = SE.StuEnrollId
                            AND SE.StudentId = S.StudentId
                            AND C.CampusId = SE.CampusId
                            AND T1.Voided = 0
                            AND ( (
                                    SELECT  COUNT(TransCodeId)
                                    FROM    saTransCodes TC
                                    WHERE   TC.TransCodeId = T1.TransCodeId
                                            AND TC.IsInstCharge = 1
                                  ) > 0 )
                            AND T1.TransTypeId IN ( 1,2 )	   
		
		-- 5. Refunds --

		
		-- Non Title IV--

		--Student Refunds with no fund source
                  UNION
                  SELECT    NEWID() AS AGID
                           ,S.LastName
                           ,S.FirstName
                           ,S.MiddleName
                           ,CASE @StudentIdentifier
                              WHEN 'StudentId' THEN S.StudentNumber
                              WHEN 'EnrollmentId' THEN SE.EnrollmentId
                              ELSE S.SSN
                            END AS StudentIdentifier
                           ,SE.CampusId
                           ,C.CampDescrip
                           ,CONVERT(DECIMAL(18,4),0.00) AS Numerator
                           ,CONVERT(DECIMAL(18,4),0.00) AS Denominator
                           ,0 AS TitleIV
                           ,0 AS RefundTypeId
                           ,(
                              SELECT    ISNULL(( SUM(COALESCE(-1 * T2.TransAmount,CONVERT(DECIMAL(18,4),0.00))) ),CONVERT(DECIMAL(18,4),0.00))
                              FROM      #TempTrans T2
                                       ,dbo.saFundSources FS2
                              WHERE     T2.TransDate >= @StartDate
                                        AND T2.TransDate < T1.TransDate
                                        AND T2.TransTypeId IN ( 2 )
                                        AND T2.StuEnrollId = T1.StuEnrollId
                                        AND T2.FundSourceId = FS2.FundSourceId
                                        AND FS2.AdvFundSourceId IN ( 1,19,23 )
                            ) AS CashPayment
                           ,CONVERT(DECIMAL(18,4),0.00) AS RefundApp
                           ,T1.TransDate
                           ,T1.TransDescrip
                           ,(
                              SELECT    TransCodeDescrip
                              FROM      saTransCodes TCI
                              WHERE     T1.TransCodeId = TCI.TransCodeId
                            ) AS TransCode
                           ,(
                              SELECT    Description
                              FROM      saTransTypes TT
                              WHERE     T1.TransTypeId = TT.TransTypeId
                            ) AS TransType
                           ,-1 * COALESCE(-1 * T1.TransAmount,CONVERT(DECIMAL(18,4),0.00)) AS Amount
                           ,(
                              SELECT    LTRIM(RTRIM(PrgVerDescrip)) + ' (' + LTRIM(RTRIM(PrgVerCode)) + ')'
                              FROM      arPrgVersions PV
                              WHERE     PV.PrgVerId = SE.PrgVerId
                            ) AS PrgVerDescrip
                           ,T1.TransactionId
                           ,T1.CreateDate
                           ,T1.ModDate
                           ,1 AS DisbNum
                  FROM      #TempTrans T1
                           ,saRefunds R
                           ,arStuEnrollments SE
                           ,arStudent S
                           ,syCampuses C
                  WHERE     T1.voided = 0
                            AND T1.TransactionId = R.TransactionId
                            AND R.RefundTypeId = 0
                            AND R.FundSourceId IS NULL
                            AND T1.StuEnrollId = SE.StuEnrollId
                            AND SE.StudentId = S.StudentId
                            AND C.CampusId = SE.CampusId
                            AND T1.Voided = 0
                            AND ( (
                                    SELECT  COUNT(TransCodeId)
                                    FROM    saTransCodes TC
                                    WHERE   TC.TransCodeId = T1.TransCodeId
                                            AND TC.IsInstCharge = 1
                                  ) > 0 )
                            AND T1.TransTypeId IN ( 1,2 )
                ) tt;

        INSERT  INTO #tempRevinueRatio
                (
                 AGID
                ,LastName
                ,FirstName
                ,MiddleName
                ,StudentIdentifier
                ,CampusId
                ,CampDescrip
                ,Numerator
                ,Denominator
                ,TitleIV
                ,RefundTypeId
                ,CashPayment
                ,RefundApp
                ,TransDate
                ,TransDescrip
                ,TransCode
                ,TransType
                ,Amount
                ,PrgVerDescrip
                ,TransactionId
                ,CreateDate
                ,ModDate
                ,DisbNum
				)
                SELECT  *
                FROM    #tempRevinueRatio1
                ORDER BY TransDate
                       ,ModDate
                       ,CreateDate
                       ,Amount DESC;

 
--Select * from #tempRevinueRatio1 order by TransDate
--Select * from #tempRevinueRatio order by TransDate
 
 
  
        DECLARE @TransAmount DECIMAL(18,4);
        DECLARE @TransDate DATETIME;
        DECLARE @TransactionId UNIQUEIDENTIFIER;
        DECLARE @TitlIV INT;
        DECLARE @RefundType INT;
        DECLARE @Num DECIMAL(18,4);
        DECLARE @Deno DECIMAL(18,4);
        DECLARE @RC INT;

        DECLARE @TitleIVPayment DECIMAL(18,4);
        DECLARE @NonTitleIVPayment DECIMAL(18,4);
        DECLARE @RefundAmount DECIMAL(18,4);
        DECLARE @TIVRefundAmount DECIMAL(18,4);

        DECLARE RefundApp CURSOR
        FOR
            SELECT  -1 * Amount
                   ,TitleIV
                   ,RefundTypeId
                   ,TransDate
                   ,TransactionId
                   ,RowId
            FROM    #tempRevinueRatio
            ORDER BY RowId;


        OPEN RefundApp;

-- Perform the first fetch.
        FETCH NEXT FROM RefundApp INTO @TransAmount,@TitlIV,@RefundType,@TransDate,@TransactionId,@RC;

-- Check @@FETCH_STATUS to see if there are any more rows to fetch.
        WHILE @@FETCH_STATUS = 0
            BEGIN
	
                SET @Num = 0.0000;
                SET @Deno = 0.0000;
                SET @TitleIVPayment = 0.0000;
                SET @NonTitleIVPayment = 0.0000;
                SET @RefundAmount = 0.0000;
                SET @TIVRefundAmount = 0.0000;
	
                SET @TitleIVPayment = (
                                        SELECT  ISNULL(SUM(-1 * Amount),CONVERT(DECIMAL(18,2),0.00))
                                        FROM    #tempRevinueRatio
                                        WHERE   TitleIV = 1
                                                AND RefundTypeId = 2
                                                AND RowId <= @RC
                                      );
                SET @NonTitleIVPayment = (
                                           SELECT   ISNULL(SUM(-1 * Amount),CONVERT(DECIMAL(18,2),0.00))
                                           FROM     #tempRevinueRatio
                                           WHERE    TitleIV = 0
                                                    AND RefundTypeId = 2
                                                    AND RowId <= @RC
                                         );
                SET @RefundAmount = (
                                      SELECT    ISNULL(SUM(-1 * Amount),CONVERT(DECIMAL(18,2),0.00))
                                      FROM      #tempRevinueRatio
                                      WHERE     (
                                                  (
                                                    RefundTypeId IN ( 0,1 )
                                                    AND TitleIV = 0
                                                  )
                                                  OR (
                                                       RefundTypeId IN ( 0 )
                                                       AND TitleIV = 1
                                                     )
                                                )
                                                AND RowId <= @RC
                                    );
                SET @TIVRefundAmount = (
                                         SELECT ISNULL(SUM(-1 * Amount),CONVERT(DECIMAL(18,2),0.00))
                                         FROM   #tempRevinueRatio
                                         WHERE  TitleIV = 1
                                                AND RefundTypeId = 1
                                                AND RowId <= @RC
                                       );
	
				--Print @RC
				--print @TitleIVPayment
				--print @NonTitleIVPayment
				--print @RefundAmount
				--print @TIVRefundAmount
	
                IF @NonTitleIVPayment + @RefundAmount > 0
                    BEGIN
                        SET @Num = @TitleIVPayment + @TIVRefundAmount;
                    END;
                ELSE
                    BEGIN
                        SET @Num = @TitleIVPayment + @NonTitleIVPayment + @RefundAmount + @TIVRefundAmount;
                    END;
                SET @Deno = @TitleIVPayment + @NonTitleIVPayment + @RefundAmount + @TIVRefundAmount;
                UPDATE  #tempRevinueRatio
                SET     Numerator = @Num
                       ,Denominator = @Deno
                WHERE   TransactionId = @TransactionId;

				--PRINT @Num
				--PRINT @deno

                FETCH NEXT FROM RefundApp INTO @TransAmount,@TitlIV,@RefundType,@TransDate,@TransactionId,@RC;
            END;

        CLOSE RefundApp;
        DEALLOCATE RefundApp;

        DECLARE @FinalNum DECIMAL(18,4);
        DECLARE @FinalDeno DECIMAL(18,4);

        SET @FinalNum = (
                          SELECT    Numerator
                          FROM      #tempRevinueRatio
                          WHERE     RowId = (
                                              SELECT    MAX(RowId)
                                              FROM      #tempRevinueRatio
                                            )
                        );
        SET @FinalDeno = (
                           SELECT   Denominator
                           FROM     #tempRevinueRatio
                           WHERE    RowId = (
                                              SELECT    MAX(RowId)
                                              FROM      #tempRevinueRatio
                                            )
                         );

        UPDATE  #tempRevinueRatio
        SET     FinalNumerator = @FinalNum
               ,FinalDenominator = @FinalDeno;

 
 
        SELECT DISTINCT
                LastName
               ,FirstName
               ,MiddleName
               ,StudentIdentifier
               ,CampusId
               ,CampDescrip
               ,PrgVerDescrip
               ,TransDate
               ,TransCode
               ,TransDescrip
               ,TransType
               ,Amount
               ,Numerator
               ,Denominator
               ,TransactionId
               ,FinalNumerator
               ,FinalDenominator
               ,CreateDate
               ,ModDate
               ,DisbNum
        FROM    #tempRevinueRatio
        UNION
        SELECT DISTINCT
                S.LastName
               ,S.FirstName
               ,S.MiddleName
               ,CASE @StudentIdentifier
                  WHEN 'StudentId' THEN S.StudentNumber
                  WHEN 'EnrollmentId' THEN SE.EnrollmentId
                  ELSE S.SSN
                END AS StudentIdentifier
               ,SE.CampusId
               ,C.CampDescrip
               ,(
                  SELECT    LTRIM(RTRIM(PrgVerDescrip)) + ' (' + LTRIM(RTRIM(PrgVerCode)) + ')'
                  FROM      arPrgVersions PV
                  WHERE     PV.PrgVerId = SE.PrgVerId
                ) AS PrgVerDescrip
               ,T1.TransDate
               ,(
                  SELECT    TransCodeDescrip
                  FROM      saTransCodes TCI
                  WHERE     T1.TransCodeId = TCI.TransCodeId
                ) AS TransCode
               ,T1.TransDescrip
               ,(
                  SELECT    Description
                  FROM      saTransTypes TT
                  WHERE     T1.TransTypeId = TT.TransTypeId
                ) AS TransType
               ,T1.TransAmount AS Amount
               ,0.00 AS Numerator
               ,0.00 AS Denominator
               ,T1.TransactionId
               ,@FinalNum AS FinalNumerator
               ,@FinalDeno AS FinalDenominator
               ,T1.CreateDate
               ,T1.ModDate
               ,0 AS DisbNum
        FROM    #TempTrans T1
               ,arStuEnrollments SE
               ,arStudent S
               ,syCampuses C
        WHERE   T1.voided = 0
                AND T1.StuEnrollId = SE.StuEnrollId
                AND SE.StudentId = S.StudentId
                AND C.CampusId = SE.CampusId
                AND T1.TransactionId NOT IN ( SELECT    TransactionId
                                              FROM      #tempRevinueRatio )
        ORDER BY LastName
               ,FirstName
               ,TransDate
               ,ModDate
               ,CreateDate
               ,DisbNum
               ,Amount DESC;
 
 

        DROP TABLE #TempTrans;
        DROP TABLE #tempRevinueRatio;
        DROP TABLE #tempRevinueRatio1;
    END;



GO
