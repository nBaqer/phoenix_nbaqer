SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[USP_IPEDS_FallCompletion_CompletersByLevel_SummaryAge_GetList]
    @CampusId VARCHAR(50)
   ,@ProgId VARCHAR(4000) = NULL
   ,@StartDate DATETIME = NULL
   ,@EndDate DATETIME = NULL
AS
    BEGIN
 
        DECLARE @Age AS INT;
    
        SELECT  Age
               ,AgeSequence
               ,NEWID() AS RowNumber
               ,SUM(Seq1Count) AS Seq1Count
               ,SUM(Seq2Count) AS Seq2Count
               ,SUM(Seq3Count) AS Seq3Count
               ,SUM(Seq4Count) AS Seq4Count
               ,SUM(Seq5Count) AS Seq5Count
               ,SUM(Seq6Count) AS Seq6Count
               ,SUM(Seq7Count) AS Seq7Count
        FROM    (
                  SELECT    NEWID() AS RowNumber
                           ,Studentid
                           ,SSN
                           ,StudentNumber
                           ,StudentName
                           ,Age
                           ,AgeSequence
                           ,Seq1
                           ,seq2
                           ,seq3
                           ,seq4
                           ,seq5
                           ,seq6
                           ,seq7
                           ,CASE Seq1
                              WHEN 'X' THEN 1
                              ELSE 0
                            END AS Seq1Count
                           ,CASE seq2
                              WHEN 'X' THEN 1
                              ELSE 0
                            END AS Seq2Count
                           ,CASE seq3
                              WHEN 'X' THEN 1
                              ELSE 0
                            END AS Seq3Count
                           ,CASE seq4
                              WHEN 'X' THEN 1
                              ELSE 0
                            END AS Seq4Count
                           ,CASE seq5
                              WHEN 'X' THEN 1
                              ELSE 0
                            END AS Seq5Count
                           ,CASE seq6
                              WHEN 'X' THEN 1
                              ELSE 0
                            END AS Seq6Count
                           ,CASE seq7
                              WHEN 'X' THEN 1
                              ELSE 0
                            END AS Seq7Count
                  FROM      (
                              SELECT    NULL AS Studentid
                                       ,NULL AS SSN
                                       ,NULL AS StudentNumber
                                       ,NULL AS StudentName
                                       ,1 AS AgeSequence
                                       ,'Under 18' AS Age
                                       ,'' AS Seq1
                                       ,'' AS seq2
                                       ,'' AS seq3
                                       ,'' AS seq4
                                       ,'' AS seq5
                                       ,'' AS seq6
                                       ,'' AS seq7
                              UNION
                              SELECT    NULL AS Studentid
                                       ,NULL AS SSN
                                       ,NULL AS StudentNumber
                                       ,NULL AS StudentName
                                       ,2 AS AgeSequence
                                       ,'18 - 24' AS Age
                                       ,'' AS Seq1
                                       ,'' AS seq2
                                       ,'' AS seq3
                                       ,'' AS seq4
                                       ,'' AS seq5
                                       ,'' AS seq6
                                       ,'' AS seq7
                              UNION
                              SELECT    NULL AS Studentid
                                       ,NULL AS SSN
                                       ,NULL AS StudentNumber
                                       ,NULL AS StudentName
                                       ,3 AS AgeSequence
                                       ,'25 - 39' AS Age
                                       ,'' AS Seq1
                                       ,'' AS seq2
                                       ,'' AS seq3
                                       ,'' AS seq4
                                       ,'' AS seq5
                                       ,'' AS seq6
                                       ,'' AS seq7
                              UNION
                              SELECT    NULL AS Studentid
                                       ,NULL AS SSN
                                       ,NULL AS StudentNumber
                                       ,NULL AS StudentName
                                       ,4 AS AgeSequence
                                       ,'40 and above' AS Age
                                       ,'' AS Seq1
                                       ,'' AS seq2
                                       ,'' AS seq3
                                       ,'' AS seq4
                                       ,'' AS seq5
                                       ,'' AS seq6
                                       ,'' AS seq7
                              UNION
                              SELECT    NULL AS Studentid
                                       ,NULL AS SSN
                                       ,NULL AS StudentNumber
                                       ,NULL AS StudentName
                                       ,5 AS AgeSequence
                                       ,'Unknown' AS Age
                                       ,'' AS Seq1
                                       ,'' AS seq2
                                       ,'' AS seq3
                                       ,'' AS seq4
                                       ,'' AS seq5
                                       ,'' AS seq6
                                       ,'' AS seq7
                              UNION
                              SELECT    Studentid
                                       ,SSN
                                       ,StudentNumber
                                       ,StudentName
                                       ,(
                                          SELECT TOP 1
                                                    CASE WHEN CAST(( DATEDIFF(mm,C4.DOB,C1.ExpGradDate) / 12 ) AS INT) < 18 THEN 1
                                                         WHEN CAST(( DATEDIFF(mm,C4.DOB,C1.ExpGradDate) / 12 ) AS INT) >= 18
                                                              AND CAST(( DATEDIFF(mm,C4.DOB,C1.ExpGradDate) / 12 ) AS INT) < 25 THEN 2
                                                         WHEN CAST(( DATEDIFF(mm,C4.DOB,C1.ExpGradDate) / 12 ) AS INT) >= 25
                                                              AND CAST(( DATEDIFF(mm,C4.DOB,C1.ExpGradDate) / 12 ) AS INT) < 40 THEN 3
                                                         WHEN CAST(( DATEDIFF(mm,C4.DOB,C1.ExpGradDate) / 12 ) AS INT) >= 40 THEN 4
                                                         ELSE 5
                                                    END
                                          FROM      arStudent C4
                                                   ,arStuEnrollments C1
                                                   ,arPrgVersions C2
                                                   ,arProgTypes C3
                                          WHERE     C4.StudentId = C1.StudentId
                                                    AND C1.PrgVerId = C2.PrgVerId
                                                    AND C2.ProgTypId = C3.ProgTypId
                                                    AND C1.StudentId = dt1.StudentId
                                                    AND C1.ExpGradDate >= @StartDate
                                                    AND C1.ExpGradDate <= @EndDate
                                          ORDER BY  C1.ExpGradDate DESC
                                        ) AS AgeSequence
                                       ,(
                                          SELECT TOP 1
                                                    CASE WHEN CAST(( DATEDIFF(mm,C4.DOB,C1.ExpGradDate) / 12 ) AS INT) < 18 THEN 'Under 18'
                                                         WHEN CAST(( DATEDIFF(mm,C4.DOB,C1.ExpGradDate) / 12 ) AS INT) >= 18
                                                              AND CAST(( DATEDIFF(mm,C4.DOB,C1.ExpGradDate) / 12 ) AS INT) < 25 THEN '18 - 24'
                                                         WHEN CAST(( DATEDIFF(mm,C4.DOB,C1.ExpGradDate) / 12 ) AS INT) >= 25
                                                              AND CAST(( DATEDIFF(mm,C4.DOB,C1.ExpGradDate) / 12 ) AS INT) < 40 THEN '25 - 39'
                                                         WHEN CAST(( DATEDIFF(mm,C4.DOB,C1.ExpGradDate) / 12 ) AS INT) >= 40 THEN '40 and above'
                                                         ELSE 'Unknown'
                                                    END
                                          FROM      arStudent C4
                                                   ,arStuEnrollments C1
                                                   ,arPrgVersions C2
                                                   ,arProgTypes C3
                                          WHERE     C4.StudentId = C1.StudentId
                                                    AND C1.PrgVerId = C2.PrgVerId
                                                    AND C2.ProgTypId = C3.ProgTypId
                                                    AND C1.StudentId = dt1.StudentId
                                                    AND C1.ExpGradDate >= @StartDate
                                                    AND C1.ExpGradDate <= @EndDate
                                          ORDER BY  C1.ExpGradDate DESC
                                        ) AS Age
                                       ,Seq1
                                       ,Seq2
                                       ,Seq3
                                       ,Seq4
                                       ,Seq5
                                       ,Seq6
                                       ,Seq7
                              FROM      (
                                          SELECT DISTINCT
                                                    t1.StudentId
                                                   ,t1.SSN
                                                   ,t1.StudentNumber
                                                   ,t1.LastName + ', ' + t1.FirstName + ' ' + ISNULL(t1.MiddleName,'') AS StudentName
                                                   ,CASE WHEN t13.IPEDSValue = 154 THEN 'X'
                                                         ELSE ''
                                                    END AS Seq1
                                                   ,CASE WHEN t13.IPEDSValue = 155 THEN 'X'
                                                         ELSE ''
                                                    END AS Seq2
                                                   ,CASE WHEN t13.IPEDSValue = 156 THEN 'X'
                                                         ELSE ''
                                                    END AS Seq3
                                                   ,CASE WHEN t13.IPEDSValue = 157 THEN 'X'
                                                         ELSE ''
                                                    END AS Seq4
                                                   ,CASE WHEN t13.IPEDSValue = 158 THEN 'X'
                                                         ELSE ''
                                                    END AS Seq5
                                                   ,CASE WHEN t13.IPEDSValue = 159 THEN 'X'
                                                         ELSE ''
                                                    END AS Seq6
                                                   ,CASE WHEN t13.IPEDSValue = 160 THEN 'X'
                                                         ELSE ''
                                                    END AS Seq7
                                          FROM      adGenders t3
                                          LEFT JOIN arStudent t1 ON t3.GenderId = t1.Gender
                                          LEFT JOIN adEthCodes t4 ON t4.EthCodeId = t1.Race
                                          INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                                          INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                                          INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                                                       AND t6.SysStatusId NOT IN ( 7,8 )
                                          INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                                          INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                                          INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                                          LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                                          INNER JOIN adCitizenships t12 ON t1.Citizen = t12.CitizenshipId
                                          LEFT JOIN arProgCredential t13 ON t8.CredentialLvlId = t13.CredentialId
                                          WHERE     t2.CampusId = @CampusId
                                                    AND (
                                                          (
                                                            t8.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
                                                            AND @ProgId IS NULL
                                                          )
                                                          OR t8.ProgId IN ( SELECT  Val
                                                                            FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                                        )
                                                    AND t6.SysStatusId IN ( 14 )
                                                    AND (
                                                          t3.IPEDSValue = 30
                                                          OR t3.IPEDSValue = 31
                                                        )
                                                    AND t1.Race IS NOT NULL
                                                    AND t2.ExpGradDate >= @StartDate
                                                    AND t2.ExpGradDate <= @EndDate
                                                    AND t12.IPEDSValue = 65
                                                    AND NOT t8.CredentialLvlId IS NULL
						--and t4.IPEDSSequence is not null
                                                    AND t13.IPEDSSequence IS NOT NULL
                                        ) dt1
                              UNION
                              SELECT    Studentid
                                       ,SSN
                                       ,StudentNumber
                                       ,StudentName
                                       ,(
                                          SELECT TOP 1
                                                    CASE WHEN CAST(( DATEDIFF(mm,C4.DOB,C1.ExpGradDate) / 12 ) AS INT) < 18 THEN 1
                                                         WHEN CAST(( DATEDIFF(mm,C4.DOB,C1.ExpGradDate) / 12 ) AS INT) >= 18
                                                              AND CAST(( DATEDIFF(mm,C4.DOB,C1.ExpGradDate) / 12 ) AS INT) < 25 THEN 2
                                                         WHEN CAST(( DATEDIFF(mm,C4.DOB,C1.ExpGradDate) / 12 ) AS INT) >= 25
                                                              AND CAST(( DATEDIFF(mm,C4.DOB,C1.ExpGradDate) / 12 ) AS INT) < 40 THEN 3
                                                         WHEN CAST(( DATEDIFF(mm,C4.DOB,C1.ExpGradDate) / 12 ) AS INT) >= 40 THEN 4
                                                         ELSE 5
                                                    END
                                          FROM      arStudent C4
                                                   ,arStuEnrollments C1
                                                   ,arPrgVersions C2
                                                   ,arProgTypes C3
                                          WHERE     C4.StudentId = C1.StudentId
                                                    AND C1.PrgVerId = C2.PrgVerId
                                                    AND C2.ProgTypId = C3.ProgTypId
                                                    AND C1.StudentId = dt3.StudentId
                                                    AND C1.ExpGradDate >= @StartDate
                                                    AND C1.ExpGradDate <= @EndDate
                                          ORDER BY  C1.ExpGradDate DESC
                                        ) AS AgeSequence
                                       ,(
                                          SELECT TOP 1
                                                    CASE WHEN CAST(( DATEDIFF(mm,C4.DOB,C1.ExpGradDate) / 12 ) AS INT) < 18 THEN 'Under 18'
                                                         WHEN CAST(( DATEDIFF(mm,C4.DOB,C1.ExpGradDate) / 12 ) AS INT) >= 18
                                                              AND CAST(( DATEDIFF(mm,C4.DOB,C1.ExpGradDate) / 12 ) AS INT) < 25 THEN '18 - 24'
                                                         WHEN CAST(( DATEDIFF(mm,C4.DOB,C1.ExpGradDate) / 12 ) AS INT) >= 25
                                                              AND CAST(( DATEDIFF(mm,C4.DOB,C1.ExpGradDate) / 12 ) AS INT) < 40 THEN '25 - 39'
                                                         WHEN CAST(( DATEDIFF(mm,C4.DOB,C1.ExpGradDate) / 12 ) AS INT) >= 40 THEN '40 and above'
                                                         ELSE 'Unknown'
                                                    END
                                          FROM      arStudent C4
                                                   ,arStuEnrollments C1
                                                   ,arPrgVersions C2
                                                   ,arProgTypes C3
                                          WHERE     C4.StudentId = C1.StudentId
                                                    AND C1.PrgVerId = C2.PrgVerId
                                                    AND C2.ProgTypId = C3.ProgTypId
                                                    AND C1.StudentId = dt3.StudentId
                                                    AND C1.ExpGradDate >= @StartDate
                                                    AND C1.ExpGradDate <= @EndDate
                                          ORDER BY  C1.ExpGradDate DESC
                                        ) AS Age
                                       ,Seq1
                                       ,Seq2
                                       ,Seq3
                                       ,Seq4
                                       ,Seq5
                                       ,Seq6
                                       ,Seq7
                              FROM      (
                                          SELECT DISTINCT
                                                    t1.StudentId
                                                   ,t1.SSN
                                                   ,t1.StudentNumber
                                                   ,t1.LastName + ', ' + t1.FirstName + ' ' + ISNULL(t1.MiddleName,'') AS StudentName
                                                   ,CASE WHEN t13.IPEDSValue = 154 THEN 'X'
                                                         ELSE ''
                                                    END AS Seq1
                                                   ,CASE WHEN t13.IPEDSValue = 155 THEN 'X'
                                                         ELSE ''
                                                    END AS Seq2
                                                   ,CASE WHEN t13.IPEDSValue = 156 THEN 'X'
                                                         ELSE ''
                                                    END AS Seq3
                                                   ,CASE WHEN t13.IPEDSValue = 157 THEN 'X'
                                                         ELSE ''
                                                    END AS Seq4
                                                   ,CASE WHEN t13.IPEDSValue = 158 THEN 'X'
                                                         ELSE ''
                                                    END AS Seq5
                                                   ,CASE WHEN t13.IPEDSValue = 159 THEN 'X'
                                                         ELSE ''
                                                    END AS Seq6
                                                   ,CASE WHEN t13.IPEDSValue = 160 THEN 'X'
                                                         ELSE ''
                                                    END AS Seq7
                                          FROM      adGenders t3
                                          LEFT JOIN arStudent t1 ON t3.GenderId = t1.Gender
                                          LEFT JOIN adEthCodes t4 ON t4.EthCodeId = t1.Race
                                          INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                                          INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                                          INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                                                       AND t6.SysStatusId NOT IN ( 7,8 )
                                          INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                                          INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                                          INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                                          LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                                          INNER JOIN adCitizenships t12 ON t1.Citizen = t12.CitizenshipId
                                          LEFT JOIN arProgCredential t13 ON t8.CredentialLvlId = t13.CredentialId
                                          WHERE     t2.CampusId = @CampusId
                                                    AND (
                                                          (
                                                            t8.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
                                                            AND @ProgId IS NULL
                                                          )
                                                          OR t8.ProgId IN ( SELECT  Val
                                                                            FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                                        )
                                                    AND t6.SysStatusId IN ( 14 )
                                                    AND (
                                                          t3.IPEDSValue = 30
                                                          OR t3.IPEDSValue = 31
                                                        )
                                                    AND t1.Race IS NOT NULL
                                                    AND t2.ExpGradDate >= @StartDate
                                                    AND t2.ExpGradDate <= @EndDate
                                                    AND t12.IPEDSValue <> 65
                                                    AND NOT t8.CredentialLvlId IS NULL
                                                    AND t4.IPEDSSequence IS NOT NULL
                                                    AND t13.IPEDSSequence IS NOT NULL
                                        ) dt3
                            ) dt
                ) dt2
        GROUP BY AgeSequence
               ,Age
        ORDER BY AgeSequence; 
    END;

GO
