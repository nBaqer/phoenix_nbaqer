SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[usp_GetStudenEnrollmentinfoforBadgeNumber]
    (
     @BadgeNumber AS VARCHAR(8000)
    )
AS
    BEGIN 


        SELECT DISTINCT
                SE.StuEnrollId
               ,SE.StatusCodeID
               ,SC.SysStatusID
               ,ABS(SE.badgeNumber) AS BadgeNumber
        FROM    dbo.arStuEnrollments SE
               ,syStatusCodes SC
        WHERE   SC.StatusCodeId = SE.StatusCodeId
                AND SC.SysStatusId IN ( 7,9,10,11,20 )
                AND ISNUMERIC(SE.BAdgeNumber) = 1
                AND ABS(BadgeNumber) IN ( SELECT    strval
                                          FROM      dbo.SPLIT(@BadgeNumber) );
               
    END;






GO
