SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author:		Ginzo, John
-- Create date: 03/23/2015
-- Description:	NSLDS Data Export
-- =============================================
CREATE PROCEDURE [dbo].[GetNSLDSExportData]
    @AwardYear VARCHAR(20)
   ,@CampusId VARCHAR(50)
   ,@ProgramId VARCHAR(8000) = NULL
AS
    BEGIN
        -- SET NOCOUNT ON added to prevent extra result sets from
        -- interfering with SELECT statements.
        SET NOCOUNT ON;



        DECLARE @AwardStartDate DATETIME;
        DECLARE @AwardEndDate DATETIME;

        SET @AwardStartDate = '07/01/' + LTRIM(RTRIM(SUBSTRING(@AwardYear, 1, 4)));
        SET @AwardEndDate = '06/30/' + LTRIM(RTRIM(SUBSTRING(@AwardYear, 5, 4)));

        -------------------------------------------------------------
        -- Table to store student status changes
        -------------------------------------------------------------
        DECLARE @syStudentStatusChangesView AS TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER NOT NULL
               ,SysStatusId INTEGER
               ,SysStatusDescrip NVARCHAR(80)
               ,GEProgramStatus NVARCHAR(1)
               ,DateOfChange DATETIME
               ,ModDate DATETIME
               ,N INT
            );

        INSERT INTO @syStudentStatusChangesView
                    SELECT   V.StuEnrollId
                            ,V.SysStatusId
                            ,V.SysStatusDescrip
                            ,V.GEProgramStatus
                            ,V.DateOfChange
                            ,V.ModDate
                            ,V.N
                    FROM     (
                             SELECT     DISTINCT SSSC.StuEnrollId
                                       ,SSC1.SysStatusId
                                       ,SSS1.SysStatusDescrip
                                       ,SSS1.GEProgramStatus
                                       ,SSSC.DateOfChange
                                       ,SSSC.ModDate
                                       ,RANK() OVER ( PARTITION BY SSSC.StuEnrollId
                                                      ORDER BY SSSC.DateOfChange DESC
                                                              ,SSSC.ModDate DESC
                                                    ) AS N
                             FROM       dbo.syStudentStatusChanges AS SSSC
                             LEFT JOIN  dbo.syStatusCodes AS SSC1 ON SSSC.NewStatusId = SSC1.StatusCodeId
                             INNER JOIN dbo.sySysStatus AS SSS1 ON SSC1.SysStatusId = SSS1.SysStatusId
                             WHERE      SSSC.DateOfChange <= @AwardEndDate
                                        AND SSS1.GEProgramStatus IS NOT NULL
                             ) AS V
                    WHERE    V.N = 1
                    ORDER BY V.StuEnrollId;
        -------------------------------------------------------------

        -------------------------------------------------------------
        -- List of enrollments where dropped, grad, or lda is before
        -- start of award
        -------------------------------------------------------------
        DECLARE @DroppedTable TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
            );
        INSERT INTO @DroppedTable
                    SELECT     DISTINCT se.StuEnrollId
                    FROM       arStuEnrollments se
                    INNER JOIN dbo.syStatusCodes sc ON se.StatusCodeId = sc.StatusCodeId
                    WHERE      StartDate <= @AwardEndDate
                               AND LTRIM(RTRIM(se.CampusId)) = LTRIM(RTRIM(@CampusId))
                               AND (
                                   (
                                   sc.SysStatusId = 12
                                   AND se.DateDetermined < @AwardStartDate
                                   )
                                   OR (
                                      sc.SysStatusId = 14
                                      AND ExpGradDate < @AwardStartDate
                                      )
                                   OR (
                                      sc.SysStatusId = 19
                                      AND LDA < @AwardStartDate
                                      )
                                   );
        -------------------------------------------------------------

        -------------------------------------------------------------
        -- Get the minimum attendance date for enrollments in the student attendance table
        -------------------------------------------------------------
        DECLARE @MinAttendedDate TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,StudentAttendedDate DATETIME
            );
        INSERT INTO @MinAttendedDate
                    SELECT   StuEnrollId
                            ,MIN(StudentAttendedDate)
                    FROM     dbo.syStudentAttendanceSummary
                    GROUP BY StuEnrollId;
        -------------------------------------------------------------


        -------------------------------------------------------------
        -- JG - maxattendeddateduringawardyear
        -------------------------------------------------------------
        DECLARE @MaxAttendedDateDuringAwardYear TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,MaxAttendedDate DATETIME
               ,N INT
            );


        IF EXISTS (
                  SELECT *
                  FROM   dbo.syStudentAttendanceSummary
                  )
            BEGIN
                INSERT INTO @MaxAttendedDateDuringAwardYear
                            SELECT     e.StuEnrollId
                                      ,V.StudentAttendedDate
                                      ,V.N
                            FROM       dbo.arStuEnrollments e
                            INNER JOIN (
                                       SELECT    e1.StuEnrollId
                                                ,r.StudentAttendedDate
                                                ,RANK() OVER ( PARTITION BY e1.StuEnrollId
                                                               ORDER BY r.StudentAttendedDate DESC
                                                             ) AS N
                                       FROM      dbo.arStuEnrollments e1
                                       LEFT JOIN dbo.syStudentAttendanceSummary r ON e1.StuEnrollId = r.StuEnrollId
                                       WHERE     r.StudentAttendedDate
                                       BETWEEN   @AwardStartDate AND @AwardEndDate
                                       ) V ON e.StuEnrollId = V.StuEnrollId
                            WHERE      V.N = 1
                            ORDER BY   e.StuEnrollId
                                      ,V.StudentAttendedDate;

            END;
        ELSE
            INSERT INTO @MaxAttendedDateDuringAwardYear
                        SELECT     e.StuEnrollId
                                  ,V.ModDate
                                  ,V.N
                        FROM       dbo.arStuEnrollments e
                        INNER JOIN (
                                   SELECT    e1.StuEnrollId
                                            ,r.ModDate
                                            ,RANK() OVER ( PARTITION BY e1.StuEnrollId
                                                           ORDER BY r.ModDate DESC
                                                         ) AS N
                                   FROM      dbo.arStuEnrollments e1
                                   LEFT JOIN arResults r ON e1.StuEnrollId = r.StuEnrollId
                                   WHERE     r.ModDate
                                   BETWEEN   @AwardStartDate AND @AwardEndDate
                                   ) V ON e.StuEnrollId = V.StuEnrollId
                        WHERE      V.N = 1
                        ORDER BY   e.StuEnrollId
                                  ,V.ModDate;
        -------------------------------------------------------------

        -------------------------------------------------------------
        -- Get the re-enroll dates for enrollments
        -------------------------------------------------------------
        DECLARE @ReEnrollData TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,DropDate DATETIME
               ,ReEnrollmentDate DATETIME
               ,ReEnrolledValue INT
            );
        INSERT INTO @ReEnrollData
                    SELECT     e.StuEnrollId
                              ,RE.DropDate
                              ,RE.ReEnrollDate
                              ,DATEDIFF(DAY, RE.DropDate, RE.ReEnrollDate) AS ReEnrolledValue
                    FROM       dbo.arStuEnrollments e
                    INNER JOIN dbo.syCampuses c ON e.CampusId = c.CampusId
                    INNER JOIN dbo.arStudent s ON e.StudentId = s.StudentId
                    INNER JOIN dbo.arPrgVersions pv ON e.PrgVerId = pv.PrgVerId
                    INNER JOIN dbo.arPrograms p ON pv.ProgId = p.ProgId
                    INNER JOIN dbo.syStatusCodes sc ON e.StatusCodeId = sc.StatusCodeId
                    INNER JOIN dbo.sySysStatus ss ON sc.SysStatusId = ss.SysStatusId
                    INNER JOIN (
                               SELECT     DISTINCT SSSC1.StuEnrollId
                                         ,SSSC1.DateOfChange AS DropDate
                                         ,SSSC2.DateOfChange AS ReEnrollDate
                                         ,RANK() OVER ( PARTITION BY SSSC1.StuEnrollId
                                                        ORDER BY SSSC1.DateOfChange DESC
                                                                ,SSSC2.DateOfChange DESC
                                                      ) AS N
                               FROM       dbo.syStudentStatusChanges AS SSSC1
                               LEFT JOIN  dbo.syStatusCodes AS SC1 ON SC1.StatusCodeId = SSSC1.NewStatusId
                               INNER JOIN dbo.sySysStatus AS SS1 ON SS1.SysStatusId = SC1.SysStatusId
                               INNER JOIN syStudentStatusChanges AS SSSC2 ON SSSC2.StuEnrollId = SSSC1.StuEnrollId
                               LEFT JOIN  dbo.syStatusCodes AS SC2 ON SC2.StatusCodeId = SSSC2.NewStatusId
                               INNER JOIN dbo.sySysStatus AS SS2 ON SS2.SysStatusId = SC2.SysStatusId
                               WHERE      SSSC2.DateOfChange >= @AwardStartDate
                                          AND SSSC2.DateOfChange <= @AwardEndDate
                                          AND (
                                              SS2.SysStatusId = 7
                                              OR SS2.GEProgramStatus IN ( 'E', 'C' )
                                              )
                                          AND SSSC1.DateOfChange <= SSSC2.DateOfChange
                                          AND SS1.GEProgramStatus IN ( 'W' )
                                          AND DATEDIFF(DAY, SSSC1.DateOfChange, SSSC2.DateOfChange) <= 180
                               ) AS RE ON RE.StuEnrollId = e.StuEnrollId
                    LEFT JOIN  @syStudentStatusChangesView SView ON e.StuEnrollId = SView.StuEnrollId
                    LEFT JOIN  @MaxAttendedDateDuringAwardYear MAD ON e.StuEnrollId = MAD.StuEnrollId
                    WHERE      e.StartDate <= @AwardEndDate
                               AND NOT (
                                       SView.GEProgramStatus IN ( 'C', 'W' )
                                       AND SView.DateOfChange > @AwardEndDate
                                       )
                               AND c.CampusId = @CampusId
                               AND s.SSN IS NOT NULL
                               AND p.IsGEProgram = 1
                               AND sc.SysStatusId <> 8
                               AND (
                                   SView.SysStatusId = 7
                                   OR SView.GEProgramStatus IN ( 'E', 'C' )
                                   )
                               AND RE.N = 1;


        -------------------------------------------------------------
        -------------------------------------------------------------
        -- Get STUDENTS WITH  for Title IV
        -------------------------------------------------------------
        DECLARE @TitleIVData TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
            );
        INSERT INTO @TitleIVData
                    SELECT     DISTINCT ST.StuEnrollId
                    FROM       saTransactions AS ST
                    INNER JOIN saFundSources AS SFS ON SFS.FundSourceId = ST.FundSourceId
                    INNER JOIN syAdvFundSources AFS ON AFS.AdvFundSourceId = SFS.AdvFundSourceId
                    WHERE      SFS.TitleIV = 1;
        -------------------------------------------------------------

        -------------------------------------------------------------
        -- Get Private Loan Data
        -------------------------------------------------------------
        DECLARE @PrivateLoanData TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,PrivateLoanAmount DECIMAL
            );
        INSERT INTO @PrivateLoanData
                    SELECT     a.StuEnrollId
                              ,ABS(ISNULL(SUM(a.TransAmount), 0))
                    FROM       dbo.saTransactions a
                    INNER JOIN dbo.saFundSources fs ON a.FundSourceId = fs.FundSourceId
                    INNER JOIN syAdvFundSources AFS ON fs.AdvFundSourceId = AFS.AdvFundSourceId
                    WHERE      a.FundSourceId IS NOT NULL
                               AND fs.TitleIV = 0
                               AND AFS.AdvFundSourceId IN ( 9, 10, 11, 13, 22 )
                    GROUP BY   a.StuEnrollId;
        -------------------------------------------------------------

        -------------------------------------------------------------
        -- Get InsitutionalDebt for grads
        -------------------------------------------------------------
        DECLARE @GradInstDebt TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,InstitutionalDebt DECIMAL
            );
        INSERT INTO @GradInstDebt
                    SELECT   R.StuEnrollId
                            ,CASE WHEN ROUND(SUM(ISNULL(TransAmount, 0)), 0) < 0 THEN 0
                                  ELSE ROUND(SUM(ISNULL(TransAmount, 0)), 0)
                             END AS InstitutionalDebt
                    FROM     (
                             SELECT     trn.StuEnrollId
                                       ,trn.TransAmount
                             FROM       dbo.saTransactions trn
                             INNER JOIN dbo.arStuEnrollments e ON trn.StuEnrollId = e.StuEnrollId
                             INNER JOIN dbo.saTransCodes tc ON trn.TransCodeId = tc.TransCodeId
                             WHERE      IsInstCharge = 1
                                        AND trn.Voided = 0
                                        AND trn.TransDate <= DATEADD(DAY, 10, e.ExpGradDate)
                             UNION ALL
                             SELECT     trn.StuEnrollId
                                       ,ap.Amount * -1
                             FROM       dbo.saTransactions trn
                             INNER JOIN dbo.arStuEnrollments e ON trn.StuEnrollId = e.StuEnrollId
                             INNER JOIN arStudent st ON st.StudentId = e.StudentId
                             INNER JOIN dbo.saAppliedPayments ap ON ap.TransactionId = trn.TransactionId
                             INNER JOIN dbo.saTransactions trn2 ON trn2.TransactionId = ap.ApplyToTransId
                             INNER JOIN dbo.saTransCodes tc2 ON tc2.TransCodeId = trn2.TransCodeId
                             WHERE      IsInstCharge = 1
                                        AND trn.Voided = 0
                                        AND trn.TransDate <= DATEADD(DAY, 10, e.ExpGradDate)
                                        AND trn.TransCodeId IS NULL
                             ) R
                    GROUP BY R.StuEnrollId;
        -------------------------------------------------------------

        -------------------------------------------------------------
        -- Get Institutionaldebt for drops
        -------------------------------------------------------------
        DECLARE @DropInstDebt TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,InstitutionalDebt DECIMAL
            );
        INSERT INTO @DropInstDebt
                    SELECT   R.StuEnrollId
                            ,CASE WHEN ROUND(SUM(ISNULL(TransAmount, 0)), 0) < 0 THEN 0
                                  ELSE ROUND(SUM(ISNULL(TransAmount, 0)), 0)
                             END AS InstitutionalDebt
                    FROM     (
                             SELECT     trn.StuEnrollId
                                       ,trn.TransAmount
                             FROM       dbo.saTransactions trn
                             INNER JOIN dbo.arStuEnrollments e ON trn.StuEnrollId = e.StuEnrollId
                             INNER JOIN dbo.saTransCodes tc ON trn.TransCodeId = tc.TransCodeId
                             WHERE      IsInstCharge = 1
                                        AND trn.Voided = 0
                                        AND trn.TransDate < DATEADD(DAY, 10, e.DateDetermined)
                             UNION ALL
                             SELECT     trn.StuEnrollId
                                       ,ap.Amount * -1
                             FROM       dbo.saTransactions trn
                             INNER JOIN dbo.arStuEnrollments e ON trn.StuEnrollId = e.StuEnrollId
                             INNER JOIN arStudent st ON st.StudentId = e.StudentId
                             INNER JOIN dbo.saAppliedPayments ap ON ap.TransactionId = trn.TransactionId
                             INNER JOIN dbo.saTransactions trn2 ON trn2.TransactionId = ap.ApplyToTransId
                             INNER JOIN dbo.saTransCodes tc2 ON tc2.TransCodeId = trn2.TransCodeId
                             WHERE      IsInstCharge = 1
                                        AND trn.Voided = 0
                                        AND trn.TransDate < DATEADD(DAY, 10, e.DateDetermined)
                                        AND trn.TransCodeId IS NULL
                             ) R
                    GROUP BY R.StuEnrollId;
        -------------------------------------------------------------

        -------------------------------------------------------------
        -- Get tutition and fees for enrollments
        -------------------------------------------------------------
        DECLARE @TuitFeesData TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,TuitionAndFeesAmount DECIMAL
            );
        INSERT INTO @TuitFeesData
                    SELECT     trn.StuEnrollId
                              ,CASE WHEN ROUND(SUM(ISNULL(TransAmount, 0)), 0) < 0 THEN 0
                                    ELSE ROUND(SUM(ISNULL(TransAmount, 0)), 0)
                               END AS TutitionAndFeesAmount
                    FROM       saTransactions trn
                    INNER JOIN dbo.arStuEnrollments e ON trn.StuEnrollId = e.StuEnrollId
                    INNER JOIN saTransCodes TC ON trn.TransCodeId = TC.TransCodeId
                    INNER JOIN dbo.saSysTransCodes st ON TC.SysTransCodeId = st.SysTransCodeId
                    WHERE      TC.IsInstCharge = 1
                               AND trn.TransTypeId IN ( 0, 1 )
                               AND st.SysTransCodeId IN ( 1, 2, 7, 20 )
                               AND trn.Voided = 0
                    GROUP BY   trn.StuEnrollId;
        -------------------------------------------------------------

        -------------------------------------------------------------
        -- Get Books and supplies amount for enrollments
        -------------------------------------------------------------
        DECLARE @BooksSuppliesData TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,BooksSuppliesEquipment DECIMAL
            );
        INSERT INTO @BooksSuppliesData
                    SELECT     t.StuEnrollId
                              ,CASE WHEN ROUND(SUM(ISNULL(TransAmount, 0)), 0) < 0 THEN 0
                                    ELSE ROUND(SUM(ISNULL(TransAmount, 0)), 0)
                               END AS BooksSuppliesEquipment
                    FROM       dbo.saTransactions t
                    INNER JOIN dbo.saTransCodes c ON t.TransCodeId = c.TransCodeId
                    INNER JOIN dbo.saSysTransCodes sc ON c.SysTransCodeId = sc.SysTransCodeId
                    WHERE      sc.Description IN ( 'Supplies', 'Books', 'Computer Equipment' )
                               AND Voided = 0
                               AND TransTypeId = 0
                    GROUP BY   t.StuEnrollId;
        -------------------------------------------------------------

        -------------------------------------------------------------
        -- Get agency value for fulltime, half time values
        -------------------------------------------------------------
        DECLARE @EnrollStatusFirstdayData TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,AgencyValue CHAR
            );
        INSERT INTO @EnrollStatusFirstdayData
                    SELECT     StuEnrollId
                              ,AgencyValue
                    FROM       dbo.arStuEnrollments e
                    INNER JOIN dbo.arAttendTypes aat ON e.attendtypeid = aat.AttendTypeId
                    INNER JOIN dbo.syRptAgencyFldValues SRAFV ON aat.GERptAgencyFldValId = SRAFV.RptAgencyFldValId;
        -------------------------------------------------------------

        -------------------------------------------------------------
        -- Get Data
        -------------------------------------------------------------
        SELECT
            --	se.StuEnrollId,
            --	s.StudentId,
            --	p.ProgId,
            --	c.CampusId,
                   @AwardYear AS [Award Year]
                  ,s.SSN AS [Student Social Security Number]
                  ,s.FirstName AS [Student First Name]
                  ,s.MiddleName AS [Student Middle Name]
                  ,s.LastName AS [Student Last Name]
                  ,ISNULL(s.DOB, '01/01/1900') AS [Student Date of Birth]
                  ,c.OPEID AS [Institution Code (OPEID)]
                  ,c.SchoolName AS [Institution Name]
                  ,p.ProgDescrip AS [Program Name]
                  ,p.CIPCode AS [CIP Code]
                  ,CASE WHEN p.CredentialLvlId IS NULL THEN NULL
                        ELSE raf.AgencyValue
                   END AS [Credential Level]
                  ,'N' AS [Medical or Dental Internship or Residency]
                  ,se.StartDate AS [Program Attendance Begin Date]
                  ,CASE WHEN ma.StudentAttendedDate IS NULL THEN CASE WHEN se.StartDate
                                                                           BETWEEN @AwardStartDate AND @AwardEndDate THEN se.StartDate
                                                                      ELSE @AwardStartDate
                                                                 END
                        ELSE CASE WHEN ma.StudentAttendedDate
                                       BETWEEN @AwardStartDate AND @AwardEndDate THEN ma.StudentAttendedDate
                                  ELSE @AwardStartDate
                             END
                   END AS [Program Attendance Begin Date for This Award Year]
                  ,CASE WHEN (
                             RE.StuEnrollId IS NOT NULL
                             AND RE.ReEnrolledValue <= 180
                             ) THEN 'W'
                        WHEN SView.SysStatusDescrip = 'Future start'
                             AND (
                                 ISNULL(MAD.MaxAttendedDate, se.LDA) IS NOT NULL
                                 OR se.StartDate IS NOT NULL
                                 ) THEN 'E'
                        WHEN SView.GEProgramStatus = 'C' THEN 'G'
                        ELSE SView.GEProgramStatus
                   END AS [Program Attendance Status During Award Year]
                  ,CASE WHEN RE.StuEnrollId IS NOT NULL
                             AND RE.ReEnrolledValue <= 180 THEN RE.DropDate
                        WHEN SView.SysStatusDescrip = 'Future start'
                             AND (
                                 ISNULL(MAD.MaxAttendedDate, se.LDA) IS NOT NULL
                                 OR se.StartDate IS NOT NULL
                                 ) THEN @AwardEndDate
                        WHEN SView.GEProgramStatus = 'C'
                             OR SView.GEProgramStatus = 'G' THEN se.ExpGradDate
                        WHEN SView.GEProgramStatus = 'W' THEN SView.DateOfChange
                        ELSE @AwardEndDate
                   END AS [Program Attendance Status Date]
                  ,CASE WHEN (
                             RE.StuEnrollId IS NOT NULL
                             AND RE.ReEnrolledValue <= 180
                             ) THEN ISNULL(PLD.PrivateLoanAmount, 0)
                        WHEN SView.SysStatusDescrip = 'Future start'
                             AND (
                                 ISNULL(MAD.MaxAttendedDate, se.LDA) IS NOT NULL
                                 OR se.StartDate IS NOT NULL
                                 ) THEN NULL
                        WHEN SView.GEProgramStatus IN ( 'C', 'W' ) THEN ISNULL(PLD.PrivateLoanAmount, 0)
                        ELSE NULL
                   END AS [Private Loans Amount]
                  ,CASE WHEN (
                             RE.StuEnrollId IS NOT NULL
                             AND RE.ReEnrolledValue <= 180
                             ) THEN ISNULL(GI.InstitutionalDebt, 0)
                        WHEN SView.SysStatusDescrip = 'Future start'
                             AND (
                                 ISNULL(MAD.MaxAttendedDate, se.LDA) IS NOT NULL
                                 OR se.StartDate IS NOT NULL
                                 ) THEN NULL
                        WHEN SView.GEProgramStatus IN ( 'C', 'G', 'W' ) THEN ISNULL(GI.InstitutionalDebt, 0)
                        ELSE NULL
                   END AS [Institutional Debt]
                  ,CASE WHEN (
                             RE.StuEnrollId IS NOT NULL
                             AND RE.ReEnrolledValue <= 180
                             ) THEN ISNULL(TF.TuitionAndFeesAmount, 0)
                        WHEN SView.SysStatusDescrip = 'Future start'
                             AND (
                                 ISNULL(MAD.MaxAttendedDate, se.LDA) IS NOT NULL
                                 OR se.StartDate IS NOT NULL
                                 ) THEN NULL
                        WHEN SView.GEProgramStatus IN ( 'C', 'W' ) THEN ISNULL(TF.TuitionAndFeesAmount, 0)
                        ELSE NULL
                   END AS [Tuition and Fees Amount]
                  ,CASE WHEN (
                             RE.StuEnrollId IS NOT NULL
                             AND RE.ReEnrolledValue <= 180
                             ) THEN ISNULL(BSE.BooksSuppliesEquipment, 0)
                        WHEN SView.SysStatusDescrip = 'Future start'
                             AND (
                                 ISNULL(MAD.MaxAttendedDate, se.LDA) IS NOT NULL
                                 OR se.StartDate IS NOT NULL
                                 ) THEN NULL
                        WHEN SView.GEProgramStatus IN ( 'C', 'G', 'W' ) THEN ISNULL(BSE.BooksSuppliesEquipment, 0)
                        ELSE NULL
                   END AS [Allowance for Books, Supplies, and Equipment]
                  ,pv.Weeks AS [Length of GE Program]
                  ,'W' AS [Length of GE Program Measurement]
                  ,ESF.AgencyValue AS [Student's Enrollment Status as of the 1st Day of Enrollment in Program]
                  ,ROW_NUMBER() OVER ( ORDER BY s.StudentId ) AS RowNumber
        FROM       arStudent AS s
        INNER JOIN arStuEnrollments AS se ON s.StudentId = se.StudentId
        INNER JOIN @TitleIVData AS TID ON TID.StuEnrollId = se.StuEnrollId
        LEFT JOIN  @DroppedTable AS DET ON se.StuEnrollId = DET.StuEnrollId
        INNER JOIN syCampuses AS c ON se.CampusId = c.CampusId
        INNER JOIN arPrgVersions AS pv ON se.PrgVerId = pv.PrgVerId
        INNER JOIN arPrograms AS p ON pv.ProgId = p.ProgId
        LEFT JOIN  arProgCredential AS pcr ON p.CredentialLvlId = pcr.CredentialId
        LEFT JOIN  syRptAgencyFldValues AS raf ON pcr.GERptAgencyFldValId = raf.RptAgencyFldValId
        LEFT JOIN  @syStudentStatusChangesView AS SView ON se.StuEnrollId = SView.StuEnrollId
        LEFT JOIN  @MinAttendedDate AS ma ON se.StuEnrollId = ma.StuEnrollId
        LEFT JOIN  @ReEnrollData AS RE ON se.StuEnrollId = RE.StuEnrollId
        LEFT JOIN  @PrivateLoanData AS PLD ON se.StuEnrollId = PLD.StuEnrollId
        LEFT JOIN  @GradInstDebt AS GI ON se.StuEnrollId = GI.StuEnrollId
        LEFT JOIN  @DropInstDebt AS DI ON se.StuEnrollId = DI.StuEnrollId
        LEFT JOIN  @TuitFeesData AS TF ON se.StuEnrollId = TF.StuEnrollId
        LEFT JOIN  @BooksSuppliesData AS BSE ON se.StuEnrollId = BSE.StuEnrollId
        LEFT JOIN  @EnrollStatusFirstdayData AS ESF ON se.StuEnrollId = ESF.StuEnrollId
        LEFT JOIN  @MaxAttendedDateDuringAwardYear AS MAD ON se.StuEnrollId = MAD.StuEnrollId
        WHERE      se.StartDate <= @AwardEndDate
                   AND NOT (
                           SView.GEProgramStatus IN ( 'C', 'W' )
                           AND SView.DateOfChange > @AwardEndDate
                           )
                   AND c.CampusId = @CampusId
                   AND s.SSN IS NOT NULL
                   AND p.IsGEProgram = 1
                   AND SView.SysStatusId <> 8
                   AND (
                       SView.SysStatusId = 7
                       OR SView.GEProgramStatus IS NOT NULL
                       )
                   AND (
                       @ProgramId IS NULL
                       OR p.ProgId IN (
                                      SELECT Val
                                      FROM   MultipleValuesForReportParameters(@ProgramId, ',', 1)
                                      )
                       )
                   AND DET.StuEnrollId IS NULL
        UNION ALL
        SELECT
            --		se.StuEnrollId,
            --		s.StudentId,
            --		p.ProgId,
            --		c.CampusId,
                   @AwardYear AS [Award Year]
                  ,s.SSN AS [Student Social Security Number]
                  ,s.FirstName AS [Student First Name]
                  ,s.MiddleName AS [Student Middle Name]
                  ,s.LastName AS [Student Last Name]
                  ,ISNULL(s.DOB, '01/01/1900') AS [Student Date of Birth]
                  ,c.OPEID AS [Institution Code (OPEID)]
                  ,c.SchoolName AS [Institution Name]
                  ,p.ProgDescrip AS [Program Name]
                  ,p.CIPCode AS [CIP Code]
                  ,CASE WHEN p.CredentialLvlId IS NULL THEN NULL
                        ELSE raf.AgencyValue
                   END AS [Credential Level]
                  ,'N' AS [Medical or Dental Internship or Residency]
                  ,ISNULL(ma.StudentAttendedDate, se.StartDate) AS [Program Attendance Begin Date]
                  ,CASE WHEN (
                             RE.StuEnrollId IS NOT NULL
                             AND RE.ReEnrolledValue <= 180
                             ) THEN RE.ReEnrollmentDate
                        WHEN ma.StudentAttendedDate IS NULL THEN ISNULL(RE.ReEnrollmentDate, @AwardStartDate)
                        ELSE CASE WHEN ma.StudentAttendedDate
                                       BETWEEN @AwardStartDate AND @AwardEndDate THEN ma.StudentAttendedDate
                                  ELSE ISNULL(RE.ReEnrollmentDate, @AwardStartDate)
                             END
                   END AS [Program Attendance Begin Date for This Award Year]
                  ,CASE WHEN SView.SysStatusDescrip = 'Future start'
                             AND (
                                 ISNULL(MAD.MaxAttendedDate, se.LDA) IS NOT NULL
                                 OR se.StartDate IS NOT NULL
                                 ) THEN 'E'
                        WHEN SView.GEProgramStatus = 'C' THEN 'G'
                        ELSE SView.GEProgramStatus
                   END AS [Program Attendance Status During Award Year]
                  ,CASE WHEN SView.GEProgramStatus IN ( 'C', 'W' ) THEN ISNULL(SView.DateOfChange, @AwardEndDate)
                        ELSE @AwardEndDate
                   END AS [Program Attendance Status Date]
                  ,CASE WHEN SView.SysStatusDescrip = 'Future start'
                             AND (
                                 ISNULL(MAD.MaxAttendedDate, se.LDA) IS NOT NULL
                                 OR se.StartDate IS NOT NULL
                                 ) THEN NULL
                        WHEN SView.GEProgramStatus IN ( 'C', 'W' ) THEN ISNULL(PLD.PrivateLoanAmount, 0)
                        ELSE NULL
                   END AS [Private Loans Amount]
                  ,CASE WHEN SView.SysStatusDescrip = 'Future start'
                             AND (
                                 ISNULL(MAD.MaxAttendedDate, se.LDA) IS NOT NULL
                                 OR se.StartDate IS NOT NULL
                                 ) THEN NULL
                        WHEN SView.GEProgramStatus IN ( 'C', 'G', 'W' ) THEN ISNULL(GI.InstitutionalDebt, 0)
                        ELSE NULL
                   END AS [Institutional Debt]
                  ,CASE WHEN SView.SysStatusDescrip = 'Future start'
                             AND (
                                 ISNULL(MAD.MaxAttendedDate, se.LDA) IS NOT NULL
                                 OR se.StartDate IS NOT NULL
                                 ) THEN NULL
                        WHEN SView.GEProgramStatus IN ( 'C', 'W' ) THEN ISNULL(TF.TuitionAndFeesAmount, 0)
                        ELSE NULL
                   END AS [Tuition and Fees Amount]
                  ,CASE WHEN SView.SysStatusDescrip = 'Future start'
                             AND (
                                 ISNULL(MAD.MaxAttendedDate, se.LDA) IS NOT NULL
                                 OR se.StartDate IS NOT NULL
                                 ) THEN NULL
                        WHEN SView.GEProgramStatus IN ( 'C', 'G', 'W' ) THEN ISNULL(BSE.BooksSuppliesEquipment, 0)
                        ELSE NULL
                   END AS [Allowance for Books, Supplies, and Equipment]
                  ,pv.Weeks AS [Length of GE Program]
                  ,'W' AS [Length of GE Program Measurement]
                  ,ESF.AgencyValue AS [Student's Enrollment Status as of the 1st Day of Enrollment in Program]
                  ,ROW_NUMBER() OVER ( ORDER BY s.StudentId ) AS RowNumber
        FROM       arStudent AS s
        INNER JOIN arStuEnrollments AS se ON s.StudentId = se.StudentId
        INNER JOIN @TitleIVData AS TID ON TID.StuEnrollId = se.StuEnrollId
        LEFT JOIN  @DroppedTable AS DET ON se.StuEnrollId = DET.StuEnrollId
        INNER JOIN syCampuses AS c ON se.CampusId = c.CampusId
        INNER JOIN arPrgVersions AS pv ON se.PrgVerId = pv.PrgVerId
        INNER JOIN arPrograms AS p ON pv.ProgId = p.ProgId
        LEFT JOIN  arProgCredential AS pcr ON p.CredentialLvlId = pcr.CredentialId
        LEFT JOIN  syRptAgencyFldValues AS raf ON pcr.GERptAgencyFldValId = raf.RptAgencyFldValId
        LEFT JOIN  @syStudentStatusChangesView AS SView ON se.StuEnrollId = SView.StuEnrollId
        LEFT JOIN  @MinAttendedDate AS ma ON se.StuEnrollId = ma.StuEnrollId
        LEFT JOIN  @ReEnrollData AS RE ON se.StuEnrollId = RE.StuEnrollId
        LEFT JOIN  @PrivateLoanData AS PLD ON se.StuEnrollId = PLD.StuEnrollId
        LEFT JOIN  @GradInstDebt AS GI ON se.StuEnrollId = GI.StuEnrollId
        LEFT JOIN  @DropInstDebt AS DI ON se.StuEnrollId = DI.StuEnrollId
        LEFT JOIN  @TuitFeesData AS TF ON se.StuEnrollId = TF.StuEnrollId
        LEFT JOIN  @BooksSuppliesData AS BSE ON se.StuEnrollId = BSE.StuEnrollId
        LEFT JOIN  @EnrollStatusFirstdayData AS ESF ON se.StuEnrollId = ESF.StuEnrollId
        LEFT JOIN  @MaxAttendedDateDuringAwardYear AS MAD ON se.StuEnrollId = MAD.StuEnrollId
        WHERE      se.StartDate <= @AwardEndDate
                   AND NOT (
                           SView.GEProgramStatus IN ( 'C', 'W' )
                           AND SView.DateOfChange > @AwardEndDate
                           )
                   AND c.CampusId = @CampusId
                   AND s.SSN IS NOT NULL
                   AND p.IsGEProgram = 1
                   AND SView.SysStatusId <> 8
                   AND (
                       SView.SysStatusId = 7
                       OR SView.GEProgramStatus IS NOT NULL
                       )
                   AND (
                       @ProgramId IS NULL
                       OR p.ProgId IN (
                                      SELECT Val
                                      FROM   MultipleValuesForReportParameters(@ProgramId, ',', 1)
                                      )
                       )
                   AND DET.StuEnrollId IS NULL
                   AND RE.StuEnrollId IS NOT NULL
        ORDER BY [CIP Code]
                ,[Credential Level];
    END;






GO
