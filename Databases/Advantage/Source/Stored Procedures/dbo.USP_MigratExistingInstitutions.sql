SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_MigratExistingInstitutions]
AS
    DECLARE @AddressTypeId UNIQUEIDENTIFIER
       ,@PhoneTypeId UNIQUEIDENTIFIER;
    SET @AddressTypeId = (
                           SELECT TOP 1
                                    AddressTypeId
                           FROM     dbo.plAddressTypes
                           WHERE    AddressDescrip = 'Work'
                         );
    SET @PhoneTypeId = (
                         SELECT TOP 1
                                PhoneTypeId
                         FROM   dbo.syPhoneType
                         WHERE  PhoneTypeDescrip = 'Work'
                       );

	
    IF (
         SELECT COUNT(*)
         FROM   dbo.syInstitutions
       ) = 0
        BEGIN
            INSERT  INTO syInstitutions
                    (
                     HSId
                    ,HSCode
                    ,StatusId
                    ,HSName
                    ,CampGrpId
                    ,ModUser
                    ,ModDate
                    ,ImportTypeId
                    ,TypeId
                    ,LevelID
                    )
                    SELECT  HSId
                           ,HSCode
                           ,StatusId
                           ,HSName
                           ,CampGrpId
                           ,ModUser
                           ,ModDate
                           ,1
                           ,1
                           ,2
                    FROM    dbo.adHighSchools; 

            INSERT  INTO syInstitutionAddresses
                    (
                     InstitutionAddressId
                    ,InstitutionId
                    ,AddressTypeId
                    ,Address1
                    ,Address2
                    ,City
                    ,StateId
                    ,ZipCode
                    ,CountryId
                    ,StatusId
                    ,IsMailingAddress
                    ,ModUser
                    ,ModDate
                    ,State
                    ,IsInternational
                    ,CountyId
                    ,ForeignCountyStr
                    ,AddressApto
                    ,OtherState
                    )
                    SELECT  NEWID()
                           ,HSId
                           ,@AddressTypeId
                           ,Address1
                           ,Address2
                           ,City
                           ,StateId
                           ,Zip
                           ,CountryId
                           ,StatusId
                           ,0
                           ,ModUser
                           ,ModDate
                           ,NULL
                           ,CASE ForeignZip
                              WHEN 1 THEN 1
                              ELSE 0
                            END AS IsInternational
                           ,NULL
                           ,NULL
                           ,NULL
                           ,NULL
                    FROM    dbo.adHighSchools;

            INSERT  INTO syInstitutionPhone
                    (
                     InstitutionPhoneId
                    ,InstitutionId
                    ,PhoneTypeId
                    ,Phone
                    ,ModDate
                    ,ModUser
                    ,IsForeignPhone
                    ,StatusId
                    )
                    SELECT  NEWID()
                           ,HSId
                           ,@PhoneTypeId
                           ,Phone
                           ,ModDate
                           ,ModUser
                           ,CASE ForeignPhone
                              WHEN 1 THEN 1
                              ELSE 0
                            END AS IsForeignPhone
                           ,StatusId
                    FROM    dbo.adHighSchools
                    WHERE   Phone IS NOT NULL;

			--Colleges
            INSERT  INTO syInstitutions
                    (
                     HSId
                    ,HSCode
                    ,StatusId
                    ,HSName
                    ,CampGrpId
                    ,ModUser
                    ,ModDate
                    ,ImportTypeId
                    ,TypeId
                    ,LevelID
                    )
                    SELECT  CollegeId
                           ,CollegeCode
                           ,StatusId
                           ,CollegeName
                           ,CampGrpId
                           ,ModUser
                           ,ModDate
                           ,1
                           ,1
                           ,1
                    FROM    dbo.adColleges; 

            INSERT  INTO syInstitutionAddresses
                    (
                     InstitutionAddressId
                    ,InstitutionId
                    ,AddressTypeId
                    ,Address1
                    ,Address2
                    ,City
                    ,StateId
                    ,ZipCode
                    ,CountryId
                    ,StatusId
                    ,IsMailingAddress
                    ,ModUser
                    ,ModDate
                    ,State
                    ,IsInternational
                    ,CountyId
                    ,ForeignCountyStr
                    ,AddressApto
                    ,OtherState
                    )
                    SELECT  NEWID()
                           ,CollegeId
                           ,@AddressTypeId
                           ,Address1
                           ,Address2
                           ,City
                           ,StateId
                           ,Zip
                           ,CountryId
                           ,StatusId
                           ,0
                           ,ModUser
                           ,ModDate
                           ,NULL
                           ,CASE ForeignZip
                              WHEN 1 THEN 1
                              ELSE 0
                            END AS IsInternational
                           ,NULL
                           ,NULL
                           ,NULL
                           ,NULL
                    FROM    dbo.adColleges;

            INSERT  INTO syInstitutionPhone
                    (
                     InstitutionPhoneId
                    ,InstitutionId
                    ,PhoneTypeId
                    ,Phone
                    ,ModDate
                    ,ModUser
                    ,IsForeignPhone
                    ,StatusId
                    )
                    SELECT  NEWID()
                           ,CollegeId
                           ,@PhoneTypeId
                           ,Phone
                           ,ModDate
                           ,ModUser
                           ,CASE ForeignPhone
                              WHEN 1 THEN 1
                              ELSE 0
                            END AS IsForeignPhone
                           ,StatusId
                    FROM    dbo.adColleges
                    WHERE   Phone IS NOT NULL;
        END;
GO
