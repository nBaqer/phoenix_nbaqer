SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--========================================================================================== 
-- USP_GetStudentDetailsForStatusBar 
--========================================================================================== 
CREATE PROCEDURE [dbo].[USP_GetStudentDetailsForStatusBar]     
/*  
CREATED:  
 
PURPOSE:  
Get student info for student status bar 
 
MODIFIED: 
7/25/2012	Sara	DE7930 QA: Education tab is missing in Center for allied school. 
7/25/2012	Sara	DE7509 QA: StudentID issue in the placeholder for Student entity 
11/4/2013	WP		US4380 Multiple Enrollment Indicator on Student Enrollment Bar 
							added optional @StuEnrollId parameter and get default enrollment function 
8/29/18     KB      AD-4881 Update Student Bar to show latest Title IV SAP Status
 
*/  @StudentId UNIQUEIDENTIFIER 
   ,@CampusId UNIQUEIDENTIFIER 
   ,@StuEnrollId UNIQUEIDENTIFIER = '00000000-0000-0000-0000-000000000000' 
AS 
    DECLARE @MinimunDate AS DATE; 
    SET @MinimunDate = '1900-01-01'; 
    BEGIN 
        IF ISNULL(@StuEnrollId,'00000000-0000-0000-0000-000000000000') = '00000000-0000-0000-0000-000000000000' 
            BEGIN 
                SET @StuEnrollId = dbo.udf_GetDefaultStudentEnrollment(@StudentId,@CampusId); 
            END; 
	 
        SELECT  CASE WHEN AST.MiddleName IS NULL 
                          OR AST.MiddleName = '' THEN AST.FirstName + ' ' + AST.LastName 
                     ELSE AST.FirstName + ' ' + AST.MiddleName + ' ' + AST.LastName 
                END AS StudentName 
               ,SC.CampDescrip AS CampusDescrip 
               ,APV.PrgVerDescrip 
               ,CASE WHEN ISNULL(APVT.ProgramVersionTypeId,0) = 0 THEN '' 
                     ELSE APVT.DescriptionProgramVersionType 
                END AS PrgVerTypeDescrip 
               ,ASE.StartDate AS StartDate 
               ,ASE.ExpGradDate AS GradDate 
               ,SSC.StatusCodeDescrip AS EnrollmentStatus 
               ,AST.StudentNumber AS StudentIdentifier 
               ,'studentid' AS StudentIdentifierCaption 
               ,SSC.SysStatusId AS SystemStatus 
               ,ASE.PrgVerId 
               ,ASE.StudentId 
               ,ASE.LeadId 
               ,ASE.CampusId 
               ,ASE.StuEnrollId 
			  ,TitleIVResult.Description
        FROM       arStuEnrollments AS ASE
        INNER JOIN adLeads AS AST ON AST.StudentId = ASE.StudentId
        INNER JOIN arPrgVersions AS APV ON APV.PrgVerId = ASE.PrgVerId
        INNER JOIN syStatusCodes AS SSC ON SSC.StatusCodeId = ASE.StatusCodeId
        INNER JOIN syCampuses AS SC ON SC.CampusId = ASE.CampusId
        INNER JOIN arProgramVersionType AS APVT ON APVT.ProgramVersionTypeId = ASE.PrgVersionTypeId
		LEFT JOIN (
				SELECT TOP 1 IVSTAT.Description, FASAP.StuEnrollId
				FROM arFASAPChkResults AS FASAP 
				INNER JOIN  syTitleIVSapStatus AS IVSTAT ON IVSTAT.Id = FASAP.TitleIVStatusId
				WHERE FASAP.StuEnrollId = @StuEnrollId
				ORDER BY FASAP.Period DESC, FASAP.DatePerformed DESC
		) AS TitleIVResult 
		ON TitleIVResult.StuEnrollId = ASE.StuEnrollId
        WHERE      ASE.StuEnrollId = @StuEnrollId
        ORDER BY   ISNULL(ASE.StartDate, @MinimunDate) DESC;
    END;
 
--========================================================================================== 
-- END  --  USP_GetStudentDetailsForStatusBar 
--========================================================================================== 
 
 
GO
