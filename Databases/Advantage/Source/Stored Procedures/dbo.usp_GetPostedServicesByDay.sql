SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/* 
PURPOSE: 
Get existing posted service for a selected student enrollment, class section, component type, and post date
see arGrdBkResults index [Cannot save duplicate of student, gradingcomponent, gradingcomponent number,PostDate&ClassSectionId- UniqueIndex Violation]

CREATED: 
10/30/2013 WP	US4547 Refactor Post Services by Student process

MODIFIED:


*/

CREATE PROCEDURE [dbo].[usp_GetPostedServicesByDay]
    (
     @StuEnrollId UNIQUEIDENTIFIER
    ,@ClsSectionId UNIQUEIDENTIFIER
    ,@InstrGrdBkWgtDetailId UNIQUEIDENTIFIER
    ,@PostDate DATETIME
    )
AS
    SET NOCOUNT ON;
    SELECT  GrdBkResultId
           ,StuEnrollId
           ,ClsSectionId
           ,InstrGrdBkWgtDetailId
           ,Score
           ,Comments
           ,ModUser
           ,ModDate
           ,ResNum
           ,PostDate
           ,IsCompGraded
           ,isCourseCredited
    FROM    arGrdBkResults
    WHERE   StuEnrollId = @StuEnrollId
            AND ClsSectionId = @ClsSectionId
            AND InstrGrdBkWgtDetailId = @InstrGrdBkWgtDetailId
            AND CONVERT(VARCHAR(10),PostDate,101) = CONVERT(VARCHAR(10),@PostDate,101);




GO
