SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[USP_AR_DoesReqhaveCoReqs]
    (
     @ReqID UNIQUEIDENTIFIER
    )
AS /*----------------------------------------------------------------------------------------------------
	Author          :	Saraswathi Lakshmanan
    
    Create date		:	02/03/2010
    
	Procedure Name	:	USP_AR_DoesReqhaveCoReqs

	Objective		:	find if the Student has OverRidden the Prereqs
	
	Parameters		:	Name			Type	Data Type			Required? 	
						=====			====	=========			=========	
						@ReqID			In		UniqueIdentifier	Required
	
	Output			:	Returns the rowcount					
						
*/-----------------------------------------------------------------------------------------------------

    BEGIN
	
        SELECT  COUNT(*) AS Count
        FROM    arCourseReqs a
        WHERE   a.ReqId = @ReqID
                AND CourseReqTypId = 2;
    END;



GO
