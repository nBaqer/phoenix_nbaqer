SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =========================================================================================================
-- Usp_GETPrgVerIdForAGivenStuEnrollId
-- =========================================================================================================
CREATE PROCEDURE [dbo].[Usp_GETPrgVerIdForAGivenStuEnrollId]
    @StuEnrollId UNIQUEIDENTIFIER
AS -- Def Program Version Track Credits it  Credits is > 0.0
    DECLARE @PrgVerId UNIQUEIDENTIFIER;
    BEGIN
        SELECT  @PrgVerId = ASE.PrgVerId
        FROM    arStuEnrollments AS ASE
        WHERE   ASE.StuEnrollId = @StuEnrollId;

        SELECT  @PrgVerId;
    END;
-- =========================================================================================================
-- END  Usp_GETPrgVerIdForAGivenStuEnrollId
-- =========================================================================================================

GO
