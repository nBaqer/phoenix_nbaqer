SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_GetGradeDescriptionsForEnrollmentNotNULL]
    (
     @stuEnrollId UNIQUEIDENTIFIER
    )
AS
    SET NOCOUNT ON;
    SELECT
		 DISTINCT
            gsd.Grade
           ,gsd.GradeDescription
    FROM    arStuEnrollments se
    INNER JOIN arPrgVersions pv ON se.PrgVerId = pv.PrgVerId
                                   AND se.StuEnrollId = @stuEnrollId
    INNER JOIN arGradeSystems gs ON pv.GrdSystemId = gs.GrdSystemId
    INNER JOIN arGradeSystemDetails gsd ON gs.GrdSystemId = gsd.GrdSystemId
                                           AND gsd.GradeDescription IS NOT NULL
                                           AND LEN(gsd.GradeDescription) > 0
    ORDER BY gsd.Grade;



GO
