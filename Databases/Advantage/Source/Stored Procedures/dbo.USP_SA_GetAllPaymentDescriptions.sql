SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[USP_SA_GetAllPaymentDescriptions]
    (
     @ShowActive NVARCHAR(50)
    ,@CampusId UNIQUEIDENTIFIER
    )
AS /*----------------------------------------------------------------------------------------------------
	Author          :	Saraswathi Lakshmanan
    
    Create date		:	04/12/2010
    
	Procedure Name	:	[USP_SA_GetAllPaymentDescriptions]

	Objective		:	Get the payment descriptions
	
	Parameters		:	Name			Type	Data Type			Required? 	
						=====			====	=========			=========	
						@ShowActive		In		nvarchar			Required
						@CampusId       In      uniqueidentifier    Required	                    
	                     
	Output			:	returns all the transcode of systype 11,12,13					
						
*/-----------------------------------------------------------------------------------------------------

    BEGIN
        DECLARE @ShowActiveValue NVARCHAR(20);	
        IF LOWER(@ShowActive) = 'true'
            BEGIN
                SET @ShowActiveValue = 'Active';
            END;
        ELSE
            BEGIN
                SET @ShowActiveValue = 'InActive';
            END;
	
        SELECT  TC.TransCodeId
               ,TC.TransCodeCode
               ,TC.TransCodeDescrip
               ,TC.StatusId
               ,TC.CampGrpId
               ,ST.StatusId
               ,ST.Status
               ,SysTransCodeId
        FROM    saTransCodes TC
               ,syStatuses ST
        WHERE   TC.StatusId = ST.StatusId
                AND SysTransCodeId IN ( 11,12,13 )
                AND ST.Status = 'active'
                AND TC.CampGrpId IN ( SELECT    CampGrpId
                                      FROM      syCmpGrpCmps
                                      WHERE     CampusId = @CampusId )
        ORDER BY TC.TransCodeDescrip;

	--SELECT    
	--	TC.TransCodeId,TC.TransCodeCode,
	--	TC.TransCodeDescrip,TC.StatusId,
	--	TC.CampGrpId,ST.StatusId,ST.Status,SysTransCodeId 
	--FROM   
	--	saTransCodes TC, syStatuses ST 
	--WHERE    
	--	TC.StatusId = ST.StatusId 
	--	and  SysTransCodeId  in (11,12,13) 
	--	AND    ST.Status = @ShowActiveValue		
	--ORDER BY
	--	TC.TransCodeDescrip

    END;





GO
