SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_WinterIPEDS_FallPartE_FTPTG_GetList]
    @CampusId VARCHAR(50)
   ,@ProgId VARCHAR(4000) = NULL
   ,@StartDate DATETIME = NULL
   ,@EndDate DATETIME = NULL
   ,@DateRangeText VARCHAR(100) = NULL
   ,@OrderBy VARCHAR(100)
AS
    DECLARE @ReturnValue VARCHAR(100);
    DECLARE @ContinuingStartDate DATETIME;
    DECLARE @Value VARCHAR(50);

    DECLARE @PreviousYear_StartDate_program DATETIME
       ,@PreviousYear_StartDate_academic DATETIME;
    DECLARE @PreviousYear_EndDate_program DATETIME
       ,@PreviousYear_EndDate_academic DATETIME;
    DECLARE @NextYear_StartDate_program DATETIME;
    DECLARE @StatusAsOf_Date DATETIME;

    SET @StatusAsOf_Date = '08/01/' + CONVERT(VARCHAR(4),DATEPART("yyyy",@EndDate));
    SET @NextYear_StartDate_program = DATEADD(YEAR,1,@StartDate);
    SET @PreviousYear_StartDate_program = DATEADD(YEAR,-1,@StartDate);
    SET @PreviousYear_EndDate_program = DATEADD(YEAR,-1,@EndDate);
    SET @PreviousYear_EndDate_academic = DATEADD(YEAR,-1,@EndDate);
    
    SET @PreviousYear_StartDate_academic = DATEADD(YEAR,-2,@EndDate);

--set @Value='letter'
-- Check if School tracks grades by letter or numeric 
    SET @Value = (
                   SELECT TOP 1
                            Value
                   FROM     dbo.syConfigAppSetValues
                   WHERE    SettingId = 47
                 );
    SET @ContinuingStartDate = @StartDate;
-- For Academic Year Reporter, the End Date should be 10/15/Cohort Year
-- and Start Date should be blank
	--if day(@EndDate)=15 and month(@EndDate)=10 
	--		begin
	--			set @EndDate = @StartDate -- it will always be 10/15/cohort year example: for cohort year 2009, it is 10/15/2009
	--			set @StartDate = @StartDate -- it will always be 10/15/cohort year example: for cohort year 2009, it is 10/15/2009
	--		end

    IF @ProgId IS NOT NULL
        BEGIN
            SELECT  @ReturnValue = COALESCE(@ReturnValue,'') + ProgDescrip + ','
            FROM    arPrograms t1
            WHERE   t1.ProgId IN ( SELECT   Val
                                   FROM     MultipleValuesForReportParameters(@ProgId,',',1) );
            SET @ReturnValue = SUBSTRING(@ReturnValue,1,LEN(@ReturnValue) - 1);
        END;
    ELSE
        BEGIN
            SELECT  @ReturnValue = COALESCE(@ReturnValue,'') + ProgDescrip + ','
            FROM    arPrograms t1
            WHERE   t1.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
                    AND t1.CampGrpId IN ( SELECT    CampGrpId
                                          FROM      syCmpGrpCmps
                                          WHERE     CampusId = @CampusId );
            SET @ReturnValue = SUBSTRING(@ReturnValue,1,LEN(@ReturnValue) - 1);
        END;

	
    CREATE TABLE #FallPartEFTG
        (
         RowNumber UNIQUEIDENTIFIER
        ,SSN VARCHAR(50)
        ,StudentNumber VARCHAR(50)
        ,StudentName VARCHAR(100)
        ,StudentId UNIQUEIDENTIFIER
        ,EnrolledFullTime VARCHAR(50)
        ,EnrolledPartTime VARCHAR(50)
        ,FullTimeReEnrolled VARCHAR(50)
        ,PartTimeReEnrolled VARCHAR(50)
        ,FullTimeCompleted VARCHAR(50)
        ,PartTimeCompleted VARCHAR(50)
        ,FullTimeExclutedFromCohort VARCHAR(50)
        ,PartTimeExclutedFromCohort VARCHAR(50)
        ,FirstTime INT
        );
		
			
    IF ( SUBSTRING(LOWER(@DateRangeText),1,10) = 'enrollment' ) -- IF ACADEMIC PROGRAM
        BEGIN
            INSERT  INTO #FallPartEFTG
                    SELECT  NEWID() AS RowNumber
                           ,dbo.UDF_FormatSSN(SSN) AS SSN
                           ,StudentNumber
                           ,StudentName
                           ,StudentId
                           ,EnrolledFullTime
                           ,EnrolledPartTime
                           ,FullTimeReEnrolled
                           ,PartTimeReEnrolled
                           ,FullTimeCompleted
                           ,PartTimeCompleted
                           ,CASE WHEN FullTimeReEnrolled = 'X' THEN ''
                                 ELSE FullTimeExclutedFromCohort
                            END AS FullTimeExclutedFromCohort
                           ,CASE WHEN PartTimeReEnrolled = 'X' THEN ''
                                 ELSE PartTimeExclutedFromCohort
                            END AS PartTimeExclutedFromCohort
                           ,1 AS FirstTime
                    FROM    (
                              SELECT    t1.SSN
                                       ,t1.StudentNumber
                                       ,t1.LastName + ', ' + t1.FirstName + ' ' + ISNULL(t1.MiddleName,'') AS StudentName
                                       ,t1.StudentId
                                       ,CASE WHEN t10.IPEDSValue = 61 THEN CASE WHEN (
                                                                                       t2.StartDate <= DATEADD(YEAR,-1,@EndDate)
                                                                                       AND (
                                                                                             SELECT COUNT(*)
                                                                                             FROM   arDropReasons dr
                                                                                             WHERE  dr.DropReasonId = t2.DropReasonId
                                                                                                    AND dr.IPEDSValue IN ( 15,16,17,18,19 )
                                                                                           ) > 0
                                                                                       AND (
                                                                                             SELECT SysStatusId
                                                                                             FROM   syStatusCodes
                                                                                             WHERE  StatusCodeId = (
                                                                                                                     SELECT TOP 1
                                                                                                                            NewStatusId
                                                                                                                     FROM   syStudentStatusChanges
                                                                                                                     WHERE  StuEnrollId = t2.StuEnrollId
                                                                                                                     ORDER BY ModDate DESC
                                                                                                                   )
                                                                                           ) = 12 --12 is DROP
                                                                                     ) THEN 'X'
                                                                                ELSE ''
                                                                           END
                                             ELSE ''
                                        END AS FullTimeExclutedFromCohort
                                       ,CASE WHEN t10.IPEDSValue = 62 THEN CASE WHEN (
                                                                                       t2.StartDate <= DATEADD(YEAR,-1,@EndDate)
                                                                                       AND (
                                                                                             SELECT COUNT(*)
                                                                                             FROM   arDropReasons dr
                                                                                             WHERE  dr.DropReasonId = t2.DropReasonId
                                                                                                    AND dr.IPEDSValue IN ( 15,16,17,18,19 )
                                                                                           ) > 0
                                                                                       AND (
                                                                                             SELECT SysStatusId
                                                                                             FROM   syStatusCodes
                                                                                             WHERE  StatusCodeId = (
                                                                                                                     SELECT TOP 1
                                                                                                                            NewStatusId
                                                                                                                     FROM   syStudentStatusChanges
                                                                                                                     WHERE  StuEnrollId = t2.StuEnrollId
                                                                                                                     ORDER BY ModDate DESC
                                                                                                                   )
                                                                                           ) = 12 --12 is DROP
                                                                                     ) THEN 'X'
                                                                                ELSE ''
                                                                           END
                                             ELSE ''
                                        END AS PartTimeExclutedFromCohort
                                       ,CASE WHEN t10.IPEDSValue = 61 THEN CASE WHEN t2.StartDate <= @EndDate THEN 'X'
                                                                                ELSE ''
                                                                           END
                                             ELSE ''
                                        END AS EnrolledFullTime
                                       ,CASE WHEN t10.IPEDSValue = 62 THEN CASE WHEN t2.StartDate <= @EndDate THEN 'X'
                                                                                ELSE ''
                                                                           END
                                             ELSE ''
                                        END AS EnrolledPartTime
                                       ,CASE WHEN t10.IPEDSValue = 61
                                             THEN CASE WHEN (
                                                              SELECT TOP 1
                                                                        B.SysStatusId
                                                              FROM      dbo.syStudentStatusChanges A
                                                              LEFT JOIN dbo.syStatusCodes B ON A.NewStatusId = B.StatusCodeId
                                                              WHERE     A.StuEnrollId = t2.StuEnrollId
                                                                        AND A.ModDate <= @EndDate
                                                              ORDER BY  A.ModDate DESC
                                                            ) IN ( 7,9,20,11,10,22 ) THEN 'X'
                                                       ELSE CASE WHEN (
                                                                        (
                                                                          SELECT TOP 1
                                                                                    B.SysStatusId
                                                                          FROM      dbo.syStudentStatusChanges A
                                                                          LEFT JOIN dbo.syStatusCodes B ON A.NewStatusId = B.StatusCodeId
                                                                          WHERE     A.StuEnrollId = t2.StuEnrollId
                                                                                    AND A.ModDate <= @EndDate
                                                                          ORDER BY  A.ModDate DESC
                                                                        ) IN ( 12,14,19,8 )
                                                                        AND (
                                                                              t2.ReEnrollmentDate >= @PreviousYear_StartDate_program
                                                                              AND t2.ReEnrollmentDate <= @EndDate
                                                                            )
                                                                      ) THEN 'X'
                                                                 ELSE ''
                                                            END
                                                  END
                                        END AS FullTimeReEnrolled
                                       ,
     --                                        THEN CASE WHEN ( ( ISNULL(t6.InSchool,
     --                                                         0) = 1
     --                                                         AND t2.StuEnrollId NOT IN (
     --                                                         SELECT
     --                                                         t1.StuEnrollId
     --                                                         FROM
     --                                                         arStuEnrollments t1 ,
     --                                                         SyStatusCodes t2
     --                                                         WHERE
     --                                                         t1.StatusCodeId = t2.StatusCodeId
     --                                                         AND LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
     --                                                         AND ( @ProgId IS NULL
     --                                                         OR t8.ProgId IN (
     --                                                         SELECT
     --                                                         Val
     --                                                         FROM
     --                                                         [MultipleValuesForReportParameters](@ProgId,
     --                                                         ',', 1) )
     --                                                         )
     --                                                         AND t2.SysStatusId IN (
     --                                                         12, 14, 19, 8 ) -- Dropped out/Transferred/Graduated/No Start
					--			-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
     --                                                         AND ( t1.DateDetermined < @EndDate
     --                                                         OR ExpGradDate < @EndDate
     --                                                         OR LDA < @EndDate
     --                                                         ) )
     --                                                         )
     --                                                         AND 
					----ReEnrolled
					----(Select COUNT(*) From arStuEnrollments SEI Where SEI.StuEnrollId=t2.StuEnrollId AND
					------(SEI.ReEnrollmentDate >=@StartDate and SEI.ReEnrollmentDate<=@EndDate))>=1)  then 'X' else '' end else '' end as FullTimeReEnrolled,
					----(SEI.ReEnrollmentDate<=@NextYear_StartDate_program))>=1
					----OR
					----Still Enrolled (Active Status)
     --                                                         ( SELECT
     --                                                         COUNT(*)
     --                                                         FROM
     --                                                         arStuEnrollments SEI ,
     --                                                         syStudentStatusChanges SSC ,
     --                                                         syStatusCodes SC
     --                                                         WHERE
     --                                                         SEI.StuEnrollId = t2.StuEnrollId
     --                                                         AND SSC.StuEnrollId = t2.StuEnrollId
     --                                                         AND ( SELECT TOP 1
     --                                                         B.SysStatusId
     --                                                         FROM
     --                                                         dbo.syStudentStatusChanges A
     --                                                         LEFT JOIN dbo.syStatusCodes B ON A.NewStatusId = B.StatusCodeId
     --                                                         WHERE
     --                                                         A.StuEnrollId = t2.StuEnrollId
     --                                                         AND A.ModDate <= @EndDate
     --                                                         ORDER BY A.ModDate DESC
     --                                                         ) IN ( 7, 9, 20,
     --                                                         11, 10, 22 )
     --                                                                                                                                                                                    --AND ( SEI.ReEnrollmentDate <= @NextYear_StartDate_program )
     --                                                         OR ( SEI.ReEnrollmentDate >= @PreviousYear_StartDate_program
     --                                                         AND SEI.ReEnrollmentDate <= @StartDate
     --                                                         )
     --                                                         ) >= 1
     --                                                       ) THEN 'X'
     --                                                  ELSE ''
     --                                             END
     --                                        ELSE ''
     --                                   END AS FullTimeReEnrolled ,
                                        CASE WHEN t10.IPEDSValue = 62
                                             THEN CASE WHEN (
                                                              SELECT TOP 1
                                                                        B.SysStatusId
                                                              FROM      dbo.syStudentStatusChanges A
                                                              LEFT JOIN dbo.syStatusCodes B ON A.NewStatusId = B.StatusCodeId
                                                              WHERE     A.StuEnrollId = t2.StuEnrollId
                                                                        AND A.ModDate <= @EndDate
                                                              ORDER BY  A.ModDate DESC
                                                            ) IN ( 7,9,20,11,10,22 ) THEN 'X'
                                                       ELSE CASE WHEN (
                                                                        (
                                                                          SELECT TOP 1
                                                                                    B.SysStatusId
                                                                          FROM      dbo.syStudentStatusChanges A
                                                                          LEFT JOIN dbo.syStatusCodes B ON A.NewStatusId = B.StatusCodeId
                                                                          WHERE     A.StuEnrollId = t2.StuEnrollId
                                                                                    AND A.ModDate <= @EndDate
                                                                          ORDER BY  A.ModDate DESC
                                                                        ) IN ( 12,14,19,8 )
                                                                        AND (
                                                                              t2.ReEnrollmentDate >= @PreviousYear_StartDate_program
                                                                              AND t2.ReEnrollmentDate <= @EndDate
                                                                            )
                                                                      ) THEN 'X'
                                                                 ELSE ''
                                                            END
                                                  END
                                        END AS PartTimeReEnrolled
                                       ,
     --                                        THEN CASE WHEN ( ( ISNULL(t6.InSchool,
     --                                                         0) = 1  
					 
					----t2.StartDate>=@StartDate and t2.StartDate<=@EndDate
     --                                                         AND t2.StuEnrollId NOT IN (
     --                                                         SELECT
     --                                                         t1.StuEnrollId
     --                                                         FROM
     --                                                         arStuEnrollments t1 ,
     --                                                         SyStatusCodes t2
     --                                                         WHERE
     --                                                         t1.StatusCodeId = t2.StatusCodeId
     --                                                         AND LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
     --                                                         AND ( @ProgId IS NULL
     --                                                         OR t8.ProgId IN (
     --                                                         SELECT
     --                                                         Val
     --                                                         FROM
     --                                                         [MultipleValuesForReportParameters](@ProgId,
     --                                                         ',', 1) )
     --                                                         )
     --                                                         AND t2.SysStatusId IN (
     --                                                         12, 14, 19, 8 ) -- Dropped out/Transferred/Graduated/No Start
					--			-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
     --                                                         AND ( t1.DateDetermined < @EndDate
     --                                                         OR ExpGradDate < @EndDate
     --                                                         OR LDA < @EndDate
     --                                                         ) )
     --                                                         )
     --                                                         AND
					------ReEnrolled
					----(Select COUNT(*) From arStuEnrollments SEI Where SEI.StuEnrollId=t2.StuEnrollId AND
					------(SEI.ReEnrollmentDate >=@StartDate and SEI.ReEnrollmentDate<=@EndDate))>=1)  then 'X' else '' end else '' end as FullTimeReEnrolled,
					----(SEI.ReEnrollmentDate<=@NextYear_StartDate_program))>=1
					----OR
					----Still Enrolled (Active Status)
     --                                                         ( SELECT
     --                                                         COUNT(*)
     --                                                         FROM
     --                                                         arStuEnrollments SEI ,
     --                                                         syStudentStatusChanges SSC ,
     --                                                         syStatusCodes SC
     --                                                         WHERE
     --                                                         SEI.StuEnrollId = t2.StuEnrollId
     --                                                         AND SSC.StuEnrollId = t2.StuEnrollId
     --                                                         AND ( SELECT TOP 1
     --                                                         B.SysStatusId
     --                                                         FROM
     --                                                         dbo.syStudentStatusChanges A
     --                                                         LEFT JOIN dbo.syStatusCodes B ON A.NewStatusId = B.StatusCodeId
     --                                                         WHERE
     --                                                         A.StuEnrollId = t2.StuEnrollId
     --                                                         AND A.ModDate <= @EndDate
     --                                                         ORDER BY A.ModDate DESC
     --                                                         ) IN ( 7, 9, 20,
     --                                                         11, 10, 22 )
     --                                                         --AND ( SEI.ReEnrollmentDate <= @NextYear_StartDate_program )
     --                                                         OR ( SEI.ReEnrollmentDate >= @PreviousYear_StartDate_program
     --                                                         AND SEI.ReEnrollmentDate <= @StartDate
     --                                                         )
     --                                                         ) >= 1
     --                                                       ) THEN 'X'
     --                                                  ELSE ''
     --                                             END
     --                                        ELSE ''
     --                                   END AS PartTimeReEnrolled ,
                                        CASE WHEN t10.IPEDSValue = 61 THEN CASE WHEN (
                                                                                       t5.SysStatusId = 14
                                                                                       AND t2.ExpGradDate <= @StatusAsOf_Date
                                                                                     ) THEN 'X'
                                                                                ELSE ''
                                                                           END
                                             ELSE ''
                                        END AS FullTimeCompleted
                                       ,CASE WHEN t10.IPEDSValue = 62 THEN CASE WHEN (
                                                                                       t5.SysStatusId = 14
                                                                                       AND t2.ExpGradDate <= @StatusAsOf_Date
                                                                                     ) THEN 'X'
                                                                                ELSE ''
                                                                           END
                                             ELSE ''
                                        END AS PartTimeCompleted

					--  -- IF ACADEMIC PROGRAM
					--(
					--	 Select Count(*) from arStuEnrollments SQ1,adDegCertSeeking  SQ2
					--	 where 
					--			SQ1.StuEnrollId = t2.StuEnrollId and 
					--			SQ1.StartDate<=@EndDate and
					--			SQ1.degcertseekingid = SQ2.DegCertSeekingId  and
					--			SQ2.IPEDSValue=11
					--)

					--as FirstTime
					--SELECT * FROM sySysStatus
                              FROM      arStudent t1
                              INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                              INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                              INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                                           AND t6.SysStatusId NOT IN ( 8 )
                              INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                              INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                              INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                              LEFT JOIN arAttendTypes t10 ON t2.AttendTypeId = t10.AttendTypeId
                              LEFT JOIN adDegCertSeeking t11 ON t2.DegCertSeekingId = t11.DegCertSeekingId
                              WHERE     LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                                        AND (
                                              @ProgId IS NULL
                                              OR t8.ProgId IN ( SELECT  Val
                                                                FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                            )
                                        AND t10.IPEDSValue IN ( 61,62 )  -- Full Time, Part Time
                                        AND t11.IPEDSValue = 11 -- First Time Degree Seelikg Students
                                        -- AND t2.StartDate <= @PreviousYear_EndDate_academic 
                                        AND (
                                              t2.StartDate > @PreviousYear_StartDate_academic
                                              AND t2.StartDate <= @PreviousYear_EndDate_academic
                                            )
					  -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
							-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
                                        AND t2.StuEnrollId NOT IN ( SELECT  t1.StuEnrollId
                                                                    FROM    arStuEnrollments t1
                                                                           ,syStatusCodes t2
                                                                    WHERE   t1.StatusCodeId = t2.StatusCodeId
                                                                            AND StartDate <= @PreviousYear_EndDate_academic
                                                                            AND -- Student started before the end date range
                                                                            LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                                                            AND (
                                                                                  @ProgId IS NULL
                                                                                  OR t8.ProgId IN ( SELECT  Val
                                                                                                    FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                                                                )
                                                                            AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
								-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                                                            AND (
                                                                                  t1.DateDetermined < @PreviousYear_EndDate_academic
                                                                                  OR ExpGradDate < @PreviousYear_EndDate_academic
                                                                                  OR LDA < @PreviousYear_EndDate_academic
                                                                                ) ) 
							-- Student Should Have Started Before the Report End Date  
							-- If Student is enrolled in only one program version and if that program version   
						   -- happens to be a continuing ed program exclude the student  
                                        AND t2.StudentId NOT IN ( SELECT DISTINCT
                                                                            StudentId
                                                                  FROM      (
                                                                              SELECT    StudentId
                                                                                       ,COUNT(*) AS RowCounter
                                                                              FROM      arStuEnrollments
                                                                              WHERE     PrgVerId IN ( SELECT    PrgVerId
                                                                                                      FROM      arPrgVersions
                                                                                                      WHERE     IsContinuingEd = 1 )
                                                                              GROUP BY  StudentId
                                                                              HAVING    COUNT(*) = 1
                                                                            ) dtStudent_ContinuingEd ) 
						 -- Exclude students who were Transferred in to your institution   
							-- This was used in FALL Part B report and we can reuse it here  
                                        AND t2.StuEnrollId NOT IN (
                                        SELECT DISTINCT
                                                SQ1.StuEnrollId
                                        FROM    arStuEnrollments SQ1
                                               ,adDegCertSeeking SQ2
                                        WHERE   SQ1.StuEnrollId = t2.StuEnrollId
                                                AND   
										-- To be considered for TransferIn, Student should be a First-Time Student  
										--SQ1.LeadId IS NOT NULL AND
                                                SQ1.degcertseekingid = SQ2.DegCertSeekingId
                                                AND SQ2.IPEDSValue = 11
                                                AND (
                                                      SQ1.TransferHours > 0
                                                      OR SQ1.StuEnrollId = CASE WHEN @Value = 'letter'
                                                                                THEN (
                                                                                       SELECT TOP 1
                                                                                                StuEnrollId
                                                                                       FROM     arTransferGrades
                                                                                       WHERE    StuEnrollId = SQ1.StuEnrollId
                                                                                                AND GrdSysDetailId IN ( SELECT  GrdSysDetailId
                                                                                                                        FROM    arGradeSystemDetails
                                                                                                                        WHERE   IsTransferGrade = 1 )
                                                                                     )
                                                                                ELSE (
                                                                                       SELECT TOP 1
                                                                                                StuEnrollId
                                                                                       FROM     arTransferGrades
                                                                                       WHERE    IsTransferred = 1
                                                                                                AND StuEnrollId = SQ1.StuEnrollId
                                                                                     )
                                                                           END
                                                    ) )
                                        AND t2.StuEnrollId IN ( SELECT TOP 1
                                                                        StuEnrollId
                                                                FROM    arStuEnrollments A1
                                                                       ,arPrgVersions A2
                                                                       ,arProgTypes A3
                                                                WHERE   A1.PrgVerId = A2.PrgVerId
                                                                        AND A2.ProgTypId = A3.ProgTypId
                                                                        AND A1.StudentId = t1.StudentId
                                                                        AND A3.IPEDSValue = 58
                                                                ORDER BY StartDate
                                                                       ,EnrollDate ASC )
                            ) dt;

        END;
    ELSE -- If PROGRAM Reporter
        BEGIN
            INSERT  INTO #FallPartEFTG
                    SELECT  NEWID() AS RowNumber
                           ,dbo.UDF_FormatSSN(SSN) AS SSN
                           ,StudentNumber
                           ,StudentName
                           ,StudentId
                           ,EnrolledFullTime
                           ,EnrolledPartTime
                           ,FullTimeReEnrolled
                           ,PartTimeReEnrolled
                           ,FullTimeCompleted
                           ,PartTimeCompleted
                           , 
			-- If ReEnrolled is set to True, Excluded from cohort set to False
                            CASE WHEN FullTimeReEnrolled = 'X' THEN ''
                                 ELSE FullTimeExclutedFromCohort
                            END AS FullTimeExclutedFromCohort
                           ,CASE WHEN PartTimeReEnrolled = 'X' THEN ''
                                 ELSE PartTimeExclutedFromCohort
                            END AS PartTimeExclutedFromCohort
                           ,1 AS FirstTime
                    FROM    (
                              SELECT    t1.SSN
                                       ,t1.StudentNumber
                                       ,t1.LastName + ', ' + t1.FirstName + ' ' + ISNULL(t1.MiddleName,'') AS StudentName
                                       ,t1.StudentId
                                       ,CASE WHEN t10.IPEDSValue = 61 THEN CASE WHEN (
                                                                                       (
                                                                                         t2.StartDate >= @PreviousYear_StartDate_program
                                                                                         AND t2.StartDate <= @PreviousYear_EndDate_program
                                                                                       )
                                                                                       AND (
                                                                                             SELECT COUNT(*)
                                                                                             FROM   arDropReasons dr
                                                                                             WHERE  dr.DropReasonId = t2.DropReasonId
                                                                                                    AND dr.IPEDSValue IN ( 15,16,17,18,19 )
                                                                                           ) > 0
                                                                                       AND (
                                                                                             SELECT SysStatusId
                                                                                             FROM   syStatusCodes
                                                                                             WHERE  StatusCodeId = (
                                                                                                                     SELECT TOP 1
                                                                                                                            NewStatusId
                                                                                                                     FROM   syStudentStatusChanges
                                                                                                                     WHERE  StuEnrollId = t2.StuEnrollId
                                                                                                                     ORDER BY ModDate DESC
                                                                                                                   )
                                                                                           ) = 12 --12 is DROP
                                                                                     ) THEN 'X'
                                                                                ELSE ''
                                                                           END
                                             ELSE ''
                                        END AS FullTimeExclutedFromCohort
                                       ,CASE WHEN t10.IPEDSValue = 62 THEN CASE WHEN (
                                                                                       (
                                                                                         t2.StartDate >= @PreviousYear_StartDate_program
                                                                                         AND t2.StartDate <= @PreviousYear_EndDate_program
                                                                                       )
                                                                                       AND (
                                                                                             SELECT COUNT(*)
                                                                                             FROM   arDropReasons dr
                                                                                             WHERE  dr.DropReasonId = t2.DropReasonId
                                                                                                    AND dr.IPEDSValue IN ( 15,16,17,18,19 )
                                                                                           ) > 0
                                                                                       AND (
                                                                                             SELECT SysStatusId
                                                                                             FROM   syStatusCodes
                                                                                             WHERE  StatusCodeId = (
                                                                                                                     SELECT TOP 1
                                                                                                                            NewStatusId
                                                                                                                     FROM   syStudentStatusChanges
                                                                                                                     WHERE  StuEnrollId = t2.StuEnrollId
                                                                                                                     ORDER BY ModDate DESC
                                                                                                                   )
                                                                                           ) = 12 --12 is DROP
                                                                                     ) THEN 'X'
                                                                                ELSE ''
                                                                           END
                                             ELSE ''
                                        END AS PartTimeExclutedFromCohort
                                       ,CASE WHEN t10.IPEDSValue = 61 THEN CASE WHEN (
                                                                                       (t2.StartDate >= @PreviousYear_StartDate_program
                                                                                       AND t2.StartDate <= @PreviousYear_EndDate_program)
                                                                                     ) THEN 'X'
                                                                                ELSE ''
                                                                           END
                                             ELSE ''
                                        END AS EnrolledFullTime
                                       ,CASE WHEN t10.IPEDSValue = 62 THEN CASE WHEN (
                                                                                       t2.StartDate >= @PreviousYear_StartDate_program
                                                                                       AND t2.StartDate <= @PreviousYear_EndDate_program
                                                                                     ) THEN 'X'
                                                                                ELSE ''
                                                                           END
                                             ELSE ''
                                        END AS EnrolledPartTime
                                       ,CASE WHEN t10.IPEDSValue = 61
                                             THEN CASE WHEN (
                                                              SELECT TOP 1
                                                                        B.SysStatusId
                                                              FROM      dbo.syStudentStatusChanges A
                                                              LEFT JOIN dbo.syStatusCodes B ON A.NewStatusId = B.StatusCodeId
                                                              WHERE     A.StuEnrollId = t2.StuEnrollId
                                                                        AND A.ModDate <= @StatusAsOf_Date
                                                              ORDER BY  A.ModDate DESC
                                                            ) IN ( 7,9,20,11,10,22 ) THEN 'X'
                                                       ELSE CASE WHEN (
                                                                        (
                                                                          SELECT TOP 1
                                                                                    B.SysStatusId
                                                                          FROM      dbo.syStudentStatusChanges A
                                                                          LEFT JOIN dbo.syStatusCodes B ON A.NewStatusId = B.StatusCodeId
                                                                          WHERE     A.StuEnrollId = t2.StuEnrollId
                                                                                    AND A.ModDate <= @StatusAsOf_Date
                                                                          ORDER BY  A.ModDate DESC
                                                                        ) IN ( 12,14,19,8 )
                                                                        AND (
                                                                              t2.ReEnrollmentDate >= @PreviousYear_StartDate_program
                                                                              AND t2.ReEnrollmentDate <= @StatusAsOf_Date
                                                                            )
                                                                      ) THEN 'X'
                                                                 ELSE ''
                                                            END
                                                  END
                                        END AS FullTimeReEnrolled
                                       ,
                                                                              
                                        
     --                                   CASE WHEN t10.IPEDSValue = 61
                                        
                                        
                                        
     --                                        THEN CASE WHEN ( ( ISNULL(t6.InSchool,
     --                                                         0) = 1
     --                                                         AND 
					----t2.StartDate>=@StartDate and t2.StartDate<=@EndDate
     --                                                         t2.StuEnrollId NOT IN (
     --                                                         SELECT
     --                                                         t1.StuEnrollId
     --                                                         FROM
     --                                                         arStuEnrollments t1 ,
     --                                                         SyStatusCodes t2
     --                                                         WHERE
     --                                                         t1.StatusCodeId = t2.StatusCodeId
     --                                                         AND LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
     --                                                         AND ( @ProgId IS NULL
     --                                                         OR t8.ProgId IN (
     --                                                         SELECT
     --                                                         Val
     --                                                         FROM
     --                                                         [MultipleValuesForReportParameters](@ProgId,
     --                                                         ',', 1) )
     --                                                         )
     --                                                         AND t2.SysStatusId IN (
     --                                                         12, 14, 19, 8 ) -- Dropped out/Transferred/Graduated/No Start
					--			-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
     --                                                         AND ( t1.DateDetermined < @StatusAsOf_Date
     --                                                         OR ExpGradDate < @StatusAsOf_Date
     --                                                         OR LDA < @StatusAsOf_Date
     --                                                         ) )
     --                                                         )
     --                                                         AND 
					----ReEnrolled
					----(Select COUNT(*) From arStuEnrollments SEI Where SEI.StuEnrollId=t2.StuEnrollId AND
					------(SEI.ReEnrollmentDate >=@StartDate and SEI.ReEnrollmentDate<=@EndDate))>=1)  then 'X' else '' end else '' end as FullTimeReEnrolled,
					----(SEI.ReEnrollmentDate<=@NextYear_StartDate_program))>=1
					----OR
					----Still Enrolled (Active Status)
                                                          
                                                          
     --                                                         ( SELECT
     --                                                         COUNT(*)
     --                                                         FROM
     --                                                         arStuEnrollments SEI ,
     --                                                         syStudentStatusChanges SSC ,
     --                                                         syStatusCodes SC
     --                                                         WHERE
     --                                                         SEI.StuEnrollId = t2.StuEnrollId
     --                                                         AND SSC.StuEnrollId = t2.StuEnrollId
     --                                                         AND ( SELECT TOP 1
     --                                                         B.SysStatusId
     --                                                         FROM
     --                                                         dbo.syStudentStatusChanges A
     --                                                         LEFT JOIN dbo.syStatusCodes B ON A.NewStatusId = B.StatusCodeId
     --                                                         WHERE
     --                                                         A.StuEnrollId = t2.StuEnrollId
     --                                                         AND A.ModDate <= @StatusAsOf_Date
     --                                                         ORDER BY A.ModDate DESC
     --                                                         ) IN ( 7, 9, 20,
     --                                                         11, 10, 22 )
     --                                                         --AND ( SEI.ReEnrollmentDate <= @NextYear_StartDate_program )
     --                                                         OR ( SEI.ReEnrollmentDate >= @PreviousYear_StartDate_program
     --                                                         AND SEI.ReEnrollmentDate <= @StartDate
     --                                                         )
     --                                                         ) >= 1
     --                                                       ) THEN 'X'
     --                                                  ELSE ''
                                                       
                                                       
                                                       
                                                       
     --                                             END
     --                                        ELSE ''
     --                                   END AS FullTimeReEnrolled ,
                                        CASE WHEN t10.IPEDSValue = 62
                                             THEN CASE WHEN (
                                                              SELECT TOP 1
                                                                        B.SysStatusId
                                                              FROM      dbo.syStudentStatusChanges A
                                                              LEFT JOIN dbo.syStatusCodes B ON A.NewStatusId = B.StatusCodeId
                                                              WHERE     A.StuEnrollId = t2.StuEnrollId
                                                                        AND A.ModDate <= @StatusAsOf_Date
                                                              ORDER BY  A.ModDate DESC
                                                            ) IN ( 7,9,20,11,10,22 ) THEN 'X'
                                                       ELSE CASE WHEN (
                                                                        (
                                                                          SELECT TOP 1
                                                                                    B.SysStatusId
                                                                          FROM      dbo.syStudentStatusChanges A
                                                                          LEFT JOIN dbo.syStatusCodes B ON A.NewStatusId = B.StatusCodeId
                                                                          WHERE     A.StuEnrollId = t2.StuEnrollId
                                                                                    AND A.ModDate <= @StatusAsOf_Date
                                                                          ORDER BY  A.ModDate DESC
                                                                        ) IN ( 12,14,19,8 )
                                                                        AND (
                                                                              t2.ReEnrollmentDate >= @PreviousYear_StartDate_program
                                                                              AND t2.ReEnrollmentDate <= @StatusAsOf_Date
                                                                            )
                                                                      ) THEN 'X'
                                                                 ELSE ''
                                                            END
                                                  END
                                        END AS PartTimeReEnrolled
                                       ,
                                       
     --                                        THEN CASE WHEN ( ( ISNULL(t6.InSchool,
     --                                                         0) = 1
     --                                                         AND t2.StuEnrollId NOT IN (
     --                                                         SELECT
     --                                                         t1.StuEnrollId
     --                                                         FROM
     --                                                         arStuEnrollments t1 ,
     --                                                         SyStatusCodes t2
     --                                                         WHERE
     --                                                         t1.StatusCodeId = t2.StatusCodeId
     --                                                         AND LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
     --                                                         AND ( @ProgId IS NULL
     --                                                         OR t8.ProgId IN (
     --                                                         SELECT
     --                                                         Val
     --                                                         FROM
     --                                                         [MultipleValuesForReportParameters](@ProgId,
     --                                                         ',', 1) )
     --                                                         )
     --                                                         AND t2.SysStatusId IN (
     --                                                         12, 14, 19, 8 ) -- Dropped out/Transferred/Graduated/No Start
					--			-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
     --                                                         AND ( t1.DateDetermined < @StatusAsOf_Date
     --                                                         OR ExpGradDate < @StatusAsOf_Date
     --                                                         OR LDA < @StatusAsOf_Date
     --                                                         ) )
     --                                                         )
     --                                                         AND 
					----ReEnrolled
					----(Select COUNT(*) From arStuEnrollments SEI Where SEI.StuEnrollId=t2.StuEnrollId AND
					----(SEI.ReEnrollmentDate >=@StartDate and SEI.ReEnrollmentDate<=@EndDate))>=1)  then 'X' else '' end else '' end as FullTimeReEnrolled,
					----(SEI.ReEnrollmentDate<=@NextYear_StartDate_program))>=1
					----AND
					----Still Enrolled (Active Status)
     --                                                         ( SELECT
     --                                                         COUNT(*)
     --                                                         FROM
     --                                                         arStuEnrollments SEI ,
     --                                                         syStudentStatusChanges SSC ,
     --                                                         syStatusCodes SC
     --                                                         WHERE
     --                                                         SEI.StuEnrollId = t2.StuEnrollId
     --                                                         AND SSC.StuEnrollId = t2.StuEnrollId
     --                                                         AND ( SELECT TOP 1
     --                                                         B.SysStatusId
     --                                                         FROM
     --                                                         dbo.syStudentStatusChanges A
     --                                                         LEFT JOIN dbo.syStatusCodes B ON A.NewStatusId = B.StatusCodeId
     --                                                         WHERE
     --                                                         A.StuEnrollId = t2.StuEnrollId
     --                                                         AND A.ModDate <= @StatusAsOf_Date
     --                                                         ORDER BY A.ModDate DESC
     --                                                         ) IN ( 7, 9, 20,
     --                                                         11, 10, 22 )
     --                                                         --AND ( SEI.ReEnrollmentDate <= @NextYear_StartDate_program )
     --                                                         OR ( SEI.ReEnrollmentDate >= @PreviousYear_StartDate_program
     --                                                         AND SEI.ReEnrollmentDate <= @StartDate
     --                                                         )
     --                                                         ) >= 1
     --                                                       ) THEN 'X'
     --                                                  ELSE ''
     --                                             END
     --                                        ELSE ''
     --                                   END AS PartTimeReEnrolled ,
					
					--case When t10.IPEDSValue=61 then Case when (t5.SysStatusId=14 AND t2.ExpGradDate>=@StartDate and 
					--t2.ExpGradDate<=@EndDate) then 'X' else '' end else '' end as FullTimeCompleted,
					--case When t10.IPEDSValue=61 then Case when (t5.SysStatusId=14 AND 
					--t2.ExpGradDate<=@NextYear_StartDate_program) then 'X' else '' end else '' end as FullTimeCompleted,
                                        CASE WHEN t10.IPEDSValue = 61 THEN CASE WHEN (
                                                                                       t5.SysStatusId = 14
                                                                                       AND t2.ExpGradDate <= @StatusAsOf_Date
                                                                                     ) THEN 'X'
                                                                                ELSE ''
                                                                           END
                                             ELSE ''
                                        END AS FullTimeCompleted
                                       ,
					
					
					--case When t10.IPEDSValue=62 then Case when (t5.SysStatusId=14 AND t2.ExpGradDate>=@StartDate and 
					--t2.ExpGradDate<=@EndDate) then 'X' else '' end else '' end as PartTimeCompleted
					--case When t10.IPEDSValue=62 then Case when (t5.SysStatusId=14 AND 
					--t2.ExpGradDate<=@NextYear_StartDate_program) then 'X' else '' end else '' end as PartTimeCompleted
                                        CASE WHEN t10.IPEDSValue = 62 THEN CASE WHEN (
                                                                                       t5.SysStatusId = 14
                                                                                       AND t2.ExpGradDate <= @StatusAsOf_Date
                                                                                     ) THEN 'X'
                                                                                ELSE ''
                                                                           END
                                             ELSE ''
                                        END AS PartTimeCompleted
					--  (
					--		 Select Count(*) from arStuEnrollments SQ1,adDegCertSeeking  SQ2
					--		 where 
					--				SQ1.StuEnrollId = t2.StuEnrollId and 
					--				(SQ1.StartDate>=@StartDate AND SQ1.StartDate<=@EndDate) and
					--				SQ1.degcertseekingid = SQ2.DegCertSeekingId  and
					--				SQ2.IPEDSValue=11
					--  )
		    
			  --as FirstTime
                              FROM      arStudent t1
                              INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                              INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                              INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                                           AND t6.SysStatusId NOT IN ( 8 )
                              INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                              INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                              INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                              LEFT JOIN arAttendTypes t10 ON t2.AttendTypeId = t10.AttendTypeId
                              LEFT JOIN adDegCertSeeking t11 ON t2.DegCertSeekingId = t11.DegCertSeekingId
                              WHERE     LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                                        AND (
                                              @ProgId IS NULL
                                              OR t8.ProgId IN ( SELECT  Val
                                                                FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                            )
                                        AND t10.IPEDSValue IN ( 61,62 )  -- Full Time, Part Time
                                        AND t11.IPEDSValue = 11 -- First Time Degree Seelikg Students
                                        AND (
                                              t2.StartDate >= @PreviousYear_StartDate_program
                                              AND t2.StartDate <= @PreviousYear_EndDate_program
                                            )
						 -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
							-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
                                        AND t2.StuEnrollId NOT IN ( SELECT  t1.StuEnrollId
                                                                    FROM    arStuEnrollments t1
                                                                           ,syStatusCodes t2
                                                                    WHERE   t1.StatusCodeId = t2.StatusCodeId
                                                                            AND StartDate <= @PreviousYear_EndDate_program
                                                                            AND -- Student started before the end date range
                                                                            LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                                                            AND (
                                                                                  @ProgId IS NULL
                                                                                  OR t8.ProgId IN ( SELECT  Val
                                                                                                    FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                                                                )
                                                                            AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
								-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                                                            AND (
                                                                                  t1.DateDetermined < @PreviousYear_EndDate_program
                                                                                  OR ExpGradDate < @PreviousYear_EndDate_program
                                                                                  OR LDA < @PreviousYear_EndDate_program
                                                                                ) ) 
							-- Student Should Have Started Before the Report End Date  
							-- If Student is enrolled in only one program version and if that program version   
						   -- happens to be a continuing ed program exclude the student  
                                        AND t2.StudentId NOT IN ( SELECT DISTINCT
                                                                            StudentId
                                                                  FROM      (
                                                                              SELECT    StudentId
                                                                                       ,COUNT(*) AS RowCounter
                                                                              FROM      arStuEnrollments
                                                                              WHERE     PrgVerId IN ( SELECT    PrgVerId
                                                                                                      FROM      arPrgVersions
                                                                                                      WHERE     IsContinuingEd = 1 )
                                                                              GROUP BY  StudentId
                                                                              HAVING    COUNT(*) = 1
                                                                            ) dtStudent_ContinuingEd )  
				 -- Exclude students who were Transferred in to your institution   
							-- This was used in FALL Part B report and we can reuse it here  
                                        AND t2.StuEnrollId NOT IN (
                                        SELECT DISTINCT
                                                SQ1.StuEnrollId
                                        FROM    arStuEnrollments SQ1
                                               ,adDegCertSeeking SQ2
                                        WHERE   SQ1.StuEnrollId = t2.StuEnrollId
                                                AND   
										-- To be considered for TransferIn, Student should be a First-Time Student  
										--SQ1.LeadId IS NOT NULL AND
                                                SQ1.degcertseekingid = SQ2.DegCertSeekingId
                                                AND SQ2.IPEDSValue = 11
                                                AND (
                                                      SQ1.TransferHours > 0
                                                      OR SQ1.StuEnrollId = CASE WHEN @Value = 'letter'
                                                                                THEN (
                                                                                       SELECT TOP 1
                                                                                                StuEnrollId
                                                                                       FROM     arTransferGrades
                                                                                       WHERE    StuEnrollId = SQ1.StuEnrollId
                                                                                                AND GrdSysDetailId IN ( SELECT  GrdSysDetailId
                                                                                                                        FROM    arGradeSystemDetails
                                                                                                                        WHERE   IsTransferGrade = 1 )
                                                                                     )
                                                                                ELSE (
                                                                                       SELECT TOP 1
                                                                                                StuEnrollId
                                                                                       FROM     arTransferGrades
                                                                                       WHERE    IsTransferred = 1
                                                                                                AND StuEnrollId = SQ1.StuEnrollId
                                                                                     )
                                                                           END
                                                    ) )
                                        AND t2.StuEnrollId IN ( SELECT TOP 1
                                                                        StuEnrollId
                                                                FROM    arStuEnrollments A1
                                                                       ,arPrgVersions A2
                                                                       ,arProgTypes A3
                                                                WHERE   A1.PrgVerId = A2.PrgVerId
                                                                        AND A2.ProgTypId = A3.ProgTypId
                                                                        AND A1.StudentId = t1.StudentId
                                                                        AND A3.IPEDSValue = 58
                                                                ORDER BY StartDate
                                                                       ,EnrollDate ASC )
                            ) dt;

        END;

    SELECT  *
    FROM    (
              SELECT    FR.SSN
                       ,FR.StudentNumber
                       ,FR.StudentName
                       ,(
                          SELECT TOP 1
                                    EnrollmentId
                          FROM      arStuEnrollments C1
                                   ,arPrgVersions C2
                                   ,arProgTypes C3
                          WHERE     C1.PrgVerId = C2.PrgVerId
                                    AND C2.ProgTypId = C3.ProgTypId
                                    AND C3.IPEDSValue = 59
                                    AND C1.StudentId = FR.StudentId
                          ORDER BY  C1.StartDate
                                   ,C1.EnrollDate
                        ) AS EnrollmentId
                       ,EnrolledFullTime
                       ,EnrolledPartTime
                       ,FullTimeReEnrolled
                       ,PartTimeReEnrolled
                       ,FullTimeCompleted
                       ,PartTimeCompleted
                       ,FullTimeExclutedFromCohort
                       ,PartTimeExclutedFromCohort
                       ,CASE ( EnrolledFullTime )
                          WHEN 'X' THEN 1
                          ELSE 0
                        END AS EnrollFTCount
                       ,CASE ( EnrolledPartTime )
                          WHEN 'X' THEN 1
                          ELSE 0
                        END AS EnrollPTCount
                       ,CASE ( FullTimeReEnrolled )
                          WHEN 'X' THEN 1
                          ELSE 0
                        END AS ReenrollFTCount
                       ,CASE ( PartTimeReEnrolled )
                          WHEN 'X' THEN 1
                          ELSE 0
                        END AS ReenrollPTCount
                       ,CASE ( FullTimeCompleted )
                          WHEN 'X' THEN 1
                          ELSE 0
                        END AS CompletedFTCount
                       ,CASE ( PartTimeCompleted )
                          WHEN 'X' THEN 1
                          ELSE 0
                        END AS CompletedPTCount
                       ,CASE ( FullTimeExclutedFromCohort )
                          WHEN 'X' THEN 1
                          ELSE 0
                        END AS ExcludedCohortFTCount
                       ,CASE ( PartTimeExclutedFromCohort )
                          WHEN 'X' THEN 1
                          ELSE 0
                        END AS ExcludedCohortPTCount
                       ,FirstTime
              FROM      #FallPartEFTG FR
            ) dt --Where dt.FirstTime >= 1
ORDER BY    CASE WHEN @OrderBy = 'SSN' THEN SSN
            END
           ,CASE WHEN @OrderBy = 'LastName' THEN StudentName
            END
           ,CASE WHEN @OrderBy = 'Student Number' THEN StudentNumber
            END
           ,CASE WHEN @OrderBy = 'EnrollmentId' THEN EnrollmentId
            END;

    DROP TABLE #FallPartEFTG;



GO
