SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--EXEC Usp_Advantage_ReportsNavigation_GetList 1689
--go
CREATE PROCEDURE [dbo].[Usp_Advantage_ReportsNavigation_GetList]
    @SchoolEnumerator INT
AS
    BEGIN
	-- declare local variables
        DECLARE @ShowRossOnlyTabs BIT
           ,@SchedulingMethod VARCHAR(50)
           ,@TrackSAPAttendance VARCHAR(50);
        DECLARE @ShowCollegeOfCourtReporting VARCHAR(5)
           ,@FameESP VARCHAR(5)
           ,@EdExpress VARCHAR(5); 
        DECLARE @GradeBookWeightingLevel VARCHAR(20)
           ,@ShowExternshipTabs VARCHAR(5);
        DECLARE @AdmissionsResourceId INT
           ,@AcademicRecordsResourceId INT
           ,@StudentAccountsResourceId INT;
        DECLARE @FacultyResourceId INT
           ,@FinancialAidResourceId INT
           ,@PlacementResourceId INT;
        DECLARE @HumanResourcesResourceId INT
           ,@SystemResourceId INT; 
	--DECLARE @SchoolEnumerator INT
	--SET @SchoolEnumerator=1689
	
	-- Set Module ResourceIds
        SET @AdmissionsResourceId = 189;
        SET @AcademicRecordsResourceId = 26;
        SET @StudentAccountsResourceId = 194;
        SET @FacultyResourceId = 300;
        SET @FinancialAidResourceId = 191;
        SET @PlacementResourceId = 193;
        SET @HumanResourcesResourceId = 192;
        SET @SystemResourceId = 195;
	
	-- Get Values
        SET @ShowRossOnlyTabs = (
                                  SELECT    VALUE
                                  FROM      dbo.syConfigAppSetValues
                                  WHERE     SettingId = 68
                                );
        SET @SchedulingMethod = (
                                  SELECT    VALUE
                                  FROM      dbo.syConfigAppSetValues
                                  WHERE     SettingId = 65
                                );
        SET @TrackSAPAttendance = (
                                    SELECT  VALUE
                                    FROM    dbo.syConfigAppSetValues
                                    WHERE   SettingId = 72
                                  );
        SET @ShowCollegeOfCourtReporting = (
                                             SELECT VALUE
                                             FROM   dbo.syConfigAppSetValues
                                             WHERE  SettingId = 118
                                           );
        SET @FameESP = (
                         SELECT VALUE
                         FROM   dbo.syConfigAppSetValues
                         WHERE  SettingId = 37
                       );
        SET @EdExpress = (
                           SELECT   VALUE
                           FROM     dbo.syConfigAppSetValues
                           WHERE    SettingId = 91
                         );
        SET @GradeBookWeightingLevel = (
                                         SELECT VALUE
                                         FROM   dbo.syConfigAppSetValues
                                         WHERE  SettingId = 43
                                       );
        SET @ShowExternshipTabs = (
                                    SELECT  VALUE
                                    FROM    dbo.syConfigAppSetValues
                                    WHERE   SettingId = 71
                                  );

	-- The first column always refers to the Module Resource Id (@AdmissionsResourceId or @AcademicRecordsResourceId or .....)
	/******************** Admissions *****************************/
        SELECT  ModuleResourceId
               ,ChildResourceId
               ,ChildResource
               ,ChildResourceURL
               ,ParentResourceId
               ,ParentResource
        FROM    (
                  SELECT    @AdmissionsResourceId AS ModuleResourceId
                           ,@AdmissionsResourceId AS ChildResourceId
                           ,'Admissions' AS ChildResource
                           ,NULL AS ChildResourceURL
                           ,NULL ParentResourceId
                           ,NULL AS ParentResource
                           ,1 AS ModuleSortOrder
                  UNION
                  SELECT    @AdmissionsResourceId AS ModuleResourceId
                           ,NNChild.ResourceId AS ChildResourceId
                           ,RChild.RESOURCE AS ChildResource
                           ,RChild.ResourceURL AS ChildResourceURL
                           ,NNParent.ResourceId AS ParentResourceId
                           ,RParent.Resource AS ParentResource
                           ,1 AS ModuleSortOrder
                  FROM      syResources RChild
                  INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
                  INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                  INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                  LEFT OUTER JOIN (
                                    SELECT  *
                                    FROM    syResources
                                    WHERE   ResourceTypeId = 1
                                  ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                  WHERE     RChild.ResourceTypeId IN ( 2,5 )
                            AND (
                                  RChild.ChildTypeId IS NULL
                                  OR RChild.ChildTypeId = 5
                                )
                            AND NNParent.ResourceId IN ( @AdmissionsResourceId,402,617 )
			-- The following condition is necessary to bring in items based on numeric or letter grade school
                            AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
                  UNION ALL
	/********************Academic Records *****************************/
                  SELECT    @AcademicRecordsResourceId AS ModuleResourceId
                           ,@AcademicRecordsResourceId AS ChildResourceId
                           ,'Academic Records' AS ChildResource
                           ,NULL AS ChildResourceURL
                           ,NULL ParentResourceId
                           ,NULL AS ParentResource
                           ,2 AS ModuleSortOrder
                  UNION
                  SELECT    @AcademicRecordsResourceId AS ModuleResourceId
                           ,NNChild.ResourceId AS ChildResourceId
                           ,RChild.RESOURCE AS ChildResource
                           ,RChild.ResourceURL AS ChildResourceURL
                           ,NNParent.ResourceId AS ParentResourceId
                           ,RParent.Resource AS ParentResource
                           ,2 AS ModuleSortOrder
                  FROM      syResources RChild
                  INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
                  INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                  INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                  LEFT OUTER JOIN (
                                    SELECT  *
                                    FROM    syResources
                                    WHERE   ResourceTypeId = 1
                                  ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                  WHERE     RChild.ResourceTypeId IN ( 2,5 )
                            AND (
                                  RChild.ChildTypeId IS NULL
                                  OR RChild.ChildTypeId = 5
                                )
					-- Bring in all report menu item tied to Academic Records, IPEDS Federal Survey, Fall, Winter, Spring
                            AND NNParent.ResourceId IN ( @AcademicRecordsResourceId,409,472,473,474 )
                            AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
                  UNION ALL
	--/********************** Student Accounts ************************************/
                  SELECT    @StudentAccountsResourceId AS ModuleResourceId
                           ,@StudentAccountsResourceId AS ChildResourceId
                           ,'Student Accounts' AS ChildResource
                           ,NULL AS ChildResourceURL
                           ,NULL ParentResourceId
                           ,NULL AS ParentResource
                           ,3 AS ModuleSortOrder
                  UNION
                  SELECT    @StudentAccountsResourceId AS ModuleResourceId
                           ,NNChild.ResourceId AS ChildResourceId
                           ,RChild.RESOURCE AS ChildResource
                           ,RChild.ResourceURL AS ChildResourceURL
                           ,NNParent.ResourceId AS ParentResourceId
                           ,RParent.Resource AS ParentResource
                           ,3 AS ModuleSortOrder
                  FROM      syResources RChild
                  INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
                  INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                  INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                  LEFT OUTER JOIN (
                                    SELECT  *
                                    FROM    syResources
                                    WHERE   ResourceTypeId = 1
                                  ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                  WHERE     RChild.ResourceTypeId IN ( 2,5 )
                            AND (
                                  RChild.ChildTypeId IS NULL
                                  OR RChild.ChildTypeId = 5
                                )
					-- Bring in all report menu item tied to Student Accounts
                            AND NNParent.ResourceId IN ( @StudentAccountsResourceId )
                            AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
                  UNION ALL
	--/********************** FACULTY ************************************/
                  SELECT    @FacultyResourceId AS ModuleResourceId
                           ,@FacultyResourceId AS ChildResourceId
                           ,'Faculty' AS ChildResource
                           ,NULL AS ChildResourceURL
                           ,NULL ParentResourceId
                           ,NULL AS ParentResource
                           ,4 AS ModuleSortOrder
                  UNION
                  SELECT    @FacultyResourceId AS ModuleResourceId
                           ,NNChild.ResourceId AS ChildResourceId
                           ,RChild.RESOURCE AS ChildResource
                           ,RChild.ResourceURL AS ChildResourceURL
                           ,NNParent.ResourceId AS ParentResourceId
                           ,RParent.Resource AS ParentResource
                           ,4 AS ModuleSortOrder
                  FROM      syResources RChild
                  INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
                  INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                  INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                  LEFT OUTER JOIN (
                                    SELECT  *
                                    FROM    syResources
                                    WHERE   ResourceTypeId = 1
                                  ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                  WHERE     RChild.ResourceTypeId IN ( 2,5 )
                            AND (
                                  RChild.ChildTypeId IS NULL
                                  OR RChild.ChildTypeId = 5
                                )
					-- Bring in all report menu item tied to Student Accounts
                            AND NNParent.ResourceId IN ( @FacultyResourceId )
                            AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
                  UNION ALL
	--/***************************** Financial Aid ******************************/
                  SELECT    @FinancialAidResourceId AS ModuleResourceId
                           ,@FinancialAidResourceId AS ChildResourceId
                           ,'Financial Aid' AS ChildResource
                           ,NULL AS ChildResourceURL
                           ,NULL ParentResourceId
                           ,NULL AS ParentResource
                           ,5 AS ModuleSortOrder
                  UNION
                  SELECT    @FinancialAidResourceId AS ModuleResourceId
                           ,NNChild.ResourceId AS ChildResourceId
                           ,RChild.RESOURCE AS ChildResource
                           ,RChild.ResourceURL AS ChildResourceURL
                           ,NNParent.ResourceId AS ParentResourceId
                           ,RParent.Resource AS ParentResource
                           ,5 AS ModuleSortOrder
                  FROM      syResources RChild
                  INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
                  INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                  INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                  LEFT OUTER JOIN (
                                    SELECT  *
                                    FROM    syResources
                                    WHERE   ResourceTypeId = 1
                                  ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                  WHERE     RChild.ResourceTypeId IN ( 2,5 )
                            AND (
                                  RChild.ChildTypeId IS NULL
                                  OR RChild.ChildTypeId = 5
                                )
					-- Bring in all report menu item tied to Student Accounts
                            AND NNParent.ResourceId IN ( @FinancialAidResourceId )
                            AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
                  UNION ALL
	--/***************************** Placement ******************************/
                  SELECT    @PlacementResourceId AS ModuleResourceId
                           ,@PlacementResourceId AS ChildResourceId
                           ,'Placement' AS ChildResource
                           ,NULL AS ChildResourceURL
                           ,NULL ParentResourceId
                           ,NULL AS ParentResource
                           ,6 AS ModuleSortOrder
                  UNION
                  SELECT    @PlacementResourceId AS ModuleResourceId
                           ,NNChild.ResourceId AS ChildResourceId
                           ,RChild.RESOURCE AS ChildResource
                           ,RChild.ResourceURL AS ChildResourceURL
                           ,NNParent.ResourceId AS ParentResourceId
                           ,RParent.Resource AS ParentResource
                           ,6 AS ModuleSortOrder
                  FROM      syResources RChild
                  INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
                  INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                  INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                  LEFT OUTER JOIN (
                                    SELECT  *
                                    FROM    syResources
                                    WHERE   ResourceTypeId = 1
                                  ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                  WHERE     RChild.ResourceTypeId IN ( 2,5 )
                            AND (
                                  RChild.ChildTypeId IS NULL
                                  OR RChild.ChildTypeId = 5
                                )
					-- Bring in all report menu item tied to Student Accounts
                            AND NNParent.ResourceId IN ( @PlacementResourceId )
                            AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
                  UNION ALL
	--/***************************** System ******************************/
                  SELECT    @SystemResourceId AS ModuleResourceId
                           ,@SystemResourceId AS ChildResourceId
                           ,'System' AS ChildResource
                           ,NULL AS ChildResourceURL
                           ,NULL ParentResourceId
                           ,NULL AS ParentResource
                           ,8 AS ModuleSortOrder
                  UNION
                  SELECT    @SystemResourceId AS ModuleResourceId
                           ,NNChild.ResourceId AS ChildResourceId
                           ,RChild.RESOURCE AS ChildResource
                           ,RChild.ResourceURL AS ChildResourceURL
                           ,NNParent.ResourceId AS ParentResourceId
                           ,RParent.Resource AS ParentResource
                           ,8 AS ModuleSortOrder
                  FROM      syResources RChild
                  INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
                  INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                  INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                  LEFT OUTER JOIN (
                                    SELECT  *
                                    FROM    syResources
                                    WHERE   ResourceTypeId = 1
                                  ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                  WHERE     RChild.ResourceTypeId IN ( 2,5 )
                            AND (
                                  RChild.ChildTypeId IS NULL
                                  OR RChild.ChildTypeId = 5
                                )
					-- Bring in all report menu item tied to Student Accounts
                            AND NNParent.ResourceId IN ( @SystemResourceId )
                            AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
                  UNION ALL
	--/***************************** Human Resources ******************************/
                  SELECT    @HumanResourcesResourceId AS ModuleResourceId
                           ,@HumanResourcesResourceId AS ChildResourceId
                           ,'Human Resources' AS ChildResource
                           ,NULL AS ChildResourceURL
                           ,NULL ParentResourceId
                           ,NULL AS ParentResource
                           ,7 AS ModuleSortOrder
                  UNION
                  SELECT    @HumanResourcesResourceId AS ModuleResourceId
                           ,NNChild.ResourceId AS ChildResourceId
                           ,RChild.RESOURCE AS ChildResource
                           ,RChild.ResourceURL AS ChildResourceURL
                           ,NNParent.ResourceId AS ParentResourceId
                           ,RParent.Resource AS ParentResource
                           ,7 AS ModuleSortOrder
                  FROM      syResources RChild
                  INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
                  INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                  INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                  LEFT OUTER JOIN (
                                    SELECT  *
                                    FROM    syResources
                                    WHERE   ResourceTypeId = 1
                                  ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                  WHERE     RChild.ResourceTypeId IN ( 2,5 )
                            AND (
                                  RChild.ChildTypeId IS NULL
                                  OR RChild.ChildTypeId = 5
                                )
					-- Bring in all report menu item tied to Student Accounts
                            AND NNParent.ResourceId IN ( @HumanResourcesResourceId )
                            AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
                ) MainQuery
        WHERE   -- Hide resources based on Configuration Settings
                (
                  -- The following expression means if showross... is set to false, hide 
					-- ResourceIds 541,542,532,534,535,538,543,539
					-- (Not False) OR (Condition)
				  (
                    ( @ShowRossOnlyTabs <> 0 )
                    OR ( ChildResourceId NOT IN ( 541,542,532,534,535,538,543,539 ) )
                  )
                  AND
					-- The following expression means if showross... is set to true, hide 
					-- ResourceIds 142,375,330,476,508,102,107,237
					-- (Not True) OR (Condition)
                  (
                    ( @ShowRossOnlyTabs <> 1 )
                    OR ( ChildResourceId NOT IN ( 142,375,330,476,508,102,107,237 ) )
                  )
                  AND
					---- The following expression means if SchedulingMethod=regulartraditional, hide 
					---- ResourceIds 91 and 497
					---- (Not True) OR (Condition)
                  (
                    (
                      NOT LOWER(LTRIM(RTRIM(@SchedulingMethod))) = 'regulartraditional'
                    )
                    OR ( ChildResourceId NOT IN ( 91,497 ) )
                  )
                  AND
					-- The following expression means if TrackSAPAttendance=byday, hide 
					-- ResourceIds 633
					-- (Not True) OR (Condition)
                  (
                    (
                      NOT LOWER(LTRIM(RTRIM(@TrackSAPAttendance))) = 'byday'
                    )
                    OR ( ChildResourceId NOT IN ( 633 ) )
                  )
                  AND
					---- The following expression means if @ShowCollegeOfCourtReporting is set to false, hide 
					---- ResourceIds 614,615
					---- (Not False) OR (Condition)
                  (
                    (
                      NOT LOWER(LTRIM(RTRIM(@ShowCollegeOfCourtReporting))) = 'no'
                    )
                    OR ( ChildResourceId NOT IN ( 614,615 ) )
                  )
                  AND
					-- The following expression means if @ShowCollegeOfCourtReporting is set to true, hide 
					-- ResourceIds 497
					-- (Not True) OR (Condition)
                  (
                    (
                      NOT LOWER(LTRIM(RTRIM(@ShowCollegeOfCourtReporting))) = 'yes'
                    )
                    OR ( ChildResourceId NOT IN ( 497 ) )
                  )
                  AND
					-- The following expression means if FAMEESP is set to false, hide 
					-- ResourceIds 517,523, 525
					-- (Not False) OR (Condition)
                  (
                    (
                      NOT LOWER(LTRIM(RTRIM(@FameESP))) = 'no'
                    )
                    OR ( ChildResourceId NOT IN ( 517,523,525 ) )
                  )
                  AND
					---- The following expression means if EDExpress is set to false, hide 
					---- ResourceIds 603,604,606,619
					---- (Not False) OR (Condition)
                  (
                    (
                      NOT LOWER(LTRIM(RTRIM(@EdExpress))) = 'no'
                    )
                    OR ( ChildResourceId NOT IN ( 603,604,605,606,619 ) )
                  )
                  AND
					---- The following expression means if @GradeBookWeightingLevel is set to courselevel, hide 
					---- ResourceIds 107,96,222
					---- (Not False) OR (Condition)
                  (
                    (
                      NOT LOWER(LTRIM(RTRIM(@GradeBookWeightingLevel))) = 'courselevel'
                    )
                    OR ( ChildResourceId NOT IN ( 107,96,222 ) )
                  )
                  AND (
                        (
                          NOT LOWER(LTRIM(RTRIM(@GradeBookWeightingLevel))) = 'instructorlevel'
                        )
                        OR ( ChildResourceId NOT IN ( 476 ) )
                      )
                  AND (
                        (
                          NOT LOWER(LTRIM(RTRIM(@ShowExternshipTabs))) = 'no'
                        )
                        OR ( ChildResourceId NOT IN ( 543,538 ) )
                      )
                )
        ORDER BY ModuleSortOrder
               ,ModuleResourceId
               ,ChildResource
               ,ParentResourceId; 
    END; 		



GO
