SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_GetLeadDetailsForStatusBar]
    @LeadId UNIQUEIDENTIFIER
AS
    DECLARE @LeadIdentifier VARCHAR(50);  
    SET @LeadIdentifier = (
                            SELECT  Value
                            FROM    dbo.syConfigAppSetValues
                            WHERE   SettingId = 69
                          );  
   
    SELECT  S.FirstName + ' ' + ISNULL(S.MiddleName,'') + ' ' + S.LastName AS LeadName
           ,S.LeadId AS LeadId
           ,(
              SELECT TOP 1
                        PrgVerDescrip
              FROM      arPrgVersions SQ1
              WHERE     SQ1.PrgVerId = S.PrgVerId
            ) AS PrgVerDesc
           ,(
              SELECT TOP 1
                        StartDate
              FROM      arStuEnrollments SQ2
              WHERE     SQ2.LeadId = S.LeadId
              ORDER BY  StartDate DESC
            ) AS StudentStartDate
           ,(
              SELECT TOP 1
                        ExpGradDate
              FROM      arStuEnrollments SQ2
              WHERE     SQ2.LeadId = S.LeadId
              ORDER BY  StartDate DESC
            ) AS GraduationDate
           ,(
              SELECT TOP 1
                        StatusCodeDescrip
              FROM      syStatusCodes SQ1
              WHERE     SQ1.StatusCodeId = S.LeadStatus
            ) AS LeadStatus
           ,(
              SELECT TOP 1
                        StudentId
              FROM      arStuEnrollments SQ2
              WHERE     SQ2.LeadId = S.LeadId
            ) AS StudentId
           ,DATEADD(dd,DATEDIFF(dd,0,S.AssignedDate),0) AS AssignedDate
           ,(
              SELECT    U.FullName
              FROM      syUsers U
              WHERE     U.UserId = S.AdmissionsRep
            ) AS AdmissionsRep
           ,(
              SELECT    C.CampDescrip
              FROM      dbo.syCampuses C
              WHERE     C.CampusId = S.CampusId
            ) AS CampusDescription
           ,S.CampusId AS CampusId
           ,(
              SELECT TOP 1
                        SysStatusId
              FROM      syStatusCodes SQ1
              WHERE     SQ1.StatusCodeId = S.LeadStatus
            ) AS SystemStatus
    FROM    adLeads S
    WHERE   S.LeadId = @LeadId;  
 



GO
