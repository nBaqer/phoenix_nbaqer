SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_GetLDAForTermCutOffDate]
    (
     @stuEnrollId UNIQUEIDENTIFIER
    ,@cutOffDate DATETIME  
	)
AS
    SET NOCOUNT ON;  
  
    SELECT  DATEADD(dd,0,DATEDIFF(dd,0,MAX(LDA)))
    FROM    (
              SELECT    MAX(AttendedDate) AS LDA
              FROM      arExternshipAttendance
              WHERE     StuEnrollId = @stuEnrollId
                        AND AttendedDate <= @cutOffDate
              UNION ALL
              SELECT    MAX(MeetDate) AS LDA
              FROM      atClsSectAttendance
              WHERE     StuEnrollId = @stuEnrollId
                        AND Actual >= 1
                        AND MeetDate <= @cutOffDate
              UNION ALL
              SELECT    MAX(AttendanceDate) AS LDA
              FROM      atAttendance
              WHERE     EnrollId = @stuEnrollId
                        AND Actual >= 1
                        AND AttendanceDate <= @cutOffDate
              UNION ALL
              SELECT    MAX(RecordDate) AS LDA
              FROM      arStudentClockAttendance
              WHERE     StuEnrollId = @stuEnrollId
                        AND (
                              ActualHours >= 1.00
                              AND ActualHours <> 99.00
                              AND ActualHours <> 999.00
                              AND ActualHours <> 9999.00
                            )
                        AND RecordDate <= @cutOffDate
              UNION ALL
              SELECT    MAX(MeetDate) AS LDA
              FROM      atConversionAttendance
              WHERE     StuEnrollId = @stuEnrollId
                        AND (
                              Actual >= 1.00
                              AND Actual <> 99.00
                              AND Actual <> 999.00
                              AND Actual <> 9999.00
                            )
                        AND MeetDate <= @cutOffDate
              UNION ALL
              SELECT    LDA
              FROM      arStuEnrollments
              WHERE     StuEnrollId = @stuEnrollId
                        AND LDA <= @cutOffDate
            ) T0;   
			



GO
