SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[sp_update_messageschema]
    @TemplateDescrip VARCHAR(200)
   ,@MessageSchemaId UNIQUEIDENTIFIER
AS
    UPDATE  syMessageTemplates
    SET     MessageSchemaId = @MessageSchemaId
    WHERE   TemplateDescrip = @TemplateDescrip;



GO
