SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_GetGradeDescriptionsForEnrollment]
    (
     @stuEnrollId UNIQUEIDENTIFIER
    )
AS
    SET NOCOUNT ON;
    SELECT 
		DISTINCT
            gsd.Grade
           ,gsd.GPA
           ,(
              SELECT TOP 1
                        CAST(MinVal AS VARCHAR) + ' - ' + CAST(MaxVal AS VARCHAR)
              FROM      arGradeScaleDetails gscd
                       ,arGradeScales gsc
              WHERE     gscd.GrdScaleId = gsc.GrdScaleId
                        AND gscd.GrdSysDetailId = gsd.GrdSysDetailId
                        AND gsc.GrdSystemId = gs.GrdSystemId
            ) AS Range
           ,gsd.GradeDescription
           ,gsd.quality
    FROM    arStuEnrollments se
    INNER JOIN arPrgVersions pv ON se.PrgVerId = pv.PrgVerId
                                   AND se.StuEnrollId = @stuEnrollId
    INNER JOIN arGradeSystems gs ON pv.GrdSystemId = gs.GrdSystemId
    INNER JOIN arGradeSystemDetails gsd ON gs.GrdSystemId = gsd.GrdSystemId
    ORDER BY gsd.GPA DESC
           ,gsd.Grade;



GO
