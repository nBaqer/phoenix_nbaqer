SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_SA_LeadPayments_PrintReceipt]
    @CampusId UNIQUEIDENTIFIER
   ,@TransactionId UNIQUEIDENTIFIER
AS
    SET NOCOUNT ON;
    BEGIN
	
        DECLARE @LeadReceiptData TABLE
            (
             SchoolName VARCHAR(100) NOT NULL
            ,SchoolAddress1 VARCHAR(200) NOT NULL
            ,SchoolAddress2 VARCHAR(200) NULL
            ,SchoolCity VARCHAR(50) NOT NULL
            ,SchoolState VARCHAR(50) NOT NULL
            ,SchoolZip VARCHAR(20) NOT NULL
            ,SchoolCountry VARCHAR(50) NULL
            ,LeadName VARCHAR(100) NOT NULL
            ,LeadAddress1 VARCHAR(200) NULL
            ,LeadAddress2 VARCHAR(200) NULL
            ,LeadCity VARCHAR(50) NULL
            ,LeadState VARCHAR(50) NULL
            ,LeadZip VARCHAR(20) NULL
            ,LeadCountry VARCHAR(50) NULL
            ,TransDate VARCHAR(10) NOT NULL
            ,TransDesc VARCHAR(50) NULL
            ,PaymentTypeDesc VARCHAR(50) NULL
            ,CheckNumber VARCHAR(50) NULL
            ,TransAmount DECIMAL(19,4) NOT NULL
            ,Voided BIT NULL
            );
		
        DECLARE @SchoolData TABLE
            (
             SchoolName VARCHAR(100) NOT NULL
            ,SchoolAddress1 VARCHAR(200) NOT NULL
            ,SchoolAddress2 VARCHAR(200) NULL
            ,SchoolCity VARCHAR(50) NOT NULL
            ,SchoolState VARCHAR(50) NULL
            ,SchoolZip VARCHAR(20) NOT NULL
            ,SchoolCountry VARCHAR(50) NULL
            );
		
        DECLARE @LeadData TABLE
            (
             LeadName VARCHAR(100) NOT NULL
            ,LeadAddress1 VARCHAR(200) NULL
            ,LeadAddress2 VARCHAR(200) NULL
            ,LeadCity VARCHAR(50) NULL
            ,LeadState VARCHAR(50) NULL
            ,LeadZip VARCHAR(20) NULL
            ,LeadCountry VARCHAR(50) NULL
            );
		
				
		--Get School Address
        DECLARE @AddressToBePrintedInReceipts VARCHAR(50);
        SET @AddressToBePrintedInReceipts = (
                                              SELECT    Value
                                              FROM      syConfigAppSetValues v
                                              INNER JOIN syConfigAppSettings s ON s.SettingId = v.SettingId
                                              WHERE     KeyName = 'AddressToBePrintedInReceipts'
                                            );
		
        BEGIN
            IF @AddressToBePrintedInReceipts = 'CorporateAddress'
                BEGIN
                    INSERT  INTO @SchoolData
                            (
                             SchoolName
                            ,SchoolAddress1
                            ,SchoolAddress2
                            ,SchoolCity
                            ,SchoolState
                            ,SchoolZip
                            ,SchoolCountry 				
				            )
                            SELECT  (
                                      SELECT    Value
                                      FROM      dbo.syConfigAppSetValues v
                                      INNER JOIN dbo.syConfigAppSettings s ON s.SettingId = v.SettingId
                                      WHERE     KeyName LIKE 'CorporateName'
                                    ) AS SchoolName
                                   ,(
                                      SELECT    Value
                                      FROM      dbo.syConfigAppSetValues v
                                      INNER JOIN dbo.syConfigAppSettings s ON s.SettingId = v.SettingId
                                      WHERE     KeyName LIKE 'CorporateAddress1'
                                    ) AS SchoolAddress1
                                   ,(
                                      SELECT    Value
                                      FROM      dbo.syConfigAppSetValues v
                                      INNER JOIN dbo.syConfigAppSettings s ON s.SettingId = v.SettingId
                                      WHERE     KeyName LIKE 'CorporateAddress2'
                                    ) AS SchoolAddress2
                                   ,(
                                      SELECT    Value
                                      FROM      dbo.syConfigAppSetValues v
                                      INNER JOIN dbo.syConfigAppSettings s ON s.SettingId = v.SettingId
                                      WHERE     KeyName LIKE 'CorporateCity'
                                    ) AS SchoolCity
                                   ,(
                                      SELECT    Value
                                      FROM      dbo.syConfigAppSetValues v
                                      INNER JOIN dbo.syConfigAppSettings s ON s.SettingId = v.SettingId
                                      WHERE     KeyName LIKE 'CorporateState'
                                    ) AS SchoolState
                                   ,(
                                      SELECT    Value
                                      FROM      dbo.syConfigAppSetValues v
                                      INNER JOIN dbo.syConfigAppSettings s ON s.SettingId = v.SettingId
                                      WHERE     KeyName LIKE 'CorporateZip'
                                    ) AS SchoolZip
                                   ,(
                                      SELECT    Value
                                      FROM      dbo.syConfigAppSetValues v
                                      INNER JOIN dbo.syConfigAppSettings s ON s.SettingId = v.SettingId
                                      WHERE     KeyName LIKE 'CorporateCountry'
                                    ) AS SchoolCountry;
                END;
				
            IF @AddressToBePrintedInReceipts = 'CampusAddress'
                BEGIN			
                    INSERT  INTO @SchoolData
                            (
                             SchoolName
                            ,SchoolAddress1
                            ,SchoolAddress2
                            ,SchoolCity
                            ,SchoolState
                            ,SchoolZip
                            ,SchoolCountry 	
				            )
                            SELECT  (
                                      SELECT    CampDescrip
                                      FROM      dbo.syCampuses
                                      WHERE     CampusId = @CampusId
                                    ) AS SchoolName
                                   ,(
                                      SELECT    Address1
                                      FROM      dbo.syCampuses
                                      WHERE     CampusId = @CampusId
                                    ) AS SchoolAddress1
                                   ,(
                                      SELECT    Address2
                                      FROM      dbo.syCampuses
                                      WHERE     CampusId = @CampusId
                                    ) AS SchoolAddress2
                                   ,(
                                      SELECT    City
                                      FROM      dbo.syCampuses
                                      WHERE     CampusId = @CampusId
                                    ) AS SchoolCity
                                   ,(
                                      SELECT    StateDescrip
                                      FROM      syStates s
                                      INNER JOIN syCampuses c ON s.StateId = c.StateId
                                      WHERE     CampusId = @CampusId
                                    ) AS SchoolState
                                   ,(
                                      SELECT    Zip
                                      FROM      dbo.syCampuses
                                      WHERE     CampusId = @CampusId
                                    ) AS SchoolZip
                                   ,(
                                      SELECT    CountryDescrip
                                      FROM      dbo.adCountries s
                                      INNER JOIN syCampuses c ON s.CountryId = c.CountryId
                                      WHERE     CampusId = @CampusId
                                    ) AS SchoolCountry;
                END;	
		
        END;		
		
		--Get Lead Address
        BEGIN		
            INSERT  INTO @LeadData
                    (
                     LeadName
                    ,LeadAddress1
                    ,LeadAddress2
                    ,LeadCity
                    ,LeadState
                    ,LeadZip
                    ,LeadCountry 		
			        )
                    SELECT  ( LastName + ', ' + FirstName ) AS LeadName
                           ,Address1
                           ,Address2
                           ,City
                           ,(
                              SELECT    StateDescrip
                              FROM      syStates
                              WHERE     StateId = AL.StateId
                            ) AS LeadState
                           ,Zip
                           ,(
                              SELECT    CountryDescrip
                              FROM      adCountries
                              WHERE     CountryId = AL.Country
                            ) AS LeadCountry
                    FROM    adLeads AL
                    WHERE   LeadId = (
                                       SELECT   LeadId
                                       FROM     dbo.adLeadTransactions
                                       WHERE    TransactionId = @TransactionId
                                     );
            BEGIN
                INSERT  INTO @LeadReceiptData
                        (
                         SchoolName
                        ,SchoolAddress1
                        ,SchoolAddress2
                        ,SchoolCity
                        ,SchoolState
                        ,SchoolZip
                        ,SchoolCountry
                        ,LeadName
                        ,LeadAddress1
                        ,LeadAddress2
                        ,LeadCity
                        ,LeadState
                        ,LeadZip
                        ,LeadCountry
                        ,TransDate
                        ,TransDesc
                        ,PaymentTypeDesc
                        ,CheckNumber
                        ,TransAmount
                        ,Voided 			
			            )
                        SELECT  SchoolName AS SchoolName
                               ,SchoolAddress1 AS SchoolAddress1
                               ,SchoolAddress2 AS SchoolAddress2
                               ,SchoolCity AS SchoolCity
                               ,SchoolState AS SchoolState
                               ,SchoolZip AS SchoolZip
                               ,SchoolCountry AS SchoolCountry
                               ,LeadName AS LeadName
                               ,LeadAddress1 AS LeadAddress1
                               ,LeadAddress2 AS LeadAddress2
                               ,LeadCity AS LeadtCity
                               ,LeadState AS LeadState
                               ,LeadZip AS LeadZip
                               ,LeadCountry AS LeadCountry
                               ,CONVERT(VARCHAR(10),(
                                                      SELECT    CreatedDate
                                                      FROM      dbo.adLeadTransactions
                                                      WHERE     TransactionId = @TransactionId
                                                    ),101) AS TransDate
                               ,(
                                  SELECT    TransCodeDescrip
                                  FROM      dbo.saTransCodes
                                  WHERE     TransCodeId = (
                                                            SELECT  TransCodeId
                                                            FROM    dbo.adLeadTransactions
                                                            WHERE   TransactionId = @TransactionId
                                                          )
                                ) AS TransDesc
                               ,(
                                  SELECT    Description
                                  FROM      dbo.saPaymentTypes PT
                                  INNER JOIN dbo.adLeadPayments P ON PT.PaymentTypeId = P.PaymentTypeId
                                  WHERE     P.TransactionId = @TransactionId
                                ) AS PaymentTypeDesc
                               ,(
                                  SELECT    CheckNumber
                                  FROM      dbo.adLeadPayments
                                  WHERE     TransactionId = @TransactionId
                                ) AS CheckNumber
                               ,(
                                  SELECT    TransAmount
                                  FROM      dbo.adLeadTransactions
                                  WHERE     TransactionId = @TransactionId
                                ) AS TransAmount
                               ,(
                                  SELECT    Voided
                                  FROM      dbo.adLeadTransactions
                                  WHERE     TransactionId = @TransactionId
                                ) AS Voided
			-- T.TransAmount AS TransAmount
			-- FROM @SchoolData, @LeadData, dbo.saTransactions T, dbo.saPayments P
			-- WHERE T.TransactionId = @TransactionId AND T.Voided = 0
			--   AND P.TransactionId = @TransactionId
                        FROM    @SchoolData
                               ,@LeadData;
            END;
		
            SELECT  SchoolName
                   ,SchoolAddress1
                   ,SchoolAddress2
                   ,SchoolCity
                   ,SchoolState
                   ,SchoolZip
                   ,SchoolCountry
                   ,LeadName
                   ,LeadAddress1
                   ,LeadAddress2
                   ,LeadCity
                   ,LeadState
                   ,LeadZip
                   ,LeadCountry
                   ,TransDate
                   ,TransDesc
                   ,PaymentTypeDesc
                   ,CheckNumber
                   ,ABS(CONVERT(DECIMAL(18,2),ROUND(TransAmount,2))) AS TransAmount
                   ,Voided
				--ABS([TransAmount]) AS TransAmount 	
            FROM    @LeadReceiptData;
		
        END;
    END;


GO
