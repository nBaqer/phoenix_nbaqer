SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[USP_IL_AddImportLeadsLookupValsMapping]
    @MappingId UNIQUEIDENTIFIER
   ,@ILFieldId INT
   ,@FileValue VARCHAR(200)
   ,@AdvValue VARCHAR(50)
AS
    INSERT  INTO dbo.adImportLeadsLookupValsMap
            (
             LookupValuesMapId
            ,MappingId
            ,ILFieldId
            ,FileValue
            ,AdvValue
			)
            SELECT  NEWID()
                   ,@MappingId
                   ,@ILFieldId
                   ,@FileValue
                   ,@AdvValue; 









GO
