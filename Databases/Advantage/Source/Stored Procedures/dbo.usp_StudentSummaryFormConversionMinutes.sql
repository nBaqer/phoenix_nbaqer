SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_StudentSummaryFormConversionMinutes]
    (
     @stuEnrollId UNIQUEIDENTIFIER
    ,@cutOffDate DATETIME
    )
AS
    SET NOCOUNT ON;

    SELECT DISTINCT
            SCA.StuEnrollId
           ,(
              SELECT    SUM(Schedule)
              FROM      atConversionAttendance
              WHERE     StuEnrollId = SCA.StuEnrollId
                        AND Schedule >= 1.00
                        AND (
                              Actual <> 999.00
                              AND Actual <> 9999.00
                            )
            ) AS SchedHours
           ,(
              SELECT    SUM(Actual)
              FROM      atConversionAttendance
              WHERE     StuEnrollId = SCA.StuEnrollId
                        AND (
                              Actual >= 1.00
                              AND Actual <> 999.00
                              AND Actual <> 9999.00
                            )
            ) AS TotalPresentHours
           ,(
              SELECT    SUM(Schedule - Actual)
              FROM      atConversionAttendance
              WHERE     StuEnrollId = SCA.StuEnrollId
                        AND (
                              Actual <> 999.00
                              AND Actual <> 9999.00
                            )
                        AND ( Schedule >= 1.00 )
            ) AS TotalHoursAbsent
           ,(
              SELECT    SUM(Actual)
              FROM      atConversionAttendance
              WHERE     StuEnrollId = SCA.StuEnrollId
                        AND (
                              (
                                Actual = 1.00
                                AND Actual <> 999.00
                                AND Actual <> 9999.00
                              )
                              AND (
                                    Schedule IS NULL
                                    OR Schedule = 0.00
                                    OR Schedule = 999.00
                                  )
                            )
            ) AS TotalMakeUpHours
           ,(
              SELECT    SUM(t4.total)
              FROM      arStuEnrollments t1
                       ,arStudentSchedules t2
                       ,arProgSchedules t3
                       ,arProgScheduleDetails t4
              WHERE     t1.StuEnrollId = t2.StuEnrollId
                        AND t1.PrgVerId = t3.PrgVerId
                        AND t2.ScheduleId = t3.ScheduleId
                        AND t3.ScheduleId = t4.ScheduleId
                        AND t2.StuEnrollId = SCA.StuEnrollId
                        AND t4.total IS NOT NULL
            ) AS ScheduleHoursByWeek
           ,(
              SELECT    MAX(MeetDate)
              FROM      atConversionAttendance
              WHERE     StuEnrollId = SCA.StuEnrollId
                        AND (
                              Actual >= 1.00
                              AND Actual <> 999.00
                              AND Actual <> 9999.00
                            )
            ) AS LastDateAttended
           ,(
              SELECT    COUNT(*)
              FROM      atConversionAttendance
              WHERE     StuEnrollId = SCA.StuEnrollId
                        AND Tardy = 1
            ) AS TardyCount
    FROM    atConversionAttendance SCA
    WHERE   SCA.StuEnrollId = @stuEnrollId
            AND SCA.MeetDate <= @cutOffDate
    ORDER BY SchedHours DESC
           ,TotalPresentHours DESC; 



GO
