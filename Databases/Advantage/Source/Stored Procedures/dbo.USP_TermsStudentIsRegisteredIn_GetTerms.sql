SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_TermsStudentIsRegisteredIn_GetTerms]
    @StudentId UNIQUEIDENTIFIER
   ,@CampusId UNIQUEIDENTIFIER
   ,@TermStartDate DATETIME
AS
    DECLARE @DictationTestComponent TABLE
        (
         GrdComponentTypeId UNIQUEIDENTIFIER
        );
    INSERT  INTO @DictationTestComponent
            SELECT  GrdComponentTypeId
            FROM    arGrdComponentTypes
            WHERE   SysComponentTypeId = 612;
    SELECT DISTINCT
            T.TermId
           ,T.TermDescrip
           ,T.StartDate
    FROM    arTerm T
    INNER JOIN syStatuses ST ON T.StatusId = ST.StatusId
    INNER JOIN arClassSectionTerms CST ON T.TermId = CST.TermId
    INNER JOIN arClassSections CS ON CST.ClsSectionId = CS.ClsSectionId
    INNER JOIN arBridge_GradeComponentTypes_Courses BGC ON CS.ReqId = BGC.ReqId
    INNER JOIN @DictationTestComponent DTC ON BGC.GrdComponentTypeId = DTC.GrdComponentTypeId
    INNER JOIN arResults R ON CS.ClsSectionId = R.TestId
    INNER JOIN arStuEnrollments SE ON R.StuEnrollId = SE.StuEnrollId
    INNER JOIN arStudent S ON SE.StudentId = S.StudentId
    WHERE   T.StartDate <= @TermStartDate
            AND ST.Status = 'Active'
            AND S.StudentId = @StudentId
            AND (
                  T.CampGrpId IN ( SELECT   CampGrpId
                                   FROM     syCmpGrpCmps
                                   WHERE    CampusId = @CampusId
                                            AND CampGrpId <> (
                                                               SELECT   CampGrpId
                                                               FROM     syCampGrps
                                                               WHERE    CampGrpDescrip = 'All'
                                                             ) )
                  OR CampGrpId = (
                                   SELECT   CampGrpId
                                   FROM     syCampGrps
                                   WHERE    CampGrpDescrip = 'All'
                                 )
                )
    ORDER BY T.StartDate
           ,T.TermDescrip;




GO
