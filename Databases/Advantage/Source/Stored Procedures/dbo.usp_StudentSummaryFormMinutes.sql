SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_StudentSummaryFormMinutes]
    (
     @stuEnrollId UNIQUEIDENTIFIER
    ,@cutOffDate DATETIME
    )
AS
    SET NOCOUNT ON;

    SELECT DISTINCT
            SCA.StuEnrollId
           ,(
              SELECT    SUM(SchedHours)
              FROM      arStudentClockAttendance
              WHERE     StuEnrollId = SCA.StuEnrollId
                        AND SchedHours >= 0.00
                        AND (
                              ActualHours IS NOT NULL
                              AND ActualHours <> 999.00
                              AND ActualHours <> 9999.00
                            )
            ) AS SchedHours
           ,(
              SELECT    SUM(ActualHours)
              FROM      arStudentClockAttendance
              WHERE     StuEnrollId = SCA.StuEnrollId
                        AND (
                              ActualHours >= 0.00
                              AND ActualHours <> 999.00
                              AND ActualHours <> 9999.00
                            )
            ) AS TotalPresentHours
           ,(
              SELECT    SUM(SchedHours - ActualHours)
              FROM      arStudentClockAttendance
              WHERE     StuEnrollId = SCA.StuEnrollId
                        AND (
                              ActualHours <> 999.00
                              AND ActualHours <> 9999.00
                              AND SchedHours >= 0.00
                              AND SchedHours > ActualHours
                            )
            ) AS TotalHoursAbsent
           ,(
              SELECT    SUM(ActualHours - SchedHours)
              FROM      arStudentClockAttendance
              WHERE     StuEnrollId = SCA.StuEnrollId
                        AND (
                              (
                                ActualHours >= 0.00
                                AND ActualHours <> 999.00
                                AND ActualHours <> 9999.00
                              )
                              AND ( SchedHours < ActualHours )
                            )
            ) AS TotalMakeUpHours
           ,(
              SELECT    SUM(t4.total)
              FROM      arStuEnrollments t1
                       ,arStudentSchedules t2
                       ,arProgSchedules t3
                       ,arProgScheduleDetails t4
              WHERE     t1.StuEnrollId = t2.StuEnrollId
                        AND t1.PrgVerId = t3.PrgVerId
                        AND t2.ScheduleId = t3.ScheduleId
                        AND t3.ScheduleId = t4.ScheduleId
                        AND t2.StuEnrollId = SCA.StuEnrollId
                        AND t4.total IS NOT NULL
            ) AS ScheduleHoursByWeek
           ,(
              SELECT    MAX(RecordDate)
              FROM      arStudentClockAttendance
              WHERE     StuEnrollId = SCA.StuEnrollId
                        AND (
                              ActualHours >= 1.00
                              AND ActualHours <> 999.00
                              AND ActualHours <> 9999.00
                            )
            ) AS LastDateAttended
           ,(
              SELECT    COUNT(*)
              FROM      arStudentClockAttendance
              WHERE     StuEnrollId = SCA.StuEnrollId
                        AND isTardy = 1
            ) AS TardyCount
    FROM    arStudentClockAttendance SCA
    WHERE   SCA.StuEnrollId = @stuEnrollId
            AND SCA.RecordDate <= @cutOffDate
    ORDER BY SchedHours DESC
           ,TotalPresentHours DESC; 




GO
