SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ginzo,John
-- Create date: 07/18/2014
-- Description:	Set Local envrionment
-- =============================================
CREATE PROCEDURE [dbo].[Update_Development_LocalEnvironment]
    @TenantId INT
   ,@SiteUrl VARCHAR(500)
   ,@ServicesUrl VARCHAR(500)
AS
    BEGIN
	
        DECLARE @authKey VARCHAR(100);

	-------------------------------------------------------------
	-- Get authentication key from tenantAuthDB
	-- and update config settings
	-------------------------------------------------------------
        SET @authKey = (
                         SELECT TOP 1
                                [Key]
                         FROM   TenantAuthDB.dbo.ApiAuthenticationKey
                         WHERE  TenantId = @TenantId
                       );
        IF @authKey IS NOT NULL
            BEGIN
			
                DECLARE @settingId INT;
                SET @settingId = (
                                   SELECT   SettingId
                                   FROM     dbo.syConfigAppSettings
                                   WHERE    KeyName = 'AdvantageServiceAuthenticationKey'
                                 );
						
                UPDATE  dbo.syConfigAppSetValues
                SET     Value = @authKey
                WHERE   SettingId = @settingId;
            END;
	-------------------------------------------------------------


	-------------------------------------------------------------
	-- Update config settings for services url
	-------------------------------------------------------------
        DECLARE @settingId2 INT;
        SET @settingId2 = (
                            SELECT  SettingId
                            FROM    dbo.syConfigAppSettings
                            WHERE   KeyName = 'AdvantageServiceUri'
                          );

        UPDATE  dbo.syConfigAppSetValues
        SET     Value = @ServicesUrl
        WHERE   SettingId = @settingId2;
	-------------------------------------------------------------


	-------------------------------------------------------------
	-- Update config settings for site url
	-------------------------------------------------------------
        DECLARE @settingId3 INT;
        SET @settingId3 = (
                            SELECT  SettingId
                            FROM    dbo.syConfigAppSettings
                            WHERE   KeyName = 'AdvantageSiteUri'
                          );

        UPDATE  dbo.syConfigAppSetValues
        SET     Value = @SiteUrl
        WHERE   SettingId = @settingId3;
	-------------------------------------------------------------




    END;



GO
