SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_GetScoresPosted_GetList]
    @StuEnrollId UNIQUEIDENTIFIER
AS
    SELECT DISTINCT
            t1.Id
           ,t6.TermId
           ,t6.TermDescrip
           ,t6.StartDate
           ,t6.EndDate
           ,t1.DatePassed
           ,t1.grdcomponenttypeid
           ,t7.Descrip
           ,t1.accuracy
           ,t1.speed
           ,t1.GrdSysDetailId
           ,t5.Grade
           ,t1.InstructorId
           ,t8.FullName
           ,t1.istestmentorproctored
    FROM    arPostScoreDictationSpeedTest t1
    INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
    INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
    INNER JOIN arGradeSystems t4 ON t3.GrdSystemId = t4.GrdSystemId
    INNER JOIN arGradeSystemDetails t5 ON t4.GrdSystemId = t5.GrdSystemId
                                          AND t1.GrdSysDetailId = t5.GrdSysDetailId
    INNER JOIN arTerm t6 ON t1.termId = t6.TermId
    INNER JOIN (
                 SELECT DISTINCT
                        GrdComponentTypeId
                       ,Descrip
                 FROM   arGrdComponentTypes
                 WHERE  SysComponentTypeId = 612
               ) t7 ON t1.grdcomponenttypeid = t7.GrdComponentTypeId
    INNER JOIN syUsers t8 ON t1.InstructorId = t8.UserId
    WHERE   t1.StuEnrollId = @StuEnrollId
    ORDER BY t1.DatePassed
           ,t6.startdate
           ,t6.enddate;



GO
