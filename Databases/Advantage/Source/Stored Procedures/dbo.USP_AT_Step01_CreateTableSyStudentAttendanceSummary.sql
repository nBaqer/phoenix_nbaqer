SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

--=================================================================================================
-- USP_AT_Step01_CreateTableSyStudentAttendanceSummary
--=================================================================================================
CREATE PROCEDURE [dbo].[USP_AT_Step01_CreateTableSyStudentAttendanceSummary]
AS -- 
    -- Step 1  --  Create_Table  or Clean Table syStudentAttendanceSummary
    BEGIN -- Step 1  --  Create_Table
        IF NOT EXISTS (
                      SELECT 1
                      FROM   sys.objects
                      WHERE  object_id = OBJECT_ID(N'syStudentAttendanceSummary')
                             AND type IN ( N'U', N'PC' )
                      )
            BEGIN
                CREATE TABLE syStudentAttendanceSummary
                    (
                        StuEnrollId UNIQUEIDENTIFIER NULL
                       ,ClsSectionId UNIQUEIDENTIFIER NULL
                       ,StudentAttendedDate DATETIME NULL
                       ,ScheduledDays DECIMAL(18, 2) NULL
                       ,ActualDays DECIMAL(18, 2) NULL
                       ,ActualRunningScheduledDays DECIMAL(18, 2) NULL
                       ,ActualRunningPresentDays DECIMAL(18, 2) NULL
                       ,ActualRunningAbsentDays DECIMAL(18, 2) NULL
                       ,ActualRunningMakeupDays DECIMAL(18, 2) NULL
                       ,ActualRunningTardyDays DECIMAL(18, 2) NULL
                       ,AdjustedPresentDays DECIMAL(18, 2) NULL
                       ,AdjustedAbsentDays DECIMAL(18, 2) NULL
                       ,AttendanceTrackType VARCHAR(50) NULL
                       ,ModUser VARCHAR(50) NULL
                       ,ModDate DATETIME NULL
                       ,TermId UNIQUEIDENTIFIER NULL
                       ,ActualPresentDays_ConvertTo_Hours_Calc DECIMAL(18, 2) NULL
                       ,ScheduledHours DECIMAL(18, 2) NULL
                       ,ActualAbsentDays_ConvertTo_Hours_Calc DECIMAL(18, 2) NULL
                       ,TermStartDate DATETIME NULL
                       ,ActualPresentDays_ConvertTo_Hours DECIMAL(18, 2) NULL
                       ,ActualAbsentDays_ConvertTo_Hours DECIMAL(18, 2) NULL
                       ,ActualTardyDays_ConvertTo_Hours DECIMAL(18, 2) NULL
                       ,TermDescrip VARCHAR(100) NULL
                       ,tardiesmakingabsence INT NULL
                       ,IsExcused BIT NULL
                       ,ActualRunningExcusedDays INT NULL
                       ,IsTardy INT NULL
                    );
            END;

        TRUNCATE TABLE syStudentAttendanceSummary;
    END;
--=================================================================================================
-- END  --  USP_AT_Step01_CreateTableSyStudentAttendanceSummary
--=================================================================================================
GO
