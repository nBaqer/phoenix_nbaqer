SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_GetClsSectMtgsWithPeriods]
    @clssectId UNIQUEIDENTIFIER
AS
    SELECT  a.ClsSectMeetingId
           ,(
              SELECT    PeriodDescrip
              FROM      syPeriods
              WHERE     PeriodId = a.PeriodId
            ) AS PeriodDescrip
           ,a.PeriodId
           ,(
              SELECT    PeriodDescrip
              FROM      syPeriods
              WHERE     PeriodId = a.AltPeriodId
            ) AS AltPeriodDescrip
           ,a.AltPeriodId
           ,(
              SELECT    Descrip
              FROM      arRooms
              WHERE     RoomId = a.RoomId
            ) AS RoomDescrip
           ,a.RoomId
           ,a.StartDate
           ,a.EndDate
           ,(
              SELECT    InstructionTypeDescrip
              FROM      arInstructionType
              WHERE     InstructionTypeId = a.InstructionTypeId
            ) AS InstructionTypeDescrip
           ,a.InstructionTypeID
           ,0 AS CmdType
           ,(
              SELECT    COUNT(*)
              FROM      dbo.atClsSectAttendance t
              WHERE     t.ClsSectMeetingId = a.ClsSectMeetingId
            ) AS AttUsedCnt
           ,a.WorkDaysId
           ,a.TimeIntervalId
           ,a.ClsSectionId
           ,a.EndIntervalId
           ,a.ModUser
           ,a.ModDate
           ,a.BreakDuration
    FROM    arClsSectMeetings a
    WHERE   ClsSectionId = @clssectId
    ORDER BY a.StartDate
           ,a.EndDate; 



GO
