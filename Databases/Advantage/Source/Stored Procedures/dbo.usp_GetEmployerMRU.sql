SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_GetEmployerMRU]
    @UserId UNIQUEIDENTIFIER
   ,@CampusId UNIQUEIDENTIFIER
AS
    SET NOCOUNT ON;
-- For new users pull 20 students from the user's default campus
-- For existing users pull 20 students from the syMRU table, students can be from multiple campuses
    DECLARE @CountMRU INT;
    DECLARE @StudentIdentifier VARCHAR(50);

    SET @StudentIdentifier = (
                               SELECT   Value
                               FROM     dbo.syConfigAppSetValues
                               WHERE    SettingId = 69
                                        AND CampusId IS NULL
                             );

    DECLARE @DefaultCampusId UNIQUEIDENTIFIER;
-- used to return MRU results
    DECLARE @MRUList TABLE
        (
         MRUId UNIQUEIDENTIFIER NOT NULL
        , -- PRIMARY KEY,
         Counter INT NOT NULL
        ,ChildId UNIQUEIDENTIFIER NOT NULL
        ,MRUTypeId TINYINT NOT NULL
        ,UserId UNIQUEIDENTIFIER NOT NULL
        ,CampusId UNIQUEIDENTIFIER NULL
        ,SortOrder INT NULL
        ,IsSticky BIT NULL
        ,EmployerId UNIQUEIDENTIFIER NOT NULL
        ,FullName VARCHAR(200) NOT NULL
        ,CampusDescrip VARCHAR(100) NOT NULL
        ,ModDate DATETIME
        );
-- used to create default MRU list for the user if one doesn't exist
    DECLARE @DefaultMRU TABLE
        (
         ChildId UNIQUEIDENTIFIER NOT NULL
        ,MRUTypeId TINYINT NOT NULL
        ,UserId UNIQUEIDENTIFIER NOT NULL
        ,CampusId UNIQUEIDENTIFIER NULL
        ,SortOrder INT NOT NULL
                       IDENTITY
        ,IsSticky BIT NULL
        ,ModDate DATETIME
        );
    SELECT  @CountMRU = COUNT(*)
    FROM    dbo.syMRUS
    WHERE   UserId = @UserId
            AND MRUTypeId = 2;

--PRINT @CountMRU
    IF ( @CountMRU > 0 )
        BEGIN
-- Has MRU rows 
            INSERT  INTO @MRUList
                    (
                     MRUId
                    ,Counter
                    ,ChildId
                    ,MRUTypeId
                    ,UserId
                    ,CampusId
                    ,SortOrder
                    ,IsSticky
                    ,EmployerId
                    ,FullName
                    ,CampusDescrip
                    ,ModDate
		            )
                    SELECT DISTINCT TOP 20
                            M.MRUId
                           ,M.Counter
                           ,M.ChildId
                           ,M.MRUTypeId
                           ,M.UserId
                           ,M.CampusId
                           ,M.SortOrder
                           ,M.IsSticky
                           ,S.EmployerId
                           ,S.EmployerDescrip AS FullName
                           ,C.CampDescrip
                           ,M.ModDate
                    FROM    dbo.syMRUS AS M
                    INNER JOIN dbo.syMRUTypes AS T ON M.MRUTypeId = T.MRUTypeId
                    INNER JOIN dbo.plEmployers AS S ON M.ChildId = S.EmployerId
                    INNER  JOIN dbo.syCampuses AS C ON M.CampusId = C.CampusId
                    WHERE   M.MRUTypeId = 2
                            AND UserId = @UserId
                            AND
-- MRU List will show leads from the campuses user has access to
                            C.CampusId IN ( SELECT DISTINCT
                                                    C.CampusId
                                            FROM    syUsers U
                                            INNER JOIN syUsersRolesCampGrps URC ON U.UserId = URC.UserId
                                            INNER JOIN syCmpGrpCmps CGC ON URC.CampGrpId = CGC.CampGrpId
                                            INNER JOIN syCampuses C ON C.CampusId = CGC.CampusId
                                            WHERE   U.UserId = @UserId
                                                    AND C.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' --	Order by C.CampDescrip
		)
                    ORDER BY M.ModDate DESC;

            DECLARE @MruListCount INT; 
            SET @MruListCount = 0;
            SET @MruListCount = (
                                  SELECT    COUNT(*) AS MruCountForFirstTimeUser
                                  FROM      @MRUList
                                );
			
            SET @DefaultCampusId = (
                                     SELECT DISTINCT
                                            U.CampusId
                                     FROM   syUsers U
                                     WHERE  U.UserId = @UserId
                                   );

            IF @DefaultCampusId IS NULL
                BEGIN
                    SET @DefaultCampusId = (
                                             SELECT DISTINCT TOP 1
                                                    SC.CampusId
                                             FROM   dbo.syCmpGrpCmps SC
                                                   ,dbo.syUsersRolesCampGrps URC
                                                   ,dbo.syCampuses S
                                             WHERE  URC.UserId = @UserId
                                                    AND SC.CampGrpId = URC.CampGrpId
                                                    AND S.CampusId = SC.CampusId
                                                    AND S.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
					   -- Order by S.CampDescrip
                                           );

                END;
			
			-- The MRU list is currently empty
			-- Get the most recently added 20 leads from default campus
            IF @MruListCount = 0
                BEGIN
                    INSERT  INTO @MRUList
                            (
                             MRUId
                            ,Counter
                            ,ChildId
                            ,MRUTypeId
                            ,UserId
                            ,CampusId
                            ,SortOrder
                            ,IsSticky
                            ,EmployerId
                            ,FullName
                            ,CampusDescrip
                            ,ModDate
					        )
                            SELECT DISTINCT TOP 20
                                    NEWID() AS MRUId
                                   ,101 AS Counter
                                   ,  -- not being used so hardcoded a value
                                    S.EmployerId AS ChildId
                                   ,2 AS MRUTypeId
                                   ,@UserId AS UserId
                                   ,C.CampusId
                                   ,0 AS SortOrder
                                   ,0 AS IsSticky
                                   ,S.EmployerId AS EmployerId
                                   ,S.EmployerDescrip AS FullName
                                   ,C.CampDescrip
                                   ,S.ModDate
                            FROM    dbo.plEmployers AS S
                            INNER JOIN dbo.syCmpGrpCmps CG ON S.CampGrpId = CG.CampGrpId
                            INNER JOIN dbo.syCampuses AS C ON CG.CampusId = C.CampusId
                            WHERE   -- MRU List will show leads from the campuses user has access to
                                    C.CampusId = @DefaultCampusId
                            ORDER BY S.ModDate DESC;
                END;
				
            SET @MruListCount = (
                                  SELECT    COUNT(*) AS MruCountForFirstTimeUser
                                  FROM      @MRUList
                                );
			
				-- The MRU list from default campus is currently empty
				-- Generate the list again from other campus
            IF @MruListCount = 0 
                BEGIN
                    INSERT  INTO @MRUList
                            (
                             MRUId
                            ,Counter
                            ,ChildId
                            ,MRUTypeId
                            ,UserId
                            ,CampusId
                            ,SortOrder
                            ,IsSticky
                            ,EmployerId
                            ,FullName
                            ,CampusDescrip
                            ,ModDate
							)
                            SELECT DISTINCT TOP 20
                                    NEWID() AS MRUId
                                   ,101 AS Counter
                                   ,  -- not being used so hardcoded a value
                                    S.EmployerId AS ChildId
                                   ,2 AS MRUTypeId
                                   ,@UserId AS UserId
                                   ,C.CampusId
                                   ,0 AS SortOrder
                                   ,0 AS IsSticky
                                   ,S.EmployerId AS EmployerId
                                   ,S.EmployerDescrip AS FullName
                                   ,C.CampDescrip
                                   ,S.ModDate
                            FROM    dbo.plEmployers AS S
                            INNER JOIN dbo.syCmpGrpCmps CG ON S.CampGrpId = CG.CampGrpId
                            INNER JOIN dbo.syCampuses AS C ON CG.CampusId = C.CampusId
                            WHERE   -- MRU List will show leads from the campuses user has access to
                                    C.CampusId IN ( SELECT DISTINCT
                                                            C.CampusId
                                                    FROM    syUsers U
                                                    INNER JOIN syUsersRolesCampGrps URC ON U.UserId = URC.UserId
                                                    INNER JOIN syCmpGrpCmps CGC ON URC.CampGrpId = CGC.CampGrpId
                                                    INNER JOIN syCampuses C ON C.CampusId = CGC.CampusId
                                                    WHERE   U.UserId = @UserId
                                                            AND C.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' --Order by C.CampDescrip
									)
                            ORDER BY S.ModDate DESC;
                END;
					
					-- Always keep the count to 20.
					-- If existing users had more than 20 records delete it
            DELETE  FROM syMRUS
            WHERE   UserId = @UserId
                    AND MRUTypeId = 2
                    AND MRUId NOT IN ( SELECT DISTINCT
                                                MRUId
                                       FROM     @MRUList );
			/**** User is not a first time user - Ends Here ********/

        END;
    ELSE
        BEGIN

            SET @DefaultCampusId = (
                                     SELECT DISTINCT
                                            U.CampusId
                                     FROM   syUsers U
                                     WHERE  U.UserId = @UserId
                                   );
            IF @DefaultCampusId IS NULL
                BEGIN
                    SET @DefaultCampusId = (
                                             SELECT DISTINCT TOP 1
                                                    SC.CampusId
                                             FROM   dbo.syCmpGrpCmps SC
                                                   ,dbo.syUsersRolesCampGrps URC
                                                   ,dbo.syCampuses S
                                             WHERE  URC.UserId = @UserId
                                                    AND SC.CampGrpId = URC.CampGrpId
                                                    AND S.CampusId = SC.CampusId
                                                    AND S.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
					  --  Order by S.CampDescrip
                                           );

                END;

-- no rows so we must fill the MRUs with a default MRU list
            INSERT  INTO @DefaultMRU
                    (
                     ChildId
                    ,MRUTypeId
                    ,UserId
                    ,CampusId
                    ,IsSticky
                    ,ModDate 
		            )
                    SELECT DISTINCT TOP 20
                            S.EmployerId AS ChildId
                           ,2 AS MRUTypeId
                           ,@UserId AS UserId
                           ,C.CampusId AS CampusId
                           ,0 AS IsSticky
                           ,S.ModDate
                    FROM    dbo.plEmployers AS S
                    INNER JOIN dbo.syCmpGrpCmps CG ON S.CampGrpId = CG.CampGrpId
                    INNER JOIN dbo.syCampuses AS C ON CG.CampusId = C.CampusId
                    WHERE   -- MRU List will show leads from the campuses user has access to
                            C.CampusId = @DefaultCampusId
                    ORDER BY S.ModDate DESC;

            DECLARE @MruCountForFirstTimeUser INT; 
            SET @MruCountForFirstTimeUser = 0;
            SET @MruCountForFirstTimeUser = (
                                              SELECT    COUNT(*) AS MruCountForFirstTimeUser
                                              FROM      @DefaultMRU
                                            );
			
			-- There are no leads from default campus, so get leads from other campus
			-- user has access to
            IF @MruCountForFirstTimeUser = 0
                BEGIN
                    INSERT  INTO @DefaultMRU
                            (
                             ChildId
                            ,MRUTypeId
                            ,UserId
                            ,CampusId
                            ,IsSticky
                            ,ModDate 
						    )
                            SELECT DISTINCT TOP 20
                                    S.EmployerId AS ChildId
                                   ,2 AS MRUTypeId
                                   ,@UserId AS UserId
                                   ,C.CampusId
                                   ,0 AS IsSticky
                                   ,S.ModDate
                            FROM    dbo.plEmployers AS S
                            INNER JOIN dbo.syCmpGrpCmps CG ON S.CampGrpId = CG.CampGrpId
                            INNER JOIN dbo.syCampuses AS C ON CG.CampusId = C.CampusId
                            WHERE   -- MRU List will show leads from the campuses user has access to
                                    C.CampusId IN ( SELECT DISTINCT
                                                            C.CampusId
                                                    FROM    syUsers U
                                                    INNER JOIN syUsersRolesCampGrps URC ON U.UserId = URC.UserId
                                                    INNER JOIN syCmpGrpCmps CGC ON URC.CampGrpId = CGC.CampGrpId
                                                    INNER JOIN syCampuses C ON C.CampusId = CGC.CampusId
                                                    WHERE   U.UserId = @UserId
                                                            AND C.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' --Order by C.CampDescrip
								)
                            ORDER BY S.ModDate DESC;
                END; 



--SELECT * FROM dbo.plEmployers
-- Fill the MRU results into the @MRUList table variable
            SET @MruCountForFirstTimeUser = (
                                              SELECT    COUNT(*) AS MruCountForFirstTimeUser
                                              FROM      @DefaultMRU
                                            );
			
			-- There are no leads from default campus, so get leads from other campus
			-- user has access to
            IF @MruCountForFirstTimeUser = 0
                BEGIN
                    INSERT  INTO @MRUList
                            (
                             MRUId
                            ,Counter
                            ,ChildId
                            ,MRUTypeId
                            ,UserId
                            ,CampusId
                            ,SortOrder
                            ,IsSticky
                            ,EmployerId
                            ,FullName
                            ,CampusDescrip
                            ,ModDate
								
                            )
                            SELECT DISTINCT TOP 20
                                    M.MRUId
                                   ,M.Counter
                                   ,M.ChildId
                                   ,M.MRUTypeId
                                   ,M.UserId
                                   ,M.CampusId
                                   ,M.SortOrder
                                   ,M.IsSticky
                                   ,S.EmployerId
                                   ,S.EmployerDescrip AS FullName
                                   ,C.CampDescrip
                                   ,M.ModDate
                            FROM    dbo.syMRUS AS M
                            INNER JOIN dbo.syMRUTypes AS T ON M.MRUTypeId = T.MRUTypeId
                            INNER JOIN dbo.plEmployers AS S ON M.ChildId = S.EmployerId
                            INNER JOIN dbo.syCmpGrpCmps CG ON S.CampGrpId = CG.CampGrpId 
						--INNER JOIN dbo.arStuEnrollments AS E ON S.StudentId = E.StudentId
                            INNER JOIN dbo.syCampuses AS C ON CG.CampusId = C.CampusId
						--INNER JOIN dbo.adLeads AS L ON E.EmployerId = S.EmployerId
                            WHERE   M.MRUTypeId = 2
                                    AND UserId = @UserId
                                    AND C.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
                            ORDER BY M.ModDate DESC;
                END;
-- Fill the MRU results into the @MRUList table variable
            INSERT  INTO @MRUList
                    (
                     MRUId
                    ,Counter
                    ,ChildId
                    ,MRUTypeId
                    ,UserId
                    ,CampusId
                    ,SortOrder
                    ,IsSticky
                    ,EmployerId
                    ,FullName
                    ,CampusDescrip
                    ,ModDate
					)
                    SELECT DISTINCT
                            NEWID() AS MRUId
                           ,101 AS Counter
                           ,ChildId
                           ,MRUTypeId
                           ,UserId
                           ,CampusId
                           ,SortOrder
                           ,IsSticky
                           ,ChildId AS EmployerId
                           ,(
                              SELECT    EmployerDescrip
                              FROM      plEmployers
                              WHERE     EmployerId = DM.ChildId
                            ) AS FullName
                           ,(
                              SELECT DISTINCT
                                        CampDescrip
                              FROM      syCampuses
                              WHERE     CampusId = DM.CampusId
                            ) AS CampusDescrip
                           ,ModDate
                    FROM    @DefaultMRU DM
                    ORDER BY ModDate DESC;
					
					-- insert records into the MRU table
            INSERT  INTO dbo.syMRUS
                    (
                     ChildId
                    ,MRUTypeId
                    ,UserId
                    ,CampusId
                    ,SortOrder
                    ,IsSticky
                    ,ModDate 
		            )
                    SELECT  ChildId
                           ,MRUTypeId
                           ,UserId
                           ,CampusId
                           ,SortOrder
                           ,IsSticky
                           ,ModDate
                    FROM    @DefaultMRU
                    ORDER BY ModDate DESC;
			   -- Select * from @MRUList
		   /**** User is a first time user - Ends Here ********/ 
        END;


-- return the @MRUList table variable
    SELECT  MRUId
           ,Counter
           ,ChildId
           ,MRUTypeId
           ,UserId
           ,CampusId
           ,SortOrder
           ,IsSticky
           ,EmployerId
           ,FullName
           ,CampusDescrip
    FROM    @MRUList
    ORDER BY ModDate DESC;
GO
