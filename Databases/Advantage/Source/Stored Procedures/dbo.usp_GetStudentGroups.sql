SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_GetStudentGroups]
    (
     @stuEnrollId UNIQUEIDENTIFIER
    )
AS
    SET NOCOUNT ON;
    SELECT  LG.Descrip
    FROM    adLeadByLeadGroups LBG
           ,adLeadGroups LG
    WHERE   LBG.LeadGrpId = LG.LeadGrpId
            AND StuEnrollId = @stuEnrollId
    ORDER BY LG.Descrip;  



GO
