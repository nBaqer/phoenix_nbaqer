SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[usp_EdExpress_BuildExceptionReport_DisbursementTable]
    @DetailId NVARCHAR(50)
   ,@ExceptionReportId NVARCHAR(50)
   ,@DbIn NVARCHAR(10)
   ,@Filter NVARCHAR(10)
   ,@AccDisbAmount DECIMAL(19,4)
   ,@DisbDate NVARCHAR(20)
   ,@DisbNum INT
   ,@DisbRelIndi NVARCHAR(5)
   ,@DusbSeqNum INT
   ,@SimittedDisbAmount DECIMAL(19,4)
   ,@ActionStatusDisb NVARCHAR(5)
   ,@OriginalSSN NVARCHAR(20)
   ,@ModDate NVARCHAR(20)
   ,@ErrorMsg NVARCHAR(500)
   ,@FileName NVARCHAR(200)
   ,@ExceptionGUID NVARCHAR(50)
   ,@MsgType NVARCHAR(50)
   ,@LoanId NVARCHAR(50) = ''
   ,@DisbLoanFeeAmount NVARCHAR(50) = '0.0'
AS
    IF @MsgType = 'PELL'
        BEGIN
            INSERT  INTO syEDExpressExceptionReportPell_ACG_SMART_Teach_DisbursementTable
                    (
                     DetailId
                    ,ExceptionReportId
                    ,DbIn
                    ,Filter
                    ,AccDisbAmount
                    ,DisbDate
                    ,DisbNum
                    ,DisbRelIndi
                    ,DusbSeqNum
                    ,SimittedDisbAmount
                    ,ActionStatusDisb
                    ,OriginalSSN
                    ,ModDate
                    ,ErrorMsg
                    ,FileName
                    ,ExceptionGUID
                    )
            VALUES  (
                     @DetailId
                    ,@ExceptionReportId
                    ,@DbIn
                    ,@Filter
                    ,@AccDisbAmount
                    ,@DisbDate
                    ,@DisbNum
                    ,@DisbRelIndi
                    ,@DusbSeqNum
                    ,@SimittedDisbAmount
                    ,@ActionStatusDisb
                    ,@OriginalSSN
                    ,@ModDate
                    ,@ErrorMsg
                    ,@FileName
                    ,@ExceptionGUID
                    );
        END;
    ELSE
        BEGIN
            INSERT  INTO syEDExpressExceptionReportDirectLoan_DisbursementTable
                    (
                     DetailId
                    ,ExceptionReportId
                    ,DbIn
                    ,Filter
                    ,DisbDate
                    ,DisbGrossAmount
                    ,DisbIntRebAmount
                    ,DisbLoanFeeAmount
                    ,DisbNetAdjAmount
                    ,DisbNetAmount
                    ,DisbNum
                    ,DisbSeqNum
                    ,DisbStatus_RelInd
                    ,DisbType
                    ,LoanId
                    ,OriginalSSN
                    ,ModDate
                    ,ErrorMsg
                    ,FileName
                    ,ExceptionGUID
                    )
            VALUES  (
                     @DetailId
                    ,@ExceptionReportId
                    ,@DbIn
                    ,@Filter
                    ,@DisbDate
                    ,@SimittedDisbAmount
                    ,'0.0'
                    ,@DisbLoanFeeAmount
                    ,'0.00'
                    ,@AccDisbAmount
                    ,@DisbNum
                    ,@DusbSeqNum
                    ,@DisbRelIndi
                    ,''
                    ,@LoanId
                    ,@OriginalSSN
                    ,@ModDate
                    ,@ErrorMsg
                    ,@FileName
                    ,@ExceptionGUID
                    );
        END;





GO
