SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[SSP_TempStatusChangeHistoryTable]
AS
    BEGIN
        SET NOCOUNT ON; 
        SELECT  StudentEnrollmentId
               ,EffectiveDate
               ,StatusDescription
               ,Reason
               ,RequestedBy
               ,ChangedBy
               ,CaseNo
               ,DateOfChange
        FROM    TempStatusChangeHistoryTbl;	
    END;

GO
