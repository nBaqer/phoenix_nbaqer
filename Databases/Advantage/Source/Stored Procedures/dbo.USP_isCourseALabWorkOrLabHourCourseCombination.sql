SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_isCourseALabWorkOrLabHourCourseCombination]
    (
     @reqId UNIQUEIDENTIFIER
    )
AS
    SET NOCOUNT ON;
    SELECT 
		DISTINCT
            COUNT(GC.Descrip)
    FROM    arGrdBkWeights GBW
    INNER JOIN arGrdBkWgtDetails GD ON GBW.InstrGrdBkWgtId = GD.InstrGrdBkWgtId
                                       AND GBW.ReqId = @reqId
    INNER JOIN arGrdComponentTypes GC ON GC.GrdComponentTypeId = GD.GrdComponentTypeId
                                         AND GC.SysComponentTypeID IS NOT NULL
                                         AND GC.SysComponentTypeID IN ( 499,501,502,533,544 );



GO
