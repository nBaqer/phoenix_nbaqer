SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_TermStudentIsCurrentlyRegisteredIn_GetList]
    @StudentId UNIQUEIDENTIFIER
   ,@CampusId UNIQUEIDENTIFIER
AS
    SELECT DISTINCT
            T.TermId
           ,T.TermDescrip
           ,CONVERT(CHAR(10),T.StartDate,101) + ' - ' + CONVERT(CHAR(10),T.EndDate,101) AS TermPeriod
           ,T.ProgId
    FROM    arPrgVersions PV
           ,arPrograms P
           ,arTerm T
           ,arStuEnrollments SE
           ,arResults R
           ,arClassSections CS
    WHERE   PV.ProgId = P.ProgId
            AND (
                  T.ProgId = P.ProgId
                  OR T.ProgId IS NULL
                )
            AND PV.PrgVerId = SE.PrgVerId
            AND SE.StuEnrollId = R.StuEnrollId
            AND R.TestId = CS.ClsSectionId
            AND CS.TermId = T.TermId
            AND SE.StudentId = @StudentId
            AND SE.CampusId = @CampusId
    ORDER BY T.TermDescrip
           ,TermPeriod;



GO
