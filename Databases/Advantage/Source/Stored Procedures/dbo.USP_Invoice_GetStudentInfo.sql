SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Edwin Sosa
-- Create date: 11/19/2018
-- Description:	Procedure for getting invoice info per student.
-- =============================================
CREATE PROCEDURE [dbo].[USP_Invoice_GetStudentInfo]
    -- Add the parameters for the stored procedure here
    @campusId UNIQUEIDENTIFIER
   ,@stuEnrollIdList VARCHAR(MAX)
   ,@refDate DATETIME
AS


    BEGIN
        -- SET NOCOUNT ON added to prevent extra result sets from
        -- interfering with SELECT statements.
        SET NOCOUNT ON;

        SELECT   payPlansAmounts.Address
                ,payPlansAmounts.City
                ,payPlansAmounts.Zip
                ,payPlansAmounts.StateDescrip
                ,payPlansAmounts.CountryDescrip
                ,SUM(PlanAmount) - SUM(payPlansAmounts.AmountPaid) AS OutstandingBalance
                ,SUM(payPlansAmounts.PlanAmount) AS OriginalTotal
                ,payPlansAmounts.StuEnrollId
                ,payPlansAmounts.SSN
                ,payPlansAmounts.StudentNumber
                ,payPlansAmounts.LastName
                ,payPlansAmounts.FirstName
                ,payPlansAmounts.MiddleName
                ,payPlansAmounts.Description
                ,payPlansAmounts.PaymentPlanId
				,payPlansAmounts.CampusName
				,payPlansAmounts.StartDate
				,payPlansAmounts.StatusCodeDescrip
				,payPlansAmounts.ProgDescrip
        FROM     (
                 SELECT     ASA2.Address
                           ,ASA2.City
                           ,ASA2.Zip
                           ,ASA2.StateDescrip
                           ,ASA2.CountryDescrip
                           ,ISNULL(FSPPS.Amount, 0) AS PlanAmount
                           ,ISNULL(SPDR2.AmountPaid, 0) AS AmountPaid
                           ,ASE.StuEnrollId
                           ,AST.SSN
                           ,AST.StudentNumber
                           ,AST.LastName
                           ,AST.FirstName
                           ,AST.MiddleName
                           ,FSPP.PayPlanDescrip AS Description
                           ,FSPP.PaymentPlanId
						   ,camp.CampDescrip AS CampusName
						   ,ASE.StartDate
						   ,SSC.StatusCodeDescrip
						   ,PRO.ProgDescrip
                 FROM       faStudentPaymentPlans AS FSPP
                 INNER JOIN faStuPaymentPlanSchedule AS FSPPS ON FSPPS.PaymentPlanId = FSPP.PaymentPlanId
                 INNER JOIN arStuEnrollments AS ASE ON ASE.StuEnrollId = FSPP.StuEnrollId
				 JOIN dbo.syCampuses camp ON camp.CampusId = ASE.CampusId
                 INNER JOIN arStudent AS AST ON AST.StudentId = ASE.StudentId
                 INNER JOIN syStatusCodes AS SSC ON SSC.StatusCodeId = ASE.StatusCodeId
                 INNER JOIN arPrgVersions AS APV ON APV.PrgVerId = ASE.PrgVerId
				 INNER JOIN dbo.arPrograms AS PRO ON PRO.ProgId = APV.ProgId
                 LEFT JOIN  (
                            SELECT CASE WHEN amountsPaid.AmountPaid < 0 THEN amountsPaid.AmountPaid * -1
                                        ELSE amountsPaid.AmountPaid
                                   END AS AmountPaid
                                  ,amountsPaid.PayPlanScheduleId
                            FROM   (
                                   SELECT     SUM(ISNULL(SPDR.Amount, 0)) AS AmountPaid
                                             ,SPDR.PayPlanScheduleId
                                   FROM       saPmtDisbRel AS SPDR
                                   INNER JOIN saTransactions AS ST ON ST.TransactionId = SPDR.TransactionId
                                   INNER JOIN faStuPaymentPlanSchedule AS FSPPS2 ON FSPPS2.PayPlanScheduleId = SPDR.PayPlanScheduleId
                                   WHERE      ST.Voided = 0
                                   GROUP BY   SPDR.PayPlanScheduleId
                                   ) AS amountsPaid
                            ) AS SPDR2 ON SPDR2.PayPlanScheduleId = FSPPS.PayPlanScheduleId
                 LEFT JOIN  (
                            SELECT T.StudentId
                                  ,T.StdAddressId
                                  ,T.Address
                                  ,T.City
                                  ,T.Zip
                                  ,T.StateDescrip
                                  ,T.CountryDescrip
                            FROM   (
                                   SELECT     ASA.StudentId
                                             ,ASA.StdAddressId
                                             ,( Address1 + ' ' + ISNULL(Address2, '')) AS Address
                                             ,ASA.City
                                             ,ASA.Zip
                                             ,ISNULL(adc.CountryDescrip, '') AS CountryDescrip
                                             ,ISNULL(SS2.StateDescrip, '') AS StateDescrip
                                             ,ROW_NUMBER() OVER ( PARTITION BY ASA.StudentId
                                                                  ORDER BY ASA.default1 DESC
                                                                ) AS RowNumber
                                   FROM       arStudAddresses AS ASA
                                   INNER JOIN syStatuses AS SS ON SS.StatusId = ASA.StatusId
                                   LEFT JOIN  syStates AS SS2 ON SS2.StateId = ASA.StateId
                                   LEFT JOIN  dbo.adCountries adc ON adc.CountryId = SS2.CountryId
                                   WHERE      SS.StatusCode = 'A'
                                   ) AS T
                            WHERE  T.RowNumber = 1
                            ) AS ASA2 ON ASA2.StudentId = AST.StudentId
                 WHERE      FSPPS.ExpectedDate <= @refDate -- RefDate  
                            AND ASE.CampusId = @campusId -- CampusId 
                            AND @stuEnrollIdList IS NULL
                            OR ( ASE.StuEnrollId IN (
                                                    SELECT Val
                                                    FROM   MultipleValuesForReportParameters(@stuEnrollIdList, ',', 1)
                                                    )
                               )
                 ) payPlansAmounts
        GROUP BY payPlansAmounts.Address
                ,payPlansAmounts.City
                ,payPlansAmounts.Zip
                ,payPlansAmounts.StateDescrip
                ,payPlansAmounts.CountryDescrip
                ,payPlansAmounts.StuEnrollId
                ,payPlansAmounts.SSN
                ,payPlansAmounts.StudentNumber
                ,payPlansAmounts.LastName
                ,payPlansAmounts.FirstName
                ,payPlansAmounts.MiddleName
                ,payPlansAmounts.Description
                ,payPlansAmounts.PaymentPlanId
				,payPlansAmounts.CampusName
				,payPlansAmounts.StartDate
				,payPlansAmounts.StatusCodeDescrip
				,payPlansAmounts.ProgDescrip
        ORDER BY payPlansAmounts.StuEnrollId;

    END;
GO
