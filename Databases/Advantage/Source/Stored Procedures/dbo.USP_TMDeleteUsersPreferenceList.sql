SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[USP_TMDeleteUsersPreferenceList]
    (
     @UserID AS VARCHAR(8000)
    )
AS
    BEGIN
        DECLARE @OtherUserId UNIQUEIDENTIFIER;
        DECLARE @ExistingDefaultView VARCHAR(50);
        SET @ExistingDefaultView = NULL;
    
       
        IF EXISTS ( SELECT  *
                    FROM    tmUsersTaskDefaultView
                    WHERE   UserID = @UserID )
            BEGIN
                DELETE  FROM tmUsersTaskDefaultView
                WHERE   UserID = @UserID;
            END;
    END;




GO
