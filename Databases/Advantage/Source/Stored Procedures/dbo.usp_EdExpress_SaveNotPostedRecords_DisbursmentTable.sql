SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_EdExpress_SaveNotPostedRecords_DisbursmentTable]
    @ParentId VARCHAR(50)
   ,@AwardScheduleId VARCHAR(50)
   ,@DbIn VARCHAR(50)
   ,@Filter VARCHAR(50)
   ,@AccDisbAmount DECIMAL(19,4)
   ,@DisbDate VARCHAR(50)
   ,@DisbNum INT
   ,@DisbRelIndi VARCHAR(50)
   ,@DusbSeqNum INT
   ,@SimittedDisbAmount DECIMAL(19,4)
   ,@ActionStatusDisb VARCHAR(50)
   ,@OriginalSSN VARCHAR(50)
   ,@ModDate VARCHAR(50)
   ,@FileName VARCHAR(250)
   ,@MsgType VARCHAR(50)
AS
    IF @MsgType = 'PELL'
        BEGIN
				--Before we insert the record we want to remove any other pending payment record for this award schedule id. (DE5696)
            DELETE  FROM syEDExpNotPostPell_ACG_SMART_Teach_DisbursementTable
            WHERE   AwardScheduleId = @AwardScheduleId;
			
				--Insert the new record
            INSERT  INTO syEDExpNotPostPell_ACG_SMART_Teach_DisbursementTable
                    (
                     DetailId
                    ,ParentId
                    ,AwardScheduleId
                    ,DbIn
                    ,Filter
                    ,AccDisbAmount
                    ,DisbDate
                    ,DisbNum
                    ,DisbRelIndi
                    ,DusbSeqNum
                    ,SimittedDisbAmount
                    ,ActionStatusDisb
                    ,OriginalSSN
                    ,ModDate
                    ,FileName
					)
            VALUES  (
                     NEWID()
                    ,@ParentId
                    ,@AwardScheduleId
                    ,@DbIn
                    ,@Filter
                    ,@AccDisbAmount
                    ,@DisbDate
                    ,@DisbNum
                    ,@DisbRelIndi
                    ,@DusbSeqNum
                    ,@SimittedDisbAmount
                    ,@ActionStatusDisb
                    ,@OriginalSSN
                    ,@ModDate
                    ,@FileName
					);
        END;



GO
