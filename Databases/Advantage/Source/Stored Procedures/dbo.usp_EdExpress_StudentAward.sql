SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_EdExpress_StudentAward]
    @StuEnrollID VARCHAR(50)
   ,@AcademicYearId VARCHAR(50)
   ,@fa_id VARCHAR(25)
   ,@GrossAmount DECIMAL(19,4)
   ,@AwardStartDate DATETIME
   ,@AwardEndDate DATETIME
   ,@LoanFees VARCHAR(50)
   ,@moduser VARCHAR(50)
   ,@moddate DATETIME
   ,@Disbursements VARCHAR(50)
   ,@LoanId VARCHAR(50)
   ,@GrantType VARCHAR(25)
   ,@IsOverrideAdvDateWithEDExpDate BIT
   ,@IsOverrideAdvAmtWithEDExpAmt BIT
   ,
	
	--New Parameter--
    @IsPostPayment BIT
   ,@IsPayOutOfSchool BIT
   ,@ParentId VARCHAR(50)
   ,@DbIn VARCHAR(10)
   ,@Filter VARCHAR(10)
   ,@FirstName VARCHAR(50)
   ,@LastName VARCHAR(50)
   ,@SSN VARCHAR(50)
   ,@CampusName VARCHAR(500)
   ,@CampusId VARCHAR(50)
   ,@AddDate VARCHAR(20)
   ,@AddTime VARCHAR(20)
   ,@OriginalSSN VARCHAR(20)
   ,@UpdateDate VARCHAR(20)
   ,@UpdateTime VARCHAR(20)
   ,@OriginationStatus VARCHAR(50)
   ,@FileName VARCHAR(250)
   ,@IsInSchool BIT
	--New Parameter--
AS
    BEGIN
        DECLARE @StudentAwardID VARCHAR(50);
        DECLARE @AwardTypeID VARCHAR(50);
        DECLARE @GrantTypeDesc VARCHAR(15);
	
        IF @GrantType = ''
            OR @GrantType = 'P'
            BEGIN
                SET @GrantTypeDesc = 'PELL';
            END;
        ELSE
            IF @GrantType = 'TT'
                BEGIN
                    SET @GrantTypeDesc = 'TEACH';
                END;
            ELSE
                IF @GrantType = 'A'
                    BEGIN
                        SET @GrantTypeDesc = 'ACG';
                    END;
                ELSE
                    IF @GrantType = 'S'
                        OR @GrantType = 'T'
                        BEGIN
                            SET @GrantTypeDesc = 'SMART'; 
                        END;
	
        SELECT DISTINCT TOP 1
                @StudentAwardID = StudentAwardId
               ,@AwardTypeID = AwardTypeId
        FROM    faStudentAwards
        WHERE   StuEnrollId = @StuEnrollID
                AND AwardTypeId IN ( SELECT DISTINCT
                                            FundSourceId
                                     FROM   saFundSources
                                           ,syAdvFundSources
                                     WHERE  syAdvFundSources.AdvFundSourceId = saFundSources.AdvFundSourceId
                                            AND syAdvFundSources.Descrip = @GrantTypeDesc )
                AND FA_ID = @fa_id;
	--and AwardStartDate=@AwardStartDate and AwardEndDate=@AwardEndDate
	
        IF @StudentAwardID <> ''
            BEGIN
                IF @IsOverrideAdvDateWithEDExpDate = 1
                    AND @IsOverrideAdvAmtWithEDExpAmt = 1
                    BEGIN
                        UPDATE  faStudentAwards
                        SET     fa_id = @fa_id
                               ,moduser = @moduser
                               ,moddate = @moddate
                               ,Disbursements = @Disbursements
                               ,LoanId = @LoanId
                               ,AcademicYearId = @AcademicYearId
                               ,AwardStartDate = @AwardStartDate
                               ,AwardEndDate = @AwardEndDate
                               ,GrossAmount = @GrossAmount
                        WHERE   StudentAwardId = @StudentAwardID;
                    END;
                ELSE
                    IF @IsOverrideAdvDateWithEDExpDate = 1
                        AND @IsOverrideAdvAmtWithEDExpAmt = 0
                        BEGIN
                            UPDATE  faStudentAwards
                            SET     fa_id = @fa_id
                                   ,moduser = @moduser
                                   ,moddate = @moddate
                                   ,Disbursements = @Disbursements
                                   ,LoanId = @LoanId
                                   ,AcademicYearId = @AcademicYearId
                                   ,AwardStartDate = @AwardStartDate
                                   ,AwardEndDate = @AwardEndDate
                            WHERE   StudentAwardId = @StudentAwardID;
                        END;
                    ELSE
                        IF @IsOverrideAdvDateWithEDExpDate = 0
                            AND @IsOverrideAdvAmtWithEDExpAmt = 1
                            BEGIN
                                UPDATE  faStudentAwards
                                SET     fa_id = @fa_id
                                       ,moduser = @moduser
                                       ,moddate = @moddate
                                       ,Disbursements = @Disbursements
                                       ,LoanId = @LoanId
                                       ,GrossAmount = @GrossAmount
                                WHERE   StudentAwardId = @StudentAwardID;
                            END;
                        ELSE
                            BEGIN
                                UPDATE  faStudentAwards
                                SET     fa_id = @fa_id
                                       ,moduser = @moduser
                                       ,moddate = @moddate
                                       ,Disbursements = @Disbursements
                                       ,LoanId = @LoanId
                                WHERE   StudentAwardId = @StudentAwardID;
                            END;
            END;
        ELSE
            BEGIN
                SET @StudentAwardID = NEWID();
                SELECT TOP 1
                        @AwardTypeID = FundSourceId
                FROM    saFundSources
                WHERE   ( advFundSourceId = (
                                              SELECT TOP 1
                                                        AdvFundSourceId
                                              FROM      syAdvFundSources
                                              WHERE     ( Descrip = @GrantTypeDesc )
                                            ) )
                ORDER BY AwardYear ASC; 
                IF @AwardTypeID <> ''
                    BEGIN 
                        INSERT  INTO faStudentAwards
                                (
                                 StudentAwardID
                                ,StuEnrollID
                                ,AwardTypeID
                                ,AcademicYearId
                                ,fa_id
                                ,GrossAmount
                                ,AwardStartDate
                                ,AwardEndDate
                                ,LoanFees
                                ,moduser
                                ,moddate
                                ,Disbursements
                                ,LoanId
								)
                        VALUES  (
                                 @StudentAwardID
                                ,@StuEnrollID
                                ,@AwardTypeID
                                ,@AcademicYearId
                                ,@fa_id
                                ,@GrossAmount
                                ,@AwardStartDate
                                ,@AwardEndDate
                                ,@LoanFees
                                ,@moduser
                                ,@moddate
                                ,@Disbursements
                                ,@LoanId
								);
                    END;
                ELSE
                    BEGIN
                        SET @StudentAwardID = 'Unable award type was not found';
                    END;
            END;
        IF @IsPostPayment = 0
            BEGIN
                EXEC usp_EdExpress_SaveNotPostedRecords_StudentTable @ParentId,@StudentAwardID,@AwardTypeID,@DbIn,@Filter,@FirstName,@LastName,@SSN,@CampusName,
                    @CampusId,@StuEnrollID,@GrossAmount,@fa_id,@GrantType,@AddDate,@AddTime,@OriginalSSN,@UpdateDate,@UpdateTime,@OriginationStatus,
                    @AcademicYearId,@AwardStartDate,@AwardEndDate,@ModDate,@FileName,@IsInSchool,'PELL';
            END;
        ELSE
            BEGIN
                IF @IsPayOutOfSchool = 0
                    AND @IsInSchool = 0
                    BEGIN
                        EXEC usp_EdExpress_SaveNotPostedRecords_StudentTable @ParentId,@StudentAwardID,@AwardTypeID,@DbIn,@Filter,@FirstName,@LastName,@SSN,
                            @CampusName,@CampusId,@StuEnrollID,@GrossAmount,@fa_id,@GrantType,@AddDate,@AddTime,@OriginalSSN,@UpdateDate,@UpdateTime,
                            @OriginationStatus,@AcademicYearId,@AwardStartDate,@AwardEndDate,@ModDate,@FileName,@IsInSchool,'PELL';
                    END;
            END;
        SELECT  @StudentAwardID;
    END;




GO
