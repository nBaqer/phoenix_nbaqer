SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_Courses_Insert]
    @CourseValues NTEXT
AS
    DECLARE @hDoc INT;
    --Prepare input values as an XML document
    EXEC sp_xml_preparedocument @hDoc OUTPUT
                               ,@CourseValues;

    -- Delete all records from bridge table based on GrdComponentTypeId
    DELETE FROM arBridge_GradeComponentTypes_Courses
    WHERE GrdComponentTypeId = (
                               SELECT TOP 1 GrdComponentTypeId
                               FROM
                                      OPENXML(@hDoc, '/NewDataSet/GradeComponentTypes_Courses', 1)
                                          WITH (
                                               GrdComponentTypeId VARCHAR(50)
                                               )
                               );
    -- Insert records into Bridge Table 
    INSERT INTO arBridge_GradeComponentTypes_Courses (
                                                     GrdComponentTypeId_ReqId
                                                    ,GrdComponentTypeId
                                                    ,ReqId
                                                     )
                SELECT DISTINCT GrdComponentTypeId_ReqId
                      ,GrdComponentTypeId
                      ,ReqId
                FROM
                       OPENXML(@hDoc, '/NewDataSet/GradeComponentTypes_Courses', 1)
                           WITH (
                                GrdComponentTypeId_ReqId VARCHAR(50)
                               ,GrdComponentTypeId VARCHAR(50)
                               ,ReqId VARCHAR(50)
                                );

    EXEC sp_xml_removedocument @hDoc;



GO
