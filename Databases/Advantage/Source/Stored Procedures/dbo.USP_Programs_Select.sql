SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/****** Object:  StoredProcedure [dbo].[USP_Programs_Select]    Script Date: 1/7/2016 3:33:44 PM ******/
CREATE PROCEDURE [dbo].[USP_Programs_Select] @ProgId VARCHAR(50)
AS -- SELECT DISTINCT CIPCODE,CredentialLevel,IsGEProgram FROM arPrograms 
    SELECT DISTINCT
            CIPCode
           ,IsGEProgram
           ,Is1098T
    FROM    arPrograms
    WHERE   ProgId = @ProgId;

GO
