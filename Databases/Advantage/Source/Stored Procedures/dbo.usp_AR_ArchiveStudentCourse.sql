SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_AR_ArchiveStudentCourse]
    @ClsSectionId UNIQUEIDENTIFIER
   ,@StuEnrollId UNIQUEIDENTIFIER
   ,@User NVARCHAR(50)
AS
    BEGIN

        INSERT  INTO _archive_arResults
                (
                 Archive_User
                ,ResultId
                ,TestId
                ,Score
                ,GrdSysDetailId
                ,Cnt
                ,Hours
                ,StuEnrollId
                ,IsInComplete
                ,DroppedInAddDrop
                ,ModUser
                ,ModDate
                ,IsTransfered
                ,IsClinicsSatisfied
                ,DateDetermined
                ,IsCourseCompleted
                ,IsGradeOverridden
                ,GradeOverriddenDate
                )
                SELECT  @User
                       ,ResultId
                       ,TestId
                       ,Score
                       ,GrdSysDetailId
                       ,Cnt
                       ,Hours
                       ,StuEnrollId
                       ,IsInComplete
                       ,DroppedInAddDrop
                       ,ModUser
                       ,ModDate
                       ,IsTransfered
                       ,IsClinicsSatisfied
                       ,DateDetermined
                       ,IsCourseCompleted
                       ,IsGradeOverridden
                       ,GradeOverriddenDate
                FROM    arResults
                WHERE   StuEnrollId = @StuEnrollId
                        AND TestId = @ClsSectionId;

        DELETE  arResults
        WHERE   StuEnrollId = @StuEnrollId
                AND TestId = @ClsSectionId;

    END;



GO
