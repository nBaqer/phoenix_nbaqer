SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_InsertMRUsForFirstTimeUsers] @UserId VARCHAR(50)
AS
    BEGIN
        IF (
             SELECT COUNT(*)
             FROM   dbo.syMRUS
             WHERE  UserId = @UserId
           ) = 0
            BEGIN
                DECLARE @UserCampusId UNIQUEIDENTIFIER
                   ,@MRUTypeId INT
                   ,@UserName VARCHAR(50); 
                DECLARE @ModuleTbl TABLE ( ModuleId INT );

                DECLARE @CampusTbl TABLE
                    (
                     CampusId UNIQUEIDENTIFIER
                    );

                INSERT  INTO @ModuleTbl
                        SELECT DISTINCT
                                syModules.ResourceID
                        FROM    dbo.syRolesModules RM
                        INNER JOIN syRoles R ON RM.RoleId = R.RoleId
                        INNER JOIN (
                                     SELECT *
                                     FROM   syResources
                                     WHERE  ResourceTypeID = 1
                                   ) syModules ON syModules.ResourceID = RM.ModuleId
                        INNER JOIN dbo.syUsersRolesCampGrps URC ON URC.RoleId = R.RoleId
                        INNER JOIN dbo.syUsers U ON U.UserId = URC.UserId
                        WHERE   U.UserId = @UserId;

                INSERT  INTO @CampusTbl
                        SELECT DISTINCT
                                CGC.CampusId
                        FROM    dbo.syRolesModules RM
                        INNER JOIN syRoles R ON RM.RoleId = R.RoleId
                        INNER JOIN (
                                     SELECT *
                                     FROM   syResources
                                     WHERE  ResourceTypeID = 1
                                   ) syModules ON syModules.ResourceID = RM.ModuleId
                        INNER JOIN dbo.syUsersRolesCampGrps URC ON URC.RoleId = R.RoleId
                        INNER JOIN dbo.syUsers U ON U.UserId = URC.UserId
                        INNER JOIN dbo.syCmpGrpCmps CGC ON CGC.CampGrpId = URC.CampGrpId
                        INNER JOIN dbo.syCampuses C ON C.CampusId = CGC.CampusId
                        WHERE   U.UserId = @UserId
                                AND C.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965';

			
                DECLARE getUserCampuses_Cursor CURSOR
                FOR
                    SELECT DISTINCT
                            CampusId
                    FROM    @CampusTbl;					
                OPEN getUserCampuses_Cursor;
                FETCH NEXT FROM getUserCampuses_Cursor
			INTO @UserCampusId;

                WHILE @@FETCH_STATUS = 0
                    BEGIN
                        SET @UserName = (
                                          SELECT TOP 1
                                                    UserName
                                          FROM      syUsers
                                          WHERE     UserId = @UserId
                                        );
                        IF (
                             SELECT COUNT(*)
                             FROM   @ModuleTbl
                             WHERE  ModuleId = 189
                           ) >= 1
                            BEGIN
                                SET @MRUTypeId = 4; --Leads
                                IF (
                                     SELECT COUNT(*)
                                     FROM   dbo.syMRUS
                                     WHERE  CampusId = @UserCampusId
                                            AND UserId = @UserId
                                            AND MRUTypeId = @MRUTypeId
                                   ) = 0
                                    BEGIN
                                        INSERT  INTO dbo.syMRUS
                                                (
                                                 MRUId
                                                ,ChildId
                                                ,MRUTypeId
                                                ,UserId
                                                ,CampusId
                                                ,SortOrder
                                                ,IsSticky
                                                ,ModUser
                                                ,ModDate
								                )
                                                SELECT TOP 2
                                                        NEWID()
                                                       ,LeadId
                                                       ,@MRUTypeId
                                                       ,@UserId
                                                       ,@UserCampusId
                                                       ,0
                                                       ,0
                                                       ,@UserName
                                                       ,GETDATE()
                                                FROM    adLeads
                                                WHERE   CampusId = @UserCampusId
                                                ORDER BY ModDate ASC;
                                    END;
                            END;

                        IF (
                             SELECT COUNT(*)
                             FROM   @ModuleTbl
                             WHERE  ModuleId IN ( 26,191,193,194,300 )
                           ) >= 1
                            BEGIN
                                SET @MRUTypeId = 1; --Leads
                                IF (
                                     SELECT COUNT(*)
                                     FROM   dbo.syMRUS
                                     WHERE  CampusId = @UserCampusId
                                            AND UserId = @UserId
                                            AND MRUTypeId = @MRUTypeId
                                   ) = 0
                                    BEGIN
                                        INSERT  INTO dbo.syMRUS
                                                (
                                                 MRUId
                                                ,ChildId
                                                ,MRUTypeId
                                                ,UserId
                                                ,CampusId
                                                ,SortOrder
                                                ,IsSticky
                                                ,ModUser
                                                ,ModDate
							                    )
                                                SELECT TOP 2
                                                        NEWID()
                                                       ,StudentId
                                                       ,@MRUTypeId
                                                       ,@UserId
                                                       ,@UserCampusId
                                                       ,0
                                                       ,0
                                                       ,@UserName
                                                       ,GETDATE()
                                                FROM    dbo.arStuEnrollments
                                                WHERE   CampusId = @UserCampusId
                                                ORDER BY ModDate ASC;
                                    END;

                            END;
					
                        IF (
                             SELECT COUNT(*)
                             FROM   @ModuleTbl
                             WHERE  ModuleId IN ( 193 )
                           ) >= 1
                            BEGIN
                                SET @MRUTypeId = 2; --Employer
                                IF (
                                     SELECT COUNT(*)
                                     FROM   dbo.syMRUS
                                     WHERE  CampusId = @UserCampusId
                                            AND UserId = @UserId
                                            AND MRUTypeId = @MRUTypeId
                                   ) = 0
                                    BEGIN
                                        INSERT  INTO dbo.syMRUS
                                                (
                                                 MRUId
                                                ,ChildId
                                                ,MRUTypeId
                                                ,UserId
                                                ,CampusId
                                                ,SortOrder
                                                ,IsSticky
                                                ,ModUser
                                                ,ModDate
								                )
                                                SELECT TOP 2
                                                        NEWID()
                                                       ,EmployerId
                                                       ,@MRUTypeId
                                                       ,@UserId
                                                       ,@UserCampusId
                                                       ,0
                                                       ,0
                                                       ,@UserName
                                                       ,GETDATE()
                                                FROM    dbo.plEmployers
                                                ORDER BY ModDate ASC;
                                    END;
                            END;
				
                        IF (
                             SELECT COUNT(*)
                             FROM   @ModuleTbl
                             WHERE  ModuleId IN ( 192 )
                           ) >= 1
                            BEGIN
                                SET @MRUTypeId = 3; --Employee
                                IF (
                                     SELECT COUNT(*)
                                     FROM   dbo.syMRUS
                                     WHERE  CampusId = @UserCampusId
                                            AND UserId = @UserId
                                            AND MRUTypeId = @MRUTypeId
                                   ) = 0
                                    BEGIN
                                        INSERT  INTO dbo.syMRUS
                                                (
                                                 MRUId
                                                ,ChildId
                                                ,MRUTypeId
                                                ,UserId
                                                ,CampusId
                                                ,SortOrder
                                                ,IsSticky
                                                ,ModUser
                                                ,ModDate
						                        )
                                                SELECT TOP 2
                                                        NEWID()
                                                       ,EmpId
                                                       ,@MRUTypeId
                                                       ,@UserId
                                                       ,@UserCampusId
                                                       ,0
                                                       ,0
                                                       ,@UserName
                                                       ,GETDATE()
                                                FROM    dbo.hrEmployees
                                                ORDER BY ModDate ASC;
                                    END;
                            END;
                        FETCH NEXT FROM getUserCampuses_Cursor
				INTO @UserCampusId;
                    END;
                CLOSE getUserCampuses_Cursor;
                DEALLOCATE getUserCampuses_Cursor;
            END;
    END;

GO
