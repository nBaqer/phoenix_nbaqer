SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_GetWeightSum]
    (
     @reqId UNIQUEIDENTIFIER  
    )
AS
    SET NOCOUNT ON;  
    SELECT  SUM(ISNULL(weight,0)) AS WeightCnt
    FROM    arReqs A
           ,dbo.arGrdBkWeights B
           ,dbo.arGrdBkWgtDetails C
           ,dbo.arGrdComponentTypes D
    WHERE   A.ReqId = B.ReqId
            AND B.InstrGrdBkWgtId = C.InstrGrdBkWgtId
            AND C.GrdComponentTypeId = D.GrdComponentTypeId
            AND D.SysComponentTypeId NOT IN ( 500,503,544 )
            AND B.ReqId = @reqId
    HAVING  COUNT(*) > 0;  




GO
