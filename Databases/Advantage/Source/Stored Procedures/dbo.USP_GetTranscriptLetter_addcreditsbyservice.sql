SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_GetTranscriptLetter_addcreditsbyservice]
    (
     @stuEnrollid UNIQUEIDENTIFIER
    ,@termId VARCHAR(8000) = NULL
    ,@clsStartDate DATETIME
    ,@clsEndDate DATETIME
    )
AS
    SET NOCOUNT ON;
    EXEC dbo.USP_GetDirectChildrenForTranscript @stuEnrollid; 
    EXEC dbo.USP_GetResultsForTranscriptLetter_addcreditsbyservice @stuEnrollid,@termId,@clsStartDate,@clsEndDate;
    EXEC dbo.USP_GetCoursesFortheCourseGroupsForTranscript @stuEnrollid;

GO
