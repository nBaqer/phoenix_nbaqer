SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_IsCoursePFandSU]
    (
     @clsSectionId UNIQUEIDENTIFIER
 

    )
AS
    SET NOCOUNT ON;
    SELECT  ISNULL(t2.PF,0) AS PF
           ,ISNULL(t2.SU,0) AS SU
    FROM    arClassSections t1
           ,arReqs t2
    WHERE   t1.ReqId = t2.ReqId
            AND t1.ClsSectionId = @clsSectionId;



GO
