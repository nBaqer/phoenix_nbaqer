SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ginzo, John
-- Create date: 11/16/2015
-- Description:	get the image url data
-- =============================================
CREATE PROCEDURE [dbo].[GetPhotoDocumentInformation]
    @EntityId UNIQUEIDENTIFIER
   ,@IsLeadType BIT
AS
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;

        IF @IsLeadType = 1
            BEGIN
                SELECT TOP 1
                        DocumentType
                       ,FileId
                       ,FileExtension
                FROM    dbo.syDocumentHistory
                WHERE   LeadId = @EntityId
                        AND DocumentType = 'PhotoID';
            END;
        ELSE
            BEGIN
                SELECT TOP 1
                        DocumentType
                       ,FileId
                       ,FileExtension
                FROM    dbo.syDocumentHistory
                WHERE   StudentId = @EntityId
                        AND DocumentType = 'PhotoID';
            END;

    END;
GO
