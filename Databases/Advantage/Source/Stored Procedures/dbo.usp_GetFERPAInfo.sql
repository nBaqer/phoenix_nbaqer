SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_GetFERPAInfo]
    @FERPACategoryID UNIQUEIDENTIFIER
AS
    SET NOCOUNT ON;
    SELECT  B.StatusId
           ,(
              SELECT    Status
              FROM      syStatuses
              WHERE     StatusId = B.StatusId
            ) AS Status
           ,B.FERPACategoryCode
           ,B.FERPACategoryDEscrip
           ,B.CampGrpId
           ,(
              SELECT    CampGrpDescrip
              FROM      syCampGrps
              WHERE     CampGrpId = B.CampGrpId
            ) AS CampGrpDescrip
    FROM    arFERPACategory B
    WHERE   B.FERPACategoryID = @FERPACategoryID;




GO
