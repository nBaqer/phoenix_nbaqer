SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[USP_AR_HasStudentPassedSpeedTestsForCourse]
    (
     @StuEnrollId UNIQUEIDENTIFIER
    ,@ReqId UNIQUEIDENTIFIER
    )
AS /*----------------------------------------------------------------------------------------------------
	Author          :	Saraswathi Lakshmanan
    
    Create date		:	02/03/2010
    
	Procedure Name	:	USP_AR_HasStudentPassedSpeedTestsForCourse

	Objective		:	To get the value for a webconfig app settings key
	
	Parameters		:	Name			Type	Data Type	Required? 	
						=====			====	=========	=========	
						@StuEnrollID	In		Varchar		Required	
						@ReqId			In		Varchar		Required	
						
*/-----------------------------------------------------------------------------------------------------

    BEGIN
        SELECT  COUNT(*)
        FROM    (
                  SELECT    S.*
                           ,(
                              SELECT    COUNT(*)
                              FROM      arPostScoreDictationSpeedTest psdt
                              WHERE     psdt.grdcomponenttypeid = S.GrdComponentTypeId
                                        AND psdt.stuenrollid = @StuEnrollId
                                        AND psdt.speed >= S.MinimumScore
                            ) AS ActualNumberOfTestsPassed
                  FROM      (
                              SELECT    rq.Descrip
                                       ,gct.Descrip AS Component
                                       ,R.EffectiveDate
                                       ,R.NumberofTests
                                       ,R.MinimumScore
                                       ,rq.ReqId
                                       ,g.GrdComponentTypeId
                              FROM      arBridge_GradeComponentTypes_Courses g
                              INNER JOIN arRules_GradeComponentTypes_Courses R ON g.GrdComponentTypeId_ReqId = R.GrdComponentTypeId_ReqId
                              INNER JOIN arReqs rq ON g.ReqId = rq.ReqId
                              INNER JOIN arGrdComponentTypes gct ON g.GrdComponentTypeId = gct.GrdComponentTypeId
                              WHERE     -- 'The effective date must be based on the last time the student took the course
                                        R.EffectiveDate <= (
                                                             SELECT TOP 1
                                                                    cs.StartDate
                                                             FROM   arResults rs
                                                                   ,arClassSections cs
                                                             WHERE  rs.StuEnrollId = @StuEnrollId
                                                                    AND rs.TestId = cs.ClsSectionId
                                                                    AND cs.ReqId = @ReqId
                                                                    AND cs.ReqId = g.ReqId
                                                             ORDER BY cs.StartDate DESC
                                                           )
                                        AND R.EffectiveDate = (
                                                                SELECT  MAX(EffectiveDate)
                                                                FROM    (
                                                                          SELECT    R.EffectiveDate
                                                                          FROM      arBridge_GradeComponentTypes_Courses g
                                                                          INNER JOIN arRules_GradeComponentTypes_Courses R ON g.GrdComponentTypeId_ReqId = R.GrdComponentTypeId_ReqId
                                                                          INNER JOIN arReqs rq ON g.ReqId = rq.ReqId
                                                                          INNER JOIN arGrdComponentTypes gct ON g.GrdComponentTypeId = gct.GrdComponentTypeId
                                                                          WHERE     R.EffectiveDate <= (
                                                                                                         SELECT TOP 1
                                                                                                                cs.StartDate
                                                                                                         FROM   arResults rs
                                                                                                         INNER JOIN arClassSections cs ON rs.TestId = cs.ClsSectionId
                                                                                                         WHERE  rs.StuEnrollId = @StuEnrollId
                                                                                                                AND cs.ReqId = @ReqId
                                                                                                                AND cs.ReqId = g.ReqId
                                                                                                         ORDER BY cs.StartDate DESC
                                                                                                       )
                                                                        ) R
                                                              )
                            ) S
                ) T
        WHERE   NumberofTests > ActualNumberOfTestsPassed; 
    END;



GO
