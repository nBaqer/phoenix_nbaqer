SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_pr_main_subreport4_showunderterm_LabWork500_Revised_CAH]
    @StuEnrollId VARCHAR(50)
   ,@TermId VARCHAR(50) = NULL
   ,@SysComponentTypeId VARCHAR(50) = NULL
   ,@ShowWorkUnitGrouping BIT = 0
   ,@SetGradeBookAt VARCHAR(50)
AS
    IF LOWER(@SetGradeBookAt) = 'instructorlevel'
        BEGIN
	--Select 
	--	GradeBookDescription, GradeBookScore, MinResult, 
	--	GradeBookSysComponentTypeId,GradeComponentDescription,
	--	CampDescrip,FirstName,LastName,MiddleName,
	--	rownumber,GrdBkResultId,
	--	TermStartDate,TermEndDate,TermDescription,CourseDescription,
	--	PrgVerDescrip
	--	from 
	--	(
	--		select  
 --               				4 as Tag,  
 --               				3 as Parent,  
 --               				PV.PrgVerId,          
 --               				PV.PrgVerDescrip,    
 --               				null As ProgramCredits,   
	--							T.TermId, 
 --               				T.TermDescrip as TermDescription,  
 --               				T.StartDate as TermStartDate,  
 --               				T.EndDate as TermEndDate,     
 --               				R.ReqId as CourseId,  
 --               				R.Descrip as CourseDescription,   
 --                				'(' +  	R.Code + ' ) '  + R.Descrip as CourseCodeDescrip, 
 --               				null as CourseCredits,  
 --               				null as CourseFinAidCredits,  
 --               				null as CoursePassingGrade,  
	--							null as CourseScore, 
 --               				GBR.GrdBkResultId,   
	--							Case When Lower(@SetGradeBookAt)='instructorlevel' Then RTRIM(GBWD.Descrip) Else GCT.Descrip End as GradeBookDescription,
	--							(CASE GCT.SysComponentTypeId 
	--								WHEN 544 THEN (SELECT SUM(HoursAttended) FROM arExternshipAttendance WHERE StuEnrollId=SE.StuEnrollId) 
	--								ELSE GBR.Score 
	--								END 
	--							) as GradeBookScore, 
 --               				null as GradeBookPostDate,null as GradeBookPassingGrade,null as GradeBookWeight,  
 --               				null as GradeBookRequired,null as GradeBookMustPass,
	--							GCT.SysComponentTypeId as GradeBookSysComponentTypeId, 
 --               				null as GradeBookHoursRequired,null as GradeBookHoursCompleted,
	--								SE.StuEnrollId, 
	--							 (CASE WHEN GCT.SysComponentTypeId  in (500,503,504,544) THEN GBWD.Number 
	--								   ELSE (SELECT MIN(MinVal) 
	--											FROM arGradeScaleDetails GSD, arGradeSystemDetails GSS 
	--											WHERE GSD.GrdSysDetailId=GSS.GrdSysDetailId 
	--											AND GSS.IsPass=1 and GSD.GrdScaleId=CS.GrdScaleId) 
	--									END 
	--								) AS MinResult,SYRES.Resource as GradeComponentDescription,
	--							null as CreditsAttempted,
	--							null as CreditsEarned,
	--							null as Completed,
	--							null as CurrentScore,
	--							null as CurrentGrade,
	--							GCT.SysComponentTypeId as FinalScore,
	--							null as FinalGrade,
	--							null as WeightedAverage_GPA,null as SimpleAverage_GPA,null as WeightedAverage_CumGPA,null as SimpleAverage_CumGPA,C.CampusId,C.CampDescrip, 
	--							ROW_NUMBER() OVER (partition by SE.StuEnrollId,GCT.SysComponentTypeId Order By C.CampDescrip,PV.PrgVerDescrip,T.StartDate,T.EndDate,T.TermId,T.TermDescrip,R.ReqId,R.Descrip,GCT.SysComponentTypeId,GCT.Descrip) as rownumber,
	--							S.FirstName as FirstName,
	--							S.LastName as LastName,
	--							S.MiddleName
	--					from	
	--							arGrdBkResults GBR INNER JOIN arStuEnrollments SE ON GBR.StuEnrollId = SE.StuEnrollId
	--							INNER JOIN (Select StudentId,FirstName,LastName,MiddleName from arStudent) S ON S.StudentId = SE.StudentId
	--							INNER JOIN arClassSections CS ON CS.ClsSectionId=GBR.ClsSectionId
	--							INNER JOIN arPrgVersions PV ON SE.prgVerId = PV.PrgVerId
	--							INNER JOIN arTerm T ON CS.TermId = T.TermId
	--							INNER JOIN arReqs R  ON CS.ReqId = R.ReqId
	--							INNER JOIN arResults RES ON RES.StuEnrollId = GBR.StuEnrollId and RES.TestId = CS.ClsSectionId
	--							INNER JOIN arGrdBkWgtDetails GBWD ON GBR.InstrGrdBkWgtDetailId = GBWD.InstrGrdBkWgtDetailId
	--							INNER JOIN arGrdComponentTypes GCT on GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
	--							INNER JOIN (Select Resource,ResourceId from syResources where ResourceTypeId=10) SYRES ON SYRES.ResourceId=GCT.SysComponentTypeId
	--							INNER JOIN syCampuses C ON SE.CampusId = C.CampusId
	--					where
	--							SE.StuEnrollId = @StuEnrollId and 
	--							T.TermId = @TermId and --R.ReqId = @ReqId and 
	--							(@SysComponentTypeId is null or GCT.SysComponentTypeId in (Select Val from [MultipleValuesForReportParameters](@SysComponentTypeId,',',1)))
	--	union   
	--					select  
 --               				4 as Tag,  
 --               				3,  
 --               				PV.PrgVerId,        
 --               				PV.PrgVerDescrip,   
 --               				null,  
	--							T.TermId, 
 --               				T.TermDescrip,  
 --               				T.StartDate as termStartdate,  
 --               				T.EndDate as TermEndDate,   
 --               				GBCR.ReqId,   
 --               				R.Descrip as CourseDescrip,   
 --                				'(' +  R.Code + ' ) ' + R.Descrip  as CourseCodeDescrip, 
 --               				null,  
 --               				null,  
	--							null, 
 --               				null,  
 --               				GBCR.ConversionResultId as GrdBkResultId,   
 --               				Case When Lower(@SetGradeBookAt)='instructorlevel' Then RTRIM(GBWD.Descrip) Else GCT.Descrip End as GradeBookDescription,
 --               				GBCR.Score as GradeBookResult,
	--							null,null,null,null,null,GCT.SysComponentTypeId,null,null,  
	--							SE.StuEnrollId, GBCR.MinResult,SYRES.Resource -- Student data  
	--							,null as CreditsAttempted,
	--							null as CreditsEarned,
	--							null as Completed,
	--							null as CurrentScore,
	--							null as CurrentGrade,
	--							null as FinalScore,
	--							null as FinalGrade,null as WeightedAverage_GPA,null as SimpleAverage_GPA,null,null,C.CampusId,
	--							C.CampDescrip
	--							,ROW_NUMBER() OVER (partition by SE.StuEnrollId,GCT.SysComponentTypeId Order By C.CampDescrip,PV.PrgVerDescrip,T.StartDate,T.EndDate,T.TermId,T.TermDescrip,R.ReqId,R.Descrip,GCT.SysComponentTypeId,GCT.Descrip) as rownumber,
	--							S.FirstName as FirstName,
	--							S.LastName as LastName,
	--							S.MiddleName
							
	--					from	arGrdBkConversionResults GBCR INNER JOIN arStuEnrollments SE ON GBCR.StuEnrollId = SE.StuEnrollId
	--							 INNER JOIN (Select StudentId,FirstName,LastName,MiddleName from arStudent) S ON S.StudentId = SE.StudentId
	--							INNER JOIN arPrgVersions PV  ON SE.prgVerId = PV.PrgVerId
	--							INNER JOIN arTerm T ON GBCR.TermId = T.TermId
	--							INNER JOIN arReqs R ON GBCR.ReqId = R.ReqId
	--							INNER JOIN arGrdComponentTypes GCT ON GCT.GrdComponentTypeId=GBCR.GrdComponentTypeId
	--							INNER JOIN arGrdBkWgtDetails GBWD ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId AND GBWD.GrdComponentTypeId=GBCR.GrdComponentTypeId
	--							INNER JOIN arGrdBkWeights GBW ON GBWD.InstrGrdBkWgtId=GBW.InstrGrdBkWgtId AND GBCR.ReqId=GBW.ReqId
	--							INNER JOIN (select ReqId,Max(EffectiveDate) as EffectiveDate from arGrdBkWeights group by ReqId) as MaxEffectiveDatesByCourse ON GBCR.ReqId = MaxEffectiveDatesByCourse.ReqId
	--							INNER JOIN (Select Resource,ResourceId from syResources where ResourceTypeId=10) SYRES ON SYRES.ResourceId=GCT.SysComponentTypeId
	--							INNER JOIN syCampuses C ON SE.CampusId = C.CampusId
	--					where  
	--							MaxEffectiveDatesByCourse.EffectiveDate <= T.StartDate and
	--							SE.StuEnrollId = @StuEnrollId and T.TermId = @TermId and --R.ReqId = @ReqId and 
	--							(@SysComponentTypeId is null or GCT.SysComponentTypeId in (Select Val from [MultipleValuesForReportParameters](@SysComponentTypeId,',',1)))
	--	) dt
	--	where GradeBookSysComponentTypeId=500
	--	Order By			
	--				--TermStartDate,TermEndDate,TermDescription,CourseDescription,
	--				GradeComponentDescription,rownumber,GradeBookDescription
	
            SELECT  GradeBookDescription
                   ,MinResult
                   ,GradeBookScore
                   ,GradeComponentDescription
                   ,GradeBookSysComponentTypeId
                   ,rownumber
            FROM    (
                      SELECT    d.ReqId
                               ,d.TermId
                               ,CASE WHEN a.Descrip IS NULL THEN e.Descrip
                                     ELSE a.Descrip
                                END AS GradeBookDescription
                               ,( CASE e.SysComponentTypeId
                                    WHEN 500 THEN a.Number
                                    WHEN 503 THEN a.Number
                                    WHEN 544 THEN a.Number
                                    ELSE (
                                           SELECT   MIN(MinVal)
                                           FROM     arGradeScaleDetails GSD
                                                   ,arGradeSystemDetails GSS
                                           WHERE    GSD.GrdSysDetailId = GSS.GrdSysDetailId
                                                    AND GSS.IsPass = 1
                                                    AND GSD.GrdScaleId = d.GrdScaleId
                                         )
                                  END ) AS MinResult
                               ,a.Required
                               ,a.MustPass
                               ,ISNULL(e.SysComponentTypeId,0) AS GradeBookSysComponentTypeId
                               ,a.Number
                               ,(
                                  SELECT    Resource
                                  FROM      syResources
                                  WHERE     ResourceID = e.SysComponentTypeId
                                ) AS GradeComponentDescription
                               ,a.InstrGrdBkWgtDetailId
                               ,c.StuEnrollId
                               ,0 AS IsExternShip
                               ,(
                                  SELECT    Score
                                  FROM      arGrdBkResults
                                  WHERE     StuEnrollId = @StuEnrollId
                                            AND InstrGrdBkWgtDetailId = a.InstrGrdBkWgtDetailId
                                            AND ClsSectionId = d.ClsSectionId
                                ) AS GradeBookScore
                               ,ROW_NUMBER() OVER ( PARTITION BY ST.StuEnrollId,T.TermId,R.ReqId ORDER BY ST.CampusId, T.StartDate, T.EndDate, T.TermId, T.TermDescrip, R.ReqId, R.Descrip, RES.Resource, e.Descrip ) AS rownumber
                      FROM      arGrdBkWgtDetails a
                               ,arGrdBkWeights b
                               ,arResults c
                               ,arClassSections d
                               ,arGrdComponentTypes e
                               ,arStuEnrollments ST
                               ,arTerm T
                               ,arReqs R
                               ,syResources RES
                      WHERE     a.InstrGrdBkWgtId = b.InstrGrdBkWgtId
                                AND c.TestId = d.ClsSectionId
                                AND b.InstrGrdBkWgtId = d.InstrGrdBkWgtId
                                AND e.GrdComponentTypeId = a.GrdComponentTypeId
                                AND c.StuEnrollId = ST.StuEnrollId
                                AND d.TermId = T.TermId
                                AND d.ReqId = R.ReqId
                                AND e.SysComponentTypeId = RES.ResourceID
                                AND c.StuEnrollId = @StuEnrollId
                                AND d.TermId = @TermId --and d.ReqId=@ReqId 
                                AND (
                                      @SysComponentTypeId IS NULL
                                      OR e.SysComponentTypeId IN ( SELECT   Val
                                                                   FROM     MultipleValuesForReportParameters(@SysComponentTypeId,',',1) )
                                    ) 
		 --union
			--Select d.reqid,d.termId,Case When a.Descrip is null Then e.Descrip  else a.Descrip end as GradeBookDescription,  
   --         (CASE e.SysComponentTypeId    
   --          WHEN 500 THEN a.Number  
   --          WHEN 503 THEN a.Number  
   --          WHEN 544 THEN a.Number 
   --          ELSE (SELECT MIN(MinVal) FROM arGradeScaleDetails GSD, arGradeSystemDetails GSS   
   --          WHERE GSD.GrdSysDetailId=GSS.GrdSysDetailId              AND GSS.IsPass=1 and GSD.GrdScaleId=d.GrdScaleId) END )	AS MinResult, 
   --          a.required, a.mustpass, isnull(e.SysComponentTypeId,0) as GradeBookSysComponentTypeId, a.number,
   --          (select Resource from syResources where Resourceid=e.SysComponentTypeId) as GradeComponentDescription, 
   --          a.InstrGrdBkWgtDetailId, c.stuenrollid,0 as IsExternShip,
   --          (select Score from arGrdBkResults where StuEnrollId=@StuEnrollId and InstrGrdBkWgtDetailId=a.InstrGrdBkWgtDetailId and ClsSectionId=d.ClsSectionId) as GradeBookScore,
   --          ROW_NUMBER() OVER (partition by ST.StuEnrollId,T.TermId,R.ReqId Order By ST.CampusId,T.StartDate,T.EndDate,T.TermId,T.TermDescrip,R.ReqId,R.Descrip,RES.Resource,e.Descrip) as rownumber
   --          from 
			--		arGrdBkWgtDetails a,arGrdBkWeights b,arResults c,arClassSections d,arGrdComponentTypes e, arReqs R,arStuEnrollments ST, arTerm T,syResources RES
   --          where 
			--		a.InstrGrdBkWgtId=b.InstrGrdBkWgtId and c.testid = d.clsSectionid 
			--		and b.ReqId=R.ReqId and e.GrdComponentTypeId=a.GrdComponentTypeId and 
			--		d.ReqId=R.ReqId and d.TermId=T.TermId and
			--		c.StuEnrollId = ST.StuEnrollId  and e.SysComponentTypeId=RES.ResourceID  
			--		and c.StuEnrollid= @StuEnrollId and 
			--		d.TermId=@TermId --and R.ReqId=@ReqId
			--		and (@SysComponentTypeId is null or e.SysComponentTypeId in (Select Val from [MultipleValuesForReportParameters](@SysComponentTypeId,',',1))) 
                      UNION
                      SELECT    R.ReqId
                               ,T.TermId
                               ,CASE WHEN GBW.Descrip IS NULL THEN (
                                                                     SELECT Resource
                                                                     FROM   syResources
                                                                     WHERE  ResourceID = GCT.SysComponentTypeId
                                                                   )
                                     ELSE GBW.Descrip
                                END AS GradeBookDescription
                               ,( CASE GCT.SysComponentTypeId
                                    WHEN 500 THEN GBWD.Number
                                    WHEN 503 THEN GBWD.Number
                                    WHEN 544 THEN GBWD.Number
                                    ELSE (
                                           SELECT   MIN(MinVal)
                                           FROM     arGradeScaleDetails GSD
                                                   ,arGradeSystemDetails GSS
                                           WHERE    GSD.GrdSysDetailId = GSS.GrdSysDetailId
                                                    AND GSS.IsPass = 1
                                                    AND GSD.GrdScaleId = CSC.GrdScaleId
                                         )
                                  END ) AS MinResult
                               ,GBWD.Required
                               ,GBWD.MustPass
                               ,ISNULL(GCT.SysComponentTypeId,0) AS GradeBookSysComponentTypeId
                               ,GBWD.Number
                               ,(
                                  SELECT    Resource
                                  FROM      syResources
                                  WHERE     ResourceID = GCT.SysComponentTypeId
                                ) AS GradeComponentDescription
                               ,GBWD.InstrGrdBkWgtDetailId
                               ,GBCR.StuEnrollId
                               ,0 AS IsExternShip
                               ,(
                                  SELECT    Score
                                  FROM      arGrdBkResults
                                  WHERE     StuEnrollId = @StuEnrollId
                                            AND InstrGrdBkWgtDetailId = GBWD.InstrGrdBkWgtDetailId
                                            AND ClsSectionId = CSC.ClsSectionId
                                ) AS GradeBookScore
                               ,ROW_NUMBER() OVER ( PARTITION BY SE.StuEnrollId,T.TermId,R.ReqId ORDER BY SE.CampusId, T.StartDate, T.EndDate, T.TermId, T.TermDescrip, R.ReqId, R.Descrip, SYRES.Resource, GCT.Descrip ) AS rownumber
                      FROM      arGrdBkConversionResults GBCR
                      INNER JOIN arStuEnrollments SE ON GBCR.StuEnrollId = SE.StuEnrollId
                      INNER JOIN (
                                   SELECT   StudentId
                                           ,FirstName
                                           ,LastName
                                           ,MiddleName
                                   FROM     arStudent
                                 ) S ON S.StudentId = SE.StudentId
                      INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                      INNER JOIN arTerm T ON GBCR.TermId = T.TermId
                      INNER JOIN arReqs R ON GBCR.ReqId = R.ReqId
                      INNER JOIN arGrdComponentTypes GCT ON GCT.GrdComponentTypeId = GBCR.GrdComponentTypeId
                      INNER JOIN arGrdBkWgtDetails GBWD ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
                                                           AND GBWD.GrdComponentTypeId = GBCR.GrdComponentTypeId
                      INNER JOIN arGrdBkWeights GBW ON GBWD.InstrGrdBkWgtId = GBW.InstrGrdBkWgtId
                                                       AND GBCR.ReqId = GBW.ReqId
                      INNER JOIN (
                                   SELECT   ReqId
                                           ,MAX(EffectiveDate) AS EffectiveDate
                                   FROM     arGrdBkWeights
                                   GROUP BY ReqId
                                 ) AS MaxEffectiveDatesByCourse ON GBCR.ReqId = MaxEffectiveDatesByCourse.ReqId
                      INNER JOIN (
                                   SELECT   Resource
                                           ,ResourceID
                                   FROM     syResources
                                   WHERE    ResourceTypeID = 10
                                 ) SYRES ON SYRES.ResourceID = GCT.SysComponentTypeId
                      INNER JOIN syCampuses C ON SE.CampusId = C.CampusId
                      INNER JOIN arClassSections CSC ON CSC.TermId = T.TermId
                                                        AND CSC.ReqId = R.ReqId
                      WHERE     MaxEffectiveDatesByCourse.EffectiveDate <= T.StartDate
                                AND SE.StuEnrollId = @StuEnrollId
                                AND T.TermId = @TermId --and R.ReqId = @ReqId  
                                AND (
                                      @SysComponentTypeId IS NULL
                                      OR GCT.SysComponentTypeId IN ( SELECT Val
                                                                     FROM   MultipleValuesForReportParameters(@SysComponentTypeId,',',1) )
                                    )
                    ) dt
            WHERE   GradeBookSysComponentTypeId = 500
            ORDER BY GradeBookSysComponentTypeId
                   ,rownumber
                   ,GradeBookDescription; 
        END;
    ELSE
        BEGIN
            CREATE TABLE #Temp1
                (
                 Id UNIQUEIDENTIFIER
                ,StuEnrollId UNIQUEIDENTIFIER
                ,TermId UNIQUEIDENTIFIER
                ,GradeBookDescription VARCHAR(50)
                ,Number INT
                ,GradeBookSysComponentTypeId INT
                ,GradeBookScore DECIMAL(18,2)
                ,MinResult DECIMAL(18,2)
                ,GradeComponentDescription VARCHAR(50)
                ,RowNumber INT
                ,ClsSectionId UNIQUEIDENTIFIER
                );
            DECLARE @Id UNIQUEIDENTIFIER
               ,@Descrip VARCHAR(50)
               ,@Number INT
               ,@GrdComponentTypeId INT
               ,@Counter INT
               ,@times INT;
            DECLARE @MinResult DECIMAL(18,2)
               ,@GrdComponentDescription VARCHAR(50)
               ,@rownumber INT;
		 --Declare @TermId uniqueidentifier,@StuEnrollId uniqueidentifier
            DECLARE @ClsSectionId UNIQUEIDENTIFIER;
		 --set @TermId='87D587A4-5E22-4685-943C-096758C00447'
		 --set @StuEnrollId = '66F5BB2D-EC46-470D-8A4A-6E1B87B10171'
            SET @Counter = 0;
		
            DECLARE @TermStartDate1 DATETIME;
            SET @TermStartDate1 = (
                                    SELECT  StartDate
                                    FROM    arTerm
                                    WHERE   TermId = @TermId
                                  );
		--set @InstrGrdBkWgtId = (select Distinct Top 1 InstrGrdBkWgtId from arGrdBkWeights t1,arReqs t2,arClassSections t3,arResults t4
		--						where t1.ReqId=t2.ReqId and t2.ReqId=t3.ReqId and t3.TermId=@TermId and 
		--						t3.ClsSectionId=t4.TestId and t4.StuEnrollId=@StuEnrollId and 
		--						Max(t1.EffectiveDate)<=@TermStartDate1)
            CREATE TABLE #temp2
                (
                 ReqId UNIQUEIDENTIFIER
                ,EffectiveDate DATETIME
                );
            INSERT  INTO #Temp2
                    SELECT  ReqId
                           ,MAX(EffectiveDate) AS EffectiveDate
                    FROM    arGrdBkWeights
                    WHERE   ReqId IN ( SELECT   ReqId
                                       FROM     syCreditSummary
                                       WHERE    StuEnrollId = @StuEnrollId
                                                AND TermId = @TermId )
                            AND EffectiveDate <= @TermStartDate1
                    GROUP BY ReqId;
		
            DECLARE getUsers_Cursor CURSOR
            FOR
                SELECT  *
                       ,ROW_NUMBER() OVER ( PARTITION BY @StuEnrollId,@TermId,SysComponentTypeId ORDER BY SysComponentTypeId, Descrip ) AS rownumber
                FROM    (
                          SELECT DISTINCT
                                    ISNULL(GD.InstrGrdBkWgtDetailId,NEWID()) AS ID
                                   ,GC.Descrip
                                   ,GD.Number
                                   ,GC.SysComponentTypeId
                                   ,( CASE WHEN GC.SysComponentTypeId IN ( 500,503,504,544 ) THEN GD.Number
                                           ELSE (
                                                  SELECT    MIN(MinVal)
                                                  FROM      arGradeScaleDetails GSD
                                                           ,arGradeSystemDetails GSS
                                                  WHERE     GSD.GrdSysDetailId = GSS.GrdSysDetailId
                                                            AND GSS.IsPass = 1
                                                            AND GSD.GrdScaleId = CS.GrdScaleId
                                                )
                                      END ) AS MinResult
                                   ,S.Resource AS GradeComponentDescription
                                   ,CS.ClsSectionId
				--,MaxEffectiveDatesByCourse.ReqId 
                          FROM      arGrdComponentTypes GC
                                   ,(
                                      SELECT    *
                                      FROM      arGrdBkWgtDetails
                                      WHERE     InstrGrdBkWgtId IN ( SELECT t1.InstrGrdBkWgtId
                                                                     FROM   arGrdBkWeights t1
                                                                           ,#Temp2 t2
                                                                     WHERE  t1.ReqId = t2.ReqId
                                                                            AND t1.EffectiveDate = t2.EffectiveDate )
                                    ) GD
                                   ,arGrdBkWeights GW
                                   ,arReqs R
                                   ,arClassSections CS
                                   ,syResources S
                                   ,arResults RES
                                   ,arTerm T
                          WHERE     GC.GrdComponentTypeId = GD.GrdComponentTypeId
                                    AND GD.InstrGrdBkWgtId = GW.InstrGrdBkWgtId
                                    AND GW.ReqId = R.ReqId
                                    AND R.ReqId = CS.ReqId
                                    AND CS.TermId = @TermId
                                    AND RES.TestId = CS.ClsSectionId
                                    AND RES.StuEnrollId = @StuEnrollId
                                    AND GD.Number > 0
                                    AND GC.SysComponentTypeId = S.ResourceID
                                    AND CS.TermId = T.TermId
                        ) dt
                WHERE   SysComponentTypeId = 500
                ORDER BY SysComponentTypeId
                       ,rownumber;
            OPEN getUsers_Cursor;
            FETCH NEXT FROM getUsers_Cursor
		INTO @Id,@Descrip,@Number,@GrdComponentTypeId,@MinResult,@GrdComponentDescription,@ClsSectionId,@rownumber;
            SET @Counter = 0;
            DECLARE @GrdCompDescrip VARCHAR(50)
               ,@Score DECIMAL(18,2);
            WHILE @@FETCH_STATUS = 0
                BEGIN
                    SET @times = 1;
                    SET @GrdCompDescrip = @Descrip;
			--set @Score = (select Top 1 Score from arGrdBkResults where StuEnrollId=@StuEnrollId and InstrGrdBkWgtDetailId=@Id and ResNum=@times) 
			--if @Score is NULL
			--			begin
			--				set @Score = (select Top 1 Score from arGrdBkResults where StuEnrollId=@StuEnrollId and InstrGrdBkWgtDetailId=@Id and ResNum=(@times-1)) 	
			--			end
			
                    SET @Score = (
                                   SELECT   SUM(Score)
                                   FROM     arGrdBkResults
                                   WHERE    StuEnrollId = @StuEnrollId
                                            AND InstrGrdBkWgtDetailId = @Id
                                            AND ClsSectionId = @ClsSectionId
                                 ); 
										
                    INSERT  INTO #Temp1
                    VALUES  ( @Id,@StuEnrollId,@TermId,@GrdCompDescrip,@Number,@GrdComponentTypeId,@Score,@MinResult,@GrdComponentDescription,@rownumber,
                              @ClsSectionId );
			
                    FETCH NEXT FROM getUsers_Cursor
			INTO @Id,@Descrip,@Number,@GrdComponentTypeId,@MinResult,@GrdComponentDescription,@ClsSectionId,@rownumber;
                END;
            CLOSE getUsers_Cursor;
            DEALLOCATE getUsers_Cursor;
            SELECT  *
            FROM    #Temp1
            WHERE   GradeBookSysComponentTypeId = 500
		--order by 
		--	GradeBookSysComponentTypeId,GradeBookDescription,RowNumber
            UNION
            SELECT  GBWD.InstrGrdBkWgtDetailId
                   ,SE.StuEnrollId
                   ,T.TermId
                   ,GCT.Descrip AS GradeBookDescription
                   ,GBWD.Number
                   ,GCT.SysComponentTypeId
                   ,GBCR.Score
                   ,GBCR.MinResult
                   ,SYRES.Resource
                   ,ROW_NUMBER() OVER ( PARTITION BY SE.StuEnrollId,T.TermId,GCT.SysComponentTypeId ORDER BY GCT.SysComponentTypeId, GCT.Descrip ) AS rownumber
                   ,(
                      SELECT TOP 1
                                ClsSectionId
                      FROM      arClassSections
                      WHERE     TermId = T.TermId
                                AND ReqId = R.ReqId
                    ) AS ClsSectionId
            FROM    arGrdBkConversionResults GBCR
            INNER JOIN arStuEnrollments SE ON GBCR.StuEnrollId = SE.StuEnrollId
            INNER JOIN (
                         SELECT StudentId
                               ,FirstName
                               ,LastName
                               ,MiddleName
                         FROM   arStudent
                       ) S ON S.StudentId = SE.StudentId
            INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
            INNER JOIN arTerm T ON GBCR.TermId = T.TermId
            INNER JOIN arReqs R ON GBCR.ReqId = R.ReqId
            INNER JOIN arGrdComponentTypes GCT ON GCT.GrdComponentTypeId = GBCR.GrdComponentTypeId
            INNER JOIN arGrdBkWgtDetails GBWD ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
                                                 AND GBWD.GrdComponentTypeId = GBCR.GrdComponentTypeId
            INNER JOIN arGrdBkWeights GBW ON GBWD.InstrGrdBkWgtId = GBW.InstrGrdBkWgtId
                                             AND GBCR.ReqId = GBW.ReqId
            INNER JOIN (
                         SELECT ReqId
                               ,MAX(EffectiveDate) AS EffectiveDate
                         FROM   arGrdBkWeights
                         GROUP BY ReqId
                       ) AS MaxEffectiveDatesByCourse ON GBCR.ReqId = MaxEffectiveDatesByCourse.ReqId
            INNER JOIN (
                         SELECT Resource
                               ,ResourceID
                         FROM   syResources
                         WHERE  ResourceTypeID = 10
                       ) SYRES ON SYRES.ResourceID = GCT.SysComponentTypeId
            INNER JOIN syCampuses C ON SE.CampusId = C.CampusId
            WHERE   MaxEffectiveDatesByCourse.EffectiveDate <= T.StartDate
                    AND SE.StuEnrollId = @StuEnrollId
                    AND T.TermId = @TermId
                    AND --R.ReqId = @ReqId and 
                    (
                      @SysComponentTypeId IS NULL
                      OR GCT.SysComponentTypeId IN ( SELECT Val
                                                     FROM   MultipleValuesForReportParameters(@SysComponentTypeId,',',1) )
                    )
                    AND GCT.SysComponentTypeId = 500
            ORDER BY GradeBookSysComponentTypeId
                   ,GradeBookDescription
                   ,rownumber;
            DROP TABLE #Temp2;
            DROP TABLE #Temp1;
        END;

GO
