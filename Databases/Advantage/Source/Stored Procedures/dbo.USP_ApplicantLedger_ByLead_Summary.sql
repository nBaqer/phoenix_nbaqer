SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[USP_ApplicantLedger_ByLead_Summary]
    @CampusId UNIQUEIDENTIFIER
   ,@TransStartDate DATE
   ,@TransEndDate DATE

---[dbo].[USP_ApplicantLedger_ByLead_Summary] '5EBF6C1F-1FB1-492D-AD53-348F3986A178', '01/01/2011' , '01/01/2013'
AS
    BEGIN
	
        DECLARE @NewTbl TABLE
            (
             TransactionID UNIQUEIDENTIFIER
            ,TransactionAmount DECIMAL(19,4)
            ,LeadName VARCHAR(100)
            ,TransactionType VARCHAR(50)
            );
        INSERT  INTO @NewTbl
                (
                 TransactionID
                ,TransactionAmount
                ,LeadName
                ,TransactionType
                )
                SELECT  TransactionID
                       ,TransactionAmount
                       ,LeadName
                       ,TransactionType
                FROM    dbo.TBL_ApplicantLedger_ByLead_Summary(@CampusId,@TransStartDate,@TransEndDate);
	
        SELECT --LeadName,----,
                TransactionType
               ,COUNT(TransactionID) AS TransactionCount
               ,CASE TransactionType
                  WHEN 'Payment' THEN CONVERT(DECIMAL(10,2),SUM(TransactionAmount)) * -1
                  WHEN 'Payment (Adjusted)' THEN CONVERT(DECIMAL(10,2),SUM(TransactionAmount)) * -1
                  ELSE CONVERT(DECIMAL(10,2),SUM(TransactionAmount))
                END AS TransactionAmount
        FROM    @NewTbl
        WHERE   TransactionType IN ( 'Payment','Payment (Adjusted)','Payment (Voided)' )
        GROUP BY TransactionType;---LeadName---,
	---ORDER BY LeadName
	
	---SELECT TransactionCount,TransactionAmount,LeadName,TransactionType FROM dbo.TBL_ApplicantLedger_ByLead_Summary ('5EBF6C1F-1FB1-492D-AD53-348F3986A178', '01/01/2011' , '01/01/2013')
	---SELECT * FROM dbo.TBL_ApplicantLedger_ByLead_Summary('5EBF6C1F-1FB1-492D-AD53-348F3986A178', '01/01/2011' , '01/01/2013')
    END;




GO
