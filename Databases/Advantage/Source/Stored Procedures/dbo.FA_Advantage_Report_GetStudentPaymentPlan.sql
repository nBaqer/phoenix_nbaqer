
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[FA_Advantage_Report_GetStudentPaymentPlan] 
	-- Add the parameters for the stored procedure here
    @paymentPlanId UNIQUEIDENTIFIER = NULL
   ,@voided BIT = 0
AS
    BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
        SET NOCOUNT ON;
-- Insert statements for procedure here
-- GET PAYMENT PLAN
        SELECT  PP.PaymentPlanId
               ,PP.StuEnrollId
               ,PP.PayPlanDescrip
               ,PP.AcademicYearId
               ,( SELECT    AcademicYearDescrip
                  FROM      saAcademicYears
                  WHERE     AcademicYearId = PP.AcademicYearId
                ) AS AcademicYear
               ,PP.TotalAmountDue
               ,PP.AwardId
               ,PP.Disbursements
               ,PP.PayPlanStartDate
               ,PP.PayPlanEndDate
               ,PP.ModUser
               ,PP.ModDate
               ,STU.StudentNumber
               ,EN.StartDate
               ,EN.ExpGradDate
               ,ST.StatusCodeDescrip
               ,VE.PrgVerDescrip
               ,STU.FirstName + ' ' + STU.LastName AS StudentName
        FROM    faStudentPaymentPlans PP
        INNER JOIN arStuEnrollments EN ON EN.StuEnrollId = PP.StuEnrollId
        INNER JOIN syStatusCodes ST ON ST.StatusCodeId = EN.StatusCodeId
        INNER JOIN arPrgVersions VE ON VE.PrgVerId = EN.PrgVerId
        INNER JOIN arStudent STU ON STU.StudentId = EN.StudentId
        WHERE   @paymentPlanId IS NULL
                OR PP.PaymentPlanId = @paymentPlanId;
 
    END;
GO
