SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_CampusName_GetList] @CampusId VARCHAR(50)
AS
    SELECT  CampDescrip
    FROM    syCampuses
    WHERE   CampusId = @CampusId;   



GO
