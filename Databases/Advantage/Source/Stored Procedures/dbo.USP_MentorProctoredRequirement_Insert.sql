SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_MentorProctoredRequirement_Insert]
    @OldEffectiveDate DATETIME
   ,@EffectiveDate DATETIME
   ,@RuleValues NTEXT
   ,@ModUser VARCHAR(50)
   ,@ModDate DATETIME
AS
    DECLARE @hDoc INT;
--Prepare input values as an XML document
    EXEC sp_xml_preparedocument @hDoc OUTPUT,@RuleValues;
-- Delete all records from bridge table based on GrdComponentTypeId
    IF YEAR(@OldEffectiveDate) <> 1900
        BEGIN
            DELETE  FROM arMentor_GradeComponentTypes_Courses
            WHERE   EffectiveDate = @OldEffectiveDate
                    AND ReqId IN ( SELECT   ReqId
                                   FROM     OPENXML(@hDoc,'/NewDataSet/SetupMentor',1) 
WITH (ReqId VARCHAR(50)) )
                    AND speed IN ( SELECT   Speed
                                   FROM     OPENXML(@hDoc,'/NewDataSet/SetupMentor',1) 
WITH (Speed INT) );
        END;
-- Insert records into Bridge Table 
    INSERT  INTO arMentor_GradeComponentTypes_Courses
            SELECT DISTINCT
                    MentorId
                   ,ReqId
                   ,@EffectiveDate
                   ,MentorRequirement
                   ,MentorOperator
                   ,GrdComponentTypeId
                   ,Speed
                   ,@ModUser
                   ,@ModDate
                   ,OperatorSequence
            FROM    OPENXML(@hDoc,'/NewDataSet/SetupMentor',1) 
WITH (MentorId UNIQUEIDENTIFIER,ReqId UNIQUEIDENTIFIER,EffectiveDate DATETIME,MentorRequirement VARCHAR(250),
MentorOperator VARCHAR(50),GrdComponentTypeId 
UNIQUEIDENTIFIER,Speed INT,ModUser VARCHAR(50),ModDate DATETIME,OperatorSequence INT);
    EXEC sp_xml_removedocument @hDoc;



GO
