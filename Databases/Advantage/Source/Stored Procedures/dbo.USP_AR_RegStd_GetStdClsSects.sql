SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[USP_AR_RegStd_GetStdClsSects]
    (
     @StuEnrollID UNIQUEIDENTIFIER
    ,@ClsSectId UNIQUEIDENTIFIER
    )
AS /*----------------------------------------------------------------------------------------------------
	Author          :	Saraswathi Lakshmanan
    
    Create date		:	02/04/2010
    
	Procedure Name	:	USP_AR_RegStd_GetClsSectDates

	Objective		:	get theStudent class Sections and Start and End Dates for a given Clssection
	
	Parameters		:	Name			Type	Data Type			Required? 	
						=====			====	=========			=========	
						@StuEnrollId	In		UniqueIdentifier
						@ClsSectID		In		Uniqueidentifier
	
	Output			:	Returns the dataset 	
						
*/-----------------------------------------------------------------------------------------------------
/*
Procedure created by Saraswathi Lakshmanan on Jan 29 2010
--To fix issue 17548: ENH: All: Register Students Page is Taking Too Long to Populate Available Students 
*/

    BEGIN
        SELECT  a.TestId
               ,b.ClsSection
               ,b.StartDate
               ,b.EndDate
        FROM    arResults a
        INNER JOIN arClassSections b ON a.TestId = b.ClsSectionId
        WHERE   a.StuEnrollId = @StuEnrollID
                AND b.ClsSectionId = @ClsSectId
                AND NOT EXISTS ( SELECT *
                                 FROM   arOverridenConflicts
                                 WHERE  leftClsSectionId = @ClsSectId
                                        OR rightClsSectionId = @ClsSectId ); 
    END;




GO
