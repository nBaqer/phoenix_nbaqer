SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[USP_IsCourseCompleted_ForClinicWorkorLabHours]
    @StuEnrollId UNIQUEIDENTIFIER
   ,@ClsSectionId UNIQUEIDENTIFIER
AS
    DECLARE @GrdBkResultId UNIQUEIDENTIFIER
       ,@ClassSectionId UNIQUEIDENTIFIER
       ,@InstrGrdBkWgtDetailId UNIQUEIDENTIFIER;
    DECLARE @AttemptedLabWorkorHours DECIMAL(18,2)
       ,@MinLabWorkorHoursNeeded DECIMAL(18,2);
    DECLARE @IsCourseCompleted BIT;
    DECLARE @ReqId UNIQUEIDENTIFIER;
    DECLARE @AllGrdBkWgtDetails TABLE
        (
         ClsSectionId UNIQUEIDENTIFIER
        ,InstrGrdBkWgId UNIQUEIDENTIFIER
        ,InstrGrdBkWgtDetailId UNIQUEIDENTIFIER
        ,Score DECIMAL(18,2)
        ,Number DECIMAL(18,2)
        );
    DECLARE @StudGrdBkWgtDetails TABLE
        (
         ClsSectionId UNIQUEIDENTIFIER
        ,InstrGrdBkWgId UNIQUEIDENTIFIER
        ,InstrGrdBkWgtDetailId UNIQUEIDENTIFIER
        ,Score DECIMAL(18,2)
        ,Number DECIMAL(18,2)
        );

    SET @ReqId = (
                   SELECT   ReqId
                   FROM     dbo.arReqs
                   WHERE    ReqId IN ( SELECT   ReqId
                                       FROM     dbo.arClassSections
                                       WHERE    ClsSectionId = @ClsSectionId )
                 );

    INSERT  INTO @AllGrdBkWgtDetails
            (
             ClsSectionId
            ,InstrGrdBkWgId
            ,InstrGrdBkWgtDetailId
            ,Score
            ,Number
            )
            SELECT  @ClsSectionId
                   ,w.InstrGrdBkWgtId
                   ,d.InstrGrdBkWgtDetailId
                   ,0
                   ,Number
            FROM    dbo.arGrdBkWeights w
            INNER JOIN dbo.arGrdBkWgtDetails d ON d.InstrGrdBkWgtId = w.InstrGrdBkWgtId
            WHERE   ReqId = @ReqId; 
		
    INSERT  INTO @StudGrdBkWgtDetails
            (
             ClsSectionId
            ,InstrGrdBkWgId
            ,InstrGrdBkWgtDetailId
            ,Score
            ,Number
            )
            SELECT DISTINCT
                    t1.ClsSectionId
                   ,t2.InstrGrdBkWgtId
                   ,t1.InstrGrdBkWgtDetailId
                   ,SUM(t1.Score) AS Score
                   ,MAX(t2.Number) AS Number
            FROM    arGrdBkResults t1
            INNER JOIN arGrdBkWgtDetails t2 ON t1.InstrGrdBkWgtDetailId = t2.InstrGrdBkWgtDetailId
            INNER JOIN arGrdComponentTypes t3 ON t2.GrdComponentTypeId = t3.GrdComponentTypeId
            LEFT OUTER JOIN @AllGrdBkWgtDetails a ON a.InstrGrdBkWgtDetailId = t1.InstrGrdBkWgtDetailId
            WHERE   StuEnrollId = @StuEnrollId
                    AND t1.ClsSectionId = @ClsSectionId
                    AND t3.SysComponentTypeId IN ( 500,503 )
            GROUP BY t1.ClsSectionId
                   ,t2.InstrGrdBkWgtId
                   ,t1.InstrGrdBkWgtDetailId
            ORDER BY t1.ClsSectionId
                   ,t2.InstrGrdBkWgtId
                   ,t1.InstrGrdBkWgtDetailId;
		
    INSERT  INTO @StudGrdBkWgtDetails
            (
             ClsSectionId
            ,InstrGrdBkWgId
            ,InstrGrdBkWgtDetailId
            ,Score
            ,Number
            )
            SELECT  ClsSectionId
                   ,InstrGrdBkWgId
                   ,InstrGrdBkWgtDetailId
                   ,Score
                   ,Number
            FROM    @AllGrdBkWgtDetails a
            WHERE   InstrGrdBkWgtDetailId NOT IN ( SELECT DISTINCT
                                                            InstrGrdBkWgtDetailId
                                                   FROM     @StudGrdBkWgtDetails );

    DECLARE getGradeBookResultsForLabWorkorLabHours CURSOR
    FOR
        SELECT  ClsSectionId
               ,InstrGrdBkWgtDetailId
               ,Score
               ,Number
        FROM    @StudGrdBkWgtDetails;

  --      Select Distinct 
		--t1.GrdBkResultId,
		--t1.ClsSectionId,t1.InstrGrdBkWgtDetailId, COALESCE(Sum(t1.Score),0) as Score,Max(t2.Number) as Number
		--from arGrdBkResults t1 inner join arGrdBkWgtDetails t2 on 
		--t1.InstrGrdBkWgtDetailId = t2.InstrGrdBkWgtDetailId
		--inner join arGrdComponentTypes t3 on t2.GrdComponentTypeId = t3.GrdComponentTypeId
		--where StuEnrollId=@StuEnrollId
		--and ClsSectionId=@ClsSectionId
		--and t3.SysComponentTypeId in (500,503) 
		--group by t1.ClsSectionId,t1.InstrGrdBkWgtDetailId,t1.GrdBkResultId
		
		
		/**
		Select Distinct t1.ClsSectionId,t1.InstrGrdBkWgtDetailId, SUM(t1.Score) as Score,Max(t2.Number) as Number
		from arGrdBkResults t1 inner join arGrdBkWgtDetails t2 on 
		t1.InstrGrdBkWgtDetailId = t2.InstrGrdBkWgtDetailId
		inner join arGrdComponentTypes t3 on t2.GrdComponentTypeId = t3.GrdComponentTypeId
		where StuEnrollId='710466B4-C4E6-46F5-A769-21FDE7488A98'
		and ClsSectionId='0BCEE910-C95E-4580-956B-AAAD079D4CF3'
		and t3.SysComponentTypeId in (500,503)
		group by t1.ClsSectionId,t1.InstrGrdBkWgtDetailId
		**/
		
    OPEN getGradeBookResultsForLabWorkorLabHours;

    FETCH NEXT FROM getGradeBookResultsForLabWorkorLabHours
INTO @ClassSectionId,@InstrGrdBkWgtDetailId,@AttemptedLabWorkorHours,@MinLabWorkorHoursNeeded;

    SET @IsCourseCompleted = 0; -- by default False

    WHILE @@FETCH_STATUS = 0
        BEGIN		
            IF @AttemptedLabWorkorHours >= @MinLabWorkorHoursNeeded
                BEGIN
                    SET @IsCourseCompleted = 1;
                END;
		
            IF @AttemptedLabWorkorHours < @MinLabWorkorHoursNeeded
                BEGIN
                    SET @IsCourseCompleted = 0; 
                    BREAK; --Exit while loop if any one of component was not completed
                END;
       
            FETCH NEXT FROM getGradeBookResultsForLabWorkorLabHours
		INTO @ClassSectionId,@InstrGrdBkWgtDetailId,@AttemptedLabWorkorHours,@MinLabWorkorHoursNeeded;
        END;
    CLOSE getGradeBookResultsForLabWorkorLabHours;
    DEALLOCATE getGradeBookResultsForLabWorkorLabHours;

    IF @IsCourseCompleted = 1
        BEGIN
            DECLARE @IsComponentAnExam INT
               ,@IsFinalGradePosted INT;
		
		-- Among the components assigned to course see if there are any exam components
            SET @IsComponentAnExam = (
                                       SELECT   COUNT(*) AS RowCounter
                                       FROM     arGrdBkWeights CourseGradeBook
                                       INNER JOIN arGrdBkWgtDetails CourseGradeBookDetails ON CourseGradeBook.InstrGrdBkWgtId = CourseGradeBookDetails.InstrGrdBkWgtId
                                       INNER JOIN arGrdComponentTypes ComponentTypes ON CourseGradeBookDetails.GrdComponentTypeId = ComponentTypes.GrdComponentTypeId
                                       WHERE    CourseGradeBook.ReqId IN ( SELECT TOP 1
                                                                                    ReqId
                                                                           FROM     arClassSections
                                                                           WHERE    ClsSectionId = @ClassSectionId )  --  'F33BCEE0-6334-47D5-98BC-A35B5AE6B2B1'
                                                AND ComponentTypes.SysComponentTypeId IN ( 501,533 )
                                     );
	
		--Print 'ExamComponent='
		--Print @IsComponentAnExam
		--If there is an exam component check if there is a final grade available on
		--the course in arResults table
            IF @IsComponentAnExam >= 1
                BEGIN
                    SET @IsFinalGradePosted = (
                                                SELECT  COUNT(*)
                                                FROM    arResults
                                                WHERE   StuEnrollId = @StuEnrollId
                                                        AND TestId = @ClsSectionId
                                                        AND (
                                                              GrdSysDetailId IS NOT NULL
                                                              OR Score IS NOT NULL
                                                            )
                                              );
				
				--Print 'Final Grade Posted='
				--Print @IsFinalGradePosted
				
				-- If student has a final grade on the course set CourseCompleted Flag to 1					 
                    IF @IsFinalGradePosted >= 1
                        BEGIN
                            UPDATE  arResults
                            SET     IsCourseCompleted = 1
                            WHERE   StuEnrollId = @StuEnrollId
                                    AND TestId = @ClsSectionId;			
                        END; 		
                    ELSE
                        BEGIN
                            UPDATE  arResults
                            SET     IsCourseCompleted = 0
                            WHERE   StuEnrollId = @StuEnrollId
                                    AND TestId = @ClsSectionId;			
                        END;
                END;
            ELSE
                BEGIN
                    UPDATE  arResults
                    SET     IsCourseCompleted = 1
                    WHERE   StuEnrollId = @StuEnrollId
                            AND TestId = @ClsSectionId;			
                END;
        END;
    ELSE
        BEGIN
            UPDATE  arResults
            SET     IsCourseCompleted = 0
            WHERE   StuEnrollId = @StuEnrollId
                    AND TestId = @ClsSectionId;
        END;




GO
