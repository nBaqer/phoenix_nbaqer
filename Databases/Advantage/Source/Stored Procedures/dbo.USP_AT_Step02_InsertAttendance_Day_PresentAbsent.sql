SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

--=================================================================================================
-- USP_AT_Step02_InsertAttendance_Day_PresentAbsent
--=================================================================================================
CREATE PROCEDURE [dbo].[USP_AT_Step02_InsertAttendance_Day_PresentAbsent]
    (
        @StuEnrollIdList AS VARCHAR(MAX) = NULL
    )
AS
    BEGIN

        DECLARE @MyEnrollments TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
            );

        DECLARE @HourTotals2 TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,RecordDate DATETIME
               ,ScheduledHours DECIMAL(18, 2)
               ,ActualHours DECIMAL(18, 2)
               ,ActualRunningScheduledDays DECIMAL(18, 2)
               ,ActualRunningPresentHours DECIMAL(18, 2)
               ,ActualRunningAbsentHours DECIMAL(18, 2)
               ,ActualRunningMakeupHours DECIMAL(18, 2)
               ,ActualRunningTardyHours DECIMAL(18, 2)
               ,ActualRunningExcusedDays INT
               ,Tardy DECIMAL(18, 2)
               ,AdjustedRunningPresentHours DECIMAL(18, 2)
               ,AdjustedRunningAbsentHours DECIMAL(18, 2)
               ,tracktardies INT
               ,tardiesMakingAbsence INT
               ,intTardyBreakPoint INT
               ,rownumber INT
            );

        INSERT INTO @MyEnrollments
                    SELECT enrollments.StuEnrollId
                    FROM   dbo.arStuEnrollments enrollments
                    WHERE  (
                           @StuEnrollIdList IS NULL
                           OR ( enrollments.StuEnrollId IN (
                                                           SELECT Val
                                                           FROM   MultipleValuesForReportParameters(@StuEnrollIdList, ',', 1)
                                                           )
                              )
                           );


        INSERT INTO @HourTotals2 (
                                 StuEnrollId
                                ,RecordDate
                                ,ScheduledHours
                                ,ActualHours
                                ,Tardy
                                ,ActualRunningScheduledDays
                                ,ActualRunningPresentHours
                                ,ActualRunningAbsentHours
                                ,ActualRunningMakeupHours
                                ,ActualRunningTardyHours
                                ,tracktardies
                                ,tardiesMakingAbsence
                                ,intTardyBreakPoint
                                ,rownumber
                                 )
                    SELECT   hoursBucketAll.StuEnrollId
                            ,hoursBucketAll.RecordDate
                            ,hoursBucketAll.SchedHours
                            ,hoursBucketAll.ActualHours
                            ,hoursBucketAll.isTardy
                            ,hoursBucketAll.ActualRunningScheduledHours
                            ,hoursBucketAll.ActualRunningPresentHours
                            ,hoursBucketAll.ActualRunningAbsentHours
                            ,hoursBucketAll.ActualRunningMakeupHours
                            ,hoursBucketAll.ActualRunningTardyHours
                            ,hoursBucketAll.TrackTardies
                            ,hoursBucketAll.TardiesMakingAbsence
                            ,hoursBucketAll.intTardyBreakPoint
                            ,hoursBucketAll.RowNumber
                    FROM     (
                             SELECT *
                                   ,CASE WHEN hoursBucketWithTardy.TrackTardies = 1
                                              AND hoursBucketWithTardy.intTardyBreakPoint % hoursBucketWithTardy.TardiesMakingAbsence = 0 THEN
                                   ( -hoursBucketWithTardy.ActualRunningPresentHours )
                                         ELSE hoursBucketWithTardy.ActualRunningPresentHours
                                    END AS AdjustedRunningPresentHours
                                   ,CASE WHEN hoursBucketWithTardy.TrackTardies = 1
                                              AND hoursBucketWithTardy.intTardyBreakPoint = hoursBucketWithTardy.TardiesMakingAbsence THEN
                                   ( -hoursBucketWithTardy.ActualRunningAbsentHours + hoursBucketWithTardy.SchedHours )
                                         ELSE hoursBucketWithTardy.ActualRunningAbsentHours
                                    END AS AdjustedRunningAbsentHours
                             FROM   (
                                    SELECT hoursBucket.StuEnrollId
                                          ,hoursBucket.RecordDate
                                          ,hoursBucket.SchedHours
                                          ,hoursBucket.ActualHours
                                          ,hoursBucket.isTardy
                                          ,hoursBucket.Absent
                                          ,SUM(   CASE WHEN hoursBucket.ActualHours <> 9999.00
                                                            AND hoursBucket.ActualHours <> 999 THEN hoursBucket.SchedHours
                                                       ELSE 0
                                                  END
                                              ) OVER ( PARTITION BY hoursBucket.StuEnrollId
                                                       ORDER BY hoursBucket.RecordDate
                                                     ) AS ActualRunningScheduledHours
                                          ,SUM(   CASE WHEN (
                                                            hoursBucket.ActualHours > 0
                                                            AND hoursBucket.ActualHours > hoursBucket.SchedHours
                                                            AND hoursBucket.ActualHours <> 9999.00
                                                            ) THEN ( hoursBucket.ActualHours - ( hoursBucket.ActualHours - hoursBucket.SchedHours ))
                                                       WHEN hoursBucket.ActualHours <> 9999.00 THEN hoursBucket.ActualHours
                                                       ELSE 0
                                                  END
                                              ) OVER ( PARTITION BY hoursBucket.StuEnrollId
                                                       ORDER BY hoursBucket.RecordDate
                                                     ) AS ActualRunningPresentHours
                                          ,SUM(   hoursBucket.Absent
                                                  + CASE WHEN hoursBucket.ActualHours > 0
                                                              AND hoursBucket.ActualHours < hoursBucket.SchedHours THEN
                                                             hoursBucket.SchedHours - hoursBucket.ActualHours
                                                         ELSE 0
                                                    END
                                              ) OVER ( PARTITION BY hoursBucket.StuEnrollId
                                                       ORDER BY hoursBucket.RecordDate
                                                     ) AS ActualRunningAbsentHours
                                          ,SUM(   CASE WHEN hoursBucket.ActualHours > 0
                                                            AND hoursBucket.ActualHours > hoursBucket.SchedHours
                                                            AND hoursBucket.ActualHours <> 9999.00 THEN hoursBucket.ActualHours - hoursBucket.SchedHours
                                                       ELSE 0
                                                  END
                                              ) OVER ( PARTITION BY hoursBucket.StuEnrollId
                                                       ORDER BY hoursBucket.RecordDate
                                                     ) AS ActualRunningMakeupHours
                                          ,SUM(   CASE WHEN hoursBucket.ActualHours > 0
                                                            AND hoursBucket.ActualHours < hoursBucket.SchedHours
                                                            AND hoursBucket.isTardy = 1 THEN hoursBucket.SchedHours - hoursBucket.ActualHours
                                                       ELSE 0
                                                  END
                                              ) OVER ( PARTITION BY hoursBucket.StuEnrollId
                                                       ORDER BY hoursBucket.RecordDate
                                                     ) AS ActualRunningTardyHours
                                          ,SUM(   CASE WHEN hoursBucket.TrackTardies = 1
                                                            AND hoursBucket.isTardy = 1 THEN 1
                                                       ELSE 0
                                                  END
                                              ) OVER ( PARTITION BY hoursBucket.StuEnrollId
                                                       ORDER BY hoursBucket.RecordDate
                                                     ) AS intTardyBreakPoint
                                          ,hoursBucket.TrackTardies
                                          ,hoursBucket.TardiesMakingAbsence
                                          ,hoursBucket.RowNumber
                                    FROM   (
                                           SELECT *
                                                 ,ROW_NUMBER() OVER ( ORDER BY dt.RecordDate ) AS RowNumber
                                           FROM   (
                                                  SELECT     t1.StuEnrollId
                                                            ,t1.RecordDate
                                                            ,t1.SchedHours
                                                            ,t1.ActualHours
                                                            ,CASE WHEN (
                                                                       (
                                                                       (
                                                                       (
                                                                       AAUT.UnitTypeDescrip = 'Present Absent'
                                                                       AND t1.SchedHours >= 1
                                                                       )
                                                                       OR (
                                                                          AAUT.UnitTypeDescrip = 'Clock Hours'
                                                                          AND t1.SchedHours > 0
                                                                          )
                                                                       )
                                                                       AND t1.SchedHours NOT IN ( 999, 9999 )
                                                                       )
                                                                       AND t1.ActualHours = 0
                                                                       ) THEN t1.SchedHours
                                                                  ELSE 0
                                                             END AS Absent
                                                            ,t1.isTardy
                                                            ,t3.TrackTardies
                                                            ,t3.TardiesMakingAbsence
                                                  FROM       arStudentClockAttendance t1
                                                  INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                                                  INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                                                  INNER JOIN arAttUnitType AS AAUT ON AAUT.UnitTypeId = t3.UnitTypeId
                                                  INNER JOIN @MyEnrollments ON t1.StuEnrollId = [@MyEnrollments].StuEnrollId
                                                  WHERE --t3.UnitTypeId IN ( 'ef5535c2-142c-4223-ae3c-25a50a153cc6','B937C92E-FD7A-455E-A731-527A9918C734' ) -- UnitTypeDescrip = ('Present Absent', 'Clock Hours') -
                                                             AAUT.UnitTypeDescrip IN ( 'Present Absent', 'Clock Hours' )
                                                             AND t1.ActualHours <> 9999.00
                                                  ) dt
                                           --ORDER BY StuEnrollId
                                           --        ,MeetDate
                                           ) hoursBucket
                                    ) hoursBucketWithTardy
                             ) hoursBucketAll
                    ORDER BY hoursBucketAll.StuEnrollId
                            ,hoursBucketAll.RecordDate;


        DELETE FROM dbo.syStudentAttendanceSummary
        WHERE StuEnrollId IN (
                             SELECT DISTINCT StuEnrollId
                             FROM   @HourTotals2
                             );

        INSERT INTO syStudentAttendanceSummary (
                                               StuEnrollId
                                              ,ClsSectionId
                                              ,StudentAttendedDate
                                              ,ScheduledDays
                                              ,ActualDays
                                              ,ActualRunningScheduledDays
                                              ,ActualRunningPresentDays
                                              ,ActualRunningAbsentDays
                                              ,ActualRunningMakeupDays
                                              ,ActualRunningTardyDays
                                              ,AdjustedPresentDays
                                              ,AdjustedAbsentDays
                                              ,AttendanceTrackType
                                              ,ModUser
                                              ,ModDate
                                              ,IsExcused
                                              ,ActualRunningExcusedDays
                                              ,IsTardy
                                               )
                    SELECT StuEnrollId
                          ,NULL
                          ,RecordDate
                          ,ScheduledHours
                          ,ActualHours
                          ,ActualRunningScheduledDays
                          ,ActualRunningPresentHours
                          ,ActualRunningAbsentHours
                          ,ISNULL(ActualRunningMakeupHours, 0)
                          ,ActualRunningTardyHours
                          ,AdjustedRunningPresentHours
                          ,AdjustedRunningAbsentHours
                          ,'Post Attendance by Class'
                          ,'sa'
                          ,GETDATE()
                          ,0
                          ,NULL
                          ,Tardy
                    FROM   @HourTotals2;


    END;


--=================================================================================================
-- END  --  USP_AT_Step02_InsertAttendance_Day_PresentAbsent
--=================================================================================================
GO
