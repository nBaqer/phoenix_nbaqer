SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[USP_AR_GetCoreqClsSects]
    (
     @ReqID UNIQUEIDENTIFIER
    ,@TermID UNIQUEIDENTIFIER
    )
AS /*----------------------------------------------------------------------------------------------------
	Author          :	Saraswathi Lakshmanan
    
    Create date		:	02/03/2010
    
	Procedure Name	:	USP_AR_GetCoreqClsSects

	Objective		:	get the Class Sections for the given ReqId and TermID
	
	Parameters		:	Name			Type	Data Type			Required? 	
						=====			====	=========			=========	
						@ReqID			In		UniqueIdentifier	Required
						@TermID			In		UniqueIdentifier	Required
						
	Output			:	Returns the ClsSections				
						
*/-----------------------------------------------------------------------------------------------------

    BEGIN
	
        SELECT  ClsSectionId
               ,ClsSection
        FROM    arClassSections C
        INNER JOIN arCourseReqs R ON C.ReqId = R.PreCoReqID
        WHERE   R.ReqId = @ReqID
                AND TermId = @TermID
        GROUP BY ClsSectionId
               ,ClsSection; 
    END;



GO
