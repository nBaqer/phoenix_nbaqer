SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[USP_GetDirectChildrenForCETranscript]
    (
     @stuEnrollid UNIQUEIDENTIFIER
    )
AS
    SET NOCOUNT ON;
    SELECT       
		 DISTINCT
            C.ReqId
           ,R.Descrip AS Req
           ,R.Code
           ,R.Credits
           ,R.Hours
           ,R.ReqTypeId
           ,0 AS DefCredits
           ,0 AS ProgCredits
           ,0 AS ProgHours
           ,PV.PrgVerDescrip
           ,PV.IsContinuingEd
           ,PV.PrgVerId
           ,R.FinAidCredits
           ,E.StuEnrollId
           ,R.Hours AS ScheduledHours
    FROM    arResults T
    INNER JOIN arClassSections C ON T.TestId = C.ClsSectionId
                                    AND T.StuEnrollId = @stuEnrollid
    INNER JOIN arReqs R ON C.ReqId = R.ReqId
    INNER JOIN arStuEnrollments E ON E.StuEnrollId = T.StuEnrollId
    INNER JOIN arPrgVersions PV ON PV.PrgVerId = E.PrgVerId
    WHERE   R.ReqId NOT IN ( SELECT DISTINCT
                                    ReqId
                             FROM   arTransferGrades
                             WHERE  StuEnrollId = @stuEnrollid )
    UNION ALL
    SELECT      
		DISTINCT
            G.ReqId
           ,R.Descrip AS Req
           ,R.Code
           ,R.Credits
           ,R.Hours
           ,R.ReqTypeId
           ,0 AS DefCredits
           ,0 AS ProgCredits
           ,0 AS ProgHours
           ,PV.PrgVerDescrip
           ,PV.IsContinuingEd
           ,PV.PrgVerId
           ,R.FinAidCredits
           ,E.StuEnrollId
		/** New code added by kamalesh ahuja on march 08 2010 **/
           ,R.Hours AS ScheduledHours
		/**/
    FROM    arTransferGrades G
    INNER JOIN arReqs R ON G.ReqId = R.ReqId
    INNER JOIN arStuEnrollments E ON E.StuEnrollId = G.StuEnrollId
    INNER JOIN arPrgVersions PV ON PV.PrgVerId = E.PrgVerId
    WHERE   G.StuEnrollId = @stuEnrollid;     




GO
