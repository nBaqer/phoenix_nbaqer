SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[usp_EdExpress_PostPendingDisbursement]
    @StudentAwardID VARCHAR(50)
   ,@AwardScheduleID VARCHAR(50)
   ,@StuEnrollmentID VARCHAR(50)
   ,@CampusID VARCHAR(50)
   ,@ExpectedDate DATETIME
   ,@Amount DECIMAL(19,4)
   ,@moduser VARCHAR(50)
   ,@moddate DATETIME
   ,@recid VARCHAR(50)
   ,@GrantType VARCHAR(50)
   ,@AcademicYearId VARCHAR(50)
   ,@IsPostPayment BIT
   ,@IsInSchool BIT
   ,@IsPayOutOfSchool BIT
   ,@IsUseExpDate BIT
   ,@SpecifiedDate DATETIME
   ,@PaymentType INT
   ,@ParentId VARCHAR(50)
   ,@DetailId VARCHAR(50)
AS
    BEGIN
    
        DECLARE @TransactionId VARCHAR(50);
        DECLARE @Reference VARCHAR(100);
        DECLARE @TransDesc VARCHAR(100);
        DECLARE @PmtDisbRelId VARCHAR(50);
        DECLARE @CountPosted INT;
        DECLARE @AmountOld DECIMAL(19,4);
        DECLARE @DisbDateOld DATETIME;
        DECLARE @CountDetails INT;
    
        --if @IsPostPayment = 1 and @IsInSchool=1
        IF @IsPostPayment = 1
            AND (
                  @IsInSchool = 1
                  OR @IsPayOutOfSchool = 1
                )
            BEGIN
                SELECT  @TransactionId = TransactionId
                       ,@AmountOld = Amount
                       ,@DisbDateOld = ExpectedDate
                FROM    faStudentAwardSchedule
                WHERE   AwardScheduleId = @AwardScheduleID;
                SELECT  @CountPosted = COUNT(*)
                FROM    saPmtDisbRel PDR
                       ,saTransactions T
                WHERE   PDR.TransactionId = T.TransactionId
                        AND T.Voided = 0
                        AND AwardScheduleId = @AwardScheduleID;
                IF @CountPosted > 0
                    BEGIN
                        IF (
                             @AmountOld <> @Amount
                             OR @DisbDateOld <> @ExpectedDate
                           )
                            AND @Amount > 0
                            BEGIN
                
                                IF @DisbDateOld <> @ExpectedDate
                                    BEGIN
                                        UPDATE  faStudentAwardSchedule
                                        SET     Amount = @Amount
                                               ,ExpectedDate = @ExpectedDate
                                               ,recid = @recid
                                        WHERE   AwardScheduleId = @AwardScheduleID;
                                    END;
                        
                                IF @AmountOld <> @Amount
                                    BEGIN
                        --Update saTransactions set Voided=1 where TransactionId=@TransactionId
                                        UPDATE  faStudentAwardSchedule
                                        SET     Amount = @Amount
                                               ,ExpectedDate = @ExpectedDate
                                               ,recid = @recid
                                        WHERE   AwardScheduleId = @AwardScheduleID;
                                        SET @TransactionId = NEWID();
                                        SET @TransDesc = 'EdExpress - ' + @GrantType;
                        --if @IsUseExpDate=1
                        --begin
                                        SET @Reference = 'EdExpress - ' + CONVERT(VARCHAR,@ExpectedDate,101) + ' ' + @recid;
                        --end
                        --else
                        --begin
                        -- set @Reference='EdExpress - ' + CONVERT(varchar, @SpecifiedDate,101) + ' '+ @recid
                        --end
                        --if @IsUseExpDate=1
                        --begin
                        -- INSERT INTO saTransactions(TransactionID,StuEnrollID,CampusId,TransDate,TransAmount,CreateDate,TransReference,TransDescrip,moduser,moddate,TransTypeID,IsPosted,IsAutomatic,AcademicYearId)
                        -- VALUES(@TransactionId,@StuEnrollmentID,@CampusID,@ExpectedDate,-1*@Amount,GETDATE(),@Reference,@TransDesc,@moduser,@moddate,2,1,0,@AcademicYearId)
                        --end
                        --else
                        --begin
                                        INSERT  INTO saTransactions
                                                (
                                                 TransactionID
                                                ,StuEnrollID
                                                ,CampusId
                                                ,TransDate
                                                ,TransAmount
                                                ,CreateDate
                                                ,TransReference
                                                ,TransDescrip
                                                ,moduser
                                                ,moddate
                                                ,TransTypeID
                                                ,IsPosted
                                                ,IsAutomatic
                                                ,AcademicYearId
                                                )
                                        VALUES  (
                                                 @TransactionId
                                                ,@StuEnrollmentID
                                                ,@CampusID
                                                ,@SpecifiedDate
                                                ,-1 * @Amount
                                                ,GETDATE()
                                                ,@Reference
                                                ,@TransDesc
                                                ,@moduser
                                                ,@moddate
                                                ,2
                                                ,1
                                                ,0
                                                ,@AcademicYearId
                                                );
                        --end
                        
                                        SET @PmtDisbRelId = NEWID();
                                        UPDATE  faStudentAwardSchedule
                                        SET     TransactionId = @TransactionId
                                        WHERE   AwardScheduleID = @AwardScheduleID;
                        
                                        INSERT  INTO saPmtDisbRel
                                                (
                                                 PmtDisbRelID
                                                ,TransactionID
                                                ,Amount
                                                ,StuAwardID
                                                ,AwardScheduleId
                                                ,moduser
                                                ,moddate
                                                )
                                        VALUES  (
                                                 @PmtDisbRelId
                                                ,@TransactionId
                                                ,@Amount
                                                ,@StudentAwardID
                                                ,@AwardScheduleID
                                                ,@moduser
                                                ,@moddate
                                                );
                        
                                        INSERT  INTO saPayments
                                                (
                                                 TransactionID
                                                ,CheckNumber
                                                ,ScheduledPayment
                                                ,PaymentTypeID
                                                ,IsDeposited
                                                ,moduser
                                                ,moddate
                                                )
                                        VALUES  (
                                                 @TransactionId
                                                ,NULL
                                                ,0
                                                ,@PaymentType
                                                ,0
                                                ,@moduser
                                                ,@moddate
                                                );
                                    END;
                            END;
                        ELSE
                            BEGIN
                                IF @Amount <= 0
                                    BEGIN
                        --Delete From saPmtDisbRel Where AwardScheduleID=@AwardScheduleId
                        --Delete From saPayments Where TransactionId=@TransactionId
                        --Delete From saTransactions Where TransactionId=@TransactionId
                        --Delete from faStudentAwardSchedule Where AwardScheduleID=@AwardScheduleId
                                        UPDATE  faStudentAwardSchedule
                                        SET     Amount = @Amount
                                        WHERE   AwardScheduleID = @AwardScheduleID;
                                    END;
                            END;
                
                    END;
                ELSE
                    BEGIN
                        IF @Amount > 0
                            BEGIN
                                SET @TransactionId = NEWID();
                                SET @TransDesc = 'EdExpress - ' + @GrantType;
                    --if @IsUseExpDate=1
                    --begin
                                SET @Reference = 'EdExpress - ' + CONVERT(VARCHAR,@ExpectedDate,101) + ' ' + @recid;
                    --end
                    --else
                    --begin
                    -- set @Reference='EdExpress - ' + CONVERT(varchar, @SpecifiedDate,101) + ' '+ @recid
                    --end
                    --if @IsUseExpDate=1
                    --begin
                    -- INSERT INTO saTransactions(TransactionID,StuEnrollID,CampusId,TransDate,TransAmount,CreateDate,TransReference,TransDescrip,moduser,moddate,TransTypeID,IsPosted,IsAutomatic,AcademicYearId)
                    -- VALUES(@TransactionId,@StuEnrollmentID,@CampusID,@ExpectedDate,-1*@Amount,GETDATE(),@Reference,@TransDesc,@moduser,@moddate,2,1,0,@AcademicYearId)
                    --end
                    --else
                    --begin
                                INSERT  INTO saTransactions
                                        (
                                         TransactionID
                                        ,StuEnrollID
                                        ,CampusId
                                        ,TransDate
                                        ,TransAmount
                                        ,CreateDate
                                        ,TransReference
                                        ,TransDescrip
                                        ,moduser
                                        ,moddate
                                        ,TransTypeID
                                        ,IsPosted
                                        ,IsAutomatic
                                        ,AcademicYearId
                                        )
                                VALUES  (
                                         @TransactionId
                                        ,@StuEnrollmentID
                                        ,@CampusID
                                        ,@SpecifiedDate
                                        ,-1 * @Amount
                                        ,GETDATE()
                                        ,@Reference
                                        ,@TransDesc
                                        ,@moduser
                                        ,@moddate
                                        ,2
                                        ,1
                                        ,0
                                        ,@AcademicYearId
                                        );
                    --end
                    
                                SET @PmtDisbRelId = NEWID();
                                UPDATE  faStudentAwardSchedule
                                SET     TransactionId = @TransactionId
                                WHERE   AwardScheduleID = @AwardScheduleID;
                    
                                INSERT  INTO saPmtDisbRel
                                        (
                                         PmtDisbRelID
                                        ,TransactionID
                                        ,Amount
                                        ,StuAwardID
                                        ,AwardScheduleId
                                        ,moduser
                                        ,moddate
                                        )
                                VALUES  (
                                         @PmtDisbRelId
                                        ,@TransactionId
                                        ,@Amount
                                        ,@StudentAwardID
                                        ,@AwardScheduleID
                                        ,@moduser
                                        ,@moddate
                                        );
                    
                                INSERT  INTO saPayments
                                        (
                                         TransactionID
                                        ,CheckNumber
                                        ,ScheduledPayment
                                        ,PaymentTypeID
                                        ,IsDeposited
                                        ,moduser
                                        ,moddate
                                        )
                                VALUES  (
                                         @TransactionId
                                        ,NULL
                                        ,0
                                        ,@PaymentType
                                        ,0
                                        ,@moduser
                                        ,@moddate
                                        );
                            END;
                        ELSE
                            BEGIN
                    --Delete From saPmtDisbRel Where AwardScheduleID=@AwardScheduleId
                    --Delete From saPayments Where TransactionId=@TransactionId
                    --Delete From saTransactions Where TransactionId=@TransactionId
                    --Delete from faStudentAwardSchedule Where AwardScheduleID=@AwardScheduleId
                                UPDATE  faStudentAwardSchedule
                                SET     Amount = @Amount
                                WHERE   AwardScheduleID = @AwardScheduleID;
                            END;
                    END;
            END;
        DELETE  FROM syEDExpNotPostPell_ACG_SMART_Teach_DisbursementTable
        WHERE   DetailId = @DetailId;
        SELECT  @CountDetails = COUNT(*)
        FROM    syEDExpNotPostPell_ACG_SMART_Teach_DisbursementTable
        WHERE   ParentId = @ParentId;
        IF @CountDetails = 0
            BEGIN
                DELETE  FROM syEDExpNotPostPell_ACG_SMART_Teach_StudentTable
                WHERE   ParentId = @ParentId;
            END;
        SELECT  @AwardScheduleID;
    END;




GO
