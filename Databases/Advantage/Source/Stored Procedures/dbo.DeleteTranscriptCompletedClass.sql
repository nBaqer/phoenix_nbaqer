SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Ginzo, John
-- Create date: 06/23/2015
-- Description:	Delete a completed or transferred 
--				class in transcript
-- =============================================
CREATE PROCEDURE [dbo].[DeleteTranscriptCompletedClass]
    @ResultIdOrTransferId VARCHAR(50)
   ,@TestId VARCHAR(50) = NULL
   ,@StuEnrollId VARCHAR(50)
AS
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;    
        DECLARE @ReqId UNIQUEIDENTIFIER;
        DECLARE @TermId UNIQUEIDENTIFIER;	
        DECLARE @StuEnrollIdTable AS TABLE
            (
             StuEnrollId UNIQUEIDENTIFIER
            );
        
        INSERT  INTO @StuEnrollIdTable
                (
                 StuEnrollId
                )
                SELECT  ASE.StuEnrollId
                FROM    arStuEnrollments AS ASE
                INNER JOIN arStuEnrollments AS ASE2 ON ASE2.StudentId = ASE.StudentId
                WHERE   ASE2.StuEnrollId = @StuEnrollId; 

        IF @TestId IS NULL
            BEGIN
                SELECT  @ReqId = ATG.ReqId
                       ,@TermId = ATG.TermId
                FROM    arTransferGrades AS ATG
                WHERE   ATG.TransferId = @ResultIdOrTransferId;

                BEGIN TRANSACTION DeleteTranscriptCompletedClass;

                BEGIN TRY
                    DELETE  FROM dbo.arTransferGrades
                    WHERE   Reqid = @ReqId
                            AND TermId = @TermId
                            AND StuEnrollId IN ( SELECT SEIT.StuEnrollId
                                                 FROM   @StuEnrollIdTable AS SEIT );

                    DELETE  FROM syCreditSummary
                    WHERE   TermId = @TermId
                            AND ReqId = @ReqId
                            AND StuEnrollId IN ( SELECT SEIT.StuEnrollId
                                                 FROM   @StuEnrollIdTable AS SEIT );

                END TRY
                BEGIN CATCH
                    ROLLBACK TRANSACTION DeleteTranscriptCompletedClass;				
                END CATCH;

                IF @@TranCount > 0
                    COMMIT TRANSACTION DeleteTranscriptCompletedClass;
                             		
            END;
        ELSE
            BEGIN
                SELECT  @ReqId = ACS.ReqId
                       ,@TermId = ACS.TermId
                FROM    dbo.arClassSections AS ACS
                WHERE   ACS.ClsSectionId = @TestId;

                BEGIN TRANSACTION DeleteTranscriptCompletedClass;

                BEGIN TRY
                    DELETE  FROM dbo.arResults
                    WHERE   TestId = @TestId
                            AND StuEnrollId IN ( SELECT SEIT.StuEnrollId
                                                 FROM   @StuEnrollIdTable AS SEIT );
												 
                    DELETE  FROM dbo.arGrdBkResults
                    WHERE   ClsSectionId = @TestId
                            AND StuEnrollId IN ( SELECT SEIT.StuEnrollId
                                                 FROM   @StuEnrollIdTable AS SEIT );

                    DELETE  FROM dbo.atClsSectAttendance
                    WHERE   ClsSectionId = @TestId
                            AND StuEnrollId IN ( SELECT SEIT.StuEnrollId
                                                 FROM   @StuEnrollIdTable AS SEIT );

                    DELETE  FROM syCreditSummary
                    WHERE   TermId = @TermId
                            AND ReqId = @ReqId
                            AND ClsSectionId = @TestId
                            AND StuEnrollId IN ( SELECT SEIT.StuEnrollId
                                                 FROM   @StuEnrollIdTable AS SEIT );
                      
                    DELETE  FROM syStudentAttendanceSummary
                    WHERE   ClsSectionId = @TestId
                            AND StuEnrollId IN ( SELECT SEIT.StuEnrollId
                                                 FROM   @StuEnrollIdTable AS SEIT );

                END TRY
                BEGIN CATCH
                    ROLLBACK TRANSACTION DeleteTranscriptCompletedClass;				
                END CATCH;

                IF @@TranCount > 0
                    COMMIT TRANSACTION  DeleteTranscriptCompletedClass;

            END;
    END;
-- =========================================================================================================
-- END  --  DeleteTranscriptCompletedClass
-- =========================================================================================================


GO
