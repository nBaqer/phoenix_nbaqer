SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_GetStudentByLead]
    @LeadId UNIQUEIDENTIFIER
AS
    SELECT TOP 1
            StudentId
           ,CampusId
           ,StuEnrollId
    FROM    arStuEnrollments
    WHERE   LeadId = @LeadId
            AND LeadId IS NOT NULL;



GO
