SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


--=================================================================================================
-- USP_GetAbsentToday
--=================================================================================================
-- =============================================
-- Author:		Kimberly 
-- Create date: 05/13/2019
-- Description: Get List of students that are Absent Today
--             
--                 
-- =============================================
CREATE PROCEDURE [dbo].[USP_GetAbsentToday]
    @DateRun DATE,
    @CampusId UNIQUEIDENTIFIER
AS
BEGIN

    DECLARE @isTimeClock BIT;
    SET @isTimeClock = 1;
    IF EXISTS
    (
        SELECT *
        FROM dbo.arPrgVersions prg
            JOIN dbo.arStuEnrollments ste
                ON ste.PrgVerId = prg.PrgVerId
        WHERE ste.CampusId = @CampusId
              AND UseTimeClock = 0
    )
    BEGIN
        SET @isTimeClock = 0;
    END;


    IF (@isTimeClock = 0)
    BEGIN
        
        SELECT DISTINCT
               LTRIM(RTRIM(cp.CampDescrip)) AS CampDescrip,
               l.StudentNumber,
               (FirstName + ' ' + LastName) AS FullName,
               FirstName,
               LastName,
               (
                   SELECT STUFF(STUFF(STUFF(
                                      (
                                          SELECT TOP (1)
                                                 Phone
                                          FROM adLeadPhone
                                          WHERE LeadId = l.LeadId
                                          ORDER BY IsBest DESC,
                                                   IsShowOnLeadPage DESC,
                                                   Position DESC
                                      ),
                                      1,
                                      0,
                                      '('
                                           ),
                                      5,
                                      0,
                                      ') '
                                     ),
                                10,
                                0,
                                '-'
                               )
               ) AS Phone,
               sc.StatusCodeDescrip,
               (
                   SELECT TOP (1)
                          CONVERT(DATE, RecordDate)
                   FROM arStudentClockAttendance
                   WHERE SchedHours > 0
                         AND ActualHours > 0
                         AND StuEnrollId = e.StuEnrollId
						 AND ActualHours NOT IN ( 999,9999,99999 )
                   ORDER BY RecordDate DESC
               ) AS LDA
        FROM dbo.arStuEnrollments e
            JOIN syStatusCodes sc
                ON sc.StatusCodeId = e.StatusCodeId
            JOIN dbo.arStudentSchedules ss
                ON ss.StuEnrollId = e.StuEnrollId
            JOIN dbo.arProgSchedules ps
                ON ps.ScheduleId = ss.ScheduleId
            JOIN dbo.arProgScheduleDetails psd
                ON psd.ScheduleId = ss.ScheduleId
            JOIN arStudentClockAttendance sca
                ON sca.StuEnrollId = e.StuEnrollId
            JOIN adLeads l
                ON l.LeadId = e.LeadId
            JOIN dbo.adLeadPhone lp
                ON lp.LeadId = l.LeadId
            JOIN dbo.syCampuses cp
                ON cp.CampusId = e.CampusId
        WHERE e.ExpStartDate <= @DateRun
              AND e.ExpGradDate >= @DateRun
              AND cp.CampusId = @CampusId
              AND e.StuEnrollId IN
                  (
                      SELECT DISTINCT
                             sc.StuEnrollId
                      FROM arStudentClockAttendance sc
                          JOIN dbo.arStuEnrollments e
                              ON e.StuEnrollId = sc.StuEnrollId
                      WHERE CONVERT(DATE, sc.RecordDate) = @DateRun
                            AND sc.ActualHours = 0
                            AND sc.SchedHours > 0
							AND sc.ActualHours NOT IN ( 999,9999,99999 )
                           
                  );
        RETURN;

    END;

    IF (@isTimeClock = 1)
    BEGIN

        DECLARE @dayOfWeek INT;
        SET @dayOfWeek =
        (
            SELECT CASE DATENAME(WEEKDAY, @DateRun)
                       WHEN 'Monday' THEN
                           1
                       WHEN 'Tuesday' THEN
                           2
                       WHEN 'Wednesday' THEN
                           3
                       WHEN 'Thursday' THEN
                           4
                       WHEN 'Friday' THEN
                           5
                       WHEN 'Saturday' THEN
                           6
                       WHEN 'Sunday' THEN
                           7
                   END AS wDay
        );

        SELECT DISTINCT
               LTRIM(RTRIM(cp.CampDescrip)) AS CampDescrip,
               l.StudentNumber,
               (FirstName + ' ' + LastName) AS FullName,
               FirstName,
               LastName,
               (
                   SELECT STUFF(STUFF(STUFF(
                                      (
                                          SELECT TOP (1)
                                                 Phone
                                          FROM adLeadPhone
                                          WHERE LeadId = l.LeadId
                                          ORDER BY IsBest DESC,
                                                   IsShowOnLeadPage DESC,
                                                   Position DESC
                                      ),
                                      1,
                                      0,
                                      '('
                                           ),
                                      5,
                                      0,
                                      ') '
                                     ),
                                10,
                                0,
                                '-'
                               )
               ) AS Phone,
               sc.StatusCodeDescrip,
               (
                   SELECT TOP (1)
                          CONVERT(DATE, RecordDate)
                   FROM arStudentClockAttendance
                   WHERE SchedHours > 0
                         AND ActualHours > 0
                         AND StuEnrollId = e.StuEnrollId
						 AND ActualHours NOT IN ( 999,9999,99999 )
                   ORDER BY RecordDate DESC
               ) AS LDA
        FROM dbo.arStuEnrollments e
            JOIN syStatusCodes sc
                ON sc.StatusCodeId = e.StatusCodeId
            JOIN dbo.arStudentSchedules ss
                ON ss.StuEnrollId = e.StuEnrollId
            JOIN dbo.arProgSchedules ps
                ON ps.ScheduleId = ss.ScheduleId
            JOIN dbo.arProgScheduleDetails psd
                ON psd.ScheduleId = ss.ScheduleId
            JOIN arStudentTimeClockPunches p
                ON p.StuEnrollId = e.StuEnrollId
            JOIN adLeads l
                ON l.LeadId = e.LeadId
            JOIN dbo.adLeadPhone lp
                ON lp.LeadId = l.LeadId
            JOIN dbo.syCampuses cp
                ON cp.CampusId = e.CampusId
        WHERE e.ExpStartDate <= @DateRun
              AND e.ExpGradDate >= @DateRun
              AND psd.dw = @dayOfWeek
              AND psd.total > 0
              AND p.Status = 1
              AND e.CampusId = @CampusId
              AND e.StuEnrollId NOT IN
                  (
                      SELECT DISTINCT
                             sp.StuEnrollId
                      FROM arStudentTimeClockPunches sp
                          JOIN dbo.arStuEnrollments e
                              ON e.StuEnrollId = sp.StuEnrollId
                      WHERE CONVERT(DATE, PunchTime) = @DateRun
                  )
              AND SysStatusId IN ( 9, 20, 6 )
        ORDER BY l.LastName,
                 l.FirstName;
        RETURN;
    END;


END;
--=================================================================================================
-- END  --  USP_GetAbsentToday
--=================================================================================================


GO
