SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_ApplicantLedger_ByLead_Reverse_GetList]
    @CampusId UNIQUEIDENTIFIER
   ,@TransStartDate DATE
   ,@TransEndDate DATE
   ,@LeadId UNIQUEIDENTIFIER
AS
    BEGIN
        SELECT  LT.TransactionId
               ,LT.LeadId
               ,(
                  SELECT    L.Firstname + ' ' + L.LastName
                ) AS LeadName
               ,LT.TransCodeId
               ,LT.IsEnrolled
               ,(
                  SELECT DISTINCT
                            TransCodeDescrip
                  FROM      saTransCodes
                  WHERE     TransCodeId = LT.TransCodeId
                ) AS TransactionCode
               ,LT.TransDescrip AS TransDescription
               ,
			--Case when Voided=1 Then 'Payment (Voided)' else   
			--(select Distinct Description from saTransTypes where TransTypeId=LT.TransTypeId) end as TransactionType,  
                CASE WHEN transTypeId = 1
                          AND TransAmount < 0 THEN 'Charge (Adjusted)'
                     ELSE CASE WHEN transTypeId = 1
                                    AND TransAmount > 0 THEN 'Payment (Adjusted)'
                               ELSE (
                                      SELECT DISTINCT
                                                Description
                                      FROM      saTransTypes
                                      WHERE     TransTypeId = LT.TransTypeId
                                    )
                          END
                END AS TransactionType
               ,LT.ModUser AS Postedby
               ,LT.TransAmount AS TransactionAmount
               ,CONVERT(CHAR(10),LT.CreatedDate,101) AS TransDate
               ,TransReference
               ,(
                  SELECT TOP 1
                            CheckNumber
                  FROM      adLeadPayments
                  WHERE     TransactionId = LT.TransactionId
                ) AS DocumentId
               ,LT.Voided AS Voided
               ,LT.ReversalReason
        FROM    adLeadTransactions LT
        INNER JOIN adLeads L ON LT.LeadId = L.LeadId
        WHERE   L.CampusId = @CampusId
                AND L.LeadId = @LeadId
                AND LT.TransTypeId IN ( 1 )
                AND (
                      CONVERT(CHAR(10),LT.CreatedDate,101) >= @TransStartDate
                      AND CONVERT(CHAR(10),LT.CreatedDate,101) <= @TransEndDate
                    )
                AND LT.Voided = 0
                AND (
                      transTypeId = 1
                      AND TransAmount > 0
                    ) -- show only payments
			--and (LT.ReversalReason is not null or LT.TransDescrip like '%- Adjustment')
        ORDER BY LT.DisplaySequence
               ,LT.SecondDisplaySequence
               ,TransTypeId;
    END;



GO
