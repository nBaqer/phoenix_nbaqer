SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ginzo, John
-- Create date: 02/19/2015
-- Description:	Copy program version
-- =============================================
CREATE PROCEDURE [dbo].[CopyProgramVersion]
    @prVersion VARCHAR(100)
   ,@Code VARCHAR(50)
   ,@Description VARCHAR(50)
   ,@CampGrp VARCHAR(100)
   ,@user VARCHAR(50)
   ,@NewPrgVerId UNIQUEIDENTIFIER OUTPUT
AS
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;
        BEGIN TRAN COPYPROGRAM;

        SET @NewPrgVerId = NEWID();

        INSERT  INTO arPrgVersions
                (
                 PrgVerId
                ,PrgVerCode
                ,PrgVerDescrip
                ,CampGrpId
                ,ModUser
                ,ModDate
                ,ProgId
                ,StatusId
                ,PrgGrpId
                ,DegreeId
                ,SAPId
                ,ThGrdScaleId
                ,TestingModelId
                ,Weeks
                ,Terms
                ,Hours
                ,Credits
                ,LTHalfTime
                ,HalfTime
                ,ThreeQuartTime
                ,FullTime
                ,DeptId
                ,GrdSystemId
                ,[Weighted GPA]
                ,BillingMethodId
                ,TuitionEarningId
                ,AttendanceLevel
                ,CustomAttendance
                ,ProgTypId
                ,IsContinuingEd
                ,UnitTypeId
                ,TrackTardies
                ,TardiesMakingAbsence
                ,SchedMethodId
                ,UseTimeClock
                ,TotalCost
                ,AcademicYearLength
                ,AcademicYearWeeks
                ,PayPeriodPerAcYear
                ,FASAPId
                ,IsBillingMethodCharged
                )
                SELECT  @NewPrgVerId
                       ,@Code
                       ,@Description
                       ,CAST(@CampGrp AS UNIQUEIDENTIFIER)
                       ,@user
                       ,GETDATE()
                       ,ProgId
                       ,StatusId
                       ,PrgGrpId
                       ,DegreeId
                       ,SAPId
                       ,ThGrdScaleId
                       ,TestingModelId
                       ,Weeks
                       ,Terms
                       ,Hours
                       ,Credits
                       ,LTHalfTime
                       ,HalfTime
                       ,ThreeQuartTime
                       ,FullTime
                       ,DeptId
                       ,GrdSystemId
                       ,[Weighted GPA]
                       ,NULL
                       ,TuitionEarningId
                       ,AttendanceLevel
                       ,CustomAttendance
                       ,ProgTypId
                       ,IsContinuingEd
                       ,UnitTypeId
                       ,TrackTardies
                       ,TardiesMakingAbsence
                       ,SchedMethodId
                       ,UseTimeClock
                       ,TotalCost
                       ,AcademicYearLength
                       ,AcademicYearWeeks
                       ,PayPeriodPerAcYear
                       ,FASAPId
                       ,IsBillingMethodCharged
                FROM    arPrgVersions
                WHERE   PrgVerId = CAST(@prVersion AS UNIQUEIDENTIFIER);
				
        IF @@ERROR <> 0
            BEGIN
                ROLLBACK TRAN COPYPROGRAM;			
                RAISERROR ('Error inserting program version.', 16, 1);
                RETURN -1;
            END;


        INSERT  INTO arProgVerDef
                (
                 PrgVerId
                ,ReqId
                ,ReqSeq
                ,IsRequired
                ,Cnt
                ,Hours
                ,TrkForCompletion
                ,Credits
                ,GrdSysDetailId
                ,ModUser
                ,ModDate
                ,TermNo
                )
                SELECT  @NewPrgVerId
                       ,ReqId
                       ,ReqSeq
                       ,IsRequired
                       ,Cnt
                       ,Hours
                       ,TrkForCompletion
                       ,Credits
                       ,GrdSysDetailId
                       ,@user
                       ,GETDATE()
                       ,TermNo
                FROM    arProgVerDef
                WHERE   PrgVerId = @prVersion; 


        IF @@ERROR <> 0
            BEGIN
                ROLLBACK TRAN COPYPROGRAM;			
                RAISERROR ('Error inserting program definitiion.', 16, 1);
                RETURN -2;
            END;


        INSERT  INTO saProgramVersionFees
                (
                 PrgVerFeeId
                ,StatusId
                ,PrgVerId
                ,TransCodeId
                ,TuitionCategoryId
                ,Amount
                ,RateScheduleId
                ,UnitId
                ,ModUser
                ,ModDate
                )
                SELECT  NEWID()
                       ,StatusId
                       ,@NewPrgVerId
                       ,TransCodeId
                       ,TuitionCategoryId
                       ,Amount
                       ,RateScheduleId
                       ,UnitId
                       ,@user
                       ,GETDATE()
                FROM    saProgramVersionFees
                WHERE   PrgVerId = @prVersion; 

        IF @@ERROR <> 0
            BEGIN
                ROLLBACK TRAN COPYPROGRAM;			
                RAISERROR ('Error inserting program fees.', 16, 1);
                RETURN -3;
            END;


        INSERT  INTO adPrgVerTestDetails
                (
                 PrgVerId
                ,ReqGrpId
                ,adReqId
                ,MinScore
                ,ModUser
                ,ModDate
                )
                SELECT  @NewPrgVerId
                       ,ReqGrpId
                       ,adReqId
                       ,MinScore
                       ,@user
                       ,GETDATE()
                FROM    adPrgVerTestDetails
                WHERE   PrgVerId = @prVersion; 


        IF @@ERROR <> 0
            BEGIN
                ROLLBACK TRAN COPYPROGRAM;			
                RAISERROR ('Error inserting program test details.', 16, 1);
                RETURN -4;
            END;

        COMMIT TRAN COPYPROGRAM;
    END;



GO
