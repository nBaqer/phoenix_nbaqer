SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_configsettings_getlist]
    @StuEnrollId VARCHAR(50)
AS
    BEGIN  
        DECLARE @CampusId UNIQUEIDENTIFIER;  
      
        SET @CampusId = (
                          SELECT TOP 1
                                    CampusId
                          FROM      arStuEnrollments
                          WHERE     StuEnrollId = @StuEnrollId
                        );  
      
        DECLARE @GradeCourseRepetitionsMethod VARCHAR(50)
           ,@GradeCourseRepetitionsMethodId INT;  
        SET @GradeCourseRepetitionsMethodId = (
                                                SELECT TOP 1
                                                        SettingId
                                                FROM    syConfigAppSettings
                                                WHERE   KeyName = 'GradeCourseRepetitionsMethod'
                                              );  
        IF (
             SELECT COUNT(*)
             FROM   syConfigAppSetValues
             WHERE  SettingId = @GradeCourseRepetitionsMethodId
                    AND CampusId = @CampusId
           ) >= 1
            BEGIN  
                SET @GradeCourseRepetitionsMethod = (
                                                      SELECT TOP 1
                                                                Value
                                                      FROM      dbo.syConfigAppSetValues
                                                      WHERE     SettingId = @GradeCourseRepetitionsMethodId
                                                                AND CampusId = @CampusId
                                                    );  
      
            END;  
        ELSE
            BEGIN  
                SET @GradeCourseRepetitionsMethod = (
                                                      SELECT TOP 1
                                                                Value
                                                      FROM      dbo.syConfigAppSetValues
                                                      WHERE     SettingId = @GradeCourseRepetitionsMethodId
                                                                AND CampusId IS NULL
                                                    );  
            END;  
      
      
      
        DECLARE @IncludeHoursForFailingGrade BIT
           ,@IncludeHoursForFailingGradeId INT;  
        SET @IncludeHoursForFailingGradeId = (
                                               SELECT TOP 1
                                                        SettingId
                                               FROM     syConfigAppSettings
                                               WHERE    KeyName = 'IncludeHoursForFailingGrade'
                                             );  
        IF (
             SELECT COUNT(*)
             FROM   syConfigAppSetValues
             WHERE  SettingId = @IncludeHoursForFailingGradeId
                    AND CampusId = @CampusId
           ) >= 1
            BEGIN  
                SET @IncludeHoursForFailingGrade = (
                                                     SELECT TOP 1
                                                            Value
                                                     FROM   dbo.syConfigAppSetValues
                                                     WHERE  SettingId = @IncludeHoursForFailingGradeId
                                                            AND CampusId = @CampusId
                                                   );  
      
            END;  
        ELSE
            BEGIN  
                SET @IncludeHoursForFailingGrade = (
                                                     SELECT TOP 1
                                                            Value
                                                     FROM   dbo.syConfigAppSetValues
                                                     WHERE  SettingId = @IncludeHoursForFailingGradeId
                                                            AND CampusId IS NULL
                                                   );  
            END;  
      
        DECLARE @GradesFormat VARCHAR(50)
           ,@GradesFormatId INT;  
        SET @GradesFormatId = (
                                SELECT TOP 1
                                        SettingId
                                FROM    syConfigAppSettings
                                WHERE   KeyName = 'GradesFormat'
                              );  
        IF (
             SELECT COUNT(*)
             FROM   syConfigAppSetValues
             WHERE  SettingId = @GradesFormatId
                    AND CampusId = @CampusId
           ) >= 1
            BEGIN  
                SET @GradesFormat = (
                                      SELECT TOP 1
                                                Value
                                      FROM      dbo.syConfigAppSetValues
                                      WHERE     SettingId = @GradesFormatId
                                                AND CampusId = @CampusId
                                    );  
      
            END;  
        ELSE
            BEGIN  
                SET @GradesFormat = (
                                      SELECT TOP 1
                                                Value
                                      FROM      dbo.syConfigAppSetValues
                                      WHERE     SettingId = @GradesFormatId
                                                AND CampusId IS NULL
                                    );  
            END;  
      
        DECLARE @TranscriptType VARCHAR(50)
           ,@TranscriptTypeId INT;  
        SET @TranscriptTypeId = (
                                  SELECT TOP 1
                                            SettingId
                                  FROM      syConfigAppSettings
                                  WHERE     KeyName = 'TranscriptType'
                                );  
        IF (
             SELECT COUNT(*)
             FROM   syConfigAppSetValues
             WHERE  SettingId = @TranscriptTypeId
                    AND CampusId = @CampusId
           ) >= 1
            BEGIN  
                SET @TranscriptType = (
                                        SELECT TOP 1
                                                Value
                                        FROM    dbo.syConfigAppSetValues
                                        WHERE   SettingId = @TranscriptTypeId
                                                AND CampusId = @CampusId
                                      );  
      
            END;  
        ELSE
            BEGIN  
                SET @TranscriptType = (
                                        SELECT TOP 1
                                                Value
                                        FROM    dbo.syConfigAppSetValues
                                        WHERE   SettingId = @TranscriptTypeId
                                                AND CampusId IS NULL
                                      );  
            END;  
      
        DECLARE @addcreditsbyservice VARCHAR(50)
           ,@addcreditsbyserviceId INT;  
        SET @addcreditsbyserviceId = (
                                       SELECT TOP 1
                                                SettingId
                                       FROM     syConfigAppSettings
                                       WHERE    KeyName = 'addcreditsbyservice'
                                     );  
        IF (
             SELECT COUNT(*)
             FROM   syConfigAppSetValues
             WHERE  SettingId = @addcreditsbyserviceId
                    AND CampusId = @CampusId
           ) >= 1
            BEGIN  
                SET @addcreditsbyservice = (
                                             SELECT TOP 1
                                                    Value
                                             FROM   dbo.syConfigAppSetValues
                                             WHERE  SettingId = @addcreditsbyserviceId
                                                    AND CampusId = @CampusId
                                           );  
      
            END;  
        ELSE
            BEGIN  
                SET @addcreditsbyservice = (
                                             SELECT TOP 1
                                                    Value
                                             FROM   dbo.syConfigAppSetValues
                                             WHERE  SettingId = @addcreditsbyserviceId
                                                    AND CampusId IS NULL
                                           );  
            END;  
      
        SELECT  @GradeCourseRepetitionsMethod AS GradeCourseRepetitionsMethod
               ,@IncludeHoursForFailingGrade AS IncludeHoursForFailingGrade
               ,@GradesFormat AS GradesFormat
               ,@TranscriptType AS TranscriptType
               ,@addcreditsbyservice AS AddCreditsByService;  
      
    END;  
      

GO
