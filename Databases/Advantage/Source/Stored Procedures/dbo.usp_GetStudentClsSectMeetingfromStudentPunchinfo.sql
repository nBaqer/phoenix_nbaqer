SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[usp_GetStudentClsSectMeetingfromStudentPunchinfo]
    (
     @Punch DATETIME
    ,@StuEnrollId VARCHAR(50)
    ,@PunchSpecialCode VARCHAR(5)
    ,@CampusID VARCHAR(50)
    )
AS
    BEGIN 

        DECLARE @NumberofClsSections AS INTEGER;

----DECLARE @Punch AS DATETIME
--        SET @Punch = '08/29/2011 12:50:00'
--        SET @StuEnrollId='793A898B-634F-4CDB-8C8C-DD47788A1699'
        DECLARE @punchDate AS DATETIME;
        DECLARE @PunchDay AS VARCHAR(5);
        SET @PunchDay = (
                          SELECT    dbo.udf_DayOfWeek(@Punch)
                        ); 
        SET @punchDate = CONVERT(VARCHAR,@Punch,101);
       
        --DECLARE @punchTime AS DATETIME
        --SET @PunchTime = CONVERT(VARCHAR, @Punch, 108)
        DECLARE @ClsSectionId AS VARCHAR(4000);
        DECLARE @ClsSectMeetingId AS VARCHAR(4000);
        SET @ClsSectMeetingId = 'Exception';

        SET @NumberofClsSections = (
                                     SELECT COUNT(DISTINCT CS.ClsSectionId)
                                     FROM   dbo.arClassSections CS
                                           ,arresults AR
                                     WHERE  CS.ClsSectionId = AR.TestId
                                            AND StuEnrollId = @StuEnrollId
                                            AND CS.StartDate <= @punchDate
                                            AND CS.EndDate >= @punchDate
                                   );
                
        IF @NumberofClsSections = 0
            BEGIN
                SET @ClsSectMeetingId = 'Exception';
            END;
        
        IF @NumberofClsSections = 1
            BEGIN
                SET @ClsSectionId = (
                                      SELECT  DISTINCT
                                                CS.ClsSectionId
                                      FROM      dbo.arClassSections CS
                                               ,arresults AR
                                      WHERE     CS.ClsSectionId = AR.TestId
                                                AND StuEnrollId = @StuEnrollId
                                                AND CS.StartDate <= @punchDate
                                                AND CS.EndDate >= @punchDate
                                    );
                
                SET @ClsSectMeetingId = (
                                          SELECT    dbo.udf_GetStudentClsSectMeetingfromClsSectandPunchinfo(@Punch,@PunchSpecialCode,@ClsSectionId,@CampusID)
                                        );       
                
        
    
        
            END;       
        IF @NumberofClsSections > 1
            BEGIN 
                SET @ClsSectMeetingId = (
                                          SELECT    dbo.udf_GetStudentClsSectMeetingfromStudentPunchinfo_moreClsSectExists(@Punch,@PunchSpecialCode,@StuEnrollId,
                                                                                                                           @CampusID)
                                        );       
                
            END;
                
        SELECT  @ClsSectMeetingId;        
                
                            
    END;




GO
