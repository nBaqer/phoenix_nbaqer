SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================  
-- Author:	Ginzo, John  
-- Create date: 06/18/2014  
-- Description:	Get Student Enrollments for portal  
-- =============================================  
CREATE PROCEDURE [dbo].[sp_getStuEnrollmentsPortal]
    @StudentNumber VARCHAR(50)
AS
    BEGIN  
	-- SET NOCOUNT ON added to prevent extra result sets from  
	-- interfering with SELECT statements.  
        SET NOCOUNT ON;  
	  
        SELECT	DISTINCT
                E.StuEnrollId
               ,Program.ProgDescrip AS Enrollment
        FROM    arStuEnrollments E
        INNER JOIN arStudent S ON E.StudentId = S.StudentId
        INNER JOIN arPrgVersions V ON E.PrgVerId = V.PrgVerId
        INNER JOIN dbo.arPrograms Program ON V.ProgId = Program.ProgId
        INNER JOIN syStatuses ST ON V.StatusId = ST.StatusId
        INNER JOIN syCampuses C ON E.CampusId = C.CampusId
        INNER JOIN syStatusCodes SC ON E.StatusCodeId = SC.StatusCodeId
        INNER JOIN sySysStatus SS ON SC.SysStatusId = SS.SysStatusId
        WHERE   SS.StatusLevelId = 2
                AND SS.SysStatusId IN ( 7,9,10,11,12,14,19,20,21,22 )
                AND S.StudentNumber = @StudentNumber
                AND E.StartDate <= GETDATE()
        ORDER BY E.StuEnrollId DESC;  
    END;  
	  
GO
