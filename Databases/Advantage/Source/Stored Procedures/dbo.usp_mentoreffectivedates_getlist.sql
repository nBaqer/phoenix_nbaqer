SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_mentoreffectivedates_getlist]
    @ReqId UNIQUEIDENTIFIER
AS
    SELECT  EffectiveDate
           ,MentorRequirement
           ,MentorOperator
    FROM    arMentor_GradeComponentTypes_Courses
    WHERE   ReqId = @ReqId
            AND EffectiveDate = (
                                  SELECT TOP 1
                                            t1.EffectiveDate
                                  FROM      arMentor_GradeComponentTypes_Courses t1
                                           ,arClassSections t2
                                  WHERE     t1.ReqId = t2.ReqId
                                            AND t1.ReqId = @ReqId
                                            AND DATEDIFF(DAY,t1.EffectiveDate,t2.StartDate) >= 0
                                  ORDER BY  DATEDIFF(DAY,t1.EffectiveDate,t2.StartDate) ASC
                                );



GO
