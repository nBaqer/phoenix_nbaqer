SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_GetClockHourProgramVersions]
    @prgVerId VARCHAR(8000)
AS
    DECLARE @cnt INTEGER;
    SET NOCOUNT ON;
    SET @cnt = (
                 SELECT COUNT(*) AS cnt
                 FROM   arPrograms
                       ,syAcademicCalendars
                       ,arPrgVersions
                 WHERE  arPrograms.ACId = syAcademicCalendars.ACId
                        AND LOWER(syAcademicCalendars.ACDescrip) = 'clock hour'
                        AND arPrograms.ProgId = arPrgVersions.Progid
                        AND PrgVerid IN ( SELECT    strval
                                          FROM      dbo.SPLIT(@prgVerId) )
               );

    IF @cnt > 0
        BEGIN
            SELECT  PrgVerDescrip
            FROM    arPrograms
                   ,syAcademicCalendars
                   ,arPrgVersions
            WHERE   arPrograms.ACId = syAcademicCalendars.ACId
                    AND LOWER(syAcademicCalendars.ACDescrip) = 'clock hour'
                    AND arPrograms.ProgId = arPrgVersions.Progid
                    AND PrgVerid IN ( SELECT    strval
                                      FROM      dbo.SPLIT(@prgVerId) );
        END;



GO
