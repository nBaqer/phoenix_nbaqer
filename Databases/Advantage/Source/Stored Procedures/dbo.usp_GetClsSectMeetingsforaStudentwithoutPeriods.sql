SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE PROCEDURE [dbo].[usp_GetClsSectMeetingsforaStudentwithoutPeriods]
    (
     @StuEnrollID UNIQUEIDENTIFIER 
       
    )
AS
    BEGIN 

        SELECT DISTINCT
                t1.ClsSectMeetingId
               ,R.Descrip + '/' + t2.WorkDaysDescrip + '(' + CONVERT(NVARCHAR(30),t3.TimeIntervalDescrip,107) + '-'
                + CONVERT(NVARCHAR(30),t4.TimeIntervalDescrip,107) + +') - ' + ACT.InstructionTypeDescrip + '- ('
                + CONVERT(NVARCHAR(30),(
                                         SELECT startDate
                                         FROM   arClassSections
                                         WHERE  ClsSectionId = t1.ClsSectionId
                                       ),107) + '-' + CONVERT(NVARCHAR(30),(
                                                                             SELECT EndDate
                                                                             FROM   arClassSections
                                                                             WHERE  ClsSectionId = t1.ClsSectionId
                                                                           ),107) + ')' AS Descrip
               ,(
                  SELECT    startDate
                  FROM      arClassSections
                  WHERE     ClsSectionId = t1.ClsSectionId
                ) AS StartDate
               ,(
                  SELECT    EndDate
                  FROM      arClassSections
                  WHERE     ClsSectionId = t1.ClsSectionId
                ) AS EndDate
               ,t1.InstructionTypeID
               ,ACT.InstructionTypeDescrip
        FROM    arClsSectMeetings t1
               ,plWorkDays t2
               ,cmTimeInterval t3
               ,cmTimeInterval t4
               ,arInstructionType ACT
               ,dbo.arClassSections CS
               ,dbo.arReqs R
               ,arresults AR
        WHERE   t1.WorkDaysId = t2.WorkDaysId
                AND t1.TimeIntervalId = t3.TimeIntervalId
                AND t1.EndIntervalId = t4.TimeIntervalId
                AND ACT.InstructionTypeID = t1.InstructionTypeID
                AND CS.ClsSectionId = t1.ClsSectionId
                AND CS.ClsSectionId = AR.TestId
                AND StuEnrollId = @StuEnrollID
                AND R.ReqId = CS.ReqId
                AND R.UnitTypeId <> '2600592A-9739-4A13-BDCE-7A25FE4A7478';
    END;







GO
