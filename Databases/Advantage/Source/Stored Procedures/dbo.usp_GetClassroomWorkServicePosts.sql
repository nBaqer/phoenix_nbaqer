SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/* 
PURPOSE: 
Get posted services for a selected student enrollment, class section and component type

CREATED: 
10/25/2013 WP	US4547 Refactor Post Services by Student process

MODIFIED:


ResourceId
Homework = 499
LabWork = 500Score
Exam = 501
Final = 502
LabHours = 503
Externship = 544

*/


CREATE PROCEDURE [dbo].[usp_GetClassroomWorkServicePosts]
    (
     @StuEnrollId UNIQUEIDENTIFIER
    ,@ClsSectionId UNIQUEIDENTIFIER
    ,@InstrGrdBkWgtDetailId UNIQUEIDENTIFIER
    )
AS
    SET NOCOUNT ON;
    SELECT  gct.Descrip
           ,ISNULL(Score,0) AS Score
           ,Comments
           ,PostDate
           ,ResNum
           ,IsCompGraded
           ,IsCourseCredited
           ,gbr.ModUser
           ,gbr.ModDate
           ,gbr.InstrGrdBkWgtDetailId
           ,gbr.ClsSectionId
           ,gbr.StuEnrollId
           ,gbr.GrdBkResultId
		   ,se.EnrollDate 
    FROM    arGrdBkResults gbr
    JOIN    arGrdBkWgtDetails gbwd ON gbwd.InstrGrdBkWgtDetailId = gbr.InstrGrdBkWgtDetailId
    JOIN    arGrdComponentTypes gct ON gct.GrdComponentTypeId = gbwd.GrdComponentTypeId
	INNER JOIN	arStuEnrollments se ON  gbr.StuEnrollId = se.StuEnrollId
    WHERE   gbr.StuEnrollId = @StuEnrollId
            AND gbr.ClsSectionId = @ClsSectionId
            AND gbr.InstrGrdBkWgtDetailId = @InstrGrdBkWgtDetailId
    ORDER BY gbr.PostDate
           ,gbr.ResNum;





GO
