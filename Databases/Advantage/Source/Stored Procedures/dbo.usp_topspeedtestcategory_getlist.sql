SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_topspeedtestcategory_getlist]
    @reqId UNIQUEIDENTIFIER
   ,@effectivedate DATETIME
AS
    SELECT DISTINCT
            t2.GrdComponentTypeId
           ,t2.Descrip
           ,t2.Code
           ,t4.MinimumScore
           ,t4.NumberofTests
    FROM    arGrdComponentTypes t2
           ,arBridge_GradeComponentTypes_Courses t3
           ,arRules_GradeComponentTypes_Courses t4
    WHERE   t2.GrdComponentTypeId = t3.GrdComponentTypeId
            AND t3.GrdComponentTypeId_ReqId = t4.GrdComponentTypeId_ReqId
            AND t3.ReqId = @reqId
            AND t4.EffectiveDate = @effectivedate
    ORDER BY t2.Descrip;



GO
