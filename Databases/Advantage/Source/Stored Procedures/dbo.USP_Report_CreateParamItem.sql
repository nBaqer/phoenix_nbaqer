SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_Report_CreateParamItem]
    @Caption AS VARCHAR(MAX)
   ,@ViewNameNoExtension AS VARCHAR(MAX)

-- WITH ENCRYPTION, RECOMPILE, EXECUTE AS CALLER|SELF|OWNER| 'user_name'
AS
    DECLARE @Return_ItemId INT = 0;
    IF NOT EXISTS (
                  SELECT TOP 1 ItemId
                  FROM   dbo.ParamItem
                  WHERE  Caption = @Caption
                  )
        BEGIN

            DECLARE @ControllerClass VARCHAR(MAX) = @ViewNameNoExtension + '.ascx';
            DECLARE @ReturnValue VARCHAR(MAX) = @ViewNameNoExtension + 'Option';
            INSERT INTO dbo.ParamItem (
                                      Caption
                                     ,ControllerClass
                                     ,valueprop
                                     ,ReturnValueName
                                     ,IsItemOverriden
                                      )
            VALUES ( @Caption         -- Caption - nvarchar(50)
                    ,@ControllerClass -- ControllerClass - nvarchar(50)
                    ,0                -- valueprop - int
                    ,@ReturnValue     -- ReturnValueName - nvarchar(50)
                    ,NULL             -- IsItemOverriden - bit
                );
        END;

    SET @Return_ItemId = (
                         SELECT TOP 1 ItemId
                         FROM   dbo.ParamItem
                         WHERE  Caption = @Caption
                         );

    RETURN @Return_ItemId;
GO
