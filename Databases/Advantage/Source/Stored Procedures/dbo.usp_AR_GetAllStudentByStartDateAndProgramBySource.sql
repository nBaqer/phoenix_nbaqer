SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[usp_AR_GetAllStudentByStartDateAndProgramBySource]
    @StartDate DATETIME
   ,@CampGrpId AS VARCHAR(8000)
   ,@ProgVerId AS VARCHAR(8000) = NULL
AS /*----------------------------------------------------------------------------------------------------
    Author : Vijay Ramteke
    
    Create date : 09/13/2010
    
    Procedure Name : usp_AR_GetAllStudentByStartDateAndProgramBySource

    Objective : Get All The Student By Start Date And Program Version
    
    Parameters : Name Type Data Type Required? 
    
    Output : Returns All The Student By Start Date And Program Version for Report Dataset
                        
*/-----------------------------------------------------------------------------------------------------

    BEGIN

        SELECT  SourceCatagoryDescrip
               ,SUM(StartCount) AS StartCount
        FROM    (
                  SELECT    CASE WHEN (
                                        ST.SourceTypeDescrip IS NOT NULL
                                        AND SOC.SourceCatagoryDescrip IS NOT NULL
                                      ) THEN ISNULL(SOC.SourceCatagoryDescrip,'') + ' - ' + ISNULL(ST.SourceTypeDescrip,'')
                                 ELSE 'No Lead Source Specified'
                            END AS SourceCatagoryDescrip
                           ,COUNT(DISTINCT SE.StuEnrollId) AS StartCount
                  FROM      arStuEnrollments SE
                  INNER JOIN syStatusCodes SC ON SE.StatusCodeId = SC.StatusCodeId
                  INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                  LEFT OUTER JOIN adLeadByLeadGroups LBLG ON SE.StuEnrollId = LBLG.StuEnrollId
                  LEFT JOIN adLeads L ON L.LeadId = LBLG.LeadId
                  LEFT JOIN adSourceCatagory SOC ON SOC.SourceCatagoryId = L.SourceCategoryID
                  LEFT JOIN adSourceType ST ON ST.SourceCatagoryId = SOC.SourceCatagoryId
                                               AND ST.SourceTypeId = L.SourceTypeID
                           ,syCmpGrpCmps E
                           ,syCampuses F
                           ,syCampGrps
                  WHERE     SE.CampusId IN ( SELECT DISTINCT
                                                    t1.CampusId
                                             FROM   syCmpGrpCmps t1
                                             WHERE  t1.CampGrpId IN ( SELECT    strval
                                                                      FROM      dbo.SPLIT(@CampGrpId) ) )
                            AND CAST(SE.StartDate AS DATE) = CAST(@StartDate AS DATE)
                            AND (
                                  SE.PrgVerId IN ( SELECT   strval
                                                   FROM     dbo.SPLIT(@ProgVerId) )
                                  OR @ProgVerId IS NULL
                                )
                            --AND SC.SysStatusId = 9
                            AND SE.CampusId = F.CampusId
                            AND F.CampusId = E.CampusId
                            AND E.CampGrpId = syCampGrps.CampGrpId
                            AND syCampGrps.CampGrpId IN ( SELECT DISTINCT
                                                                    t1.CampGrpId
                                                          FROM      syCmpGrpCmps t1
                                                          WHERE     t1.CampGrpId IN ( SELECT    strval
                                                                                      FROM      dbo.SPLIT(@CampGrpId) ) )
                  GROUP BY  SOC.SourceCatagoryDescrip
                           ,ST.SourceTypeDescrip
                ) AS Result
        GROUP BY SourceCatagoryDescrip;


    END;









GO
