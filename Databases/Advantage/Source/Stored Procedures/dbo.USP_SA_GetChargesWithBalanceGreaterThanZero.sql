SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[USP_SA_GetChargesWithBalanceGreaterThanZero]
    (
     @StuEnrollId UNIQUEIDENTIFIER
    )
AS /*----------------------------------------------------------------------------------------------------
	Author          :	Saraswathi Lakshmanan
    
    Create date		:	04/12/2010
    
	Procedure Name	:	[USP_SA_GetChargesWithBalanceGreaterThanZero]

	Objective		:	Get charges with balance greater than zero
	
	Parameters		:	Name			Type	Data Type			Required? 	
						=====			====	=========			=========	
						@StuEnrollId	In		Uniqueidentifier	Required
	
	Output			:	returns a record set with charges greater than zero
						
*/-----------------------------------------------------------------------------------------------------

    BEGIN	
--has Charge greater than zero
--GetChargesWithBalanceGreaterThanZero
        SELECT  R.*
               ,( R.TransAmount - ISNULL(R.TotalApplied,0) ) AS Balance
        FROM    (
                  SELECT    t1.TransactionId
                           ,t1.TransDate
                           ,t1.TransReference
                           ,t1.TransAmount
                           ,t1.TransDescrip
                           ,TransCodeDescrip = CASE WHEN LEN(t1.TransCodeId) > 0 THEN (
                                                                                        SELECT  t2.TransCodeDescrip
                                                                                        FROM    saTransCodes t2
                                                                                        WHERE   t1.TransCodeId = t2.TransCodeId
                                                                                      )
                                                    ELSE t1.TransDescrip
                                               END
                           ,(
                              SELECT    ISNULL(SUM(Amount),0)
                              FROM      saAppliedPayments t3
                                       ,saTransactions t4
                                       ,saTransactions t5
                              WHERE     t1.TransactionId = t3.ApplyToTransId
                                        AND t3.ApplyToTransId = t4.TransactionId
                                        AND t4.Voided = 0
                                        AND t3.TransactionId = t5.TransactionId
                                        AND t5.Voided = 0
                            ) AS TotalApplied
                  FROM      saTransactions t1
                  WHERE     StuEnrollId = @StuEnrollId
                            AND t1.Voided = 0
                            AND t1.TransAmount > 0
                            AND t1.TransTypeId <> 1
                ) R
        WHERE   ( R.TransAmount - ISNULL(R.TotalApplied,0) ) > 0
        ORDER BY TransDate
               ,TransCodeDescrip; 

    END;




GO
