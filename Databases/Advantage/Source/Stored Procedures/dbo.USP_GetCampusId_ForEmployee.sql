SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_GetCampusId_ForEmployee]
    @EntityId UNIQUEIDENTIFIER
AS
    SELECT TOP 1
            CampusId
    FROM    hrEmployees
    WHERE   EmpId = @EntityId;



GO
