SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_GetWorkDaysOverlap_Count]
    @PeriodId1 UNIQUEIDENTIFIER
   ,@PeriodId2 UNIQUEIDENTIFIER
AS
    SELECT  COUNT(*) AS WorkDayOverlap
    FROM    (
              SELECT    t1.*
                       ,t3.WorkDaysDescrip
              FROM      syPeriods t1
                       ,dbo.syPeriodsWorkDays t2
                       ,plWorkDays t3
              WHERE     t1.PeriodId = t2.PeriodId
                        AND t2.WorkDayId = t3.WorkDaysId
                        AND t1.PeriodId IN ( @PeriodId1 )
            ) A1
           ,(
              SELECT    t1.*
                       ,t3.WorkDaysDescrip
              FROM      syPeriods t1
                       ,dbo.syPeriodsWorkDays t2
                       ,plWorkDays t3
              WHERE     t1.PeriodId = t2.PeriodId
                        AND t2.WorkDayId = t3.WorkDaysId
                        AND t1.PeriodId IN ( @PeriodId2 )
            ) A2
    WHERE   A1.WorkDaysDescrip = A2.WorkDaysDescrip;



GO
