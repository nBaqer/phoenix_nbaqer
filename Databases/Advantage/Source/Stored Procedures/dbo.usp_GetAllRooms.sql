SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_GetAllRooms]
    @showActiveOnly BIT
   ,@campusId VARCHAR(50)
AS
    SET NOCOUNT ON;
                   
    IF @showActiveOnly = 1
        BEGIN
            EXEC usp_GetAllActiveRooms @campusId;
        END;
    ELSE
        BEGIN
            EXEC usp_GetAllActiveAndInActiveRooms @campusId;
        END;

        



GO
