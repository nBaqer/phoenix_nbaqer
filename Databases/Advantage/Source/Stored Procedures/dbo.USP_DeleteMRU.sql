SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[USP_DeleteMRU]
    @MRUId UNIQUEIDENTIFIER
AS
    DECLARE @UserId UNIQUEIDENTIFIER
       ,@MRUTypeId INT; 
    SET @UserId = (
                    SELECT  UserId
                    FROM    syMRUs
                    WHERE   MRUId = @MRUId
                  );
    SET @MRUTypeId = (
                       SELECT   MRUTypeId
                       FROM     syMRUs
                       WHERE    MRUId = @MRUId
                     );
--If there is only one item in the MRU list DO NOT DELETE
    IF (
         SELECT COUNT(*)
         FROM   syMRUS
         WHERE  UserId = @UserId
                AND MRUTypeId = @MRUTypeId
       ) > 1
        BEGIN
            DELETE  FROM syMRUS
            WHERE   MRUId = @MRUId;
        END; 
    SELECT  @MRUTypeId; 



GO
