SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- Starts here
CREATE PROCEDURE [dbo].[USP_AD_Navigation_GetList]
    @SchoolEnumerator INT
AS
    DECLARE @ShowRossOnlyTabs BIT
       ,@SchedulingMethod VARCHAR(50)
       ,@TrackSAPAttendance VARCHAR(50);
    DECLARE @ShowCollegeOfCourtReporting VARCHAR(5)
       ,@FameESP VARCHAR(5)
       ,@EdExpress VARCHAR(5); 
    DECLARE @GradeBookWeightingLevel VARCHAR(20)
       ,@ShowExternshipTabs VARCHAR(5);

    DECLARE @AR_ParentId UNIQUEIDENTIFIER
       ,@PL_ParentId UNIQUEIDENTIFIER
       ,@FA_ParentId UNIQUEIDENTIFIER;
    DECLARE @FAC_ParentId UNIQUEIDENTIFIER
       ,@SA_ParentId UNIQUEIDENTIFIER
       ,@AD_ParentId UNIQUEIDENTIFIER;
	
-- Get Values
--SET @SchoolEnumerator = 1689
    SET @ShowRossOnlyTabs = (
                              SELECT    VALUE
                              FROM      dbo.syConfigAppSetValues
                              WHERE     SettingId = 68
                            );
    SET @SchedulingMethod = (
                              SELECT    VALUE
                              FROM      dbo.syConfigAppSetValues
                              WHERE     SettingId = 65
                            );
    SET @TrackSAPAttendance = (
                                SELECT  VALUE
                                FROM    dbo.syConfigAppSetValues
                                WHERE   SettingId = 72
                              );
    SET @ShowCollegeOfCourtReporting = (
                                         SELECT VALUE
                                         FROM   dbo.syConfigAppSetValues
                                         WHERE  SettingId = 118
                                       );
    SET @FameESP = (
                     SELECT VALUE
                     FROM   dbo.syConfigAppSetValues
                     WHERE  SettingId = 37
                   );
    SET @EdExpress = (
                       SELECT   VALUE
                       FROM     dbo.syConfigAppSetValues
                       WHERE    SettingId = 91
                     );
    SET @GradeBookWeightingLevel = (
                                     SELECT VALUE
                                     FROM   dbo.syConfigAppSetValues
                                     WHERE  SettingId = 43
                                   );
    SET @ShowExternshipTabs = (
                                SELECT  VALUE
                                FROM    dbo.syConfigAppSetValues
                                WHERE   SettingId = 71
                              );

--SET @AD_ParentId = (SELECT t1.HierarchyId FROM syNavigationNodes t1 INNER JOIN syNavigationNodes t2 
--					ON t1.ParentId=t2.HierarchyId WHERE t1.ResourceId IN (686,687,689) AND t2.ResourceId=189)
-- Admissions						
    SELECT  *
           ,CASE WHEN ChildResourceId = 686 THEN 1
                 ELSE 2
            END AS SortOrder
           ,ResourceTypeId
    FROM    (
              SELECT    189 AS ModuleResourceId
                       ,ResourceId AS ChildResourceId
                       ,Resource AS ChildResource
                       ,NULL AS ChildResourceURL
                       ,NULL AS ParentResourceId
                       ,NULL AS ParentResource
                       ,NULL AS ParentId2
                       ,6 AS ResourceTypeId
              FROM      syResources
              WHERE     ResourceId IN ( 687,688,689 )
              UNION
              SELECT    189 AS ModuleResourceId
                       ,ChildResourceId
                       ,ChildResource
                       ,ChildResourceURL
                       ,ParentResourceId
                       ,ParentResource
                       ,ParentId2
                       ,ResourceTypeId
              FROM      (
                          SELECT DISTINCT
                                    NNChild.ResourceId AS ChildResourceId
                                   ,CASE WHEN NNChild.ResourceId IN ( 687,688,689 ) THEN 'Student Tabs'
                                         ELSE RChild.Resource
                                    END AS ChildResource
                                   ,RChild.ResourceURL AS ChildResourceURL
                                   ,CASE WHEN NNChild.ResourceId IN ( 687,688,689 ) THEN NULL
                                         ELSE NNParent.ResourceId
                                    END AS ParentResourceId
                                   ,RParent.Resource AS ParentResource
                                   ,RParentModule.Resource AS MODULE
                                   ,NNParent.Parentid AS ParentId2
                                   ,RChild.ResourceTypeID AS ResourceTypeId
                          FROM      syResources RChild
                          INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
                          INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                          INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                          LEFT OUTER JOIN (
                                            SELECT  *
                                            FROM    syResources
                                            WHERE   ResourceTypeId = 1
                                          ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                          WHERE     RChild.ResourceTypeId IN ( 2,3,4,5 )
                                    AND (
                                          RChild.ChildTypeId IS NULL
                                          OR RChild.ChildTypeId = 3
                                          OR RChild.ChildTypeId = 4
                                          OR RChild.ChildTypeId = 5
                                        )
                                    AND (
                                          ( NNParent.ResourceId IN ( 189,687,688,689 )
                                          AND NNChild.Parentid IN ( SELECT  t1.HierarchyId
                                                                    FROM    syNavigationNodes t1
                                                                    INNER JOIN syNavigationNodes t2 ON t1.ParentId = t2.HierarchyId
                                                                    WHERE   t1.ResourceId IN ( 687,688,689 )
                                                                            AND t2.ResourceId = 189 )
					)
                                        )
                                    AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
                        ) t1
              UNION
--Academic Records
              SELECT    26 AS ModuleResourceId
                       ,ResourceId AS ChildResourceId
                       ,Resource AS ChildResource
                       ,NULL AS ChildResourceURL
                       ,NULL AS ParentResourceId
                       ,NULL AS ParentResource
                       ,NULL AS ParentId2
                       ,6 AS ResourceTypeId
              FROM      syResources
              WHERE     ResourceId IN ( 687,688,689 )
              UNION
              SELECT    26 AS ModuleResourceId
                       ,ChildResourceId
                       ,ChildResource
                       ,ChildResourceURL
                       ,ParentResourceId
                       ,ParentResource
                       ,ParentId2
                       ,ResourceTypeId
              FROM      (
                          SELECT DISTINCT
                                    NNChild.ResourceId AS ChildResourceId
                                   ,CASE WHEN NNChild.ResourceId IN ( 687,688,689 ) THEN 'Student Tabs'
                                         ELSE RChild.Resource
                                    END AS ChildResource
                                   ,RChild.ResourceURL AS ChildResourceURL
                                   ,CASE WHEN NNChild.ResourceId IN ( 687,688,689 ) THEN NULL
                                         ELSE NNParent.ResourceId
                                    END AS ParentResourceId
                                   ,RParent.Resource AS ParentResource
                                   ,RParentModule.Resource AS MODULE
                                   ,NNParent.Parentid AS ParentId2
                                   ,RChild.ResourceTypeID AS ResourceTypeId
                          FROM      syResources RChild
                          INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
                          INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                          INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                          LEFT OUTER JOIN (
                                            SELECT  *
                                            FROM    syResources
                                            WHERE   ResourceTypeId = 1
                                          ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                          WHERE     RChild.ResourceTypeId IN ( 2,3,4,5 )
                                    AND (
                                          RChild.ChildTypeId IS NULL
                                          OR RChild.ChildTypeId = 3
                                          OR RChild.ChildTypeId = 4
                                          OR RChild.ChildTypeId = 5
                                        )
                                    AND (
                                          ( NNParent.ResourceId IN ( 26,687,688,689 )
                                          AND NNChild.Parentid IN ( SELECT  t1.HierarchyId
                                                                    FROM    syNavigationNodes t1
                                                                    INNER JOIN syNavigationNodes t2 ON t1.ParentId = t2.HierarchyId
                                                                    WHERE   t1.ResourceId IN ( 687,688,689 )
                                                                            AND t2.ResourceId = 26 )
							)
                                        )
                                    AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
                        ) t1
            ) MainQuery
    WHERE   --Remove SubMenus
            ( ResourceTypeId <> 2 )
            AND
				-- Hide resources based on Configuration Settings
            (
              -- The following expression means if showross... is set to false, hide 
					-- ResourceIds 541,542,532,534,535,538,543,539
					-- (Not False) OR (Condition)
			  (
                ( @ShowRossOnlyTabs <> 0 )
                OR ( ChildResourceId NOT IN ( 541,542,532,534,535,538,543,539 ) )
              )
              AND
					-- The following expression means if showross... is set to true, hide 
					-- ResourceIds 142,375,330,476,508,102,107,237
					-- (Not True) OR (Condition)
              (
                ( @ShowRossOnlyTabs <> 1 )
                OR ( ChildResourceId NOT IN ( 142,375,330,476,508,102,107,237 ) )
              )
              AND
					---- The following expression means if SchedulingMethod=regulartraditional, hide 
					---- ResourceIds 91 and 497
					---- (Not True) OR (Condition)
              (
                (
                  NOT LOWER(LTRIM(RTRIM(@SchedulingMethod))) = 'regulartraditional'
                )
                OR ( ChildResourceId NOT IN ( 91,497 ) )
              )
              AND
					-- The following expression means if TrackSAPAttendance=byday, hide 
					-- ResourceIds 633
					-- (Not True) OR (Condition)
              (
                (
                  NOT LOWER(LTRIM(RTRIM(@TrackSAPAttendance))) = 'byday'
                )
                OR ( ChildResourceId NOT IN ( 633 ) )
              )
              AND
					---- The following expression means if @ShowCollegeOfCourtReporting is set to false, hide 
					---- ResourceIds 614,615
					---- (Not False) OR (Condition)
              (
                (
                  NOT LOWER(LTRIM(RTRIM(@ShowCollegeOfCourtReporting))) = 'no'
                )
                OR ( ChildResourceId NOT IN ( 614,615 ) )
              )
              AND
					-- The following expression means if @ShowCollegeOfCourtReporting is set to true, hide 
					-- ResourceIds 497
					-- (Not True) OR (Condition)
              (
                (
                  NOT LOWER(LTRIM(RTRIM(@ShowCollegeOfCourtReporting))) = 'yes'
                )
                OR ( ChildResourceId NOT IN ( 497 ) )
              )
              AND
					-- The following expression means if FAMEESP is set to false, hide 
					-- ResourceIds 517,523, 525
					-- (Not False) OR (Condition)
              (
                (
                  NOT LOWER(LTRIM(RTRIM(@FameESP))) = 'no'
                )
                OR ( ChildResourceId NOT IN ( 517,523,525 ) )
              )
              AND
					---- The following expression means if EDExpress is set to false, hide 
					---- ResourceIds 603,604,606,619
					---- (Not False) OR (Condition)
              (
                (
                  NOT LOWER(LTRIM(RTRIM(@EdExpress))) = 'no'
                )
                OR ( ChildResourceId NOT IN ( 603,604,605,606,619 ) )
              )
              AND
					---- The following expression means if @GradeBookWeightingLevel is set to courselevel, hide 
					---- ResourceIds 107,96,222
					---- (Not False) OR (Condition)
              (
                (
                  NOT LOWER(LTRIM(RTRIM(@GradeBookWeightingLevel))) = 'courselevel'
                )
                OR ( ChildResourceId NOT IN ( 107,96,222 ) )
              )
              AND (
                    (
                      NOT LOWER(LTRIM(RTRIM(@GradeBookWeightingLevel))) = 'instructorlevel'
                    )
                    OR ( ChildResourceId NOT IN ( 476 ) )
                  )
              AND (
                    (
                      NOT LOWER(LTRIM(RTRIM(@ShowExternshipTabs))) = 'no'
                    )
                    OR ( ChildResourceId NOT IN ( 543,538 ) )
                  )
            )
    ORDER BY ModuleResourceId
           ,ParentResourceId
           ,SortOrder
           ,ChildResource;



GO
