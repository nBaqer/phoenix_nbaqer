SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_ConflictsClassesInsideSameTermIgnoreRooms_GetList]
    @CurrentClassSectionId VARCHAR(8000)
   ,@TargetTermId VARCHAR(8000)
   ,@CampusId UNIQUEIDENTIFIER
AS
    BEGIN
--SET @CurrentClassSectionId='2F55F323-7BF6-4676-A81B-354C4A2D2DD8'
--SET @TargetTermId = 'C2F9775B-BBA6-4B79-BD6F-083D743188D1'
--SET @CampusId = '1CC3AB0C-59A7-4840-9D15-8D9A48556AD3'
        DECLARE @countDateRangeConflicts INT; 
        SET @countDateRangeConflicts = 0;
        DECLARE @ClassesWithOverlappingDates TABLE
            (
             CourseDescrip VARCHAR(50)
            ,ClsSectionDescrip VARCHAR(50)
            ,ClsSectionId UNIQUEIDENTIFIER
            ,RoomId UNIQUEIDENTIFIER
            ,StartDate DATETIME
            ,EndDate DATETIME
            );

        DECLARE @ClassesWithOverlappingDays TABLE
            (
             StartDate DATETIME
            ,EndDate DATETIME
            ,WorkDaysDescrip VARCHAR(50)
            ,StartTime VARCHAR(50)
            ,EndTime VARCHAR(50)
            ,ViewOrder INT
            ,ClsSection VARCHAR(50)
            ,CourseDescrip VARCHAR(50)
            ,ClsSectionId VARCHAR(50)
            ,CourseId VARCHAR(50)
            );
											
        DECLARE @ClassesWithOverlappingTimeInterval TABLE
            (
             StartDate DATETIME
            ,EndDate DATETIME
            ,WorkDaysDescrip VARCHAR(50)
            ,StartTime VARCHAR(50)
            ,EndTime VARCHAR(50)
            ,ViewOrder INT
            ,ClsSection VARCHAR(50)
            ,CourseDescrip VARCHAR(50)
            ,ClsSectionId VARCHAR(50)
            ,CourseId VARCHAR(50)
            );


/**************************** Business Rules for Schedule Conflicts *************

There are 3 layers involved in this stored proc

a. Level 1 - Check if class you are trying to add to advantage via class sections page,
			 class section with periods page has the same start date/end date or 
			 overlapping date ranges.
			 
			 Example: 
					  English Class is offered from 03/01/2012 - 05/01/2012
					  Maths Class is offered from 03/05/2012 - 05/14/2012
				
b. Level 2 - If a conflict is identified in Level 1, then check if 
			 both classes are scheduled on same day. It is perfectly normal
			 for two classes to fall in the same date range but on different dates.
			 So this check is neccessary.
			 
			 Example: 
					English class (03/01/2012-05/01/2012) - Mon, Tues
					Maths Class (03/05/2012 - 05/14/2012) - Mon, Wed
					
			In the above cases, both classes have overlapping date ranges and 
			they both offer classes on Mon
			
c. Level 3 - If conflict is identified in Level 2, then check if both classes are 
			 offered at the same time or during overlapping times. Its quite normal
			 for two classes to be offered on the same day but different times 
			 (ex:day and eve classes). Also, there is a probability for 
			 two classes to have overlapping time
			 			 
			 Example: 
				English class - Mon - 8AM - 1PM
				Maths Class - Mon - 8AM - 3.30PM
			 
**************************************************************************/
		
        INSERT  INTO @ClassesWithOverlappingDates
                SELECT  *
                FROM    (
                          SELECT DISTINCT
                                    A2.Descrip
                                   ,A2.ClsSection
                                   ,A2.ClsSectionId
                                   ,A2.RoomId
                                   ,A2.StartDate
                                   ,A2.EndDate
                          FROM      (
                                      -- Get the class startdate and enddate of classes that already exist for this term
			-- exclude the current class
			
			-- Get the class startdate and enddate that is being currently put in
			SELECT DISTINCT
                    t3.*
                   ,t4.Descrip
                   ,t2.ClsSection
                   ,t2.TermId
                   ,t2.InstructorId
            FROM    arClassSections t2
                   ,arClsSectMeetings t3
                   ,arReqs t4
            WHERE   t2.ClsSectionId = t3.ClsSectionId
                    AND t2.ReqId = t4.ReqId
                    AND ( t2.ClsSectionId IN ( SELECT   Val
                                               FROM     MultipleValuesForReportParameters(@CurrentClassSectionId,',',1) ) )
                                    ) A1
                                   ,(
                                      SELECT DISTINCT
                                                t3.*
                                               ,t4.Descrip
                                               ,t2.ClsSection
                                               ,t2.TermId
                                               ,t2.InstructorId
                                      FROM      arClassSections t2
                                               ,arClsSectMeetings t3
                                               ,arReqs t4
                                      WHERE     t2.ClsSectionId = t3.ClsSectionId
                                                AND t2.ReqId = t4.ReqId
                                                AND t2.ClsSectionId <> @CurrentClassSectionId
                                                AND ( t2.TermId IN ( SELECT Val
                                                                     FROM   MultipleValuesForReportParameters(@TargetTermId,',',1) ) )
                                                AND t3.PeriodId IS NOT NULL
                                                AND CampusId = @CampusId
                                    ) A2
                          WHERE     A1.StartDate <= A2.EndDate
                                    AND 
			--A1.EndDate>A2.StartDate AND 
                                    A1.ClsSectionId <> A2.ClsSectionId
                                    AND -- Different classes
			--(A1.RoomId=A2.RoomId OR A1.InstructorId=A2.InstructorId) AND  -- Same room or Same INstructor
                                    A1.TermId = A2.TermId -- in Same Term
                        ) dt;
		
	--SELECT * FROM @ClassesWithOverlappingDates
	
	-- If there are classes with overlapping days in same term
	-- Get the days on which they overlap
        INSERT  INTO @ClassesWithOverlappingDays
                SELECT DISTINCT
                        A1.StartDate
                       ,A1.EndDate
                       ,A1.WorkDaysDescrip
                       ,A1.StartTime
                       ,A1.EndTime
                       ,A1.ViewOrder
                       ,A1.ClsSection
                       ,A1.Descrip AS CourseDescrip
                       ,A1.ClsSectionId
                       ,A1.reqId
                FROM    (
                          SELECT DISTINCT
                                    t3.ClsSectMeetingId
                                   ,t3.StartDate
                                   ,t3.EndDate
                                   ,t4.Descrip
                                   ,t2.ClsSection
                                   ,t2.TermId
                                   ,(
                                      SELECT    CONVERT(VARCHAR,TimeIntervalDescrip,108)
                                      FROM      dbo.cmTimeInterval
                                      WHERE     TimeIntervalId = t5.StartTimeId
                                    ) AS StartTime
                                   ,(
                                      SELECT    CONVERT(VARCHAR,TimeIntervalDescrip,108)
                                      FROM      dbo.cmTimeInterval
                                      WHERE     TimeIntervalId = t5.EndTimeId
                                    ) AS EndTime
                                   ,t7.WorkDaysDescrip
                                   ,t7.ViewOrder
                                   ,t2.ClsSectionId
                                   ,t4.ReqId
                          FROM      arClassSections t2
                                   ,arClsSectMeetings t3
                                   ,arReqs t4
                                   ,syPeriods t5
                                   ,dbo.syPeriodsWorkDays t6
                                   ,plWorkDays t7
                          WHERE     t2.ClsSectionId = t3.ClsSectionId
                                    AND t2.ReqId = t4.ReqId
                                    AND t3.PeriodId = t5.PeriodId
                                    AND t5.PeriodId = t6.PeriodId
                                    AND t6.WorkDayId = t7.WorkDaysId
                                    AND t2.ClsSectionId IN ( SELECT DISTINCT
                                                                    ClsSectionId
                                                             FROM   @ClassesWithOverlappingDates )
		--AND t2.ClsSectionId <> @CurrentClassSectionId
		--Select Distinct t2.ClsSectionId,t4.ReqId,t3.ClsSectMeetingId,t3.StartDate,t3.EndDate,t4.Descrip,t2.ClsSection,t2.TermId,
		--(SELECT convert(varchar, TimeIntervalDescrip, 108) FROM dbo.cmTimeInterval WHERE TimeIntervalId=t3.TimeIntervalId) AS StartTime,
		--(SELECT CONVERT(varchar,TimeIntervalDescrip,108) FROM dbo.cmTimeInterval WHERE TimeIntervalId=t3.EndIntervalId) AS EndTime,t7.WorkDaysDescrip,t7.ViewOrder
		--from arClassSections t2,arClsSectMeetings t3,arReqs t4,plWorkDays t7
		--where t2.ClsSectionId=t3.ClsSectionId AND t2.ReqId=t4.ReqId AND t3.WorkDaysId=t7.WorkDaysId
		--AND t2.ClsSectionId IN (SELECT DISTINCT ClsSectionId FROM @ClassesWithOverlappingDates)
                        ) A1
                       ,(
                          SELECT DISTINCT
                                    t3.ClsSectMeetingId
                                   ,t3.StartDate
                                   ,t3.EndDate
                                   ,t4.Descrip
                                   ,t2.ClsSection
                                   ,t2.TermId
                                   ,(
                                      SELECT    CONVERT(VARCHAR,TimeIntervalDescrip,108)
                                      FROM      dbo.cmTimeInterval
                                      WHERE     TimeIntervalId = t5.StartTimeId
                                    ) AS StartTime
                                   ,(
                                      SELECT    CONVERT(VARCHAR,TimeIntervalDescrip,108)
                                      FROM      dbo.cmTimeInterval
                                      WHERE     TimeIntervalId = t5.EndTimeId
                                    ) AS EndTime
                                   ,t7.WorkDaysDescrip
                                   ,t7.ViewOrder
                          FROM      arClassSections t2
                                   ,arClsSectMeetings t3
                                   ,arReqs t4
                                   ,syPeriods t5
                                   ,dbo.syPeriodsWorkDays t6
                                   ,plWorkDays t7
                          WHERE     t2.ClsSectionId = t3.ClsSectionId
                                    AND t2.ReqId = t4.ReqId
                                    AND t3.PeriodId = t5.PeriodId
                                    AND t5.PeriodId = t6.PeriodId
                                    AND t6.WorkDayId = t7.WorkDaysId
                                    AND ( t2.ClsSectionId IN ( SELECT   Val
                                                               FROM     MultipleValuesForReportParameters(@CurrentClassSectionId,',',1) ) )
		--Select Distinct t3.ClsSectMeetingId,t3.StartDate,t3.EndDate,t4.Descrip,t2.ClsSection,t2.TermId,
		--(SELECT convert(varchar, TimeIntervalDescrip, 108) FROM dbo.cmTimeInterval WHERE TimeIntervalId=t3.TimeIntervalId) AS StartTime,
		--(SELECT CONVERT(varchar,TimeIntervalDescrip,108) FROM dbo.cmTimeInterval WHERE TimeIntervalId=t3.EndIntervalId) AS EndTime,t7.WorkDaysDescrip,t7.ViewOrder
		--from arClassSections t2,arClsSectMeetings t3,arReqs t4,plWorkDays t7
		--where t2.ClsSectionId=t3.ClsSectionId AND t2.ReqId=t4.ReqId AND t3.WorkDaysId=t7.WorkDaysId
		--AND t2.ClsSectionId=@CurrentClassSectionId
                        ) A2
                WHERE   A1.WorkDaysDescrip = A2.WorkDaysDescrip
                ORDER BY A1.ViewOrder
                       ,A1.ClsSection
                       ,A1.StartDate
                       ,A1.EndDate
                       ,A1.StartTime
                       ,A1.EndTime;
	
 	
    -- If classes overlap on a day check the timings to see if there is a overlap
        INSERT  INTO @ClassesWithOverlappingTimeInterval
                SELECT  A1.*
                FROM    (
                          SELECT DISTINCT
                                    t3.ClsSectMeetingId
                                   ,t3.StartDate
                                   ,t3.EndDate
                                   ,t4.Descrip
                                   ,t2.ClsSection
                                   ,t2.TermId
                                   ,(
                                      SELECT    CONVERT(VARCHAR,TimeIntervalDescrip,108)
                                      FROM      dbo.cmTimeInterval
                                      WHERE     TimeIntervalId = t5.StartTimeId
                                    ) AS StartTime
                                   ,(
                                      SELECT    CONVERT(VARCHAR,TimeIntervalDescrip,108)
                                      FROM      dbo.cmTimeInterval
                                      WHERE     TimeIntervalId = t5.EndTimeId
                                    ) AS EndTime
                                   ,t7.WorkDaysDescrip
                                   ,t7.ViewOrder
                          FROM      arClassSections t2
                                   ,arClsSectMeetings t3
                                   ,arReqs t4
                                   ,syPeriods t5
                                   ,dbo.syPeriodsWorkDays t6
                                   ,plWorkDays t7
                          WHERE     t2.ClsSectionId = t3.ClsSectionId
                                    AND t2.ReqId = t4.ReqId
                                    AND t3.PeriodId = t5.PeriodId
                                    AND t5.PeriodId = t6.PeriodId
                                    AND t6.WorkDayId = t7.WorkDaysId
                                    AND t2.ClsSectionId = t3.ClsSectionId
                                    AND ( t2.TermId IN ( SELECT Val
                                                         FROM   MultipleValuesForReportParameters(@TargetTermId,',',1) ) )
                                    AND ( t2.ClsSectionId IN ( SELECT   Val
                                                               FROM     MultipleValuesForReportParameters(@CurrentClassSectionId,',',1) ) )
                        ) A2
                       ,@ClassesWithOverlappingDays A1
                WHERE   --(A1.StartDate>=A2.StartDate OR A1.StartDate <= A2.EndDate) AND
                        ( A1.StartDate <= A2.EndDate 
	--AND A1.EndDate>A2.StartDate
                          )
                        AND ( A1.WorkDaysDescrip = A2.WorkDaysDescrip )
                        AND (
                              A1.StartTime < A2.EndTime
                              AND A1.EndTime > A2.StartTime
                            );
	
	--SELECT DISTINCT CourseDescrip,ClsSection,StartDate,EndDate,WorkDaysDescrip,StartTime,EndTime FROM @ClassesWithOverlappingTimeInterval
        SELECT DISTINCT
                CourseId
               ,CourseDescrip
               ,ClsSectionId
               ,ClsSection
               ,CONVERT(VARCHAR,StartDate,101) + '- ' + CONVERT(VARCHAR,EndDate,101) AS ClassPeriod
               ,WorkDaysDescrip + ' (' + StartTime + ' - ' + EndTime + ')' AS MeetingSchedule
               ,ViewOrder
               ,StartDate
               ,EndDate
        FROM    @ClassesWithOverlappingTimeInterval
        ORDER BY CourseDescrip
               ,ClsSection
               ,StartDate
               ,EndDate
               ,ViewOrder;
    END;



GO
