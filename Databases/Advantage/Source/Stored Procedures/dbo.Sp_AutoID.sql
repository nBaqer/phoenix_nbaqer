SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[Sp_AutoID]
AS
    DECLARE @Dash VARCHAR(1); 
    DECLARE @Values VARCHAR(10); 
    DECLARE @FrmTable VARCHAR(10); 
    SET @Dash = '-';
    Back: 
    PRINT ( CONVERT(CHAR(40),NEWID()) );
    PRINT ( CONVERT(CHAR(10),RIGHT(NEWID(),4)) );
    SET @Values = RIGHT(NEWID(),4) + @Dash + RIGHT(NEWID(),2) + @Dash + LEFT(NEWID(),1); 
    PRINT ( CONVERT(CHAR(10),@Values) );
--SELECT @FrmTable=userid FROM personal WHERE userid=@values 
--if (@values=@FrmTable) goto Back 
--select AutoID=@Values GO 
--exec Sp_AutoID 



GO
