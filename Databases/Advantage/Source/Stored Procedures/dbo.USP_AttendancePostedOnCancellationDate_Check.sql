SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_AttendancePostedOnCancellationDate_Check]
    @ClsSectMeetingId UNIQUEIDENTIFIER
   ,@StartDate DATETIME
   ,@EndDate DATETIME
AS
    SELECT  COUNT(*) AS RowCounter
    FROM    atClsSectAttendance
    WHERE   ClsSectMeetingId = @ClsSectMeetingId
            AND (
                  LTRIM(CONVERT(CHAR(10),MeetDate,101)) >= @StartDate
                  AND LTRIM(CONVERT(CHAR(10),MeetDate,101)) <= @EndDate
                )
            AND ( Actual <> 9999 )
            AND ( Actual <> 999 );



GO
