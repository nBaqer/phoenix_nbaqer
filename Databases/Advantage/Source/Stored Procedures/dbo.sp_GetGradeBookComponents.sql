SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[sp_GetGradeBookComponents]
    @StuEnrollId VARCHAR(38)
   ,@ClsSectionId VARCHAR(38)
   ,@TermId VARCHAR(38)
AS
    BEGIN

        SET FMTONLY OFF;

        IF OBJECT_ID('tempdb..#tmpGradeBookComponents') IS NOT NULL
            DROP TABLE #tmpGradeBookComponents;

        CREATE TABLE #tmpGradeBookComponents
            (
             StuEnrollId VARCHAR(38)
            ,Term VARCHAR(50)
            ,TermId VARCHAR(38)
            ,ClassStartDate DATETIME
            ,ClassEndDate DATETIME
            ,CourseName VARCHAR(100)
            ,ClsSectionId VARCHAR(38)
            ,InstructorName VARCHAR(100)
            ,CourseCode VARCHAR(12)
            ,Credits DECIMAL(6,2)
            ,Hours DECIMAL(6,2)
            ,GradeBookComponent VARCHAR(50)
            ,ComponentType VARCHAR(50)
            ,Score DECIMAL(6,2)
            ,Grade CHAR(10)
            ,Weight VARCHAR(50)
            );

        INSERT  INTO #tmpGradeBookComponents
                SELECT DISTINCT
                        t1.StuEnrollId
                       ,t3.TermDescrip AS Term
                       ,t4.TermId
                       ,t4.StartDate AS ClassStartDate
                       ,t4.EndDate AS ClassEndDate
                       ,t2.Descrip AS CourseName
                       ,t4.ClsSectionId
                       ,Users.FullName AS InstructorName
                       ,t2.Code AS CourseCode
                       ,t2.Credits
                       ,t2.Hours
                       ,WgtDetail.Descrip AS GradebookComponent
                       ,CType.Descrip AS ComponentType
                       ,GrdResult.Score
                       ,(
                          SELECT    Grade
                          FROM      arGradeSystemDetails
                          WHERE     GrdSysDetailId = t1.GrdSysDetailId
                        ) AS Grade
                       ,CAST(WgtDetail.Weight AS INTEGER) AS Weight
                FROM    arResults t1
                INNER JOIN arClassSections t4 ON t1.TestId = t4.ClsSectionId
                INNER JOIN arClassSectionTerms t5 ON t4.ClsSectionId = t5.ClsSectionId
                INNER JOIN arTerm t3 ON t5.TermId = t3.TermId
                INNER JOIN arReqs t2 ON t4.ReqId = t2.ReqId
                INNER JOIN arStuEnrollments t6 ON t1.StuEnrollId = t6.StuEnrollId
                INNER JOIN syUsers Users ON t4.InstructorId = Users.UserId
                LEFT JOIN dbo.arGrdBkResults GrdResult ON t1.StuEnrollId = GrdResult.StuEnrollId
                                                          AND t1.TestId = GrdResult.ClsSectionId
                LEFT JOIN dbo.arGradeSystemDetails GrdSysDetail ON t1.GrdSysDetailId = GrdSysDetail.GrdSysDetailId
                LEFT JOIN dbo.arGrdBkWgtDetails WgtDetail ON WgtDetail.InstrGrdBkWgtDetailId = GrdResult.InstrGrdBkWgtDetailId
                                                             AND WgtDetail.InstrGrdBkWgtId = t4.InstrGrdBkWgtId
                LEFT JOIN arGrdComponentTypes CType ON WgtDetail.GrdComponentTypeId = CType.GrdComponentTypeId
                WHERE   (
                          t1.GrdSysDetailId IS NOT NULL
                          OR (
                               t1.GrdSysDetailId IS NULL
                               AND t1.isClinicsSatisfied = 1
                             )
                          OR (
                               t1.GrdSysDetailId IS NULL
                               AND (
                                     t1.isClinicsSatisfied = 0
                                     OR t1.isClinicsSatisfied IS NULL
                                   )
                               AND (
                                     SELECT COUNT(*)
                                     FROM   arGrdBkResults
                                     WHERE  StuEnrollId = t1.StuEnrollId
                                            AND ClsSectionId = t1.TestId
                                            AND Score IS NOT NULL
                                   ) >= 1
                             )
                        )
                        AND t1.StuEnrollId = @StuEnrollId
                UNION
                SELECT DISTINCT
                        t10.StuEnrollId
                       ,t30.TermDescrip AS Term
                       ,t10.TermId
                       ,'1/1/1900' AS ClassStartDate
                       ,'1/1/1900' AS ClassEndDate
                       ,t20.Descrip AS CourseName
                       ,NULL
                       ,NULL
                       ,t20.Code AS CourseCode
                       ,t20.Credits
                       ,t20.Hours
                       ,NULL
                       ,NULL
                       ,NULL
                       ,(
                          SELECT    Grade
                          FROM      arGradeSystemDetails
                          WHERE     GrdSysDetailId = t10.GrdSysDetailId
                        ) AS Grade
                       ,NULL
                FROM    arTransferGrades t10
                INNER JOIN arTerm t30 ON t10.TermId = t30.TermId
                INNER JOIN arReqs t20 ON t10.ReqId = t20.ReqId
                WHERE   (
                          t10.GrdSysDetailId IS NOT NULL
                          OR (
                               t10.GrdSysDetailId IS NULL
                               AND t10.isClinicsSatisfied = 1
                             )
                          OR (
                               t10.GrdSysDetailId IS NULL
                               AND (
                                     t10.isClinicsSatisfied = 0
                                     OR t10.isClinicsSatisfied IS NULL
                                   )
                               AND (
                                     SELECT COUNT(*)
                                     FROM   arGrdBkResults
                                     WHERE  StuEnrollId = t10.StuEnrollId
                                            AND ClsSectionId IN ( SELECT DISTINCT
                                                                            ClsSectionId
                                                                  FROM      arClassSections
                                                                  WHERE     ReqId = t10.ReqId
                                                                            AND TermId = t10.TermId )
                                            AND Score IS NOT NULL
                                   ) >= 1
                             )
                        )
                        AND t10.StuEnrollId = @StuEnrollId
        --UNION
        --SELECT DISTINCT
        --        t1.StuEnrollId
        --      , t3.TermDescrip AS Term
        --      , t4.TermId
        --      , t4.StartDate AS ClASsStartDate
        --      , t4.EndDate AS ClASsEndDate
        --      , t2.Descrip AS [CourseName]
        --      , NULL
        --      , NULL
        --      , t2.Code AS CourseCode
        --      , t2.Credits
        --      , t2.Hours
        --      , NULL
        --      , NULL
        --      , NULL
        --      , (
        --         SELECT Grade
        --         FROM   arGradeSystemDetails
        --         WHERE  GrdSysDetailId = t1.GrdSysDetailId
        --        ) AS Grade
        --      , NULL
        --FROM    (
        --         SELECT DISTINCT
        --                t700.ReqId AS EqReqId
        --              , t3.ReqId
        --              , t100.StuEnrollId
        --         FROM   arStuEnrollments t100
        --              , arReqs t3
        --              , arProgVerDef t400
        --              , arStudent t500
        --              , arPrgVersions t600
        --              , arCourseEquivalent t700
        --         WHERE  (t100.StudentId = t500.StudentId)
        --                AND t3.ReqId = t400.ReqId
        --                AND t100.PrgVerId = t400.PrgVerId
        --                AND t100.PrgVerId = t600.PrgVerId
        --                AND t3.ReqId = t700.EquivReqId
        --                AND t100.StuEnrollId = @StuEnrollId
        --                AND t3.ReqId NOT IN (
        --                SELECT  ReqId
        --                FROM    arResults a
        --                      , arClassSections b
        --                WHERE   a.TestId = b.ClsSectionId
        --                        AND StuEnrollId = @StuEnrollId
        --                        AND ReqId = t400.ReqId)
        --        ) S
        --      , arResults t1
        --      , arReqs t2
        --      , arTerm t3
        --      , arClassSections t4
        --      , arClassSectionTerms t5
        --      , arStuEnrollments t6
        --WHERE   t1.TestId = t4.ClsSectionId
        --        AND t4.ClsSectionId = t5.ClsSectionId
        --        AND t5.TermId = t3.TermId
        --        AND t4.ReqId = t2.ReqId
        --        AND t1.StuEnrollId = t6.StuEnrollId
        --        AND t1.GrdSysDetailId IS NOT NULL
        --        AND t1.StuEnrollId = S.StuEnrollId
        --        AND t2.ReqId = S.EqReqId
                ORDER BY Term DESC
                       ,ClassStartDate
                       ,ClassEndDate; 

        BEGIN 
            SELECT DISTINCT
                    GradeBookComponent
                   ,ComponentType
                   ,Score
                   ,( Weight + '%' ) AS Weight
            FROM    #tmpGradeBookComponents
            WHERE   GradeBookComponent IS NOT NULL
                    AND StuEnrollId = @StuEnrollId
                    AND ClsSectionId = @ClsSectionId
                    AND TermId = @TermId;
        END;

    END;
-- ********************************************************************************************************
-- END  --  sp_GetGradeBookComponents
-- ********************************************************************************************************

GO
