SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[USP_PostedServices_GetList]
    (
     @StuEnrollId UNIQUEIDENTIFIER 
    )
AS
    SET NOCOUNT ON;    
    SELECT  DISTINCT
            t.TermId
           ,t.TermDescrip
           ,r.Descrip
           ,cs.ClsSection
           ,r.ReqId
           ,s.FirstName + ' ' + s.LastName AS StudentName
           ,pv.PrgVerDescrip
           ,sc.StatusCodeDescrip
           ,se.StartDate
           ,se.ExpGradDate
    FROM    arResults ar
    JOIN    arClassSections cs ON cs.ClsSectionId = ar.TestId
    JOIN    arReqs r ON r.ReqId = cs.ReqId
    JOIN    arTerm t ON t.TermId = cs.TermId
    JOIN    arStuEnrollments se ON se.StuEnrollId = ar.StuEnrollId
    JOIN    arStudent s ON s.StudentId = se.StudentId
    JOIN    arPrgVersions pv ON pv.PrgVerId = se.PrgVerId
    JOIN    syStatusCodes sc ON sc.StatusCodeId = se.StatusCodeId
    WHERE   (
              SELECT    COUNT(0)
              FROM      arGrdBkWeights gbw
              JOIN      arGrdBkWgtDetails gbwd ON gbwd.InstrGrdBkWgtId = gbw.InstrGrdBkWgtId
              JOIN      arGrdComponentTypes gct ON gct.GrdComponentTypeId = gbwd.GrdComponentTypeId
              WHERE     gct.SysComponentTypeId IN ( 500,503 )
                        AND gbw.ReqId = cs.ReqId
                        AND gbw.InstrGrdBkWgtId = (
                                                    SELECT TOP 1
                                                            eff_gbw.InstrGrdBkWgtId
                                                    FROM    arGrdBkWeights eff_gbw
                                                    WHERE   eff_gbw.EffectiveDate <= cs.StartDate
                                                            AND eff_gbw.ReqId = cs.ReqId
                                                    ORDER BY eff_gbw.EffectiveDate DESC
                                                  )
            ) > 0
            AND ar.StuEnrollId = @StuEnrollId
    ORDER BY t.TermDescrip
           ,r.Descrip;




GO
