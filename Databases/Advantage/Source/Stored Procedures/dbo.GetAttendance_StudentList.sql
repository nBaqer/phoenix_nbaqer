SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ginzo, John
-- Create date: 05-20-2015
-- Description:	Get the list of students for attendance
-- Modification at 1/20/2016 by JAGG to correct DE12444
-- =============================================
CREATE PROCEDURE [dbo].[GetAttendance_StudentList]
    @TestId UNIQUEIDENTIFIER
   ,@ClsMeetingId UNIQUEIDENTIFIER
AS
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;

        DECLARE @BeginDate DATETIME = (
                                        SELECT  StartDate
                                        FROM    arClassSections
                                        WHERE   ClsSectionId = @TestId
                                      );

        DECLARE @MinGradDate TABLE
            (
             StuEnrollId UNIQUEIDENTIFIER
            );

       
        DECLARE @student UNIQUEIDENTIFIER;

        DECLARE db_cursor CURSOR
        FOR
            SELECT  DISTINCT
                    ( se.StudentId )
            FROM    arResults r
            INNER JOIN arStuEnrollments se ON r.StuEnrollId = se.StuEnrollId
            INNER JOIN arStudent s ON se.StudentId = s.StudentId
            INNER JOIN arClsSectMeetings CSM ON r.TestId = CSM.ClsSectionId
            WHERE   r.TestId = @TestId
                    AND CSM.ClsSectMeetingId = @ClsMeetingId;
					

        OPEN db_cursor;   
        FETCH NEXT FROM db_cursor INTO @student;   

        WHILE @@FETCH_STATUS = 0
            BEGIN   			
                
                DECLARE @stEn UNIQUEIDENTIFIER;
                SET @stEn = (
                              SELECT TOP 1
                                        se.StuEnrollId
                              FROM      dbo.arStuEnrollments se
                              INNER JOIN arResults r ON se.StuEnrollId = r.StuEnrollId
                              INNER JOIN dbo.syStatusCodes sc ON se.StatusCodeId = sc.StatusCodeId
                              INNER JOIN dbo.sySysStatus ss ON sc.SysStatusId = ss.SysStatusId
                              WHERE     se.StudentId = @student
                                        AND r.TestId = @TestId
                                        AND ss.InSchool = 1 -- Correct DE12444
                              ORDER BY  ExpGradDate
                            );

               
			
                IF @stEn IS NOT NULL
                    BEGIN
                        INSERT  INTO @MinGradDate
                                SELECT  @stEn;   				
                    END;
				

                FETCH NEXT FROM db_cursor INTO @student;   
            END;   

        CLOSE db_cursor;   
        DEALLOCATE db_cursor;	            
			
        
			
        SELECT  se.StuEnrollId
               ,s.StudentId
               ,s.LastName
               ,s.FirstName
               ,se.StatusCodeId
               ,se.DateDetermined
               ,sc.StatusCodeDescrip
               ,sc.SysStatusId
               ,se.StartDate
               ,IsFutureStart = ( CASE WHEN ( sc.SysStatusId = 7 ) THEN 'True'
                                       ELSE 'False'
                                  END )
               ,PV.PrgVerDescrip
               ,PV.UseTimeClock
               ,s.SSN
               ,CONVERT(VARCHAR,TM1.TimeIntervalDescrip,108) AS StartTime
               ,CONVERT(VARCHAR,TM2.TimeIntervalDescrip,108) AS EndTime
        FROM    arResults r
        INNER JOIN arStuEnrollments se ON r.StuEnrollId = se.StuEnrollId
        INNER JOIN arStudent s ON se.StudentId = s.StudentId
        INNER JOIN syStatusCodes sc ON se.StatusCodeId = sc.StatusCodeId
        INNER JOIN arPrgVersions PV ON se.PrgVerId = PV.PrgVerId
        INNER JOIN arClsSectMeetings CSM ON r.TestId = CSM.ClsSectionId
        INNER JOIN syPeriods P ON CSM.PeriodId = P.PeriodId
        INNER JOIN dbo.cmTimeInterval TM1 ON P.StartTimeId = TM1.TimeIntervalId
        INNER JOIN dbo.cmTimeInterval TM2 ON P.EndTimeId = TM2.TimeIntervalId
        INNER JOIN @MinGradDate MG ON se.StuEnrollId = MG.StuEnrollId
        WHERE   r.TestId = @TestId
                AND CSM.ClsSectMeetingId = @ClsMeetingId
        ORDER BY s.LastName
               ,s.FirstName;
    END;

GO
