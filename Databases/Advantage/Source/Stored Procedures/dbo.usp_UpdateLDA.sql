SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_UpdateLDA]
    (
     @stuEnrollId UNIQUEIDENTIFIER
	)
AS
    SET NOCOUNT ON;
    UPDATE  arStuEnrollments
    SET     LDA = ISNULL(dbo.GetLDAFromAttendance(@StuEnrollId),LDA)
    WHERE   StuEnrollId = @StuEnrollId;




GO
