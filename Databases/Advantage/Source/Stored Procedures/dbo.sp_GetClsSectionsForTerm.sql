SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[sp_GetClsSectionsForTerm]
    @StuEnrollId VARCHAR(100)
   ,@TermId VARCHAR(100)
AS
    BEGIN  
      
        SET FMTONLY OFF;  
      
        IF OBJECT_ID('tempdb..#tmpClsSectionsForTerm') IS NOT NULL
            DROP TABLE #tmpClsSectionsForTerm;  
      
        CREATE TABLE #tmpClsSectionsForTerm
            (
             ClsSectionID VARCHAR(100)
            ,ClsSection VARCHAR(100)
            );  
      
        IF ( @TermId <> '00000000-0000-0000-0000-000000000000' )
            BEGIN  
      
                INSERT  INTO #tmpClsSectionsForTerm
                        SELECT DISTINCT
                                B.ClsSectionId
                               ,( '(' + R.Code + ')' + ' - ' + R.Descrip ) AS ClsSection
                        FROM    arResults A
                        INNER JOIN arClassSections B ON A.TestId = B.ClsSectionId
                        INNER JOIN arTerm C ON B.TermId = C.TermId
                        INNER JOIN syStatuses S ON C.StatusId = S.StatusId
                        INNER JOIN dbo.arReqs R ON B.ReqId = R.ReqId
                        WHERE   B.TermId = @TermId
                                AND A.StuEnrollId = @StuEnrollId
                        ORDER BY B.ClsSectionId;  
            END;  
      
        ELSE
            BEGIN  
      
                INSERT  INTO #tmpClsSectionsForTerm
                        SELECT DISTINCT
                                B.ClsSectionId
                               ,( '(' + R.Code + ')' + ' - ' + R.Descrip ) AS ClsSection
                        FROM    arResults A
                        INNER JOIN arClassSections B ON A.TestId = B.ClsSectionId
                        INNER JOIN arTerm C ON B.TermId = C.TermId
                        INNER JOIN syStatuses S ON C.StatusId = S.StatusId
                        INNER JOIN dbo.arReqs R ON B.ReqId = R.ReqId
                        WHERE   A.StuEnrollId = @StuEnrollId
                        ORDER BY B.ClsSectionId;  
      
            END;  
      
        IF (
             SELECT COUNT(*)
             FROM   #tmpClsSectionsForTerm
           ) > 1
            BEGIN  
      
                INSERT  INTO #tmpClsSectionsForTerm
                VALUES  ( '00000000-0000-0000-0000-000000000000','All' );  
      
            END;  
      
        SELECT  *
        FROM    #tmpClsSectionsForTerm
        ORDER BY 1;  
      
    END;  
      
GO
