SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- ********************************************************************************************************
-- Usp_TR_GetAcademicType
-- ********************************************************************************************************
CREATE PROCEDURE [dbo].[Usp_TR_GetAcademicType]
    @StuEnrollIdList VARCHAR(MAX) = NULL
   ,@AcademicType NVARCHAR(50) OUTPUT
AS
    DECLARE @ReturnMessage AS NVARCHAR(1000);
    --DECLARE @Message01 AS NVARCHAR(1000)
    DECLARE @Message02 AS NVARCHAR(1000);
    --SET     @Message01 = 'Academic type for selected Enrollments must be same.'
    SET @Message02 = 'Program Version does not have credits, hours defined or is not Continue Education set up.';

    CREATE TABLE #Temp1
        (
         StuEnrollId UNIQUEIDENTIFIER
        ,AcademicType NVARCHAR(50)
        );
 
    BEGIN                                                     
        INSERT  INTO #Temp1
                SELECT  ASE.StuEnrollId
                       ,( CASE WHEN (
                                      APV.Credits > 0.0
                                      AND APV.Hours > 0
                                    )
                                    OR ( APV.IsContinuingEd = 1 ) THEN 'Credits-ClockHours'
                               WHEN APV.Credits > 0.0 THEN 'Credits'
                               WHEN APV.Hours > 0.0 THEN 'ClockHours'
                               ELSE ''
                          END )
                FROM    arPrgVersions AS APV
                INNER JOIN arStuEnrollments AS ASE ON ASE.PrgVerId = APV.PrgVerId
                WHERE   ASE.StuEnrollId IN ( SELECT Val
                                             FROM   MultipleValuesForReportParameters(@StuEnrollIdList,',',1) );

        SELECT TOP 1
                @AcademicType = T1.AcademicType
        FROM    #Temp1 AS T1
        ORDER BY T1.AcademicType;

        IF ( @AcademicType = '' )
            BEGIN
                SET @AcademicType = @Message02; --   'Program Version does not have credits or hours defined.'
            END;
        --ELSE
        --    BEGIN
        --        IF EXISTS ( SELECT 1
        --                    FROM #Temp1 AS T1 
        --                    WHERE T1.AcademicType <> @AcademicType
        --                  )
        --            BEGIN
        --                SET @AcademicType  = @Message01 --   'Academic type for selected Enrollments must be same.'
        --            END
        --END
    END;
-- ********************************************************************************************************
-- END  --  Usp_TR_GetAcademicType
-- ********************************************************************************************************   

GO
