SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- ========================================================================================================= 
-- Usp_PR_Sub4_Courses_forOneTermAllEnrolmnents 
-- ========================================================================================================= 
CREATE PROCEDURE [dbo].[Usp_PR_Sub4_Courses_forOneTermAllEnrolmnents]
    @StuEnrollIdList VARCHAR(MAX)
   ,@TermId VARCHAR(50) = NULL
   ,@SysComponentTypeId VARCHAR(50) = NULL
AS -- source usp_pr_main_ssrs_subreport3_byterm  
    BEGIN
        DECLARE @GradesFormat AS VARCHAR(50);
        DECLARE @GPAMethod AS VARCHAR(50);
        DECLARE @GradeBookAt AS VARCHAR(50);
        DECLARE @StuEnrollCampusId AS UNIQUEIDENTIFIER;
        DECLARE @Counter AS INT;
        DECLARE @times AS INT;
        DECLARE @TermStartDate1 AS DATETIME;
        DECLARE @Score AS DECIMAL(18, 2);
        DECLARE @GrdCompDescrip AS VARCHAR(50);

        DECLARE @curId AS UNIQUEIDENTIFIER;
        DECLARE @curReqId AS UNIQUEIDENTIFIER;
        DECLARE @curStuEnrollId AS UNIQUEIDENTIFIER;
        DECLARE @curDescrip AS VARCHAR(50);
        DECLARE @curNumber AS INT;
        DECLARE @curGrdComponentTypeId AS INT;
        DECLARE @curMinResult AS DECIMAL(18, 2);
        DECLARE @curGrdComponentDescription AS VARCHAR(50);
        DECLARE @curClsSectionId AS UNIQUEIDENTIFIER;
        DECLARE @curRownumber AS INT;


        CREATE TABLE #tempTermWorkUnitCount
            (
                TermId UNIQUEIDENTIFIER
               ,ReqId UNIQUEIDENTIFIER
               ,sysComponentTypeId INT
               ,WorkUnitCount INT
            );
        CREATE TABLE #Temp21
            (
                Id UNIQUEIDENTIFIER
               ,TermId UNIQUEIDENTIFIER
               ,ReqId UNIQUEIDENTIFIER
               ,GradeBookDescription VARCHAR(50)
               ,Number INT
               ,GradeBookSysComponentTypeId INT
               ,GradeBookScore DECIMAL(18, 2)
               ,MinResult DECIMAL(18, 2)
               ,GradeComponentDescription VARCHAR(50)
               ,ClsSectionId UNIQUEIDENTIFIER
               ,StuEnrollId UNIQUEIDENTIFIER
               ,RowNumber INT
            );
        CREATE TABLE #Temp22
            (
                ReqId UNIQUEIDENTIFIER
               ,EffectiveDate DATETIME
            );
        SET @StuEnrollCampusId = COALESCE((
                                          SELECT TOP 1 ASE.CampusId
                                          FROM   arStuEnrollments AS ASE
                                          WHERE  ASE.StuEnrollId IN (
                                                                    SELECT Val
                                                                    FROM   MultipleValuesForReportParameters(@StuEnrollIdList, ',', 1)
                                                                    )
                                          )
                                         ,NULL
                                         );
        SET @GradesFormat = dbo.GetAppSettingValueByKeyName('GradesFormat', @StuEnrollCampusId);
        IF ( @GradesFormat IS NOT NULL )
            BEGIN
                SET @GradesFormat = LOWER(LTRIM(RTRIM(@GradesFormat)));
            END;
        SET @GPAMethod = dbo.GetAppSettingValueByKeyName('GPAMethod', @StuEnrollCampusId);
        IF ( @GPAMethod IS NOT NULL )
            BEGIN
                SET @GPAMethod = LOWER(LTRIM(RTRIM(@GPAMethod)));
            END;
        SET @GradeBookAt = dbo.GetAppSettingValueByKeyName('GradeBookWeightingLevel', @StuEnrollCampusId);
        IF ( @GradeBookAt IS NOT NULL )
            BEGIN
                SET @GradeBookAt = LOWER(LTRIM(RTRIM(@GradeBookAt)));
            END;

        IF LOWER(@GradeBookAt) = 'instructorlevel'
            BEGIN
                INSERT INTO #tempTermWorkUnitCount
                            SELECT   dt.TermId
                                    ,dt.ReqId
                                    ,dt.GradeBookSysComponentTypeId
                                    ,COUNT(dt.GradeBookDescription)
                            FROM     (
                                     SELECT     d.ReqId
                                               ,d.TermId
                                               ,CASE WHEN a.Descrip IS NULL THEN e.Descrip
                                                     --THEN (  
                                                     --      SELECT   Resource  
                                                     --      FROM     syResources  
                                                     --      WHERE    ResourceID = e.SysComponentTypeId  
                                                     --     )  
                                                     ELSE a.Descrip
                                                END AS GradeBookDescription
                                               ,( CASE e.SysComponentTypeId
                                                       WHEN 500 THEN a.Number
                                                       WHEN 503 THEN a.Number
                                                       WHEN 544 THEN a.Number
                                                       ELSE (
                                                            SELECT MIN(MinVal)
                                                            FROM   arGradeScaleDetails GSD
                                                                  ,arGradeSystemDetails GSS
                                                            WHERE  GSD.GrdSysDetailId = GSS.GrdSysDetailId
                                                                   AND GSS.IsPass = 1
                                                                   AND GSD.GrdScaleId = d.GrdScaleId
                                                            )
                                                  END
                                                ) AS MinResult
                                               ,a.Required
                                               ,a.MustPass
                                               ,ISNULL(e.SysComponentTypeId, 0) AS GradeBookSysComponentTypeId
                                               ,a.Number
                                               ,(
                                                SELECT Resource
                                                FROM   syResources
                                                WHERE  ResourceID = e.SysComponentTypeId
                                                ) AS GradeComponentDescription
                                               ,a.InstrGrdBkWgtDetailId
                                               ,c.StuEnrollId
                                               ,0 AS IsExternShip
                                               ,CASE e.SysComponentTypeId
                                                     WHEN 544 THEN (
                                                                   SELECT ISNULL(SUM(HoursAttended), '0.00')
                                                                   FROM   arExternshipAttendance
                                                                   WHERE  StuEnrollId = c.StuEnrollId
                                                                   )
                                                     ELSE (
                                                          SELECT SUM(AGBR.Score)
                                                          FROM   arGrdBkResults AS AGBR
                                                          WHERE  AGBR.StuEnrollId = c.StuEnrollId
                                                                 AND AGBR.InstrGrdBkWgtDetailId = a.InstrGrdBkWgtDetailId
                                                                 AND AGBR.ClsSectionId = d.ClsSectionId
                                                          )
                                                END AS GradeBookScore
                                               ,ROW_NUMBER() OVER ( PARTITION BY ST.StuEnrollId
                                                                                --T.TermId, R.ReqId ORDER BY ST.CampusId, T.StartDate, T.EndDate, T.TermId, T.TermDescrip, R.ReqId, R.Descrip, RES.Resource, e.Descrip) AS rownumber  
                                                                                ,T.TermId
                                                                                ,R.ReqId
                                                                    ORDER BY e.SysComponentTypeId
                                                                            ,a.Descrip
                                                                  ) AS rownumber
                                     FROM       arGrdBkWgtDetails AS a
                                     INNER JOIN arGrdBkWeights AS b ON b.InstrGrdBkWgtId = a.InstrGrdBkWgtId
                                     INNER JOIN arClassSections AS d ON d.InstrGrdBkWgtId = a.InstrGrdBkWgtId
                                     INNER JOIN arResults AS c ON c.TestId = d.ClsSectionId
                                     INNER JOIN arGrdComponentTypes AS e ON e.GrdComponentTypeId = a.GrdComponentTypeId
                                     INNER JOIN arStuEnrollments AS ST ON ST.StuEnrollId = c.StuEnrollId
                                     INNER JOIN arTerm AS T ON T.TermId = d.TermId
                                     INNER JOIN arReqs AS R ON R.ReqId = d.ReqId
                                     INNER JOIN syResources AS RES ON RES.ResourceID = e.SysComponentTypeId
                                     WHERE      c.StuEnrollId IN (
                                                                 SELECT Val
                                                                 FROM   MultipleValuesForReportParameters(@StuEnrollIdList, ',', 1)
                                                                 )
                                                AND (
                                                    @SysComponentTypeId IS NULL
                                                    OR e.SysComponentTypeId IN (
                                                                               SELECT Val
                                                                               FROM   MultipleValuesForReportParameters(@SysComponentTypeId, ',', 1)
                                                                               )
                                                    )
                                     UNION
                                     SELECT     R.ReqId
                                               ,T.TermId
                                               ,CASE WHEN GBW.Descrip IS NULL THEN (
                                                                                   SELECT Resource
                                                                                   FROM   syResources
                                                                                   WHERE  ResourceID = GCT.SysComponentTypeId
                                                                                   )
                                                     ELSE GBW.Descrip
                                                END AS GradeBookDescription
                                               ,( CASE GCT.SysComponentTypeId
                                                       WHEN 500 THEN GBWD.Number
                                                       WHEN 503 THEN GBWD.Number
                                                       WHEN 544 THEN GBWD.Number
                                                       ELSE (
                                                            SELECT MIN(MinVal)
                                                            FROM   arGradeScaleDetails GSD
                                                                  ,arGradeSystemDetails GSS
                                                            WHERE  GSD.GrdSysDetailId = GSS.GrdSysDetailId
                                                                   AND GSS.IsPass = 1
                                                                   AND GSD.GrdScaleId = CSC.GrdScaleId
                                                            )
                                                  END
                                                ) AS MinResult
                                               ,GBWD.Required
                                               ,GBWD.MustPass
                                               ,ISNULL(GCT.SysComponentTypeId, 0) AS GradeBookSysComponentTypeId
                                               ,GBWD.Number
                                               ,(
                                                SELECT Resource
                                                FROM   syResources
                                                WHERE  ResourceID = GCT.SysComponentTypeId
                                                ) AS GradeComponentDescription
                                               ,GBWD.InstrGrdBkWgtDetailId
                                               ,GBCR.StuEnrollId
                                               ,0 AS IsExternShip
                                               ,CASE GCT.SysComponentTypeId
                                                     WHEN 544 THEN (
                                                                   SELECT ISNULL(SUM(HoursAttended), '0.00')
                                                                   FROM   arExternshipAttendance
                                                                   WHERE  StuEnrollId = SE.StuEnrollId
                                                                   )
                                                     ELSE (
                                                          SELECT ISNULL(SUM(AGBR.Score), 0.00)
                                                          FROM   arGrdBkResults AS AGBR
                                                          WHERE  AGBR.StuEnrollId = SE.StuEnrollId
                                                                 AND InstrGrdBkWgtDetailId = GBWD.InstrGrdBkWgtDetailId
                                                                 AND ClsSectionId = CSC.ClsSectionId
                                                          )
                                                END AS GradeBookScore
                                               ,ROW_NUMBER() OVER ( PARTITION BY SE.StuEnrollId
                                                                                ,T.TermId
                                                                                ,R.ReqId
                                                                    ORDER BY SE.CampusId
                                                                            ,T.StartDate
                                                                            ,T.EndDate
                                                                            ,T.TermId
                                                                            ,T.TermDescrip
                                                                            ,R.ReqId
                                                                            ,R.Descrip
                                                                            ,SYRES.Resource
                                                                            ,GCT.Descrip
                                                                  ) AS rownumber
                                     FROM       arGrdBkConversionResults GBCR
                                     INNER JOIN arStuEnrollments SE ON GBCR.StuEnrollId = SE.StuEnrollId
                                     INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                                     INNER JOIN arTerm T ON GBCR.TermId = T.TermId
                                     INNER JOIN arReqs R ON GBCR.ReqId = R.ReqId
                                     INNER JOIN arGrdComponentTypes GCT ON GCT.GrdComponentTypeId = GBCR.GrdComponentTypeId
                                     INNER JOIN arGrdBkWgtDetails GBWD ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
                                                                          AND GBWD.GrdComponentTypeId = GBCR.GrdComponentTypeId
                                     INNER JOIN arGrdBkWeights GBW ON GBWD.InstrGrdBkWgtId = GBW.InstrGrdBkWgtId
                                                                      AND GBCR.ReqId = GBW.ReqId
                                     INNER JOIN (
                                                SELECT   ReqId
                                                        ,MAX(EffectiveDate) AS EffectiveDate
                                                FROM     arGrdBkWeights
                                                GROUP BY ReqId
                                                ) AS MaxEffectiveDatesByCourse ON GBCR.ReqId = MaxEffectiveDatesByCourse.ReqId
                                     INNER JOIN (
                                                SELECT Resource
                                                      ,ResourceID
                                                FROM   syResources
                                                WHERE  ResourceTypeID = 10
                                                ) SYRES ON SYRES.ResourceID = GCT.SysComponentTypeId
                                     INNER JOIN syCampuses C ON SE.CampusId = C.CampusId
                                     INNER JOIN arClassSections CSC ON CSC.TermId = T.TermId
                                                                       AND CSC.ReqId = R.ReqId
                                     WHERE      MaxEffectiveDatesByCourse.EffectiveDate <= T.StartDate
                                                AND SE.StuEnrollId IN (
                                                                      SELECT Val
                                                                      FROM   MultipleValuesForReportParameters(@StuEnrollIdList, ',', 1)
                                                                      )
                                                AND (
                                                    @SysComponentTypeId IS NULL
                                                    OR GCT.SysComponentTypeId IN (
                                                                                 SELECT Val
                                                                                 FROM   MultipleValuesForReportParameters(@SysComponentTypeId, ',', 1)
                                                                                 )
                                                    )
                                     ) AS dt
                            GROUP BY dt.TermId
                                    ,dt.ReqId
                                    ,dt.GradeBookSysComponentTypeId;
            END;
        ELSE
            BEGIN
                SET @Counter = 0;
                SET @TermStartDate1 = (
                                      SELECT AT.StartDate
                                      FROM   dbo.arTerm AS AT
                                      WHERE  AT.TermId = @TermId
                                      );
                INSERT INTO #Temp22
                            SELECT     AGBW.ReqId
                                      ,MAX(AGBW.EffectiveDate) AS EffectiveDate
                            FROM       dbo.arGrdBkWeights AS AGBW
                            INNER JOIN dbo.syCreditSummary AS SCS ON SCS.ReqId = AGBW.ReqId
                            WHERE      SCS.TermId = @TermId
                                       AND SCS.StuEnrollId IN (
                                                              SELECT Val
                                                              FROM   dbo.MultipleValuesForReportParameters(@StuEnrollIdList, ',', 1)
                                                              )
                                       AND AGBW.EffectiveDate <= @TermStartDate1
                            GROUP BY   AGBW.ReqId;

                DECLARE getUsers_Cursor CURSOR FOR
                    SELECT   dt.ID
                            ,dt.ReqId
                            ,dt.Descrip
                            ,dt.Number
                            ,dt.SysComponentTypeId
                            ,dt.MinResult
                            ,dt.GradeComponentDescription
                            ,dt.ClsSectionId
                            ,dt.StuEnrollId
                            ,ROW_NUMBER() OVER ( PARTITION BY dt.StuEnrollId
                                                             ,@TermId
                                                             ,dt.SysComponentTypeId
                                                 ORDER BY dt.SysComponentTypeId
                                                         ,dt.Descrip
                                               ) AS rownumber
                    FROM     (
                             SELECT     ISNULL(AGBWD.InstrGrdBkWgtDetailId, NEWID()) AS ID
                                       ,AR.ReqId
                                       ,AGBWD.Descrip
                                       ,AGBWD.Number
                                       ,AGCT.SysComponentTypeId
                                       ,( CASE WHEN AGCT.SysComponentTypeId IN ( 500, 503, 544 ) THEN AGBWD.Number
                                               ELSE (
                                                    SELECT     MIN(AGSD.MinVal) AS MinVal
                                                    FROM       dbo.arGradeScaleDetails AS AGSD
                                                    INNER JOIN dbo.arGradeSystemDetails AS AGSDetails ON AGSDetails.GrdSysDetailId = AGSD.GrdSysDetailId
                                                    WHERE      AGSDetails.IsPass = 1
                                                               AND AGSD.GrdScaleId = ACS.GrdScaleId
                                                    )
                                          END
                                        ) AS MinResult
                                       ,SR.Resource AS GradeComponentDescription
                                       ,ACS.ClsSectionId
                                       ,AGBR.StuEnrollId
                             FROM       dbo.arGrdBkResults AS AGBR
                             INNER JOIN dbo.arGrdBkWgtDetails AS AGBWD ON AGBWD.InstrGrdBkWgtDetailId = AGBR.InstrGrdBkWgtDetailId
                             INNER JOIN dbo.arGrdBkWeights AS AGBW ON AGBW.InstrGrdBkWgtId = AGBWD.InstrGrdBkWgtId
                             INNER JOIN dbo.arGrdComponentTypes AS AGCT ON AGCT.GrdComponentTypeId = AGBWD.GrdComponentTypeId
                             INNER JOIN dbo.syResources AS SR ON SR.ResourceID = AGCT.SysComponentTypeId
                             --INNER JOIN #Temp22 AS T2 ON T2.ReqId = AGBW.ReqId 
                             INNER JOIN dbo.arReqs AS AR ON AR.ReqId = AGBW.ReqId
                             INNER JOIN dbo.arClassSections AS ACS ON ACS.ReqId = AR.ReqId
                                                                      AND ACS.ClsSectionId = AGBR.ClsSectionId
                             INNER JOIN dbo.arResults AS AR2 ON AR2.TestId = ACS.ClsSectionId
                                                                AND AR2.StuEnrollId = AGBR.StuEnrollId --AND AR2.TestId = AGBR.ClsSectionId 

                             WHERE      (
                                        @StuEnrollIdList IS NULL
                                        OR AGBR.StuEnrollId IN (
                                                               SELECT Val
                                                               FROM   dbo.MultipleValuesForReportParameters(@StuEnrollIdList, ',', 1)
                                                               )
                                        )
                                        --AND T2.EffectiveDate = AGBW.EffectiveDate 
                                        AND AGBWD.Number > 0
                             ) dt
                    ORDER BY SysComponentTypeId
                            ,rownumber;
                OPEN getUsers_Cursor;
                FETCH NEXT FROM getUsers_Cursor
                INTO @curId
                    ,@curReqId
                    ,@curDescrip
                    ,@curNumber
                    ,@curGrdComponentTypeId
                    ,@curMinResult
                    ,@curGrdComponentDescription
                    ,@curClsSectionId
                    ,@curStuEnrollId
                    ,@curRownumber;
                SET @Counter = 0;
                WHILE @@FETCH_STATUS = 0
                    BEGIN
                        --PRINT @curNumber  
                        SET @times = 1;
                        WHILE @times <= @curNumber
                            BEGIN
                                --  PRINT @times  
                                IF @curNumber > 1
                                    BEGIN
                                        SET @GrdCompDescrip = @curDescrip + CAST(@times AS CHAR);


                                        IF @curGrdComponentTypeId = 544
                                            BEGIN
                                                SET @Score = (
                                                             SELECT ISNULL(SUM(HoursAttended), '0.00')
                                                             FROM   arExternshipAttendance
                                                             WHERE  StuEnrollId = @curStuEnrollId
                                                             );
                                            END;
                                        ELSE
                                            BEGIN
                                                SET @Score = (
                                                             SELECT ISNULL(SUM(Score), 0.00)
                                                             FROM   arGrdBkResults
                                                             WHERE  StuEnrollId = @curStuEnrollId
                                                                    AND InstrGrdBkWgtDetailId = @curId
                                                                    AND ResNum = @times
                                                                    AND ClsSectionId = @curClsSectionId
                                                             );
                                            END;

                                        SET @curRownumber = @times;
                                    END;
                                ELSE
                                    BEGIN
                                        SET @GrdCompDescrip = @curDescrip;

                                        IF @curGrdComponentTypeId = 544
                                            BEGIN
                                                SET @Score = (
                                                             SELECT ISNULL(SUM(HoursAttended), '0.00')
                                                             FROM   arExternshipAttendance
                                                             WHERE  StuEnrollId = @curStuEnrollId
                                                             );
                                            END;
                                        ELSE
                                            BEGIN
                                                SET @Score = (
                                                             SELECT ISNULL(SUM(Score), 0.00)
                                                             FROM   arGrdBkResults
                                                             WHERE  StuEnrollId = @curStuEnrollId
                                                                    AND InstrGrdBkWgtDetailId = @curId
                                                                    AND ResNum = @times
                                                             );
                                            END;


                                    END;
                                --PRINT @Score  
                                INSERT INTO #Temp21
                                VALUES ( @curId, @TermId, @curReqId, @GrdCompDescrip, @curNumber, @curGrdComponentTypeId, @Score, @curMinResult
                                        ,@curGrdComponentDescription, @curClsSectionId, @curStuEnrollId, @curRownumber );

                                SET @times = @times + 1;
                            END;
                        FETCH NEXT FROM getUsers_Cursor
                        INTO @curId
                            ,@curReqId
                            ,@curDescrip
                            ,@curNumber
                            ,@curGrdComponentTypeId
                            ,@curMinResult
                            ,@curGrdComponentDescription
                            ,@curClsSectionId
                            ,@curStuEnrollId
                            ,@curRownumber;
                    END;
                CLOSE getUsers_Cursor;
                DEALLOCATE getUsers_Cursor;

                INSERT INTO #tempTermWorkUnitCount
                            SELECT   dt.TermId
                                    ,dt.ReqId
                                    ,dt.GradeBookSysComponentTypeId
                                    ,COUNT(dt.GradeBookDescription)
                            FROM     (
                                     SELECT T21.TermId
                                           ,T21.ReqId
                                           ,T21.GradeBookDescription
                                           ,T21.GradeBookSysComponentTypeId
                                     FROM   #Temp21 AS T21
                                     UNION
                                     SELECT     T.TermId
                                               ,GBCR.ReqId
                                               ,GCT.Descrip AS GradeBookDescription
                                               ,GCT.SysComponentTypeId AS GradeBookSysComponentTypeId
                                     FROM       arGrdBkConversionResults GBCR
                                     INNER JOIN arStuEnrollments SE ON GBCR.StuEnrollId = SE.StuEnrollId
                                     INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                                     INNER JOIN arTerm T ON GBCR.TermId = T.TermId
                                     INNER JOIN arReqs R ON GBCR.ReqId = R.ReqId
                                     INNER JOIN arGrdComponentTypes GCT ON GCT.GrdComponentTypeId = GBCR.GrdComponentTypeId
                                     INNER JOIN arGrdBkWgtDetails GBWD ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
                                                                          AND GBWD.GrdComponentTypeId = GBCR.GrdComponentTypeId
                                     INNER JOIN arGrdBkWeights GBW ON GBWD.InstrGrdBkWgtId = GBW.InstrGrdBkWgtId
                                                                      AND GBCR.ReqId = GBW.ReqId
                                     INNER JOIN (
                                                SELECT   ReqId
                                                        ,MAX(EffectiveDate) AS EffectiveDate
                                                FROM     arGrdBkWeights
                                                GROUP BY ReqId
                                                ) AS MED ON MED.ReqId = GBCR.ReqId -- MED --> MaxEffectiveDatesByCourse  
                                     INNER JOIN (
                                                SELECT Resource
                                                      ,ResourceID
                                                FROM   syResources
                                                WHERE  ResourceTypeID = 10
                                                ) SYRES ON SYRES.ResourceID = GCT.SysComponentTypeId
                                     INNER JOIN syCampuses C ON SE.CampusId = C.CampusId
                                     WHERE      MED.EffectiveDate <= T.StartDate
                                                AND SE.StuEnrollId IN (
                                                                      SELECT Val
                                                                      FROM   MultipleValuesForReportParameters(@StuEnrollIdList, ',', 1)
                                                                      )
                                                AND T.TermId = @TermId
                                                AND (
                                                    @SysComponentTypeId IS NULL
                                                    OR GCT.SysComponentTypeId IN (
                                                                                 SELECT Val
                                                                                 FROM   MultipleValuesForReportParameters(@SysComponentTypeId, ',', 1)
                                                                                 )
                                                    )
                                     ) dt
                            GROUP BY dt.TermId
                                    ,dt.ReqId
                                    ,dt.GradeBookSysComponentTypeId;
            END;

        DROP TABLE #Temp22;
        DROP TABLE #Temp21;

        -- SELECT * FROM #tempTermWorkUnitCount AS TTWUC  ORDER BY TTWUC.TermId  -- , TTWUC.StuEnrollId  


        SELECT   dt1.PrgVerId
                ,dt1.PrgVerDescrip
                ,dt1.PrgVersionTrackCredits
                ,dt1.TermId
                ,dt1.TermDescription
                ,dt1.TermStartDate
                ,dt1.TermEndDate
                ,dt1.CourseId
                ,dt1.CouseStartDate
                ,dt1.CourseCode
                ,dt1.CourseDescription
                ,dt1.CourseCodeDescription
                ,dt1.CourseCredits
                ,dt1.CourseFinAidCredits
                ,dt1.MinVal
                ,dt1.CourseScore
                ,dt1.GradeBook_ResultId
                ,dt1.GradeBookDescription
                ,dt1.GradeBookScore
                ,dt1.GradeBookPostDate
                ,dt1.GradeBookPassingGrade
                ,dt1.GradeBookWeight
                ,dt1.GradeBookRequired
                ,dt1.GradeBookMustPass
                ,dt1.GradeBookSysComponentTypeId
                ,dt1.GradeBookHoursRequired
                ,dt1.GradeBookHoursCompleted
                ,dt1.StuEnrollId
                ,dt1.MinResult
                ,dt1.GradeComponentDescription -- Student data     
                ,dt1.CreditsAttempted
                ,dt1.CreditsEarned
                ,dt1.Completed
                ,dt1.CurrentScore
                ,dt1.CurrentGrade
                ,CASE WHEN (
                           SELECT TOP 1 P.ACId
                           FROM   dbo.arStuEnrollments E
                           JOIN   dbo.arPrgVersions PV ON PV.PrgVerId = E.PrgVerId
                           JOIN   dbo.arPrograms P ON P.ProgId = PV.ProgId
                           WHERE  E.StuEnrollId = dt1.StuEnrollId
                           ) = 5
                           AND @GradesFormat = 'numeric' THEN dbo.CalculateStudentAverage(dt1.StuEnrollId, NULL, NULL, dt1.ClsSectionId, NULL, NULL)
                      ELSE dt1.FinalScore
                 END AS FinalScore
                ,dt1.FinalGrade
                ,dt1.CampusId
                ,dt1.CampDescrip
                ,dt1.rownumber
                ,dt1.FirstName
                ,dt1.LastName
                ,dt1.MiddleName
                ,dt1.GrdBkWgtDetailsCount
                ,dt1.ClockHourProgram
                ,dt1.GradesFormat
                ,dt1.GPAMethod
                ,dt1.WorkUnitCount
                ,dt1.WorkUnitCount_501
                ,dt1.WorkUnitCount_544
                ,dt1.WorkUnitCount_502
                ,dt1.WorkUnitCount_499
                ,dt1.WorkUnitCount_503
                ,dt1.WorkUnitCount_500
                ,dt1.WorkUnitCount_533
                ,dt1.rownumber2
        FROM     (
                 SELECT dt.PrgVerId
                       ,dt.PrgVerDescrip
                       ,dt.PrgVersionTrackCredits
                       ,dt.TermId
                       ,dt.TermDescription
                       ,dt.TermStartDate
                       ,dt.TermEndDate
                       ,dt.CourseId
                       ,dt.CouseStartDate
                       ,dt.CourseCode
                       ,dt.CourseDescription
                       ,dt.CourseCodeDescription
                       ,dt.CourseCredits
                       ,dt.CourseFinAidCredits
                       ,dt.MinVal
                       ,dt.CourseScore
                       ,dt.GradeBook_ResultId
                       ,dt.GradeBookDescription
                       ,dt.GradeBookScore
                       ,dt.GradeBookPostDate
                       ,dt.GradeBookPassingGrade
                       ,dt.GradeBookWeight
                       ,dt.GradeBookRequired
                       ,dt.GradeBookMustPass
                       ,dt.GradeBookSysComponentTypeId
                       ,dt.GradeBookHoursRequired
                       ,dt.GradeBookHoursCompleted
                       ,dt.StuEnrollId
                       ,dt.MinResult
                       ,dt.GradeComponentDescription -- Student data     
                       ,dt.CreditsAttempted
                       ,dt.CreditsEarned
                       ,dt.Completed
                       ,dt.CurrentScore
                       ,dt.CurrentGrade
                       ,dt.FinalScore
                       ,dt.FinalGrade
                       ,dt.CampusId
                       ,dt.CampDescrip
                       ,dt.rownumber
                       ,dt.FirstName
                       ,dt.LastName
                       ,dt.MiddleName
                       ,dt.GrdBkWgtDetailsCount
                       ,dt.ClockHourProgram
                       ,@GradesFormat AS GradesFormat
                       ,@GPAMethod AS GPAMethod
                       ,(
                        SELECT SUM(WorkUnitCount)
                        FROM   #tempTermWorkUnitCount AS T
                        WHERE  T.TermId = dt.TermId
                               AND T.ReqId = dt.CourseId
                               AND sysComponentTypeId IN ( 501, 544, 502, 499, 503, 500, 533 )
                        ) AS WorkUnitCount
                       ,(
                        SELECT WorkUnitCount
                        FROM   #tempTermWorkUnitCount AS T
                        WHERE  TermId = dt.TermId
                               AND T.ReqId = dt.CourseId
                               AND sysComponentTypeId = 501
                        ) AS WorkUnitCount_501
                       ,(
                        SELECT WorkUnitCount
                        FROM   #tempTermWorkUnitCount AS T
                        WHERE  TermId = dt.TermId
                               AND T.ReqId = dt.CourseId
                               AND sysComponentTypeId = 544
                        ) AS WorkUnitCount_544
                       ,(
                        SELECT WorkUnitCount
                        FROM   #tempTermWorkUnitCount AS T
                        WHERE  TermId = dt.TermId
                               AND T.ReqId = dt.CourseId
                               AND sysComponentTypeId = 502
                        ) AS WorkUnitCount_502
                       ,(
                        SELECT WorkUnitCount
                        FROM   #tempTermWorkUnitCount AS T
                        WHERE  TermId = dt.TermId
                               AND T.ReqId = dt.CourseId
                               AND sysComponentTypeId = 499
                        ) AS WorkUnitCount_499
                       ,(
                        SELECT WorkUnitCount
                        FROM   #tempTermWorkUnitCount AS T
                        WHERE  TermId = dt.TermId
                               AND T.ReqId = dt.CourseId
                               AND sysComponentTypeId = 503
                        ) AS WorkUnitCount_503
                       ,(
                        SELECT WorkUnitCount
                        FROM   #tempTermWorkUnitCount AS T
                        WHERE  TermId = dt.TermId
                               AND T.ReqId = dt.CourseId
                               AND sysComponentTypeId = 500
                        ) AS WorkUnitCount_500
                       ,(
                        SELECT WorkUnitCount
                        FROM   #tempTermWorkUnitCount AS T
                        WHERE  TermId = dt.TermId
                               AND T.ReqId = dt.CourseId
                               AND sysComponentTypeId = 533
                        ) AS WorkUnitCount_533
                       ,ROW_NUMBER() OVER ( PARTITION BY StuEnrollId
                                                        ,TermId
                                                        ,CourseId
                                            ORDER BY TermStartDate
                                                    ,TermEndDate
                                                    ,TermDescription
                                                    ,CourseDescription
                                          ) AS rownumber2
                       ,dt.ClsSectionId
					   ,dt.ReqSeq
                 FROM   (
                        SELECT     DISTINCT PV.PrgVerId
                                           ,PV.PrgVerDescrip
                                           ,CASE WHEN ( PV.Credits > 0.0 ) THEN 1
                                                 ELSE 0
                                            END AS PrgVersionTrackCredits
                                           ,T.TermId
                                           ,T.TermDescrip AS TermDescription
                                           ,T.StartDate AS TermStartDate
                                           ,T.EndDate AS TermEndDate
                                           ,CS.ReqId AS CourseId
                                           ,CS.StartDate AS CouseStartDate
                                           ,R.Code AS CourseCode
                                           ,R.Descrip AS CourseDescription
                                           ,'(' + R.Code + ') ' + R.Descrip AS CourseCodeDescription
                                           ,R.Credits AS CourseCredits
                                           ,R.FinAidCredits AS CourseFinAidCredits
                                           ,(
                                            SELECT MIN(MinVal)
                                            FROM   arGradeScaleDetails GCD
                                                  ,arGradeSystemDetails GSD
                                            WHERE  GCD.GrdSysDetailId = GSD.GrdSysDetailId
                                                   AND GSD.IsPass = 1
                                                   AND GCD.GrdScaleId = CS.GrdScaleId
                                            ) AS MinVal
                                           ,RES.Score AS CourseScore
                                           ,NULL AS GradeBook_ResultId
                                           ,NULL AS GradeBookDescription
                                           ,NULL AS GradeBookScore
                                           ,NULL AS GradeBookPostDate
                                           ,NULL AS GradeBookPassingGrade
                                           ,NULL AS GradeBookWeight
                                           ,NULL AS GradeBookRequired
                                           ,NULL AS GradeBookMustPass
                                           ,NULL AS GradeBookSysComponentTypeId
                                           ,NULL AS GradeBookHoursRequired
                                           ,NULL AS GradeBookHoursCompleted
                                           ,SE.StuEnrollId
                                           ,NULL AS MinResult
                                           ,NULL AS GradeComponentDescription -- Student data     
                                           ,ISNULL(SCS.CreditsAttempted, 0) AS CreditsAttempted
                                           ,ISNULL(SCS.CreditsEarned, 0) AS CreditsEarned
                                           ,SCS.Completed AS Completed
                                           ,SCS.CurrentScore AS CurrentScore
                                           ,SCS.CurrentGrade AS CurrentGrade
                                           ,SCS.FinalScore AS FinalScore
                                           ,SCS.FinalGrade AS FinalGrade
                                           ,C.CampusId
                                           ,C.CampDescrip
                                           ,NULL AS rownumber
                                           ,S.FirstName AS FirstName
                                           ,S.LastName AS LastName
                                           ,S.MiddleName
                                           ,(
                                            SELECT COUNT(*) AS GrdBkWgtDetailsCount
                                            FROM   arGrdBkResults
                                            WHERE  StuEnrollId = SE.StuEnrollId
                                                   AND ClsSectionId = RES.TestId
                                            ) AS GrdBkWgtDetailsCount
                                           ,CASE WHEN P.ACId = 5 THEN 'True'
                                                 ELSE 'False'
                                            END AS ClockHourProgram
                                           ,CS.ClsSectionId
										    ,PVD.ReqSeq
                        FROM       arClassSections CS
                        INNER JOIN arResults GBR ON CS.ClsSectionId = GBR.TestId
                        INNER JOIN arStuEnrollments SE ON GBR.StuEnrollId = SE.StuEnrollId
                        INNER JOIN (
                                   SELECT StudentId
                                         ,FirstName
                                         ,LastName
                                         ,MiddleName
                                   FROM   adLeads
                                   ) S ON S.StudentId = SE.StudentId
                        INNER JOIN syCampuses C ON SE.CampusId = C.CampusId
                        INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                        INNER JOIN arPrograms P ON P.ProgId = PV.ProgId
                        INNER JOIN arTerm T ON CS.TermId = T.TermId
                        INNER JOIN arReqs R ON CS.ReqId = R.ReqId
						      LEFT JOIN  dbo.arProgVerDef PVD ON PVD.PrgVerId = PV.PrgVerId
                                                       AND PVD.ReqId = R.ReqId
                        INNER JOIN arResults RES ON RES.StuEnrollId = GBR.StuEnrollId
                                                    AND RES.TestId = CS.ClsSectionId
                        LEFT JOIN  syCreditSummary SCS ON SE.StuEnrollId = SCS.StuEnrollId
                                                          AND T.TermId = SCS.TermId
                                                          AND R.ReqId = SCS.ReqId
                        WHERE      SE.StuEnrollId IN (
                                                     SELECT Val
                                                     FROM   MultipleValuesForReportParameters(@StuEnrollIdList, ',', 1)
                                                     )
                                   AND (
                                       @TermId IS NULL
                                       OR T.TermId = @TermId
                                       )
                                   AND R.IsAttendanceOnly = 0
                        UNION
                        SELECT     DISTINCT PV.PrgVerId
                                           ,PV.PrgVerDescrip
                                           ,CASE WHEN ( PV.Credits > 0.0 ) THEN 1
                                                 ELSE 0
                                            END AS PrgVersionTrackCredits
                                           ,T.TermId
                                           ,T.TermDescrip
                                           ,T.StartDate
                                           ,T.EndDate
                                           ,GBCR.ReqId
                                           ,T.StartDate AS CouseStartDate     -- tranfered  
                                           ,R.Code AS CourseCode
                                           ,R.Descrip AS CourseDescrip
                                           ,'(' + R.Code + ') ' + R.Descrip AS CourseCodeDescrip
                                           ,R.Credits
                                           ,R.FinAidCredits
                                           ,(
                                            SELECT MIN(MinVal)
                                            FROM   arGradeScaleDetails GCD
                                                  ,arGradeSystemDetails GSD
                                            WHERE  GCD.GrdSysDetailId = GSD.GrdSysDetailId
                                                   AND GSD.IsPass = 1
                                            )
                                           ,ISNULL(GBCR.Score, 0)
                                           ,NULL
                                           ,NULL
                                           ,NULL
                                           ,NULL
                                           ,NULL
                                           ,NULL
                                           ,NULL
                                           ,NULL
                                           ,NULL
                                           ,NULL
                                           ,NULL
                                           ,SE.StuEnrollId
                                           ,NULL AS MinResult
                                           ,NULL AS GradeComponentDescription -- Student data      
                                           ,ISNULL(SCS.CreditsAttempted, 0) AS CreditsAttempted
                                           ,ISNULL(SCS.CreditsEarned, 0) AS CreditsEarned
                                           ,SCS.Completed AS Completed
                                           ,SCS.CurrentScore AS CurrentScore
                                           ,SCS.CurrentGrade AS CurrentGrade
                                           ,SCS.FinalScore AS FinalScore
                                           ,SCS.FinalGrade AS FinalGrade
                                           ,C.CampusId
                                           ,C.CampDescrip
                                           ,NULL AS rownumber
                                           ,S.FirstName AS FirstName
                                           ,S.LastName AS LastName
                                           ,S.MiddleName
                                           ,(
                                            SELECT COUNT(*) AS GrdBkWgtDetailsCount
                                            FROM   arGrdBkConversionResults
                                            WHERE  StuEnrollId = SE.StuEnrollId
                                                   AND TermId = GBCR.TermId
                                                   AND ReqId = GBCR.ReqId
                                            ) AS GrdBkWgtDetailsCount
                                           ,CASE WHEN P.ACId = 5 THEN 'True'
                                                 ELSE 'False'
                                            END AS ClockHourProgram
                                           ,SCS.ClsSectionId
										    ,PVD.ReqSeq
                        FROM       arTransferGrades GBCR
                        INNER JOIN arStuEnrollments SE ON GBCR.StuEnrollId = SE.StuEnrollId
                        INNER JOIN (
                                   SELECT StudentId
                                         ,FirstName
                                         ,LastName
                                         ,MiddleName
                                   FROM   adLeads
                                   ) S ON S.StudentId = SE.StudentId
                        INNER JOIN syCampuses C ON SE.CampusId = C.CampusId
                        INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                        INNER JOIN arPrograms P ON P.ProgId = PV.ProgId
                        INNER JOIN arTerm T ON GBCR.TermId = T.TermId
                        INNER JOIN arReqs R ON GBCR.ReqId = R.ReqId
						      LEFT JOIN  dbo.arProgVerDef PVD ON PVD.PrgVerId = PV.PrgVerId
                                                       AND PVD.ReqId = R.ReqId
                        --INNER JOIN arTransferGrades AR ON GBCR.StuEnrollId = AR.StuEnrollId  
                        LEFT JOIN  syCreditSummary SCS ON SE.StuEnrollId = SCS.StuEnrollId
                                                          AND T.TermId = SCS.TermId
                                                          AND R.ReqId = SCS.ReqId
                        WHERE      SE.StuEnrollId IN (
                                                     SELECT Val
                                                     FROM   MultipleValuesForReportParameters(@StuEnrollIdList, ',', 1)
                                                     )
                                   AND (
                                       @TermId IS NULL
                                       OR T.TermId = @TermId
                                       )
                                   AND R.IsAttendanceOnly = 0
                        ) dt
                 ) dt1
        --WHERE rownumber2<=2  
        ORDER BY TermStartDate
                ,TermEndDate
                -- , TermDescription  

                ,CouseStartDate
                ,dt1.ReqSeq ASC
                ,CourseCode;
    --, CASE WHEN LTRIM(RTRIM(LOWER(@GradesFormat))) = 'letter'  
    --       THEN (RANK() OVER (ORDER BY FinalGrade DESC))  
    --  END  
    --, CASE WHEN LTRIM(RTRIM(LOWER(@GradesFormat))) <> 'letter'  
    --       THEN (RANK() OVER (ORDER BY FinalScore DESC))  
    --  END;  
    END;
-- ========================================================================================================= 
-- END  --  Usp_PR_Sub4_Courses_forOneTermAllEnrolmnents 
-- ========================================================================================================= 


GO
