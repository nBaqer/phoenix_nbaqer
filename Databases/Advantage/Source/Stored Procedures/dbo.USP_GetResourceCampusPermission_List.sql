SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_GetResourceCampusPermission_List]
    @UserId UNIQUEIDENTIFIER
AS
    SELECT  ResourceId
           ,CampusId
           ,AccessLevel
           ,CASE WHEN AccessLevel = 15 THEN 1
                 ELSE 0
            END AS FullPermission
           ,CASE WHEN ( AccessLevel >= 8 ) THEN 1
                 ELSE 0
            END AS EditPermission
           ,CASE WHEN ( AccessLevel IN ( 4,6,12,14,15 ) ) THEN 1
                 ELSE 0
            END AS CreatePermission
           ,CASE WHEN ( AccessLevel IN ( 2,6,10,14,15 ) ) THEN 1
                 ELSE 0
            END AS DeletePermission
           ,CASE WHEN ( AccessLevel IN ( 1,13 ) ) THEN 1
                 ELSE 0
            END AS ReadPermission
    FROM    (
              SELECT    RRL.ResourceId
                       ,CGC.CampusId
                       ,MAX(RRL.AccessLevel) AS AccessLevel
              FROM      syUsersRolesCampGrps URC
              INNER JOIN syRlsResLvls RRL ON URC.RoleId = RRL.RoleId
              INNER JOIN syCmpGrpCmps CGC ON URC.CampGrpId = CGC.CampGrpId
              WHERE     URC.UserId = @UserId
              GROUP BY  RRL.ResourceId
                       ,CGC.CampusId
            ) tblDerived
    ORDER BY ResourceId;



GO
