SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_GetFERPAEntities]
    @showActiveOnly INTEGER
   ,@campusId VARCHAR(50)
AS
    SET NOCOUNT ON;
                 
    IF @showActiveOnly = 1
        BEGIN
            EXEC usp_GetActiveFERPAEntities @campusId;
        END; 
    ELSE
        IF @showActiveOnly = 0
            BEGIN
                EXEC usp_GetInActiveFERPAEntities @campusId;
            END;
        ELSE
            BEGIN
                EXEC usp_GetAllFERPAEntities @campusId;
            END;



GO
