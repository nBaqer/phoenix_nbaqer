SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author: <Kimberly,Befera>
-- Create date: <8/5/19>
-- Description:	<Adds arResults place holder for student moved back to Future Start >
-- =============================================
CREATE  PROCEDURE [dbo].[USP_RE_REGISTER_STUDENT_RESULTS]
    @StuEnrollId UNIQUEIDENTIFIER,
    @ModUser VARCHAR(50)
AS

BEGIN

	DECLARE @ProgramVersionId UNIQUEIDENTIFIER
	 SET @ProgramVersionId =
    (
        SELECT TOP (1) PrgVerId
        FROM dbo.arStuEnrollments
		WHERE StuEnrollId =  @StuEnrollId
    );

   DECLARE @RegistrationType INT;
    SET @RegistrationType =
    (
        SELECT TOP (1)
               ProgramRegistrationType
        FROM dbo.arPrgVersions
        WHERE PrgVerId = @ProgramVersionId
        ORDER BY ModDate DESC
    );

    DECLARE @ShowROSSOnlyTabsForStudent VARCHAR(50);
    SET @ShowROSSOnlyTabsForStudent =
    (
        SELECT TOP (1)
               Value
        FROM dbo.syConfigAppSetValues AS configValues
            INNER JOIN dbo.syConfigAppSettings settings
                ON settings.SettingId = configValues.SettingId
        WHERE settings.KeyName = 'ShowROSSOnlyTabsForStudent'
        ORDER BY configValues.ModDate DESC
    );

    IF (@RegistrationType = 1)
    BEGIN
        INSERT INTO dbo.arResults
        (
            ResultId,
            TestId,
            Score,
            GrdSysDetailId,
            Cnt,
            Hours,
            StuEnrollId,
            IsInComplete,
            DroppedInAddDrop,
            ModUser,
            ModDate,
            IsTransfered,
            isClinicsSatisfied,
            DateDetermined,
            IsCourseCompleted,
            IsGradeOverridden,
            GradeOverriddenBy,
            GradeOverriddenDate,
            DateCompleted
        )
        SELECT NEWID(),              -- ResultId - uniqueidentifier
               classes.ClsSectionId, -- TestId - uniqueidentifier
               NULL,                 -- Score - decimal(18, 2)
               NULL,                 -- GrdSysDetailId - uniqueidentifier
               0,                    -- Cnt - int
               0,                    -- Hours - int
               @StuEnrollId, -- StuEnrollId - uniqueidentifier
               NULL,                 -- IsInComplete - bit
               NULL,                 -- DroppedInAddDrop - bit
               @ModUser,        -- ModUser - varchar(50)
               GETDATE(),            -- ModDate - datetime
               NULL,                 -- IsTransfered - bit
               NULL,                 -- isClinicsSatisfied - bit
               GETDATE(),            -- DateDetermined - datetime
               0,                    -- IsCourseCompleted - bit
               0,                    -- IsGradeOverridden - bit
               '',                   -- GradeOverriddenBy - varchar(50)
               GETDATE(),            -- GradeOverriddenDate - datetime
               GETDATE()             -- DateCompleted - datetime
        FROM dbo.arPrgVersions programVersion
            INNER JOIN dbo.arTerm terms
                ON terms.ProgramVersionId = programVersion.PrgVerId
            INNER JOIN dbo.arClassSections classes
                ON classes.TermId = terms.TermId
        WHERE programVersion.PrgVerId = @ProgramVersionId;
END;
END;
GO
