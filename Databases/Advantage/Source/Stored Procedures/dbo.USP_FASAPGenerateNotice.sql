SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[USP_FASAPGenerateNotice]
    (
        @EnrollmentId VARCHAR(MAX) = NULL
    )
AS
    BEGIN
        DECLARE @ActiveStatusId UNIQUEIDENTIFIER;
        SET @ActiveStatusId = (
                              SELECT TOP 1 StatusId
                              FROM   dbo.syStatuses
                              WHERE  UPPER(StatusCode) = 'A'
                              );
        DECLARE @SchoolType VARCHAR(20);
        SET @SchoolType = LOWER((
                                SELECT TOP 1 Value
                                FROM   dbo.syConfigAppSetValues
                                WHERE  SettingId = 47
                                )
                               );
        DECLARE @enrollment TABLE
            (
                rNum INT IDENTITY(1, 1)
               ,stuEnrollId UNIQUEIDENTIFIER
               ,completedHours DECIMAL(18, 2) NULL
               ,ScheduleHrs DECIMAL(18, 2) NULL
            );
        INSERT INTO @enrollment (
                                stuEnrollId
                                )
                    SELECT Val
                    FROM   MultipleValuesForReportParameters(@EnrollmentId, ',', 1);

        DECLARE @total INT;
        DECLARE @attedType VARCHAR(50);
        DECLARE @enrollId UNIQUEIDENTIFIER;
        SET @total = (
                     SELECT COUNT(*)
                     FROM   @enrollment
                     );
        DECLARE @i INT = 1;
        DECLARE @attenUnitTypeId UNIQUEIDENTIFIER;
        DECLARE @SchedulecompletedHrs TABLE
            (
                schRId INT IDENTITY(1, 1)
               ,TotalHoursAbsent DECIMAL(18, 2)
               ,TotalPresentHours DECIMAL(18, 2)
               ,SchedHours DECIMAL(18, 2)
               ,TotalMakeupHours DECIMAL(18, 2)
               ,ScheduleHoursByWeek DECIMAL(18, 2)
               ,LastDateAttended DATETIME
               ,TardyCount INT
            );
        --WHILE ( @i <= @total )
        --    BEGIN
        --        SET @attenUnitTypeId = (
        --                               SELECT     arAttUnitType.UnitTypeId
        --                               FROM       dbo.arStuEnrollments
        --                               INNER JOIN dbo.arPrgVersions ON arPrgVersions.PrgVerId = arStuEnrollments.PrgVerId
        --                               INNER JOIN dbo.arAttUnitType ON arAttUnitType.UnitTypeId = arPrgVersions.UnitTypeId
        --                               INNER JOIN @enrollment ON [@enrollment].stuEnrollId = arStuEnrollments.StuEnrollId
        --                               WHERE      rNum = @i
        --                               );
        --        SET @attedType = CASE @attenUnitTypeId
        --                              WHEN 'EF5535C2-142C-4223-AE3C-25A50A153CC6' THEN 'pa'
        --                              WHEN 'A1389C74-0BB9-4BBF-A47F-68428BE7FA4D' THEN 'minutes'
        --                              WHEN 'B937C92E-FD7A-455E-A731-527A9918C734' THEN 'hrs'
        --                              ELSE ''
        --                         END;
        --        SET @enrollId = (
        --                        SELECT stuEnrollId
        --                        FROM   @enrollment
        --                        WHERE  rNum = @i
        --                        );
        --        INSERT INTO @SchedulecompletedHrs
        --        EXEC USP_SchedAndCompletedHours_ByDay @enrollId
        --                                             ,@attedType;
        --        UPDATE @enrollment
        --        SET    completedHours = (
        --                                SELECT TotalPresentHours
        --                                FROM   @SchedulecompletedHrs
        --                                WHERE  schRId = @i
        --                                )
        --              ,ScheduleHrs = (
        --                             SELECT SchedHours
        --                             FROM   @SchedulecompletedHrs
        --                             WHERE  schRId = @i
        --                             )
        --        WHERE  rNum = @i;
        --        SET @i = @i + 1;
        --    END;



        SELECT     TOP 1 R.StdRecKey AS FASAPKey
                        ,E.StuEnrollId AS EnrollmentId
                        ,S.FirstName
                        ,S.MiddleName
                        ,S.LastName
                        ,S.StudentNumber
                        ,P.ProgDescrip AS ProgramDescription
                        ,PV.PrgVerDescrip AS ProgramVersionDescription
                        ,E.StartDate
                        ,R.Period AS Increment
                        ,TUT.TrigUnitTypDescrip AS UnitType
                        ,TOST.TrigOffTypDescrip AS TriggerOffsetType
                        ,R.CheckPointDate AS SAPCheckDate
                        ,CASE WHEN @SchoolType = 'letter' THEN R.GPA
                              WHEN @SchoolType = 'numeric' THEN R.Average
                              ELSE 'School is not numeric or letter graded'
                         END AS QualitativeResults
                        ,QMT.QualMinTypDesc AS MinimumQualitativeLabel
                        ,SD.QualMinValue AS MinimumGrade
                        ,R.PercentCompleted AS QuantitativeResult
                        ,QMUT.QuantMinTypDesc AS MinimumQuantityUnitLabel
                        ,SD.QuantMinValue AS MinimumAttendance
                        ,REPLACE(REPLACE(   CASE WHEN CV.Message IS NOT NULL THEN CV.Message
                                                 ELSE TIVSS.DefaultMessage
                                            END
                                           ,'<<Minimum quantity>>'
                                           ,SD.QuantMinValue
                                        )
                                ,'<<MinimumGPA>>'
                                ,SD.QualMinValue
                                ) AS FASAPMessage
                        ,C.CampDescrip CampusDescription
                        ,CAST(CAST(TrigValue AS DECIMAL) / 100 AS DECIMAL(8, 2)) AS TriggerValue
                        ,TIVSS.Code AS TitleIVStatus
                        ,r.HrsEarned AS CompletedHours
                        ,r.HrsAttended AS ScheduledHours
        FROM       dbo.arFASAPChkResults R
        JOIN       dbo.arStuEnrollments E ON E.StuEnrollId = R.StuEnrollId
        JOIN       dbo.arPrgVersions PV ON PV.PrgVerId = E.PrgVerId
        JOIN       dbo.arPrograms P ON P.ProgId = PV.ProgId
        JOIN       dbo.arStudent S ON E.StudentId = S.StudentId
        JOIN       dbo.arSAPDetails SD ON SD.SAPDetailId = R.SAPDetailId
        JOIN       dbo.arQuantMinUnitTyps QMUT ON QMUT.QuantMinUnitTypId = SD.QuantMinUnitTypId
        JOIN       dbo.arQualMinTyps QMT ON QMT.QualMinTypId = SD.QualMinTypId
        JOIN       dbo.arTrigUnitTyps TUT ON TUT.TrigUnitTypId = SD.TrigUnitTypId
        JOIN       dbo.arTrigOffsetTyps TOST ON TOST.TrigOffsetTypId = SD.TrigOffsetTypId
        JOIN       dbo.syTitleIVSapStatus TIVSS ON TIVSS.Id = R.TitleIVStatusId
        JOIN       dbo.syCampuses C ON C.CampusId = E.CampusId
        LEFT JOIN  dbo.syTitleIVSapCustomVerbiage CV ON CV.SapId = SD.SAPId
                                                        AND CV.TitleIVSapStatusId = R.TitleIVStatusId
                                                        AND CV.StatusId = @ActiveStatusId
        INNER JOIN @enrollment ET ON ET.stuEnrollId = E.StuEnrollId
        WHERE      R.StatusId = @ActiveStatusId
                   AND @EnrollmentId IS NULL
                   OR ( E.StuEnrollId IN (
                                         SELECT Val
                                         FROM   MultipleValuesForReportParameters(@EnrollmentId, ',', 1)
                                         )
                      )
        ORDER BY   Period DESC
                  ,SAPCheckDate DESC;

    END;

GO
