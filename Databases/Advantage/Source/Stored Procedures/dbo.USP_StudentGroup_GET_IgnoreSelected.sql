SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_StudentGroup_GET_IgnoreSelected]
    @CampusId UNIQUEIDENTIFIER
   ,@ShowActiveOnly BIT = 1
   ,@SelectedStudentGroup VARCHAR(8000)
AS
    BEGIN
        IF ( @ShowActiveOnly = 1 )
            BEGIN
                SELECT DISTINCT
                        LG.LeadGrpId
                       ,LG.Descrip AS LeadGroupDescription
                FROM    dbo.adLeadGroups LG
                INNER JOIN syCampGrps CG ON CG.CampGrpId = LG.CampGrpId
                INNER JOIN dbo.syCmpGrpCmps CGC ON CG.CampGrpId = CGC.CampGrpId
                WHERE   CGC.CampusId = @CampusId
                        AND LG.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
                        AND LG.LeadGrpId NOT IN ( SELECT    Val
                                                  FROM      MultipleValuesForReportParameters(@SelectedStudentGroup,',',1) )
                ORDER BY LeadGroupDescription;
		
            END;
        ELSE
            BEGIN
                SELECT DISTINCT
                        LG.LeadGrpId
                       ,LG.Descrip AS LeadGroupDescription
                FROM    dbo.adLeadGroups LG
                INNER JOIN syCampGrps CG ON CG.CampGrpId = LG.CampGrpId
                INNER JOIN dbo.syCmpGrpCmps CGC ON CG.CampGrpId = CGC.CampGrpId
                WHERE   CGC.CampusId = @CampusId
                        AND LG.LeadGrpId NOT IN ( SELECT    Val
                                                  FROM      MultipleValuesForReportParameters(@SelectedStudentGroup,',',1) )
                ORDER BY LeadGroupDescription;
            END;
    END;

GO
