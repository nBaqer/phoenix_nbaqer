SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_GetModulesCoursesResultsForMultipleStudents]
    (
     @campGrpId VARCHAR(8000)
    ,@prgVerId VARCHAR(8000)
    ,@statusCodeId VARCHAR(8000) = NULL
    ,@termId UNIQUEIDENTIFIER = NULL
    ,@termDate DATETIME = NULL

    )
AS
    SET NOCOUNT ON;

    SELECT  *
    FROM    (
              SELECT    tg.StuEnrollId
                       ,tg.TermId
                       ,tm.TermDescrip
                       ,tg.Score AS score
                       ,tm.StartDate
                       ,tm.EndDate
                       ,rq.Code
                       ,rq.Descrip
                       ,tg.ReqId
                       ,rq.Credits
                       ,rq.FinAidCredits
                       ,'00000000-0000-0000-0000-000000000000' AS ClsSectionId
                       ,(
                          SELECT    Grade
                          FROM      arGradesystemdetails
                          WHERE     GrdSysdetailid = tg.GrdSysdetailid
                        ) AS Grade
                       ,(
                          SELECT    IsPass
                          FROM      arGradesystemdetails
                          WHERE     GrdSysdetailid = tg.GrdSysdetailid
                        ) AS IsPass
                       ,(
                          SELECT    IsCreditsAttempted
                          FROM      arGradesystemdetails
                          WHERE     GrdSysdetailid = tg.GrdSysdetailid
                        ) AS IsCreditsAttempted
                       ,(
                          SELECT    IsCreditsEarned
                          FROM      arGradesystemdetails
                          WHERE     GrdSysdetailid = tg.GrdSysdetailid
                        ) AS IsCreditsEarned
                       ,(
                          SELECT    ExpGradDate
                          FROM      arStuEnrollments
                          WHERE     stuEnrollid = tg.StuEnrollId
                        ) AS ExpGradDate
                       ,(
                          SELECT DISTINCT
                                    COUNT(GC.Descrip)
                          FROM      arGrdBkWeights GBW
                                   ,arGrdComponentTypes GC
                                   ,arGrdBkWgtDetails GD
                          WHERE     GBW.InstrGrdBkWgtId = GD.InstrGrdBkWgtId
                                    AND GC.GrdComponentTypeId = GD.GrdComponentTypeId
                                    AND GBW.ReqId = tg.ReqId
                                    AND GC.SysComponentTypeID IS NOT NULL
                                    AND GC.SysComponentTypeID IN ( SELECT DISTINCT
                                                                            ResourceId
                                                                   FROM     syResources
                                                                   WHERE    Resource IN ( 'Lab Work','Lab Hours' ) )
                        ) AS labCount
              FROM      arTransferGrades tg
                       ,arTerm tm
                       ,arReqs rq
              WHERE     tg.TermId = tm.TermId
                        AND tg.ReqId = rq.ReqId
                        AND (
                              @termId IS NULL
                              OR tm.Termid = @termId
                            )
                        AND (
                              @termDate IS NULL
                              OR tm.StartDate <= @termDate
                            )
                        AND tg.IsCourseCompleted = 1
              UNION
              SELECT    ar.StuEnrollId
                       ,cs.TermId
                       ,tm2.TermDescrip
                       ,ar.Score AS score
                       ,tm2.StartDate
                       ,tm2.EndDate
                       ,rq2.Code
                       ,rq2.Descrip
                       ,cs.ReqId
                       ,rq2.Credits
                       ,rq2.FinAidCredits
                       ,cs.ClsSectionid
                       ,(
                          SELECT    Grade
                          FROM      arGradesystemdetails
                          WHERE     GrdSysdetailid = ar.GrdSysdetailid
                        ) AS Grade
                       ,(
                          SELECT    IsPass
                          FROM      arGradesystemdetails
                          WHERE     GrdSysdetailid = ar.GrdSysdetailid
                        ) AS IsPass
                       ,(
                          SELECT    IsCreditsAttempted
                          FROM      arGradesystemdetails
                          WHERE     GrdSysdetailid = ar.GrdSysdetailid
                        ) AS IsCreditsAttempted
                       ,(
                          SELECT    IsCreditsEarned
                          FROM      arGradesystemdetails
                          WHERE     GrdSysdetailid = ar.GrdSysdetailid
                        ) AS IsCreditsEarned
                       ,(
                          SELECT    ExpGradDate
                          FROM      arStuEnrollments
                          WHERE     stuEnrollid = ar.StuEnrollId
                        ) AS ExpGradDate
                       ,(
                          SELECT DISTINCT
                                    COUNT(GC.Descrip)
                          FROM      arGrdBkWeights GBW
                                   ,arGrdComponentTypes GC
                                   ,arGrdBkWgtDetails GD
                          WHERE     GBW.InstrGrdBkWgtId = GD.InstrGrdBkWgtId
                                    AND GC.GrdComponentTypeId = GD.GrdComponentTypeId
                                    AND GBW.ReqId = cs.ReqId
                                    AND GC.SysComponentTypeID IS NOT NULL
                                    AND GC.SysComponentTypeID IN ( SELECT DISTINCT
                                                                            ResourceId
                                                                   FROM     syResources
                                                                   WHERE    Resource IN ( 'Lab Work','Lab Hours' ) )
                        ) AS labCount
              FROM      arResults ar
                       ,arClassSections cs
                       ,arTerm tm2
                       ,arReqs rq2
              WHERE     ar.TestId = cs.ClsSectionId
                        AND cs.TermId = tm2.TermId
                        AND cs.ReqId = rq2.ReqId
                        AND (
                              @termId IS NULL
                              OR tm2.Termid = @termId
                            )
                        AND (
                              @termDate IS NULL
                              OR tm2.StartDate <= @termDate
                            )
                        AND ar.IsCourseCompleted = 1
            ) P
    WHERE   EXISTS ( SELECT DISTINCT
                            arStuEnrollments.StuEnrollId
                     FROM   arStuEnrollments
                           ,arStudent A
                           ,syCampuses C
                           ,syCmpGrpCmps
                           ,syCampGrps
                     WHERE  arStuEnrollments.StudentId = A.StudentId
                            AND arStuEnrollments.CampusId = C.CampusId
                            AND arStuEnrollments.StuEnrollId = P.StuEnrollId
                            AND syCmpGrpCmps.CampusId = arStuEnrollments.CampusId
                            AND syCampGrps.CampGrpId = syCmpGrpCmps.CampGrpId
                            AND syCmpGrpCmps.campgrpid IN ( SELECT  strval
                                                            FROM    dbo.SPLIT(@campGrpId) )
                            AND arStuEnrollments.prgverid IN ( SELECT   strval
                                                               FROM     dbo.SPLIT(@prgVerId) )
                            AND (
                                  @statusCodeId IS NULL
                                  OR arStuEnrollments.statuscodeid IN ( SELECT  strval
                                                                        FROM    dbo.SPLIT(@statusCodeId) )
                                ) )
            AND P.StuEnrollId NOT IN ( SELECT   SGS.StuEnrollId
                                       FROM     adStuGrpStudents SGS
                                               ,adStudentGroups SG
                                       WHERE    SGS.StuGrpId = SG.StuGrpId
                                                AND SG.IsTransHold = 1
                                                AND SGS.IsDeleted = 0
                                                AND SGS.StuEnrollId IS NOT NULL )
    ORDER BY stuEnrollId
           ,StartDate
           ,Grade DESC
           ,Score DESC; 




GO
