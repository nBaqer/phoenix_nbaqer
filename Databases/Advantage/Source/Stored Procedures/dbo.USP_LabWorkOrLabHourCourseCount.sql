SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_LabWorkOrLabHourCourseCount]
    (
     @reqId UNIQUEIDENTIFIER
    )
AS
    SET NOCOUNT ON;
    SELECT
		DISTINCT
            COUNT(GC.Descrip)
    FROM    arGrdBkWeights GBW
           ,arGrdComponentTypes GC
           ,arGrdBkWgtDetails GD
    WHERE   GBW.InstrGrdBkWgtId = GD.InstrGrdBkWgtId
            AND GC.GrdComponentTypeId = GD.GrdComponentTypeId
            AND GBW.ReqId = @reqId
            AND GC.SysComponentTypeID IS NOT NULL
            AND GC.SysComponentTypeID IN ( SELECT DISTINCT
                                                    ResourceId
                                           FROM     syResources
                                           WHERE    Resource IN ( 'Lab Work','Lab Hours' ) );



GO
