SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_MaritalStatus_GetList] @Descrip VARCHAR(50)
AS
    SELECT  MaritalStatId
    FROM    adMaritalStatus
    WHERE   MaritalStatDescrip LIKE @Descrip + '%';



GO
