SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--=================================================================================================
-- USP_getClasses_byCampusDate_andTermCourseInstructorShift
--=================================================================================================
CREATE PROCEDURE [dbo].[USP_getClasses_byCampusDate_andTermCourseInstructorShift]
    @Campus UNIQUEIDENTIFIER
   ,@WeekEndDate DATE
   ,@TermId VARCHAR(8000) = NULL
   ,@CourseId VARCHAR(8000) = NULL
   ,@InstructorId VARCHAR(8000) = NULL
   ,@ShiftId VARCHAR(8000) = NULL
AS
    BEGIN      
        IF LEN(@TermId) = 0
            BEGIN
             
                SET @TermId = NULL;
            END;
        IF LEN(@CourseId) = 0
            BEGIN
             
                SET @CourseId = NULL;
            END;
        IF LEN(@InstructorId) = 0
            BEGIN
             
                SET @InstructorId = NULL;
            END;  
        IF LEN(@ShiftId) = 0
            BEGIN
             
                SET @ShiftId = NULL;
            END; 
        DECLARE @OriginalSectionList TABLE
            (
             ClsSectionId UNIQUEIDENTIFIER
            ,ClsSection VARCHAR(12)
            );        
        
        INSERT  INTO @OriginalSectionList
                SELECT DISTINCT
                        ARC.ClsSectionId
                       ,ARC.ClsSection
                FROM    dbo.arClassSections ARC
                WHERE   @WeekEndDate BETWEEN ARC.StartDate AND ARC.EndDate
                        AND ARC.CampusId = @Campus
                        AND (
                              @TermId IS NULL
                              OR ARC.TermId IN ( SELECT strval
                                                 FROM   dbo.SPLIT(@TermId) )
                            )
                        AND (
                              @CourseId IS NULL
                              OR ARC.ReqId IN ( SELECT  strval
                                                FROM    dbo.SPLIT(@CourseId) )
                            )
                        AND (
                              @InstructorId IS NULL
                              OR ARC.InstructorId IN ( SELECT   strval
                                                       FROM     dbo.SPLIT(@InstructorId) )
                            )
                        AND (
                              @ShiftId IS NULL
                              OR ARC.shiftid IN ( SELECT    strval
                                                  FROM      dbo.SPLIT(@ShiftId) )
                            )
                ORDER BY ARC.ClsSection;     
    
        SELECT DISTINCT
                ClsSectionIds
               ,ClsSection
        FROM    @OriginalSectionList osl
        CROSS APPLY (
                      SELECT    STUFF( (SELECT  ',' + CONVERT(VARCHAR(100),ClsSectionId)
                                        FROM    @OriginalSectionList sl
                                        WHERE   sl.ClsSection = osl.ClsSection
                                        ORDER BY ClsSectionId
                                FOR   XML PATH('')
                                         ,TYPE).value('.','varchar(max)'),1,1,'')
                    ) D ( ClsSectionIds )
        ORDER BY ClsSection;    
    END;
--=================================================================================================
-- END  --  USP_getClasses_byCampusDate_andTermCourseInstructorShift
--=================================================================================================	
GO
