SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[fmAuditHistAdd]
    @AuditHistId UNIQUEIDENTIFIER
   ,@TableName VARCHAR(50)
   ,@Event CHAR(1)
   ,@EventRows INT
   ,@EventDate DATETIME
   ,@UserName VARCHAR(128)
AS
    INSERT  INTO syAuditHist
            (
             AuditHistId
            ,TableName
            ,Event
            ,EventRows
            ,EventDate
            ,UserName
            )
    VALUES  (
             @AuditHistId
            ,@TableName
            ,@Event
            ,@EventRows
            ,@EventDate
            ,@UserName
            );



GO
