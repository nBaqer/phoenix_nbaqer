SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

--=================================================================================================
-- USP_IPEDS_MissingDataReportGained
--=================================================================================================
CREATE PROCEDURE [dbo].[USP_IPEDS_MissingDataReportGained]
    @campusId AS VARCHAR(50)
   ,@ProgId AS VARCHAR(8000) = NULL
   --'112AE539-EF1F-4513-A795-04D360C254C3,C1B7EB2C-9E8D-490E-A04A-50544FCBA85D,F8DB1537-7F52-49B4-805F-6F2603BF44BD',  
   ,@OrderBY AS INT = 1
AS
    BEGIN
        /************************************************************************************************************************************  
   Business Rules for Missing Data Report  

   1. Exclude students whose current enrollment status is  
     b. No Start  

   2. If student’s current status is Dropped, Expelled or Terminated  
     a. Check if drop date is less than or equal to Official Reporting date (or date range)  
       i. If above condition is met and if student’s LDA falls before the “Exclude” date,   
       then exclude student.   

   3. If student is currently in Grad status,  
     a. Check if expected grad date is less than or equal to Official Reporting date (or date range)  
       i. If above condition is met and if student’s expected Grad Date falls   
       before the “Exclude” date, then exclude student.   
 **************************************************************************************************************************************/
        -- Resolve All to NULL and all are listed....  
        IF @ProgId = 'E711CC07-3806-4E01-AC15-8788BBDC0E4F'
            BEGIN
                SET @ProgId = NULL;
            END;

        SELECT   t.*
                ,(
                     SELECT CASE WHEN t.StudentIdentifier = 'X' THEN 1
                                 ELSE 0
                            END
                 ) AS StudentIdentifierCount
                ,(
                     SELECT CASE WHEN t.BirthDate = 'X' THEN 1
                                 ELSE 0
                            END
                 ) AS BirthDateCount
                ,(
                     SELECT CASE WHEN t.OPEID = 'X' THEN 1
                                 ELSE 0
                            END
                 ) AS OPEIDCount
                --(select case when t.ProgramName ='X' then 1 else 0 end) as ProgramNameCount,  
                ,(
                     SELECT CASE WHEN t.CIPCode = 'X' THEN 1
                                 ELSE 0
                            END
                 ) AS CIPCodeCount
                ,(
                     SELECT CASE WHEN t.CredentialLevel = 'X' THEN 1
                                 ELSE 0
                            END
                 ) AS CredentialLevelCount
                --(SELECT CASE WHEN t.InstitutionName ='X' then 1 else 0 end) as InstitutionNameCount,  
                ,(
                     SELECT CASE WHEN t.ProgramAttendanceBeginDate = 'X' THEN 1
                                 ELSE 0
                            END
                 ) AS ProgramAttendanceBeginDateCount
                ,(
                     SELECT CASE WHEN t.ProgramAttendanceStatusDate = 'X' THEN 1
                                 ELSE 0
                            END
                 ) AS ProgramAttendanceStatusDateCount
                ,(
                     SELECT CASE WHEN t.LengthOfGEProgram = 'X' THEN 1
                                 ELSE 0
                            END
                 ) AS LengthOfGEProgramCount
                ,(
                     SELECT CASE WHEN t.StudentEnrollmentStatusAsOfThe1stDayOfEnrollmentInProgram = 'X' THEN 1
                                 ELSE 0
                            END
                 ) AS StudentEnrollmentStatusAsOfThe1stDayOfEnrollmentInProgramCount
        FROM     (
                     SELECT *
                     FROM   (
                                SELECT
                                    --Select case @ShowSSN when 0 then S.StudentNumber else case when S.SSN IS null then '' else substring(S.SSN,1,3)+'-'+substring(S.SSN,4,2)+'-' + substring(S.SSN,6,4) end end AS StudentIdentifier,   
                                      S.LastName
                                     ,S.FirstName
                                     ,CASE ( ISNULL(S.SSN, ''))
                                           WHEN '' THEN 'X'
                                           ELSE ''
                                      END AS StudentIdentifier
                                     ,CASE ( ISNULL(DOB, ''))
                                           WHEN '' THEN 'X'
                                           ELSE ''
                                      END AS BirthDate
                                     ,CASE ( ISNULL(syCampuses.OPEID, ''))
                                           WHEN '' THEN 'X'
                                           ELSE ''
                                      END AS OPEID
                                     --CASE (isnull(syCampuses.SchoolName,'')) When '' then 'X' else '' end as InstitutionName,  
                                     --CASE (isnull(PV.PrgVerDescrip,'')) When '' then 'X' else '' end as ProgramName,  
                                     ,CASE ( ISNULL(P.CIPCode, ''))
                                           WHEN '' THEN 'X'
                                           ELSE ''
                                      END AS CIPCode
                                     --P.CIPCode AS CIPCodex,  
                                     ,CASE ( ISNULL(CONVERT(NVARCHAR(50), P.CredentialLvlId), ''))
                                           WHEN '' THEN 'X'
                                           ELSE ''
                                      END AS CredentialLevel
                                     ,CASE ( ISNULL(summ.StudentAttendedDate, ''))
                                           WHEN '' THEN ( CASE LOWER(UT.UnitTypeDescrip)
                                                               WHEN 'none' THEN ''
                                                               ELSE 'X'
                                                          END
                                                        )
                                           ELSE ''
                                      END AS ProgramAttendanceBeginDate
                                     ,CASE ( ISNULL(   CASE WHEN T7.SysStatusDescrip = 'Future start'
                                                                 AND (
                                                                         SE.LDA <> NULL
                                                                         OR SE.StartDate <> NULL
                                                                     ) THEN NULL
                                                            WHEN T7.GEProgramStatus = 'C'
                                                                 OR T7.GEProgramStatus = 'G' THEN SE.ExpGradDate
                                                            WHEN T7.GEProgramStatus = 'W' THEN SE.DateDetermined
                                                            ELSE NULL
                                                       END
                                                      ,''
                                                   )
                                           )
                                           WHEN '' THEN 'X'
                                           ELSE ''
                                      END AS ProgramAttendanceStatusDate
                                     ,CASE ( ISNULL(PV.Weeks, ''))
                                           WHEN '' THEN 'X'
                                           ELSE ''
                                      END AS LengthOfGEProgram
                                     ,CASE ( ISNULL(T10.GERptAgencyFldValId, ''))
                                           WHEN '' THEN 'X'
                                           ELSE ''
                                      END AS StudentEnrollmentStatusAsOfThe1stDayOfEnrollmentInProgram
                                FROM  arStudent S
                                LEFT OUTER JOIN arStuEnrollments SE ON S.StudentId = SE.StudentId
                                INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                                INNER JOIN arPrograms P ON P.ProgId = PV.ProgId
                                INNER JOIN syStatusCodes SC ON SE.StatusCodeId = SC.StatusCodeId
                                JOIN  dbo.syCampuses ON syCampuses.CampusId = SE.CampusId
                                LEFT JOIN dbo.syStudentAttendanceSummary summ ON summ.StuEnrollId = SE.StuEnrollId
                                JOIN  dbo.sySysStatus T7 ON T7.SysStatusId = SC.SysStatusId
                                LEFT JOIN dbo.arAttendTypes T10 ON T10.AttendTypeId = SE.attendtypeid
                                INNER JOIN dbo.arAttUnitType UT ON UT.UnitTypeId = PV.UnitTypeId
                                --      left JOIN syRptAgencyFldValues T11 ON T10.gerptagencyfldvalid = T11.RptAgencyFldValId  
                                WHERE (
                                          @ProgId IS NULL
                                          OR P.ProgId IN (
                                                             SELECT strval
                                                             FROM   dbo.SPLIT(@ProgId)
                                                         )
                                      )
                                      AND SE.CampusId = @campusId
                                      AND ( SC.SysStatusId <> 8 )
                                      --AND  SE.StartDate<=@OffRepDate   
                                      AND SC.SysStatusId IN ( 12, 14 )
                                      AND P.IsGEProgram = 1
                            ) t1
                     --  WHERE    
                     --ExcludeDate>@ExcludeStuBefThisDate     
                     UNION
                     SELECT
                         --case @ShowSSN when 0 then S.StudentNumber else case when S.SSN IS null then '' else substring(S.SSN,1,3)+'-'+substring(S.SSN,4,2)+'-' + substring(S.SSN,6,4) end end AS StudentIdentifier,   
                           S.LastName
                          ,S.FirstName
                          ,CASE ( ISNULL(S.SSN, ''))
                                WHEN '' THEN 'X'
                                ELSE ''
                           END AS StudentIdentifier
                          ,CASE ( ISNULL(DOB, ''))
                                WHEN '' THEN 'X'
                                ELSE ''
                           END AS BirthDate
                          ,CASE ( ISNULL(syCampuses.OPEID, ''))
                                WHEN '' THEN 'X'
                                ELSE ''
                           END AS OPEID
                          --CASE(ISNULL (syCampuses.SchoolName,'')) When '' then 'X' else '' end as InstitutionName,  
                          --CASE(ISNULL (PV.PrgVerDescrip,'')) When '' then 'X' else '' end as ProgramName,  
                          ,CASE ( ISNULL(P.CIPCode, ''))
                                WHEN '' THEN 'X'
                                ELSE ''
                           END AS CIPCode
                          --P.CIPCode AS CIPCodex,  
                          ,CASE ( ISNULL(CONVERT(NVARCHAR(50), P.CredentialLvlId), ''))
                                WHEN '' THEN 'X'
                                ELSE ''
                           END AS CredentialLevel
                          ,CASE ( ISNULL(summ.StudentAttendedDate, ''))
                                WHEN '' THEN ( CASE LOWER(UT.UnitTypeDescrip)
                                                    WHEN 'none' THEN ''
                                                    ELSE 'X'
                                               END
                                             )
                                ELSE ''
                           END AS ProgramAttendanceBeginDate
                          ,CASE ( ISNULL(   CASE WHEN T7.SysStatusDescrip = 'Future start'
                                                      AND (
                                                              SE.LDA <> NULL
                                                              OR SE.StartDate <> NULL
                                                          ) THEN NULL
                                                 WHEN T7.GEProgramStatus = 'C'
                                                      OR T7.GEProgramStatus = 'G' THEN SE.ExpGradDate
                                                 WHEN T7.GEProgramStatus = 'W' THEN SE.DateDetermined
                                                 ELSE NULL
                                            END
                                           ,''
                                        )
                                )
                                WHEN '' THEN 'X'
                                ELSE ''
                           END AS ProgramAttendanceStatusDate
                          ,CASE ( ISNULL(PV.Weeks, ''))
                                WHEN '' THEN 'X'
                                ELSE ''
                           END AS LengthOfGEProgram
                          ,CASE ( ISNULL(T10.GERptAgencyFldValId, ''))
                                WHEN '' THEN 'X'
                                ELSE ''
                           END AS StudentEnrollmentStatusAsOfThe1stDayOfEnrollmentInProgram
                     FROM  arStudent S
                     LEFT OUTER JOIN arStuEnrollments SE ON S.StudentId = SE.StudentId
                     INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                     INNER JOIN arPrograms P ON P.ProgId = PV.ProgId
                     INNER JOIN syStatusCodes SC ON SE.StatusCodeId = SC.StatusCodeId
                     JOIN  dbo.syCampuses ON syCampuses.CampusId = SE.CampusId
                     LEFT JOIN dbo.syStudentAttendanceSummary summ ON summ.StuEnrollId = SE.StuEnrollId
                     JOIN  dbo.sySysStatus T7 ON T7.SysStatusId = SC.SysStatusId
                     LEFT JOIN dbo.arAttendTypes T10 ON T10.AttendTypeId = SE.attendtypeid
                     INNER JOIN dbo.arAttUnitType UT ON UT.UnitTypeId = PV.UnitTypeId
                     --LEFT JOIN syRptAgencyFldValues T11 ON T10.gerptagencyfldvalid = T11.RptAgencyFldValId  
                     WHERE (
                               @ProgId IS NULL
                               OR P.ProgId IN (
                                                  SELECT strval
                                                  FROM   dbo.SPLIT(@ProgId)
                                              )
                           )
                           AND SE.CampusId = @campusId
                           AND ( SC.SysStatusId <> 8 )
                           --AND  SE.StartDate<=@OffRepDate   
                           AND SC.SysStatusId NOT IN ( 12, 14 )
                           AND P.IsGEProgram = 1
                 ) t
        WHERE    (
                     StudentIdentifier = 'X'
                     OR BirthDate = 'X'
                     OR OPEID = 'X'
                     --OR InstitutionName='X'   
                     --OR ProgramName='X'   
                     OR CIPCode = 'X'
                     OR CredentialLevel = 'X'
                     OR ProgramAttendanceBeginDate = 'X'
                     OR ProgramAttendanceStatusDate = 'X'
                     OR LengthOfGEProgram = 'X'
                     OR StudentEnrollmentStatusAsOfThe1stDayOfEnrollmentInProgram = 'X'
                 )
        GROUP BY StudentIdentifier
                ,OPEID
                --, InstitutionName  
                ,LastName
                ,FirstName
                --, ProgramName  
                --,CIPCodex  
                ,ProgramAttendanceBeginDate
                ,ProgramAttendanceStatusDate
                ,LengthOfGEProgram
                ,StudentEnrollmentStatusAsOfThe1stDayOfEnrollmentInProgram
                ,BirthDate
                ,CIPCode
                ,CredentialLevel
        ORDER BY
            --case @OrderBY when 1 then t.StudentIdentifier else t.LastName end   
                 t.LastName;


    END;
--=================================================================================================
-- END  --  USP_IPEDS_MissingDataReportGained
--=================================================================================================
GO
