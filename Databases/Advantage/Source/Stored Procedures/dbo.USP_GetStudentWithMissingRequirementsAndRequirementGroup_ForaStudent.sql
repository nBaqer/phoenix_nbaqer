SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

 
CREATE PROCEDURE [dbo].[USP_GetStudentWithMissingRequirementsAndRequirementGroup_ForaStudent]
    (
     @StudentID UNIQUEIDENTIFIER
    ,@CampusID UNIQUEIDENTIFIER
    ,@ProgID UNIQUEIDENTIFIER = NULL
    ,@ShiftID UNIQUEIDENTIFIER = NULL
    ,@StatusCodeID UNIQUEIDENTIFIER = NULL 
    )
AS
    BEGIN

        DECLARE @tab1 TABLE
            (
             Descrip VARCHAR(100)
            ,StudentID UNIQUEIDENTIFIER
            );
        
            
        INSERT  INTO @tab1
                EXEC USP_GetStudentWithMissingRequirementsAndRequirementGroup_EnrollforaStudent @StudentID,@CampusID,@ProgID;
        DECLARE @tab2 TABLE
            (
             Descrip VARCHAR(100)
            ,StudentID UNIQUEIDENTIFIER
            );
        INSERT  INTO @tab2
                EXEC USP_GetStudentWithMissingRequirementsAndRequirementGroup_GradForaStudent @StudentID,@CampusID,@ProgID;
        DECLARE @tab3 TABLE
            (
             Descrip VARCHAR(100)
            ,StudentID UNIQUEIDENTIFIER
            );
        INSERT  INTO @tab3
                EXEC USP_GetStudentWithMissingRequirementsAndRequirementGroup_FinAidforaStudent @StudentID,@CampusID,@ProgID;

        SELECT  *
        FROM    @tab1
        UNION
        SELECT  *
        FROM    @tab2
        UNION
        SELECT  *
        FROM    @tab3;

    END;






GO
