SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--exec usp_GeographicTypes_GetList "1"
CREATE PROCEDURE [dbo].[usp_GeographicTypes_GetList]
    @GeographicTypeDescrip VARCHAR(50)
AS
    SELECT  GeographicTypeId
    FROM    adGeographicTypes
    WHERE   Descrip LIKE @GeographicTypeDescrip + '%';



GO
