SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[USP_AR_RegStd_GetStudentGradewithCoreqID]
    (
     @StuEnrollId UNIQUEIDENTIFIER
    ,@CoReqID UNIQUEIDENTIFIER
    )
AS /*----------------------------------------------------------------------------------------------------
	Author          :	Saraswathi Lakshmanan
    
    Create date		:	02/03/2010
    
	Procedure Name	:	USP_AR_RegStd_GetStudentGradewithCoreqID

	Objective		:	get the Student grade given the Student EnrollmentId and coreqID
	
	Parameters		:	Name			Type	Data Type			Required? 	
						=====			====	=========			=========	
						@StuEnrollId	In		Uniqueidentifier	
						@CoreqID		In		Uniqueidentifier
	
	Output			:	Returns the Grades			
						
*/-----------------------------------------------------------------------------------------------------

/*
--To fix issue 17548: ENH: All: Register Students Page is Taking Too Long to Populate Available Students 
This query is used to find if the student has passing Grades 

*/

    BEGIN
        SELECT  X.ABC
        FROM    (
                  SELECT    COALESCE((
                                       SELECT TOP 1
                                                b.Grade
                                       FROM     arResults a
                                       INNER JOIN arGradeSystemDetails b ON b.GrdSysDetailId = a.GrdSysDetailId
                                       INNER JOIN arClassSections c ON a.TestId = c.ClsSectionId
                                       WHERE    a.StuEnrollId = @StuEnrollId
                                                AND c.ReqId = @CoReqID
                                       ORDER BY a.moddate DESC
                                     ),'') AS ABC
                  UNION ALL
                  SELECT    COALESCE((
                                       SELECT TOP 1
                                                b.Grade
                                       FROM     arTransferGrades a
                                       INNER JOIN arGradeSystemDetails b ON b.GrdSysDetailId = a.GrdSysDetailId
                                       INNER JOIN arTerm c ON a.TermId = c.TermId
                                       WHERE    a.StuEnrollId = @StuEnrollId
                                                AND a.ReqId = @CoReqID
                                       ORDER BY c.StartDate DESC
                                               ,a.moddate DESC
                                     ),'') AS ABC
                ) X
        WHERE   X.ABC <> ''; 	
    END;

 -----------------------------------------------------------------------------------------------------------------------------------
  
    IF EXISTS ( SELECT  *
                FROM    sysobjects
                WHERE   type = 'P'
                        AND name = 'USP_AR_RegStd_GetStudentGrade' )
        BEGIN
            DROP PROCEDURE USP_AR_RegStd_GetStudentGrade;
        END;



GO
