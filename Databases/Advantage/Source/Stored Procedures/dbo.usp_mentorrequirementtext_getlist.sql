SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_mentorrequirementtext_getlist]
    @ReqId UNIQUEIDENTIFIER
   ,@EffectiveDate DATETIME
AS
    SELECT DISTINCT
            MentorRequirement
           ,MentorOperator
           ,MentorOperatorOrder = CASE MentorOperator
                                    WHEN 'Select' THEN 0
                                    WHEN 'And' THEN 1
                                    WHEN 'Or' THEN 2
                                    ELSE 3
                                  END
           ,Speed
           ,OperatorSequence
    FROM    arMentor_GradeComponentTypes_Courses
    WHERE   ReqId = @ReqId
            AND EffectiveDate = @EffectiveDate
    ORDER BY OperatorSequence;



GO
