SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[usp_AR_GetAllStudentByStartDateAndProgram]
    @StartDate DATETIME
   ,@CampGrpId AS VARCHAR(8000)
   ,@ProgVerId AS VARCHAR(8000) = NULL
   ,@StudentIdentifier AS VARCHAR(50)
AS /*----------------------------------------------------------------------------------------------------
    Author : Vijay Ramteke
    
    Create date : 09/13/2010
    
    Procedure Name : usp_AR_GetAllStudentByStartDateAndProgram

    Objective : Get All The Student By Start Date And Program Version
    
    Parameters : Name Type Data Type Required? 
    
    Output : Returns All The Student By Start Date And Program Version for Report Dataset
                        
*/-----------------------------------------------------------------------------------------------------

    BEGIN

        DECLARE @IsCampusgrpAll INT;
        SELECT  @IsCampusgrpAll = COUNT(*)
        FROM    syCampGrps
        WHERE   CampGrpId IN ( SELECT   strval
                               FROM     dbo.SPLIT(@CampGrpId) )
                AND CampGrpDescrip = 'All';
        IF @IsCampusgrpAll > 0
            BEGIN
                SELECT  S.LastName + ', ' + S.FirstName + ' ' + ISNULL(S.MiddleName,'') AS StudentName
                       ,CASE @StudentIdentifier
                          WHEN 'StudentId' THEN S.StudentNumber
                          WHEN 'EnrollmentId' THEN SE.EnrollmentId
                          ELSE S.SSN
                        END AS StudentIdentifier
                       ,SS.ShiftDescrip AS Shift
                       ,SC.StatusCodeDescrip
                       , 
--L.LastName+', '+ L.FirstName As LeadName, 
                        SE.StartDate
                       ,SE.ExpGradDate
                       ,AR.FullName AS AdmissionRep
                       ,SE.PrgVerId
                       ,PV.PrgVerDescrip
                       ,@StartDate AS ParamStartDate
                       ,@CampGrpId AS ParamCampGrpId
                       ,@ProgVerId AS ParamProgVerId
                       ,SE.CampusId
                       ,C.CampDescrip
                       ,CG.CampGrpId
                       ,CG.CampGrpDescrip
                FROM    arStudent S
                INNER JOIN arStuEnrollments SE ON S.StudentId = SE.StudentId
                INNER JOIN syStatusCodes SC ON SE.StatusCodeId = SC.StatusCodeId
                INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                LEFT OUTER JOIN arShifts SS ON SE.ShiftId = SS.ShiftId
                LEFT OUTER JOIN syUsers AR ON SE.AdmissionsRep = AR.UserId
---
                INNER JOIN syCampuses C ON SE.CampusId = C.CampusId
                INNER JOIN syCmpGrpCmps CGC ON CGC.CampusId = C.CampusId
                INNER JOIN syCampGrps CG ON CGC.CampGrpId = CG.CampGrpId
----
                WHERE   SE.CampusId IN ( SELECT DISTINCT
                                                t1.CampusId
                                         FROM   syCmpGrpCmps t1
                                         WHERE  t1.CampGrpId IN ( SELECT    strval
                                                                  FROM      dbo.SPLIT(@CampGrpId) ) )
                        AND CAST(SE.StartDate AS DATE) = CAST(@StartDate AS DATE)
                        AND (
                              SE.PrgVerId IN ( SELECT   strval
                                               FROM     dbo.SPLIT(@ProgVerId) )
                              OR @ProgVerId IS NULL
                            )
                        AND CG.CampGrpDescrip = 'All'
                        AND CG.CampGrpId IN ( SELECT DISTINCT
                                                        t1.CampGrpId
                                              FROM      syCmpGrpCmps t1
                                              WHERE     t1.CampGrpId IN ( SELECT    strval
                                                                          FROM      dbo.SPLIT(@CampGrpId) ) )
                ORDER BY SE.PrgVerId
                       ,PV.PrgVerDescrip
                       ,S.LastName
                       ,S.FirstName
                       ,SE.StartDate;
            END;
        ELSE
            BEGIN
                SELECT  S.LastName + ', ' + S.FirstName + ' ' + ISNULL(S.MiddleName,'') AS StudentName
                       ,CASE @StudentIdentifier
                          WHEN 'StudentId' THEN S.StudentNumber
                          WHEN 'EnrollmentId' THEN SE.EnrollmentId
                          ELSE S.SSN
                        END AS StudentIdentifier
                       ,SS.ShiftDescrip AS Shift
                       ,SC.StatusCodeDescrip
                       , 
--L.LastName+', '+ L.FirstName As LeadName, 
                        SE.StartDate
                       ,SE.ExpGradDate
                       ,AR.FullName AS AdmissionRep
                       ,SE.PrgVerId
                       ,PV.PrgVerDescrip
                       ,@StartDate AS ParamStartDate
                       ,@CampGrpId AS ParamCampGrpId
                       ,@ProgVerId AS ParamProgVerId
                       ,SE.CampusId
                       ,C.CampDescrip
                       ,CG.CampGrpId
                       ,CG.CampGrpDescrip
                FROM    arStudent S
                INNER JOIN arStuEnrollments SE ON S.StudentId = SE.StudentId
                INNER JOIN syStatusCodes SC ON SE.StatusCodeId = SC.StatusCodeId
                INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                LEFT OUTER JOIN arShifts SS ON SE.ShiftId = SS.ShiftId
                LEFT OUTER JOIN syUsers AR ON SE.AdmissionsRep = AR.UserId
---
                INNER JOIN syCampuses C ON SE.CampusId = C.CampusId
                INNER JOIN syCmpGrpCmps CGC ON CGC.CampusId = C.CampusId
                INNER JOIN syCampGrps CG ON CGC.CampGrpId = CG.CampGrpId
----
                WHERE   SE.CampusId IN ( SELECT DISTINCT
                                                t1.CampusId
                                         FROM   syCmpGrpCmps t1
                                         WHERE  t1.CampGrpId IN ( SELECT    strval
                                                                  FROM      dbo.SPLIT(@CampGrpId) ) )
                        AND CAST(SE.StartDate AS DATE) = CAST(@StartDate AS DATE)
                        AND (
                              SE.PrgVerId IN ( SELECT   strval
                                               FROM     dbo.SPLIT(@ProgVerId) )
                              OR @ProgVerId IS NULL
                            )
                        AND CG.CampGrpDescrip <> 'All'
                        AND CG.CampGrpId IN ( SELECT DISTINCT
                                                        t1.CampGrpId
                                              FROM      syCmpGrpCmps t1
                                              WHERE     t1.CampGrpId IN ( SELECT    strval
                                                                          FROM      dbo.SPLIT(@CampGrpId) ) )
                ORDER BY SE.PrgVerId
                       ,PV.PrgVerDescrip
                       ,S.LastName
                       ,S.FirstName
                       ,SE.StartDate;
            END;

    END;








GO
