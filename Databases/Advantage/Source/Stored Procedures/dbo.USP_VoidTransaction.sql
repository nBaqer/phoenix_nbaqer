SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[USP_VoidTransaction]
    @TransactionId UNIQUEIDENTIFIER
AS
    BEGIN

        BEGIN TRY
	
            SET NOCOUNT ON;
            BEGIN
		
                BEGIN TRANSACTION;
			
                UPDATE  adLeadTransactions
                SET     Voided = 1
                WHERE   TransactionId = @TransactionId;
			
                DECLARE @LeadId UNIQUEIDENTIFIER;
                SET @LeadId = (
                                SELECT  LeadId
                                FROM    dbo.adLeadTransactions
                                WHERE   TransactionId = @TransactionId
                              );

                DECLARE @ChgTransId UNIQUEIDENTIFIER;
                SET @ChgTransId = (
                                    SELECT  TransactionId
                                    FROM    adLeadTransactions a
                                    WHERE   a.TransCodeId = (
                                                              SELECT    b.TransCodeId
                                                              FROM      dbo.adLeadTransactions b
                                                              WHERE     LeadId = @LeadId
                                                                        AND TransactionId = @TransactionId
                                                            )
                                            AND a.CreatedDate = (
                                                                  SELECT    b.CreatedDate
                                                                  FROM      dbo.adLeadTransactions b
                                                                  WHERE     LeadId = @LeadId
                                                                            AND TransactionId = @TransactionId
                                                                )
                                            AND a.TransReference = (
                                                                     SELECT b.TransReference
                                                                     FROM   dbo.adLeadTransactions b
                                                                     WHERE  LeadId = @LeadId
                                                                            AND TransactionId = @TransactionId
                                                                   )
                                            AND ABS(a.TransAmount) = (
                                                                       SELECT   ABS(b.TransAmount) AS TransAmt
                                                                       FROM     dbo.adLeadTransactions b
                                                                       WHERE    LeadId = @LeadId
                                                                                AND TransactionId = @TransactionId
                                                                     )
                                            AND a.TransTypeId = 0
                                  );
								
								
                UPDATE  adLeadTransactions
                SET     Voided = 1
                WHERE   TransactionId = @ChgTransId;					
								
                COMMIT TRANSACTION;
			
            END;

        END TRY
		
        BEGIN CATCH
	
            ROLLBACK TRANSACTION;
		
        END CATCH;	  
    END;



GO
