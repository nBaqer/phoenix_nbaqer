SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[USP_FL_GetStudentInfo] @SSN VARCHAR(12)
AS
    SELECT DISTINCT
            FirstName
           ,LastName
    FROM    arStudent
    WHERE   SSN = @SSN;




GO
