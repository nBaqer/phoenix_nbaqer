SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create PROCEDURE [dbo].[CreateAdvantageDbPermission]
    @User VARCHAR(100)
AS
    BEGIN
	DECLARE @DatabaseName NVARCHAR(100);
	DECLARE @sql NVARCHAR(max);
	SET @DatabaseName = (
                            SELECT DB_NAME() AS [Current Database]
                            );
	SET @sql = 'use ' + @DatabaseName + ' CREATE USER [' + @User  + '] FOR LOGIN [' + @User + '];  ALTER ROLE [db_owner] ADD MEMBER [' + @User + '];'
	EXEC(@sql)
        SET NOCOUNT OFF;
    END;
GO
