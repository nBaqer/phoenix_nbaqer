SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_GetRemainingCourses]
    @stuenrollid UNIQUEIDENTIFIER
   ,@termid UNIQUEIDENTIFIER
AS /*----------------------------------------------------------------------------------------------------
	Author          :	Bruce Shanblatt
	
	Create date		:	06/18/2013
	
	Procedure Name	:	USP_GetRemainingCourses
	
	Parameters		:	Name			Type	Data Type	            Required? 	
						=====			====	=========	            =========	
						@stuenrollid	In		UNIQUEIDENTIFIER		Required	
						@termid		    In		UNIQUEIDENTIFIER		Required	
						
*/-----------------------------------------------------------------------------------------------------
-- Section 1 : Get the list of student's courses offered for the selected term and currently enrolled program


--declare @StuEnrollId uniqueidentifier, @TermId uniqueidentifier

--Set @StuEnrollId = '62ADD563-C9FA-4072-B281-43240579BAA3'
--Set @TermId = '00000000-0000-0000-0000-000000000000'

--declare @StuEnrollId uniqueidentifier, @TermId uniqueidentifier

--Set @StuEnrollId ='62ADD563-C9FA-4072-B281-43240579BAA3'
--Set @TermId = '00000000-0000-0000-0000-000000000000'

--Select * from arResults 
--Select * from arStuEnrollments where StudentId in (Select StudentId from arStudent where firstname='QATest05')

    DECLARE @ListCoursesOfferedForTermAndProgramStudentIsEnrolledIn TABLE
        (
         StuEnrollId UNIQUEIDENTIFIER
        ,StartDate DATETIME
        ,ExpStartDate DATETIME
        ,LastName VARCHAR(50)
        ,FirstName VARCHAR(50)
        ,ExpGradDate DATETIME
        ,PrgVerId UNIQUEIDENTIFIER
        ,PrgVerDescrip VARCHAR(50)
        ,ReqId UNIQUEIDENTIFIER
        ,Req VARCHAR(100)
        ,Code VARCHAR(50)
        ,Credits DECIMAL(18,2)
        ,Hours DECIMAL(18,2)
        ,AllowCompletedCourseRetake BIT
        ,IsRequired VARCHAR(10)
        ,CampDescrip VARCHAR(100)
        ,Override UNIQUEIDENTIFIER
        ,ClsSectionId UNIQUEIDENTIFIER
        ,ClassStartDate DATETIME
        ,ClassEndDate DATETIME
			--,TermDescrip varchar(50),TermStartDate datetime,TermEndDate datetime
			
		);

-- Section 2: Get the list of student's previous attempts in the currently enrolled program
    DECLARE @ListCoursesStudentHasAlreadyAttempted TABLE
        (
         CourseId UNIQUEIDENTIFIER
        ,Code VARCHAR(50)
        ,CourseDescription VARCHAR(50)
        ,StuEnrollId UNIQUEIDENTIFIER
        ,IsPass BIT
        ,TermDescrip VARCHAR(50)
        );

-- Section 3: Get the list of courses student is currently scheduled in		
    DECLARE @ListCoursesStudentIsCurrentlyRegisteredIn TABLE
        (
         CourseId UNIQUEIDENTIFIER
        ,Code VARCHAR(50)
        ,CourseDescription VARCHAR(50)
        ,TermDescrip VARCHAR(50)
        );
		


-- Section 1: List of courses offered for the term and program the student is currently enrolled in
    INSERT  INTO @ListCoursesOfferedForTermAndProgramStudentIsEnrolledIn
            SELECT  *
            FROM    (
                      SELECT DISTINCT
                                StudentEnrollment.StuEnrollId
                               ,StudentEnrollment.StartDate
                               ,StudentEnrollment.ExpStartDate
                               ,Student.LastName
                               ,Student.FirstName
                               ,StudentEnrollment.ExpGradDate
                               ,StudentEnrollment.PrgVerId
                               ,ProgramVersions.PrgVerDescrip
                               ,ProgramVersionDefinitions.ReqId AS ReqId
                               ,Requirements.Descrip AS Req
                               ,Requirements.Code AS Code
                               ,Requirements.Credits
                               ,Requirements.Hours
                               ,Requirements.AllowCompletedCourseRetake
                               ,IsRequired = ( CASE ProgramVersionDefinitions.IsRequired
                                                 WHEN 0 THEN 'False'
                                                 ELSE 'True'
                                               END )
                               ,(
                                  SELECT    CampDescrip
                                  FROM      syCampuses
                                  WHERE     CampusId = StudentEnrollment.CampusId
                                ) AS CampDescrip
                               ,ISNULL(ProgramVersionDefinitions.GrdSysDetailId,'00000000-0000-0000-0000-000000000000') AS OVERRIDE
                               ,ClassSections.ClsSectionId
                               ,ClassSections.StartDate AS ClassStartDate
                               ,ClassSections.EndDate AS ClassEndDate
	--Term.TermDescrip,Term.StartDate as TermStartDate,Term.EndDate as TermEndDate
                      FROM      arStuEnrollments StudentEnrollment
                      INNER JOIN arStudent Student ON StudentEnrollment.StudentId = Student.StudentId
                      INNER JOIN arPrgVersions ProgramVersions ON StudentEnrollment.PrgVerId = ProgramVersions.PrgVerId
                      INNER JOIN arProgVerDef ProgramVersionDefinitions ON ProgramVersions.PrgVerId = ProgramVersionDefinitions.PrgVerId
                      INNER JOIN arReqs Requirements ON ProgramVersionDefinitions.ReqId = Requirements.ReqId
                      INNER JOIN syStatuses Statuses ON Requirements.StatusId = Statuses.StatusId
                      INNER JOIN arClassSections ClassSections ON Requirements.ReqId = ClassSections.ReqId
                      INNER JOIN dbo.arTerm Term ON ClassSections.TermId = Term.TermId
                      WHERE     Requirements.ReqTypeId = 1
                                AND StudentEnrollment.StuEnrollId = @stuenrollid
                                AND (
                                      @termid = '00000000-0000-0000-0000-000000000000'
                                      OR ClassSections.TermId IN ( @termid )
                                    )
                                AND ClassSections.MaxStud > (
                                                              SELECT    COUNT(*)
                                                              FROM      arResults RS
                                                              WHERE     RS.TestId = ClassSections.ClsSectionId
                                                            )
                      UNION
                      SELECT DISTINCT
                                StudentEnrollment.StuEnrollId
                               ,StudentEnrollment.StartDate
                               ,StudentEnrollment.ExpStartDate
                               ,Student.LastName
                               ,Student.FirstName
                               ,StudentEnrollment.ExpGradDate
                               ,StudentEnrollment.PrgVerId
                               ,ProgramVersions.PrgVerDescrip
                               ,RequirementGroupDefinitions.ReqId AS ReqId
                               ,Requirements.Descrip AS Req
                               ,Requirements.Code AS Code
                               ,Requirements.Credits
                               ,Requirements.Hours
                               ,Requirements.AllowCompletedCourseRetake
                               ,IsRequired = ( CASE ProgramVersionDefinitions.IsRequired
                                                 WHEN 0 THEN 'False'
                                                 ELSE 'True'
                                               END )
                               ,(
                                  SELECT    CampDescrip
                                  FROM      syCampuses
                                  WHERE     CampusId = StudentEnrollment.CampusId
                                ) AS CampDescrip
                               ,ISNULL(ProgramVersionDefinitions.GrdSysDetailId,'00000000-0000-0000-0000-000000000000') AS Override
                               ,ClassSections.ClsSectionId
                               ,ClassSections.StartDate AS ClassStartDate
                               ,ClassSections.EndDate AS ClassEndDate
	--Term.TermDescrip,Term.StartDate as TermStartDate,Term.EndDate as TermEndDate
                      FROM      arStuEnrollments StudentEnrollment
                      INNER JOIN arStudent Student ON StudentEnrollment.StudentId = Student.StudentId
                      INNER JOIN arPrgVersions ProgramVersions ON StudentEnrollment.PrgVerId = ProgramVersions.PrgVerId
                      INNER JOIN arProgVerDef ProgramVersionDefinitions ON ProgramVersions.PrgVerId = ProgramVersionDefinitions.PrgVerId
                      INNER JOIN arReqGrpDef RequirementGroupDefinitions ON ProgramVersionDefinitions.ReqId = RequirementGroupDefinitions.GrpId
                      INNER JOIN arReqs Requirements ON RequirementGroupDefinitions.ReqId = Requirements.ReqId
                      INNER JOIN syStatuses Statuses ON Requirements.StatusId = Statuses.StatusId
                      INNER JOIN arClassSections ClassSections ON Requirements.ReqId = ClassSections.ReqId
                      INNER JOIN dbo.arTerm Term ON ClassSections.TermId = Term.TermId
                      WHERE     Requirements.ReqTypeId = 1
                                AND StudentEnrollment.StuEnrollId = @stuenrollid
                                AND (
                                      @termid = '00000000-0000-0000-0000-000000000000'
                                      OR ClassSections.TermId IN ( @termid )
                                    )
                                AND ClassSections.MaxStud > (
                                                              SELECT    COUNT(*)
                                                              FROM      arResults RS
                                                              WHERE     RS.TestId = ClassSections.ClsSectionId
                                                            )
                      UNION
                      SELECT DISTINCT
                                StudentEnrollment.StuEnrollId
                               ,StudentEnrollment.StartDate
                               ,StudentEnrollment.ExpStartDate
                               ,Student.LastName
                               ,Student.FirstName
                               ,StudentEnrollment.ExpGradDate
                               ,StudentEnrollment.PrgVerId
                               ,ProgramVersions.PrgVerDescrip
                               ,RequirementGroupDefinitions_1.ReqId AS ReqId
                               ,Requirements.Descrip AS Req
                               ,Requirements.Code AS Code
                               ,Requirements.Credits
                               ,Requirements.Hours
                               ,Requirements.AllowCompletedCourseRetake
                               ,IsRequired = ( CASE ProgramVersionDefinitions.IsRequired
                                                 WHEN 0 THEN 'False'
                                                 ELSE 'True'
                                               END )
                               ,(
                                  SELECT    CampDescrip
                                  FROM      syCampuses
                                  WHERE     CampusId = StudentEnrollment.CampusId
                                ) AS CampDescrip
                               ,ISNULL(ProgramVersionDefinitions.GrdSysDetailId,'00000000-0000-0000-0000-000000000000') AS Override
                               ,ClassSections.ClsSectionId
                               ,ClassSections.StartDate AS ClassStartDate
                               ,ClassSections.EndDate AS ClassEndDate
	--Term.TermDescrip,Term.StartDate as TermStartDate,Term.EndDate as TermEndDate
                      FROM      arStuEnrollments StudentEnrollment
                      INNER JOIN arStudent Student ON StudentEnrollment.StudentId = Student.StudentId
                      INNER JOIN arPrgVersions ProgramVersions ON StudentEnrollment.PrgVerId = ProgramVersions.PrgVerId
                      INNER JOIN arProgVerDef ProgramVersionDefinitions ON ProgramVersions.PrgVerId = ProgramVersionDefinitions.PrgVerId
                      INNER JOIN arReqGrpDef RequirementGroupDefinitions ON ProgramVersionDefinitions.ReqId = RequirementGroupDefinitions.GrpId
                      INNER JOIN arReqGrpDef RequirementGroupDefinitions_1 ON RequirementGroupDefinitions.ReqId = RequirementGroupDefinitions_1.GrpId
                      INNER JOIN arReqs Requirements ON RequirementGroupDefinitions.ReqId = Requirements.ReqId
                      INNER JOIN syStatuses Statuses ON Requirements.StatusId = Statuses.StatusId
                      INNER JOIN arClassSections ClassSections ON Requirements.ReqId = ClassSections.ReqId
                      INNER JOIN dbo.arTerm Term ON ClassSections.TermId = Term.TermId
                      WHERE     Requirements.ReqTypeId = 1
                                AND StudentEnrollment.StuEnrollId = @stuenrollid
                                AND (
                                      @termid = '00000000-0000-0000-0000-000000000000'
                                      OR ClassSections.TermId IN ( @termid )
                                    )
                                AND ClassSections.MaxStud > (
                                                              SELECT    COUNT(*)
                                                              FROM      arResults RS
                                                              WHERE     RS.TestId = ClassSections.ClsSectionId
                                                            )
                      UNION
                      SELECT DISTINCT
                                StudentEnrollment.StuEnrollId
                               ,StudentEnrollment.StartDate
                               ,StudentEnrollment.ExpStartDate
                               ,Student.LastName
                               ,Student.FirstName
                               ,StudentEnrollment.ExpGradDate
                               ,StudentEnrollment.PrgVerId
                               ,ProgramVersions.PrgVerDescrip
                               ,RequirementGroupDefinitions_2.ReqId AS ReqId
                               ,Requirements.Descrip AS Req
                               ,Requirements.Code AS Code
                               ,Requirements.Credits
                               ,Requirements.Hours
                               ,Requirements.AllowCompletedCourseRetake
                               ,IsRequired = ( CASE ProgramVersionDefinitions.IsRequired
                                                 WHEN 0 THEN 'False'
                                                 ELSE 'True'
                                               END )
                               ,(
                                  SELECT    CampDescrip
                                  FROM      syCampuses
                                  WHERE     CampusId = StudentEnrollment.CampusId
                                ) AS CampDescrip
                               ,ISNULL(ProgramVersionDefinitions.GrdSysDetailId,'00000000-0000-0000-0000-000000000000') AS Override
                               ,ClassSections.ClsSectionId
                               ,ClassSections.StartDate AS ClassStartDate
                               ,ClassSections.EndDate AS ClassEndDate
	--Term.TermDescrip,Term.StartDate as TermStartDate,Term.EndDate as TermEndDate
                      FROM      arStuEnrollments StudentEnrollment
                      INNER JOIN arStudent Student ON StudentEnrollment.StudentId = Student.StudentId
                      INNER JOIN arPrgVersions ProgramVersions ON StudentEnrollment.PrgVerId = ProgramVersions.PrgVerId
                      INNER JOIN arProgVerDef ProgramVersionDefinitions ON StudentEnrollment.PrgVerId = ProgramVersionDefinitions.PrgVerId
                      INNER JOIN arReqGrpDef RequirementGroupDefinitions ON ProgramVersionDefinitions.ReqId = RequirementGroupDefinitions.GrpId
                      INNER JOIN arReqGrpDef RequirementGroupDefinitions_1 ON RequirementGroupDefinitions.ReqId = RequirementGroupDefinitions_1.GrpId
                      INNER JOIN arReqs Requirements ON RequirementGroupDefinitions.ReqId = Requirements.ReqId
                      INNER JOIN arReqGrpDef RequirementGroupDefinitions_2 ON Requirements.ReqId = RequirementGroupDefinitions_2.ReqId
                      INNER JOIN syStatuses Statuses ON Requirements.StatusId = Statuses.StatusId
                      INNER JOIN arClassSections ClassSections ON Requirements.ReqId = ClassSections.ReqId
                      INNER JOIN dbo.arTerm Term ON ClassSections.TermId = Term.TermId
                      WHERE     Requirements.ReqTypeId = 1
                                AND StudentEnrollment.StuEnrollId = @stuenrollid
                                AND (
                                      @termid = '00000000-0000-0000-0000-000000000000'
                                      OR ClassSections.TermId IN ( @termid )
                                    )
                                AND ClassSections.MaxStud > (
                                                              SELECT    COUNT(*)
                                                              FROM      arResults RS
                                                              WHERE     RS.TestId = ClassSections.ClsSectionId
                                                            )
                      UNION
                      SELECT DISTINCT
                                StudentEnrollment.StuEnrollId
                               ,StudentEnrollment.StartDate
                               ,StudentEnrollment.ExpStartDate
                               ,Student.LastName
                               ,Student.FirstName
                               ,StudentEnrollment.ExpGradDate
                               ,StudentEnrollment.PrgVerId
                               ,ProgramVersions.PrgVerDescrip
                               ,RequirementGroupDefinitions_3.ReqId AS ReqId
                               ,Requirements.Descrip AS Req
                               ,Requirements.Code AS Code
                               ,Requirements.Credits
                               ,Requirements.Hours
                               ,Requirements.AllowCompletedCourseRetake
                               ,IsRequired = ( CASE ProgramVersionDefinitions.IsRequired
                                                 WHEN 0 THEN 'False'
                                                 ELSE 'True'
                                               END )
                               ,(
                                  SELECT    CampDescrip
                                  FROM      syCampuses
                                  WHERE     CampusId = StudentEnrollment.CampusId
                                ) AS CampDescrip
                               ,ISNULL(ProgramVersionDefinitions.GrdSysDetailId,'00000000-0000-0000-0000-000000000000') AS Override
                               ,ClassSections.ClsSectionId
                               ,ClassSections.StartDate AS ClassStartDate
                               ,ClassSections.EndDate AS ClassEndDate
	--Term.TermDescrip,Term.StartDate as TermStartDate,Term.EndDate as TermEndDate
                      FROM      arStuEnrollments StudentEnrollment
                      INNER JOIN arStudent Student ON StudentEnrollment.StudentId = Student.StudentId
                      INNER JOIN arPrgVersions ProgramVersions ON StudentEnrollment.PrgVerId = ProgramVersions.PrgVerId
                      INNER JOIN arProgVerDef ProgramVersionDefinitions ON StudentEnrollment.PrgVerId = ProgramVersionDefinitions.PrgVerId
                      INNER JOIN arReqGrpDef RequirementGroupDefinitions ON ProgramVersionDefinitions.ReqId = RequirementGroupDefinitions.GrpId
                      INNER JOIN arReqGrpDef RequirementGroupDefinitions_1 ON RequirementGroupDefinitions.ReqId = RequirementGroupDefinitions_1.GrpId
                      INNER JOIN arReqGrpDef RequirementGroupDefinitions_2 ON RequirementGroupDefinitions_1.ReqId = RequirementGroupDefinitions_2.GrpId
                      INNER JOIN arReqGrpDef RequirementGroupDefinitions_3 ON RequirementGroupDefinitions_2.ReqId = RequirementGroupDefinitions_3.GrpId
                      INNER JOIN arReqs Requirements ON Requirements.ReqId = RequirementGroupDefinitions_3.ReqId
                      INNER JOIN syStatuses Statuses ON Requirements.StatusId = Statuses.StatusId
                      INNER JOIN arClassSections ClassSections ON Requirements.ReqId = ClassSections.ReqId
                      INNER JOIN dbo.arTerm Term ON ClassSections.TermId = Term.TermId
                      WHERE     Requirements.ReqTypeId = 1
                                AND StudentEnrollment.StuEnrollId = @stuenrollid
                                AND (
                                      @termid = '00000000-0000-0000-0000-000000000000'
                                      OR ClassSections.TermId IN ( @termid )
                                    )
                                AND ClassSections.MaxStud > (
                                                              SELECT    COUNT(*)
                                                              FROM      arResults RS
                                                              WHERE     RS.TestId = ClassSections.ClsSectionId
                                                            )
                      UNION
                      SELECT DISTINCT
                                StudentEnrollment.StuEnrollId
                               ,StudentEnrollment.StartDate
                               ,StudentEnrollment.ExpStartDate
                               ,Student.LastName
                               ,Student.FirstName
                               ,StudentEnrollment.ExpGradDate
                               ,StudentEnrollment.PrgVerId
                               ,ProgramVersions.PrgVerDescrip
                               ,RequirementGroupDefinitions_4.ReqId AS ReqId
                               ,Requirements.Descrip AS Req
                               ,Requirements.Code AS Code
                               ,Requirements.Credits
                               ,Requirements.Hours
                               ,Requirements.AllowCompletedCourseRetake
                               ,IsRequired = ( CASE ProgramVersionDefinitions.IsRequired
                                                 WHEN 0 THEN 'False'
                                                 ELSE 'True'
                                               END )
                               ,(
                                  SELECT    CampDescrip
                                  FROM      syCampuses
                                  WHERE     CampusId = StudentEnrollment.CampusId
                                ) AS CampDescrip
                               ,ISNULL(ProgramVersionDefinitions.GrdSysDetailId,'00000000-0000-0000-0000-000000000000') AS Override
                               ,ClassSections.ClsSectionId
                               ,ClassSections.StartDate AS ClassStartDate
                               ,ClassSections.EndDate AS ClassEndDate
	--Term.TermDescrip,Term.StartDate as TermStartDate,Term.EndDate as TermEndDate
                      FROM      arStuEnrollments StudentEnrollment --t100,
                      INNER JOIN arStudent Student ON StudentEnrollment.StudentId = Student.StudentId
                      INNER JOIN arPrgVersions ProgramVersions ON StudentEnrollment.PrgVerId = ProgramVersions.PrgVerId
                      INNER JOIN arProgVerDef ProgramVersionDefinitions ON StudentEnrollment.PrgVerId = ProgramVersionDefinitions.PrgVerId
                      INNER JOIN arReqGrpDef RequirementGroupDefinitions ON ProgramVersionDefinitions.ReqId = RequirementGroupDefinitions.GrpId
                      INNER JOIN arReqGrpDef RequirementGroupDefinitions_1 ON RequirementGroupDefinitions.ReqId = RequirementGroupDefinitions_1.GrpId
                      INNER JOIN arReqGrpDef RequirementGroupDefinitions_2 ON RequirementGroupDefinitions_1.ReqId = RequirementGroupDefinitions_2.GrpId
                      INNER JOIN arReqGrpDef RequirementGroupDefinitions_3 ON RequirementGroupDefinitions_2.ReqId = RequirementGroupDefinitions_3.GrpId
                      INNER JOIN arReqGrpDef RequirementGroupDefinitions_4 ON RequirementGroupDefinitions_3.ReqId = RequirementGroupDefinitions_4.GrpId
                      INNER JOIN arReqs Requirements ON Requirements.ReqId = RequirementGroupDefinitions_4.ReqId
                      INNER JOIN syStatuses Statuses ON Requirements.StatusId = Statuses.StatusId
                      INNER JOIN arClassSections ClassSections ON Requirements.ReqId = ClassSections.ReqId
                      INNER JOIN dbo.arTerm Term ON ClassSections.TermId = Term.TermId
                      WHERE     Requirements.ReqTypeId = 1
                                AND StudentEnrollment.StuEnrollId = @stuenrollid
                                AND (
                                      @termid = '00000000-0000-0000-0000-000000000000'
                                      OR ClassSections.TermId IN ( @termid )
                                    )
                                AND ClassSections.MaxStud > (
                                                              SELECT    COUNT(*)
                                                              FROM      arResults RS
                                                              WHERE     RS.TestId = ClassSections.ClsSectionId
                                                            )
                    ) P; 
	 
 -- Section 2: This section gets the list of courses student has already attempted 
    INSERT  INTO @ListCoursesStudentHasAlreadyAttempted
            SELECT DISTINCT
                    ReqId
                   ,Code
                   ,Descrip
                   ,StuEnrollId
                   ,ISPass
                   ,TermDescrip
            FROM    (
                      SELECT DISTINCT
                                ClassSections.TermId
                               ,Terms.TermDescrip
                               ,ClassSections.ReqId
                               ,Requirements.Code
                               ,Requirements.Descrip
                               ,Requirements.Credits
                               ,Requirements.Hours
                               ,Requirements.AllowCompletedCourseRetake
                               ,Terms.StartDate
                               ,Terms.EndDate
                               ,Results.GrdSysDetailId
                               ,(
                                  SELECT    Grade
                                  FROM      arGradeSystemDetails
                                  WHERE     GrdSysDetailId = Results.GrdSysDetailId
                                ) AS Grade
                               ,CASE WHEN (
                                            SELECT  GrdSysDetailId
                                            FROM    arProgVerDef
                                            WHERE   ReqId = ClassSections.ReqId
                                                    AND PrgVerId = StudentEnrollment.PrgVerId
                                          ) IS NOT NULL THEN ( 0 )
                                     ELSE (
                                            SELECT  IsPass
                                            FROM    arGradeSystemDetails
                                            WHERE   GrdSysDetailId = Results.GrdSysDetailId
                                          )
                                END AS ISPass
                               ,(
                                  SELECT    GPA
                                  FROM      arGradeSystemDetails
                                  WHERE     GrdSysDetailId = Results.GrdSysDetailId
                                ) AS GPA
                               ,(
                                  SELECT    IsCreditsAttempted
                                  FROM      arGradeSystemDetails
                                  WHERE     GrdSysDetailId = Results.GrdSysDetailId
                                ) AS IsCreditsAttempted
                               ,(
                                  SELECT    IsCreditsEarned
                                  FROM      arGradeSystemDetails
                                  WHERE     GrdSysDetailId = Results.GrdSysDetailId
                                ) AS IsCreditsEarned
                               ,(
                                  SELECT    IsInGPA
                                  FROM      arGradeSystemDetails
                                  WHERE     GrdSysDetailId = Results.GrdSysDetailId
                                ) AS IsInGPA
                               ,Results.StuEnrollId
                      FROM      arResults Results --t1,
                      INNER JOIN arClassSections ClassSections ON Results.TestId = ClassSections.ClsSectionId
                      INNER JOIN arTerm Terms ON ClassSections.TermId = Terms.TermId
                      INNER JOIN arReqs Requirements ON ClassSections.ReqId = Requirements.ReqId
                      INNER JOIN arStuEnrollments StudentEnrollment ON Results.StuEnrollId = StudentEnrollment.StuEnrollId
                      WHERE     Results.GrdSysDetailId IS NOT NULL
                                AND  -- Checking to see if student is currently registered in a course
                                Results.StuEnrollId = @stuenrollid
                      UNION
                      SELECT DISTINCT
                                TransferGrades.TermId
                               ,Terms.TermDescrip
                               ,TransferGrades.ReqId
                               ,Requirements.Code
                               ,Requirements.Descrip
                               ,Requirements.Credits
                               ,Requirements.Hours
                               ,Requirements.AllowCompletedCourseRetake
                               ,Terms.StartDate
                               ,Terms.EndDate
                               ,TransferGrades.GrdSysDetailId
                               ,(
                                  SELECT    Grade
                                  FROM      arGradeSystemDetails
                                  WHERE     GrdSysDetailId = TransferGrades.GrdSysDetailId
                                ) AS Grade
                               ,CASE WHEN (
                                            SELECT  GrdSysDetailId
                                            FROM    arProgVerDef
                                            WHERE   ReqId = Requirements.ReqId
                                                    AND PrgVerId = StudentEnrollment.PrgVerId
                                          ) IS NOT NULL THEN ( 0 )
                                     ELSE (
                                            SELECT  IsPass
                                            FROM    arGradeSystemDetails
                                            WHERE   GrdSysDetailId = TransferGrades.GrdSysDetailId
                                          )
                                END AS ISPass
                               ,(
                                  SELECT    GPA
                                  FROM      arGradeSystemDetails
                                  WHERE     GrdSysDetailId = TransferGrades.GrdSysDetailId
                                ) AS GPA
                               ,(
                                  SELECT    IsCreditsAttempted
                                  FROM      arGradeSystemDetails
                                  WHERE     GrdSysDetailId = TransferGrades.GrdSysDetailId
                                ) AS IsCreditsAttempted
                               ,(
                                  SELECT    IsCreditsEarned
                                  FROM      arGradeSystemDetails
                                  WHERE     GrdSysDetailId = TransferGrades.GrdSysDetailId
                                ) AS IsCreditsEarned
                               ,(
                                  SELECT    IsInGPA
                                  FROM      arGradeSystemDetails
                                  WHERE     GrdSysDetailId = TransferGrades.GrdSysDetailId
                                ) AS IsInGPA
                               ,TransferGrades.StuEnrollId
                      FROM      arTransferGrades TransferGrades --t10,
                      INNER JOIN arTerm Terms ON TransferGrades.TermId = Terms.TermId
                      INNER JOIN arReqs Requirements ON TransferGrades.ReqId = Requirements.ReqId
                      INNER JOIN arStuEnrollments StudentEnrollment ON TransferGrades.StuEnrollId = StudentEnrollment.StuEnrollId
                      WHERE     TransferGrades.GrdSysDetailId IS NOT NULL
                                AND -- Checking to see if student is currently registered in a transferred course
                                TransferGrades.StuEnrollId = @stuenrollid
                      UNION
                      SELECT DISTINCT
                                ClassSections.TermId
                               ,Terms.TermDescrip
                               ,S.ReqId
                               ,Requirements.Code
                               ,Requirements.Descrip
                               ,Requirements.Credits
                               ,Requirements.Hours
                               ,Requirements.AllowCompletedCourseRetake
                               ,Terms.StartDate
                               ,Terms.EndDate
                               ,Results.GrdSysDetailId
                               ,(
                                  SELECT    Grade
                                  FROM      arGradeSystemDetails
                                  WHERE     GrdSysDetailId = Results.GrdSysDetailId
                                ) AS Grade
                               ,(
                                  SELECT    IsPass
                                  FROM      arGradeSystemDetails
                                  WHERE     GrdSysDetailId = Results.GrdSysDetailId
                                ) AS IsPass
                               ,(
                                  SELECT    ISNULL(GPA,0)
                                  FROM      arGradeSystemDetails
                                  WHERE     GrdSysDetailId = Results.GrdSysDetailId
                                ) AS GPA
                               ,(
                                  SELECT    IsCreditsAttempted
                                  FROM      arGradeSystemDetails
                                  WHERE     GrdSysDetailId = Results.GrdSysDetailId
                                ) AS IsCreditsAttempted
                               ,(
                                  SELECT    IsCreditsEarned
                                  FROM      arGradeSystemDetails
                                  WHERE     GrdSysDetailId = Results.GrdSysDetailId
                                ) AS IsCreditsEarned
                               ,(
                                  SELECT    IsInGPA
                                  FROM      arGradeSystemDetails
                                  WHERE     GrdSysDetailId = Results.GrdSysDetailId
                                ) AS IsInGPA
                               ,Results.StuEnrollId
                      FROM      (
                                  -- get equivalent courses
                                  SELECT DISTINCT
                                            CourseEquivalent.ReqId AS EqReqId
                                           ,Requirements.ReqId
                                           ,StudentEnrollment.StuEnrollId
                                  FROM      arStuEnrollments StudentEnrollment --t100,
                                  INNER JOIN arStudent Student ON StudentEnrollment.StudentId = Student.StudentId
                                  INNER JOIN arPrgVersions ProgramVersions ON StudentEnrollment.PrgVerId = ProgramVersions.PrgVerId
                                  INNER JOIN arProgVerDef ProgramVersionDefinitions ON StudentEnrollment.PrgVerId = ProgramVersionDefinitions.PrgVerId
                                  INNER JOIN arReqs Requirements ON Requirements.ReqId = ProgramVersionDefinitions.ReqId
                                  INNER JOIN arCourseEquivalent CourseEquivalent ON Requirements.ReqId = CourseEquivalent.EquivReqId
                                  WHERE     StudentEnrollment.StuEnrollId = @stuenrollid
                                            AND 
	-- Checking to see if student already has not taken equivalent courses
                                            Requirements.ReqId NOT IN ( SELECT  ReqId
                                                                        FROM    arResults Results
                                                                        INNER JOIN arClassSections ClassSections ON Results.TestId = ClassSections.ClsSectionId
                                                                        WHERE   StuEnrollId = @stuenrollid
                                                                                AND ReqId = ProgramVersionDefinitions.ReqId )
                                ) S
                               ,arResults Results
                               , --t1, 
                                arReqs Requirements
                               , --t2, 
                                arTerm Terms
                               , --t3, 
                                arClassSections ClassSections
                               , -- t4, 
                                arClassSectionTerms ClassSectionTerms
                               , --t5, 
                                arStuEnrollments StudentEnrollment --t6 
                      WHERE     Results.TestId = ClassSections.ClsSectionId
                                AND ClassSections.ClsSectionId = ClassSectionTerms.ClsSectionId
                                AND ClassSectionTerms.TermId = Terms.TermId
                                AND ClassSections.ReqId = Requirements.ReqId
                                AND Results.StuEnrollId = StudentEnrollment.StuEnrollId
                                AND Results.GrdSysDetailId IS NOT NULL
                                AND --Graded courses
                                Results.StuEnrollId = S.StuEnrollId
                                AND Requirements.ReqId = S.EqReqId
                    ) A
            WHERE   (
                      A.StuEnrollId = @stuenrollid
  -- AND  P.ReqId=A.ReqId  -- don't need to look at first table variable, we just need all failed courses attempted earlier
  -- AND A.IsPass=0   -- If don't have to check if student passed or failed
                      AND A.AllowCompletedCourseRetake = 0 -- If course doesn't allow retakes ignore completed courses
                      AND A.ISPass = 1
                    );
  --OR  --OR condition added for DE9783
  --(A.StuEnrollId = @stuEnrollId
  --AND A.IsPass=0
  --AND A.AllowCompletedCourseRetake= 1)
  
 -- Section 3: Ignore courses the student currently scheduled in
    INSERT  INTO @ListCoursesStudentIsCurrentlyRegisteredIn
            SELECT DISTINCT
                    Courses.ReqId
                   ,Courses.Code
                   ,Courses.Descrip
                   ,Term.TermDescrip
            FROM    arResults CourseResults
            INNER JOIN arClassSections Class ON CourseResults.TestId = Class.ClsSectionId
            INNER JOIN arTerm Term ON Class.TermId = Term.TermId
            INNER JOIN syStatuses Statuses ON Term.StatusId = Statuses.StatusId
            INNER JOIN arReqs Courses ON Class.ReqId = Courses.ReqId
            WHERE   CourseResults.StuEnrollId = @stuenrollid
                    AND CourseResults.GrdSysDetailId IS NULL; 
	--P.ReqId=Class.ReqId -- don't need to look at first table variable
  

--/**   
--Select * from @ListCoursesOfferedForTermAndProgramStudentIsEnrolledIn
--Select * from @ListCoursesStudentHasAlreadyAttempted
--Select * from @ListCoursesStudentIsCurrentlyRegisteredIn
--**/

---- Final Resultset

    SELECT DISTINCT
            *
    FROM    @ListCoursesOfferedForTermAndProgramStudentIsEnrolledIn
    WHERE   ReqId NOT IN ( SELECT   CourseId
                           FROM     @ListCoursesStudentHasAlreadyAttempted ) --Ignore attempted courses
            AND ReqId NOT IN ( SELECT   CourseId
                               FROM     @ListCoursesStudentIsCurrentlyRegisteredIn ) -- Ignore Scheduled courses
			--AND StartDate <= ClassStartDate -- Ignore courses that begin before the begin of enrollment
            AND ClassEndDate >= GETDATE() -- Take only courses that had not ended
 
	--and Code='BUS101'
ORDER BY    CampDescrip
           ,Code
           ,Req
           ,PrgVerDescrip;
GO
