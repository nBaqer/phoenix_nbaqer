SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--=========================================================================================================
-- usp_UpdateStudentStatusChangeFromAuditHistoryEnrolledLeads 
-- to insert syStudentStatusChanges for records that are missing when enroll Leads run
-- update the inicial status (from lead status to student status 'Future Start')
--=========================================================================================================
CREATE PROCEDURE [dbo].[usp_UpdateStudentStatusChangeFromAuditHistoryEnrolledLeads]
AS
    BEGIN
		------------------------------------------------------------
		-- Table to store student status changes 
		-------------------------------------------------------------
        DECLARE @ProcessDate AS DATETIME;
        DECLARE @User AS NVARCHAR(50);
        SET @ProcessDate = GETDATE();
		
        DECLARE @syStudentStatusChangesFromAudit AS TABLE
            (
             StuEnrollId UNIQUEIDENTIFIER NOT NULL
            ,OrigStatusCodeId UNIQUEIDENTIFIER NULL
            ,NewStatusCodeId UNIQUEIDENTIFIER NULL
            ,CampusId UNIQUEIDENTIFIER NOT NULL
            ,ModDate DATETIME NULL
            ,ModUser VARCHAR(50) NULL
            ,IsReversal BIT NOT NULL
            ,DropReasonId UNIQUEIDENTIFIER NULL
            ,DateOfChange DATETIME NULL
            ,Lda DATETIME2 NULL PRIMARY KEY ( StuEnrollId ASC )
            ); 
		-----------------------------------------------------------------------------------------------------
		-- Get Missing info from syAuditHist and syAuditHistDetail
		-----------------------------------------------------------------------------------------------------
        INSERT  INTO @syStudentStatusChangesFromAudit
                SELECT  Q1.StuEnrollId
                       ,Q1.OrigStatusCodeId
                       ,Q1.NewStatusCodeId
                       ,Q1.CampusID
                       ,Q1.ModDate
                       ,Q1.ModUser
                       ,Q1.IsReversal
                       ,Q1.DropReasonId
                       ,Q1.DateOfChange
                       ,Q1.Lda
                FROM    (
                          SELECT    ASE.StuEnrollId AS StuEnrollId
                                   ,SLSC.OrigStatusId AS OrigStatusCodeId
                                   ,(
                                      SELECT    SSC.StatusCodeId
                                      FROM      dbo.syStatusCodes AS SSC
                                      INNER JOIN dbo.sySysStatus AS SSS ON SSS.SysStatusId = SSC.SysStatusId
                                      WHERE     SSS.SysStatusId = 7   -- AND SSS.SysStatusDescrip = 'Future Start'
                                    ) AS NewStatusCodeId
                                   ,ASE.CampusId AS CampusID
                                   ,SAH.EventDate AS ModDate
                                   ,SAH.UserName AS ModUser
                                   ,0 AS IsReversal
                                   ,NULL AS DropReasonId
                                   ,ASE.EnrollDate AS DateOfChange
                                   ,NULL AS Lda
                                   ,RANK() OVER ( PARTITION BY ASE.StuEnrollId ORDER BY ASE.EnrollDate ASC, SAH.EventDate ASC ) AS N
                          FROM      syAuditHist AS SAH
                          INNER JOIN syAuditHistDetail AS SAHD ON SAHD.AuditHistId = SAH.AuditHistId
                          LEFT JOIN syStatusCodes AS SSC ON ISNULL(CONVERT(NVARCHAR(40),SSC.StatusCodeId),'') = ISNULL(SAHD.NewValue,'')
                          INNER JOIN sySysStatus AS SSS ON SSS.SysStatusId = SSC.SysStatusId
                          INNER JOIN syLeadStatusesChanges AS SLSC ON SLSC.StatusChangeId = SAHD.RowId
                          INNER JOIN adLeads AS AL ON AL.LeadId = SLSC.LeadId
                          INNER JOIN arStuEnrollments AS ASE ON ASE.LeadId = AL.LeadId
                          WHERE     SAH.TableName = 'syLeadStatusesChanges'
						--AND SAH.Event = 'I'
                                    AND SAHD.ColumnName = 'NewStatusID'
                                    AND SSS.SysStatusId = 6 -- AND SSS.StatusCode = 'Enrolled'
                        ) AS Q1
                WHERE   Q1.N = 1
                ORDER BY Q1.DateOfChange ASC
                       ,Q1.ModDate ASC;

		-----------------------------------------------------------------------------------------------------
		-- Insert new records in  SSSC  syStudentStatusChanges AS SSSC  from @syStudentStatusChangesFromAudit
		-----------------------------------------------------------------------------------------------------
        DECLARE @CurStuEnrollId UNIQUEIDENTIFIER; 
        DECLARE @CurOrigStatusCodeId UNIQUEIDENTIFIER; 
        DECLARE @CurNewStatusCodeId UNIQUEIDENTIFIER; 
        DECLARE @CurCampusId UNIQUEIDENTIFIER; 
        DECLARE @CurModDate DATETIME; 
        DECLARE @CurModUser NVARCHAR(50); 
        DECLARE @CurIsReversal BIT; 
        DECLARE @CurDropReasonId UNIQUEIDENTIFIER;
        DECLARE @CurDateOfChange DATETIME; 
        DECLARE @CurLda DATETIME2;

        DECLARE @existStudentStatusChangeId AS UNIQUEIDENTIFIER;
        DECLARE @controlTotalUpdated AS INT;
        DECLARE @controlTotalInserted AS INT;

        SET @controlTotalUpdated = 0;
        SET @controlTotalInserted = 0;

        DECLARE ToInsertCursor CURSOR
        FOR
            SELECT  D1.StuEnrollId
                   ,D1.OrigStatusCodeId
                   ,D1.NewStatusCodeId
                   ,D1.CampusId
                   ,D1.ModDate
                   ,D1.ModUser
                   ,D1.IsReversal
                   ,D1.DropReasonId
                   ,D1.DateOfChange
                   ,D1.Lda
				  --, SSSC1.*
            FROM    @syStudentStatusChangesFromAudit AS D1
            LEFT JOIN (
                        SELECT  MIN(ISNULL(SSSC.DateOfChange,'')) AS MinDate
                               ,SSSC.StuEnrollId
                        FROM    syStudentStatusChanges AS SSSC
                        GROUP BY SSSC.StuEnrollId
                      ) AS SSSC1 ON SSSC1.StuEnrollId = D1.StuEnrollId
            WHERE   D1.DateOfChange <= ISNULL(SSSC1.MinDate,GETDATE())
            ORDER BY D1.StuEnrollId;

        OPEN ToInsertCursor; 
        FETCH NEXT FROM ToInsertCursor  
			INTO @CurStuEnrollId,@CurOrigStatusCodeId,@CurNewStatusCodeId,@CurCampusId,@CurModDate,@CurModUser,@CurIsReversal,@CurDropReasonId,@CurDateOfChange,
            @CurLda; 
	
        WHILE @@FETCH_STATUS = 0
            BEGIN
                SET @existStudentStatusChangeId = NULL;
				-- to select only one record even repeated
                SELECT  @existStudentStatusChangeId = SSSC.StudentStatusChangeId
                FROM    syStudentStatusChanges AS SSSC
                INNER JOIN (
                             SELECT SSSC1.StuEnrollId
                                   ,MIN(SSSC1.DateOfChange) AS MINDateOfChange
                                   ,MIN(SSSC1.ModDate) AS MINModDate
                             FROM   syStudentStatusChanges AS SSSC1
                             GROUP BY SSSC1.StuEnrollId
                           ) AS MD ON MD.StuEnrollId = SSSC.StuEnrollId
                LEFT JOIN dbo.syStatusCodes AS SSCorg ON SSCorg.StatusCodeId = SSSC.OrigStatusId
                LEFT JOIN dbo.syStatusCodes AS SSCnew ON SSCnew.StatusCodeId = SSSC.NewStatusId
                WHERE   MD.MINDateOfChange = SSSC.DateOfChange
                        AND MD.MINModDate = SSSC.ModDate
                        AND SSCorg.SysStatusId IS NULL
                        AND SSCnew.SysStatusId = 7
                        AND SSSC.StuEnrollId = @CurStuEnrollId; 

                IF ( @existStudentStatusChangeId IS NOT NULL )
                    BEGIN    
						--Update the first record of syStudentStatusChange
                        UPDATE  SSSC
                        SET     SSSC.OrigStatusId = @CurOrigStatusCodeId
                               ,SSSC.ModUser = @CurModUser
                        FROM    syStudentStatusChanges AS SSSC
                        WHERE   SSSC.StudentStatusChangeId = @existStudentStatusChangeId;
						
                        SET @controlTotalUpdated = @controlTotalUpdated + 1;
                    END;
                ELSE
                    BEGIN
						--Insert the first record of syStudentStatusChange
                        INSERT  INTO dbo.syStudentStatusChanges
                                (
                                 StudentStatusChangeId
                                ,StuEnrollId
                                ,OrigStatusId
                                ,NewStatusId
                                ,CampusId
                                ,ModDate
                                ,ModUser
                                ,IsReversal
                                ,DropReasonId
                                ,DateOfChange
                                ,Lda
								)
                        VALUES  (
                                 NEWID()
                                ,@CurStuEnrollId
                                ,@CurOrigStatusCodeId
                                ,@CurNewStatusCodeId
                                ,@CurCampusId
                                ,@CurModDate
                                ,@CurModUser
                                ,@CurIsReversal
                                ,@CurDropReasonId
                                ,@CurDateOfChange
                                ,@CurLda
								);

                        SET @controlTotalInserted = @controlTotalInserted + 1;
                    END;
                FETCH NEXT FROM ToInsertCursor 
					INTO @CurStuEnrollId,@CurOrigStatusCodeId,@CurNewStatusCodeId,@CurCampusId,@CurModDate,@CurModUser,@CurIsReversal,@CurDropReasonId,
                    @CurDateOfChange,@CurLda; 
            END;
		
        CLOSE ToInsertCursor;
        DEALLOCATE ToInsertCursor;	

    END;

GO
