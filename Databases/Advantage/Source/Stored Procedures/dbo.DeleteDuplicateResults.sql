SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[DeleteDuplicateResults]
AS
    DECLARE @counter AS INT;
    DECLARE @StuEnrollId UNIQUEIDENTIFIER;
    DECLARE @ClsSectionId UNIQUEIDENTIFIER;
    DECLARE @ResultId UNIQUEIDENTIFIER;
    DECLARE @times INT;
    DECLARE getUsers_Cursor CURSOR
    FOR
        SELECT  StuEnrollId
               ,TestId
               ,COUNT(*)
        FROM    arResults
        GROUP BY StuEnrollId
               ,TestId
        HAVING  COUNT(*) > 1;
    OPEN getUsers_Cursor;
    FETCH NEXT FROM getUsers_Cursor
INTO @StuEnrollId,@ClsSectionId,@counter;

    WHILE @@FETCH_STATUS = 0
        BEGIN
            SET @times = @counter - 1;
            WHILE @times > 0
                BEGIN
                    SET @ResultId = ( SELECT TOP 1
                                                ResultId
                                      FROM      arResults
                                      WHERE     TestId = @ClsSectionId
                                                AND StuEnrollId = @StuEnrollId
                                    );
                    DELETE  FROM arResults
                    WHERE   ResultId = @ResultId;
                    SET @times = @times - 1;
                END;
            FETCH NEXT FROM getUsers_Cursor
INTO @StuEnrollId,@ClsSectionId,@counter;
        END;
    CLOSE getUsers_Cursor;
    DEALLOCATE getUsers_Cursor;
GO
