SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

----For testing purposes only
--declare @TermId uniqueidentifier	
--declare @User varchar(1000)
--declare @CampusID uniqueidentifier
--declare @PostFeesDate datetime
--declare @CohortStartDate DATETIME

----set @TermId='F3F89663-C5E9-413D-810F-D3144193AE88' 
--set @CampusID='3F5E839A-589A-4B2A-B258-35A1A8B3B819' 
--set @CohortStartDate = '8/15/2011' 

CREATE PROCEDURE [dbo].[USP_ApplyFeesByTermForCohort]
    (
     @CohortStartDate DATETIME
    ,@User VARCHAR(100)
    ,@CampusID UNIQUEIDENTIFIER
    ,@PostFeesDate DATETIME
    ,@Result INTEGER OUTPUT
    )
AS
    SET NOCOUNT ON;
    BEGIN 
    
        DECLARE @StuEnrollID UNIQUEIDENTIFIER;
        DECLARE @PeriodicfeeID AS UNIQUEIDENTIFIER;
        DECLARE @TransCodeID AS UNIQUEIDENTIFIER;
        DECLARE @TransCodeDescrip AS VARCHAR(50);
        DECLARE @StudentName AS VARCHAR(50);
        DECLARE @TermDescrip AS VARCHAR(50);
        DECLARE @TransDescrip AS VARCHAR(50);
        DECLARE @TransAmount DECIMAL(18,2);
        SET @TransAmount = 0;
        DECLARE @TermID AS UNIQUEIDENTIFIER;
    
        BEGIN TRANSACTION;
       
        DECLARE c1 CURSOR READ_ONLY
        FOR
            (
              SELECT    SE.StuEnrollId
                       ,PF.PeriodicFeeId
                       ,PF.TransCodeId
                       ,( (
                            SELECT  TransCodeDescrip
                            FROM    saTransCodes
                            WHERE   TransCodeId = PF.TransCodeId
                          ) + CASE PF.ApplyTo
                                WHEN 0 THEN '- All Programs'
                                WHEN 1 THEN '-' + PT.Description
                                WHEN 2 THEN '-' + PV.PrgVerDescrip
                                ELSE ''
                              END ) TransCodeDescrip
                       ,(
                          SELECT    LastName + ' ' + FirstName
                          FROM      arStudent
                          WHERE     StudentId = (
                                                  SELECT    StudentId
                                                  FROM      arStuEnrollments
                                                  WHERE     StuEnrollId = SE.StuEnrollId
                                                )
                        ) StudentName
                       ,(
                          SELECT    TermDescrip
                          FROM      arTerm
                          WHERE     TermId = PF.TermId
                        ) TransDescrip
                       ,T.TermDescrip
                       ,( CASE LEN(PF.RateScheduleId)
                            WHEN 36
                            THEN (
                                   SELECT   CASE RSD.FlatAmount
                                              WHEN 0.00 THEN CASE RSD.UnitId
                                                               WHEN 0 THEN Rate * (
                                                                                    SELECT  COALESCE(SUM(RQ.Credits),0)
                                                                                    FROM    arResults R
                                                                                           ,arClassSections CS
                                                                                           ,arClassSectionTerms CST
                                                                                           ,arReqs RQ
                                                                                    WHERE   R.TestId = CS.ClsSectionId
                                                                                            AND CS.ReqId = RQ.ReqId
                                                                                            AND R.StuEnrollId = SE.StuEnrollId
                                                                                            AND CST.ClsSectionId = CS.ClsSectionId
                                                                                            AND CST.TermId IN ( SELECT  arClassSections.TermId
                                                                                                                FROM    arClassSections
                                                                                                                       ,arTerm
                                                                                                                WHERE   arClassSections.TermId = arTerm.TermId
                                                                                                                        AND arTerm.StartDate <= GETDATE()
                                                                                                                        AND arTerm.EndDate >= GETDATE()
                                                                                                                        AND cohortstartdate = @CohortStartDate )
                                                                                  )
                                                               WHEN 1 THEN Rate * (
                                                                                    SELECT  COALESCE(SUM(RQ.Hours),0)
                                                                                    FROM    arResults R
                                                                                           ,arClassSections CS
                                                                                           ,arClassSectionTerms CST
                                                                                           ,arReqs RQ
                                                                                    WHERE   R.TestId = CS.ClsSectionId
                                                                                            AND CS.ReqId = RQ.ReqId
                                                                                            AND R.StuEnrollId = SE.StuEnrollId
                                                                                            AND CST.ClsSectionId = CS.ClsSectionId
                                                                                            AND CST.TermId IN ( SELECT  arClassSections.TermId
                                                                                                                FROM    arClassSections
                                                                                                                       ,arTerm
                                                                                                                WHERE   arClassSections.TermId = arTerm.TermId
                                                                                                                        AND arTerm.StartDate <= GETDATE()
                                                                                                                        AND arTerm.EndDate >= GETDATE()
                                                                                                                        AND cohortstartdate = @CohortStartDate )
                                                                                  )
                                                             END
                                              ELSE FlatAmount
                                            END
                                   FROM     saRateSchedules RS
                                           ,saRateScheduleDetails RSD
                                   WHERE    RS.RateScheduleId = RSD.RateScheduleId
                                            AND RS.RateScheduleId = PF.RateScheduleId
                                            AND (
                                                  RSD.TuitionCategoryId = SE.TuitionCategoryId
                                                  OR (
                                                       RSD.TuitionCategoryId IS NULL
                                                       AND SE.TuitionCategoryId IS NULL
                                                     )
                                                )
                                            AND (
                                                  (
                                                    RSD.FlatAmount = 0.00
                                                    AND RSD.UnitId = 0
						 --AND RSD.MaxUnits=(SELECT MIN(MaxUnits)
							--			   FROM saRateScheduleDetails
							--			   WHERE MaxUnits>=(SELECT COALESCE(SUM(RQ.Credits),0)
							--							    FROM arResults R,arClassSections CS, arClassSectionTerms CST,  arReqs RQ
							--							    WHERE R.TestId = CS.ClsSectionId
							--							    AND CS.ReqId=RQ.ReqId 
							--							    AND R.StuEnrollId=SE.StuEnrollId
							--							    AND CST.ClsSectionId=CS.ClsSectionId
							--							    AND CST.TermId  IN (
							--													SELECT arClassSections.TermId
							--													FROM arClassSections,arTerm
							--													WHERE arClassSections.TermId =arTerm.TermId 
							--													AND arTerm.StartDate <= GETDATE()
							--													AND arTerm.EndDate>= GETDATE()
							--													AND cohortstartdate = @CohortStartDate
							--												   ) 
							--								) 
										
							--			 AND RateScheduleId=RSD.RateScheduleId 
							--			 AND (
							--					RSD.TuitionCategoryId=SE.TuitionCategoryId
							--									 OR 
							--					(RSD.TuitionCategoryId IS NULL AND SE.TuitionCategoryId IS NULL)
							--				)
							--			)
                                                    AND RSD.MinUnits <= (
                                                                          SELECT    COALESCE(SUM(RQ.Credits),0)
                                                                          FROM      arResults R
                                                                                   ,arClassSections CS
                                                                                   ,arClassSectionTerms CST
                                                                                   ,arReqs RQ
                                                                          WHERE     R.TestId = CS.ClsSectionId
                                                                                    AND CS.ReqId = RQ.ReqId
                                                                                    AND R.StuEnrollId = SE.StuEnrollId
                                                                                    AND CST.ClsSectionId = CS.ClsSectionId
                                                                                    AND CST.TermId IN ( SELECT  arClassSections.TermId
                                                                                                        FROM    arClassSections
                                                                                                               ,arTerm
                                                                                                        WHERE   arClassSections.TermId = arTerm.TermId
                                                                                                                AND arTerm.StartDate <= GETDATE()
                                                                                                                AND arTerm.EndDate >= GETDATE()
                                                                                                                AND cohortstartdate = @CohortStartDate )
                                                                        )
                                                    AND RSD.MaxUnits >= (
                                                                          SELECT    COALESCE(SUM(RQ.Credits),0)
                                                                          FROM      arResults R
                                                                                   ,arClassSections CS
                                                                                   ,arClassSectionTerms CST
                                                                                   ,arReqs RQ
                                                                          WHERE     R.TestId = CS.ClsSectionId
                                                                                    AND CS.ReqId = RQ.ReqId
                                                                                    AND R.StuEnrollId = SE.StuEnrollId
                                                                                    AND CST.ClsSectionId = CS.ClsSectionId
                                                                                    AND CST.TermId IN ( SELECT  arClassSections.TermId
                                                                                                        FROM    arClassSections
                                                                                                               ,arTerm
                                                                                                        WHERE   arClassSections.TermId = arTerm.TermId
                                                                                                                AND arTerm.StartDate <= GETDATE()
                                                                                                                AND arTerm.EndDate >= GETDATE()
                                                                                                                AND cohortstartdate = @CohortStartDate )
                                                                        )
                                                  )
                                                  OR (
                                                       RSD.FlatAmount = 0.00
                                                       AND RSD.UnitId = 1 
						 --AND RSD.MaxUnits=(	 SELECT MIN(MaxUnits)
							--				 FROM saRateScheduleDetails
							--				 WHERE MaxUnits>=(	SELECT COALESCE(SUM(RQ.Hours),0) 
							--							        FROM arResults R, arClassSections CS, arClassSectionTerms CST,  arReqs RQ
							--							        WHERE R.TestId = CS.ClsSectionId
							--							        AND CS.ReqId=RQ.ReqId
							--							        AND R.StuEnrollId=SE.StuEnrollId
							--							        AND CST.ClsSectionId=CS.ClsSectionId
							--							        AND CST.TermId IN (	SELECT arClassSections.TermId
							--														FROM arClassSections,arTerm
							--														WHERE arClassSections.TermId =arTerm.TermId 
							--														AND arTerm.StartDate <= GETDATE()
							--														AND arTerm.EndDate>=GETDATE()
							--														AND cohortstartdate = @CohortStartDate
							--														)
							--								) 
							--				AND RateScheduleId=RSD.RateScheduleId
							--				AND (	RSD.TuitionCategoryId=SE.TuitionCategoryId
							--								 OR 
							--						(RSD.TuitionCategoryId IS NULL AND SE.TuitionCategoryId IS NULL)
							--					)
							--			)
                                                       AND RSD.MinUnits <= (
                                                                             SELECT COALESCE(SUM(RQ.Hours),0)
                                                                             FROM   arResults R
                                                                                   ,arClassSections CS
                                                                                   ,arClassSectionTerms CST
                                                                                   ,arReqs RQ
                                                                             WHERE  R.TestId = CS.ClsSectionId
                                                                                    AND CS.ReqId = RQ.ReqId
                                                                                    AND R.StuEnrollId = SE.StuEnrollId
                                                                                    AND CST.ClsSectionId = CS.ClsSectionId
                                                                                    AND CST.TermId IN ( SELECT  arClassSections.TermId
                                                                                                        FROM    arClassSections
                                                                                                               ,arTerm
                                                                                                        WHERE   arClassSections.TermId = arTerm.TermId
                                                                                                                AND arTerm.StartDate <= GETDATE()
                                                                                                                AND arTerm.EndDate >= GETDATE()
                                                                                                                AND cohortstartdate = @CohortStartDate )
                                                                           )
                                                       AND RSD.MaxUnits >= (
                                                                             SELECT COALESCE(SUM(RQ.Hours),0)
                                                                             FROM   arResults R
                                                                                   ,arClassSections CS
                                                                                   ,arClassSectionTerms CST
                                                                                   ,arReqs RQ
                                                                             WHERE  R.TestId = CS.ClsSectionId
                                                                                    AND CS.ReqId = RQ.ReqId
                                                                                    AND R.StuEnrollId = SE.StuEnrollId
                                                                                    AND CST.ClsSectionId = CS.ClsSectionId
                                                                                    AND CST.TermId IN ( SELECT  arClassSections.TermId
                                                                                                        FROM    arClassSections
                                                                                                               ,arTerm
                                                                                                        WHERE   arClassSections.TermId = arTerm.TermId
                                                                                                                AND arTerm.StartDate <= GETDATE()
                                                                                                                AND arTerm.EndDate >= GETDATE()
                                                                                                                AND cohortstartdate = @CohortStartDate )
                                                                           )
                                                     )
                                                  OR (
                                                       RSD.FlatAmount > 0.00
					   --AND RSD.MaxUnits=(	SELECT MIN(MaxUnits) 
								--			FROM saRateScheduleDetails
								--			WHERE MaxUnits>=(	SELECT COALESCE(SUM(RQ.Credits),0)
								--								FROM arResults R, arClassSections CS, arClassSectionTerms CST,  arReqs RQ
								--								WHERE R.TestId = CS.ClsSectionId
								--								AND CS.ReqId=RQ.ReqId
								--								AND R.StuEnrollId=SE.StuEnrollId
								--								AND CST.ClsSectionId=CS.ClsSectionId
								--								AND CST.TermId IN (	SELECT arClassSections.TermId
								--													FROM arClassSections,arTerm
								--													WHERE arClassSections.TermId =arTerm.TermId 
								--													AND arTerm.StartDate <= GETDATE() 
								--													AND arTerm.EndDate>= GETDATE()
								--													AND cohortstartdate = @CohortStartDate
								--												  )
								--							)
								--		 AND RateScheduleId=RSD.RateScheduleId
								--		 AND (	RSD.TuitionCategoryId=SE.TuitionCategoryId 
								--							OR 
								--				(RSD.TuitionCategoryId IS NULL AND SE.TuitionCategoryId IS NULL)
								--			  )
								--	)
                                                       AND RSD.MinUnits <= (
                                                                             SELECT COALESCE(SUM(RQ.Credits),0)
                                                                             FROM   arResults R
                                                                                   ,arClassSections CS
                                                                                   ,arClassSectionTerms CST
                                                                                   ,arReqs RQ
                                                                             WHERE  R.TestId = CS.ClsSectionId
                                                                                    AND CS.ReqId = RQ.ReqId
                                                                                    AND R.StuEnrollId = SE.StuEnrollId
                                                                                    AND CST.ClsSectionId = CS.ClsSectionId
                                                                                    AND CST.TermId IN ( SELECT  arClassSections.TermId
                                                                                                        FROM    arClassSections
                                                                                                               ,arTerm
                                                                                                        WHERE   arClassSections.TermId = arTerm.TermId
                                                                                                                AND arTerm.StartDate <= GETDATE()
                                                                                                                AND arTerm.EndDate >= GETDATE()
                                                                                                                AND cohortstartdate = @CohortStartDate )
                                                                           )
                                                       AND RSD.MaxUnits >= (
                                                                             SELECT COALESCE(SUM(RQ.Credits),0)
                                                                             FROM   arResults R
                                                                                   ,arClassSections CS
                                                                                   ,arClassSectionTerms CST
                                                                                   ,arReqs RQ
                                                                             WHERE  R.TestId = CS.ClsSectionId
                                                                                    AND CS.ReqId = RQ.ReqId
                                                                                    AND R.StuEnrollId = SE.StuEnrollId
                                                                                    AND CST.ClsSectionId = CS.ClsSectionId
                                                                                    AND CST.TermId IN ( SELECT  arClassSections.TermId
                                                                                                        FROM    arClassSections
                                                                                                               ,arTerm
                                                                                                        WHERE   arClassSections.TermId = arTerm.TermId
                                                                                                                AND arTerm.StartDate <= GETDATE()
                                                                                                                AND arTerm.EndDate >= GETDATE()
                                                                                                                AND cohortstartdate = @CohortStartDate )
                                                                           )
                                                     )
                                                )
                                 )
                            ELSE ( CASE PF.UnitId
                                     WHEN 0 THEN PF.Amount * (
                                                               SELECT   SUM(RQ.Credits)
                                                               FROM     arResults R
                                                                       ,arClassSections CS
                                                                       ,arClassSectionTerms CST
                                                                       ,arReqs RQ
                                                               WHERE    R.TestId = CS.ClsSectionId
                                                                        AND CS.ReqId = RQ.ReqId
                                                                        AND R.StuEnrollId = SE.StuEnrollId
                                                                        AND CST.ClsSectionId = CS.ClsSectionId
                                                                        AND CST.TermId IN ( SELECT  arClassSections.TermId
                                                                                            FROM    arClassSections
                                                                                                   ,arTerm
                                                                                            WHERE   arClassSections.TermId = arTerm.TermId
                                                                                                    AND arTerm.StartDate <= GETDATE()
                                                                                                    AND arTerm.EndDate >= GETDATE()
                                                                                                    AND cohortstartdate = @CohortStartDate )
                                                                        AND (
                                                                              (
                                                                                SE.TuitionCategoryId IS NULL
                                                                                AND PF.TuitionCategoryId IS NULL
                                                                              )
                                                                              OR ( SE.TuitionCategoryId = PF.TuitionCategoryId )
                                                                            )
                                                             )
                                     WHEN 1 THEN PF.Amount * (
                                                               SELECT   SUM(RQ.Hours)
                                                               FROM     arResults R
                                                                       ,arClassSections CS
                                                                       ,arClassSectionTerms CST
                                                                       ,arReqs RQ
                                                               WHERE    R.TestId = CS.ClsSectionId
                                                                        AND CS.ReqId = RQ.ReqId
                                                                        AND R.StuEnrollId = SE.StuEnrollId
                                                                        AND CST.ClsSectionId = CS.ClsSectionId
                                                                        AND CST.TermId IN ( SELECT  arClassSections.TermId
                                                                                            FROM    arClassSections
                                                                                                   ,arTerm
                                                                                            WHERE   arClassSections.TermId = arTerm.TermId
                                                                                                    AND arTerm.StartDate <= GETDATE()
                                                                                                    AND arTerm.EndDate >= GETDATE()
                                                                                                    AND cohortstartdate = @CohortStartDate )
                                                                        AND (
                                                                              (
                                                                                SE.TuitionCategoryId IS NULL
                                                                                AND PF.TuitionCategoryId IS NULL
                                                                              )
                                                                              OR ( SE.TuitionCategoryId = PF.TuitionCategoryId )
                                                                            )
                                                             )
                                     WHEN 2
                                     THEN PF.Amount
                                          * (
                                              SELECT    ( CASE WHEN EXISTS ( SELECT *
                                                                             FROM   arResults
                                                                             WHERE  StuEnrollId = SE.StuEnrollId
                                                                                    AND TestId IN (
                                                                                    SELECT  ClsSectionId
                                                                                    FROM    arClassSections
                                                                                    WHERE   TermId IN ( SELECT  arClassSections.TermId
                                                                                                        FROM    arClassSections
                                                                                                               ,arTerm
                                                                                                        WHERE   arClassSections.TermId = arTerm.TermId
                                                                                                                AND arTerm.StartDate <= GETDATE()
                                                                                                                AND arTerm.EndDate >= GETDATE()
                                                                                                                AND cohortstartdate = @CohortStartDate )
                                                                                            AND (
                                                                                                  (
                                                                                                    SE.TuitionCategoryId IS NULL
                                                                                                    AND PF.TuitionCategoryId IS NULL
                                                                                                  )
                                                                                                  OR ( SE.TuitionCategoryId = PF.TuitionCategoryId )
                                                                                                ) ) ) THEN 1
                                                               ELSE 0
                                                          END )
                                            )
                                   END )
                          END ) TransAmount
                       ,T.TermId
              FROM      saPeriodicFees PF
                       ,arTerm T
                       ,arStuEnrollments SE
                       ,syStatusCodes SC
                       ,arPrgVersions PV
                       ,arProgTypes PT
              WHERE     PF.TermId = T.TermId
                        AND PF.TermId IN ( SELECT   arClassSections.TermId
                                           FROM     arClassSections
                                                   ,arTerm
                                           WHERE    arClassSections.TermId = arTerm.TermId
                                                    AND arTerm.StartDate <= GETDATE()
                                                    AND arTerm.EndDate >= GETDATE()
                                                    AND cohortstartdate = @CohortStartDate )
                        AND SE.CampusId = @CampusID
                        AND SE.StatusCodeId = SC.StatusCodeId
                        AND SC.SysStatusId IN ( 7,9,13,20 )
                        AND SE.PrgVerId = PV.PrgVerId
                        AND PV.ProgTypId = PT.ProgTypId
                        AND (
                              ( PF.ApplyTo = 0 )
                              OR (
                                   PF.ApplyTo = 1
                                   AND PV.ProgTypId = PF.ProgTypId
                                 )
                              OR (
                                   PF.ApplyTo = 2
                                   AND PV.PrgVerId = PF.PrgVerId
                                 )
                            )
                        AND (
                              ( PF.TermStartDate IS NULL )
                              OR ( PF.TermStartDate = SE.CohortStartDate )
                            )
                        AND SE.CohortStartDate = @CohortStartDate
                        AND EXISTS ( SELECT R.*
                                     FROM   arResults R
                                           ,arClassSections CS
                                           ,arClassSectionTerms CST
                                     WHERE  R.TestId = CS.ClsSectionId
                                            AND R.StuEnrollId = SE.StuEnrollId
                                            AND CST.ClsSectionId = CS.ClsSectionId
                                            AND CST.TermId = PF.TermId
                                            AND CS.CampusId = @CampusID )
            ); 
  
        OPEN c1; 
  
        FETCH NEXT FROM c1 
        INTO @StuEnrollID,@PeriodicfeeID,@TransCodeID,@TransCodeDescrip,@StudentName,@TransDescrip,@TermDescrip,@TransAmount,@TermID;
  
        WHILE @@FETCH_STATUS = 0
            BEGIN 
  
                IF ISNULL(@TransAmount,0) <> 0
                    BEGIN
                        INSERT  INTO dbo.saTransactions
                                (
                                 TransactionId
                                ,StuEnrollId
                                ,TermId
                                ,CampusId
                                ,TransDate
                                ,TransCodeId
                                ,TransReference
                                ,AcademicYearId
                                ,TransDescrip
                                ,TransAmount
                                ,TransTypeId
                                ,IsPosted
                                ,CreateDate
                                ,BatchPaymentId
                                ,ViewOrder
                                ,IsAutomatic
                                ,ModUser
                                ,ModDate
                                ,Voided
                                ,FeeLevelId
                                ,FeeId
                                ,PaymentCodeId
                                ,FundSourceId 
                                )
                        VALUES  (
                                 NEWID()
                                , -- TransactionId - uniqueidentifier
                                 @StuEnrollID
                                , -- StuEnrollId - uniqueidentifier
                                 @TermID
                                , -- TermId - uniqueidentifier
                                 @CampusID
                                , -- CampusId - uniqueidentifier
                                 @PostFeesDate
                                , -- TransDate - datetime
                                 @TransCodeID
                                , -- TransCodeId - uniqueidentifier
                                 @TransDescrip
                                , -- TransReference - varchar(50)
                                 NULL
                                , -- AcademicYearId - uniqueidentifier
                                 @TransCodeDescrip
                                , -- TransDescrip - varchar(50)
                                 ISNULL(@TransAmount,0)
                                , -- TransAmount - decimal
                                 0
                                , -- TransTypeId - int
                                 1
                                , -- IsPosted - bit
                                 GETDATE()
                                , -- CreateDate - datetime
                                 NULL
                                , -- BatchPaymentId - uniqueidentifier
                                 0
                                , -- ViewOrder - int
                                 1
                                , -- IsAutomatic - bit
                                 @User
                                , -- ModUser - varchar(50)
                                 GETDATE()
                                ,-- ModDate - datetime
                                 0
                                , -- Voided - bit
                                 1
                                , -- FeeLevelId - tinyint
                                 @PeriodicfeeID
                                , -- FeeId - uniqueidentifier
                                 NULL
                                , -- PaymentCodeId - uniqueidentifier
                                 NULL -- FundSourceId - uniqueidentifier
                          
                                );
  
                        IF @@ERROR <> 0
                            BEGIN
								--Rollback the transaction
                                ROLLBACK; 
                                SET @Result = 0;
                                RETURN 0;--"Error while inserting"
                            END;
							
                    END;
  


                FETCH NEXT FROM c1 
				INTO @StuEnrollID,@PeriodicfeeID,@TransCodeID,@TransCodeDescrip,@StudentName,@TransDescrip,@TermDescrip,@TransAmount,@TermID;
  
            END; 
        CLOSE c1; 
        DEALLOCATE c1; 
 
        IF @@ERROR = 0
            BEGIN
                COMMIT TRANSACTION;
                SET @Result = 1;
                RETURN 1;
            END;
        ELSE
            BEGIN
					 --Rollback the transaction
                ROLLBACK TRANSACTION;	
                SET @Result = 0;
                RETURN 0;--"Error while inserting"	
            END;
	  
    END;




GO
