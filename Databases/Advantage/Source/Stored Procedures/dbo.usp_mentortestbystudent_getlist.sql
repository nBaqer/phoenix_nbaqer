SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_mentortestbystudent_getlist]
    @StuEnrollId UNIQUEIDENTIFIER
   ,@ReqId UNIQUEIDENTIFIER
   ,@EffectiveDate DATETIME
AS
    SELECT DISTINCT
            t3.GrdComponentTypeId
           ,t1.Speed
           ,t1.Accuracy
           ,t5.Speed AS MentorSpeed
    FROM    arPostScoreDictationSpeedTest t1
           ,arGrdComponentTypes t2
           ,arBridge_GradeComponentTypes_Courses t3
           ,arMentor_GradeComponentTypes_Courses t5
    WHERE   t1.GrdComponentTypeId = t2.GrdComponentTypeId
            AND t2.GrdComponentTypeId = t3.GrdComponentTypeId
            AND t1.GrdComponentTypeId = t5.GrdComponentTypeId
            AND t1.StuEnrollId = @StuEnrollId
            AND t5.ReqId = @ReqId
            AND t5.EffectiveDate = @EffectiveDate
            AND t1.istestmentorproctored = 'yes'; 



GO
