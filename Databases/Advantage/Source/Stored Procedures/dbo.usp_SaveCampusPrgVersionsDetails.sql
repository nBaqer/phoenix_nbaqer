SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Dheeraj Kumar>
-- Create date: <07-04-2018>
-- Description:	<This stored procedure saves or updates the table arCampusPrgVersions details for each campus.>
-- =============================================
CREATE PROCEDURE [dbo].[usp_SaveCampusPrgVersionsDetails]
    -- Add the parameters for the stored procedure here
    @PrgVerId UNIQUEIDENTIFIER
   ,@CampusId UNIQUEIDENTIFIER
   ,@IsTitleIV BIT
   ,@IsFAMEApproved BIT
   ,@IsSelfPaced BIT
   ,@CalculationPeriodTypeId UNIQUEIDENTIFIER
   ,@AllowExcusAbsPerPayPrd FLOAT
   ,@TermSubEqualInLen BIT
   ,@ModUser VARCHAR(50)
   ,@ModDate DATETIME
AS
    BEGIN
        -- SET NOCOUNT ON added to prevent extra result sets from
        -- interfering with SELECT statements.
        SET NOCOUNT ON;

        INSERT INTO [dbo].[arCampusPrgVersions] (
                                                [CampusPrgVerId]
                                               ,[CampusId]
                                               ,[PrgVerId]
                                               ,[IsTitleIV]
                                               ,[IsFAMEApproved]
                                               ,[IsSelfPaced]
                                               ,[CalculationPeriodTypeId]
                                               ,[AllowExcusAbsPerPayPrd]
                                               ,[TermSubEqualInLen]
                                               ,[ModUser]
                                               ,[ModDate]
                                                )
        VALUES ( NEWID(), @CampusId, @PrgVerId, @IsTitleIV, @IsFAMEApproved, @IsSelfPaced, @CalculationPeriodTypeId, @AllowExcusAbsPerPayPrd
                ,@TermSubEqualInLen, @ModUser, @ModDate );
    END;
GO
