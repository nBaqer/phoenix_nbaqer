SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/****** Object:  StoredProcedure [dbo].[USP_Programs_Update]    Script Date: 1/7/2016 3:34:15 PM ******/
CREATE PROCEDURE [dbo].[USP_Programs_Update]
    @CIPCODE VARCHAR(6)
   ,@CredentialLevel VARCHAR(2)
   ,@IsGEProgram BIT
   ,@ProgId VARCHAR(50)
   ,@Is1098T BIT
AS -- UPDATE arPrograms SET CIPCode=@CIPCODE,CredentialLevel=@CredentialLevel,IsGEProgram=@IsGEProgram
    UPDATE  arPrograms
    SET     CIPCode = @CIPCODE
           ,IsGEProgram = @IsGEProgram
           ,Is1098T = @Is1098T
    WHERE   ProgId = @ProgId;

GO
