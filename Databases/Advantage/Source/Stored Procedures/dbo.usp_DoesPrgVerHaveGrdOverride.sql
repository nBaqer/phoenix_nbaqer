SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_DoesPrgVerHaveGrdOverride]
    (
     @reqId UNIQUEIDENTIFIER
    ,@prgVerId UNIQUEIDENTIFIER
    ,@grade VARCHAR(100)
    )
AS
    SET NOCOUNT ON;
    DECLARE @cnt INT
       ,@GrdSysDetailId UNIQUEIDENTIFIER;
    SET @GrdSysDetailId = (
                            SELECT TOP 1
                                    GrdSysDetailId
                            FROM    arProgVerDef
                            WHERE   ReqId = @reqId
                                    AND PrgVerId = @prgVerId
                                    AND GrdSysDetailId IS NOT NULL
                            GROUP BY GrdSysDetailId
                          );
    IF @GrdSysDetailId IS NULL
        BEGIN
            EXEC dbo.usp_PassingGradeFromPrgVersion @prgVerId,@grade;
        END;
    ELSE
        BEGIN
            EXEC dbo.usp_PassingGradeFromGrdSystem @GrdSysDetailId,@grade;
        END;



GO
