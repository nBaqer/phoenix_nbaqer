SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_Courses_GetList]
    @CampusId UNIQUEIDENTIFIER
   ,@StatusId UNIQUEIDENTIFIER
AS
    SELECT DISTINCT
            t1.ReqId
           ,t1.Descrip
           ,t1.Code
    FROM    arReqs t1
    WHERE   t1.ReqTypeId = 1
            AND t1.CampGrpId IN ( SELECT    CampGrpId
                                  FROM      syCmpGrpCmps
                                  WHERE     CampusId = @CampusId )
            AND t1.StatusId = @StatusId
    ORDER BY t1.Descrip;



GO
