SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_minimumpercombination_getlist]
    @reqId UNIQUEIDENTIFIER
   ,@effectivedate DATETIME
   ,@stuenrollId UNIQUEIDENTIFIER
AS
    SELECT DISTINCT
            t3.GrdComponentTypeId
           ,COUNT(t1.speed) AS MinimumPerCombination
    FROM    arPostScoreDictationSpeedTest t1
           ,arGrdComponentTypes t2
           ,arBridge_GradeComponentTypes_Courses t3
           ,arRules_GradeComponentTypes_Courses t4
    WHERE   t1.grdcomponenttypeid = t2.GrdComponentTypeId
            AND t2.GrdComponentTypeId = t3.GrdComponentTypeId
            AND t3.GrdComponentTypeId_ReqId = t4.GrdComponentTypeId_ReqId
            AND t3.ReqId = @reqId
            AND t1.stuenrollid = @stuenrollId
            AND t4.EffectiveDate = @effectivedate
    GROUP BY t3.GrdComponentTypeId;



GO
