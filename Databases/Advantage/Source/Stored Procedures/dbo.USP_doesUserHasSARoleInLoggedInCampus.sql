SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_doesUserHasSARoleInLoggedInCampus]
    @UserId UNIQUEIDENTIFIER
   ,@CampusId UNIQUEIDENTIFIER
AS
    SELECT  COUNT(t1.UserId) AS RowCounter
    FROM    syUsersRolesCampGrps t1
    INNER JOIN syRoles t2 ON t1.RoleId = t2.RoleId
                             AND t2.SysRoleId = 1
    INNER JOIN syCmpGrpCmps t3 ON t1.CampGrpId = t3.CampGrpId
    WHERE   t1.UserId = @UserId
            AND t3.CampusId = @CampusId;



GO
