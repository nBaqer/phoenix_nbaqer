SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*
select * from syCampGrps 
select * from arPrgVersions where PrgVerDescrip like 'Dental%'

	exec Usp_ProgressReport_Master_Query null,'4ECB3BBB-3543-41BA-8F8F-4F28228DC9B7',null,null,null,null,'numeric','weightedavg',
	0,'501,544,502,499,503,500,533','LastName  Asc,FirstName  Asc,MiddleName  Asc',0,0,0,0,0,1,1,null,null,null,null,'0C6D456C-F480-43FC-90B8-4D6701BCD072'

select * from syUsers 

select distinct campusid from syCmpGrpCmps where CampGrpId='4ECB3BBB-3543-41BA-8F8F-4F28228DC9B7'

*/
CREATE PROCEDURE [dbo].[Usp_ProgressReport_Master_Query]
    @StuEnrollId VARCHAR(MAX) = NULL
   ,@CampGrpId VARCHAR(4000) = NULL
   ,@PrgVerId VARCHAR(4000) = NULL
   ,@StatusCodeId VARCHAR(4000) = NULL
   ,@TermId VARCHAR(4000) = NULL
   ,@StudentGrpId VARCHAR(4000) = NULL
   ,@GradesFormat VARCHAR(50)
   ,@GPAMethod VARCHAR(50)
   ,@ShowWorkUnitGrouping BIT = 0
   ,@SysComponentTypeId VARCHAR(100) = NULL
   ,@OrderBy VARCHAR(100) = NULL
   ,@ShowFinanceCalculations BIT = 0
   ,@ShowWeeklySchedule BIT = 0
   ,@GradeRounding BIT = 0
   ,@ShowStudentSignatureLine BIT = 0
   ,@ShowSchoolSignatureLine BIT = 0
   ,@ShowPageNumber BIT = 1
   ,@ShowHeading BIT = 1
   ,@StartDateModifier VARCHAR(10) = NULL
   ,@StartDate VARCHAR(50) = NULL
   ,@ExpectedGradDateModifier VARCHAR(10) = NULL
   ,@ExpectedGradDate VARCHAR(50) = NULL
   ,@UserId VARCHAR(50) = NULL
AS
    DECLARE @rownumber INT;
    SET @rownumber = 0;

-- If User selects no filters in the progress report, then go in to the if condition and there is no need to store the student enrollments in a table variable
-- If User selects filters in the progress report, then go in to the else condition and there is a need to store the student enrollments in a table variable
    IF (
         @CampGrpId IS NULL
         AND @PrgVerId IS NULL
         AND @StatusCodeId IS NULL
         AND @StartDate IS NULL
         AND @StuEnrollId IS NULL
         AND @StudentGrpId IS NULL
         AND @TermId IS NULL
       )
        BEGIN
--		INSERT INTO @StoreEnrollments (StuEnrollId)
--		Select Distinct StuEnrollId from arStuEnrollments
            SELECT DISTINCT
                    1 AS Tag
                   ,NULL AS Parent
                   ,PV.PrgVerId AS PrgVerId
                   ,PV.PrgVerDescrip AS PrgVerDescrip
                   ,PV.Credits AS ProgramCredits
                   ,NULL AS TermId
                   ,NULL AS TermDescription
                   ,NULL AS TermStartDate
                   ,NULL AS TermEndDate
                   ,NULL AS CourseId
                   ,NULL AS CourseDescription
                   ,NULL AS CourseCodeDescription
                   ,NULL AS CourseCredits
                   ,NULL AS CourseFinAidCredits
                   ,NULL AS CoursePassingGrade
                   ,NULL AS CourseScore
                   ,NULL AS GradeBook_ResultId
                   ,NULL AS GradeBookDescription
                   ,NULL AS GradeBookScore
                   ,NULL AS GradeBookPostDate
                   ,NULL AS GradeBookPassingGrade
                   ,NULL AS GradeBookWeight
                   ,NULL AS GradeBookRequired
                   ,NULL AS GradeBookMustPass
                   ,NULL AS GradeBookSysComponentTypeId
                   ,NULL AS GradeBookHoursRequired
                   ,NULL AS GradeBookHoursCompleted
                   ,SE.StuEnrollId
                   ,NULL AS MinResult
                   ,NULL AS GradeComponentDescription
                   ,NULL AS CreditsAttempted
                   ,NULL AS CreditsEarned
                   ,NULL AS Completed
                   ,NULL AS CurrentScore
                   ,NULL AS CurrentGrade
                   ,NULL AS FinalScore
                   ,NULL AS FinalGrade
                   ,NULL AS WeightedAverage_GPA
                   ,NULL AS SimpleAverage_GPA
                   ,( CASE WHEN (
                                  SELECT    SUM(Count_WeightedAverage_Credits)
                                  FROM      syCreditSummary
                                  WHERE     StuEnrollId = SE.StuEnrollId
                                ) > 0 THEN (
                                             SELECT SUM(Product_WeightedAverage_Credits_GPA) / SUM(Count_WeightedAverage_Credits)
                                             FROM   syCreditSummary
                                             WHERE  StuEnrollId = SE.StuEnrollId
                                           )
                           ELSE 0
                      END ) AS WeightedAverage_CumGPA
                   ,( CASE WHEN (
                                  SELECT    SUM(Count_SimpleAverage_Credits)
                                  FROM      syCreditSummary
                                  WHERE     StuEnrollId = SE.StuEnrollId
                                ) > 0 THEN (
                                             SELECT SUM(Product_SimpleAverage_Credits_GPA) / SUM(Count_SimpleAverage_Credits)
                                             FROM   syCreditSummary
                                             WHERE  StuEnrollId = SE.StuEnrollId
                                           )
                           ELSE 0
                      END ) AS SimpleAverage_CumGPA
                   ,C.CampusId
                   ,C.CampDescrip
                   ,NULL AS rownumber
                   ,S.FirstName AS FirstName
                   ,S.LastName AS LastName
                   ,S.MiddleName
            FROM    arResults GBR
            INNER JOIN arStuEnrollments SE ON GBR.StuEnrollId = SE.StuEnrollId
            INNER JOIN (
                         SELECT StudentId
                               ,FirstName
                               ,LastName
                               ,MiddleName
                         FROM   arStudent
                       ) S ON S.StudentId = SE.StudentId
            INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
            INNER JOIN (
                         SELECT DISTINCT
                                SC.CampusId
                               ,SC.CampDescrip
                         FROM   syCampuses SC
                         INNER JOIN syCmpGrpCmps t1 ON SC.CampusId = t1.CampusId
                         INNER JOIN syCampGrps t2 ON t1.CampGrpId = t2.CampGrpId
                         INNER JOIN syUsersRolesCampGrps t3 ON t2.CampGrpId = t3.CampGrpId
                         WHERE  t3.UserId = @UserId
                       ) C ON SE.CampusId = C.CampusId
            UNION
            SELECT DISTINCT
                    1 AS Tag
                   ,NULL AS Parent
                   ,PV.PrgVerId AS PrgVerId
                   ,PV.PrgVerDescrip AS PrgVerDescrip
                   ,PV.Credits AS ProgramCredits
                   ,NULL AS TermId
                   ,NULL AS TermDescription
                   ,NULL AS TermStartDate
                   ,NULL AS TermEndDate
                   ,NULL AS CourseId
                   ,NULL AS CourseDescription
                   ,NULL AS CourseCodeDescription
                   ,NULL AS CourseCredits
                   ,NULL AS CourseFinAidCredits
                   ,NULL AS CoursePassingGrade
                   ,NULL AS CourseScore
                   ,NULL AS GradeBook_ResultId
                   ,NULL AS GradeBookDescription
                   ,NULL AS GradeBookScore
                   ,NULL AS GradeBookPostDate
                   ,NULL AS GradeBookPassingGrade
                   ,NULL AS GradeBookWeight
                   ,NULL AS GradeBookRequired
                   ,NULL AS GradeBookMustPass
                   ,NULL AS GradeBookSysComponentTypeId
                   ,NULL AS GradeBookHoursRequired
                   ,NULL AS GradeBookHoursRequired
                   ,SE.StuEnrollId
                   ,NULL AS MinResult
                   ,NULL AS GradeComponentDescription
                   ,NULL AS CreditsAttempted
                   ,NULL AS CreditsEarned
                   ,NULL AS Completed
                   ,NULL AS CurrentScore
                   ,NULL AS CurrentGrade
                   ,NULL AS FinalScore
                   ,NULL AS FinalGrade
                   ,NULL AS WeightedAverage_GPA
                   ,NULL AS SimpleAverage_GPA
                   ,( CASE WHEN (
                                  SELECT    SUM(Count_WeightedAverage_Credits)
                                  FROM      syCreditSummary
                                  WHERE     StuEnrollId = SE.StuEnrollId
                                ) > 0 THEN (
                                             SELECT SUM(Product_WeightedAverage_Credits_GPA) / SUM(Count_WeightedAverage_Credits)
                                             FROM   syCreditSummary
                                             WHERE  StuEnrollId = SE.StuEnrollId
                                           )
                           ELSE 0
                      END ) AS WeightedAverage_CumGPA
                   ,( CASE WHEN (
                                  SELECT    SUM(Count_SimpleAverage_Credits)
                                  FROM      syCreditSummary
                                  WHERE     StuEnrollId = SE.StuEnrollId
                                ) > 0 THEN (
                                             SELECT SUM(Product_SimpleAverage_Credits_GPA) / SUM(Count_SimpleAverage_Credits)
                                             FROM   syCreditSummary
                                             WHERE  StuEnrollId = SE.StuEnrollId
                                           )
                           ELSE 0
                      END ) AS SimpleAverage_CumGPA
                   ,C.CampusId
                   ,C.CampDescrip
                   ,NULL AS rownumber
                   ,S.FirstName AS FirstName
                   ,S.LastName AS LastName
                   ,S.MiddleName
            FROM    arTransferGrades GBR
            INNER JOIN arStuEnrollments SE ON GBR.StuEnrollId = SE.StuEnrollId
            INNER JOIN (
                         SELECT StudentId
                               ,FirstName
                               ,LastName
                               ,MiddleName
                         FROM   arStudent
                       ) S ON S.StudentId = SE.StudentId
            INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
            INNER JOIN (
                         SELECT DISTINCT
                                SC.CampusId
                               ,SC.CampDescrip
                         FROM   syCampuses SC
                         INNER JOIN syCmpGrpCmps t1 ON SC.CampusId = t1.CampusId
                         INNER JOIN syCampGrps t2 ON t1.CampGrpId = t2.CampGrpId
                         INNER JOIN syUsersRolesCampGrps t3 ON t2.CampGrpId = t3.CampGrpId
                         WHERE  t3.UserId = @UserId
                       ) C ON SE.CampusId = C.CampusId
				--Get Second Level (Brings in all the terms in which student is registered for classes)
            UNION
            SELECT DISTINCT
                    2
                   ,1
                   ,PV.PrgVerId
                   ,PV.PrgVerDescrip
                   ,NULL
                   ,CS.TermId
                   ,T.TermDescrip
                   ,T.StartDate
                   ,T.EndDate
                   ,NULL
                   ,NULL
                   ,NULL
                   ,NULL
                   ,NULL
                   ,NULL
                   ,NULL
                   ,NULL
                   ,NULL
                   ,NULL
                   ,NULL
                   ,NULL
                   ,NULL
                   ,NULL
                   ,NULL
                   ,NULL
                   ,NULL
                   ,NULL
                   ,SE.StuEnrollId
                   ,NULL AS MinResult
                   ,NULL AS GradeComponentDescription
                   , -- Student data  
                    NULL AS CreditsAttempted
                   ,NULL AS CreditsEarned
                   ,NULL AS Completed
                   ,NULL AS CurrentScore
                   ,NULL AS CurrentGrade
                   ,NULL AS FinalScore
                   ,NULL AS FinalGrade
                   ,( CASE WHEN (
                                  SELECT    SUM(Count_WeightedAverage_Credits)
                                  FROM      syCreditSummary
                                  WHERE     StuEnrollId = SE.StuEnrollId
                                            AND TermId = T.TermId
                                ) > 0 THEN (
                                             SELECT SUM(Product_WeightedAverage_Credits_GPA) / SUM(Count_WeightedAverage_Credits)
                                             FROM   syCreditSummary
                                             WHERE  StuEnrollId = SE.StuEnrollId
                                                    AND TermId = T.TermId
                                           )
                           ELSE 0
                      END ) AS WeightedAverage_GPA
                   ,( CASE WHEN (
                                  SELECT    SUM(Count_SimpleAverage_Credits)
                                  FROM      syCreditSummary
                                  WHERE     StuEnrollId = SE.StuEnrollId
                                            AND TermId = T.TermId
                                ) > 0 THEN (
                                             SELECT SUM(Product_SimpleAverage_Credits_GPA) / SUM(Count_SimpleAverage_Credits)
                                             FROM   syCreditSummary
                                             WHERE  StuEnrollId = SE.StuEnrollId
                                                    AND TermId = T.TermId
                                           )
                           ELSE 0
                      END ) AS SimpleAverage_GPA
                   ,NULL
                   ,NULL
                   ,C.CampusId
                   ,C.CampDescrip
                   ,NULL AS rownumber
                   ,S.FirstName AS FirstName
                   ,S.LastName AS LastName
                   ,S.MiddleName
            FROM    arClassSections CS
            INNER JOIN arResults GBR ON CS.ClsSectionId = GBR.TestId
            INNER JOIN arStuEnrollments SE ON GBR.StuEnrollId = SE.StuEnrollId
            INNER JOIN (
                         SELECT StudentId
                               ,FirstName
                               ,LastName
                               ,MiddleName
                         FROM   arStudent
                       ) S ON S.StudentId = SE.StudentId
            INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
            INNER JOIN arTerm T ON CS.TermId = T.TermId
            INNER JOIN (
                         SELECT DISTINCT
                                SC.CampusId
                               ,SC.CampDescrip
                         FROM   syCampuses SC
                         INNER JOIN syCmpGrpCmps t1 ON SC.CampusId = t1.CampusId
                         INNER JOIN syCampGrps t2 ON t1.CampGrpId = t2.CampGrpId
                         INNER JOIN syUsersRolesCampGrps t3 ON t2.CampGrpId = t3.CampGrpId
                         WHERE  t3.UserId = @UserId
                       ) C ON SE.CampusId = C.CampusId
            UNION
            SELECT DISTINCT
                    2
                   ,1
                   ,PV.PrgVerId
                   ,PV.PrgVerDescrip
                   ,NULL
                   ,GBR.TermId
                   ,T.TermDescrip
                   ,T.StartDate
                   ,T.EndDate
                   ,NULL
                   ,NULL
                   ,NULL
                   ,NULL
                   ,NULL
                   ,NULL
                   ,NULL
                   ,NULL
                   ,NULL
                   ,NULL
                   ,NULL
                   ,NULL
                   ,NULL
                   ,NULL
                   ,NULL
                   ,NULL
                   ,NULL
                   ,NULL
                   ,SE.StuEnrollId
                   ,NULL AS MinResult
                   ,NULL AS GradeComponentDescription -- Student data    
                   ,NULL AS CreditsAttempted
                   ,NULL AS CreditsEarned
                   ,NULL AS Completed
                   ,NULL AS CurrentScore
                   ,NULL AS CurrentGrade
                   ,NULL AS FinalScore
                   ,NULL AS FinalGrade
                   ,( CASE WHEN (
                                  SELECT    SUM(Count_WeightedAverage_Credits)
                                  FROM      syCreditSummary
                                  WHERE     StuEnrollId = SE.StuEnrollId
                                            AND TermId = T.TermId
                                ) > 0 THEN (
                                             SELECT SUM(Product_WeightedAverage_Credits_GPA) / SUM(Count_WeightedAverage_Credits)
                                             FROM   syCreditSummary
                                             WHERE  StuEnrollId = SE.StuEnrollId
                                                    AND TermId = T.TermId
                                           )
                           ELSE 0
                      END ) AS WeightedAverage_GPA
                   ,( CASE WHEN (
                                  SELECT    SUM(Count_SimpleAverage_Credits)
                                  FROM      syCreditSummary
                                  WHERE     StuEnrollId = SE.StuEnrollId
                                            AND TermId = T.TermId
                                ) > 0 THEN (
                                             SELECT SUM(Product_SimpleAverage_Credits_GPA) / SUM(Count_SimpleAverage_Credits)
                                             FROM   syCreditSummary
                                             WHERE  StuEnrollId = SE.StuEnrollId
                                                    AND TermId = T.TermId
                                           )
                           ELSE 0
                      END ) AS SimpleAverage_GPA
                   ,NULL
                   ,NULL
                   ,C.CampusId
                   ,C.CampDescrip
                   ,NULL AS rownumber
                   ,S.FirstName AS FirstName
                   ,S.LastName AS LastName
                   ,S.MiddleName
            FROM    arTransferGrades GBR
            INNER JOIN arStuEnrollments SE ON GBR.StuEnrollId = SE.StuEnrollId
            INNER JOIN (
                         SELECT StudentId
                               ,FirstName
                               ,LastName
                               ,MiddleName
                         FROM   arStudent
                       ) S ON S.StudentId = SE.StudentId
            INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
            INNER JOIN arTerm T ON GBR.TermId = T.TermId
            INNER JOIN (
                         SELECT DISTINCT
                                SC.CampusId
                               ,SC.CampDescrip
                         FROM   syCampuses SC
                         INNER JOIN syCmpGrpCmps t1 ON SC.CampusId = t1.CampusId
                         INNER JOIN syCampGrps t2 ON t1.CampGrpId = t2.CampGrpId
                         INNER JOIN syUsersRolesCampGrps t3 ON t2.CampGrpId = t3.CampGrpId
                         WHERE  t3.UserId = @UserId
                       ) C ON SE.CampusId = C.CampusId

				-- Level 3 (Returns all courses tied to a term)
            UNION
            SELECT DISTINCT
                    3 AS Tag
                   ,2
                   ,PV.PrgVerId
                   ,PV.PrgVerDescrip
                   ,NULL
                   ,T.TermId
                   ,T.TermDescrip AS TermDescription
                   ,T.StartDate AS TermStartDate
                   ,T.EndDate
                   ,CS.ReqId
                   ,R.Descrip
                   ,'(' + R.Code + ')' + R.Descrip AS CourseCodeDescrip
                   ,R.Credits AS Credits
                   ,R.FinAidCredits AS FinAidCredits
                   ,(
                      SELECT    MIN(MinVal)
                      FROM      arGradeScaleDetails GCD
                               ,arGradeSystemDetails GSD
                      WHERE     GCD.GrdSysDetailId = GSD.GrdSysDetailId
                                AND GSD.IsPass = 1
                                AND GCD.GrdScaleId = CS.GrdScaleId
                    ) AS MinVal
                   ,RES.Score
                   ,NULL
                   ,NULL
                   ,NULL
                   ,NULL
                   ,NULL
                   ,NULL
                   ,NULL
                   ,NULL
                   ,NULL
                   ,NULL
                   ,NULL
                   ,SE.StuEnrollId
                   ,NULL AS MinResult
                   ,NULL AS GradeComponentDescription -- Student data   
                   ,SCS.CreditsAttempted AS CreditsAttempted
                   ,SCS.CreditsEarned AS CreditsEarned
                   ,SCS.Completed AS Completed
                   ,SCS.CurrentScore AS CurrentScore
                   ,SCS.CurrentGrade AS CurrentGrade
                   ,SCS.FinalScore AS FinalScore
                   ,SCS.FinalGrade AS FinalGrade
                   ,( CASE WHEN (
                                  SELECT    SUM(Count_WeightedAverage_Credits)
                                  FROM      syCreditSummary
                                  WHERE     StuEnrollId = SE.StuEnrollId
                                            AND TermId = T.TermId
                                            AND ReqId = R.ReqId
                                ) > 0 THEN (
                                             SELECT SUM(Product_WeightedAverage_Credits_GPA) / SUM(Count_WeightedAverage_Credits)
                                             FROM   syCreditSummary
                                             WHERE  StuEnrollId = SE.StuEnrollId
                                                    AND TermId = T.TermId
                                                    AND ReqId = R.ReqId
                                           )
                           ELSE 0
                      END ) AS WeightedAverage_GPA
                   ,( CASE WHEN (
                                  SELECT    SUM(Count_SimpleAverage_Credits)
                                  FROM      syCreditSummary
                                  WHERE     StuEnrollId = SE.StuEnrollId
                                            AND TermId = T.TermId
                                            AND ReqId = R.ReqId
                                ) > 0 THEN (
                                             SELECT SUM(Product_SimpleAverage_Credits_GPA) / SUM(Count_SimpleAverage_Credits)
                                             FROM   syCreditSummary
                                             WHERE  StuEnrollId = SE.StuEnrollId
                                                    AND TermId = T.TermId
                                                    AND ReqId = R.ReqId
                                           )
                           ELSE 0
                      END ) AS SimpleAverage_GPA
                   ,NULL
                   ,NULL
                   ,C.CampusId
                   ,C.CampDescrip
                   ,NULL AS rownumber
                   ,S.FirstName AS FirstName
                   ,S.LastName AS LastName
                   ,S.MiddleName
            FROM    arClassSections CS
            INNER JOIN arResults GBR ON CS.ClsSectionId = GBR.TestId
            INNER JOIN arStuEnrollments SE ON GBR.StuEnrollId = SE.StuEnrollId
            INNER JOIN (
                         SELECT StudentId
                               ,FirstName
                               ,LastName
                               ,MiddleName
                         FROM   arStudent
                       ) S ON S.StudentId = SE.StudentId
            INNER JOIN (
                         SELECT DISTINCT
                                SC.CampusId
                               ,SC.CampDescrip
                         FROM   syCampuses SC
                         INNER JOIN syCmpGrpCmps t1 ON SC.CampusId = t1.CampusId
                         INNER JOIN syCampGrps t2 ON t1.CampGrpId = t2.CampGrpId
                         INNER JOIN syUsersRolesCampGrps t3 ON t2.CampGrpId = t3.CampGrpId
                         WHERE  t3.UserId = @UserId
                       ) C ON SE.CampusId = C.CampusId
            INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
            INNER JOIN arTerm T ON CS.TermId = T.TermId
            INNER JOIN arReqs R ON CS.ReqId = R.ReqId
            INNER JOIN arResults RES ON RES.StuEnrollId = GBR.StuEnrollId
                                        AND RES.TestId = CS.ClsSectionId
            LEFT JOIN syCreditSummary SCS ON SE.StuEnrollId = SCS.StuEnrollId
                                             AND T.TermId = SCS.TermId
                                             AND R.ReqId = SCS.ReqId
            UNION
            SELECT DISTINCT
                    3
                   ,2
                   ,PV.PrgVerId
                   ,PV.PrgVerDescrip
                   ,NULL
                   ,T.TermId
                   ,T.TermDescrip
                   ,T.StartDate
                   ,T.EndDate
                   ,GBCR.ReqId
                   ,R.Descrip AS CourseDescrip
                   ,'(' + R.Code + ' ) ' + R.Descrip AS CourseCodeDescrip
                   ,R.Credits
                   ,R.FinAidCredits
                   ,(
                      SELECT    MIN(MinVal)
                      FROM      arGradeScaleDetails GCD
                               ,arGradeSystemDetails GSD
                      WHERE     GCD.GrdSysDetailId = GSD.GrdSysDetailId
                                AND GSD.IsPass = 1
                    )
                   ,AR.Score
                   ,NULL
                   ,NULL
                   ,NULL
                   ,NULL
                   ,NULL
                   ,NULL
                   ,NULL
                   ,NULL
                   ,NULL
                   ,NULL
                   ,NULL
                   ,SE.StuEnrollId
                   ,NULL AS MinResult
                   ,NULL AS GradeComponentDescription -- Student data    
                   ,SCS.CreditsAttempted AS CreditsAttempted
                   ,SCS.CreditsEarned AS CreditsEarned
                   ,SCS.Completed AS Completed
                   ,SCS.CurrentScore AS CurrentScore
                   ,SCS.CurrentGrade AS CurrentGrade
                   ,SCS.FinalScore AS FinalScore
                   ,SCS.FinalGrade AS FinalGrade
                   ,( CASE WHEN (
                                  SELECT    SUM(Count_WeightedAverage_Credits)
                                  FROM      syCreditSummary
                                  WHERE     StuEnrollId = SE.StuEnrollId
                                            AND TermId = T.TermId
                                            AND ReqId = R.ReqId
                                ) > 0 THEN (
                                             SELECT SUM(Product_WeightedAverage_Credits_GPA) / SUM(Count_WeightedAverage_Credits)
                                             FROM   syCreditSummary
                                             WHERE  StuEnrollId = SE.StuEnrollId
                                                    AND TermId = T.TermId
                                                    AND ReqId = R.ReqId
                                           )
                           ELSE 0
                      END ) AS WeightedAverage_GPA
                   ,( CASE WHEN (
                                  SELECT    SUM(Count_SimpleAverage_Credits)
                                  FROM      syCreditSummary
                                  WHERE     StuEnrollId = SE.StuEnrollId
                                            AND TermId = T.TermId
                                            AND ReqId = R.ReqId
                                ) > 0 THEN (
                                             SELECT SUM(Product_SimpleAverage_Credits_GPA) / SUM(Count_SimpleAverage_Credits)
                                             FROM   syCreditSummary
                                             WHERE  StuEnrollId = SE.StuEnrollId
                                                    AND TermId = T.TermId
                                                    AND ReqId = R.ReqId
                                           )
                           ELSE 0
                      END ) AS SimpleAverage_GPA
                   ,NULL
                   ,NULL
                   ,C.CampusId
                   ,C.CampDescrip
                   ,NULL AS rownumber
                   ,S.FirstName AS FirstName
                   ,S.LastName AS LastName
                   ,S.MiddleName
            FROM    arTransferGrades GBCR
            INNER JOIN arStuEnrollments SE ON GBCR.StuEnrollId = SE.StuEnrollId
            INNER JOIN (
                         SELECT StudentId
                               ,FirstName
                               ,LastName
                               ,MiddleName
                         FROM   arStudent
                       ) S ON S.StudentId = SE.StudentId
            INNER JOIN (
                         SELECT DISTINCT
                                SC.CampusId
                               ,SC.CampDescrip
                         FROM   syCampuses SC
                         INNER JOIN syCmpGrpCmps t1 ON SC.CampusId = t1.CampusId
                         INNER JOIN syCampGrps t2 ON t1.CampGrpId = t2.CampGrpId
                         INNER JOIN syUsersRolesCampGrps t3 ON t2.CampGrpId = t3.CampGrpId
                         WHERE  t3.UserId = @UserId
                       ) C ON SE.CampusId = C.CampusId
            INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
            INNER JOIN arTerm T ON GBCR.TermId = T.TermId
            INNER JOIN arReqs R ON GBCR.ReqId = R.ReqId
            INNER JOIN arResults AR ON GBCR.StuEnrollId = AR.StuEnrollId
            LEFT JOIN syCreditSummary SCS ON SE.StuEnrollId = SCS.StuEnrollId
                                             AND T.TermId = SCS.TermId
                                             AND R.ReqId = SCS.ReqId
            UNION
            SELECT  4 AS Tag
                   ,3
                   ,PV.PrgVerId
                   ,PV.PrgVerDescrip
                   ,NULL
                   ,T.TermId
                   ,T.TermDescrip
                   ,T.StartDate AS TermStartDate
                   ,T.EndDate AS TermEndDate
                   ,R.ReqId
                   ,R.Descrip
                   ,'(' + R.Code + ' ) ' + R.Descrip AS CourseCodeDescrip
                   ,NULL
                   ,NULL
                   ,NULL
                   ,NULL
                   ,GBR.GrdBkResultId
                   ,RTRIM(GCT.Descrip) AS GradeBookDescription
                   ,( CASE GCT.SysComponentTypeId
                        WHEN 544 THEN (
                                        SELECT  SUM(HoursAttended)
                                        FROM    arExternshipAttendance
                                        WHERE   StuEnrollId = SE.StuEnrollId
                                      )
                        ELSE GBR.Score
                      END ) AS GradeBookResult
                   ,NULL
                   ,NULL
                   ,NULL
                   ,NULL
                   ,NULL
                   ,GCT.SysComponentTypeId
                   ,NULL
                   ,NULL
                   ,SE.StuEnrollId
                   ,( CASE WHEN GCT.SysComponentTypeId IN ( 500,503,504 ) THEN GBWD.Number
                           ELSE (
                                  SELECT    MIN(MinVal)
                                  FROM      arGradeScaleDetails GSD
                                           ,arGradeSystemDetails GSS
                                  WHERE     GSD.GrdSysDetailId = GSS.GrdSysDetailId
                                            AND GSS.IsPass = 1
                                )
                      END ) AS MinResult
                   ,SYRES.Resource AS GradeComponentDescription
                   ,NULL AS CreditsAttempted
                   ,NULL AS CreditsEarned
                   ,NULL AS Completed
                   ,NULL AS CurrentScore
                   ,NULL AS CurrentGrade
                   ,NULL AS FinalScore
                   ,NULL AS FinalGrade
                   ,NULL AS WeightedAverage_GPA
                   ,NULL AS SimpleAverage_GPA
                   ,NULL
                   ,NULL
                   ,C.CampusId
                   ,C.CampDescrip
                   ,ROW_NUMBER() OVER ( PARTITION BY SE.StuEnrollId,R.ReqId ORDER BY C.CampDescrip, PV.PrgVerDescrip, T.StartDate, T.EndDate, T.TermId, T.TermDescrip, R.ReqId, R.Descrip, GCT.SysComponentTypeId, GCT.Descrip ) AS RowNumber
                   ,S.FirstName AS FirstName
                   ,S.LastName AS LastName
                   ,S.MiddleName
            FROM    arGrdBkResults GBR
            INNER JOIN arStuEnrollments SE ON GBR.StuEnrollId = SE.StuEnrollId
            INNER JOIN (
                         SELECT StudentId
                               ,FirstName
                               ,LastName
                               ,MiddleName
                         FROM   arStudent
                       ) S ON S.StudentId = SE.StudentId
            INNER JOIN arClassSections CS ON CS.ClsSectionId = GBR.ClsSectionId
            INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
            INNER JOIN arTerm T ON CS.TermId = T.TermId
            INNER JOIN arReqs R ON CS.ReqId = R.ReqId
            INNER JOIN arResults RES ON RES.StuEnrollId = GBR.StuEnrollId
                                        AND RES.TestId = CS.ClsSectionId
            INNER JOIN arGrdBkWgtDetails GBWD ON GBR.InstrGrdBkWgtDetailId = GBWD.InstrGrdBkWgtDetailId
            INNER JOIN arGrdComponentTypes GCT ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
            INNER JOIN (
                         SELECT Resource
                               ,ResourceID
                         FROM   syResources
                         WHERE  ResourceTypeID = 10
                       ) SYRES ON SYRES.ResourceID = GCT.SysComponentTypeId
            INNER JOIN (
                         SELECT DISTINCT
                                SC.CampusId
                               ,SC.CampDescrip
                         FROM   syCampuses SC
                         INNER JOIN syCmpGrpCmps t1 ON SC.CampusId = t1.CampusId
                         INNER JOIN syCampGrps t2 ON t1.CampGrpId = t2.CampGrpId
                         INNER JOIN syUsersRolesCampGrps t3 ON t2.CampGrpId = t3.CampGrpId
                         WHERE  t3.UserId = @UserId
                       ) C ON SE.CampusId = C.CampusId
            UNION
            SELECT  4 AS Tag
                   ,3
                   ,PV.PrgVerId
                   ,PV.PrgVerDescrip
                   ,NULL
                   ,T.TermId
                   ,T.TermDescrip
                   ,T.StartDate AS termStartdate
                   ,T.EndDate AS TermEndDate
                   ,GBCR.ReqId
                   ,R.Descrip AS CourseDescrip
                   ,'(' + R.Code + ' ) ' + R.Descrip AS CourseCodeDescrip
                   ,NULL
                   ,NULL
                   ,NULL
                   ,NULL
                   ,GBCR.ConversionResultId AS GrdBkResultId
                   ,RTRIM(GCT.Descrip) AS Comments
                   ,GBCR.Score AS GradeBookResult
                   ,NULL
                   ,NULL
                   ,NULL
                   ,NULL
                   ,NULL
                   ,GCT.SysComponentTypeId
                   ,NULL
                   ,NULL
                   ,SE.StuEnrollId
                   ,GBCR.MinResult
                   ,SYRES.Resource -- Student data  
                   ,NULL AS CreditsAttempted
                   ,NULL AS CreditsEarned
                   ,NULL AS Completed
                   ,NULL AS CurrentScore
                   ,NULL AS CurrentGrade
                   ,NULL AS FinalScore
                   ,NULL AS FinalGrade
                   ,NULL AS WeightedAverage_GPA
                   ,NULL AS SimpleAverage_GPA
                   ,NULL
                   ,NULL
                   ,C.CampusId
                   ,C.CampDescrip
                   ,ROW_NUMBER() OVER ( PARTITION BY SE.StuEnrollId,R.ReqId ORDER BY C.CampDescrip, PV.PrgVerDescrip, T.StartDate, T.EndDate, T.TermId, T.TermDescrip, R.ReqId, R.Descrip, GCT.SysComponentTypeId, GCT.Descrip ) AS RowNumber
                   ,S.FirstName AS FirstName
                   ,S.LastName AS LastName
                   ,S.MiddleName
            FROM    arGrdBkConversionResults GBCR
            INNER JOIN arStuEnrollments SE ON GBCR.StuEnrollId = SE.StuEnrollId
            INNER JOIN (
                         SELECT StudentId
                               ,FirstName
                               ,LastName
                               ,MiddleName
                         FROM   arStudent
                       ) S ON S.StudentId = SE.StudentId
            INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
            INNER JOIN arTerm T ON GBCR.TermId = T.TermId
            INNER JOIN arReqs R ON GBCR.ReqId = R.ReqId
            INNER JOIN arGrdComponentTypes GCT ON GCT.GrdComponentTypeId = GBCR.GrdComponentTypeId
            INNER JOIN arGrdBkWgtDetails GBWD ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
                                                 AND GBWD.GrdComponentTypeId = GBCR.GrdComponentTypeId
            INNER JOIN arGrdBkWeights GBW ON GBWD.InstrGrdBkWgtId = GBW.InstrGrdBkWgtId
                                             AND GBCR.ReqId = GBW.ReqId
            INNER JOIN (
                         SELECT ReqId
                               ,MAX(EffectiveDate) AS EffectiveDate
                         FROM   arGrdBkWeights
                         GROUP BY ReqId
                       ) AS MaxEffectiveDatesByCourse ON GBCR.ReqId = MaxEffectiveDatesByCourse.ReqId
            INNER JOIN (
                         SELECT Resource
                               ,ResourceID
                         FROM   syResources
                         WHERE  ResourceTypeID = 10
                       ) SYRES ON SYRES.ResourceID = GCT.SysComponentTypeId
            INNER JOIN (
                         SELECT DISTINCT
                                SC.CampusId
                               ,SC.CampDescrip
                         FROM   syCampuses SC
                         INNER JOIN syCmpGrpCmps t1 ON SC.CampusId = t1.CampusId
                         INNER JOIN syCampGrps t2 ON t1.CampGrpId = t2.CampGrpId
                         INNER JOIN syUsersRolesCampGrps t3 ON t2.CampGrpId = t3.CampGrpId
                         WHERE  t3.UserId = @UserId
                       ) C ON SE.CampusId = C.CampusId
            WHERE   MaxEffectiveDatesByCourse.EffectiveDate <= T.StartDate
            ORDER BY Tag
                   ,CampDescrip
                   ,PrgVerDescrip
                   ,termStartdate
                   ,TermEndDate
                   ,TermId
                   ,TermDescription
                   ,CourseID
                   ,CourseDescription
                   ,GradeBookSysComponentTypeId
                   ,GradeBookDescription;
        END;
    ELSE
        BEGIN
            DECLARE @StoreEnrollments TABLE
                (
                 StuEnrollId UNIQUEIDENTIFIER UNIQUE
                );


            INSERT  INTO @StoreEnrollments
                    (
                     StuEnrollId
                    )
                    SELECT  StuEnrollId
                    FROM    (
                              SELECT DISTINCT
                                        SE.StuEnrollId
                                       ,S.FirstName
                                       ,S.LastName
                                       ,S.MiddleName
                              FROM      arStuEnrollments SE
                              INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                              INNER JOIN (
                                           SELECT   StudentId
                                                   ,FirstName
                                                   ,LastName
                                                   ,MiddleName
                                           FROM     arStudent
                                         ) S ON S.StudentId = SE.StudentId
                              INNER JOIN syCmpGrpCmps CGC ON SE.CampusId = CGC.CampusId
                              INNER JOIN syStatusCodes SC ON SE.StatusCodeId = SC.StatusCodeId
                              INNER JOIN arResults R ON SE.StuEnrollId = R.StuEnrollId
                              INNER JOIN arClassSections CS ON R.TestId = CS.ClsSectionId
                              INNER JOIN arTerm T ON CS.TermId = T.TermId 
					--INNER JOIN (select distinct campgrpid,userid from syUsersRolesCampGrps where userid=@userid) urc on CGC.CampGrpId=urc.CampGrpId 
                              LEFT OUTER JOIN adLeadByLeadGroups LLG ON SE.StuEnrollId = LLG.StuEnrollId
                              WHERE     --					URC.UserId= @UserId and 
                                        (
                                          @CampGrpId IS NULL
                                          OR CGC.CampGrpId IN ( SELECT  Val
                                                                FROM    MultipleValuesForReportParameters(@CampGrpId,',',1) )
                                        )
                                        AND (
                                              @StatusCodeId IS NULL
                                              OR SC.StatusCodeId IN ( SELECT    Val
                                                                      FROM      MultipleValuesForReportParameters(@StatusCodeId,',',1) )
                                            )
                                        AND (
                                              @PrgVerId IS NULL
                                              OR SE.PrgVerId IN ( SELECT    Val
                                                                  FROM      MultipleValuesForReportParameters(@PrgVerId,',',1) )
                                            )
                                        AND
					-- if A Then B is equivalent to (Not A) or B
                                        (
                                          @StartDate IS NULL
                                          OR @StartDateModifier IS NULL
                                          OR (
                                               (
                                                 ( @StartDateModifier <> '=' )
                                                 OR ( T.StartDate = @StartDate )
                                               )
                                               AND (
                                                     ( @StartDateModifier <> '>' )
                                                     OR ( T.StartDate > @StartDate )
                                                   )
                                               AND (
                                                     ( @StartDateModifier <> '<' )
                                                     OR ( T.StartDate < @StartDate )
                                                   )
                                               AND (
                                                     ( @StartDateModifier <> '>=' )
                                                     OR ( T.StartDate >= @StartDate )
                                                   )
                                               AND (
                                                     ( @StartDateModifier <> '<=' )
                                                     OR ( T.StartDate <= @StartDate )
                                                   )
                                             )
                                        )
                                        AND (
                                              @ExpectedGradDate IS NULL
                                              OR @ExpectedGradDateModifier IS NULL
                                              OR (
                                                   (
                                                     ( @ExpectedGradDateModifier <> '=' )
                                                     OR ( SE.ExpGradDate = @ExpectedGradDate )
                                                   )
                                                   AND (
                                                         ( @ExpectedGradDateModifier <> '>' )
                                                         OR ( SE.ExpGradDate > @ExpectedGradDate )
                                                       )
                                                   AND (
                                                         ( @ExpectedGradDateModifier <> '<' )
                                                         OR ( SE.ExpGradDate < @ExpectedGradDate )
                                                       )
                                                   AND (
                                                         ( @ExpectedGradDateModifier <> '>=' )
                                                         OR ( SE.ExpGradDate >= @ExpectedGradDate )
                                                       )
                                                   AND (
                                                         ( @ExpectedGradDateModifier <> '<=' )
                                                         OR ( SE.ExpGradDate <= @ExpectedGradDate )
                                                       )
                                                 )
                                            )
                                        AND (
                                              @StuEnrollId IS NULL
                                              OR SE.StuEnrollId IN ( SELECT Val
                                                                     FROM   MultipleValuesForReportParameters(@StuEnrollId,',',1) )
                                            )
                                        AND (
                                              @TermId IS NULL
                                              OR T.TermId IN ( SELECT   Val
                                                               FROM     MultipleValuesForReportParameters(@TermId,',',1) )
                                            )
                                        AND (
                                              @StudentGrpId IS NULL
                                              OR LLG.LeadGrpId IN ( SELECT  Val
                                                                    FROM    MultipleValuesForReportParameters(@StudentGrpId,',',1) )
                                            )
                            ) dt
                    ORDER BY CASE WHEN @OrderBy = 'LastName Asc,FirstName Asc,MiddleName Asc' THEN dt.LastName
                             END
                           ,CASE WHEN @OrderBy = 'LastName Asc,FirstName Asc,MiddleName Asc' THEN dt.FirstName
                            END
                           ,CASE WHEN @OrderBy = 'LastName Asc,FirstName Asc,MiddleName Asc' THEN dt.MiddleName
                            END
                           ,CASE WHEN @OrderBy = 'LastName Asc,FirstName Asc,MiddleName Desc' THEN dt.LastName
                            END
                           ,CASE WHEN @OrderBy = 'LastName Asc,FirstName Asc,MiddleName Desc' THEN dt.FirstName
                            END
                           ,CASE WHEN @OrderBy = 'LastName Asc,FirstName Asc,MiddleName Desc' THEN dt.MiddleName
                            END DESC
                           ,CASE WHEN @OrderBy = 'LastName Asc,FirstNameDesc,MiddleName Asc' THEN dt.LastName
                            END
                           ,CASE WHEN @OrderBy = 'LastName Asc,FirstNameDesc,MiddleName Asc' THEN dt.FirstName
                            END DESC
                           ,CASE WHEN @OrderBy = 'LastName Asc,FirstNameDesc,MiddleName Asc' THEN dt.MiddleName
                            END
                           ,CASE WHEN @OrderBy = 'LastName Asc,FirstNameDesc,MiddleName Desc' THEN dt.LastName
                            END
                           ,CASE WHEN @OrderBy = 'LastName Asc,FirstNameDesc,MiddleName Desc' THEN dt.FirstName
                            END DESC
                           ,CASE WHEN @OrderBy = 'LastName Asc,FirstNameDesc,MiddleName Desc' THEN dt.MiddleName
                            END DESC
                           ,
			-- LD
                            CASE WHEN @OrderBy = 'LastName Desc,FirstName Asc,MiddleName Asc' THEN dt.LastName
                            END DESC
                           ,CASE WHEN @OrderBy = 'LastName Desc,FirstName Asc,MiddleName Asc' THEN dt.FirstName
                            END
                           ,CASE WHEN @OrderBy = 'LastName Desc,FirstName Asc,MiddleName Asc' THEN dt.MiddleName
                            END
                           ,CASE WHEN @OrderBy = 'LastName Desc,FirstName Asc,MiddleName Desc' THEN dt.LastName
                            END DESC
                           ,CASE WHEN @OrderBy = 'LastName Desc,FirstName Asc,MiddleName Desc' THEN dt.FirstName
                            END
                           ,CASE WHEN @OrderBy = 'LastName Desc,FirstName Asc,MiddleName Desc' THEN dt.MiddleName
                            END DESC
                           ,CASE WHEN @OrderBy = 'LastName Desc,FirstName Desc,MiddleName Asc' THEN dt.LastName
                            END DESC
                           ,CASE WHEN @OrderBy = 'LastName Desc,FirstName Desc,MiddleName Asc' THEN dt.FirstName
                            END DESC
                           ,CASE WHEN @OrderBy = 'LastName Desc,FirstName Desc,MiddleName Asc' THEN dt.MiddleName
                            END
                           ,CASE WHEN @OrderBy = 'LastName Desc,FirstName Desc,MiddleName Desc' THEN dt.LastName
                            END DESC
                           ,CASE WHEN @OrderBy = 'LastName Desc,FirstName Desc,MiddleName Desc' THEN dt.FirstName
                            END DESC
                           ,CASE WHEN @OrderBy = 'LastName Desc,FirstName Desc,MiddleName Desc' THEN dt.MiddleName
                            END DESC
                           ,CASE WHEN @OrderBy = 'LastName Asc,MiddleName Asc,FirstName Asc' THEN dt.LastName
                            END
                           ,CASE WHEN @OrderBy = 'LastName Asc,MiddleName Asc,FirstName Asc' THEN dt.MiddleName
                            END
                           ,CASE WHEN @OrderBy = 'LastName Asc,MiddleName Asc,FirstName Asc' THEN dt.FirstName
                            END
                           ,CASE WHEN @OrderBy = 'LastName Asc,MiddleName Asc,FirstName Desc' THEN dt.LastName
                            END
                           ,CASE WHEN @OrderBy = 'LastName Asc,MiddleName Asc,FirstName Desc' THEN dt.MiddleName
                            END
                           ,CASE WHEN @OrderBy = 'LastName Asc,MiddleName Asc,FirstName Desc' THEN dt.FirstName
                            END DESC
                           ,CASE WHEN @OrderBy = 'LastName Asc,MiddleName Desc,FirstName Asc' THEN dt.LastName
                            END
                           ,CASE WHEN @OrderBy = 'LastName Asc,MiddleName Desc,FirstName Asc' THEN dt.MiddleName
                            END DESC
                           ,CASE WHEN @OrderBy = 'LastName Asc,MiddleName Desc,FirstName Asc' THEN dt.FirstName
                            END
                           ,CASE WHEN @OrderBy = 'LastName Asc,MiddleName Desc,FirstName Desc' THEN dt.LastName
                            END
                           ,CASE WHEN @OrderBy = 'LastName Asc,MiddleName Desc,FirstName Desc' THEN dt.MiddleName
                            END DESC
                           ,CASE WHEN @OrderBy = 'LastName Asc,MiddleName Desc,FirstName Desc' THEN dt.FirstName
                            END DESC
                           ,CASE WHEN @OrderBy = 'LastName Desc,MiddleName Asc,FirstName Asc' THEN dt.LastName
                            END DESC
                           ,CASE WHEN @OrderBy = 'LastName Desc,MiddleName Asc,FirstName Asc' THEN dt.MiddleName
                            END
                           ,CASE WHEN @OrderBy = 'LastName Desc,MiddleName Asc,FirstName Asc' THEN dt.FirstName
                            END
                           ,CASE WHEN @OrderBy = 'LastName Desc,MiddleName Asc,FirstName Desc' THEN dt.LastName
                            END DESC
                           ,CASE WHEN @OrderBy = 'LastName Desc,MiddleName Asc,FirstName Desc' THEN dt.MiddleName
                            END
                           ,CASE WHEN @OrderBy = 'LastName Desc,MiddleName Asc,FirstName Desc' THEN dt.FirstName
                            END DESC
                           ,CASE WHEN @OrderBy = 'LastName Desc,MiddleName Desc,FirstName Asc' THEN dt.LastName
                            END DESC
                           ,CASE WHEN @OrderBy = 'LastName Desc,MiddleName Desc,FirstName Asc' THEN dt.MiddleName
                            END DESC
                           ,CASE WHEN @OrderBy = 'LastName Desc,MiddleName Desc,FirstName Asc' THEN dt.FirstName
                            END
                           ,CASE WHEN @OrderBy = 'LastName Desc,MiddleName Desc,FirstName Desc' THEN dt.LastName
                            END DESC
                           ,CASE WHEN @OrderBy = 'LastName Desc,MiddleName Desc,FirstName Desc' THEN dt.MiddleName
                            END DESC
                           ,CASE WHEN @OrderBy = 'LastName Desc,MiddleName Desc,FirstName Desc' THEN dt.FirstName
                            END DESC
                           ,CASE WHEN @OrderBy = 'FirstName Asc,LastName Asc,MiddleName Asc' THEN dt.FirstName
                            END
                           ,CASE WHEN @OrderBy = 'FirstName Asc,LastName Asc,MiddleName Asc' THEN dt.LastName
                            END
                           ,CASE WHEN @OrderBy = 'FirstName Asc,LastName Asc,MiddleName Asc' THEN dt.MiddleName
                            END
                           ,CASE WHEN @OrderBy = 'FirstName Asc,LastName Asc,MiddleName Desc' THEN dt.FirstName
                            END
                           ,CASE WHEN @OrderBy = 'FirstName Asc,LastName Asc,MiddleName Desc' THEN dt.LastName
                            END
                           ,CASE WHEN @OrderBy = 'FirstName Asc,LastName Asc,MiddleName Desc' THEN dt.MiddleName
                            END DESC
                           ,CASE WHEN @OrderBy = 'FirstName Asc,LastName Desc,MiddleName Asc' THEN dt.FirstName
                            END
                           ,CASE WHEN @OrderBy = 'FirstName Asc,LastName Desc,MiddleName Asc' THEN dt.LastName
                            END DESC
                           ,CASE WHEN @OrderBy = 'FirstName Asc,LastName Desc,MiddleName Asc' THEN dt.MiddleName
                            END
                           ,CASE WHEN @OrderBy = 'FirstName Asc,LastName Desc,MiddleName Desc' THEN dt.FirstName
                            END
                           ,CASE WHEN @OrderBy = 'FirstName Asc,LastName Desc,MiddleName Desc' THEN dt.LastName
                            END DESC
                           ,CASE WHEN @OrderBy = 'FirstName Asc,LastName Desc,MiddleName Desc' THEN dt.MiddleName
                            END DESC
                           ,
			-- LD
                            CASE WHEN @OrderBy = 'FirstName Desc,LastName Asc,MiddleName Asc' THEN dt.FirstName
                            END DESC
                           ,CASE WHEN @OrderBy = 'FirstName Desc,LastName Asc,MiddleName Asc' THEN dt.LastName
                            END
                           ,CASE WHEN @OrderBy = 'FirstName Desc,LastName Asc,MiddleName Asc' THEN dt.MiddleName
                            END
                           ,CASE WHEN @OrderBy = 'FirstName Desc,LastName Asc,MiddleName Desc' THEN dt.FirstName
                            END DESC
                           ,CASE WHEN @OrderBy = 'FirstName Desc,LastName Asc,MiddleName Desc' THEN dt.LastName
                            END
                           ,CASE WHEN @OrderBy = 'FirstName Desc,LastName Asc,MiddleName Desc' THEN dt.MiddleName
                            END DESC
                           ,CASE WHEN @OrderBy = 'FirstName Desc,LastName Desc,MiddleName Asc' THEN dt.FirstName
                            END DESC
                           ,CASE WHEN @OrderBy = 'FirstName Desc,LastName Desc,MiddleName Asc' THEN dt.LastName
                            END DESC
                           ,CASE WHEN @OrderBy = 'FirstName Desc,LastName Desc,MiddleName Asc' THEN dt.MiddleName
                            END
                           ,CASE WHEN @OrderBy = 'FirstName Desc,LastName Desc,MiddleName Desc' THEN dt.FirstName
                            END DESC
                           ,CASE WHEN @OrderBy = 'FirstName Desc,LastName Desc,MiddleName Desc' THEN dt.LastName
                            END DESC
                           ,CASE WHEN @OrderBy = 'FirstName Desc,LastName Desc,MiddleName Desc' THEN dt.MiddleName
                            END DESC
                           ,CASE WHEN @OrderBy = 'FirstName Asc,MiddleName Asc,LastName Asc' THEN dt.FirstName
                            END
                           ,CASE WHEN @OrderBy = 'FirstName Asc,MiddleName Asc,LastName Asc' THEN dt.MiddleName
                            END
                           ,CASE WHEN @OrderBy = 'FirstName Asc,MiddleName Asc,LastName Asc' THEN dt.LastName
                            END
                           ,CASE WHEN @OrderBy = 'FirstName Asc,MiddleName Asc,LastName Desc' THEN dt.FirstName
                            END
                           ,CASE WHEN @OrderBy = 'FirstName Asc,MiddleName Asc,LastName Desc' THEN dt.MiddleName
                            END
                           ,CASE WHEN @OrderBy = 'FirstName Asc,MiddleName Asc,LastName Desc' THEN dt.LastName
                            END DESC
                           ,CASE WHEN @OrderBy = 'FirstName Asc,MiddleName Desc,LastName Asc' THEN dt.FirstName
                            END
                           ,CASE WHEN @OrderBy = 'FirstName Asc,MiddleName Desc,LastName Asc' THEN dt.MiddleName
                            END DESC
                           ,CASE WHEN @OrderBy = 'FirstName Asc,MiddleName Desc,LastName Asc' THEN dt.LastName
                            END
                           ,CASE WHEN @OrderBy = 'FirstName Asc,MiddleName Desc,LastName Desc' THEN dt.FirstName
                            END
                           ,CASE WHEN @OrderBy = 'FirstName Asc,MiddleName Desc,LastName Desc' THEN dt.MiddleName
                            END DESC
                           ,CASE WHEN @OrderBy = 'FirstName Asc,MiddleName Desc,LastName Desc' THEN dt.LastName
                            END DESC
                           ,CASE WHEN @OrderBy = 'FirstName Desc,MiddleName Asc,LastName Asc' THEN dt.FirstName
                            END DESC
                           ,CASE WHEN @OrderBy = 'FirstName Desc,MiddleName Asc,LastName Asc' THEN dt.MiddleName
                            END
                           ,CASE WHEN @OrderBy = 'FirstName Desc,MiddleName Asc,LastName Asc' THEN dt.LastName
                            END
                           ,CASE WHEN @OrderBy = 'FirstName Desc,MiddleName Asc,LastName Desc' THEN dt.FirstName
                            END DESC
                           ,CASE WHEN @OrderBy = 'FirstName Desc,MiddleName Asc,LastName Desc' THEN dt.MiddleName
                            END
                           ,CASE WHEN @OrderBy = 'FirstName Desc,MiddleName Asc,LastName Desc' THEN dt.LastName
                            END DESC
                           ,CASE WHEN @OrderBy = 'FirstName Desc,MiddleName Desc,LastName Asc' THEN dt.FirstName
                            END DESC
                           ,CASE WHEN @OrderBy = 'FirstName Desc,MiddleName Desc,LastName Asc' THEN dt.MiddleName
                            END DESC
                           ,CASE WHEN @OrderBy = 'FirstName Desc,MiddleName Desc,LastName Asc' THEN dt.LastName
                            END
                           ,CASE WHEN @OrderBy = 'FirstName Desc,MiddleName Desc,LastName Desc' THEN dt.FirstName
                            END DESC
                           ,CASE WHEN @OrderBy = 'FirstName Desc,MiddleName Desc,LastName Desc' THEN dt.MiddleName
                            END DESC
                           ,CASE WHEN @OrderBy = 'FirstName Desc,MiddleName Desc,LastName Desc' THEN dt.LastName
                            END DESC
                           ,CASE WHEN @OrderBy = 'MiddleName Asc,LastName Asc,FirstName Asc' THEN dt.MiddleName
                            END
                           ,CASE WHEN @OrderBy = 'MiddleName Asc,LastName Asc,FirstName Asc' THEN dt.LastName
                            END
                           ,CASE WHEN @OrderBy = 'MiddleName Asc,LastName Asc,FirstName Asc' THEN dt.FirstName
                            END
                           ,CASE WHEN @OrderBy = 'MiddleName Asc,LastName Asc,FirstName Desc' THEN dt.MiddleName
                            END
                           ,CASE WHEN @OrderBy = 'MiddleName Asc,LastName Asc,FirstName Desc' THEN dt.LastName
                            END
                           ,CASE WHEN @OrderBy = 'MiddleName Asc,LastName Asc,FirstName Desc' THEN dt.FirstName
                            END DESC
                           ,CASE WHEN @OrderBy = 'MiddleName Asc,LastName Desc,FirstName Asc' THEN dt.MiddleName
                            END
                           ,CASE WHEN @OrderBy = 'MiddleName Asc,LastName Desc,FirstName Asc' THEN dt.LastName
                            END DESC
                           ,CASE WHEN @OrderBy = 'MiddleName Asc,LastName Desc,FirstName Asc' THEN dt.FirstName
                            END
                           ,CASE WHEN @OrderBy = 'MiddleName Asc,LastName Desc,FirstName Desc' THEN dt.MiddleName
                            END
                           ,CASE WHEN @OrderBy = 'MiddleName Asc,LastName Desc,FirstName Desc' THEN dt.LastName
                            END DESC
                           ,CASE WHEN @OrderBy = 'MiddleName Asc,LastName Desc,FirstName Desc' THEN dt.FirstName
                            END DESC
                           ,CASE WHEN @OrderBy = 'MiddleName Desc,LastName Asc,FirstName Asc' THEN dt.MiddleName
                            END DESC
                           ,CASE WHEN @OrderBy = 'MiddleName Desc,LastName Asc,FirstName Asc' THEN dt.LastName
                            END
                           ,CASE WHEN @OrderBy = 'MiddleName Desc,LastName Asc,FirstName Asc' THEN dt.FirstName
                            END
                           ,CASE WHEN @OrderBy = 'MiddleName Desc,LastName Asc,FirstName Desc' THEN dt.MiddleName
                            END DESC
                           ,CASE WHEN @OrderBy = 'MiddleName Desc,LastName Asc,FirstName Desc' THEN dt.LastName
                            END
                           ,CASE WHEN @OrderBy = 'MiddleName Desc,LastName Asc,FirstName Desc' THEN dt.FirstName
                            END DESC
                           ,CASE WHEN @OrderBy = 'MiddleName Desc,LastName Desc,FirstName Asc' THEN dt.MiddleName
                            END DESC
                           ,CASE WHEN @OrderBy = 'MiddleName Desc,LastName Desc,FirstName Asc' THEN dt.LastName
                            END DESC
                           ,CASE WHEN @OrderBy = 'MiddleName Desc,LastName Desc,FirstName Asc' THEN dt.FirstName
                            END
                           ,CASE WHEN @OrderBy = 'MiddleName Desc,LastName Desc,FirstName Desc' THEN dt.MiddleName
                            END DESC
                           ,CASE WHEN @OrderBy = 'MiddleName Desc,LastName Desc,FirstName Desc' THEN dt.LastName
                            END DESC
                           ,CASE WHEN @OrderBy = 'MiddleName Desc,LastName Desc,FirstName Desc' THEN dt.FirstName
                            END DESC
                           ,CASE WHEN @OrderBy = 'MiddleName Asc,FirstName Asc,LastName Asc' THEN dt.MiddleName
                            END
                           ,CASE WHEN @OrderBy = 'MiddleName Asc,FirstName Asc,LastName Asc' THEN dt.FirstName
                            END
                           ,CASE WHEN @OrderBy = 'MiddleName Asc,FirstName Asc,LastName Asc' THEN dt.LastName
                            END
                           ,CASE WHEN @OrderBy = 'MiddleName Asc,FirstName Asc,LastName Desc' THEN dt.MiddleName
                            END
                           ,CASE WHEN @OrderBy = 'MiddleName Asc,FirstName Asc,LastName Desc' THEN dt.FirstName
                            END
                           ,CASE WHEN @OrderBy = 'MiddleName Asc,FirstName Asc,LastName Desc' THEN dt.LastName
                            END DESC
                           ,CASE WHEN @OrderBy = 'MiddleName Asc,FirstName Desc,LastName Asc' THEN dt.MiddleName
                            END
                           ,CASE WHEN @OrderBy = 'MiddleName Asc,FirstName Desc,LastName Asc' THEN dt.FirstName
                            END DESC
                           ,CASE WHEN @OrderBy = 'MiddleName Asc,FirstName Desc,LastName Asc' THEN dt.LastName
                            END
                           ,CASE WHEN @OrderBy = 'MiddleName Asc,FirstName Desc,LastName Desc' THEN dt.MiddleName
                            END
                           ,CASE WHEN @OrderBy = 'MiddleName Asc,FirstName Desc,LastName Desc' THEN dt.FirstName
                            END DESC
                           ,CASE WHEN @OrderBy = 'MiddleName Asc,FirstName Desc,LastName Desc' THEN dt.LastName
                            END DESC
                           ,CASE WHEN @OrderBy = 'MiddleName Desc,FirstName Asc,LastName Asc' THEN dt.MiddleName
                            END DESC
                           ,CASE WHEN @OrderBy = 'MiddleName Desc,FirstName Asc,LastName Asc' THEN dt.FirstName
                            END
                           ,CASE WHEN @OrderBy = 'MiddleName Desc,FirstName Asc,LastName Asc' THEN dt.LastName
                            END
                           ,CASE WHEN @OrderBy = 'MiddleName Desc,FirstName Asc,LastName Desc' THEN dt.MiddleName
                            END DESC
                           ,CASE WHEN @OrderBy = 'MiddleName Desc,FirstName Asc,LastName Desc' THEN dt.FirstName
                            END
                           ,CASE WHEN @OrderBy = 'MiddleName Desc,FirstName Asc,LastName Desc' THEN dt.LastName
                            END DESC
                           ,CASE WHEN @OrderBy = 'MiddleName Desc,FirstName Desc,LastName Asc' THEN dt.MiddleName
                            END DESC
                           ,CASE WHEN @OrderBy = 'MiddleName Desc,FirstName Desc,LastName Asc' THEN dt.FirstName
                            END DESC
                           ,CASE WHEN @OrderBy = 'MiddleName Desc,FirstName Desc,LastName Asc' THEN dt.LastName
                            END
                           ,CASE WHEN @OrderBy = 'MiddleName Desc,FirstName Desc,LastName Desc' THEN dt.MiddleName
                            END DESC
                           ,CASE WHEN @OrderBy = 'MiddleName Desc,FirstName Desc,LastName Desc' THEN dt.FirstName
                            END DESC
                           ,CASE WHEN @OrderBy = 'MiddleName Desc,FirstName Desc,LastName Desc' THEN dt.LastName
                            END DESC;

		
							
            SELECT  *
            FROM    (
                      SELECT DISTINCT
                                1 AS Tag
                               ,NULL AS Parent
                               ,PV.PrgVerId AS PrgVerId
                               ,PV.PrgVerDescrip AS PrgVerDescrip
                               ,PV.Credits AS ProgramCredits
                               ,NULL AS TermId
                               ,NULL AS TermDescription
                               ,NULL AS TermStartDate
                               ,NULL AS TermEndDate
                               ,NULL AS CourseId
                               ,NULL AS CourseDescription
                               ,NULL AS CourseCodeDescription
                               ,NULL AS CourseCredits
                               ,NULL AS CourseFinAidCredits
                               ,NULL AS CoursePassingGrade
                               ,NULL AS CourseScore
                               ,NULL AS GradeBook_ResultId
                               ,NULL AS GradeBookDescription
                               ,NULL AS GradeBookScore
                               ,NULL AS GradeBookPostDate
                               ,NULL AS GradeBookPassingGrade
                               ,NULL AS GradeBookWeight
                               ,NULL AS GradeBookRequired
                               ,NULL AS GradeBookMustPass
                               ,NULL AS GradeBookSysComponentTypeId
                               ,NULL AS GradeBookHoursRequired
                               ,NULL AS GradeBookHoursCompleted
                               ,SE.StuEnrollId
                               ,NULL AS MinResult
                               ,NULL AS GradeComponentDescription
                               ,NULL AS CreditsAttempted
                               ,NULL AS CreditsEarned
                               ,NULL AS Completed
                               ,NULL AS CurrentScore
                               ,NULL AS CurrentGrade
                               ,NULL AS FinalScore
                               ,NULL AS FinalGrade
                               ,NULL AS WeightedAverage_GPA
                               ,NULL AS SimpleAverage_GPA
                               ,
--						(Select Sum(Product_WeightedAverage_Credits_GPA)/Sum(Count_WeightedAverage_Credits) from syCreditSummary where StuEnrollId=SE.StuEnrollId) as WeightedAverage_CumGPA,
--						(Select Sum(Product_SimpleAverage_Credits_GPA)/Sum(Count_SimpleAverage_Credits) from syCreditSummary where StuEnrollId=SE.StuEnrollId) as SimpleAverage_CumGPA,
                                ( CASE WHEN (
                                              SELECT    SUM(Count_WeightedAverage_Credits)
                                              FROM      syCreditSummary
                                              WHERE     StuEnrollId = SE.StuEnrollId
                                            ) > 0 THEN (
                                                         SELECT SUM(Product_WeightedAverage_Credits_GPA) / SUM(Count_WeightedAverage_Credits)
                                                         FROM   syCreditSummary
                                                         WHERE  StuEnrollId = SE.StuEnrollId
                                                       )
                                       ELSE 0
                                  END ) AS WeightedAverage_CumGPA
                               ,( CASE WHEN (
                                              SELECT    SUM(Count_SimpleAverage_Credits)
                                              FROM      syCreditSummary
                                              WHERE     StuEnrollId = SE.StuEnrollId
                                            ) > 0 THEN (
                                                         SELECT SUM(Product_SimpleAverage_Credits_GPA) / SUM(Count_SimpleAverage_Credits)
                                                         FROM   syCreditSummary
                                                         WHERE  StuEnrollId = SE.StuEnrollId
                                                       )
                                       ELSE 0
                                  END ) AS SimpleAverage_CumGPA
                               ,C.CampusId
                               ,C.CampDescrip
                               ,NULL AS rownumber
                               ,S.FirstName AS FirstName
                               ,S.LastName AS LastName
                               ,S.MiddleName
                      FROM      arResults GBR
                      INNER JOIN arStuEnrollments SE ON GBR.StuEnrollId = SE.StuEnrollId
                      INNER JOIN (
                                   SELECT   StuEnrollId
                                   FROM     @StoreEnrollments
                                 ) t5 ON SE.StuEnrollId = t5.StuEnrollId
                      INNER JOIN (
                                   SELECT   StudentId
                                           ,FirstName
                                           ,LastName
                                           ,MiddleName
                                   FROM     arStudent
                                 ) S ON S.StudentId = SE.StudentId
                      INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                      INNER JOIN syCampuses C ON SE.CampusId = C.CampusId
                      UNION
                      SELECT DISTINCT
                                1 AS Tag
                               ,NULL AS Parent
                               ,PV.PrgVerId AS PrgVerId
                               ,PV.PrgVerDescrip AS PrgVerDescrip
                               ,PV.Credits AS ProgramCredits
                               ,NULL AS TermId
                               ,NULL AS TermDescription
                               ,NULL AS TermStartDate
                               ,NULL AS TermEndDate
                               ,NULL AS CourseId
                               ,NULL AS CourseDescription
                               ,NULL AS CourseCodeDescription
                               ,NULL AS CourseCredits
                               ,NULL AS CourseFinAidCredits
                               ,NULL AS CoursePassingGrade
                               ,NULL AS CourseScore
                               ,NULL AS GradeBook_ResultId
                               ,NULL AS GradeBookDescription
                               ,NULL AS GradeBookScore
                               ,NULL AS GradeBookPostDate
                               ,NULL AS GradeBookPassingGrade
                               ,NULL AS GradeBookWeight
                               ,NULL AS GradeBookRequired
                               ,NULL AS GradeBookMustPass
                               ,NULL AS GradeBookSysComponentTypeId
                               ,NULL AS GradeBookHoursRequired
                               ,NULL AS GradeBookHoursRequired
                               ,SE.StuEnrollId
                               ,NULL AS MinResult
                               ,NULL AS GradeComponentDescription
                               ,NULL AS CreditsAttempted
                               ,NULL AS CreditsEarned
                               ,NULL AS Completed
                               ,NULL AS CurrentScore
                               ,NULL AS CurrentGrade
                               ,NULL AS FinalScore
                               ,NULL AS FinalGrade
                               ,NULL AS WeightedAverage_GPA
                               ,NULL AS SimpleAverage_GPA
                               ,
--						(Select Sum(Product_WeightedAverage_Credits_GPA)/Sum(Count_WeightedAverage_Credits) from syCreditSummary where StuEnrollId=SE.StuEnrollId) as WeightedAverage_CumGPA,
--						(Select Sum(Product_SimpleAverage_Credits_GPA)/Sum(Count_SimpleAverage_Credits) from syCreditSummary where StuEnrollId=SE.StuEnrollId) as SimpleAverage_CumGPA,
                                ( CASE WHEN (
                                              SELECT    SUM(Count_WeightedAverage_Credits)
                                              FROM      syCreditSummary
                                              WHERE     StuEnrollId = SE.StuEnrollId
                                            ) > 0 THEN (
                                                         SELECT SUM(Product_WeightedAverage_Credits_GPA) / SUM(Count_WeightedAverage_Credits)
                                                         FROM   syCreditSummary
                                                         WHERE  StuEnrollId = SE.StuEnrollId
                                                       )
                                       ELSE 0
                                  END ) AS WeightedAverage_CumGPA
                               ,( CASE WHEN (
                                              SELECT    SUM(Count_SimpleAverage_Credits)
                                              FROM      syCreditSummary
                                              WHERE     StuEnrollId = SE.StuEnrollId
                                            ) > 0 THEN (
                                                         SELECT SUM(Product_SimpleAverage_Credits_GPA) / SUM(Count_SimpleAverage_Credits)
                                                         FROM   syCreditSummary
                                                         WHERE  StuEnrollId = SE.StuEnrollId
                                                       )
                                       ELSE 0
                                  END ) AS SimpleAverage_CumGPA
                               ,C.CampusId
                               ,C.CampDescrip
                               ,NULL AS rownumber
                               ,S.FirstName AS FirstName
                               ,S.LastName AS LastName
                               ,S.MiddleName
                      FROM      arTransferGrades GBR
                      INNER JOIN arStuEnrollments SE ON GBR.StuEnrollId = SE.StuEnrollId
                      INNER JOIN (
                                   SELECT   StuEnrollId
                                   FROM     @StoreEnrollments
                                 ) t5 ON SE.StuEnrollId = t5.StuEnrollId
                      INNER JOIN (
                                   SELECT   StudentId
                                           ,FirstName
                                           ,LastName
                                           ,MiddleName
                                   FROM     arStudent
                                 ) S ON S.StudentId = SE.StudentId
                      INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                      INNER JOIN syCampuses C ON SE.CampusId = C.CampusId
				--Get Second Level (Brings in all the terms in which student is registered for classes)
                      UNION
                      SELECT DISTINCT
                                2
                               ,1
                               ,PV.PrgVerId
                               ,PV.PrgVerDescrip
                               ,NULL
                               ,CS.TermId
                               ,T.TermDescrip
                               ,T.StartDate
                               ,T.EndDate
                               ,NULL
                               ,NULL
                               ,NULL
                               ,NULL
                               ,NULL
                               ,NULL
                               ,NULL
                               ,NULL
                               ,NULL
                               ,NULL
                               ,NULL
                               ,NULL
                               ,NULL
                               ,NULL
                               ,NULL
                               ,NULL
                               ,NULL
                               ,NULL
                               ,SE.StuEnrollId
                               ,NULL AS MinResult
                               ,NULL AS GradeComponentDescription
                               , -- Student data  
                                NULL AS CreditsAttempted
                               ,NULL AS CreditsEarned
                               ,NULL AS Completed
                               ,NULL AS CurrentScore
                               ,NULL AS CurrentGrade
                               ,NULL AS FinalScore
                               ,NULL AS FinalGrade
                               ,( CASE WHEN (
                                              SELECT    SUM(Count_WeightedAverage_Credits)
                                              FROM      syCreditSummary
                                              WHERE     StuEnrollId = SE.StuEnrollId
                                                        AND TermId = T.TermId
                                            ) > 0 THEN (
                                                         SELECT SUM(Product_WeightedAverage_Credits_GPA) / SUM(Count_WeightedAverage_Credits)
                                                         FROM   syCreditSummary
                                                         WHERE  StuEnrollId = SE.StuEnrollId
                                                                AND TermId = T.TermId
                                                       )
                                       ELSE 0
                                  END ) AS WeightedAverage_GPA
                               ,( CASE WHEN (
                                              SELECT    SUM(Count_SimpleAverage_Credits)
                                              FROM      syCreditSummary
                                              WHERE     StuEnrollId = SE.StuEnrollId
                                                        AND TermId = T.TermId
                                            ) > 0 THEN (
                                                         SELECT SUM(Product_SimpleAverage_Credits_GPA) / SUM(Count_SimpleAverage_Credits)
                                                         FROM   syCreditSummary
                                                         WHERE  StuEnrollId = SE.StuEnrollId
                                                                AND TermId = T.TermId
                                                       )
                                       ELSE 0
                                  END ) AS SimpleAverage_GPA
                               ,
--						(Case When (Select Sum(Count_WeightedAverage_Credits) from syCreditSummary where StuEnrollId=SE.StuEnrollId AND TermId=T.TermId)>0 THEN
--						 (Select Sum(Product_WeightedAverage_Credits_GPA)/Sum(Count_WeightedAverage_Credits) 
--							from syCreditSummary where StuEnrollId=SE.StuEnrollId AND TermId=T.TermId)
--						 Else
--							0 
--						 End) as WeightedAverage_CumGPA,
--						(Case When (Select Sum(Count_SimpleAverage_Credits) from syCreditSummary where StuEnrollId=SE.StuEnrollId and TermId=T.TermId)>0 THEN
--							 (Select Sum(Product_SimpleAverage_Credits_GPA)/Sum(Count_SimpleAverage_Credits) 
--							 from syCreditSummary where StuEnrollId=SE.StuEnrollId and TermId=T.TermId) 
--							Else 0 End) as SimpleAverage_CumGPA,
                                NULL
                               ,NULL
                               ,C.CampusId
                               ,C.CampDescrip
                               ,NULL AS rownumber
                               ,S.FirstName AS FirstName
                               ,S.LastName AS LastName
                               ,S.MiddleName
                      FROM      arClassSections CS
                      INNER JOIN arResults GBR ON CS.ClsSectionId = GBR.TestId
                      INNER JOIN arStuEnrollments SE ON GBR.StuEnrollId = SE.StuEnrollId
                      INNER JOIN (
                                   SELECT   StuEnrollId
                                   FROM     @StoreEnrollments
                                 ) t5 ON SE.StuEnrollId = t5.StuEnrollId
                      INNER JOIN (
                                   SELECT   StudentId
                                           ,FirstName
                                           ,LastName
                                           ,MiddleName
                                   FROM     arStudent
                                 ) S ON S.StudentId = SE.StudentId
                      INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                      INNER JOIN arTerm T ON CS.TermId = T.TermId
                      INNER JOIN syCampuses C ON SE.CampusId = C.CampusId
                      UNION
                      SELECT DISTINCT
                                2
                               ,1
                               ,PV.PrgVerId
                               ,PV.PrgVerDescrip
                               ,NULL
                               ,GBR.TermId
                               ,T.TermDescrip
                               ,T.StartDate
                               ,T.EndDate
                               ,NULL
                               ,NULL
                               ,NULL
                               ,NULL
                               ,NULL
                               ,NULL
                               ,NULL
                               ,NULL
                               ,NULL
                               ,NULL
                               ,NULL
                               ,NULL
                               ,NULL
                               ,NULL
                               ,NULL
                               ,NULL
                               ,NULL
                               ,NULL
                               ,SE.StuEnrollId
                               ,NULL AS MinResult
                               ,NULL AS GradeComponentDescription -- Student data    
                               ,NULL AS CreditsAttempted
                               ,NULL AS CreditsEarned
                               ,NULL AS Completed
                               ,NULL AS CurrentScore
                               ,NULL AS CurrentGrade
                               ,NULL AS FinalScore
                               ,NULL AS FinalGrade
                               ,
--						(Select Sum(Product_WeightedAverage_Credits_GPA)/Sum(Count_WeightedAverage_Credits) from syCreditSummary where StuEnrollId=SE.StuEnrollId and 
--						 TermId=T.TermId) as WeightedAverage_GPA,
--						(Select Sum(Product_SimpleAverage_Credits_GPA)/Sum(Count_SimpleAverage_Credits) from syCreditSummary where StuEnrollId=SE.StuEnrollId and 
--						 TermId=T.TermId) as SimpleAverage_GPA,
--						(Case When (Select Sum(Count_WeightedAverage_Credits) from syCreditSummary where StuEnrollId=SE.StuEnrollId AND TermId=T.TermId)>0 THEN
--						 (Select Sum(Product_WeightedAverage_Credits_GPA)/Sum(Count_WeightedAverage_Credits) 
--							from syCreditSummary where StuEnrollId=SE.StuEnrollId AND TermId=T.TermId)
--						 Else
--							0 
--						 End) as WeightedAverage_CumGPA,
--						(Case When (Select Sum(Count_SimpleAverage_Credits) from syCreditSummary where StuEnrollId=SE.StuEnrollId and TermId=T.TermId)>0 THEN
--							 (Select Sum(Product_SimpleAverage_Credits_GPA)/Sum(Count_SimpleAverage_Credits) 
--							 from syCreditSummary where StuEnrollId=SE.StuEnrollId and TermId=T.TermId) 
--							Else 0 End) as SimpleAverage_CumGPA,
                                ( CASE WHEN (
                                              SELECT    SUM(Count_WeightedAverage_Credits)
                                              FROM      syCreditSummary
                                              WHERE     StuEnrollId = SE.StuEnrollId
                                                        AND TermId = T.TermId
                                            ) > 0 THEN (
                                                         SELECT SUM(Product_WeightedAverage_Credits_GPA) / SUM(Count_WeightedAverage_Credits)
                                                         FROM   syCreditSummary
                                                         WHERE  StuEnrollId = SE.StuEnrollId
                                                                AND TermId = T.TermId
                                                       )
                                       ELSE 0
                                  END ) AS WeightedAverage_GPA
                               ,( CASE WHEN (
                                              SELECT    SUM(Count_SimpleAverage_Credits)
                                              FROM      syCreditSummary
                                              WHERE     StuEnrollId = SE.StuEnrollId
                                                        AND TermId = T.TermId
                                            ) > 0 THEN (
                                                         SELECT SUM(Product_SimpleAverage_Credits_GPA) / SUM(Count_SimpleAverage_Credits)
                                                         FROM   syCreditSummary
                                                         WHERE  StuEnrollId = SE.StuEnrollId
                                                                AND TermId = T.TermId
                                                       )
                                       ELSE 0
                                  END ) AS SimpleAverage_GPA
                               ,NULL
                               ,NULL
                               ,C.CampusId
                               ,C.CampDescrip
                               ,NULL AS rownumber
                               ,S.FirstName AS FirstName
                               ,S.LastName AS LastName
                               ,S.MiddleName
                      FROM      arTransferGrades GBR
                      INNER JOIN arStuEnrollments SE ON GBR.StuEnrollId = SE.StuEnrollId
                      INNER JOIN (
                                   SELECT   StuEnrollId
                                   FROM     @StoreEnrollments
                                 ) t5 ON SE.StuEnrollId = t5.StuEnrollId
                      INNER JOIN (
                                   SELECT   StudentId
                                           ,FirstName
                                           ,LastName
                                           ,MiddleName
                                   FROM     arStudent
                                 ) S ON S.StudentId = SE.StudentId
                      INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                      INNER JOIN arTerm T ON GBR.TermId = T.TermId
                      INNER JOIN syCampuses C ON SE.CampusId = C.CampusId
				-- Level 3 (Returns all courses tied to a term)
                      UNION
                      SELECT DISTINCT
                                3 AS Tag
                               ,2
                               ,PV.PrgVerId
                               ,PV.PrgVerDescrip
                               ,NULL
                               ,T.TermId
                               ,T.TermDescrip AS TermDescription
                               ,T.StartDate AS TermStartDate
                               ,T.EndDate
                               ,CS.ReqId
                               ,R.Descrip
                               ,'(' + R.Code + ')' + R.Descrip AS CourseCodeDescrip
                               ,R.Credits AS Credits
                               ,R.FinAidCredits AS FinAidCredits
                               ,(
                                  SELECT    MIN(MinVal)
                                  FROM      arGradeScaleDetails GCD
                                           ,arGradeSystemDetails GSD
                                  WHERE     GCD.GrdSysDetailId = GSD.GrdSysDetailId
                                            AND GSD.IsPass = 1
                                            AND GCD.GrdScaleId = CS.GrdScaleId
                                ) AS MinVal
                               ,RES.Score
                               ,NULL
                               ,NULL
                               ,NULL
                               ,NULL
                               ,NULL
                               ,NULL
                               ,NULL
                               ,NULL
                               ,NULL
                               ,NULL
                               ,NULL
                               ,SE.StuEnrollId
                               ,NULL AS MinResult
                               ,NULL AS GradeComponentDescription -- Student data   
                               ,SCS.CreditsAttempted AS CreditsAttempted
                               ,SCS.CreditsEarned AS CreditsEarned
                               ,SCS.Completed AS Completed
                               ,SCS.CurrentScore AS CurrentScore
                               ,SCS.CurrentGrade AS CurrentGrade
                               ,SCS.FinalScore AS FinalScore
                               ,SCS.FinalGrade AS FinalGrade
                               ,
--						(Case When Sum(Count_WeightedAverage_Credits)>0 Then Select Sum(Product_WeightedAverage_Credits_GPA)/Sum(Count_WeightedAverage_Credits) from syCreditSummary where StuEnrollId=SE.StuEnrollId and 
--						 TermId=T.TermId and ReqId=R.ReqId Else 0 End) as WeightedAverage_GPA,
--						(Case When Sum(Count_SimpleAverage_Credits)>0 Then Select Sum(Product_SimpleAverage_Credits_GPA)/Sum(Count_SimpleAverage_Credits) from syCreditSummary where StuEnrollId=SE.StuEnrollId and 
--						 TermId=T.TermId and ReqId=R.ReqId Else 0 End) as SimpleAverage_GPA,
                                ( CASE WHEN (
                                              SELECT    SUM(Count_WeightedAverage_Credits)
                                              FROM      syCreditSummary
                                              WHERE     StuEnrollId = SE.StuEnrollId
                                                        AND TermId = T.TermId
                                                        AND ReqId = R.ReqId
                                            ) > 0 THEN (
                                                         SELECT SUM(Product_WeightedAverage_Credits_GPA) / SUM(Count_WeightedAverage_Credits)
                                                         FROM   syCreditSummary
                                                         WHERE  StuEnrollId = SE.StuEnrollId
                                                                AND TermId = T.TermId
                                                                AND ReqId = R.ReqId
                                                       )
                                       ELSE 0
                                  END ) AS WeightedAverage_GPA
                               ,( CASE WHEN (
                                              SELECT    SUM(Count_SimpleAverage_Credits)
                                              FROM      syCreditSummary
                                              WHERE     StuEnrollId = SE.StuEnrollId
                                                        AND TermId = T.TermId
                                                        AND ReqId = R.ReqId
                                            ) > 0 THEN (
                                                         SELECT SUM(Product_SimpleAverage_Credits_GPA) / SUM(Count_SimpleAverage_Credits)
                                                         FROM   syCreditSummary
                                                         WHERE  StuEnrollId = SE.StuEnrollId
                                                                AND TermId = T.TermId
                                                                AND ReqId = R.ReqId
                                                       )
                                       ELSE 0
                                  END ) AS SimpleAverage_GPA
                               ,NULL
                               ,NULL
                               ,C.CampusId
                               ,C.CampDescrip
                               ,NULL AS rownumber
                               ,S.FirstName AS FirstName
                               ,S.LastName AS LastName
                               ,S.MiddleName
                      FROM      arClassSections CS
                      INNER JOIN arResults GBR ON CS.ClsSectionId = GBR.TestId
                      INNER JOIN arStuEnrollments SE ON GBR.StuEnrollId = SE.StuEnrollId
                      INNER JOIN (
                                   SELECT   StuEnrollId
                                   FROM     @StoreEnrollments
                                 ) t5 ON SE.StuEnrollId = t5.StuEnrollId
                      INNER JOIN (
                                   SELECT   StudentId
                                           ,FirstName
                                           ,LastName
                                           ,MiddleName
                                   FROM     arStudent
                                 ) S ON S.StudentId = SE.StudentId
                      INNER JOIN syCampuses C ON SE.CampusId = C.CampusId
                      INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                      INNER JOIN arTerm T ON CS.TermId = T.TermId
                      INNER JOIN arReqs R ON CS.ReqId = R.ReqId
                      INNER JOIN arResults RES ON RES.StuEnrollId = GBR.StuEnrollId
                                                  AND RES.TestId = CS.ClsSectionId
                      LEFT JOIN syCreditSummary SCS ON SE.StuEnrollId = SCS.StuEnrollId
                                                       AND T.TermId = SCS.TermId
                                                       AND R.ReqId = SCS.ReqId
                      UNION
                      SELECT DISTINCT
                                3
                               ,2
                               ,PV.PrgVerId
                               ,PV.PrgVerDescrip
                               ,NULL
                               ,T.TermId
                               ,T.TermDescrip
                               ,T.StartDate
                               ,T.EndDate
                               ,GBCR.ReqId
                               ,R.Descrip AS CourseDescrip
                               ,'(' + R.Code + ' ) ' + R.Descrip AS CourseCodeDescrip
                               ,R.Credits
                               ,R.FinAidCredits
                               ,(
                                  SELECT    MIN(MinVal)
                                  FROM      arGradeScaleDetails GCD
                                           ,arGradeSystemDetails GSD
                                  WHERE     GCD.GrdSysDetailId = GSD.GrdSysDetailId
                                            AND GSD.IsPass = 1
                                )
                               ,AR.Score
                               ,NULL
                               ,NULL
                               ,NULL
                               ,NULL
                               ,NULL
                               ,NULL
                               ,NULL
                               ,NULL
                               ,NULL
                               ,NULL
                               ,NULL
                               ,SE.StuEnrollId
                               ,NULL AS MinResult
                               ,NULL AS GradeComponentDescription -- Student data    
                               ,SCS.CreditsAttempted AS CreditsAttempted
                               ,SCS.CreditsEarned AS CreditsEarned
                               ,SCS.Completed AS Completed
                               ,SCS.CurrentScore AS CurrentScore
                               ,SCS.CurrentGrade AS CurrentGrade
                               ,SCS.FinalScore AS FinalScore
                               ,SCS.FinalGrade AS FinalGrade
                               ,
--						(Select Sum(Product_WeightedAverage_Credits_GPA)/Sum(Count_WeightedAverage_Credits) from syCreditSummary where StuEnrollId=SE.StuEnrollId and 
--						 TermId=T.TermId and ReqId=R.ReqId) as WeightedAverage_GPA,
--						(Select Sum(Product_SimpleAverage_Credits_GPA)/Sum(Count_SimpleAverage_Credits) from syCreditSummary where StuEnrollId=SE.StuEnrollId and 
--						 TermId=T.TermId and ReqId=R.ReqId) as SimpleAverage_GPA,
                                ( CASE WHEN (
                                              SELECT    SUM(Count_WeightedAverage_Credits)
                                              FROM      syCreditSummary
                                              WHERE     StuEnrollId = SE.StuEnrollId
                                                        AND TermId = T.TermId
                                                        AND ReqId = R.ReqId
                                            ) > 0 THEN (
                                                         SELECT SUM(Product_WeightedAverage_Credits_GPA) / SUM(Count_WeightedAverage_Credits)
                                                         FROM   syCreditSummary
                                                         WHERE  StuEnrollId = SE.StuEnrollId
                                                                AND TermId = T.TermId
                                                                AND ReqId = R.ReqId
                                                       )
                                       ELSE 0
                                  END ) AS WeightedAverage_GPA
                               ,( CASE WHEN (
                                              SELECT    SUM(Count_SimpleAverage_Credits)
                                              FROM      syCreditSummary
                                              WHERE     StuEnrollId = SE.StuEnrollId
                                                        AND TermId = T.TermId
                                                        AND ReqId = R.ReqId
                                            ) > 0 THEN (
                                                         SELECT SUM(Product_SimpleAverage_Credits_GPA) / SUM(Count_SimpleAverage_Credits)
                                                         FROM   syCreditSummary
                                                         WHERE  StuEnrollId = SE.StuEnrollId
                                                                AND TermId = T.TermId
                                                                AND ReqId = R.ReqId
                                                       )
                                       ELSE 0
                                  END ) AS SimpleAverage_GPA
                               ,NULL
                               ,NULL
                               ,C.CampusId
                               ,C.CampDescrip
                               ,NULL AS rownumber
                               ,S.FirstName AS FirstName
                               ,S.LastName AS LastName
                               ,S.MiddleName
                      FROM      arTransferGrades GBCR
                      INNER JOIN arStuEnrollments SE ON GBCR.StuEnrollId = SE.StuEnrollId
                      INNER JOIN (
                                   SELECT   StuEnrollId
                                   FROM     @StoreEnrollments
                                 ) t5 ON SE.StuEnrollId = t5.StuEnrollId
                      INNER JOIN (
                                   SELECT   StudentId
                                           ,FirstName
                                           ,LastName
                                           ,MiddleName
                                   FROM     arStudent
                                 ) S ON S.StudentId = SE.StudentId
                      INNER JOIN syCampuses C ON SE.CampusId = C.CampusId
                      INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                      INNER JOIN arTerm T ON GBCR.TermId = T.TermId
                      INNER JOIN arReqs R ON GBCR.ReqId = R.ReqId
                      INNER JOIN arResults AR ON GBCR.StuEnrollId = AR.StuEnrollId
                      LEFT JOIN syCreditSummary SCS ON SE.StuEnrollId = SCS.StuEnrollId
                                                       AND T.TermId = SCS.TermId
                                                       AND R.ReqId = SCS.ReqId
                      UNION
                      SELECT    4 AS Tag
                               ,3
                               ,PV.PrgVerId
                               ,PV.PrgVerDescrip
                               ,NULL
                               ,T.TermId
                               ,T.TermDescrip
                               ,T.StartDate AS TermStartDate
                               ,T.EndDate AS TermEndDate
                               ,R.ReqId
                               ,R.Descrip
                               ,'(' + R.Code + ' ) ' + R.Descrip AS CourseCodeDescrip
                               ,NULL
                               ,NULL
                               ,NULL
                               ,NULL
                               ,GBR.GrdBkResultId
                               ,RTRIM(GCT.Descrip) AS GradeBookDescription
                               ,( CASE GCT.SysComponentTypeId
                                    WHEN 544 THEN (
                                                    SELECT  SUM(HoursAttended)
                                                    FROM    arExternshipAttendance
                                                    WHERE   StuEnrollId = SE.StuEnrollId
                                                  )
                                    ELSE GBR.Score
                                  END ) AS GradeBookResult
                               ,NULL
                               ,NULL
                               ,NULL
                               ,NULL
                               ,NULL
                               ,GCT.SysComponentTypeId
                               ,NULL
                               ,NULL
                               ,SE.StuEnrollId
                               ,( CASE WHEN GCT.SysComponentTypeId IN ( 500,503,504 ) THEN GBWD.Number
                                       ELSE (
                                              SELECT    MIN(MinVal)
                                              FROM      arGradeScaleDetails GSD
                                                       ,arGradeSystemDetails GSS
                                              WHERE     GSD.GrdSysDetailId = GSS.GrdSysDetailId
                                                        AND GSS.IsPass = 1
                                            )
                                  END ) AS MinResult
                               ,SYRES.Resource AS GradeComponentDescription
                               ,NULL AS CreditsAttempted
                               ,NULL AS CreditsEarned
                               ,NULL AS Completed
                               ,NULL AS CurrentScore
                               ,NULL AS CurrentGrade
                               ,NULL AS FinalScore
                               ,NULL AS FinalGrade
                               ,NULL AS WeightedAverage_GPA
                               ,NULL AS SimpleAverage_GPA
                               ,NULL
                               ,NULL
                               ,C.CampusId
                               ,C.CampDescrip
                               ,ROW_NUMBER() OVER ( PARTITION BY SE.StuEnrollId,R.ReqId ORDER BY C.CampDescrip, PV.PrgVerDescrip, T.StartDate, T.EndDate, T.TermId, T.TermDescrip, R.ReqId, R.Descrip, GCT.SysComponentTypeId, GCT.Descrip ) AS RowNumber
                               ,S.FirstName AS FirstName
                               ,S.LastName AS LastName
                               ,S.MiddleName
                      FROM      arGrdBkResults GBR
                      INNER JOIN arStuEnrollments SE ON GBR.StuEnrollId = SE.StuEnrollId
                      INNER JOIN (
                                   SELECT   StuEnrollId
                                   FROM     @StoreEnrollments
                                 ) t5 ON SE.StuEnrollId = t5.StuEnrollId
                      INNER JOIN (
                                   SELECT   StudentId
                                           ,FirstName
                                           ,LastName
                                           ,MiddleName
                                   FROM     arStudent
                                 ) S ON S.StudentId = SE.StudentId
                      INNER JOIN arClassSections CS ON CS.ClsSectionId = GBR.ClsSectionId
                      INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                      INNER JOIN arTerm T ON CS.TermId = T.TermId
                      INNER JOIN arReqs R ON CS.ReqId = R.ReqId
                      INNER JOIN arResults RES ON RES.StuEnrollId = GBR.StuEnrollId
                                                  AND RES.TestId = CS.ClsSectionId
                      INNER JOIN arGrdBkWgtDetails GBWD ON GBR.InstrGrdBkWgtDetailId = GBWD.InstrGrdBkWgtDetailId
                      INNER JOIN arGrdComponentTypes GCT ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
--						INNER JOIN 
--							(Case when @ShowWorkUnitGrouping=1 
--								Then 
--									(select * from arGrdComponentTypes where GrdComponentTypeId=@SysComponentTypeId) 
--								Else
--									arGrdComponentTypes 
--								End
--							) GCT on GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
                      INNER JOIN (
                                   SELECT   Resource
                                           ,ResourceID
                                   FROM     syResources
                                   WHERE    ResourceTypeID = 10
                                 ) SYRES ON SYRES.ResourceID = GCT.SysComponentTypeId
                      INNER JOIN syCampuses C ON SE.CampusId = C.CampusId
                      WHERE     (
                                  @SysComponentTypeId IS NULL
                                  OR GCT.SysComponentTypeId IN ( SELECT Val
                                                                 FROM   MultipleValuesForReportParameters(@SysComponentTypeId,',',1) )
                                )
                      UNION
                      SELECT    4 AS Tag
                               ,3
                               ,PV.PrgVerId
                               ,PV.PrgVerDescrip
                               ,NULL
                               ,T.TermId
                               ,T.TermDescrip
                               ,T.StartDate AS termStartdate
                               ,T.EndDate AS TermEndDate
                               ,GBCR.ReqId
                               ,R.Descrip AS CourseDescrip
                               ,'(' + R.Code + ' ) ' + R.Descrip AS CourseCodeDescrip
                               ,NULL
                               ,NULL
                               ,NULL
                               ,NULL
                               ,GBCR.ConversionResultId AS GrdBkResultId
                               ,RTRIM(GCT.Descrip) AS Comments
                               ,GBCR.Score AS GradeBookResult
                               ,NULL
                               ,NULL
                               ,NULL
                               ,NULL
                               ,NULL
                               ,GCT.SysComponentTypeId
                               ,NULL
                               ,NULL
                               ,SE.StuEnrollId
                               ,GBCR.MinResult
                               ,SYRES.Resource -- Student data  
                               ,NULL AS CreditsAttempted
                               ,NULL AS CreditsEarned
                               ,NULL AS Completed
                               ,NULL AS CurrentScore
                               ,NULL AS CurrentGrade
                               ,NULL AS FinalScore
                               ,NULL AS FinalGrade
                               ,NULL AS WeightedAverage_GPA
                               ,NULL AS SimpleAverage_GPA
                               ,NULL
                               ,NULL
                               ,C.CampusId
                               ,C.CampDescrip
                               ,ROW_NUMBER() OVER ( PARTITION BY SE.StuEnrollId,R.ReqId ORDER BY C.CampDescrip, PV.PrgVerDescrip, T.StartDate, T.EndDate, T.TermId, T.TermDescrip, R.ReqId, R.Descrip, GCT.SysComponentTypeId, GCT.Descrip ) AS RowNumber
                               ,S.FirstName AS FirstName
                               ,S.LastName AS LastName
                               ,S.MiddleName
                      FROM      arGrdBkConversionResults GBCR
                      INNER JOIN arStuEnrollments SE ON GBCR.StuEnrollId = SE.StuEnrollId
                      INNER JOIN (
                                   SELECT   StuEnrollId
                                   FROM     @StoreEnrollments
                                 ) t5 ON SE.StuEnrollId = t5.StuEnrollId
                      INNER JOIN (
                                   SELECT   StudentId
                                           ,FirstName
                                           ,LastName
                                           ,MiddleName
                                   FROM     arStudent
                                 ) S ON S.StudentId = SE.StudentId
                      INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                      INNER JOIN arTerm T ON GBCR.TermId = T.TermId
                      INNER JOIN arReqs R ON GBCR.ReqId = R.ReqId
                      INNER JOIN arGrdComponentTypes GCT ON GCT.GrdComponentTypeId = GBCR.GrdComponentTypeId
                      INNER JOIN arGrdBkWgtDetails GBWD ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
                                                           AND GBWD.GrdComponentTypeId = GBCR.GrdComponentTypeId
                      INNER JOIN arGrdBkWeights GBW ON GBWD.InstrGrdBkWgtId = GBW.InstrGrdBkWgtId
                                                       AND GBCR.ReqId = GBW.ReqId
                      INNER JOIN (
                                   SELECT   ReqId
                                           ,MAX(EffectiveDate) AS EffectiveDate
                                   FROM     arGrdBkWeights
                                   GROUP BY ReqId
                                 ) AS MaxEffectiveDatesByCourse ON GBCR.ReqId = MaxEffectiveDatesByCourse.ReqId
                      INNER JOIN (
                                   SELECT   Resource
                                           ,ResourceID
                                   FROM     syResources
                                   WHERE    ResourceTypeID = 10
                                 ) SYRES ON SYRES.ResourceID = GCT.SysComponentTypeId
                      INNER JOIN syCampuses C ON SE.CampusId = C.CampusId
                      WHERE     MaxEffectiveDatesByCourse.EffectiveDate <= T.StartDate
                                AND (
                                      @SysComponentTypeId IS NULL
                                      OR GCT.SysComponentTypeId IN ( SELECT Val
                                                                     FROM   MultipleValuesForReportParameters(@SysComponentTypeId,',',1) )
                                    )
                    ) dt
            ORDER BY Tag
                   ,CampDescrip
                   ,CASE WHEN @OrderBy = 'LastName Asc,FirstName Asc,MiddleName Asc' THEN dt.LastName
                    END
                   ,CASE WHEN @OrderBy = 'LastName Asc,FirstName Asc,MiddleName Asc' THEN dt.FirstName
                    END
                   ,CASE WHEN @OrderBy = 'LastName Asc,FirstName Asc,MiddleName Asc' THEN dt.MiddleName
                    END
                   ,CASE WHEN @OrderBy = 'LastName Asc,FirstName Asc,MiddleName Desc' THEN dt.LastName
                    END
                   ,CASE WHEN @OrderBy = 'LastName Asc,FirstName Asc,MiddleName Desc' THEN dt.FirstName
                    END
                   ,CASE WHEN @OrderBy = 'LastName Asc,FirstName Asc,MiddleName Desc' THEN dt.MiddleName
                    END DESC
                   ,CASE WHEN @OrderBy = 'LastName Asc,FirstNameDesc,MiddleName Asc' THEN dt.LastName
                    END
                   ,CASE WHEN @OrderBy = 'LastName Asc,FirstNameDesc,MiddleName Asc' THEN dt.FirstName
                    END DESC
                   ,CASE WHEN @OrderBy = 'LastName Asc,FirstNameDesc,MiddleName Asc' THEN dt.MiddleName
                    END
                   ,CASE WHEN @OrderBy = 'LastName Asc,FirstNameDesc,MiddleName Desc' THEN dt.LastName
                    END
                   ,CASE WHEN @OrderBy = 'LastName Asc,FirstNameDesc,MiddleName Desc' THEN dt.FirstName
                    END DESC
                   ,CASE WHEN @OrderBy = 'LastName Asc,FirstNameDesc,MiddleName Desc' THEN dt.MiddleName
                    END DESC
                   ,
			-- LD
                    CASE WHEN @OrderBy = 'LastName Desc,FirstName Asc,MiddleName Asc' THEN dt.LastName
                    END DESC
                   ,CASE WHEN @OrderBy = 'LastName Desc,FirstName Asc,MiddleName Asc' THEN dt.FirstName
                    END
                   ,CASE WHEN @OrderBy = 'LastName Desc,FirstName Asc,MiddleName Asc' THEN dt.MiddleName
                    END
                   ,CASE WHEN @OrderBy = 'LastName Desc,FirstName Asc,MiddleName Desc' THEN dt.LastName
                    END DESC
                   ,CASE WHEN @OrderBy = 'LastName Desc,FirstName Asc,MiddleName Desc' THEN dt.FirstName
                    END
                   ,CASE WHEN @OrderBy = 'LastName Desc,FirstName Asc,MiddleName Desc' THEN dt.MiddleName
                    END DESC
                   ,CASE WHEN @OrderBy = 'LastName Desc,FirstName Desc,MiddleName Asc' THEN dt.LastName
                    END DESC
                   ,CASE WHEN @OrderBy = 'LastName Desc,FirstName Desc,MiddleName Asc' THEN dt.FirstName
                    END DESC
                   ,CASE WHEN @OrderBy = 'LastName Desc,FirstName Desc,MiddleName Asc' THEN dt.MiddleName
                    END
                   ,CASE WHEN @OrderBy = 'LastName Desc,FirstName Desc,MiddleName Desc' THEN dt.LastName
                    END DESC
                   ,CASE WHEN @OrderBy = 'LastName Desc,FirstName Desc,MiddleName Desc' THEN dt.FirstName
                    END DESC
                   ,CASE WHEN @OrderBy = 'LastName Desc,FirstName Desc,MiddleName Desc' THEN dt.MiddleName
                    END DESC
                   ,CASE WHEN @OrderBy = 'LastName Asc,MiddleName Asc,FirstName Asc' THEN dt.LastName
                    END
                   ,CASE WHEN @OrderBy = 'LastName Asc,MiddleName Asc,FirstName Asc' THEN dt.MiddleName
                    END
                   ,CASE WHEN @OrderBy = 'LastName Asc,MiddleName Asc,FirstName Asc' THEN dt.FirstName
                    END
                   ,CASE WHEN @OrderBy = 'LastName Asc,MiddleName Asc,FirstName Desc' THEN dt.LastName
                    END
                   ,CASE WHEN @OrderBy = 'LastName Asc,MiddleName Asc,FirstName Desc' THEN dt.MiddleName
                    END
                   ,CASE WHEN @OrderBy = 'LastName Asc,MiddleName Asc,FirstName Desc' THEN dt.FirstName
                    END DESC
                   ,CASE WHEN @OrderBy = 'LastName Asc,MiddleName Desc,FirstName Asc' THEN dt.LastName
                    END
                   ,CASE WHEN @OrderBy = 'LastName Asc,MiddleName Desc,FirstName Asc' THEN dt.MiddleName
                    END DESC
                   ,CASE WHEN @OrderBy = 'LastName Asc,MiddleName Desc,FirstName Asc' THEN dt.FirstName
                    END
                   ,CASE WHEN @OrderBy = 'LastName Asc,MiddleName Desc,FirstName Desc' THEN dt.LastName
                    END
                   ,CASE WHEN @OrderBy = 'LastName Asc,MiddleName Desc,FirstName Desc' THEN dt.MiddleName
                    END DESC
                   ,CASE WHEN @OrderBy = 'LastName Asc,MiddleName Desc,FirstName Desc' THEN dt.FirstName
                    END DESC
                   ,CASE WHEN @OrderBy = 'LastName Desc,MiddleName Asc,FirstName Asc' THEN dt.LastName
                    END DESC
                   ,CASE WHEN @OrderBy = 'LastName Desc,MiddleName Asc,FirstName Asc' THEN dt.MiddleName
                    END
                   ,CASE WHEN @OrderBy = 'LastName Desc,MiddleName Asc,FirstName Asc' THEN dt.FirstName
                    END
                   ,CASE WHEN @OrderBy = 'LastName Desc,MiddleName Asc,FirstName Desc' THEN dt.LastName
                    END DESC
                   ,CASE WHEN @OrderBy = 'LastName Desc,MiddleName Asc,FirstName Desc' THEN dt.MiddleName
                    END
                   ,CASE WHEN @OrderBy = 'LastName Desc,MiddleName Asc,FirstName Desc' THEN dt.FirstName
                    END DESC
                   ,CASE WHEN @OrderBy = 'LastName Desc,MiddleName Desc,FirstName Asc' THEN dt.LastName
                    END DESC
                   ,CASE WHEN @OrderBy = 'LastName Desc,MiddleName Desc,FirstName Asc' THEN dt.MiddleName
                    END DESC
                   ,CASE WHEN @OrderBy = 'LastName Desc,MiddleName Desc,FirstName Asc' THEN dt.FirstName
                    END
                   ,CASE WHEN @OrderBy = 'LastName Desc,MiddleName Desc,FirstName Desc' THEN dt.LastName
                    END DESC
                   ,CASE WHEN @OrderBy = 'LastName Desc,MiddleName Desc,FirstName Desc' THEN dt.MiddleName
                    END DESC
                   ,CASE WHEN @OrderBy = 'LastName Desc,MiddleName Desc,FirstName Desc' THEN dt.FirstName
                    END DESC
                   ,CASE WHEN @OrderBy = 'FirstName Asc,LastName Asc,MiddleName Asc' THEN dt.FirstName
                    END
                   ,CASE WHEN @OrderBy = 'FirstName Asc,LastName Asc,MiddleName Asc' THEN dt.LastName
                    END
                   ,CASE WHEN @OrderBy = 'FirstName Asc,LastName Asc,MiddleName Asc' THEN dt.MiddleName
                    END
                   ,CASE WHEN @OrderBy = 'FirstName Asc,LastName Asc,MiddleName Desc' THEN dt.FirstName
                    END
                   ,CASE WHEN @OrderBy = 'FirstName Asc,LastName Asc,MiddleName Desc' THEN dt.LastName
                    END
                   ,CASE WHEN @OrderBy = 'FirstName Asc,LastName Asc,MiddleName Desc' THEN dt.MiddleName
                    END DESC
                   ,CASE WHEN @OrderBy = 'FirstName Asc,LastName Desc,MiddleName Asc' THEN dt.FirstName
                    END
                   ,CASE WHEN @OrderBy = 'FirstName Asc,LastName Desc,MiddleName Asc' THEN dt.LastName
                    END DESC
                   ,CASE WHEN @OrderBy = 'FirstName Asc,LastName Desc,MiddleName Asc' THEN dt.MiddleName
                    END
                   ,CASE WHEN @OrderBy = 'FirstName Asc,LastName Desc,MiddleName Desc' THEN dt.FirstName
                    END
                   ,CASE WHEN @OrderBy = 'FirstName Asc,LastName Desc,MiddleName Desc' THEN dt.LastName
                    END DESC
                   ,CASE WHEN @OrderBy = 'FirstName Asc,LastName Desc,MiddleName Desc' THEN dt.MiddleName
                    END DESC
                   ,
			-- LD
                    CASE WHEN @OrderBy = 'FirstName Desc,LastName Asc,MiddleName Asc' THEN dt.FirstName
                    END DESC
                   ,CASE WHEN @OrderBy = 'FirstName Desc,LastName Asc,MiddleName Asc' THEN dt.LastName
                    END
                   ,CASE WHEN @OrderBy = 'FirstName Desc,LastName Asc,MiddleName Asc' THEN dt.MiddleName
                    END
                   ,CASE WHEN @OrderBy = 'FirstName Desc,LastName Asc,MiddleName Desc' THEN dt.FirstName
                    END DESC
                   ,CASE WHEN @OrderBy = 'FirstName Desc,LastName Asc,MiddleName Desc' THEN dt.LastName
                    END
                   ,CASE WHEN @OrderBy = 'FirstName Desc,LastName Asc,MiddleName Desc' THEN dt.MiddleName
                    END DESC
                   ,CASE WHEN @OrderBy = 'FirstName Desc,LastName Desc,MiddleName Asc' THEN dt.FirstName
                    END DESC
                   ,CASE WHEN @OrderBy = 'FirstName Desc,LastName Desc,MiddleName Asc' THEN dt.LastName
                    END DESC
                   ,CASE WHEN @OrderBy = 'FirstName Desc,LastName Desc,MiddleName Asc' THEN dt.MiddleName
                    END
                   ,CASE WHEN @OrderBy = 'FirstName Desc,LastName Desc,MiddleName Desc' THEN dt.FirstName
                    END DESC
                   ,CASE WHEN @OrderBy = 'FirstName Desc,LastName Desc,MiddleName Desc' THEN dt.LastName
                    END DESC
                   ,CASE WHEN @OrderBy = 'FirstName Desc,LastName Desc,MiddleName Desc' THEN dt.MiddleName
                    END DESC
                   ,CASE WHEN @OrderBy = 'FirstName Asc,MiddleName Asc,LastName Asc' THEN dt.FirstName
                    END
                   ,CASE WHEN @OrderBy = 'FirstName Asc,MiddleName Asc,LastName Asc' THEN dt.MiddleName
                    END
                   ,CASE WHEN @OrderBy = 'FirstName Asc,MiddleName Asc,LastName Asc' THEN dt.LastName
                    END
                   ,CASE WHEN @OrderBy = 'FirstName Asc,MiddleName Asc,LastName Desc' THEN dt.FirstName
                    END
                   ,CASE WHEN @OrderBy = 'FirstName Asc,MiddleName Asc,LastName Desc' THEN dt.MiddleName
                    END
                   ,CASE WHEN @OrderBy = 'FirstName Asc,MiddleName Asc,LastName Desc' THEN dt.LastName
                    END DESC
                   ,CASE WHEN @OrderBy = 'FirstName Asc,MiddleName Desc,LastName Asc' THEN dt.FirstName
                    END
                   ,CASE WHEN @OrderBy = 'FirstName Asc,MiddleName Desc,LastName Asc' THEN dt.MiddleName
                    END DESC
                   ,CASE WHEN @OrderBy = 'FirstName Asc,MiddleName Desc,LastName Asc' THEN dt.LastName
                    END
                   ,CASE WHEN @OrderBy = 'FirstName Asc,MiddleName Desc,LastName Desc' THEN dt.FirstName
                    END
                   ,CASE WHEN @OrderBy = 'FirstName Asc,MiddleName Desc,LastName Desc' THEN dt.MiddleName
                    END DESC
                   ,CASE WHEN @OrderBy = 'FirstName Asc,MiddleName Desc,LastName Desc' THEN dt.LastName
                    END DESC
                   ,CASE WHEN @OrderBy = 'FirstName Desc,MiddleName Asc,LastName Asc' THEN dt.FirstName
                    END DESC
                   ,CASE WHEN @OrderBy = 'FirstName Desc,MiddleName Asc,LastName Asc' THEN dt.MiddleName
                    END
                   ,CASE WHEN @OrderBy = 'FirstName Desc,MiddleName Asc,LastName Asc' THEN dt.LastName
                    END
                   ,CASE WHEN @OrderBy = 'FirstName Desc,MiddleName Asc,LastName Desc' THEN dt.FirstName
                    END DESC
                   ,CASE WHEN @OrderBy = 'FirstName Desc,MiddleName Asc,LastName Desc' THEN dt.MiddleName
                    END
                   ,CASE WHEN @OrderBy = 'FirstName Desc,MiddleName Asc,LastName Desc' THEN dt.LastName
                    END DESC
                   ,CASE WHEN @OrderBy = 'FirstName Desc,MiddleName Desc,LastName Asc' THEN dt.FirstName
                    END DESC
                   ,CASE WHEN @OrderBy = 'FirstName Desc,MiddleName Desc,LastName Asc' THEN dt.MiddleName
                    END DESC
                   ,CASE WHEN @OrderBy = 'FirstName Desc,MiddleName Desc,LastName Asc' THEN dt.LastName
                    END
                   ,CASE WHEN @OrderBy = 'FirstName Desc,MiddleName Desc,LastName Desc' THEN dt.FirstName
                    END DESC
                   ,CASE WHEN @OrderBy = 'FirstName Desc,MiddleName Desc,LastName Desc' THEN dt.MiddleName
                    END DESC
                   ,CASE WHEN @OrderBy = 'FirstName Desc,MiddleName Desc,LastName Desc' THEN dt.LastName
                    END DESC
                   ,CASE WHEN @OrderBy = 'MiddleName Asc,LastName Asc,FirstName Asc' THEN dt.MiddleName
                    END
                   ,CASE WHEN @OrderBy = 'MiddleName Asc,LastName Asc,FirstName Asc' THEN dt.LastName
                    END
                   ,CASE WHEN @OrderBy = 'MiddleName Asc,LastName Asc,FirstName Asc' THEN dt.FirstName
                    END
                   ,CASE WHEN @OrderBy = 'MiddleName Asc,LastName Asc,FirstName Desc' THEN dt.MiddleName
                    END
                   ,CASE WHEN @OrderBy = 'MiddleName Asc,LastName Asc,FirstName Desc' THEN dt.LastName
                    END
                   ,CASE WHEN @OrderBy = 'MiddleName Asc,LastName Asc,FirstName Desc' THEN dt.FirstName
                    END DESC
                   ,CASE WHEN @OrderBy = 'MiddleName Asc,LastName Desc,FirstName Asc' THEN dt.MiddleName
                    END
                   ,CASE WHEN @OrderBy = 'MiddleName Asc,LastName Desc,FirstName Asc' THEN dt.LastName
                    END DESC
                   ,CASE WHEN @OrderBy = 'MiddleName Asc,LastName Desc,FirstName Asc' THEN dt.FirstName
                    END
                   ,CASE WHEN @OrderBy = 'MiddleName Asc,LastName Desc,FirstName Desc' THEN dt.MiddleName
                    END
                   ,CASE WHEN @OrderBy = 'MiddleName Asc,LastName Desc,FirstName Desc' THEN dt.LastName
                    END DESC
                   ,CASE WHEN @OrderBy = 'MiddleName Asc,LastName Desc,FirstName Desc' THEN dt.FirstName
                    END DESC
                   ,CASE WHEN @OrderBy = 'MiddleName Desc,LastName Asc,FirstName Asc' THEN dt.MiddleName
                    END DESC
                   ,CASE WHEN @OrderBy = 'MiddleName Desc,LastName Asc,FirstName Asc' THEN dt.LastName
                    END
                   ,CASE WHEN @OrderBy = 'MiddleName Desc,LastName Asc,FirstName Asc' THEN dt.FirstName
                    END
                   ,CASE WHEN @OrderBy = 'MiddleName Desc,LastName Asc,FirstName Desc' THEN dt.MiddleName
                    END DESC
                   ,CASE WHEN @OrderBy = 'MiddleName Desc,LastName Asc,FirstName Desc' THEN dt.LastName
                    END
                   ,CASE WHEN @OrderBy = 'MiddleName Desc,LastName Asc,FirstName Desc' THEN dt.FirstName
                    END DESC
                   ,CASE WHEN @OrderBy = 'MiddleName Desc,LastName Desc,FirstName Asc' THEN dt.MiddleName
                    END DESC
                   ,CASE WHEN @OrderBy = 'MiddleName Desc,LastName Desc,FirstName Asc' THEN dt.LastName
                    END DESC
                   ,CASE WHEN @OrderBy = 'MiddleName Desc,LastName Desc,FirstName Asc' THEN dt.FirstName
                    END
                   ,CASE WHEN @OrderBy = 'MiddleName Desc,LastName Desc,FirstName Desc' THEN dt.MiddleName
                    END DESC
                   ,CASE WHEN @OrderBy = 'MiddleName Desc,LastName Desc,FirstName Desc' THEN dt.LastName
                    END DESC
                   ,CASE WHEN @OrderBy = 'MiddleName Desc,LastName Desc,FirstName Desc' THEN dt.FirstName
                    END DESC
                   ,CASE WHEN @OrderBy = 'MiddleName Asc,FirstName Asc,LastName Asc' THEN dt.MiddleName
                    END
                   ,CASE WHEN @OrderBy = 'MiddleName Asc,FirstName Asc,LastName Asc' THEN dt.FirstName
                    END
                   ,CASE WHEN @OrderBy = 'MiddleName Asc,FirstName Asc,LastName Asc' THEN dt.LastName
                    END
                   ,CASE WHEN @OrderBy = 'MiddleName Asc,FirstName Asc,LastName Desc' THEN dt.MiddleName
                    END
                   ,CASE WHEN @OrderBy = 'MiddleName Asc,FirstName Asc,LastName Desc' THEN dt.FirstName
                    END
                   ,CASE WHEN @OrderBy = 'MiddleName Asc,FirstName Asc,LastName Desc' THEN dt.LastName
                    END DESC
                   ,CASE WHEN @OrderBy = 'MiddleName Asc,FirstName Desc,LastName Asc' THEN dt.MiddleName
                    END
                   ,CASE WHEN @OrderBy = 'MiddleName Asc,FirstName Desc,LastName Asc' THEN dt.FirstName
                    END DESC
                   ,CASE WHEN @OrderBy = 'MiddleName Asc,FirstName Desc,LastName Asc' THEN dt.LastName
                    END
                   ,CASE WHEN @OrderBy = 'MiddleName Asc,FirstName Desc,LastName Desc' THEN dt.MiddleName
                    END
                   ,CASE WHEN @OrderBy = 'MiddleName Asc,FirstName Desc,LastName Desc' THEN dt.FirstName
                    END DESC
                   ,CASE WHEN @OrderBy = 'MiddleName Asc,FirstName Desc,LastName Desc' THEN dt.LastName
                    END DESC
                   ,CASE WHEN @OrderBy = 'MiddleName Desc,FirstName Asc,LastName Asc' THEN dt.MiddleName
                    END DESC
                   ,CASE WHEN @OrderBy = 'MiddleName Desc,FirstName Asc,LastName Asc' THEN dt.FirstName
                    END
                   ,CASE WHEN @OrderBy = 'MiddleName Desc,FirstName Asc,LastName Asc' THEN dt.LastName
                    END
                   ,CASE WHEN @OrderBy = 'MiddleName Desc,FirstName Asc,LastName Desc' THEN dt.MiddleName
                    END DESC
                   ,CASE WHEN @OrderBy = 'MiddleName Desc,FirstName Asc,LastName Desc' THEN dt.FirstName
                    END
                   ,CASE WHEN @OrderBy = 'MiddleName Desc,FirstName Asc,LastName Desc' THEN dt.LastName
                    END DESC
                   ,CASE WHEN @OrderBy = 'MiddleName Desc,FirstName Desc,LastName Asc' THEN dt.MiddleName
                    END DESC
                   ,CASE WHEN @OrderBy = 'MiddleName Desc,FirstName Desc,LastName Asc' THEN dt.FirstName
                    END DESC
                   ,CASE WHEN @OrderBy = 'MiddleName Desc,FirstName Desc,LastName Asc' THEN dt.LastName
                    END
                   ,CASE WHEN @OrderBy = 'MiddleName Desc,FirstName Desc,LastName Desc' THEN dt.MiddleName
                    END DESC
                   ,CASE WHEN @OrderBy = 'MiddleName Desc,FirstName Desc,LastName Desc' THEN dt.FirstName
                    END DESC
                   ,CASE WHEN @OrderBy = 'MiddleName Desc,FirstName Desc,LastName Desc' THEN dt.LastName
                    END DESC
                   ,PrgVerDescrip
                   ,TermStartDate
                   ,TermEndDate
                   ,TermId
                   ,TermDescription
                   ,CourseId
                   ,CourseDescription
                   ,GradeBookSysComponentTypeId
                   ,GradeBookDescription;
        END;
GO
