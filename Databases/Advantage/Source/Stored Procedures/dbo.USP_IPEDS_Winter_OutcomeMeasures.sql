SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =========================================================================================================
-- USP_IPEDS_Winter_OutcomeMeasures 
-- =========================================================================================================
CREATE PROCEDURE [dbo].[USP_IPEDS_Winter_OutcomeMeasures]
    @CampusId VARCHAR(50)
   ,@ProgIdList NVARCHAR(MAX) = NULL
   ,@CohortYear INTEGER
   ,@OrderBy VARCHAR(100)
AS
    BEGIN

        BEGIN --  01) Declare varialbles
            DECLARE @CohortStartDate DATETIME;
            DECLARE @CohortEndDate DATETIME;
            DECLARE @More5YearsGraduationDate DATETIME;
            DECLARE @More3YearsGraduationDate DATETIME;
            DECLARE @More1YearsGraduationDate DATETIME;
            DECLARE @MaxDate DATETIME;
            DECLARE @temp TABLE
                (
                    SSN NVARCHAR(50)
                   ,MaskedSSN NVARCHAR(20) NULL
                   ,StudentFullName NVARCHAR(250)
                   ,IsCohort INT
                   ,IsExclusion INT
                   ,IsCertificate INT
                   ,IsAssociates INT
                   ,IsBachelors INT
                   ,IsStillEnrolled INT
                   ,IsLaterEnrollment INT
                   ,TableGroup INT
                   ,IsFirstTimeInSchool INT
                   ,IsFullTime INT
                   ,IsPellGrantRecipient INT
                );

        END; --  End   01) Declare Varialbes

        BEGIN --   02) Define Periods.
            --   A) Cohort Range  -- Students who enter in Institution in this range
            SET @CohortStartDate = CONVERT (CHAR(4), @CohortYear) + '-07-01 00:00:00:000';
            SET @CohortEndDate = CONVERT (CHAR(4), (@CohortYear + 1)) + '-06-30 23:59:59:998';
            --  B) Graduation Range Dates
            SET @More5YearsGraduationDate = CONVERT (CHAR(4), (@CohortYear + 9 - 5)) + '-08-31 23:59:59:998';
            SET @More3YearsGraduationDate = CONVERT (CHAR(4), (@CohortYear + 9 - 3)) + '-08-31 23:59:59:998';
            SET @More1YearsGraduationDate = CONVERT (CHAR(4), (@CohortYear + 9 - 1)) + '-08-31 23:59:59:998';
            --  C) for student still on School GetDate or dic31
            SET @MaxDate = '2199/12/31';
        END; --   02) Define Periods.

        BEGIN -- 03) Select students enrolled in the Cohort Period
            INSERT INTO @temp (
                              SSN
                             ,MaskedSSN
                             ,StudentFullName
                             ,IsCohort
                             ,IsExclusion
                             ,IsCertificate
                             ,IsAssociates
                             ,IsBachelors
                             ,IsStillEnrolled
                             ,IsLaterEnrollment
                             ,TableGroup
                             ,IsFirstTimeInSchool
                             ,IsFullTime
                             ,IsPellGrantRecipient
                              )
                        SELECT T.SSN
                              ,'XXX-XX-' + SUBSTRING (ISNULL (T.SSN, 'XXX-XX-    '), ISNULL (LEN (T.SSN), 11) - 3, 4) AS MaskedSSN
                              ,T.StudentFullName
                              ,CASE WHEN IsExclusion = 1 THEN 0
                                    ELSE 1
                               END AS IsCohort
                              ,T.IsExclusion
                              ,CASE WHEN (
                                         T.SSSC_SysStatusId = 14 -- SysStaturId = 14 --> GraduatedT.CurrentSysStatusId = 14 -- SysStaturId = 14 --> Graduated
                                         AND T.CredentialIPEDSVal IN ( 154, 155 ) -- Less than 1 yr certificates, At least 1 but less than 4 yr certificates
                                         ) -- Credencial Code 01 --> Undergraduate Certificate 
                              THEN      1
                                    ELSE 0
                               END AS IsCertificate
                              ,CASE WHEN (
                                         T.SSSC_SysStatusId = 14 -- SysStaturId = 14 --> Graduated
                                         AND T.CredentialIPEDSVal = 156 --  Associate's degrees
                                         ) -- Credencial Code 02 --> IsAssociates
                              THEN      1
                                    ELSE 0
                               END AS IsAssociates
                              ,CASE WHEN (
                                         T.SSSC_SysStatusId = 14 -- SysStaturId = 14 --> Graduated
                                         AND T.CredentialIPEDSVal = 157 --  Bachelor's degrees
                                         ) -- Credencial Code 03 --> Bachelor's Degree
                              THEN      1
                                    ELSE 0
                               END AS IsBachelors
                              ,CASE WHEN (
                                         T.SSSC_SysStatusId NOT IN ( 12, 14 ) -- 7 9 10 11 20 22 24 24
                                         AND T.SSSC_InSchool = 1
                                         ) THEN 1
                                    WHEN (
                                         T.SSSC_SysStatusId IN ( 12, 14 ) -- SysStaturId = 12 -->Dropped, SysStaturId = 14 --> Graduated
                                         AND T.SSSC_InSchool = 0
                                         AND T.SSSC_DateOfChange > @More1YearsGraduationDate
                                         ) THEN 1
                                    ELSE 0
                               END AS IsStillEnrolled
                              ,0 AS IsLaterEnrollment
                              ,CASE WHEN T.SSSC_SysStatusId IN ( 12, 14 ) -- SysStaturId = 12 -->Dropped, SysStaturId = 14 --> Graduated
                              THEN      CASE WHEN (T.SSSC_DateOfChange <= @More5YearsGraduationDate) THEN 1
                                             WHEN (
                                                  T.SSSC_DateOfChange > @More5YearsGraduationDate
                                                  AND T.SSSC_DateOfChange <= @More3YearsGraduationDate
                                                  ) THEN 2
                                             WHEN (
                                                  T.SSSC_DateOfChange > @More3YearsGraduationDate
                                                  AND T.SSSC_DateOfChange <= @More1YearsGraduationDate
                                                  ) THEN 3
                                             ELSE 3
                                        END
                                    ELSE 3
                               END AS TableGroup
                              ,T.IsFirstTimeInSchool
                              ,T.IsFullTime
                              ,T.IsPellGrantRecipient
                        FROM   (
                               SELECT     dbo.UDF_FormatSSN (AL.SSN) AS SSN
                                         ,AL.LastName + ', ' + AL.FirstName + ' ' + ISNULL (LEFT(AL.MiddleName, 1), '') AS StudentFullName
                                         ,CASE WHEN ASE.IsFirstTimeInSchool = 1 THEN 1
                                               ELSE 0
                                          END AS IsFirstTimeInSchool
                                         ,SSS1.SysStatusId AS SSSC_SysStatusId
                                         ,SSS1.InSchool AS SSSC_InSchool
                                         ,SSSC.DateOfChange AS SSSC_DateOfChange
                                         ,APC.IPEDSValue AS CredentialIPEDSVal
                                         ,CASE WHEN AAT.IPEDSValue = 61 -- 61--> 'Full Time', 62--> 'PartTime'
                                         THEN      1
                                               ELSE 0
                                          END AS IsFullTime
                                         ,MAX (   CASE WHEN EXISTS (
                                                                   SELECT     1
                                                                   FROM       dbo.saTransactions AS ST
                                                                   INNER JOIN dbo.saFundSources AS SFS ON SFS.FundSourceId = ST.FundSourceId
                                                                   INNER JOIN dbo.syAdvFundSources AS SAFS ON SAFS.AdvFundSourceId = SFS.AdvFundSourceId
                                                                   WHERE      SAFS.AdvFundSourceId = 2 -- AdvFundSourceId =2 --> Descrip = 'PELL'
																              AND ST.Voided <> 1
                                                                              AND ST.StuEnrollId = ASE.StuEnrollId
                                                                   ) THEN 1
                                                       ELSE 0
                                                  END
                                              ) OVER (PARTITION BY AL.StudentId
                                                     --,AP.ProgId
                                                     ) AS IsPellGrantRecipient
                                         ,CASE WHEN ( --if is dropped with IPEDS reasons ( 15,16,17,18,19 )
                                                    SSS.SysStatusId = 12
                                                    AND EXISTS (
                                                               SELECT 1
                                                               FROM   dbo.arDropReasons AS ADR
                                                               WHERE  ADR.DropReasonId = ASE.DropReasonId
                                                                      AND ADR.IPEDSValue IS NOT NULL
                                                               )
                                                    ) THEN 1
                                               ELSE 0
                                          END AS IsExclusion
                                         ,ROW_NUMBER () OVER (PARTITION BY ASE.StudentId
                                                              --,ASE.StuEnrollId
                                                              --,AP.ProgId
                                                              ORDER BY CASE WHEN SSS.SysStatusId = 14 --Graduated STATUS have priority
                                                                  THEN          0
                                                                            ELSE 1
                                                                       END
                                                                      ,SSSC.DateOfChange DESC
                                                                      ,SSSC.ModDate DESC
                                                             ) AS RowNumberDateOfChange
                               FROM       dbo.adLeads AS AL
                               INNER JOIN dbo.arStuEnrollments AS ASE ON ASE.StudentId = AL.StudentId
                               INNER JOIN dbo.syStatusCodes AS SSC ON SSC.StatusCodeId = ASE.StatusCodeId
                               INNER JOIN dbo.sySysStatus AS SSS ON SSS.SysStatusId = SSC.SysStatusId
                               INNER JOIN dbo.arPrgVersions AS APV ON APV.PrgVerId = ASE.PrgVerId
                               INNER JOIN dbo.arPrograms AS AP ON AP.ProgId = APV.ProgId
                               INNER JOIN dbo.arProgCredential AS APC ON APC.CredentialId = AP.CredentialLvlId
                               INNER JOIN dbo.syStatuses AS APC_SS ON APC_SS.StatusId = APC.StatusId
                               INNER JOIN dbo.arProgTypes AS APT ON APT.ProgTypId = APV.ProgTypId
                               INNER JOIN dbo.arAttendTypes AS AAT ON AAT.AttendTypeId = ASE.attendtypeid
                               INNER JOIN dbo.syStudentStatusChanges AS SSSC ON SSSC.StuEnrollId = ASE.StuEnrollId
                               INNER JOIN dbo.syStatusCodes AS SSC1 ON SSC1.StatusCodeId = SSSC.NewStatusId
                               INNER JOIN dbo.sySysStatus AS SSS1 ON SSS1.SysStatusId = SSC1.SysStatusId
                               WHERE -- leads only Enrolled
                                          AL.StudentId <> '00000000-0000-0000-0000-000000000000'
                                          AND -- StuEnrollId in Campus selected
                                   LTRIM (RTRIM (ASE.CampusId)) = LTRIM (RTRIM (@CampusId))
                                          AND -- Enrolled in Program Selected
                                          (
                                          @ProgIdList IS NULL
                                          OR EXISTS (
                                                    SELECT Val
                                                    FROM   dbo.MultipleValuesForReportParameters (@ProgIdList, ',', 1) AS L
                                                    WHERE  L.Val = AP.ProgId
                                                    )
                                          )
                                          AND -- StuEnrollment startDate between Cohort Year Range
                                   ASE.StartDate
                                          BETWEEN @CohortStartDate AND @CohortEndDate
                                          AND -- ( 154, 155 ) -- Less than 1 yr certificates, At least 1 but less than 4 yr certificates
                                   -- 156 --  Associate's degrees,  157 --  Bachelor's degrees
                                   APC.IPEDSValue IN ( 154, 155, 156, 157 )
                                          AND -- Certificate Valid for IPEDS -- Certificate should be Active
                                          (
                                          APC.IPEDSValue IS NOT NULL
                                          AND APC_SS.Status = 'Active'
                                          )
                                          -- do not include records with one of next condition
                                          AND NOT ( -- do not include records with one of next condition
                                                  -- current status in StuEnrollment Should not be 'No Start'  (It have to be filter because No Start)
                                                  (SSS.SysStatusId = 8
                                                  --         OR SSS1.SysStatusId = 8
                                                  )
                                                  OR -- current status in StuEnrollment Should not be (19) Transfer out
                                                  (SSS.SysStatusId = 19
                                                  --         OR SSS1.SysStatusId = 19
                                                  )
                                                  OR -- if is dropped without IPEDS reasons null  not in ( 15,16,17,18,19 )
                                                  (
                                                  SSS.SysStatusId = 12
                                                  AND EXISTS (
                                                             SELECT 1
                                                             FROM   dbo.arDropReasons AS ADR
                                                             WHERE  ADR.DropReasonId = ASE.DropReasonId
                                                                    AND ADR.IPEDSValue IS NULL
                                                             )
                                                  )
                                                  OR -- Program Version is not continuing Education
                                                  (APV.IsContinuingEd = 1)
                                                  OR -- Date Determined or ExpGradDate or LDA falls before End Cohor Year Range
                                                  (
                                                  ISNULL (ASE.DateDetermined, @MaxDate) < @CohortStartDate
                                                  OR ISNULL (ASE.ExpGradDate, @MaxDate) < @CohortStartDate
                                                  OR ISNULL (ASE.LDA, @MaxDate) < @CohortStartDate
                                                  )
                                                  OR -- Do not show students with missing SSN (SSN in Ceros, Blanks Or Nulls)
                                                  (
                                                  AL.SSN IS NULL
                                                  OR RTRIM (LTRIM (AL.SSN)) = ''
                                                  OR AL.SSN NOT LIKE '%[^0]%'
                                                  OR AL.SSN NOT LIKE '%[^1]%'
                                                  )
                                                  )
                               ) AS T
                        WHERE  T.RowNumberDateOfChange = 1;
        --) AS T1


        END; -- End  03) Select students enrolled in the Cohort Period

        BEGIN -- 04) Add Row for empty Tablles  and sort
            SELECT   T1.MaskedSSN
                    ,T1.StudentFullName
                    ,T1.IsCohort
                    ,T1.IsExclusion
                    ,T1.IsCertificate
                    ,T1.IsAssociates
                    ,T1.IsBachelors
                    ,T1.IsStillEnrolled
                    ,T1.IsLaterEnrollment
                    ,T1.TableGroup
                    ,T1.IsFirstTimeInSchool
                    ,T1.IsFullTime
                    ,T1.IsPellGrantRecipient
            FROM     (
                     SELECT TE0.SSN
                           ,TE0.MaskedSSN
                           ,TE0.StudentFullName
                           ,TE0.IsCohort
                           ,TE0.IsExclusion
                           ,TE0.IsCertificate
                           ,TE0.IsAssociates
                           ,TE0.IsBachelors
                           ,TE0.IsStillEnrolled
                           ,TE0.IsLaterEnrollment
                           ,TE0.TableGroup
                           ,TE0.IsFirstTimeInSchool
                           ,TE0.IsFullTime
                           ,TE0.IsPellGrantRecipient
                     FROM   @temp AS TE0
                     UNION
                     SELECT NULL AS SSN
                           ,NULL AS MaskedSSN
                           ,'Table 1 has no data' AS StudentFullName
                           ,NULL AS IsCohort
                           ,NULL AS IsExclusion
                           ,NULL AS IsCertificate
                           ,NULL AS IsAssociates
                           ,NULL AS IsBachelors
                           ,NULL AS IsStillEnrolled
                           ,NULL AS IsLaterEnrollment
                           ,1 AS TableGroup
                           ,NULL AS IsFirstTimeInSchool
                           ,NULL AS IsFullTime
                           ,NULL AS IsPellGrantRecipient
                     FROM   @temp AS TE1
                     WHERE  TE1.TableGroup = 1
                     HAVING COUNT (TE1.TableGroup) = 0
                     UNION
                     SELECT NULL AS SSN
                           ,NULL AS MaskedSSN
                           ,'Table 2 has no data' AS StudentFullName
                           ,NULL AS IsCohort
                           ,NULL AS IsExclusion
                           ,NULL AS IsCertificate
                           ,NULL AS IsAssociates
                           ,NULL AS IsBachelors
                           ,NULL AS IsStillEnrolled
                           ,NULL AS IsLaterEnrollment
                           ,2 AS TableGroup
                           ,NULL AS IsFirstTimeInSchool
                           ,NULL AS IsFullTime
                           ,NULL AS IsPellGrantRecipient
                     FROM   @temp AS TE2
                     WHERE  TE2.TableGroup = 2
                     HAVING COUNT (TE2.TableGroup) = 0
                     UNION
                     SELECT NULL AS SSN
                           ,NULL AS MaskedSSN
                           ,'Table 3 has no data' AS StudentFullName
                           ,NULL AS IsCohort
                           ,NULL AS IsExclusion
                           ,NULL AS IsCertificate
                           ,NULL AS IsAssociates
                           ,NULL AS IsBachelors
                           ,NULL AS IsStillEnrolled
                           ,NULL AS IsLaterEnrollment
                           ,3 AS TableGroup
                           ,NULL AS IsFirstTimeInSchool
                           ,NULL AS IsFullTime
                           ,NULL AS IsPellGrantRecipient
                     FROM   @temp AS TE3
                     WHERE  TE3.TableGroup = 3
                     HAVING COUNT (TE3.TableGroup) = 0
                     ) AS T1
            ORDER BY T1.TableGroup
                    ,T1.IsFirstTimeInSchool DESC
                    ,T1.IsFullTime DESC
                    ,T1.IsPellGrantRecipient DESC
                    ,CASE WHEN @OrderBy = 'SSN' THEN T1.SSN
                          ELSE T1.StudentFullName
                     END;
        END; -- END 04) Add Row for tables

    END;
-- =========================================================================================================
-- END  --  USP_IPEDS_Winter_OutcomeMeasures 
-- =========================================================================================================
GO
