SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_campusdescription_campusid] @CampusId VARCHAR(50)
AS
    SELECT DISTINCT
            CampusId
           ,CampCode
           ,CampDescrip
           ,Address1
           ,Address2
           ,City
           ,Phone1
           ,Phone2
           ,Phone3
           ,Fax
           ,Email
           ,WebSite
           ,isCorporate
           ,TranscriptAuthZnTitle
           ,TranscriptAuthZnName
           ,TCSourcePath
           ,IsRemoteServer
           ,PortalContactEmail
           ,RemoteServerUsrNm
           ,RemoteServerPwd
           ,SourceFolderLoc
           ,TargetFolderLoc
           ,UseCampusAddress
           ,InvAddress1
           ,InvAddress2
           ,InvCity
           ,InvStateId
           ,(
              SELECT DISTINCT
                        StateDescrip
              FROM      syStates
              WHERE     StateId = t1.InvStateId
            ) AS InvStateDescription
           ,InvZip
           ,InvCountryId
           ,InvPhone1
           ,InvFax
           ,FLSourcePath
           ,FLTargetPath
           ,FLExceptionPath
           ,IsRemoteServerFL
           ,RemoteServerUsrNmFL
           ,RemoteServerPwdFL
           ,SourceFolderLocFL
           ,TargetFolderLocFL
           ,StateId
           ,(
              SELECT DISTINCT
                        StateDescrip
              FROM      syStates
              WHERE     StateId = t1.StateId
            ) AS StateDescription
           ,Zip
           ,CountryId
           ,(
              SELECT DISTINCT
                        CountryDescrip
              FROM      dbo.adCountries
              WHERE     CountryId = t1.CountryId
            ) AS CountryDescription
           ,StatusId
           ,(
              SELECT DISTINCT
                        Status
              FROM      dbo.syStatuses
              WHERE     StatusId = t1.StatusId
            ) AS StatusDescription
    FROM    syCampuses t1
    WHERE   t1.CampusId = @CampusId;



GO
