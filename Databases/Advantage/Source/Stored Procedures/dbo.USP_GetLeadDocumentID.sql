SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

 
CREATE PROCEDURE [dbo].[USP_GetLeadDocumentID]
    (
     @LeadID AS UNIQUEIDENTIFIER
    ,@DocumentID AS UNIQUEIDENTIFIER
    )
AS
    BEGIN

        SELECT  LeadDocId
        FROM    dbo.adLeadDocsReceived
        WHERE   LeadId = @LeadID
                AND DocumentId = @DocumentID;


    END;




GO
