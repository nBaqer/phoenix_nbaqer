SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- Add Page to export pdf/excel         
CREATE PROCEDURE [dbo].[USP_ManageSecurity_Reports_Permissions]  
    @RoleId VARCHAR(50)  
   ,@ModuleResourceId INT  
   ,@SchoolEnumerator INT  
   ,@CampusId UNIQUEIDENTIFIER  
AS --SET @RoleId='BB0E90D6-4D77-4362-95FA-B2D34260A4A1'         
    --SET @SchoolEnumerator=348         
    --SET @CampusId='5ebf6c1f-1fb1-492d-ad53-348f3986a178'         
  
    -- declare local variables           
    DECLARE @ShowRossOnlyTabs BIT  
           ,@SchedulingMethod VARCHAR(50)  
           ,@TrackSAPAttendance VARCHAR(50);  
    DECLARE @ShowCollegeOfCourtReporting VARCHAR(5)  
           ,@FameESP VARCHAR(5)  
           ,@EdExpress VARCHAR(5);  
    DECLARE @GradeBookWeightingLevel VARCHAR(20)  
           ,@ShowExternshipTabs VARCHAR(5);  
    DECLARE @showNaccasReports VARCHAR(5);  
    DECLARE @ShowStateBoardReports VARCHAR(5);  
 DECLARE @ShouldIncludeNaccas BIT;  
 DECLARE @ShouldIncludeState BIT;  
 --Declaring a variable table that will be used to store data about what header pages should be shown.   
 --ResID will hold ResourceID’s   
    --Header will be set to 1 if the ResourceID is a header  
    --ShouldInclude will be set to 1 if it should be shown   
  
    DECLARE @InTbl TABLE  
        (  
            ResId INT  
           ,Header BIT  
           ,ShouldInclude BIT  
        );  
  
    -- Get Values        
  
    IF  (  
        SELECT COUNT(*)  
        FROM   syConfigAppSetValues cv  
        INNER JOIN syConfigAppSettings cs ON cs.SettingId = cv.SettingId  
        WHERE  KeyName = 'ShowNACCASReports'  
               AND CampusId = @CampusId  
               AND Active = 1  
        ) >= 1  
        BEGIN  
            SET @showNaccasReports = (  
                                     SELECT Value  
                                     FROM   syConfigAppSetValues cv  
                                     INNER JOIN syConfigAppSettings cs ON cs.SettingId = cv.SettingId  
                                     WHERE  KeyName = 'ShowNACCASReports'  
                                            AND CampusId = @CampusId  
                                            AND Active = 1  
                                     );  
        END;  
    ELSE  
        BEGIN  
            SET @showNaccasReports = (  
                                     SELECT Value  
                                     FROM   syConfigAppSetValues cv  
                                     INNER JOIN syConfigAppSettings cs ON cs.SettingId = cv.SettingId  
                                     WHERE  KeyName = 'ShowNACCASReports'  
                                            AND CampusId IS NULL  
                                            AND Active = 1  
                                     );  
        END;  
    IF EXISTS (  
              SELECT 1  
              FROM   syConfigAppSetValues cv  
              INNER JOIN syConfigAppSettings cs ON cs.SettingId = cv.SettingId  
              WHERE  KeyName = 'ShowStateBoardAccreditationAgencyReports'  
                     AND Active = 1  
              )  
        BEGIN  
            SET @ShowStateBoardReports = (  
                                         SELECT Value  
                                         FROM   syConfigAppSetValues cv  
                                         INNER JOIN syConfigAppSettings cs ON cs.SettingId = cv.SettingId  
                                         WHERE  KeyName = 'ShowStateBoardAccreditationAgencyReports'  
                                                AND Active = 1  
                                         );  
        END;  
    ELSE  
        BEGIN  
            SET @ShowStateBoardReports = 'no';  
        END;  
  
  
  
    --Inserts for headers and pages that should always be shown   
    INSERT INTO @InTbl (  
                       ResId  
                      ,Header  
                      ,ShouldInclude  
                       )  
    VALUES ( 691 -- ResId - int  
            ,0   -- Header - bit  
            ,1   --ShouldInclude  -bit  
           );  
    INSERT INTO @InTbl (  
                       ResId  
      ,Header  
                      ,ShouldInclude  
                       )  
    VALUES ( 690 -- ResId - int  
            ,0   -- Header - bit  
            ,1   --ShouldInclude  -bit  
           );  
    INSERT INTO @InTbl (  
                       ResId  
                      ,Header  
                      ,ShouldInclude  
                       )  
    VALUES ( 692 -- ResId - int  
            ,0   -- Header - bit  
            ,1   --ShouldInclude  -bit  
           );  
    INSERT INTO @InTbl (  
                       ResId  
                      ,Header  
                      ,ShouldInclude  
                       )  
    VALUES ( 727 -- ResId - int  
            ,0   -- Header - bit  
            ,1   --ShouldInclude  -bit  
           );  
    INSERT INTO @InTbl (  
                       ResId  
                      ,Header  
                      ,ShouldInclude  
                       )  
    VALUES ( 729 -- ResId - int  
            ,0   -- Header - bit  
            ,1   --ShouldInclude  -bit  
           );  
    INSERT INTO @InTbl (  
                       ResId  
                      ,Header  
                      ,ShouldInclude  
                       )  
    VALUES ( 736 -- ResId - int  
            ,0   -- Header - bit  
            ,1   --ShouldInclude  -bit  
           );  
    INSERT INTO @InTbl (  
                       ResId  
                      ,Header  
                      ,ShouldInclude  
                       )  
    VALUES ( 730 -- ResId - int  
            ,0   -- Header - bit  
            ,1   --ShouldInclude  -bit  
           );  
    INSERT INTO @InTbl (  
                       ResId  
                      ,Header  
                      ,ShouldInclude  
                       )  
    VALUES ( 678 -- ResId - int  
            ,0   -- Header - bit  
            ,1   --ShouldInclude  -bit  
           );  
  
      
    IF ( @showNaccasReports = 'yes' )  
        BEGIN  
            SET @ShouldIncludeNaccas = 1;  
        END;  
    ELSE  
        BEGIN  
            SET @ShouldIncludeNaccas = 0;  
        END;  
		
    --Inserts with value of ShouldInclude based on showNaccasReports  
    INSERT INTO @InTbl (  
                       ResId  
                      ,Header  
                      ,ShouldInclude  
                       )  
    VALUES ( 845                  -- ResId - int  
            ,0                    -- Header - bit  
            ,@ShouldIncludeNaccas --ShouldInclude  -bit  
           );  
    INSERT INTO @InTbl (  
                       ResId  
                      ,Header  
                      ,ShouldInclude  
                       )  
    VALUES ( 846                  -- ResId - int  
            ,0                    -- Header - bit  
            ,@ShouldIncludeNaccas --ShouldInclude  -bit  
           );      
    INSERT INTO @InTbl (  
                       ResId  
                      ,Header  
                      ,ShouldInclude  
                       )  
    VALUES ( 844                  -- ResId - int  
            ,1                    -- Header - bit  
            ,@ShouldIncludeNaccas --ShouldInclude  -bit  
           );  
  
     
    IF ( @ShowStateBoardReports = 'yes' )  
        BEGIN  
            SET @ShouldIncludeState = 1;  
        END;  
    ELSE  
        BEGIN  
            SET @ShouldIncludeState = 0;  
        END;  
 --Inserts with value of ShouldInclude based on ShowStateBoardReports  
    INSERT INTO @InTbl (  
                       ResId  
                      ,Header  
                      ,ShouldInclude  
                       )  
    VALUES ( 864                 -- ResId - int  
            ,1                   -- Header - bit  
            ,@ShouldIncludeState --ShouldInclude  -bit  
           );  
  
     
    IF  (  
        SELECT COUNT(*)  
        FROM   syConfigAppSetValues  
        WHERE  SettingId = 68  
               AND CampusId = @CampusId  
        ) >= 1  
        BEGIN  
            SET @ShowRossOnlyTabs = (  
                                    SELECT Value  
                                    FROM   dbo.syConfigAppSetValues  
                                    WHERE  SettingId = 68  
                                           AND CampusId = @CampusId  
                                    );  
  
        END;  
    ELSE  
        BEGIN  
            SET @ShowRossOnlyTabs = (  
                                    SELECT Value  
                                    FROM   dbo.syConfigAppSetValues  
                                    WHERE  SettingId = 68  
                                           AND CampusId IS NULL  
                                    );  
        END;  
  
  
  
    IF  (  
        SELECT COUNT(*)  
        FROM   syConfigAppSetValues  
        WHERE  SettingId = 65  
               AND CampusId = @CampusId  
        ) >= 1  
        BEGIN  
            SET @SchedulingMethod = (  
                                    SELECT Value  
                                    FROM   dbo.syConfigAppSetValues  
                                    WHERE  SettingId = 65  
                                           AND CampusId = @CampusId  
                                    );  
  
        END;  
    ELSE  
        BEGIN  
            SET @SchedulingMethod = (  
                                    SELECT Value  
                                    FROM   dbo.syConfigAppSetValues  
                                    WHERE  SettingId = 65  
                                           AND CampusId IS NULL  
                                    );  
        END;  
  
  
  
    IF  (  
        SELECT COUNT(*)  
        FROM   syConfigAppSetValues  
        WHERE  SettingId = 72  
               AND CampusId = @CampusId  
        ) >= 1  
        BEGIN  
            SET @TrackSAPAttendance = (  
                                      SELECT Value  
                                      FROM   dbo.syConfigAppSetValues  
                                      WHERE  SettingId = 72  
                                             AND CampusId = @CampusId  
                                      );  
  
        END;  
    ELSE  
        BEGIN  
            SET @TrackSAPAttendance = (  
                                      SELECT Value  
                                      FROM   dbo.syConfigAppSetValues  
                                      WHERE  SettingId = 72  
                                             AND CampusId IS NULL  
                                      );  
        END;  
  
  
    IF  (  
        SELECT COUNT(*)  
        FROM   syConfigAppSetValues  
        WHERE  SettingId = 118  
               AND CampusId = @CampusId  
        ) >= 1  
        BEGIN  
            SET @ShowCollegeOfCourtReporting = (  
                                               SELECT Value  
                                               FROM   dbo.syConfigAppSetValues  
                                               WHERE  SettingId = 118  
                                                      AND CampusId = @CampusId  
                                               );  
  
        END;  
    ELSE  
        BEGIN  
            SET @ShowCollegeOfCourtReporting = (  
                                               SELECT Value  
                                               FROM   dbo.syConfigAppSetValues  
                                               WHERE  SettingId = 118  
                                                      AND CampusId IS NULL  
    );  
        END;  
  
  
    IF  (  
        SELECT COUNT(*)  
        FROM   syConfigAppSetValues  
        WHERE  SettingId = 37  
               AND CampusId = @CampusId  
        ) >= 1  
        BEGIN  
            SET @FameESP = (  
                           SELECT Value  
                           FROM   dbo.syConfigAppSetValues  
                           WHERE  SettingId = 37  
                                  AND CampusId = @CampusId  
                           );  
  
        END;  
    ELSE  
        BEGIN  
            SET @FameESP = (  
                           SELECT Value  
                           FROM   dbo.syConfigAppSetValues  
                           WHERE  SettingId = 37  
                                  AND CampusId IS NULL  
                           );  
        END;  
  
  
    IF  (  
        SELECT COUNT(*)  
        FROM   syConfigAppSetValues  
        WHERE  SettingId = 91  
               AND CampusId = @CampusId  
        ) >= 1  
        BEGIN  
            SET @EdExpress = (  
                             SELECT Value  
                             FROM   dbo.syConfigAppSetValues  
                             WHERE  SettingId = 91  
                                    AND CampusId = @CampusId  
                             );  
  
        END;  
    ELSE  
        BEGIN  
            SET @EdExpress = (  
                             SELECT Value  
                             FROM   dbo.syConfigAppSetValues  
                             WHERE  SettingId = 91  
                                    AND CampusId IS NULL  
                             );  
        END;  
  
  
  
    IF  (  
        SELECT COUNT(*)  
        FROM   syConfigAppSetValues  
        WHERE  SettingId = 43  
               AND CampusId = @CampusId  
        ) >= 1  
        BEGIN  
            SET @GradeBookWeightingLevel = (  
                                           SELECT Value  
                                           FROM   dbo.syConfigAppSetValues  
                                           WHERE  SettingId = 43  
                                                  AND CampusId = @CampusId  
                                           );  
  
        END;  
    ELSE  
        BEGIN  
            SET @GradeBookWeightingLevel = (  
                                           SELECT Value  
                                           FROM   dbo.syConfigAppSetValues  
                                           WHERE  SettingId = 43  
                                                  AND CampusId IS NULL  
                                           );  
        END;  
  
  
  
    IF  (  
        SELECT COUNT(*)  
        FROM   syConfigAppSetValues  
        WHERE  SettingId = 71  
               AND CampusId = @CampusId  
        ) >= 1  
        BEGIN  
            SET @ShowExternshipTabs = (  
                                      SELECT Value  
                                      FROM   dbo.syConfigAppSetValues  
                                      WHERE  SettingId = 71  
                                             AND CampusId = @CampusId  
                                      );  
  
        END;  
    ELSE  
        BEGIN  
            SET @ShowExternshipTabs = (  
                                      SELECT Value  
                                      FROM   dbo.syConfigAppSetValues  
                                      WHERE  SettingId = 71  
                                             AND CampusId IS NULL  
                                      );  
        END;  
  
    SELECT   *  
            ,CASE WHEN AccessLevel = 15 THEN 1  
                  ELSE 0  
             END AS FullPermission  
            ,CASE WHEN AccessLevel IN ( 14, 13, 12, 11, 10, 9, 8 ) THEN 1  
                  ELSE 0  
             END AS EditPermission  
            ,CASE WHEN AccessLevel IN ( 14, 13, 12, 7, 6, 5, 4 ) THEN 1  
                  ELSE 0  
             END AS AddPermission  
            ,CASE WHEN AccessLevel IN ( 14, 11, 10, 7, 6, 2 ) THEN 1  
                  ELSE 0  
             END AS DeletePermission  
            ,CASE WHEN AccessLevel IN ( 13, 11, 9, 7, 5, 3, 1 ) THEN 1  
                  ELSE 0  
             END AS ReadPermission  
            ,NULL AS TabId  
    FROM     (  
             SELECT 189 AS ModuleResourceId  
                   ,ResourceID AS ChildResourceId  
                   ,Resource AS ChildResource  
                   ,(  
                    SELECT AccessLevel  
                    FROM   syRlsResLvls  
                    WHERE  RoleId = @RoleId  
                           AND ResourceID = syResources.ResourceID  
                    ) AS AccessLevel  
                   ,ResourceURL AS ChildResourceURL  
                   ,NULL AS ParentResourceId  
                   ,NULL AS ParentResource  
                   ,1 AS GroupSortOrder  
                   ,NULL AS FirstSortOrder  
                   ,NULL AS DisplaySequence  
                   ,ResourceTypeID  
             FROM   syResources  
             WHERE  ResourceID = 189  
             UNION  
             SELECT 189 AS ModuleResourceId  
                   ,NNChild.ResourceId AS ChildResourceId  
                   ,CASE WHEN NNChild.ResourceId = 395 THEN 'Lead Tabs'  
                         ELSE CASE WHEN NNChild.ResourceId = 398 THEN 'Add/View Leads'  
                                   ELSE RChild.Resource  
                              END  
                    END AS ChildResource  
                   ,(  
                    SELECT AccessLevel  
                    FROM   syRlsResLvls  
                    WHERE  RoleId = @RoleId  
                           AND ResourceID = NNChild.ResourceId  
                    ) AS AccessLevel  
                   ,RChild.ResourceURL AS ChildResourceURL  
                   ,CASE WHEN (  
                              NNParent.ResourceId IN ( 689 )  
                              OR NNChild.ResourceId IN ( 710, 402, 617, 734, 794 )  
                              ) THEN NULL  
                         ELSE NNParent.ResourceId  
                    END AS ParentResourceId  
                   ,RParent.Resource AS ParentResource  
                   ,CASE WHEN (  
                              NNChild.ResourceId IN ( 710 )  
                              OR NNParent.ResourceId IN ( 710 )  
                              ) THEN 2  
                         ELSE CASE WHEN (  
                                        NNChild.ResourceId IN ( 734 )  
                                        OR NNParent.ResourceId IN ( 734 )  
                                        ) THEN 3  
                                   ELSE CASE WHEN (  
                                                  NNChild.ResourceId IN ( 617 )  
                                                  OR NNParent.ResourceId IN ( 617 )  
                                                  ) THEN 4  
                                             ELSE CASE WHEN (  
                                                            NNChild.ResourceId IN ( 794 )  
                                                            OR NNParent.ResourceId IN ( 794 )  
                                                            ) THEN 5  
                                                       ELSE 6  
                                                  END  
                                        END  
                              END  
                    END AS GroupSortOrder  
                   ,CASE WHEN NNChild.ResourceId = 398 THEN 1  
                         ELSE CASE WHEN NNChild.ResourceId = 395 THEN 2  
                                   ELSE 3  
                              END  
                    END AS FirstSortOrder  
                   ,CASE WHEN (  
                              NNChild.ResourceId IN ( 710, 734 )  
                              OR NNParent.ResourceId IN ( 710, 734 )  
                              ) THEN 1  
                         ELSE 2  
                    END AS DisplaySequence  
                   ,RChild.ResourceTypeID  
             FROM   syResources RChild  
             INNER JOIN syNavigationNodes NNChild ON RChild.ResourceID = NNChild.ResourceId  
             INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId  
             INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID  
             LEFT OUTER JOIN (  
                             SELECT *  
                             FROM   syResources  
                             WHERE  ResourceTypeID = 1  
                             ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID  
             WHERE  RChild.ResourceTypeID IN ( 2, 5, 8 )  
                    AND (  
                        RChild.ChildTypeId IS NULL  
                        OR RChild.ChildTypeId = 5  
                        )  
                    AND ( RChild.ResourceID NOT IN ( 395 ))  
                    AND ( NNParent.ResourceId NOT IN ( 395 ))  
                    AND (  
                        NNParent.ParentId IN (  
                                             SELECT HierarchyId  
                                             FROM   syNavigationNodes  
                                             WHERE  ResourceId = 189  
                                             )  
                        OR NNChild.ParentId IN (  
                                               SELECT HierarchyId  
                                               FROM   syNavigationNodes  
                                               WHERE  ResourceId IN ( 710, 402, 617, 734, 794 )  
                                               )  
                        )  
                    AND ( RChild.UsedIn & @SchoolEnumerator > 0 )  
             UNION  
             -- ACADEMICS RECORDS         
             SELECT 26 AS ModuleResourceId  
                   ,ResourceID AS ChildResourceId  
                   ,Resource AS ChildResource  
                   ,(  
                    SELECT AccessLevel  
                    FROM   syRlsResLvls  
                    WHERE  RoleId = @RoleId  
                           AND ResourceID = syResources.ResourceID  
                    ) AS AccessLevel  
                   ,ResourceURL AS ChildResourceURL  
                   ,NULL AS ParentResourceId  
                   ,NULL AS ParentResource  
                   ,1 AS GroupSortOrder  
                   ,NULL AS FirstSortOrder  
                   ,NULL AS DisplaySequence  
                   ,ResourceTypeID  
             FROM   syResources  
             WHERE  ResourceID = 26  
             UNION  
             SELECT 26 AS ModuleResourceId  
                   ,ChildResourceId  
                   ,ChildResource  
                   ,(  
                    SELECT AccessLevel  
                    FROM   syRlsResLvls  
                    WHERE  RoleId = @RoleId  
                           AND ResourceID = t1.ChildResourceId  
                    ) AS AccessLevel  
                   ,ChildResourceURL  
                   ,ParentResourceId  
                   ,ParentResource  
                   ,GroupSortOrder  
                   ,(  
                    SELECT TOP 1 HierarchyIndex  
                    FROM   dbo.syNavigationNodes  
                    WHERE  ResourceId = ChildResourceId  
                    ) AS FirstSortOrder  
                   ,CASE WHEN (  
                              ChildResourceId IN ( 690, 691, 729, 730 )  
                              OR ParentResourceId IN ( 690, 691, 729, 730 )  
                              ) THEN 1  
                         ELSE CASE WHEN (  
                                        ChildResourceId IN ( 727, 692, 736, 678 )  
                                        OR ParentResourceId IN ( 727, 736, 692, 678 )  
                                        ) THEN 2  
                                   ELSE 3  
                              END  
                    END AS DisplaySequence  
                   ,ResourceTypeID  
             FROM   (  
                    SELECT DISTINCT NNChild.ResourceId AS ChildResourceId  
                          ,CASE WHEN NNChild.ResourceId = 409 THEN 'IPEDS - General Reports'  
                                ELSE RChild.Resource  
                           END AS ChildResource  
                          ,RChild.ResourceURL AS ChildResourceURL  
                          ,CASE WHEN (  
                                     NNParent.ResourceId IN ( 689 )  
                                     OR NNChild.ResourceId IN ( 472, 474, 712, 736, 678 ) --678         
                                     ) THEN NULL  
                                ELSE NNParent.ResourceId  
                           END AS ParentResourceId  
                          ,RParent.Resource AS ParentResource  
                          ,RParentModule.Resource AS MODULE  
                          ,CASE WHEN (  
                                     NNChild.ResourceId IN ( 691 )  
                                     OR NNParent.ResourceId IN ( 691 )  
                                     ) THEN 2  
                                WHEN (  
                                     NNChild.ResourceId IN ( 690 )  
                                     OR NNParent.ResourceId IN ( 690 )  
                                     ) THEN 3  
                                WHEN (  
                                     NNChild.ResourceId IN ( 729 )  
                                     OR NNParent.ResourceId IN ( 729 )  
                                     ) THEN 4  
                                WHEN (  
                                     NNChild.ResourceId IN ( 730 )  
                                     OR NNParent.ResourceId IN ( 730 )  
                                     ) THEN 5  
                                WHEN (  
                                     NNChild.ResourceId IN ( 736 )  
                                     OR NNParent.ResourceId IN ( 736 )  
                                     ) THEN 6  
                                WHEN (  
                                     NNChild.ResourceId IN ( 678 )  
                                     OR NNParent.ResourceId IN ( 678 )  
                                     ) THEN 7  
                                WHEN (  
                                     NNChild.ResourceId IN ( 692 )  
                                     OR NNParent.ResourceId IN ( 692 )  
                                     ) THEN 8  
                                WHEN (  
                                     NNChild.ResourceId IN ( 727 )  
                                     OR NNParent.ResourceId IN ( 727 )  
                                     ) THEN 9  
                                WHEN (  
                                     NNChild.ResourceId IN ( 864 )  
                                     OR NNParent.ResourceId IN ( 864 )  
                                     ) THEN 10  
									  WHEN (  
                                     NNChild.ResourceId IN ( 839 )  
                                     OR NNParent.ResourceId IN ( 839 )  
                                     ) THEN 11
									 WHEN (  
                                     NNChild.ResourceId IN ( 844 )  
                                     OR NNParent.ResourceId IN ( 844 )  
                                     ) THEN 12
                                ELSE 13 
                           END AS GroupSortOrder  
                          ,RChild.ResourceTypeID  
                    FROM   syResources RChild  
                    INNER JOIN syNavigationNodes NNChild ON RChild.ResourceID = NNChild.ResourceId  
                    INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId  
                    INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID  
                    LEFT OUTER JOIN (  
                                    SELECT *  
                                    FROM   syResources  
                                    WHERE  ResourceTypeID = 1  
                                    ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID  
                    WHERE  RChild.ResourceTypeID IN ( 2, 5, 8 )  
                           AND (  
                               RChild.ChildTypeId IS NULL  
                               OR RChild.ChildTypeId = 5  
                               )  
                           AND ( RChild.ResourceID NOT IN (  
                                                          SELECT ResId  
                                                          FROM   @InTbl  
                                                          WHERE  Header = 1  
                                                                 AND ShouldInclude = 0  
                                                          )  
                               )  
                           AND ( RChild.ResourceID NOT IN ( 394, 472, 474, 711, 409, 719, 720, 721, 722, 723, 724, 725, 789 ))  
                           AND (  
                               NNParent.ParentId IN (  
                                                    SELECT HierarchyId  
                                                    FROM   syNavigationNodes  
                                                    WHERE  ResourceId = 26  
                                                    )  
                               OR NNChild.ParentId IN (  
                                                      SELECT HierarchyId  
                                                      FROM   syNavigationNodes  
                                                      WHERE ResourceId IN ( SELECT ResId FROM   @InTbl WHERE ShouldInclude = 1)   
                                                      )  
                               )  
                           AND ( RChild.UsedIn & @SchoolEnumerator > 0 )  
                    ) t1  
             UNION  
             SELECT 194 AS ModuleResourceId  
                   ,ResourceID AS ChildResourceId  
                   ,Resource AS ChildResource  
                   ,(  
                    SELECT AccessLevel  
                    FROM   syRlsResLvls  
                    WHERE  RoleId = @RoleId  
                           AND ResourceID = syResources.ResourceID  
                    ) AS AccessLevel  
                   ,ResourceURL AS ChildResourceURL  
                   ,NULL AS ParentResourceId  
                   ,NULL AS ParentResource  
                   ,1 AS GroupSortOrder  
                   ,NULL AS FirstSortOrder  
                   ,NULL AS DisplaySequence  
                   ,ResourceTypeID  
             FROM   syResources  
             WHERE  ResourceID = 194  
             UNION  
             SELECT 194 AS ModuleResourceId  
                   ,ChildResourceId  
                   ,ChildResource  
                   ,(  
                    SELECT AccessLevel  
                    FROM   syRlsResLvls  
                    WHERE  RoleId = @RoleId  
                           AND ResourceID = t1.ChildResourceId  
                    ) AS AccessLevel  
                   ,ChildResourceURL  
                   ,ParentResourceId  
                   ,ParentResource  
                   ,GroupSortOrder  
                   ,GroupSortOrder AS FirstSortOrder  
                   ,CASE WHEN (  
                              ChildResourceId IN ( 731, 732 )  
                              OR ParentResourceId IN ( 731, 732 )  
                              ) THEN 1  
                         ELSE 2  
                    END AS DisplaySequence  
                   ,ResourceTypeID  
             FROM   (  
                    SELECT DISTINCT NNChild.ResourceId AS ChildResourceId  
                          ,CASE WHEN NNChild.ResourceId = 409 THEN 'IPEDS - General Reports'  
                                ELSE RChild.Resource  
                           END AS ChildResource  
                          ,RChild.ResourceURL AS ChildResourceURL  
                          ,CASE WHEN NNParent.ResourceId IN ( 194 )  
                                     OR NNChild.ResourceId IN ( 731, 732, 733 ) THEN NULL  
                                ELSE NNParent.ResourceId  
                           END AS ParentResourceId  
                          ,RParent.Resource AS ParentResource  
                          ,RParentModule.Resource AS MODULE  
                          -- DE7659 updated for the Payment Report (4.2)         
                          ,CASE WHEN (  
                                     NNChild.ResourceId IN ( 732 )  
                                     OR NNParent.ResourceId IN ( 732 )  
                              ) THEN 2  
                                ELSE CASE WHEN (  
                                               NNChild.ResourceId IN ( 731 )  
                                               OR NNParent.ResourceId IN ( 731 )  
                                               ) THEN 3  
                                          ELSE CASE WHEN (  
                                                         NNChild.ResourceId IN ( 733 )  
                                                         OR NNParent.ResourceId IN ( 733 )  
                                                         ) THEN 4  
                                                    ELSE 5  
                                               END  
                                     END  
                           END AS GroupSortOrder  
                          ,RChild.ResourceTypeID  
                    FROM   syResources RChild  
                    INNER JOIN syNavigationNodes NNChild ON RChild.ResourceID = NNChild.ResourceId  
                    INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId  
                    INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID  
                    LEFT OUTER JOIN (  
                                    SELECT *  
                                    FROM   syResources  
                                    WHERE  ResourceTypeID = 1  
                                    ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID  
                    WHERE  RChild.ResourceTypeID IN ( 2, 5, 8 )  
                           AND (  
                               RChild.ChildTypeId IS NULL  
                               OR RChild.ChildTypeId = 5  
                               )  
                           --AND (NNParent.ResourceId IN (689,409,711,472,474,712))         
                           AND ( RChild.ResourceID NOT IN ( 394 ))  
                           AND (  
                               NNParent.ParentId IN (  
                                                    SELECT HierarchyId  
                                                    FROM   syNavigationNodes  
                                                    WHERE  ResourceId = 194  
                                                    )  
                               OR NNChild.ParentId IN (  
                                                      SELECT HierarchyId  
                                                      FROM   syNavigationNodes  
                                                      WHERE  ResourceId IN ( 731, 732, 733, 711 )  
                                                      )  
                               )  
                           AND ( RChild.UsedIn & @SchoolEnumerator > 0 )  
                    ) t1  
             UNION  
             SELECT 300 AS ModuleResourceId  
                   ,ResourceID AS ChildResourceId  
                   ,Resource AS ChildResource  
                   ,(  
                    SELECT AccessLevel  
                    FROM   syRlsResLvls  
                    WHERE  RoleId = @RoleId  
                           AND ResourceID = syResources.ResourceID  
                    ) AS AccessLevel  
                   ,ResourceURL AS ChildResourceURL  
                   ,NULL AS ParentResourceId  
                   ,NULL AS ParentResource  
                   ,1 AS GroupSortOrder  
                   ,NULL AS FirstSortOrder  
                   ,NULL AS DisplaySequence  
                   ,ResourceTypeID  
             FROM   syResources  
             WHERE  ResourceID = 300  
             UNION  
             SELECT 300 AS ModuleResourceId  
                   ,ChildResourceId  
                   ,ChildResource  
                   ,(  
                    SELECT AccessLevel  
                    FROM   syRlsResLvls  
                    WHERE  RoleId = @RoleId  
                           AND ResourceID = t1.ChildResourceId  
                    ) AS AccessLevel  
      ,ChildResourceURL  
                   ,ParentResourceId  
                   ,ParentResource  
                   ,GroupSortOrder  
                   ,GroupSortOrder AS FirstSortOrder  
                   ,1 AS DisplaySequence  
                   ,ResourceTypeID  
             FROM   (  
                    SELECT DISTINCT NNChild.ResourceId AS ChildResourceId  
                          ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'  
                                ELSE RChild.Resource  
                           END AS ChildResource  
                          ,RChild.ResourceURL AS ChildResourceURL  
                          ,CASE WHEN NNParent.ResourceId = 300  
                                     OR NNChild.ResourceId = 715 THEN NULL  
                                --ELSE NNParent.ResourceId         
                                -- Had to hard code the parentresourceid to 715 for DE7532 on 7/25/2012 B. Shanblatt         
                                ELSE 715  
                           END AS ParentResourceId  
                          --RParent.Resource AS ParentResource ,         
                          -- Had to hard code the parentresource to General Reports for DE7532 on 7/25/2012 B. Shanblatt         
                          ,'General Reports' AS ParentResource  
                          ,RParentModule.Resource AS MODULE  
                          ,CASE WHEN (  
                                     NNChild.ResourceId IN ( 715 )  
                                     -- Added 691 to fix the Weekly Attendance report getting duplicated         
                                     OR NNParent.ResourceId IN ( 715, 691 )  
                                     ) THEN 2  
                                ELSE 3  
                           END AS GroupSortOrder  
                          ,RChild.ResourceTypeID  
                    FROM   syResources RChild  
                    INNER JOIN syNavigationNodes NNChild ON RChild.ResourceID = NNChild.ResourceId  
                    INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId  
                    INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID  
                    LEFT OUTER JOIN (  
                                    SELECT *  
                                    FROM   syResources  
                                    WHERE  ResourceTypeID = 1  
                                    ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID  
                    WHERE  RChild.ResourceTypeID IN ( 2, 5, 8 )  
                           AND (  
                               RChild.ChildTypeId IS NULL  
                               OR RChild.ChildTypeId = 5  
                               )  
                           --AND (NNParent.ResourceId IN (689,409,711,472,474,712))         
                           -- Added resource 325, 329, 366, 492, 554, 588, 633 for DE7532 on 7/25/2012 B. Shanblatt         
                           AND ( RChild.ResourceID NOT IN ( 394, 325, 329, 366, 492, 554, 633 ))  
                           AND (  
                               NNParent.ParentId IN (  
                                                    SELECT HierarchyId  
                                                    FROM   syNavigationNodes  
                                                    WHERE  ResourceId = 300  
                                                    )  
                               OR NNChild.ParentId IN (  
                                                      SELECT HierarchyId  
                                                      FROM   syNavigationNodes  
                                                      -- Added resource 691 for DE7532 on 7/25/2012 B. Shanblatt          
                                                      WHERE  ResourceId IN ( 715, 691 )  
                                                      )  
                               )  
                           AND ( RChild.UsedIn & @SchoolEnumerator > 0 )  
                    ) t1  
             UNION  
             SELECT 191 AS ModuleResourceId  
                   ,ResourceID AS ChildResourceId  
                   ,Resource AS ChildResource  
                   ,(  
                    SELECT AccessLevel  
                    FROM   syRlsResLvls  
                    WHERE  RoleId = @RoleId  
                           AND ResourceID = syResources.ResourceID  
                    ) AS AccessLevel  
                   ,ResourceURL AS ChildResourceURL  
                   ,NULL AS ParentResourceId  
                   ,NULL AS ParentResource  
                   ,1 AS GroupSortOrder  
                   ,NULL AS FirstSortOrder  
                   ,NULL AS DisplaySequence  
                   ,ResourceTypeID  
             FROM   syResources  
             WHERE  ResourceID = 191  
             UNION  
             SELECT 191 AS ModuleResourceId  
                   ,ChildResourceId  
                   ,ChildResource  
                   ,(  
                    SELECT AccessLevel  
                    FROM   syRlsResLvls  
                    WHERE  RoleId = @RoleId  
                           AND ResourceID = t1.ChildResourceId  
                    ) AS AccessLevel  
                   ,ChildResourceURL  
                   ,ParentResourceId  
                   ,ParentResource  
                   ,GroupSortOrder  
                   ,GroupSortOrder AS FirstSortOrder  
                   ,CASE WHEN (  
                              ChildResourceId IN ( 716 )  
                              OR ParentResourceId IN ( 716 )  
                              ) THEN 1  
                         ELSE 2  
                    END AS DisplaySequence  
                   ,ResourceTypeID  
             FROM   (  
                    SELECT DISTINCT NNChild.ResourceId AS ChildResourceId  
                          ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'  
                                ELSE RChild.Resource  
                           END AS ChildResource  
                          ,RChild.ResourceURL AS ChildResourceURL  
                          ,CASE WHEN (  
                                     NNParent.ResourceId = 191  
                                     OR NNChild.ResourceId = 716  
                                     ) THEN NULL  
                                ELSE NNParent.ResourceId  
                           END AS ParentResourceId  
                          ,RParent.Resource AS ParentResource  
                          ,RParentModule.Resource AS MODULE  
                          ,CASE WHEN (  
                                     NNChild.ResourceId IN ( 716 )  
                                     OR NNParent.ResourceId IN ( 716 )  
                                     ) THEN 2  
                                ELSE 3  
                           END AS GroupSortOrder  
                          ,RChild.ResourceTypeID  
                    FROM   syResources RChild  
                    INNER JOIN syNavigationNodes NNChild ON RChild.ResourceID = NNChild.ResourceId  
                    INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId  
                    INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID  
                    LEFT OUTER JOIN (  
                                    SELECT *  
                                    FROM   syResources  
                                    WHERE  ResourceTypeID = 1  
                                    ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID  
                    WHERE  RChild.ResourceTypeID IN ( 2, 5, 8 )  
                           AND (  
                               RChild.ChildTypeId IS NULL  
                               OR RChild.ChildTypeId = 5  
                               )  
                           --AND (NNParent.ResourceId IN (689,409,711,472,474,712))         
                           AND ( RChild.ResourceID NOT IN ( 394 ))  
         AND (  
                               NNParent.ParentId IN (  
                                                    SELECT HierarchyId  
                                                    FROM   syNavigationNodes  
                                                    WHERE  ResourceId = 191  
                                                    )  
                               OR NNChild.ParentId IN (  
                                                      SELECT HierarchyId  
                                                      FROM   syNavigationNodes  
                                                      WHERE  ResourceId IN ( 716 )  
                                                      )  
                               )  
                           AND ( RChild.UsedIn & @SchoolEnumerator > 0 )  
                    ) t1  
             UNION  
             SELECT 193 AS ModuleResourceId  
                   ,ResourceID AS ChildResourceId  
                   ,Resource AS ChildResource  
                   ,(  
                    SELECT AccessLevel  
                    FROM   syRlsResLvls  
                    WHERE  RoleId = @RoleId  
                           AND ResourceID = syResources.ResourceID  
                    ) AS AccessLevel  
                   ,ResourceURL AS ChildResourceURL  
                   ,NULL AS ParentResourceId  
                   ,NULL AS ParentResource  
                   ,1 AS GroupSortOrder  
                   ,NULL AS FirstSortOrder  
                   ,NULL AS DisplaySequence  
                   ,ResourceTypeID  
             FROM   syResources  
             WHERE  ResourceID = 193  
             UNION  
             SELECT 193 AS ModuleResourceId  
                   ,ChildResourceId  
                   ,ChildResource  
                   ,(  
                    SELECT AccessLevel  
                    FROM   syRlsResLvls  
                    WHERE  RoleId = @RoleId  
                           AND ResourceID = t1.ChildResourceId  
                    ) AS AccessLevel  
                   ,ChildResourceURL  
                   ,ParentResourceId  
                   ,ParentResource  
                   ,GroupSortOrder  
                   ,GroupSortOrder AS FirstSortOrder  
                   ,CASE WHEN (  
                              ChildResourceId IN ( 713 )  
                              OR ParentResourceId IN ( 713 )  
                              ) THEN 1  
                         ELSE 2  
                    END AS DisplaySequence  
                   ,ResourceTypeID  
             FROM   (  
                    SELECT DISTINCT NNChild.ResourceId AS ChildResourceId  
                          ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'  
                                ELSE RChild.Resource  
                           END AS ChildResource  
                          ,RChild.ResourceURL AS ChildResourceURL  
                          ,CASE WHEN (  
                                     NNParent.ResourceId = 193  
                                     OR NNChild.ResourceId IN ( 713, 735 )  
                                     ) THEN NULL  
                                ELSE NNParent.ResourceId  
                           END AS ParentResourceId  
                          ,RParent.Resource AS ParentResource  
                          ,RParentModule.Resource AS MODULE  
                          ,CASE WHEN (  
                                     NNChild.ResourceId IN ( 713 )  
                                     OR NNParent.ResourceId IN ( 713 )  
                                     ) THEN 2  
                                ELSE 3  
                           END AS GroupSortOrder  
                          ,RChild.ResourceTypeID  
                    FROM   syResources RChild  
                    INNER JOIN syNavigationNodes NNChild ON RChild.ResourceID = NNChild.ResourceId  
                    INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId  
                    INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID  
                    LEFT OUTER JOIN (  
                                    SELECT *  
                                    FROM   syResources  
                                    WHERE  ResourceTypeID = 1  
                                    ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID  
                    WHERE  RChild.ResourceTypeID IN ( 2, 5, 8 )  
                           AND (  
                               RChild.ChildTypeId IS NULL  
                               OR RChild.ChildTypeId = 5  
                               )  
                           --AND (NNParent.ResourceId IN (689,409,711,472,474,712))         
                           AND ( RChild.ResourceID NOT IN ( 394 ))  
                           AND (  
                               NNParent.ParentId IN (  
                                                    SELECT HierarchyId  
                                                    FROM   syNavigationNodes  
                                                    WHERE  ResourceId = 193  
                                                    )  
                               OR NNChild.ParentId IN (  
                                                      SELECT HierarchyId  
                                                      FROM   syNavigationNodes  
                                                      WHERE  ResourceId IN ( 713, 735 )  
                                                      )  
                               )  
                           AND ( RChild.UsedIn & @SchoolEnumerator > 0 )  
                    ) t1  
             UNION  
             SELECT 195 AS ModuleResourceId  
                   ,ResourceID AS ChildResourceId  
                   ,Resource AS ChildResource  
                   ,(  
                    SELECT AccessLevel  
                    FROM   syRlsResLvls  
                    WHERE  RoleId = @RoleId  
                           AND ResourceID = syResources.ResourceID  
                    ) AS AccessLevel  
                   ,ResourceURL AS ChildResourceURL  
                   ,NULL AS ParentResourceId  
                   ,NULL AS ParentResource  
                   ,1 AS GroupSortOrder  
                   ,NULL AS FirstSortOrder  
                   ,NULL AS DisplaySequence  
                   ,ResourceTypeID  
             FROM   syResources  
             WHERE  ResourceID = 195  
             UNION  
             SELECT 195 AS ModuleResourceId  
                   ,ChildResourceId  
                   ,ChildResource  
                   ,(  
                    SELECT AccessLevel  
                    FROM   syRlsResLvls  
                    WHERE  RoleId = @RoleId  
                           AND ResourceID = t1.ChildResourceId  
                    ) AS AccessLevel  
                   ,ChildResourceURL  
                   ,ParentResourceId  
                   ,ParentResource  
                   ,GroupSortOrder  
                   ,GroupSortOrder AS FirstSortOrder  
                   ,CASE WHEN (  
                              ChildResourceId IN ( 717 )  
                              OR ParentResourceId IN ( 717 )  
                              ) THEN 1  
                         ELSE 2  
                    END AS DisplaySequence  
                   ,ResourceTypeID  
             FROM   (  
                    SELECT DISTINCT NNChild.ResourceId AS ChildResourceId  
                          ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'  
                                ELSE RChild.Resource  
                           END AS ChildResource  
                          ,RChild.ResourceURL AS ChildResourceURL  
                          ,CASE WHEN (  
                                     NNParent.ResourceId = 195  
                                     OR NNChild.ResourceId IN ( 717 )  
                                     ) THEN NULL  
                                ELSE NNParent.ResourceId  
                           END AS ParentResourceId  
                          ,RParent.Resource AS ParentResource  
                          ,RParentModule.Resource AS MODULE  
                          ,CASE WHEN (  
                                     NNChild.ResourceId IN ( 717 )  
                                     OR NNParent.ResourceId IN ( 717 )  
                                     ) THEN 2  
                                ELSE 3  
                           END AS GroupSortOrder  
                          ,RChild.ResourceTypeID  
                    FROM   syResources RChild  
                    INNER JOIN syNavigationNodes NNChild ON RChild.ResourceID = NNChild.ResourceId  
                    INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId  
                    INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID  
                    LEFT OUTER JOIN (  
                                    SELECT *  
                                    FROM   syResources  
                                    WHERE  ResourceTypeID = 1  
                                    ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID  
                    WHERE  RChild.ResourceTypeID IN ( 2, 5, 8 )  
                           AND (  
                               RChild.ChildTypeId IS NULL  
                               OR RChild.ChildTypeId = 5  
                               )  
                           --AND (NNParent.ResourceId IN (689,409,711,472,474,712))         
                           AND ( RChild.ResourceID NOT IN ( 394 ))  
                           AND (  
                               NNParent.ParentId IN (  
                                                    SELECT HierarchyId  
                                                    FROM   syNavigationNodes  
                                                    WHERE  ResourceId = 195  
                                                    )  
                               OR NNChild.ParentId IN (  
                                                      SELECT HierarchyId  
                                                      FROM   syNavigationNodes  
                                                      WHERE  ResourceId IN ( 717 )  
                                                      )  
                               )  
                           AND ( RChild.UsedIn & @SchoolEnumerator > 0 )  
                    ) t1  
             ) t2  
    WHERE    t2.ModuleResourceId = @ModuleResourceId  
  
             -- Hide resources based on Configuration Settings           
             AND (  
                 -- The following expression means if showross... is set to false, hide            
                 -- ResourceIds 541,542,532,534,535,538,543,539           
                 -- (Not False) OR (Condition)           
                 (  
                 ( @ShowRossOnlyTabs <> 0 )  
                 OR ( ChildResourceId NOT IN ( 541, 542, 532, 534, 535, 538, 543, 539 ))  
                 )  
                 AND  
                 -- The following expression means if showross... is set to true, hide            
                 -- ResourceIds 142,375,330,476,508,102,107,237           
                 -- (Not True) OR (Condition)           
                 (  
                 ( @ShowRossOnlyTabs <> 1 )  
                 OR ( ChildResourceId NOT IN ( 142, 375, 330, 476, 508, 102, 107, 237 ))  
                 )  
                 AND  
                 ---- The following expression means if SchedulingMethod=regulartraditional, hide            
                 ---- ResourceIds 91 and 497           
                 ---- (Not True) OR (Condition)           
                 (  
                 ( NOT LOWER(LTRIM(RTRIM(@SchedulingMethod))) = 'regulartraditional' )  
                 OR ( ChildResourceId NOT IN ( 91, 497 ))  
                 )  
                 AND  
                 -- The following expression means if TrackSAPAttendance=byday, hide            
                 -- ResourceIds 585,586,589,590,677,678,770,670           
                 -- (Not True) OR (Condition)           
                 -- Removed 667 and 770 and 585 on 7/25/2012 B. Shanblatt          
                 -- DE8640         
                 -- ( ( NOT LOWER(LTRIM(RTRIM(@TrackSAPAttendance))) = 'byday'         
                 -- )         
                 --OR ( ChildResourceId NOT IN ( 589, 590,         
                 --         670 ) )         
                 --  )               
                 --  AND           
                 (  
                 ( NOT LOWER(LTRIM(RTRIM(@TrackSAPAttendance))) = 'byclass' )  
                 OR ( ChildResourceId NOT IN ( 633 ))  
                 )  
                 AND  
                 ---- The following expression means if @ShowCollegeOfCourtReporting is set to false, hide            
                 ---- ResourceIds 614,615           
                 ---- (Not False) OR (Condition)           
                 (  
                 ( NOT LOWER(LTRIM(RTRIM(@ShowCollegeOfCourtReporting))) = 'no' )  
                 OR ( ChildResourceId NOT IN ( 614, 615, 729 ))  
                 )  
                 AND  
                 -- The following expression means if @ShowCollegeOfCourtReporting is set to true, hide            
                 -- ResourceIds 497           
                 -- (Not True) OR (Condition)           
                 (  
                 ( NOT LOWER(LTRIM(RTRIM(@ShowCollegeOfCourtReporting))) = 'yes' )  
                 OR ( ChildResourceId NOT IN ( 497 ))  
                 )  
                 AND  
                 -- The following expression means if FAMEESP is set to false, hide            
                 -- ResourceIds 517,523, 525           
                 -- (Not False) OR (Condition)           
                 (  
                 ( NOT LOWER(LTRIM(RTRIM(@FameESP))) = 'no' )  
                 OR ( ChildResourceId NOT IN ( 517, 523, 525 ))  
                 )  
                 AND  
                 ---- The following expression means if EDExpress is set to false, hide            
                 ---- ResourceIds 603,604,606,619           
                 ---- (Not False) OR (Condition)           
                 (  
                 ( NOT LOWER(LTRIM(RTRIM(@EdExpress))) = 'no' )  
                 OR ( ChildResourceId NOT IN ( 603, 604, 605, 619 ))  
                 )  
                 AND  
                 ---- The following expression means if @GradeBookWeightingLevel is set to courselevel, hide            
                 ---- ResourceIds 107,96,222           
                 ---- (Not False) OR (Condition)           
                 (  
                 ( NOT LOWER(LTRIM(RTRIM(@GradeBookWeightingLevel))) = 'courselevel' )  
                 OR ( ChildResourceId NOT IN ( 107, 96, 222 ))  
                 )  
                 AND (  
                     ( NOT LOWER(LTRIM(RTRIM(@GradeBookWeightingLevel))) = 'instructorlevel' )  
                     OR ( ChildResourceId NOT IN ( 476 ))  
                     )  
                 AND (  
                     ( NOT LOWER(LTRIM(RTRIM(@ShowExternshipTabs))) = 'no' )  
                     OR ( ChildResourceId NOT IN ( 543, 538 ))  
                     )  
                 )  
    ORDER BY GroupSortOrder  
            ,ParentResourceId  
            ,ChildResource;  
GO
