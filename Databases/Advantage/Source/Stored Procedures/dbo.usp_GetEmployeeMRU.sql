SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_GetEmployeeMRU]
    @UserId UNIQUEIDENTIFIER
   ,@CampusId UNIQUEIDENTIFIER
AS
    BEGIN

        SET NOCOUNT ON;

-- For new users pull 20 students from the user's default campus

-- For existing users pull 20 students from the syMRU table, students can be from multiple campuses

        DECLARE @CountMRU INT;

        DECLARE @DefaultCampusId UNIQUEIDENTIFIER;

        DECLARE @StudentIdentifier VARCHAR(50);

        SET @StudentIdentifier = (
                                   SELECT   Value
                                   FROM     dbo.syConfigAppSetValues
                                   WHERE    SettingId = 69
                                            AND CampusId IS NULL
                                 );



-- used to return MRU results

        DECLARE @MRUList TABLE
            (
             MRUId UNIQUEIDENTIFIER NOT NULL
            , -- PRIMARY KEY,
             Counter INT NOT NULL
            ,ChildId UNIQUEIDENTIFIER NOT NULL
            ,MRUTypeId TINYINT NOT NULL
            ,UserId UNIQUEIDENTIFIER NOT NULL
            ,CampusId UNIQUEIDENTIFIER NULL
            ,SortOrder INT NULL
            ,IsSticky BIT NULL
            ,EmployeeId UNIQUEIDENTIFIER NOT NULL
            ,FullName VARCHAR(200) NOT NULL
            ,CampusDescrip VARCHAR(100) NOT NULL
            ,ModDate DATETIME
            );

-- used to create default MRU list for the user if one doesn't exist

        DECLARE @DefaultMRU TABLE
            (
             ChildId UNIQUEIDENTIFIER NOT NULL
            ,MRUTypeId TINYINT NOT NULL
            ,UserId UNIQUEIDENTIFIER NOT NULL
            ,CampusId UNIQUEIDENTIFIER NULL
            ,SortOrder INT NOT NULL
                           IDENTITY
            ,IsSticky BIT NULL
            ,ModDate DATETIME
            );

        SELECT  @CountMRU = COUNT(*)
        FROM    dbo.syMRUS
        WHERE   UserId = @UserId
                AND MRUTypeId = 3; --Employee

        PRINT @CountMRU;

        IF ( @CountMRU > 0 )
            BEGIN

-- Has MRU rows 

                INSERT  INTO @MRUList
                        (
                         MRUId
                        ,Counter
                        ,ChildId
                        ,MRUTypeId
                        ,UserId
                        ,CampusId
                        ,SortOrder
                        ,IsSticky
                        ,EmployeeId
                        ,FullName
                        ,CampusDescrip
                        ,ModDate

	                    )
                        SELECT DISTINCT TOP 20
                                M.MRUId
                               ,M.Counter
                               ,M.ChildId
                               ,M.MRUTypeId
                               ,M.UserId
                               ,M.CampusId
                               ,M.SortOrder
                               ,M.IsSticky
                               ,S.EmpId
                               ,CASE WHEN S.MI IS NULL
                                          OR S.MI = '' THEN S.FirstName + ' ' + S.LastName
                                     ELSE S.FirstName + ' ' + S.MI + ' ' + S.LastName
                                END AS FullName
                               ,C.CampDescrip
                               ,M.ModDate
                        FROM    dbo.syMRUS AS M
                        INNER JOIN dbo.syMRUTypes AS T ON M.MRUTypeId = T.MRUTypeId
                        INNER JOIN dbo.hrEmployees AS S ON M.ChildId = S.EmpId
                        INNER  JOIN dbo.syCampuses AS C ON M.CampusId = C.CampusId
                        WHERE   M.MRUTypeId = 3
                                AND UserId = @UserId
                                AND 

-- MRU List will show leads from the campuses user has access to
                                C.CampusId IN ( SELECT DISTINCT
                                                        C.CampusId
                                                FROM    syUsers U
                                                INNER JOIN syUsersRolesCampGrps URC ON U.UserId = URC.UserId
                                                INNER JOIN syCmpGrpCmps CGC ON URC.CampGrpId = CGC.CampGrpId
                                                INNER JOIN syCampuses C ON C.CampusId = CGC.CampusId
                                                WHERE   U.UserId = @UserId --Order by C.CampDescrip

		)
                        ORDER BY M.ModDate DESC; 



                DECLARE @MruListCount INT; 

                SET @MruListCount = 0;

                SET @MruListCount = (
                                      SELECT    COUNT(*) AS MruCountForFirstTimeUser
                                      FROM      @MRUList
                                    );

			

                SET @DefaultCampusId = (
                                         SELECT DISTINCT
                                                U.CampusId
                                         FROM   syUsers U
                                         WHERE  U.UserId = @UserId
                                       );

                IF @DefaultCampusId IS NULL
                    BEGIN

                        SET @DefaultCampusId = (
                                                 SELECT DISTINCT TOP 1
                                                        SC.CampusId
                                                 FROM   dbo.syCmpGrpCmps SC
                                                       ,dbo.syUsersRolesCampGrps URC
                                                       ,dbo.syCampuses S
                                                 WHERE  URC.UserId = @UserId
                                                        AND SC.CampGrpId = URC.CampGrpId
                                                        AND S.CampusId = SC.CampusId
                                                        AND S.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'

					--Order by S.CampDescrip
                                               );



                    END;

			

			-- The MRU list is currently empty

			-- Get the most recently added 20 leads from default campus

                IF @MruListCount = 0
                    BEGIN

                        INSERT  INTO @MRUList
                                (
                                 MRUId
                                ,Counter
                                ,ChildId
                                ,MRUTypeId
                                ,UserId
                                ,CampusId
                                ,SortOrder
                                ,IsSticky
                                ,EmployeeId
                                ,FullName
                                ,CampusDescrip
                                ,ModDate

					            )
                                SELECT DISTINCT TOP 20
                                        NEWID() AS MRUId
                                       ,101 AS Counter
                                       ,  -- not being used so hardcoded a value
                                        L.EmpId AS ChildId
                                       ,3 AS MRUTypeId
                                       ,@UserId AS UserId
                                       ,C.CampusId
                                       ,0 AS SortOrder
                                       ,0 AS IsSticky
                                       ,L.EmpId AS EmployeeId
                                       ,CASE WHEN L.MI IS NULL
                                                  OR L.MI = '' THEN L.FirstName + ' ' + L.LastName
                                             ELSE L.FirstName + ' ' + L.MI + ' ' + L.LastName
                                        END AS FullName
                                       ,C.CampDescrip
                                       ,L.ModDate
                                FROM    dbo.hrEmployees AS L
                                INNER JOIN dbo.syCampuses AS C ON L.CampusId = C.CampusId
                                WHERE   -- MRU List will show leads from the campuses user has access to
                                        C.CampusId = @DefaultCampusId
                                ORDER BY L.ModDate DESC;

                    END;

				

                SET @MruListCount = (
                                      SELECT    COUNT(*) AS MruCountForFirstTimeUser
                                      FROM      @MRUList
                                    );

			

				-- The MRU list from default campus is currently empty

				-- Generate the list again from other campus

                IF @MruListCount = 0
                    BEGIN

                        INSERT  INTO @MRUList
                                (
                                 MRUId
                                ,Counter
                                ,ChildId
                                ,MRUTypeId
                                ,UserId
                                ,CampusId
                                ,SortOrder
                                ,IsSticky
                                ,EmployeeId
                                ,FullName
                                ,CampusDescrip
                                ,ModDate

							    )
                                SELECT DISTINCT TOP 20
                                        NEWID() AS MRUId
                                       ,101 AS Counter
                                       ,  -- not being used so hardcoded a value
                                        L.EmpId AS ChildId
                                       ,3 AS MRUTypeId
                                       ,@UserId AS UserId
                                       ,C.CampusId
                                       ,0 AS SortOrder
                                       ,0 AS IsSticky
                                       ,L.EmpId AS EmployeeId
                                       ,CASE WHEN L.MI IS NULL
                                                  OR L.MI = '' THEN L.FirstName + ' ' + L.LastName
                                             ELSE L.FirstName + ' ' + L.MI + ' ' + L.LastName
                                        END AS FullName
                                       ,C.CampDescrip
                                       ,L.ModDate
                                FROM    dbo.hrEmployees AS L
                                INNER JOIN dbo.syCampuses AS C ON L.CampusId = C.CampusId
                                WHERE   -- MRU List will show leads from the campuses user has access to
                                        C.CampusId IN ( SELECT DISTINCT
                                                                C.CampusId
                                                        FROM    syUsers U -- Commented by Balaji on 5/4/2012

											-- Reason: For first time user we will pick

											-- leads only from default campusid
                                                        INNER JOIN syUsersRolesCampGrps URC ON U.UserId = URC.UserId
                                                        INNER JOIN syCmpGrpCmps CGC ON URC.CampGrpId = CGC.CampGrpId
                                                        INNER JOIN syCampuses C ON C.CampusId = CGC.CampusId
                                                        WHERE   U.UserId = @UserId
                                                                AND C.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' --Order by C.CampDescrip

									)
                                ORDER BY L.ModDate DESC;

                    END;

					

					-- Always keep the count to 20.

					-- If existing users had more than 20 records delete it

                DELETE  FROM syMRUS
                WHERE   UserId = @UserId
                        AND MRUTypeId = 3
                        AND MRUId NOT IN ( SELECT DISTINCT
                                                    MRUId
                                           FROM     @MRUList );



            END;

        ELSE
            BEGIN



                SET @DefaultCampusId = (
                                         SELECT DISTINCT
                                                U.CampusId
                                         FROM   syUsers U
                                         WHERE  U.UserId = @UserId
                                       );

                IF @DefaultCampusId IS NULL
                    BEGIN

                        SET @DefaultCampusId = (
                                                 SELECT DISTINCT TOP 1
                                                        SC.CampusId
                                                 FROM   dbo.syCmpGrpCmps SC
                                                       ,dbo.syUsersRolesCampGrps URC
                                                       ,dbo.syCampuses S
                                                 WHERE  URC.UserId = @UserId
                                                        AND SC.CampGrpId = URC.CampGrpId
                                                        AND S.CampusId = SC.CampusId
                                                        AND S.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'

					   -- Order by S.CampDescrip
                                               );



                    END;



-- no rows so we must fill the MRUs with a default MRU list

                INSERT  INTO @DefaultMRU
                        (
                         ChildId
                        ,MRUTypeId
                        ,UserId
                        ,CampusId
                        ,IsSticky
                        ,ModDate 

		                )
                        SELECT DISTINCT TOP 20
                                S.EmpId AS ChildId
                               ,3 AS MRUTypeId
                               ,@UserId AS UserId
                               ,C.CampusId AS CampusId
                               ,0 AS IsSticky
                               ,S.ModDate
                        FROM    dbo.hrEmployees AS S
                        INNER JOIN dbo.syCampuses AS C ON S.CampusId = C.CampusId
                        WHERE   -- MRU List will show leads from the campuses user has access to
                                C.CampusId = @DefaultCampusId
                        ORDER BY S.ModDate DESC;



                DECLARE @MruCountForFirstTimeUser INT; 

                SET @MruCountForFirstTimeUser = 0;

                SET @MruCountForFirstTimeUser = (
                                                  SELECT    COUNT(*) AS MruCountForFirstTimeUser
                                                  FROM      @DefaultMRU
                                                );

			

			-- There are no leads from default campus, so get leads from other campus

			-- user has access to

                IF @MruCountForFirstTimeUser = 0
                    BEGIN

                        INSERT  INTO @DefaultMRU
                                (
                                 ChildId
                                ,MRUTypeId
                                ,UserId
                                ,CampusId
                                ,IsSticky
                                ,ModDate 

						        )
                                SELECT DISTINCT TOP 20
                                        S.EmpId AS ChildId
                                       ,3 AS MRUTypeId
                                       ,@UserId AS UserId
                                       ,S.CampusId
                                       ,0 AS IsSticky
                                       ,S.ModDate
                                FROM    dbo.hrEmployees AS S
                                WHERE   -- MRU List will show leads from the campuses user has access to
                                        S.CampusId IN ( SELECT DISTINCT
                                                                C.CampusId
                                                        FROM    syUsers U
                                                        INNER JOIN syUsersRolesCampGrps URC ON U.UserId = URC.UserId
                                                        INNER JOIN syCmpGrpCmps CGC ON URC.CampGrpId = CGC.CampGrpId
                                                        INNER JOIN syCampuses C ON C.CampusId = CGC.CampusId
                                                        WHERE   U.UserId = @UserId
                                                                AND C.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' --Order by C.CampDescrip

								)
                                ORDER BY S.ModDate DESC;

                    END; 

                PRINT @MruCountForFirstTimeUser;

			--    Select * from @DefaultMRU     

					







-- Fill the MRU results into the @MRUList table variable



                INSERT  INTO @MRUList
                        (
                         MRUId
                        ,Counter
                        ,ChildId
                        ,MRUTypeId
                        ,UserId
                        ,CampusId
                        ,SortOrder
                        ,IsSticky
                        ,EmployeeId
                        ,FullName
                        ,CampusDescrip
                        ,ModDate

					    )
                        SELECT DISTINCT
                                NEWID() AS MRUId
                               ,101 AS Counter
                               ,ChildId
                               ,MRUTypeId
                               ,UserId
                               ,CampusId
                               ,SortOrder
                               ,IsSticky
                               ,ChildId AS EmployeeId
                               ,(
                                  SELECT    FirstName + ISNULL(MI,'') + LastName
                                  FROM      hrEmployees
                                  WHERE     EmpId = DM.ChildId
                                ) AS FullName
                               ,(
                                  SELECT DISTINCT
                                            CampDescrip
                                  FROM      syCampuses
                                  WHERE     CampusId = DM.CampusId
                                ) AS CampusDescrip
                               ,ModDate
                        FROM    @DefaultMRU DM
                        ORDER BY ModDate DESC;

			   -- Select * from @MRUList

		   /**** User is a first time user - Ends Here ********/ 



-- insert records into the MRU table

                INSERT  INTO dbo.syMRUS
                        (
                         ChildId
                        ,MRUTypeId
                        ,UserId
                        ,CampusId
                        ,SortOrder
                        ,IsSticky
                        ,ModDate  

					    )
                        SELECT  ChildId
                               ,MRUTypeId
                               ,UserId
                               ,CampusId
                               ,SortOrder
                               ,IsSticky
                               ,ModDate
                        FROM    @DefaultMRU;





            END;

--Select * from hrEmployees



-- return the @MRUList table variable

        SELECT  MRUId
               ,Counter
               ,ChildId
               ,MRUTypeId
               ,UserId
               ,CampusId
               ,SortOrder
               ,IsSticky
               ,EmployeeId
               ,FullName
               ,CampusDescrip
               ,ModDate
        FROM    @MRUList
        ORDER BY ModDate DESC; 

    END; 

GO
