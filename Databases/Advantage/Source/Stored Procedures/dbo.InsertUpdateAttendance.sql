SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[InsertUpdateAttendance]
    (
        @UpdateDate DATETIME2
       ,@FirstName VARCHAR(50)
       ,@LastName VARCHAR(50)
       ,@Hours DECIMAL(18,2) = 7.5
    )
AS
    BEGIN
        IF NOT EXISTS (
                      SELECT *
                      FROM   dbo.arStudentClockAttendance SCA
                      JOIN   dbo.arStuEnrollments E ON E.StuEnrollId = SCA.StuEnrollId
                      JOIN   dbo.adLeads L ON E.LeadId = L.LeadId
                      WHERE  SCA.RecordDate = @UpdateDate
                             AND RTRIM(LTRIM(L.FirstName)) = @FirstName
                             AND RTRIM(LTRIM(L.LastName)) = @LastName
                      )
            BEGIN
                SELECT 'inserting ' + @FirstName + ' ' + @LastName + ' on ' + CAST(@UpdateDate AS VARCHAR(50));
                DECLARE @EnrollmentId UNIQUEIDENTIFIER = (
                                                         SELECT TOP 1 E.StuEnrollId
                                                         FROM   dbo.arStuEnrollments E
                                                         JOIN   adLeads L ON L.LeadId = E.LeadId
                                                         WHERE  RTRIM(LTRIM(L.FirstName)) = @FirstName
                                                                AND RTRIM(LTRIM(L.LastName)) = @LastName
                                                         );

                IF @EnrollmentId IS NOT NULL
                    BEGIN

                        DECLARE @ScheduleId UNIQUEIDENTIFIER = (
                                                               SELECT TOP 1 PS.ScheduleId
                                                               FROM   dbo.arStuEnrollments E
                                                               JOIN   dbo.arPrgVersions PV ON PV.PrgVerId = E.PrgVerId
                                                               JOIN   dbo.arProgSchedules PS ON PS.PrgVerId = PV.PrgVerId
                                                               WHERE  E.StuEnrollId = @EnrollmentId
                                                               );


                        IF @ScheduleId IS NOT NULL
                            BEGIN
                                DECLARE @ScheduledHours DECIMAL = (
                                                                  SELECT TOP 1 total
                                                                  FROM   dbo.arProgScheduleDetails
                                                                  WHERE  ScheduleId = @ScheduleId
                                                                         AND DATEPART(WEEKDAY, @UpdateDate) = dw
                                                                  );




                                INSERT INTO dbo.arStudentClockAttendance (
                                                                         StuEnrollId
                                                                        ,ScheduleId
                                                                        ,RecordDate
                                                                        ,SchedHours
                                                                        ,ActualHours
                                                                        ,ModDate
                                                                        ,ModUser
                                                                        ,isTardy
                                                                        ,PostByException
                                                                        ,comments
                                                                        ,TardyProcessed
                                                                        ,Converted
                                                                         )
                                VALUES ( @EnrollmentId                -- StuEnrollId - uniqueidentifier
                                        ,@ScheduleId                  -- ScheduleId - uniqueidentifier
                                        ,@UpdateDate, 
										@ScheduledHours -- SchedHours - decimal(18, 2)
                                        ,@Hours                       -- ActualHours - decimal(18, 2)
                                        ,GETDATE(), 'support'         -- ModUser - varchar(50)
                                        ,NULL                         -- isTardy - bit
                                        ,NULL                         -- PostByException - varchar(10)
                                        ,NULL                         -- comments - varchar(240)
                                        ,NULL                         -- TardyProcessed - bit
                                        ,0                            -- Converted - bit
                                    );
                            END;
                        ELSE
                            BEGIN
                                SELECT 'Schedule not found for ' + @FirstName + ' ' + @LastName + ' EnrollId: ' + CAST(@EnrollmentId AS VARCHAR(50));
                            END;
                    END;
                ELSE
                    BEGIN
                        SELECT 'Enrollment not found for ' + @FirstName + ' ' + @LastName;
                    END;
            END;
        ELSE
            BEGIN
                SELECT 'updating ' + @FirstName + ' ' + @LastName + ' on ' + CAST(@UpdateDate AS VARCHAR(50));
                UPDATE SCA
                SET    SCA.ActualHours = @Hours
                FROM   dbo.arStudentClockAttendance SCA
                JOIN   dbo.arStuEnrollments E ON E.StuEnrollId = SCA.StuEnrollId
                JOIN   dbo.adLeads L ON E.LeadId = L.LeadId
                WHERE  SCA.RecordDate = @UpdateDate
                       AND RTRIM(LTRIM(L.FirstName)) = @FirstName
                       AND RTRIM(LTRIM(L.LastName)) = @LastName;
            END;
    END;

GO
