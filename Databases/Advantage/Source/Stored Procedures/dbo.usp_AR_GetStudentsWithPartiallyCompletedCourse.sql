SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*   
US3466 Transfer Partially Completed Components By Class  
  
CREATED:   
9/5/2013 WMP  
  
PURPOSE:   
Select students that have partially completed selected course to be displayed in transfer components grid   
 A record in arGrdBkResults table (regardless of score) means a grade has been entered (and component is completed)  
  
  
MODIFIED:  
  
*/  
  
CREATE PROCEDURE [dbo].[usp_AR_GetStudentsWithPartiallyCompletedCourse]
    @ReqId UNIQUEIDENTIFIER = NULL
   ,@TermId UNIQUEIDENTIFIER
AS
    BEGIN  
  
        CREATE TABLE #IncludeCompletedComponentsList  
 --verifies that at least 1 completed component exists for student's selected course  
            (
             ResultId UNIQUEIDENTIFIER
            ,StuEnrollId UNIQUEIDENTIFIER
            );  
        INSERT  INTO #IncludeCompletedComponentsList
                (
                 ResultId
                ,StuEnrollId
                )
                SELECT  
   DISTINCT             rs.ResultId
                       ,gbr.StuEnrollId
                FROM    arGrdBkResults gbr
                INNER JOIN arClassSections cs ON cs.ClsSectionId = gbr.ClsSectionId
                INNER JOIN arResults rs ON rs.TestId = cs.ClsSectionId
                                           AND rs.StuEnrollId = gbr.StuEnrollId
                INNER JOIN arGrdBkWgtDetails gbwd ON gbwd.InstrGrdBkWgtDetailId = gbr.InstrGrdBkWgtDetailId
                INNER JOIN arGrdComponentTypes gct ON gct.GrdComponentTypeId = gbwd.GrdComponentTypeId
                WHERE   cs.ReqId = @ReqId
                        AND cs.TermId = @TermId
                        AND gct.SysComponentTypeId <> 544;  
  
        SELECT  rq.ReqId
               ,rs.StuEnrollId
               ,cs.ClsSectionId
               ,cs.TermId
               ,rs.ResultId
               ,CASE WHEN s.MiddleName IS NULL
                          OR s.MiddleName = '' THEN s.LastName + ', ' + s.FirstName
                     ELSE s.LastName + ', ' + s.FirstName + ' ' + s.MiddleName
                END AS FullName
               ,se.StartDate
               ,se.ExpGradDate
               ,0 AS ValidStudent
               ,p.ProgDescrip AS Program
               ,cs.ClsSection
               ,se.ShiftId
               ,cs.ShiftId
        FROM    arResults rs
        INNER JOIN arClassSections cs ON cs.ClsSectionId = rs.TestId
        INNER JOIN arReqs rq ON rq.ReqId = cs.ReqId
        INNER JOIN #IncludeCompletedComponentsList cc ON cc.ResultId = rs.ResultId
        INNER JOIN arStuEnrollments se ON se.StuEnrollId = rs.StuEnrollId
        INNER JOIN arStudent s ON s.StudentId = se.StudentId
        INNER JOIN arPrgVersions pv ON pv.PrgVerId = se.PrgVerId
        INNER JOIN arPrograms p ON p.ProgId = pv.ProgId
        WHERE   cs.ReqId = @ReqId
                AND cs.TermId = @TermId
                AND ISNULL(rs.IsCourseCompleted,0) = 0
                AND se.ShiftId = cs.ShiftId
        ORDER BY s.LastName
               ,s.FirstName;   
   
        DROP TABLE #IncludeCompletedComponentsList;  
   
    END;   



GO
