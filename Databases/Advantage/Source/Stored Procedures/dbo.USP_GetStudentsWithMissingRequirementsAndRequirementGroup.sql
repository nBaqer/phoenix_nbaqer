SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

 
CREATE PROCEDURE [dbo].[USP_GetStudentsWithMissingRequirementsAndRequirementGroup]
    (
     @CampusID UNIQUEIDENTIFIER
    ,@ProgID UNIQUEIDENTIFIER = NULL
    ,@ShiftID UNIQUEIDENTIFIER = NULL
    ,@StatusCodeID UNIQUEIDENTIFIER = NULL
    ,@LeadgrpID UNIQUEIDENTIFIER = NULL 
    )
AS
    BEGIN

        DECLARE @tab1 TABLE
            (
             Descrip VARCHAR(100)
            ,StudentID UNIQUEIDENTIFIER
            );
        INSERT  INTO @tab1
                EXEC USP_GetStudentsWithMissingRequirementsAndRequirementGroup_Enroll @CampusID,@ProgID,@ShiftID,@StatusCodeID,@LeadgrpID;
        DECLARE @tab2 TABLE
            (
             Descrip VARCHAR(100)
            ,StudentID UNIQUEIDENTIFIER
            );
        INSERT  INTO @tab2
                EXEC USP_GetStudentsWithMissingRequirementsAndRequirementGroup_Grad @CampusID,@ProgID,@ShiftID,@StatusCodeID,@LeadgrpID;
        DECLARE @tab3 TABLE
            (
             Descrip VARCHAR(100)
            ,StudentID UNIQUEIDENTIFIER
            );
        INSERT  INTO @tab3
                EXEC USP_GetStudentsWithMissingRequirementsAndRequirementGroup_FinAid @CampusID,@ProgID,@ShiftID,@StatusCodeID,@LeadgrpID;

        SELECT  *
        FROM    @tab1
        UNION
        SELECT  *
        FROM    @tab2
        UNION
        SELECT  *
        FROM    @tab3;

    END;







GO
