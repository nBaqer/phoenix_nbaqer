SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jose Alfredo Garcia Guirado
-- Create date: 11/20/2013
-- Description:	Get all Studentid nd enrollment to create a multiple student report
-- Parameter Descriptions
-- @ExpectedGraduationOperation Accept: 
-- empty string, null = No filter
-- BETWEEN, EQUALTO, NOTEQUALTO, LESSTHAN, GREATERTHAN, ISNULL = filter
-- Other value the query does not return values.   
-- =============================================
CREATE PROCEDURE [dbo].[RS_TranscriptMultipleStudentEnvelope] 
	-- Add the parameters for the stored procedure here
    @CampusList VARCHAR(MAX) = '3F5E839A-589A-4B2A-B258-35A1A8B3B819'
   ,@ProgramVersionList VARCHAR(MAX) = 'DDA3AC62-745A-423B-952D-064D72C675CD,121CE81C-3CF6-4506-89D8-885C0EE86627'
   ,@EnrollmentStatusList VARCHAR(MAX) = '5CD17254-BF5B-4159-8B1C-0E866BB6EA0B,655D594A-0881-46FF-BC73-153FCC9B755B'
   ,@ExpectedGraduationDateStart DATETIME = NULL
   ,@ExpectedGraduationDateEnd DATETIME = NULL
   ,@ExpectedGraduationOperation VARCHAR(20) = ''
AS
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.
        SET NOCOUNT ON;
	-- Set correctly DateTime parameters
        IF @ExpectedGraduationDateEnd IS NULL
            SET @ExpectedGraduationDateEnd = GETDATE();
        IF @ExpectedGraduationDateStart IS NULL
            SET @ExpectedGraduationDateStart = '01-01-1980 11:59:59 PM';

	-- Create temp table to get StudentId and StuEnrollId
        DECLARE @Temp TABLE
            (
             StudentId UNIQUEIDENTIFIER NOT NULL
            ,StuEnrollId UNIQUEIDENTIFIER NOT NULL
            );

        INSERT  @Temp
                SELECT  StudentId
                       ,StuEnrollId
                FROM    arStuEnrollments
                WHERE   PrgVerId IN ( SELECT    *
                                      FROM      dbo.MultipleValuesForReportParameters(@ProgramVersionList,',',0) )
                        AND CampusId IN ( SELECT    *
                                          FROM      dbo.MultipleValuesForReportParameters(@CampusList,',',0) )
                        AND (
                              (
                                @EnrollmentStatusList IS NOT NULL
                                AND @EnrollmentStatusList <> ''
                                AND @EnrollmentStatusList <> '00000000-0000-0000-0000-000000000000'
                                AND StatusCodeId IN ( SELECT    *
                                                      FROM      dbo.MultipleValuesForReportParameters(@EnrollmentStatusList,',',0) )
                              )
                              OR @EnrollmentStatusList IS NULL
                              OR @EnrollmentStatusList = ''
                              OR @EnrollmentStatusList = '00000000-0000-0000-0000-000000000000'
                            )
                        AND (
                              ( @ExpectedGraduationOperation = '' )
                              OR ( @ExpectedGraduationOperation IS NULL )
                              OR (
                                   @ExpectedGraduationOperation = 'BETWEEN'
                                   AND CAST(ExpGradDate AS DATE) BETWEEN CAST(@ExpectedGraduationDateStart AS DATE)
                                                                 AND     CAST(@ExpectedGraduationDateEnd AS DATE)
                                 )
                              OR (
                                   @ExpectedGraduationOperation = 'EQUALTO'
                                   AND CAST(ExpGradDate AS DATE) = CAST(@ExpectedGraduationDateStart AS DATE)
                                 )
                              OR (
                                   @ExpectedGraduationOperation = 'NOTEQUALTO'
                                   AND CAST(ExpGradDate AS DATE) <> CAST(@ExpectedGraduationDateStart AS DATE)
                                 )
                              OR (
                                   @ExpectedGraduationOperation = 'LESSTHAN'
                                   AND CAST(ExpGradDate AS DATE) < CAST(@ExpectedGraduationDateStart AS DATE)
                                 )
                              OR (
                                   @ExpectedGraduationOperation = 'GREATERTHAN'
                                   AND CAST(ExpGradDate AS DATE) > CAST(@ExpectedGraduationDateStart AS DATE)
                                 )
                              OR (
                                   @ExpectedGraduationOperation = 'ISNULL'
                                   AND CAST(ExpGradDate AS DATE) IS NULL
                                 )
                            );
							  	
	--SELECT @ExpectedGraduationOperation, CAST(@ExpectedGraduationDateStart AS DATE)					  	  
	--SELECT * FROM @Temp -- TEST TEMP					  
	-- Convert table to has a row for all StudentId with  field with all enrollment comma delimiter.				  
        SELECT  StudentId
               ,STUFF((
                        SELECT  ',' + CONVERT(NVARCHAR(50),StuEnrollId)
                        FROM    @Temp T
                        WHERE   T.StudentId = S.StudentId
                      FOR
                        XML PATH('')
                      ),1,1,'') AS EnrollmentList
        FROM    @Temp S
        GROUP BY StudentId;	
    END;




GO
