SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[TruncateAllTables]
AS
    DECLARE @TableName VARCHAR(100);
    DECLARE @SQLStatement NVARCHAR(4000);
    DECLARE GetDDL_Cursor CURSOR
    FOR
        SELECT  name
        FROM    sysobjects
        WHERE   type = 'u';

    OPEN GetDDL_Cursor;

    FETCH NEXT FROM GetDDL_Cursor
INTO @TableName;

    WHILE @@FETCH_STATUS = 0
        BEGIN
            SELECT  @SQLStatement = 'truncate table ' + @TableName;
            PRINT @SQLStatement; 
            EXEC sp_executesql @SQLStatement;
            FETCH NEXT FROM GetDDL_Cursor
      INTO @TableName;
        END;
    CLOSE GetDDL_Cursor;
    DEALLOCATE GetDDL_Cursor;



GO
