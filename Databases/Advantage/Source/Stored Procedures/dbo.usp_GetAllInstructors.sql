SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_GetAllInstructors]
    @showActiveOnly BIT
   ,@campusId VARCHAR(50)
AS
    SET NOCOUNT ON;


    IF @showActiveOnly = 1
        BEGIN
            EXEC usp_GetAllActiveInstructors @campusId;
        END; 
    ELSE
        BEGIN
            EXEC usp_GetAllActiveAndInActiveInstructors @campusId;
        END; 
                 




GO
