SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[Usp_PR_Main_DBConfigs]
    @CampusId UNIQUEIDENTIFIER
   ,@EnrollmentId UNIQUEIDENTIFIER = 0x0
AS
    BEGIN
        DECLARE @VaribleTable TABLE
            (
                SchoolName VARCHAR(1000)
               ,DisplayAttendanceUnitForProgressReportByClass VARCHAR(1000)
               ,StudentIdentifier VARCHAR(1000)
               ,GPAMethod VARCHAR(1000)
               ,GradesFormat VARCHAR(1000)
               ,TrackSapAttendance VARCHAR(1000)
               ,GradeBookWeightingLevel VARCHAR(1000)
            );

        INSERT @VaribleTable (
                             SchoolName
                            ,DisplayAttendanceUnitForProgressReportByClass
                            ,StudentIdentifier
                            ,GPAMethod
                            ,GradesFormat
                            ,TrackSapAttendance
                            ,GradeBookWeightingLevel
                             )
        VALUES (
        -- SchoolName - varchar(1000)
        ISNULL((
               SELECT RTRIM(LTRIM(SchoolName))
               FROM   dbo.syCampuses
               WHERE  CampusId = @CampusId
               )
              ,(
               SELECT RTRIM(LTRIM(C.SchoolName))
               FROM   dbo.syCampuses C
               WHERE  C.CampusId IN (
                                    SELECT E.CampusId
                                    FROM   dbo.arStuEnrollments E
                                    WHERE  E.StuEnrollId = @EnrollmentId
                                    )
               )
              )
       -- DisplayAttendanceUnitForProgressReportByClass - varchar(1000)   -- Display Hours
       ,( dbo.GetAppSettingValueByKeyName('DisplayAttendanceUnitForProgressReportByClass', @CampusId))
       -- StudentIdentifier - varchar(1000)
       ,( dbo.GetAppSettingValueByKeyName('StudentIdentifier', @CampusId))
       -- GPAMethod - varchar(1000)
       ,( dbo.GetAppSettingValueByKeyName('GPAMethod', @CampusId))
       -- GradesFormat - varchar(1000)
       ,( dbo.GetAppSettingValueByKeyName('GradesFormat', @CampusId))
       -- TrackSapAttendance - varchar(1000)
       ,( dbo.GetAppSettingValueByKeyName('TrackSapAttendance', @CampusId))
       -- SetGradeBookAt - varchar(1000)
       ,( dbo.GetAppSettingValueByKeyName('GradeBookWeightingLevel', @CampusId)));

        SELECT TOP 1 VT.SchoolName
                    ,VT.DisplayAttendanceUnitForProgressReportByClass
                    ,VT.StudentIdentifier
                    ,VT.GPAMethod
                    ,VT.GradesFormat
                    ,VT.TrackSapAttendance
                    ,VT.GradeBookWeightingLevel
        FROM   @VaribleTable AS VT;

    END;






GO
