SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_MentorProctoredRequirement_Reset]
    @EffectiveDate DATETIME
   ,@ReqId UNIQUEIDENTIFIER
AS
    DECLARE @hDoc INT;
    IF YEAR(@EffectiveDate) <> 1900
        BEGIN
            DELETE  FROM arMentor_GradeComponentTypes_Courses
            WHERE   EffectiveDate = @EffectiveDate
                    AND ReqId = @ReqId;
        END;



GO
