SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_GetResourceRoles] @ResourceId SMALLINT
AS
    BEGIN 
        SELECT DISTINCT
                t1.RoleId
               ,t1.AccessLevel
               ,t2.ResourceURL
        FROM    syRlsResLvls t1
               ,syResources t2
        WHERE   t1.ResourceID = t2.ResourceID
                AND t1.ResourceId = @ResourceId; 
    END; 



GO
