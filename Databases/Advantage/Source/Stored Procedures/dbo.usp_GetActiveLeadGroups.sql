SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_GetActiveLeadGroups]
    @campusId UNIQUEIDENTIFIER
AS
    SET NOCOUNT ON;
    SELECT  L.Descrip
           ,L.LeadGrpId
           ,L.StatusId
           ,1 AS Status
    FROM    adLeadGroups L
           ,syStatuses ST
    WHERE   L.StatusId = ST.StatusId
            AND L.UseForScheduling = 1
            AND ST.Status = 'Active'
            AND L.CampGrpId IN ( SELECT DISTINCT
                                        CampGrpId
                                 FROM   syCmpGrpCmps
                                 WHERE  CampusId = @campusId
                                        AND CampGrpId NOT IN ( SELECT DISTINCT
                                                                        CampGrpId
                                                               FROM     syCampGrps
                                                               WHERE    CampGrpDescrip = 'ALL' )
                                 UNION
                                 SELECT DISTINCT
                                        CampGrpId
                                 FROM   syCampGrps
                                 WHERE  CampGrpDescrip = 'ALL' )
    ORDER BY Status
           ,L.Descrip; 





GO
