SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[USP_SA_GetDefaultChargeCodeForTransCodeandStudent_GetList]
    (
     @StuEnrollId UNIQUEIDENTIFIER
    ,@TransCodeID UNIQUEIDENTIFIER
    )
AS /*----------------------------------------------------------------------------------------------------
	Author          :	Saraswathi Lakshmanan
    
    Create date		:	05/7/2010
    
	Procedure Name	:	[USP_SA_GetDefaultChargeCodeForTransCodeandStudent_GetList]

	Objective		:	Get the default charge  for the selected student and a given transcode
	
	Parameters		:	Name			Type		Data Type				Required? 	
						=====			====		=========				=========	
						@StuEnrollId	In			UniqueIDENTIFIER		Required
						@TransCodeID	In			UniqueIDENTIFIER		Required
						
						
	Output			:	Returns the default charge code for the selected student and a given transcode
						
*/-----------------------------------------------------------------------------------------------------

    BEGIN

        SELECT  A.FeeLevelID
               ,A.SysTransCodeID
               ,A.PeriodCanChange
        FROM    saPrgVerDefaultChargePeriods A
               ,saTransCodes B
               ,arStuEnrollments D
        WHERE   A.PrgVerID = D.PrgVerId
                AND A.SysTransCodeID = B.SysTransCodeId
                AND B.TransCodeId = @TransCodeID
                AND D.StuEnrollId = @StuEnrollId;

    END;




GO
