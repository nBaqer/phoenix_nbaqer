SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[USP_AR_TimeClockSpecialCode_Delete]
    (
     @TCSId UNIQUEIDENTIFIER
    )
AS /*----------------------------------------------------------------------------------------------------
	Author          :	Janet Robinson
    
    Create date		:	09/23/2011
    
	Procedure Name	:	[USP_AR_TimeClockSpecialCode_Delete]

	Objective		:	deletes record from table arTimeClockSpecialCode
	
	Parameters		:	Name						Type	Data Type				Required? 	
						=====						====	=========				=========	
						@TCSId						In		UniqueIDENTIFIER		Required
						
	Output			:			
						
*/-----------------------------------------------------------------------------------------------------

    BEGIN

        DELETE  arTimeClockSpecialCode
        WHERE   TCSId = @TCSId;
		 
    END;



GO
