SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_SHSAPSkillLevel_getlist]
AS
    SELECT DISTINCT
            RTRIM(CONVERT(VARCHAR(10),t4.MinimumScore)) + t3.Code AS Descrip
           ,t3.Descrip AS Description
           ,t4.GrdComponentTypeId_ReqId
           ,t4.MinimumScore
           ,CONVERT(VARCHAR(36),t3.GrdComponentTypeId) + ':' + RTRIM(CONVERT(VARCHAR(10),t4.MinimumScore)) AS ValueId
    FROM    arBridge_GradeComponentTypes_Courses t1
    INNER JOIN arGrdComponentTypes t3 ON t1.GrdComponentTypeId = t3.GrdComponentTypeId
    INNER JOIN arRules_GradeComponentTypes_Courses t4 ON t1.GrdComponentTypeId_ReqId = t4.GrdComponentTypeId_ReqId
    WHERE   t3.SysComponentTypeId = 612
    ORDER BY t4.MinimumScore;



GO
