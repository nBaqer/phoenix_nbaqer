SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--=================================================================================================
-- USP_ProcessPaymentPeriods
--=================================================================================================
-- =============================================
-- Author: Ginzo, John
-- Create date: 11/13/2014
-- Description: Create Payment Period Batch and post to ledger
-- =============================================
CREATE PROCEDURE [dbo].[USP_ProcessPaymentPeriods]
AS
    BEGIN
        -- SET NOCOUNT ON added to prevent extra result sets from
        -- interfering with SELECT statements.
        SET NOCOUNT ON;


        BEGIN TRAN PAYMENTPERIODTRANSACTION;


        ------------------------------------------------------------------------------------------------------------
        ----Table to store Student population
        -- @MasterStudentPopulation table - RowNumber, RunningCreditsEarned, RunningCreditsAttempted columns added
        ------------------------------------------------------------------------------------------------------------
        DECLARE @MasterStudentPopulationSummary TABLE
            (
                RowNumber INT
               ,StudentId UNIQUEIDENTIFIER
               ,StuEnrollId UNIQUEIDENTIFIER
               ,PrgVerId UNIQUEIDENTIFIER
               ,PrgVerCode VARCHAR(250)
               ,ClsSection VARCHAR(250)
               ,TermId UNIQUEIDENTIFIER
               ,StartDate DATETIME
               ,EndDate DATETIME
               ,Code VARCHAR(250)
               ,Credits DECIMAL
               ,CreditsAttempted DECIMAL
               ,CreditsEarned DECIMAL
               ,IsCourseCompleted BIT
               ,IsCreditsAttempted BIT
               ,IsCreditsEarned BIT
            );


        DECLARE @MasterStudentPopulation TABLE
            (
                RowNumber INT
               ,StudentId UNIQUEIDENTIFIER
               ,StuEnrollId UNIQUEIDENTIFIER
               ,PrgVerId UNIQUEIDENTIFIER
               ,PrgVerCode VARCHAR(250)
               ,ClsSection VARCHAR(250)
               ,TermId UNIQUEIDENTIFIER
               ,StartDate DATETIME
               ,EndDate DATETIME
               ,Code VARCHAR(250)
               ,Credits DECIMAL
               ,CreditsAttempted DECIMAL
               ,CreditsEarned DECIMAL
               ,IsCourseCompleted BIT
               ,IsCreditsAttempted BIT
               ,IsCreditsEarned BIT
               ,RunningCreditsEarned DECIMAL(18, 2)
               ,RunningCreditsAttempted DECIMAL(18, 2)
            );


        -----------------------------------------------------------------
        -- Insert student population into table
        -----------------------------------------------------------------
        INSERT INTO @MasterStudentPopulationSummary
                    SELECT   ROW_NUMBER() OVER (PARTITION BY SE.StuEnrollId
                                                ORDER BY SE.StuEnrollId
                                               ) AS RowNumber
                            ,SE.StudentId
                            ,SE.StuEnrollId
                            ,PV.PrgVerId
                            ,PV.PrgVerCode
                            ,CS.ClsSection
                            ,tm.TermId
                            ,tm.StartDate
                            ,tm.EndDate
                            ,R.Code
                            ,R.Credits
                            --CASE WHEN IsCreditsAttempted=1 THEN r.credits ELSE 0 END AS CreditsAttempted,
                            ,CASE WHEN CS.StartDate <= GETDATE() THEN R.Credits
                                  ELSE 0
                             END AS CreditsAttempted
                            ,CASE WHEN IsCreditsEarned = 1 THEN R.Credits
                                  ELSE 0
                             END AS CreditsEarned
                            ,RES.IsCourseCompleted
                            ,CASE WHEN CS.StartDate <= GETDATE() THEN 1
                                  ELSE 0
                             END AS IsCreditsAttempted
                            ,g.IsCreditsEarned
                    FROM     arStuEnrollments SE
                    INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                    INNER JOIN arResults RES ON RES.StuEnrollId = SE.StuEnrollId
                    INNER JOIN arClassSections CS ON RES.TestId = CS.ClsSectionId
                    INNER JOIN arReqs R ON CS.ReqId = R.ReqId
                    INNER JOIN dbo.arTerm tm ON CS.TermId = tm.TermId
                    LEFT OUTER JOIN dbo.arGradeSystemDetails g ON RES.GrdSysDetailId = g.GrdSysDetailId
                    INNER JOIN dbo.syStatusCodes SC ON SE.StatusCodeId = SC.StatusCodeId
                    INNER JOIN dbo.syStatuses ST ON PV.StatusId = ST.StatusId
                    WHERE    SC.SysStatusId IN ( 9, 20 )
					--removed due to this condition being required for by term or course charges and auto post is for payment period charging
                             --AND 1 = dbo.UDF_ShouldClassBeChargedOnThisEnrollment(SE.StuEnrollId, RES.TestId) --Added by Troy as part of fix for US8005
                             AND SE.DisableAutoCharge <> 1 --Exclude enrollments with auto charge disabled 
					--AND IsCourseCompleted=1
                    --AND SE.StuEnrollId='5182EBE8-DF7A-47A9-8B48-3F2949EA56E4'
                    ORDER BY SE.StuEnrollId
                            ,tm.StartDate;






        -----------------------------------------------------------------
        -- Insert student population into table for transfer grades
        -----------------------------------------------------------------
        INSERT INTO @MasterStudentPopulationSummary
                    SELECT ROW_NUMBER() OVER (PARTITION BY SE.StuEnrollId
                                              ORDER BY SE.StuEnrollId
                                             ) AS RowNumber
                          ,SE.StudentId
                          ,SE.StuEnrollId
                          ,PV.PrgVerId
                          ,PV.PrgVerCode
                          ,NULL AS ClsSection
                          ,tm.TermId
                          ,tm.StartDate
                          ,tm.EndDate
                          ,R.Code
                          ,R.Credits
                          --CASE WHEN IsCreditsAttempted=1 THEN r.credits ELSE 0 END AS CreditsAttempted,
                          ,CASE WHEN tm.StartDate <= GETDATE() THEN R.Credits
                                ELSE 0
                           END AS CreditsAttempted
                          ,CASE WHEN IsCreditsEarned = 1 THEN R.Credits
                                ELSE 0
                           END AS CreditsEarned
                          ,RES.IsCourseCompleted
                          --g.IsCreditsAttempted,
                          ,CASE WHEN tm.StartDate <= GETDATE() THEN 1
                                ELSE 0
                           END AS IsCreditsAttempted
                          ,g.IsCreditsEarned
                    FROM   arStuEnrollments SE
                    INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                    INNER JOIN dbo.arTransferGrades RES ON RES.StuEnrollId = SE.StuEnrollId
                    INNER JOIN arReqs R ON RES.ReqId = R.ReqId
                    INNER JOIN dbo.arTerm tm ON RES.TermId = tm.TermId
                    LEFT OUTER JOIN dbo.arGradeSystemDetails g ON RES.GrdSysDetailId = g.GrdSysDetailId
                    INNER JOIN dbo.syStatusCodes SC ON SE.StatusCodeId = SC.StatusCodeId
                    INNER JOIN dbo.syStatuses ST ON PV.StatusId = ST.StatusId
                    WHERE  SC.SysStatusId IN ( 9, 20 )
						   --removed due to this condition being required for by term or course charges and auto post is for payment period charging
                           --AND 1 = dbo.UDF_ShouldCourseBeChargedOnThisEnrollment(SE.StuEnrollId, RES.ReqId) --Added by Troy as part of fix for US8005
        				   AND SE.DisableAutoCharge <> 1 --Exclude enrollments with auto charge disabled 
		--AND IsCourseCompleted=1 
        --AND SE.StuEnrollId='5182EBE8-DF7A-47A9-8B48-3F2949EA56E4'
        IF @@ERROR <> 0
            BEGIN
                ROLLBACK TRAN PAYMENTPERIODTRANSACTION;
                RAISERROR('Error creating Student Population.', 16, 1);
                RETURN;
            END;
        -----------------------------------------------------------------






        INSERT INTO @MasterStudentPopulation
                    SELECT *
                          ,(
                           SELECT SUM(CreditsEarned)
                           FROM   @MasterStudentPopulationSummary
                           WHERE  StuEnrollId = a.StuEnrollId
                                  AND RowNumber <= a.RowNumber
                           ) AS RunningCreditsEarned
                          ,(
                           SELECT SUM(CreditsAttempted)
                           FROM   @MasterStudentPopulationSummary
                           WHERE  StuEnrollId = a.StuEnrollId
                                  AND RowNumber <= a.RowNumber
                           ) AS RunningCreditsAttempted
                    FROM   @MasterStudentPopulationSummary a;
        --WHERE a.StuEnrollId='4E0018F6-7EEA-41F4-A04B-09EC018ACCF8'




        IF @@ERROR <> 0
            BEGIN
                ROLLBACK TRAN PAYMENTPERIODTRANSACTION;
                RAISERROR('Error creating Student Population.', 16, 1);
                RETURN;
            END;
        -----------------------------------------------------------------


        -----------------------------------------------------------------
        --Table to store sum of credits
        -----------------------------------------------------------------
        DECLARE @CreditAttemptedEarned TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,PrgVerId UNIQUEIDENTIFIER
               ,CreditsAttempted DECIMAL
               ,CreditsEarned DECIMAL
            );
        -----------------------------------------------------------------




        -----------------------------------------------------------------
        -- GET sum of credits for students
        -----------------------------------------------------------------
        INSERT INTO @CreditAttemptedEarned
                    SELECT   T.StuEnrollId
                            ,T.PrgVerId
                            ,SUM(T.CreditsAttempted) AS TotalCreditsAttempted
                            ,SUM(T.CreditsEarned) AS TotalCreditsEarned
                    FROM     @MasterStudentPopulation T
                    GROUP BY T.StuEnrollId
                            ,T.PrgVerId
                    ORDER BY T.PrgVerId;


        IF @@ERROR <> 0
            BEGIN
                ROLLBACK TRAN PAYMENTPERIODTRANSACTION;
                RAISERROR('Error creating Student credits summary.', 16, 1);
                RETURN;
            END;
        -----------------------------------------------------------------


        -----------------------------------------------------------------
        -- CREATE Batch Header record for this batch
        -----------------------------------------------------------------
        DECLARE @BatchNumber INT;
        SET @BatchNumber = (
                           SELECT ISNULL(MAX(PmtPeriodBatchHeaderId), 0) + 1
                           FROM   saPmtPeriodBatchHeaders
                           );


        INSERT INTO saPmtPeriodBatchHeaders (
                                            BatchName
                                            )
                    SELECT 'Batch ' + CAST(@BatchNumber AS VARCHAR(20)) + ' (Credits) - ' + LEFT(CONVERT(VARCHAR, GETDATE(), 101), 10) + ' '
                           + LEFT(CONVERT(VARCHAR, GETDATE(), 108), 10);
        IF @@ERROR <> 0
            BEGIN
                ROLLBACK TRAN PAYMENTPERIODTRANSACTION;
                RAISERROR('Error creating Batch header.', 16, 1);
                RETURN;
            END;


        DECLARE @InsertedHeaderId INT;
        SET @InsertedHeaderId = SCOPE_IDENTITY();
        -----------------------------------------------------------------
        -----------------------------------------------------------------
        -- Get Students that satisfy requirements of payment periods and
        -- insert into batch table
        -----------------------------------------------------------------
        INSERT INTO saPmtPeriodBatchItems (
                                          PmtPeriodId
                                         ,PmtPeriodBatchHeaderId
                                         ,StuEnrollId
                                         ,ChargeAmount
                                         ,CreditsHoursValue
                                         ,TransactionReference
                                         ,TransactionDescription
                                         ,ModUser
                                         ,ModDate
                                         ,TermId
                                          )
                    SELECT DISTINCT (p.PmtPeriodId)
                          ,@InsertedHeaderId
                          ,e.StuEnrollId
                          ,p.ChargeAmount
                          ,(CASE WHEN i.IncrementType = 2 THEN CAE.CreditsAttempted
                                 WHEN i.IncrementType = 3 THEN CAE.CreditsEarned
                                 ELSE 0
                            END
                           ) AS CreditsHoursValue
                          ,'Payment Period ' + CAST(p.PeriodNumber AS VARCHAR(5)) AS TransactionReference
                          ,'Tuition: ' + CAST(p.CumulativeValue AS VARCHAR(20)) + ' ' + i.IncrementName AS TransactionDescription
                          ,'AutoPost' AS ModUser
                          ,GETDATE() AS ModDate
                          ,(CASE WHEN i.IncrementType = 2 THEN (
                                                               SELECT TOP 1 TermId
                                                               FROM   @MasterStudentPopulation
                                                               WHERE  StuEnrollId = e.StuEnrollId
                                                                      AND RunningCreditsAttempted >= p.CumulativeValue
                                                               )
                                 WHEN i.IncrementType = 3 THEN (
                                                               SELECT TOP 1 TermId
                                                               FROM   @MasterStudentPopulation
                                                               WHERE  StuEnrollId = e.StuEnrollId
                                                                      AND RunningCreditsEarned >= p.CumulativeValue
                                                               )
                                 ELSE NULL
                            END
                           ) AS TermId
                    FROM   saPmtPeriods p
                    INNER JOIN dbo.saIncrements i ON p.IncrementId = i.IncrementId
                    INNER JOIN dbo.saBillingMethods B ON i.BillingMethodId = B.BillingMethodId
                    INNER JOIN dbo.arPrgVersions prg ON B.BillingMethodId = prg.BillingMethodId
                    INNER JOIN dbo.arStuEnrollments e ON prg.PrgVerId = e.PrgVerId
                    INNER JOIN dbo.arStudent st ON e.StudentId = st.StudentId
                    INNER JOIN @CreditAttemptedEarned CAE ON e.StuEnrollId = CAE.StuEnrollId
                    WHERE  B.BillingMethod = 3
                           AND e.StartDate >= i.EffectiveDate
                           AND e.StuEnrollId NOT IN (
                                                    SELECT StuEnrollId
                                                    FROM   dbo.saTransactions
                                                    WHERE  StuEnrollId = e.StuEnrollId
                                                           AND PmtPeriodId = p.PmtPeriodId
                                                    )
                           AND e.StuEnrollId NOT IN (
                                                    SELECT StuEnrollId
                                                    FROM   dbo.saPmtPeriodBatchItems i
                                                    INNER JOIN saPmtPeriodBatchHeaders h ON i.PmtPeriodBatchHeaderId = h.PmtPeriodBatchHeaderId
                                                    WHERE  StuEnrollId = e.StuEnrollId
                                                           AND PmtPeriodId = p.PmtPeriodId
                                                           AND h.IsPosted = 0
                                                    )
                           AND (CASE WHEN i.IncrementType = 2 THEN CAE.CreditsAttempted
                                     WHEN i.IncrementType = 3 THEN CAE.CreditsEarned
                                     ELSE 0
                                END
                               ) >= p.CumulativeValue;


        IF @@ERROR <> 0
            BEGIN
                ROLLBACK TRAN PAYMENTPERIODTRANSACTION;
                RAISERROR('Error creating batch items.', 16, 1);
                RETURN;
            END;


        -----------------------------------------------------------------




        -----------------------------------------------------------------
        -- UPDATE Batch Item count in header table
        -----------------------------------------------------------------
        UPDATE saPmtPeriodBatchHeaders
        SET    BatchItemCount = (
                                SELECT COUNT(*)
                                FROM   saPmtPeriodBatchHeaders h
                                INNER JOIN saPmtPeriodBatchItems i ON h.PmtPeriodBatchHeaderId = i.PmtPeriodBatchHeaderId
                                WHERE  h.PmtPeriodBatchHeaderId = @InsertedHeaderId
                                )
        WHERE  PmtPeriodBatchHeaderId = @InsertedHeaderId;


        IF @@ERROR <> 0
            BEGIN
                ROLLBACK TRAN PAYMENTPERIODTRANSACTION;
                RAISERROR('Error updating batch count.', 16, 1);
                RETURN;
            END;
        -----------------------------------------------------------------
        -----------------------------------------------------------------
        -- GET AutoPost config setting
        -----------------------------------------------------------------
        DECLARE @IsAutoPost VARCHAR(50);
        SET @IsAutoPost = (
                          SELECT TOP 1 v.Value
                          FROM   dbo.syConfigAppSettings c
                          INNER JOIN dbo.syConfigAppSetValues v ON c.SettingId = v.SettingId
                          WHERE  c.KeyName = 'PaymentPeriodAutoPost'
                          );
        -----------------------------------------------------------------


        -----------------------------------------------------------------
        -- INSERT Transactions from Batch
        -----------------------------------------------------------------
        IF @IsAutoPost = 'yes'
            BEGIN
                IF OBJECTPROPERTY(OBJECT_ID('Trigger_Insert_Check_saTransactions'), 'IsTrigger') = 1
                    BEGIN
                        ALTER TABLE dbo.saTransactions DISABLE TRIGGER Trigger_Insert_Check_saTransactions;
                    END;


                INSERT INTO dbo.saTransactions (
                                               StuEnrollId
                                              ,TermId
                                              ,CampusId
                                              ,TransDate
                                              ,TransCodeId
                                              ,TransReference
                                              ,AcademicYearId
                                              ,TransDescrip
                                              ,TransAmount
                                              ,TransTypeId
                                              ,IsPosted
                                              ,CreateDate
                                              ,BatchPaymentId
                                              ,ViewOrder
                                              ,IsAutomatic
                                              ,ModUser
                                              ,ModDate
                                              ,Voided
                                              ,FeeLevelId
                                              ,FeeId
                                              ,PaymentCodeId
                                              ,FundSourceId
                                              ,StuEnrollPayPeriodId
                                              ,DisplaySequence
                                              ,SecondDisplaySequence
                                              ,PmtPeriodId
                                               )
                            SELECT   BI.StuEnrollId AS StuEnrollId
                                    ,BI.TermId AS TermId
                                    ,SE.CampusId
                                    ,GETDATE() AS TransDate
                                    ,(
                                     SELECT TOP 1 TransCodeId
                                     FROM   dbo.saTransCodes
                                     WHERE  TransCodeCode = 'AUTOPOSTTUIT'
                                     ) AS TranscodeId
                                    ,BI.TransactionReference
                                    ,NULL AS AcademicYearId
                            --??? HOW TO GET THIS?,
                                    ,BI.TransactionDescription
                                    ,BI.ChargeAmount
                                    ,(
                                     SELECT TOP 1 TransTypeId
                                     FROM   dbo.saTransTypes
                                     WHERE  Description = 'Charge'
                                     ) AS TransTypeId
                                    ,1 AS IsPosted
                                    ,GETDATE() AS CreatedDate
                                    ,NULL AS BatchPaymentId
                                    ,NULL AS ViewOrder
                                    ,0 AS IsAutomatic
                                    ,'AutoPost' AS ModUser
                                    ,GETDATE() AS ModDate
                                    ,0 AS Voided
                                    ,NULL AS FeeLevelId
                                    ,NULL AS FeeId
                                    ,NULL AS PaymentCodeId
                                    ,NULL AS FundSourceId
                                    ,NULL AS StuEnrollPayPeriodId
                                    ,NULL AS DisplaySequence
                                    ,NULL AS SecondDisplaySequence
                                    ,BI.PmtPeriodId AS PmtPeriodId
                            FROM     saPmtPeriodBatchItems BI
                            INNER JOIN saPmtPeriodBatchHeaders BH ON BI.PmtPeriodBatchHeaderId = BH.PmtPeriodBatchHeaderId
                            INNER JOIN dbo.arStuEnrollments SE ON BI.StuEnrollId = SE.StuEnrollId
                            WHERE    BH.PmtPeriodBatchHeaderId = @InsertedHeaderId
                                     AND BI.IncludedInPost = 1
                                     AND SE.StuEnrollId NOT IN (
                                                               SELECT StuEnrollId
                                                               FROM   dbo.saTransactions
                                                               WHERE  StuEnrollId = SE.StuEnrollId
                                                                      AND PmtPeriodId = BI.PmtPeriodId
                                                               )
                            ORDER BY SE.StuEnrollId
                                    ,BI.TransactionReference;


                IF @@ERROR <> 0
                    BEGIN
                        ROLLBACK TRAN PAYMENTPERIODTRANSACTION;
                        RAISERROR('Error inserting transactions.', 16, 1);
                        RETURN;
                    END;


                IF OBJECTPROPERTY(OBJECT_ID('Trigger_Insert_Check_saTransactions'), 'IsTrigger') = 1
                    BEGIN
                        ALTER TABLE dbo.saTransactions ENABLE TRIGGER Trigger_Insert_Check_saTransactions;
                    END;


                -----------------------------------------------------------------
                -- update batch items with the transaction id from the ledger
                -----------------------------------------------------------------
                UPDATE saPmtPeriodBatchItems
                SET    TransactionId = T.TransactionId
                FROM   dbo.saTransactions T
                INNER JOIN dbo.saPmtPeriodBatchItems BI ON T.PmtPeriodId = BI.PmtPeriodId
                                                           AND T.StuEnrollId = BI.StuEnrollId
                INNER JOIN dbo.saPmtPeriodBatchHeaders BH ON BI.PmtPeriodBatchHeaderId = BH.PmtPeriodBatchHeaderId
                WHERE  BH.PmtPeriodBatchHeaderId = @InsertedHeaderId;


                IF @@ERROR <> 0
                    BEGIN
                        ROLLBACK TRAN PAYMENTPERIODTRANSACTION;
                        RAISERROR('Error updating transaction ids in batch.', 16, 1);
                        RETURN;
                    END;
                -----------------------------------------------------------------


                -----------------------------------------------------------------
                -- Set Batch to IsPosted
                -----------------------------------------------------------------
                UPDATE dbo.saPmtPeriodBatchHeaders
                SET    IsPosted = 1
                      ,PostedDate = GETDATE()
                WHERE  PmtPeriodBatchHeaderId = @InsertedHeaderId;


                IF @@ERROR <> 0
                    BEGIN
                        ROLLBACK TRAN PAYMENTPERIODTRANSACTION;
                        RAISERROR('Error updating batch is posted flag.', 16, 1);
                        RETURN;
                    END;
                -----------------------------------------------------------------




                -----------------------------------------------------------------
                -- Set IsCharged flag for payment periods from this batch
                -----------------------------------------------------------------
                UPDATE dbo.saPmtPeriods
                SET    IsCharged = 1
                FROM   saPmtPeriodBatchItems I
                INNER JOIN dbo.saPmtPeriods P ON I.PmtPeriodId = P.PmtPeriodId
                WHERE  I.PmtPeriodBatchHeaderId = @InsertedHeaderId;


                IF @@ERROR <> 0
                    BEGIN
                        ROLLBACK TRAN PAYMENTPERIODTRANSACTION;
                        RAISERROR('Error updating batch is posted flag.', 16, 1);
                        RETURN;
                    END;
                -----------------------------------------------------------------


                -----------------------------------------------------------------
                -- Set IsBillingMethodCharged flag for Program Version from this batch
                -----------------------------------------------------------------
                UPDATE P
                SET    P.IsBillingMethodCharged = 1
                FROM   dbo.arPrgVersions P
                INNER JOIN dbo.arStuEnrollments E ON P.PrgVerId = E.PrgVerId
                INNER JOIN saPmtPeriodBatchItems I ON E.StuEnrollId = I.StuEnrollId
                INNER JOIN dbo.saPmtPeriodBatchHeaders H ON I.PmtPeriodBatchHeaderId = H.PmtPeriodBatchHeaderId
                WHERE  H.PmtPeriodBatchHeaderId = @InsertedHeaderId
                       AND I.TransactionId IS NOT NULL
                       AND I.IncludedInPost = 1;
                IF @@ERROR <> 0
                    BEGIN
                        ROLLBACK TRAN PAYMENTPERIODTRANSACTION;
                        RAISERROR('Error updating program version ischarged flag.', 16, 1);
                        RETURN;
                    END;
                -----------------------------------------------------------------




                COMMIT TRAN PAYMENTPERIODTRANSACTION;


            END;
    END;
--=================================================================================================
-- END  --  USP_ProcessPaymentPeriods
--=================================================================================================
GO
