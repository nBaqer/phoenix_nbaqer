SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[CSP_TempStatusChangeHistoryTable]
AS
    BEGIN

        SET NOCOUNT ON; 

        IF EXISTS ( SELECT  *
                    FROM    TempStatusChangeHistoryTbl )
            BEGIN 
                TRUNCATE TABLE dbo.TempStatusChangeHistoryTbl;

                DBCC	CHECKIDENT ('TempStatusChangeHistoryTbl',RESEED,1);
            END;
       
    END;
GO
