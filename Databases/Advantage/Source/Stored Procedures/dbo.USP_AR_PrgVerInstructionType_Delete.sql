SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[USP_AR_PrgVerInstructionType_Delete]
    (
     @PrgVerInstructionTypeId UNIQUEIDENTIFIER
    )
AS /*----------------------------------------------------------------------------------------------------
	Author          :	Janet Robinson
    
    Create date		:	08/11/2011
    
	Procedure Name	:	[USP_AR_PrgVerInstructionType_Delete]

	Objective		:	deletes record from table arPrgVerInstructionType
	
	Parameters		:	Name						Type	Data Type				Required? 	
						=====						====	=========				=========	
						@PrgVerInstructionTypeId	In		UniqueIDENTIFIER		Required
						
	Output			:			
						
*/-----------------------------------------------------------------------------------------------------

    BEGIN

        DELETE  arPrgVerInstructionType
        WHERE   PrgVerInstructionTypeId = @PrgVerInstructionTypeId;
		 
    END;



GO
