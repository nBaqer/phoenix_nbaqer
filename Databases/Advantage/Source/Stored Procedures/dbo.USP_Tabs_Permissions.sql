SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

--sp_helptext USP_Tabs_Permissions
CREATE PROCEDURE [dbo].[USP_Tabs_Permissions]
    @ModuleResourceId INT
   ,@TabId INT
AS
    DECLARE @ShowRossOnlyTabs BIT
       ,@SchedulingMethod VARCHAR(50)
       ,@TrackSAPAttendance VARCHAR(50);
    DECLARE @ShowCollegeOfCourtReporting VARCHAR(5)
       ,@FameESP VARCHAR(5)
       ,@EdExpress VARCHAR(5); 
    DECLARE @GradeBookWeightingLevel VARCHAR(20)
       ,@ShowExternshipTabs VARCHAR(5);

    DECLARE @AR_ParentId UNIQUEIDENTIFIER
       ,@PL_ParentId UNIQUEIDENTIFIER
       ,@FA_ParentId UNIQUEIDENTIFIER;
    DECLARE @FAC_ParentId UNIQUEIDENTIFIER
       ,@SA_ParentId UNIQUEIDENTIFIER
       ,@AD_ParentId UNIQUEIDENTIFIER;
	
-- Get Values
--SET @SchoolEnumerator = 1689
--SET @ShowRossOnlyTabs = (SELECT VALUE FROM dbo.syConfigAppSetValues WHERE SettingId=68)
--SET @SchedulingMethod = (SELECT VALUE FROM dbo.syConfigAppSetValues WHERE SettingId=65)
--SET @TrackSAPAttendance = (SELECT VALUE FROM dbo.syConfigAppSetValues WHERE SettingId=72)
--SET @ShowCollegeOfCourtReporting = (SELECT VALUE FROM dbo.syConfigAppSetValues WHERE SettingId=118)
--SET @FameESP = (SELECT VALUE FROM dbo.syConfigAppSetValues WHERE SettingId=37)
--SET @EdExpress = (SELECT VALUE FROM dbo.syConfigAppSetValues WHERE SettingId=91)
--SET @GradeBookWeightingLevel = (SELECT VALUE FROM dbo.syConfigAppSetValues WHERE SettingId=43)
--SET @ShowExternshipTabs = (SELECT VALUE FROM dbo.syConfigAppSetValues WHERE SettingId=71)


-- Get Values
--SET @SchoolEnumerator = 1689


    SET @AD_ParentId = (
                         SELECT t1.HierarchyId
                         FROM   syNavigationNodes t1
                         INNER JOIN syNavigationNodes t2 ON t1.ParentId = t2.HierarchyId
                         WHERE  t1.ResourceId = 395
                                AND t2.ResourceId = 189
                       );

    SELECT  *
	--CASE WHEN AccessLevel=15 THEN 1 ELSE 0 END AS FullPermission,
	--CASE WHEN AccessLevel IN (14,13,12,11,10,9,8) THEN 1 ELSE 0 END AS EditPermission,
	--CASE WHEN AccessLevel IN (14,13,12,7,6,5,4) THEN 1 ELSE 0 END AS AddPermission,
	--CASE WHEN AccessLevel IN (14,11,10,7,6,2) THEN 1 ELSE 0 END AS DeletePermission,
	--CASE WHEN AccessLevel IN (13,11,9,7,5,3,1) THEN 1 ELSE 0 END AS ReadPermission
    FROM    (
              SELECT    189 AS ModuleResourceId
                       ,395 AS TabId
                       ,ResourceID AS ChildResourceId
                       ,Resource AS ChildResource
                       ,
	---- (SELECT AccessLevel FROM syRlsResLvls WHERE RoleId=@RoleId AND ResourceId=syResources.ResourceId) AS AccessLevel,
                        ResourceURL AS ChildResourceURL
                       ,NULL AS ParentResourceId
                       ,NULL AS ParentResource
                       ,1 AS GroupSortOrder
                       ,NULL AS FirstSortOrder
                       ,NULL AS DisplaySequence
                       ,ResourceTypeID
              FROM      syResources
              WHERE     ResourceID = 189
              UNION
              SELECT    189 AS ModuleResourceId
                       ,395 AS TabId
                       ,NNChild.ResourceId AS ChildResourceId
                       ,CASE WHEN NNChild.ResourceId = 395 THEN 'Lead Tabs'
                             ELSE CASE WHEN NNChild.ResourceId = 398 THEN 'Add/View Leads'
                                       ELSE RChild.Resource
                                  END
                        END AS ChildResource
                       ,
	---- (SELECT AccessLevel FROM syRlsResLvls WHERE RoleId=@RoleId AND ResourceId=RChild.ResourceID) AS AccessLevel,
                        RChild.ResourceURL AS ChildResourceURL
                       ,CASE WHEN (
                                    NNParent.ResourceId IN ( 395 )
                                    OR NNChild.ResourceId IN ( 737,740,741 )
                                  ) THEN NULL
                             ELSE NNParent.ResourceId
                        END AS ParentResourceId
                       ,RParent.Resource AS ParentResource
                       ,CASE WHEN (
                                    NNChild.ResourceId = 737
                                    OR NNParent.ResourceId IN ( 737 )
                                  ) THEN 1
                             ELSE CASE WHEN (
                                              NNChild.ResourceId = 740
                                              OR NNParent.ResourceId IN ( 740 )
                                            ) THEN 2
                                       ELSE 3
                                  END
                        END AS GroupSortOrder
                       ,CASE WHEN NNChild.ResourceId = 737 THEN 1
                             ELSE CASE WHEN NNChild.ResourceId = 395 THEN 2
                                       ELSE 3
                                  END
                        END AS FirstSortOrder
                       ,CASE WHEN (
                                    NNChild.ResourceId IN ( 737 )
                                    OR NNParent.ResourceId IN ( 737 )
                                  ) THEN 1
                             ELSE 2
                        END AS DisplaySequence
                       ,RChild.ResourceTypeID
              FROM      syResources RChild
              INNER JOIN syNavigationNodes NNChild ON RChild.ResourceID = NNChild.ResourceId
              INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
              INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
              LEFT OUTER JOIN (
                                SELECT  *
                                FROM    syResources
                                WHERE   ResourceTypeID = 1
                              ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
              WHERE     RChild.ResourceTypeID IN ( 2,3,8 )
                        AND (
                              RChild.ChildTypeId IS NULL
                              OR RChild.ChildTypeId = 3
                            )
                        AND ( RChild.ResourceID NOT IN ( 394 ) )
                        AND (
                              NNParent.ParentId IN ( SELECT HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = 395 )
                              OR NNChild.HierarchyId IN ( SELECT DISTINCT
                                                                    HierarchyId
                                                          FROM      syNavigationNodes
                                                          WHERE     ResourceId IN ( 737,740,741 )
                                                                    AND ParentId IN ( SELECT    HierarchyId
                                                                                      FROM      syNavigationNodes
                                                                                      WHERE     ResourceId = 395
                                                                                                AND ParentId IN ( SELECT    HierarchyId
                                                                                                                  FROM      syNavigationNodes
                                                                                                                  WHERE     ResourceId = 189 ) ) )
                            )
              UNION ALL
	/********************Academic Records *****************************/
              SELECT    26 AS ModuleResourceId
                       ,394 AS TabId
                       ,ResourceID AS ChildResourceId
                       ,Resource AS ChildResource
                       ,
	-- (SELECT AccessLevel FROM syRlsResLvls WHERE RoleId=@RoleId AND ResourceId=syResources.ResourceId) AS AccessLevel,
                        ResourceURL AS ChildResourceURL
                       ,NULL AS ParentResourceId
                       ,NULL AS ParentResource
                       ,1 AS GroupSortOrder
                       ,NULL AS FirstSortOrder
                       ,NULL AS DisplaySequence
                       ,ResourceTypeID
              FROM      syResources
              WHERE     ResourceID = 26
              UNION
              SELECT    26 AS ModuleResourceId
                       ,394 AS TabId
                       ,ChildResourceId
                       ,ChildResource
                       ,
	-- (SELECT AccessLevel FROM syRlsResLvls WHERE RoleId=@RoleId AND ResourceId=t1.ChildResourceId) AS AccessLevel,
                        ChildResourceURL
                       ,ParentResourceId
                       ,ParentResource
                       ,CASE WHEN (
                                    t1.ChildResourceId = 737
                                    OR t1.ParentResourceId IN ( 737 )
                                  ) THEN 1
                             ELSE CASE WHEN (
                                              t1.ChildResourceId = 738
                                              OR t1.ParentResourceId IN ( 738 )
                                            ) THEN 2
                                       ELSE CASE WHEN (
                                                        t1.ChildResourceId = 739
                                                        OR t1.ParentResourceId IN ( 739 )
                                                      ) THEN 3
                                                 ELSE CASE WHEN (
                                                                  t1.ChildResourceId = 740
                                                                  OR t1.ParentResourceId IN ( 740 )
                                                                ) THEN 4
                                                           ELSE CASE WHEN (
                                                                            t1.ChildResourceId = 741
                                                                            OR t1.ParentResourceId IN ( 741 )
                                                                          ) THEN 5
                                                                     ELSE 6
                                                                END
                                                      END
                                            END
                                  END
                        END AS GroupSortOrder
                       ,CASE WHEN ChildResourceId = 738 THEN 1
                             ELSE 2
                        END AS FirstSortOrder
                       ,CASE WHEN (
                                    ChildResourceId IN ( 738 )
                                    OR ParentResourceId IN ( 738 )
                                  ) THEN 1
                             ELSE 2
                        END AS DisplaySequence
                       ,ResourceTypeID
              FROM      (
                          SELECT DISTINCT
                                    NNChild.ResourceId AS ChildResourceId
                                   ,CASE WHEN NNChild.ResourceId = 409 THEN 'IPEDS - General Reports'
                                         ELSE RChild.Resource
                                    END AS ChildResource
                                   ,RChild.ResourceURL AS ChildResourceURL
                                   ,CASE WHEN (
                                                NNParent.ResourceId IN ( 689 )
                                                OR NNChild.ResourceId IN ( 737,738,739,740,741,742 )
                                              ) THEN NULL
                                         ELSE NNParent.ResourceId
                                    END AS ParentResourceId
                                   ,RParent.Resource AS ParentResource
                                   ,RParentModule.Resource AS MODULE
                                   ,RChild.ResourceTypeID
                          FROM      syResources RChild
                          INNER JOIN syNavigationNodes NNChild ON RChild.ResourceID = NNChild.ResourceId
                          INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                          INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                          LEFT OUTER JOIN (
                                            SELECT  *
                                            FROM    syResources
                                            WHERE   ResourceTypeID = 1
                                          ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                          WHERE     RChild.ResourceTypeID IN ( 2,3,8 )
                                    AND (
                                          RChild.ChildTypeId IS NULL
                                          OR RChild.ChildTypeId = 3
                                        )
                                    AND ( NNParent.ParentId IN ( SELECT HierarchyId
                                                                 FROM   syNavigationNodes
                                                                 WHERE  ResourceId = 394
                                                                        AND ParentId IN ( SELECT    HierarchyId
                                                                                          FROM      syNavigationNodes
                                                                                          WHERE     ResourceId = 26 ) ) )
                          UNION
                          SELECT DISTINCT
                                    RChild.ResourceID AS ChildResourceId
                                   ,RChild.Resource AS ChildResource
                                   ,RChild.ResourceURL AS ChildResourceURL
                                   ,CASE WHEN ( RChild.ResourceID IN ( 737,738,739,740,741,742 ) ) THEN NULL
                                         ELSE RChild.ResourceID
                                    END AS ParentResourceId
                                   ,NULL AS ParentResource
                                   ,NULL AS MODULE
                                   ,RChild.ResourceTypeID
                          FROM      syResources RChild
                          WHERE     RChild.ResourceID IN ( 737,738,739,740,741,742 )
                        ) t1
              UNION ALL
	/********************Student Accounts *****************************/
              SELECT    194 AS ModuleResourceId
                       ,394 AS TabId
                       ,ResourceID AS ChildResourceId
                       ,Resource AS ChildResource
                       ,
	-- (SELECT AccessLevel FROM syRlsResLvls WHERE RoleId=@RoleId AND ResourceId=syResources.ResourceId) AS AccessLevel,
                        ResourceURL AS ChildResourceURL
                       ,NULL AS ParentResourceId
                       ,NULL AS ParentResource
                       ,1 AS GroupSortOrder
                       ,NULL AS FirstSortOrder
                       ,NULL AS DisplaySequence
                       ,ResourceTypeID
              FROM      syResources
              WHERE     ResourceID = 194
              UNION
              SELECT    194 AS ModuleResourceId
                       ,394 AS TabId
                       ,ChildResourceId
                       ,ChildResource
                       ,
	-- (SELECT AccessLevel FROM syRlsResLvls WHERE RoleId=@RoleId AND ResourceId=t1.ChildResourceId) AS AccessLevel,
                        ChildResourceURL
                       ,ParentResourceId
                       ,ParentResource
                       ,CASE WHEN (
                                    t1.ChildResourceId = 737
                                    OR t1.ParentResourceId IN ( 737 )
                                  ) THEN 1
                             ELSE CASE WHEN (
                                              t1.ChildResourceId = 738
                                              OR t1.ParentResourceId IN ( 738 )
                                            ) THEN 2
                                       ELSE CASE WHEN (
                                                        t1.ChildResourceId = 739
                                                        OR t1.ParentResourceId IN ( 739 )
                                                      ) THEN 3
                                                 ELSE CASE WHEN (
                                                                  t1.ChildResourceId = 740
                                                                  OR t1.ParentResourceId IN ( 740 )
                                                                ) THEN 4
                                                           ELSE CASE WHEN (
                                                                            t1.ChildResourceId = 741
                                                                            OR t1.ParentResourceId IN ( 741 )
                                                                          ) THEN 5
                                                                     ELSE 6
                                                                END
                                                      END
                                            END
                                  END
                        END AS GroupSortOrder
                       ,CASE WHEN ChildResourceId = 737 THEN 1
                             ELSE 2
                        END AS FirstSortOrder
                       ,CASE WHEN (
                                    ChildResourceId IN ( 737,738,740,741 )
                                    OR ParentResourceId IN ( 737,738,740,741 )
                                  ) THEN 1
                             ELSE 2
                        END AS DisplaySequence
                       ,ResourceTypeID
              FROM      (
                          SELECT DISTINCT
                                    NNChild.ResourceId AS ChildResourceId
                                   ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                         ELSE RChild.Resource
                                    END AS ChildResource
                                   ,RChild.ResourceURL AS ChildResourceURL
                                   ,CASE WHEN NNParent.ResourceId IN ( 194 )
                                              OR NNChild.ResourceId IN ( 737,738,739,740,741,742 ) THEN NULL
                                         ELSE NNParent.ResourceId
                                    END AS ParentResourceId
                                   ,RParent.Resource AS ParentResource
                                   ,RParentModule.Resource AS MODULE
                                   ,RChild.ResourceTypeID
                          FROM      syResources RChild
                          INNER JOIN syNavigationNodes NNChild ON RChild.ResourceID = NNChild.ResourceId
                          INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                          INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                          LEFT OUTER JOIN (
                                            SELECT  *
                                            FROM    syResources
                                            WHERE   ResourceTypeID = 1
                                          ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                          WHERE     RChild.ResourceTypeID IN ( 2,3,8 )
                                    AND (
                                          RChild.ChildTypeId IS NULL
                                          OR RChild.ChildTypeId = 3
                                        )
                                    AND ( NNParent.ParentId IN ( SELECT HierarchyId
                                                                 FROM   syNavigationNodes
                                                                 WHERE  ResourceId = 394
                                                                        AND ParentId IN ( SELECT    HierarchyId
                                                                                          FROM      syNavigationNodes
                                                                                          WHERE     ResourceId = 194 ) ) )
                          UNION
                          SELECT DISTINCT
                                    RChild.ResourceID AS ChildResourceId
                                   ,RChild.Resource AS ChildResource
                                   ,RChild.ResourceURL AS ChildResourceURL
                                   ,CASE WHEN ( RChild.ResourceID IN ( 737,738,739,740,741,742 ) ) THEN NULL
                                         ELSE RChild.ResourceID
                                    END AS ParentResourceId
                                   ,NULL AS ParentResource
                                   ,NULL AS MODULE
                                   ,RChild.ResourceTypeID
                          FROM      syResources RChild
                          WHERE     RChild.ResourceID IN ( 737,738,739,740,741,742 )
                        ) t1
              UNION ALL
	/********************FACULTY *****************************/
              SELECT    300 AS ModuleResourceId
                       ,394 AS TabId
                       ,ResourceID AS ChildResourceId
                       ,Resource AS ChildResource
                       ,
	-- (SELECT AccessLevel FROM syRlsResLvls WHERE RoleId=@RoleId AND ResourceId=syResources.ResourceId) AS AccessLevel,
                        ResourceURL AS ChildResourceURL
                       ,NULL AS ParentResourceId
                       ,NULL AS ParentResource
                       ,1 AS GroupSortOrder
                       ,NULL AS FirstSortOrder
                       ,NULL AS DisplaySequence
                       ,ResourceTypeID
              FROM      syResources
              WHERE     ResourceID = 300
              UNION
              SELECT    300 AS ModuleResourceId
                       ,394 AS TabId
                       ,ChildResourceId
                       ,ChildResource
                       ,
	-- (SELECT AccessLevel FROM syRlsResLvls WHERE RoleId=@RoleId AND ResourceId=t1.ChildResourceId) AS AccessLevel,
                        ChildResourceURL
                       ,ParentResourceId
                       ,ParentResource
                       ,CASE WHEN (
                                    t1.ChildResourceId = 737
                                    OR t1.ParentResourceId IN ( 737 )
                                  ) THEN 1
                             ELSE CASE WHEN (
                                              t1.ChildResourceId = 738
                                              OR t1.ParentResourceId IN ( 738 )
                                            ) THEN 2
                                       ELSE CASE WHEN (
                                                        t1.ChildResourceId = 739
                                                        OR t1.ParentResourceId IN ( 739 )
                                                      ) THEN 3
                                                 ELSE CASE WHEN (
                                                                  t1.ChildResourceId = 740
                                                                  OR t1.ParentResourceId IN ( 740 )
                                                                ) THEN 4
                                                           ELSE CASE WHEN (
                                                                            t1.ChildResourceId = 741
                                                                            OR t1.ParentResourceId IN ( 741 )
                                                                          ) THEN 5
                                                                     ELSE 6
                                                                END
                                                      END
                                            END
                                  END
                        END AS GroupSortOrder
                       ,CASE WHEN ChildResourceId = 737 THEN 1
                             ELSE 2
                        END AS FirstSortOrder
                       ,CASE WHEN (
                                    ChildResourceId IN ( 737,739 )
                                    OR ParentResourceId IN ( 737,739 )
                                  ) THEN 1
                             ELSE 2
                        END AS DisplaySequence
                       ,ResourceTypeID
              FROM      (
                          SELECT DISTINCT
                                    NNChild.ResourceId AS ChildResourceId
                                   ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                         ELSE RChild.Resource
                                    END AS ChildResource
                                   ,RChild.ResourceURL AS ChildResourceURL
                                   ,CASE WHEN NNParent.ResourceId = 300
                                              OR NNChild.ResourceId IN ( 737,739,740,741 ) THEN NULL
                                         ELSE NNParent.ResourceId
                                    END AS ParentResourceId
                                   ,RParent.Resource AS ParentResource
                                   ,RParentModule.Resource AS MODULE
                                   ,RChild.ResourceTypeID
                          FROM      syResources RChild
                          INNER JOIN syNavigationNodes NNChild ON RChild.ResourceID = NNChild.ResourceId
                          INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                          INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                          LEFT OUTER JOIN (
                                            SELECT  *
                                            FROM    syResources
                                            WHERE   ResourceTypeID = 1
                                          ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                          WHERE     RChild.ResourceTypeID IN ( 2,3,8 )
                                    AND (
                                          RChild.ChildTypeId IS NULL
                                          OR RChild.ChildTypeId = 3
                                        )
                                    AND ( NNParent.ParentId IN ( SELECT HierarchyId
                                                                 FROM   syNavigationNodes
                                                                 WHERE  ResourceId = 394
                                                                        AND ParentId IN ( SELECT    HierarchyId
                                                                                          FROM      syNavigationNodes
                                                                                          WHERE     ResourceId = 300 ) ) )
                          UNION
                          SELECT DISTINCT
                                    RChild.ResourceID AS ChildResourceId
                                   ,RChild.Resource AS ChildResource
                                   ,RChild.ResourceURL AS ChildResourceURL
                                   ,CASE WHEN ( RChild.ResourceID IN ( 737,739,740,741 ) ) THEN NULL
                                         ELSE RChild.ResourceID
                                    END AS ParentResourceId
                                   ,NULL AS ParentResource
                                   ,NULL AS MODULE
                                   ,RChild.ResourceTypeID
                          FROM      syResources RChild
                          WHERE     RChild.ResourceID IN ( 737,738,739,740,741,742 )
                        ) t1
              UNION
	/********************Financial Aid *****************************/
              SELECT    191 AS ModuleResourceId
                       ,394 AS TabId
                       ,ResourceID AS ChildResourceId
                       ,Resource AS ChildResource
                       ,
	-- (SELECT AccessLevel FROM syRlsResLvls WHERE RoleId=@RoleId AND ResourceId=syResources.ResourceId) AS AccessLevel,
                        ResourceURL AS ChildResourceURL
                       ,NULL AS ParentResourceId
                       ,NULL AS ParentResource
                       ,1 AS GroupSortOrder
                       ,NULL AS FirstSortOrder
                       ,NULL AS DisplaySequence
                       ,ResourceTypeID
              FROM      syResources
              WHERE     ResourceID = 191
              UNION
              SELECT    191 AS ModuleResourceId
                       ,394 AS TabId
                       ,ChildResourceId
                       ,ChildResource
                       ,
	-- (SELECT AccessLevel FROM syRlsResLvls WHERE RoleId=@RoleId AND ResourceId=t1.ChildResourceId) AS AccessLevel,
                        ChildResourceURL
                       ,ParentResourceId
                       ,ParentResource
                       ,CASE WHEN (
                                    t1.ChildResourceId = 737
                                    OR t1.ParentResourceId IN ( 737 )
                                  ) THEN 1
                             ELSE CASE WHEN (
                                              t1.ChildResourceId = 738
                                              OR t1.ParentResourceId IN ( 738 )
                                            ) THEN 2
                                       ELSE CASE WHEN (
                                                        t1.ChildResourceId = 739
                                                        OR t1.ParentResourceId IN ( 739 )
                                                      ) THEN 3
                                                 ELSE CASE WHEN (
                                                                  t1.ChildResourceId = 740
                                                                  OR t1.ParentResourceId IN ( 740 )
                                                                ) THEN 4
                                                           ELSE CASE WHEN (
                                                                            t1.ChildResourceId = 741
                                                                            OR t1.ParentResourceId IN ( 741 )
                                                                          ) THEN 5
                                                                     ELSE 6
                                                                END
                                                      END
                                            END
                                  END
                        END AS GroupSortOrder
                       ,CASE WHEN ChildResourceId = 737 THEN 1
                             ELSE 2
                        END AS FirstSortOrder
                       ,CASE WHEN (
                                    ChildResourceId IN ( 737,738,740,741 )
                                    OR ParentResourceId IN ( 737,738,740,741 )
                                  ) THEN 1
                             ELSE 2
                        END AS DisplaySequence
                       ,ResourceTypeID
              FROM      (
                          SELECT DISTINCT
                                    NNChild.ResourceId AS ChildResourceId
                                   ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                         ELSE RChild.Resource
                                    END AS ChildResource
                                   ,RChild.ResourceURL AS ChildResourceURL
                                   ,CASE WHEN (
                                                NNParent.ResourceId = 191
                                                OR NNChild.ResourceId IN ( 737,738,739,740,741,742 )
                                              ) THEN NULL
                                         ELSE NNParent.ResourceId
                                    END AS ParentResourceId
                                   ,RParent.Resource AS ParentResource
                                   ,RParentModule.Resource AS MODULE
                                   ,RChild.ResourceTypeID
                          FROM      syResources RChild
                          INNER JOIN syNavigationNodes NNChild ON RChild.ResourceID = NNChild.ResourceId
                          INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                          INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                          LEFT OUTER JOIN (
                                            SELECT  *
                                            FROM    syResources
                                            WHERE   ResourceTypeID = 1
                                          ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                          WHERE     RChild.ResourceTypeID IN ( 2,3,8 )
                                    AND (
                                          RChild.ChildTypeId IS NULL
                                          OR RChild.ChildTypeId = 3
                                        )
                                    AND ( NNParent.ParentId IN ( SELECT HierarchyId
                                                                 FROM   syNavigationNodes
                                                                 WHERE  ResourceId = 394
                                                                        AND ParentId IN ( SELECT    HierarchyId
                                                                                          FROM      syNavigationNodes
                                                                                          WHERE     ResourceId = 191 ) ) )
                          UNION
                          SELECT DISTINCT
                                    RChild.ResourceID AS ChildResourceId
                                   ,RChild.Resource AS ChildResource
                                   ,RChild.ResourceURL AS ChildResourceURL
                                   ,CASE WHEN ( RChild.ResourceID IN ( 737,738,739,740,741,742 ) ) THEN NULL
                                         ELSE RChild.ResourceID
                                    END AS ParentResourceId
                                   ,NULL AS ParentResource
                                   ,NULL AS MODULE
                                   ,RChild.ResourceTypeID
                          FROM      syResources RChild
                          WHERE     RChild.ResourceID IN ( 737,738,739,740,741,742 )
                        ) t1
              UNION
	/********************Placement *****************************/
              SELECT    193 AS ModuleResourceId
                       ,394 AS TabId
                       ,ResourceID AS ChildResourceId
                       ,Resource AS ChildResource
                       ,
	-- (SELECT AccessLevel FROM syRlsResLvls WHERE RoleId=@RoleId AND ResourceId=syResources.ResourceId) AS AccessLevel,
                        ResourceURL AS ChildResourceURL
                       ,NULL AS ParentResourceId
                       ,NULL AS ParentResource
                       ,1 AS GroupSortOrder
                       ,NULL AS FirstSortOrder
                       ,NULL AS DisplaySequence
                       ,ResourceTypeID
              FROM      syResources
              WHERE     ResourceID = 193
              UNION
              SELECT    193 AS ModuleResourceId
                       ,394 AS TabId
                       ,ChildResourceId
                       ,ChildResource
                       ,
	-- (SELECT AccessLevel FROM syRlsResLvls WHERE RoleId=@RoleId AND ResourceId=t1.ChildResourceId) AS AccessLevel,
                        ChildResourceURL
                       ,ParentResourceId
                       ,ParentResource
                       ,CASE WHEN (
                                    t1.ChildResourceId = 737
                                    OR t1.ParentResourceId IN ( 737 )
                                  ) THEN 1
                             ELSE CASE WHEN (
                                              t1.ChildResourceId = 738
                                              OR t1.ParentResourceId IN ( 738 )
                                            ) THEN 2
                                       ELSE CASE WHEN (
                                                        t1.ChildResourceId = 739
                                                        OR t1.ParentResourceId IN ( 739 )
                                                      ) THEN 3
                                                 ELSE CASE WHEN (
                                                                  t1.ChildResourceId = 740
                                                                  OR t1.ParentResourceId IN ( 740 )
                                                                ) THEN 4
                                                           ELSE CASE WHEN (
                                                                            t1.ChildResourceId = 741
                                                                            OR t1.ParentResourceId IN ( 741 )
                                                                          ) THEN 5
                                                                     ELSE 6
                                                                END
                                                      END
                                            END
                                  END
                        END AS GroupSortOrder
                       ,CASE WHEN ChildResourceId = 737 THEN 1
                             ELSE 2
                        END AS FirstSortOrder
                       ,CASE WHEN (
                                    ChildResourceId IN ( 737,738,740,741 )
                                    OR ParentResourceId IN ( 737,738,740,741 )
                                  ) THEN 1
                             ELSE 2
                        END AS DisplaySequence
                       ,ResourceTypeID
              FROM      (
                          SELECT DISTINCT
                                    NNChild.ResourceId AS ChildResourceId
                                   ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                         ELSE RChild.Resource
                                    END AS ChildResource
                                   ,RChild.ResourceURL AS ChildResourceURL
                                   ,CASE WHEN (
                                                NNParent.ResourceId = 193
                                                OR NNChild.ResourceId IN ( 713,735 )
                                              ) THEN NULL
                                         ELSE NNParent.ResourceId
                                    END AS ParentResourceId
                                   ,RParent.Resource AS ParentResource
                                   ,RParentModule.Resource AS MODULE
                                   ,RChild.ResourceTypeID
                          FROM      syResources RChild
                          INNER JOIN syNavigationNodes NNChild ON RChild.ResourceID = NNChild.ResourceId
                          INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                          INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                          LEFT OUTER JOIN (
                                            SELECT  *
                                            FROM    syResources
                                            WHERE   ResourceTypeID = 1
                                          ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                          WHERE     RChild.ResourceTypeID IN ( 2,3,8 )
                                    AND (
                                          RChild.ChildTypeId IS NULL
                                          OR RChild.ChildTypeId = 3
                                        )
                                    AND ( NNParent.ParentId IN ( SELECT HierarchyId
                                                                 FROM   syNavigationNodes
                                                                 WHERE  ResourceId = 394
                                                                        AND ParentId IN ( SELECT    HierarchyId
                                                                                          FROM      syNavigationNodes
                                                                                          WHERE     ResourceId = 193 ) ) )
                          UNION
                          SELECT DISTINCT
                                    RChild.ResourceID AS ChildResourceId
                                   ,RChild.Resource AS ChildResource
                                   ,RChild.ResourceURL AS ChildResourceURL
                                   ,CASE WHEN ( RChild.ResourceID IN ( 737,738,739,741 ) ) THEN NULL
                                         ELSE RChild.ResourceID
                                    END AS ParentResourceId
                                   ,NULL AS ParentResource
                                   ,NULL AS MODULE
                                   ,RChild.ResourceTypeID
                          FROM      syResources RChild
                          WHERE     RChild.ResourceID IN ( 737,738,739,740,741,742 )
                        ) t1
              UNION
	/********************Placement - Employer Tabs *****************************/
              SELECT    193 AS ModuleResourceId
                       ,397 AS TabId
                       ,ResourceID AS ChildResourceId
                       ,Resource AS ChildResource
                       ,
	-- (SELECT AccessLevel FROM syRlsResLvls WHERE RoleId=@RoleId AND ResourceId=syResources.ResourceId) AS AccessLevel,
                        ResourceURL AS ChildResourceURL
                       ,NULL AS ParentResourceId
                       ,NULL AS ParentResource
                       ,1 AS GroupSortOrder
                       ,NULL AS FirstSortOrder
                       ,NULL AS DisplaySequence
                       ,ResourceTypeID
              FROM      syResources
              WHERE     ResourceID = 193
              UNION
              SELECT    193 AS ModuleResourceId
                       ,397 AS TabId
                       ,ChildResourceId
                       ,ChildResource
                       ,
	-- (SELECT AccessLevel FROM syRlsResLvls WHERE RoleId=@RoleId AND ResourceId=t1.ChildResourceId) AS AccessLevel,
                        ChildResourceURL
                       ,ParentResourceId
                       ,ParentResource
                       ,CASE WHEN (
                                    t1.ChildResourceId = 737
                                    OR t1.ParentResourceId IN ( 737 )
                                  ) THEN 1
                             ELSE CASE WHEN (
                                              t1.ChildResourceId = 738
                                              OR t1.ParentResourceId IN ( 738 )
                                            ) THEN 2
                                       ELSE CASE WHEN (
                                                        t1.ChildResourceId = 739
                                                        OR t1.ParentResourceId IN ( 739 )
                                                      ) THEN 3
                                                 ELSE CASE WHEN (
                                                                  t1.ChildResourceId = 740
                                                                  OR t1.ParentResourceId IN ( 740 )
                                                                ) THEN 4
                                                           ELSE CASE WHEN (
                                                                            t1.ChildResourceId = 741
                                                                            OR t1.ParentResourceId IN ( 741 )
                                                                          ) THEN 5
                                                                     ELSE 6
                                                                END
                                                      END
                                            END
                                  END
                        END AS GroupSortOrder
                       ,CASE WHEN ChildResourceId = 737 THEN 1
                             ELSE 2
                        END AS FirstSortOrder
                       ,CASE WHEN (
                                    ChildResourceId IN ( 737 )
                                    OR ParentResourceId IN ( 737 )
                                  ) THEN 1
                             ELSE 2
                        END AS DisplaySequence
                       ,ResourceTypeID
              FROM      (
                          SELECT DISTINCT
                                    NNChild.ResourceId AS ChildResourceId
                                   ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                         ELSE RChild.Resource
                                    END AS ChildResource
                                   ,RChild.ResourceURL AS ChildResourceURL
                                   ,CASE WHEN (
                                                NNParent.ResourceId = 193
                                                OR NNChild.ResourceId IN ( 737 )
                                              ) THEN NULL
                                         ELSE NNParent.ResourceId
                                    END AS ParentResourceId
                                   ,RParent.Resource AS ParentResource
                                   ,RParentModule.Resource AS MODULE
                                   ,RChild.ResourceTypeID
                          FROM      syResources RChild
                          INNER JOIN syNavigationNodes NNChild ON RChild.ResourceID = NNChild.ResourceId
                          INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                          INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                          LEFT OUTER JOIN (
                                            SELECT  *
                                            FROM    syResources
                                            WHERE   ResourceTypeID = 1
                                          ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                          WHERE     RChild.ResourceTypeID IN ( 2,3,8 )
                                    AND (
                                          RChild.ChildTypeId IS NULL
                                          OR RChild.ChildTypeId = 3
                                        )
                                    AND ( NNParent.ParentId IN ( SELECT HierarchyId
                                                                 FROM   syNavigationNodes
                                                                 WHERE  ResourceId = 397
                                                                        AND ParentId IN ( SELECT    HierarchyId
                                                                                          FROM      syNavigationNodes
                                                                                          WHERE     ResourceId = 193 ) ) )
                          UNION
                          SELECT DISTINCT
                                    RChild.ResourceID AS ChildResourceId
                                   ,RChild.Resource AS ChildResource
                                   ,RChild.ResourceURL AS ChildResourceURL
                                   ,CASE WHEN ( RChild.ResourceID IN ( 737 ) ) THEN NULL
                                         ELSE RChild.ResourceID
                                    END AS ParentResourceId
                                   ,NULL AS ParentResource
                                   ,NULL AS MODULE
                                   ,RChild.ResourceTypeID
                          FROM      syResources RChild
                          WHERE     RChild.ResourceID IN ( 737 )
                        ) t1
              UNION
	/********************HR - Employee Tabs *****************************/
              SELECT    192 AS ModuleResourceId
                       ,396 AS TabId
                       ,ResourceID AS ChildResourceId
                       ,Resource AS ChildResource
                       ,
	-- (SELECT AccessLevel FROM syRlsResLvls WHERE RoleId=@RoleId AND ResourceId=syResources.ResourceId) AS AccessLevel,
                        ResourceURL AS ChildResourceURL
                       ,NULL AS ParentResourceId
                       ,NULL AS ParentResource
                       ,1 AS GroupSortOrder
                       ,NULL AS FirstSortOrder
                       ,NULL AS DisplaySequence
                       ,ResourceTypeID
              FROM      syResources
              WHERE     ResourceID = 192
              UNION
              SELECT    192 AS ModuleResourceId
                       ,396 AS TabId
                       ,ChildResourceId
                       ,ChildResource
                       ,
	-- (SELECT AccessLevel FROM syRlsResLvls WHERE RoleId=@RoleId AND ResourceId=t1.ChildResourceId) AS AccessLevel,
                        ChildResourceURL
                       ,ParentResourceId
                       ,ParentResource
                       ,CASE WHEN (
                                    ChildResourceId IN ( 713 )
                                    OR ParentResourceId IN ( 713 )
                                  ) THEN 1
                             ELSE 2
                        END AS GroupSortOrder
                       ,CASE WHEN (
                                    ChildResourceId IN ( 713 )
                                    OR ParentResourceId IN ( 713 )
                                  ) THEN 1
                             ELSE 2
                        END AS FirstSortOrder
                       ,CASE WHEN (
                                    ChildResourceId IN ( 713 )
                                    OR ParentResourceId IN ( 713 )
                                  ) THEN 1
                             ELSE 2
                        END AS DisplaySequence
                       ,ResourceTypeID
              FROM      (
                          SELECT DISTINCT
                                    NNChild.ResourceId AS ChildResourceId
                                   ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                         ELSE RChild.Resource
                                    END AS ChildResource
                                   ,RChild.ResourceURL AS ChildResourceURL
                                   ,CASE WHEN (
                                                NNParent.ResourceId = 193
                                                OR NNChild.ResourceId IN ( 713,735 )
                                              ) THEN NULL
                                         ELSE NNParent.ResourceId
                                    END AS ParentResourceId
                                   ,RParent.Resource AS ParentResource
                                   ,RParentModule.Resource AS MODULE
                                   ,RChild.ResourceTypeID
                          FROM      syResources RChild
                          INNER JOIN syNavigationNodes NNChild ON RChild.ResourceID = NNChild.ResourceId
                          INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                          INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                          LEFT OUTER JOIN (
                                            SELECT  *
                                            FROM    syResources
                                            WHERE   ResourceTypeID = 1
                                          ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                          WHERE     RChild.ResourceTypeID IN ( 2,3,8 )
                                    AND (
                                          RChild.ChildTypeId IS NULL
                                          OR RChild.ChildTypeId = 3
                                        )
                                    AND ( NNParent.ParentId IN ( SELECT HierarchyId
                                                                 FROM   syNavigationNodes
                                                                 WHERE  ResourceId = 396
                                                                        AND ParentId IN ( SELECT    HierarchyId
                                                                                          FROM      syNavigationNodes
                                                                                          WHERE     ResourceId = 192 ) ) )
                          UNION
                          SELECT DISTINCT
                                    RChild.ResourceID AS ChildResourceId
                                   ,RChild.Resource AS ChildResource
                                   ,RChild.ResourceURL AS ChildResourceURL
                                   ,CASE WHEN ( RChild.ResourceID IN ( 737 ) ) THEN NULL
                                         ELSE RChild.ResourceID
                                    END AS ParentResourceId
                                   ,NULL AS ParentResource
                                   ,NULL AS MODULE
                                   ,RChild.ResourceTypeID
                          FROM      syResources RChild
                          WHERE     RChild.ResourceID IN ( 737 )
                        ) t1
            ) t2
    WHERE   t2.ChildResourceURL IS NOT NULL
            AND t2.ModuleResourceId = @ModuleResourceId
            AND t2.TabId = @TabId
    ORDER BY GroupSortOrder
           ,ParentResourceId
           ,ChildResource;



GO
