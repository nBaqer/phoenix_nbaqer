SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[Sp_CoursehasLabOrExternAndIsPass]
    (
     @StuEnrollid UNIQUEIDENTIFIER
    ,@ClsSectId UNIQUEIDENTIFIER
    ,@result BIT OUTPUT 
    )
AS
    SET NOCOUNT ON;
    BEGIN 
        DECLARE @ResId INT
           ,@Combination BIT
           ,@Regular BIT
           ,@Lab BIT; 
        SET @Combination = 0; 
        SET @Regular = 0; 
        SET @Lab = 0; 
        --get the SysComponentId To find if the course has a lab Work, lab Hour or Externship 
        --pass the ClsSectionid as parameter to the qry 
        DECLARE c1 CURSOR READ_ONLY
        FOR
            (
              SELECT DISTINCT
                        ( SysComponenttypeId )
              FROM      arClassSections CS
                       ,arGrdBKWeights GBW
                       ,arGrdBkWgtDetails GBWD
                       ,arGrdComponentTypes GCT
              WHERE     ClsSectionid = @ClsSectId
                        AND CS.ReqId = GBW.ReqId
                        AND GBW.InstrGrdBkWgtId = GBWD.InstrGrdBkWgtId
                        AND GBWD.GrdComponentTypeid = GCT.GrdComponentTypeid
                        AND CS.StartDate >= GBW.EffectiveDate
                        AND GBW.EffectiveDate = (
                                                  SELECT    MAX(arGrdBkWeights.EffectiveDate)
                                                  FROM      arGrdBkWeights
                                                  WHERE     ReqId = CS.ReqId
                                                )
            ); 
  
        OPEN c1; 
  
        FETCH NEXT FROM c1 
        INTO @ResId; 
  
        WHILE @@FETCH_STATUS = 0
            BEGIN 
  
                IF @ResId IN ( 500,503,544 )
                    BEGIN 
                        SET @Lab = 1; 
                    END; 
                ELSE
                    BEGIN 
                        SET @Regular = 1; 
                    END; 
                FETCH NEXT FROM c1 
            INTO @ResId; 
  
            END; 
        CLOSE c1; 
        DEALLOCATE c1; 
        --If the course is a lab work, lab hour or Externship then find if they are completed (if completed return true else return false) 
        --else return true 
        IF @Lab = 1
            BEGIN 
  
                DECLARE @InstrGrdBkWgtDetailId UNIQUEIDENTIFIER
                   ,@TotHours DECIMAL
                   ,@Number DECIMAL; 
                DECLARE @SysComponenttypeId INT
                   ,@GrdComponentTypeid UNIQUEIDENTIFIER; 
                DECLARE @Status1 BIT
                   ,@Status2 BIT; 
            --get the InstructorGradeBookWeightDetailId( to get labwork or labhour),The sysComponentId(i.e 500,503,544)and the GradeComponentTypeId(to get externshipHrs) 
                DECLARE c1 CURSOR READ_ONLY
                FOR
                    (
                      SELECT    InstrGrdBkWgtDetailId
                               ,SysComponenttypeId
                               ,GBWD.GrdComponentTypeid
                      FROM      arClassSections CS
                               ,arGrdBKWeights GBW
                               ,arGrdBkWgtDetails GBWD
                               ,arGrdComponentTypes GCT
                      WHERE     ClsSectionid = @ClsSectId
                                AND CS.ReqId = GBW.ReqId
                                AND GBW.InstrGrdBkWgtId = GBWD.InstrGrdBkWgtId
                                AND GBWD.GrdComponentTypeid = GCT.GrdComponentTypeid
                                AND SysComponenttypeId IN ( 500,503,544 )
                                AND CS.StartDate >= GBW.EffectiveDate
                                AND GBW.EffectiveDate = (
                                                          SELECT    MAX(arGrdBkWeights.EffectiveDate)
                                                          FROM      arGrdBkWeights
                                                          WHERE     ReqId = CS.ReqId
                                                        )
                    ); 
  
                OPEN c1; 
  
                FETCH NEXT FROM c1 
            INTO @InstrGrdBkWgtDetailId,@SysComponenttypeId,@GrdComponentTypeid; 
  
                WHILE @@FETCH_STATUS = 0
                    BEGIN 
                        IF @SysComponenttypeId IN ( 500,503 )
                            BEGIN 
                    --Get the hours or Scores obtained for that InstrGrdBkWeightDetailId i.e lab work or lab hour 
                                SET @TotHours = (
                                                  SELECT    SUM(Score)
                                                  FROM      arGrdBkresults
                                                  WHERE     ClsSectionid = @ClsSectId
                                                            AND StuEnrollid = @StuEnrollid
                                                            AND instrGrdBkWgtDetailId = @InstrGrdBkWgtDetailId
                                                ); 
                                SET @Number = (
                                                SELECT  number
                                                FROM    arGrdBkWgtDetails
                                                WHERE   instrGrdBkWgtDetailId = @InstrGrdBkWgtDetailId
                                              ); 
                                IF @TotHours IS NOT NULL
                                    AND @Number IS NOT NULL
                                    BEGIN 
                        ---if they have completed the hours then saet the status as 1 
                        --else set the status =2 
                                        IF @TotHours >= @Number
                                            BEGIN 
                                                SET @Status1 = 1; 
                                            END; 
                                        ELSE
                                            BEGIN 
                                                SET @Status2 = 1; 
                                            END; 
                                    END; 
                                ELSE
                                    BEGIN 
                                        SET @result = 0; 
                    -- Select 'False' 
                                    END; 
                            END; 
                        ELSE
                            IF @SysComponenttypeId IN ( 544 )
                                BEGIN 
                    --Get the hours attended for an externship course 
                                    SET @TotHours = (
                                                      SELECT    SUM(HoursAttended)
                                                      FROM      arExternshipAttendance
                                                      WHERE     StuEnrollid = @StuEnrollid
                                                                AND GrdComponentTypeid = @GrdComponentTypeid
                                                    ); 
                                    SET @Number = (
                                                    SELECT  number
                                                    FROM    arGrdBkWgtDetails
                                                    WHERE   instrGrdBkWgtDetailId = @InstrGrdBkWgtDetailId
                                                  ); 
                                    IF @TotHours IS NOT NULL
                                        AND @Number IS NOT NULL
                                        BEGIN 
                        ---if they have completed the hours then saet the status as 1 
                        --else set the status =2 
                                            IF @TotHours >= @Number
                                                BEGIN 
                                                    SET @Status1 = 1; 
                                                END; 
                                            ELSE
                                                BEGIN 
                                                    SET @Status2 = 1; 
                                                END; 
                                        END; 
                                    ELSE
                                        BEGIN 
                                            SET @result = 0; 
                    -- Select 'False' 
                                        END; 
                                END; 
                            ELSE
                                BEGIN 
                                    SET @result = 0; 
                -- Select 'False' 
                                END; 
  
                        FETCH NEXT FROM c1 
                INTO @InstrGrdBkWgtDetailId,@SysComponenttypeId,@GrdComponentTypeid; 
  
                    END; 
                CLOSE c1; 
                DEALLOCATE c1; 
  
            --if they have not completed then return false 
            --else return true 
                IF @Status2 = 1
                    BEGIN 
                        SET @result = 0; 
            -- Select 'False' 
                    END; 
                ELSE
                    IF @Status1 = 1
                        BEGIN 
                            SET @result = 1; 
            -- Select 'true' 
                        END; 
            END; 
        ELSE
            BEGIN 
                SET @result = 1; 
        -- Select 'True' 
            END; 
    END; 




GO
