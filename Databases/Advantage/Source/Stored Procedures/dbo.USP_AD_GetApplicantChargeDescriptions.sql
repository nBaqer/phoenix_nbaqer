SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[USP_AD_GetApplicantChargeDescriptions]
    (
     @ShowActive NVARCHAR(50)
    ,@CampusId UNIQUEIDENTIFIER
    )
AS /*----------------------------------------------------------------------------------------------------
	Author          :	Janet Robinson
    
    Create date		:	09/24/2012
    
	Procedure Name	:	[USP_AD_GetApplicantChargeDescriptions]

	Objective		:	Get the Charge descriptions for the Applicant Fee system trans code
	
	Parameters		:	Name			Type	Data Type			Required? 	
						=====			====	=========			=========	
						@ShowActive		In		nvarchar			Required
						@CampusId       In      uniqueidentifier    required
	
	Output			:	Returns the trans codes that are attached to systrancodeid = 18 			
						
*/-----------------------------------------------------------------------------------------------------

    BEGIN
        DECLARE @ShowActiveValue NVARCHAR(20);
        IF LOWER(@ShowActive) = 'true'
            BEGIN
                SET @ShowActiveValue = 'Active';
            END;
        ELSE
            BEGIN
                SET @ShowActiveValue = 'InActive';
            END;

        SELECT  TC.TransCodeId
               ,TC.TransCodeCode
               ,TC.TransCodeDescrip
               ,TC.StatusId
               ,TC.CampGrpId
               ,ST.StatusId
               ,ST.Status
        FROM    saTransCodes TC
               ,syStatuses ST
        WHERE   TC.StatusId = ST.StatusId
                AND SysTransCodeId = (
                                       SELECT   SysTransCodeId
                                       FROM     dbo.saSysTransCodes
                                       WHERE    Description = 'Applicant Fee'
                                     )
                AND ST.Status = @ShowActiveValue
                AND TC.CampGrpId IN ( SELECT    campgrpid
                                      FROM      sycmpgrpcmps
                                      WHERE     campusid = @CampusId )
        ORDER BY TC.TransCodeDescrip;
		
    END;




GO
