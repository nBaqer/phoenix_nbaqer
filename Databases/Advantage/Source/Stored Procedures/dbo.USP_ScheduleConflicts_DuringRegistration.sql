SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_ScheduleConflicts_DuringRegistration]
    @CurrentClassSectionId VARCHAR(8000)
   ,@StuEnrollId VARCHAR(8000)
AS
    BEGIN
        DECLARE @countDateRangeConflicts INT; 
        SET @countDateRangeConflicts = 0;

        DECLARE @ClassesWithOverlappingDates TABLE
            (
             CourseDescrip VARCHAR(50)
            ,ClsSectionDescrip VARCHAR(50)
            ,ClsSectionId UNIQUEIDENTIFIER
            ,RoomId UNIQUEIDENTIFIER
            ,StartDate DATETIME
            ,EndDate DATETIME
            );

        DECLARE @ClassesWithOverlappingDays TABLE
            (
             StartDate DATETIME
            ,EndDate DATETIME
            ,WorkDaysDescrip VARCHAR(50)
            ,StartTime VARCHAR(50)
            ,EndTime VARCHAR(50)
            ,ViewOrder INT
            ,ClsSection VARCHAR(50)
            ,CourseDescrip VARCHAR(50)
            ,ClsSectionId VARCHAR(50)
            ,CourseId VARCHAR(50)
            );
											
        DECLARE @ClassesWithOverlappingTimeInterval TABLE
            (
             StartDate DATETIME
            ,EndDate DATETIME
            ,WorkDaysDescrip VARCHAR(50)
            ,StartTime VARCHAR(50)
            ,EndTime VARCHAR(50)
            ,ViewOrder INT
            ,ClsSection VARCHAR(50)
            ,CourseDescrip VARCHAR(50)
            ,ClsSectionId VARCHAR(50)
            ,CourseId VARCHAR(50)
            );


        INSERT  INTO @ClassesWithOverlappingDates
                SELECT  *
                FROM    (
                          SELECT DISTINCT
                                    A1.Descrip
                                   ,A1.ClsSection
                                   ,A1.ClsSectionId
                                   ,A1.RoomId
                                   ,A1.StartDate
                                   ,A1.EndDate
                          FROM      (
                                      -- Get the classes the student is currently registered in
                                      SELECT DISTINCT
                                                t3.*
                                               ,t4.Descrip
                                               ,t2.ClsSection
                                               ,t2.TermId
                                               ,t2.InstructorId
                                      FROM      arClassSections t2
                                               ,arClsSectMeetings t3
                                               ,arReqs t4
                                               ,arResults t5
                                      WHERE     t2.ClsSectionId = t3.ClsSectionId
                                                AND t2.ReqId = t4.ReqId
                                                AND t2.ClsSectionId = t5.TestId
                                                AND ( t5.StuEnrollId IN ( SELECT    Val
                                                                          FROM      MultipleValuesForReportParameters(@StuEnrollId,',',1) ) )
                                    ) A1
                                   ,(
                                      -- Get the class details of the class the student is trying to register in
                                      SELECT DISTINCT
                                                t3.*
                                               ,t4.Descrip
                                               ,t2.ClsSection
                                               ,t2.TermId
                                               ,t2.InstructorId
                                      FROM      arClassSections t2
                                               ,arClsSectMeetings t3
                                               ,arReqs t4
                                      WHERE     t2.ClsSectionId = t3.ClsSectionId
                                                AND t2.ReqId = t4.ReqId
                                                AND ( t2.ClsSectionId IN ( SELECT   Val
                                                                           FROM     MultipleValuesForReportParameters(@CurrentClassSectionId,',',1) ) )
                                    ) A2
                          WHERE     A1.StartDate <= A2.EndDate
                                    AND A1.EndDate >= A2.StartDate
                                    AND A1.ClsSectionId <> A2.ClsSectionId
                                    AND -- Different classes
			--Condition commented by Balaji on Dec 1st 2011
			--While checking conflicts for students not needed to see whether rooms match or not
			--Bottomline : Student cannot attend two classes at the same time
			--(A1.RoomId=A2.RoomId  OR A1.InstructorId=A2.InstructorId) AND -- Same room 
                                    A1.TermId = A2.TermId -- Same Term
                        ) dt;
		
	--SELECT * FROM @ClassesWithOverlappingDates
	-- If there are classes with overlapping days in same term
	-- Get the days on which they overlap
        INSERT  INTO @ClassesWithOverlappingDays
                SELECT DISTINCT
                        A1.StartDate
                       ,A1.EndDate
                       ,A1.WorkDaysDescrip
                       ,A1.StartTime
                       ,A1.EndTime
                       ,A1.ViewOrder
                       ,A1.ClsSection
                       ,A1.Descrip AS CourseDescrip
                       ,A1.ClsSectionId
                       ,A1.ReqId
                FROM    (
                          SELECT DISTINCT
                                    t3.ClsSectMeetingId
                                   ,t3.StartDate
                                   ,t3.EndDate
                                   ,t4.Descrip
                                   ,t2.ClsSection
                                   ,t2.TermId
                                   ,(
                                      SELECT    CONVERT(VARCHAR,TimeIntervalDescrip,108)
                                      FROM      dbo.cmTimeInterval
                                      WHERE     TimeIntervalId = t5.StartTimeId
                                    ) AS StartTime
                                   ,(
                                      SELECT    CONVERT(VARCHAR,TimeIntervalDescrip,108)
                                      FROM      dbo.cmTimeInterval
                                      WHERE     TimeIntervalId = t5.EndTimeId
                                    ) AS EndTime
                                   ,t7.WorkDaysDescrip
                                   ,t7.ViewOrder
                                   ,t2.ClsSectionId
                                   ,t4.ReqId
                          FROM      arClassSections t2
                                   ,arClsSectMeetings t3
                                   ,arReqs t4
                                   ,syPeriods t5
                                   ,dbo.syPeriodsWorkDays t6
                                   ,plWorkDays t7
                          WHERE     t2.ClsSectionId = t3.ClsSectionId
                                    AND t2.ReqId = t4.ReqId
                                    AND t3.PeriodId = t5.PeriodId
                                    AND t5.PeriodId = t6.PeriodId
                                    AND t6.WorkDayId = t7.WorkDaysId
                                    AND t2.ClsSectionId IN ( SELECT DISTINCT
                                                                    ClsSectionId
                                                             FROM   @ClassesWithOverlappingDates )
                        ) A1
                       ,(
                          SELECT DISTINCT
                                    t3.ClsSectMeetingId
                                   ,t3.StartDate
                                   ,t3.EndDate
                                   ,t4.Descrip
                                   ,t2.ClsSection
                                   ,t2.TermId
                                   ,(
                                      SELECT    CONVERT(VARCHAR,TimeIntervalDescrip,108)
                                      FROM      dbo.cmTimeInterval
                                      WHERE     TimeIntervalId = t5.StartTimeId
                                    ) AS StartTime
                                   ,(
                                      SELECT    CONVERT(VARCHAR,TimeIntervalDescrip,108)
                                      FROM      dbo.cmTimeInterval
                                      WHERE     TimeIntervalId = t5.EndTimeId
                                    ) AS EndTime
                                   ,t7.WorkDaysDescrip
                                   ,t7.ViewOrder
                          FROM      arClassSections t2
                                   ,arClsSectMeetings t3
                                   ,arReqs t4
                                   ,syPeriods t5
                                   ,dbo.syPeriodsWorkDays t6
                                   ,plWorkDays t7
                          WHERE     t2.ClsSectionId = t3.ClsSectionId
                                    AND t2.ReqId = t4.ReqId
                                    AND t3.PeriodId = t5.PeriodId
                                    AND t5.PeriodId = t6.PeriodId
                                    AND t6.WorkDayId = t7.WorkDaysId
                                    AND ( t2.ClsSectionId IN ( SELECT   Val
                                                               FROM     MultipleValuesForReportParameters(@CurrentClassSectionId,',',1) ) )
                        ) A2
                WHERE   A1.WorkDaysDescrip = A2.WorkDaysDescrip
                ORDER BY A1.ViewOrder
                       ,A1.ClsSection
                       ,A1.StartDate
                       ,A1.EndDate
                       ,A1.StartTime
                       ,A1.EndTime;
	
   --SELECT * FROM @ClassesWithOverlappingDays
    -- If classes overlap on a day check the timings to see if there is a overlap
        INSERT  INTO @ClassesWithOverlappingTimeInterval
                SELECT  A2.*
                FROM    (
                          SELECT DISTINCT
                                    t3.ClsSectMeetingId
                                   ,t3.StartDate
                                   ,t3.EndDate
                                   ,t4.Descrip
                                   ,t2.ClsSection
                                   ,t2.TermId
                                   ,(
                                      SELECT    CONVERT(VARCHAR,TimeIntervalDescrip,108)
                                      FROM      dbo.cmTimeInterval
                                      WHERE     TimeIntervalId = t5.StartTimeId
                                    ) AS StartTime
                                   ,(
                                      SELECT    CONVERT(VARCHAR,TimeIntervalDescrip,108)
                                      FROM      dbo.cmTimeInterval
                                      WHERE     TimeIntervalId = t5.EndTimeId
                                    ) AS EndTime
                                   ,t7.WorkDaysDescrip
                                   ,t7.ViewOrder
                          FROM      arClassSections t2
                                   ,arClsSectMeetings t3
                                   ,arReqs t4
                                   ,syPeriods t5
                                   ,dbo.syPeriodsWorkDays t6
                                   ,plWorkDays t7
                          WHERE     t2.ClsSectionId = t3.ClsSectionId
                                    AND t2.ReqId = t4.ReqId
                                    AND t3.PeriodId = t5.PeriodId
                                    AND t5.PeriodId = t6.PeriodId
                                    AND t6.WorkDayId = t7.WorkDaysId
                                    AND t2.ClsSectionId IN ( SELECT Val
                                                             FROM   MultipleValuesForReportParameters(@CurrentClassSectionId,',',1) )
                        ) A1
                       ,@ClassesWithOverlappingDays A2
                WHERE   --(A1.StartDate>=A2.StartDate OR A1.StartDate <= A2.EndDate) AND
                        (
                          A1.StartDate <= A2.EndDate
                          AND A1.EndDate >= A2.StartDate
                        )
                        AND ( A1.WorkDaysDescrip = A2.WorkDaysDescrip )
                        AND (
                              A1.StartTime < A2.EndTime
                              AND A1.EndTime > A2.StartTime
                            );
	
	--SELECT DISTINCT CourseDescrip,ClsSection,StartDate,EndDate,WorkDaysDescrip,StartTime,EndTime FROM @ClassesWithOverlappingTimeInterval
        SELECT DISTINCT
                CourseId
               ,CourseDescrip
               ,ClsSectionId
               ,ClsSection
               ,CONVERT(VARCHAR,StartDate,101) + '- ' + CONVERT(VARCHAR,EndDate,101) AS ClassPeriod
               ,WorkDaysDescrip + ' (' + StartTime + ' - ' + EndTime + ')' AS MeetingSchedule
               ,ViewOrder
               ,StartDate
               ,EndDate
        FROM    @ClassesWithOverlappingTimeInterval
        ORDER BY CourseDescrip
               ,ClsSection
               ,StartDate
               ,EndDate
               ,ViewOrder;
    END;
GO
