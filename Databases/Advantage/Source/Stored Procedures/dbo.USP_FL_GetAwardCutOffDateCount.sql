SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[USP_FL_GetAwardCutOffDateCount]
    @AwardCutOffDateGUID VARCHAR(50)
AS
    SELECT TOP 1
            AwardCutOffDateCount
    FROM    syFameESPAwardCutOffDate
    WHERE   AwardCutOffDateGUID = @AwardCutOffDateGUID;




GO
