SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[USP_IsProgramVersion_ProgramClockHourType]
    (
     @prgVerID UNIQUEIDENTIFIER
    )
AS
    SET NOCOUNT ON;
    SELECT  arPrograms.ACId
    FROM    arprgversions
           ,arprograms
           ,syAcademicCalendars
    WHERE   arPrgVersions.ProgId = arPrograms.ProgId
            AND syAcademicCalendars.ACId = arPrograms.ACId
            AND ACDescrip LIKE 'Clock%'
            AND PrgVerId = @prgVerID;



GO
