SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[USP_AR_AreStudentsRegisteredinCourse]
    (
     @PrgVerID UNIQUEIDENTIFIER
    ,@ReqID UNIQUEIDENTIFIER
    )
AS /*----------------------------------------------------------------------------------------------------
    Author : Saraswathi Lakshmanan
    
    Create date : 06/18/2010
    
    Procedure Name : USP_AR_AreStudentsRegisteredinCourse

    Objective : get the Count
    
    Parameters : Name Type Data Type 
                        ===== ==== ========= 
                        @prgVerID In UniqueIdentifier 
                        @ReqID In UniqueIdentifier 
                            
    
    Output : Returns the ReqID 
    

    --To find if any students are registered in this course.
                    
*/-----------------------------------------------------------------------------------------------------
    
    DECLARE @VariableReqtable TABLE
        (
         ReqId UNIQUEIDENTIFIER
        ,GrpID UNIQUEIDENTIFIER
        ,ID INT IDENTITY
        ); 
    
    INSERT  INTO @VariableReqtable
            SELECT  arReqGrpDef.Reqid
                   ,arReqGrpDef.GrpID
            FROM    arReqGrpDef
            WHERE   GrpID = @ReqID; 
    
    DECLARE @MaxRowCounter INT
       ,@RowCounter INT;
    SET @MaxRowCounter = (
                           SELECT   MAX(ID)
                           FROM     @VariableReqtable
                         );
    SET @RowCounter = 1;
    WHILE ( @RowCounter <= @MaxRowCounter )
        BEGIN
            INSERT  INTO @VariableReqtable
                    SELECT  ReqId
                           ,GrpID
                    FROM    arReqGrpDef
                    WHERE   GrpID IN ( SELECT   ReqId
                                       FROM     @VariableReqtable
                                       WHERE    ID = @RowCounter ); 
   
            SET @RowCounter = @RowCounter + 1;
            SET @MaxRowCounter = (
                                   SELECT   MAX(ID)
                                   FROM     @VariableReqtable
                                 ); 
        END;

    SELECT  COUNT(*)
    FROM    (
              SELECT    ResultID
              FROM      arResults t1
                       ,arClassSections t2
                       ,arReqs t3
                       ,arStuEnrollments t4
              WHERE     t1.TestID = t2.ClsSectionId
                        AND t2.ReqId = t3.ReqId
                        AND t1.StuEnrollId = t4.StuEnrollID
                        AND t3.ReqId = @ReqID
                        AND t4.PrgVerId = @PrgVerID
              UNION
              SELECT    ResultID
              FROM      arResults t1
                       ,arClassSections t2
                       ,arReqs t3
                       ,arStuEnrollments t4
              WHERE     t1.TestID = t2.ClsSectionId
                        AND t2.ReqId = t3.ReqId
                        AND t1.StuEnrollId = t4.StuEnrollID
                        AND t3.ReqId IN ( SELECT    ReqId
                                          FROM      @VariableReqtable )
                        AND t4.PrgVerId = @PrgVerID
            ) AS Result;




GO
