SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_GetFERPACategories]
    @showActiveOnly INTEGER
   ,@campusId VARCHAR(50)
AS
    SET NOCOUNT ON;
                 
    IF @showActiveOnly = 1
        BEGIN
            EXEC usp_GetActiveFERPACategories @campusId;
        END; 
    ELSE
        IF @showActiveOnly = 0
            BEGIN
                EXEC usp_GetInActiveFERPACategories @campusId;
            END;
        ELSE
            BEGIN
                EXEC usp_GetAllFERPACategories @campusId;
            END;



GO
