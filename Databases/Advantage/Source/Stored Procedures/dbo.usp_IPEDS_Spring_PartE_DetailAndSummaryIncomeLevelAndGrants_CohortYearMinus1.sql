SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[usp_IPEDS_Spring_PartE_DetailAndSummaryIncomeLevelAndGrants_CohortYearMinus1]
    @CampusId VARCHAR(50)
   ,@ProgId VARCHAR(4000) = NULL
   ,@CohortYear VARCHAR(10) = NULL
   ,@OrderBy VARCHAR(100)
   ,@StartDate DATETIME
   ,@EndDate DATETIME
   ,@SchoolType VARCHAR(50)
   ,@InstitutionType VARCHAR(50)
   ,@EndDate_Academic DATETIME = NULL
   ,@LargeProgramId VARCHAR(50) = NULL
AS
    DECLARE @ReturnValue VARCHAR(100)
       ,@Value VARCHAR(100);
    DECLARE @SSN VARCHAR(10)
       ,@FirstName VARCHAR(100)
       ,@LastName VARCHAR(100)
       ,@StudentNumber VARCHAR(50)
       ,@TransferredOut INT
       ,@Exclusions INT;
    DECLARE @CitizenShip_IPEDSValue INT
       ,@ProgramType_IPEDSValue INT
       ,@Gender_IPEDSValue INT
       ,@FullTimePartTime_IPEDSValue INT
       ,@StudentId UNIQUEIDENTIFIER; 
    DECLARE @AcademicEndDate DATETIME
       ,@AcademicStartDate DATETIME;
    DECLARE @AcadInstFirstTimeStartDate DATETIME;
-- Check if School tracks grades by letter or numeric 
    SET @Value = (
                   SELECT TOP 1
                            Value
                   FROM     dbo.syConfigAppSetValues
                   WHERE    SettingId = 47
                 );

--Here, we're determining if the school type is academic. If so, we will change the new var
--@AcademicEndDate to the @EndDate_Academic, if not, than use @EndDate. We will use the new
--@AcademicEndDate variable to help select the student list.  DD Rev 01/10/12

    DECLARE @StartDateMinus1Yr DATETIME
       ,@EndDateMinus1Yr DATETIME;
--Remove one year from the Start and End Date
    SET @StartDateMinus1Yr = DATEADD(YEAR,-1,@StartDate);
    SET @EndDateMinus1Yr = DATEADD(YEAR,-1,@EndDate);

    IF LOWER(@SchoolType) = 'academic'
        BEGIN
            SET @AcademicEndDate = DATEADD(YEAR,-1,@EndDate_Academic);
            SET @AcademicStartDate = DATEADD(YEAR,-1,@EndDate_Academic);
            --12/05/2012 DE8824 - Academic year options should have a cutoff start date
            SET @AcadInstFirstTimeStartDate = DATEADD(YEAR,-2,@EndDate_Academic);
            SET @StartDate = @StartDateMinus1Yr;
            SET @EndDate = @EndDateMinus1Yr;
        END;
    ELSE
        BEGIN
            SET @AcademicEndDate = @EndDateMinus1Yr;
            SET @AcademicStartDate = @StartDateMinus1Yr;
            SET @StartDate = @StartDateMinus1Yr;
            SET @EndDate = @EndDateMinus1Yr;
        END;		
	

    IF @ProgId IS NOT NULL
        BEGIN
            SELECT  @ReturnValue = COALESCE(@ReturnValue,'') + ProgDescrip + ','
            FROM    arPrograms t1
            WHERE   t1.ProgId IN ( SELECT   Val
                                   FROM     MultipleValuesForReportParameters(@ProgId,',',1) );
            SET @ReturnValue = SUBSTRING(@ReturnValue,1,LEN(@ReturnValue) - 1);
        END;
    ELSE
        BEGIN
            SELECT  @ReturnValue = COALESCE(@ReturnValue,'') + ProgDescrip + ','
            FROM    arPrograms t1
            WHERE   t1.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
                    AND t1.CampGrpId IN ( SELECT    CampGrpId
                                          FROM      syCmpGrpCmps
                                          WHERE     CampusId = @CampusId );
            SET @ReturnValue = SUBSTRING(@ReturnValue,1,LEN(@ReturnValue) - 1);
        END;

/*********Get the list of students that will be shown in the report - Starts Here ******************/			
    CREATE TABLE #StudentsList
        (
         StudentId UNIQUEIDENTIFIER
        ,SSN VARCHAR(10)
        ,FirstName VARCHAR(100)
        ,LastName VARCHAR(100)
        ,MiddleName VARCHAR(50)
        ,StudentNumber VARCHAR(50)
        ,TransferredOut INT
        ,Exclusions INT
        ,CitizenShip_IPEDSValue INT
        ,ProgramType_IPEDSValue INT
        ,Gender_IPEDSValue INT
        ,FullTimePartTime_IPEDSValue INT
        ,GenderId UNIQUEIDENTIFIER
        ,RaceId UNIQUEIDENTIFIER
        ,CitizenId UNIQUEIDENTIFIER
        ,GenderDescription VARCHAR(50)
        ,RaceDescription VARCHAR(50)
        ,StudentGraduatedStatusCount INT
        ,ClockHourProgramCount INT
        ,StartDate DATETIME
        ,HoustingType UNIQUEIDENTIFIER
        ,FamilyIncome UNIQUEIDENTIFIER
        );
		
-- The following table will store only Full Time First Time UnderGraduate Students
    CREATE TABLE #UnderGraduateStudentsWhoReceivedFinancialAid
        (
         StudentId UNIQUEIDENTIFIER
        ,FinancialAidType INT
        ,TitleIV BIT
        );


-- Get the list of UnderGraduate Students
    IF @SchoolType <> 'program'
        AND @InstitutionType = '1'
        BEGIN
            INSERT  INTO #StudentsList
                    SELECT DISTINCT
                            t1.StudentId
                           ,t1.SSN
                           ,t1.FirstName
                           ,t1.LastName
                           ,t1.MiddleName
                           ,t1.StudentNumber
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SQ1
                                       ,syStatusCodes SQ2
                                       ,dbo.sySysStatus SQ3
                              WHERE     SQ1.StatusCodeId = SQ2.StatusCodeId
                                        AND SQ2.SysStatusId = SQ3.SysStatusId
                                        AND SQ1.StudentId = t1.StudentId
                                        AND SQ3.SysStatusId = 19
                                        AND SQ1.TransferDate <= @AcademicEndDate
                            ) AS TransferredOut
                           ,(
					-- Check if the student was either dropped and if the drop reason is either
					-- deceased, activeduty, foreign aid service, church mission
                              CASE WHEN (
                                          SELECT    COUNT(*)
                                          FROM      arStuEnrollments SQ1
                                                   ,syStatusCodes SQ2
                                                   ,dbo.sySysStatus SQ3
                                                   ,arDropReasons SQ44
                                          WHERE     SQ1.StatusCodeId = SQ2.StatusCodeId
                                                    AND SQ2.SysStatusId = SQ3.SysStatusId
                                                    AND SQ1.DropReasonId = SQ44.DropReasonId
                                                    AND SQ1.StudentId = t1.StudentId
                                                    AND SQ3.SysStatusId IN ( 12 ) -- Dropped
                                                    AND SQ1.DateDetermined <= @AcademicEndDate
                                                    AND SQ44.IPEDSValue IN ( 15,16,17,18,19 )
                                        ) >= 1 THEN 1
                                   ELSE 0
                              END ) AS Exclusions
                           ,t11.IPEDSValue AS CitizenShip_IPEDSValue
                           ,t9.IPEDSValue AS ProgramType_IPEDSValue
                           ,t3.IPEDSValue AS Gender_IPEDSValue
                           ,t10.IPEDSValue AS FullTimePartTime_IPEDSValue
                           ,t1.Gender
                           ,t1.Race
                           ,t1.Citizen
                           ,(
                              SELECT DISTINCT
                                        AgencyDescrip
                              FROM      syRptAgencyFldValues
                              WHERE     RptAgencyFldValId = t3.IPEDSValue
                            ) AS GenderDescription
                           ,(
                              SELECT DISTINCT
                                        AgencyDescrip
                              FROM      syRptAgencyFldValues
                              WHERE     RptAgencyFldValId = t4.IPEDSValue
                            ) AS RaceDescription
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SEC
                                       ,syStatusCodes SC
                              WHERE     SEC.StudentId = t1.StudentId
                                        AND SEC.StatusCodeId = SC.StatusCodeId
                                        AND SC.SysStatusId = 14
                            ) AS StudentGraduatedStatusCount
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SEC
                                       ,arPrograms P
                                       ,arPrgVersions PV
                              WHERE     SEC.PrgVerId = PV.PrgVerId
                                        AND P.ProgId = PV.ProgId
                                        AND SEC.StudentId = t1.StudentId
                                        AND P.ACId = 5
                            ) AS ClockHourProgramCount
                           ,t2.StartDate
                           ,t1.HousingId
                           ,t1.FamilyIncome
                    FROM    adGenders t3
                    LEFT JOIN arStudent t1 ON t3.GenderId = t1.Gender
                    LEFT JOIN adEthCodes t4 ON t4.EthCodeId = t1.Race
                    INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                    INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                    INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                                 AND t6.SysStatusId NOT IN ( 8 ) -- Ignore "No Start" Students
                    INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                    INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                    INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                    LEFT JOIN arAttendTypes t10 ON t2.AttendTypeId = t10.AttendTypeId
                    LEFT JOIN dbo.adDegCertSeeking t12 ON t2.DegCertSeekingId = t12.DegCertSeekingId
                    INNER JOIN adCitizenships t11 ON t1.Citizen = t11.CitizenshipId
                    INNER JOIN dbo.saTransactions SA ON SA.StuEnrollId = t2.StuEnrollId
                    INNER JOIN dbo.saFundSources FS ON SA.FundSourceId = FS.FundSourceId
                            --DE8813 - Item 2
                            --INNER JOIN dbo.saTuitionCategories TC ON t2.TuitionCategoryId = TC.TuitionCategoryId
                    WHERE   LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                            AND (
                                  @ProgId IS NULL
                                  OR t8.ProgId IN ( SELECT  Val
                                                    FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                )
                            AND t10.IPEDSValue = 61
                            AND -- Full Time
                            t12.IPEDSValue = 11
                            AND -- First Time
                            t9.IPEDSValue = 58
                            AND SA.Voided = 0
                            AND FS.AdvFundSourceId NOT IN ( 1,19,23 )
                            AND -- Under Graduate
                            --12/05/2012 DE8824 - Academic year options should have a cutoff start date
                            --( t2.StartDate <= @AcademicEndDate )
                            (
                              t2.StartDate > @AcadInstFirstTimeStartDate
                              AND t2.StartDate <= @AcademicEndDate
                            )
                            AND t2.StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
						( SELECT    t1.StuEnrollId
                          FROM      arStuEnrollments t1
                                   ,syStatusCodes t2
                          WHERE     t1.StatusCodeId = t2.StatusCodeId
                                    AND StartDate <= @AcademicEndDate
                                    AND 
							-- Student started before the end date range
                                    LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                    AND (
                                          @ProgId IS NULL
                                          OR t8.ProgId IN ( SELECT  Val
                                                            FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                        )
                                    AND t2.SysStatusId IN ( 12,14,19,8 ) 
							-- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                    AND (
                                          t1.DateDetermined < @AcademicStartDate
                                          OR ExpGradDate < @AcademicStartDate
                                          OR LDA < @AcademicStartDate
                                        ) ) 
                       -- If Student is enrolled in only one program version and if that program version 
                       -- happens to be a continuing ed program exclude the student
                            AND t2.StudentId NOT IN ( SELECT DISTINCT
                                                                StudentId
                                                      FROM      (
                                                                  SELECT    StudentId
                                                                           ,COUNT(*) AS RowCounter
                                                                  FROM      arStuEnrollments
                                                                  WHERE     PrgVerId IN ( SELECT    PrgVerId
                                                                                          FROM      arPrgVersions
                                                                                          WHERE     IsContinuingEd = 1 )
                                                                  GROUP BY  StudentId
                                                                  HAVING    COUNT(*) = 1
                                                                ) dtStudent_ContinuingEd )

                         
       --                  --Also check if the student has received any Title IV Funds (Group 4 Students)
       --                 AND t2.StuEnrollId IN 
       --                  (
							--SELECT DISTINCT t1.StuEnrollId FROM faStudentAwards t1,saFundSources t2 
							--WHERE t1.AwardTypeId=t2.FundSourceId AND t2.TitleIV=1 AND
							--t1.AwardStartDate>=@StartDate AND t1.AwardstartDate<=@EndDate
       --                  )
                         --AND TC.IPEDSValue IN (145,146) --Instate or In-district tuition
                            AND FS.TitleIV = 1
       --                  AND 
							--(
							--	(SA.AwardStartDate>=@StartDate AND SA.AwardstartDate<=@EndDate) 
							--	OR 
							--	(SA.AwardEndDate>=@StartDate AND SA.AwardEndDate<=@EndDate)
							--)
							-- FFEL PLUS (9)(added for DE8816)
                            AND ISNULL(FS.AdvFundSourceId,0) NOT IN ( 5,6,9 ); -- Exclude FWS and Plus Loan
                        --AND FS.IPEDSValue IN (66,67,68) -- Only Federal, State and Institution Grant
        END;
    IF @SchoolType = 'program' --and @InstitutionType='1'
        BEGIN

--Get student only from largest program for program reporter schools
            SET @ProgId = @LargeProgramId;

            INSERT  INTO #StudentsList
                    SELECT DISTINCT
                            t1.StudentId
                           ,t1.SSN
                           ,t1.FirstName
                           ,t1.LastName
                           ,t1.MiddleName
                           ,t1.StudentNumber
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SQ1
                                       ,syStatusCodes SQ2
                                       ,dbo.sySysStatus SQ3
                              WHERE     SQ1.StatusCodeId = SQ2.StatusCodeId
                                        AND SQ2.SysStatusId = SQ3.SysStatusId
                                        AND SQ1.StudentId = t1.StudentId
                                        AND SQ3.SysStatusId = 19
                                        AND SQ1.TransferDate <= @EndDate
                            ) AS TransferredOut
                           ,(
					-- Check if the student was either dropped and if the drop reason is either
					-- deceased, activeduty, foreign aid service, church mission
                              CASE WHEN (
                                          SELECT    COUNT(*)
                                          FROM      arStuEnrollments SQ1
                                                   ,syStatusCodes SQ2
                                                   ,dbo.sySysStatus SQ3
                                                   ,arDropReasons SQ44
                                          WHERE     SQ1.StatusCodeId = SQ2.StatusCodeId
                                                    AND SQ2.SysStatusId = SQ3.SysStatusId
                                                    AND SQ1.DropReasonId = SQ44.DropReasonId
                                                    AND SQ1.StudentId = t1.StudentId
                                                    AND SQ3.SysStatusId IN ( 12 ) -- Dropped
                                                    AND SQ1.DateDetermined <= @EndDate
                                                    AND SQ44.IPEDSValue IN ( 15,16,17,18,19 )
                                        ) >= 1 THEN 1
                                   ELSE 0
                              END ) AS Exclusions
                           ,t11.IPEDSValue AS CitizenShip_IPEDSValue
                           ,t9.IPEDSValue AS ProgramType_IPEDSValue
                           ,t3.IPEDSValue AS Gender_IPEDSValue
                           ,t10.IPEDSValue AS FullTimePartTime_IPEDSValue
                           ,t1.Gender
                           ,t1.Race
                           ,t1.Citizen
                           ,(
                              SELECT DISTINCT
                                        AgencyDescrip
                              FROM      syRptAgencyFldValues
                              WHERE     RptAgencyFldValId = t3.IPEDSValue
                            ) AS GenderDescription
                           ,(
                              SELECT DISTINCT
                                        AgencyDescrip
                              FROM      syRptAgencyFldValues
                              WHERE     RptAgencyFldValId = t4.IPEDSValue
                            ) AS RaceDescription
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SEC
                                       ,syStatusCodes SC
                              WHERE     SEC.StudentId = t1.StudentId
                                        AND SEC.StatusCodeId = SC.StatusCodeId
                                        AND SC.SysStatusId = 14
                            ) AS StudentGraduatedStatusCount
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SEC
                                       ,arPrograms P
                                       ,arPrgVersions PV
                              WHERE     SEC.PrgVerId = PV.PrgVerId
                                        AND P.ProgId = PV.ProgId
                                        AND SEC.StudentId = t1.StudentId
                                        AND P.ACId = 5
                            ) AS ClockHourProgramCount
                           ,t2.StartDate
                           ,t1.HousingId
                           ,t1.FamilyIncome
                    FROM    adGenders t3
                    LEFT JOIN arStudent t1 ON t3.GenderId = t1.Gender
                    LEFT JOIN adEthCodes t4 ON t4.EthCodeId = t1.Race
                    INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                    INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                    INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                                 AND t6.SysStatusId NOT IN ( 8 ) -- Ignore "No Start" Students
                    INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                    INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                    INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                    LEFT JOIN arAttendTypes t10 ON t2.AttendTypeId = t10.AttendTypeId
                    LEFT JOIN dbo.adDegCertSeeking t12 ON t2.DegCertSeekingId = t12.DegCertSeekingId
                    INNER JOIN adCitizenships t11 ON t1.Citizen = t11.CitizenshipId
                    INNER JOIN dbo.saTransactions SA ON SA.StuEnrollId = t2.StuEnrollId
                    INNER JOIN dbo.saFundSources FS ON SA.FundSourceId = FS.FundSourceId
                    WHERE   LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                            AND (
                                  @ProgId IS NULL
                                  OR t8.ProgId IN ( SELECT  Val
                                                    FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                )
                            AND t10.IPEDSValue = 61
                            AND -- Full Time
                            t12.IPEDSValue = 11
                            AND -- First Time
                            t9.IPEDSValue = 58
                            AND SA.Voided = 0
                            AND FS.AdvFundSourceId NOT IN ( 1,19,23 )
                            AND -- Under Graduate
                            (
                              t2.StartDate >= @StartDate
                              AND t2.StartDate <= @EndDate
                            )
                            AND t2.StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
						( SELECT    t1.StuEnrollId
                          FROM      arStuEnrollments t1
                                   ,syStatusCodes t2
                          WHERE     t1.StatusCodeId = t2.StatusCodeId
                                    AND StartDate <= @EndDate
                                    AND 
							-- Student started before the end date range
                                    LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                    AND (
                                          @ProgId IS NULL
                                          OR t8.ProgId IN ( SELECT  Val
                                                            FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                        )
                                    AND t2.SysStatusId IN ( 12,14,19,8 ) 
							-- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                    AND (
                                          t1.DateDetermined < @AcademicStartDate
                                          OR ExpGradDate < @AcademicStartDate
                                          OR LDA < @AcademicStartDate
                                        ) ) 
                       -- If Student is enrolled in only one program version and if that program version 
                       -- happens to be a continuing ed program exclude the student
                            AND t2.StudentId NOT IN ( SELECT DISTINCT
                                                                StudentId
                                                      FROM      (
                                                                  SELECT    StudentId
                                                                           ,COUNT(*) AS RowCounter
                                                                  FROM      arStuEnrollments
                                                                  WHERE     PrgVerId IN ( SELECT    PrgVerId
                                                                                          FROM      arPrgVersions
                                                                                          WHERE     IsContinuingEd = 1 )
                                                                  GROUP BY  StudentId
                                                                  HAVING    COUNT(*) = 1
                                                                ) dtStudent_ContinuingEd )

                       --  AND
                         --Also check if the student has received any Title IV Funds (Group 4 Students)
       --                  t2.StuEnrollId IN 
       --                  (
							--SELECT DISTINCT t1.StuEnrollId FROM faStudentAwards t1,saFundSources t2 
							--WHERE t1.AwardTypeId=t2.FundSourceId AND t2.TitleIV=1 AND
							--t1.AwardStartDate>=@StartDate AND t1.AwardstartDate<=@EndDate
       --                  )
       --                   AND 
							--(
							--	(SA.AwardStartDate>=@StartDate AND SA.AwardstartDate<=@EndDate) 
							--	OR 
							--	(SA.AwardEndDate>=@StartDate AND SA.AwardEndDate<=@EndDate)
							--)
							-- FFEL PLUS (9)(added for DE8816)
                            AND ISNULL(FS.AdvFundSourceId,0) NOT IN ( 5,6,9 ) -- Exclude FWS and Plus Loan
                            AND FS.TitleIV = 1;
                        --AND FS.IPEDSValue IN (66,67,68) -- Only Federal, State and Institution Grant
        END;

                         
/***THIS SECTION IS FOR COHORT YR - 1 ****/
/*********Get the list of students that will be shown in the report - Starts Here ******************/

	
		
    CREATE TABLE #StudentsList1
        (
         StudentId UNIQUEIDENTIFIER
        ,SSN VARCHAR(10)
        ,FirstName VARCHAR(100)
        ,LastName VARCHAR(100)
        ,MiddleName VARCHAR(50)
        ,StudentNumber VARCHAR(50)
        ,TransferredOut INT
        ,Exclusions INT
        ,CitizenShip_IPEDSValue INT
        ,ProgramType_IPEDSValue INT
        ,Gender_IPEDSValue INT
        ,FullTimePartTime_IPEDSValue INT
        ,GenderId UNIQUEIDENTIFIER
        ,RaceId UNIQUEIDENTIFIER
        ,CitizenId UNIQUEIDENTIFIER
        ,GenderDescription VARCHAR(50)
        ,RaceDescription VARCHAR(50)
        ,StudentGraduatedStatusCount INT
        ,ClockHourProgramCount INT
        ,StartDate DATETIME
        ,HoustingType UNIQUEIDENTIFIER
        ,FamilyIncome UNIQUEIDENTIFIER
        );
		
-- The following table will store only Full Time First Time UnderGraduate Students
    CREATE TABLE #UnderGraduateStudentsWhoReceivedFinancialAid1
        (
         StudentId UNIQUEIDENTIFIER
        ,FinancialAidType INT
        ,TitleIV BIT
        );


-- Get the list of UnderGraduate Students
    IF @SchoolType <> 'program'
        AND @InstitutionType = '1'
        BEGIN
            INSERT  INTO #StudentsList1
                    SELECT DISTINCT
                            t1.StudentId
                           ,t1.SSN
                           ,t1.FirstName
                           ,t1.LastName
                           ,t1.MiddleName
                           ,t1.StudentNumber
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SQ1
                                       ,syStatusCodes SQ2
                                       ,dbo.sySysStatus SQ3
                              WHERE     SQ1.StatusCodeId = SQ2.StatusCodeId
                                        AND SQ2.SysStatusId = SQ3.SysStatusId
                                        AND SQ1.StudentId = t1.StudentId
                                        AND SQ3.SysStatusId = 19
                                        AND SQ1.TransferDate <= @AcademicEndDate
                            ) AS TransferredOut
                           ,(
					-- Check if the student was either dropped and if the drop reason is either
					-- deceased, activeduty, foreign aid service, church mission
                              CASE WHEN (
                                          SELECT    COUNT(*)
                                          FROM      arStuEnrollments SQ1
                                                   ,syStatusCodes SQ2
                                                   ,dbo.sySysStatus SQ3
                                                   ,arDropReasons SQ44
                                          WHERE     SQ1.StatusCodeId = SQ2.StatusCodeId
                                                    AND SQ2.SysStatusId = SQ3.SysStatusId
                                                    AND SQ1.DropReasonId = SQ44.DropReasonId
                                                    AND SQ1.StudentId = t1.StudentId
                                                    AND SQ3.SysStatusId IN ( 12 ) -- Dropped
                                                    AND SQ1.DateDetermined <= @AcademicEndDate
                                                    AND SQ44.IPEDSValue IN ( 15,16,17,18,19 )
                                        ) >= 1 THEN 1
                                   ELSE 0
                              END ) AS Exclusions
                           ,t11.IPEDSValue AS CitizenShip_IPEDSValue
                           ,t9.IPEDSValue AS ProgramType_IPEDSValue
                           ,t3.IPEDSValue AS Gender_IPEDSValue
                           ,t10.IPEDSValue AS FullTimePartTime_IPEDSValue
                           ,t1.Gender
                           ,t1.Race
                           ,t1.Citizen
                           ,(
                              SELECT DISTINCT
                                        AgencyDescrip
                              FROM      syRptAgencyFldValues
                              WHERE     RptAgencyFldValId = t3.IPEDSValue
                            ) AS GenderDescription
                           ,(
                              SELECT DISTINCT
                                        AgencyDescrip
                              FROM      syRptAgencyFldValues
                              WHERE     RptAgencyFldValId = t4.IPEDSValue
                            ) AS RaceDescription
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SEC
                                       ,syStatusCodes SC
                              WHERE     SEC.StudentId = t1.StudentId
                                        AND SEC.StatusCodeId = SC.StatusCodeId
                                        AND SC.SysStatusId = 14
                            ) AS StudentGraduatedStatusCount
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SEC
                                       ,arPrograms P
                                       ,arPrgVersions PV
                              WHERE     SEC.PrgVerId = PV.PrgVerId
                                        AND P.ProgId = PV.ProgId
                                        AND SEC.StudentId = t1.StudentId
                                        AND P.ACId = 5
                            ) AS ClockHourProgramCount
                           ,t2.StartDate
                           ,t1.HousingId
                           ,t1.FamilyIncome
                    FROM    adGenders t3
                    LEFT JOIN arStudent t1 ON t3.GenderId = t1.Gender
                    LEFT JOIN adEthCodes t4 ON t4.EthCodeId = t1.Race
                    INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                    INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                    INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                                 AND t6.SysStatusId NOT IN ( 8 ) -- Ignore "No Start" Students
                    INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                    INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                    INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                    LEFT JOIN arAttendTypes t10 ON t2.AttendTypeId = t10.AttendTypeId
                    LEFT JOIN dbo.adDegCertSeeking t12 ON t2.DegCertSeekingId = t12.DegCertSeekingId
                    INNER JOIN adCitizenships t11 ON t1.Citizen = t11.CitizenshipId
                    INNER JOIN dbo.saTransactions SA ON SA.StuEnrollId = t2.StuEnrollId
                    INNER JOIN dbo.saFundSources FS ON SA.FundSourceId = FS.FundSourceId
                            --DE8813 - Item 2
                            --INNER JOIN dbo.saTuitionCategories TC ON t2.TuitionCategoryId = TC.TuitionCategoryId
                    WHERE   LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                            AND (
                                  @ProgId IS NULL
                                  OR t8.ProgId IN ( SELECT  Val
                                                    FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                )
                            AND t10.IPEDSValue = 61
                            AND -- Full Time
                            t12.IPEDSValue = 11
                            AND -- First Time
                            t9.IPEDSValue = 58
                            AND SA.Voided = 0
                            AND FS.AdvFundSourceId NOT IN ( 1,19,23 )
                            AND -- Under Graduate
                            --12/05/2012 DE8824 - Academic year options should have a cutoff start date
                           -- ( t2.StartDate <= @AcademicEndDate )
                            (
                              t2.StartDate > @AcadInstFirstTimeStartDate
                              AND t2.StartDate <= @AcademicEndDate
                            )
                            AND t2.StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
						( SELECT    t1.StuEnrollId
                          FROM      arStuEnrollments t1
                                   ,syStatusCodes t2
                          WHERE     t1.StatusCodeId = t2.StatusCodeId
                                    AND StartDate <= @AcademicEndDate
                                    AND 
							-- Student started before the end date range
                                    LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                    AND (
                                          @ProgId IS NULL
                                          OR t8.ProgId IN ( SELECT  Val
                                                            FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                        )
                                    AND t2.SysStatusId IN ( 12,14,19,8 ) 
							-- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                    AND (
                                          t1.DateDetermined < @AcademicStartDate
                                          OR ExpGradDate < @AcademicStartDate
                                          OR LDA < @AcademicStartDate
                                        ) ) 
                       -- If Student is enrolled in only one program version and if that program version 
                       -- happens to be a continuing ed program exclude the student
                            AND t2.StudentId NOT IN ( SELECT DISTINCT
                                                                StudentId
                                                      FROM      (
                                                                  SELECT    StudentId
                                                                           ,COUNT(*) AS RowCounter
                                                                  FROM      arStuEnrollments
                                                                  WHERE     PrgVerId IN ( SELECT    PrgVerId
                                                                                          FROM      arPrgVersions
                                                                                          WHERE     IsContinuingEd = 1 )
                                                                  GROUP BY  StudentId
                                                                  HAVING    COUNT(*) = 1
                                                                ) dtStudent_ContinuingEd )

                         
       --                  --Also check if the student has received any Title IV Funds (Group 4 Students)
       --                 AND t2.StuEnrollId IN 
       --                  (
							--SELECT DISTINCT t1.StuEnrollId FROM faStudentAwards t1,saFundSources t2 
							--WHERE t1.AwardTypeId=t2.FundSourceId AND t2.TitleIV=1 AND
							--t1.AwardStartDate>=@StartDateMinus1Yr AND t1.AwardstartDate<=@EndDate
       --                  )
                         --AND TC.IPEDSValue IN (145,146) --Instate or In-district tuition
                            AND FS.TitleIV = 1
       --                  AND 
							--(
							--	(SA.AwardStartDate>=@StartDateMinus1Yr AND SA.AwardstartDate<=@EndDateMinus1Yr) 
							--	OR 
							--	(SA.AwardEndDate>=@StartDateMinus1Yr AND SA.AwardEndDate<=@EndDateMinus1Yr)
							--)
							-- FFEL PLUS (9)(added for DE8816)
                            AND ISNULL(FS.AdvFundSourceId,0) NOT IN ( 5,6,9 ); -- Exclude FWS and Plus Loan
                        --AND FS.IPEDSValue IN (66,67,68) -- Only Federal, State and Institution Grant
        END;
    IF @SchoolType = 'program' --AND @InstitutionType='1'
        BEGIN
--Get student only from largest program for program reporter schools
            SET @ProgId = @LargeProgramId;

            INSERT  INTO #StudentsList1
                    SELECT DISTINCT
                            t1.StudentId
                           ,t1.SSN
                           ,t1.FirstName
                           ,t1.LastName
                           ,t1.MiddleName
                           ,t1.StudentNumber
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SQ1
                                       ,syStatusCodes SQ2
                                       ,dbo.sySysStatus SQ3
                              WHERE     SQ1.StatusCodeId = SQ2.StatusCodeId
                                        AND SQ2.SysStatusId = SQ3.SysStatusId
                                        AND SQ1.StudentId = t1.StudentId
                                        AND SQ3.SysStatusId = 19
                                        AND SQ1.TransferDate <= @AcademicEndDate
                            ) AS TransferredOut
                           ,(
					-- Check if the student was either dropped and if the drop reason is either
					-- deceased, activeduty, foreign aid service, church mission
                              CASE WHEN (
                                          SELECT    COUNT(*)
                                          FROM      arStuEnrollments SQ1
                                                   ,syStatusCodes SQ2
                                                   ,dbo.sySysStatus SQ3
                                                   ,arDropReasons SQ44
                                          WHERE     SQ1.StatusCodeId = SQ2.StatusCodeId
                                                    AND SQ2.SysStatusId = SQ3.SysStatusId
                                                    AND SQ1.DropReasonId = SQ44.DropReasonId
                                                    AND SQ1.StudentId = t1.StudentId
                                                    AND SQ3.SysStatusId IN ( 12 ) -- Dropped
                                                    AND SQ1.DateDetermined <= @AcademicEndDate
                                                    AND SQ44.IPEDSValue IN ( 15,16,17,18,19 )
                                        ) >= 1 THEN 1
                                   ELSE 0
                              END ) AS Exclusions
                           ,t11.IPEDSValue AS CitizenShip_IPEDSValue
                           ,t9.IPEDSValue AS ProgramType_IPEDSValue
                           ,t3.IPEDSValue AS Gender_IPEDSValue
                           ,t10.IPEDSValue AS FullTimePartTime_IPEDSValue
                           ,t1.Gender
                           ,t1.Race
                           ,t1.Citizen
                           ,(
                              SELECT DISTINCT
                                        AgencyDescrip
                              FROM      syRptAgencyFldValues
                              WHERE     RptAgencyFldValId = t3.IPEDSValue
                            ) AS GenderDescription
                           ,(
                              SELECT DISTINCT
                                        AgencyDescrip
                              FROM      syRptAgencyFldValues
                              WHERE     RptAgencyFldValId = t4.IPEDSValue
                            ) AS RaceDescription
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SEC
                                       ,syStatusCodes SC
                              WHERE     SEC.StudentId = t1.StudentId
                                        AND SEC.StatusCodeId = SC.StatusCodeId
                                        AND SC.SysStatusId = 14
                            ) AS StudentGraduatedStatusCount
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SEC
                                       ,arPrograms P
                                       ,arPrgVersions PV
                              WHERE     SEC.PrgVerId = PV.PrgVerId
                                        AND P.ProgId = PV.ProgId
                                        AND SEC.StudentId = t1.StudentId
                                        AND P.ACId = 5
                            ) AS ClockHourProgramCount
                           ,t2.StartDate
                           ,t1.HousingId
                           ,t1.FamilyIncome
                    FROM    adGenders t3
                    LEFT JOIN arStudent t1 ON t3.GenderId = t1.Gender
                    LEFT JOIN adEthCodes t4 ON t4.EthCodeId = t1.Race
                    INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                    INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                    INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                                 AND t6.SysStatusId NOT IN ( 8 ) -- Ignore "No Start" Students
                    INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                    INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                    INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                    LEFT JOIN arAttendTypes t10 ON t2.AttendTypeId = t10.AttendTypeId
                    LEFT JOIN dbo.adDegCertSeeking t12 ON t2.DegCertSeekingId = t12.DegCertSeekingId
                    INNER JOIN adCitizenships t11 ON t1.Citizen = t11.CitizenshipId
                    INNER JOIN dbo.saTransactions SA ON SA.StuEnrollId = t2.StuEnrollId
                    INNER JOIN dbo.saFundSources FS ON SA.FundSourceId = FS.FundSourceId
                    WHERE   LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                            AND (
                                  @ProgId IS NULL
                                  OR t8.ProgId IN ( SELECT  Val
                                                    FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                )
                            AND t10.IPEDSValue = 61
                            AND -- Full Time
                            t12.IPEDSValue = 11
                            AND -- First Time
                            t9.IPEDSValue = 58
                            AND SA.Voided = 0
                            AND FS.AdvFundSourceId NOT IN ( 1,19,23 )
                            AND -- Under Graduate
                            (
                              t2.StartDate >= @StartDateMinus1Yr
                              AND t2.StartDate <= @AcademicEndDate
                            )
                        --(t2.StartDate<=@AcademicEndDate)
                            AND t2.StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
						( SELECT    t1.StuEnrollId
                          FROM      arStuEnrollments t1
                                   ,syStatusCodes t2
                          WHERE     t1.StatusCodeId = t2.StatusCodeId
                                    AND StartDate <= @AcademicEndDate
                                    AND 
							-- Student started before the end date range
                                    LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                    AND (
                                          @ProgId IS NULL
                                          OR t8.ProgId IN ( SELECT  Val
                                                            FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                        )
                                    AND t2.SysStatusId IN ( 12,14,19,8 ) 
							-- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                    AND (
                                          t1.DateDetermined < @StartDateMinus1Yr
                                          OR ExpGradDate < @StartDateMinus1Yr
                                          OR LDA < @StartDateMinus1Yr
                                        ) ) 
                       -- If Student is enrolled in only one program version and if that program version 
                       -- happens to be a continuing ed program exclude the student
                            AND t2.StudentId NOT IN ( SELECT DISTINCT
                                                                StudentId
                                                      FROM      (
                                                                  SELECT    StudentId
                                                                           ,COUNT(*) AS RowCounter
                                                                  FROM      arStuEnrollments
                                                                  WHERE     PrgVerId IN ( SELECT    PrgVerId
                                                                                          FROM      arPrgVersions
                                                                                          WHERE     IsContinuingEd = 1 )
                                                                  GROUP BY  StudentId
                                                                  HAVING    COUNT(*) = 1
                                                                ) dtStudent_ContinuingEd )

                        -- AND
                         --Also check if the student has received any Title IV Funds (Group 4 Students)
       --                  t2.StuEnrollId IN 
       --                  (
							--SELECT DISTINCT t1.StuEnrollId FROM faStudentAwards t1,saFundSources t2 
							--WHERE t1.AwardTypeId=t2.FundSourceId AND t2.TitleIV=1 AND
							--t1.AwardStartDate>=@StartDateMinus1Yr AND t1.AwardstartDate<=@EndDate
       --                  )
       --                   AND 
							--(
							--	(SA.AwardStartDate>=@StartDateMinus1Yr AND SA.AwardstartDate<=@EndDate) 
							--	OR 
							--	(SA.AwardEndDate>=@StartDateMinus1Yr AND SA.AwardEndDate<=@EndDate)
							--)
							-- FFEL PLUS (9)(added for DE8816)
                            AND ISNULL(FS.AdvFundSourceId,0) NOT IN ( 5,6,9 ) -- Exclude FWS and Plus Loan
                            AND FS.TitleIV = 1;
                        --AND FS.IPEDSValue IN (66,67,68) -- Only Federal, State and Institution Grant
        END;

                         
    SELECT  RowNumber
           ,SSN
           ,StudentNumber
           ,StudentName
           ,CASE WHEN INC1 >= 1 THEN 'X'
                 ELSE ''
            END AS INC1X
           ,INC1
           ,GRNT1
           ,CASE WHEN GRNT1 >= 1 THEN 'X'
                 ELSE ''
            END AS GRNT1X
           ,INC1Aid
           ,CASE WHEN INC2 >= 1 THEN 'X'
                 ELSE ''
            END AS INC2X
           ,INC2
           ,GRNT2
           ,CASE WHEN GRNT2 >= 1 THEN 'X'
                 ELSE ''
            END AS GRNT2X
           ,INC2Aid
           ,CASE WHEN INC3 >= 1 THEN 'X'
                 ELSE ''
            END AS INC3X
           ,INC3
           ,GRNT3
           ,CASE WHEN GRNT3 >= 1 THEN 'X'
                 ELSE ''
            END AS GRNT3X
           ,INC3Aid
           ,CASE WHEN INC4 >= 1 THEN 'X'
                 ELSE ''
            END AS INC4X
           ,INC4
           ,GRNT4
           ,CASE WHEN GRNT4 >= 1 THEN 'X'
                 ELSE ''
            END AS GRNT4X
           ,INC4Aid
           ,CASE WHEN INC5 >= 1 THEN 'X'
                 ELSE ''
            END AS INC5X
           ,INC5
           ,GRNT5
           ,CASE WHEN GRNT5 >= 1 THEN 'X'
                 ELSE ''
            END AS GRNT5X
           ,INC5Aid
    INTO    #Result1
    FROM    (
              SELECT    NEWID() AS RowNumber
                       ,dbo.UDF_FormatSSN(SSN) AS SSN
                       ,StudentNumber
                       ,LastName + ', ' + FirstName + ' ' + ISNULL(MiddleName,'') AS StudentName
                       ,
		--(SELECT COUNT(DISTINCT t1.StudentId) FROM arStudent t1 INNER JOIN syFamilyIncome t2 ON t1.FamilyIncome=t2.FamilyIncomeID 
		--where t1.StudentId=SL.StudentId AND t2.IPEDSValue=148) AS INC1,
                        (
                          SELECT    CASE WHEN SUM(TransAmount * -1) > 0 THEN 1
                                         ELSE 0
                                    END AS NetAmount
                          FROM      dbo.saTransactions t1
                          INNER JOIN saFundSources t2 ON t1.FundSourceId = t2.FundSourceId
                          INNER JOIN arStuEnrollments t3 ON t1.StuEnrollId = t3.StuEnrollId
                                                            AND t3.StudentId = SL.StudentId
                          INNER JOIN arStudent t4 ON t4.StudentId = t3.StudentId
                          INNER JOIN syFamilyIncome t5 ON t4.FamilyIncome = t5.FamilyIncomeID
                          WHERE     t2.TitleIV = 1
                                    AND t5.IPEDSValue = 148
                                    AND ISNULL(t2.AdvFundSourceId,0) NOT IN ( 5,6,9 ) -- FFEL PLUS (9)(added for DE8816)
                                    AND ( t1.TransDate BETWEEN @StartDate AND @EndDate )
                                    AND t1.Voided = 0
                                    AND t2.AdvFundSourceId NOT IN ( 1,19,23 )
                                    AND t3.StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
						( SELECT    t1.StuEnrollId
                          FROM      arStuEnrollments t1
                                   ,syStatusCodes t2
                          WHERE     t1.StatusCodeId = t2.StatusCodeId
                                    AND StartDate <= @EndDate
                                    AND -- Student started before the end date range
                                    LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                    AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                    AND (
                                          t1.DateDetermined < @AcademicStartDate
                                          OR ExpGradDate < @AcademicStartDate
                                          OR LDA < @AcademicStartDate
                                        ) ) 
		--AND t2.IPEDSValue IN (66,67,68)
                        ) AS INC1
                       ,(
                          SELECT    CASE WHEN SUM(TransAmount * -1) > 0 THEN 1
                                         ELSE 0
                                    END AS NetAmount
                          FROM      dbo.saTransactions t1
                          INNER JOIN saFundSources t2 ON t1.FundSourceId = t2.FundSourceId
                          INNER JOIN arStuEnrollments t3 ON t1.StuEnrollId = t3.StuEnrollId
                                                            AND t3.StudentId = SL.StudentId
                          INNER JOIN arStudent t4 ON t4.StudentId = t3.StudentId
                          INNER JOIN syFamilyIncome t5 ON t4.FamilyIncome = t5.FamilyIncomeID
                          WHERE     t2.TitleIV = 1
                                    AND t5.IPEDSValue = 148
                                    AND ISNULL(t2.AdvFundSourceId,0) NOT IN ( 5,6,9 ) -- FFEL PLUS (9)(added for DE8816)
                                    AND ( t1.TransDate BETWEEN @StartDate AND @EndDate )
                                    AND t1.Voided = 0
                                    AND t2.AdvFundSourceId NOT IN ( 1,19,23 )
                                    AND t3.StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
						( SELECT    t1.StuEnrollId
                          FROM      arStuEnrollments t1
                                   ,syStatusCodes t2
                          WHERE     t1.StatusCodeId = t2.StatusCodeId
                                    AND StartDate <= @EndDate
                                    AND -- Student started before the end date range
                                    LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                    AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                    AND (
                                          t1.DateDetermined < @AcademicStartDate
                                          OR ExpGradDate < @AcademicStartDate
                                          OR LDA < @AcademicStartDate
                                        ) )
                                    AND t2.IPEDSValue IN ( 66,67,68 )
                        ) AS GRNT1
                       ,(
                          SELECT    SUM(TransAmount * -1) AS NetAmount
                          FROM      dbo.saTransactions t1
                          INNER JOIN saFundSources t2 ON t1.FundSourceId = t2.FundSourceId
                          INNER JOIN arStuEnrollments t3 ON t1.StuEnrollId = t3.StuEnrollId
                                                            AND t3.StudentId = SL.StudentId
                          INNER JOIN arStudent t4 ON t4.StudentId = t3.StudentId
                          INNER JOIN syFamilyIncome t5 ON t4.FamilyIncome = t5.FamilyIncomeID
                          WHERE     t2.TitleIV = 1
                                    AND t5.IPEDSValue = 148
                                    AND ISNULL(t2.AdvFundSourceId,0) NOT IN ( 5,6,9 )-- FFEL PLUS (9)(added for DE8816)
                                    AND ( t1.TransDate BETWEEN @StartDateMinus1Yr AND @EndDate )
                                    AND t1.Voided = 0
                                    AND t2.AdvFundSourceId NOT IN ( 1,19,23 )
                                    AND t3.StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
						( SELECT    t1.StuEnrollId
                          FROM      arStuEnrollments t1
                                   ,syStatusCodes t2
                          WHERE     t1.StatusCodeId = t2.StatusCodeId
                                    AND StartDate <= @AcademicEndDate
                                    AND -- Student started before the end date range
                                    LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                    AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                    AND (
                                          t1.DateDetermined < @StartDateMinus1Yr
                                          OR ExpGradDate < @StartDateMinus1Yr
                                          OR LDA < @StartDateMinus1Yr
                                        ) )
                                    AND t2.IPEDSValue IN ( 66,67,68 )
                        ) AS INC1Aid
                       ,
		
				
		--(SELECT COUNT(DISTINCT t1.StudentId) FROM arStudent t1 INNER JOIN syFamilyIncome t2 ON t1.FamilyIncome=t2.FamilyIncomeID 
		--where t1.StudentId=SL.StudentId AND t2.IPEDSValue=149) AS INC2,
                        (
                          SELECT    CASE WHEN SUM(TransAmount * -1) > 0 THEN 1
                                         ELSE 0
                                    END AS NetAmount
                          FROM      dbo.saTransactions t1
                          INNER JOIN saFundSources t2 ON t1.FundSourceId = t2.FundSourceId
                          INNER JOIN arStuEnrollments t3 ON t1.StuEnrollId = t3.StuEnrollId
                                                            AND t3.StudentId = SL.StudentId
                          INNER JOIN arStudent t4 ON t4.StudentId = t3.StudentId
                          INNER JOIN syFamilyIncome t5 ON t4.FamilyIncome = t5.FamilyIncomeID
                          WHERE     t2.TitleIV = 1
                                    AND t5.IPEDSValue = 149
                                    AND ISNULL(t2.AdvFundSourceId,0) NOT IN ( 5,6,9 ) -- FFEL PLUS (9)(added for DE8816)
                                    AND ( t1.TransDate BETWEEN @StartDate AND @EndDate )
                                    AND t1.Voided = 0
                                    AND t2.AdvFundSourceId NOT IN ( 1,19,23 )
                                    AND t3.StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
						( SELECT    t1.StuEnrollId
                          FROM      arStuEnrollments t1
                                   ,syStatusCodes t2
                          WHERE     t1.StatusCodeId = t2.StatusCodeId
                                    AND StartDate <= @EndDate
                                    AND -- Student started before the end date range
                                    LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                    AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                    AND (
                                          t1.DateDetermined < @AcademicStartDate
                                          OR ExpGradDate < @AcademicStartDate
                                          OR LDA < @AcademicStartDate
                                        ) ) 
		--AND t2.IPEDSValue IN (66,67,68)
                        ) AS INC2
                       ,(
                          SELECT    CASE WHEN SUM(TransAmount * -1) > 0 THEN 1
                                         ELSE 0
                                    END AS NetAmount
                          FROM      dbo.saTransactions t1
                          INNER JOIN saFundSources t2 ON t1.FundSourceId = t2.FundSourceId
                          INNER JOIN arStuEnrollments t3 ON t1.StuEnrollId = t3.StuEnrollId
                                                            AND t3.StudentId = SL.StudentId
                          INNER JOIN arStudent t4 ON t4.StudentId = t3.StudentId
                          INNER JOIN syFamilyIncome t5 ON t4.FamilyIncome = t5.FamilyIncomeID
                          WHERE     t2.TitleIV = 1
                                    AND t5.IPEDSValue = 149
                                    AND ISNULL(t2.AdvFundSourceId,0) NOT IN ( 5,6,9 ) -- FFEL PLUS (9)(added for DE8816)
                                    AND ( t1.TransDate BETWEEN @StartDate AND @EndDate )
                                    AND t1.Voided = 0
                                    AND t2.AdvFundSourceId NOT IN ( 1,19,23 )
                                    AND t3.StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
						( SELECT    t1.StuEnrollId
                          FROM      arStuEnrollments t1
                                   ,syStatusCodes t2
                          WHERE     t1.StatusCodeId = t2.StatusCodeId
                                    AND StartDate <= @EndDate
                                    AND -- Student started before the end date range
                                    LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                    AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                    AND (
                                          t1.DateDetermined < @AcademicStartDate
                                          OR ExpGradDate < @AcademicStartDate
                                          OR LDA < @AcademicStartDate
                                        ) )
                                    AND t2.IPEDSValue IN ( 66,67,68 )
                        ) AS GRNT2
                       ,(
                          SELECT    SUM(TransAmount * -1) AS NetAmount
                          FROM      dbo.saTransactions t1
                          INNER JOIN saFundSources t2 ON t1.FundSourceId = t2.FundSourceId
                          INNER JOIN arStuEnrollments t3 ON t1.StuEnrollId = t3.StuEnrollId
                                                            AND t3.StudentId = SL.StudentId
                          INNER JOIN arStudent t4 ON t4.StudentId = t3.StudentId
                          INNER JOIN syFamilyIncome t5 ON t4.FamilyIncome = t5.FamilyIncomeID
                          WHERE     t2.TitleIV = 1
                                    AND t5.IPEDSValue = 149
                                    AND ISNULL(t2.AdvFundSourceId,0) NOT IN ( 5,6,9 ) -- FFEL PLUS (9)(added for DE8816)
                                    AND ( t1.TransDate BETWEEN @StartDateMinus1Yr AND @EndDate )
                                    AND t1.Voided = 0
                                    AND t2.AdvFundSourceId NOT IN ( 1,19,23 )
                                    AND t3.StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
						( SELECT    t1.StuEnrollId
                          FROM      arStuEnrollments t1
                                   ,syStatusCodes t2
                          WHERE     t1.StatusCodeId = t2.StatusCodeId
                                    AND StartDate <= @AcademicEndDate
                                    AND -- Student started before the end date range
                                    LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                    AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                    AND (
                                          t1.DateDetermined < @StartDateMinus1Yr
                                          OR ExpGradDate < @StartDateMinus1Yr
                                          OR LDA < @StartDateMinus1Yr
                                        ) )
                                    AND t2.IPEDSValue IN ( 66,67,68 )
                        ) AS INC2Aid
                       ,	
		
		--(SELECT COUNT(DISTINCT t1.StudentId) FROM arStudent t1 INNER JOIN 
		--syFamilyIncome t2 ON t1.FamilyIncome=t2.FamilyIncomeID 
		--where t1.StudentId=SL.StudentId AND t2.IPEDSValue=151) AS INC3,
                        (
                          SELECT    CASE WHEN SUM(TransAmount * -1) > 0 THEN 1
                                         ELSE 0
                                    END AS NetAmount
                          FROM      dbo.saTransactions t1
                          INNER JOIN saFundSources t2 ON t1.FundSourceId = t2.FundSourceId
                          INNER JOIN arStuEnrollments t3 ON t1.StuEnrollId = t3.StuEnrollId
                                                            AND t3.StudentId = SL.StudentId
                          INNER JOIN arStudent t4 ON t4.StudentId = t3.StudentId
                          INNER JOIN syFamilyIncome t5 ON t4.FamilyIncome = t5.FamilyIncomeID
                          WHERE     t2.TitleIV = 1
                                    AND t5.IPEDSValue = 151
                                    AND ISNULL(t2.AdvFundSourceId,0) NOT IN ( 5,6,9 ) -- FFEL PLUS (9)(added for DE8816)
                                    AND ( t1.TransDate BETWEEN @StartDate AND @EndDate )
                                    AND t1.Voided = 0
                                    AND t2.AdvFundSourceId NOT IN ( 1,19,23 )
                                    AND t3.StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
						( SELECT    t1.StuEnrollId
                          FROM      arStuEnrollments t1
                                   ,syStatusCodes t2
                          WHERE     t1.StatusCodeId = t2.StatusCodeId
                                    AND StartDate <= @EndDate
                                    AND -- Student started before the end date range
                                    LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                    AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                    AND (
                                          t1.DateDetermined < @AcademicStartDate
                                          OR ExpGradDate < @AcademicStartDate
                                          OR LDA < @AcademicStartDate
                                        ) ) 
		--AND t2.IPEDSValue IN (66,67,68)
                        ) AS INC3
                       ,(
                          SELECT    CASE WHEN SUM(TransAmount * -1) > 0 THEN 1
                                         ELSE 0
                                    END AS NetAmount
                          FROM      dbo.saTransactions t1
                          INNER JOIN saFundSources t2 ON t1.FundSourceId = t2.FundSourceId
                          INNER JOIN arStuEnrollments t3 ON t1.StuEnrollId = t3.StuEnrollId
                                                            AND t3.StudentId = SL.StudentId
                          INNER JOIN arStudent t4 ON t4.StudentId = t3.StudentId
                          INNER JOIN syFamilyIncome t5 ON t4.FamilyIncome = t5.FamilyIncomeID
                          WHERE     t2.TitleIV = 1
                                    AND t5.IPEDSValue = 151
                                    AND ISNULL(t2.AdvFundSourceId,0) NOT IN ( 5,6,9 ) -- FFEL PLUS (9)(added for DE8816)
                                    AND ( t1.TransDate BETWEEN @StartDate AND @EndDate )
                                    AND t1.Voided = 0
                                    AND t2.AdvFundSourceId NOT IN ( 1,19,23 )
                                    AND t3.StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
						( SELECT    t1.StuEnrollId
                          FROM      arStuEnrollments t1
                                   ,syStatusCodes t2
                          WHERE     t1.StatusCodeId = t2.StatusCodeId
                                    AND StartDate <= @EndDate
                                    AND -- Student started before the end date range
                                    LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                    AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                    AND (
                                          t1.DateDetermined < @AcademicStartDate
                                          OR ExpGradDate < @AcademicStartDate
                                          OR LDA < @AcademicStartDate
                                        ) )
                                    AND t2.IPEDSValue IN ( 66,67,68 )
                        ) AS GRNT3
                       ,(
                          SELECT    SUM(TransAmount * -1) AS NetAmount
                          FROM      dbo.saTransactions t1
                          INNER JOIN saFundSources t2 ON t1.FundSourceId = t2.FundSourceId
                          INNER JOIN arStuEnrollments t3 ON t1.StuEnrollId = t3.StuEnrollId
                                                            AND t3.StudentId = SL.StudentId
                          INNER JOIN arStudent t4 ON t4.StudentId = t3.StudentId
                          INNER JOIN syFamilyIncome t5 ON t4.FamilyIncome = t5.FamilyIncomeID
                          WHERE     t2.TitleIV = 1
                                    AND t5.IPEDSValue = 151
                                    AND ISNULL(t2.AdvFundSourceId,0) NOT IN ( 5,6,9 ) -- FFEL PLUS (9)(added for DE8816)
                                    AND ( t1.TransDate BETWEEN @StartDateMinus1Yr AND @EndDate )
                                    AND t1.Voided = 0
                                    AND t2.AdvFundSourceId NOT IN ( 1,19,23 )
                                    AND t3.StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
						( SELECT    t1.StuEnrollId
                          FROM      arStuEnrollments t1
                                   ,syStatusCodes t2
                          WHERE     t1.StatusCodeId = t2.StatusCodeId
                                    AND StartDate <= @AcademicEndDate
                                    AND -- Student started before the end date range
                                    LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                    AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                    AND (
                                          t1.DateDetermined < @StartDateMinus1Yr
                                          OR ExpGradDate < @StartDateMinus1Yr
                                          OR LDA < @StartDateMinus1Yr
                                        ) )
                                    AND t2.IPEDSValue IN ( 66,67,68 )
                        ) AS INC3Aid
                       ,	
		
		--(SELECT COUNT(DISTINCT t1.StudentId) FROM arStudent t1 INNER JOIN syFamilyIncome t2 ON t1.FamilyIncome=t2.FamilyIncomeID 
		--where t1.StudentId=SL.StudentId AND t2.IPEDSValue=152) AS INC4,
                        (
                          SELECT    CASE WHEN SUM(TransAmount * -1) > 0 THEN 1
                                         ELSE 0
                                    END AS NetAmount
                          FROM      dbo.saTransactions t1
                          INNER JOIN saFundSources t2 ON t1.FundSourceId = t2.FundSourceId
                          INNER JOIN arStuEnrollments t3 ON t1.StuEnrollId = t3.StuEnrollId
                                                            AND t3.StudentId = SL.StudentId
                          INNER JOIN arStudent t4 ON t4.StudentId = t3.StudentId
                          INNER JOIN syFamilyIncome t5 ON t4.FamilyIncome = t5.FamilyIncomeID
                          WHERE     t2.TitleIV = 1
                                    AND t5.IPEDSValue = 152
                                    AND ISNULL(t2.AdvFundSourceId,0) NOT IN ( 5,6,9 ) -- FFEL PLUS (9)(added for DE8816)
                                    AND ( t1.TransDate BETWEEN @StartDate AND @EndDate )
                                    AND t1.Voided = 0
                                    AND t2.AdvFundSourceId NOT IN ( 1,19,23 )
                                    AND t3.StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
						( SELECT    t1.StuEnrollId
                          FROM      arStuEnrollments t1
                                   ,syStatusCodes t2
                          WHERE     t1.StatusCodeId = t2.StatusCodeId
                                    AND StartDate <= @EndDate
                                    AND -- Student started before the end date range
                                    LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                    AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                    AND (
                                          t1.DateDetermined < @AcademicStartDate
                                          OR ExpGradDate < @AcademicStartDate
                                          OR LDA < @AcademicStartDate
                                        ) ) 
		--AND t2.IPEDSValue IN (66,67,68)
                        ) AS INC4
                       ,(
                          SELECT    CASE WHEN SUM(TransAmount * -1) > 0 THEN 1
                                         ELSE 0
                                    END AS NetAmount
                          FROM      dbo.saTransactions t1
                          INNER JOIN saFundSources t2 ON t1.FundSourceId = t2.FundSourceId
                          INNER JOIN arStuEnrollments t3 ON t1.StuEnrollId = t3.StuEnrollId
                                                            AND t3.StudentId = SL.StudentId
                          INNER JOIN arStudent t4 ON t4.StudentId = t3.StudentId
                          INNER JOIN syFamilyIncome t5 ON t4.FamilyIncome = t5.FamilyIncomeID
                          WHERE     t2.TitleIV = 1
                                    AND t5.IPEDSValue = 152
                                    AND ISNULL(t2.AdvFundSourceId,0) NOT IN ( 5,6,9 ) -- FFEL PLUS (9)(added for DE8816)
                                    AND ( t1.TransDate BETWEEN @StartDate AND @EndDate )
                                    AND t1.Voided = 0
                                    AND t2.AdvFundSourceId NOT IN ( 1,19,23 )
                                    AND t3.StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
						( SELECT    t1.StuEnrollId
                          FROM      arStuEnrollments t1
                                   ,syStatusCodes t2
                          WHERE     t1.StatusCodeId = t2.StatusCodeId
                                    AND StartDate <= @EndDate
                                    AND -- Student started before the end date range
                                    LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                    AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                    AND (
                                          t1.DateDetermined < @AcademicStartDate
                                          OR ExpGradDate < @AcademicStartDate
                                          OR LDA < @AcademicStartDate
                                        ) )
                                    AND t2.IPEDSValue IN ( 66,67,68 )
                        ) AS GRNT4
                       ,(
                          SELECT    SUM(TransAmount * -1) AS NetAmount
                          FROM      dbo.saTransactions t1
                          INNER JOIN saFundSources t2 ON t1.FundSourceId = t2.FundSourceId
                          INNER JOIN arStuEnrollments t3 ON t1.StuEnrollId = t3.StuEnrollId
                                                            AND t3.StudentId = SL.StudentId
                          INNER JOIN arStudent t4 ON t4.StudentId = t3.StudentId
                          INNER JOIN syFamilyIncome t5 ON t4.FamilyIncome = t5.FamilyIncomeID
                          WHERE     t2.TitleIV = 1
                                    AND t5.IPEDSValue = 152
                                    AND ISNULL(t2.AdvFundSourceId,0) NOT IN ( 5,6,9 ) -- FFEL PLUS (9)(added for DE8816)
                                    AND ( t1.TransDate BETWEEN @StartDateMinus1Yr AND @EndDate )
                                    AND t1.Voided = 0
                                    AND t2.AdvFundSourceId NOT IN ( 1,19,23 )
                                    AND t3.StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
						( SELECT    t1.StuEnrollId
                          FROM      arStuEnrollments t1
                                   ,syStatusCodes t2
                          WHERE     t1.StatusCodeId = t2.StatusCodeId
                                    AND StartDate <= @AcademicEndDate
                                    AND -- Student started before the end date range
                                    LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                    AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                    AND (
                                          t1.DateDetermined < @StartDateMinus1Yr
                                          OR ExpGradDate < @StartDateMinus1Yr
                                          OR LDA < @StartDateMinus1Yr
                                        ) )
                                    AND t2.IPEDSValue IN ( 66,67,68 )
                        ) AS INC4Aid
                       ,	
		
		--(SELECT COUNT(DISTINCT t1.StudentId) FROM arStudent t1 INNER JOIN syFamilyIncome t2 ON t1.FamilyIncome=t2.FamilyIncomeID 
		--where t1.StudentId=SL.StudentId AND t2.IPEDSValue=153) AS INC5,
                        (
                          SELECT    CASE WHEN SUM(TransAmount * -1) > 0 THEN 1
                                         ELSE 0
                                    END AS NetAmount
                          FROM      dbo.saTransactions t1
                          INNER JOIN saFundSources t2 ON t1.FundSourceId = t2.FundSourceId
                          INNER JOIN arStuEnrollments t3 ON t1.StuEnrollId = t3.StuEnrollId
                                                            AND t3.StudentId = SL.StudentId
                          INNER JOIN arStudent t4 ON t4.StudentId = t3.StudentId
                          INNER JOIN syFamilyIncome t5 ON t4.FamilyIncome = t5.FamilyIncomeID
                          WHERE     t2.TitleIV = 1
                                    AND t5.IPEDSValue = 153
                                    AND ISNULL(t2.AdvFundSourceId,0) NOT IN ( 5,6,9 ) -- FFEL PLUS (9)(added for DE8816)
                                    AND ( t1.TransDate BETWEEN @StartDate AND @EndDate )
                                    AND t1.Voided = 0
                                    AND t2.AdvFundSourceId NOT IN ( 1,19,23 )
                                    AND t3.StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
						( SELECT    t1.StuEnrollId
                          FROM      arStuEnrollments t1
                                   ,syStatusCodes t2
                          WHERE     t1.StatusCodeId = t2.StatusCodeId
                                    AND StartDate <= @EndDate
                                    AND -- Student started before the end date range
                                    LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                    AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                    AND (
                                          t1.DateDetermined < @AcademicStartDate
                                          OR ExpGradDate < @AcademicStartDate
                                          OR LDA < @AcademicStartDate
                                        ) ) 
		--AND t2.IPEDSValue IN (66,67,68)
                        ) AS INC5
                       ,(
                          SELECT    CASE WHEN SUM(TransAmount * -1) > 0 THEN 1
                                         ELSE 0
                                    END AS NetAmount
                          FROM      dbo.saTransactions t1
                          INNER JOIN saFundSources t2 ON t1.FundSourceId = t2.FundSourceId
                          INNER JOIN arStuEnrollments t3 ON t1.StuEnrollId = t3.StuEnrollId
                                                            AND t3.StudentId = SL.StudentId
                          INNER JOIN arStudent t4 ON t4.StudentId = t3.StudentId
                          INNER JOIN syFamilyIncome t5 ON t4.FamilyIncome = t5.FamilyIncomeID
                          WHERE     t2.TitleIV = 1
                                    AND t5.IPEDSValue = 153
                                    AND ISNULL(t2.AdvFundSourceId,0) NOT IN ( 5,6,9 ) -- FFEL PLUS (9)(added for DE8816)
                                    AND ( t1.TransDate BETWEEN @StartDate AND @EndDate )
                                    AND t1.Voided = 0
                                    AND t2.AdvFundSourceId NOT IN ( 1,19,23 )
                                    AND t3.StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
						( SELECT    t1.StuEnrollId
                          FROM      arStuEnrollments t1
                                   ,syStatusCodes t2
                          WHERE     t1.StatusCodeId = t2.StatusCodeId
                                    AND StartDate <= @EndDate
                                    AND -- Student started before the end date range
                                    LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                    AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                    AND (
                                          t1.DateDetermined < @AcademicStartDate
                                          OR ExpGradDate < @AcademicStartDate
                                          OR LDA < @AcademicStartDate
                                        ) )
                                    AND t2.IPEDSValue IN ( 66,67,68 )
                        ) AS GRNT5
                       ,(
                          SELECT    SUM(TransAmount * -1) AS NetAmount
                          FROM      dbo.saTransactions t1
                          INNER JOIN saFundSources t2 ON t1.FundSourceId = t2.FundSourceId
                          INNER JOIN arStuEnrollments t3 ON t1.StuEnrollId = t3.StuEnrollId
                                                            AND t3.StudentId = SL.StudentId
                          INNER JOIN arStudent t4 ON t4.StudentId = t3.StudentId
                          INNER JOIN syFamilyIncome t5 ON t4.FamilyIncome = t5.FamilyIncomeID
                          WHERE     t2.TitleIV = 1
                                    AND t5.IPEDSValue = 153
                                    AND ISNULL(t2.AdvFundSourceId,0) NOT IN ( 5,6,9 ) -- FFEL PLUS (9)(added for DE8816)
                                    AND ( t1.TransDate BETWEEN @StartDateMinus1Yr AND @EndDate )
                                    AND t1.Voided = 0
                                    AND t2.AdvFundSourceId NOT IN ( 1,19,23 )
                                    AND t3.StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
						( SELECT    t1.StuEnrollId
                          FROM      arStuEnrollments t1
                                   ,syStatusCodes t2
                          WHERE     t1.StatusCodeId = t2.StatusCodeId
                                    AND StartDate <= @AcademicEndDate
                                    AND -- Student started before the end date range
                                    LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                    AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                    AND (
                                          t1.DateDetermined < @StartDateMinus1Yr
                                          OR ExpGradDate < @StartDateMinus1Yr
                                          OR LDA < @StartDateMinus1Yr
                                        ) )
                                    AND t2.IPEDSValue IN ( 66,67,68 )
                        ) AS INC5Aid
              FROM      #StudentsList1 SL
            ) dt
    ORDER BY CASE WHEN @OrderBy = 'SSN' THEN dt.SSN
             END
           ,CASE WHEN @OrderBy = 'LastName' THEN dt.StudentName
            END
           ,
            -- Updated 11/27/2012 - sort by student number not working
			--Case When @OrderBy='StudentNumber' Then Convert(int,dt.StudentNumber)
            CASE WHEN @OrderBy = 'Student Number' THEN dt.StudentNumber
            END;
		
		
    CREATE TABLE #CohortYearMinus1
        (
         SSN_CohortYrMinus1 VARCHAR(11)
        ,StudentNumber_CohortYrMinus1 VARCHAR(50)
        ,StudentName_CohortYrMinus1 VARCHAR(200)
        ,INC1X_CohortYrMinus1 VARCHAR(1)
        ,GRNT1X_CohortYrMinus1 VARCHAR(1)
        ,INC1_CohortYrMinus1 INT
        ,GRNT1_CohortYrMinus1 INT
        ,INC1Aid_CohortYrMinus1 DECIMAL(18,2)
        ,INC2X_CohortYrMinus1 VARCHAR(1)
        ,GRNT2X_CohortYrMinus1 VARCHAR(1)
        ,INC2_CohortYrMinus1 INT
        ,GRNT2_CohortYrMinus1 INT
        ,INC2Aid_CohortYrMinus1 DECIMAL(18,2)
        ,INC3X_CohortYrMinus1 VARCHAR(1)
        ,GRNT3X_CohortYrMinus1 VARCHAR(1)
        ,INC3_CohortYrMinus1 INT
        ,GRNT3_CohortYrMinus1 INT
        ,INC3Aid_CohortYrMinus1 DECIMAL(18,2)
        ,INC4X_CohortYrMinus1 VARCHAR(1)
        ,GRNT4X_CohortYrMinus1 VARCHAR(1)
        ,INC4_CohortYrMinus1 INT
        ,GRNT4_CohortYrMinus1 INT
        ,INC4Aid_CohortYrMinus1 DECIMAL(18,2)
        ,INC5X_CohortYrMinus1 VARCHAR(1)
        ,GRNT5X_CohortYrMinus1 VARCHAR(1)
        ,INC5_CohortYrMinus1 INT
        ,GRNT5_CohortYrMinus1 INT
        ,INC5Aid_CohortYrMinus1 DECIMAL(18,2)
        );	
		
    INSERT  INTO #CohortYearMinus1
            SELECT  SSN
                   ,StudentNumber
                   ,StudentName
                   ,INC1X
                   ,GRNT1X
                   ,INC1
                   ,GRNT1
                   ,SUM(INC1Aid) AS INC1Aid
                   ,INC2X
                   ,GRNT2X
                   ,INC2
                   ,GRNT2
                   ,SUM(INC2Aid) AS INC2Aid
                   ,INC3X
                   ,GRNT3X
                   ,INC3
                   ,GRNT3
                   ,SUM(INC3Aid) AS INC3Aid
                   ,INC4X
                   ,GRNT4X
                   ,INC4
                   ,GRNT4
                   ,SUM(INC4Aid) AS INC4Aid
                   ,INC5X
                   ,GRNT5X
                   ,INC5
                   ,GRNT5
                   ,SUM(INC5Aid) AS INC5Aid
            FROM    #Result1
            GROUP BY SSN
                   ,StudentNumber
                   ,StudentName
                   ,INC1X
                   ,GRNT1X
                   ,INC1
                   ,GRNT1
                   ,INC2X
                   ,GRNT2X
                   ,INC2
                   ,GRNT2
                   ,INC3X
                   ,GRNT3X
                   ,INC3
                   ,GRNT3
                   ,INC4X
                   ,GRNT4X
                   ,INC4
                   ,GRNT4
                   ,INC5X
                   ,GRNT5X
                   ,INC5
                   ,GRNT5
            ORDER BY CASE WHEN @OrderBy = 'SSN' THEN SSN
                     END
                   ,CASE WHEN @OrderBy = 'LastName' THEN StudentName
                    END
                   ,
			-- Updated 11/27/2012 - sort by student number not working
			--Case When @OrderBy='StudentNumber' Then Convert(int,StudentNumber)
                    CASE WHEN @OrderBy = 'Student Number' THEN StudentNumber
                    END;

    DROP TABLE #UnderGraduateStudentsWhoReceivedFinancialAid1;
    DROP TABLE #StudentsList1;
    DROP TABLE #Result1;

    SELECT  *
    FROM    #CohortYearMinus1;
    DROP TABLE #CohortYearMinus1;



GO
