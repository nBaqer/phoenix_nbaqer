SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_TestScores_SATMath_Detail]
    @CampusId VARCHAR(50)
   ,@ProgId VARCHAR(4000) = NULL
   ,@StartDate DATETIME = NULL
   ,@EndDate DATETIME = NULL
   ,@DateRangeText VARCHAR(100) = NULL
   ,@OrderBy VARCHAR(100)
   ,@AreasOfInterest VARCHAR(4000) = NULL
AS
    BEGIN -- sycampuses

        DECLARE @ContinuingStartDate DATETIME;
        SET @ContinuingStartDate = @StartDate;
-- For Academic Year Reporter, the End Date should be 10/15/Cohort Year
-- and Start Date should be blank
        IF DAY(@EndDate) = 15
            AND MONTH(@EndDate) = 10
            BEGIN
                SET @EndDate = @StartDate; -- it will always be 10/15/cohort year example: for cohort year 2009, it is 10/15/2009
                SET @StartDate = @StartDate;
            END;


        DECLARE @LeadswithScores TABLE
            (
             LeadId UNIQUEIDENTIFIER
            );

        DECLARE @StudentwithScores TABLE
            (
             StudentId UNIQUEIDENTIFIER
            );

        INSERT  INTO @LeadswithScores
                SELECT DISTINCT
                        LET.LeadId
                FROM    dbo.adLeadEntranceTest LET
                INNER JOIN adReqs R ON R.adReqId = LET.EntrTestId
                WHERE   R.IPEDSValue IN ( 162 ) -- SAT Math
                        AND LET.ActualScore IS NOT NULL
                        AND LET.LeadId IS NOT NULL;

        INSERT  INTO @StudentwithScores
                SELECT DISTINCT
                        LET.StudentId
                FROM    dbo.adLeadEntranceTest LET
                INNER JOIN adReqs R ON R.adReqId = LET.EntrTestId
                WHERE   R.IPEDSValue IN ( 162 ) -- SAT Math
                        AND LET.ActualScore IS NOT NULL
                        AND LET.StudentId IS NOT NULL;

        SELECT  *
        FROM    (
                  SELECT    dbo.UDF_FormatSSN(t1.SSN) AS SSN
                           ,t1.StudentNumber
                           ,t1.LastName + ', ' + t1.FirstName + ' ' + ISNULL(t1.MiddleName,'') AS StudentName
                           ,t3.IPEDSValue
                           ,t2.StartDate
                           ,t3.IPEDSSequence AS GenderSequence
                           ,
				-- Men --
                            CASE WHEN ( t3.IPEDSValue = 30 ) THEN 'X'
                                 ELSE ''
                            END AS ApplicantMen
                           ,CASE WHEN ( t3.IPEDSValue = 30 ) THEN 'X'
                                 ELSE ''
                            END AS AdmissionMen
                           ,CASE WHEN ( t3.IPEDSValue = 30 ) THEN 1
                                 ELSE 0
                            END AS ApplicantMenCount
                           ,CASE WHEN ( t3.IPEDSValue = 30 ) THEN 1
                                 ELSE 0
                            END AS AdmissionMenCount
                           ,CASE WHEN (
                                        t10.IPEDSValue = 61
                                        AND t3.IPEDSValue = 30
                                      ) THEN 'X'
                                 ELSE ''
                            END AS FullTimeMen
                           ,CASE WHEN (
                                        t10.IPEDSValue = 62
                                        AND t3.IPEDSValue = 30
                                      ) THEN 'X'
                                 ELSE ''
                            END AS PartTimeMen
                           ,CASE WHEN (
                                        t10.IPEDSValue = 61
                                        AND t3.IPEDSValue = 30
                                      ) THEN 1
                                 ELSE 0
                            END AS FullTimeCountMen
                           ,CASE WHEN (
                                        t10.IPEDSValue = 62
                                        AND t3.IPEDSValue = 30
                                      ) THEN 1
                                 ELSE 0
                            END AS PartTimeCountMen
                           ,
				-- Men --
				-- Women --
                            CASE WHEN ( t3.IPEDSValue = 31 ) THEN 'X'
                                 ELSE ''
                            END AS ApplicantWomen
                           ,CASE WHEN ( t3.IPEDSValue = 31 ) THEN 'X'
                                 ELSE ''
                            END AS AdmissionWomen
                           ,CASE WHEN ( t3.IPEDSValue = 31 ) THEN 1
                                 ELSE 0
                            END AS ApplicantWomenCount
                           ,CASE WHEN ( t3.IPEDSValue = 31 ) THEN 1
                                 ELSE 0
                            END AS AdmissionWomenCount
                           ,CASE WHEN (
                                        t10.IPEDSValue = 61
                                        AND t3.IPEDSValue = 31
                                      ) THEN 'X'
                                 ELSE ''
                            END AS FullTimeWomen
                           ,CASE WHEN (
                                        t10.IPEDSValue = 62
                                        AND t3.IPEDSValue = 31
                                      ) THEN 'X'
                                 ELSE ''
                            END AS PartTimeWomen
                           ,CASE WHEN (
                                        t10.IPEDSValue = 61
                                        AND t3.IPEDSValue = 31
                                      ) THEN 1
                                 ELSE 0
                            END AS FullTimeCountWomen
                           ,CASE WHEN (
                                        t10.IPEDSValue = 62
                                        AND t3.IPEDSValue = 31
                                      ) THEN 1
                                 ELSE 0
                            END AS PartTimeCountWomen
                           ,(
                              SELECT TOP 1
                                        EnrollmentId
                              FROM      arStuEnrollments C1
                                       ,arPrgVersions C2
                                       ,arProgTypes C3
                              WHERE     C1.PrgVerId = C2.PrgVerId
                                        AND C2.ProgTypId = C3.ProgTypId
                                        AND C3.IPEDSValue = 58
                                        AND C1.StudentId = t1.StudentId
                              ORDER BY  C1.StartDate
                                       ,C1.EnrollDate
                            ) AS EnrollmentId
                           ,(
                              SELECT TOP 1
                                        ActualScore
                              FROM      dbo.adLeadEntranceTest LET
                              INNER JOIN adReqs R ON R.adReqId = LET.EntrTestId
                              WHERE     (
                                          LET.LeadId = t2.LeadId
                                          OR LET.StudentId = t1.StudentId
                                        )
                                        AND R.IPEDSValue IN ( 162 ) -- SAT Math
                                        AND LET.ActualScore IS NOT NULL
                              ORDER BY  LET.ActualScore DESC
                            ) AS Score
                  FROM      arStudent t1
                  LEFT JOIN adGenders t3 ON t1.Gender = t3.GenderId
                  LEFT JOIN adEthCodes t4 ON t4.EthCodeId = t1.Race
                  INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                  LEFT JOIN adDegCertSeeking t11 ON t11.DegCertSeekingId = t2.DegCertSeekingId ---- For Degree Seeking --
                  INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                  INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                               AND t6.SysStatusId NOT IN ( 8 )
                  INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                  INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                  INNER JOIN arPrgGrp t12 ON t12.PrgGrpId = t7.PrgGrpId
                  INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                  LEFT JOIN arAttendTypes t10 ON t2.AttendTypeId = t10.AttendTypeId
                  WHERE     t2.CampusId = @CampusId
                            AND (
                                  @ProgId IS NULL
                                  OR t8.ProgId IN ( SELECT  Val
                                                    FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                )
                            AND t9.IPEDSValue = 58
                            AND -- Under Graduate
                            (
                              t10.IPEDSValue = 61
                              OR t10.IPEDSValue = 62
                            )
                            AND --Part Time or Full Time
                            (
                              t3.IPEDSValue = 30
                              OR t3.IPEDSValue = 31
                            )
                            AND -- Men or Women
                            t1.Race IS NOT NULL
                            AND t6.SysStatusId NOT IN ( 8 )  -- Exclude students who are No Start Students
                            AND t2.StartDate <= @EndDate
                            AND t2.StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						( SELECT    t1.StuEnrollId
                          FROM      arStuEnrollments t1
                                   ,syStatusCodes t2
                          WHERE     t1.StatusCodeId = t2.StatusCodeId
                                    AND StartDate <= @EndDate
                                    AND -- Student started before the end date range
                                    LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                    AND (
                                          @ProgId IS NULL
                                          OR t8.ProgId IN ( SELECT  Val
                                                            FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                        )
                                    AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                    AND (
                                          t1.DateDetermined < @StartDate
                                          OR ExpGradDate < @StartDate
                                          OR LDA < @StartDate
                                        ) )
					-- If Student is enrolled in only program version and if that program version 
						-- happens to be a continuing ed program exclude the student
                            AND t2.StudentId NOT IN ( SELECT DISTINCT
                                                                StudentId
                                                      FROM      (
                                                                  SELECT    StudentId
                                                                           ,COUNT(*) AS RowCounter
                                                                  FROM      arStuEnrollments
                                                                  WHERE     PrgVerId IN ( SELECT    PrgVerId
                                                                                          FROM      arPrgVersions
                                                                                          WHERE     IsContinuingEd = 1 )
                                                                  GROUP BY  StudentId
                                                                  HAVING    COUNT(*) = 1
                                                                ) dtStudent_ContinuingEd )
					--DE8492
					-- Transfer-In Students need to be excluded
                            AND t2.StuEnrollId NOT IN ( SELECT DISTINCT
                                                                StuEnrollId
                                                        FROM    arStuEnrollments
                                                        WHERE   CampusId = LTRIM(RTRIM(@CampusId))
                                                                AND LeadId IS NOT NULL
                                                                AND StuEnrollId IN ( SELECT StuEnrollId
                                                                                     FROM   arStuEnrollments
                                                                                     WHERE  TransferHours > 0
                                                                                            AND CampusId = LTRIM(RTRIM(@CampusId))
                                                                                     UNION
                                                                                     SELECT StuEnrollId
                                                                                     FROM   arTransferGrades
                                                                                     WHERE  GrdSysDetailId IN ( SELECT  GrdSysDetailId
                                                                                                                FROM    arGradeSystemDetails
                                                                                                                WHERE   IsTransferGrade = 1 ) ) )
                            AND ( t11.IPEDSValue = 11 ) -- Degree/Cert Seeking
                            AND (
                                  @AreasOfInterest IS NULL
                                  OR t12.PrgGrpId IN ( SELECT   Val
                                                       FROM     MultipleValuesForReportParameters(@AreasOfInterest,',',1) )
                                ) --Areas Of Intrest --
                            AND (
                                  t2.LeadId IN ( SELECT DISTINCT
                                                        LeadId
                                                 FROM   @LeadswithScores )
                                  OR t2.StudentId IN ( SELECT DISTINCT
                                                                StudentId
                                                       FROM     @StudentwithScores )
                                )
                ) dt
        ORDER BY CASE WHEN @OrderBy = 'SSN' THEN SSN
                 END
               ,CASE WHEN @OrderBy = 'LastName' THEN StudentName
                END
               , -- LastName end,
                CASE WHEN @OrderBy = 'Student Number' THEN StudentNumber
                END
               ,CASE WHEN @OrderBy = 'EnrollmentId' THEN EnrollmentId
                END;
    END;
GO
