SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_GetGradeDescriptionsForTranscript]
    (
     @stuEnrollId UNIQUEIDENTIFIER
    )
AS
    EXEC dbo.USP_GetGradeDescriptionsForEnrollment @stuEnrollId;
    EXEC dbo.USP_GetGradeDescriptionsForEnrollmentNotNULL @stuEnrollId;

GO
