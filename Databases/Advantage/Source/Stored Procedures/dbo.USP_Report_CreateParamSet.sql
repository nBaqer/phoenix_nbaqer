SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_Report_CreateParamSet]
    @SetName AS VARCHAR(MAX)
   ,@ReportName AS VARCHAR(MAX)
   ,@Type AS VARCHAR(MAX)
-- WITH ENCRYPTION, RECOMPILE, EXECUTE AS CALLER|SELF|OWNER| 'user_name'
AS
    DECLARE @Return_Set INT = 0;
    IF NOT EXISTS (
                  SELECT TOP 1 SetId
                  FROM   dbo.ParamSet
                  WHERE  SetName = @SetName
                  )
        BEGIN
            DECLARE @FilterSet VARCHAR(MAX) = @ReportName + 'Filter Set';
            INSERT INTO dbo.ParamSet (
                                     SetName
                                    ,SetDisplayName
                                    ,SetDescription
                                    ,SetType
                                     )
            VALUES ( @SetName    -- SetName - nvarchar(50)
                    ,@FilterSet  -- SetDisplayName - nvarchar(50)
                    ,@ReportName -- SetDescription - nvarchar(1000)
                    ,@Type       -- SetType - nvarchar(50)
                );
        END;

    SET @Return_Set = (
                      SELECT TOP 1 SetId
                      FROM   dbo.ParamSet
                      WHERE  SetName = @SetName
                      );



    RETURN @Return_Set;
GO
