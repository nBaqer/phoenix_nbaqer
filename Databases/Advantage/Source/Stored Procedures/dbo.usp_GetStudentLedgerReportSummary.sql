SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_GetStudentLedgerReportSummary]
    @StudentEnrollmentId UNIQUEIDENTIFIER
AS
    SELECT   RTRIM(LTRIM(Summary.FundSourceDescrip)) AS Description
            ,Summary.AwardTypeId
            ,FORMAT((
                    SELECT SUM(GrossAmount) - SUM(TA.LoanFees)
                    FROM   dbo.faStudentAwards TA
                    WHERE  TA.AwardTypeId = Summary.AwardTypeId
                           AND TA.StuEnrollId = @StudentEnrollmentId
                    )
                   ,'C'
                   ) AS Amount
            ,FORMAT(SUM(   CASE WHEN Summary.IsDisbursement = 1 THEN Summary.Amount
                                ELSE 0
                           END
                       )
                   ,'C'
                   ) AS Received
            ,FORMAT(SUM(   CASE WHEN Summary.IsDisbursement = 0 THEN Summary.Amount
                                ELSE 0
                           END
                       )
                   ,'C'
                   ) AS Refunded
				   ,ROW_NUMBER() OVER (ORDER BY Summary.FundSourceDescrip) AS RowNumber
    FROM     (
             SELECT    D.PmtDisbRelId AS Id
                      ,D.Amount AS Amount
                      ,S.StudentAwardId AS AwardId
                      ,A.AwardTypeId
                      ,FS.FundSourceDescrip
                      ,A.GrossAmount
                      ,1 AS IsDisbursement
             FROM      dbo.faStudentAwardSchedule S
             LEFT JOIN dbo.saPmtDisbRel D ON S.AwardScheduleId = D.AwardScheduleId
             LEFT JOIN dbo.faStudentAwards A ON A.StudentAwardId = S.StudentAwardId
             LEFT JOIN dbo.saFundSources FS ON FS.FundSourceId = A.AwardTypeId
             LEFT JOIN dbo.saTransactions T ON T.TransactionId = D.TransactionId
             WHERE     A.StuEnrollId = @StudentEnrollmentId
                       AND (
                           T.Voided = 0
                           OR T.TransactionId IS NULL
                           )
             UNION
             SELECT    R.TransactionId AS Id
                      ,R.RefundAmount AS Amount
                      ,S.StudentAwardId AS AwardId
                      ,A.AwardTypeId
                      ,FS.FundSourceDescrip
                      ,A.GrossAmount
                      ,0 AS IsDisbursement
             FROM      dbo.faStudentAwardSchedule S
             LEFT JOIN dbo.saRefunds R ON S.AwardScheduleId = R.AwardScheduleId
             LEFT JOIN dbo.faStudentAwards A ON A.StudentAwardId = S.StudentAwardId
             LEFT JOIN dbo.saFundSources FS ON FS.FundSourceId = A.AwardTypeId
             LEFT JOIN dbo.saTransactions T ON T.TransactionId = R.TransactionId
             WHERE     A.StuEnrollId = @StudentEnrollmentId
                       AND (
                           T.Voided = 0
                           OR T.TransactionId IS NULL
                           )
             ) Summary
    GROUP BY Summary.AwardTypeId
            ,Summary.FundSourceDescrip;






GO
