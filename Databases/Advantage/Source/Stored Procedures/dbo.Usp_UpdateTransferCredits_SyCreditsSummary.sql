SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =========================================================================================================
-- Usp_UpdateTransferCredits_SyCreditsSummary
-- =========================================================================================================
CREATE PROCEDURE [dbo].[Usp_UpdateTransferCredits_SyCreditsSummary]
    @StuEnrollIdList NVARCHAR(MAX) = NULL
AS
    BEGIN
        DECLARE @PrgVerId UNIQUEIDENTIFIER
               ,@rownumber INT
               ,@StuEnrollId UNIQUEIDENTIFIER;
        DECLARE @PrevStuEnrollId UNIQUEIDENTIFIER
               ,@PrevReqId UNIQUEIDENTIFIER
               ,@PrevTermId UNIQUEIDENTIFIER
               ,@CreditsAttempted DECIMAL(18, 2)
               ,@CreditsEarned DECIMAL(18, 2)
               ,@TermId UNIQUEIDENTIFIER
               ,@TermDescrip VARCHAR(50);
        DECLARE @reqid UNIQUEIDENTIFIER
               ,@CourseCodeDescrip VARCHAR(50)
               ,@FinalGrade UNIQUEIDENTIFIER
               ,@FinalScore DECIMAL(18, 2)
               ,@ClsSectionId UNIQUEIDENTIFIER
               ,@Grade VARCHAR(50)
               ,@IsGradeBookNotSatisified BIT
               ,@TermStartDate DATETIME;
        DECLARE @IsPass BIT
               ,@IsCreditsAttempted BIT
               ,@IsCreditsEarned BIT
               ,@Completed BIT
               ,@CurrentScore DECIMAL(18, 2)
               ,@CurrentGrade VARCHAR(10)
               ,@FinalGradeDesc VARCHAR(50)
               ,@FinalGPA DECIMAL(18, 2)
               ,@GrdBkResultId UNIQUEIDENTIFIER;
        DECLARE @Product_WeightedAverage_Credits_GPA DECIMAL(18, 2)
               ,@Count_WeightedAverage_Credits DECIMAL(18, 2)
               ,@Product_SimpleAverage_Credits_GPA DECIMAL(18, 2)
               ,@Count_SimpleAverage_Credits DECIMAL(18, 2);
        DECLARE @CreditsPerService DECIMAL(18, 2)
               ,@NumberOfServicesAttempted INT
               ,@boolCourseHasLabWorkOrLabHours INT
               ,@sysComponentTypeId INT
               ,@RowCount INT;
        DECLARE @decGPALoop DECIMAL(18, 2)
               ,@intCourseCount INT
               ,@decWeightedGPALoop DECIMAL(18, 2)
               ,@IsInGPA BIT
               ,@isGradeEligibleForCreditsEarned BIT
               ,@isGradeEligibleForCreditsAttempted BIT;
        DECLARE @ComputedSimpleGPA DECIMAL(18, 2)
               ,@ComputedWeightedGPA DECIMAL(18, 2)
               ,@CourseCredits DECIMAL(18, 2);
        DECLARE @FinAidCreditsEarned DECIMAL(18, 2)
               ,@FinAidCredits DECIMAL(18, 2)
               ,@TermAverage DECIMAL(18, 2)
               ,@TermAverageCount INT;
        DECLARE @IsWeighted INT;
        DECLARE @IsTransferred BIT;
        DECLARE @MyEnrollments TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
            );
        INSERT INTO @MyEnrollments
                    SELECT enrollments.StuEnrollId
                    FROM   dbo.arStuEnrollments enrollments
                    WHERE  (
                           @StuEnrollIdList IS NULL
                           OR ( enrollments.StuEnrollId IN (
                                                           SELECT Val
                                                           FROM   MultipleValuesForReportParameters(@StuEnrollIdList, ',', 1) 
                                                           )
                              )
                           );
        SET @decGPALoop = 0;
        SET @intCourseCount = 0;
        SET @decWeightedGPALoop = 0;
        SET @ComputedSimpleGPA = 0;
        SET @ComputedWeightedGPA = 0;
        SET @CourseCredits = 0;
        DECLARE GetCreditsSummary_Cursor CURSOR FOR
            SELECT     DISTINCT SE.StuEnrollId
                               ,T.TermId
                               ,T.TermDescrip
                               ,T.StartDate
                               ,R.ReqId
                               ,'(' + R.Code + ' ) ' + R.Descrip AS CourseCodeDescrip
                               ,RES.Score AS FinalScore
                               ,RES.GrdSysDetailId AS FinalGrade
                               ,NULL
                               ,R.Credits AS CreditsAttempted
                               ,NULL AS ClsSectionId
                               ,GSD.Grade
                               ,GSD.IsPass
                               ,GSD.IsCreditsAttempted
                               ,GSD.IsCreditsEarned
                               ,SE.PrgVerId
                               ,GSD.IsInGPA
                               ,R.FinAidCredits AS FinAidCredits
                               ,RES.IsTransferred
            FROM       arStuEnrollments SE
            INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
            INNER JOIN arTransferGrades RES ON RES.StuEnrollId = SE.StuEnrollId
            --INNER JOIN arClassSections CS ON RES.ReqId = CS.ReqId and RES.TermId=CS.TermId 
            INNER JOIN arTerm T ON RES.TermId = T.TermId
            INNER JOIN arReqs R ON RES.ReqId = R.ReqId
            INNER JOIN @MyEnrollments ON [@MyEnrollments].StuEnrollId = RES.StuEnrollId
            --LEFT JOIN arGrdBkResults GBR ON CS.ClsSectionId=GBR.ClsSectionId
            --LEFT JOIN arGrdBkWgtDetails GBWD ON GBR.InstrGrdBkWgtDetailId = GBWD.InstrGrdBkWgtDetailId
            --LEFT JOIN arGrdComponentTypes GCT on GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId 
            LEFT JOIN  arGradeSystemDetails GSD ON RES.GrdSysDetailId = GSD.GrdSysDetailId
            ORDER BY   T.StartDate
                      ,T.TermDescrip
                      ,R.ReqId;
        OPEN GetCreditsSummary_Cursor;
        SET @PrevStuEnrollId = NULL;
        SET @PrevTermId = NULL;
        SET @PrevReqId = NULL;
        SET @RowCount = 0;
        FETCH NEXT FROM GetCreditsSummary_Cursor
        INTO @StuEnrollId
            ,@TermId
            ,@TermDescrip
            ,@TermStartDate
            ,@reqid
            ,@CourseCodeDescrip
            ,@FinalScore
            ,@FinalGrade
            ,@sysComponentTypeId
            ,@CreditsAttempted
            ,@ClsSectionId
            ,@Grade
            ,@IsPass
            ,@IsCreditsAttempted
            ,@IsCreditsEarned
            ,@PrgVerId
            ,@IsInGPA
            ,@FinAidCredits
            ,@IsTransferred; --,@GrdBkResultId
        WHILE @@FETCH_STATUS = 0
            BEGIN
                SET @CourseCredits = @CreditsAttempted;
                SET @RowCount = @RowCount + 1;
                SET @IsGradeBookNotSatisified = (
                                                SELECT COUNT(*) AS UnsatisfiedWorkUnits
                                                FROM   (
                                                       SELECT D.*
                                                             ,CASE WHEN D.MinimumScore > D.Score THEN ( D.MinimumScore - D.Score )
                                                                   ELSE 0
                                                              END AS Remaining
                                                             ,CASE WHEN ( D.MinimumScore > D.Score )
                                                                        AND ( D.MustPass = 1 ) THEN 0
                                                                   WHEN D.Score IS NULL
                                                                        AND ( D.Required = 1 ) THEN 0
                                                                   ELSE 1
                                                              END AS IsWorkUnitSatisfied
                                                       FROM   (
                                                              SELECT     T.TermId
                                                                        ,T.TermDescrip
                                                                        ,'(' + R.Code + ' ) ' + R.Descrip AS CourseCodeDescrip
                                                                        ,R.ReqId
                                                                        ,RTRIM(GCT.Descrip) AS GradeBookDescription
                                                                        ,( CASE WHEN GCT.SysComponentTypeId IN ( 500, 503, 504 ) THEN GBWD.Number
                                                                                ELSE (
                                                                                     SELECT MIN(MinVal)
                                                                                     FROM   arGradeScaleDetails GSD
                                                                                           ,arGradeSystemDetails GSS
                                                                                     WHERE  GSD.GrdSysDetailId = GSS.GrdSysDetailId
                                                                                            AND GSS.IsPass = 1
                                                                                     )
                                                                           END
                                                                         ) AS MinimumScore
                                                                        ,GBR.Score AS Score
                                                                        ,GBWD.Weight AS Weight
                                                                        ,RES.Score AS FinalScore
                                                                        ,RES.GrdSysDetailId AS FinalGrade
                                                                        ,GBWD.Required
                                                                        ,GBWD.MustPass
                                                                        ,GBWD.GrdPolicyId
                                                                        ,( CASE GCT.SysComponentTypeId
                                                                                WHEN 544 THEN (
                                                                                              SELECT SUM(HoursAttended)
                                                                                              FROM   arExternshipAttendance
                                                                                              WHERE  StuEnrollId = SE.StuEnrollId
                                                                                              )
                                                                                ELSE GBR.Score
                                                                           END
                                                                         ) AS GradeBookResult
                                                                        ,GCT.SysComponentTypeId
                                                                        ,SE.StuEnrollId
                                                                        ,GBR.GrdBkResultId
                                                                        ,R.Credits AS CreditsAttempted
                                                                        ,CS.ClsSectionId
                                                                        ,GSD.Grade
                                                                        ,GSD.IsPass
                                                                        ,GSD.IsCreditsAttempted
                                                                        ,GSD.IsCreditsEarned
                                                              FROM       arStuEnrollments SE
                                                              INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                                                              INNER JOIN arTransferGrades RES ON RES.StuEnrollId = SE.StuEnrollId
                                                              INNER JOIN arClassSections CS ON RES.ReqId = CS.ReqId
                                                                                               AND RES.TermId = CS.TermId
                                                              INNER JOIN arTerm T ON CS.TermId = T.TermId
                                                              INNER JOIN arReqs R ON CS.ReqId = R.ReqId
                                                              LEFT JOIN  arGrdBkResults GBR ON CS.ClsSectionId = GBR.ClsSectionId
                                                                                               AND GBR.StuEnrollId = SE.StuEnrollId
                                                              LEFT JOIN  arGrdBkWgtDetails GBWD ON GBR.InstrGrdBkWgtDetailId = GBWD.InstrGrdBkWgtDetailId
                                                              LEFT JOIN  arGrdComponentTypes GCT ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
                                                              LEFT JOIN  arGradeSystemDetails GSD ON RES.GrdSysDetailId = GSD.GrdSysDetailId
                                                              WHERE      SE.StuEnrollId = @StuEnrollId
                                                                         AND T.TermId = @TermId
                                                                         AND R.ReqId = @reqid
                                                              ) D
                                                       ) E
                                                WHERE  IsWorkUnitSatisfied = 0
                                                );


                --Check if IsCreditsAttempted is set to True
                IF (
                   @IsCreditsAttempted IS NULL
                   OR @IsCreditsAttempted = 0
                   )
                    BEGIN
                        SET @CreditsAttempted = 0;
                    END;
                IF (
                   @IsCreditsEarned IS NULL
                   OR @IsCreditsEarned = 0
                   )
                    BEGIN
                        SET @CreditsEarned = 0;
                    END;

                IF ( @IsGradeBookNotSatisified >= 1 )
                    BEGIN
                        SET @CreditsEarned = 0;
                        SET @Completed = 0;
                    END;
                ELSE
                    BEGIN
                        SET @GrdBkResultId = (
                                             SELECT TOP 1 GrdBkResultId
                                             FROM   arGrdBkResults
                                             WHERE  StuEnrollId = @StuEnrollId
                                                    AND ClsSectionId = @ClsSectionId
                                             );
                        IF @GrdBkResultId IS NOT NULL
                            BEGIN
                                IF ( @IsCreditsEarned = 1 )
                                    BEGIN
                                        SET @CreditsEarned = @CreditsAttempted;
                                        SET @FinAidCreditsEarned = @FinAidCredits;
                                    END;
                                SET @Completed = 1;
                            END;
                        IF (
                           @GrdBkResultId IS NULL
                           AND @Grade IS NOT NULL
                           )
                            BEGIN
                                IF ( @IsCreditsEarned = 1 )
                                    BEGIN
                                        SET @CreditsEarned = @CreditsAttempted;
                                        SET @FinAidCreditsEarned = @FinAidCredits;
                                    END;
                                SET @Completed = 1;
                            END;
                    END;
                IF (
                   @FinalScore IS NOT NULL
                   AND @Grade IS NOT NULL
                   )
                    BEGIN
                        IF ( @IsCreditsEarned = 1 )
                            BEGIN
                                SET @CreditsEarned = @CreditsAttempted;
                                SET @FinAidCreditsEarned = @FinAidCredits;
                            END;
                        SET @Completed = 1;

                    END;

                -- If course is not part of the program version definition do not add credits earned and credits attempted
                -- set the credits earned and attempted to zero
                DECLARE @coursepartofdefinition INT;
                SET @coursepartofdefinition = 0;

                -- Commented by Balaji on 1/16/2016 as it conflicts with equivalent courses
                --AMC Miguel Carabello, course:Swedish & Medical Management
                --	set @coursepartofdefinition = (select COUNT(*) as RowCountOfProgramDefinition from 
                --									(
                --										select * from arProgVerDef where PrgVerId=@PrgVerId and ReqId=@ReqId
                --											union
                --										select * from arProgVerDef where PrgVerId=@PrgVerId and ReqId in
                --											(select GrpId from arReqGrpDef where ReqId=@ReqId)
                --									) dt
                --								)
                --if (@coursepartofdefinition = 0)
                --	begin
                --		set @CreditsEarned = 0
                --		set @CreditsAttempted = 0
                --		set @FinAidCreditsEarned = 0
                --	end


                -- Check the grade scale associated with the class section and figure out of the final score was a passing score
                DECLARE @coursepassrowcount INT;
                SET @coursepassrowcount = 0;
                IF (
                   @FinalScore IS NOT NULL
                   AND @IsTransferred = 0
                   )

                    -- If the student scores 56 and the score is a passing score then we consider this course as completed
                    BEGIN
                        SET @coursepassrowcount = (
                                                  SELECT     COUNT(t2.MinVal) AS IsCourseCompleted
                                                  FROM       arClassSections t1
                                                  INNER JOIN arGradeScaleDetails t2 ON t1.GrdScaleId = t2.GrdScaleId
                                                  INNER JOIN arGradeSystemDetails t3 ON t2.GrdSysDetailId = t3.GrdSysDetailId
                                                  WHERE      t1.ClsSectionId = @ClsSectionId
                                                             AND t3.IsPass = 1
                                                             AND @FinalScore >= t2.MinVal
                                                  );
                        IF @coursepassrowcount >= 1
                            BEGIN
                                SET @Completed = 1;
                            END;
                        ELSE
                            BEGIN
                                SET @Completed = 0;
                            END;
                    END;

                -- If Student Scored a Failing Grade (IsPass set to 0 in Grade System)
                -- then mark this course as Incomplete
                IF ( @FinalGrade IS NOT NULL )
                    BEGIN
                        IF ( @IsPass = 0 )
                            BEGIN
                                SET @Completed = 0;
                                IF ( @IsCreditsEarned = 0 )
                                    BEGIN
                                        SET @CreditsEarned = 0;
                                        SET @FinAidCreditsEarned = 0;
                                    END;
                                IF ( @IsCreditsAttempted = 0 )
                                    BEGIN
                                        SET @CreditsAttempted = 0;
                                    END;
                            END;
                    END;

                SET @CurrentScore = (
                                    SELECT CASE WHEN SUM(GradeBookWeight) > 0 THEN ( SUM(ActualWeight) / SUM(GradeBookWeight)) * 100
                                                ELSE NULL
                                           END AS CurrentScore
                                    FROM   (
                                           SELECT   InstrGrdBkWgtDetailId
                                                   ,Code
                                                   ,Descrip
                                                   ,Weight AS GradeBookWeight
                                                   ,CASE WHEN S.NumberOfComponents > 0 THEN SUM(( Weight * ( Score / NumberOfComponents )) / 100)
                                                         ELSE 0
                                                    END AS ActualWeight
                                           FROM     (
                                                    SELECT   C.InstrGrdBkWgtDetailId
                                                            ,D.Code
                                                            ,D.Descrip
                                                            ,ISNULL(C.Weight, 0) AS Weight
                                                            ,C.Number AS MinNumber
                                                            ,C.GrdPolicyId
                                                            ,C.Parameter AS Param
                                                            ,X.GrdScaleId
                                                            ,SUM(GR.Score) AS Score
                                                            ,COUNT(D.Descrip) AS NumberOfComponents
                                                    FROM     (
                                                             SELECT   DISTINCT TOP 1 A.InstrGrdBkWgtId
                                                                                    ,A.EffectiveDate
                                                                                    ,B.GrdScaleId
                                                             FROM     arGrdBkWeights A
                                                                     ,arClassSections B
                                                             WHERE    A.ReqId = B.ReqId
                                                                      AND A.EffectiveDate <= B.StartDate
                                                                      AND B.ClsSectionId = @ClsSectionId
                                                             ORDER BY A.EffectiveDate DESC
                                                             ) X
                                                            ,arGrdBkWgtDetails C
                                                            ,arGrdComponentTypes D
                                                            ,arGrdBkResults GR
                                                    WHERE    X.InstrGrdBkWgtId = C.InstrGrdBkWgtId
                                                             AND C.GrdComponentTypeId = D.GrdComponentTypeId
                                                             AND C.InstrGrdBkWgtDetailId = GR.InstrGrdBkWgtDetailId
                                                             AND D.SysComponentTypeId NOT IN ( 500, 503 )
                                                             AND GR.StuEnrollId = @StuEnrollId
                                                             AND GR.ClsSectionId = @ClsSectionId
                                                             AND GR.Score IS NOT NULL
                                                    GROUP BY C.InstrGrdBkWgtDetailId
                                                            ,D.Code
                                                            ,D.Descrip
                                                            ,C.Weight
                                                            ,C.Number
                                                            ,C.GrdPolicyId
                                                            ,C.Parameter
                                                            ,X.GrdScaleId
                                                    ) S
                                           GROUP BY InstrGrdBkWgtDetailId
                                                   ,Code
                                                   ,Descrip
                                                   ,Weight
                                                   ,NumberOfComponents
                                           ) FinalTblToComputeCurrentScore
                                    );
                IF ( @CurrentScore IS NULL )
                    BEGIN
                        -- instructor grade books
                        SET @CurrentScore = (
                                            SELECT CASE WHEN SUM(GradeBookWeight) > 0 THEN ( SUM(ActualWeight) / SUM(GradeBookWeight)) * 100
                                                        ELSE NULL
                                                   END AS CurrentScore
                                            FROM   (
                                                   SELECT   InstrGrdBkWgtDetailId
                                                           ,Code
                                                           ,Descrip
                                                           ,Weight AS GradeBookWeight
                                                           ,CASE WHEN S.NumberOfComponents > 0 THEN SUM(( Weight * ( Score / NumberOfComponents )) / 100)
                                                                 ELSE 0
                                                            END AS ActualWeight
                                                   FROM     (
                                                            SELECT   C.InstrGrdBkWgtDetailId
                                                                    ,D.Code
                                                                    ,D.Descrip
                                                                    ,ISNULL(C.Weight, 0) AS Weight
                                                                    ,C.Number AS MinNumber
                                                                    ,C.GrdPolicyId
                                                                    ,C.Parameter AS Param
                                                                    ,X.GrdScaleId
                                                                    ,SUM(GR.Score) AS Score
                                                                    ,COUNT(D.Descrip) AS NumberOfComponents
                                                            FROM     (
                                                                     --SELECT Distinct Top 1 A.InstrGrdBkWgtId,A.EffectiveDate,B.GrdScaleId        
                                                                     --FROM          arGrdBkWeights A,arClassSections B        
                                                                     --WHERE         A.ReqId=B.ReqId AND A.EffectiveDate<=B.StartDate AND B.ClsSectionId=@ClsSectionId
                                                                     --ORDER BY      A.EffectiveDate DESC
                                                                     SELECT DISTINCT TOP 1 t1.InstrGrdBkWgtId
                                                                                          ,t1.GrdScaleId
                                                                     FROM   arClassSections t1
                                                                           ,arGrdBkWeights t2
                                                                     WHERE  t1.InstrGrdBkWgtId = t2.InstrGrdBkWgtId
                                                                            AND t1.ClsSectionId = @ClsSectionId
                                                                     ) X
                                                                    ,arGrdBkWgtDetails C
                                                                    ,arGrdComponentTypes D
                                                                    ,arGrdBkResults GR
                                                            WHERE    X.InstrGrdBkWgtId = C.InstrGrdBkWgtId
                                                                     AND C.GrdComponentTypeId = D.GrdComponentTypeId
                                                                     AND C.InstrGrdBkWgtDetailId = GR.InstrGrdBkWgtDetailId
                                                                     AND
                                                                -- D.SysComponentTypeID not in (500,503) and 
                                                                GR.StuEnrollId = @StuEnrollId
                                                                     AND GR.ClsSectionId = @ClsSectionId
                                                                     AND GR.Score IS NOT NULL
                                                            GROUP BY C.InstrGrdBkWgtDetailId
                                                                    ,D.Code
                                                                    ,D.Descrip
                                                                    ,C.Weight
                                                                    ,C.Number
                                                                    ,C.GrdPolicyId
                                                                    ,C.Parameter
                                                                    ,X.GrdScaleId
                                                            ) S
                                                   GROUP BY InstrGrdBkWgtDetailId
                                                           ,Code
                                                           ,Descrip
                                                           ,Weight
                                                           ,NumberOfComponents
                                                   ) FinalTblToComputeCurrentScore
                                            );

                    END;

                IF ( @CurrentScore IS NOT NULL )
                    BEGIN
                        SET @CurrentGrade = (
                                            SELECT t2.Grade
                                            FROM   arGradeScaleDetails t1
                                                  ,arGradeSystemDetails t2
                                            WHERE  t1.GrdSysDetailId = t2.GrdSysDetailId
                                                   AND t1.GrdScaleId IN (
                                                                        SELECT GrdScaleId
                                                                        FROM   arClassSections
                                                                        WHERE  ClsSectionId = @ClsSectionId
                                                                        )
                                                   AND @CurrentScore >= t1.MinVal
                                                   AND @CurrentScore <= t1.MaxVal
                                            );

                    END;
                ELSE
                    BEGIN
                        SET @CurrentGrade = NULL;
                    END;

                IF (
                   @CurrentScore IS NULL
                   AND @CurrentGrade IS NULL
                   AND @FinalScore IS NULL
                   AND @FinalGrade IS NULL
                   )
                    BEGIN
                        SET @Completed = 0;
                        SET @CreditsAttempted = 0;
                        SET @CreditsEarned = 0;
                        SET @FinAidCreditsEarned = 0;
                    END;

                IF (
                   @FinalScore IS NOT NULL
                   OR @FinalGrade IS NOT NULL
                   )
                    BEGIN

                        SET @FinalGradeDesc = (
                                              SELECT Grade
                                              FROM   arGradeSystemDetails
                                              WHERE  GrdSysDetailId = @FinalGrade
                                              );



                        IF ( @FinalGradeDesc IS NULL )
                            BEGIN
                                SET @FinalGradeDesc = (
                                                      SELECT t2.Grade
                                                      FROM   arGradeScaleDetails t1
                                                            ,arGradeSystemDetails t2
                                                      WHERE  t1.GrdSysDetailId = t2.GrdSysDetailId
                                                             AND t1.GrdScaleId IN (
                                                                                  SELECT GrdScaleId
                                                                                  FROM   arClassSections
                                                                                  WHERE  ClsSectionId = @ClsSectionId
                                                                                  )
                                                             AND @FinalScore >= t1.MinVal
                                                             AND @FinalScore <= t1.MaxVal
                                                      );
                            END;
                        SET @FinalGPA = (
                                        SELECT GPA
                                        FROM   arGradeSystemDetails
                                        WHERE  GrdSysDetailId = @FinalGrade
                                        );
                        IF @FinalGPA IS NULL
                            BEGIN
                                SET @FinalGPA = (
                                                SELECT t2.GPA
                                                FROM   arGradeScaleDetails t1
                                                      ,arGradeSystemDetails t2
                                                WHERE  t1.GrdSysDetailId = t2.GrdSysDetailId
                                                       AND t1.GrdScaleId IN (
                                                                            SELECT GrdScaleId
                                                                            FROM   arClassSections
                                                                            WHERE  ClsSectionId = @ClsSectionId
                                                                            )
                                                       AND @FinalScore >= t1.MinVal
                                                       AND @FinalScore <= t1.MaxVal
                                                );
                            END;
                    END;
                ELSE
                    BEGIN
                        SET @FinalGradeDesc = NULL;
                        SET @FinalGPA = NULL;
                    END;

                --set @IsInGPA = (SELECT t2.IsInGPA FROM arGradeScaleDetails t1,arGradeSystemDetails t2
                --										WHERE  t1.GrdSysDetailId=t2.GrdSysDetailId and 
                --										t1.GrdScaleId In (Select GrdScaleId from arClassSections where ClsSectionId =@ClsSectionId) 
                --										and t2.Grade=@FinalGradeDesc)

                SET @isGradeEligibleForCreditsEarned = (
                                                       SELECT t2.IsCreditsEarned
                                                       FROM   arGradeScaleDetails t1
                                                             ,arGradeSystemDetails t2
                                                       WHERE  t1.GrdSysDetailId = t2.GrdSysDetailId
                                                              AND t1.GrdScaleId IN (
                                                                                   SELECT GrdScaleId
                                                                                   FROM   arClassSections
                                                                                   WHERE  ClsSectionId = @ClsSectionId
                                                                                   )
                                                              AND t2.Grade = @FinalGradeDesc
                                                       );

                SET @isGradeEligibleForCreditsAttempted = (
                                                          SELECT t2.IsCreditsAttempted
                                                          FROM   arGradeScaleDetails t1
                                                                ,arGradeSystemDetails t2
                                                          WHERE  t1.GrdSysDetailId = t2.GrdSysDetailId
                                                                 AND t1.GrdScaleId IN (
                                                                                      SELECT GrdScaleId
                                                                                      FROM   arClassSections
                                                                                      WHERE  ClsSectionId = @ClsSectionId
                                                                                      )
                                                                 AND t2.Grade = @FinalGradeDesc
                                                          );

                IF ( @isGradeEligibleForCreditsEarned IS NULL )
                    BEGIN
                        --Print   'Credits Earned is NULL'
                        SET @isGradeEligibleForCreditsEarned = (
                                                               SELECT TOP 1 t2.IsCreditsEarned
                                                               FROM   arGradeSystemDetails t2
                                                               WHERE  t2.Grade = @FinalGradeDesc
                                                               );
                    END;

                IF ( @isGradeEligibleForCreditsAttempted IS NULL )
                    BEGIN
                        --Print   'Credits Attempted is NULL'
                        SET @isGradeEligibleForCreditsAttempted = (
                                                                  SELECT TOP 1 t2.IsCreditsAttempted
                                                                  FROM   arGradeSystemDetails t2
                                                                  WHERE  t2.Grade = @FinalGradeDesc
                                                                  );
                    END;

                IF @isGradeEligibleForCreditsEarned = 0
                    BEGIN
                        SET @CreditsEarned = 0;
                        SET @FinAidCreditsEarned = 0;
                    END;
                IF @isGradeEligibleForCreditsAttempted = 0
                    BEGIN
                        SET @CreditsAttempted = 0;
                        SET @FinAidCreditsEarned = 0;
                    END;

                IF ( @IsPass = 0 )
                    BEGIN
                        SET @CreditsEarned = 0;
                        SET @FinAidCreditsEarned = 0;
                    END;

                --For Letter Grade Schools if the score is null but final grade was posted then the 
                --Final grade will be the current grade
                IF @CurrentGrade IS NULL
                   AND @FinalGradeDesc IS NOT NULL
                    BEGIN
                        SET @CurrentGrade = @FinalGradeDesc;
                    END;

                --			if Trim(@PrevTermId) = Trim(@TermId)
                --			begin
                --				set @Sum_Product_WeightedAverage_Credits_GPA = @Sum_Product_WeightedAverage_Credits_GPA + (@Product_WeightedAverage_Credits_GPA)
                --				set @Sum
                --			end


                --Check if course has lab work or lab hours
                --			set @boolCourseHasLabWorkOrLabHours = (select distinct Count(GC.Descrip) from arGrdBkWeights GBW,arGrdComponentTypes GC, arGrdBkWgtDetails GD  where 
                --													GBW.InstrGrdBkWgtId = GD.InstrGrdBkWgtId And GC.GrdComponentTypeId = GD.GrdComponentTypeId 
                --													and     GBW.ReqId = @ReqId and GC.SysComponentTypeID in (500,503))

                IF (
                   @sysComponentTypeId = 503
                   OR @sysComponentTypeId = 500
                   ) -- Lab work or Lab Hours
                    BEGIN
                        -- This course has lab work and lab hours
                        IF ( @Completed = 0 )
                            BEGIN
                                SET @CreditsPerService = (
                                                         SELECT TOP 1 GD.CreditsPerService
                                                         FROM   arGrdBkWeights GBW
                                                               ,arGrdComponentTypes GC
                                                               ,arGrdBkWgtDetails GD
                                                         WHERE  GBW.InstrGrdBkWgtId = GD.InstrGrdBkWgtId
                                                                AND GC.GrdComponentTypeId = GD.GrdComponentTypeId
                                                                AND GBW.ReqId = @reqid
                                                                AND GC.SysComponentTypeId IN ( 500, 503 )
                                                         );
                                SET @NumberOfServicesAttempted = (
                                                                 SELECT     TOP 1 GBR.Score AS NumberOfServicesAttempted
                                                                 FROM       arStuEnrollments SE
                                                                 INNER JOIN arGrdBkResults GBR ON SE.StuEnrollId = GBR.StuEnrollId
                                                                                                  AND GBR.ClsSectionId = @ClsSectionId
                                                                 );

                                SET @CreditsEarned = ISNULL(@CreditsPerService, 0) * ISNULL(@NumberOfServicesAttempted, 0);
                            END;
                    END;

                DECLARE @rowAlreadyInserted INT;
                SET @rowAlreadyInserted = 0;

                -- Get the final Gpa only when IsCreditsAttempted is set to 1 and IsInGPA is set to 1
                IF @IsInGPA = 1
                    BEGIN
                        IF ( @IsCreditsAttempted = 0 )
                            BEGIN
                                SET @FinalGPA = NULL;
                            END;
                    END;
                ELSE
                    BEGIN
                        SET @FinalGPA = NULL;
                    END;

                IF @FinalScore IS NOT NULL
                   AND @CurrentScore IS NULL
                    BEGIN
                        SET @CurrentScore = @FinalScore;
                    END;

                -- Rally case DE 738 KeyBoarding Courses
                SET @IsWeighted = (
                                  SELECT COUNT(*) AS WeightsCount
                                  FROM   (
                                         SELECT     T.TermId
                                                   ,T.TermDescrip
                                                   ,'(' + R.Code + ' ) ' + R.Descrip AS CourseCodeDescrip
                                                   ,R.ReqId
                                                   ,RTRIM(GCT.Descrip) AS GradeBookDescription
                                                   ,( CASE WHEN GCT.SysComponentTypeId IN ( 500, 503, 504, 544 ) THEN GBWD.Number
                                                           ELSE (
                                                                SELECT MIN(MinVal)
                                                                FROM   arGradeScaleDetails GSD
                                                                      ,arGradeSystemDetails GSS
                                                                WHERE  GSD.GrdSysDetailId = GSS.GrdSysDetailId
                                                                       AND GSS.IsPass = 1
                                                                )
                                                      END
                                                    ) AS MinimumScore
                                                   ,GBR.Score AS Score
                                                   ,GBWD.Weight AS Weight
                                                   ,RES.Score AS FinalScore
                                                   ,RES.GrdSysDetailId AS FinalGrade
                                                   ,GBWD.Required
                                                   ,GBWD.MustPass
                                                   ,GBWD.GrdPolicyId
                                                   ,( CASE GCT.SysComponentTypeId
                                                           WHEN 544 THEN (
                                                                         SELECT SUM(HoursAttended)
                                                                         FROM   arExternshipAttendance
                                                                         WHERE  StuEnrollId = SE.StuEnrollId
                                                                         )
                                                           ELSE GBR.Score
                                                      END
                                                    ) AS GradeBookResult
                                                   ,GCT.SysComponentTypeId
                                                   ,SE.StuEnrollId
                                                   ,GBR.GrdBkResultId
                                                   ,R.Credits AS CreditsAttempted
                                                   ,CS.ClsSectionId
                                                   ,GSD.Grade
                                                   ,GSD.IsPass
                                                   ,GSD.IsCreditsAttempted
                                                   ,GSD.IsCreditsEarned
                                         FROM       arStuEnrollments SE
                                         INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                                         INNER JOIN arTransferGrades RES ON RES.StuEnrollId = SE.StuEnrollId
                                         --inner join Inserted t4 on RES.TransferId = t4.TransferId
                                         INNER JOIN arClassSections CS ON RES.ReqId = CS.ReqId
                                                                          AND RES.TermId = CS.TermId
                                         INNER JOIN arTerm T ON CS.TermId = T.TermId
                                         INNER JOIN arReqs R ON CS.ReqId = R.ReqId
                                         LEFT JOIN  arGrdBkResults GBR ON CS.ClsSectionId = GBR.ClsSectionId
                                         LEFT JOIN  arGrdBkWgtDetails GBWD ON GBR.InstrGrdBkWgtDetailId = GBWD.InstrGrdBkWgtDetailId
                                         LEFT JOIN  arGrdComponentTypes GCT ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
                                         LEFT JOIN  arGradeSystemDetails GSD ON RES.GrdSysDetailId = GSD.GrdSysDetailId
                                         WHERE      SE.StuEnrollId = @StuEnrollId
                                                    AND T.TermId = @TermId
                                                    AND R.ReqId = @reqid
                                         ) D
                                  WHERE  Weight >= 1
                                  );

                /************************************************* Changes for Build 2816 *********************/
                -- Rally case DE 738 KeyBoarding Courses
                DECLARE @GradesFormat VARCHAR(50);
                SET @GradesFormat = (
                                    SELECT Value
                                    FROM   syConfigAppSetValues
                                    WHERE  SettingId = 47
                                    ); -- 47 refers to grades format
                -- This condition is met only for numeric grade schools
                IF (
                   @IsGradeBookNotSatisified = 0
                   AND @IsWeighted = 0
                   AND @FinalScore IS NULL
                   AND @FinalGradeDesc IS NULL
                   AND LOWER(LTRIM(RTRIM(@GradesFormat))) <> 'letter'
                   )
                    BEGIN
                        SET @Completed = 1;
                        SET @CreditsAttempted = (
                                                SELECT Credits
                                                FROM   arReqs
                                                WHERE  ReqId = @reqid
                                                );
                        SET @FinAidCredits = (
                                             SELECT FinAidCredits
                                             FROM   arReqs
                                             WHERE  ReqId = @reqid
                                             );
                        SET @CreditsAttempted = @CreditsAttempted;
                        SET @CreditsEarned = @CreditsAttempted;
                        SET @FinAidCreditsEarned = @FinAidCredits;
                    END;

                -- DE748 Name: ROSS: Completed field should also check for the Must Pass property of the work unit. 
                IF LOWER(LTRIM(RTRIM(@GradesFormat))) <> 'letter'
                   AND @IsGradeBookNotSatisified >= 1
                    BEGIN
                        SET @Completed = 0;
                        SET @CreditsEarned = 0;
                        SET @FinAidCreditsEarned = 0;
                    END;

                --DE738 Name: ROSS: Progress Report not taking care of courses that are not weighted. 

                -- Print @TermDescrip
                -- Print @CourseCodeDescrip
                -- Print @Completed
                --			-- Print LOWER(LTRIM(RTRIM(@GradesFormat)))
                --			-- Print @FinalScore
                --			-- Print @FinalGradedesc
                --			-- Print '@IsWeighted='
                --			-- Print @IsWeighted

                IF (
                   LOWER(LTRIM(RTRIM(@GradesFormat))) <> 'letter'
                   AND @Completed = 1
                   AND @FinalScore IS NULL
                   AND @FinalGradeDesc IS NULL
                   )
                    BEGIN
                        SET @CreditsAttempted = @CreditsAttempted;
                        SET @CreditsEarned = @CreditsAttempted;
                        SET @FinAidCreditsEarned = @FinAidCredits;
                    END;

                -- In Ross Example : Externship, the student may not have completed the course but once he attempts a work unit
                -- we need to take the credits as attempted
                IF (
                   LOWER(LTRIM(RTRIM(@GradesFormat))) <> 'letter'
                   AND @Completed = 0
                   AND @FinalScore IS NULL
                   AND @FinalGradeDesc IS NULL
                   )
                    BEGIN
                        DECLARE @rowcount4 INT;
                        SET @rowcount4 = (
                                         SELECT COUNT(*)
                                         FROM   arGrdBkResults
                                         WHERE  StuEnrollId = @StuEnrollId
                                                AND ClsSectionId = @ClsSectionId
                                                AND Score IS NOT NULL
                                         );
                        IF @rowcount4 >= 1
                            BEGIN
                                -- Print 'Gets in to if'
                                SET @CreditsAttempted = (
                                                        SELECT Credits
                                                        FROM   arReqs
                                                        WHERE  ReqId = @reqid
                                                        );
                                SET @CreditsEarned = 0;
                                SET @FinAidCreditsEarned = 0;
                            -- Print @CreditsAttempted
                            END;
                        ELSE
                            BEGIN
                                SET @rowcount4 = (
                                                 SELECT COUNT(*)
                                                 FROM   arGrdBkConversionResults
                                                 WHERE  StuEnrollId = @StuEnrollId
                                                        AND ReqId = @reqid
                                                        AND TermId = @TermId
                                                        AND Score IS NOT NULL
                                                 );
                                IF @rowcount4 >= 1
                                    BEGIN
                                        SET @CreditsAttempted = (
                                                                SELECT Credits
                                                                FROM   arReqs
                                                                WHERE  ReqId = @reqid
                                                                );
                                        SET @CreditsEarned = 0;
                                        SET @FinAidCreditsEarned = 0;
                                    END;
                            END;

                        --For Externship Attendance						
                        IF @sysComponentTypeId = 544
                            BEGIN
                                SET @rowcount4 = (
                                                 SELECT COUNT(*)
                                                 FROM   arExternshipAttendance
                                                 WHERE  StuEnrollId = @StuEnrollId
                                                        AND HoursAttended >= 1
                                                 );
                                IF @rowcount4 >= 1
                                    BEGIN
                                        SET @CreditsAttempted = (
                                                                SELECT Credits
                                                                FROM   arReqs
                                                                WHERE  ReqId = @reqid
                                                                );
                                        SET @CreditsEarned = 0;
                                        SET @FinAidCreditsEarned = 0;
                                    END;

                            END;

                    END;
                /************************************************* Changes for Build 2816 *********************/

                -- If the final grade is not null the final grade will over ride current grade 
                IF @FinalGradeDesc IS NOT NULL
                    BEGIN
                        SET @CurrentGrade = @FinalGradeDesc;

                    END;
                -- If Credits was earned set completed to yes because here we are working with a Transfer credits
                IF @IsCreditsEarned = 1
                    BEGIN
                        SET @CreditsEarned = @CourseCredits;
                        SET @Completed = 1;
                    END;

                IF @IsCreditsAttempted = 0
                    BEGIN
                        SET @CreditsAttempted = 0;
                    END;

                DELETE FROM syCreditSummary
                WHERE StuEnrollId = @StuEnrollId
                      AND TermId = @TermId
                      AND ReqId = @reqid
                      AND (
                          ClsSectionId = @ClsSectionId
                          OR ClsSectionId IS NULL
                          );

                INSERT INTO syCreditSummary
                VALUES ( @StuEnrollId, @TermId, @TermDescrip, @reqid, @CourseCodeDescrip, @ClsSectionId, @CreditsEarned, @CreditsAttempted, @CurrentScore
                        ,@CurrentGrade, @FinalScore, @FinalGradeDesc, @Completed, @FinalGPA, @Product_WeightedAverage_Credits_GPA
                        ,@Count_WeightedAverage_Credits, @Product_SimpleAverage_Credits_GPA, @Count_SimpleAverage_Credits, 'sa', GETDATE(), @ComputedSimpleGPA
                        ,@ComputedWeightedGPA, @CourseCredits, NULL, NULL, @FinAidCreditsEarned, NULL, NULL, @TermStartDate );

                DECLARE @wCourseCredits DECIMAL(18, 2)
                       ,@wWeighted_GPA_Credits DECIMAL(18, 2)
                       ,@sCourseCredits DECIMAL(18, 2)
                       ,@sSimple_GPA_Credits DECIMAL(18, 2);
                -- For weighted average
                SET @ComputedWeightedGPA = 0;
                SET @ComputedSimpleGPA = 0;
                SET @wCourseCredits = (
                                      SELECT SUM(coursecredits)
                                      FROM   syCreditSummary
                                      WHERE  StuEnrollId = @StuEnrollId
                                             AND TermId = @TermId
                                             AND FinalGPA IS NOT NULL
                                      );
                SET @wWeighted_GPA_Credits = (
                                             SELECT SUM(coursecredits * FinalGPA)
                                             FROM   syCreditSummary
                                             WHERE  StuEnrollId = @StuEnrollId
                                                    AND TermId = @TermId
                                                    AND FinalGPA IS NOT NULL
                                             );

                IF @wCourseCredits >= 1
                    BEGIN
                        SET @ComputedWeightedGPA = @wWeighted_GPA_Credits / @wCourseCredits;
                    END;



                --For Simple Average
                SET @sCourseCredits = (
                                      SELECT COUNT(*)
                                      FROM   syCreditSummary
                                      WHERE  StuEnrollId = @StuEnrollId
                                             AND TermId = @TermId
                                             AND FinalGPA IS NOT NULL
                                      );
                SET @sSimple_GPA_Credits = (
                                           SELECT SUM(FinalGPA)
                                           FROM   syCreditSummary
                                           WHERE  StuEnrollId = @StuEnrollId
                                                  AND TermId = @TermId
                                                  AND FinalGPA IS NOT NULL
                                           );
                IF @sCourseCredits >= 1
                    BEGIN
                        SET @ComputedSimpleGPA = @sSimple_GPA_Credits / @sCourseCredits;
                    END;


                --CumulativeGPA
                DECLARE @cumCourseCredits DECIMAL(18, 2)
                       ,@cumWeighted_GPA_Credits DECIMAL(18, 2)
                       ,@cumWeightedGPA DECIMAL(18, 2);
                SET @cumWeightedGPA = 0;
                SET @cumCourseCredits = (
                                        SELECT SUM(coursecredits)
                                        FROM   syCreditSummary
                                        WHERE  StuEnrollId = @StuEnrollId
                                               AND FinalGPA IS NOT NULL
                                        );
                SET @cumWeighted_GPA_Credits = (
                                               SELECT SUM(coursecredits * FinalGPA)
                                               FROM   syCreditSummary
                                               WHERE  StuEnrollId = @StuEnrollId
                                                      AND FinalGPA IS NOT NULL
                                               );

                IF @cumCourseCredits >= 1
                    BEGIN
                        SET @cumWeightedGPA = @cumWeighted_GPA_Credits / @cumCourseCredits;
                    END;

                --CumulativeSimpleGPA
                DECLARE @cumSimpleCourseCredits DECIMAL(18, 2)
                       ,@cumSimple_GPA_Credits DECIMAL(18, 2)
                       ,@cumSimpleGPA DECIMAL(18, 2);
                SET @cumSimpleGPA = 0;
                SET @cumSimpleCourseCredits = (
                                              SELECT COUNT(coursecredits)
                                              FROM   syCreditSummary
                                              WHERE  StuEnrollId = @StuEnrollId
                                                     AND FinalGPA IS NOT NULL
                                              );
                SET @cumSimple_GPA_Credits = (
                                             SELECT SUM(FinalGPA)
                                             FROM   syCreditSummary
                                             WHERE  StuEnrollId = @StuEnrollId
                                                    AND FinalGPA IS NOT NULL
                                             );

                IF @cumSimpleCourseCredits >= 1
                    BEGIN
                        SET @cumSimpleGPA = @cumSimple_GPA_Credits / @cumSimpleCourseCredits;
                    END;

                --Average calculation
                DECLARE @termAverageSum DECIMAL(18, 2)
                       ,@CumAverage DECIMAL(18, 2)
                       ,@cumAverageSum DECIMAL(18, 2)
                       ,@cumAveragecount INT;

                -- Term Average
                SET @TermAverageCount = (
                                        SELECT COUNT(*)
                                        FROM   syCreditSummary
                                        WHERE  StuEnrollId = @StuEnrollId
                                               --and Completed=1 
                                               AND TermId = @TermId
                                               AND FinalScore IS NOT NULL
                                        );
                SET @termAverageSum = (
                                      SELECT SUM(FinalScore)
                                      FROM   syCreditSummary
                                      WHERE  StuEnrollId = @StuEnrollId
                                             --and Completed=1 
                                             AND TermId = @TermId
                                             AND FinalScore IS NOT NULL
                                      );
                SET @TermAverage = @termAverageSum / @TermAverageCount;

                -- Cumulative Average
                SET @cumAveragecount = (
                                       SELECT COUNT(*)
                                       FROM   syCreditSummary
                                       WHERE  StuEnrollId = @StuEnrollId
                                              --and Completed=1 
                                              AND FinalScore IS NOT NULL
                                       );
                SET @cumAverageSum = (
                                     SELECT SUM(FinalScore)
                                     FROM   syCreditSummary
                                     WHERE  StuEnrollId = @StuEnrollId
                                            AND
                                         --Completed=1 and 
                                         FinalScore IS NOT NULL
                                     );
                SET @CumAverage = @cumAverageSum / @cumAveragecount;


                UPDATE syCreditSummary
                SET    TermGPA_Simple = @ComputedSimpleGPA
                      ,TermGPA_Weighted = @ComputedWeightedGPA
                      ,Average = @TermAverage
                WHERE  StuEnrollId = @StuEnrollId
                       AND TermId = @TermId;

                --Update Cumulative GPA
                UPDATE syCreditSummary
                SET    CumulativeGPA = @cumWeightedGPA
                      ,CumulativeGPA_Simple = @cumSimpleGPA
                      ,CumAverage = @CumAverage
                WHERE  StuEnrollId = @StuEnrollId;



                SET @PrevStuEnrollId = @StuEnrollId;
                SET @PrevTermId = @TermId;
                SET @PrevReqId = @reqid;

                FETCH NEXT FROM GetCreditsSummary_Cursor
                INTO @StuEnrollId
                    ,@TermId
                    ,@TermDescrip
                    ,@TermStartDate
                    ,@reqid
                    ,@CourseCodeDescrip
                    ,@FinalScore
                    ,@FinalGrade
                    ,@sysComponentTypeId
                    ,@CreditsAttempted
                    ,@ClsSectionId
                    ,@Grade
                    ,@IsPass
                    ,@IsCreditsAttempted
                    ,@IsCreditsEarned
                    ,@PrgVerId
                    ,@IsInGPA
                    ,@FinAidCredits
                    ,@IsTransferred;
            END;
        CLOSE GetCreditsSummary_Cursor;
        DEALLOCATE GetCreditsSummary_Cursor;
    END;
-- =========================================================================================================
-- END  --  Usp_UpdateTransferCredits_SyCreditsSummary 
-- =========================================================================================================


GO
