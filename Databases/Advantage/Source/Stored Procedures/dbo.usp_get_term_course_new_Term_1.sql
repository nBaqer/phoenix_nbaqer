SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_get_term_course_new_Term_1]
    @StuEnrollId VARCHAR(50)
   ,@TermId VARCHAR(50) = NULL
   ,@StartDate VARCHAR(50) = NULL
   ,@StartDateModifier VARCHAR(10) = NULL
AS
    DECLARE @SetGradeBookAt VARCHAR(50);
    DECLARE @StuEnrollCampId UNIQUEIDENTIFIER;

    SET @StuEnrollCampId = (
                             SELECT CampusId
                             FROM   dbo.arStuEnrollments
                             WHERE  StuEnrollId = @StuEnrollId
                           );
--set @SetGradeBookAt = (select Value from syConfigAppSetValues where SettingId=43)
    SET @SetGradeBookAt = (
                            SELECT  dbo.GetAppSettingValue(43,@StuEnrollCampId)
                          );
    CREATE TABLE #tempTermWorkUnitCount
        (
         TermId UNIQUEIDENTIFIER
        ,WorkUnitCount INT
        ,sysComponentTypeId INT
        );


    IF LOWER(@SetGradeBookAt) = 'instructorlevel'
        BEGIN
            INSERT  INTO #tempTermWorkUnitCount
                    SELECT  TermId
                           ,COUNT(GradeBookDescription)
                           ,GradeBookSysComponentTypeId
                    FROM    (
                              SELECT    d.ReqId
                                       ,d.TermId
                                       ,CASE WHEN a.Descrip IS NULL THEN (
                                                                           SELECT   Resource
                                                                           FROM     syResources
                                                                           WHERE    ResourceID = e.SysComponentTypeId
                                                                         )
                                             ELSE a.Descrip
                                        END AS GradeBookDescription
                                       ,( CASE e.SysComponentTypeId
                                            WHEN 500 THEN a.Number
                                            WHEN 503 THEN a.Number
                                            WHEN 544 THEN a.Number
                                            ELSE (
                                                   SELECT   MIN(MinVal)
                                                   FROM     arGradeScaleDetails GSD
                                                           ,arGradeSystemDetails GSS
                                                   WHERE    GSD.GrdSysDetailId = GSS.GrdSysDetailId
                                                            AND GSS.IsPass = 1
                                                            AND GSD.GrdScaleId = d.GrdScaleId
                                                 )
                                          END ) AS MinResult
                                       ,a.Required
                                       ,a.MustPass
                                       ,ISNULL(e.SysComponentTypeId,0) AS GradeBookSysComponentTypeId
                                       ,a.Number
                                       ,(
                                          SELECT    Resource
                                          FROM      syResources
                                          WHERE     ResourceID = e.SysComponentTypeId
                                        ) AS GradeComponentDescription
                                       ,a.InstrGrdBkWgtDetailId
                                       ,c.StuEnrollId
                                       ,0 AS IsExternShip
                                       ,(
                                          SELECT    Score
                                          FROM      arGrdBkResults
                                          WHERE     StuEnrollId = @StuEnrollId
                                                    AND InstrGrdBkWgtDetailId = a.InstrGrdBkWgtDetailId
                                                    AND ClsSectionId = d.ClsSectionId
                                        ) AS GradeBookScore
                                       ,ROW_NUMBER() OVER ( PARTITION BY ST.StuEnrollId,T.TermId,R.ReqId ORDER BY ST.CampusId, T.StartDate, T.EndDate, T.TermId, T.TermDescrip, R.ReqId, R.Descrip, RES.Resource, e.Descrip ) AS rownumber
                              FROM      arGrdBkWgtDetails a
                                       ,arGrdBkWeights b
                                       ,arResults c
                                       ,arClassSections d
                                       ,arGrdComponentTypes e
                                       ,arStuEnrollments ST
                                       ,arTerm T
                                       ,arReqs R
                                       ,syResources RES
                              WHERE     a.InstrGrdBkWgtId = b.InstrGrdBkWgtId
                                        AND c.TestId = d.ClsSectionId
                                        AND b.InstrGrdBkWgtId = d.InstrGrdBkWgtId
                                        AND e.GrdComponentTypeId = a.GrdComponentTypeId
                                        AND c.StuEnrollId = ST.StuEnrollId
                                        AND d.TermId = T.TermId
                                        AND d.ReqId = R.ReqId
                                        AND e.SysComponentTypeId = RES.ResourceID
                                        AND c.StuEnrollId = @StuEnrollId
                              UNION
                              SELECT    R.ReqId
                                       ,T.TermId
                                       ,CASE WHEN GBW.Descrip IS NULL THEN (
                                                                             SELECT Resource
                                                                             FROM   syResources
                                                                             WHERE  ResourceID = GCT.SysComponentTypeId
                                                                           )
                                             ELSE GBW.Descrip
                                        END AS GradeBookDescription
                                       ,( CASE GCT.SysComponentTypeId
                                            WHEN 500 THEN GBWD.Number
                                            WHEN 503 THEN GBWD.Number
                                            WHEN 544 THEN GBWD.Number
                                            ELSE (
                                                   SELECT   MIN(MinVal)
                                                   FROM     arGradeScaleDetails GSD
                                                           ,arGradeSystemDetails GSS
                                                   WHERE    GSD.GrdSysDetailId = GSS.GrdSysDetailId
                                                            AND GSS.IsPass = 1
                                                            AND GSD.GrdScaleId = CSC.GrdScaleId
                                                 )
                                          END ) AS MinResult
                                       ,GBWD.Required
                                       ,GBWD.MustPass
                                       ,ISNULL(GCT.SysComponentTypeId,0) AS GradeBookSysComponentTypeId
                                       ,GBWD.Number
                                       ,(
                                          SELECT    Resource
                                          FROM      syResources
                                          WHERE     ResourceID = GCT.SysComponentTypeId
                                        ) AS GradeComponentDescription
                                       ,GBWD.InstrGrdBkWgtDetailId
                                       ,GBCR.StuEnrollId
                                       ,0 AS IsExternShip
                                       ,(
                                          SELECT    Score
                                          FROM      arGrdBkResults
                                          WHERE     StuEnrollId = @StuEnrollId
                                                    AND InstrGrdBkWgtDetailId = GBWD.InstrGrdBkWgtDetailId
                                                    AND ClsSectionId = CSC.ClsSectionId
                                        ) AS GradeBookScore
                                       ,ROW_NUMBER() OVER ( PARTITION BY SE.StuEnrollId,T.TermId,R.ReqId ORDER BY SE.CampusId, T.StartDate, T.EndDate, T.TermId, T.TermDescrip, R.ReqId, R.Descrip, SYRES.Resource, GCT.Descrip ) AS rownumber
                              FROM      arGrdBkConversionResults GBCR
                              INNER JOIN arStuEnrollments SE ON GBCR.StuEnrollId = SE.StuEnrollId
                              INNER JOIN (
                                           SELECT   StudentId
                                                   ,FirstName
                                                   ,LastName
                                                   ,MiddleName
                                           FROM     arStudent
                                         ) S ON S.StudentId = SE.StudentId
                              INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                              INNER JOIN arTerm T ON GBCR.TermId = T.TermId
                              INNER JOIN arReqs R ON GBCR.ReqId = R.ReqId
                              INNER JOIN arGrdComponentTypes GCT ON GCT.GrdComponentTypeId = GBCR.GrdComponentTypeId
                              INNER JOIN arGrdBkWgtDetails GBWD ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
                                                                   AND GBWD.GrdComponentTypeId = GBCR.GrdComponentTypeId
                              INNER JOIN arGrdBkWeights GBW ON GBWD.InstrGrdBkWgtId = GBW.InstrGrdBkWgtId
                                                               AND GBCR.ReqId = GBW.ReqId
                              INNER JOIN (
                                           SELECT   ReqId
                                                   ,MAX(EffectiveDate) AS EffectiveDate
                                           FROM     arGrdBkWeights
                                           GROUP BY ReqId
                                         ) AS MaxEffectiveDatesByCourse ON GBCR.ReqId = MaxEffectiveDatesByCourse.ReqId
                              INNER JOIN (
                                           SELECT   Resource
                                                   ,ResourceID
                                           FROM     syResources
                                           WHERE    ResourceTypeID = 10
                                         ) SYRES ON SYRES.ResourceID = GCT.SysComponentTypeId
                              INNER JOIN syCampuses C ON SE.CampusId = C.CampusId
                              INNER JOIN arClassSections CSC ON CSC.TermId = T.TermId
                                                                AND CSC.ReqId = R.ReqId
                              WHERE     MaxEffectiveDatesByCourse.EffectiveDate <= T.StartDate
                                        AND SE.StuEnrollId = @StuEnrollId
                            ) dt
                    GROUP BY TermId
                           ,GradeBookSysComponentTypeId; 
        END;
    ELSE
        BEGIN	
            IF @TermId IS NULL
                BEGIN
                    CREATE TABLE #Temp1
                        (
                         Id UNIQUEIDENTIFIER
                        ,StuEnrollId UNIQUEIDENTIFIER
                        ,TermId UNIQUEIDENTIFIER
                        ,GradeBookDescription VARCHAR(50)
                        ,Number INT
                        ,GradeBookSysComponentTypeId INT
                        ,GradeBookScore DECIMAL(18,2)
                        ,MinResult DECIMAL(18,2)
                        ,GradeComponentDescription VARCHAR(50)
                        ,RowNumber INT
                        ,ClsSectionId UNIQUEIDENTIFIER
                        );
                    CREATE TABLE #temp2
                        (
                         ReqId UNIQUEIDENTIFIER
                        ,EffectiveDate DATETIME
                        );
                    DECLARE @Id UNIQUEIDENTIFIER
                       ,@Descrip VARCHAR(50)
                       ,@Number INT
                       ,@GrdComponentTypeId INT
                       ,@Counter INT
                       ,@times INT;
                    DECLARE @MinResult DECIMAL(18,2)
                       ,@GrdComponentDescription VARCHAR(50)
                       ,@rownumber INT;
                    DECLARE @ClsSectionId UNIQUEIDENTIFIER;
                    SET @Counter = 0;
                    DECLARE @TermStartDate1 DATETIME;
                    DECLARE @TermId_Cursor UNIQUEIDENTIFIER;
				-- Select Distinct TermId from syCreditSummary where StuEnrollId=@StuEnrollId
                    DECLARE getTerms_Cursor CURSOR
                    FOR
                        SELECT DISTINCT
                                TermId
                        FROM    syCreditSummary
                        WHERE   StuEnrollId = @StuEnrollId;
                    OPEN getTerms_Cursor;
                    FETCH NEXT FROM getTerms_Cursor
				 INTO @TermId_Cursor;
                    WHILE @@FETCH_STATUS = 0
                        BEGIN
                            PRINT @TermId_Cursor;
                            SET @TermStartDate1 = (
                                                    SELECT  StartDate
                                                    FROM    arTerm
                                                    WHERE   TermId = @TermId_Cursor
                                                  );
					
                            INSERT  INTO #temp2
                                    SELECT  ReqId
                                           ,MAX(EffectiveDate) AS EffectiveDate
                                    FROM    arGrdBkWeights
                                    WHERE   ReqId IN ( SELECT   ReqId
                                                       FROM     syCreditSummary
                                                       WHERE    StuEnrollId = @StuEnrollId
                                                                AND TermId = @TermId_Cursor )
                                            AND EffectiveDate <= @TermStartDate1
                                    GROUP BY ReqId;
						
						--getUsers Cursor Starts Here
                            DECLARE getUsers_Cursor CURSOR
                            FOR
                                SELECT  *
                                       ,ROW_NUMBER() OVER ( PARTITION BY @StuEnrollId,@TermId_Cursor,SysComponentTypeId ORDER BY SysComponentTypeId, Descrip ) AS rownumber
                                FROM    (
                                          SELECT DISTINCT
                                                    ISNULL(GD.InstrGrdBkWgtDetailId,NEWID()) AS ID
                                                   ,GC.Descrip
                                                   ,GD.Number
                                                   ,GC.SysComponentTypeId
                                                   ,( CASE WHEN GC.SysComponentTypeId IN ( 500,503,504,544 ) THEN GD.Number
                                                           ELSE (
                                                                  SELECT    MIN(MinVal)
                                                                  FROM      arGradeScaleDetails GSD
                                                                           ,arGradeSystemDetails GSS
                                                                  WHERE     GSD.GrdSysDetailId = GSS.GrdSysDetailId
                                                                            AND GSS.IsPass = 1
                                                                            AND GSD.GrdScaleId = CS.GrdScaleId
                                                                )
                                                      END ) AS MinResult
                                                   ,S.Resource AS GradeComponentDescription
                                                   ,CS.ClsSectionId
				--,MaxEffectiveDatesByCourse.ReqId 
                                          FROM      arGrdComponentTypes GC
                                                   ,(
                                                      SELECT    *
                                                      FROM      arGrdBkWgtDetails
                                                      WHERE     InstrGrdBkWgtId IN ( SELECT t1.InstrGrdBkWgtId
                                                                                     FROM   arGrdBkWeights t1
                                                                                           ,#temp2 t2
                                                                                     WHERE  t1.ReqId = t2.ReqId
                                                                                            AND t1.EffectiveDate = t2.EffectiveDate )
                                                    ) GD
                                                   ,arGrdBkWeights GW
                                                   ,arReqs R
                                                   ,arClassSections CS
                                                   ,syResources S
                                                   ,arResults RES
                                                   ,arTerm T
                                          WHERE     GC.GrdComponentTypeId = GD.GrdComponentTypeId
                                                    AND GD.InstrGrdBkWgtId = GW.InstrGrdBkWgtId
                                                    AND GW.ReqId = R.ReqId
                                                    AND R.ReqId = CS.ReqId
                                                    AND CS.TermId = @TermId_Cursor
                                                    AND RES.TestId = CS.ClsSectionId
                                                    AND RES.StuEnrollId = @StuEnrollId
                                                    AND GD.Number > 0
                                                    AND GC.SysComponentTypeId = S.ResourceID
                                                    AND CS.TermId = T.TermId
                                        ) dt;
                            OPEN getUsers_Cursor;
                            FETCH NEXT FROM getUsers_Cursor
						INTO @Id,@Descrip,@Number,@GrdComponentTypeId,@MinResult,@GrdComponentDescription,@ClsSectionId,@rownumber;
                            SET @Counter = 0;
                            WHILE @@FETCH_STATUS = 0
                                BEGIN
                                    SET @times = 1;
                                    WHILE @times <= @Number
                                        BEGIN
                                            PRINT @times;
                                            DECLARE @Score DECIMAL(18,2)
                                               ,@GrdCompDescrip VARCHAR(50);
                                            IF @Number > 1
                                                BEGIN
                                                    SET @GrdCompDescrip = @Descrip + CAST(@times AS CHAR);
                                                    SET @Score = (
                                                                   SELECT   Score
                                                                   FROM     arGrdBkResults
                                                                   WHERE    StuEnrollId = @StuEnrollId
                                                                            AND InstrGrdBkWgtDetailId = @Id
                                                                            AND ResNum = @times
                                                                            AND ClsSectionId = @ClsSectionId
                                                                 );
														  
																			
                                                    SET @rownumber = @times;
                                                END;
                                            ELSE
                                                BEGIN
                                                    SET @GrdCompDescrip = @Descrip;
                                                    SET @Score = (
                                                                   SELECT TOP 1
                                                                            Score
                                                                   FROM     arGrdBkResults
                                                                   WHERE    StuEnrollId = @StuEnrollId
                                                                            AND InstrGrdBkWgtDetailId = @Id
                                                                            AND ResNum = ( @times - 1 )
                                                                 ); 
                                                END;
									
                                            PRINT @Score;
                                            INSERT  INTO #Temp1
                                            VALUES  ( @Id,@StuEnrollId,@TermId_Cursor,@GrdCompDescrip,@Number,@GrdComponentTypeId,@Score,@MinResult,
                                                      @GrdComponentDescription,@rownumber,@ClsSectionId );
									
                                            SET @times = @times + 1;
                                        END;
                                    FETCH NEXT FROM getUsers_Cursor
								INTO @Id,@Descrip,@Number,@GrdComponentTypeId,@MinResult,@GrdComponentDescription,@ClsSectionId,@rownumber;
                                END;
                            CLOSE getUsers_Cursor;
                            DEALLOCATE getUsers_Cursor;
					-- Get the next row for getTerms_Cursor
                            FETCH NEXT FROM getTerms_Cursor
					INTO @TermId_Cursor;
                        END;
                    CLOSE getTerms_Cursor;
                    DEALLOCATE getTerms_Cursor;
				--select * from #temp1
						
                    INSERT  INTO #tempTermWorkUnitCount
                            SELECT  TermId
                                   ,COUNT(GradeBookDescription)
                                   ,GradeBookSysComponentTypeId
                            FROM    (
                                      SELECT    TermId
                                               ,GradeBookDescription
                                               ,GradeBookSysComponentTypeId
                                      FROM      #Temp1 --where GradeBookSysComponentTypeId=501
                                      UNION
                                      SELECT    T.TermId
                                               ,GCT.Descrip AS GradeBookDescription
                                               ,GCT.SysComponentTypeId AS GradeBookSysComponentTypeId
                                      FROM      arGrdBkConversionResults GBCR
                                      INNER JOIN arStuEnrollments SE ON GBCR.StuEnrollId = SE.StuEnrollId
                                      INNER JOIN (
                                                   SELECT   StudentId
                                                           ,FirstName
                                                           ,LastName
                                                           ,MiddleName
                                                   FROM     arStudent
                                                 ) S ON S.StudentId = SE.StudentId
                                      INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                                      INNER JOIN arTerm T ON GBCR.TermId = T.TermId
                                      INNER JOIN arReqs R ON GBCR.ReqId = R.ReqId
                                      INNER JOIN arGrdComponentTypes GCT ON GCT.GrdComponentTypeId = GBCR.GrdComponentTypeId
                                      INNER JOIN arGrdBkWgtDetails GBWD ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
                                                                           AND GBWD.GrdComponentTypeId = GBCR.GrdComponentTypeId
                                      INNER JOIN arGrdBkWeights GBW ON GBWD.InstrGrdBkWgtId = GBW.InstrGrdBkWgtId
                                                                       AND GBCR.ReqId = GBW.ReqId
                                      INNER JOIN (
                                                   SELECT   ReqId
                                                           ,MAX(EffectiveDate) AS EffectiveDate
                                                   FROM     arGrdBkWeights
                                                   GROUP BY ReqId
                                                 ) AS MaxEffectiveDatesByCourse ON GBCR.ReqId = MaxEffectiveDatesByCourse.ReqId
                                      INNER JOIN (
                                                   SELECT   Resource
                                                           ,ResourceID
                                                   FROM     syResources
                                                   WHERE    ResourceTypeID = 10
                                                 ) SYRES ON SYRES.ResourceID = GCT.SysComponentTypeId
                                      INNER JOIN syCampuses C ON SE.CampusId = C.CampusId
                                      WHERE     MaxEffectiveDatesByCourse.EffectiveDate <= T.StartDate
                                                AND SE.StuEnrollId = @StuEnrollId
                                                AND T.TermId = @TermId_Cursor
                                    ) dt
                            GROUP BY TermId
                                   ,GradeBookSysComponentTypeId;
								
						--select * from #tempTermWorkUnitCount
						
                    DROP TABLE #temp2;
                    DROP TABLE #Temp1;
	 -- If Statement Closes Here
                END;				
            ELSE
				-- Else Begins here
                BEGIN
                    CREATE TABLE #Temp21
                        (
                         Id UNIQUEIDENTIFIER
                        ,StuEnrollId UNIQUEIDENTIFIER
                        ,TermId UNIQUEIDENTIFIER
                        ,GradeBookDescription VARCHAR(50)
                        ,Number INT
                        ,GradeBookSysComponentTypeId INT
                        ,GradeBookScore DECIMAL(18,2)
                        ,MinResult DECIMAL(18,2)
                        ,GradeComponentDescription VARCHAR(50)
                        ,RowNumber INT
                        ,ClsSectionId UNIQUEIDENTIFIER
                        );
					 --Declare @Id uniqueidentifier,@Descrip varchar(50),@Number int,@GrdComponentTypeId int,@Counter int,@times int
					 --Declare @MinResult decimal(18,2),@GrdComponentDescription varchar(50),@rownumber int
					 ----Declare @TermId uniqueidentifier,@StuEnrollId uniqueidentifier
					 --Declare @ClsSectionId uniqueidentifier
					 --set @TermId='87D587A4-5E22-4685-943C-096758C00447'
					 --set @StuEnrollId = '66F5BB2D-EC46-470D-8A4A-6E1B87B10171'
                    SET @Counter = 0;
					
					--Declare @TermStartDate1 datetime
                    SET @TermStartDate1 = (
                                            SELECT  StartDate
                                            FROM    arTerm
                                            WHERE   TermId = @TermId
                                          );
					--set @InstrGrdBkWgtId = (select Distinct Top 1 InstrGrdBkWgtId from arGrdBkWeights t1,arReqs t2,arClassSections t3,arResults t4
					--						where t1.ReqId=t2.ReqId and t2.ReqId=t3.ReqId and t3.TermId=@TermId and 
					--						t3.ClsSectionId=t4.TestId and t4.StuEnrollId=@StuEnrollId and 
					--						Max(t1.EffectiveDate)<=@TermStartDate1)
                    CREATE TABLE #Temp22
                        (
                         ReqId UNIQUEIDENTIFIER
                        ,EffectiveDate DATETIME
                        );
                    INSERT  INTO #Temp22
                            SELECT  ReqId
                                   ,MAX(EffectiveDate) AS EffectiveDate
                            FROM    arGrdBkWeights
                            WHERE   ReqId IN ( SELECT   ReqId
                                               FROM     syCreditSummary
                                               WHERE    StuEnrollId = @StuEnrollId
                                                        AND TermId = @TermId )
                                    AND EffectiveDate <= @TermStartDate1
                            GROUP BY ReqId;
					
                    DECLARE getUsers_Cursor CURSOR
                    FOR
                        SELECT  *
                               ,ROW_NUMBER() OVER ( PARTITION BY @StuEnrollId,@TermId,SysComponentTypeId ORDER BY SysComponentTypeId, Descrip ) AS rownumber
                        FROM    (
                                  SELECT DISTINCT
                                            ISNULL(GD.InstrGrdBkWgtDetailId,NEWID()) AS ID
                                           ,GC.Descrip
                                           ,GD.Number
                                           ,GC.SysComponentTypeId
                                           ,( CASE WHEN GC.SysComponentTypeId IN ( 500,503,504,544 ) THEN GD.Number
                                                   ELSE (
                                                          SELECT    MIN(MinVal)
                                                          FROM      arGradeScaleDetails GSD
                                                                   ,arGradeSystemDetails GSS
                                                          WHERE     GSD.GrdSysDetailId = GSS.GrdSysDetailId
                                                                    AND GSS.IsPass = 1
                                                                    AND GSD.GrdScaleId = CS.GrdScaleId
                                                        )
                                              END ) AS MinResult
                                           ,S.Resource AS GradeComponentDescription
                                           ,CS.ClsSectionId
							--,MaxEffectiveDatesByCourse.ReqId 
                                  FROM      arGrdComponentTypes GC
                                           ,(
                                              SELECT    *
                                              FROM      arGrdBkWgtDetails
                                              WHERE     InstrGrdBkWgtId IN ( SELECT t1.InstrGrdBkWgtId
                                                                             FROM   arGrdBkWeights t1
                                                                                   ,#Temp22 t2
                                                                             WHERE  t1.ReqId = t2.ReqId
                                                                                    AND t1.EffectiveDate = t2.EffectiveDate )
                                            ) GD
                                           ,arGrdBkWeights GW
                                           ,arReqs R
                                           ,arClassSections CS
                                           ,syResources S
                                           ,arResults RES
                                           ,arTerm T
                                  WHERE     GC.GrdComponentTypeId = GD.GrdComponentTypeId
                                            AND GD.InstrGrdBkWgtId = GW.InstrGrdBkWgtId
                                            AND GW.ReqId = R.ReqId
                                            AND R.ReqId = CS.ReqId
                                            AND CS.TermId = @TermId
                                            AND RES.TestId = CS.ClsSectionId
                                            AND RES.StuEnrollId = @StuEnrollId
                                            AND GD.Number > 0
                                            AND GC.SysComponentTypeId = S.ResourceID
                                            AND CS.TermId = T.TermId
                                ) dt;
                    OPEN getUsers_Cursor;
                    FETCH NEXT FROM getUsers_Cursor
					INTO @Id,@Descrip,@Number,@GrdComponentTypeId,@MinResult,@GrdComponentDescription,@ClsSectionId,@rownumber;
                    SET @Counter = 0;
                    WHILE @@FETCH_STATUS = 0
                        BEGIN
                            PRINT @Number;
                            SET @times = 1;
                            WHILE @times <= @Number
                                BEGIN
                                    PRINT @times;
							--declare @Score decimal(18,2),@GrdCompDescrip varchar(50)
                                    IF @Number > 1
                                        BEGIN
                                            SET @GrdCompDescrip = @Descrip + CAST(@times AS CHAR);
                                            SET @Score = (
                                                           SELECT   Score
                                                           FROM     arGrdBkResults
                                                           WHERE    StuEnrollId = @StuEnrollId
                                                                    AND InstrGrdBkWgtDetailId = @Id
                                                                    AND ResNum = @times
                                                                    AND ClsSectionId = @ClsSectionId
                                                         );
												  
																	
                                            SET @rownumber = @times;
                                        END;
                                    ELSE
                                        BEGIN
                                            SET @GrdCompDescrip = @Descrip;
                                            SET @Score = (
                                                           SELECT TOP 1
                                                                    Score
                                                           FROM     arGrdBkResults
                                                           WHERE    StuEnrollId = @StuEnrollId
                                                                    AND InstrGrdBkWgtDetailId = @Id
                                                                    AND ResNum = ( @times - 1 )
                                                         ); 
                                        END;
							
                                    PRINT @Score;
                                    INSERT  INTO #Temp21
                                    VALUES  ( @Id,@StuEnrollId,@TermId,@GrdCompDescrip,@Number,@GrdComponentTypeId,@Score,@MinResult,@GrdComponentDescription,
                                              @rownumber,@ClsSectionId );
							
                                    SET @times = @times + 1;
                                END;
                            FETCH NEXT FROM getUsers_Cursor
						INTO @Id,@Descrip,@Number,@GrdComponentTypeId,@MinResult,@GrdComponentDescription,@ClsSectionId,@rownumber;
                        END;
                    CLOSE getUsers_Cursor;
                    DEALLOCATE getUsers_Cursor;
					
					
					
                    INSERT  INTO #tempTermWorkUnitCount
                            SELECT  TermId
                                   ,COUNT(GradeBookDescription)
                                   ,GradeBookSysComponentTypeId
                            FROM    (
                                      SELECT    TermId
                                               ,GradeBookDescription
                                               ,GradeBookSysComponentTypeId
                                      FROM      #Temp21 --where GradeBookSysComponentTypeId=501
                                      UNION
                                      SELECT    T.TermId
                                               ,GCT.Descrip AS GradeBookDescription
                                               ,GCT.SysComponentTypeId AS GradeBookSysComponentTypeId
                                      FROM      arGrdBkConversionResults GBCR
                                      INNER JOIN arStuEnrollments SE ON GBCR.StuEnrollId = SE.StuEnrollId
                                      INNER JOIN (
                                                   SELECT   StudentId
                                                           ,FirstName
                                                           ,LastName
                                                           ,MiddleName
                                                   FROM     arStudent
                                                 ) S ON S.StudentId = SE.StudentId
                                      INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                                      INNER JOIN arTerm T ON GBCR.TermId = T.TermId
                                      INNER JOIN arReqs R ON GBCR.ReqId = R.ReqId
                                      INNER JOIN arGrdComponentTypes GCT ON GCT.GrdComponentTypeId = GBCR.GrdComponentTypeId
                                      INNER JOIN arGrdBkWgtDetails GBWD ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
                                                                           AND GBWD.GrdComponentTypeId = GBCR.GrdComponentTypeId
                                      INNER JOIN arGrdBkWeights GBW ON GBWD.InstrGrdBkWgtId = GBW.InstrGrdBkWgtId
                                                                       AND GBCR.ReqId = GBW.ReqId
                                      INNER JOIN (
                                                   SELECT   ReqId
                                                           ,MAX(EffectiveDate) AS EffectiveDate
                                                   FROM     arGrdBkWeights
                                                   GROUP BY ReqId
                                                 ) AS MaxEffectiveDatesByCourse ON GBCR.ReqId = MaxEffectiveDatesByCourse.ReqId
                                      INNER JOIN (
                                                   SELECT   Resource
                                                           ,ResourceID
                                                   FROM     syResources
                                                   WHERE    ResourceTypeID = 10
                                                 ) SYRES ON SYRES.ResourceID = GCT.SysComponentTypeId
                                      INNER JOIN syCampuses C ON SE.CampusId = C.CampusId
                                      WHERE     MaxEffectiveDatesByCourse.EffectiveDate <= T.StartDate
                                                AND SE.StuEnrollId = @StuEnrollId
                                                AND T.TermId = @TermId
                                    ) dt
                            GROUP BY TermId
                                   ,GradeBookSysComponentTypeId;
                    DROP TABLE #Temp22;
                    DROP TABLE #Temp21;
                END;
					-- Else Closes here
        END;

    SELECT  *
    FROM    (
              SELECT    *
                       ,(
                          SELECT    WorkUnitCount
                          FROM      #tempTermWorkUnitCount
                          WHERE     TermId = dt.TermId
                                    AND sysComponentTypeId = 499
                        ) AS WorkUnitCount_499
                       ,(
                          SELECT    WorkUnitCount
                          FROM      #tempTermWorkUnitCount
                          WHERE     TermId = dt.TermId
                                    AND sysComponentTypeId = 500
                        ) AS WorkUnitCount_500
                       ,(
                          SELECT    WorkUnitCount
                          FROM      #tempTermWorkUnitCount
                          WHERE     TermId = dt.TermId
                                    AND sysComponentTypeId = 501
                        ) AS WorkUnitCount_501
                       ,(
                          SELECT    WorkUnitCount
                          FROM      #tempTermWorkUnitCount
                          WHERE     TermId = dt.TermId
                                    AND sysComponentTypeId = 502
                        ) AS WorkUnitCount_502
                       ,(
                          SELECT    WorkUnitCount
                          FROM      #tempTermWorkUnitCount
                          WHERE     TermId = dt.TermId
                                    AND sysComponentTypeId = 503
                        ) AS WorkUnitCount_503
                       ,(
                          SELECT    WorkUnitCount
                          FROM      #tempTermWorkUnitCount
                          WHERE     TermId = dt.TermId
                                    AND sysComponentTypeId = 533
                        ) AS WorkUnitCount_533
                       ,(
                          SELECT    WorkUnitCount
                          FROM      #tempTermWorkUnitCount
                          WHERE     TermId = dt.TermId
                                    AND sysComponentTypeId = 544
                        ) AS WorkUnitCount_544
                       ,(
                          SELECT    SUM(WorkUnitCount)
                          FROM      #tempTermWorkUnitCount
                          WHERE     TermId = dt.TermId
                        ) AS WorkUnitCount
                       ,DENSE_RANK() OVER ( ORDER BY PrgVerDescrip, TermStartDate, TermEndDate, TermDescription ) AS RowNumber1
                       ,DENSE_RANK() OVER ( ORDER BY PrgVerDescrip, TermStartDate, TermEndDate, TermDescription, FinalGrade DESC, FinalScore DESC, CourseCode ) AS CourseNumber
              FROM      (
                          SELECT DISTINCT
                                    3 AS Tag
                                   ,2 AS Parent
                                   ,PV.PrgVerId
                                   ,PV.PrgVerDescrip
                                   ,NULL AS ProgramCredits
                                   ,T.TermId
                                   ,T.TermDescrip AS TermDescription
                                   ,T.StartDate AS TermStartDate
                                   ,T.EndDate AS TermEndDate
                                   ,CS.ReqId AS CourseId
                                   ,R.Code AS CourseCode
                                   ,R.Descrip AS CourseDescription
                                   ,'(' + R.Code + ')' + R.Descrip AS CourseCodeDescription
                                   ,R.Credits AS CourseCredits
                                   ,(
                                      SELECT    SUM(FACreditsEarned)
                                      FROM      syCreditSummary
                                      WHERE     StuEnrollId = SCS.StuEnrollId
                                                AND TermId = SCS.TermId
                                    ) AS CourseFinAidCredits
                                   ,(
                                      SELECT    MIN(MinVal)
                                      FROM      arGradeScaleDetails GCD
                                               ,arGradeSystemDetails GSD
                                      WHERE     GCD.GrdSysDetailId = GSD.GrdSysDetailId
                                                AND GSD.IsPass = 1
                                                AND GCD.GrdScaleId = CS.GrdScaleId
                                    ) AS MinVal
                                   ,RES.Score AS CourseScore
                                   ,NULL AS GradeBook_ResultId
                                   ,NULL AS GradeBookDescription
                                   ,NULL AS GradeBookScore
                                   ,NULL AS GradeBookPostDate
                                   ,NULL AS GradeBookPassingGrade
                                   ,NULL AS GradeBookWeight
                                   ,NULL AS GradeBookRequired
                                   ,NULL AS GradeBookMustPass
                                   ,NULL AS GradeBookSysComponentTypeId
                                   ,NULL AS GradeBookHoursRequired
                                   ,NULL AS GradeBookHoursCompleted
                                   ,SE.StuEnrollId
                                   ,NULL AS MinResult
                                   ,NULL AS GradeComponentDescription -- Student data   
                                   ,SCS.CreditsAttempted AS CreditsAttempted
                                   ,SCS.CreditsEarned AS CreditsEarned
                                   ,SCS.Completed AS Completed
                                   ,SCS.CurrentScore AS CurrentScore
                                   ,SCS.CurrentGrade AS CurrentGrade
                                   ,SCS.FinalScore AS FinalScore
                                   ,SCS.FinalGrade AS FinalGrade
                                   ,( CASE WHEN (
                                                  SELECT    SUM(Count_WeightedAverage_Credits)
                                                  FROM      syCreditSummary
                                                  WHERE     StuEnrollId = SE.StuEnrollId
                                                            AND TermId = T.TermId
                                                ) > 0 THEN (
                                                             SELECT SUM(Product_WeightedAverage_Credits_GPA) / SUM(Count_WeightedAverage_Credits)
                                                             FROM   syCreditSummary
                                                             WHERE  StuEnrollId = SE.StuEnrollId
                                                                    AND TermId = T.TermId
                                                           )
                                           ELSE 0
                                      END ) AS WeightedAverage_GPA
                                   ,( CASE WHEN (
                                                  SELECT    SUM(Count_SimpleAverage_Credits)
                                                  FROM      syCreditSummary
                                                  WHERE     StuEnrollId = SE.StuEnrollId
                                                            AND TermId = T.TermId
                                                ) > 0 THEN (
                                                             SELECT SUM(Product_SimpleAverage_Credits_GPA) / SUM(Count_SimpleAverage_Credits)
                                                             FROM   syCreditSummary
                                                             WHERE  StuEnrollId = SE.StuEnrollId
                                                                    AND TermId = T.TermId
                                                           )
                                           ELSE 0
                                      END ) AS SimpleAverage_GPA
                                   ,NULL AS WeightedAverage_CumGPA
                                   ,NULL AS SimpleAverage_CumGPA
                                   ,C.CampusId
                                   ,C.CampDescrip
                                   ,NULL AS rownumber
                                   ,S.FirstName AS FirstName
                                   ,S.LastName AS LastName
                                   ,S.MiddleName
                                   ,
						-- Newly added fields
                                    SCS.TermGPA_Simple
                                   ,SCS.TermGPA_Weighted
                                   ,NULL AS newcol
                                   ,(
                                      SELECT    SUM(ISNULL(CreditsAttempted,0))
                                      FROM      syCreditSummary
                                      WHERE     TermId = T.TermId
                                                AND StuEnrollId = SE.StuEnrollId
                                    ) AS Term_CreditsAttempted
                                   ,(
                                      SELECT    SUM(ISNULL(CreditsEarned,0))
                                      FROM      syCreditSummary
                                      WHERE     TermId = T.TermId
                                                AND StuEnrollId = SE.StuEnrollId
                                    ) AS Term_CreditsEarned
                                   ,
						--Newly added ends here
                                    (
                                      SELECT TOP 1
                                                Average
                                      FROM      syCreditSummary
                                      WHERE     StuEnrollId = SE.StuEnrollId
                                                AND TermId = T.TermId
                                    ) AS termAverage
                                   ,(
                                      SELECT    COUNT(*) AS GrdBkWgtDetailsCount
                                      FROM      arGrdBkResults
                                      WHERE     StuEnrollId = SE.StuEnrollId
                                                AND ClsSectionId = RES.TestId
                                    ) AS GrdBkWgtDetailsCount
                                   ,CASE WHEN P.ACId = 5 THEN 'True'
                                         ELSE 'False'
                                    END AS ClockHourProgram
                          FROM      arClassSections CS
                          INNER JOIN arResults GBR ON CS.ClsSectionId = GBR.TestId
                          INNER JOIN arStuEnrollments SE ON GBR.StuEnrollId = SE.StuEnrollId
                          INNER JOIN (
                                       SELECT   StudentId
                                               ,FirstName
                                               ,LastName
                                               ,MiddleName
                                       FROM     arStudent
                                     ) S ON S.StudentId = SE.StudentId
                          INNER JOIN syCampuses C ON SE.CampusId = C.CampusId
                          INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                          INNER JOIN arPrograms P ON P.ProgId = PV.ProgId
                          INNER JOIN arTerm T ON CS.TermId = T.TermId
                          INNER JOIN arReqs R ON CS.ReqId = R.ReqId
                          INNER JOIN arResults RES ON RES.StuEnrollId = GBR.StuEnrollId
                                                      AND RES.TestId = CS.ClsSectionId
                          LEFT JOIN syCreditSummary SCS ON SE.StuEnrollId = SCS.StuEnrollId
                                                           AND T.TermId = SCS.TermId
                                                           AND R.ReqId = SCS.ReqId
                          WHERE     SE.StuEnrollId = @StuEnrollId
                                    AND (
                                          @StartDate IS NULL
                                          OR @StartDateModifier IS NULL
                                          OR (
                                               (
                                                 ( @StartDateModifier <> '=' )
                                                 OR ( T.StartDate = @StartDate )
                                               )
                                               AND (
                                                     ( @StartDateModifier <> '>' )
                                                     OR ( T.StartDate > @StartDate )
                                                   )
                                               AND (
                                                     ( @StartDateModifier <> '<' )
                                                     OR ( T.StartDate < @StartDate )
                                                   )
                                               AND (
                                                     ( @StartDateModifier <> '>=' )
                                                     OR ( T.StartDate >= @StartDate )
                                                   )
                                               AND (
                                                     ( @StartDateModifier <> '<=' )
                                                     OR ( T.StartDate <= @StartDate )
                                                   )
                                             )
                                        )
                                    AND (
                                          @TermId IS NULL
                                          OR T.TermId IN ( SELECT   Val
                                                           FROM     MultipleValuesForReportParameters(@TermId,',',1) )
                                        )
                          UNION
                          SELECT DISTINCT
                                    3
                                   ,2
                                   ,PV.PrgVerId
                                   ,PV.PrgVerDescrip
                                   ,NULL
                                   ,T.TermId
                                   ,T.TermDescrip
                                   ,T.StartDate
                                   ,T.EndDate
                                   ,GBCR.ReqId
                                   ,R.Code AS CourseCode
                                   ,R.Descrip AS CourseDescrip
                                   ,'(' + R.Code + ' ) ' + R.Descrip AS CourseCodeDescrip
                                   ,R.Credits
                                   ,(
                                      SELECT    SUM(FACreditsEarned)
                                      FROM      syCreditSummary
                                      WHERE     StuEnrollId = SCS.StuEnrollId
                                                AND TermId = SCS.TermId
                                    ) AS CourseFinAidCredits
                                   ,(
                                      SELECT    MIN(MinVal)
                                      FROM      arGradeScaleDetails GCD
                                               ,arGradeSystemDetails GSD
                                      WHERE     GCD.GrdSysDetailId = GSD.GrdSysDetailId
                                                AND GSD.IsPass = 1
                                    )
                                   ,GBCR.Score
                                   ,NULL
                                   ,NULL
                                   ,NULL
                                   ,NULL
                                   ,NULL
                                   ,NULL
                                   ,NULL
                                   ,NULL
                                   ,NULL
                                   ,NULL
                                   ,NULL
                                   ,SE.StuEnrollId
                                   ,NULL AS MinResult
                                   ,NULL AS GradeComponentDescription -- Student data    
                                   ,SCS.CreditsAttempted AS CreditsAttempted
                                   ,SCS.CreditsEarned AS CreditsEarned
                                   ,SCS.Completed AS Completed
                                   ,SCS.CurrentScore AS CurrentScore
                                   ,SCS.CurrentGrade AS CurrentGrade
                                   ,SCS.FinalScore AS FinalScore
                                   ,SCS.FinalGrade AS FinalGrade
                                   ,( CASE WHEN (
                                                  SELECT    SUM(Count_WeightedAverage_Credits)
                                                  FROM      syCreditSummary
                                                  WHERE     StuEnrollId = SE.StuEnrollId
                                                            AND TermId = T.TermId
                                                ) > 0 THEN (
                                                             SELECT SUM(Product_WeightedAverage_Credits_GPA) / SUM(Count_WeightedAverage_Credits)
                                                             FROM   syCreditSummary
                                                             WHERE  StuEnrollId = SE.StuEnrollId
                                                                    AND TermId = T.TermId
                                                           )
                                           ELSE 0
                                      END ) AS WeightedAverage_GPA
                                   ,( CASE WHEN (
                                                  SELECT    SUM(Count_SimpleAverage_Credits)
                                                  FROM      syCreditSummary
                                                  WHERE     StuEnrollId = SE.StuEnrollId
                                                            AND TermId = T.TermId
                                                ) > 0 THEN (
                                                             SELECT SUM(Product_SimpleAverage_Credits_GPA) / SUM(Count_SimpleAverage_Credits)
                                                             FROM   syCreditSummary
                                                             WHERE  StuEnrollId = SE.StuEnrollId
                                                                    AND TermId = T.TermId
                                                           )
                                           ELSE 0
                                      END ) AS SimpleAverage_GPA
                                   ,NULL AS WeightedAverage_CumGPA
                                   ,NULL AS SimpleAverage_CumGPA
                                   ,C.CampusId
                                   ,C.CampDescrip
                                   ,NULL AS rownumber
                                   ,S.FirstName AS FirstName
                                   ,S.LastName AS LastName
                                   ,S.MiddleName
                                   ,
						-- Newly added fields
                                    SCS.TermGPA_Simple
                                   ,SCS.TermGPA_Weighted
                                   ,NULL AS newcol
                                   ,(
                                      SELECT    SUM(ISNULL(CreditsAttempted,0))
                                      FROM      syCreditSummary
                                      WHERE     TermId = T.TermId
                                                AND StuEnrollId = SE.StuEnrollId
                                    ) AS Term_CreditsAttempted
                                   ,(
                                      SELECT    SUM(ISNULL(CreditsEarned,0))
                                      FROM      syCreditSummary
                                      WHERE     TermId = T.TermId
                                                AND StuEnrollId = SE.StuEnrollId
                                    ) AS Term_CreditsEarned
                                   ,
						--Newly added ends here
                                    (
                                      SELECT TOP 1
                                                Average
                                      FROM      syCreditSummary
                                      WHERE     StuEnrollId = SE.StuEnrollId
                                                AND TermId = T.TermId
                                    ) AS termAverage
                                   ,(
                                      SELECT    COUNT(*) AS GrdBkWgtDetailsCount
                                      FROM      arGrdBkConversionResults
                                      WHERE     StuEnrollId = SE.StuEnrollId
                                                AND TermId = T.TermId
                                                AND ReqId = R.ReqId
                                    ) AS GrdBkWgtDetailsCount
                                   ,CASE WHEN P.ACId = 5 THEN 'True'
                                         ELSE 'False'
                                    END AS ClockHourProgram
                          FROM      arTransferGrades GBCR
                          INNER JOIN arStuEnrollments SE ON GBCR.StuEnrollId = SE.StuEnrollId
                          INNER JOIN (
                                       SELECT   StudentId
                                               ,FirstName
                                               ,LastName
                                               ,MiddleName
                                       FROM     arStudent
                                     ) S ON S.StudentId = SE.StudentId
                          INNER JOIN syCampuses C ON SE.CampusId = C.CampusId
                          INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                          INNER JOIN arPrograms P ON P.ProgId = PV.ProgId
                          INNER JOIN arTerm T ON GBCR.TermId = T.TermId
                          INNER JOIN arReqs R ON GBCR.ReqId = R.ReqId
						--INNER JOIN arResults AR ON GBCR.StuEnrollId = AR.StuEnrollId
                          LEFT JOIN syCreditSummary SCS ON SE.StuEnrollId = SCS.StuEnrollId
                                                           AND T.TermId = SCS.TermId
                                                           AND R.ReqId = SCS.ReqId
                          WHERE     SE.StuEnrollId = @StuEnrollId
                                    AND (
                                          @StartDate IS NULL
                                          OR @StartDateModifier IS NULL
                                          OR (
                                               (
                                                 ( @StartDateModifier <> '=' )
                                                 OR ( T.StartDate = @StartDate )
                                               )
                                               AND (
                                                     ( @StartDateModifier <> '>' )
                                                     OR ( T.StartDate > @StartDate )
                                                   )
                                               AND (
                                                     ( @StartDateModifier <> '<' )
                                                     OR ( T.StartDate < @StartDate )
                                                   )
                                               AND (
                                                     ( @StartDateModifier <> '>=' )
                                                     OR ( T.StartDate >= @StartDate )
                                                   )
                                               AND (
                                                     ( @StartDateModifier <> '<=' )
                                                     OR ( T.StartDate <= @StartDate )
                                                   )
                                             )
                                        )
                                    AND (
                                          @TermId IS NULL
                                          OR T.TermId IN ( SELECT   Val
                                                           FROM     MultipleValuesForReportParameters(@TermId,',',1) )
                                        )
                        ) dt
            ) dt1
    WHERE   RowNumber1 = 1 --AND CourseNumber<=2 --RowNumber1=1 means First Term in the result set
ORDER BY    PrgVerDescrip
           ,TermStartDate
           ,TermEndDate
           ,TermDescription
           ,rownumber
           ,FinalGrade DESC
           ,FinalScore DESC
           ,CourseCode;


GO
