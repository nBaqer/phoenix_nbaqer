SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

--=================================================================================================
-- USP_KlassAppMissingStudentRequirementDuplicateEmail
--=================================================================================================
CREATE PROCEDURE [dbo].[USP_KlassAppMissingStudentRequirementDuplicateEmail]
AS
    BEGIN
        DECLARE @Lead TABLE
            (
                FirstName VARCHAR(50) NOT NULL
               ,LastName VARCHAR(50) NOT NULL
               ,LeadId UNIQUEIDENTIFIER NOT NULL
               ,StudentId UNIQUEIDENTIFIER NOT NULL
               ,StudentNumber VARCHAR(50) NULL
               ,Email VARCHAR(100) NOT NULL
            );

        INSERT INTO @Lead (
                              FirstName
                             ,LastName
                             ,LeadId
                             ,StudentId
                             ,Email
                             ,StudentNumber
                          )
                    SELECT FirstName
                          ,LastName
                          ,l.LeadId
                          ,StudentId
                          ,EMail
                          ,l.StudentNumber
                    FROM   dbo.adLeads l
                    JOIN   dbo.AdLeadEmail em ON em.LeadId = l.LeadId
                    WHERE  StudentId <> '00000000-0000-0000-0000-000000000000';

        SELECT   FirstName
                ,LastName
                ,StudentNumber
                ,Email
        FROM     @Lead
        WHERE    Email IN (
                              SELECT mail
                              FROM   (
                                         SELECT   l2.Email AS mail
                                                 ,COUNT(*) AS Co
                                         FROM     @Lead l1
                                         JOIN     @Lead l2 ON l1.Email = l2.Email
                                                              AND l1.LeadId <> l2.LeadId
                                         GROUP BY l2.Email
                                         HAVING   COUNT(*) > 1
                                     ) AS T
                          )
        ORDER BY Email
                ,FirstName;

    END;
--=================================================================================================
-- END  --  USP_KlassAppMissingStudentRequirementDuplicateEmail
--=================================================================================================
GO
