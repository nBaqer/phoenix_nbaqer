SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[USP_SA_GetStudentTerms_WithTerms]
    (
     @stuEnrollId UNIQUEIDENTIFIER
    ,@CampusId UNIQUEIDENTIFIER
    ,@StartDate DATETIME
    )
AS /*----------------------------------------------------------------------------------------------------
	Author          :	Bruce Shanblatt
    
    Create date		:	01/31/2011
    
	Procedure Name	:	USP_SA_GetStudentTerms_WithTerms

	Objective		:	Get the students terms for payments
	
	Parameters		:	Name			Type	Data Type			Required? 	
						=====			====	=========			=========	
						@stuEnrollId	In		uniqueidentifier	Required
						@CampusId       In      uniqueidentifier    Required
						@StartDate      In      date                Required	                    
	                     
	Output			:	returns student terms for payments			
						
*/-----------------------------------------------------------------------------------------------------
    BEGIN
        SELECT DISTINCT
                T.TermId
               ,T.TermDescrip
               ,T.StartDate
        FROM    arTerm AS T
        INNER JOIN arClassSections AS CS ON T.TermId = CS.TermId
        INNER JOIN arResults AS R ON CS.ClsSectionId = R.TestId
        INNER JOIN arStuEnrollments AS SE ON R.StuEnrollId = SE.StuEnrollId
        WHERE   SE.StuEnrollId = @stuEnrollId
                AND SE.CampusId = @CampusId
        UNION
        SELECT DISTINCT
                T.TermId
               ,T.TermDescrip
               ,T.StartDate
        FROM    arTerm AS T
        INNER JOIN arTransferGrades AS R ON T.TermId = R.TermId
        INNER JOIN arStuEnrollments AS SE ON R.StuEnrollId = SE.StuEnrollId
        WHERE   SE.StuEnrollId = @stuEnrollId
                AND SE.CampusId = @CampusId
        ORDER BY T.StartDate;
	
    END;




GO
