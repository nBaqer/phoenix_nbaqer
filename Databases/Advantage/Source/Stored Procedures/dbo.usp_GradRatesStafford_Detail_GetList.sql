SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =========================================================================================================  
-- usp_GradRatesStafford_Detail_GetList   
-- =========================================================================================================  
CREATE PROCEDURE [dbo].[usp_GradRatesStafford_Detail_GetList]
    @CampusId VARCHAR(50)
   ,@ProgId VARCHAR(4000) = NULL
   ,@CohortYear VARCHAR(10) = NULL
   ,@CohortPossible VARCHAR(20) = NULL
   ,@OrderBy VARCHAR(100)
   ,@StartDate DATETIME
   ,@EndDate DATETIME
AS --SET @CampusId='3F5E839A-589A-4B2A-B258-35A1A8B3B819'    
    --SET @ProgId=NULL    
    --SET @CohortYear='2013'    
    --SET @CohortPossible= 'full year'    
    --SET @OrderBy = 'SSN'    
    --SET @StartDate='09/01/2010'    
    --SET @EndDate='08/31/2011'    

    BEGIN
        DECLARE @AcadInstFirstTimeStartDate DATETIME;
        DECLARE @ReturnValue VARCHAR(100)
               ,@Value VARCHAR(100);
        DECLARE @SSN VARCHAR(10)
               ,@FirstName VARCHAR(100)
               ,@LastName VARCHAR(100)
               ,@StudentNumber VARCHAR(50)
               ,@TransferredOut INT
               ,@Exclusions INT;
        DECLARE @CitizenShip_IPEDSValue INT
               ,@ProgramType_IPEDSValue INT
               ,@Gender_IPEDSValue INT
               ,@FullTimePartTime_IPEDSValue INT
               ,@StudentId UNIQUEIDENTIFIER;

        DECLARE @StatusDate DATETIME;

        SET @StatusDate = '08/31/' + CONVERT(CHAR(4), YEAR(GETDATE()) - 1);

        -- Check if School tracks grades by letter or numeric       
        --SET @Value = (SELECT TOP 1 Value FROM dbo.syConfigAppSetValues WHERE SettingId=47)      
        -- 2/07/2013 - updated the @value     
        SET @Value = (
                     SELECT dbo.GetAppSettingValue(47, @CampusId)
                     );

        IF @ProgId IS NOT NULL
            BEGIN
                SELECT @ReturnValue = COALESCE(@ReturnValue, '') + ProgDescrip + ','
                FROM   arPrograms t1
                WHERE  t1.ProgId IN (
                                    SELECT Val
                                    FROM   MultipleValuesForReportParameters(@ProgId, ',', 1)
                                    );
                SET @ReturnValue = SUBSTRING(@ReturnValue, 1, LEN(@ReturnValue) - 1);
            END;
        ELSE
            BEGIN
                SELECT @ReturnValue = COALESCE(@ReturnValue, '') + ProgDescrip + ','
                FROM   arPrograms t1
                WHERE  t1.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
                       AND t1.CampGrpId IN (
                                           SELECT CampGrpId
                                           FROM   syCmpGrpCmps
                                           WHERE  CampusId = @CampusId
                                           );
                SET @ReturnValue = SUBSTRING(@ReturnValue, 1, LEN(@ReturnValue) - 1);
            END;

        -- Create a temp table to hold the final output of this stored proc      
        CREATE TABLE #GraduationRate
            (
                RowNumber UNIQUEIDENTIFIER
               ,SSN VARCHAR(50)
               ,StudentNumber VARCHAR(50)
               ,StudentName VARCHAR(100)
               ,Gender VARCHAR(50)
               ,Race VARCHAR(50)
               ,RevisedCohort VARCHAR(10)
               ,Exclusions VARCHAR(10)
               ,CopmPrg100Less2Yrs VARCHAR(10)
               ,CopmPrg150Less2Yrs VARCHAR(10)
               ,StillInProg150Percent VARCHAR(10)
               ,TransOut VARCHAR(10)
               ,RevisedCohortCount INT
               ,ExclusionsCount INT
               ,CopmPrg100Less2YrsCount INT
               ,CopmPrg150Less2YrsCount INT
               ,TransOutCount INT
               ,StillInProg150PercentCount INT
               ,GenderSequence INT
               ,RaceSequence INT
               ,StudentId UNIQUEIDENTIFIER
               ,StuEnrollId UNIQUEIDENTIFIER
               ,StartDate DATETIME
            );


        CREATE TABLE #StudentsList
            (
                StudentId UNIQUEIDENTIFIER
               ,StuENrollId UNIQUEIDENTIFIER
               ,SSN VARCHAR(10)
               ,FirstName VARCHAR(100)
               ,LastName VARCHAR(100)
               ,MiddleName VARCHAR(50)
               ,StudentNumber VARCHAR(50)
               ,TransferredOut INT
               ,Exclusions INT
               ,CitizenShip_IPEDSValue INT
               ,ProgramType_IPEDSValue INT
               ,Gender_IPEDSValue INT
               ,FullTimePartTime_IPEDSValue INT
               ,GenderId UNIQUEIDENTIFIER
               ,RaceId UNIQUEIDENTIFIER
               ,CitizenId UNIQUEIDENTIFIER
               ,GenderDescription VARCHAR(50)
               ,RaceDescription VARCHAR(50)
               ,StudentGraduatedStatusCount INT
               ,ClockHourProgramCount INT
            );

        CREATE TABLE #StudentsListPELL
            (
                StudentId UNIQUEIDENTIFIER
               ,StuENrollId UNIQUEIDENTIFIER
               ,SSN VARCHAR(10)
               ,FirstName VARCHAR(100)
               ,LastName VARCHAR(100)
               ,MiddleName VARCHAR(50)
               ,StudentNumber VARCHAR(50)
               ,TransferredOut INT
               ,Exclusions INT
               ,CitizenShip_IPEDSValue INT
               ,ProgramType_IPEDSValue INT
               ,Gender_IPEDSValue INT
               ,FullTimePartTime_IPEDSValue INT
               ,GenderId UNIQUEIDENTIFIER
               ,RaceId UNIQUEIDENTIFIER
               ,CitizenId UNIQUEIDENTIFIER
               ,GenderDescription VARCHAR(50)
               ,RaceDescription VARCHAR(50)
               ,StudentGraduatedStatusCount INT
               ,ClockHourProgramCount INT
            );

        -- Get the list of FullTime, FirstTime, UnderGraduate Students      
        -- Exclude students who are Transferred in to the institution     
        IF LOWER(@CohortPossible) = 'fall'
            BEGIN
                SET @AcadInstFirstTimeStartDate = DATEADD(YEAR, -1, @EndDate);
                INSERT INTO #StudentsListPELL
                            SELECT DISTINCT t1.StudentId
                                  ,t2.StuEnrollId
                                  ,t1.SSN
                                  ,t1.FirstName
                                  ,t1.LastName
                                  ,t1.MiddleName
                                  ,t1.StudentNumber
                                  ,(
                                   SELECT COUNT(*)
                                   FROM   arStuEnrollments SQ1
                                         ,syStatusCodes SQ2
                                         ,dbo.sySysStatus SQ3
                                   WHERE  SQ1.StatusCodeId = SQ2.StatusCodeId
                                          AND SQ2.SysStatusId = SQ3.SysStatusId
                                          AND SQ1.StuEnrollId = t2.StuEnrollId
                                          AND SQ3.SysStatusId = 19
                                          AND --SQ1.TransferDate<=@EndDate AND     
                                       SQ1.StuEnrollId NOT IN (
                                                              SELECT DISTINCT StuEnrollId
                                                              FROM   arTrackTransfer
                                                              )
                                          AND SQ1.DateDetermined <= @StatusDate
                                   ) AS TransferredOut
                                  ,(
                                  -- Check if the student was either dropped and if the drop reason is either      
                                  -- deceased, active duty, foreign aid service, church mission      
                                  CASE WHEN (
                                            SELECT COUNT(*)
                                            FROM   arStuEnrollments SQ1
                                                  ,syStatusCodes SQ2
                                                  ,dbo.sySysStatus SQ3
                                                  ,arDropReasons SQ44
                                            WHERE  SQ1.StatusCodeId = SQ2.StatusCodeId
                                                   AND SQ2.SysStatusId = SQ3.SysStatusId
                                                   AND SQ1.DropReasonId = SQ44.DropReasonId
                                                   AND SQ1.StuEnrollId = t2.StuEnrollId
                                                   AND SQ3.SysStatusId IN ( 12 ) -- Dropped      
                                                   AND SQ1.DateDetermined <= @StatusDate
                                                   AND SQ44.IPEDSValue IN ( 15, 16, 17, 18, 19 )
                                            ) >= 1 THEN 1
                                       ELSE 0
                                  END
                                   ) AS Exclusions
                                  ,t11.IPEDSValue AS CitizenShip_IPEDSValue
                                  ,t9.IPEDSValue AS ProgramType_IPEDSValue
                                  ,t3.IPEDSValue AS Gender_IPEDSValue
                                  ,t10.IPEDSValue AS FullTimePartTime_IPEDSValue
                                  ,t1.Gender
                                  ,CASE WHEN (
                                             t1.Race IS NULL
                                             AND t11.IPEDSValue <> 65
                                             ) THEN (
                                                    SELECT TOP 1 EthCodeId
                                                    FROM   adEthCodes
                                                    WHERE  EthCodeDescrip = 'Race/ethnicity unknown'
                                                    )
                                        ELSE t1.Race
                                   END AS Race
                                  ,t1.Citizen
                                  ,(
                                   SELECT DISTINCT AgencyDescrip
                                   FROM   syRptAgencyFldValues
                                   WHERE  RptAgencyFldValId = t3.IPEDSValue
                                   ) AS GenderDescription
                                  ,CASE WHEN (
                                             t1.Race IS NULL
                                             AND t11.IPEDSValue <> 65
                                             ) THEN 'Race/ethnicity unknown'
                                        ELSE (
                                             SELECT DISTINCT AgencyDescrip
                                             FROM   syRptAgencyFldValues
                                             WHERE  RptAgencyFldValId = t4.IPEDSValue
                                             )
                                   END AS RaceDescription
                                  ,(
                                   SELECT COUNT(*)
                                   FROM   arStuEnrollments SEC
                                         ,syStatusCodes SC
                                   WHERE  SEC.StuEnrollId = t2.StuEnrollId
                                          AND SEC.StatusCodeId = SC.StatusCodeId
                                          AND SC.SysStatusId = 14
                                          AND ExpGradDate <= @StatusDate
                                   ) AS StudentGraduatedStatusCount
                                  ,(
                                   SELECT COUNT(*)
                                   FROM   arStuEnrollments SEC
                                         ,arPrograms P
                                         ,arPrgVersions PV
                                   WHERE  SEC.PrgVerId = PV.PrgVerId
                                          AND P.ProgId = PV.ProgId
                                          AND SEC.StuEnrollId = t2.StuEnrollId
                                          AND P.ACId = 5
                                   ) AS ClockHourProgramCount
                            FROM   adGenders t3
                            LEFT JOIN arStudent t1 ON t3.GenderId = t1.Gender
                            LEFT JOIN adEthCodes t4 ON t4.EthCodeId = t1.Race
                            INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                            INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                            INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                                         AND t6.SysStatusId NOT IN ( 8 ) -- Ignore "No Start" Students      
                            INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                            INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                            INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                            LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                            LEFT JOIN dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                            INNER JOIN adCitizenships t11 ON t1.Citizen = t11.CitizenshipId
                            INNER JOIN faStudentAwards SA ON SA.StuEnrollId = t2.StuEnrollId
                            INNER JOIN saFundSources FS ON FS.FundSourceId = SA.AwardTypeId
                            INNER JOIN saAwardTypes AT ON AT.AwardTypeId = FS.AwardTypeId
                            INNER JOIN syAdvFundSources AFS ON AFS.AdvFundSourceId = FS.AdvFundSourceId
                            WHERE  LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                                   AND (
                                       @ProgId IS NULL
                                       OR t8.ProgId IN (
                                                       SELECT Val
                                                       FROM   MultipleValuesForReportParameters(@ProgId, ',', 1)
                                                       )
                                       )
                                   AND t10.IPEDSValue = 61 -- Full Time     
                                   AND t12.IPEDSValue = 11 -- First Time      
                                   AND t9.IPEDSValue = 58 -- Under Graduate    
                                   AND AFS.AdvFundSourceId = 2 -- DL Sub    
                                   AND AT.AwardTypeId = 1 --Loan    
                                   AND
                                --2/07/2012 - DE9062 - Academic year options should have a cutoff start date For First Time     
                                --t2.StartDate<=@EndDate -- Student Should Have Started Before the Report End Date    
                                (
                                t2.StartDate > @AcadInstFirstTimeStartDate
                                AND t2.StartDate <= @EndDate
                                )
                                   -- Exclude students who are Dropped out/Transferred/Graduated/No Start     
                                   -- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate    
                                   AND t2.StuEnrollId NOT IN (
                                                             SELECT t1.StuEnrollId
                                                             FROM   arStuEnrollments t1
                                                                   ,syStatusCodes t2
                                                             WHERE  t1.StatusCodeId = t2.StatusCodeId
                                                                    AND StartDate <= @EndDate
                                                                    AND -- Student started before the end date range    
                                                                 LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                                                    AND (
                                                                        @ProgId IS NULL
                                                                        OR t8.ProgId IN (
                                                                                        SELECT Val
                                                                                        FROM   MultipleValuesForReportParameters(@ProgId, ',', 1)
                                                                                        )
                                                                        )
                                                                    AND t2.SysStatusId IN ( 12, 19, 8 ) -- Dropped out/Transferred/Graduated/No Start    
                                                                    -- Date Determined or ExpGradDate or LDA falls before 08/31/2010    
                                                                    AND (
                                                                        t1.DateDetermined < @EndDate
                                                                        OR ExpGradDate < @EndDate
                                                                        OR LDA < @EndDate
                                                                        )
                                                             )
                                   -- If Student is enrolled in only one program version and if that program version       
                                   -- happens to be a continuing ed program exclude the student      
                                   AND t2.StudentId NOT IN (
                                                           SELECT DISTINCT StudentId
                                                           FROM   (
                                                                  SELECT   StudentId
                                                                          ,COUNT(*) AS RowCounter
                                                                  FROM     arStuEnrollments
                                                                  WHERE    PrgVerId IN (
                                                                                       SELECT PrgVerId
                                                                                       FROM   arPrgVersions
                                                                                       WHERE  IsContinuingEd = 1
                                                                                       )
                                                                  GROUP BY StudentId
                                                                  HAVING   COUNT(*) = 1
                                                                  ) dtStudent_ContinuingEd
                                                           )
                                   -- Exclude students who were Transferred in to your institution       
                                   -- This was used in FALL Part B report and we can reuse it here      
                                   AND t2.StuEnrollId NOT IN (
                                                             SELECT DISTINCT SQ1.StuEnrollId
                                                             FROM   arStuEnrollments SQ1
                                                                   ,adDegCertSeeking SQ2
                                                             WHERE  SQ1.StuEnrollId = t2.StuEnrollId
                                                                    AND
                                                                 -- To be considered for TransferIn, Student should be a First-Time Student      
                                                                 --SQ1.LeadId IS NOT NULL AND     
                                                                 SQ1.degcertseekingid = SQ2.DegCertSeekingId
                                                                    AND SQ2.IPEDSValue = 11
                                                                    AND (
                                                                        SQ1.TransferHours > 0
                                                                        OR SQ1.StuEnrollId = CASE WHEN @Value = 'letter' THEN
                                                                                                      (
                                                                                                      SELECT TOP 1 StuEnrollId
                                                                                                      FROM   arTransferGrades
                                                                                                      WHERE  StuEnrollId = SQ1.StuEnrollId
                                                                                                             AND GrdSysDetailId IN (
                                                                                                                                   SELECT GrdSysDetailId
                                                                                                                                   FROM   arGradeSystemDetails
                                                                                                                                   WHERE  IsTransferGrade = 1
                                                                                                                                   )
                                                                                                      )
                                                                                                  ELSE (
                                                                                                       SELECT TOP 1 StuEnrollId
                                                                                                       FROM   arTransferGrades
                                                                                                       WHERE  IsTransferred = 1
                                                                                                              AND StuEnrollId = SQ1.StuEnrollId
                                                                                                       )
                                                                                             END
                                                                        )
                                                                    AND SQ1.StartDate < @EndDate
                                                                    AND NOT EXISTS (
                                                                                   SELECT StuEnrollId
                                                                                   FROM   arStuEnrollments
                                                                                   WHERE  StudentId = SQ1.StudentId
                                                                                          AND StartDate < SQ1.StartDate
                                                                                   )
                                                             )
                                   AND t2.StartDate IN (
                                                       SELECT MIN(StartDate)
                                                       FROM   arStuEnrollments t2
                                                       INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                                                       INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                                                       INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                                                       LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                                                       LEFT JOIN dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                                                       WHERE  LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                                                              AND (
                                                                  @ProgId IS NULL
                                                                  OR t8.ProgId IN (
                                                                                  SELECT Val
                                                                                  FROM   MultipleValuesForReportParameters(@ProgId, ',', 1)
                                                                                  )
                                                                  )
                                                              AND t10.IPEDSValue = 61
                                                              AND -- Full Time      
                                                           t12.IPEDSValue = 11
                                                              AND -- First Time      
                                                           t9.IPEDSValue = 58
                                                              AND -- Under Graduate      
                                                           t2.StudentId = t1.StudentId
                                                              AND
                                                           --2/07/2012 - DE9062 - Academic year options should have a cutoff start date For First Time     
                                                           --t2.StartDate<=@EndDate -- Student Should Have Started Before the Report End Date    
                                                           (
                                                           t2.StartDate > @AcadInstFirstTimeStartDate
                                                           AND t2.StartDate <= @EndDate
                                                           )
                                                       )
                                   -- If two enrollments fall on same start date pick any one     
                                   AND t2.StuEnrollId IN (
                                                         SELECT TOP 1 StuEnrollId
                                                         FROM   arStuEnrollments t2
                                                         INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                                                         INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                                                         INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                                                         LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                                                         LEFT JOIN dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                                                         WHERE  LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                                                                AND (
                                                                    @ProgId IS NULL
                                                                    OR t8.ProgId IN (
                                                                                    SELECT Val
                                                                                    FROM   MultipleValuesForReportParameters(@ProgId, ',', 1)
                                                                                    )
                                                                    )
                                                                AND t10.IPEDSValue = 61
                                                                AND -- Full Time      
                                                             t12.IPEDSValue = 11
                                                                AND -- First Time      
                                                             t9.IPEDSValue = 58
                                                                AND -- Under Graduate      
                                                             t2.StudentId = t1.StudentId
                                                                AND
                                                             --2/07/2012 - DE9062 - Academic year options should have a cutoff start date For First Time     
                                                             --t2.StartDate<=@EndDate -- Student Should Have Started Before the Report End Date    
                                                             (
                                                             t2.StartDate > @AcadInstFirstTimeStartDate
                                                             AND t2.StartDate <= @EndDate
                                                             )
                                                                AND t2.StartDate IN (
                                                                                    SELECT MIN(StartDate)
                                                                                    FROM   arStuEnrollments t2
                                                                                    INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                                                                                    INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                                                                                    INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                                                                                    LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                                                                                    LEFT JOIN dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                                                                                    WHERE  LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                                                                                           AND (
                                                                                               @ProgId IS NULL
                                                                                               OR t8.ProgId IN (
                                                                                                               SELECT Val
                                                                                                               FROM   MultipleValuesForReportParameters(
                                                                                                                                                           @ProgId
                                                                                                                                                          ,','
                                                                                                                                                          ,1
                                                                                                                                                       )
                                                                                                               )
                                                                                               )
                                                                                           AND t10.IPEDSValue = 61
                                                                                           AND -- Full Time      
                                                                                        t12.IPEDSValue = 11
                                                                                           AND -- First Time      
                                                                                        t9.IPEDSValue = 58
                                                                                           AND -- Under Graduate      
                                                                                        t2.StudentId = t1.StudentId
                                                                                           AND
                                                                                        --2/07/2012 - DE9062 - Academic year options should have a cutoff start date For First Time     
                                                                                        --t2.StartDate<=@EndDate -- Student Should Have Started Before the Report End Date    
                                                                                        (
                                                                                        t2.StartDate > @AcadInstFirstTimeStartDate
                                                                                        AND t2.StartDate <= @EndDate
                                                                                        )
                                                                                    )
                                                         );
                INSERT INTO #StudentsList
                            SELECT DISTINCT t1.StudentId
                                  ,t2.StuEnrollId
                                  ,t1.SSN
                                  ,t1.FirstName
                                  ,t1.LastName
                                  ,t1.MiddleName
                                  ,t1.StudentNumber
                                  ,(
                                   SELECT COUNT(*)
                                   FROM   arStuEnrollments SQ1
                                         ,syStatusCodes SQ2
                                         ,dbo.sySysStatus SQ3
                                   WHERE  SQ1.StatusCodeId = SQ2.StatusCodeId
                                          AND SQ2.SysStatusId = SQ3.SysStatusId
                                          AND SQ1.StuEnrollId = t2.StuEnrollId
                                          AND SQ3.SysStatusId = 19
                                          AND --SQ1.TransferDate<=@EndDate AND     
                                       SQ1.StuEnrollId NOT IN (
                                                              SELECT DISTINCT StuEnrollId
                                                              FROM   arTrackTransfer
                                                              )
                                          AND SQ1.DateDetermined <= @StatusDate
                                   ) AS TransferredOut
                                  ,(
                                  -- Check if the student was either dropped and if the drop reason is either      
                                  -- deceased, active duty, foreign aid service, church mission      
                                  CASE WHEN (
                                            SELECT COUNT(*)
                                            FROM   arStuEnrollments SQ1
                                                  ,syStatusCodes SQ2
                                                  ,dbo.sySysStatus SQ3
                                                  ,arDropReasons SQ44
                                            WHERE  SQ1.StatusCodeId = SQ2.StatusCodeId
                                                   AND SQ2.SysStatusId = SQ3.SysStatusId
                                                   AND SQ1.DropReasonId = SQ44.DropReasonId
                                                   AND SQ1.StuEnrollId = t2.StuEnrollId
                                                   AND SQ3.SysStatusId IN ( 12 ) -- Dropped      
                                                   AND SQ1.DateDetermined <= @StatusDate
                                                   AND SQ44.IPEDSValue IN ( 15, 16, 17, 18, 19 )
                                            ) >= 1 THEN 1
                                       ELSE 0
                                  END
                                   ) AS Exclusions
                                  ,t11.IPEDSValue AS CitizenShip_IPEDSValue
                                  ,t9.IPEDSValue AS ProgramType_IPEDSValue
                                  ,t3.IPEDSValue AS Gender_IPEDSValue
                                  ,t10.IPEDSValue AS FullTimePartTime_IPEDSValue
                                  ,t1.Gender
                                  ,CASE WHEN (
                                             t1.Race IS NULL
                                             AND t11.IPEDSValue <> 65
                                             ) THEN (
                                                    SELECT TOP 1 EthCodeId
                                                    FROM   adEthCodes
                                                    WHERE  EthCodeDescrip = 'Race/ethnicity unknown'
                                                    )
                                        ELSE t1.Race
                                   END AS Race
                                  ,t1.Citizen
                                  ,(
                                   SELECT DISTINCT AgencyDescrip
                                   FROM   syRptAgencyFldValues
                                   WHERE  RptAgencyFldValId = t3.IPEDSValue
                                   ) AS GenderDescription
                                  ,CASE WHEN (
                                             t1.Race IS NULL
                                             AND t11.IPEDSValue <> 65
                                             ) THEN 'Race/ethnicity unknown'
                                        ELSE (
                                             SELECT DISTINCT AgencyDescrip
                                             FROM   syRptAgencyFldValues
                                             WHERE  RptAgencyFldValId = t4.IPEDSValue
                                             )
                                   END AS RaceDescription
                                  ,(
                                   SELECT COUNT(*)
                                   FROM   arStuEnrollments SEC
                                         ,syStatusCodes SC
                                   WHERE  SEC.StuEnrollId = t2.StuEnrollId
                                          AND SEC.StatusCodeId = SC.StatusCodeId
                                          AND SC.SysStatusId = 14
                                          AND ExpGradDate <= @StatusDate
                                   ) AS StudentGraduatedStatusCount
                                  ,(
                                   SELECT COUNT(*)
                                   FROM   arStuEnrollments SEC
                                         ,arPrograms P
                                         ,arPrgVersions PV
                                   WHERE  SEC.PrgVerId = PV.PrgVerId
                                          AND P.ProgId = PV.ProgId
                                          AND SEC.StuEnrollId = t2.StuEnrollId
                                          AND P.ACId = 5
                                   ) AS ClockHourProgramCount
                            FROM   adGenders t3
                            LEFT JOIN arStudent t1 ON t3.GenderId = t1.Gender
                            LEFT JOIN adEthCodes t4 ON t4.EthCodeId = t1.Race
                            INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                            INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                            INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                                         AND t6.SysStatusId NOT IN ( 8 ) -- Ignore "No Start" Students      
                            INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                            INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                            INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                            LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                            LEFT JOIN dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                            INNER JOIN adCitizenships t11 ON t1.Citizen = t11.CitizenshipId
                            INNER JOIN faStudentAwards SA ON SA.StuEnrollId = t2.StuEnrollId
                            INNER JOIN saFundSources FS ON FS.FundSourceId = SA.AwardTypeId
                            INNER JOIN saAwardTypes AT ON AT.AwardTypeId = FS.AwardTypeId
                            INNER JOIN syAdvFundSources AFS ON AFS.AdvFundSourceId = FS.AdvFundSourceId
                            WHERE  LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                                   AND t2.StuEnrollId NOT IN (
                                                             SELECT DISTINCT StuENrollId
                                                             FROM   #StudentsListPELL
                                                             )
                                   AND (
                                       @ProgId IS NULL
                                       OR t8.ProgId IN (
                                                       SELECT Val
                                                       FROM   MultipleValuesForReportParameters(@ProgId, ',', 1)
                                                       )
                                       )
                                   AND t10.IPEDSValue = 61 -- Full Time     
                                   AND t12.IPEDSValue = 11 -- First Time      
                                   AND t9.IPEDSValue = 58 -- Under Graduate    
                                   AND AFS.AdvFundSourceId = 7 -- DL Sub    
                                   AND AT.AwardTypeId = 2 --Loan    
                                   AND
                                --2/07/2012 - DE9062 - Academic year options should have a cutoff start date For First Time     
                                --t2.StartDate<=@EndDate -- Student Should Have Started Before the Report End Date    
                                (
                                t2.StartDate > @AcadInstFirstTimeStartDate
                                AND t2.StartDate <= @EndDate
                                )
                                   -- Exclude students who are Dropped out/Transferred/Graduated/No Start     
                                   -- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate    
                                   AND t2.StuEnrollId NOT IN (
                                                             SELECT t1.StuEnrollId
                                                             FROM   arStuEnrollments t1
                                                                   ,syStatusCodes t2
                                                             WHERE  t1.StatusCodeId = t2.StatusCodeId
                                                                    AND StartDate <= @EndDate
                                                                    AND -- Student started before the end date range    
                                                                 LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                                                    AND (
                                                                        @ProgId IS NULL
                                                                        OR t8.ProgId IN (
                                                                                        SELECT Val
                                                                                        FROM   MultipleValuesForReportParameters(@ProgId, ',', 1)
                                                                                        )
                                                                        )
                                                                    AND t2.SysStatusId IN ( 12, 19, 8 ) -- Dropped out/Transferred/Graduated/No Start    
                                                                    -- Date Determined or ExpGradDate or LDA falls before 08/31/2010    
                                                                    AND (
                                                                        t1.DateDetermined < @EndDate
                                                                        OR ExpGradDate < @EndDate
                                                                        OR LDA < @EndDate
                                                                        )
                                                             )
                                   -- If Student is enrolled in only one program version and if that program version       
                                   -- happens to be a continuing ed program exclude the student      
                                   AND t2.StudentId NOT IN (
                                                           SELECT DISTINCT StudentId
                                                           FROM   (
                                                                  SELECT   StudentId
                                                                          ,COUNT(*) AS RowCounter
                                                                  FROM     arStuEnrollments
                                                                  WHERE    PrgVerId IN (
                                                                                       SELECT PrgVerId
                                                                                       FROM   arPrgVersions
                                                                                       WHERE  IsContinuingEd = 1
                                                                                       )
                                                                  GROUP BY StudentId
                                                                  HAVING   COUNT(*) = 1
                                                                  ) dtStudent_ContinuingEd
                                                           )
                                   -- Exclude students who were Transferred in to your institution       
                                   -- This was used in FALL Part B report and we can reuse it here      
                                   AND t2.StuEnrollId NOT IN (
                                                             SELECT DISTINCT SQ1.StuEnrollId
                                                             FROM   arStuEnrollments SQ1
                                                                   ,adDegCertSeeking SQ2
                                                             WHERE  SQ1.StuEnrollId = t2.StuEnrollId
                                                                    AND
                                                                 -- To be considered for TransferIn, Student should be a First-Time Student      
                                                                 --SQ1.LeadId IS NOT NULL AND     
                                                                 SQ1.degcertseekingid = SQ2.DegCertSeekingId
                                                                    AND SQ2.IPEDSValue = 11
                                                                    AND (
                                                                        SQ1.TransferHours > 0
                                                                        OR SQ1.StuEnrollId = CASE WHEN @Value = 'letter' THEN
                                                                                                      (
                                                                                                      SELECT TOP 1 StuEnrollId
                                                                                                      FROM   arTransferGrades
                                                                                                      WHERE  StuEnrollId = SQ1.StuEnrollId
                                                                                                             AND GrdSysDetailId IN (
                                                                                                                                   SELECT GrdSysDetailId
                                                                                                                                   FROM   arGradeSystemDetails
                                                                                                                                   WHERE  IsTransferGrade = 1
                                                                                                                                   )
                                                                                                      )
                                                                                                  ELSE (
                                                                                                       SELECT TOP 1 StuEnrollId
                                                                                                       FROM   arTransferGrades
                                                                                                       WHERE  IsTransferred = 1
                                                                                                              AND StuEnrollId = SQ1.StuEnrollId
                                                                                                       )
                                                                                             END
                                                                        )
                                                                    AND SQ1.StartDate < @EndDate
                                                                    AND NOT EXISTS (
                                                                                   SELECT StuEnrollId
                                                                                   FROM   arStuEnrollments
                                                                                   WHERE  StudentId = SQ1.StudentId
                                                                                          AND StartDate < SQ1.StartDate
                                                                                   )
                                                             )
                                   AND t2.StartDate IN (
                                                       SELECT MIN(StartDate)
                                                       FROM   arStuEnrollments t2
                                                       INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                                                       INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                                                       INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                                                       LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                                                       LEFT JOIN dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                                                       WHERE  LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                                                              AND (
                                                                  @ProgId IS NULL
                                                                  OR t8.ProgId IN (
                                                                                  SELECT Val
                                                                                  FROM   MultipleValuesForReportParameters(@ProgId, ',', 1)
                                                                                  )
                                                                  )
                                                              AND t10.IPEDSValue = 61
                                                              AND -- Full Time      
                                                           t12.IPEDSValue = 11
                                                              AND -- First Time      
                                                           t9.IPEDSValue = 58
                                                              AND -- Under Graduate      
                                                           t2.StudentId = t1.StudentId
                                                              AND
                                                           --2/07/2012 - DE9062 - Academic year options should have a cutoff start date For First Time     
                                                           --t2.StartDate<=@EndDate -- Student Should Have Started Before the Report End Date    
                                                           (
                                                           t2.StartDate > @AcadInstFirstTimeStartDate
                                                           AND t2.StartDate <= @EndDate
                                                           )
                                                       )
                                   -- If two enrollments fall on same start date pick any one     
                                   AND t2.StuEnrollId IN (
                                                         SELECT TOP 1 StuEnrollId
                                                         FROM   arStuEnrollments t2
                                                         INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                                                         INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                                                         INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                                                         LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                                                         LEFT JOIN dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                                                         WHERE  LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                                                                AND (
                                                                    @ProgId IS NULL
                                                                    OR t8.ProgId IN (
                                                                                    SELECT Val
                                                                                    FROM   MultipleValuesForReportParameters(@ProgId, ',', 1)
                                                                                    )
                                                                    )
                                                                AND t10.IPEDSValue = 61
                                                                AND -- Full Time      
                                                             t12.IPEDSValue = 11
                                                                AND -- First Time      
                                                             t9.IPEDSValue = 58
                                                                AND -- Under Graduate      
                                                             t2.StudentId = t1.StudentId
                                                                AND
                                                             --2/07/2012 - DE9062 - Academic year options should have a cutoff start date For First Time     
                                                             --t2.StartDate<=@EndDate -- Student Should Have Started Before the Report End Date    
                                                             (
                                                             t2.StartDate > @AcadInstFirstTimeStartDate
                                                             AND t2.StartDate <= @EndDate
                                                             )
                                                                AND t2.StartDate IN (
                                                                                    SELECT MIN(StartDate)
                                                                                    FROM   arStuEnrollments t2
                                                                                    INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                                                                                    INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                                                                                    INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                                                                                    LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                                                                                    LEFT JOIN dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                                                                                    WHERE  LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                                                                                           AND (
                                                                                               @ProgId IS NULL
                                                                                               OR t8.ProgId IN (
                                                                                                               SELECT Val
                                                                                                               FROM   MultipleValuesForReportParameters(
                                                                                                                                                           @ProgId
                                                                                                                                                          ,','
                                                                                                                                                          ,1
                                                                                                                                                       )
                                                                                                               )
                                                                                               )
                                                                                           AND t10.IPEDSValue = 61
                                                                                           AND -- Full Time      
                                                                                        t12.IPEDSValue = 11
                                                                                           AND -- First Time      
                                                                                        t9.IPEDSValue = 58
                                                                                           AND -- Under Graduate      
                                                                                        t2.StudentId = t1.StudentId
                                                                                           AND
                                                                                        --2/07/2012 - DE9062 - Academic year options should have a cutoff start date For First Time     
                                                                                        --t2.StartDate<=@EndDate -- Student Should Have Started Before the Report End Date    
                                                                                        (
                                                                                        t2.StartDate > @AcadInstFirstTimeStartDate
                                                                                        AND t2.StartDate <= @EndDate
                                                                                        )
                                                                                    )
                                                         );
            END;
        IF LOWER(@CohortPossible) = 'full'
            BEGIN
                INSERT INTO #StudentsListPELL
                            SELECT DISTINCT t1.StudentId
                                  ,t2.StuEnrollId
                                  ,t1.SSN
                                  ,t1.FirstName
                                  ,t1.LastName
                                  ,t1.MiddleName
                                  ,t1.StudentNumber
                                  ,(
                                   SELECT COUNT(*)
                                   FROM   arStuEnrollments SQ1
                                         ,syStatusCodes SQ2
                                         ,dbo.sySysStatus SQ3
                                   WHERE  SQ1.StatusCodeId = SQ2.StatusCodeId
                                          AND SQ2.SysStatusId = SQ3.SysStatusId
                                          AND SQ1.StuEnrollId = t2.StuEnrollId
                                          AND SQ3.SysStatusId = 19
                                          AND --SQ1.TransferDate<=@EndDate AND     
                                       SQ1.StuEnrollId NOT IN (
                                                              SELECT DISTINCT StuEnrollId
                                                              FROM   arTrackTransfer
                                                              )
                                          AND SQ1.DateDetermined <= @StatusDate
                                   ) AS TransferredOut
                                  ,(
                                  -- Check if the student was either dropped and if the drop reason is either      
                                  -- deceased, active duty, foreign aid service, church mission      
                                  CASE WHEN (
                                            SELECT COUNT(*)
                                            FROM   arStuEnrollments SQ1
                                                  ,syStatusCodes SQ2
                                                  ,dbo.sySysStatus SQ3
                                                  ,arDropReasons SQ44
                                            WHERE  SQ1.StatusCodeId = SQ2.StatusCodeId
                                                   AND SQ2.SysStatusId = SQ3.SysStatusId
                                                   AND SQ1.DropReasonId = SQ44.DropReasonId
                                                   AND SQ1.StuEnrollId = t2.StuEnrollId
                                                   AND SQ3.SysStatusId IN ( 12 ) -- Dropped      
                                                   AND SQ1.DateDetermined <= @StatusDate
                                                   AND SQ44.IPEDSValue IN ( 15, 16, 17, 18, 19 )
                                            ) >= 1 THEN 1
                                       ELSE 0
                                  END
                                   ) AS Exclusions
                                  ,t11.IPEDSValue AS CitizenShip_IPEDSValue
                                  ,t9.IPEDSValue AS ProgramType_IPEDSValue
                                  ,t3.IPEDSValue AS Gender_IPEDSValue
                                  ,t10.IPEDSValue AS FullTimePartTime_IPEDSValue
                                  ,t1.Gender
                                  ,CASE WHEN (
                                             t1.Race IS NULL
                                             AND t11.IPEDSValue <> 65
                                             ) THEN (
                                                    SELECT TOP 1 EthCodeId
                                                    FROM   adEthCodes
                                                    WHERE  EthCodeDescrip = 'Race/ethnicity unknown'
                                                    )
                                        ELSE t1.Race
                                   END AS Race
                                  ,t1.Citizen
                                  ,(
                                   SELECT DISTINCT AgencyDescrip
                                   FROM   syRptAgencyFldValues
                                   WHERE  RptAgencyFldValId = t3.IPEDSValue
                                   ) AS GenderDescription
                                  ,CASE WHEN (
                                             t1.Race IS NULL
                                             AND t11.IPEDSValue <> 65
                                             ) THEN 'Race/ethnicity unknown'
                                        ELSE (
                                             SELECT DISTINCT AgencyDescrip
                                             FROM   syRptAgencyFldValues
                                             WHERE  RptAgencyFldValId = t4.IPEDSValue
                                             )
                                   END AS RaceDescription
                                  ,(
                                   SELECT COUNT(*)
                                   FROM   arStuEnrollments SEC
                                         ,syStatusCodes SC
                                   WHERE  SEC.StuEnrollId = t2.StuEnrollId
                                          AND SEC.StatusCodeId = SC.StatusCodeId
                                          AND SC.SysStatusId = 14
                                          AND ExpGradDate <= @StatusDate
                                   ) AS StudentGraduatedStatusCount
                                  ,(
                                   SELECT COUNT(*)
                                   FROM   arStuEnrollments SEC
                                         ,arPrograms P
                                         ,arPrgVersions PV
                                   WHERE  SEC.PrgVerId = PV.PrgVerId
                                          AND P.ProgId = PV.ProgId
                                          AND SEC.StuEnrollId = t2.StuEnrollId
                                          AND P.ACId = 5
                                   ) AS ClockHourProgramCount
                            FROM   adGenders t3
                            LEFT JOIN arStudent t1 ON t3.GenderId = t1.Gender
                            LEFT JOIN adEthCodes t4 ON t4.EthCodeId = t1.Race
                            INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                            INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                            INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                                         AND t6.SysStatusId NOT IN ( 8 ) -- Ignore "No Start" Students      
                            INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                            INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                            INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                            LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                            LEFT JOIN dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                            INNER JOIN adCitizenships t11 ON t1.Citizen = t11.CitizenshipId
                            INNER JOIN faStudentAwards SA ON SA.StuEnrollId = t2.StuEnrollId
                            INNER JOIN saFundSources FS ON FS.FundSourceId = SA.AwardTypeId
                            INNER JOIN saAwardTypes AT ON AT.AwardTypeId = FS.AwardTypeId
                            INNER JOIN syAdvFundSources AFS ON AFS.AdvFundSourceId = FS.AdvFundSourceId
                            WHERE  LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                                   AND (
                                       @ProgId IS NULL
                                       OR t8.ProgId IN (
                                                       SELECT Val
                                                       FROM   MultipleValuesForReportParameters(@ProgId, ',', 1)
                                                       )
                                       )
                                   AND t10.IPEDSValue = 61 -- Full Time     
                                   AND t12.IPEDSValue = 11 -- First Time    
                                   AND t9.IPEDSValue = 58 -- Under Graduate      
                                   AND AFS.AdvFundSourceId = 2 -- PELL    
                                   AND AT.AwardTypeId = 1 --Grant    
                                   AND (
                                       t2.StartDate >= @StartDate
                                       AND t2.StartDate <= @EndDate
                                       )
                                   -- Exclude students who are Dropped out/Transferred/Graduated/No Start     
                                   -- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate    
                                   AND t2.StuEnrollId NOT IN (
                                                             SELECT t1.StuEnrollId
                                                             FROM   arStuEnrollments t1
                                                                   ,syStatusCodes t2
                                                             WHERE  t1.StatusCodeId = t2.StatusCodeId
                                                                    AND StartDate <= @EndDate
                                                                    AND -- Student started before the end date range    
                                                                 LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                                                    AND (
                                                                        @ProgId IS NULL
                                                                        OR t8.ProgId IN (
                                                                                        SELECT Val
                                                                                        FROM   MultipleValuesForReportParameters(@ProgId, ',', 1)
                                                                                        )
                                                                        )
                                                                    AND t2.SysStatusId IN ( 12, 19, 8 ) -- Dropped out/Transferred/Graduated/No Start    
                                                                    -- Date Determined or ExpGradDate or LDA falls before 08/31/2010    
                                                                    AND (
                                                                        t1.DateDetermined < @StartDate
                                                                        OR ExpGradDate < @StartDate
                                                                        OR LDA < @StartDate
                                                                        )
                                                             )
                                   -- Student Should Have Started Before the Report End Date      
                                   -- If Student is enrolled in only one program version and     
                                   --if that program version       
                                   -- happens to be a continuing ed program exclude the student      
                                   AND t2.StudentId NOT IN (
                                                           SELECT DISTINCT StudentId
                                                           FROM   (
                                                                  SELECT   StudentId
                                                                          ,COUNT(*) AS RowCounter
                                                                  FROM     arStuEnrollments
                                                                  WHERE    PrgVerId IN (
                                                                                       SELECT PrgVerId
                                                                                       FROM   arPrgVersions
                                                                                       WHERE  IsContinuingEd = 1
                                                                                       )
                                                                  GROUP BY StudentId
                                                                  HAVING   COUNT(*) = 1
                                                                  ) dtStudent_ContinuingEd
                                                           )
                                   -- Exclude students who were Transferred in to your institution       
                                   -- This was used in FALL Part B report and we can reuse it here      
                                   AND t2.StuEnrollId NOT IN (
                                                             SELECT DISTINCT SQ1.StuEnrollId
                                                             FROM   arStuEnrollments SQ1
                                                                   ,adDegCertSeeking SQ2
                                                             WHERE  SQ1.StuEnrollId = t2.StuEnrollId
                                                                    AND
                                                                 -- To be considered for TransferIn, Student should be a First-Time Student      
                                                                 --SQ1.LeadId IS NOT NULL AND     
                                                                 SQ1.degcertseekingid = SQ2.DegCertSeekingId
                                                                    AND SQ2.IPEDSValue = 11
                                                                    AND (
                                                                        SQ1.TransferHours > 0
                                                                        OR SQ1.StuEnrollId = CASE WHEN @Value = 'letter' THEN
                                                                                                      (
                                                                                                      SELECT TOP 1 StuEnrollId
                                                                                                      FROM   arTransferGrades
                                                                                                      WHERE  StuEnrollId = SQ1.StuEnrollId
                                                                                                             AND GrdSysDetailId IN (
                                                                                                                                   SELECT GrdSysDetailId
                                                                                                                                   FROM   arGradeSystemDetails
                                                                                                                                   WHERE  IsTransferGrade = 1
                                                                                                                                   )
                                                                                                      )
                                                                                                  ELSE (
                                                                                                       SELECT TOP 1 StuEnrollId
                                                                                                       FROM   arTransferGrades
                                                                                                       WHERE  IsTransferred = 1
                                                                                                              AND StuEnrollId = SQ1.StuEnrollId
                                                                                                       )
                                                                                             END
                                                                        )
                                                                    AND SQ1.StartDate < @EndDate
                                                                    AND NOT EXISTS (
                                                                                   SELECT StuEnrollId
                                                                                   FROM   arStuEnrollments
                                                                                   WHERE  StudentId = SQ1.StudentId
                                                                                          AND StartDate < SQ1.StartDate
                                                                                   )
                                                             )
                                   AND t2.StartDate IN (
                                                       SELECT MIN(StartDate)
                                                       FROM   arStuEnrollments t2
                                                       INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                                                       INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                                                       INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                                                       LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                                                       LEFT JOIN dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                                                       WHERE  LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                                                              AND (
                                                                  @ProgId IS NULL
                                                                  OR t8.ProgId IN (
                                                                                  SELECT Val
                                                                                  FROM   MultipleValuesForReportParameters(@ProgId, ',', 1)
                                                                                  )
                                                                  )
                                                              AND t10.IPEDSValue = 61
                                                              AND -- Full Time      
                                                           t12.IPEDSValue = 11
                                                              AND -- First Time      
                                                           t9.IPEDSValue = 58
                                                              AND -- Under Graduate      
                                                           t2.StudentId = t1.StudentId
                                                              AND (
                                                                  t2.StartDate >= @StartDate
                                                                  AND t2.StartDate <= @EndDate
                                                                  )
                                                       )
                                   -- If two enrollments fall on same start date pick any one     
                                   AND t2.StuEnrollId IN (
                                                         SELECT TOP 1 StuEnrollId
                                                         FROM   arStuEnrollments t2
                                                         INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                                                         INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                                                         INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                                                         LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                                                         LEFT JOIN dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                                                         WHERE  LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                                                                AND (
                                                                    @ProgId IS NULL
                                                                    OR t8.ProgId IN (
                                                                                    SELECT Val
                                                                                    FROM   MultipleValuesForReportParameters(@ProgId, ',', 1)
                                                                                    )
                                                                    )
                                                                AND t10.IPEDSValue = 61
                                                                AND -- Full Time      
                                                             t12.IPEDSValue = 11
                                                                AND -- First Time      
                                                             t9.IPEDSValue = 58
                                                                AND -- Under Graduate      
                                                             t2.StudentId = t1.StudentId
                                                                AND (
                                                                    t2.StartDate >= @StartDate
                                                                    AND t2.StartDate <= @EndDate
                                                                    )
                                                                AND t2.StartDate IN (
                                                                                    SELECT MIN(StartDate)
                                                                                    FROM   arStuEnrollments t2
                                                                                    INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                                                                                    INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                                                                                    INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                                                                                    LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                                                                                    LEFT JOIN dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                                                                                    WHERE  LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                                                                                           AND (
                                                                                               @ProgId IS NULL
                                                                                               OR t8.ProgId IN (
                                                                                                               SELECT Val
                                                                                                               FROM   MultipleValuesForReportParameters(
                                                                                                                                                           @ProgId
                                                                                                                                                          ,','
                                                                                                                                                          ,1
                                                                                                                                                       )
                                                                                                               )
                                                                                               )
                                                                                           AND t10.IPEDSValue = 61
                                                                                           AND -- Full Time      
                                                                                        t12.IPEDSValue = 11
                                                                                           AND -- First Time      
                                                                                        t9.IPEDSValue = 58
                                                                                           AND -- Under Graduate      
                                                                                        t2.StudentId = t1.StudentId
                                                                                           AND (
                                                                                               t2.StartDate >= @StartDate
                                                                                               AND t2.StartDate <= @EndDate
                                                                                               )
                                                                                    )
                                                         );
                INSERT INTO #StudentsList
                            SELECT DISTINCT t1.StudentId
                                  ,t2.StuEnrollId
                                  ,t1.SSN
                                  ,t1.FirstName
                                  ,t1.LastName
                                  ,t1.MiddleName
                                  ,t1.StudentNumber
                                  ,(
                                   SELECT COUNT(*)
                                   FROM   arStuEnrollments SQ1
                                         ,syStatusCodes SQ2
                                         ,dbo.sySysStatus SQ3
                                   WHERE  SQ1.StatusCodeId = SQ2.StatusCodeId
                                          AND SQ2.SysStatusId = SQ3.SysStatusId
                                          AND SQ1.StuEnrollId = t2.StuEnrollId
                                          AND SQ3.SysStatusId = 19
                                          AND --SQ1.TransferDate<=@EndDate AND     
                                       SQ1.StuEnrollId NOT IN (
                                                              SELECT DISTINCT StuEnrollId
                                                              FROM   arTrackTransfer
                                                              )
                                          AND SQ1.DateDetermined <= @StatusDate
                                   ) AS TransferredOut
                                  ,(
                                  -- Check if the student was either dropped and if the drop reason is either      
                                  -- deceased, active duty, foreign aid service, church mission      
                                  CASE WHEN (
                                            SELECT COUNT(*)
                                            FROM   arStuEnrollments SQ1
                                                  ,syStatusCodes SQ2
                                                  ,dbo.sySysStatus SQ3
                                                  ,arDropReasons SQ44
                                            WHERE  SQ1.StatusCodeId = SQ2.StatusCodeId
                                                   AND SQ2.SysStatusId = SQ3.SysStatusId
                                                   AND SQ1.DropReasonId = SQ44.DropReasonId
                                                   AND SQ1.StuEnrollId = t2.StuEnrollId
                                                   AND SQ3.SysStatusId IN ( 12 ) -- Dropped      
                                                   AND SQ1.DateDetermined <= @StatusDate
                                                   AND SQ44.IPEDSValue IN ( 15, 16, 17, 18, 19 )
                                            ) >= 1 THEN 1
                                       ELSE 0
                                  END
                                   ) AS Exclusions
                                  ,t11.IPEDSValue AS CitizenShip_IPEDSValue
                                  ,t9.IPEDSValue AS ProgramType_IPEDSValue
                                  ,t3.IPEDSValue AS Gender_IPEDSValue
                                  ,t10.IPEDSValue AS FullTimePartTime_IPEDSValue
                                  ,t1.Gender
                                  ,CASE WHEN (
                                             t1.Race IS NULL
                                             AND t11.IPEDSValue <> 65
                                             ) THEN (
                                                    SELECT TOP 1 EthCodeId
                                                    FROM   adEthCodes
                                                    WHERE  EthCodeDescrip = 'Race/ethnicity unknown'
                                                    )
                                        ELSE t1.Race
                                   END AS Race
                                  ,t1.Citizen
                                  ,(
                                   SELECT DISTINCT AgencyDescrip
                                   FROM   syRptAgencyFldValues
                                   WHERE  RptAgencyFldValId = t3.IPEDSValue
                                   ) AS GenderDescription
                                  ,CASE WHEN (
                                             t1.Race IS NULL
                                             AND t11.IPEDSValue <> 65
                                             ) THEN 'Race/ethnicity unknown'
                                        ELSE (
                                             SELECT DISTINCT AgencyDescrip
                                             FROM   syRptAgencyFldValues
                                             WHERE  RptAgencyFldValId = t4.IPEDSValue
                                             )
                                   END AS RaceDescription
                                  ,(
                                   SELECT COUNT(*)
                                   FROM   arStuEnrollments SEC
                                         ,syStatusCodes SC
                                   WHERE  SEC.StuEnrollId = t2.StuEnrollId
                                          AND SEC.StatusCodeId = SC.StatusCodeId
                                          AND SC.SysStatusId = 14
                                          AND ExpGradDate <= @StatusDate
                                   ) AS StudentGraduatedStatusCount
                                  ,(
                                   SELECT COUNT(*)
                                   FROM   arStuEnrollments SEC
                                         ,arPrograms P
                                         ,arPrgVersions PV
                                   WHERE  SEC.PrgVerId = PV.PrgVerId
                                          AND P.ProgId = PV.ProgId
                                          AND SEC.StuEnrollId = t2.StuEnrollId
                                          AND P.ACId = 5
                                   ) AS ClockHourProgramCount
                            FROM   adGenders t3
                            LEFT JOIN arStudent t1 ON t3.GenderId = t1.Gender
                            LEFT JOIN adEthCodes t4 ON t4.EthCodeId = t1.Race
                            INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                            INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                            INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                                         AND t6.SysStatusId NOT IN ( 8 ) -- Ignore "No Start" Students      
                            INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                            INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                            INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                            LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                            LEFT JOIN dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                            INNER JOIN adCitizenships t11 ON t1.Citizen = t11.CitizenshipId
                            INNER JOIN faStudentAwards SA ON SA.StuEnrollId = t2.StuEnrollId
                            INNER JOIN saFundSources FS ON FS.FundSourceId = SA.AwardTypeId
                            INNER JOIN saAwardTypes AT ON AT.AwardTypeId = FS.AwardTypeId
                            INNER JOIN syAdvFundSources AFS ON AFS.AdvFundSourceId = FS.AdvFundSourceId
                            WHERE  LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                                   AND t2.StuEnrollId NOT IN (
                                                             SELECT DISTINCT StuENrollId
                                                             FROM   #StudentsListPELL
                                                             )
                                   AND (
                                       @ProgId IS NULL
                                       OR t8.ProgId IN (
                                                       SELECT Val
                                                       FROM   MultipleValuesForReportParameters(@ProgId, ',', 1)
                                                       )
                                       )
                                   AND t10.IPEDSValue = 61 -- Full Time     
                                   AND t12.IPEDSValue = 11 -- First Time    
                                   AND t9.IPEDSValue = 58 -- Under Graduate      
                                   AND AFS.AdvFundSourceId = 7 -- DL SUB    
                                   AND AT.AwardTypeId = 2 --LOAN    
                                   AND (
                                       t2.StartDate >= @StartDate
                                       AND t2.StartDate <= @EndDate
                                       )
                                   -- Exclude students who are Dropped out/Transferred/Graduated/No Start     
                                   -- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate    
                                   AND t2.StuEnrollId NOT IN (
                                                             SELECT t1.StuEnrollId
                                                             FROM   arStuEnrollments t1
                                                                   ,syStatusCodes t2
                                                             WHERE  t1.StatusCodeId = t2.StatusCodeId
                                                                    AND StartDate <= @EndDate
                                                                    AND -- Student started before the end date range    
                                                                 LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                                                    AND (
                                                                        @ProgId IS NULL
                                                                        OR t8.ProgId IN (
                                                                                        SELECT Val
                                                                                        FROM   MultipleValuesForReportParameters(@ProgId, ',', 1)
                                                                                        )
                                                                        )
                                                                    AND t2.SysStatusId IN ( 12, 19, 8 ) -- Dropped out/Transferred/Graduated/No Start    
                                                                    -- Date Determined or ExpGradDate or LDA falls before 08/31/2010    
                                                                    AND (
                                                                        t1.DateDetermined < @StartDate
                                                                        OR ExpGradDate < @StartDate
                                                                        OR LDA < @StartDate
                                                                        )
                                                             )
                                   -- Student Should Have Started Before the Report End Date      
                                   -- If Student is enrolled in only one program version and     
                                   --if that program version       
                                   -- happens to be a continuing ed program exclude the student      
                                   AND t2.StudentId NOT IN (
                                                           SELECT DISTINCT StudentId
                                                           FROM   (
                                                                  SELECT   StudentId
                                                                          ,COUNT(*) AS RowCounter
                                                                  FROM     arStuEnrollments
                                                                  WHERE    PrgVerId IN (
                                                                                       SELECT PrgVerId
                                                                                       FROM   arPrgVersions
                                                                                       WHERE  IsContinuingEd = 1
                                                                                       )
                                                                  GROUP BY StudentId
                                                                  HAVING   COUNT(*) = 1
                                                                  ) dtStudent_ContinuingEd
                                                           )
                                   -- Exclude students who were Transferred in to your institution       
                                   -- This was used in FALL Part B report and we can reuse it here      
                                   AND t2.StuEnrollId NOT IN (
                                                             SELECT DISTINCT SQ1.StuEnrollId
                                                             FROM   arStuEnrollments SQ1
                                                                   ,adDegCertSeeking SQ2
                                                             WHERE  SQ1.StuEnrollId = t2.StuEnrollId
                                                                    AND
                                                                 -- To be considered for TransferIn, Student should be a First-Time Student      
                                                                 --SQ1.LeadId IS NOT NULL AND     
                                                                 SQ1.degcertseekingid = SQ2.DegCertSeekingId
                                                                    AND SQ2.IPEDSValue = 11
                                                                    AND (
                                                                        SQ1.TransferHours > 0
                                                                        OR SQ1.StuEnrollId = CASE WHEN @Value = 'letter' THEN
                                                                                                      (
                                                                                                      SELECT TOP 1 StuEnrollId
                                                                                                      FROM   arTransferGrades
                                                                                                      WHERE  StuEnrollId = SQ1.StuEnrollId
                                                                                                             AND GrdSysDetailId IN (
                                                                                                                                   SELECT GrdSysDetailId
                                                                                                                                   FROM   arGradeSystemDetails
                                                                                                                                   WHERE  IsTransferGrade = 1
                                                                                                                                   )
                                                                                                      )
                                                                                                  ELSE (
                                                                                                       SELECT TOP 1 StuEnrollId
                                                                                                       FROM   arTransferGrades
                                                                                                       WHERE  IsTransferred = 1
                                                                                                              AND StuEnrollId = SQ1.StuEnrollId
                                                                                                       )
                                                                                             END
                                                                        )
                                                                    AND SQ1.StartDate < @EndDate
                                                                    AND NOT EXISTS (
                                                                                   SELECT StuEnrollId
                                                                                   FROM   arStuEnrollments
                                                                                   WHERE  StudentId = SQ1.StudentId
                                                                                          AND StartDate < SQ1.StartDate
                                                                                   )
                                                             )
                                   AND t2.StartDate IN (
                                                       SELECT MIN(StartDate)
                                                       FROM   arStuEnrollments t2
                                                       INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                                                       INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                                                       INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                                                       LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                                                       LEFT JOIN dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                                                       WHERE  LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                                                              AND (
                                                                  @ProgId IS NULL
                                                                  OR t8.ProgId IN (
                                                                                  SELECT Val
                                                                                  FROM   MultipleValuesForReportParameters(@ProgId, ',', 1)
                                                                                  )
                                                                  )
                                                              AND t10.IPEDSValue = 61
                                                              AND -- Full Time      
                                                           t12.IPEDSValue = 11
                                                              AND -- First Time      
                                                           t9.IPEDSValue = 58
                                                              AND -- Under Graduate      
                                                           t2.StudentId = t1.StudentId
                                                              AND (
                                                                  t2.StartDate >= @StartDate
                                                                  AND t2.StartDate <= @EndDate
                                                                  )
                                                       )
                                   -- If two enrollments fall on same start date pick any one     
                                   AND t2.StuEnrollId IN (
                                                         SELECT TOP 1 StuEnrollId
                                                         FROM   arStuEnrollments t2
                                                         INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                                                         INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                                                         INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                                                         LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                                                         LEFT JOIN dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                                                         WHERE  LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                                                                AND (
                                                                    @ProgId IS NULL
                                                                    OR t8.ProgId IN (
                                                                                    SELECT Val
                                                                                    FROM   MultipleValuesForReportParameters(@ProgId, ',', 1)
                                                                                    )
                                                                    )
                                                                AND t10.IPEDSValue = 61
                                                                AND -- Full Time      
                                                             t12.IPEDSValue = 11
                                                                AND -- First Time      
                                                             t9.IPEDSValue = 58
                                                                AND -- Under Graduate      
                                                             t2.StudentId = t1.StudentId
                                                                AND (
                                                                    t2.StartDate >= @StartDate
                                                                    AND t2.StartDate <= @EndDate
                                                                    )
                                                                AND t2.StartDate IN (
                                                                                    SELECT MIN(StartDate)
                                                                                    FROM   arStuEnrollments t2
                                                                                    INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                                                                                    INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                                                                                    INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                                                                                    LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                                                                                    LEFT JOIN dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                                                                                    WHERE  LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                                                                                           AND (
                                                                                               @ProgId IS NULL
                                                                                               OR t8.ProgId IN (
                                                                                                               SELECT Val
                                                                                                               FROM   MultipleValuesForReportParameters(
                                                                                                                                                           @ProgId
                                                                                                                                                          ,','
                                                                                                                                                          ,1
                                                                                                                                                       )
                                                                                                               )
                                                                                               )
                                                                                           AND t10.IPEDSValue = 61
                                                                                           AND -- Full Time      
                                                                                        t12.IPEDSValue = 11
                                                                                           AND -- First Time      
                                                                                        t9.IPEDSValue = 58
                                                                                           AND -- Under Graduate      
                                                                                        t2.StudentId = t1.StudentId
                                                                                           AND (
                                                                                               t2.StartDate >= @StartDate
                                                                                               AND t2.StartDate <= @EndDate
                                                                                               )
                                                                                    )
                                                         );
            END;




        --SELECT * FROM #StudentsList  --WHERE lastname='Diognardi'    

        SELECT   NEWID() AS RowNumber
                ,dbo.UDF_FormatSSN(SSN) AS SSN
                ,StudentNumber
                ,LastName + ', ' + FirstName + ' ' + ISNULL(MiddleName, '') AS StudentName
                ,1 AS RevisedCohort
                ,Exclusions
                ,CASE WHEN Exclusions >= 1 THEN 1
                      ELSE 0
                 END AS ExclusionsCount
                ,dbo.CompletedProgramin150PercentofProgramLength(StuENrollId, 'D') AS CompletedProgramin150PercentofProgramLength
                ,dbo.CompletedProgramin150PercentofProgramLength(StuENrollId, 'O') AS CompletedProgramin150PercentofProgramLengthForOthers
                ,dbo.CompletedProgramin150PercentofProgramLength(StuENrollId, 'A') AS CompletedProgramin150PercentofProgramLengthForAllDegree
                ,dbo.CompletedProgramin150PercentofProgramLength(StuENrollId, 'B') AS CompletedProgramin150PercentofProgramLengthForBachelor
        FROM     #StudentsList dt
        ORDER BY CASE WHEN @OrderBy = 'SSN' THEN dt.SSN
                 END
                ,CASE WHEN @OrderBy = 'LastName' THEN dt.LastName
                 END
                ,CASE WHEN @OrderBy = 'StudentNumber' THEN CONVERT(INT, dt.StudentNumber)
                 END;




        DROP TABLE #GraduationRate;
        DROP TABLE #StudentsList;
    END;
-- =========================================================================================================  
-- usp_GradRatesStafford_Detail_GetList     
-- =========================================================================================================  
GO
