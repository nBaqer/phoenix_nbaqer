SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jose Torres
-- Create date: 01/16/2015
-- Description: UPDATE TermEnrollSummary table for the given existing record
--              if the record do not exist it create the record and return empty string.
--
-- EXECUTE USP_UpdateTermEnrollSummary   @TermId, @StuEnrollId, @DescripXTranscript, @ModUser, @ModDate 
--   
-- =============================================

CREATE PROCEDURE [dbo].[USP_UpdateTermEnrollSummary]
    @TermId UNIQUEIDENTIFIER
   ,@StuEnrollId UNIQUEIDENTIFIER
   ,@DescripXTranscript NVARCHAR(250)
   ,@ModUser NVARCHAR(250)
   ,@ModDate DATETIME
AS
    DECLARE @Error NVARCHAR(250);            
    SET @Error = '';

    BEGIN
        BEGIN TRY
            IF EXISTS ( SELECT  1
                        FROM    arTermEnrollSummary AS ATES
                        WHERE   ATES.TermId = @TermId
                                AND ATES.StuEnrollId = @StuEnrollId )
                BEGIN 
                    UPDATE  ATES
                    SET     ATES.TermId = @TermId
                           ,ATES.StuEnrollId = @StuEnrollId
                           ,ATES.DescripXTranscript = @DescripXTranscript
                           ,ATES.ModUser = @ModUser
                           ,ATES.ModDate = @ModDate
                    FROM    dbo.arTermEnrollSummary AS ATES
                    WHERE   ATES.TermId = @TermId
                            AND ATES.StuEnrollId = @StuEnrollId;
                END;
            ELSE
                BEGIN
			 -- Create new record
                    INSERT  INTO dbo.arTermEnrollSummary
                            (
                             TESummId
                            ,TermId
                            ,StuEnrollId
                            ,DescripXTranscript
                            ,ModUser
                            ,ModDate
                            )
                    VALUES  (
                             NEWID()              -- TESummId           - UNIQUEIDENTIFIER
                            ,@TermId              -- TermId             - UNIQUEIDENTIFIER
                            ,@StuEnrollId         -- StuEnrollId        - UNIQUEIDENTIFIER
                            ,@DescripXTranscript  -- DescripXTranscript - NVARCHAR(250) 
                            ,@ModUser             -- ModUser            - NVARCHAR(250) 
                            ,@ModDate             -- ModDate            - DATETIME
						    );
                END;
        END TRY
        BEGIN CATCH
            SET @Error = ERROR_MESSAGE();
        END CATCH;
	
        RETURN @Error;
    END;




GO
