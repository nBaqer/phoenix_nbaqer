SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[USP_IL_GetImportLeadsFoldersPaths] @CampusId VARCHAR(50)
AS
    SELECT  ILSourcePath
           ,ILArchivePath
           ,ILExceptionPath
           ,RemoteServerUserNameIL
           ,RemoteServerPasswordIL
    FROM    syCampuses
    WHERE   CampusId = @CampusId;






GO
