SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_get_term_course_Term_2_RemainingCourses]
    @StuEnrollId VARCHAR(50)
   ,@TermId VARCHAR(50) = NULL
   ,@StartDate VARCHAR(50) = NULL
   ,@StartDateModifier VARCHAR(10) = NULL
AS
    DECLARE @GradesFormat VARCHAR(50);
    DECLARE @StuEnrollCampId UNIQUEIDENTIFIER;

    SET @StuEnrollCampId = (
                             SELECT CampusId
                             FROM   dbo.arStuEnrollments
                             WHERE  StuEnrollId = @StuEnrollId
                           );
--SET @GradesFormat = (SELECT VALUE FROM syConfigAppSetValues WHERE SettingId=47)
    SET @GradesFormat = (
                          SELECT    dbo.GetAppSettingValue(47,@StuEnrollCampId)
                        );

    SELECT  *
    FROM    (
              SELECT    *
                       ,CASE WHEN LTRIM(RTRIM(LOWER(@GradesFormat))) = 'letter'
                             THEN DENSE_RANK() OVER ( ORDER BY PrgVerDescrip, TermStartDate, TermEndDate, TermDescription, TermNumber, FinalGrade DESC, CourseCode )
                             ELSE DENSE_RANK() OVER ( ORDER BY PrgVerDescrip, TermStartDate, TermEndDate, TermDescription, TermNumber, FinalScore DESC, CourseCode )
                        END AS CourseNumber
              FROM      (
                          SELECT    *
                                   ,DENSE_RANK() OVER ( ORDER BY PrgVerDescrip, TermStartDate, TermEndDate, TermDescription ) AS TermNumber
                          FROM      (
                                      SELECT DISTINCT
                                                3 AS Tag
                                               ,2 AS Parent
                                               ,PV.PrgVerId
                                               ,PV.PrgVerDescrip
                                               ,NULL AS ProgramCredits
                                               ,T.TermId
                                               ,T.TermDescrip AS TermDescription
                                               ,T.StartDate AS TermStartDate
                                               ,T.EndDate AS TermEndDate
                                               ,CS.ReqId AS CourseId
                                               ,R.Code AS CourseCode
                                               ,R.Descrip AS CourseDescription
                                               ,'(' + R.Code + ')' + R.Descrip AS CourseCodeDescription
                                               ,R.Credits AS CourseCredits
                                               ,(
                                                  SELECT    SUM(FACreditsEarned)
                                                  FROM      syCreditSummary
                                                  WHERE     StuEnrollId = SCS.StuEnrollId
                                                            AND TermId = SCS.TermId
                                                ) AS CourseFinAidCredits
                                               ,(
                                                  SELECT    MIN(MinVal)
                                                  FROM      arGradeScaleDetails GCD
                                                           ,arGradeSystemDetails GSD
                                                  WHERE     GCD.GrdSysDetailId = GSD.GrdSysDetailId
                                                            AND GSD.IsPass = 1
                                                            AND GCD.GrdScaleId = CS.GrdScaleId
                                                ) AS MinVal
                                               ,RES.Score AS CourseScore
                                               ,NULL AS GradeBook_ResultId
                                               ,NULL AS GradeBookDescription
                                               ,NULL AS GradeBookScore
                                               ,NULL AS GradeBookPostDate
                                               ,NULL AS GradeBookPassingGrade
                                               ,NULL AS GradeBookWeight
                                               ,NULL AS GradeBookRequired
                                               ,NULL AS GradeBookMustPass
                                               ,NULL AS GradeBookSysComponentTypeId
                                               ,NULL AS GradeBookHoursRequired
                                               ,NULL AS GradeBookHoursCompleted
                                               ,SE.StuEnrollId
                                               ,NULL AS MinResult
                                               ,NULL AS GradeComponentDescription -- Student data   
                                               ,SCS.CreditsAttempted AS CreditsAttempted
                                               ,SCS.CreditsEarned AS CreditsEarned
                                               ,SCS.Completed AS Completed
                                               ,SCS.CurrentScore AS CurrentScore
                                               ,SCS.CurrentGrade AS CurrentGrade
                                               ,SCS.FinalScore AS FinalScore
                                               ,SCS.FinalGrade AS FinalGrade
                                               ,( CASE WHEN (
                                                              SELECT    SUM(Count_WeightedAverage_Credits)
                                                              FROM      syCreditSummary
                                                              WHERE     StuEnrollId = SE.StuEnrollId
                                                                        AND TermId = T.TermId
                                                            ) > 0 THEN (
                                                                         SELECT SUM(Product_WeightedAverage_Credits_GPA) / SUM(Count_WeightedAverage_Credits)
                                                                         FROM   syCreditSummary
                                                                         WHERE  StuEnrollId = SE.StuEnrollId
                                                                                AND TermId = T.TermId
                                                                       )
                                                       ELSE 0
                                                  END ) AS WeightedAverage_GPA
                                               ,( CASE WHEN (
                                                              SELECT    SUM(Count_SimpleAverage_Credits)
                                                              FROM      syCreditSummary
                                                              WHERE     StuEnrollId = SE.StuEnrollId
                                                                        AND TermId = T.TermId
                                                            ) > 0 THEN (
                                                                         SELECT SUM(Product_SimpleAverage_Credits_GPA) / SUM(Count_SimpleAverage_Credits)
                                                                         FROM   syCreditSummary
                                                                         WHERE  StuEnrollId = SE.StuEnrollId
                                                                                AND TermId = T.TermId
                                                                       )
                                                       ELSE 0
                                                  END ) AS SimpleAverage_GPA
                                               ,NULL AS WeightedAverage_CumGPA
                                               ,NULL AS SimpleAverage_CumGPA
                                               ,C.CampusId
                                               ,C.CampDescrip
                                               ,NULL AS rownumber
                                               ,S.FirstName AS FirstName
                                               ,S.LastName AS LastName
                                               ,S.MiddleName
                                               ,
						-- Newly added fields
                                                SCS.TermGPA_Simple
                                               ,SCS.TermGPA_Weighted
                                               ,NULL AS newcol
                                               ,(
                                                  SELECT    SUM(ISNULL(CreditsAttempted,0))
                                                  FROM      syCreditSummary
                                                  WHERE     TermId = T.TermId
                                                            AND StuEnrollId = SE.StuEnrollId
                                                ) AS Term_CreditsAttempted
                                               ,(
                                                  SELECT    SUM(ISNULL(CreditsEarned,0))
                                                  FROM      syCreditSummary
                                                  WHERE     TermId = T.TermId
                                                            AND StuEnrollId = SE.StuEnrollId
                                                ) AS Term_CreditsEarned
                                               ,
						--Newly added ends here
                                                (
                                                  SELECT TOP 1
                                                            Average
                                                  FROM      syCreditSummary
                                                  WHERE     StuEnrollId = SE.StuEnrollId
                                                            AND TermId = T.TermId
                                                ) AS termAverage
                                               ,(
                                                  SELECT    COUNT(*) AS GrdBkWgtDetailsCount
                                                  FROM      arGrdBkResults
                                                  WHERE     StuEnrollId = SE.StuEnrollId
                                                            AND ClsSectionId = RES.TestId
                                                ) AS GrdBkWgtDetailsCount
                                               ,CASE WHEN P.ACId = 5 THEN 'True'
                                                     ELSE 'False'
                                                END AS ClockHourProgram
                                      FROM      arClassSections CS
                                      INNER JOIN arResults GBR ON CS.ClsSectionId = GBR.TestId
                                      INNER JOIN arStuEnrollments SE ON GBR.StuEnrollId = SE.StuEnrollId
                                      INNER JOIN (
                                                   SELECT   StudentId
                                                           ,FirstName
                                                           ,LastName
                                                           ,MiddleName
                                                   FROM     arStudent
                                                 ) S ON S.StudentId = SE.StudentId
                                      INNER JOIN syCampuses C ON SE.CampusId = C.CampusId
                                      INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                                      INNER JOIN arPrograms P ON P.ProgId = PV.ProgId
                                      INNER JOIN arTerm T ON CS.TermId = T.TermId
                                      INNER JOIN arReqs R ON CS.ReqId = R.ReqId
                                      INNER JOIN arResults RES ON RES.StuEnrollId = GBR.StuEnrollId
                                                                  AND RES.TestId = CS.ClsSectionId
                                      LEFT JOIN syCreditSummary SCS ON SE.StuEnrollId = SCS.StuEnrollId
                                                                       AND T.TermId = SCS.TermId
                                                                       AND R.ReqId = SCS.ReqId
                                      WHERE     SE.StuEnrollId = @StuEnrollId
                                                AND (
                                                      @StartDate IS NULL
                                                      OR @StartDateModifier IS NULL
                                                      OR (
                                                           (
                                                             ( @StartDateModifier <> '=' )
                                                             OR ( T.StartDate = @StartDate )
                                                           )
                                                           AND (
                                                                 ( @StartDateModifier <> '>' )
                                                                 OR ( T.StartDate > @StartDate )
                                                               )
                                                           AND (
                                                                 ( @StartDateModifier <> '<' )
                                                                 OR ( T.StartDate < @StartDate )
                                                               )
                                                           AND (
                                                                 ( @StartDateModifier <> '>=' )
                                                                 OR ( T.StartDate >= @StartDate )
                                                               )
                                                           AND (
                                                                 ( @StartDateModifier <> '<=' )
                                                                 OR ( T.StartDate <= @StartDate )
                                                               )
                                                         )
                                                    )
                                                AND (
                                                      @TermId IS NULL
                                                      OR T.TermId IN ( SELECT   Val
                                                                       FROM     MultipleValuesForReportParameters(@TermId,',',1) )
                                                    )
                                                AND R.IsAttendanceOnly = 0
                                      UNION
                                      SELECT DISTINCT
                                                3
                                               ,2
                                               ,PV.PrgVerId
                                               ,PV.PrgVerDescrip
                                               ,NULL
                                               ,T.TermId
                                               ,T.TermDescrip
                                               ,T.StartDate
                                               ,T.EndDate
                                               ,GBCR.ReqId
                                               ,R.Code AS CourseCode
                                               ,R.Descrip AS CourseDescrip
                                               ,'(' + R.Code + ' ) ' + R.Descrip AS CourseCodeDescrip
                                               ,R.Credits
                                               ,(
                                                  SELECT    SUM(FACreditsEarned)
                                                  FROM      syCreditSummary
                                                  WHERE     StuEnrollId = SCS.StuEnrollId
                                                            AND TermId = SCS.TermId
                                                ) AS CourseFinAidCredits
                                               ,(
                                                  SELECT    MIN(MinVal)
                                                  FROM      arGradeScaleDetails GCD
                                                           ,arGradeSystemDetails GSD
                                                  WHERE     GCD.GrdSysDetailId = GSD.GrdSysDetailId
                                                            AND GSD.IsPass = 1
                                                )
                                               ,GBCR.Score
                                               ,NULL
                                               ,NULL
                                               ,NULL
                                               ,NULL
                                               ,NULL
                                               ,NULL
                                               ,NULL
                                               ,NULL
                                               ,NULL
                                               ,NULL
                                               ,NULL
                                               ,SE.StuEnrollId
                                               ,NULL AS MinResult
                                               ,NULL AS GradeComponentDescription -- Student data    
                                               ,SCS.CreditsAttempted AS CreditsAttempted
                                               ,SCS.CreditsEarned AS CreditsEarned
                                               ,SCS.Completed AS Completed
                                               ,SCS.CurrentScore AS CurrentScore
                                               ,SCS.CurrentGrade AS CurrentGrade
                                               ,SCS.FinalScore AS FinalScore
                                               ,SCS.FinalGrade AS FinalGrade
                                               ,( CASE WHEN (
                                                              SELECT    SUM(Count_WeightedAverage_Credits)
                                                              FROM      syCreditSummary
                                                              WHERE     StuEnrollId = SE.StuEnrollId
                                                                        AND TermId = T.TermId
                                                            ) > 0 THEN (
                                                                         SELECT SUM(Product_WeightedAverage_Credits_GPA) / SUM(Count_WeightedAverage_Credits)
                                                                         FROM   syCreditSummary
                                                                         WHERE  StuEnrollId = SE.StuEnrollId
                                                                                AND TermId = T.TermId
                                                                       )
                                                       ELSE 0
                                                  END ) AS WeightedAverage_GPA
                                               ,( CASE WHEN (
                                                              SELECT    SUM(Count_SimpleAverage_Credits)
                                                              FROM      syCreditSummary
                                                              WHERE     StuEnrollId = SE.StuEnrollId
                                                                        AND TermId = T.TermId
                                                            ) > 0 THEN (
                                                                         SELECT SUM(Product_SimpleAverage_Credits_GPA) / SUM(Count_SimpleAverage_Credits)
                                                                         FROM   syCreditSummary
                                                                         WHERE  StuEnrollId = SE.StuEnrollId
                                                                                AND TermId = T.TermId
                                                                       )
                                                       ELSE 0
                                                  END ) AS SimpleAverage_GPA
                                               ,NULL AS WeightedAverage_CumGPA
                                               ,NULL AS SimpleAverage_CumGPA
                                               ,C.CampusId
                                               ,C.CampDescrip
                                               ,NULL AS rownumber
                                               ,S.FirstName AS FirstName
                                               ,S.LastName AS LastName
                                               ,S.MiddleName
                                               ,
						-- Newly added fields
                                                SCS.TermGPA_Simple
                                               ,SCS.TermGPA_Weighted
                                               ,NULL AS newcol
                                               ,(
                                                  SELECT    SUM(ISNULL(CreditsAttempted,0))
                                                  FROM      syCreditSummary
                                                  WHERE     TermId = T.TermId
                                                            AND StuEnrollId = SE.StuEnrollId
                                                ) AS Term_CreditsAttempted
                                               ,(
                                                  SELECT    SUM(ISNULL(CreditsEarned,0))
                                                  FROM      syCreditSummary
                                                  WHERE     TermId = T.TermId
                                                            AND StuEnrollId = SE.StuEnrollId
                                                ) AS Term_CreditsEarned
                                               ,
						--Newly added ends here
                                                (
                                                  SELECT TOP 1
                                                            Average
                                                  FROM      syCreditSummary
                                                  WHERE     StuEnrollId = SE.StuEnrollId
                                                            AND TermId = T.TermId
                                                ) AS termAverage
                                               ,(
                                                  SELECT    COUNT(*) AS GrdBkWgtDetailsCount
                                                  FROM      arGrdBkConversionResults
                                                  WHERE     StuEnrollId = SE.StuEnrollId
                                                            AND TermId = T.TermId
                                                            AND ReqId = R.ReqId
                                                ) AS GrdBkWgtDetailsCount
                                               ,CASE WHEN P.ACId = 5 THEN 'True'
                                                     ELSE 'False'
                                                END AS ClockHourProgram
                                      FROM      arTransferGrades GBCR
                                      INNER JOIN arStuEnrollments SE ON GBCR.StuEnrollId = SE.StuEnrollId
                                      INNER JOIN (
                                                   SELECT   StudentId
                                                           ,FirstName
                                                           ,LastName
                                                           ,MiddleName
                                                   FROM     arStudent
                                                 ) S ON S.StudentId = SE.StudentId
                                      INNER JOIN syCampuses C ON SE.CampusId = C.CampusId
                                      INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                                      INNER JOIN arPrograms P ON P.ProgId = PV.ProgId
                                      INNER JOIN arTerm T ON GBCR.TermId = T.TermId
                                      INNER JOIN arReqs R ON GBCR.ReqId = R.ReqId
						--INNER JOIN arResults AR ON GBCR.StuEnrollId = AR.StuEnrollId
                                      LEFT JOIN syCreditSummary SCS ON SE.StuEnrollId = SCS.StuEnrollId
                                                                       AND T.TermId = SCS.TermId
                                                                       AND R.ReqId = SCS.ReqId
                                      WHERE     SE.StuEnrollId = @StuEnrollId
                                                AND (
                                                      @StartDate IS NULL
                                                      OR @StartDateModifier IS NULL
                                                      OR (
                                                           (
                                                             ( @StartDateModifier <> '=' )
                                                             OR ( T.StartDate = @StartDate )
                                                           )
                                                           AND (
                                                                 ( @StartDateModifier <> '>' )
                                                                 OR ( T.StartDate > @StartDate )
                                                               )
                                                           AND (
                                                                 ( @StartDateModifier <> '<' )
                                                                 OR ( T.StartDate < @StartDate )
                                                               )
                                                           AND (
                                                                 ( @StartDateModifier <> '>=' )
                                                                 OR ( T.StartDate >= @StartDate )
                                                               )
                                                           AND (
                                                                 ( @StartDateModifier <> '<=' )
                                                                 OR ( T.StartDate <= @StartDate )
                                                               )
                                                         )
                                                    )
                                                AND (
                                                      @TermId IS NULL
                                                      OR T.TermId IN ( SELECT   Val
                                                                       FROM     MultipleValuesForReportParameters(@TermId,',',1) )
                                                    )
                                                AND R.IsAttendanceOnly = 0
                                    ) dt
                        ) dt2
              WHERE     TermNumber = 2 --AND CourseNumber<=2
            ) dt3
    WHERE   CourseNumber > 3
    ORDER BY PrgVerDescrip
           ,TermStartDate
           ,TermEndDate
           ,TermDescription
           ,CASE WHEN LTRIM(RTRIM(LOWER(@GradesFormat))) = 'letter' THEN ( RANK() OVER ( ORDER BY FinalGrade DESC ) )
            END
           ,CASE WHEN LTRIM(RTRIM(LOWER(@GradesFormat))) <> 'letter' THEN ( RANK() OVER ( ORDER BY FinalScore DESC ) )
            END
           ,CourseCode;
GO
