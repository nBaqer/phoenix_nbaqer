SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_FERPAInfo_Insert]
    (
     @FERPACategoryID UNIQUEIDENTIFIER
    ,@FERPACategoryCode VARCHAR(12)
    ,@StatusId UNIQUEIDENTIFIER
    ,@FERPACategoryDescrip VARCHAR(50)
    ,@CampGrpId UNIQUEIDENTIFIER
    ,@ModUser VARCHAR(50)
    ,@moddate DATETIME
    )
AS
    SET NOCOUNT ON;
    INSERT  INTO arFERPACategory
    VALUES  ( @FERPACategoryID,@FERPACategoryCode,@StatusId,@FERPACategoryDescrip,@CampGrpId,@ModUser,@moddate );




GO
