SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[RS_TranscriptIndividualStudentTerms] 
	-- Add the parameters for the stored procedure here
    @StudentId UNIQUEIDENTIFIER = '00000000-0000-0000-0000-000000000000' -- 
   ,@EnrollmentId VARCHAR(MAX) = '00000000-0000-0000-0000-000000000000'--
   ,@FilterStartDate DATE = NULL
   ,@FilterEndDate DATE = NULL
   ,@FilterTermDescription VARCHAR(1500) = NULL
   ,@CGPASetting VARCHAR(10) = 'TAKEBEST'
AS
    BEGIN
        SET NOCOUNT ON;
        DECLARE @TempAll TABLE
            (
             GrdSysDetailId UNIQUEIDENTIFIER
            ,DropDate DATETIME
            ,Grade CHAR(5)
            ,IsPass BIT
            ,GPA DECIMAL(18,2)
            ,IsCreditsAttempted BIT
            ,IsCreditsEarned BIT
            ,IsInGPA BIT
            ,IsDrop BIT
            ,TermId UNIQUEIDENTIFIER
            ,ClassStartDate DATETIME
            ,ClassEndDate DATETIME
            ,TermDescrip VARCHAR(50)
            ,DescripXTranscript NVARCHAR(250)
            ,StartDate DATETIME
            ,EndDate DATETIME
						--,ReqId uniqueidentifier
            ,Code VARCHAR(20)
            ,Descrip VARCHAR(100)
            ,Credits DECIMAL(6,2)
            ,Hours DECIMAL(18,2)
            ,ScheduledHours DECIMAL(18,2)
            ,CourseCategoryId UNIQUEIDENTIFIER
            ,FinAidCredits DECIMAL(18,2)
            ,CourseCategory VARCHAR(50)
            ,IsTransferGrade BIT
            ,DateIssue DATETIME
            ,seq INT
            );
        DECLARE @ReturnData TABLE
            (
             GrdSysDetailId UNIQUEIDENTIFIER
            ,DropDate DATETIME
            ,Grade CHAR(5)
            ,IsPass BIT
            ,GPA DECIMAL(18,2)
            ,IsCreditsAttempted BIT
            ,IsCreditsEarned BIT
            ,IsInGPA BIT
            ,IsDrop BIT
            ,TermId UNIQUEIDENTIFIER
            ,ClassStartDate DATETIME
            ,ClassEndDate DATETIME
            ,TermDescrip VARCHAR(50)
            ,DescripXTranscript NVARCHAR(250)
            ,StartDate DATETIME
            ,EndDate DATETIME
						--,ReqId uniqueidentifier
            ,Code VARCHAR(20)
            ,Descrip VARCHAR(100)
            ,Credits DECIMAL(6,2)
            ,Hours DECIMAL(18,2)
            ,ScheduledHours DECIMAL(18,2)
            ,CourseCategoryId UNIQUEIDENTIFIER
            ,FinAidCredits DECIMAL(18,2)
            ,CourseCategory VARCHAR(50)
            ,IsTransferGrade BIT
            ,DateIssue DATETIME
            ,seq INT
            );					 

-- organize Date Filter 
        IF @FilterEndDate IS NULL
            SET @FilterEndDate = GETDATE();
        IF @FilterStartDate IS NULL
            SET @FilterStartDate = '1900-01-01';
        WITH    partitioned
                  AS (
                       SELECT   *
                               ,ROW_NUMBER() OVER ( PARTITION BY Descrip,Grade ORDER BY EndDate DESC ) AS seq
                       FROM     (
                                  SELECT  DISTINCT
                                            ar.GrdSysDetailId
                                           ,ar.DateDetermined AS DropDate
                                           ,detail.Grade
                                           ,detail.IsPass
                                           ,ISNULL(detail.GPA,0) AS GPA
                                           ,detail.IsCreditsAttempted
                                           ,detail.IsCreditsEarned
                                           ,detail.IsInGPA
                                           ,detail.IsDrop
                                           ,term.TermId
	--,term.TermId
	--, '00000000-0000-0000-0000-000000000000' as ReqId
	--,seccion.ReqId
                                           ,seccion.StartDate AS ClassStartDate
                                           ,seccion.EndDate AS ClassEndDate
                                           ,term.TermDescrip
                                           ,ATES.DescripXTranscript
                                           ,term.StartDate
                                           ,term.EndDate
                                           ,req.Code
                                           ,req.Descrip
                                           ,req.Credits
                                           ,req.Hours
                                           ,req.Hours AS ScheduledHours
                                           ,req.CourseCategoryId
                                           ,req.FinAidCredits
                                           ,cat.Descrip AS CourseCategory
                                           ,detail.IsTransferGrade
                                           ,CASE ( req.IsExternship )
                                              WHEN 1 THEN (
                                                            SELECT  MAX(AttendedDate)
                                                            FROM    arExternshipAttendance
                                                            WHERE   arExternshipAttendance.StuEnrollId = ar.StuEnrollId
                                                          )
                                              ELSE seccion.EndDate
                                            END AS DateIssue
                                  FROM      arResults ar --t1
                                  JOIN      arGradeSystemDetails detail ON detail.GrdSysDetailId = ar.GrdSysDetailId
                                  JOIN      arClassSections seccion ON seccion.ClsSectionId = ar.TestId -- t4
                                  JOIN      arClassSectionTerms seccionTerm ON seccionTerm.ClsSectionId = seccion.ClsSectionId -- t5
                                  JOIN      arTerm term ON term.TermId = seccionTerm.TermId --t3
                                  JOIN      arReqs req ON req.ReqId = seccion.ReqId -- t2
                                  JOIN      arStuEnrollments enroll ON ar.StuEnrollId = enroll.StuEnrollId -- t6
                                  JOIN      arCourseCategories cat ON cat.CourseCategoryId = req.CourseCategoryId
                                  LEFT OUTER JOIN dbo.arTermEnrollSummary AS ATES ON term.TermId = ATES.TermId
                                                                                     AND enroll.StuEnrollId = ATES.StuEnrollId
                                  WHERE     req.IsAttendanceOnly = 0
                                            AND (
                                                  IsCourseCompleted = 1
                                                  AND detail.IsPass = 1
                                                  AND (
                                                        (
                                                          @EnrollmentId <> '00000000-0000-0000-0000-000000000000'
                                                          AND ar.StuEnrollId IN ( SELECT    *
                                                                                  FROM      dbo.MultipleValuesForReportParameters(@EnrollmentId,',',0) )
                                                        )
                                                        OR (
                                                             @EnrollmentId = '00000000-0000-0000-0000-000000000000'
                                                             AND ar.StuEnrollId IN ( SELECT roll.StuEnrollId
                                                                                     FROM   arStuEnrollments roll
                                                                                     WHERE  roll.StudentId = @StudentId )
                                                           )
                                                      )
                                                  AND (
                                                        ( @FilterTermDescription IS NOT NULL )
                                                        AND ( @FilterTermDescription <> '' )
                                                        AND term.TermDescrip IN ( SELECT    *
                                                                                  FROM      dbo.MultipleValuesForReportParameters(@FilterTermDescription,',',0) )
                                                        OR @FilterTermDescription IS NULL
                                                        OR @FilterTermDescription = ''
                                                      )
                                                  AND seccion.StartDate >= @FilterStartDate
                                                  AND seccion.EndDate <= @FilterEndDate
                                                )
                                  UNION
                                  SELECT 

	--'00000000-0000-0000-0000-000000000000' As TestId
	--,NULL as Score
	--,CourseResults.Score
	--,NULL as ResultId
                                            CourseResults.GrdSysDetailId
                                           ,CourseResults.GradeOverriddenDate 
	-- ar.DateDetermined AS DropDate
                                           ,detail.Grade
                                           ,detail.IsPass
                                           ,ISNULL(detail.GPA,0) AS GPA
                                           ,detail.IsCreditsAttempted
                                           ,detail.IsCreditsEarned
                                           ,detail.IsInGPA
                                           ,detail.IsDrop
                                           ,term.TermId
	--, '00000000-0000-0000-0000-000000000000' as ReqId
	--,seccion.ReqId
                                           ,seccion.StartDate AS ClassStartDate
                                           ,seccion.EndDate AS ClassEndDate
                                           ,term.TermDescrip
                                           ,ATES.DescripXTranscript
                                           ,term.StartDate
                                           ,term.EndDate
                                           ,req.Code
                                           ,req.Descrip
                                           ,req.Credits
                                           ,req.Hours
                                           ,req.Hours AS ScheduledHours
                                           ,req.CourseCategoryId
                                           ,req.FinAidCredits
                                           ,cat.Descrip AS CourseCategory
                                           ,detail.IsTransferGrade
                                           ,CASE ( req.IsExternship )
                                              WHEN 1 THEN (
                                                            SELECT  MAX(AttendedDate)
                                                            FROM    arExternshipAttendance
                                                            WHERE   arExternshipAttendance.StuEnrollId = CourseResults.StuEnrollId
                                                          )
                                              ELSE seccion.EndDate
                                            END AS DateIssue
                                  FROM      arTransferGrades CourseResults
                                  JOIN      arGradeSystemDetails detail ON detail.GrdSysDetailId = CourseResults.GrdSysDetailId
                                  JOIN      arReqs req ON CourseResults.ReqId = req.ReqId
                                  JOIN      arTerm term ON CourseResults.TermId = term.TermId
                                  JOIN      arStuEnrollments enroll ON CourseResults.StuEnrollId = enroll.StuEnrollId
                                  JOIN      arCourseCategories cat ON cat.CourseCategoryId = req.CourseCategoryId
                                  JOIN      arClassSections seccion ON seccion.TermId = CourseResults.TermId
                                  LEFT OUTER JOIN dbo.arTermEnrollSummary AS ATES ON term.TermId = ATES.TermId
                                                                                     AND enroll.StuEnrollId = ATES.StuEnrollId
                                  WHERE     req.IsAttendanceOnly = 0
                                            AND (
                                                  (
                                                    @EnrollmentId <> '00000000-0000-0000-0000-000000000000'
                                                    AND CourseResults.StuEnrollId IN ( SELECT   *
                                                                                       FROM     dbo.MultipleValuesForReportParameters(@EnrollmentId,',',0) )
                                                  )
                                                  OR (
                                                       @EnrollmentId = '00000000-0000-0000-0000-000000000000'
                                                       AND CourseResults.StuEnrollId IN ( SELECT    roll.StuEnrollId
                                                                                          FROM      arStuEnrollments roll
                                                                                          WHERE     roll.StudentId = @StudentId )
                                                     )
                                                )
                                            AND (
                                                  ( @FilterTermDescription IS NOT NULL )
                                                  AND ( @FilterTermDescription <> '' )
                                                  AND term.TermDescrip IN ( SELECT  *
                                                                            FROM    dbo.MultipleValuesForReportParameters(@FilterTermDescription,',',0) )
                                                  OR @FilterTermDescription IS NULL
                                                  OR @FilterTermDescription = ''
                                                )
                                            AND seccion.StartDate >= @FilterStartDate
                                            AND seccion.EndDate <= @FilterEndDate
                                ) result
                     )
            --SELECT * FROM partitioned WHERE seq = 1

INSERT  @TempAll
        SELECT  *
        FROM    partitioned
        WHERE   seq = 1;

--SELECT * FROM @TempAll

-- Union off all----------------------------	
        INSERT  @ReturnData
                SELECT  DISTINCT
	-- '00000000-0000-0000-0000-000000000000' as testId
	--ar.TestId
	--,NULL as Score
	--,ar.Score
	--,NULL as ResultId
	--,ar.ResultId
                        ar.GrdSysDetailId
                       ,ar.DateDetermined AS DropDate
                       ,detail.Grade
                       ,detail.IsPass
                       ,ISNULL(detail.GPA,0) AS GPA
                       ,detail.IsCreditsAttempted
                       ,detail.IsCreditsEarned
                       ,detail.IsInGPA
                       ,detail.IsDrop
                       ,term.TermId
	--, '00000000-0000-0000-0000-000000000000' as ReqId
	--,seccion.ReqId
                       ,seccion.StartDate AS ClassStartDate
                       ,seccion.EndDate AS ClassEndDate
                       ,term.TermDescrip
                       ,ISNULL(ATES.DescripXTranscript,'') AS DescripXTranscript
                       ,term.StartDate
                       ,term.EndDate
                       ,req.Code
                       ,req.Descrip
                       ,req.Credits
                       ,req.Hours
                       ,req.Hours AS ScheduledHours
                       ,req.CourseCategoryId
                       ,req.FinAidCredits
                       ,cat.Descrip AS CourseCategory
                       ,detail.IsTransferGrade
                       ,CASE ( req.IsExternship )
                          WHEN 1 THEN (
                                        SELECT  MAX(AttendedDate)
                                        FROM    arExternshipAttendance
                                        WHERE   arExternshipAttendance.StuEnrollId = ar.StuEnrollId
                                      )
                          ELSE seccion.EndDate
                        END AS DateIssue
                       ,0 AS seq
                FROM    arResults ar --t1
                JOIN    arGradeSystemDetails detail ON detail.GrdSysDetailId = ar.GrdSysDetailId
                JOIN    arClassSections seccion ON seccion.ClsSectionId = ar.TestId -- t4
                JOIN    arClassSectionTerms seccionTerm ON seccionTerm.ClsSectionId = seccion.ClsSectionId -- t5
                JOIN    arTerm term ON term.TermId = seccionTerm.TermId --t3
                JOIN    arReqs req ON req.ReqId = seccion.ReqId -- t2
                JOIN    arStuEnrollments enroll ON ar.StuEnrollId = enroll.StuEnrollId -- t6
                JOIN    arCourseCategories cat ON cat.CourseCategoryId = req.CourseCategoryId
                LEFT OUTER JOIN dbo.arTermEnrollSummary AS ATES ON term.TermId = ATES.TermId
                                                                   AND enroll.StuEnrollId = ATES.StuEnrollId
                WHERE   detail.IsPass = 0
                        AND req.IsAttendanceOnly = 0
                        AND (
                              (
                                @EnrollmentId <> '00000000-0000-0000-0000-000000000000'
                                AND ar.StuEnrollId IN ( SELECT  *
                                                        FROM    dbo.MultipleValuesForReportParameters(@EnrollmentId,',',0) )
                              )
                              OR (
                                   @EnrollmentId = '00000000-0000-0000-0000-000000000000'
                                   AND ar.StuEnrollId IN ( SELECT   roll.StuEnrollId
                                                           FROM     arStuEnrollments roll
                                                           WHERE    roll.StudentId = @StudentId )
                                 )
                            )
                        AND (
                              ( @FilterTermDescription IS NOT NULL )
                              AND ( @FilterTermDescription <> '' )
                              AND term.TermDescrip IN ( SELECT  *
                                                        FROM    dbo.MultipleValuesForReportParameters(@FilterTermDescription,',',0) )
                              OR @FilterTermDescription IS NULL
                              OR @FilterTermDescription = ''
                            )
                        AND seccion.StartDate >= @FilterStartDate
                        AND seccion.EndDate <= @FilterEndDate
                UNION
                SELECT  *
                FROM    @TempAll; 
  
  -- -- Calculate total fields ---------------------------------------------------------------------
  -- GET SIMPLE COUNTS OF ALL COURSES............
  -- Get all passsed coursed except the transfered courses.
        DECLARE @CountCourses INT;
        SET @CountCourses = (
                              SELECT    COUNT(*)
                              FROM      @ReturnData
                              WHERE     IsCreditsAttempted = 1
                            );
  
  
        DECLARE @Credits DECIMAL(18,2);
	-- Get total credit in both case.
        SET @Credits = (
                         SELECT TOP ( 1 )
                                SUM(Credits)
                         FROM   (
                                  SELECT    Descrip
                                           ,MAX(Credits) AS Credits
                                  FROM      @ReturnData
                                  WHERE     IsCreditsEarned = 1
                                  GROUP BY  Descrip
                                ) AS C
                       );
   
        DECLARE @Points DECIMAL(18,2);
        DECLARE @gpaCount DECIMAL(18,2);
  
        IF @CGPASetting = 'TAKEBEST'
            BEGIN
  -- Get total points in the case of @CGPASetting = 'TAKEBEST' 
                SET @Points = (
                                SELECT TOP ( 1 )
                                        SUM(Credits * GPA) AS POINTS
                                FROM    (
                                          SELECT DISTINCT
                                                    Descrip
                                                   ,Credits
                                                   ,MAX(GPA) AS GPA
                                          FROM      @ReturnData
                                          GROUP BY  Descrip
                                                   ,Credits
                                        ) AS C
                              );
  -- Get total GPA in the case of @CGPASetting = 'TAKEBEST' 
                SET @gpaCount = (
                                  SELECT TOP ( 1 )
                                            SUM(GPA)
                                  FROM      (
                                              SELECT DISTINCT
                                                        Descrip
                                                       ,MAX(GPA) AS GPA
                                              FROM      @ReturnData
                                              GROUP BY  Descrip
                                            ) AS C
                                );
  
            END;
  
  
        IF @CGPASetting = 'TAKELAST'
            BEGIN
  
   -- Get total points in the case of @CGPASetting = 'TAKELAST' 
                SET @Points = (
                                SELECT  SUM(GPA * Credits)
                                FROM    @ReturnData
                                WHERE   ( Descrip + CAST(StartDate AS VARCHAR(15)) + CAST(GPA AS VARCHAR(10)) ) NOT IN (
                                        SELECT  ( d.Descrip + CAST(d.StartDate AS VARCHAR(15)) + CAST(d.GPA AS VARCHAR(10)) ) AS id
                                        FROM    @ReturnData d
                                        JOIN    @ReturnData ret ON d.Descrip = ret.Descrip
                                        WHERE   ( d.StartDate < ret.StartDate ) )
                              );
 
                SET @gpaCount = (
                                  SELECT    SUM(GPA)
                                  FROM      @ReturnData
                                  WHERE     ( Descrip + CAST(StartDate AS VARCHAR(15)) + CAST(GPA AS VARCHAR(10)) ) NOT IN (
                                            SELECT  ( d.Descrip + CAST(d.StartDate AS VARCHAR(15)) + CAST(d.GPA AS VARCHAR(10)) ) AS id
                                            FROM    @ReturnData d
                                            JOIN    @ReturnData ret ON d.Descrip = ret.Descrip
                                            WHERE   ( d.StartDate < ret.StartDate ) )
                                );
 
 
            END; 
  --  Get quantity of courses for all conditions
        DECLARE @CountGPACourses INT;
        SET @CountGPACourses = (
                                 SELECT COUNT(1)
                                 FROM   (
                                          SELECT    Descrip
                                          FROM      @ReturnData
                                          WHERE     IsCreditsAttempted = 1
                                          GROUP BY  Descrip
                                        ) AS x
                               );
  
  -- Get CGPA in all cases
        DECLARE @CGPA DECIMAL(18,2);
        IF ( @CountGPACourses <> 0 )
            BEGIN
                SET @CGPA = @gpaCount / @CountGPACourses;
            END;
        ELSE
            BEGIN
                SET @CGPA = 0.0;
            END; 
   
  -- Return results...
        SELECT  @CountCourses AS TotalCourses
               ,@CountGPACourses AS TotalGpaCourses
               ,@Credits AS TotalCredits
               ,@Points AS TotalPoints
               ,@CGPA AS CGPA
               ,*
        FROM    @ReturnData
        ORDER BY ClassStartDate;
  
    END;

GO
