SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[USP_ScheduleConflicts_AddingClass_NoPeriods]
    @CurrentClassSectionId UNIQUEIDENTIFIER
   ,@ClsMeetingStartDate VARCHAR(8000)
   ,@ClsMeetingEndDate VARCHAR(8000)
   ,@ClsMeetingRoomId VARCHAR(8000)
   ,@InstructorId UNIQUEIDENTIFIER
   ,@CurrentTermId UNIQUEIDENTIFIER
   ,@CurrentCourseId UNIQUEIDENTIFIER
   ,@WorkDayId VARCHAR(8000)
   ,@StartTimeId VARCHAR(8000)
   ,@EndTimeId VARCHAR(8000)
   ,@CurrentCampusId UNIQUEIDENTIFIER
AS
    BEGIN
--DECLARE @CurrentClassSectionId UNIQUEIDENTIFIER,@StuEnrollId UNIQUEIDENTIFIER,@countDateRangeConflicts INT 
-- ClsSectionId is already available in Register Students Page 
-- StuEnrollId is already available in Register Students Page 
--SET @CurrentClassSectionId = '3240B0DF-D76E-4EEA-A6ED-338D54391150E' --Class for course:  Adult Nursing II 
--SET @StuEnrollId = '31B06798-650D-4839-A15B-5AF002102317' -- Kimbely Hogan
        DECLARE @countDateRangeConflicts INT; 
        SET @countDateRangeConflicts = 0;
        DECLARE @ClassesWithOverlappingDates TABLE
            (
             CourseDescrip VARCHAR(50)
            ,ClsSectionDescrip VARCHAR(50)
            ,ClsSectionId UNIQUEIDENTIFIER
            ,RoomId UNIQUEIDENTIFIER
            ,StartDate VARCHAR(50)
            ,EndDate VARCHAR(50)
            ,StartTime VARCHAR(50)
            ,EndTime VARCHAR(50)
            );

        DECLARE @ClassesWithOverlappingDays TABLE
            (
             StartDate DATETIME
            ,EndDate DATETIME
            ,WorkDaysDescrip VARCHAR(50)
            ,StartTime VARCHAR(50)
            ,EndTime VARCHAR(50)
            ,ViewOrder INT
            ,ClsSection VARCHAR(50)
            ,CourseDescrip VARCHAR(50)
            ,ClsSectionId VARCHAR(50)
            ,CourseId VARCHAR(50)
            );
											
        DECLARE @ClassesWithOverlappingTimeInterval TABLE
            (
             StartDate DATETIME
            ,EndDate DATETIME
            ,WorkDaysDescrip VARCHAR(50)
            ,StartTime VARCHAR(50)
            ,EndTime VARCHAR(50)
            ,ViewOrder INT
            ,ClsSection VARCHAR(50)
            ,CourseDescrip VARCHAR(50)
            ,ClsSectionId VARCHAR(50)
            ,CourseId VARCHAR(50)
            );
											
        DECLARE @ClassMeetingRoomId TABLE
            (
             RowNumber INT IDENTITY(1,1)
            ,RoomId UNIQUEIDENTIFIER
            );
        DECLARE @ClassMeetingDayId TABLE
            (
             RowNumber INT IDENTITY(1,1)
            ,DayId UNIQUEIDENTIFIER
            );
        DECLARE @ClassMeetingStartTimeId TABLE
            (
             RowNumber INT IDENTITY(1,1)
            ,StartTimeId UNIQUEIDENTIFIER
            );
        DECLARE @ClassMeetingEndTimeId TABLE
            (
             RowNumber INT IDENTITY(1,1)
            ,EndTimeId UNIQUEIDENTIFIER
            );
        DECLARE @ClassMeetingArray TABLE
            (
             CurrentCourseId UNIQUEIDENTIFIER
            ,StartDate VARCHAR(50)
            ,EndDate VARCHAR(50)
            ,StartTimeId UNIQUEIDENTIFIER
            ,EndTimeId UNIQUEIDENTIFIER
            ,RoomId UNIQUEIDENTIFIER
            ,WorkDaysId UNIQUEIDENTIFIER
            );


        INSERT  INTO @ClassMeetingStartTimeId
                (
                 StartTimeId
                )
                SELECT  Val
                FROM    MultipleValuesForReportParameters(@StartTimeId,',',1); 
	
        INSERT  INTO @ClassMeetingEndTimeId
                (
                 EndTimeId
                )
                SELECT  Val
                FROM    MultipleValuesForReportParameters(@EndTimeId,',',1);
	
        INSERT  INTO @ClassMeetingRoomId
                (
                 RoomId
                )
                SELECT  Val
                FROM    MultipleValuesForReportParameters(@ClsMeetingRoomId,',',1); 

        INSERT  INTO @ClassMeetingDayId
                (
                 DayId
                )
                SELECT  Val
                FROM    MultipleValuesForReportParameters(@WorkDayId,',',1); 

--SELECT * FROM @ClassMeetingStartTimeId
--SELECT * FROM @ClassMeetingEndTimeId

--SELECT DISTINCT @CurrentCourseId, DayId, RoomId, StartTimeId, EndTimeId FROM 
--@ClassMeetingStartTimeId t1 INNER JOIN @ClassMeetingEndTimeId  t2 ON  t1.RowNumber = t2.RowNumber
--LEFT OUTER JOIN @ClassMeetingRoomId t3 ON t2.RowNumber = t3.RowNumber
--LEFT OUTER JOIN @ClassMeetingDayId t4 ON t3.RowNumber = t4.RowNumber

        INSERT  INTO @ClassMeetingArray
                SELECT DISTINCT
                        @CurrentCourseId
                       ,@ClsMeetingStartDate
                       ,@ClsMeetingEndDate
                       ,StartTimeId
                       ,EndTimeId
                       ,RoomId
                       ,DayId
                FROM    @ClassMeetingStartTimeId t1
                INNER JOIN @ClassMeetingEndTimeId t2 ON t1.RowNumber = t2.RowNumber
                LEFT OUTER JOIN @ClassMeetingRoomId t3 ON t2.RowNumber = t3.RowNumber
                LEFT OUTER JOIN @ClassMeetingDayId t4 ON t3.RowNumber = t4.RowNumber;
 
/**************************** Business Rules for Schedule Conflicts *************

There are 3 layers involved in this stored proc

a. Level 1 - Check if class you are trying to add to advantage via class sections page,
			 class section with periods page has the same start date/end date or 
			 overlapping date ranges.
			 
			 Example: 
					  English Class is offered from 03/01/2012 - 05/01/2012
					  Maths Class is offered from 03/05/2012 - 05/14/2012
				
b. Level 2 - If a conflict is identified in Level 1, then check if 
			 both classes are scheduled on same day. It is perfectly normal
			 for two classes to fall in the same date range but on different dates.
			 So this check is neccessary.
			 
			 Example: 
					English class (03/01/2012-05/01/2012) - Mon, Tues
					Maths Class (03/05/2012 - 05/14/2012) - Mon, Wed
					
			In the above cases, both classes have overlapping date ranges and 
			they both offer classes on Mon
			
c. Level 3 - If conflict is identified in Level 2, then check if both classes are 
			 offered at the same time or during overlapping times. Its quite normal
			 for two classes to be offered on the same day but different times 
			 (ex:day and eve classes). Also, there is a probability for 
			 two classes to have overlapping time
			 			 
			 Example: 
				English class - Mon - 8AM - 1PM
				Maths Class - Mon - 8AM - 3.30PM
			 
**************************************************************************/
--@ClsMeetingStartDate DATETIME,
--@ClsMeetingEndDate DATETIME,
--@ClsMeetingPeriodId UNIQUEIDENTIFIER,
--@InstructorId UNIQUEIDENTIFIER

--	Select Distinct t3.*,t4.Descrip,t2.ClsSection,t2.TermId,
--	(SELECT CONVERT(VARCHAR,TimeIntervalDescrip,108) FROM cmTimeInterval b  WHERE b.TimeIntervalId = t3.TimeIntervalId) AS StartTime,
--	(SELECT CONVERT(VARCHAR,TimeIntervalDescrip,108) FROM cmTimeInterval b  WHERE b.TimeIntervalId = t3.EndIntervalId) AS EndTime
--	from arClassSections t2,arClsSectMeetings t3,arReqs t4 
--	where t2.ClsSectionId=t3.ClsSectionId AND t2.ReqId=t4.ReqId and 
--	t2.ClsSectionId<>@CurrentClassSectionId

--SELECT * FROM @ClassMeetingArray

--Select Distinct t3.ClsSectionId,t3.RoomId,@ClsMeetingStartDate AS StartDate, @ClsMeetingEndDate AS EndDate, t4.Descrip,t2.ClsSection,t2.TermId,t2.InstructorId,
--			(SELECT CONVERT(VARCHAR,TimeIntervalDescrip,108) FROM cmTimeInterval b  WHERE b.TimeIntervalId = t3.TimeIntervalId) AS StartTime,
--			(SELECT CONVERT(VARCHAR,TimeIntervalDescrip,108) FROM cmTimeInterval b  WHERE b.TimeIntervalId = t3.EndIntervalId) AS EndTime
--			from arClassSections t2,arClsSectMeetings t3,arReqs t4 
--			where t2.ClsSectionId=t3.ClsSectionId AND t2.ReqId=t4.ReqId and 
--			t2.ClsSectionId<>@CurrentClassSectionId and t2.CampusId=@CurrentCampusId
--			AND WorkDaysId IS NOT NULL 

--	Select Distinct @ClsMeetingStartDate AS StartDate, @ClsMeetingEndDate AS EndDate,  
--			t5.StartTimeId AS StartTimeId,t5.EndTimeId AS EndTimeId,@ClsMeetingRoomId AS RoomId, 
--			@CurrentClassSectionId AS ClsSectionId,@CurrentTermId AS TermId,t5.WorkDaysId AS DayId,
--			(SELECT CONVERT(VARCHAR,TimeIntervalDescrip,108) FROM cmTimeInterval b  WHERE b.TimeIntervalId = t5.StartTimeId) AS StartTime,
--			(SELECT CONVERT(VARCHAR,TimeIntervalDescrip,108) FROM cmTimeInterval b  WHERE b.TimeIntervalId = t5.EndTimeId) AS EndTime,
--			t4.Descrip from arReqs t4 INNER JOIN @ClassMeetingArray t5 ON t4.ReqId=t5.CurrentCourseId 
			
		
--			-- Get the class startdate and enddate of classes that already exist for this term
--			-- exclude the current class
--			Select Distinct t3.ClsSectionId,RoomId,@ClsMeetingStartDate AS StartDate, @ClsMeetingEndDate AS EndDate, 
--			t4.Descrip,t2.ClsSection,t2.TermId,t2.InstructorId,
--			(SELECT CONVERT(VARCHAR,TimeIntervalDescrip,108) FROM cmTimeInterval b  WHERE b.TimeIntervalId = t3.TimeIntervalId) AS StartTime,
--			(SELECT CONVERT(VARCHAR,TimeIntervalDescrip,108) FROM cmTimeInterval b  WHERE b.TimeIntervalId = t3.EndIntervalId) AS EndTime
--			from arClassSections t2,arClsSectMeetings t3,arReqs t4 
--			where t2.ClsSectionId=t3.ClsSectionId AND t2.ReqId=t4.ReqId and t2.CampusId=@CurrentCampusId AND 
--			t2.ClsSectionId<>@CurrentClassSectionId AND t3.WorkDaysId IS NOT NULL

--SELECT * FROM 
--	(SELECT DISTINCT A1.Descrip,A1.ClsSection,A1.ClsSectionId,A1.RoomId,A1.StartDate,A1.EndDate,A1.StartTime,A1.EndTime
--		FROM
--		(
--			-- Get the class startdate and enddate of classes that already exist for this term
--			-- exclude the current class
--			Select Distinct t3.ClsSectionId,RoomId,@ClsMeetingStartDate AS StartDate, @ClsMeetingEndDate AS EndDate, 
--			t4.Descrip,t2.ClsSection,t2.TermId,t2.InstructorId,
--			(SELECT CONVERT(VARCHAR,TimeIntervalDescrip,108) FROM cmTimeInterval b  WHERE b.TimeIntervalId = t3.TimeIntervalId) AS StartTime,
--			(SELECT CONVERT(VARCHAR,TimeIntervalDescrip,108) FROM cmTimeInterval b  WHERE b.TimeIntervalId = t3.EndIntervalId) AS EndTime
--			from arClassSections t2,arClsSectMeetings t3,arReqs t4 
--			where t2.ClsSectionId=t3.ClsSectionId AND t2.ReqId=t4.ReqId and t2.CampusId=@CurrentCampusId AND 
--			t2.ClsSectionId<>@CurrentClassSectionId AND t3.WorkDaysId IS NOT NULL) A1,
--			(
--			-- Get the class startdate and enddate that is being currently put in
--			Select Distinct @ClsMeetingStartDate AS StartDate,@ClsMeetingEndDate AS EndDate, 
--			t5.StartTimeId AS StartTimeId,t5.EndTimeId AS EndTimeId,@ClsMeetingRoomId AS RoomId, 
--			@CurrentClassSectionId AS ClsSectionId,@CurrentTermId AS TermId,t5.WorkDaysId AS DayId,
--			(SELECT CONVERT(VARCHAR,TimeIntervalDescrip,108) FROM cmTimeInterval b  WHERE b.TimeIntervalId = t5.StartTimeId) AS StartTime,
--			(SELECT CONVERT(VARCHAR,TimeIntervalDescrip,108) FROM cmTimeInterval b  WHERE b.TimeIntervalId = t5.EndTimeId) AS EndTime,
--			t4.Descrip from arReqs t4 INNER JOIN @ClassMeetingArray t5 ON t4.ReqId=t5.CurrentCourseId 
--			--where t4.reqId=@CurrentCourseId
--			) A2
--			WHERE 
--			A1.StartDate < A2.EndDate AND 
--			A1.EndDate>A2.StartDate  
--			AND A1.ClsSectionId <> A2.ClsSectionId AND -- Different classes
--			A1.RoomId=A2.RoomId  AND -- Same room
--			A1.TermId = A2.TermId -- Same Term
--		) dt


        INSERT  INTO @ClassesWithOverlappingDates
                SELECT  *
                FROM    (
                          SELECT DISTINCT
                                    A1.Descrip
                                   ,A1.ClsSection
                                   ,A1.ClsSectionId
                                   ,A1.RoomId
                                   ,A1.StartDate
                                   ,A1.EndDate
                                   ,A1.StartTime
                                   ,A1.EndTime
                          FROM      (
                                      -- Get the class startdate and enddate of classes that already exist for this term
			-- exclude the current class
                                      SELECT DISTINCT
                                                t3.ClsSectionId
                                               ,RoomId
                                               ,@ClsMeetingStartDate AS StartDate
                                               ,@ClsMeetingEndDate AS EndDate
                                               ,t4.Descrip
                                               ,t2.ClsSection
                                               ,t2.TermId
                                               ,t2.InstructorId
                                               ,(
                                                  SELECT    CONVERT(VARCHAR,TimeIntervalDescrip,108)
                                                  FROM      cmTimeInterval b
                                                  WHERE     b.TimeIntervalId = t3.TimeIntervalId
                                                ) AS StartTime
                                               ,(
                                                  SELECT    CONVERT(VARCHAR,TimeIntervalDescrip,108)
                                                  FROM      cmTimeInterval b
                                                  WHERE     b.TimeIntervalId = t3.EndIntervalId
                                                ) AS EndTime
                                      FROM      arClassSections t2
                                               ,arClsSectMeetings t3
                                               ,arReqs t4
                                      WHERE     t2.ClsSectionId = t3.ClsSectionId
                                                AND t2.ReqId = t4.ReqId
                                                AND t2.CampusId = @CurrentCampusId
                                                AND t2.ClsSectionId <> @CurrentClassSectionId
                                                AND t3.WorkDaysId IS NOT NULL
                                    ) A1
                                   ,(
                                      -- Get the class startdate and enddate that is being currently put in
                                      SELECT DISTINCT
                                                @ClsMeetingStartDate AS StartDate
                                               ,@ClsMeetingEndDate AS EndDate
                                               ,t5.StartTimeId AS StartTimeId
                                               ,t5.EndTimeId AS EndTimeId
                                               ,@ClsMeetingRoomId AS RoomId
                                               ,@CurrentClassSectionId AS ClsSectionId
                                               ,@CurrentTermId AS TermId
                                               ,t5.WorkDaysId AS DayId
                                               ,(
                                                  SELECT    CONVERT(VARCHAR,TimeIntervalDescrip,108)
                                                  FROM      cmTimeInterval b
                                                  WHERE     b.TimeIntervalId = t5.StartTimeId
                                                ) AS StartTime
                                               ,(
                                                  SELECT    CONVERT(VARCHAR,TimeIntervalDescrip,108)
                                                  FROM      cmTimeInterval b
                                                  WHERE     b.TimeIntervalId = t5.EndTimeId
                                                ) AS EndTime
                                               ,t4.Descrip
                                      FROM      arReqs t4
                                      INNER JOIN @ClassMeetingArray t5 ON t4.ReqId = t5.CurrentCourseId 
			--where t4.reqId=@CurrentCourseId
                                    ) A2
                          WHERE     A1.StartDate <= A2.EndDate
                                    AND A1.EndDate >= A2.StartDate
                                    AND A1.ClsSectionId <> A2.ClsSectionId
                                    AND -- Different classes
                                    A1.RoomId = A2.RoomId
                                    AND -- Same room
                                    A1.TermId = A2.TermId -- Same Term
                        ) dt;
		
        SELECT  *
        FROM    @ClassesWithOverlappingDates;
	
	-- If there are classes with overlapping days in same term
	-- Get the days on which they overlap
        INSERT  INTO @ClassesWithOverlappingDays
                SELECT DISTINCT
                        A1.StartDate
                       ,A1.EndDate
                       ,A1.WorkDaysDescrip
                       ,A1.StartTime
                       ,A1.EndTime
                       ,A1.ViewOrder
                       ,A1.ClsSection
                       ,A1.Descrip AS CourseDescrip
                       ,A1.ClsSectionId
                       ,A1.ReqId
                FROM    (
                          SELECT DISTINCT
                                    t3.ClsSectMeetingId
                                   ,t3.StartDate
                                   ,t3.EndDate
                                   ,t4.Descrip
                                   ,t2.ClsSection
                                   ,t2.TermId
                                   ,(
                                      SELECT    CONVERT(VARCHAR,TimeIntervalDescrip,108)
                                      FROM      cmTimeInterval b
                                      WHERE     b.TimeIntervalId = t3.TimeIntervalId
                                    ) AS StartTime
                                   ,(
                                      SELECT    CONVERT(VARCHAR,TimeIntervalDescrip,108)
                                      FROM      cmTimeInterval b
                                      WHERE     b.TimeIntervalId = t3.EndIntervalId
                                    ) AS EndTime
                                   ,t5.WorkDaysDescrip
                                   ,t5.ViewOrder
                                   ,t2.ClsSectionId
                                   ,t4.ReqId
                          FROM      arClassSections t2
                                   ,arClsSectMeetings t3
                                   ,arReqs t4
                                   ,plWorkDays t5
                          WHERE     t2.ClsSectionId = t3.ClsSectionId
                                    AND t2.ReqId = t4.ReqId
                                    AND t3.WorkDaysId = t5.WorkDaysId
                                    AND t2.ClsSectionId IN ( SELECT DISTINCT
                                                                    ClsSectionId
                                                             FROM   @ClassesWithOverlappingDates )
                        ) A1
                       ,(
                          SELECT DISTINCT
                                    t4.RoomId AS RoomId
                                   ,@CurrentClassSectionId AS ClsSectionId
                                   ,@CurrentTermId AS TermId
                                   ,t3.Descrip AS Descrip
                                   ,(
                                      SELECT    CONVERT(VARCHAR,TimeIntervalDescrip,108)
                                      FROM      cmTimeInterval b
                                      WHERE     b.TimeIntervalId = t4.StartTimeId
                                    ) AS StartTime
                                   ,(
                                      SELECT    CONVERT(VARCHAR,TimeIntervalDescrip,108)
                                      FROM      cmTimeInterval b
                                      WHERE     b.TimeIntervalId = t4.EndTimeId
                                    ) AS EndTime
                                   ,t6.WorkDaysDescrip
                                   ,t6.ViewOrder
                          FROM      arReqs t3
                          INNER JOIN @ClassMeetingArray t4 ON t3.ReqId = t4.CurrentCourseId
                          INNER JOIN dbo.plWorkDays t6 ON t4.WorkDaysId = t6.WorkDaysId
                        ) A2
                WHERE   A1.WorkDaysDescrip = A2.WorkDaysDescrip
                ORDER BY A1.ViewOrder
                       ,A1.ClsSection
                       ,A1.StartDate
                       ,A1.EndDate
                       ,A1.StartTime
                       ,A1.EndTime;
	
        SELECT  *
        FROM    @ClassesWithOverlappingDays;
		
	--Select Distinct t4.RoomId AS RoomId, 
	--		@CurrentClassSectionId AS ClsSectionId,@CurrentTermId AS TermId,
	--		t3.Descrip AS Descrip,
	--		(SELECT convert(varchar, TimeIntervalDescrip, 108) FROM dbo.cmTimeInterval WHERE TimeIntervalId=t4.StartTimeId) AS StartTime,
	--		(SELECT CONVERT(varchar,TimeIntervalDescrip,108) FROM dbo.cmTimeInterval WHERE TimeIntervalId=t4.EndTimeId) AS EndTime,t7.WorkDaysDescrip,t7.ViewOrder
	--	FROM
	--		--(SELECT * FROM syPeriods WHERE PeriodId=@CurrentPeriodId) t5, dbo.syPeriodsWorkDays t6, plWorkDays t7
	--		arReqs t3 INNER JOIN @ClassMeetingArray t4 ON t3.ReqId=t4.CurrentCourseId 
	--		INNER JOIN plWorkDays t7 ON t4.WorkDaysId=t7.WorkDaysId
	
    -- If classes overlap on a day check the timings to see if there is a overlap
        INSERT  INTO @ClassesWithOverlappingTimeInterval
                SELECT  A2.*
                FROM    (
                          SELECT DISTINCT
                                    t4.RoomId AS RoomId
                                   ,@CurrentClassSectionId AS ClsSectionId
                                   ,@CurrentTermId AS TermId
                                   ,t3.Descrip AS Descrip
                                   ,(
                                      SELECT    CONVERT(VARCHAR,TimeIntervalDescrip,108)
                                      FROM      dbo.cmTimeInterval
                                      WHERE     TimeIntervalId = t4.StartTimeId
                                    ) AS StartTime
                                   ,(
                                      SELECT    CONVERT(VARCHAR,TimeIntervalDescrip,108)
                                      FROM      dbo.cmTimeInterval
                                      WHERE     TimeIntervalId = t4.EndTimeId
                                    ) AS EndTime
                                   ,t7.WorkDaysDescrip
                                   ,t7.ViewOrder
                          FROM      --(SELECT * FROM syPeriods WHERE PeriodId=@CurrentPeriodId) t5, dbo.syPeriodsWorkDays t6, plWorkDays t7
                                    arReqs t3
                          INNER JOIN @ClassMeetingArray t4 ON t3.ReqId = t4.CurrentCourseId
                          INNER JOIN plWorkDays t7 ON t4.WorkDaysId = t7.WorkDaysId
                        ) A1
                       ,@ClassesWithOverlappingDays A2
                WHERE   --(A1.StartDate>=A2.StartDate OR A1.StartDate <= A2.EndDate) AND
	--(A1.StartTime<A2.EndTime AND A1.EndTime>A2.StartTime) AND
                        ( A1.WorkDaysDescrip = A2.WorkDaysDescrip )
                        AND (
                              A1.StartTime < A2.EndTime
                              AND A1.EndTime > A2.StartTime
                            );
	
	--SELECT * FROM @ClassesWithOverlappingTimeInterval
	
	--SELECT DISTINCT CourseDescrip,ClsSection,StartDate,EndDate,WorkDaysDescrip,StartTime,EndTime FROM @ClassesWithOverlappingTimeInterval
        SELECT DISTINCT
                CourseId
               ,CourseDescrip
               ,ClsSectionId
               ,ClsSection
               ,CONVERT(VARCHAR,StartDate,101) + '- ' + CONVERT(VARCHAR,EndDate,101) AS ClassPeriod
               ,WorkDaysDescrip + ' (' + StartTime + ' - ' + EndTime + ')' AS MeetingSchedule
               ,ViewOrder
               ,StartDate
               ,EndDate
        FROM    @ClassesWithOverlappingTimeInterval
        ORDER BY CourseDescrip
               ,ClsSection
               ,StartDate
               ,EndDate
               ,ViewOrder;
    END;
GO
