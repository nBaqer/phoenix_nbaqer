SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_AR_GetAllStudentByStartDateAndProgramCountNewStartAndReEnrolledTotal]
    @StartDate DATETIME
   ,@CampGrpId AS VARCHAR(8000)
   ,@ProgVerId AS VARCHAR(8000) = NULL
AS /*----------------------------------------------------------------------------------------------------
    Author : Vijay Ramteke
    
    Create date : 09/13/2010
    
    Procedure Name : usp_AR_GetAllStudentByStartDateAndProgramCountNewStartAndReEnrolledTotal

    Objective : Get All The Student New Start And Reenrolled Count By Start Date And Program Version
    
    Parameters : Name Type Data Type Required? 
    
    Output : Returns All The Student New Start And Reenrolled Count By Start Date And Program Version for Report Dataset
                        
*/-----------------------------------------------------------------------------------------------------

    BEGIN

        SELECT  S.StudentId
               ,SE.PrgVerId
        INTO    #TempStudent
        FROM    arStudent S
               ,arStuEnrollments SE
               ,syCmpGrpCmps E
               ,syCampuses F
               ,syCampGrps
        WHERE   S.StudentId = SE.StudentId
                AND CAST(SE.StartDate AS DATE) = CAST(@StartDate AS DATE)
                AND (
                      SE.PrgVerId IN ( SELECT   strval
                                       FROM     dbo.SPLIT(@ProgVerId) )
                      OR @ProgVerId IS NULL
                    )
                AND SE.CampusId IN ( SELECT DISTINCT
                                            t1.CampusId
                                     FROM   syCmpGrpCmps t1
                                     WHERE  t1.CampGrpId IN ( SELECT    strval
                                                              FROM      dbo.SPLIT(@CampGrpId) ) )
                AND SE.CampusId = F.CampusId
                AND F.CampusId = E.CampusId
                AND E.CampGrpId = syCampGrps.CampGrpId
                AND syCampGrps.CampGrpId IN ( SELECT DISTINCT
                                                        t1.CampGrpId
                                              FROM      syCmpGrpCmps t1
                                              WHERE     t1.CampGrpId IN ( SELECT    strval
                                                                          FROM      dbo.SPLIT(@CampGrpId) ) );


        SELECT  COUNT(SE.StuEnrollId) AS ReEnrollCount
               ,SE.StudentId
               ,SE.PrgVerId
        INTO    #TempReEnrollCount
        FROM    arStuEnrollments SE
               ,#TempStudent TS
               ,syCmpGrpCmps E
               ,syCampuses F
               ,syCampGrps
        WHERE   TS.StudentId = SE.StudentId
                AND TS.PrgVerId = SE.PrgVerId
                AND (
                      SE.PrgVerId IN ( SELECT   strval
                                       FROM     dbo.SPLIT(@ProgVerId) )
                      OR @ProgVerId IS NULL
                    )
                AND CAST(SE.StartDate AS DATE) <= CAST(@StartDate AS DATE)
                AND SE.CampusId = F.CampusId
                AND F.CampusId = E.CampusId
                AND E.CampGrpId = syCampGrps.CampGrpId
                AND syCampGrps.CampGrpId IN ( SELECT DISTINCT
                                                        t1.CampGrpId
                                              FROM      syCmpGrpCmps t1
                                              WHERE     t1.CampGrpId IN ( SELECT    strval
                                                                          FROM      dbo.SPLIT(@CampGrpId) ) )
        GROUP BY SE.PrgVerId
               ,SE.StudentId
        HAVING  COUNT(SE.StudentId) > 1;

        SELECT  S.StudentId
               ,SE.PrgVerId
        INTO    #TempStudentREEnroll
        FROM    arStudent S
               ,arStuEnrollments SE
               ,syCmpGrpCmps E
               ,syCampuses F
               ,syCampGrps
        WHERE   S.StudentId = SE.StudentId
                AND CAST(SE.StartDate AS DATE) = CAST(@StartDate AS DATE)
                AND (
                      SE.PrgVerId IN ( SELECT   strval
                                       FROM     dbo.SPLIT(@ProgVerId) )
                      OR @ProgVerId IS NULL
                    )
                AND SE.CampusId IN ( SELECT DISTINCT
                                            t1.CampusId
                                     FROM   syCmpGrpCmps t1
                                     WHERE  t1.CampGrpId IN ( SELECT    strval
                                                              FROM      dbo.SPLIT(@CampGrpId) ) )
                AND ReEnrollmentDate IS NOT NULL
                AND SE.StudentId NOT IN ( SELECT    StudentId
                                          FROM      #TempReEnrollCount )
                AND SE.CampusId = F.CampusId
                AND F.CampusId = E.CampusId
                AND E.CampGrpId = syCampGrps.CampGrpId
                AND syCampGrps.CampGrpId IN ( SELECT DISTINCT
                                                        t1.CampGrpId
                                              FROM      syCmpGrpCmps t1
                                              WHERE     t1.CampGrpId IN ( SELECT    strval
                                                                          FROM      dbo.SPLIT(@CampGrpId) ) );



        SELECT  SUM(CountNew) AS CountNew
               ,SUM(CountReenrolls) AS CountReenrolls
        FROM    (
                  SELECT    CountNew - CountReenrolls AS CountNew
                           ,CountReenrolls
                  FROM      (
                              SELECT    COUNT(StudentId) AS CountNew
                                       ,0 AS CountReenrolls
                              FROM      #TempStudent
                              UNION ALL
                              SELECT    0 AS CountNew
                                       ,COUNT(ReEnrollCount) AS CountReenrolls
                              FROM      #TempReEnrollCount
                              UNION ALL
                              SELECT    0 AS CountNew
                                       ,COUNT(StudentId) AS CountReenrolls
                              FROM      #TempStudentREEnroll
                            ) tt
                ) ttt; 


        DROP TABLE #TempStudent;
        DROP TABLE #TempReEnrollCount;
        DROP TABLE #TempStudentREEnroll;


    END;





GO
