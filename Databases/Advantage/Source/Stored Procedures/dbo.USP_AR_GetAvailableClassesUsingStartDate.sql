SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

--=================================================================================================
-- USP_AR_GetAvailableClassesUsingStartDate
--=================================================================================================
--exec [USP_AR_GetAvailableClassesUsingStartDate] 'ead46332-790c-4666-bf41-0bacfc47dfb4'    
CREATE PROCEDURE [dbo].[USP_AR_GetAvailableClassesUsingStartDate]
    (
     @StuEnrollId UNIQUEIDENTIFIER    
    )
AS /*----------------------------------------------------------------------------------------------------    
 Author          : Janet Robinson    
        
    Create date  : 06/11/2013    
        
 Procedure Name : USP_AR_GetAvailableClassesUsingStartDate    
    
 Objective  : Refactoring of Function: GetAvailableClassesForEnrollmentUsingStartDate    
      in File Name: TermProgressDB.vb in DataAccess Project    
      Query to get the available classes for Student Schedule page     
     
 Parameters  : Name   Type Data Type Required?      
      =====   ==== ========= =========     
      @StuEnrollID In  Varchar  Required     
           
          
*/-----------------------------------------------------------------------------------------------------    
    
    BEGIN    
    --    
 -- File Name: TermProgressDB.vb in DataAccess Project    
 -- Function: GetAvailableClassesForEnrollmentUsingStartDate    
 -- Query to get the available classes    
 -- Discussion points : 1. Subqueries need to be moved into a table variable    
 -- Inline sql will be replaced with stored procedure    
 -- Look out of Business Rules/Changes comment    
     
        DECLARE @ListCoursesStudentPassed TABLE
            (
             CourseId UNIQUEIDENTIFIER
            ,AllowRetake BIT
            ,IsPass BIT
            ,PassedEndDate DATETIME
            );    
 --Declare @ListCoursesOrEquivalentCoursesStudentRegisteredFor table(ClassId uniqueidentifier)    
        DECLARE @ProgramId UNIQUEIDENTIFIER;    
        DECLARE @IsCEProgram BIT;          
        DECLARE @Result TABLE
            (
             ClsSectionId UNIQUEIDENTIFIER
            ,ClsSection VARCHAR(12)
            ,ReqId UNIQUEIDENTIFIER
            ,TermId UNIQUEIDENTIFIER
            ,TermDescrip VARCHAR(50)
            ,TermStart DATETIME
            ,StartDate DATETIME
            ,EndDate DATETIME
            ,Code VARCHAR(12)
            ,Descrip VARCHAR(100)
            );    
    
 -- List of Courses the Student Passed     
        INSERT  INTO @ListCoursesStudentPassed
                (
                 CourseId
                ,AllowRetake
                ,IsPass
                ,PassedEndDate    
                )     
 -- Get Courses Student Passed from arResults    
                SELECT DISTINCT
                        Courses.ReqId
                       ,Courses.AllowCompletedCourseRetake
                       ,GradeDetails.IsPass
                       ,Classes.EndDate
                FROM    arReqs Courses
                INNER JOIN arClassSections Classes ON Classes.ReqId = Courses.ReqId
                INNER JOIN arResults CourseGrades ON CourseGrades.TestId = Classes.ClsSectionId
                INNER JOIN arGradeSystemDetails GradeDetails ON CourseGrades.GrdSysDetailId = GradeDetails.GrdSysDetailId
                WHERE   CourseGrades.StuEnrollId = @StuEnrollId
                        AND CourseGrades.GrdSysDetailId IS NOT NULL
                        AND GradeDetails.IsIncomplete = 0 -- Courses with incomplete grades are considered incomplete courses    
                UNION    
 -- Get Courses Student Passed from arTransferGrades    
                SELECT DISTINCT
                        Courses.ReqId
                       ,Courses.AllowCompletedCourseRetake
                       ,GradeDetails.IsPass
                       ,Classes.EndDate
                FROM    arTransferGrades CourseGrades
                INNER JOIN arReqs Courses ON CourseGrades.ReqId = Courses.ReqId
                INNER JOIN arGradeSystemDetails GradeDetails ON CourseGrades.GrdSysDetailId = GradeDetails.GrdSysDetailId
                INNER JOIN arClassSections Classes ON Classes.ReqId = Courses.ReqId
                WHERE   CourseGrades.StuEnrollId = @StuEnrollId
                        AND CourseGrades.GrdSysDetailId IS NOT NULL
                        AND GradeDetails.IsIncomplete = 0; -- Courses with incomplete grades are considered incomplete courses    
       
        SET @ProgramId = (
                           SELECT TOP 1
                                    ProgId
                           FROM     arPrgVersions ProgramVersion
                           INNER JOIN arStuEnrollments StudentEnrollment ON ProgramVersion.PrgVerId = StudentEnrollment.PrgVerId
                           WHERE    StudentEnrollment.StuEnrollId = @StuEnrollId
                         );    
      
        SET @IsCEProgram = ISNULL((
                                    SELECT  pv.IsContinuingEd
                                    FROM    arPrgVersions pv
                                           ,dbo.arStuEnrollments se
                                    WHERE   pv.PrgVerId = se.PrgVerId
                                            AND se.StuEnrollId = @StuEnrollId
                                  ),0);     
            
         
  --returned results.    
  --If we are dealing with a CE program then the student can take any course offered by the campus    
        IF @IsCEProgram = 1
            BEGIN    
   --PRINT 'Processing CE Program'    
                INSERT  INTO @Result
                        (
                         ClsSectionId
                        ,ClsSection
                        ,ReqId
                        ,TermDescrip
                        ,TermId
                        ,TermStart
                        ,StartDate
                        ,EndDate
                        ,Code
                        ,Descrip    
                        )
                        SELECT DISTINCT
                                t0.ClsSectionId
                               ,t0.ClsSection
                               ,t0.ReqId
                               ,t11.TermDescrip
                               ,t11.TermId AS TermId
                               ,t11.StartDate AS TermStart
                               ,t0.StartDate
                               ,t0.EndDate
                               ,t9.Code
                               ,t9.Descrip
                        FROM    arClassSections t0
                               ,arStuEnrollments t8
                               ,arReqs t9
                               ,arClassSectionTerms t10
                               ,arTerm t11
                        WHERE   -- Business Rule 1    
      -- Ignore the courses the student has already taken and got a passing score    
      -- Change 1: If the "AllowRetakesForCompletedCourse" is set to True    
      -- then by pass business rule 1    
                                (
                                  t0.ReqId NOT IN ( SELECT DISTINCT
                                                            CourseId
                                                    FROM    @ListCoursesStudentPassed
                                                    WHERE   AllowRetake = 0
                                                            AND IsPass = 1 )
                                  OR t0.ReqId IN ( SELECT DISTINCT
                                                            CourseId
                                                   FROM     @ListCoursesStudentPassed
                                                   WHERE    AllowRetake = 1 )
                                )
                                AND t0.ReqId = t9.ReqId
                                AND t0.ClsSectionId = t10.ClsSectionId
                                AND t10.TermId = t11.TermId    
         --                       AND (    
         --t0.ShiftId = t8.ShiftId    
         --                             OR (    
         --                                  t0.ShiftId IS NULL    
         --                                  AND t8.ShiftId IS NULL    
         --                                )    
         --                           )    
                                AND t8.StuEnrollId = @StuEnrollId    
      -- Business Rule 2    
        -- Make sure the Student is not registered in any class and    
        -- also make sure the student didn't pass a course marked as a equivalent course    
        -- Change 2: If the "AllowRetakesForCompletedCourse" is set to True    
        -- then by pass business rule 2                                                       
                                AND (
                                      t0.ClsSectionId NOT IN (
                                      SELECT    CourseResults.TestId
                                      FROM      arResults CourseResults
                                      WHERE     CourseResults.StuEnrollId = @StuEnrollId
                                      UNION
                                      SELECT    Class.ClsSectionId
                                      FROM      arResults CourseResults
                                      INNER JOIN arClassSections Class ON CourseResults.TestId = Class.ClsSectionId
                                      INNER JOIN arCourseEquivalent EquivalentCourse ON EquivalentCourse.EquivReqId = t0.ReqId
                                                                                        AND Class.ReqId = EquivalentCourse.ReqId
                                      INNER JOIN arGradeSystemDetails GradeDetails ON CourseResults.GrdSysDetailId = GradeDetails.GrdSysDetailId
                                      INNER JOIN @ListCoursesStudentPassed ListCoursesStudentPassed ON ListCoursesStudentPassed.CourseId = EquivalentCourse.EquivReqId
                                      WHERE     CourseResults.StuEnrollId = @StuEnrollId
                                                AND GradeDetails.IsPass = 1
                                                AND ListCoursesStudentPassed.AllowRetake = 0 )
                                      OR t0.ClsSectionId IN (
                                      SELECT    Class.ClsSectionId
                                      FROM      arResults CourseResults
                                      INNER JOIN arClassSections Class ON CourseResults.TestId = Class.ClsSectionId
                                      INNER JOIN arCourseEquivalent EquivalentCourse ON EquivalentCourse.EquivReqId = t0.ReqId
                                                                                        AND Class.ReqId = EquivalentCourse.ReqId
                                      INNER JOIN arGradeSystemDetails GradeDetails ON CourseResults.GrdSysDetailId = GradeDetails.GrdSysDetailId
                                      INNER JOIN @ListCoursesStudentPassed ListCoursesStudentPassed ON ListCoursesStudentPassed.CourseId = EquivalentCourse.EquivReqId
                                      WHERE     CourseResults.StuEnrollId = @StuEnrollId
                                                AND GradeDetails.IsPass = 1
                                                AND ListCoursesStudentPassed.AllowRetake = 1 )
                                    )
                                AND t0.EndDate >= t8.StartDate
                                AND t0.EndDate <= t8.ExpGradDate    
                        -- Modified by Balaji on 6.17.2013    
     -- The following line was not allowing a course to be re-taken if grade was transferred for a course    
     --and t9.ReqId not in (select Distinct ReqId from arTransferGrades where StuEnrollId=@StuEnrollID)     
                                AND t9.ReqId NOT IN ( SELECT DISTINCT
                                                                CourseId
                                                      FROM      @ListCoursesStudentPassed
                                                      WHERE     AllowRetake = 0
                                                                AND IsPass = 1 )
                                AND t0.CampusId = t8.CampusId;     
                            
       
            END;          
        ELSE
            BEGIN    
                INSERT  INTO @Result
                        (
                         ClsSectionId
                        ,ClsSection
                        ,ReqId
                        ,TermDescrip
                        ,TermId
                        ,TermStart
                        ,StartDate
                        ,EndDate
                        ,Code
                        ,Descrip    
                        )
                        SELECT DISTINCT
                                t0.ClsSectionId
                               ,t0.ClsSection
                               ,t0.ReqId
                               ,t11.TermDescrip
                               ,t11.TermId AS TermId
                               ,t11.StartDate AS TermStart
                               ,t0.StartDate
                               ,t0.EndDate
                               ,t9.Code
                               ,t9.Descrip
                        FROM    arClassSections t0
                               ,arStuEnrollments t8
                               ,arReqs t9
                               ,arClassSectionTerms t10
                               ,arTerm t11
                               ,arProgVerDef t12
                        WHERE   -- Business Rule 1    
      -- Ignore the courses the student has already taken and got a passing score    
      -- Change 1: If the "AllowRetakesForCompletedCourse" is set to True    
      -- then by pass business rule 1    
                                (
                                  t0.ReqId NOT IN ( SELECT DISTINCT
                                                            CourseId
                                                    FROM    @ListCoursesStudentPassed
                                                    WHERE   AllowRetake = 0
                                                            AND IsPass = 1 )
                                  OR t0.ReqId IN ( SELECT DISTINCT
                                                            CourseId
                                                   FROM     @ListCoursesStudentPassed
                                                   WHERE    AllowRetake = 1 )
                                )
                                AND t0.ReqId = t9.ReqId
                                AND t0.ClsSectionId = t10.ClsSectionId
                                AND t10.TermId = t11.TermId    
                                --AND (    
                                --      t0.ShiftId = t8.ShiftId    
                                --      OR (    
                                --           t0.ShiftId IS NULL    
                                --           AND t8.ShiftId IS NULL    
                                --         )    
                                --    )    
                                AND t8.StuEnrollId = @StuEnrollId
                                AND (
                                      t11.ProgId = @ProgramId
                                      OR t11.ProgId IS NULL
                                    )     
        
      -- Business Rule 2    
      -- Make sure the Student is not registered in any class and    
      -- also make sure the student didn't pass a course marked as a equivalent course    
      -- Change 2: If the "AllowRetakesForCompletedCourse" is set to True    
      -- then by pass business rule 2                                                       
                                AND (
                                      t0.ClsSectionId NOT IN (
                                      SELECT    CourseResults.TestId
                                      FROM      arResults CourseResults
                                      WHERE     CourseResults.StuEnrollId = @StuEnrollId
                                      UNION
                                      SELECT    Class.ClsSectionId
                                      FROM      arResults CourseResults
                                      INNER JOIN arClassSections Class ON CourseResults.TestId = Class.ClsSectionId
                                      INNER JOIN arCourseEquivalent EquivalentCourse ON EquivalentCourse.EquivReqId = t0.ReqId
                                                                                        AND Class.ReqId = EquivalentCourse.ReqId
                                      INNER JOIN arGradeSystemDetails GradeDetails ON CourseResults.GrdSysDetailId = GradeDetails.GrdSysDetailId
                                      INNER JOIN @ListCoursesStudentPassed ListCoursesStudentPassed ON ListCoursesStudentPassed.CourseId = EquivalentCourse.EquivReqId
                                      WHERE     CourseResults.StuEnrollId = @StuEnrollId
                                                AND GradeDetails.IsPass = 1
                                                AND ListCoursesStudentPassed.AllowRetake = 0 )
                                      OR t0.ClsSectionId IN (
                                      SELECT    Class.ClsSectionId
                                      FROM      arResults CourseResults
                                      INNER JOIN arClassSections Class ON CourseResults.TestId = Class.ClsSectionId
                                      INNER JOIN arCourseEquivalent EquivalentCourse ON EquivalentCourse.EquivReqId = t0.ReqId
                                                                                        AND Class.ReqId = EquivalentCourse.ReqId
                                      INNER JOIN arGradeSystemDetails GradeDetails ON CourseResults.GrdSysDetailId = GradeDetails.GrdSysDetailId
                                      INNER JOIN @ListCoursesStudentPassed ListCoursesStudentPassed ON ListCoursesStudentPassed.CourseId = EquivalentCourse.EquivReqId
                                      WHERE     CourseResults.StuEnrollId = @StuEnrollId
                                                AND GradeDetails.IsPass = 1
                                                AND ListCoursesStudentPassed.AllowRetake = 1 )
                                    )
                                AND t0.EndDate >= t8.StartDate
                                AND t0.EndDate <= t8.ExpGradDate
                                AND t8.PrgVerId = t12.PrgVerId
                                AND t9.ReqId IN ( SELECT DISTINCT
                                                            ReqId
                                                  FROM      arProgVerDef
                                                  WHERE     PrgVerId = t12.PrgVerId
                                                  UNION
                                                  SELECT DISTINCT
                                                            ReqId
                                                  FROM      arReqGrpDef
                                                  WHERE     GrpId IN ( SELECT DISTINCT
                                                                                ReqId
                                                                       FROM     arProgVerDef
                                                                       WHERE    PrgVerId = t12.PrgVerId ) )     
        
    -- Modified by Balaji on 6.17.2013    
       -- The following line was not allowing a course to be re-taken if grade was transferred for a course    
    --and t9.ReqId not in (select Distinct ReqId from arTransferGrades where StuEnrollId=@StuEnrollID)     
                                AND t9.ReqId NOT IN ( SELECT DISTINCT
                                                                CourseId
                                                      FROM      @ListCoursesStudentPassed
                                                      WHERE     AllowRetake = 0
                                                                AND IsPass = 1 )
                                AND t0.CampusId = t8.CampusId
                        UNION
                        SELECT DISTINCT
                                t0.ClsSectionId
                               ,t0.ClsSection
                               ,t0.ReqId
                               ,t11.TermDescrip
                               ,t11.TermId AS TermId
                               ,t11.StartDate AS TermStart
                               ,t0.StartDate
                               ,t0.EndDate
                               ,t9.Code
                               ,t9.Descrip
                        FROM    arClassSections t0
                               ,arStuEnrollments t8
                               ,(
                                  SELECT    t1.ReqId
                                           ,ReqSeq
                                           ,IsRequired
                                           ,t2.Descrip
                                           ,t2.Code
                                           ,t2.ReqTypeId
                                           ,t2.Hours
                                           ,t2.Credits
                                           ,t1.ModUser
                                           ,t1.ModDate
                                  FROM      arReqGrpDef t1
                                           ,arReqs t2
                                  WHERE     t1.ReqId = t2.ReqId
                                            AND t1.GrpId IN ( SELECT DISTINCT
                                                                        t0.ReqId
                                                              FROM      arReqs t0
                                                                       ,arProgVerDef t1
                                                                       ,arReqGrpDef t2
                                                              WHERE     PrgVerId = (
                                                                                     SELECT PrgVerId
                                                                                     FROM   arStuEnrollments
                                                                                     WHERE  StuEnrollId = @StuEnrollId
                                                                                   )
                                                                        AND t1.ReqId = t2.GrpId
                                                                        AND t0.ReqId = t1.ReqId )
                                ) t9
                               ,arClassSectionTerms t10
                               ,arTerm t11
                        WHERE   -- Modified by Balaji on 6.17.2013    
      -- The following conditions were earlier looking for AllowRetake=1, it has been modified.    
                                (
                                  t0.ReqId NOT IN ( SELECT DISTINCT
                                                            CourseId
                                                    FROM    @ListCoursesStudentPassed
                                                    WHERE   AllowRetake = 0
                                                            AND IsPass = 1 )
                                  OR t0.ReqId IN ( SELECT DISTINCT
                                                            CourseId
                                                   FROM     @ListCoursesStudentPassed
                                                   WHERE    AllowRetake = 1 )
                                )
                                AND t0.ReqId = t9.ReqId
                                AND t0.ClsSectionId = t10.ClsSectionId
                                AND t10.TermId = t11.TermId    
--                                AND (    
--                                      t0.ShiftId = t8.ShiftId    
--                                      OR (    
--                                           t0.ShiftId IS NULL    
--AND t8.ShiftId IS NULL    
--                                         )    
--                                    )     
     -- AND t0.ShiftId = t8.ShiftId     
                                AND t8.StuEnrollId = @StuEnrollId
                                AND (
                                      t11.ProgId = @ProgramId
                                      OR t11.ProgId IS NULL
                                    )     
                 
      -- Business Rule 4    
      -- Ignore the graded courses the student has already taken    
      -- Ignore the courses with "transferred" grade    
      -- Change 3: If the "AllowRetakesForCompletedCourse" is set to True    
      -- then by pass business rule 4    
                                AND t0.ClsSectionId NOT IN ( SELECT t7.TestId
                                                             FROM   arResults t7
                                                             WHERE  t7.StuEnrollId = @StuEnrollId )     
          
      -- Modified by Balaji on 6.17.2013    
      -- The following line was not allowing a course to be re-taken if grade was transferred for a course    
      --and t9.ReqId not in (select Distinct ReqId from arTransferGrades where StuEnrollId=@StuEnrollID)     
                                AND t9.ReqId NOT IN ( SELECT DISTINCT
                                                                CourseId
                                                      FROM      @ListCoursesStudentPassed
                                                      WHERE     AllowRetake = 0
                                                                AND IsPass = 1 )    
                 
    --DE1140: Student's Scheduled tab does not show the class sections if it is current.     
                                AND t0.EndDate >= t8.StartDate
                                AND t0.EndDate <= t8.ExpGradDate     
    --Added to show the ClassSections related to the student campus related to the student    
    --Added on mAy 12 2010    
    --To fix Mantis case : 18921 data Fix issues for Ross    
                                AND t0.CampusId = t8.CampusId;     
      
            END;          
    
            
       
        DECLARE @enrollmentStartDate DATE = (
                                              SELECT    StartDate
                                              FROM      dbo.arStuEnrollments
                                              WHERE     StuEnrollId = @StuEnrollId
                                            );    
  --SELECT @enrollmentStartDate;    
        SELECT  ClsSectionId
               ,ClsSection
               ,ReqId
               ,TermDescrip
               ,TermId AS TermId
               ,TermStart
               ,StartDate
               ,EndDate
               ,Code
               ,Descrip
        FROM    @Result
        WHERE   (
                  ReqId NOT IN ( SELECT CourseId
                                 FROM   @ListCoursesStudentPassed )
                  OR ReqId IN ( SELECT  CourseId
                                FROM    @ListCoursesStudentPassed ListCoursesStudentPassed
                                WHERE   ListCoursesStudentPassed.AllowRetake = 1
                                        AND StartDate > ListCoursesStudentPassed.PassedEndDate )
                )    
    -- if the class has began you can not register in.    
                AND @enrollmentStartDate <= StartDate    
    -- You can not register the class if the End date is before to today.    
                AND GETDATE() <= EndDate
        ORDER BY TermStart DESC
               ,Descrip
               ,StartDate;     
        
    END;     
--=================================================================================================
-- END  --  USP_AR_GetAvailableClassesUsingStartDate
--=================================================================================================    
GO
