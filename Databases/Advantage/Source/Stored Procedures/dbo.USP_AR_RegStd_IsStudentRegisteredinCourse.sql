SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[USP_AR_RegStd_IsStudentRegisteredinCourse]
    (
     @StuEnrollId UNIQUEIDENTIFIER
    ,@ClsSectID UNIQUEIDENTIFIER
    )
AS /*----------------------------------------------------------------------------------------------------
	Author          :	Saraswathi Lakshmanan
    
    Create date		:	02/02/2010
    
	Procedure Name	:	USP_AR_RegStd_IsStudentRegisteredinCourse

	Objective		:	Check if student schedules conflict
	
	Parameters		:	Name			Type	Data Type			
						=====			====	=========			
						@StuEnrollid	In		UniqueIdentifier
						@ClsSectID		In		UniqueIdentifier
						
	
	Output			:	Returns the count of courses the student is reguistered in 
	
	
						
*/-----------------------------------------------------------------------------------------------------

/*
--To fix issue 17548: ENH: All: Register Students Page is Taking Too Long to Populate Available Students 
This query is used to find if the student is currently registered in course

*/

    BEGIN

        SELECT  COUNT(*) AS Count
        FROM    arResults a
               ,arClassSections b
        WHERE   a.StuEnrollId = @StuEnrollId
                AND a.TestId IN ( SELECT    c.ClsSectionId
                                  FROM      arClassSections c
                                  WHERE     ReqId IN ( SELECT   ReqId
                                                       FROM     arClassSections
                                                       WHERE    ClsSectionId = @ClsSectID ) )
                AND a.TestID = b.ClsSectionID
                AND b.TermId IN ( SELECT    TermId
                                  FROM      arClassSections
                                  WHERE     ClsSectionId = @ClsSectID );

    END;



GO
