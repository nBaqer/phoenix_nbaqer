SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[USP_IPEDS_FallCompletionAllCompleters_GetList]
    @CampusId VARCHAR(50)
   ,@ProgId VARCHAR(4000) = NULL
   ,@StartDate DATETIME = NULL
   ,@EndDate DATETIME = NULL
   ,@DateRangeText VARCHAR(100) = NULL
   ,@OrderBy VARCHAR(100)
AS
    BEGIN


        CREATE TABLE #AllCompleters
            (
             RowNumber UNIQUEIDENTIFIER
            ,StudentId UNIQUEIDENTIFIER
            ,SSN VARCHAR(50)
            ,StudentNumber VARCHAR(50)
            ,StudentName VARCHAR(100)
            ,Men VARCHAR(1)
            ,Women VARCHAR(1)
            ,Race VARCHAR(100)
            ,Gender INT
            ,GenderSequence INT
            ,RaceSequence INT
            ,MenCount INT
            ,WomenCount INT
            );
		
        INSERT  INTO #AllCompleters
                SELECT  NEWID() AS RowNumber
                       ,StudentId
                       ,dbo.UDF_FormatSSN(SSN) AS SSN
                       ,StudentNumber
                       ,StudentName
                       ,Men
                       ,Women
                       ,Race
                       ,Gender
                       ,GenderSequence
                       ,RaceSequence
                       ,CASE Men
                          WHEN 'X' THEN 1
                          ELSE 0
                        END AS MenCount
                       ,CASE Women
                          WHEN 'X' THEN 1
                          ELSE 0
                        END AS WomenCount--,Null as EnrollmentId  -- EnrollmentId
                FROM    (
                          SELECT    NULL AS StudentId
                                   ,NULL AS SSN
                                   ,NULL AS StudentNumber
                                   ,NULL AS StudentName
                                   ,'' AS Men
                                   ,'' AS Women
                                   ,'Nonresident Alien' AS Race
                                   ,NULL AS Gender
                                   ,1 AS GenderSequence
                                   ,1 AS RaceSequence
                          UNION
                          SELECT    NULL AS StudentId
                                   ,NULL AS SSN
                                   ,NULL AS StudentNumber
                                   ,NULL AS StudentName
                                   ,'' AS Men
                                   ,'' AS Women
                                   ,AgencyDescrip AS Race
                                   ,NULL AS Gender
                                   ,1 AS GenderSequence
                                   ,CASE WHEN RptAgencyFldValId = 26 THEN 2
                                         WHEN RptAgencyFldValId = 27 THEN 3
                                         WHEN RptAgencyFldValId = 23 THEN 4
                                         WHEN RptAgencyFldValId = 24 THEN 5
                                         WHEN RptAgencyFldValId = 82 THEN 6
                                         WHEN RptAgencyFldValId = 25 THEN 7
                                         WHEN RptAgencyFldValId = 83 THEN 8
                                         WHEN RptAgencyFldValId = 28 THEN 9
                                    END AS RaceSequence
                          FROM      syRptAgencyFldValues
                          WHERE     RptAgencyFldId = 15
                                    AND RptAgencyFldValId <> 29
		-- select * from syRptAgencyFldValues			
                          UNION
                          SELECT DISTINCT
                                    t1.StudentId
                                   ,t1.SSN
                                   ,t1.StudentNumber
                                   ,t1.LastName + ', ' + t1.FirstName + ' ' + ISNULL(t1.MiddleName,'') AS StudentName
                                   ,CASE WHEN t3.IPEDSValue = 30 THEN 'X'
                                         ELSE ''
                                    END AS Men
                                   ,CASE WHEN t3.IPEDSValue = 31 THEN 'X'
                                         ELSE ''
                                    END AS Women
                                   ,'Nonresident Alien' AS Race
                                   ,t3.IPEDSValue AS Gender
                                   ,t3.IPEDSSequence AS GenderSequence
                                   ,1 AS RaceSequence
                          FROM      adGenders t3
                          LEFT JOIN arStudent t1 ON t3.GenderId = t1.Gender
                          LEFT JOIN adEthCodes t4 ON t4.EthCodeId = t1.Race
                          INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                          INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                          INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                                       AND t6.SysStatusId NOT IN ( 7,8 )
                          INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                          INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                          INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                          LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                          INNER JOIN adCitizenships t12 ON t1.Citizen = t12.CitizenshipId
                          WHERE     t2.CampusId = @CampusId
                                    AND (
                                          (
                                            t8.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
                                            AND @ProgId IS NULL
                                          )
                                          OR t8.ProgId IN ( SELECT  Val
                                                            FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                        )
                                    AND t6.SysStatusId IN ( 14 )
                                    AND (
                                          t3.IPEDSValue = 30
                                          OR t3.IPEDSValue = 31
                                        )
                                    AND t1.Race IS NOT NULL 
		-- and t4.IPEDSValue is not null
                                    AND t2.ExpGradDate >= @StartDate
                                    AND t2.ExpGradDate <= @EndDate
                                    AND t12.IPEDSValue = 65
                          UNION
                          SELECT DISTINCT
                                    t1.StudentId
                                   ,t1.SSN
                                   ,t1.StudentNumber
                                   ,t1.LastName + ', ' + t1.FirstName + ' ' + ISNULL(t1.MiddleName,'') AS StudentName
                                   ,CASE WHEN t3.IPEDSValue = 30 THEN 'X'
                                         ELSE ''
                                    END AS Men
                                   ,CASE WHEN t3.IPEDSValue = 31 THEN 'X'
                                         ELSE ''
                                    END AS Women
                                   ,CASE WHEN t4.IPEDSValue IS NULL THEN 'Race/ethnicity unknown'
                                         ELSE (
                                                SELECT DISTINCT
                                                        AgencyDescrip
                                                FROM    syRptAgencyFldValues
                                                WHERE   RptAgencyFldValId = t4.IPEDSValue
                                              )
                                    END AS Race
                                   ,t3.IPEDSValue AS Gender
                                   ,t3.IPEDSSequence AS GenderSequence
                                   ,CASE WHEN t4.IPEDSValue IS NULL THEN 9
                                         ELSE t4.IPEDSSequence
                                    END AS RaceSequence--, NULL as EnrollmentId -- ,t2.EnrollmentId
                          FROM      adGenders t3
                          LEFT JOIN arStudent t1 ON t3.GenderId = t1.Gender
                          LEFT JOIN adEthCodes t4 ON t4.EthCodeId = t1.Race
                          INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                          INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                          INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                                       AND t6.SysStatusId NOT IN ( 7,8 )
                          INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                          INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                          INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                          LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                          INNER JOIN adCitizenships t12 ON t1.Citizen = t12.CitizenshipId
                          WHERE     t2.CampusId = @CampusId
                                    AND (
                                          (
                                            t8.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
                                            AND @ProgId IS NULL
                                          )
                                          OR t8.ProgId IN ( SELECT  Val
                                                            FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                        )
                                    AND t6.SysStatusId IN ( 14 )
                                    AND (
                                          t3.IPEDSValue = 30
                                          OR t3.IPEDSValue = 31
                                        )
                                    AND t1.Race IS NOT NULL
                                    AND t4.IPEDSValue IS NOT NULL
                                    AND t2.ExpGradDate >= @StartDate
                                    AND t2.ExpGradDate <= @EndDate
                                    AND t12.IPEDSValue <> 65
                        ) dt; 
	
        SELECT  *
        FROM    (
                  SELECT    *
                           ,(
                              SELECT TOP 1
                                        EnrollmentId
                              FROM      arStuEnrollments C1
                                       ,arPrgVersions C2
                                       ,arProgTypes C3
                              WHERE     C1.PrgVerId = C2.PrgVerId
                                        AND C2.ProgTypId = C3.ProgTypId
                                        AND C1.StudentId = #AllCompleters.StudentId
                                        AND NOT C1.EnrollmentId IS NULL
                              ORDER BY  C1.StartDate
                                       ,C1.EnrollDate
                            ) AS EnrollmentId
                  FROM      #AllCompleters
                ) dt
        ORDER BY RaceSequence
               ,CASE WHEN @OrderBy = 'SSN' THEN SSN
                END
               ,CASE WHEN @OrderBy = 'LastName' THEN StudentName
                END
               ,CASE WHEN @OrderBy = 'student number' THEN StudentNumber
                END
               ,CASE WHEN @OrderBy = 'enrollmentid' THEN EnrollmentId
                END;


        DROP TABLE  #AllCompleters;

    END;


GO
