SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_GetClassSections]
    @campusId VARCHAR(50)
   ,@TermId VARCHAR(50) = '00000000-0000-0000-0000-000000000000'
   ,@ReqId VARCHAR(50) = '00000000-0000-0000-0000-000000000000'
   ,@InstrId VARCHAR(50) = '00000000-0000-0000-0000-000000000000'
   ,@ShiftId VARCHAR(50) = '00000000-0000-0000-0000-000000000000'
   ,@StartDate DATETIME = NULL
   ,@EndDate DATETIME = NULL
AS
    BEGIN
        SET NOCOUNT ON;  
        DECLARE @sql NVARCHAR(4000);  
  
        SELECT  a.ClsSectionId
               ,a.TermId
               ,a.ReqId
               ,a.InstructorId
               ,a.ShiftId
               ,a.ClsSection
               ,a.StartDate
               ,a.EndDate
               ,a.MaxStud
               ,b.TermDescrip
               ,c.Descrip
               ,c.Code
               ,'(' + Code + ') ' + Descrip AS CodeAndDescrip
        FROM    arClassSections a
        INNER JOIN arTerm b ON a.TermId = b.TermId
        INNER JOIN arReqs c ON a.ReqId = c.ReqId
        WHERE   a.CampusId = @campusId
                AND (
                      @TermId = '00000000-0000-0000-0000-000000000000'
                      OR a.TermId = @TermId
                    )
                AND (
                      @ReqId = '00000000-0000-0000-0000-000000000000'
                      OR a.ReqId = @ReqId
                    )
                AND (
                      @InstrId = '00000000-0000-0000-0000-000000000000'
                      OR a.InstructorId = @InstrId
                    )
                AND (
                      @ShiftId = '00000000-0000-0000-0000-000000000000'
                      OR a.ShiftId = @ShiftId
                    )
                AND (
                      (
                        @StartDate IS NULL
                        OR ( a.StartDate >= CONVERT(CHAR(10),@StartDate,101) )
                      )
                      AND (
                            @EndDate IS NULL
                            OR a.EndDate <= CONVERT(CHAR(10),@EndDate,101)
                          )
                    );
  
    END;



GO
