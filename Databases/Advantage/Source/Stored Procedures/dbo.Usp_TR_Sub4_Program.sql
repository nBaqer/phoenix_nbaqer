SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =========================================================================================================
-- Usp_TR_Sub4_Program
-- =========================================================================================================
CREATE PROCEDURE [dbo].[Usp_TR_Sub4_Program]
    @StuEnrollId UNIQUEIDENTIFIER  --VARCHAR(50)
AS
    BEGIN
        DECLARE @TermStartDate AS DATETIME; 
        DECLARE @GPAMethod AS VARCHAR(1000);
        DECLARE @GradesFormat AS VARCHAR(1000);
        DECLARE @TrackSapAttendance AS VARCHAR(1000);
        DECLARE @displayHours AS VARCHAR(1000); 
        DECLARE @MajorsMinorsConcentrations AS VARCHAR(1000);  
        DECLARE @StuEnrollCampusId AS UNIQUEIDENTIFIER;
        DECLARE @ActualPresentDays_ConvertTo_Hours AS DECIMAL(18,2);
        DECLARE @ActualAbsentDays_ConvertTo_Hours AS DECIMAL(18,2);
        DECLARE @CumAverage AS DECIMAL(18,2); 
        DECLARE @cumWeightedGPA AS DECIMAL(18,2);
        DECLARE @cumSimpleGPA AS DECIMAL(18,2);
    
        SET @TermStartDate = CONVERT(DATE,GETDATE(),120);

	--Refresh the Credit Summary Table Just in case any modifications were made
        EXECUTE Usp_Update_SyCreditsSummary @StuEnrollId;
        EXECUTE Usp_UpdateTransferCredits_SyCreditsSummary @StuEnrollId;

        SET @StuEnrollCampusId = COALESCE((
                                            SELECT  ASE.CampusId
                                            FROM    arStuEnrollments AS ASE
                                            WHERE   ASE.StuEnrollId = @StuEnrollId
                                          ),NULL);
                                       
        SET @GPAMethod = dbo.GetAppSettingValueByKeyName('GPAMethod',@StuEnrollCampusId);
        IF ( @GPAMethod IS NOT NULL )
            BEGIN
                SET @GPAMethod = LOWER(LTRIM(RTRIM(@GPAMethod)));
            END;

        SET @GradesFormat = dbo.GetAppSettingValueByKeyName('GradesFormat',@StuEnrollCampusId);
        IF ( @GradesFormat IS NOT NULL )
            BEGIN
                SET @GradesFormat = LOWER(LTRIM(RTRIM(@GradesFormat)));
            END;
        SET @TrackSapAttendance = dbo.GetAppSettingValueByKeyName('TrackSapAttendance',@StuEnrollCampusId);
        IF ( @TrackSapAttendance IS NOT NULL )
            BEGIN
                SET @TrackSapAttendance = LOWER(LTRIM(RTRIM(@TrackSapAttendance)));
            END;
        --
        SET @displayHours = dbo.GetAppSettingValueByKeyName('DisplayAttendanceUnitForProgressReportByClass',@StuEnrollCampusId);
        IF ( @displayHours IS NOT NULL )
            BEGIN
                SET @displayHours = LOWER(LTRIM(RTRIM(@displayHours)));
            END;
    
        SET @MajorsMinorsConcentrations = dbo.GetAppSettingValueByKeyName('MajorsMinorsConcentrations',@StuEnrollCampusId);    

    
        SET @ActualPresentDays_ConvertTo_Hours = (
                                                   SELECT   SUM(ActualPresentDays_ConvertTo_Hours)
                                                   FROM     syStudentAttendanceSummary
                                                   WHERE    StuEnrollId = @StuEnrollId
                                                 ); 
        SET @ActualAbsentDays_ConvertTo_Hours = (
                                                  SELECT    SUM(ActualAbsentDays_ConvertTo_Hours)
                                                  FROM      syStudentAttendanceSummary
                                                  WHERE     StuEnrollId = @StuEnrollId
                                                );


	

-- Calculus of GPA (include update syCreditSummary and syStudentAttendanceSummary)
        EXECUTE Usp_TR_Sub4_GetCumGPAandAverageforAGivenStuEnrollId @StuEnrollId,@CumAverage = @CumAverage OUTPUT,@cumWeightedGPA = @cumWeightedGPA OUTPUT,
            @cumSimpleGPA = @cumSimpleGPA OUTPUT;	
   
   /*
    SELECT  StuEnrollId
          , MAX(ActualRunningScheduledDays) AS ScheduledDays
          , MAX(AdjustedPresentDays) AS AttendedDays
          , MAX(AdjustedAbsentDays) AS AbsentDays
          , MAX(ActualRunningMakeupDays) AS MakeupDays
          , MAX(StudentAttendedDate) AS LDA
    INTO    #temp1
    FROM    syStudentAttendanceSummary
    WHERE   StuEnrollId = @StuEnrollId
    GROUP BY StuEnrollId
	*/

	 
        DECLARE @UnitTypeDescrip VARCHAR(50);

        SET @displayHours = dbo.GetAppSettingValueByKeyName('DisplayAttendanceUnitForProgressReportByClass',@StuEnrollCampusId);
        IF ( @displayHours IS NOT NULL )
            BEGIN
                SET @displayHours = LOWER(LTRIM(RTRIM(@displayHours)));
            END;

        SET @UnitTypeDescrip = (
                                 SELECT LTRIM(RTRIM(AAUT.UnitTypeDescrip))
                                 FROM   arAttUnitType AS AAUT
                                 INNER JOIN arPrgVersions AS APV ON APV.UnitTypeId = AAUT.UnitTypeId
                                 INNER JOIN arStuEnrollments AS ASE ON ASE.PrgVerId = APV.PrgVerId
                                 WHERE  ASE.StuEnrollId = @StuEnrollId
                               );

	
        DECLARE @MeetDate DATETIME
           ,@WeekDay VARCHAR(15)
           ,@StartDate DATETIME
           ,@EndDate DATETIME;
        DECLARE @PeriodDescrip VARCHAR(50)
           ,@Actual DECIMAL(18,2)
           ,@Excused DECIMAL(18,2)
           ,@ClsSectionId UNIQUEIDENTIFIER;
        DECLARE @Absent DECIMAL(18,2)
           ,@SchedHours DECIMAL(18,2)
           ,@TardyMinutes DECIMAL(18,2);
        DECLARE @tardy DECIMAL(18,2)
           ,@tracktardies INT
           ,@rownumber INT
           ,@IsTardy BIT
           ,@ActualRunningScheduledHours DECIMAL(18,2);
        DECLARE @ActualRunningPresentHours DECIMAL(18,2)
           ,@ActualRunningAbsentHours DECIMAL(18,2)
           ,@ActualRunningTardyHours DECIMAL(18,2)
           ,@ActualRunningMakeupHours DECIMAL(18,2);
        DECLARE @PrevStuEnrollId UNIQUEIDENTIFIER
           ,@intTardyBreakPoint INT
           ,@AdjustedRunningPresentHours DECIMAL(18,2)
           ,@AdjustedRunningAbsentHours DECIMAL(18,2)
           ,@ActualRunningScheduledDays DECIMAL(18,2);
		--declare @Scheduledhours decimal(18,2),@ActualPresentDays_ConvertTo_Hours decimal(18,2),@ActualAbsentDays_ConvertTo_Hours decimal(18,2),@AttendanceTrack varchar(50)
        DECLARE @TermDescrip VARCHAR(50)
           ,@PrgVerId UNIQUEIDENTIFIER;
        DECLARE @TardyHit VARCHAR(10)
           ,@TardiesMakingAbsence INT
           ,@ScheduledMinutes DECIMAL(18,2);
        DECLARE @Scheduledhours_noperiods DECIMAL(18,2)
           ,@ActualPresentDays_ConvertTo_Hours_NoPeriods DECIMAL(18,2)
           ,@ActualAbsentDays_ConvertTo_Hours_NoPeriods DECIMAL(18,2);
        DECLARE @ActualTardyDays_ConvertTo_Hours_NoPeriods DECIMAL(18,2);


        IF @UnitTypeDescrip IN ( 'none','present absent' )
            AND @TrackSapAttendance = 'byclass'
            BEGIN
			---- PRINT 'Step1!!!!'
                DELETE  FROM syStudentAttendanceSummary
                WHERE   StuEnrollId = @StuEnrollId;
                DECLARE @boolReset BIT
                   ,@MakeupHours DECIMAL(18,2)
                   ,@AdjustedPresentDaysComputed DECIMAL(18,2);
                DECLARE GetAttendance_Cursor CURSOR FORWARD_ONLY FAST_FORWARD
                FOR
                    SELECT  dt.StuEnrollId
                           ,dt.ClsSectionId
                           ,dt.MeetDate
                           ,dt.WeekDay
                           ,dt.StartDate
                           ,dt.EndDate
                           ,dt.PeriodDescrip
                           ,dt.Actual
                           ,dt.Excused
                           ,dt.Absent
                           ,dt.ScheduledMinutes
                           ,dt.TardyMinutes
                           ,dt.Tardy
                           ,dt.TrackTardies
                           ,dt.TardiesMakingAbsence
                           ,dt.PrgVerId
                           ,ROW_NUMBER() OVER ( ORDER BY MeetDate ) AS RowNumber
                    FROM    (
                              SELECT DISTINCT
                                        t1.StuEnrollId
                                       ,t1.ClsSectionId
                                       ,t1.MeetDate
                                       ,DATENAME(dw,t1.MeetDate) AS WeekDay
                                       ,t4.StartDate
                                       ,t4.EndDate
                                       ,t5.PeriodDescrip
                                       ,t1.Actual
                                       ,t1.Excused
                                       ,CASE WHEN (
                                                    t1.Actual = 0
                                                    AND t1.Excused = 0
                                                  ) THEN t1.Scheduled
                                             ELSE CASE WHEN (
                                                              t1.Actual <> 9999.00
                                                              AND t1.Actual < t1.Scheduled
														  --AND t1.Excused <> 1
                                                            ) THEN ( t1.Scheduled - t1.Actual )
                                                       ELSE 0
                                                  END
                                        END AS Absent
                                       ,t1.Scheduled AS ScheduledMinutes
                                       ,CASE WHEN (
                                                    t1.Actual > 0
                                                    AND t1.Actual < t1.Scheduled
                                                  ) THEN ( t1.Scheduled - t1.Actual )
                                             ELSE 0
                                        END AS TardyMinutes
                                       ,t1.Tardy AS Tardy
                                       ,t3.TrackTardies
                                       ,t3.TardiesMakingAbsence
                                       ,t3.PrgVerId
                              FROM      atClsSectAttendance t1
                              INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                              INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                              INNER JOIN arAttUnitType AS AAUT1 ON AAUT1.UnitTypeId = t3.UnitTypeId
                              INNER JOIN arClsSectMeetings t4 ON t1.ClsSectionId = t4.ClsSectionId
                                                                 AND (
                                                                       CONVERT(DATE,t1.MeetDate,111) >= CONVERT(DATE,t4.StartDate,111)
                                                                       AND CONVERT(DATE,t1.MeetDate,111) <= CONVERT(DATE,t4.EndDate,111)
                                                                     )
                                                                 AND t1.ClsSectMeetingId = t4.ClsSectMeetingId --DE8479 line added
                              INNER JOIN syPeriods t5 ON t4.PeriodId = t5.PeriodId
                              INNER JOIN cmTimeInterval t6 ON t5.StartTimeId = t6.TimeIntervalId
                              INNER JOIN cmTimeInterval t7 ON t5.EndTimeId = t7.TimeIntervalId
                              INNER JOIN dbo.arClassSections t9 ON t9.ClsSectionId = t1.ClsSectionId
                              INNER JOIN dbo.arReqs t10 ON t10.ReqId = t9.ReqId
                              INNER JOIN arAttUnitType AS AAUT2 ON AAUT2.UnitTypeId = t10.UnitTypeId
                              WHERE     t2.StuEnrollId = @StuEnrollId
                                        AND (
                                              AAUT1.UnitTypeDescrip IN ( 'None','Present Absent' )
                                              OR AAUT2.UnitTypeDescrip IN ( 'None','Present Absent' )
                                            )
                                        AND t1.Actual <> 9999
                            ) dt
                    ORDER BY StuEnrollId
                           ,MeetDate;
                OPEN GetAttendance_Cursor;
                FETCH NEXT FROM GetAttendance_Cursor INTO @StuEnrollId,@ClsSectionId,@MeetDate,@WeekDay,@StartDate,@EndDate,@PeriodDescrip,@Actual,@Excused,
                    @Absent,@ScheduledMinutes,@TardyMinutes,@tardy,@tracktardies,@TardiesMakingAbsence,@PrgVerId,@rownumber;
                SET @ActualRunningPresentHours = 0;
                SET @ActualRunningPresentHours = 0;
                SET @ActualRunningAbsentHours = 0;
                SET @ActualRunningTardyHours = 0;
                SET @ActualRunningMakeupHours = 0;
                SET @intTardyBreakPoint = 0;
                SET @AdjustedRunningPresentHours = 0;
                SET @AdjustedRunningAbsentHours = 0;
                SET @ActualRunningScheduledDays = 0;
                SET @boolReset = 0;
                SET @MakeupHours = 0;
                WHILE @@FETCH_STATUS = 0
                    BEGIN

                        IF @PrevStuEnrollId <> @StuEnrollId
                            BEGIN
                                SET @ActualRunningPresentHours = 0;
                                SET @ActualRunningAbsentHours = 0;
                                SET @intTardyBreakPoint = 0;
                                SET @ActualRunningTardyHours = 0;
                                SET @AdjustedRunningPresentHours = 0;
                                SET @AdjustedRunningAbsentHours = 0;
                                SET @ActualRunningScheduledDays = 0;
                                SET @MakeupHours = 0;
                                SET @boolReset = 1;
                            END;

					  
					  -- Scheduled and Actual: Calculate Students Actual and Adjusted Running Present Hours/Min/Day
                        IF @Actual <> 9999.00
                            BEGIN
					-- Commented by Balaji on 2.6.2015 as Excused Absence is still considered an absence
					-- @Excused is not added to Present Hours
                                SET @ActualRunningPresentHours = @ActualRunningPresentHours + @Actual;  -- + @Excused
                                SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours + @Actual; -- + @Excused
					
					-- If there are make up hrs deduct that otherwise it will be added again in progress report
                                IF (
                                     @Actual > 0
                                     AND @Actual > @ScheduledMinutes
                                     AND @Actual <> 9999.00
                                   )
                                    BEGIN
                                        SET @ActualRunningPresentHours = @ActualRunningPresentHours - ( @Actual - @ScheduledMinutes );
                                        SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - ( @Actual - @ScheduledMinutes );
                                    END;
					  
                                SET @ActualRunningScheduledDays = @ActualRunningScheduledDays + @ScheduledMinutes;                      
                            END;
				   
					-- Absent: Calculate Students Actual and Adjusted Running Absent Hours/Min/Day
                        SET @ActualRunningAbsentHours = @ActualRunningAbsentHours + @Absent;
                        SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Absent;	
			  
					-- Tardy: Calculate Students Actual Running Tardy Hours/Min/Day			
                        IF (
                             @Actual > 0
                             AND @Actual < @ScheduledMinutes
                             AND @tardy = 1
                           )
                            BEGIN
                                SET @ActualRunningTardyHours = @ActualRunningTardyHours + ( @ScheduledMinutes - @Actual );
                            END;
                        ELSE
                            IF (
                                 @Actual = 1
                                 AND @Actual = @ScheduledMinutes
                                 AND @tardy = 1
                               )
                                BEGIN
                                    SET @ActualRunningTardyHours += 1;
                                END;
							
					-- Track how many days student has been tardy only when 
					-- program version requires to track tardy
                        IF (
                             @tracktardies = 1
                             AND @tardy = 1
                           )
                            BEGIN
                                SET @intTardyBreakPoint = @intTardyBreakPoint + 1;
                            END;	    
		   
			
					-- Tardy: If student is tardy multiple times and when the number of days student is tardy matches
					-- the tardy rule set (for ex: 2 Tardies make 1 Absence). This rule indicates that
					-- when student is tardy the second time, that second occurance will be considered as
					-- absence
					-- Variable @intTardyBreakpoint tracks how many times the student was tardy
					-- Variable @tardiesMakingAbsence tracks the tardy rule
                        IF (
                             @tracktardies = 1
                             AND @TardiesMakingAbsence > 0
                             AND @intTardyBreakPoint = @TardiesMakingAbsence
                           )
                            BEGIN
                                SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - @Actual - @Excused;
                                SET @AdjustedRunningAbsentHours = ( @AdjustedRunningAbsentHours - @Absent ) + @ScheduledMinutes; --@TardyMinutes
                                SET @intTardyBreakPoint = 0;
                            END;
				   
				   -- Makeup: Calculate Makeup Hours when Student Attended more than scheduled hours          	       
                        IF (
                             @Actual > 0
                             AND @Actual > @ScheduledMinutes
                             AND @Actual <> 9999.00
                           )
                            BEGIN
                                SET @MakeupHours = @MakeupHours + ( @Actual - @ScheduledMinutes );
                            END;
			  
                        IF ( @tracktardies = 1 )
                            BEGIN
                                SET @AdjustedPresentDaysComputed = @AdjustedRunningPresentHours; --+IsNULL(@ActualRunningTardyHours,0)
                            END;
                        ELSE
                            BEGIN
                                SET @AdjustedPresentDaysComputed = @AdjustedRunningPresentHours;
                            END;
				
				---- PRINT @MeetDate
				---- PRINT @ActualRunningAbsentHours
				
                        DELETE  FROM syStudentAttendanceSummary
                        WHERE   StuEnrollId = @StuEnrollId
                                AND ClsSectionId = @ClsSectionId
                                AND StudentAttendedDate = @MeetDate;
                        INSERT  INTO syStudentAttendanceSummary
                                (
                                 StuEnrollId
                                ,ClsSectionId
                                ,StudentAttendedDate
                                ,ScheduledDays
                                ,ActualDays
                                ,ActualRunningScheduledDays
                                ,ActualRunningPresentDays
                                ,ActualRunningAbsentDays
                                ,ActualRunningMakeupDays
                                ,ActualRunningTardyDays
                                ,AdjustedPresentDays
                                ,AdjustedAbsentDays
                                ,AttendanceTrackType
                                ,ModUser
                                ,ModDate
                                ,tardiesmakingabsence
							    )
                        VALUES  (
                                 @StuEnrollId
                                ,@ClsSectionId
                                ,@MeetDate
                                ,@ScheduledMinutes
                                ,@Actual
                                ,@ActualRunningScheduledDays
                                ,@ActualRunningPresentHours
                                ,@ActualRunningAbsentHours
                                ,ISNULL(@MakeupHours,0)
                                ,@ActualRunningTardyHours
                                ,@AdjustedPresentDaysComputed
                                ,@AdjustedRunningAbsentHours
                                ,'Post Attendance by Class Min'
                                ,'sa'
                                ,GETDATE()
                                ,@TardiesMakingAbsence
							    );

				--update syStudentAttendanceSummary set TardiesMakingAbsence=@TardiesMakingAbsence where StuEnrollId=@StuEnrollId
                        SET @PrevStuEnrollId = @StuEnrollId; 

                        FETCH NEXT FROM GetAttendance_Cursor INTO @StuEnrollId,@ClsSectionId,@MeetDate,@WeekDay,@StartDate,@EndDate,@PeriodDescrip,@Actual,
                            @Excused,@Absent,@ScheduledMinutes,@TardyMinutes,@tardy,@tracktardies,@TardiesMakingAbsence,@PrgVerId,@rownumber;
                    END;
                CLOSE GetAttendance_Cursor;
                DEALLOCATE GetAttendance_Cursor;
			
                DECLARE @MyTardyTable TABLE
                    (
                     ClsSectionId UNIQUEIDENTIFIER
                    ,TardiesMakingAbsence INT
                    ,AbsentHours DECIMAL(18,2)
                    ); 

                INSERT  INTO @MyTardyTable
                        SELECT  ClsSectionId
                               ,PV.TardiesMakingAbsence
                               ,CASE WHEN ( COUNT(*) >= PV.TardiesMakingAbsence ) THEN ( COUNT(*) / PV.TardiesMakingAbsence )
                                     ELSE 0
                                END AS AbsentHours 
--Count(*) as NumberofTimesTardy 
                        FROM    syStudentAttendanceSummary SAS
                        INNER JOIN arStuEnrollments SE ON SAS.StuEnrollId = SE.StuEnrollId
                        INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                        WHERE   SE.StuEnrollId = @StuEnrollId
                                AND SAS.IsTardy = 1
                                AND PV.TrackTardies = 1
                        GROUP BY ClsSectionId
                               ,PV.TardiesMakingAbsence; 

--Drop table @MyTardyTable
                DECLARE @TotalTardyAbsentDays DECIMAL(18,2);
                SET @TotalTardyAbsentDays = (
                                              SELECT    ISNULL(SUM(AbsentHours),0)
                                              FROM      @MyTardyTable
                                            );

--Print @TotalTardyAbsentDays

                UPDATE  syStudentAttendanceSummary
                SET     AdjustedPresentDays = AdjustedPresentDays - @TotalTardyAbsentDays
                       ,AdjustedAbsentDays = AdjustedAbsentDays + @TotalTardyAbsentDays
                WHERE   StuEnrollId = @StuEnrollId
                        AND StudentAttendedDate = (
                                                    SELECT TOP 1
                                                            StudentAttendedDate
                                                    FROM    syStudentAttendanceSummary
                                                    WHERE   StuEnrollId = @StuEnrollId
                                                    ORDER BY StudentAttendedDate DESC
                                                  );
			
            END;	
		
		-- By Class and Attendance Unit Type - Minutes and Clock Hour
		
        IF @UnitTypeDescrip IN ( 'minutes','clock hours' )
            AND @TrackSapAttendance = 'byclass'
            BEGIN
                DELETE  FROM syStudentAttendanceSummary
                WHERE   StuEnrollId = @StuEnrollId;
				
                DECLARE GetAttendance_Cursor CURSOR
                FOR
                    SELECT  *
                           ,ROW_NUMBER() OVER ( ORDER BY MeetDate ) AS RowNumber
                    FROM    (
                              SELECT DISTINCT
                                        t1.StuEnrollId
                                       ,t1.ClsSectionId
                                       ,t1.MeetDate
                                       ,DATENAME(dw,t1.MeetDate) AS WeekDay
                                       ,t4.StartDate
                                       ,t4.EndDate
                                       ,t5.PeriodDescrip
                                       ,t1.Actual
                                       ,t1.Excused
                                       ,CASE WHEN (
                                                    t1.Actual = 0
                                                    AND t1.Excused = 0
                                                  ) THEN t1.Scheduled
                                             ELSE CASE WHEN (
                                                              t1.Actual <> 9999.00
                                                              AND t1.Actual < t1.Scheduled
                                                            ) THEN ( t1.Scheduled - t1.Actual )
                                                       ELSE 0
                                                  END
                                        END AS Absent
                                       ,t1.Scheduled AS ScheduledMinutes
                                       ,CASE WHEN (
                                                    t1.Actual > 0
                                                    AND t1.Actual < t1.Scheduled
                                                  ) THEN ( t1.Scheduled - t1.Actual )
                                             ELSE 0
                                        END AS TardyMinutes
                                       ,t1.Tardy AS Tardy
                                       ,t3.TrackTardies
                                       ,t3.TardiesMakingAbsence
                                       ,t3.PrgVerId
                              FROM      atClsSectAttendance t1
                              INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                              INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                              INNER JOIN arAttUnitType AS AAUT1 ON AAUT1.UnitTypeId = t3.UnitTypeId
                              INNER JOIN arClsSectMeetings t4 ON t1.ClsSectionId = t4.ClsSectionId
                                                                 AND (
                                                                       CONVERT(DATE,t1.MeetDate,111) >= CONVERT(DATE,t4.StartDate,111)
                                                                       AND CONVERT(DATE,t1.MeetDate,111) <= CONVERT(DATE,t4.EndDate,111)
                                                                     )
                                                                 AND t1.ClsSectMeetingId = t4.ClsSectMeetingId --DE8479 line added
                              INNER JOIN syPeriods t5 ON t4.PeriodId = t5.PeriodId
                              INNER JOIN cmTimeInterval t6 ON t5.StartTimeId = t6.TimeIntervalId
                              INNER JOIN cmTimeInterval t7 ON t5.EndTimeId = t7.TimeIntervalId
                              INNER JOIN dbo.arClassSections t9 ON t9.ClsSectionId = t1.ClsSectionId
                              INNER JOIN dbo.arReqs t10 ON t10.ReqId = t9.ReqId
                              INNER JOIN arAttUnitType AS AAUT2 ON AAUT2.UnitTypeId = t10.UnitTypeId
                              WHERE     t2.StuEnrollId = @StuEnrollId
                                        AND (
                                              AAUT1.UnitTypeDescrip IN ( 'Minutes','Clock Hours' )
                                              OR AAUT2.UnitTypeDescrip IN ( 'Minutes','Clock Hours' )
                                            )
                                        AND t1.Actual <> 9999
                              UNION
                              SELECT DISTINCT
                                        t1.StuEnrollId
                                       ,NULL AS ClsSectionId
                                       ,t1.RecordDate AS MeetDate
                                       ,DATENAME(dw,t1.RecordDate) AS WeekDay
                                       ,NULL AS StartDate
                                       ,NULL AS EndDate
                                       ,NULL AS PeriodDescrip
                                       ,t1.ActualHours
                                       ,NULL AS Excused
                                       ,CASE WHEN ( t1.ActualHours = 0 ) THEN t1.SchedHours
                                             ELSE 0
								 --ELSE 
									--Case when (t1.ActualHours <> 9999.00 and t1.ActualHours < t1.SchedHours)
									--		THEN (t1.SchedHours - t1.ActualHours)
									--		ELSE 
									--			0
									--		End
                                        END AS Absent
                                       ,t1.SchedHours AS ScheduledMinutes
                                       ,CASE WHEN (
                                                    t1.ActualHours <> 9999.00
                                                    AND t1.ActualHours > 0
                                                    AND t1.ActualHours < t1.SchedHours
                                                  ) THEN ( t1.SchedHours - t1.ActualHours )
                                             ELSE 0
                                        END AS TardyMinutes
                                       ,NULL AS Tardy
                                       ,t3.TrackTardies
                                       ,t3.TardiesMakingAbsence
                                       ,t3.PrgVerId
                              FROM      arStudentClockAttendance t1
                              INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                              INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                              WHERE     t2.StuEnrollId = @StuEnrollId
                                        AND t1.Converted = 1
                                        AND t1.ActualHours <> 9999
                            ) dt
                    ORDER BY StuEnrollId
                           ,MeetDate;
                OPEN GetAttendance_Cursor;
                FETCH NEXT FROM GetAttendance_Cursor INTO @StuEnrollId,@ClsSectionId,@MeetDate,@WeekDay,@StartDate,@EndDate,@PeriodDescrip,@Actual,@Excused,
                    @Absent,@ScheduledMinutes,@TardyMinutes,@tardy,@tracktardies,@TardiesMakingAbsence,@PrgVerId,@rownumber;
                SET @ActualRunningPresentHours = 0;
                SET @ActualRunningPresentHours = 0;
                SET @ActualRunningAbsentHours = 0;
                SET @ActualRunningTardyHours = 0;
                SET @ActualRunningMakeupHours = 0;
                SET @intTardyBreakPoint = 0;
                SET @AdjustedRunningPresentHours = 0;
                SET @AdjustedRunningAbsentHours = 0;
                SET @ActualRunningScheduledDays = 0;
                SET @boolReset = 0;
                SET @MakeupHours = 0;
                WHILE @@FETCH_STATUS = 0
                    BEGIN

                        IF @PrevStuEnrollId <> @StuEnrollId
                            BEGIN
                                SET @ActualRunningPresentHours = 0;
                                SET @ActualRunningAbsentHours = 0;
                                SET @intTardyBreakPoint = 0;
                                SET @ActualRunningTardyHours = 0;
                                SET @AdjustedRunningPresentHours = 0;
                                SET @AdjustedRunningAbsentHours = 0;
                                SET @ActualRunningScheduledDays = 0;
                                SET @MakeupHours = 0;
                                SET @boolReset = 1;
                            END;

					  
			-- Scheduled and Actual: Calculate Students Actual and Adjusted Running Present Hours/Min/Day
                        IF @Actual <> 9999.00
                            BEGIN
                                SET @ActualRunningPresentHours = @ActualRunningPresentHours + @Actual;
                                SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours + @Actual; 
					-- If there are make up hrs deduct that otherwise it will be added again in progress report
                                IF (
                                     @Actual > 0
                                     AND @Actual > @ScheduledMinutes
                                     AND @Actual <> 9999.00
                                   )
                                    BEGIN
                                        SET @ActualRunningPresentHours = @ActualRunningPresentHours - ( @Actual - @ScheduledMinutes );
                                        SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - ( @Actual - @ScheduledMinutes );
                                    END; 
                                SET @ActualRunningScheduledDays = @ActualRunningScheduledDays + @ScheduledMinutes;                      
                            END;
		   
			-- Absent: Calculate Students Actual and Adjusted Running Absent Hours/Min/Day
                        SET @ActualRunningAbsentHours = @ActualRunningAbsentHours + @Absent;
                        SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Absent;	
	  
			-- Tardy: Calculate Students Actual Running Tardy Hours/Min/Day			
                        IF (
                             @Actual > 0
                             AND @Actual < @ScheduledMinutes
                             AND @tardy = 1
                           )
                            BEGIN
                                SET @ActualRunningTardyHours = @ActualRunningTardyHours + ( @ScheduledMinutes - @Actual );
                            END;
					
			-- Track how many days student has been tardy only when 
			-- program version requires to track tardy
                        IF (
                             @tracktardies = 1
                             AND @tardy = 1
                           )
                            BEGIN
                                SET @intTardyBreakPoint = @intTardyBreakPoint + 1;
                            END;	    
		   
			
			-- Tardy: If student is tardy multiple times and when the number of days student is tardy matches
			-- the tardy rule set (for ex: 2 Tardies make 1 Absence). This rule indicates that
			-- when student is tardy the second time, that second occurance will be considered as
			-- absence
			-- Variable @intTardyBreakpoint tracks how many times the student was tardy
			-- Variable @tardiesMakingAbsence tracks the tardy rule
                        IF (
                             @tracktardies = 1
                             AND @TardiesMakingAbsence > 0
                             AND @intTardyBreakPoint = @TardiesMakingAbsence
                           )
                            BEGIN
                                SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - @Actual;
                                SET @AdjustedRunningAbsentHours = ( @AdjustedRunningAbsentHours - @Absent ) + @ScheduledMinutes; --@TardyMinutes
                                SET @intTardyBreakPoint = 0;
                            END;
		   
		   -- Makeup: Calculate Makeup Hours when Student Attended more than scheduled hours          	       
                        IF (
                             @Actual > 0
                             AND @Actual > @ScheduledMinutes
                             AND @Actual <> 9999.00
                           )
                            BEGIN
                                SET @MakeupHours = @MakeupHours + ( @Actual - @ScheduledMinutes );
                            END;
	  
	 
                        IF ( @tracktardies = 1 )
                            BEGIN
                                SET @AdjustedPresentDaysComputed = @AdjustedRunningPresentHours; --+IsNULL(@ActualRunningTardyHours,0)
                            END;
                        ELSE
                            BEGIN
                                SET @AdjustedPresentDaysComputed = @AdjustedRunningPresentHours;
                            END;
		
		
		
		
                        DELETE  FROM syStudentAttendanceSummary
                        WHERE   StuEnrollId = @StuEnrollId
                                AND ClsSectionId = @ClsSectionId
                                AND StudentAttendedDate = @MeetDate;
                        INSERT  INTO syStudentAttendanceSummary
                                (
                                 StuEnrollId
                                ,ClsSectionId
                                ,StudentAttendedDate
                                ,ScheduledDays
                                ,ActualDays
                                ,ActualRunningScheduledDays
                                ,ActualRunningPresentDays
                                ,ActualRunningAbsentDays
                                ,ActualRunningMakeupDays
                                ,ActualRunningTardyDays
                                ,AdjustedPresentDays
                                ,AdjustedAbsentDays
                                ,AttendanceTrackType
                                ,ModUser
                                ,ModDate
							    )
                        VALUES  (
                                 @StuEnrollId
                                ,@ClsSectionId
                                ,@MeetDate
                                ,@ScheduledMinutes
                                ,@Actual
                                ,@ActualRunningScheduledDays
                                ,@ActualRunningPresentHours
                                ,@ActualRunningAbsentHours
                                ,ISNULL(@MakeupHours,0)
                                ,@ActualRunningTardyHours
                                ,@AdjustedPresentDaysComputed
                                ,@AdjustedRunningAbsentHours
                                ,'Post Attendance by Class Min'
                                ,'sa'
                                ,GETDATE()
                                );

                        UPDATE  syStudentAttendanceSummary
                        SET     tardiesmakingabsence = @TardiesMakingAbsence
                        WHERE   StuEnrollId = @StuEnrollId;
                        SET @PrevStuEnrollId = @StuEnrollId; 

                        FETCH NEXT FROM GetAttendance_Cursor INTO @StuEnrollId,@ClsSectionId,@MeetDate,@WeekDay,@StartDate,@EndDate,@PeriodDescrip,@Actual,
                            @Excused,@Absent,@ScheduledMinutes,@TardyMinutes,@tardy,@tracktardies,@TardiesMakingAbsence,@PrgVerId,@rownumber;
                    END;
                CLOSE GetAttendance_Cursor;
                DEALLOCATE GetAttendance_Cursor;
            END;
--Select * from arAttUnitType

			
-- By Minutes/Day
-- remove clock hour from here
        IF @UnitTypeDescrip IN ( 'minutes' )
            AND @TrackSapAttendance = 'byday'
	-- -- PRINT GETDATE();	
	---- PRINT @UnitTypeId;
	---- PRINT @TrackSapAttendance;
            BEGIN
			--Delete from syStudentAttendanceSummary where StuEnrollId=@StuEnrollId

                DECLARE GetAttendance_Cursor CURSOR FORWARD_ONLY FAST_FORWARD
                FOR
                    SELECT  t1.StuEnrollId
                           ,NULL AS ClsSectionId
                           ,t1.RecordDate AS MeetDate
                           ,t1.ActualHours
                           ,t1.SchedHours AS ScheduledMinutes
                           ,CASE WHEN (
                                        (
                                          t1.SchedHours >= 1
                                          AND t1.SchedHours NOT IN ( 999,9999 )
                                        )
                                        AND t1.ActualHours = 0
                                      ) THEN t1.SchedHours
                                 ELSE 0
                            END AS Absent
                           ,t1.isTardy
                           ,(
                              SELECT    ISNULL(SUM(SchedHours),0)
                              FROM      arStudentClockAttendance
                              WHERE     StuEnrollId = t1.StuEnrollId
                                        AND RecordDate <= t1.RecordDate
                                        AND (
                                              t1.SchedHours >= 1
                                              AND t1.SchedHours NOT IN ( 999,9999 )
                                              AND t1.ActualHours NOT IN ( 999,9999 )
                                            )
                            ) AS ActualRunningScheduledHours
                           ,(
                              SELECT    SUM(ActualHours)
                              FROM      arStudentClockAttendance
                              WHERE     StuEnrollId = t1.StuEnrollId
                                        AND RecordDate <= t1.RecordDate
                                        AND (
                                              t1.SchedHours >= 1
                                              AND t1.SchedHours NOT IN ( 999,9999 )
                                            )
                                        AND ActualHours >= 1
                                        AND ActualHours NOT IN ( 999,9999 )
                            ) AS ActualRunningPresentHours
                           ,(
                              SELECT    COUNT(ActualHours)
                              FROM      arStudentClockAttendance
                              WHERE     StuEnrollId = t1.StuEnrollId
                                        AND RecordDate <= t1.RecordDate
                                        AND (
                                              t1.SchedHours >= 1
                                              AND t1.SchedHours NOT IN ( 999,9999 )
                                            )
                                        AND ActualHours = 0
                                        AND ActualHours NOT IN ( 999,9999 )
                            ) AS ActualRunningAbsentHours
                           ,(
                              SELECT    SUM(ActualHours)
                              FROM      arStudentClockAttendance
                              WHERE     StuEnrollId = t1.StuEnrollId
                                        AND RecordDate <= t1.RecordDate
                                        AND SchedHours = 0
                                        AND ActualHours >= 1
                                        AND ActualHours NOT IN ( 999,9999 )
                            ) AS ActualRunningMakeupHours
                           ,(
                              SELECT    SUM(SchedHours - ActualHours)
                              FROM      arStudentClockAttendance
                              WHERE     StuEnrollId = t1.StuEnrollId
                                        AND RecordDate <= t1.RecordDate
                                        AND (
                                              (
                                                t1.SchedHours >= 1
                                                AND t1.SchedHours NOT IN ( 999,9999 )
                                              )
                                              AND ActualHours >= 1
                                              AND ActualHours NOT IN ( 999,9999 )
                                            )
                                        AND isTardy = 1
                            ) AS ActualRunningTardyHours
                           ,t3.TrackTardies
                           ,t3.TardiesMakingAbsence
                           ,t3.PrgVerId
                           ,ROW_NUMBER() OVER ( ORDER BY t1.RecordDate ) AS RowNumber
                    FROM    arStudentClockAttendance t1
                    INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                    INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                    INNER JOIN arAttUnitType AS AAUT1 ON AAUT1.UnitTypeId = t3.UnitTypeId
			--inner join Inserted t4 on t1.StuEnrollId = t4.StuEnrollId and t1.RecordDate=t4.RecordDate
                    WHERE   AAUT1.UnitTypeDescrip IN ( 'Minutes' )
                            AND t2.StuEnrollId = @StuEnrollId
                            AND t1.ActualHours <> 9999.00
                    ORDER BY t1.StuEnrollId
                           ,MeetDate;
                OPEN GetAttendance_Cursor;
--Declare @ActualHours decimal(18,2),@SchedHours decimal(18,2)
                FETCH NEXT FROM GetAttendance_Cursor INTO @StuEnrollId,@ClsSectionId,@MeetDate,@Actual,@ScheduledMinutes,@Absent,@IsTardy,
                    @ActualRunningScheduledHours,@ActualRunningPresentHours,@ActualRunningAbsentHours,@ActualRunningMakeupHours,@ActualRunningTardyHours,
                    @tracktardies,@TardiesMakingAbsence,@PrgVerId,@rownumber;

                SET @ActualRunningPresentHours = 0;
                SET @ActualRunningPresentHours = 0;
                SET @ActualRunningAbsentHours = 0;
                SET @ActualRunningTardyHours = 0;
                SET @ActualRunningMakeupHours = 0;
                SET @intTardyBreakPoint = 0;
                SET @AdjustedRunningPresentHours = 0;
                SET @AdjustedRunningAbsentHours = 0;
                SET @ActualRunningScheduledDays = 0;
                WHILE @@FETCH_STATUS = 0
                    BEGIN

                        IF @PrevStuEnrollId <> @StuEnrollId
                            BEGIN
                                SET @ActualRunningPresentHours = 0;
                                SET @ActualRunningAbsentHours = 0;
                                SET @intTardyBreakPoint = 0;
                                SET @ActualRunningTardyHours = 0;
                                SET @AdjustedRunningPresentHours = 0;
                                SET @AdjustedRunningAbsentHours = 0;
                                SET @ActualRunningScheduledDays = 0;
                            END;

                        IF (
                             @ScheduledMinutes >= 1
                             AND (
                                   @Actual <> 9999
                                   AND @Actual <> 999
                                 )
                             AND (
                                   @ScheduledMinutes <> 9999
                                   AND @Actual <> 999
                                 )
                           )
                            BEGIN
                                SET @ActualRunningScheduledDays = ISNULL(@ActualRunningScheduledDays,0) + ISNULL(@ScheduledMinutes,0);
                            END;
                        ELSE
                            BEGIN
                                SET @ActualRunningScheduledDays = ISNULL(@ActualRunningScheduledDays,0); 
                            END;
	   
                        IF (
                             @Actual <> 9999
                             AND @Actual <> 999
                           )
                            BEGIN
                                SET @ActualRunningPresentHours = @ActualRunningPresentHours + @Actual;
                            END;
                        SET @ActualRunningAbsentHours = @ActualRunningAbsentHours + @Absent;

                        IF (
                             @Actual > 0
                             AND @Actual < @ScheduledMinutes
                           )
                            BEGIN
                                SET @ActualRunningAbsentHours = @ActualRunningAbsentHours + ( @ScheduledMinutes - @Actual );
                            END;
			-- Make up hours
		--sched=5, Actual =7, makeup= 2,ab = 0
		--sched=0, Actual =7, makeup= 7,ab = 0
                        IF (
                             @Actual > 0
                             AND @ScheduledMinutes > 0
                             AND @Actual > @ScheduledMinutes
                             AND (
                                   @Actual <> 9999
                                   AND @Actual <> 999
                                 )
                           )
                            BEGIN
                                SET @ActualRunningMakeupHours = @ActualRunningMakeupHours + ( @Actual - @ScheduledMinutes );
                            END;

		
                        IF (
                             @Actual <> 9999
                             AND @Actual <> 999
                           )
                            BEGIN
                                SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours + @Actual;
                            END;	
                        IF (
                             @Actual = 0
                             AND @ScheduledMinutes >= 1
                             AND @ScheduledMinutes NOT IN ( 999,9999 )
                           )
                            BEGIN
                                SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Absent;
                            END;
		-- Absent hours
		--1. sched = 5, Actual = 2 then Ab = (5-3) = 2
                        IF (
                             @Absent = 0
                             AND @ActualRunningAbsentHours > 0
                             AND ( @Actual < @ScheduledMinutes )
                           )
                            BEGIN
                                SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + ( @ScheduledMinutes - @Actual );	
                            END;
                        IF (
                             @Actual > 0
                             AND @Actual < @ScheduledMinutes
                             AND (
                                   @Actual <> 9999.00
                                   AND @Actual <> 999.00
                                 )
                           )
                            BEGIN
                                SET @ActualRunningTardyHours = @ActualRunningTardyHours + ( @ScheduledMinutes - @Actual );
                            END;
	
                        IF @tracktardies = 1
                            AND (
                                  @TardyMinutes > 0
                                  OR @IsTardy = 1
                                )
                            BEGIN
                                SET @intTardyBreakPoint = @intTardyBreakPoint + 1;
                            END;	    
                        IF (
                             @tracktardies = 1
                             AND @TardiesMakingAbsence > 0
                             AND @intTardyBreakPoint = @TardiesMakingAbsence
                           )
                            BEGIN
                                SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - @Actual;
                                SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Actual; --@TardyMinutes
                                SET @intTardyBreakPoint = 0;
                            END;
                        DELETE  FROM syStudentAttendanceSummary
                        WHERE   StuEnrollId = @StuEnrollId
                                AND StudentAttendedDate = @MeetDate;
                        INSERT  INTO syStudentAttendanceSummary
                                (
                                 StuEnrollId
                                ,ClsSectionId
                                ,StudentAttendedDate
                                ,ScheduledDays
                                ,ActualDays
                                ,ActualRunningScheduledDays
                                ,ActualRunningPresentDays
                                ,ActualRunningAbsentDays
                                ,ActualRunningMakeupDays
                                ,ActualRunningTardyDays
                                ,AdjustedPresentDays
                                ,AdjustedAbsentDays
                                ,AttendanceTrackType
                                ,ModUser
                                ,ModDate
							    )
                        VALUES  (
                                 @StuEnrollId
                                ,@ClsSectionId
                                ,@MeetDate
                                ,@ScheduledMinutes
                                ,@Actual
                                ,@ActualRunningScheduledDays
                                ,@ActualRunningPresentHours
                                ,@ActualRunningAbsentHours
                                ,@ActualRunningMakeupHours
                                ,@ActualRunningTardyHours
                                ,@AdjustedRunningPresentHours
                                ,@AdjustedRunningAbsentHours
                                ,'Post Attendance by Class'
                                ,'sa'
                                ,GETDATE()
                                );
				
                        UPDATE  syStudentAttendanceSummary
                        SET     tardiesmakingabsence = @TardiesMakingAbsence
                        WHERE   StuEnrollId = @StuEnrollId;
			--end
                        SET @PrevStuEnrollId = @StuEnrollId; 
                        FETCH NEXT FROM GetAttendance_Cursor INTO @StuEnrollId,@ClsSectionId,@MeetDate,@Actual,@ScheduledMinutes,@Absent,@IsTardy,
                            @ActualRunningScheduledHours,@ActualRunningPresentHours,@ActualRunningAbsentHours,@ActualRunningMakeupHours,@ActualRunningTardyHours,
                            @tracktardies,@TardiesMakingAbsence,@PrgVerId,@rownumber;
                    END;
                CLOSE GetAttendance_Cursor;
                DEALLOCATE GetAttendance_Cursor;
            END;

	---- PRINT 'Does it get to Clock Hour/PA';
	---- PRINT @UnitTypeId;
	---- PRINT @TrackSapAttendance;
	-- By Day and PA, Clock Hour
        IF @UnitTypeDescrip IN ( 'present absent','clock hours' )
            AND @TrackSapAttendance = 'byday'
            BEGIN
			---- PRINT 'By Day inside day';
                DECLARE GetAttendance_Cursor CURSOR
                FOR
                    SELECT  t1.StuEnrollId
                           ,t1.RecordDate
                           ,t1.ActualHours
                           ,t1.SchedHours
                           ,CASE WHEN (
                                        (
                                          t1.SchedHours >= 1
                                          AND t1.SchedHours NOT IN ( 999,9999 )
                                        )
                                        AND t1.ActualHours = 0
                                      ) THEN t1.SchedHours
                                 ELSE 0
                            END AS Absent
                           ,t1.isTardy
                           ,t3.TrackTardies
                           ,t3.TardiesMakingAbsence
                    FROM    arStudentClockAttendance t1
                    INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                    INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                    INNER JOIN arAttUnitType AS AAUT1 ON AAUT1.UnitTypeId = t3.UnitTypeId
			--inner join Inserted t4 on t1.StuEnrollId = t4.StuEnrollId and t1.RecordDate=t4.RecordDate
                    WHERE   -- Unit Types: Present Absent and Clock Hour
                            AAUT1.UnitTypeDescrip IN ( 'present absent','clock hours' )
                            AND ActualHours <> 9999.00
                            AND t2.StuEnrollId = @StuEnrollId
                    ORDER BY t1.StuEnrollId
                           ,t1.RecordDate;
                OPEN GetAttendance_Cursor;
--Declare @ActualHours decimal(18,2),@SchedHours decimal(18,2)
                FETCH NEXT FROM GetAttendance_Cursor INTO @StuEnrollId,@MeetDate,@Actual,@ScheduledMinutes,@Absent,@IsTardy,@tracktardies,@TardiesMakingAbsence;

                SET @ActualRunningPresentHours = 0;
                SET @ActualRunningPresentHours = 0;
                SET @ActualRunningAbsentHours = 0;
                SET @ActualRunningTardyHours = 0;
                SET @ActualRunningMakeupHours = 0;
                SET @intTardyBreakPoint = 0;
                SET @AdjustedRunningPresentHours = 0;
                SET @AdjustedRunningAbsentHours = 0;
                SET @ActualRunningScheduledDays = 0;
                SET @MakeupHours = 0;
                WHILE @@FETCH_STATUS = 0
                    BEGIN

                        IF @PrevStuEnrollId <> @StuEnrollId
                            BEGIN
                                SET @ActualRunningPresentHours = 0;
                                SET @ActualRunningAbsentHours = 0;
                                SET @intTardyBreakPoint = 0;
                                SET @ActualRunningTardyHours = 0;
                                SET @AdjustedRunningPresentHours = 0;
                                SET @AdjustedRunningAbsentHours = 0;
                                SET @ActualRunningScheduledDays = 0;
                                SET @MakeupHours = 0;
                            END;
	   
                        IF (
                             @Actual <> 9999
                             AND @Actual <> 999
                           )
                            BEGIN
                                SET @ActualRunningScheduledDays = ISNULL(@ActualRunningScheduledDays,0) + @ScheduledMinutes;
                                SET @ActualRunningPresentHours = ISNULL(@ActualRunningPresentHours,0) + @Actual;
                                SET @AdjustedRunningPresentHours = ISNULL(@AdjustedRunningPresentHours,0) + @Actual;
                            END;
                        SET @ActualRunningAbsentHours = ISNULL(@ActualRunningAbsentHours,0) + @Absent;
                        IF (
                             @Actual = 0
                             AND @ScheduledMinutes >= 1
                             AND @ScheduledMinutes NOT IN ( 999,9999 )
                           )
                            BEGIN
                                SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Absent;
                            END;
		
		-- NWH 
		-- If Scheduled = 3.0 hrs and Actual = 1.0 Then 2 hrs is considered absent
                        IF (
                             @ScheduledMinutes >= 1
                             AND @ScheduledMinutes NOT IN ( 999,9999 )
                             AND @Actual > 0
                             AND ( @Actual < @ScheduledMinutes )
                           )
                            BEGIN
                                SET @ActualRunningAbsentHours = ISNULL(@ActualRunningAbsentHours,0) + ( @ScheduledMinutes - @Actual );
                                SET @AdjustedRunningAbsentHours = ISNULL(@AdjustedRunningAbsentHours,0) + ( @ScheduledMinutes - @Actual );
                            END; 
		
                        IF (
                             @tracktardies = 1
                             AND @IsTardy = 1
                           )
                            BEGIN
                                SET @ActualRunningTardyHours = @ActualRunningTardyHours + 1;
                            END;
		 --commented by balaji on 10/22/2012 as report (rdl) doesn't add days attended and make up days
		 ---- If there are make up hrs deduct that otherwise it will be added again in progress report
                        IF (
                             @Actual > 0
                             AND @Actual > @ScheduledMinutes
                             AND @Actual <> 9999.00
                           )
                            BEGIN
                                SET @ActualRunningPresentHours = @ActualRunningPresentHours - ( @Actual - @ScheduledMinutes );
                                SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - ( @Actual - @ScheduledMinutes );
                            END;
			
                        IF @tracktardies = 1
                            AND (
                                  @TardyMinutes > 0
                                  OR @IsTardy = 1
                                )
                            BEGIN
                                SET @intTardyBreakPoint = @intTardyBreakPoint + 1;
                            END;	    
		
		
		
		-- Makeup: Calculate Makeup Hours when Student Attended more than scheduled hours          	       
                        IF (
                             @Actual > 0
                             AND @Actual > @ScheduledMinutes
                             AND @Actual <> 9999.00
                           )
                            BEGIN
                                SET @MakeupHours = @MakeupHours + ( @Actual - @ScheduledMinutes );
                            END;
		
                        IF (
                             @tracktardies = 1
                             AND @TardiesMakingAbsence > 0
                             AND @intTardyBreakPoint = @TardiesMakingAbsence
                           )
                            BEGIN
                                SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - @Actual;
                                SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @ScheduledMinutes; --@TardyMinutes
                                SET @intTardyBreakPoint = 0;
                            END;
				
                        DELETE  FROM syStudentAttendanceSummary
                        WHERE   StuEnrollId = @StuEnrollId
                                AND StudentAttendedDate = @MeetDate;
                        INSERT  INTO syStudentAttendanceSummary
                                (
                                 StuEnrollId
                                ,ClsSectionId
                                ,StudentAttendedDate
                                ,ScheduledDays
                                ,ActualDays
                                ,ActualRunningScheduledDays
                                ,ActualRunningPresentDays
                                ,ActualRunningAbsentDays
                                ,ActualRunningMakeupDays
                                ,ActualRunningTardyDays
                                ,AdjustedPresentDays
                                ,AdjustedAbsentDays
                                ,AttendanceTrackType
                                ,ModUser
                                ,ModDate
                                ,tardiesmakingabsence
							    )
                        VALUES  (
                                 @StuEnrollId
                                ,@ClsSectionId
                                ,@MeetDate
                                ,ISNULL(@ScheduledMinutes,0)
                                ,@Actual
                                ,ISNULL(@ActualRunningScheduledDays,0)
                                ,ISNULL(@ActualRunningPresentHours,0)
                                ,ISNULL(@ActualRunningAbsentHours,0)
                                ,ISNULL(@MakeupHours,0)
                                ,ISNULL(@ActualRunningTardyHours,0)
                                ,ISNULL(@AdjustedRunningPresentHours,0)
                                ,ISNULL(@AdjustedRunningAbsentHours,0)
                                ,'Post Attendance by Class'
                                ,'sa'
                                ,GETDATE()
                                ,@TardiesMakingAbsence
							    );

		--update syStudentAttendanceSummary set TardiesMakingAbsence=@TardiesMakingAbsence where StuEnrollId=@StuEnrollId
		--end
                        SET @PrevStuEnrollId = @StuEnrollId; 
                        FETCH NEXT FROM GetAttendance_Cursor INTO @StuEnrollId,@MeetDate,@Actual,@ScheduledMinutes,@Absent,@IsTardy,@tracktardies,
                            @TardiesMakingAbsence;
                    END;
                CLOSE GetAttendance_Cursor;
                DEALLOCATE GetAttendance_Cursor;
            END;		
--end


        SELECT TOP 1
                StuEnrollId
               ,ActualRunningScheduledDays AS ScheduledDays
               ,AdjustedPresentDays AS AttendedDays
               ,AdjustedAbsentDays AS AbsentDays
               ,ActualRunningMakeupDays AS MakeupDays
               ,StudentAttendedDate AS LDA
        INTO    #temp1
        FROM    syStudentAttendanceSummary
        WHERE   StuEnrollId = @StuEnrollId
        ORDER BY StudentAttendedDate DESC;


 -- Main query starts here 
        SELECT DISTINCT TOP 1
                AP.ProgDescrip AS ProgDescrip
               ,APV.PrgVerId AS PrgVerId
               ,APV.PrgVerDescrip AS PrgVerDescrip
               ,CASE WHEN ( APV.Credits > 0.0 ) THEN 1
                     ELSE 0
                END AS PrgVersionTrackCredits
               ,APVT.ProgramVersionTypeId AS ProgramVersionTypeId
               ,APVT.DescriptionProgramVersionType AS DescriptionProgramVersionType
               ,ISNULL(AD.DegreeDescrip,'') AS DegreeDescrip
               ,@CumAverage AS OverallAverage
               ,@cumWeightedGPA AS WeightedAverage_CumGPA
               ,@cumSimpleGPA AS SimpleAverage_CumGPA
               ,ASE.StartDate AS StudentStartDate
               ,ASE.ExpGradDate AS GraduantionDate
               ,SSC.StatusCodeDescrip AS StatusDescrip
               ,dbo.GetLDAforAGivenStuEnrollId(@StuEnrollId) AS StudentLastDateAttended
               ,AAUT.UnitTypeDescrip AS UnitTypeDescripFrom_arAttUnitType
               ,CASE WHEN AAUT.UnitTypeDescrip = 'Present Absent'
                          AND @displayHours = 'hours' THEN 'Hours'
                     WHEN AAUT.UnitTypeDescrip = 'Present Absent'
                          AND @displayHours <> 'hours' THEN 'Days'
                     ELSE 'Hours'
                END AS UnitTypeDescrip
               ,CASE WHEN (
                            @TrackSapAttendance = 'byclass'
                            AND @displayHours = 'hours'
                            AND AAUT.UnitTypeDescrip = 'Present Absent'
                          ) -- PA
--THEN @ActualPresentDays_ConvertTo_Hours
                          THEN CASE WHEN @TermStartDate IS NULL THEN ( (SAS_Noterm.AttendedDays) )
                                    ELSE ( ( SAS_ForTerm.AttendedDays) )
                               END
                     ELSE CASE WHEN (
                                      @TrackSapAttendance = 'byclass'
                                      AND @displayHours = 'hours'
                                      AND AAUT.UnitTypeDescrip = 'Minutes'
                                    ) -- Minutes
                                    THEN CASE WHEN @TermStartDate IS NULL THEN SAS_Noterm.AttendedDays / 60 
                                              ELSE SAS_ForTerm.AttendedDays / 60  
                                         END
                               WHEN (
                                      @TrackSapAttendance = 'byclass'
                                      AND @displayHours = 'hours'
                                      AND AAUT.UnitTypeDescrip = 'Clock Hours'
                                    ) -- Clock Hour
                                    THEN CASE WHEN @TermStartDate IS NULL THEN SAS_Noterm.AttendedDays / 60
                                              ELSE SAS_ForTerm.AttendedDays / 60
                                         END
                               WHEN (
                                      @TrackSapAttendance = 'byclass'
                                      AND @displayHours = 'presentabsent'
                                      AND AAUT.UnitTypeDescrip = 'Clock Hours'
                                    ) -- Clock Hour
                                    THEN CASE WHEN @TermStartDate IS NULL THEN SAS_Noterm.AttendedDays / 60
                                              ELSE SAS_ForTerm.AttendedDays / 60
                                         END
                               WHEN (
                                      @TrackSapAttendance = 'byclass'
                                      AND @displayHours = 'presentabsent'
                                      AND AAUT.UnitTypeDescrip = 'Minutes'
                                    ) -- Minutes
                                    THEN CASE WHEN @TermStartDate IS NULL THEN ( (SAS_Noterm.AttendedDays) )
                                              ELSE ( ( SAS_ForTerm.AttendedDays) )
                                         END
                               ELSE CASE WHEN @TermStartDate IS NULL THEN SAS_Noterm.AttendedDays
                                         ELSE SAS_ForTerm.AttendedDays
                                    END
                          END
                END AS TotalDaysAttended
               ,CASE WHEN (
                            @TrackSapAttendance = 'byclass'
                            AND LOWER(RTRIM(LTRIM(@displayHours))) = 'hours'
                            AND AAUT.UnitTypeDescrip = 'Present Absent'
                          ) THEN SAS_ForTerm.MakeupDays
                     ELSE CASE WHEN (
                                      @TrackSapAttendance = 'byclass'
                                      AND @displayHours = 'hours'
                                      AND AAUT.UnitTypeDescrip = 'Minutes'
                                    ) -- Minutes
                                    THEN CASE WHEN @TermStartDate IS NULL THEN SAS_Noterm.MakeupDays / 60
                                              ELSE SAS_ForTerm.MakeupDays / 60
                                         END
                               WHEN (
                                      @TrackSapAttendance = 'byclass'
                                      AND @displayHours = 'hours'
                                      AND AAUT.UnitTypeDescrip = 'Clock Hours'
                                    ) -- Clock Hour
                                    THEN CASE WHEN @TermStartDate IS NULL THEN SAS_Noterm.MakeupDays / 60
                                              ELSE SAS_ForTerm.MakeupDays / 60
                                         END
                               WHEN (
                                      @TrackSapAttendance = 'byclass'
                                      AND @displayHours = 'presentabsent'
                                      AND AAUT.UnitTypeDescrip = 'Clock Hours'
                                    ) -- Clock Hour
                                    THEN CASE WHEN @TermStartDate IS NULL THEN SAS_Noterm.MakeupDays / 60
                                              ELSE SAS_ForTerm.MakeupDays / 60
                                         END
                               WHEN (
                                      @TrackSapAttendance = 'byclass'
                                      AND @displayHours = 'presentabsent'
                                      AND AAUT.UnitTypeDescrip = 'Minutes'
                                    ) -- Minutes
                                    THEN CASE WHEN @TermStartDate IS NULL THEN ( ( SAS_Noterm.MakeupDays) )
                                              ELSE ( ( SAS_ForTerm.MakeupDays) )
                                         END
                               ELSE CASE WHEN @TermStartDate IS NULL THEN ( SAS_Noterm.MakeupDays )
                                         ELSE ( SAS_ForTerm.MakeupDays )
                                    END
                          END
                END AS MakeupDays
               ,@GPAMethod AS GPAMethod
               ,@GradesFormat AS GradesFormat
               ,@TrackSapAttendance AS TrackSapAttendance
               ,@displayHours AS DisplayHours
               ,@MajorsMinorsConcentrations AS MajorsMinorsConcentrations
        FROM    arStuEnrollments AS ASE
        INNER JOIN syStatusCodes AS SSC ON SSC.StatusCodeId = ASE.StatusCodeId
        INNER JOIN arProgramVersionType AS APVT ON APVT.ProgramVersionTypeId = ASE.PrgVersionTypeId
        INNER JOIN arPrgVersions AS APV ON APV.PrgVerId = ASE.PrgVerId
        INNER JOIN arPrograms AS AP ON AP.ProgId = APV.ProgId
        INNER JOIN arAttUnitType AS AAUT ON AAUT.UnitTypeId = APV.UnitTypeId
        LEFT JOIN arDegrees AS AD ON AD.DegreeId = APV.DegreeId
        LEFT JOIN #temp1 AS SAS_Noterm ON ASE.StuEnrollId = SAS_Noterm.StuEnrollId
        LEFT JOIN #temp1 AS SAS_ForTerm ON ASE.StuEnrollId = SAS_ForTerm.StuEnrollId
        WHERE   ASE.StuEnrollId = @StuEnrollId;
    END;
-- =========================================================================================================
-- END  --  Usp_TR_Sub4_Program
-- =========================================================================================================

GO
