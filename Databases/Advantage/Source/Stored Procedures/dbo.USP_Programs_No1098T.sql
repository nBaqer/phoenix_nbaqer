SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_Programs_No1098T]
    @CampusId UNIQUEIDENTIFIER
AS
    SELECT  DISTINCT
            P.ProgDescrip
    FROM    dbo.arPrograms P
    INNER JOIN dbo.syCmpGrpCmps CGC ON CGC.CampGrpId = P.CampGrpId
    INNER JOIN syCampuses C ON C.CampusId = CGC.CampusId
    WHERE   P.Is1098T = 0
            AND P.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
            AND C.CampusId = @CampusId
    ORDER BY ProgDescrip; 

GO
