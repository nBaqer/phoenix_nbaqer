SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_SA_GetCampusDataAsReceiptAddress]
    (
     @CAmpusID UNIQUEIDENTIFIER
    )
AS /*----------------------------------------------------------------------------------------------------
    Author : Saraswathi Lakshmanan
    
    Create date : 04/12/2010
    
    Procedure Name : [USP_SA_GetCampusDataAsReceiptAddress]

    Objective : Get student ledger balance
    
    Parameters : Name Type Data Type Required? 
                        ===== ==== ========= ========= 
                        @CampusID In Uniqueidentifier Required
    
    Output : returns thecampus address for the given CAmpus
                        
*/-----------------------------------------------------------------------------------------------------

    BEGIN 
--Get Campus Data As Receipt Address

        SELECT  CampDescrip
               ,Address1
               ,Address2
               ,City
               ,(
                  SELECT    StateDescrip
                  FROM      syStates
                  WHERE     StateId = C.StateId
                ) AS State
               ,Zip
               ,(
                  SELECT    CountryDescrip
                  FROM      dbo.adCountries
                  WHERE     CountryId = C.CountryId
                ) AS Country
        FROM    syCampuses C
        WHERE   CampusId = @CAmpusID;
    
    END;




GO
