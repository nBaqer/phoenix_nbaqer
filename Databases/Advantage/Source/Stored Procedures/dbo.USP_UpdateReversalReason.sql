SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_UpdateReversalReason]
    @TransactionId UNIQUEIDENTIFIER
   ,@ReversalReason VARCHAR(250)
   ,@LeadId UNIQUEIDENTIFIER
   ,@TransCodeId UNIQUEIDENTIFIER
   ,@TransReference VARCHAR(50)
   ,@TransDescrip VARCHAR(50)
   ,@TransAmount DECIMAL(19,4)
   ,
--@TransDate DATETIME,
    @CampusId UNIQUEIDENTIFIER
   ,@TransTypeId INT
   ,@IsEnrolled BIT
   ,@Voided BIT
   ,@ModUser VARCHAR(50)
--@ModDate DATETIME
AS
    BEGIN

        BEGIN TRY
	
            SET NOCOUNT ON;
            BEGIN
		
                BEGIN TRANSACTION;
			
                UPDATE  adLeadTransactions
                SET     ReversalReason = @ReversalReason
                WHERE   TransactionId = @TransactionId;
			
                DECLARE @DisplaySequence INT
                   ,@SecondDisplaySequence INT;
                SET @DisplaySequence = (
                                         SELECT TOP 1
                                                ISNULL(DisplaySequence,0)
                                         FROM   adLeadTransactions
                                         WHERE  TransactionId = @TransactionId
                                       );
                SET @SecondDisplaySequence = @DisplaySequence + 1;
			
			-- reverse payment
                INSERT  INTO dbo.adLeadTransactions
                        (
                         TransactionId
                        ,LeadId
                        ,TransCodeId
                        ,TransReference
                        ,TransDescrip
                        ,TransAmount
                        ,TransDate
                        ,TransTypeId
                        ,isEnrolled
                        ,CreatedDate
                        ,Voided
                        ,ModUser
                        ,ModDate
                        ,CampusId
                        ,DisplaySequence
                        ,SecondDisplaySequence
						)
                VALUES  (
                         NEWID()
                        ,@LeadId
                        ,@TransCodeId
                        ,@TransReference
                        ,@TransDescrip
                        ,( @TransAmount ) * -1
                        ,DATEADD(dd,DATEDIFF(dd,0,GETDATE()),0)
                        ,@TransTypeId
                        ,@IsEnrolled
                        ,GETDATE()
                        ,@Voided
                        ,@ModUser
                        ,GETDATE()
                        ,@CampusId
                        ,@DisplaySequence
                        ,@SecondDisplaySequence
						 );
						 
                SET @DisplaySequence = (
                                         SELECT TOP 1
                                                ISNULL(DisplaySequence,0)
                                         FROM   adLeadTransactions
                                         WHERE  TransactionId = (
                                                                  SELECT TOP 1
                                                                            MapTransactionId
                                                                  FROM      adLeadTransactions
                                                                  WHERE     TransactionId = @TransactionId
                                                                )
                                       );
			--Set @DisplaySequence =(select Top 1 IsNULL(DisplaySequence,0) from adLeadTransactions where TransactionId=@TransactionId)
                SET @SecondDisplaySequence = @DisplaySequence + 1;
			
			-- reverse charge
                INSERT  INTO dbo.adLeadTransactions
                        (
                         TransactionId
                        ,LeadId
                        ,TransCodeId
                        ,TransReference
                        ,TransDescrip
                        ,TransAmount
                        ,TransDate
                        ,TransTypeId
                        ,isEnrolled
                        ,CreatedDate
                        ,Voided
                        ,ModUser
                        ,ModDate
                        ,CampusId
                        ,DisplaySequence
                        ,SecondDisplaySequence
						)
                VALUES  (
                         NEWID()
                        ,@LeadId
                        ,@TransCodeId
                        ,@TransReference
                        ,
						  --(@TransDescrip + ' Chrg') ,
                         ( @TransDescrip )
                        ,( @TransAmount )
                        ,DATEADD(dd,DATEDIFF(dd,0,GETDATE()),0)
                        ,@TransTypeId
                        ,@IsEnrolled
                        ,GETDATE()
                        ,@Voided
                        ,@ModUser
                        ,GETDATE()
                        ,@CampusId
                        ,@DisplaySequence
                        ,@SecondDisplaySequence
						 );
							 
                COMMIT TRANSACTION;
			
            END;

        END TRY
		
        BEGIN CATCH
	
            ROLLBACK TRANSACTION;
        END CATCH;	  
    END;



GO
