SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ginzo, John
-- Create date: 09/03/2015
-- Description:	GetGradesForFinalGrades
-- =============================================
CREATE PROCEDURE [dbo].[GetGradesForFinalGrades] @ClsSectId VARCHAR(50)
AS
    BEGIN
	
        SET NOCOUNT ON;

        SELECT DISTINCT
                a.StuEnrollId
               ,a.GrdSysDetailId
               ,a.Score
               ,(
                  SELECT    b.Grade
                  FROM      arGradeSystemDetails b
                  WHERE     b.GrdSysDetailId = a.GrdSysDetailId
                )
        FROM    arResults a
        WHERE   a.TestId = @ClsSectId;
    END;


GO
