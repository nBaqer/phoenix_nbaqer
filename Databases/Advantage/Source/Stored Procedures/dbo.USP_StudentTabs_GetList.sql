SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_StudentTabs_GetList]
    @SchoolEnumerator INT
   ,@CampusId UNIQUEIDENTIFIER
AS
    DECLARE @ShowRossOnlyTabs BIT
       ,@SchedulingMethod VARCHAR(50)
       ,@TrackSAPAttendance VARCHAR(50);
    DECLARE @ShowCollegeOfCourtReporting VARCHAR(5)
       ,@FameESP VARCHAR(5)
       ,@EdExpress VARCHAR(5); 
    DECLARE @GradeBookWeightingLevel VARCHAR(20)
       ,@ShowExternshipTabs VARCHAR(5);

    DECLARE @AR_ParentId UNIQUEIDENTIFIER
       ,@PL_ParentId UNIQUEIDENTIFIER
       ,@FA_ParentId UNIQUEIDENTIFIER;
    DECLARE @FAC_ParentId UNIQUEIDENTIFIER
       ,@SA_ParentId UNIQUEIDENTIFIER;
	
-- Get Values
--SET @SchoolEnumerator = 1689
    IF (
         SELECT COUNT(*)
         FROM   syConfigAppSettings
         WHERE  SettingId = 68
                AND CampusSpecific = 1
       ) >= 1
        BEGIN
            SET @ShowRossOnlyTabs = (
                                      SELECT TOP 1
                                                Value
                                      FROM      (
                                                  SELECT    Value
                                                           ,CampusId
                                                           ,1 AS DisplaySequence
                                                  FROM      syConfigAppSetValues
                                                  WHERE     SettingId = 68
                                                            AND CampusId = @CampusId
                                                  UNION
                                                  SELECT    Value
                                                           ,CampusId
                                                           ,2 AS DisplaySequence
                                                  FROM      syConfigAppSetValues
                                                  WHERE     SettingId = 68
                                                            AND CampusId IS NULL
                                                ) dt
                                      ORDER BY  DisplaySequence ASC
                                    );
								
        END; 
    ELSE
        BEGIN
            SET @ShowRossOnlyTabs = (
                                      SELECT    Value
                                      FROM      dbo.syConfigAppSetValues
                                      WHERE     SettingId = 68
                                    );
        END;

    IF (
         SELECT COUNT(*)
         FROM   syConfigAppSettings
         WHERE  SettingId = 65
                AND CampusSpecific = 1
       ) >= 1
        BEGIN
            SET @SchedulingMethod = (
                                      SELECT TOP 1
                                                Value
                                      FROM      (
                                                  SELECT    Value
                                                           ,CampusId
                                                           ,1 AS DisplaySequence
                                                  FROM      syConfigAppSetValues
                                                  WHERE     SettingId = 65
                                                            AND CampusId = @CampusId
                                                  UNION
                                                  SELECT    Value
                                                           ,CampusId
                                                           ,2 AS DisplaySequence
                                                  FROM      syConfigAppSetValues
                                                  WHERE     SettingId = 65
                                                            AND CampusId IS NULL
                                                ) dt
                                      ORDER BY  DisplaySequence ASC
                                    );
        END; 
    ELSE
        BEGIN
            SET @SchedulingMethod = (
                                      SELECT    Value
                                      FROM      dbo.syConfigAppSetValues
                                      WHERE     SettingId = 65
                                    );
        END;
	
    IF (
         SELECT COUNT(*)
         FROM   syConfigAppSettings
         WHERE  SettingId = 72
                AND CampusSpecific = 1
       ) >= 1
        BEGIN
            SET @TrackSAPAttendance = (
                                        SELECT TOP 1
                                                Value
                                        FROM    (
                                                  SELECT    Value
                                                           ,CampusId
                                                           ,1 AS DisplaySequence
                                                  FROM      syConfigAppSetValues
                                                  WHERE     SettingId = 72
                                                            AND CampusId = @CampusId
                                                  UNION
                                                  SELECT    Value
                                                           ,CampusId
                                                           ,2 AS DisplaySequence
                                                  FROM      syConfigAppSetValues
                                                  WHERE     SettingId = 72
                                                            AND CampusId IS NULL
                                                ) dt
                                        ORDER BY DisplaySequence ASC
                                      );
        END; 
    ELSE
        BEGIN
            SET @TrackSAPAttendance = (
                                        SELECT  Value
                                        FROM    dbo.syConfigAppSetValues
                                        WHERE   SettingId = 72
                                      );
        END;

    IF (
         SELECT COUNT(*)
         FROM   syConfigAppSettings
         WHERE  SettingId = 118
                AND CampusSpecific = 1
       ) >= 1
        BEGIN
            SET @ShowCollegeOfCourtReporting = (
                                                 SELECT TOP 1
                                                        Value
                                                 FROM   (
                                                          SELECT    Value
                                                                   ,CampusId
                                                                   ,1 AS DisplaySequence
                                                          FROM      syConfigAppSetValues
                                                          WHERE     SettingId = 118
                                                                    AND CampusId = @CampusId
                                                          UNION
                                                          SELECT    Value
                                                                   ,CampusId
                                                                   ,2 AS DisplaySequence
                                                          FROM      syConfigAppSetValues
                                                          WHERE     SettingId = 118
                                                                    AND CampusId IS NULL
                                                        ) dt
                                                 ORDER BY DisplaySequence ASC
                                               );
        END; 
    ELSE
        BEGIN
            SET @ShowCollegeOfCourtReporting = (
                                                 SELECT Value
                                                 FROM   dbo.syConfigAppSetValues
                                                 WHERE  SettingId = 118
                                               );
        END;

    IF (
         SELECT COUNT(*)
         FROM   syConfigAppSettings
         WHERE  SettingId = 37
                AND CampusSpecific = 1
       ) >= 1
        BEGIN
            SET @FameESP = (
                             SELECT TOP 1
                                    Value
                             FROM   (
                                      SELECT    Value
                                               ,CampusId
                                               ,1 AS DisplaySequence
                                      FROM      syConfigAppSetValues
                                      WHERE     SettingId = 37
                                                AND CampusId = @CampusId
                                      UNION
                                      SELECT    Value
                                               ,CampusId
                                               ,2 AS DisplaySequence
                                      FROM      syConfigAppSetValues
                                      WHERE     SettingId = 37
                                                AND CampusId IS NULL
                                    ) dt
                             ORDER BY DisplaySequence ASC
                           );
        END; 
    ELSE
        BEGIN
            SET @FameESP = (
                             SELECT Value
                             FROM   dbo.syConfigAppSetValues
                             WHERE  SettingId = 37
                           );
        END;
	
    IF (
         SELECT COUNT(*)
         FROM   syConfigAppSettings
         WHERE  SettingId = 91
                AND CampusSpecific = 1
       ) >= 1
        BEGIN
            SET @EdExpress = (
                               SELECT TOP 1
                                        Value
                               FROM     (
                                          SELECT    Value
                                                   ,CampusId
                                                   ,1 AS DisplaySequence
                                          FROM      syConfigAppSetValues
                                          WHERE     SettingId = 91
                                                    AND CampusId = @CampusId
                                          UNION
                                          SELECT    Value
                                                   ,CampusId
                                                   ,2 AS DisplaySequence
                                          FROM      syConfigAppSetValues
                                          WHERE     SettingId = 91
                                                    AND CampusId IS NULL
                                        ) dt
                               ORDER BY DisplaySequence ASC
                             );
        END; 
    ELSE
        BEGIN
            SET @EdExpress = (
                               SELECT   Value
                               FROM     dbo.syConfigAppSetValues
                               WHERE    SettingId = 91
                             );
        END;
	
    IF (
         SELECT COUNT(*)
         FROM   syConfigAppSettings
         WHERE  SettingId = 43
                AND CampusSpecific = 1
       ) >= 1
        BEGIN
            SET @GradeBookWeightingLevel = (
                                             SELECT TOP 1
                                                    Value
                                             FROM   (
                                                      SELECT    Value
                                                               ,CampusId
                                                               ,1 AS DisplaySequence
                                                      FROM      syConfigAppSetValues
                                                      WHERE     SettingId = 43
                                                                AND CampusId = @CampusId
                                                      UNION
                                                      SELECT    Value
                                                               ,CampusId
                                                               ,2 AS DisplaySequence
                                                      FROM      syConfigAppSetValues
                                                      WHERE     SettingId = 43
                                                                AND CampusId IS NULL
                                                    ) dt
                                             ORDER BY DisplaySequence ASC
                                           );
        END; 
    ELSE
        BEGIN
            SET @GradeBookWeightingLevel = (
                                             SELECT Value
                                             FROM   dbo.syConfigAppSetValues
                                             WHERE  SettingId = 43
                                           );
        END;
	
    IF (
         SELECT COUNT(*)
         FROM   syConfigAppSettings
         WHERE  SettingId = 71
                AND CampusSpecific = 1
       ) >= 1
        BEGIN
            SET @ShowExternshipTabs = (
                                        SELECT TOP 1
                                                Value
                                        FROM    (
                                                  SELECT    Value
                                                           ,CampusId
                                                           ,1 AS DisplaySequence
                                                  FROM      syConfigAppSetValues
                                                  WHERE     SettingId = 71
                                                            AND CampusId = @CampusId
                                                  UNION
                                                  SELECT    Value
                                                           ,CampusId
                                                           ,2 AS DisplaySequence
                                                  FROM      syConfigAppSetValues
                                                  WHERE     SettingId = 71
                                                            AND CampusId IS NULL
                                                ) dt
                                        ORDER BY DisplaySequence ASC
                                      );
        END; 
    ELSE
        BEGIN
            SET @ShowExternshipTabs = (
                                        SELECT  Value
                                        FROM    dbo.syConfigAppSetValues
                                        WHERE   SettingId = 71
                                      );
        END;

    SET @AR_ParentId = (
                         SELECT t1.HierarchyId
                         FROM   syNavigationNodes t1
                         INNER JOIN syNavigationNodes t2 ON t1.ParentId = t2.HierarchyId
                         WHERE  t1.ResourceId = 394
                                AND t2.ResourceId = 26
                       );
    SET @PL_ParentId = (
                         SELECT t1.HierarchyId
                         FROM   syNavigationNodes t1
                         INNER JOIN syNavigationNodes t2 ON t1.ParentId = t2.HierarchyId
                         WHERE  t1.ResourceId = 394
                                AND t2.ResourceId = 193
                       );
    SET @SA_ParentId = (
                         SELECT t1.HierarchyId
                         FROM   syNavigationNodes t1
                         INNER JOIN syNavigationNodes t2 ON t1.ParentId = t2.HierarchyId
                         WHERE  t1.ResourceId = 394
                                AND t2.ResourceId = 194
                       );
    SET @FAC_ParentId = (
                          SELECT    t1.HierarchyId
                          FROM      syNavigationNodes t1
                          INNER JOIN syNavigationNodes t2 ON t1.ParentId = t2.HierarchyId
                          WHERE     t1.ResourceId = 394
                                    AND t2.ResourceId = 300
                        );					
    SET @FA_ParentId = (
                         SELECT t1.HierarchyId
                         FROM   syNavigationNodes t1
                         INNER JOIN syNavigationNodes t2 ON t1.ParentId = t2.HierarchyId
                         WHERE  t1.ResourceId = 394
                                AND t2.ResourceId = 191
                       );
						
--SELECT 26 AS ModuleResourceId,ResourceId AS ChildResourceId,Resource AS ChildResource,
--NULL AS ChildResourceURL,NULL AS ParentResourceId,NULL AS ParentResource
--FROM syResources WHERE ResourceId IN (394)
--UNION
--SELECT 191 AS ModuleResourceId,ResourceId AS ChildResourceId,Resource AS ChildResource,
--NULL AS ChildResourceURL,NULL AS ParentResourceId,NULL AS ParentResource
--FROM syResources WHERE ResourceId IN (394)
--UNION
--SELECT 193 AS ModuleResourceId,ResourceId AS ChildResourceId,Resource AS ChildResource,
--NULL AS ChildResourceURL,NULL AS ParentResourceId,NULL AS ParentResource
--FROM syResources WHERE ResourceId IN (394)
--UNION
--SELECT 194 AS ModuleResourceId,ResourceId AS ChildResourceId,Resource AS ChildResource,
--NULL AS ChildResourceURL,NULL AS ParentResourceId,NULL AS ParentResource
--FROM syResources WHERE ResourceId IN (394)
--UNION
--SELECT 300 AS ModuleResourceId,ResourceId AS ChildResourceId,Resource AS ChildResource,
--NULL AS ChildResourceURL,NULL AS ParentResourceId,NULL AS ParentResource
--FROM syResources WHERE ResourceId IN (394)
--UNION
    SELECT  *
           ,CASE WHEN ChildResourceId = 679 THEN 1
                 ELSE CASE WHEN ChildResourceId = 680 THEN 2
                           ELSE CASE WHEN ChildResourceId = 681 THEN 3
                                     ELSE CASE WHEN ChildResourceId = 682 THEN 4
                                               ELSE 5
                                          END
                                END
                      END
            END AS SortOrder
    FROM    (
              SELECT    26 AS ModuleResourceId
                       ,ResourceID AS ChildResourceId
                       ,Resource AS ChildResource
                       ,NULL AS ChildResourceURL
                       ,NULL AS ParentResourceId
                       ,NULL AS ParentResource
              FROM      syResources
              WHERE     ResourceID IN ( 679,680,681,682 )
              UNION
              SELECT    26 AS ModuleResourceId
                       ,ChildResourceId
                       ,ChildResource
                       ,ChildResourceURL
                       ,ParentResourceId
                       ,ParentResource
              FROM      (
                          SELECT DISTINCT
                                    NNChild.ResourceId AS ChildResourceId
                                   ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                         ELSE RChild.Resource
                                    END AS ChildResource
                                   ,RChild.ResourceURL AS ChildResourceURL
                                   ,CASE WHEN NNParent.ResourceId = 394 THEN NULL
                                         ELSE NNParent.ResourceId
                                    END AS ParentResourceId
                                   ,RParent.Resource AS ParentResource
                                   ,RParentModule.Resource AS MODULE
                                   ,NNParent.ParentId AS ParentId2
                          FROM      syResources RChild
                          INNER JOIN syNavigationNodes NNChild ON RChild.ResourceID = NNChild.ResourceId
                          INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                          INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                          LEFT OUTER JOIN (
                                            SELECT  *
                                            FROM    syResources
                                            WHERE   ResourceTypeID = 1
                                          ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                          WHERE     RChild.ResourceTypeID IN ( 2,3,8 )
                                    AND (
                                          RChild.ChildTypeId IS NULL
                                          OR RChild.ChildTypeId = 3
                                        )
                                    AND (
                                          ( NNParent.ResourceId IN ( 679,680,681,682 )
                                          AND NNParent.ParentId IN ( @AR_ParentId )
					)
                                        )
                                    AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
                        ) t1
              UNION ALL
              SELECT    194 AS ModuleResourceId
                       ,ResourceID AS ChildResourceId
                       ,Resource AS ChildResource
                       ,NULL AS ChildResourceURL
                       ,NULL AS ParentResourceId
                       ,NULL AS ParentResource
              FROM      syResources
              WHERE     ResourceID IN ( 679,680,681,682 )
              UNION
              SELECT    194 AS ModuleResourceId
                       ,ChildResourceId
                       ,ChildResource
                       ,ChildResourceURL
                       ,ParentResourceId
                       ,ParentResource
              FROM      (
                          SELECT DISTINCT
                                    NNChild.ResourceId AS ChildResourceId
                                   ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                         ELSE RChild.Resource
                                    END AS ChildResource
                                   ,RChild.ResourceURL AS ChildResourceURL
                                   ,CASE WHEN NNParent.ResourceId = 394 THEN NULL
                                         ELSE NNParent.ResourceId
                                    END AS ParentResourceId
                                   ,RParent.Resource AS ParentResource
                                   ,RParentModule.Resource AS MODULE
                                   ,NNParent.ParentId AS ParentId2
                          FROM      syResources RChild
                          INNER JOIN syNavigationNodes NNChild ON RChild.ResourceID = NNChild.ResourceId
                          INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                          INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                          LEFT OUTER JOIN (
                                            SELECT  *
                                            FROM    syResources
                                            WHERE   ResourceTypeID = 1
                                          ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                          WHERE     RChild.ResourceTypeID IN ( 2,3,8 )
                                    AND (
                                          RChild.ChildTypeId IS NULL
                                          OR RChild.ChildTypeId = 3
                                        )
                                    AND (
                                          ( NNParent.ResourceId IN ( 679,680,681,682 )
                                          AND NNParent.ParentId IN ( @SA_ParentId )
					)
                                        )
                                    AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
                        ) t1
              UNION ALL
              SELECT    193 AS ModuleResourceId
                       ,ResourceID AS ChildResourceId
                       ,Resource AS ChildResource
                       ,NULL AS ChildResourceURL
                       ,NULL AS ParentResourceId
                       ,NULL AS ParentResource
              FROM      syResources
              WHERE     ResourceID IN ( 679,680,681,682 )
              UNION
              SELECT    193 AS ModuleResourceId
                       ,ChildResourceId
                       ,ChildResource
                       ,ChildResourceURL
                       ,ParentResourceId
                       ,ParentResource
              FROM      (
                          SELECT DISTINCT
                                    NNChild.ResourceId AS ChildResourceId
                                   ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                         ELSE RChild.Resource
                                    END AS ChildResource
                                   ,RChild.ResourceURL AS ChildResourceURL
                                   ,CASE WHEN NNParent.ResourceId = 394 THEN NULL
                                         ELSE NNParent.ResourceId
                                    END AS ParentResourceId
                                   ,RParent.Resource AS ParentResource
                                   ,RParentModule.Resource AS MODULE
                                   ,NNParent.ParentId AS ParentId2
                          FROM      syResources RChild
                          INNER JOIN syNavigationNodes NNChild ON RChild.ResourceID = NNChild.ResourceId
                          INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                          INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                          LEFT OUTER JOIN (
                                            SELECT  *
                                            FROM    syResources
                                            WHERE   ResourceTypeID = 1
                                          ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                          WHERE     RChild.ResourceTypeID IN ( 2,3,8 )
                                    AND (
                                          RChild.ChildTypeId IS NULL
                                          OR RChild.ChildTypeId = 3
                                        )
                                    AND (
                                          ( NNParent.ResourceId IN ( 679,680,681,682 )
                                          AND NNParent.ParentId IN ( @PL_ParentId )
					)
                                        )
                                    AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
                        ) t1
              UNION ALL
              SELECT    300 AS ModuleResourceId
                       ,ResourceID AS ChildResourceId
                       ,Resource AS ChildResource
                       ,NULL AS ChildResourceURL
                       ,NULL AS ParentResourceId
                       ,NULL AS ParentResource
              FROM      syResources
              WHERE     ResourceID IN ( 679,680,681,682 )
              UNION
              SELECT    300 AS ModuleResourceId
                       ,ChildResourceId
                       ,ChildResource
                       ,ChildResourceURL
                       ,ParentResourceId
                       ,ParentResource
              FROM      (
                          SELECT DISTINCT
                                    NNChild.ResourceId AS ChildResourceId
                                   ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                         ELSE RChild.Resource
                                    END AS ChildResource
                                   ,RChild.ResourceURL AS ChildResourceURL
                                   ,CASE WHEN NNParent.ResourceId = 394 THEN NULL
                                         ELSE NNParent.ResourceId
                                    END AS ParentResourceId
                                   ,RParent.Resource AS ParentResource
                                   ,RParentModule.Resource AS MODULE
                                   ,NNParent.ParentId AS ParentId2
                          FROM      syResources RChild
                          INNER JOIN syNavigationNodes NNChild ON RChild.ResourceID = NNChild.ResourceId
                          INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                          INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                          LEFT OUTER JOIN (
                                            SELECT  *
                                            FROM    syResources
                                            WHERE   ResourceTypeID = 1
                                          ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                          WHERE     RChild.ResourceTypeID IN ( 2,3,8 )
                                    AND (
                                          RChild.ChildTypeId IS NULL
                                          OR RChild.ChildTypeId = 3
                                        )
                                    AND (
                                          ( NNParent.ResourceId IN ( 679,680,681,682 )
                                          AND NNParent.ParentId IN ( @FAC_ParentId )
					)
                                        )
                                    AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
                        ) t1
              UNION ALL
              SELECT    191 AS ModuleResourceId
                       ,ResourceID AS ChildResourceId
                       ,Resource AS ChildResource
                       ,NULL AS ChildResourceURL
                       ,NULL AS ParentResourceId
                       ,NULL AS ParentResource
              FROM      syResources
              WHERE     ResourceID IN ( 679,680,681,682 )
              UNION
              SELECT    191 AS ModuleResourceId
                       ,ChildResourceId
                       ,ChildResource
                       ,ChildResourceURL
                       ,ParentResourceId
                       ,ParentResource
              FROM      (
                          SELECT DISTINCT
                                    NNChild.ResourceId AS ChildResourceId
                                   ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                         ELSE RChild.Resource
                                    END AS ChildResource
                                   ,RChild.ResourceURL AS ChildResourceURL
                                   ,CASE WHEN NNParent.ResourceId = 394 THEN NULL
                                         ELSE NNParent.ResourceId
                                    END AS ParentResourceId
                                   ,RParent.Resource AS ParentResource
                                   ,RParentModule.Resource AS MODULE
                                   ,NNParent.ParentId AS ParentId2
                          FROM      syResources RChild
                          INNER JOIN syNavigationNodes NNChild ON RChild.ResourceID = NNChild.ResourceId
                          INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                          INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                          LEFT OUTER JOIN (
                                            SELECT  *
                                            FROM    syResources
                                            WHERE   ResourceTypeID = 1
                                          ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                          WHERE     RChild.ResourceTypeID IN ( 2,3,8 )
                                    AND (
                                          RChild.ChildTypeId IS NULL
                                          OR RChild.ChildTypeId = 3
                                        )
                                    AND (
                                          ( NNParent.ResourceId IN ( 679,680,681,682 )
                                          AND NNParent.ParentId IN ( @FA_ParentId )
					)
                                        )
                                    AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
                        ) t1
            ) MainQuery
    WHERE   -- Hide resources based on Configuration Settings
            (
              -- The following expression means if showross... is set to false, hide 
					-- ResourceIds 541,542,532,534,535,538,543,539
					-- (Not False) OR (Condition)
			  (
                ( @ShowRossOnlyTabs <> 0 )
                OR ( ChildResourceId NOT IN ( 541,542,532,534,535,538,543,539 ) )
              )
              AND
					-- The following expression means if showross... is set to true, hide 
					-- ResourceIds 142,375,330,476,508,102,107,237
					-- (Not True) OR (Condition)
              (
                ( @ShowRossOnlyTabs <> 1 )
                OR ( ChildResourceId NOT IN ( 142,375,330,476,508,102,107,237 ) )
              )
              AND
					---- The following expression means if SchedulingMethod=regulartraditional, hide 
					---- ResourceIds 91 and 497
					---- (Not True) OR (Condition)
              (
                (
                  NOT LOWER(LTRIM(RTRIM(@SchedulingMethod))) = 'regulartraditional'
                )
                OR ( ChildResourceId NOT IN ( 91,497 ) )
              )
              AND
					-- The following expression means if TrackSAPAttendance=byday, hide 
					-- ResourceIds 633
					-- (Not True) OR (Condition)
              (
                (
                  NOT LOWER(LTRIM(RTRIM(@TrackSAPAttendance))) = 'byday'
                )
                OR ( ChildResourceId NOT IN ( 633 ) )
              )
              AND
					---- The following expression means if @ShowCollegeOfCourtReporting is set to false, hide 
					---- ResourceIds 614,615
					---- (Not False) OR (Condition)
              (
                (
                  NOT LOWER(LTRIM(RTRIM(@ShowCollegeOfCourtReporting))) = 'no'
                )
                OR ( ChildResourceId NOT IN ( 614,615 ) )
              )
              AND
					-- The following expression means if @ShowCollegeOfCourtReporting is set to true, hide 
					-- ResourceIds 497
					-- (Not True) OR (Condition)
              (
                (
                  NOT LOWER(LTRIM(RTRIM(@ShowCollegeOfCourtReporting))) = 'yes'
                )
                OR ( ChildResourceId NOT IN ( 497 ) )
              )
              AND
					-- The following expression means if FAMEESP is set to false, hide 
					-- ResourceIds 517,523, 525
					-- (Not False) OR (Condition)
              (
                (
                  NOT LOWER(LTRIM(RTRIM(@FameESP))) = 'no'
                )
                OR ( ChildResourceId NOT IN ( 517,523,525 ) )
              )
              AND
					---- The following expression means if EDExpress is set to false, hide 
					---- ResourceIds 603,604,606,619
					---- (Not False) OR (Condition)
              (
                (
                  NOT LOWER(LTRIM(RTRIM(@EdExpress))) = 'no'
                )
                OR ( ChildResourceId NOT IN ( 603,604,605,606,619 ) )
              )
              AND
					---- The following expression means if @GradeBookWeightingLevel is set to courselevel, hide 
					---- ResourceIds 107,96,222
					---- (Not False) OR (Condition)
              (
                (
                  NOT LOWER(LTRIM(RTRIM(@GradeBookWeightingLevel))) = 'courselevel'
                )
                OR ( ChildResourceId NOT IN ( 107,96,222 ) )
              )
              AND (
                    (
                      NOT LOWER(LTRIM(RTRIM(@GradeBookWeightingLevel))) = 'instructorlevel'
                    )
                    OR ( ChildResourceId NOT IN ( 476 ) )
                  )
              AND (
                    (
                      NOT LOWER(LTRIM(RTRIM(@ShowExternshipTabs))) = 'no'
                    )
                    OR ( ChildResourceId NOT IN ( 543,538 ) )
                  )
            )
    ORDER BY ModuleResourceId
           ,ParentResourceId
           ,SortOrder
           ,ChildResource;



GO
