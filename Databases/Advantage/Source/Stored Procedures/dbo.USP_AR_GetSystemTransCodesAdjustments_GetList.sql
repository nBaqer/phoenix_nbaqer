SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[USP_AR_GetSystemTransCodesAdjustments_GetList]
    (
     @ShowActive NVARCHAR(50)
    )
AS /*----------------------------------------------------------------------------------------------------
    Author : Saraswathi Lakshmanan
    
    Create date : 05/05/2010
    
    Procedure Name : [USP_AR_GetSystemTransCodesAdjustments_GetList]

    Objective : Get the system TransactionCodes of type charges
    
    Parameters : Name Type Data Type Required? 
                        ===== ==== ========= ========= 
                        @ShowActive In nvarchar Required
    
    Output : Returns the TransCodes of type whose systransCodeID 14, 15 i.e credit or debit adjustment
                        
*/-----------------------------------------------------------------------------------------------------

    BEGIN

        DECLARE @ShowActiveValue NVARCHAR(20);
        IF LOWER(@ShowActive) = 'true'
            BEGIN
                SET @ShowActiveValue = 'Active';
            END;
        ELSE
            BEGIN
                SET @ShowActiveValue = 'InActive';
            END;

        SELECT  TC.TransCodeId
               ,TC.TransCodeCode
               ,SysTransCodeID
               ,TC.TransCodeDescrip
               ,TC.StatusId
               ,TC.CampGrpId
               ,ST.StatusId
               ,ST.Status
        FROM    saTransCodes TC
               ,syStatuses ST
        WHERE   TC.StatusId = ST.StatusId
                AND SysTransCodeId IN ( 14,15 )
                AND ST.Status = @ShowActiveValue
        ORDER BY TC.TransCodeDescrip;

    END;





GO
