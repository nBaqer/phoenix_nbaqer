SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[sp_GetStuEnrollments]
    @StudentNumber VARCHAR(50)
AS
    BEGIN  
	  
        SELECT DISTINCT
                E.StuEnrollId
               ,Program.ProgDescrip AS Enrollment
        FROM    arStuEnrollments AS E
        INNER JOIN arStudent AS S ON E.StudentId = S.StudentId
        INNER JOIN arPrgVersions AS V ON E.PrgVerId = V.PrgVerId
        INNER JOIN dbo.arPrograms Program ON V.ProgId = Program.ProgId
        INNER JOIN syStatuses AS ST ON V.StatusId = ST.StatusId
        INNER JOIN syCampuses AS C ON E.CampusId = C.CampusId
        INNER JOIN syStatusCodes AS SC ON E.StatusCodeId = SC.StatusCodeId
        INNER JOIN sySysStatus AS SS ON SC.SysStatusId = SS.SysStatusId
        WHERE   ( SS.StatusLevelId = 2 )
                AND SS.SysStatusId IN ( 9,10,11,12,14,19,20,21,22 )
                AND S.StudentNumber = @StudentNumber
                AND E.StartDate <= GETDATE()
        ORDER BY E.StuEnrollId DESC;  
    END;  
	  
GO
