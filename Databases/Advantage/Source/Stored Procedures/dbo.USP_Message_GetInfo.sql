SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Sweta>
-- Create date: <7/15/2016>
-- Description:	<Retrives a MessageInfo given a messageid>
-- =============================================
CREATE PROCEDURE [dbo].[USP_Message_GetInfo] @MessageId VARCHAR(50)
AS
    BEGIN
        DECLARE @Email VARCHAR(100);
        DECLARE @leadId UNIQUEIDENTIFIER;
-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;

        IF ( (
               SELECT   RecipientType
               FROM     dbo.msgMessages
               WHERE    MessageId = @MessageId
             ) = '395' )
            BEGIN
                SET @leadId = (
                                SELECT  RecipientId
                                FROM    msgMessages
                                WHERE   MessageId = @MessageId
                              );	
                IF ( (
                       SELECT   COUNT(EMail)
                       FROM     dbo.AdLeadEmail
                       WHERE    LeadId = @leadId
                     ) > 0 )
                    BEGIN
                        IF EXISTS ( SELECT  EMail
                                    FROM    dbo.AdLeadEmail
                                    WHERE   LeadId = @leadId
                                            AND IsPreferred = 1 )
                            SET @Email = (
                                           SELECT TOP 1
                                                    EMail
                                           FROM     dbo.AdLeadEmail
                                           WHERE    LeadId = @leadId
                                                    AND IsPreferred = 1
                                         );
                        ELSE
                            SET @Email = (
                                           SELECT TOP 1
                                                    EMail
                                           FROM     dbo.AdLeadEmail
                                           WHERE    LeadId = @leadId
                                                    AND IsPreferred = 0
                                         );
                    END;
            END;

	
        SELECT  M.MessageId
               ,M.TemplateId
               ,M.MsgContent
               ,M.FromId
               ,(
                  SELECT    Email
                  FROM      syUsers U
                  WHERE     U.UserId = M.FromId
                ) AS MailFrom
               ,ISNULL(MT.Descrip,'{Undefined}') AS TemplateDescrip
               ,(
                  SELECT    Descrip
                  FROM      msgGroups
                  WHERE     GroupId = MT.GroupId
                ) AS GroupDescrip
               ,M.DeliveryType
               ,M.RecipientId
               ,M.RecipientType
               ,CASE M.RecipientType
                  WHEN '394' THEN (
                                    SELECT  FirstName + ' ' + LastName
                                    FROM    arStudent S
                                    WHERE   S.StudentId = M.RecipientId
                                  )
                  WHEN '395' THEN (
                                    SELECT  FirstName + ' ' + LastName
                                    FROM    adLeads
                                    WHERE   LeadId = M.RecipientId
                                  )
                  WHEN '434' THEN (
                                    SELECT  LenderDescrip
                                    FROM    faLenders
                                    WHERE   LenderId = M.RecipientId
                                  )
                  WHEN '396' THEN (
                                    SELECT  FirstName + ' ' + LastName
                                    FROM    hrEmployees E
                                    WHERE   E.EmpId = M.RecipientId
                                  )
                  WHEN '397' THEN (
                                    SELECT  EmployerDescrip
                                    FROM    plEmployers E
                                    WHERE   E.EmployerId = M.RecipientId
                                  )
                END AS RecipientName
               ,CASE M.RecipientType
                  WHEN '394' THEN (
                                    SELECT  HomeEmail
                                    FROM    arStudent S
                                    WHERE   S.StudentId = M.RecipientId
                                  )
                  WHEN '395' THEN (
                                    SELECT  @Email
                                  )
     --removed the following code so as to take the email from the newly craeted table for leads 3.8 onwards                             )	
	 --SELECT HomeEmail from adLeads where LeadId=M.RecipientId	 
                  WHEN '434' THEN (
                                    SELECT  Email
                                    FROM    faLenders
                                    WHERE   LenderId = M.RecipientId
                                  )
                  WHEN '396' THEN (
                                    SELECT TOP 1
                                            WorkEmail
                                    FROM    hrEmpContactInfo E
                                    WHERE   E.EmpId = M.RecipientId
                                  )
                  WHEN '397' THEN (
                                    SELECT  Email
                                    FROM    plEmployers E
                                    WHERE   E.EmployerId = M.RecipientId
                                  )
                END AS MailTo
               ,M.ReId
               ,M.ReType
               ,CASE M.ReType
                  WHEN '394' THEN (
                                    SELECT  FirstName + ' ' + LastName
                                    FROM    arStudent S
                                    WHERE   S.StudentId = M.ReId
                                  )
                  WHEN '395' THEN (
                                    SELECT  FirstName + ' ' + LastName
                                    FROM    adLeads
                                    WHERE   LeadId = M.ReId
                                  )
                  WHEN '434' THEN (
                                    SELECT  LenderDescrip
                                    FROM    faLenders
                                    WHERE   LenderId = M.ReId
                                  )
                  WHEN '396' THEN (
                                    SELECT  FirstName + ' ' + LastName
                                    FROM    hrEmployees E
                                    WHERE   E.EmpId = M.ReId
                                  )
                  WHEN '397' THEN (
                                    SELECT  EmployerDescrip
                                    FROM    plEmployers E
                                    WHERE   E.EmployerId = M.ReId
                                  )
                END AS ReName
               ,M.ChangeId
               ,MT.Data AS TemplateData
               ,M.MsgContent
               ,M.CreatedDate
               ,M.DeliveryDate
               ,M.LastDeliveryAttempt
               ,M.LastDeliveryMsg
               ,M.DateDelivered
               ,M.ModDate
               ,M.ModUser
        FROM    msgMessages M
        LEFT OUTER JOIN msgTemplates MT ON M.TemplateId = MT.TemplateId
        WHERE   M.MessageId = @MessageId
        ORDER BY M.CreatedDate;
    
    END;
GO
