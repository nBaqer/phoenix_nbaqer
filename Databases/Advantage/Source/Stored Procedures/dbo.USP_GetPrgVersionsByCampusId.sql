SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

--=================================================================================================
-- USP_GetPrgVersionsByCampusId
--=================================================================================================
-- =============================================
-- Author:		JAGG
-- Create date: 06/06/2017
-- Description: Get List of active Program Versions for CampusId 
-- EXECUTE  [USP_GetPrgVersionsByCampusId   @CampusId = N'F335D044-F05B-4F4F-8590-39552C6FDC3F'              
--                 
-- =============================================
CREATE PROCEDURE [dbo].[USP_GetPrgVersionsByCampusId]
    @CampusId NVARCHAR(50)
AS
    BEGIN
        SELECT   DISTINCT APV.PrgVerId
                ,APV.PrgVerDescrip AS PrgVerDescrip
        FROM     arPrgVersions AS APV
        INNER JOIN syStatuses AS SS ON APV.StatusId = SS.StatusId
        INNER JOIN dbo.syCampGrps AS GG ON GG.CampGrpId = APV.CampGrpId
        LEFT JOIN dbo.syCampuses AS CC ON CC.CampusId = GG.CampusId
        WHERE    SS.StatusCode = 'A'
                 AND (
                         CC.CampusId = @CampusId
                         OR GG.CampGrpCode = 'All'
                     )
        ORDER BY APV.PrgVerDescrip;
    END;
--=================================================================================================
-- END  --  USP_GetPrgVersionsByCampusId
--=================================================================================================
GO
