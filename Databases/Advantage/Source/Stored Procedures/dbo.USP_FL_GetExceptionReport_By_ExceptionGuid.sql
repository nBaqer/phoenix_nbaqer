SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[USP_FL_GetExceptionReport_By_ExceptionGuid]
    @ExceptionGUID VARCHAR(50)
AS
    SELECT  *
    FROM    syFameESPExceptionReport
    WHERE   ExceptionGUID = @ExceptionGUID
    ORDER BY SSN
           ,moddate;




GO
