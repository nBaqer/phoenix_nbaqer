SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[USP_SA_GetPmtPlanScheduledDisb]
    (
     @StuEnrollID UNIQUEIDENTIFIER
    )
AS /*----------------------------------------------------------------------------------------------------
	Author          :	Saraswathi Lakshmanan
    
    Create date		:	04/12/2010
    
	Procedure Name	:	[USP_SA_GetPmtPlanScheduledDisb]

	Objective		:	Get the awards for the student
	
	Parameters		:	Name			Type	Data Type			Required? 	
						=====			====	=========			=========	
						@StuEnrollID	In		Uniqueidentifier	Required
	
	Output			:	returns all the available awards for a student
						
*/-----------------------------------------------------------------------------------------------------

    BEGIN

        SELECT  ScheduleId
               ,ScheduleDescrip
               ,AcademicYearDescrip
               ,StartDate
               ,EndDate
               ,ExpectedDate
               ,Amount
               ,Balance
               ,IsAwardDisb
               ,IsInDB
               ,ModDate
        FROM    (
                  SELECT    PayPlanScheduleId AS ScheduleId
                           ,PayPlanDescrip AS ScheduleDescrip
                           ,(
                              SELECT    AcademicYearDescrip
                              FROM      saAcademicYears
                              WHERE     AcademicYearId = SPP.AcademicYearId
                            ) AS AcademicYearDescrip
                           ,SPP.PayPlanStartDate AS StartDate
                           ,SPP.PayPlanEndDate AS EndDate
                           ,ExpectedDate
                           ,Amount
                           ,Amount - COALESCE((
                                                SELECT  SUM(Amount)
                                                FROM    saPmtDisbRel PDR
                                                       ,saTransactions T
                                                WHERE   PayPlanScheduleId = PPS.PayPlanScheduleId
                                                        AND PDR.TransactionId = T.TransactionId
                                                        AND T.Voided = 0
                                                GROUP BY PDR.PayPlanScheduleId
                                              ),0) AS Balance
                           ,0 AS IsAwardDisb
                           ,0 AS IsInDB
                           ,PPS.ModDate
                  FROM      faStudentPaymentPlans SPP
                           ,faStuPaymentPlanSchedule PPS
                  WHERE     SPP.StuEnrollId = @StuEnrollID
                            AND SPP.PaymentPlanId = PPS.PaymentPlanId
                ) T
        WHERE   Balance > 0
        ORDER BY ScheduleDescrip
               ,AcademicYearDescrip
               ,StartDate
               ,EndDate
               ,ExpectedDate;   		
    END;



GO
