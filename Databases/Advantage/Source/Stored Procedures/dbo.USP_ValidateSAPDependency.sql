SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[USP_ValidateSAPDependency]
    (
     @SAPId UNIQUEIDENTIFIER 
    )
AS
    BEGIN 
        SELECT  COUNT(SE.StuEnrollId) AS STEnrCount
        FROM    arSAP SAP
        INNER JOIN arPrgVersions PV ON SAP.SAPId = PV.SAPId
        INNER JOIN arStuEnrollments SE ON SE.PrgVerId = PV.PrgVerId
        WHERE   SAP.SAPId = @SAPId;
    END;



GO
