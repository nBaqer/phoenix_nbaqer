SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[USP_GetUsersCampuslist_ForwhomtheUserCanAssignTasksTo]
    (
     @UserID AS VARCHAR(50)
    ,@RoleID AS VARCHAR(8000) = NULL
    )
AS
    SET NOCOUNT ON;
    BEGIN 

        SELECT DISTINCT
                CampDescrip
        FROM    SyUsers S
               ,dbo.syCampuses SC
               ,dbo.syUsersRolesCampGrps SR
        WHERE   S.CAmpusID = SC.CampusId
                AND SR.UserId = S.USerID
                AND (
                      SR.Roleid = @RoleID
                      OR @RoleID IS NULL
                    )
                AND S.USerID IN ( SELECT    USR.UserID
                                  FROM      dbo.syUsersRolesCampGrps USR
                                           ,tmPermissions TMP
                                  WHERE     USR.RoleId = TMP.RoleId
                                            AND USR.CampGrpId IN ( SELECT DISTINCT
                                                                            campgrpId
                                                                   FROM     dbo.syCmpGrpCmps
                                                                   WHERE    CampusId IN ( SELECT    CampusID
                                                                                          FROM      dbo.syCmpGrpCmps
                                                                                          WHERE     CampGrpId = TMP.CampGrpId ) )
                                            AND TMP.UserID = @UserID );

    END;






GO
