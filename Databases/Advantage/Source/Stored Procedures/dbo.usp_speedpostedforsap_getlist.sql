SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_speedpostedforsap_getlist]
    @SAPDetailId UNIQUEIDENTIFIER
   ,@StuEnrollId UNIQUEIDENTIFIER
AS
    SELECT  t1.*
           ,t2.Speed AS MentorSpeed
           ,t3.accuracy AS RequiredAccuracy
    FROM    arPostScoreDictationSpeedTest t1
           ,arSAP_ShortHandSkillRequirement t2
           ,arSAPDetails t3
    WHERE   t1.grdcomponenttypeid = t2.GrdComponentTypeId
            AND t2.SAPDetailId = t3.SAPDetailId
            AND t1.speed >= t2.Speed
            AND t2.SAPDetailId = @SAPDetailId
            AND t1.stuenrollid = @StuEnrollId;



GO
