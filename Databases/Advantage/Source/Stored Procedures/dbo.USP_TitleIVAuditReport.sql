SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




--Query to fetch the Total Disbursed amount

CREATE PROCEDURE [dbo].[USP_TitleIVAuditReport]
    (
     @CampusID VARCHAR(50)
    ,@FromDate DATETIME
    ,@ToDate DATETIME
    )
AS --Modified Union to Union All
--Added CreateDate from trnansaction table and removed union All
--The transaction should not be a reversed transaction
--CASt added for convert to datetime of student status
--Externship Status Addded
--And only The awards shown in the reprot is picked
 
 --DECLARE @CampusID VARCHAR(50),@FromDate DATETIME,@ToDate DATETIME
 --SET @CampusID='3F5E839A-589A-4B2A-B258-35A1A8B3B819'
 --SET @FromDate='1/1/2014'
 --SET @ToDate='12/31/2014'


    SELECT  SSN
           ,LastName
           ,FirstName
           ,MiddleName
           ,StuEnrollId
           ,PrgVerDescrip
           ,SUM(PellDisb) PELLDisb
           ,SUM(PellRefund) PellRefund
           ,SUM(DLSUBDisb) DlSubDisb
           ,SUM(DLSUBRefund) DlSubRefund
           ,SUM(DLUNSUBDisb) DlUnsubDisb
           ,SUM(DLUNSUBRefund) DlUnsubRefund
           ,SUM(SEOGDisb) SEOGDisb
           ,SUM(SEOGRefund) SEOGRefund
           ,SUM(FWSDisb) FWSDisb
           ,SUM(FWSRefund) FWSRefund
           ,SUM(DLPLUSDisb) DLPLUSDisb
           ,SUM(DLPLUSRefund) DLPLUSRefund
           ,SUM(FFELPLUSDisb) FFELPLUSDisb
           ,SUM(FFELPLUSRefund) FFELPLUSRefund
           ,SUM(FFELSUBDisb) FFELSUBDisb
           ,SUM(FFELSUBRefund) FFELSUBRefund
           ,SUM(FFELUNSUBDisb) FFELUNSUBDisb
           ,SUM(FFELUNSUBRefund) FFELUNSUBRefund
           ,SUM(PERKINSDisb) PERKINSDisb
           ,SUM(PERKINSRefund) PERKINSRefund
           ,StatusAtYearEnd
           ,SystemStatus
           ,CampDescrip
    FROM    (
              SELECT    SSN
                       ,LastName
                       ,FirstName
                       ,MiddleName
                       ,StuEnrollId
                       ,PrgVerDescrip
                       ,PellDisb = ( CASE AdvFundSourceId
                                       WHEN 2 THEN TotalDisb
                                     END )
                       ,PellRefund = ( CASE AdvFundSourceId
                                         WHEN 2 THEN TotalRefund
                                       END )
                       ,DLSUBDisb = ( CASE AdvFundSourceId
                                        WHEN 7 THEN TotalDisb
                                      END )
                       ,DLSUBRefund = ( CASE AdvFundSourceId
                                          WHEN 7 THEN TotalRefund
                                        END )
                       ,DLUNSUBDisb = ( CASE WHEN (
                                                    AdvFundSourceId = 8
                                                    OR AdvFundSourceId = 21
                                                  ) THEN TotalDisb
                                        END )
                       ,DLUNSUBRefund = ( CASE WHEN (
                                                      AdvFundSourceId = 8
                                                      OR AdvFundSourceId = 21
                                                    ) THEN TotalRefund
                                          END )
                       ,SEOGDisb = ( CASE AdvFundSourceId
                                       WHEN 3 THEN TotalDisb
                                     END )
                       ,SEOGRefund = ( CASE AdvFundSourceId
                                         WHEN 3 THEN TotalRefund
                                       END )
                       ,FWSDisb = ( CASE AdvFundSourceId
                                      WHEN 5 THEN TotalDisb
                                    END )
                       ,FWSRefund = ( CASE AdvFundSourceId
                                        WHEN 5 THEN TotalRefund
                                      END )
                       ,DLPLUSDisb = ( CASE AdvFundSourceId
                                         WHEN 6 THEN TotalDisb
                                       END )
                       ,DLPLUSRefund = ( CASE AdvFundSourceId
                                           WHEN 6 THEN TotalRefund
                                         END )
                       ,FFELPLUSDisb = ( CASE AdvFundSourceId
                                           WHEN 9 THEN TotalDisb
                                         END )
                       ,FFELPLUSRefund = ( CASE AdvFundSourceId
                                             WHEN 9 THEN TotalRefund
                                           END )
                       ,FFELSUBDisb = ( CASE AdvFundSourceId
                                          WHEN 10 THEN TotalDisb
                                        END )
                       ,FFELSUBRefund = ( CASE AdvFundSourceId
                                            WHEN 10 THEN TotalRefund
                                          END )
                       ,FFELUNSUBDisb = ( CASE WHEN (
                                                      AdvFundSourceId = 11
                                                      OR AdvFundSourceId = 22
                                                    ) THEN TotalDisb
                                          END )
                       ,FFELUNSUBRefund = ( CASE WHEN (
                                                        AdvFundSourceId = 11
                                                        OR AdvFundSourceId = 22
                                                      ) THEN TotalRefund
                                            END )
                       ,PERKINSDisb = ( CASE AdvFundSourceId
                                          WHEN 4 THEN TotalDisb
                                        END )
                       ,PERKINSRefund = ( CASE AdvFundSourceId
                                            WHEN 4 THEN TotalRefund
                                          END )
                       ,DLAddUNSUBDisb = ( CASE AdvFundSourceId
                                             WHEN 21 THEN TotalDisb
                                           END )
                       ,DLAddUNSUBRefund = ( CASE AdvFundSourceId
                                               WHEN 21 THEN TotalRefund
                                             END )
                       ,FFELAddUNSUBDisb = ( CASE AdvFundSourceId
                                               WHEN 22 THEN TotalDisb
                                             END )
                       ,FFELAddUNSUBRefund = ( CASE AdvFundSourceId
                                                 WHEN 22 THEN TotalRefund
                                               END )
                       ,(
                          SELECT TOP 1
                                    SC.StatusCodeDescrip
                          FROM      dbo.syStudentStatusChanges SSS
                                   ,dbo.syStatusCodes SC
                          WHERE     StuEnrollId = Result1.StuEnrollId
                                    AND SSS.NewStatusId = SC.StatusCodeId
                               -- AND SSS.Moddate < = @ToDate
                                    AND CAST(CONVERT(VARCHAR,SSS.DateOfChange,101) AS DATETIME) <= CAST(CONVERT(VARCHAR,@ToDate,101) AS DATETIME)
                          ORDER BY  SSS.DateOfChange DESC
                        ) StatusAtYearEnd
                       ,(
                          SELECT TOP 1
                                    SysStatusId
                          FROM      dbo.syStudentStatusChanges SSS
                                   ,dbo.syStatusCodes SC
                          WHERE     StuEnrollId = Result1.StuEnrollId
                                    AND SSS.NewStatusId = SC.StatusCodeId
                              --  AND SSS.Moddate < = @ToDate
                                    AND CAST(CONVERT(VARCHAR,SSS.DateOfChange,101) AS DATETIME) <= CAST(CONVERT(VARCHAR,@ToDate,101) AS DATETIME)
                          ORDER BY  SSS.DateOfChange DESC
                        ) SystemStatus
                       ,(
                          SELECT TOP 1
                                    CampDescrip
                          FROM      dbo.syCampuses
                          WHERE     CampusId = @CampusID
                        ) CampDescrip
              FROM      (
                          SELECT    SSN
                                   ,LastName
                                   ,FirstName
                                   ,MiddleName
                                   ,StuEnrollId
                                   ,PrgVerDescrip
                                   ,FundSourceDescrip
                                   ,FundSourceId
                                   ,AdvFundSourceId
                                   ,SUM(TotalDisb) AS TotalDisb
                                   ,NULL AS TotalRefund
                          FROM      (
                                      SELECT    S.SSN
                                               ,S.LastName
                                               ,S.FirstName
                                               ,S.MiddleName
                                               ,SE.StuEnrollId
                                               ,PV.PrgVerDescrip
                                               ,FS.FundSourceDescrip
                                               ,T.FundSourceId
                                               ,-( T.TransAmount ) AS TotalDisb
                                               ,FS.AdvFundSourceId
                                               ,T.CreateDate
                                      FROM      saTransactions T
                                               ,dbo.saFundSources FS
                                               ,arStuEnrollments SE
                                               ,arStudent S
                                               ,dbo.arPrgVersions PV
                                               ,arPrograms P
                                      WHERE     T.FundSourceId = FS.FundSourceId
                                                AND T.Voided = 0
                                                AND T.StuEnrollId = SE.StuEnrollId
                                                AND SE.StudentId = S.StudentId
                                                AND SE.PrgVerId = PV.PrgVerId
                                                AND PV.ProgId = P.ProgId
                                                AND SE.CampusId = @CampusID
                                                AND T.TransDate >= @FromDate
                                                AND T.TransDate <= @ToDate
                                                AND FS.TitleIV = 1
                                                AND T.TransactionId NOT IN ( SELECT TransactionId
                                                                             FROM   dbo.saReversedTransactions )
                                                AND T.TransactionId NOT IN ( SELECT ReversedTransactionId
                                                                             FROM   dbo.saReversedTransactions )
												AND T.TransTypeId=2
                                    ) AS Result1
                          GROUP BY  SSN
                                   ,LastName
                                   ,FirstName
                                   ,MiddleName
                                   ,StuEnrollId
                                   ,PrgVerDescrip
                                   ,FundSourceDescrip
                                   ,FundSourceId
                                   ,AdvFundSourceId
                          UNION   
        
--Total refund AMount
                          SELECT    S.SSN
                                   ,S.LastName
                                   ,S.FirstName
                                   ,S.MiddleName
                                   ,SE.StuEnrollId
                                   ,PV.PrgVerDescrip
                                   ,FS.FundSourceDescrip
                                   ,FS.FundSourceId
                                   ,FS.AdvFundSourceId
                                   ,NULL AS TotalDisb
                                   ,( SUM(T.TransAmount) ) AS TotalRefund
                          FROM      saTransactions T
                                   ,saRefunds R
                                   ,dbo.saFundSources FS
                                   ,arStuEnrollments SE
                                   ,arStudent S
                                   ,dbo.arPrgVersions PV
                                   ,arPrograms P
                          WHERE     T.TransactionId = R.TransactionId
                                    AND R.FundSourceId = FS.FundSourceId
                                    AND T.StuEnrollId = SE.StuEnrollId
                                    AND SE.StudentId = S.StudentId
                                    AND SE.PrgVerId = PV.PrgVerId
                                    AND PV.ProgId = P.ProgId
                                    AND T.Voided = 0
                                    AND SE.CampusId = @CampusID
                                    AND T.TransDate >= @FromDate
                                    AND T.TransDate <= @ToDate
                                    AND FS.TitleIV = 1
                                    AND T.TransactionId NOT IN ( SELECT TransactionId
                                                                 FROM   dbo.saReversedTransactions )
                                    AND T.TransactionId NOT IN ( SELECT ReversedTransactionId
                                                                 FROM   dbo.saReversedTransactions )
                          GROUP BY  S.SSN
                                   ,S.LastName
                                   ,S.FirstName
                                   ,S.MiddleName
                                   ,SE.StuEnrollId
                                   ,PrgVerDescrip
                                   ,FS.FundSourceDescrip
                                   ,FS.FundSourceId
                                   ,FS.AdvFundSourceId
                        ) AS Result1
              WHERE     AdvFundSourceId IN ( 2,7,8,21,3,5,6,9,10,11,22,4 )
            ) AS FinalResult
   -- WHERE   SystemStatus IN ( 7, 9, 10, 11, 21, 14, 20, 12, 19, 22 )
    GROUP BY SSN
           ,LastName
           ,FirstName
           ,MiddleName
           ,StuEnrollId
           ,PrgVerDescrip
           ,StatusAtYearEnd
           ,SystemStatus
           ,CampDescrip
    ORDER BY --PrgVerDescrip ,
            LastName
           ,FirstName;




GO
