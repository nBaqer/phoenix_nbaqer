SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE PROCEDURE [dbo].[USP_IL_GetAdvImportLeadsFields]
AS
    SELECT  ILFieldId
           ,FldName
           ,Caption
    FROM    adImportLeadsFields
    ORDER BY Caption;








GO
