SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_GetInActiveFERPACategories] @campusId VARCHAR(50)
AS
    SET NOCOUNT ON; 
    SELECT  FC.FERPACategoryID
           ,FC.FERPACategoryCode
           ,FC.FERPACategoryDescrip
           ,FC.StatusId
           ,FC.CampGrpId
           ,ST.StatusId
           ,ST.Status
    FROM    arFERPACategory FC
           ,syStatuses ST
    WHERE   FC.statusid = ST.StatusId
            AND ST.Status = 'InActive'
            AND FC.CampGrpId IN ( SELECT    CampGrpId
                                  FROM      syCmpGrpCmps
                                  WHERE     CampusId = @campusId )
    ORDER BY FC.FerpaCategoryDescrip;



GO
