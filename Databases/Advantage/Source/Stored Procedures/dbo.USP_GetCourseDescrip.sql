SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_GetCourseDescrip]
    (
     @reqId UNIQUEIDENTIFIER
    )
AS
    SET NOCOUNT ON;
    SELECT  Descrip
    FROM    arReqs
    WHERE   Reqid = @reqId;
 



GO
