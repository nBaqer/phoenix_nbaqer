SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_GetAllActiveAndInActiveShifts]
AS
    SET NOCOUNT ON;
    SELECT  T.Shiftid
           ,T.ShiftDescrip
           ,1 AS Status
    FROM    arShifts T
           ,syStatuses S
    WHERE   T.StatusId = S.StatusId
    ORDER BY T.ShiftDescrip; 






GO
