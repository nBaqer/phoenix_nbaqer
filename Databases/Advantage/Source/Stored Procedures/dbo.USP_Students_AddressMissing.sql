SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_Students_AddressMissing] @CampusId VARCHAR(50)
AS
    DECLARE @SettingId INT
       ,@SettingValue VARCHAR(50);
    SET @SettingId = (
                       SELECT   SettingId
                       FROM     dbo.syConfigAppSettings
                       WHERE    LOWER(KeyName) = 'studentidentifier'
                     );
    SET @SettingValue = (
                          SELECT TOP 1
                                    Value
                          FROM      dbo.syConfigAppSetValues
                          WHERE     SettingId = @SettingId
                        );

    PRINT @SettingValue;

    SELECT DISTINCT
            CASE WHEN LOWER(@SettingValue) = 'ssn' THEN CASE WHEN LEN(S.SSN) = 9 THEN '***-**-' + SUBSTRING(S.SSN,6,4)
                                                             ELSE S.SSN
                                                        END
                 ELSE S.StudentNumber
            END AS StudentID
           ,S.LastName
           ,S.MiddleName
           ,S.FirstName
           ,CASE WHEN SA.Address1 IS NULL THEN 'X'
                 ELSE ''
            END AS Address1Missing
           ,CASE WHEN SA.City IS NULL THEN 'X'
                 ELSE ''
            END AS CityMissing
           ,CASE WHEN SA.StateId IS NULL THEN 'X'
                 ELSE ''
            END AS StateMissing
           ,CASE WHEN (
                        S.FirstName IS NULL
                        OR S.LastName IS NULL
                      ) THEN 'X'
                 ELSE ''
            END AS NameMissing
           ,CASE WHEN (
                        S.SSN IS NULL
                        OR LEN(ISNULL(S.SSN,0)) < 9
                      ) THEN 'X'
                 ELSE ''
            END AS SSNMissing
           ,CASE WHEN (
                        SA.Zip IS NULL
                        OR (
                             SA.Zip IS NOT NULL
                             AND ( LEN(SA.Zip) < 5 )
                           ) -- If Zip less than 5 digits
                        OR (
                             SA.Zip IS NOT NULL
                             AND (
                                   (
                                     LEN(SA.Zip) > 5
                                     AND LEN(SA.Zip) < 9
                                   )
                                   OR LEN(SA.Zip) > 9
                                 )
                           ) -- if zip is not 9 characters long
                        OR (
                             SA.Zip IS NOT NULL
                             AND LEN(SA.Zip) >= 5
                             AND PATINDEX('%[A-Z]%',SUBSTRING(SA.Zip,1,5)) > 0
                           )
                      ) THEN 'X'
                 ELSE ''
            END AS ZipMissing
    FROM    arStudent S
    INNER JOIN (
                 SELECT *
                       ,ROW_NUMBER() OVER ( PARTITION BY StudentId ORDER BY default1 DESC, ModDate DESC ) AS RowNumber
                 FROM   dbo.arStudAddresses
               ) SA ON SA.StudentId = S.StudentId
    INNER JOIN dbo.arStuEnrollments SE ON S.StudentId = SE.StudentId
    INNER JOIN dbo.arPrgVersions PV ON PV.PrgVerId = SE.PrgVerId
    INNER JOIN dbo.arPrograms P ON P.ProgId = PV.ProgId
    WHERE   SE.CampusId = @CampusId
            AND SA.RowNumber = 1 -- If there are multiple addresses bring the default address or the latest one
            AND SA.ForeignZip = 0
            AND P.Is1098T = 1
            AND (
                  SA.Address1 IS NULL
                  OR SA.Zip IS NULL
                  OR (
                       SA.Zip IS NOT NULL
                       AND ( LEN(SA.Zip) < 5 )
                     ) -- If Zip less than 5 digits
                  OR (
                       SA.Zip IS NOT NULL
                       AND (
                             (
                               LEN(SA.Zip) > 5
                               AND LEN(SA.Zip) < 9
                             )
                             OR LEN(SA.Zip) > 9
                           )
                     ) -- if zip is not 9 characters long
                  OR (
                       SA.Zip IS NOT NULL
                       AND LEN(SA.Zip) >= 5
                       AND PATINDEX('%[A-Z]%',SUBSTRING(SA.Zip,1,5)) > 0
                     ) -- if there is a character in the first 5 positions
                  OR SA.City IS NULL
                  OR SA.StateId IS NULL
                  OR (
                       S.SSN IS NULL
                       OR LEN(ISNULL(S.SSN,0)) < 9
                     )
                  OR (
                       S.FirstName IS NULL
                       OR S.LastName IS NULL
                     )
                )
            --AND S.StudentStatus = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
    ORDER BY S.LastName; 
GO
