SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[USP_AR_SetPayPeriods]
    (
     @PrgVerId AS UNIQUEIDENTIFIER = NULL
    )
AS /*----------------------------------------------------------------------------------------------------
	Author          :	Bruce Shanblatt
    
    Create date		:	11/18/2010
    
	Procedure Name	:	[USP_AR_SetPayPeriods]

	Objective		:	Global and Group Pay Period Table Updates
	
	Parameters		:	Name			Type	Data Type			Required? 	
						=====			====	=========			=========	
						@PrgVerId   	In		Uniqueidentifier	Required
	
	Output			:	updates the arStuPayPeriod table
						
*/-----------------------------------------------------------------------------------------------------

    BEGIN		

        DECLARE @RowsToProcess INT; 
        DECLARE @CurrentRow INT; 
        DECLARE @SelectCol1 UNIQUEIDENTIFIER;

        DECLARE @updstupayperiod TABLE
            (
             RowID INT NOT NULL
                       PRIMARY KEY
                       IDENTITY(1,1)
            ,stuenrollid UNIQUEIDENTIFIER
            );

        IF @PrgVerId IS NULL
            BEGIN
                INSERT  INTO @updstupayperiod
                        (
                         stuenrollid
                        )
                        SELECT  stuenrollid
                        FROM    dbo.arStuEnrollments;
            END;
        ELSE
            INSERT  INTO @updstupayperiod
                    (
                     stuenrollid
                    )
                    SELECT  stuenrollid
                    FROM    arstuenrollments
                    WHERE   prgverid = @PrgVerId;


        SET @RowsToProcess = @@ROWCOUNT; 
 
        SET @CurrentRow = 0; 
        WHILE @CurrentRow < @RowsToProcess
            BEGIN 
                SET @CurrentRow = @CurrentRow + 1; 
                SELECT  @SelectCol1 = stuenrollid
                FROM    @updstupayperiod
                WHERE   RowID = @CurrentRow;      
                EXEC dbo.USP_AR_StuEnrollPayPeriods @SelectCol1;
            END;
    END;




GO
