SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_GetClsSectMeetingsFromClsSectionwithPeriods]
    (
     @ClsSectionID UNIQUEIDENTIFIER 
       
    )
AS
    BEGIN 

        SELECT  CSM.ClsSectMeetingId
               ,RM.Descrip + ' - ' + PR.PeriodDescrip + ' - ' + ACT.InstructionTypeDescrip + '( ' + CONVERT(NVARCHAR(30),CSM.StartDate,107) + '-'
                + CONVERT(NVARCHAR(30),CSM.EndDate,107) + ')' AS Descrip
               ,CSM.StartDate
               ,CSM.EndDate
        FROM    dbo.arClsSectMeetings CSM
               ,arRooms RM
               ,SyPeriods PR
               ,dbo.arInstructionType ACT
        WHERE   CSM.RoomId = RM.RoomId
                AND CSM.PeriodId = PR.PeriodId
                AND CSM.InstructionTypeID = ACT.InstructionTypeID
                AND CSM.ClsSectionId = @ClsSectionID;

        --SELECT  
        --dbo.arClsSectMeetings.ClsSectionId,
        --dbo.arClsSectMeetings.ClsSectMeetingId,
        --dbo.arClsSectMeetings.InstructionTypeID ,
        --        dbo.arInstructionType.InstructionTypeDescrip
        --FROM    dbo.arClsSectMeetings ,
        --        dbo.arInstructionType
        --WHERE   dbo.arClsSectMeetings.InstructionTypeID = dbo.arInstructionType.InstructionTypeID
        --        AND dbo.arClsSectMeetings.ClsSectionId = @ClsSectionID 
 
    END;



GO
