SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[USP_SA_Get1098TTransactionValidEnrollmentForTaxYear]
	-- Add the parameters for the stored procedure here
    @TaxYear VARCHAR(4) = 2015
   ,@EnrollId VARCHAR(MAX) = ''
AS
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;

        SELECT DISTINCT
                t1.TransDate
               ,t1.TransAmount AS TransAmount
               ,t2.TransCodeCode
               ,t3.EnrollmentId
               ,A5.AwardTypeId AS PayType
        FROM    saTransactions t1
        JOIN    saTransCodes t2 ON t1.TransCodeId = t2.TransCodeId
        JOIN    arStuEnrollments t3 ON t3.StuEnrollId = t1.StuEnrollId
        JOIN    saPmtDisbRel A2 ON A2.TransactionId = t1.TransactionId
        LEFT JOIN faStudentAwardSchedule A3 ON A3.AwardScheduleId = A2.AwardScheduleId
        LEFT JOIN faStudentAwards A4 ON A4.StudentAwardId = A3.StudentAwardId
        LEFT JOIN dbo.saFundSources A5 ON A5.FundSourceId = A4.AwardTypeId
        WHERE   t1.Voided = 0
                AND YEAR(TransDate) = @TaxYear
                AND (
                      @EnrollId = ''
                      OR t3.EnrollmentId IN ( @EnrollId )
                    );
    END;

GO
