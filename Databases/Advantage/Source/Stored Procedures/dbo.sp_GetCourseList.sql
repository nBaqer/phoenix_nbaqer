SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
     
       
     
CREATE PROCEDURE [dbo].[sp_GetCourseList]
AS
    BEGIN  
      
        SET NOCOUNT ON;  
      
        SELECT  DISTINCT
                Campus.CampDescrip AS Campus
               ,PrgGrp.PrgGrpDescrip AS Program
               ,Req.Code
               ,Req.Descrip AS Course
               ,Degree.DegreeCode AS Degree
        FROM    dbo.arReqs Req
        INNER JOIN dbo.arClassSections CSection ON Req.ReqId = CSection.ReqId
        INNER JOIN dbo.syCampuses Campus ON CSection.CampusId = Campus.CampusId
        INNER JOIN dbo.arProgVerDef PrVerDef ON CSection.ReqId = PrVerDef.ReqId
        INNER JOIN dbo.arPrgVersions PrgVer ON PrVerDef.PrgVerId = PrgVer.PrgVerId
                                               AND Req.CampGrpId = PrgVer.CampGrpId
        INNER JOIN dbo.arDegrees Degree ON PrgVer.DegreeId = Degree.DegreeId
        INNER JOIN arPrgGrp PrgGrp ON PrgVer.PrgGrpId = PrgGrp.PrgGrpId
        WHERE   Req.ReqTypeId = 1 --Test  
                AND Req.StatusId = (
                                     SELECT StatusId
                                     FROM   dbo.syStatuses
                                     WHERE  Status = 'Active'
                                   )
        ORDER BY Campus.CampDescrip
               ,PrgGrp.PrgGrpDescrip
               ,Req.Code;  
      
    END;  
      
GO
