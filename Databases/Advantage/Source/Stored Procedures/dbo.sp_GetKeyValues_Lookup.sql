SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- =============================================
-- Author: Dennis Dreissigmayer
-- Create date: 9/22/09
-- Description: Retrieves the App Config Value Lookups for the SettingId Provided.
-- =============================================
CREATE PROCEDURE [dbo].[sp_GetKeyValues_Lookup]
    --Set up the parameters
    @Setting AS INT
AS
    BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
        SET NOCOUNT ON;



        SELECT  *
        FROM    syConfigAppSet_Lookup
        WHERE   SettingId = @Setting
        ORDER BY ValueOptions;

    END; 



GO
