SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[USP_BuildDataForExport]
    @AwardYear VARCHAR(8)
   ,@CampusId VARCHAR(50)
   ,@ProgramId VARCHAR(8000) = NULL
AS
    DECLARE @StudentId UNIQUEIDENTIFIER;
    DECLARE @InstitutionCode VARCHAR(6);
    DECLARE @CIPCode VARCHAR(6);
    DECLARE @CredentialLevel VARCHAR(2);
	
	
    SET @StudentId = NULL;
    SET @InstitutionCode = NULL;
    SET @CIPCode = NULL;
    SET @CredentialLevel = '01';	
	
	
    DECLARE @StudentStartDate DATETIME;
    DECLARE @StudentEndDate DATETIME;  

    SET @StudentStartDate = '07/01/' + LTRIM(RTRIM(SUBSTRING(@AwardYear,1,4))); 
    SET @StudentEndDate = '06/30/' + LTRIM(RTRIM(SUBSTRING(@AwardYear,5,4))); 

    SELECT  AwardYear AS [Award Year]
           ,SSN AS [Student Social Security Number]
           ,FirstName AS [Student First Name]
           ,MiddleName AS [Student Middle Name]
           ,LastName AS [Student Last Name]
           ,DOB AS [Student Date of Birth]
           ,InstitutionCode AS [Institution Code (OPEID)]
           ,InstitutionName AS [Institution Name]
           ,ProgramName AS [Program Name]
           ,CIPCode AS [CIP Code]
           ,CASE WHEN CredentialLevel IS NULL THEN NULL
                 ELSE CredentialLevel
            END AS [Credential Level]
           ,MedicalorDentalorInternshiporResidency AS [Medical or Dental Internship or Residency]
           ,ProgramAttendanceBeginDate AS [Program Attendance Begin Date]
           ,ProgramAttendanceBeginDateInAwardYear AS [Program Attendance Begin Date for This Award Year]
           ,ProgramAttendanceStatusDuringAwardYear AS [Program Attendance Status During Award Year]
           ,ProgramAttendanceStatusDate AS [Program Attendance Status Date]
           ,CASE WHEN (
                        ProgramAttendanceStatus = 'E'
                        AND PrivateLoanAmount = 0
                      ) THEN 0
                 ELSE CONVERT(INT,ROUND(PrivateLoanAmount,0))
            END AS [Private Loans Amount]
           ,CASE WHEN CAST(ROUND(InstitutionalDebt,0) AS INT) < 0 THEN 0
                 ELSE CAST(ROUND(InstitutionalDebt,0) AS INT)
            END AS [Institutional Debt]
           ,CONVERT(INT,ROUND(TuitionAndFeesAmount,0)) AS [Tuition and Fees Amount]
           ,CASE WHEN CAST(ROUND(AllowanceForBooksSuppliesAndEquipment,0) AS INT) < 0 THEN 0
                 ELSE CAST(ROUND(AllowanceForBooksSuppliesAndEquipment,0) AS INT)
            END AS [Allowance For Books, Supplies, and Equipment]
           ,LengthOfGEProgram AS [Length of GE Program]
           ,LengthOfGEProgramMeasurement AS [Length of GE Program Measurement]
           ,StudentEnrollmentStatusAsOfThe1stDayOfEnrollmentInProgram AS [Students Enrollment Status 1st Day Enrollment in Program]
           ,RowNumber
    FROM    (
              SELECT    AwardYear
                       ,SSN
                       ,FirstName
                       ,MiddleName
                       ,LastName
                       ,DOB
                       ,InstitutionCode
                       ,InstitutionName
                       ,ProgramName
                       ,CIPCode
                       ,CASE WHEN CredentialLevel IS NULL THEN NULL
                             ELSE CredentialLevel
                        END AS CredentialLevel
                       ,MedicalorDentalorInternshiporResidency
                       ,FFELDL
                       ,ProgramAttendanceBeginDate
                       ,ProgramAttendanceBeginDateInAwardYear
                       ,ProgramAttendanceStatusDuringAwardYear
                       ,ProgramAttendanceStatusDate
                       ,ProgramAttendanceStatus
                       ,CASE WHEN (
                                    ProgramAttendanceStatus = 'C'
                                    OR ProgramAttendanceStatus = 'W'
                                  ) THEN (
                                           SELECT   ISNULL(SUM(SA.GrossAmount),0)
                                           FROM     faStudentAwards SA
                                                   ,saFundSources FS
                                           WHERE    SA.StuEnrollId = DT.StuEnrollId
                                                    AND FS.FundSourceId = SA.AwardTypeId
                                                    AND FS.TitleIV = 0
                                                    AND FS.AwardTypeId = 2
                                         )
                             ELSE 0--NULL 
                        END AS PrivateLoanAmount
                       ,InstitutionalDebt
                       ,TuitionAndFeesAmount
                       ,AllowanceForBooksSuppliesAndEquipment
                       ,LengthOfGEProgram
                       ,LengthOfGEProgramMeasurement
                       ,StudentEnrollmentStatusAsOfThe1stDayOfEnrollmentInProgram
                       ,
						--Must be N or Blank if Program Attendance Status Equals 'E'                      
                        StuEnrollId
                       ,StudentId
                       ,ProgId
                       ,CampusId
                       ,RowNumber
                       ,CurrentBalance
                       ,TotalAidDue
              FROM      (
                          SELECT    @AwardYear AS AwardYear
                                   ,SSN
                                   ,FirstName
                                   ,MiddleName
                                   ,LastName
                                   ,CASE WHEN DOB IS NOT NULL THEN DOB
                                         ELSE '01/01/1900'
                                    END AS DOB
                                   ,t3.OPEID AS InstitutionCode
                                   ,t3.SchoolName AS InstitutionName
                                   ,t5.ProgDescrip AS ProgramName
                                   ,t5.CIPCode AS CIPCode
                                   ,                                    
									-- Get the credential value acording with mapping values syRptAgencyFldValues.
                                    CASE WHEN t5.CredentialLvlId IS NULL THEN NULL
                                         ELSE (
                                                SELECT  SRAFV.AgencyValue
                                                FROM    dbo.syRptAgencyFldValues AS SRAFV
                                                INNER JOIN dbo.arProgCredential AS APC ON APC.GERptAgencyFldValId = SRAFV.RptAgencyFldValId
                                                WHERE   APC.CredentialId = t5.CredentialLvlId
                                              )
                                    END AS CredentialLevel
                                   ,'N' AS MedicalorDentalorInternshiporResidency
                                   ,CASE WHEN (
                                                SELECT  COUNT(SA.GrossAmount)
                                                FROM    faStudentAwards SA
                                                INNER JOIN dbo.saFundSources FS ON SA.AwardTypeId = FS.FundSourceId
                                                INNER JOIN dbo.syAdvFundSources AFS ON FS.AdvFundSourceId = AFS.AdvFundSourceId
                                                WHERE   StuEnrollId = t2.StuEnrollId
                                                        AND AFS.AdvFundSourceId IN ( 7,8,10,11 ) --AND SAS.TransactionId IS NOT NULL 
                                                        AND FS.AwardTypeId IN ( 2 )
                                              ) >= 1 THEN 'Y'
                                         ELSE 'N'
                                    END AS FFELDL
                                   ,CASE WHEN (
                                                SELECT  COUNT(StuEnrollId)
                                                FROM    dbo.syStudentAttendanceSummary
                                              ) = 0 THEN (
                                                           SELECT   StartDate
                                                           FROM     dbo.arStuEnrollments
                                                           WHERE    StuEnrollId = t2.StuEnrollId
                                                         )
                                         ELSE (
                                                SELECT TOP 1
                                                        StudentAttendedDate
                                                FROM    syStudentAttendanceSummary
                                                WHERE   StuEnrollId = t2.StuEnrollId
                                                ORDER BY StuEnrollId
                                                       ,StudentAttendedDate
                                              )
                                    END AS ProgramAttendanceBeginDate
                                   ,CASE WHEN (
                                                SELECT  COUNT(StuEnrollId)
                                                FROM    dbo.syStudentAttendanceSummary
                                              ) = 0 THEN ( CASE WHEN (
                                                                       SELECT   StartDate
                                                                       FROM     dbo.arStuEnrollments
                                                                       WHERE    StuEnrollId = t2.StuEnrollId
                                                                     ) BETWEEN @StudentStartDate
                                                                       AND     @StudentEndDate THEN (
                                                                                                      SELECT    StartDate
                                                                                                      FROM      dbo.arStuEnrollments
                                                                                                      WHERE     StuEnrollId = t2.StuEnrollId
                                                                                                    )
                                                                ELSE @StudentStartDate
                                                           END )
                                         ELSE CASE WHEN (
                                                          SELECT TOP 1
                                                                    StudentAttendedDate
                                                          FROM      syStudentAttendanceSummary
                                                          WHERE     StuEnrollId = t2.StuEnrollId
                                                                    AND StudentAttendedDate >= @StudentStartDate
                                                                    AND StudentAttendedDate <= @StudentEndDate
                                                          ORDER BY  StuEnrollId
                                                                   ,StudentAttendedDate
                                                        ) IS NULL THEN @StudentStartDate
                                                   ELSE (
                                                          SELECT TOP 1
                                                                    StudentAttendedDate
                                                          FROM      syStudentAttendanceSummary
                                                          WHERE     StuEnrollId = t2.StuEnrollId
                                                                    AND StudentAttendedDate >= @StudentStartDate
                                                                    AND StudentAttendedDate <= @StudentEndDate
                                                          ORDER BY  StuEnrollId
                                                                   ,StudentAttendedDate
                                                        )
                                              END
                                    END AS ProgramAttendanceBeginDateInAwardYear
                                   ,
									--  if the student has been reenrolment (t2.ReEnrollmentDate  IS NOT NULL  )
									--                 and the difference between las Drop Date and reenrolmentdate >= 180 days
									--          so ProgramAttendanceStatusDuringAwardYear should be droped 'W'
                                    CASE WHEN (
                                                t2.ReEnrollmentDate IS NOT NULL
                                                AND DATEDIFF(DAY,
                                                             (
                                                               SELECT TOP 1
                                                                        SAHD.NewValue
                                                               FROM     dbo.syAuditHist AS SAH
                                                               INNER JOIN dbo.syAuditHistDetail AS SAHD ON SAHD.AuditHistId = SAH.AuditHistId
                                                               WHERE    SAH.TableName = 'arStuEnrollments'
                                                                        AND SAH.AuditHistId IN (
                                                                        SELECT  SAH1.AuditHistId
                                                                        FROM    dbo.syAuditHist AS SAH1
                                                                        INNER JOIN dbo.syAuditHistDetail AS SAHD1 ON SAHD1.AuditHistId = SAH1.AuditHistId
                                                                        INNER JOIN dbo.syStatusCodes AS SSC1 ON CONVERT(NVARCHAR(40),SSC1.StatusCodeId) = SAHD1.NewValue
                                                                        WHERE   SAH1.TableName = 'arStuEnrollments'
                                                                                AND SAHD1.ColumnName = 'StatusCodeId'
                                                                                AND SAHD1.RowId = t2.StuEnrollId
                                                                                AND SAHD1.NewValue IN ( SELECT  CONVERT(NVARCHAR(36),SSC.StatusCodeId)
                                                                                                        FROM    dbo.syStatusCodes AS SSC
                                                                                                        WHERE   SSC.SysStatusId = 12 ) )
                                                                        AND SAHD.ColumnName = 'DateDetermined'
                                                                        AND SAHD.RowId = t2.StuEnrollId
                                                               ORDER BY SAHD.NewValue DESC
                                                             ),t2.ReEnrollmentDate) <= 180
                                              ) THEN 'W'
                                         WHEN t7.SysStatusDescrip = 'Future start'
                                              AND (
                                                    t2.LDA <> NULL
                                                    OR t2.StartDate <> NULL
                                                  ) THEN 'E'
                                         WHEN t7.GEProgramStatus = 'C' THEN 'G'
                                         ELSE t7.GEProgramStatus
                                    END AS ProgramAttendanceStatusDuringAwardYear
                                   , 
									--  if the student has been reenrolment (t2.ReEnrollmentDate  IS NOT NULL  )
									--                 and the difference between las Drop Date and reenrolmentdate >= 180 days
									--        so Program Attendance Status Date should be the droping date
                                    CASE WHEN (
                                                t2.ReEnrollmentDate IS NOT NULL
                                                AND DATEDIFF(DAY,
                                                             (
                                                               SELECT TOP 1
                                                                        SAHD.NewValue
                                                               FROM     dbo.syAuditHist AS SAH
                                                               INNER JOIN dbo.syAuditHistDetail AS SAHD ON SAHD.AuditHistId = SAH.AuditHistId
                                                               WHERE    SAH.TableName = 'arStuEnrollments'
                                                                        AND SAH.AuditHistId IN (
                                                                        SELECT  SAH1.AuditHistId
                                                                        FROM    dbo.syAuditHist AS SAH1
                                                                        INNER JOIN dbo.syAuditHistDetail AS SAHD1 ON SAHD1.AuditHistId = SAH1.AuditHistId
                                                                        INNER JOIN dbo.syStatusCodes AS SSC1 ON CONVERT(NVARCHAR(40),SSC1.StatusCodeId) = SAHD1.NewValue
                                                                        WHERE   SAH1.TableName = 'arStuEnrollments'
                                                                                AND SAHD1.ColumnName = 'StatusCodeId'
                                                                                AND SAHD1.RowId = t2.StuEnrollId
                                                                                AND SAHD1.NewValue IN ( SELECT  CONVERT(NVARCHAR(36),SSC.StatusCodeId)
                                                                                                        FROM    dbo.syStatusCodes AS SSC
                                                                                                        WHERE   SSC.SysStatusId = 12 ) )
                                                                        AND SAHD.ColumnName = 'DateDetermined'
                                                                        AND SAHD.RowId = t2.StuEnrollId
                                                               ORDER BY SAHD.NewValue DESC
                                                             ),t2.ReEnrollmentDate) <= 180
                                              )
                                         THEN (
                                                SELECT TOP 1
                                                        SAHD.NewValue
                                                FROM    dbo.syAuditHist AS SAH
                                                INNER JOIN dbo.syAuditHistDetail AS SAHD ON SAHD.AuditHistId = SAH.AuditHistId
                                                WHERE   SAH.TableName = 'arStuEnrollments'
                                                        AND SAH.AuditHistId IN (
                                                        SELECT  SAH1.AuditHistId
                                                        FROM    dbo.syAuditHist AS SAH1
                                                        INNER JOIN dbo.syAuditHistDetail AS SAHD1 ON SAHD1.AuditHistId = SAH1.AuditHistId
                                                        INNER JOIN dbo.syStatusCodes AS SSC1 ON CONVERT(NVARCHAR(40),SSC1.StatusCodeId) = SAHD1.NewValue
                                                        WHERE   SAH1.TableName = 'arStuEnrollments'
                                                                AND SAHD1.ColumnName = 'StatusCodeId'
                                                                AND SAHD1.RowId = t2.StuEnrollId
                                                                AND SAHD1.NewValue IN ( SELECT  CONVERT(NVARCHAR(36),SSC.StatusCodeId)
                                                                                        FROM    dbo.syStatusCodes AS SSC
                                                                                        WHERE   SSC.SysStatusId = 12 ) )
                                                        AND SAHD.ColumnName = 'DateDetermined'
                                                        AND SAHD.RowId = t2.StuEnrollId
                                                ORDER BY SAHD.NewValue DESC
                                              )
                                         WHEN t7.SysStatusDescrip = 'Future start'
                                              AND (
                                                    t2.LDA <> NULL
                                                    OR t2.StartDate <> NULL
                                                  ) THEN @StudentEndDate
                                         WHEN t7.GEProgramStatus = 'C'
                                              OR t7.GEProgramStatus = 'G' THEN t2.ExpGradDate
                                         WHEN t7.GEProgramStatus = 'W' THEN t2.DateDetermined
                                         ELSE @StudentEndDate
                                    END AS ProgramAttendanceStatusDate
                                   ,																	
									--If the student status is set to completed check if the student graduated prior to award end date
									--if the student graduated after award end date (for ex:in FALL) then mark him as Enrolled
                                    CASE WHEN ( t7.GEProgramStatus = 'C' ) THEN CASE WHEN (
                                                                                            SELECT  COUNT(*)
                                                                                            FROM    arStuEnrollments
                                                                                            WHERE   StuEnrollId = t2.StuEnrollId
                                                                                                    AND ExpGradDate <= @StudentEndDate
                                                                                          ) >= 1 THEN 'C'
                                                                                     ELSE 'E'
                                                                                END
                                         ELSE CASE WHEN ( t7.GEProgramStatus = 'W' ) THEN CASE WHEN (
                                                                                                      SELECT    COUNT(*)
                                                                                                      FROM      arStuEnrollments
                                                                                                      WHERE     StuEnrollId = t2.StuEnrollId
                                                                                                                AND DateDetermined <= @StudentEndDate
                                                                                                    ) >= 1 THEN 'W'
                                                                                               ELSE 'E'
                                                                                          END
                                                   ELSE t7.GEProgramStatus
                                              END
                                    END AS ProgramAttendanceStatus
                                   ,t2.StuEnrollId
                                   ,t1.StudentId
                                   ,t5.ProgId
                                   ,t2.CampusId
                                   ,ROW_NUMBER() OVER ( ORDER BY t1.StudentId ) AS RowNumber
                                   ,(
                                      SELECT    SUM(ISNULL(TransAmount,0)) AS CurrentBalance
                                      FROM      saTransactions
                                      WHERE     StuEnrollId = t2.StuEnrollId
                                                AND Voided = 0
                                    ) AS CurrentBalance
                                   ,(
                                      SELECT    SUM(DT1.TotalAidDue) AS TotalAidDue
                                      FROM      (
                                                  SELECT    DT.*
                                                           ,CASE WHEN ( DT.RefundAmount = DT.ReceivedAmount ) -- If the amount paid was completly refunded it means disbursement is still owed
                                                                      THEN DT.ReceivedAmount
                                                                 ELSE CASE WHEN ( DT.RefundAmount < DT.ReceivedAmount ) -- If only partial refund was made
                                                                                THEN CASE WHEN ( DT.Amount > 0 )
                                                                                               AND ISNULL(( DT.ReceivedAmount - DT.RefundAmount ),0) > 0
                                                                                          THEN -- If the partial refund is less than disburement amount owed
                                                                                               ( DT.Amount - ISNULL(( DT.ReceivedAmount - DT.RefundAmount ),0) )
                                                                                          ELSE 0
                                                                                     END
                                                                           ELSE 0
                                                                      END
                                                            END AS TotalAidDue
                                                  FROM      (
                                                              SELECT    t4.*
                                                                       ,(
                                                                          SELECT DISTINCT
                                                                                    SUM(ISNULL(Amount,0))
                                                                          FROM      saPmtDisbRel P1
                                                                                   ,saTransactions P2
                                                                          WHERE     AwardScheduleId = t4.AwardScheduleId
                                                                                    AND P1.TransactionId = P2.TransactionId
                                                                                    AND P2.StuEnrollId = t5.StuEnrollId
                                                                                    AND P2.Voided = 0
                                                                        ) AS ReceivedAmount
                                                                       ,t6.RefundAmount
                                                              FROM      dbo.faStudentAwardSchedule t4
                                                              INNER JOIN dbo.faStudentAwards t5 ON t4.StudentAwardId = t5.StudentAwardId
                                                              LEFT OUTER JOIN (
                                                                                SELECT  *
                                                                                FROM    saRefunds
                                                                                WHERE   RefundAmount IS NOT NULL
                                                                              ) t6 ON t4.AwardScheduleId = t6.AwardScheduleId
                                                              WHERE     t5.StuEnrollId = t2.StuEnrollId
                                                                        AND
															-- This query will bring in disbursements that have been received and refund was posted
                                                                        t4.AwardScheduleId IN ( SELECT  t1.AwardScheduleId
                                                                                                FROM    saPmtDisbRel t1
                                                                                                       ,saRefunds t2
                                                                                                WHERE   t1.AwardScheduleId = t2.AwardScheduleId )
                                                            ) DT
                                                  UNION
                                                  SELECT    t4.*
                                                           ,NULL AS ReceivedAmount
                                                           ,NULL AS RefundAmount
                                                           ,t4.Amount AS DisbursementAmount
                                                  FROM      dbo.faStudentAwardSchedule t4
                                                  INNER JOIN dbo.faStudentAwards t5 ON t4.StudentAwardId = t5.StudentAwardId
                                                  WHERE     t5.StuEnrollId = t2.StuEnrollId
                                                            AND
												-- This query will bring in disbursements that have not been received
                                                            t4.AwardScheduleId NOT IN ( SELECT  t1.AwardScheduleId
                                                                                        FROM    saPmtDisbRel t1
                                                                                        WHERE   AwardScheduleId IS NOT NULL )
                                                ) DT1
                                    ) AS TotalAidDue
                                   ,CASE WHEN t7.SysStatusDescrip = 'Future start'
                                              AND (
                                                    t2.LDA <> NULL
                                                    OR t2.StartDate <> NULL
                                                  ) THEN 0
                                         WHEN t7.GEProgramStatus IN ( 'C','G' ) THEN (
                                                                                       SELECT   SUM(ISNULL(TransAmount,0))
                                                                                       FROM     dbo.saTransactions trn
                                                                                       INNER JOIN dbo.saTransCodes tc ON trn.TransCodeId = tc.TransCodeId
                                                                                       WHERE    trn.StuEnrollId = t2.StuEnrollId
                                                                                                AND IsInstCharge = 1
                                                                                                AND trn.Voided = 0
                                                                                                AND trn.TransDate <= t2.ExpGradDate
                                                                                     )
                                         WHEN t7.GEProgramStatus IN ( 'W' ) THEN (
                                                                                   SELECT   SUM(ISNULL(TransAmount,0))
                                                                                   FROM     dbo.saTransactions trn
                                                                                   INNER JOIN dbo.saTransCodes tc ON trn.TransCodeId = tc.TransCodeId
                                                                                   WHERE    trn.StuEnrollId = t2.StuEnrollId
                                                                                            AND IsInstCharge = 1
                                                                                            AND trn.Voided = 0
                                                                                            AND trn.TransDate <= t2.DateDetermined
                                                                                 )
                                         ELSE 0
                                    END AS InstitutionalDebt
                                   ,(
                                      SELECT    SUM(ISNULL(TransAmount,0))
                                      FROM      saTransactions T
                                      INNER JOIN saTransCodes TC ON T.TransCodeId = TC.TransCodeId
                                      INNER JOIN dbo.saSysTransCodes st ON TC.SysTransCodeId = st.SysTransCodeId
                                      WHERE     T.StuEnrollId = t2.StuEnrollId
                                                AND TC.IsInstCharge = 1
                                                AND T.TransTypeId IN ( 0,1 )
                                                AND st.SysTransCodeId IN ( 1,2,7,20 )
                                                AND T.Voided = 0
                                    ) AS TuitionAndFeesAmount
                                   ,CASE WHEN t7.SysStatusDescrip = 'Future start'
                                              AND (
                                                    t2.LDA <> NULL
                                                    OR t2.StartDate <> NULL
                                                  ) THEN 0
                                         WHEN t7.GEProgramStatus IN ( 'C','G','W' )
                                         THEN (
                                                SELECT  ISNULL(SUM(TransAmount),0)
                                                FROM    dbo.saTransactions t
                                                INNER JOIN dbo.saTransCodes c ON t.TransCodeId = c.TransCodeId
                                                INNER JOIN dbo.saSysTransCodes sc ON c.SysTransCodeId = sc.SysTransCodeId
                                                WHERE   t.StuEnrollId = t2.StuEnrollId
                                                        AND sc.Description IN ( 'Supplies','Books','Computer Equipment' )
                                                        AND Voided = 0
                                                        AND TransTypeId = 0
                                              )
                                         WHEN t7.GEProgramStatus = 'E' THEN NULL
                                         ELSE NULL
                                    END AS AllowanceForBooksSuppliesAndEquipment
                                   ,t4.Weeks AS LengthOfGEProgram
                                   ,'W' AS LengthOfGEProgramMeasurement
                                   ,(
                                      SELECT    SRAFV.AgencyValue
                                      FROM      syRptAgencyFldValues AS SRAFV
                                      INNER JOIN arAttendTypes AS AAT ON AAT.GERptAgencyFldValId = SRAFV.RptAgencyFldValId
                                      WHERE     AAT.AttendTypeId = t2.AttendTypeId
                                    ) AS StudentEnrollmentStatusAsOfThe1stDayOfEnrollmentInProgram
                          FROM      arStudent t1
                          INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                          INNER JOIN syCampuses t3 ON t2.CampusId = t3.CampusId
                          INNER JOIN arPrgVersions t4 ON t2.PrgVerId = t4.PrgVerId
                          INNER JOIN arPrograms t5 ON t4.ProgId = t5.ProgId
                          INNER JOIN syStatusCodes t6 ON t2.StatusCodeId = t6.StatusCodeId
                          INNER JOIN dbo.sySysStatus t7 ON t6.SysStatusId = t7.SysStatusId

									-- Next Join is to get the DateOfChange of stuatus when trigger works  instead use audit history and audit history detalils    
									--LEFT JOIN (
									--                 SELECT TOP 1 SSSC1.StuEnrollId, SSSC1.ModDate, SSSC1.DateOfChange 
									--                 FROM dbo.syStudentStatusChanges AS SSSC1
									--                     INNER JOIN dbo.syStatusCodes AS SSC1 ON SSSC1.NewStatusId = SSC1.StatusCodeId
									--                     INNER JOIN dbo.sySysStatus AS SSS1 ON SSS1.SysStatusId = SSC1.SysStatusId
									--                 WHERE SSC1.SysStatusId = 12   
									--                 ORDER BY SSSC1.ModDate DESC    
									--          ) AS SSSC ON SSSC.StuEnrollId = T2.StuEnrollId
                          WHERE     --(t2.StartDate>=@StudentStartDate AND t2.StartDate<=@StudentEndDate)
						-- brings in students whose start date is less than the report end date OR who have an LDA that is after the report start date.
						-- In Freedom - AND Condition is used, but in Advantage attendance is not tracked or posted immediatly, so OR condition is used.
                                    (
                                      t2.StartDate <= @StudentEndDate
                                      AND t2.LDA >= @StudentStartDate
                                    )
                                    AND t3.CampusId = @CampusId
                                    AND t1.SSN IS NOT NULL -- SSN should not be NULL
                                    AND t5.IsGEProgram = 1
                                    AND t7.SysStatusId <> 8
                                    AND -- No Start should be excluded
                                    (
                                      @ProgramId IS NULL
                                      OR t5.ProgId IN ( SELECT  Val
                                                        FROM    MultipleValuesForReportParameters(@ProgramId,',',1) )
                                    )
                                    AND t2.StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
								-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
									( SELECT DISTINCT
                                                t1.StuEnrollId
                                      FROM      arStuEnrollments t1
                                               ,syStatusCodes t2
                                      WHERE     t1.StatusCodeId = t2.StatusCodeId
                                                AND StartDate <= @StudentEndDate
                                                AND -- Student started before the end date range
                                                LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                                AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
									-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                                AND (
                                                      t1.DateDetermined < @StudentStartDate
                                                      OR ExpGradDate < @StudentStartDate
                                                      OR LDA < @StudentStartDate
                                                    ) )
                          UNION
						  --         This query is for same group of records selected before and also 
						  --         The student has been reenrolment (t2.ReEnrollmentDate  IS NOT NULL  )
						  --                      and the difference between las Drop Date and reenrolmentdate >= 180 days
                          SELECT    @AwardYear AS AwardYear
                                   ,SSN
                                   ,FirstName
                                   ,MiddleName
                                   ,LastName
                                   ,CASE WHEN DOB IS NOT NULL THEN DOB
                                         ELSE '01/01/1900'
                                    END AS DOB
                                   ,t3.OPEID AS InstitutionCode
                                   ,t3.SchoolName AS InstitutionName
                                   ,t5.ProgDescrip AS ProgramName
                                   ,t5.CIPCode AS CIPCode
                                   ,CASE WHEN t5.CredentialLvlId IS NULL THEN NULL
                                         ELSE (
                                                SELECT  SRAFV.AgencyValue
                                                FROM    dbo.syRptAgencyFldValues AS SRAFV
                                                INNER JOIN dbo.arProgCredential AS APC ON APC.GERptAgencyFldValId = SRAFV.RptAgencyFldValId
                                                WHERE   APC.CredentialId = t5.CredentialLvlId
                                              )
                                    END AS CredentialLevel
                                   ,'N' AS MedicalorDentalorInternshiporResidency
                                   ,CASE WHEN (
                                                SELECT  COUNT(SA.GrossAmount)
                                                FROM    faStudentAwards SA
                                                INNER JOIN dbo.saFundSources FS ON SA.AwardTypeId = FS.FundSourceId
                                                INNER JOIN dbo.syAdvFundSources AFS ON FS.AdvFundSourceId = AFS.AdvFundSourceId
                                                WHERE   StuEnrollId = t2.StuEnrollId
                                                        AND AFS.AdvFundSourceId IN ( 7,8,10,11 ) --AND SAS.TransactionId IS NOT NULL 
                                                        AND FS.AwardTypeId IN ( 2 )
                                              ) >= 1 THEN 'Y'
                                         ELSE 'N'
                                    END AS FFELDL
                                   ,CASE WHEN (
                                                SELECT  COUNT(StuEnrollId)
                                                FROM    dbo.syStudentAttendanceSummary
                                              ) = 0 THEN (
                                                           SELECT   StartDate
                                                           FROM     dbo.arStuEnrollments
                                                           WHERE    StuEnrollId = t2.StuEnrollId
                                                         )
                                         ELSE (
                                                SELECT TOP 1
                                                        StudentAttendedDate
                                                FROM    syStudentAttendanceSummary
                                                WHERE   StuEnrollId = t2.StuEnrollId
                                                ORDER BY StuEnrollId
                                                       ,StudentAttendedDate
                                              )
                                    END AS ProgramAttendanceBeginDate
                                   ,
									--( SELECT TOP 1
									--            StudentAttendedDate
									--  FROM      syStudentAttendanceSummary
									--  WHERE     StuENrollId = t2.StuEnrollId
									--            AND StudentAttendedDate >= @StudentStartDate
									--            AND StudentAttendedDate <= @StudentEndDate
									--  ORDER BY  StuEnrollId ,
									--            StudentAttendedDate
									--) AS ProgramAttendanceBeginDateInAwardYear ,
									
									-- as this record belong for reenrollment student in less than 180 days so  Program Attendance Begin Date In Award Year
                                    t2.ReEnrollmentDate AS ProgramAttendanceBeginDateInAwardYear
                                   ,CASE WHEN t7.SysStatusDescrip = 'Future start'
                                              AND (
                                                    t2.LDA <> NULL
                                                    OR t2.StartDate <> NULL
                                                  ) THEN 't'
                                         WHEN t7.GEProgramStatus = 'C' THEN 'G'
                                         ELSE t7.GEProgramStatus
                                    END AS ProgramAttendanceStatusDuringAwardYear
                                   ,CASE WHEN t7.SysStatusDescrip = 'Future start'
                                              AND (
                                                    t2.LDA <> NULL
                                                    OR t2.StartDate <> NULL
                                                  ) THEN @StudentEndDate
                                         WHEN t7.GEProgramStatus = 'C'
                                              OR t7.GEProgramStatus = 'G' THEN t2.ExpGradDate
                                         WHEN t7.GEProgramStatus = 'W' THEN t2.DateDetermined
                                         ELSE @StudentEndDate
                                    END AS ProgramAttendanceStatusDate
                                   ,																		
									--If the student status is set to completed check if the student graduated prior to award end date
									--if the student graduated after award end date (for ex:in FALL) then mark him as Enrolled
                                    CASE WHEN ( t7.GEProgramStatus = 'C' ) THEN CASE WHEN (
                                                                                            SELECT  COUNT(*)
                                                                                            FROM    arStuEnrollments
                                                                                            WHERE   StuEnrollId = t2.StuEnrollId
                                                                                                    AND ExpGradDate <= @StudentEndDate
                                                                                          ) >= 1 THEN 'C'
                                                                                     ELSE 'E'
                                                                                END
                                         ELSE CASE WHEN ( t7.GEProgramStatus = 'W' ) THEN CASE WHEN (
                                                                                                      SELECT    COUNT(*)
                                                                                                      FROM      arStuEnrollments
                                                                                                      WHERE     StuEnrollId = t2.StuEnrollId
                                                                                                                AND DateDetermined <= @StudentEndDate
                                                                                                    ) >= 1 THEN 'W'
                                                                                               ELSE 'E'
                                                                                          END
                                                   ELSE t7.GEProgramStatus
                                              END
                                    END AS ProgramAttendanceStatus
                                   ,t2.StuEnrollId
                                   ,t1.StudentId
                                   ,t5.ProgId
                                   ,t2.CampusId
                                   ,ROW_NUMBER() OVER ( ORDER BY t1.StudentId ) AS RowNumber
                                   ,(
                                      SELECT    SUM(ISNULL(TransAmount,0)) AS CurrentBalance
                                      FROM      saTransactions
                                      WHERE     StuEnrollId = t2.StuEnrollId
                                                AND Voided = 0
                                    ) AS CurrentBalance
                                   ,(
                                      SELECT    SUM(DT1.TotalAidDue) AS TotalAidDue
                                      FROM      (
                                                  SELECT    DT.*
                                                           ,CASE WHEN ( DT.RefundAmount = DT.ReceivedAmount ) -- If the amount paid was completly refunded it means disbursement is still owed
                                                                      THEN DT.ReceivedAmount
                                                                 ELSE CASE WHEN ( DT.RefundAmount < DT.ReceivedAmount ) -- If only partial refund was made
                                                                                THEN CASE WHEN ( DT.Amount > 0 )
                                                                                               AND ISNULL(( DT.ReceivedAmount - DT.RefundAmount ),0) > 0
                                                                                          THEN -- If the partial refund is less than disburement amount owed
                                                                                               ( DT.Amount - ISNULL(( DT.ReceivedAmount - DT.RefundAmount ),0) )
                                                                                          ELSE 0
                                                                                     END
                                                                           ELSE 0
                                                                      END
                                                            END AS TotalAidDue
                                                  FROM      (
                                                              SELECT    t4.*
                                                                       ,(
                                                                          SELECT DISTINCT
                                                                                    SUM(ISNULL(Amount,0))
                                                                          FROM      saPmtDisbRel P1
                                                                                   ,saTransactions P2
                                                                          WHERE     AwardScheduleId = t4.AwardScheduleId
                                                                                    AND P1.TransactionId = P2.TransactionId
                                                                                    AND P2.StuEnrollId = t5.StuEnrollId
                                                                                    AND P2.Voided = 0
                                                                        ) AS ReceivedAmount
                                                                       ,t6.RefundAmount
                                                              FROM      dbo.faStudentAwardSchedule t4
                                                              INNER JOIN dbo.faStudentAwards t5 ON t4.StudentAwardId = t5.StudentAwardId
                                                              LEFT OUTER JOIN (
                                                                                SELECT  *
                                                                                FROM    saRefunds
                                                                                WHERE   RefundAmount IS NOT NULL
                                                                              ) t6 ON t4.AwardScheduleId = t6.AwardScheduleId
                                                              WHERE     t5.StuEnrollId = t2.StuEnrollId
                                                                        AND
															-- This query will bring in disbursements that have been received and refund was posted
                                                                        t4.AwardScheduleId IN ( SELECT  t1.AwardScheduleId
                                                                                                FROM    saPmtDisbRel t1
                                                                                                       ,saRefunds t2
                                                                                                WHERE   t1.AwardScheduleId = t2.AwardScheduleId )
                                                            ) DT
                                                  UNION
                                                  SELECT    t4.*
                                                           ,NULL AS ReceivedAmount
                                                           ,NULL AS RefundAmount
                                                           ,t4.Amount AS DisbursementAmount
                                                  FROM      dbo.faStudentAwardSchedule t4
                                                  INNER JOIN dbo.faStudentAwards t5 ON t4.StudentAwardId = t5.StudentAwardId
                                                  WHERE     t5.StuEnrollId = t2.StuEnrollId
                                                            AND
												-- This query will bring in disbursements that have not been received
                                                            t4.AwardScheduleId NOT IN ( SELECT  t1.AwardScheduleId
                                                                                        FROM    saPmtDisbRel t1
                                                                                        WHERE   AwardScheduleId IS NOT NULL )
                                                ) DT1
                                    ) AS TotalAidDue
                                   ,CASE WHEN t7.SysStatusDescrip = 'Future start'
                                              AND (
                                                    t2.LDA <> NULL
                                                    OR t2.StartDate <> NULL
                                                  ) THEN 0
                                         WHEN t7.GEProgramStatus IN ( 'C','G' ) THEN (
                                                                                       SELECT   SUM(ISNULL(TransAmount,0))
                                                                                       FROM     dbo.saTransactions trn
                                                                                       INNER JOIN dbo.saTransCodes tc ON trn.TransCodeId = tc.TransCodeId
                                                                                       WHERE    trn.StuEnrollId = t2.StuEnrollId
                                                                                                AND IsInstCharge = 1
                                                                                                AND trn.Voided = 0
                                                                                                AND trn.TransDate <= t2.ExpGradDate
                                                                                     )
                                         WHEN t7.GEProgramStatus IN ( 'W' ) THEN (
                                                                                   SELECT   SUM(ISNULL(TransAmount,0))
                                                                                   FROM     dbo.saTransactions trn
                                                                                   INNER JOIN dbo.saTransCodes tc ON trn.TransCodeId = tc.TransCodeId
                                                                                   WHERE    trn.StuEnrollId = t2.StuEnrollId
                                                                                            AND IsInstCharge = 1
                                                                                            AND trn.Voided = 0
                                                                                            AND trn.TransDate <= t2.DateDetermined
                                                                                 )
                                         ELSE 0
                                    END AS InstitutionalDebt
                                   ,(
                                      SELECT    SUM(ISNULL(TransAmount,0))
                                      FROM      saTransactions T
                                      INNER JOIN saTransCodes TC ON T.TransCodeId = TC.TransCodeId
                                      INNER JOIN dbo.saSysTransCodes st ON TC.SysTransCodeId = st.SysTransCodeId
                                      WHERE     T.StuEnrollId = t2.StuEnrollId
                                                AND TC.IsInstCharge = 1
                                                AND T.TransTypeId IN ( 0,1 )
                                                AND st.SysTransCodeId IN ( 1,2,7,20 )
                                                AND T.Voided = 0
                                    ) AS TuitionAndFeesAmount
                                   ,CASE WHEN t7.SysStatusDescrip = 'Future start'
                                              AND (
                                                    t2.LDA <> NULL
                                                    OR t2.StartDate <> NULL
                                                  ) THEN 0
                                         WHEN t7.GEProgramStatus IN ( 'C','G','W' )
                                         THEN (
                                                SELECT  ISNULL(SUM(TransAmount),0)
                                                FROM    dbo.saTransactions t
                                                INNER JOIN dbo.saTransCodes c ON t.TransCodeId = c.TransCodeId
                                                INNER JOIN dbo.saSysTransCodes sc ON c.SysTransCodeId = sc.SysTransCodeId
                                                WHERE   t.StuEnrollId = t2.StuEnrollId
                                                        AND sc.Description IN ( 'Supplies','Books','Computer Equipment' )
                                                        AND Voided = 0
                                                        AND TransTypeId = 0
                                              )
                                         WHEN t7.GEProgramStatus = 'E' THEN NULL
                                         ELSE NULL
                                    END AS AllowanceForBooksSuppliesAndEquipment
                                   ,t4.Weeks AS LengthOfGEProgram
                                   ,'W' AS LengthOfGEProgramMeasurement
                                   ,(
                                      SELECT    SRAFV.AgencyValue
                                      FROM      syRptAgencyFldValues AS SRAFV
                                      INNER JOIN arAttendTypes AS AAT ON AAT.GERptAgencyFldValId = SRAFV.RptAgencyFldValId
                                      WHERE     AAT.AttendTypeId = t2.AttendTypeId
                                    ) AS StudentEnrollmentStatusAsOfThe1stDayOfEnrollmentInProgram
                          FROM      arStudent t1
                          INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                          INNER JOIN syCampuses t3 ON t2.CampusId = t3.CampusId
                          INNER JOIN arPrgVersions t4 ON t2.PrgVerId = t4.PrgVerId
                          INNER JOIN arPrograms t5 ON t4.ProgId = t5.ProgId
                          INNER JOIN syStatusCodes t6 ON t2.StatusCodeId = t6.StatusCodeId
                          INNER JOIN dbo.sySysStatus t7 ON t6.SysStatusId = t7.SysStatusId

									-- Next Join is to get the DateOfChange of stuatus when trigger works  instead use audit history and audit history detalils    
									--INNER JOIN (
									--                 SELECT TOP 1 SSSC1.StuEnrollId, SSSC1.ModDate, SSSC1.DateOfChange 
									--                 FROM dbo.syStudentStatusChanges AS SSSC1
									--                     INNER JOIN dbo.syStatusCodes AS SSC1 ON SSSC1.NewStatusId = SSC1.StatusCodeId
									--                     INNER JOIN dbo.sySysStatus AS SSS1 ON SSS1.SysStatusId = SSC1.SysStatusId
									--                 WHERE SSC1.SysStatusId = 12   
									--                 ORDER BY SSSC1.ModDate DESC    
									--           ) AS SSSC ON SSSC.StuEnrollId = T2.StuEnrollId
                          WHERE     --(t2.StartDate>=@StudentStartDate AND t2.StartDate<=@StudentEndDate)
						-- brings in students whose start date is less than the report end date OR who have an LDA that is after the report start date.
						-- In Freedom - AND Condition is used, but in Advantage attendance is not tracked or posted immediatly, so OR condition is used.
                                    (
                                      t2.StartDate <= @StudentEndDate
                                      OR t2.LDA >= @StudentStartDate
                                    )
                                    AND t3.CampusId = @CampusId
                                    AND t1.SSN IS NOT NULL -- SSN should not be NULL
                                    AND t5.IsGEProgram = 1
                                    AND t7.SysStatusId <> 8
                                    AND -- No Start should be excluded
                                    (
                                      @ProgramId IS NULL
                                      OR t5.ProgId IN ( SELECT  Val
                                                        FROM    MultipleValuesForReportParameters(@ProgramId,',',1) )
                                    )
                                    AND t2.StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
								-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
									( SELECT DISTINCT
                                                t1.StuEnrollId
                                      FROM      arStuEnrollments t1
                                               ,syStatusCodes t2
                                      WHERE     t1.StatusCodeId = t2.StatusCodeId
                                                AND StartDate <= @StudentEndDate
                                                AND -- Student started before the end date range
                                                LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                                AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
									-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                                AND (
                                                      t1.DateDetermined < @StudentStartDate
                                                      OR ExpGradDate < @StudentStartDate
                                                      OR LDA < @StudentStartDate
                                                    ) )
                                    AND (
                                          t2.ReEnrollmentDate IS NOT NULL
                                          AND DATEDIFF(DAY,
                                                       (
                                                         SELECT TOP 1
                                                                SAHD.NewValue
                                                         FROM   dbo.syAuditHist AS SAH
                                                         INNER JOIN dbo.syAuditHistDetail AS SAHD ON SAHD.AuditHistId = SAH.AuditHistId
                                                         WHERE  SAH.TableName = 'arStuEnrollments'
                                                                AND SAH.AuditHistId IN (
                                                                SELECT  SAH1.AuditHistId
                                                                FROM    dbo.syAuditHist AS SAH1
                                                                INNER JOIN dbo.syAuditHistDetail AS SAHD1 ON SAHD1.AuditHistId = SAH1.AuditHistId
                                                                INNER JOIN dbo.syStatusCodes AS SSC1 ON CONVERT(NVARCHAR(40),SSC1.StatusCodeId) = SAHD1.NewValue
                                                                WHERE   SAH1.TableName = 'arStuEnrollments'
                                                                        AND SAHD1.ColumnName = 'StatusCodeId'
                                                                        AND SAHD1.RowId = t2.StuEnrollId
                                                                        AND SAHD1.NewValue IN ( SELECT  CONVERT(NVARCHAR(36),SSC.StatusCodeId)
                                                                                                FROM    dbo.syStatusCodes AS SSC
                                                                                                WHERE   SSC.SysStatusId = 12 ) )
                                                                AND SAHD.ColumnName = 'DateDetermined'
                                                                AND SAHD.RowId = t2.StuEnrollId
                                                         ORDER BY SAHD.NewValue DESC
                                                       ),t2.ReEnrollmentDate) <= 180
                                        )
                        ) DT --Level 1
            ) DT1
    ORDER BY CIPCode
           ,CredentialLevel;
  -- Level2





GO
