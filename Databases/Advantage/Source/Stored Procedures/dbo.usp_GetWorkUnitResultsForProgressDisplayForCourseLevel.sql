SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/* 
CREATED: 


PURPOSE: 


MODIFIED:
11.6.2013	WP	Remove ResNum from services description

*/
CREATE PROCEDURE [dbo].[usp_GetWorkUnitResultsForProgressDisplayForCourseLevel]
    (
     @stuEnrollId UNIQUEIDENTIFIER 
    )
AS
    SET NOCOUNT ON; 
  
    SELECT  ConversionResultid
           ,GBCR.ReqId
           ,GBCR.TermId
           ,RTRIM(GCT.Descrip) + ( CASE WHEN GBCR.ResNum IN ( 0,1 ) THEN ''
                                        ELSE CAST(GBCR.ResNum AS CHAR)
                                   END ) AS Descrip
           ,GCT.Descrip AS Des
           ,GBCR.Score
           ,GBCR.MinResult
           ,GBCR.Required
           ,GBCR.MustPass
           ,( CASE WHEN GCT.SysComponentTypeId IN ( 500,503,544 ) THEN ( CASE WHEN ISNULL(Score,0) > MinResult THEN NULL
                                                                              WHEN ISNULL(Score,0) = MinResult THEN 0
                                                                              WHEN MinResult > ISNULL(Score,0) THEN ( MinResult - ISNULL(Score,0) )
                                                                         END )
                   ELSE NULL
              END ) AS Remaining
           ,GCT.SysComponentTypeId AS WorkUnitType
           ,(
              SELECT    Resource
              FROM      syResources
              WHERE     Resourceid = GCT.SysComponentTypeId
            ) AS WorkUnitDescrip
           ,NULL AS InstrGrdBkWgtDetailId
           ,GBCR.StuEnrollid
           ,GBCR.ResNum
           ,'false' AS RossType
    FROM    arGrdBkConversionResults GBCR
           ,arGrdComponentTypes GCT
    WHERE   GBCR.GrdComponentTypeId = GCT.GrdComponentTypeId
            AND GBCR.StuEnrollId = @stuEnrollId
    UNION
    SELECT  grdBkResultid
           ,ReqId
           ,TermId
           ,Descrip
           ,Des
           ,Score
           ,MinResult
           ,Required
           ,MustPass
           ,( CASE WHEN WorkUnitType IN ( 500,503,544 ) THEN ( CASE WHEN ISNULL(Score,0) > MinResult THEN NULL
                                                                    WHEN ISNULL(Score,0) = MinResult THEN 0
                                                                    WHEN MinResult > ISNULL(Score,0) THEN ( MinResult - ISNULL(Score,0) )
                                                               END )
                   ELSE NULL
              END ) AS Remaining
           ,WorkUnitType
           ,WorkUnitDescrip
           ,InstrGrdBkWgtDetailId
           ,StuEnrollid
           ,ResNum
           ,'false' AS RossType
    FROM    (
              SELECT    CS.ReqId
                       ,CS.TermId
                       ,( CASE WHEN (
                                      SELECT DISTINCT
                                                number
                                      FROM      arGrdBkWgtDetails
                                      WHERE     InstrGrdBkWgtDetailId = GBWD.InstrGrdBkWgtDetailId
                                    ) = 1 THEN GCT.Descrip
                               ELSE RTRIM(GCT.Descrip) + ( CASE WHEN GBRS.ResNum = 0 THEN ''
                                                                ELSE ' ' + CAST(GBRS.ResNum AS CHAR)
                                                           END )
                          END ) AS Descrip
                       ,( CASE GCT.SysComponentTypeId
                            WHEN 544 THEN (
                                            SELECT  SUM(HoursAttended)
                                            FROM    arExternshipAttendance
                                            WHERE   StuEnrollId = @stuEnrollId
                                          )
                            ELSE GBRS.Score
                          END ) AS Score
                       ,( CASE GCT.SysComponentTypeId
                            WHEN 500 THEN GBWD.Number
                            WHEN 503 THEN GBWD.Number
                            WHEN 544 THEN GBWD.Number
                            ELSE (
                                   SELECT   MIN(MinVal)
                                   FROM     arGradeScaleDetails GSD
                                           ,arGradeSystemDetails GSS
                                           ,arClassSections CSR
                                   WHERE    GSD.GrdSysDetailId = GSS.GrdSysDetailId
                                            AND GSS.IsPass = 1
                                            AND GSD.GrdScaleId = CSR.GrdScaleId
                                            AND CSR.ClsSectionId = CS.ClsSectionId
                                 )
                          END ) AS MinResult
                       ,GBWD.Required
                       ,GBWD.MustPass
                       ,GCT.SysComponentTypeId AS WorkUnitType
                       ,(
                          SELECT    Resource
                          FROM      syResources
                          WHERE     Resourceid = GCT.SysComponentTypeId
                        ) AS WorkUnitDescrip
                       ,GBRS.grdBkResultid
                       ,GBRS.InstrGrdBkWgtDetailId
                       ,GBRS.StuEnrollid
                       ,GCT.Descrip AS Des
                       ,GBRS.ResNum
                       ,GBWD.Descrip AS grdWgtDescrip
              FROM      arGrdBkResults GBRS
                       ,arClassSections CS
                       ,arGrdBkWgtDetails GBWD
                       ,arGrdComponentTypes GCT
              WHERE     GBRS.StuEnrollId = @stuEnrollId
                        AND GBRS.ClsSectionId = CS.ClsSectionId
                        AND GBRS.InstrGrdBkWgtDetailId = GBWD.InstrGrdBkWgtDetailId
                        AND GBWD.GrdComponentTypeId = GCT.GrdComponentTypeId
            ) P
    WHERE   WorkUnitType NOT IN ( 500,503,544 )
    UNION
    SELECT  '00000000-0000-0000-0000-000000000000' AS grdBkResultid
           ,ReqId
           ,TermId
           ,Descrip
           ,Des
           ,SUM(Score) AS score
           ,ISNULL(MinResult,0) AS MinResult
           ,Required
           ,MustPass
           ,( CASE WHEN SUM(ISNULL(Score,0)) > MinResult THEN NULL
                   WHEN SUM(ISNULL(Score,0)) = MinResult THEN 0
                   WHEN MinResult > SUM(ISNULL(Score,0)) THEN ( MinResult - SUM(ISNULL(Score,0)) )
              END ) AS remaining
           ,WorkUnitType
           ,WorkUnitDescrip
           ,InstrGrdBkWgtDetailId
           ,StuEnrollId
           ,ResNum
           ,'false' AS RossType
    FROM    (
              SELECT    CS.ReqId
                       ,CS.TermId
                       ,GCT.Descrip AS Descrip
                       ,( CASE GCT.SysComponentTypeId
                            WHEN 544 THEN (
                                            SELECT  SUM(HoursAttended)
                                            FROM    arExternshipAttendance
                                            WHERE   StuEnrollId = @stuEnrollId
                                          )
                            ELSE GBRS.Score
                          END ) AS Score
                       ,( CASE GCT.SysComponentTypeId
                            WHEN 500 THEN GBWD.Number
                            WHEN 503 THEN GBWD.Number
                            WHEN 544 THEN GBWD.Number
                            ELSE (
                                   SELECT   MIN(MinVal)
                                   FROM     arGradeScaleDetails GSD
                                           ,arGradeSystemDetails GSS
                                           ,arClassSections CSR
                                   WHERE    GSD.GrdSysDetailId = GSS.GrdSysDetailId
                                            AND GSS.IsPass = 1
                                            AND GSD.GrdScaleId = CSR.GrdScaleId
                                            AND CSR.ClsSectionId = CS.ClsSectionId
                                 )
                          END ) AS MinResult
                       ,GBWD.Required
                       ,GBWD.MustPass
                       ,GCT.SysComponentTypeId AS WorkUnitType
                       ,(
                          SELECT    Resource
                          FROM      syResources
                          WHERE     Resourceid = GCT.SysComponentTypeId
                        ) AS WorkUnitDescrip
                       ,GBRS.grdBkResultid
                       ,GBRS.InstrGrdBkWgtDetailId
                       ,GBRS.StuEnrollid
                       ,GCT.Descrip AS Des
                       ,GBRS.ResNum
                       ,GBWD.Descrip AS grdWgtDescrip
              FROM      arGrdBkResults GBRS
                       ,arClassSections CS
                       ,arGrdBkWgtDetails GBWD
                       ,arGrdComponentTypes GCT
              WHERE     GBRS.StuEnrollId = @stuEnrollId
                        AND GBRS.ClsSectionId = CS.ClsSectionId
                        AND GBRS.InstrGrdBkWgtDetailId = GBWD.InstrGrdBkWgtDetailId
                        AND GBWD.GrdComponentTypeId = GCT.GrdComponentTypeId
            ) P
    WHERE   WorkUnitType IN ( 500,503,544 )
    GROUP BY ReqId
           ,TermId
           ,Descrip
           ,Des
           ,MinResult
           ,Required
           ,MustPass
           ,WorkUnitType
           ,WorkUnitDescrip
           ,InstrGrdBkWgtDetailId
           ,StuEnrollId
           ,ResNum
    ORDER BY Descrip; 





GO
