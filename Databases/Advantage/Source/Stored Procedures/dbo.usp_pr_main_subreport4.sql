SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_pr_main_subreport4]
    @StuEnrollId VARCHAR(50)
   ,@TermId VARCHAR(50) = NULL
   ,@ReqId VARCHAR(50) = NULL
   ,@SysComponentTypeId VARCHAR(50) = NULL
   ,@ShowWorkUnitGrouping BIT = 0
   ,@SetGradeBookAt VARCHAR(50)
AS
    DECLARE @clsStartDate DATETIME;
    SET @clsStartDate = (
                          SELECT TOP 1
                                    StartDate
                          FROM      arClassSections
                          WHERE     TermId = @TermId
                                    AND ReqId = @ReqId
                        );
    IF LOWER(@SetGradeBookAt) = 'instructorlevel'
        BEGIN
            SELECT DISTINCT
                    GradeBookDescription
                   ,GradeBookScore
                   ,MinResult
                   ,GradeBookSysComponentTypeId
                   ,GradeComponentDescription
                   ,CampDescrip
                   ,FirstName
                   ,LastName
                   ,MiddleName
                   ,rownumber
                   ,GrdBkResultId
                   ,TermStartDate
                   ,TermEndDate
                   ,TermDescription
                   ,CourseDescription
                   ,PrgVerDescrip
                   ,ResourceID
            FROM    (
                      SELECT    4 AS Tag
                               ,3 AS Parent
                               ,PV.PrgVerId
                               ,PV.PrgVerDescrip
                               ,NULL AS ProgramCredits
                               ,T.TermId
                               ,T.TermDescrip AS TermDescription
                               ,T.StartDate AS TermStartDate
                               ,T.EndDate AS TermEndDate
                               ,R.ReqId AS CourseId
                               ,R.Descrip AS CourseDescription
                               ,'(' + R.Code + ' ) ' + R.Descrip AS CourseCodeDescrip
                               ,NULL AS CourseCredits
                               ,NULL AS CourseFinAidCredits
                               ,NULL AS CoursePassingGrade
                               ,NULL AS CourseScore
                               ,(
                                  SELECT TOP 1
                                            GrdBkResultId
                                  FROM      arGrdBkResults
                                  WHERE     StuEnrollId = SE.StuEnrollId
                                            AND InstrGrdBkWgtDetailId = GBWD.InstrGrdBkWgtDetailId
                                            AND ClsSectionId = CS.ClsSectionId
                                ) AS GrdBkResultId
                               ,CASE WHEN LOWER(@SetGradeBookAt) = 'instructorlevel' THEN RTRIM(GBWD.Descrip)
                                     ELSE GCT.Descrip
                                END AS GradeBookDescription
                               ,( CASE GCT.SysComponentTypeId
                                    WHEN 544 THEN (
                                                    SELECT  SUM(HoursAttended)
                                                    FROM    arExternshipAttendance
                                                    WHERE   StuEnrollId = SE.StuEnrollId
                                                  )
                                    ELSE (
                                           SELECT TOP 1
                                                    Score
                                           FROM     arGrdBkResults
                                           WHERE    StuEnrollId = SE.StuEnrollId
                                                    AND InstrGrdBkWgtDetailId = GBWD.InstrGrdBkWgtDetailId
                                                    AND ClsSectionId = CS.ClsSectionId
                                           ORDER BY ModDate DESC
                                         )
                                  END ) AS GradeBookScore
                               ,NULL AS GradeBookPostDate
                               ,NULL AS GradeBookPassingGrade
                               ,NULL AS GradeBookWeight
                               ,NULL AS GradeBookRequired
                               ,NULL AS GradeBookMustPass
                               ,GCT.SysComponentTypeId AS GradeBookSysComponentTypeId
                               ,NULL AS GradeBookHoursRequired
                               ,NULL AS GradeBookHoursCompleted
                               ,SE.StuEnrollId
                               ,( CASE WHEN GCT.SysComponentTypeId IN ( 500,503,504,544 ) THEN GBWD.Number
                                       ELSE (
                                              SELECT    MIN(MinVal)
                                              FROM      arGradeScaleDetails GSD
                                                       ,arGradeSystemDetails GSS
                                              WHERE     GSD.GrdSysDetailId = GSS.GrdSysDetailId
                                                        AND GSS.IsPass = 1
                                                        AND GSD.GrdScaleId = CS.GrdScaleId
                                            )
                                  END ) AS MinResult
                               ,SYRES.Resource AS GradeComponentDescription
                               ,NULL AS CreditsAttempted
                               ,NULL AS CreditsEarned
                               ,NULL AS Completed
                               ,NULL AS CurrentScore
                               ,NULL AS CurrentGrade
                               ,GCT.SysComponentTypeId AS FinalScore
                               ,NULL AS FinalGrade
                               ,NULL AS WeightedAverage_GPA
                               ,NULL AS SimpleAverage_GPA
                               ,NULL AS WeightedAverage_CumGPA
                               ,NULL AS SimpleAverage_CumGPA
                               ,C.CampusId
                               ,C.CampDescrip
                               ,ROW_NUMBER() OVER ( PARTITION BY SE.StuEnrollId,R.ReqId ORDER BY C.CampDescrip, PV.PrgVerDescrip, T.StartDate, T.EndDate, T.TermId, T.TermDescrip, R.ReqId, R.Descrip, GCT.SysComponentTypeId, GCT.Descrip ) AS rownumber
                               ,S.FirstName AS FirstName
                               ,S.LastName AS LastName
                               ,S.MiddleName
                               ,SYRES.ResourceID
                      FROM      -- MOdified by Balaji
                                arStuEnrollments SE
                      INNER JOIN (
                                   SELECT   StudentId
                                           ,FirstName
                                           ,LastName
                                           ,MiddleName
                                   FROM     arStudent
                                 ) S ON S.StudentId = SE.StudentId
                      INNER JOIN arResults RES ON RES.StuEnrollId = SE.StuEnrollId -- RES.TestId = CS.ClsSectionId -- and RES.StuEnrollId = GBR.StuEnrollId
                      INNER JOIN arClassSections CS ON CS.ClsSectionId = RES.TestId
                      INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                      INNER JOIN arTerm T ON CS.TermId = T.TermId
                      INNER JOIN arReqs R ON CS.ReqId = R.ReqId
                      LEFT OUTER JOIN arGrdBkWgtDetails GBWD ON GBWD.InstrGrdBkWgtId = CS.InstrGrdBkWgtId --.InstrGrdBkWgtDetailId = CS.InstrGrdBkWgtDetailId
                      INNER JOIN arGrdComponentTypes GCT ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
                      INNER JOIN (
                                   SELECT   Resource
                                           ,ResourceID
                                   FROM     syResources
                                   WHERE    ResourceTypeID = 10
                                 ) SYRES ON SYRES.ResourceID = GCT.SysComponentTypeId
                      INNER JOIN syCampuses C ON SE.CampusId = C.CampusId
                      WHERE     SE.StuEnrollId = @StuEnrollId
                                AND T.TermId = @TermId
                                AND R.ReqId = @ReqId
                                AND (
                                      @SysComponentTypeId IS NULL
                                      OR GCT.SysComponentTypeId IN ( SELECT Val
                                                                     FROM   MultipleValuesForReportParameters(@SysComponentTypeId,',',1) )
                                    )
                      UNION
                      SELECT    4 AS Tag
                               ,3
                               ,PV.PrgVerId
                               ,PV.PrgVerDescrip
                               ,NULL
                               ,T.TermId
                               ,T.TermDescrip
                               ,T.StartDate AS termStartdate
                               ,T.EndDate AS TermEndDate
                               ,GBCR.ReqId
                               ,R.Descrip AS CourseDescrip
                               ,'(' + R.Code + ' ) ' + R.Descrip AS CourseCodeDescrip
                               ,NULL
                               ,NULL
                               ,NULL
                               ,NULL
                               ,GBCR.ConversionResultId AS GrdBkResultId
                               ,CASE WHEN LOWER(@SetGradeBookAt) = 'instructorlevel' THEN RTRIM(GBWD.Descrip)
                                     ELSE GCT.Descrip
                                END AS GradeBookDescription
                               ,GBCR.Score AS GradeBookResult
                               ,NULL
                               ,NULL
                               ,NULL
                               ,NULL
                               ,NULL
                               ,GCT.SysComponentTypeId
                               ,NULL
                               ,NULL
                               ,SE.StuEnrollId
                               ,GBCR.MinResult
                               ,SYRES.Resource -- Student data  
                               ,NULL AS CreditsAttempted
                               ,NULL AS CreditsEarned
                               ,NULL AS Completed
                               ,NULL AS CurrentScore
                               ,NULL AS CurrentGrade
                               ,NULL AS FinalScore
                               ,NULL AS FinalGrade
                               ,NULL AS WeightedAverage_GPA
                               ,NULL AS SimpleAverage_GPA
                               ,NULL
                               ,NULL
                               ,C.CampusId
                               ,C.CampDescrip
                               ,ROW_NUMBER() OVER ( PARTITION BY SE.StuEnrollId,R.ReqId ORDER BY C.CampDescrip, PV.PrgVerDescrip, T.StartDate, T.EndDate, T.TermId, T.TermDescrip, R.ReqId, R.Descrip, GCT.SysComponentTypeId, GCT.Descrip ) AS rownumber
                               ,S.FirstName AS FirstName
                               ,S.LastName AS LastName
                               ,S.MiddleName
                               ,SYRES.ResourceID
                      FROM      arGrdBkConversionResults GBCR
                      INNER JOIN arStuEnrollments SE ON GBCR.StuEnrollId = SE.StuEnrollId
                      INNER JOIN (
                                   SELECT   StudentId
                                           ,FirstName
                                           ,LastName
                                           ,MiddleName
                                   FROM     arStudent
                                 ) S ON S.StudentId = SE.StudentId
                      INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                      INNER JOIN arTerm T ON GBCR.TermId = T.TermId
                      INNER JOIN arReqs R ON GBCR.ReqId = R.ReqId
                      INNER JOIN arGrdComponentTypes GCT ON GCT.GrdComponentTypeId = GBCR.GrdComponentTypeId
                      INNER JOIN arGrdBkWgtDetails GBWD ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
                                                           AND GBWD.GrdComponentTypeId = GBCR.GrdComponentTypeId
                      INNER JOIN arGrdBkWeights GBW ON GBWD.InstrGrdBkWgtId = GBW.InstrGrdBkWgtId
                                                       AND GBCR.ReqId = GBW.ReqId
                      INNER JOIN (
                                   SELECT   ReqId
                                           ,MAX(EffectiveDate) AS EffectiveDate
                                   FROM     arGrdBkWeights
                                   GROUP BY ReqId
                                 ) AS MaxEffectiveDatesByCourse ON GBCR.ReqId = MaxEffectiveDatesByCourse.ReqId
                      INNER JOIN (
                                   SELECT   Resource
                                           ,ResourceID
                                   FROM     syResources
                                   WHERE    ResourceTypeID = 10
                                 ) SYRES ON SYRES.ResourceID = GCT.SysComponentTypeId
                      INNER JOIN syCampuses C ON SE.CampusId = C.CampusId
                      WHERE     MaxEffectiveDatesByCourse.EffectiveDate <= T.StartDate
                                AND SE.StuEnrollId = @StuEnrollId
                                AND T.TermId = @TermId
                                AND R.ReqId = @ReqId
                                AND (
                                      @SysComponentTypeId IS NULL
                                      OR GCT.SysComponentTypeId IN ( SELECT Val
                                                                     FROM   MultipleValuesForReportParameters(@SysComponentTypeId,',',1) )
                                    )
                    ) dt
            ORDER BY GradeComponentDescription
                   ,GradeBookDescription;
        END;
    ELSE
        BEGIN
		-- If CourseLevel, then the InstrGrdBkWgtId field in the arClassSections Table will be populated
		-- If InstructorLevel, then the GrdBkWeights table has a reference to Course Id (ReqId)
            SELECT  *
                   ,ROW_NUMBER() OVER ( PARTITION BY StuEnrollId,CourseId ORDER BY CampDescrip, PrgVerDescrip, TermStartDate, TermEndDate, TermDescription, CourseDescription, GradeBookSysComponentTypeId, GradeBookDescription ) AS rownumber
            FROM    (
                      SELECT DISTINCT
                                GradeBookDescription
                               ,GradeBookScore
                               ,MinResult
                               ,GradeBookSysComponentTypeId
                               ,GradeComponentDescription
                               ,CampDescrip
                               ,FirstName
                               ,LastName
                               ,MiddleName
                               ,GrdBkResultId
                               ,TermStartDate
                               ,TermEndDate
                               ,TermDescription
                               ,CourseDescription
                               ,PrgVerDescrip
                               ,StuEnrollId
                               ,CourseId
                               ,ResourceID
                      FROM      (
                                  SELECT  DISTINCT
                                            4 AS Tag
                                           ,3 AS Parent
                                           ,PV.PrgVerId
                                           ,PV.PrgVerDescrip
                                           ,NULL AS ProgramCredits
                                           ,T.TermId
                                           ,T.TermDescrip AS TermDescription
                                           ,T.StartDate AS TermStartDate
                                           ,T.EndDate AS TermEndDate
                                           ,R.ReqId AS CourseId
                                           ,R.Descrip AS CourseDescription
                                           ,'(' + R.Code + ' ) ' + R.Descrip AS CourseCodeDescrip
                                           ,NULL AS CourseCredits
                                           ,NULL AS CourseFinAidCredits
                                           ,NULL AS CoursePassingGrade
                                           ,NULL AS CourseScore
                                           ,(
                                              SELECT TOP 1
                                                        GrdBkResultId
                                              FROM      arGrdBkResults
                                              WHERE     StuEnrollId = SE.StuEnrollId
                                                        AND InstrGrdBkWgtDetailId = b.InstrGrdBkWgtDetailId
                                                        AND ClsSectionId = CS.ClsSectionId
                                            ) AS GrdBkResultId
                                           ,RTRIM(GCT.Descrip) + ( CASE WHEN a.ResNum IN ( 0,1 ) THEN ''
                                                                        ELSE CAST(a.ResNum AS CHAR)
                                                                   END ) AS GradeBookDescription
                                           ,( CASE GCT.SysComponentTypeId
                                                WHEN 544 THEN (
                                                                SELECT  SUM(HoursAttended)
                                                                FROM    arExternshipAttendance
                                                                WHERE   StuEnrollId = SE.StuEnrollId
                                                              )
                                                ELSE 
--								(select Top 1 Score from arGrdBkResults where StuEnrollId=SE.StuEnrollId and 
--								InstrGrdBkWgtDetailId=b.InstrGrdBkWgtDetailId and
--								ClsSectionId=CS.ClsSectionId order by moddate desc) 
                                                     a.Score
                                              END ) AS GradeBookScore
                                           ,NULL AS GradeBookPostDate
                                           ,NULL AS GradeBookPassingGrade
                                           ,NULL AS GradeBookWeight
                                           ,NULL AS GradeBookRequired
                                           ,NULL AS GradeBookMustPass
                                           ,GCT.SysComponentTypeId AS GradeBookSysComponentTypeId
                                           ,NULL AS GradeBookHoursRequired
                                           ,NULL AS GradeBookHoursCompleted
                                           ,SE.StuEnrollId
                                           ,( CASE WHEN GCT.SysComponentTypeId IN ( 500,503,504,544 ) THEN b.Number
                                                   ELSE (
                                                          SELECT    MIN(MinVal)
                                                          FROM      arGradeScaleDetails GSD
                                                                   ,arGradeSystemDetails GSS
                                                          WHERE     GSD.GrdSysDetailId = GSS.GrdSysDetailId
                                                                    AND GSS.IsPass = 1
                                                                    AND GSD.GrdScaleId = CS.GrdScaleId
                                                        )
                                              END ) AS MinResult
                                           ,SYRES.Resource AS GradeComponentDescription
                                           ,NULL AS CreditsAttempted
                                           ,NULL AS CreditsEarned
                                           ,NULL AS Completed
                                           ,NULL AS CurrentScore
                                           ,NULL AS CurrentGrade
                                           ,GCT.SysComponentTypeId AS FinalScore
                                           ,NULL AS FinalGrade
                                           ,NULL AS WeightedAverage_GPA
                                           ,NULL AS SimpleAverage_GPA
                                           ,NULL AS WeightedAverage_CumGPA
                                           ,NULL AS SimpleAverage_CumGPA
                                           ,C1.CampusId
                                           ,C1.CampDescrip
                                           , 
						--ROW_NUMBER() OVER (partition by SE.StuEnrollId,R.ReqId Order By C1.CampDescrip,PV.PrgVerDescrip,T.StartDate,T.EndDate,T.TermId,T.TermDescrip,R.ReqId,R.Descrip,GCT.SysComponentTypeId,GCT.Descrip) as rownumber,
                                            c.FirstName AS FirstName
                                           ,c.LastName AS LastName
                                           ,c.MiddleName
                                           ,SYRES.ResourceID
                                  FROM      arGrdBkResults a
                                           ,arGrdBkWgtDetails b
                                           ,arStudent c
                                           ,arStuEnrollments SE
                                           ,arResults e
                                           ,arClassSections CS
                                           ,arReqs R
                                           ,arTerm T
                                           ,arGrdComponentTypes GCT
                                           ,(
                                              SELECT    Resource
                                                       ,ResourceID
                                              FROM      syResources
                                              WHERE     ResourceTypeID = 10
                                            ) SYRES
                                           ,syCampuses C1
                                           ,arPrgVersions PV
                                  WHERE     a.InstrGrdBkWgtDetailId = b.InstrGrdBkWgtDetailId
                                            AND a.StuEnrollId = @StuEnrollId
                                            AND a.StuEnrollId = SE.StuEnrollId
                                            AND SE.StudentId = c.StudentId
                                            AND a.StuEnrollId = e.StuEnrollId
                                            AND a.ClsSectionId = e.TestId
                                            AND e.TestId = CS.ClsSectionId
                                            AND CS.ReqId = R.ReqId
                                            AND CS.TermId = T.TermId
                                            AND GCT.GrdComponentTypeId = b.GrdComponentTypeId
                                            AND SYRES.ResourceID = GCT.SysComponentTypeId
                                            AND SE.CampusId = C1.CampusId
                                            AND SE.PrgVerId = PV.PrgVerId 
						--and a.ClsSectionId = 'CD64FA81-7E91-4E22-A323-34E7B5695E2E' 
                                            AND R.ReqId = @ReqId
                                            AND T.TermId = @TermId
                                            AND (
                                                  @SysComponentTypeId IS NULL
                                                  OR GCT.SysComponentTypeId IN ( SELECT Val
                                                                                 FROM   MultipleValuesForReportParameters(@SysComponentTypeId,',',1) )
                                                )
                                            AND a.ResNum >= 1
                                  UNION
                                  SELECT  DISTINCT
                                            4 AS Tag
                                           ,3 AS Parent
                                           ,PV.PrgVerId
                                           ,PV.PrgVerDescrip
                                           ,NULL AS ProgramCredits
                                           ,T.TermId
                                           ,T.TermDescrip AS TermDescription
                                           ,T.StartDate AS TermStartDate
                                           ,T.EndDate AS TermEndDate
                                           ,R.ReqId AS CourseId
                                           ,R.Descrip AS CourseDescription
                                           ,'(' + R.Code + ' ) ' + R.Descrip AS CourseCodeDescrip
                                           ,NULL AS CourseCredits
                                           ,NULL AS CourseFinAidCredits
                                           ,NULL AS CoursePassingGrade
                                           ,NULL AS CourseScore
                                           ,(
                                              SELECT TOP 1
                                                        GrdBkResultId
                                              FROM      arGrdBkResults
                                              WHERE     StuEnrollId = SE.StuEnrollId
                                                        AND InstrGrdBkWgtDetailId = b.InstrGrdBkWgtDetailId
                                                        AND ClsSectionId = CS.ClsSectionId
                                            ) AS GrdBkResultId
                                           ,RTRIM(GCT.Descrip) + ( CASE WHEN a.ResNum IN ( 0,1 ) THEN ''
                                                                        ELSE CAST(a.ResNum AS CHAR)
                                                                   END ) AS GradeBookDescription
                                           ,( CASE GCT.SysComponentTypeId
                                                WHEN 544 THEN (
                                                                SELECT  SUM(HoursAttended)
                                                                FROM    arExternshipAttendance
                                                                WHERE   StuEnrollId = SE.StuEnrollId
                                                              )
                                                ELSE 
--								(select Top 1 Score from arGrdBkResults where StuEnrollId=SE.StuEnrollId and 
--								InstrGrdBkWgtDetailId=b.InstrGrdBkWgtDetailId and
--								ClsSectionId=CS.ClsSectionId order by moddate desc) 
                                                     a.Score
                                              END ) AS GradeBookScore
                                           ,NULL AS GradeBookPostDate
                                           ,NULL AS GradeBookPassingGrade
                                           ,NULL AS GradeBookWeight
                                           ,NULL AS GradeBookRequired
                                           ,NULL AS GradeBookMustPass
                                           ,GCT.SysComponentTypeId AS GradeBookSysComponentTypeId
                                           ,NULL AS GradeBookHoursRequired
                                           ,NULL AS GradeBookHoursCompleted
                                           ,SE.StuEnrollId
                                           ,( CASE WHEN GCT.SysComponentTypeId IN ( 500,503,504,544 ) THEN b.Number
                                                   ELSE (
                                                          SELECT    MIN(MinVal)
                                                          FROM      arGradeScaleDetails GSD
                                                                   ,arGradeSystemDetails GSS
                                                          WHERE     GSD.GrdSysDetailId = GSS.GrdSysDetailId
                                                                    AND GSS.IsPass = 1
                                                                    AND GSD.GrdScaleId = CS.GrdScaleId
                                                        )
                                              END ) AS MinResult
                                           ,SYRES.Resource AS GradeComponentDescription
                                           ,NULL AS CreditsAttempted
                                           ,NULL AS CreditsEarned
                                           ,NULL AS Completed
                                           ,NULL AS CurrentScore
                                           ,NULL AS CurrentGrade
                                           ,GCT.SysComponentTypeId AS FinalScore
                                           ,NULL AS FinalGrade
                                           ,NULL AS WeightedAverage_GPA
                                           ,NULL AS SimpleAverage_GPA
                                           ,NULL AS WeightedAverage_CumGPA
                                           ,NULL AS SimpleAverage_CumGPA
                                           ,C1.CampusId
                                           ,C1.CampDescrip
                                           , 
						--ROW_NUMBER() OVER (partition by SE.StuEnrollId,R.ReqId Order By C1.CampDescrip,PV.PrgVerDescrip,T.StartDate,T.EndDate,T.TermId,T.TermDescrip,R.ReqId,R.Descrip,GCT.SysComponentTypeId,GCT.Descrip) as rownumber,
                                            c.FirstName AS FirstName
                                           ,c.LastName AS LastName
                                           ,c.MiddleName
                                           ,SYRES.ResourceID
                                  FROM      arGrdBkResults a
                                           ,arGrdBkWgtDetails b
                                           ,arStudent c
                                           ,arStuEnrollments SE
                                           ,arResults e
                                           ,arClassSections CS
                                           ,arReqs R
                                           ,arTerm T
                                           ,arGrdComponentTypes GCT
                                           ,(
                                              SELECT    Resource
                                                       ,ResourceID
                                              FROM      syResources
                                              WHERE     ResourceTypeID = 10
                                            ) SYRES
                                           ,syCampuses C1
                                           ,arPrgVersions PV
                                  WHERE     a.InstrGrdBkWgtDetailId = b.InstrGrdBkWgtDetailId
                                            AND a.StuEnrollId = @StuEnrollId
                                            AND a.StuEnrollId = SE.StuEnrollId
                                            AND SE.StudentId = c.StudentId
                                            AND a.StuEnrollId = e.StuEnrollId
                                            AND a.ClsSectionId = e.TestId
                                            AND e.TestId = CS.ClsSectionId
                                            AND CS.ReqId = R.ReqId
                                            AND CS.TermId = T.TermId
                                            AND GCT.GrdComponentTypeId = b.GrdComponentTypeId
                                            AND SYRES.ResourceID = GCT.SysComponentTypeId
                                            AND SE.CampusId = C1.CampusId
                                            AND SE.PrgVerId = PV.PrgVerId 
						--and a.ClsSectionId = 'CD64FA81-7E91-4E22-A323-34E7B5695E2E' 
                                            AND R.ReqId = @ReqId
                                            AND T.TermId = @TermId
                                            AND (
                                                  @SysComponentTypeId IS NULL
                                                  OR GCT.SysComponentTypeId IN ( SELECT Val
                                                                                 FROM   MultipleValuesForReportParameters(@SysComponentTypeId,',',1) )
                                                )
                                            AND a.ResNum = 0
                                            AND a.GrdBkResultId NOT IN (
                                            SELECT DISTINCT
                                                    GrdBkResultId
                                            FROM    arGrdBkResults a
                                                   ,arGrdBkWgtDetails b
                                                   ,arStudent c
                                                   ,arStuEnrollments SE
                                                   ,arResults e
                                                   ,arClassSections CS
                                                   ,arReqs R
                                                   ,arTerm T
                                                   ,arGrdComponentTypes GCT
                                                   ,(
                                                      SELECT    Resource
                                                               ,ResourceID
                                                      FROM      syResources
                                                      WHERE     ResourceTypeID = 10
                                                    ) SYRES
                                                   ,syCampuses C1
                                                   ,arPrgVersions PV
                                            WHERE   a.InstrGrdBkWgtDetailId = b.InstrGrdBkWgtDetailId
                                                    AND a.StuEnrollId = @StuEnrollId
                                                    AND a.StuEnrollId = SE.StuEnrollId
                                                    AND SE.StudentId = c.StudentId
                                                    AND a.StuEnrollId = e.StuEnrollId
                                                    AND a.ClsSectionId = e.TestId
                                                    AND e.TestId = CS.ClsSectionId
                                                    AND CS.ReqId = R.ReqId
                                                    AND CS.TermId = T.TermId
                                                    AND GCT.GrdComponentTypeId = b.GrdComponentTypeId
                                                    AND SYRES.ResourceID = GCT.SysComponentTypeId
                                                    AND SE.CampusId = C1.CampusId
                                                    AND SE.PrgVerId = PV.PrgVerId
                                                    AND R.ReqId = @ReqId
                                                    AND T.TermId = @TermId
                                                    AND (
                                                          @SysComponentTypeId IS NULL
                                                          OR GCT.SysComponentTypeId IN ( SELECT Val
                                                                                         FROM   MultipleValuesForReportParameters(@SysComponentTypeId,',',1) )
                                                        )
                                                    AND a.ResNum >= 1 )
                                  UNION
                                  SELECT    *
                                  FROM      (
                                              SELECT  DISTINCT
                                                        4 AS Tag
                                                       ,3 AS Parent
                                                       ,PV.PrgVerId
                                                       ,PV.PrgVerDescrip
                                                       ,NULL AS ProgramCredits
                                                       ,T.TermId
                                                       ,T.TermDescrip AS TermDescription
                                                       ,T.StartDate AS TermStartDate
                                                       ,T.EndDate AS TermEndDate
                                                       ,R.ReqId AS CourseId
                                                       ,R.Descrip AS CourseDescription
                                                       ,'(' + R.Code + ' ) ' + R.Descrip AS CourseCodeDescrip
                                                       ,NULL AS CourseCredits
                                                       ,NULL AS CourseFinAidCredits
                                                       ,NULL AS CoursePassingGrade
                                                       ,NULL AS CourseScore
                                                       ,(
                                                          SELECT TOP 1
                                                                    GrdBkResultId
                                                          FROM      arGrdBkResults
                                                          WHERE     StuEnrollId = SE.StuEnrollId
                                                                    AND InstrGrdBkWgtDetailId = b.InstrGrdBkWgtDetailId
                                                                    AND ClsSectionId = CS.ClsSectionId
                                                        ) AS GrdBkResultId
                                                       ,RTRIM(GCT.Descrip) AS GradeBookDescription
                                                       ,( CASE GCT.SysComponentTypeId
                                                            WHEN 544 THEN (
                                                                            SELECT  SUM(HoursAttended)
                                                                            FROM    arExternshipAttendance
                                                                            WHERE   StuEnrollId = SE.StuEnrollId
                                                                          )
                                                            ELSE 
--								(select Top 1 Score from arGrdBkResults where StuEnrollId=SE.StuEnrollId and 
--								InstrGrdBkWgtDetailId=b.InstrGrdBkWgtDetailId and
--								ClsSectionId=CS.ClsSectionId order by moddate desc) 
                                                                 NULL
                                                          END ) AS GradeBookScore
                                                       ,NULL AS GradeBookPostDate
                                                       ,NULL AS GradeBookPassingGrade
                                                       ,NULL AS GradeBookWeight
                                                       ,NULL AS GradeBookRequired
                                                       ,NULL AS GradeBookMustPass
                                                       ,GCT.SysComponentTypeId AS GradeBookSysComponentTypeId
                                                       ,NULL AS GradeBookHoursRequired
                                                       ,NULL AS GradeBookHoursCompleted
                                                       ,SE.StuEnrollId
                                                       ,( CASE WHEN GCT.SysComponentTypeId IN ( 500,503,504,544 ) THEN b.Number
                                                               ELSE (
                                                                      SELECT    MIN(MinVal)
                                                                      FROM      arGradeScaleDetails GSD
                                                                               ,arGradeSystemDetails GSS
                                                                      WHERE     GSD.GrdSysDetailId = GSS.GrdSysDetailId
                                                                                AND GSS.IsPass = 1
                                                                                AND GSD.GrdScaleId = CS.GrdScaleId
                                                                    )
                                                          END ) AS MinResult
                                                       ,SYRES.Resource AS GradeComponentDescription
                                                       ,NULL AS CreditsAttempted
                                                       ,NULL AS CreditsEarned
                                                       ,NULL AS Completed
                                                       ,NULL AS CurrentScore
                                                       ,NULL AS CurrentGrade
                                                       ,GCT.SysComponentTypeId AS FinalScore
                                                       ,NULL AS FinalGrade
                                                       ,NULL AS WeightedAverage_GPA
                                                       ,NULL AS SimpleAverage_GPA
                                                       ,NULL AS WeightedAverage_CumGPA
                                                       ,NULL AS SimpleAverage_CumGPA
                                                       ,C1.CampusId
                                                       ,C1.CampDescrip
                                                       , 
						--ROW_NUMBER() OVER (partition by SE.StuEnrollId,R.ReqId Order By C1.CampDescrip,PV.PrgVerDescrip,T.StartDate,T.EndDate,T.TermId,T.TermDescrip,R.ReqId,R.Descrip,GCT.SysComponentTypeId,GCT.Descrip) as rownumber,
                                                        c.FirstName AS FirstName
                                                       ,c.LastName AS LastName
                                                       ,c.MiddleName
                                                       ,SYRES.ResourceID
                                              FROM      arResults e
                                                       ,arStuEnrollments SE
                                                       ,arStudent c
                                                       ,arClassSections CS
                                                       ,arReqs R
                                                       ,arTerm T
                                                       ,arGrdComponentTypes GCT
                                                       ,(
                                                          SELECT    Resource
                                                                   ,ResourceID
                                                          FROM      syResources
                                                          WHERE     ResourceTypeID = 10
                                                        ) SYRES
                                                       ,syCampuses C1
                                                       ,arPrgVersions PV
                                                       ,(
                                                          SELECT DISTINCT TOP 1
                                                                    A.InstrGrdBkWgtId
                                                                   ,A.EffectiveDate
                                                                   ,b.GrdScaleId
                                                                   ,b.ReqId
                                                          FROM      arGrdBkWeights A
                                                                   ,arClassSections b
                                                          WHERE     A.ReqId = b.ReqId
                                                                    AND A.EffectiveDate <= b.StartDate
                                                                    AND b.TermId = @TermId
                                                                    AND b.ReqId = @ReqId
                                                          ORDER BY  EffectiveDate DESC
                                                        ) a
                                                       ,arGrdBkWgtDetails b
                                              WHERE     e.StuEnrollId = SE.StuEnrollId
                                                        AND SE.StudentId = c.StudentId
                                                        AND e.TestId = CS.ClsSectionId
                                                        AND CS.ReqId = R.ReqId
                                                        AND CS.TermId = T.TermId
                                                        AND a.InstrGrdBkWgtId = b.InstrGrdBkWgtId
                                                        AND a.ReqId = R.ReqId
                                                        AND GCT.GrdComponentTypeId = b.GrdComponentTypeId
                                                        AND SYRES.ResourceID = GCT.SysComponentTypeId
                                                        AND SE.CampusId = C1.CampusId
                                                        AND SE.PrgVerId = PV.PrgVerId
                                                        AND SE.StuEnrollId = @StuEnrollId
                                                        AND (
                                                              @SysComponentTypeId IS NULL
                                                              OR GCT.SysComponentTypeId IN ( SELECT Val
                                                                                             FROM   MultipleValuesForReportParameters(@SysComponentTypeId,',',1) )
                                                            )
                                            ) dt5
                                  WHERE     GrdBkResultId IS NULL
                                            AND TermId = @TermId
                                            AND CourseId = @ReqId
				--union
				--	Select  Distinct
				--		4 as Tag,  
    --            		3 as Parent,  
    --            		PV.PrgVerId,          
    --            		PV.PrgVerDescrip,    
    --            		null As ProgramCredits,   
    --                    T.TermId, 
    --            		T.TermDescrip as TermDescription,  
    --            		T.StartDate as TermStartDate,  
    --            		T.EndDate as TermEndDate,     
    --            		R.ReqId as CourseId,  
    --            		R.Descrip as CourseDescription,   
    --             		'(' +  	R.Code + ' ) '  + R.Descrip as CourseCodeDescrip, 
    --            		null as CourseCredits,  
    --            		null as CourseFinAidCredits,  
    --            		null as CoursePassingGrade,  
    --                    null as CourseScore, 
    --            		(select Top 1 GrdBkResultId  from arGrdBkResults where StuEnrollId=SE.StuEnrollId and 
				--				InstrGrdBkWgtDetailId=b.InstrGrdBkWgtDetailId and
				--				ClsSectionId=CS.ClsSectionId) as GrdBkResultId,   
				--		Rtrim(GCT.Descrip) as GradeBookDescription,
				--		(CASE GCT.SysComponentTypeId 
				--			WHEN 544 THEN (SELECT SUM(HoursAttended) FROM arExternshipAttendance WHERE StuEnrollId=SE.StuEnrollId) 
				--			ELSE 
				--				null 
				--			END 
				--		) as GradeBookScore, 
    --            		null as GradeBookPostDate,null as GradeBookPassingGrade,null as GradeBookWeight,  
    --            		null as GradeBookRequired,null as GradeBookMustPass,
				--		GCT.SysComponentTypeId as GradeBookSysComponentTypeId, 
    --            		null as GradeBookHoursRequired,null as GradeBookHoursCompleted,
				--			SE.StuEnrollId, 
				--		 (CASE WHEN GCT.SysComponentTypeId  in (500,503,504,544) THEN b.Number 
				--			   ELSE (SELECT MIN(MinVal) 
				--						FROM arGradeScaleDetails GSD, arGradeSystemDetails GSS 
				--						WHERE GSD.GrdSysDetailId=GSS.GrdSysDetailId 
				--						AND GSS.IsPass=1 and GSD.GrdScaleId=CS.GrdScaleId) 
				--				END 
				--			) AS MinResult,SYRES.Resource as GradeComponentDescription,
				--		null as CreditsAttempted,
				--		null as CreditsEarned,
				--		null as Completed,
				--		null as CurrentScore,
				--		null as CurrentGrade,
				--		GCT.SysComponentTypeId as FinalScore,
				--		null as FinalGrade,
				--		null as WeightedAverage_GPA,null as SimpleAverage_GPA,null as WeightedAverage_CumGPA,null as SimpleAverage_CumGPA,C1.CampusId,C1.CampDescrip, 
				--		--ROW_NUMBER() OVER (partition by SE.StuEnrollId,R.ReqId Order By C1.CampDescrip,PV.PrgVerDescrip,T.StartDate,T.EndDate,T.TermId,T.TermDescrip,R.ReqId,R.Descrip,GCT.SysComponentTypeId,GCT.Descrip) as rownumber,
				--		C.FirstName as FirstName,
				--		C.LastName as LastName,
				--		C.MiddleName,SYRES.ResourceId
				--	from 
				--		arResults e, arStuEnrollments SE, arStudent c, arClassSections CS, arReqs R,arTerm T,arGrdComponentTypes GCT,
				--		(Select Resource,ResourceId from syResources where ResourceTypeId=10) SYRES, syCampuses C1,arPrgVersions PV,
				--		(SELECT Distinct Top 1 A.InstrGrdBkWgtId,A.EffectiveDate,B.GrdScaleId,B.ReqId FROM arGrdBkWeights A,arClassSections B        
				--		WHERE A.ReqId=B.ReqId AND A.EffectiveDate<=B.StartDate AND B.TermId=@TermId and B.ReqId=@ReqId
				--		Order by EffectiveDate Desc) a,arGrdBkWgtDetails b
				--	where 
				--		e.StuEnrollId = SE.StuEnrollId and SE.StudentId = C.StudentId and 
				--		e.TestId = CS.ClsSectionId and CS.ReqId = R.ReqId and CS.TermId = T.TermId and
				--		a.InstrGrdBkWgtId=b.InstrGrdBkWgtId  and a.ReqId=R.ReqId and 
				--		GCT.GrdComponentTypeId = b.GrdComponentTypeId and 
				--		SYRES.ResourceId=GCT.SysComponentTypeId and SE.CampusId = C1.CampusId and 
				--		SE.PrgVerId = PV.PrgVerId and
				--		SE.StuEnrollId = @StuEnrollId  and
				--		R.ReqId = @ReqId 
				--		and T.TermId = @TermId and
				--		(@SysComponentTypeId is null or GCT.SysComponentTypeId in (Select Val from [MultipleValuesForReportParameters](@SysComponentTypeId,',',1)))
				--		and e.ResultId not in
				--		(
				--			Select e.ResultId from	
				--				arGrdBkResults a, arGrdBkWgtDetails b, arStudent c, arStuEnrollments SE, arResults e, arClassSections CS, arReqs R,arTerm T,arGrdComponentTypes GCT,
				--				(Select Resource,ResourceId from syResources where ResourceTypeId=10) SYRES, syCampuses C1,arPrgVersions PV
				--			where 
				--				a.InstrGrdBkWgtDetailId = b.InstrGrdBkWgtDetailId and a.StuEnrollId = @StuEnrollId and 
				--				a.StuEnrollId = SE.StuEnrollId and SE.StudentId = c.StudentId and 
				--				a.StuEnrollId = e.StuEnrollId  and a.ClsSectionId = e.TestId  and e.TestId = cs.ClsSectionId and 
				--				CS.ReqId=R.ReqId and CS.TermId = T.TermId and GCT.GrdComponentTypeId = b.GrdComponentTypeId and 
				--				SYRES.ResourceId=GCT.SysComponentTypeId and SE.CampusId = C1.CampusId and SE.PrgVerId = PV.PrgVerId 
				--				--and a.ClsSectionId = 'CD64FA81-7E91-4E22-A323-34E7B5695E2E' 
				--				and R.ReqId = @ReqId 
				--				and T.TermId = @termId
				--				and (@SysComponentTypeId is null or GCT.SysComponentTypeId in (Select Val from [MultipleValuesForReportParameters](@SysComponentTypeId,',',1)))
				--				and a.ResNum>=1
				--		)
                                  UNION
                                  SELECT  DISTINCT
                                            4 AS Tag
                                           ,3 AS Parent
                                           ,PV.PrgVerId
                                           ,PV.PrgVerDescrip
                                           ,NULL AS ProgramCredits
                                           ,T.TermId
                                           ,T.TermDescrip AS TermDescription
                                           ,T.StartDate AS TermStartDate
                                           ,T.EndDate AS TermEndDate
                                           ,R.ReqId AS CourseId
                                           ,R.Descrip AS CourseDescription
                                           ,'(' + R.Code + ' ) ' + R.Descrip AS CourseCodeDescrip
                                           ,NULL AS CourseCredits
                                           ,NULL AS CourseFinAidCredits
                                           ,NULL AS CoursePassingGrade
                                           ,NULL AS CourseScore
                                           ,(
                                              SELECT TOP 1
                                                        GrdBkResultId
                                              FROM      arGrdBkResults
                                              WHERE     StuEnrollId = SE.StuEnrollId
                                                        AND InstrGrdBkWgtDetailId = b.InstrGrdBkWgtDetailId
                                                        AND ClsSectionId = CS.ClsSectionId
                                            ) AS GrdBkResultId
                                           ,RTRIM(GCT.Descrip) AS GradeBookDescription
                                           ,( CASE GCT.SysComponentTypeId
                                                WHEN 544 THEN (
                                                                SELECT  SUM(HoursAttended)
                                                                FROM    arExternshipAttendance
                                                                WHERE   StuEnrollId = SE.StuEnrollId
                                                              )
                                                ELSE 
--								(select Top 1 Score from arGrdBkResults where StuEnrollId=SE.StuEnrollId and 
--								InstrGrdBkWgtDetailId=b.InstrGrdBkWgtDetailId and
--								ClsSectionId=CS.ClsSectionId order by moddate desc) 
                                                     NULL
                                              END ) AS GradeBookScore
                                           ,NULL AS GradeBookPostDate
                                           ,NULL AS GradeBookPassingGrade
                                           ,NULL AS GradeBookWeight
                                           ,NULL AS GradeBookRequired
                                           ,NULL AS GradeBookMustPass
                                           ,GCT.SysComponentTypeId AS GradeBookSysComponentTypeId
                                           ,NULL AS GradeBookHoursRequired
                                           ,NULL AS GradeBookHoursCompleted
                                           ,SE.StuEnrollId
                                           ,( CASE WHEN GCT.SysComponentTypeId IN ( 500,503,504,544 ) THEN b.Number
                                                   ELSE (
                                                          SELECT    MIN(MinVal)
                                                          FROM      arGradeScaleDetails GSD
                                                                   ,arGradeSystemDetails GSS
                                                          WHERE     GSD.GrdSysDetailId = GSS.GrdSysDetailId
                                                                    AND GSS.IsPass = 1
                                                                    AND GSD.GrdScaleId = CS.GrdScaleId
                                                        )
                                              END ) AS MinResult
                                           ,SYRES.Resource AS GradeComponentDescription
                                           ,NULL AS CreditsAttempted
                                           ,NULL AS CreditsEarned
                                           ,NULL AS Completed
                                           ,NULL AS CurrentScore
                                           ,NULL AS CurrentGrade
                                           ,GCT.SysComponentTypeId AS FinalScore
                                           ,NULL AS FinalGrade
                                           ,NULL AS WeightedAverage_GPA
                                           ,NULL AS SimpleAverage_GPA
                                           ,NULL AS WeightedAverage_CumGPA
                                           ,NULL AS SimpleAverage_CumGPA
                                           ,C1.CampusId
                                           ,C1.CampDescrip
                                           , 
						--ROW_NUMBER() OVER (partition by SE.StuEnrollId,R.ReqId Order By C1.CampDescrip,PV.PrgVerDescrip,T.StartDate,T.EndDate,T.TermId,T.TermDescrip,R.ReqId,R.Descrip,GCT.SysComponentTypeId,GCT.Descrip) as rownumber,
                                            c.FirstName AS FirstName
                                           ,c.LastName AS LastName
                                           ,c.MiddleName
                                           ,SYRES.ResourceID
                                  FROM      arResults e
                                           ,arStuEnrollments SE
                                           ,arStudent c
                                           ,arClassSections CS
                                           ,arReqs R
                                           ,arTerm T
                                           ,arGrdComponentTypes GCT
                                           ,(
                                              SELECT    Resource
                                                       ,ResourceID
                                              FROM      syResources
                                              WHERE     ResourceTypeID = 10
                                            ) SYRES
                                           ,syCampuses C1
                                           ,arPrgVersions PV
                                           ,(
                                              SELECT DISTINCT TOP 1
                                                        A.InstrGrdBkWgtId
                                                       ,A.EffectiveDate
                                                       ,b.GrdScaleId
                                                       ,b.ReqId
                                              FROM      arGrdBkWeights A
                                                       ,arClassSections b
                                              WHERE     A.ReqId = b.ReqId
                                                        AND A.EffectiveDate <= b.StartDate
                                                        AND b.TermId = @TermId
                                                        AND b.ReqId = @ReqId
                                              ORDER BY  A.EffectiveDate DESC
                                            ) a
                                           ,arGrdBkWgtDetails b
                                  WHERE     e.StuEnrollId = SE.StuEnrollId
                                            AND SE.StudentId = c.StudentId
                                            AND e.TestId = CS.ClsSectionId
                                            AND CS.ReqId = R.ReqId
                                            AND CS.TermId = T.TermId
                                            AND a.InstrGrdBkWgtId = b.InstrGrdBkWgtId
                                            AND a.ReqId = R.ReqId
                                            AND GCT.GrdComponentTypeId = b.GrdComponentTypeId
                                            AND SYRES.ResourceID = GCT.SysComponentTypeId
                                            AND SE.CampusId = C1.CampusId
                                            AND SE.PrgVerId = PV.PrgVerId
                                            AND SE.StuEnrollId = @StuEnrollId
                                            AND R.ReqId = @ReqId
                                            AND T.TermId = @TermId
                                            AND (
                                                  @SysComponentTypeId IS NULL
                                                  OR GCT.SysComponentTypeId IN ( SELECT Val
                                                                                 FROM   MultipleValuesForReportParameters(@SysComponentTypeId,',',1) )
                                                )
                                            AND e.TestId NOT IN ( SELECT    ClsSectionId
                                                                  FROM      arGrdBkResults
                                                                  WHERE     StuEnrollId = e.StuEnrollId
                                                                            AND ClsSectionId = e.TestId )
                                  UNION
                                  SELECT    4 AS Tag
                                           ,3
                                           ,PV.PrgVerId
                                           ,PV.PrgVerDescrip
                                           ,NULL
                                           ,T.TermId
                                           ,T.TermDescrip
                                           ,T.StartDate AS termStartdate
                                           ,T.EndDate AS TermEndDate
                                           ,GBCR.ReqId
                                           ,R.Descrip AS CourseDescrip
                                           ,'(' + R.Code + ' ) ' + R.Descrip AS CourseCodeDescrip
                                           ,NULL
                                           ,NULL
                                           ,NULL
                                           ,NULL
                                           ,GBCR.ConversionResultId AS GrdBkResultId
                                           ,GBCR.Comments AS GradeBookDescription
                                           ,GBCR.Score AS GradeBookResult
                                           ,NULL
                                           ,NULL
                                           ,NULL
                                           ,NULL
                                           ,NULL
                                           ,GCT.SysComponentTypeId
                                           ,NULL
                                           ,NULL
                                           ,SE.StuEnrollId
                                           ,GBCR.MinResult
                                           ,SYRES.Resource -- Student data  
                                           ,NULL AS CreditsAttempted
                                           ,NULL AS CreditsEarned
                                           ,NULL AS Completed
                                           ,NULL AS CurrentScore
                                           ,NULL AS CurrentGrade
                                           ,NULL AS FinalScore
                                           ,NULL AS FinalGrade
                                           ,NULL AS WeightedAverage_GPA
                                           ,NULL AS SimpleAverage_GPA
                                           ,NULL
                                           ,NULL
                                           ,C.CampusId
                                           ,C.CampDescrip
                                           ,
						--ROW_NUMBER() OVER (partition by SE.StuEnrollId,R.ReqId Order By C.CampDescrip,PV.PrgVerDescrip,T.StartDate,T.EndDate,T.TermId,T.TermDescrip,R.ReqId,R.Descrip,GCT.SysComponentTypeId,GCT.Descrip) as rownumber,
                                            S.FirstName AS FirstName
                                           ,S.LastName AS LastName
                                           ,S.MiddleName
                                           ,SYRES.ResourceID
                                  FROM      arGrdBkConversionResults GBCR
                                  INNER JOIN arStuEnrollments SE ON GBCR.StuEnrollId = SE.StuEnrollId
                                  INNER JOIN (
                                               SELECT   StudentId
                                                       ,FirstName
                                                       ,LastName
                                                       ,MiddleName
                                               FROM     arStudent
                                             ) S ON S.StudentId = SE.StudentId
                                  INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                                  INNER JOIN arTerm T ON GBCR.TermId = T.TermId
                                  INNER JOIN arReqs R ON GBCR.ReqId = R.ReqId
                                  INNER JOIN arGrdComponentTypes GCT ON GCT.GrdComponentTypeId = GBCR.GrdComponentTypeId
                                  INNER JOIN arGrdBkWgtDetails GBWD ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
                                                                       AND GBWD.GrdComponentTypeId = GBCR.GrdComponentTypeId
                                  INNER JOIN arGrdBkWeights GBW ON GBWD.InstrGrdBkWgtId = GBW.InstrGrdBkWgtId
                                                                   AND GBCR.ReqId = GBW.ReqId
                                  INNER JOIN (
                                               SELECT   Resource
                                                       ,ResourceID
                                               FROM     syResources
                                               WHERE    ResourceTypeID = 10
                                             ) SYRES ON SYRES.ResourceID = GCT.SysComponentTypeId
                                  INNER JOIN syCampuses C ON SE.CampusId = C.CampusId
                                  WHERE     --MaxEffectiveDatesByCourse.EffectiveDate <= T.StartDate and
                                            SE.StuEnrollId = @StuEnrollId
                                            AND T.TermId = @TermId
                                            AND R.ReqId = @ReqId
                                            AND (
                                                  @SysComponentTypeId IS NULL
                                                  OR GCT.SysComponentTypeId IN ( SELECT Val
                                                                                 FROM   MultipleValuesForReportParameters(@SysComponentTypeId,',',1) )
                                                )
                                ) dt
                    ) dt1
            ORDER BY ResourceID
                   ,GradeComponentDescription
                   ,GradeBookDescription;
        END;

GO
