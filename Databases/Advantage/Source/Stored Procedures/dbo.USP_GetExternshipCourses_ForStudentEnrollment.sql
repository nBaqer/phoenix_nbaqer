SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_GetExternshipCourses_ForStudentEnrollment]
    @StuEnrollId UNIQUEIDENTIFIER
AS
    SELECT DISTINCT
            t4.TermId
           ,t4.TermDescrip
           ,t3.ReqId
           ,t3.Code
           ,t3.Descrip
           ,0.00 AS Credits
           ,0.00 AS Hours
           ,NULL AS CourseCategoryId
           ,t4.StartDate AS StartDate
           ,t4.EndDate AS EndDate
           ,t2.StartDate AS ClassStartDate
           ,t2.EndDate AS ClassEndDate
           ,t1.GrdSysDetailId
           ,t1.TestId
           ,t1.ResultId
           ,NULL AS Grade
           ,NULL AS IsPass
           ,NULL AS GPA
           ,NULL AS IsCreditsAttempted
           ,NULL AS IsCreditsEarned
           ,NULL AS IsInGPA
           ,NULL AS IsDrop
           ,NULL AS CourseCategory
           ,NULL AS Score
           ,NULL AS FinAidCredits
           ,NULL AS DateIssue
           ,NULL AS DropDate
           ,NULL AS ScheduledHours
           ,NULL AS IsTransferGrade
           ,NULL AS AvgScoreNew
           ,NULL AS AvgGPANew
           ,t1.IsCourseCompleted
    FROM    arResults t1
    INNER JOIN arClassSections t2 ON t1.TestId = t2.ClsSectionId
    INNER JOIN arReqs t3 ON t2.ReqId = t3.ReqId
    INNER JOIN arTerm t4 ON t2.TermId = t4.TermId
                            AND t3.IsExternship = 1
    WHERE   t1.StuEnrollid = @StuEnrollId; 



GO
