SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_GetNeverAttemptedWUForCourseLevel]
    (
     @stuEnrollId UNIQUEIDENTIFIER
    )
AS
    SET NOCOUNT ON;

    SELECT  d.reqid
           ,d.termId
           ,e.Descrip
           ,( CASE e.SysComponentTypeId
                WHEN 500 THEN A.Number
                WHEN 503 THEN A.Number
                WHEN 544 THEN A.Number
                ELSE (
                       SELECT   MIN(MinVal)
                       FROM     arGradeScaleDetails GSD
                               ,arGradeSystemDetails GSS
                               ,arClassSections CSR
                       WHERE    GSD.GrdSysDetailId = GSS.GrdSysDetailId
                                AND GSS.IsPass = 1
                                AND GSD.GrdScaleId = CSR.GrdScaleId
                                AND CSR.ClsSectionId = d.ClsSectionId
                     )
              END ) AS MinResult
           ,A.required
           ,A.mustpass
           ,e.SysComponentTypeId AS WorkUnitType
           ,A.number
           ,(
              SELECT    Resource
              FROM      syResources
              WHERE     Resourceid = e.SysComponentTypeId
            ) AS WorkUnitDescrip
           ,A.InstrGrdBkWgtDetailId
           ,c.stuenrollid
    FROM    arGrdBkWgtDetails A
           ,arGrdBkWeights B
           ,arResults c
           ,arClassSections d
           ,arGrdComponentTypes e
    WHERE   A.InstrGrdBkWgtId = B.InstrGrdBkWgtId
            AND B.Reqid = d.reqid
            AND c.testid = d.clsSectionid
            AND e.GrdComponentTypeId = A.GrdComponentTypeId
            AND B.EffectiveDate <= d.StartDate
            AND c.StuEnrollid = @stuEnrollId
            AND A.number IS NOT NULL
            AND A.InstrGrdBkWgtDetailId NOT IN ( SELECT InstrGrdBkWgtDetailId
                                                 FROM   arGrdBkResults
                                                 WHERE  StuEnrollId = @stuEnrollId
                                                        AND clsSectionid IN ( SELECT    clsSectionid
                                                                              FROM      arClassSections
                                                                              WHERE     Reqid = d.reqid
                                                                                        AND Termid = d.TermId ) )
            AND B.EffectiveDate = (
                                    SELECT DISTINCT TOP 1
                                            A.EffectiveDate
                                    FROM    arGrdBkWeights A
                                           ,arClassSections B
                                    WHERE   A.ReqId = B.ReqId
                                            AND A.EffectiveDate <= B.StartDate
                                            AND B.ClsSectionId = d.ClsSectionId
                                    ORDER BY A.EffectiveDate DESC
                                  )
    ORDER BY d.reqid
           ,e.SysComponentTypeId
           ,e.Descrip; 




GO
