SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================

-- Author: Ginzo, John

-- Create date: 12/09/2014

-- Description: Get projected payment period charges

-- =============================================

CREATE PROCEDURE [dbo].[GetProjectedPaymentPeriodCharges]
    @ThreshValue DECIMAL
   ,@IncrementType INTEGER
AS
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;



        IF ( @IncrementType IN ( 0,1 ) )  --  ActualHours      = 0  /ScheduledHours   = 1
            BEGIN
		-----------------------------------------------------------------
		--Table to store Student population
		-----------------------------------------------------------------
                DECLARE @MasterStudentPopulationSummaryHours TABLE
                    (
                     StudentId UNIQUEIDENTIFIER
                    ,StuEnrollId UNIQUEIDENTIFIER
                    ,ClsSectionId VARCHAR(250)
                    ,MaxDate DATETIME
                    ,PrgVerId UNIQUEIDENTIFIER
                    ,PrgVerCode VARCHAR(250)
                    ,TotHours DECIMAL
                    );
		-----------------------------------------------------------------
		-- Insert student population into table
		-----------------------------------------------------------------
                INSERT  INTO @MasterStudentPopulationSummaryHours
                        SELECT  SE.StudentId
                               ,SE.StuEnrollId
                               ,CS.ClsSectionId
                               ,MD.MaxDate
                               ,PV.PrgVerId
                               ,PV.PrgVerCode
                               ,TH.TotHours
                        FROM    dbo.arStuEnrollments AS SE
                        INNER JOIN dbo.syStudentAttendanceSummary AS SA ON SE.StuEnrollId = SA.StuEnrollId
                        INNER JOIN (
                                     SELECT StuEnrollId
                                           ,CASE WHEN ( @IncrementType = 0 ) -- ActualHours      = 0
                                                      THEN MAX(ActualRunningPresentDays)
                                                 WHEN ( @IncrementType = 1 ) -- ScheduledHours   = 1
                                                      THEN MAX(ActualRunningScheduledDays)
                                                 ELSE 0
                                            END AS TotHours
                                     FROM   dbo.syStudentAttendanceSummary
                                     GROUP BY StuEnrollId
                                   ) TH ON SA.StuEnrollId = TH.StuEnrollId
                        INNER JOIN (
                                     SELECT StuEnrollId
                                           ,MAX(StudentAttendedDate) AS MaxDate
                                     FROM   dbo.syStudentAttendanceSummary
                                     GROUP BY StuEnrollId
                                   ) MD ON SE.StuEnrollId = MD.StuEnrollId
                        INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                        INNER JOIN dbo.saIncrements AS I ON PV.BillingMethodId = I.BillingMethodId
                        INNER JOIN dbo.arClassSections CS ON SA.ClsSectionId = CS.ClsSectionId
                        INNER JOIN dbo.arTerm TT ON CS.TermId = TT.TermId
                        INNER JOIN dbo.syStatusCodes SC ON SE.StatusCodeId = SC.StatusCodeId
                        INNER JOIN dbo.syStatuses ST ON PV.StatusId = ST.StatusId
                        WHERE   SC.SysStatusId IN ( 7,9,20 )
                                AND I.IncrementType = @IncrementType;
		-----------------------------------------------------------------
		-- TABLE FOR HOURS DATA
		-----------------------------------------------------------------
                DECLARE @HoursScheduledActual TABLE
                    (
                     StuEnrollId UNIQUEIDENTIFIER
                    ,PrgVerId UNIQUEIDENTIFIER
                    ,TotHours DECIMAL
                    );
		-----------------------------------------------------------------
		-- insert hours data into table
		-----------------------------------------------------------------
                INSERT  INTO @HoursScheduledActual
                        SELECT  MP.StuEnrollId
                               ,MP.PrgVerId
                               ,MP.TotHours
                        FROM    @MasterStudentPopulationSummaryHours AS MP
                        GROUP BY MP.StuEnrollId
                               ,MP.PrgVerId
                               ,MP.TotHours;
		------------------------------------------------------------------
		-- Projected result
		------------------------------------------------------------------    
                SELECT DISTINCT
                        P.PmtPeriodId
                       ,ST.StudentId
                       ,E.StuEnrollId
                       ,ST.LastName
                       ,ST.FirstName
                       ,PV.PrgVerDescrip
                       ,PV.PrgVerCode
                       ,C.CampDescrip
                       ,P.ChargeAmount
                       ,( CASE WHEN I.IncrementType = @IncrementType THEN CAE.TotHours
                               ELSE 0
                          END ) AS CreditsHoursValue
                       ,P.CumulativeValue
                       ,( CASE WHEN ( P.CumulativeValue - ( CASE WHEN I.IncrementType = @IncrementType THEN CAE.TotHours
                                                                 ELSE 0
                                                            END ) ) < 0 THEN 0
                               ELSE ( P.CumulativeValue - ( CASE WHEN I.IncrementType = @IncrementType THEN CAE.TotHours
                                                                 ELSE 0
                                                            END ) )
                          END ) AS CreditHoursLeft
                       ,( P.CumulativeValue - @ThreshValue ) AS Threshhold
                       ,I.IncrementType
                       ,@IncrementType AS InputIncrementType
                FROM    saPmtPeriods AS P
                INNER JOIN dbo.saIncrements AS I ON P.IncrementId = I.IncrementId
                INNER JOIN dbo.saBillingMethods AS B ON I.BillingMethodId = B.BillingMethodId
                INNER JOIN dbo.arPrgVersions AS PV ON B.BillingMethodId = PV.BillingMethodId
                INNER JOIN dbo.arStuEnrollments AS E ON PV.PrgVerId = E.PrgVerId
                INNER JOIN dbo.syCampuses AS C ON E.CampusId = C.CampusId
                INNER JOIN dbo.arStudent AS ST ON E.StudentId = ST.StudentId
                INNER JOIN @HoursScheduledActual AS CAE ON E.StuEnrollId = CAE.StuEnrollId
                WHERE   B.BillingMethod = 3
                        AND E.StartDate >= I.EffectiveDate
                        AND E.StuEnrollId NOT IN ( SELECT   TR.StuEnrollId
                                                   FROM     dbo.saTransactions AS TR
                                                   WHERE    TR.StuEnrollId = E.StuEnrollId
                                                            AND TR.PmtPeriodId = P.PmtPeriodId )
                        AND E.StuEnrollId NOT IN ( SELECT   BI.StuEnrollId
                                                   FROM     dbo.saPmtPeriodBatchItems AS BI
                                                   INNER JOIN saPmtPeriodBatchHeaders AS BH ON BI.PmtPeriodBatchHeaderId = BH.PmtPeriodBatchHeaderId
                                                   WHERE    BI.StuEnrollId = E.StuEnrollId
                                                            AND BI.PmtPeriodId = P.PmtPeriodId
                                                            AND BH.IsPosted = 0 )
                        AND ( CASE WHEN I.IncrementType = @IncrementType THEN CAE.TotHours
                                   ELSE 0
                              END ) >= ( P.CumulativeValue - @ThreshValue );
 
            END;
        IF ( @IncrementType IN ( 2,3 ) ) --   CreditsAttempted = 2  /CreditsEarned    = 3
            BEGIN

                DECLARE @MasterStudentPopulationSummaryCredits TABLE
                    (
                     StudentId UNIQUEIDENTIFIER
                    ,StuEnrollId UNIQUEIDENTIFIER
                    ,PrgVerId UNIQUEIDENTIFIER
                    ,PrgVerCode VARCHAR(250)
                    ,TotCredits DECIMAL
                    ,Credits DECIMAL
                    ,IsCourseCompleted BIT
                    ,IsCreditsAttempted BIT
                    ,IsCreditsEarned BIT
                    );
		-----------------------------------------------------------------
		-- Insert student population into table
		-----------------------------------------------------------------
                INSERT  INTO @MasterStudentPopulationSummaryCredits
                        SELECT  SE.StudentId
                               ,SE.StuEnrollId
                               ,PV.PrgVerId
                               ,PV.PrgVerCode
                               ,CASE WHEN @IncrementType = 2
                                          AND G.IsCreditsAttempted = 1   --  CreditsAttempted = 2  
                                          THEN ISNULL(R.Credits,0)
                                     WHEN @IncrementType = 3
                                          AND G.IsCreditsEarned = 1      --  CreditsEarned    = 3
                                          THEN ISNULL(R.Credits,0)
                                     ELSE 0
                                END AS TotCredits
                               ,R.Credits
                               ,RES.IsCourseCompleted
                               ,G.IsCreditsAttempted
                               ,G.IsCreditsEarned
                        FROM    arStuEnrollments SE
                        INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                        INNER JOIN dbo.saIncrements AS I ON PV.BillingMethodId = I.BillingMethodId
                        INNER JOIN arResults RES ON RES.StuEnrollId = SE.StuEnrollId
                        INNER JOIN arClassSections CS ON RES.TestId = CS.ClsSectionId
                        INNER JOIN arReqs R ON CS.ReqId = R.ReqId
                        INNER JOIN dbo.arTerm tm ON CS.TermId = tm.TermId
                        INNER JOIN dbo.arGradeSystemDetails AS G ON RES.GrdSysDetailId = G.GrdSysDetailId
                        INNER JOIN dbo.syStatusCodes SC ON SE.StatusCodeId = SC.StatusCodeId
                        INNER JOIN dbo.syStatuses ST ON PV.StatusId = ST.StatusId
                        WHERE   SC.SysStatusId IN ( 7,9,20 )
                                AND IsCourseCompleted = 1
                                AND I.IncrementType = @IncrementType
                        ORDER BY SE.StuEnrollId
                               ,tm.StartDate;
		-----------------------------------------------------------------
		-- Insert student population into table for transfer grades
		-----------------------------------------------------------------
                INSERT  INTO @MasterStudentPopulationSummaryCredits
                        SELECT  SE.StudentId
                               ,SE.StuEnrollId
                               ,PV.PrgVerId
                               ,PV.PrgVerCode
                               ,CASE WHEN @IncrementType = 2
                                          AND g.IsCreditsAttempted = 1   --  CreditsAttempted = 2  
                                          THEN ISNULL(R.Credits,0)
                                     WHEN @IncrementType = 3
                                          AND g.IsCreditsEarned = 1      --  CreditsEarned    = 3
                                          THEN ISNULL(R.Credits,0)
                                     ELSE 0
                                END AS TotCredits
                               ,R.Credits
                               ,RES.IsCourseCompleted
                               ,g.IsCreditsAttempted
                               ,g.IsCreditsEarned
                        FROM    arStuEnrollments SE
                        INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                        INNER JOIN dbo.saIncrements AS I ON PV.BillingMethodId = I.BillingMethodId
                        INNER JOIN dbo.arTransferGrades RES ON RES.StuEnrollId = SE.StuEnrollId
                        INNER JOIN arReqs R ON RES.ReqId = R.ReqId
                        INNER JOIN dbo.arTerm tm ON RES.TermId = tm.TermId
                        INNER JOIN dbo.arGradeSystemDetails g ON RES.GrdSysDetailId = g.GrdSysDetailId
                        INNER JOIN dbo.syStatusCodes SC ON SE.StatusCodeId = SC.StatusCodeId
                        INNER JOIN dbo.syStatuses ST ON PV.StatusId = ST.StatusId
                        WHERE   SC.SysStatusId IN ( 7,9,20 )
                                AND IsCourseCompleted = 1
                                AND I.IncrementType = @IncrementType;
		-----------------------------------------------------------------
		-- GET sum of credits for students
		-----------------------------------------------------------------       
                DECLARE @CreditAttemptedEarned TABLE
                    (
                     StuEnrollId UNIQUEIDENTIFIER
                    ,PrgVerId UNIQUEIDENTIFIER
                    ,TotCredits DECIMAL
                    );

                INSERT  INTO @CreditAttemptedEarned
                        SELECT  T.StuEnrollId
                               ,T.PrgVerId
                               ,SUM(ISNULL(T.TotCredits,0)) AS TotCredits
                        FROM    @MasterStudentPopulationSummaryCredits T
                        GROUP BY T.StuEnrollId
                               ,T.PrgVerId
                        ORDER BY T.PrgVerId;


                SELECT DISTINCT
                        P.PmtPeriodId
                       ,st.StudentId
                       ,e.StuEnrollId
                       ,st.LastName
                       ,st.FirstName
                       ,PV.PrgVerDescrip
                       ,PV.PrgVerCode
                       ,C.CampDescrip
                       ,P.ChargeAmount
                       ,CASE WHEN i.IncrementType = @IncrementType THEN CAE.TotCredits
                             ELSE 0
                        END AS CreditsHoursValue
                       ,P.CumulativeValue
                       ,( CASE WHEN ( P.CumulativeValue - ( CASE WHEN i.IncrementType = @IncrementType THEN CAE.TotCredits
                                                                 ELSE 0
                                                            END ) ) < 0 THEN 0
                               ELSE ( P.CumulativeValue - ( CASE WHEN i.IncrementType = @IncrementType THEN CAE.TotCredits
                                                                 ELSE 0
                                                            END ) )
                          END ) AS CreditHoursLeft
                       ,( P.CumulativeValue - @ThreshValue ) AS Threshhold
                       ,i.IncrementType
                       ,@IncrementType AS InputIncrementType
                FROM    saPmtPeriods AS P
                INNER JOIN dbo.saIncrements i ON P.IncrementId = i.IncrementId
                INNER JOIN dbo.saBillingMethods B ON i.BillingMethodId = B.BillingMethodId
                INNER JOIN dbo.arPrgVersions AS PV ON B.BillingMethodId = PV.BillingMethodId
                INNER JOIN dbo.arStuEnrollments e ON PV.PrgVerId = e.PrgVerId
                INNER JOIN dbo.arStudent st ON e.StudentId = st.StudentId
                INNER JOIN dbo.syCampuses C ON e.CampusId = C.CampusId
                INNER JOIN @CreditAttemptedEarned CAE ON e.StuEnrollId = CAE.StuEnrollId
                WHERE   B.BillingMethod = 3
                        AND e.StartDate >= i.EffectiveDate
                        AND e.StuEnrollId NOT IN ( SELECT   StuEnrollId
                                                   FROM     dbo.saTransactions
                                                   WHERE    StuEnrollId = e.StuEnrollId
                                                            AND PmtPeriodId = P.PmtPeriodId )
                        AND e.StuEnrollId NOT IN ( SELECT   StuEnrollId
                                                   FROM     dbo.saPmtPeriodBatchItems i
                                                   INNER JOIN saPmtPeriodBatchHeaders h ON i.PmtPeriodBatchHeaderId = h.PmtPeriodBatchHeaderId
                                                   WHERE    StuEnrollId = e.StuEnrollId
                                                            AND PmtPeriodId = P.PmtPeriodId
                                                            AND h.IsPosted = 0 )
                        AND ( CASE WHEN i.IncrementType = @IncrementType THEN CAE.TotCredits
                                   ELSE 0
                              END ) >= ( P.CumulativeValue - @ThreshValue );
                RETURN;
            END;
    END;




GO
