SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/* 
PURPOSE: 
Get terms with classes with lab work and/or lab hour components for a selected student enrollment

CREATED: 


MODIFIED:
10/25/2013 WP	US4547 Refactor Post Services by Student process

ResourceId
Homework = 499
LabWork = 500
Exam = 501
Final = 502
LabHours = 503
Externship = 544

*/

CREATE PROCEDURE [dbo].[usp_GetClassroomWorkTerm]
    (
     @stuEnrollId UNIQUEIDENTIFIER  
    )
AS
    SET NOCOUNT ON;    
    SELECT DISTINCT
            cs.TermId
           ,t.TermDescrip
           ,t.StartDate
    FROM    arResults ar
    JOIN    arClassSections cs ON cs.ClsSectionId = ar.TestId
    JOIN    arTerm t ON t.TermId = cs.TermId
    WHERE   (
              SELECT    COUNT(0)
              FROM      arGrdBkWeights gbw
              JOIN      arGrdBkWgtDetails gbwd ON gbwd.InstrGrdBkWgtId = gbw.InstrGrdBkWgtId
              JOIN      arGrdComponentTypes gct ON gct.GrdComponentTypeId = gbwd.GrdComponentTypeId
              WHERE     gct.SysComponentTypeId IN ( 500,503 )
                        AND gbw.ReqId = cs.ReqId
                        AND gbw.InstrGrdBkWgtId = (
                                                    SELECT TOP 1
                                                            eff_gbw.InstrGrdBkWgtId
                                                    FROM    arGrdBkWeights eff_gbw
                                                    WHERE   eff_gbw.EffectiveDate <= cs.StartDate
                                                            AND eff_gbw.ReqId = cs.ReqId
                                                    ORDER BY eff_gbw.EffectiveDate DESC
                                                  )
            ) > 0
            AND ar.StuEnrollId = @stuEnrollId
    ORDER BY t.StartDate; 




GO
