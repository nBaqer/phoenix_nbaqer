SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =========================================================================================================      
-- USP_ManageSecurity_Tabs_Permissions      
-- =========================================================================================================      
--sp_helptext USP_ManageSecurity_Tabs_Permissions          
CREATE PROCEDURE [dbo].[USP_ManageSecurity_Tabs_Permissions]
    @RoleId VARCHAR(50)
   ,@ModuleResourceId INT
   ,@TabId INT
   ,@SchoolEnumerator INT
   ,@campusId UNIQUEIDENTIFIER
AS --DE7659 - Updated --          
    -- declare local variables              
    DECLARE @ShowRossOnlyTabs BIT
           ,@SchedulingMethod VARCHAR(50)
           ,@TrackSAPAttendance VARCHAR(50)
           ,@PostServicesByStudent BIT
           ,@PostServicesByClass BIT;
    DECLARE @ShowCollegeOfCourtReporting VARCHAR(5)
           ,@FameESP VARCHAR(5)
           ,@EdExpress VARCHAR(5);
    DECLARE @GradeBookWeightingLevel VARCHAR(20)
           ,@ShowExternshipTabs VARCHAR(5);
    DECLARE @AR_ParentId UNIQUEIDENTIFIER
           ,@PL_ParentId UNIQUEIDENTIFIER
           ,@FA_ParentId UNIQUEIDENTIFIER;
    DECLARE @FAC_ParentId UNIQUEIDENTIFIER
           ,@SA_ParentId UNIQUEIDENTIFIER
           ,@AD_ParentId UNIQUEIDENTIFIER;
    DECLARE @showNaccasReports VARCHAR(5);
    -- Get Values  
    IF  (
        SELECT COUNT(*)
        FROM   syConfigAppSetValues cv
        INNER JOIN syConfigAppSettings cs ON cs.SettingId = cv.SettingId
        WHERE  KeyName = 'ShowNACCASReports'
               AND CampusId = @campusId
               AND Active = 1
        ) >= 1
        BEGIN
            SET @showNaccasReports = (
                                     SELECT Value
                                     FROM   syConfigAppSetValues cv
                                     INNER JOIN syConfigAppSettings cs ON cs.SettingId = cv.SettingId
                                     WHERE  KeyName = 'ShowNACCASReports'
                                            AND CampusId = @campusId
                                            AND Active = 1
                                     );
        END;
    ELSE
        BEGIN
            SET @showNaccasReports = (
                                     SELECT Value
                                     FROM   syConfigAppSetValues cv
                                     INNER JOIN syConfigAppSettings cs ON cs.SettingId = cv.SettingId
                                     WHERE  KeyName = 'ShowNACCASReports'
                                            AND CampusId IS NULL
                                            AND Active = 1
                                     );
        END;

    --SET @SchoolEnumerator = 1689          
    --SET @ShowRossOnlyTabs = (SELECT VALUE FROM dbo.syConfigAppSetValues WHERE SettingId=68)          
    --SET @SchedulingMethod = (SELECT VALUE FROM dbo.syConfigAppSetValues WHERE SettingId=65)          
    --SET @TrackSAPAttendance = (SELECT VALUE FROM dbo.syConfigAppSetValues WHERE SettingId=72)          
    --SET @ShowCollegeOfCourtReporting = (SELECT VALUE FROM dbo.syConfigAppSetValues WHERE SettingId=118)          
    --SET @FameESP = (SELECT VALUE FROM dbo.syConfigAppSetValues WHERE SettingId=37)          
    --SET @EdExpress = (SELECT VALUE FROM dbo.syConfigAppSetValues WHERE SettingId=91)          
    --SET @GradeBookWeightingLevel = (SELECT VALUE FROM dbo.syConfigAppSetValues WHERE SettingId=43)          
    --SET @ShowExternshipTabs = (SELECT VALUE FROM dbo.syConfigAppSetValues WHERE SettingId=71)          

    SET @AD_ParentId = (
                       SELECT t1.HierarchyId
                       FROM   syNavigationNodes t1
                       INNER JOIN syNavigationNodes t2 ON t1.ParentId = t2.HierarchyId
                       WHERE  t1.ResourceId = 395
                              AND t2.ResourceId = 189
                       );


    -- Get Values              
    IF  (
        SELECT COUNT(*)
        FROM   syConfigAppSetValues
        WHERE  SettingId = 68
               AND CampusId = @campusId
        ) >= 1
        BEGIN
            SET @ShowRossOnlyTabs = (
                                    SELECT Value
                                    FROM   dbo.syConfigAppSetValues
                                    WHERE  SettingId = 68
                                           AND CampusId = @campusId
                                    );

        END;
    ELSE
        BEGIN
            SET @ShowRossOnlyTabs = (
                                    SELECT Value
                                    FROM   dbo.syConfigAppSetValues
                                    WHERE  SettingId = 68
                                           AND CampusId IS NULL
                                    );
        END;

    --PRINT @ShowRossOnlyTabs            

    IF  (
        SELECT COUNT(*)
        FROM   syConfigAppSetValues
        WHERE  SettingId = 65
               AND CampusId = @campusId
        ) >= 1
        BEGIN
            SET @SchedulingMethod = (
                                    SELECT Value
                                    FROM   dbo.syConfigAppSetValues
                                    WHERE  SettingId = 65
                                           AND CampusId = @campusId
                                    );

        END;
    ELSE
        BEGIN
            SET @SchedulingMethod = (
                                    SELECT Value
                                    FROM   dbo.syConfigAppSetValues
                                    WHERE  SettingId = 65
                                           AND CampusId IS NULL
                                    );
        END;



    IF  (
        SELECT COUNT(*)
        FROM   syConfigAppSetValues
        WHERE  SettingId = 72
               AND CampusId = @campusId
        ) >= 1
        BEGIN
            SET @TrackSAPAttendance = (
                                      SELECT Value
                                      FROM   dbo.syConfigAppSetValues
                                      WHERE  SettingId = 72
                                             AND CampusId = @campusId
                                      );

        END;
    ELSE
        BEGIN
            SET @TrackSAPAttendance = (
                                      SELECT Value
                                      FROM   dbo.syConfigAppSetValues
                                      WHERE  SettingId = 72
                                             AND CampusId IS NULL
                                      );
        END;


    IF  (
        SELECT COUNT(*)
        FROM   syConfigAppSetValues
        WHERE  SettingId = 118
               AND CampusId = @campusId
        ) >= 1
        BEGIN
            SET @ShowCollegeOfCourtReporting = (
                                               SELECT Value
                                               FROM   dbo.syConfigAppSetValues
                                               WHERE  SettingId = 118
                                                      AND CampusId = @campusId
                                               );

        END;
    ELSE
        BEGIN
            SET @ShowCollegeOfCourtReporting = (
                                               SELECT Value
                                               FROM   dbo.syConfigAppSetValues
                                               WHERE  SettingId = 118
                                                      AND CampusId IS NULL
                                               );
        END;


    IF  (
        SELECT COUNT(*)
        FROM   syConfigAppSetValues
        WHERE  SettingId = 37
               AND CampusId = @campusId
        ) >= 1
        BEGIN
            SET @FameESP = (
                           SELECT Value
                           FROM   dbo.syConfigAppSetValues
                           WHERE  SettingId = 37
                                  AND CampusId = @campusId
                           );

        END;
    ELSE
        BEGIN
            SET @FameESP = (
                           SELECT Value
                           FROM   dbo.syConfigAppSetValues
                           WHERE  SettingId = 37
                                  AND CampusId IS NULL
                           );
        END;


    IF  (
        SELECT COUNT(*)
        FROM   syConfigAppSetValues
        WHERE  SettingId = 91
               AND CampusId = @campusId
        ) >= 1
        BEGIN
            SET @EdExpress = (
                             SELECT Value
                             FROM   dbo.syConfigAppSetValues
                             WHERE  SettingId = 91
                                    AND CampusId = @campusId
                             );

        END;
    ELSE
        BEGIN
            SET @EdExpress = (
                             SELECT Value
                             FROM   dbo.syConfigAppSetValues
                             WHERE  SettingId = 91
                                    AND CampusId IS NULL
                             );
        END;



    IF  (
        SELECT COUNT(*)
        FROM   syConfigAppSetValues
        WHERE  SettingId = 43
               AND CampusId = @campusId
        ) >= 1
        BEGIN
            SET @GradeBookWeightingLevel = (
                                           SELECT Value
                                           FROM   dbo.syConfigAppSetValues
                                           WHERE  SettingId = 43
                                                  AND CampusId = @campusId
                                           );

        END;
    ELSE
        BEGIN
            SET @GradeBookWeightingLevel = (
                                           SELECT Value
                                           FROM   dbo.syConfigAppSetValues
                                           WHERE  SettingId = 43
                                                  AND CampusId IS NULL
                                           );
        END;



    IF  (
        SELECT COUNT(*)
        FROM   syConfigAppSetValues
        WHERE  SettingId = 71
               AND CampusId = @campusId
        ) >= 1
        BEGIN
            SET @ShowExternshipTabs = (
                                      SELECT Value
                                      FROM   dbo.syConfigAppSetValues
                                      WHERE  SettingId = 71
                                             AND CampusId = @campusId
                                      );

        END;
    ELSE
        BEGIN
            SET @ShowExternshipTabs = (
                                      SELECT Value
                                      FROM   dbo.syConfigAppSetValues
                                      WHERE  SettingId = 71
                                             AND CampusId IS NULL
                                      );
        END;

    SET @PostServicesByStudent = (
                                 SELECT Value
                                 FROM   dbo.syConfigAppSetValues
                                 WHERE  SettingId = (
                                                    SELECT SettingId
                                                    FROM   dbo.syConfigAppSettings
                                                    WHERE  KeyName = 'PostServicesByStudent'
                                                    )
                                        AND CampusId IS NULL
                                 );
    SET @PostServicesByClass = (
                               SELECT Value
                               FROM   dbo.syConfigAppSetValues
                               WHERE  SettingId = (
                                                  SELECT SettingId
                                                  FROM   dbo.syConfigAppSettings
                                                  WHERE  KeyName = 'PostServicesByClass'
                                                  )
                                      AND CampusId IS NULL
                               );

    SELECT   *
            ,CASE WHEN AccessLevel = 15 THEN 1
                  ELSE 0
             END AS FullPermission
            ,CASE WHEN AccessLevel IN ( 14, 13, 12, 11, 10, 9, 8 ) THEN 1
                  ELSE 0
             END AS EditPermission
            ,CASE WHEN AccessLevel IN ( 14, 13, 12, 7, 6, 5, 4 ) THEN 1
                  ELSE 0
             END AS AddPermission
            ,CASE WHEN AccessLevel IN ( 14, 11, 10, 7, 6, 2 ) THEN 1
                  ELSE 0
             END AS DeletePermission
            ,CASE WHEN AccessLevel IN ( 13, 11, 9, 7, 5, 3, 1 ) THEN 1
                  ELSE 0
             END AS ReadPermission
    FROM     (
             SELECT 189 AS ModuleResourceId
                   ,395 AS TabId
                   ,ResourceID AS ChildResourceId
                   ,Resource AS ChildResource
                   ,(
                    SELECT MAX(AccessLevel)
                    FROM   syRlsResLvls
                    WHERE  RoleId = @RoleId
                           AND ResourceID = syResources.ResourceID
                    ) AS AccessLevel
                   ,ResourceURL AS ChildResourceURL
                   ,NULL AS ParentResourceId
                   ,NULL AS ParentResource
                   ,1 AS GroupSortOrder
                   ,NULL AS FirstSortOrder
                   ,NULL AS DisplaySequence
                   ,ResourceTypeID
             FROM   syResources
             WHERE  ResourceID = 189
             UNION
             SELECT 189 AS ModuleResourceId
                   ,395 AS TabId
                   ,NNChild.ResourceId AS ChildResourceId
                   ,CASE WHEN NNChild.ResourceId = 395 THEN 'Lead Tabs'
                         ELSE CASE WHEN NNChild.ResourceId = 398 THEN 'Add/View Leads'
                                   ELSE RChild.Resource
                              END
                    END AS ChildResource
                   ,(
                    SELECT MAX(AccessLevel)
                    FROM   syRlsResLvls
                    WHERE  RoleId = @RoleId
                           AND ResourceID = RChild.ResourceID
                    ) AS AccessLevel
                   ,RChild.ResourceURL AS ChildResourceURL
                   ,CASE WHEN (
                              NNParent.ResourceId IN ( 395 )
                              OR NNChild.ResourceId IN ( 737, 740, 741, 792 )
                              ) THEN NULL
                         ELSE NNParent.ResourceId
                    END AS ParentResourceId
                   ,RParent.Resource AS ParentResource
                   ,CASE WHEN (
                              NNChild.ResourceId = 737
                              OR NNParent.ResourceId IN ( 737 )
                              ) THEN 1
                         ELSE CASE WHEN (
                                        NNChild.ResourceId = 740
                                        OR NNParent.ResourceId IN ( 740 )
                                        ) THEN 2
                                   ELSE CASE WHEN (
                                                  NNChild.ResourceId = 741
                                                  OR NNParent.ResourceId IN ( 741 )
                                                  ) THEN 3
                                             ELSE 4
                                        END
                              END
                    END AS GroupSortOrder
                   ,CASE WHEN NNChild.ResourceId = 737 THEN 1
                         ELSE CASE WHEN NNChild.ResourceId = 395 THEN 2
                                   ELSE 3
                              END
                    END AS FirstSortOrder
                   ,CASE WHEN (
                              NNChild.ResourceId IN ( 737 )
                              OR NNParent.ResourceId IN ( 737 )
                              ) THEN 1
                         ELSE 2
                    END AS DisplaySequence
                   ,RChild.ResourceTypeID
             FROM   syResources RChild
             INNER JOIN syNavigationNodes NNChild ON RChild.ResourceID = NNChild.ResourceId
             INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
             INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
             LEFT OUTER JOIN (
                             SELECT *
                             FROM   syResources
                             WHERE  ResourceTypeID = 1
                             ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
             WHERE  RChild.ResourceTypeID IN ( 2, 3, 8 )
                    AND (
                        RChild.ChildTypeId IS NULL
                        OR RChild.ChildTypeId = 3
                        )
                    AND ( RChild.ResourceID NOT IN ( 394 ))
                    AND (
                        NNParent.ParentId IN (
                                             SELECT HierarchyId
                                             FROM   syNavigationNodes
                                             WHERE  ResourceId = 395
                                             )
                        OR NNChild.HierarchyId IN (
                                                  SELECT DISTINCT HierarchyId
                                                  FROM   syNavigationNodes
                                                  WHERE  ResourceId IN ( 737, 740, 741, 792 )
                                                         AND ParentId IN (
                                                                         SELECT HierarchyId
                                                                         FROM   syNavigationNodes
                                                                         WHERE  ResourceId = 395
                                                                                AND ParentId IN (
                                                                                                SELECT HierarchyId
                                                                                                FROM   syNavigationNodes
                                                                                                WHERE  ResourceId = 189
                                                                                                )
                                                                         )
                                                  )
                        )
             UNION ALL
             SELECT 26 AS ModuleResourceId
                   ,394 AS TabId
                   ,ResourceID AS ChildResourceId
                   ,Resource AS ChildResource
                   ,(
                    SELECT MAX(AccessLevel)
                    FROM   syRlsResLvls
                    WHERE  RoleId = @RoleId
                           AND ResourceID = syResources.ResourceID
                    ) AS AccessLevel
                   ,ResourceURL AS ChildResourceURL
                   ,NULL AS ParentResourceId
                   ,NULL AS ParentResource
                   ,1 AS GroupSortOrder
                   ,NULL AS FirstSortOrder
                   ,NULL AS DisplaySequence
                   ,ResourceTypeID
             FROM   syResources
             WHERE  ResourceID = 26
             UNION
             SELECT 26 AS ModuleResourceId
                   ,394 AS TabId
                   ,ChildResourceId
                   ,ChildResource
                   ,(
                    SELECT MAX(AccessLevel)
                    FROM   syRlsResLvls
                    WHERE  RoleId = @RoleId
                           AND ResourceID = t1.ChildResourceId
                    ) AS AccessLevel
                   ,ChildResourceURL
                   ,ParentResourceId
                   ,ParentResource
                   ,CASE WHEN (
                              t1.ChildResourceId = 737
                              OR t1.ParentResourceId IN ( 737 )
                              ) THEN 2
                         ELSE CASE WHEN (
                                        t1.ChildResourceId = 739
                                        OR t1.ParentResourceId IN ( 739 )
                                        ) THEN 3
                                   ELSE CASE WHEN (
                                                  t1.ChildResourceId = 738
                                                  OR t1.ParentResourceId IN ( 738 )
                                                  ) THEN 4
                                             ELSE CASE WHEN (
                                                            t1.ChildResourceId = 740
                                                            OR t1.ParentResourceId IN ( 740 )
                                                            ) THEN 5
                                                       ELSE CASE WHEN (
                                                                      t1.ChildResourceId = 741
                                                                      OR t1.ParentResourceId IN ( 741 )
                                                                      ) THEN 6
                                                                 ELSE 7
                                                            END
                                                  END
                                        END
                              END
                    END AS GroupSortOrder
                   ,CASE WHEN ChildResourceId = 738 THEN 1
                         ELSE 2
                    END AS FirstSortOrder
                   ,CASE WHEN (
                              ChildResourceId IN ( 738 )
                              OR ParentResourceId IN ( 738 )
                              ) THEN 1
                         ELSE 2
                    END AS DisplaySequence
                   ,ResourceTypeID
             FROM   (
                    SELECT DISTINCT NNChild.ResourceId AS ChildResourceId
                          ,CASE WHEN NNChild.ResourceId = 409 THEN 'IPEDS - General Reports'
                                ELSE RChild.Resource
                           END AS ChildResource
                          ,RChild.ResourceURL AS ChildResourceURL
                          ,CASE WHEN (
                                     NNParent.ResourceId IN ( 689 )
                                     OR NNChild.ResourceId IN ( 737, 738, 739, 740, 741, 742 )
                                     ) THEN NULL
                                ELSE NNParent.ResourceId
                           END AS ParentResourceId
                          ,RParent.Resource AS ParentResource
                          ,RParentModule.Resource AS MODULE
                          ,RChild.ResourceTypeID
                    FROM   syResources RChild
                    INNER JOIN syNavigationNodes NNChild ON RChild.ResourceID = NNChild.ResourceId
                    INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                    INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                    LEFT OUTER JOIN (
                                    SELECT *
                                    FROM   syResources
                                    WHERE  ResourceTypeID = 1
                                    ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                    WHERE  RChild.ResourceTypeID IN ( 2, 3, 8 )
                           AND (
                               RChild.ChildTypeId IS NULL
                               OR RChild.ChildTypeId = 3
                               )
                           AND ( NNParent.ParentId IN (
                                                      SELECT HierarchyId
                                                      FROM   syNavigationNodes
                                                      WHERE  ResourceId = 394
                                                             AND ParentId IN (
                                                                             SELECT HierarchyId
                                                                             FROM   syNavigationNodes
                                                                             WHERE  ResourceId = 26
                                                                             )
                                                      )
                               )
                    UNION
                    SELECT DISTINCT RChild.ResourceID AS ChildResourceId
                          ,RChild.Resource AS ChildResource
                          ,RChild.ResourceURL AS ChildResourceURL
                          ,CASE WHEN ( RChild.ResourceID IN ( 737, 738, 739, 740, 741, 742 )) THEN NULL
                                ELSE RChild.ResourceID
                           END AS ParentResourceId
                          ,NULL AS ParentResource
                          ,NULL AS MODULE
                          ,RChild.ResourceTypeID
                    FROM   syResources RChild
                    WHERE  RChild.ResourceID IN ( 737, 738, 739, 740, 741, 742 )
                    ) t1
             UNION ALL


             --IPEDS TAB   - DE7659                     
             SELECT 26 AS ModuleResourceId
                   ,689 AS TabId
                   ,ResourceID AS ChildResourceId
                   ,Resource AS ChildResource
                   ,(
                    SELECT MAX(AccessLevel)
                    FROM   syRlsResLvls
                    WHERE  RoleId = @RoleId
                           AND ResourceID = syResources.ResourceID
                    ) AS AccessLevel
                   ,ResourceURL AS ChildResourceURL
                   ,NULL AS ParentResourceId
                   ,NULL AS ParentResource
                   ,1 AS GroupSortOrder
                   ,NULL AS FirstSortOrder
                   ,NULL AS DisplaySequence
                   ,ResourceTypeID
             FROM   syResources
             WHERE  ResourceID = 26
             UNION
             SELECT 26 AS ModuleResourceId
                   ,689 AS TabId
                   ,ChildResourceId
                   ,ChildResource
                   ,(
                    SELECT MAX(AccessLevel)
                    FROM   syRlsResLvls
                    WHERE  RoleId = @RoleId
                           AND ResourceID = t1.ChildResourceId
                    ) AS AccessLevel
                   ,ChildResourceURL
                   ,ParentResourceId
                   ,ParentResource
                   ,CASE WHEN (
                              t1.ChildResourceId = 409
                              OR t1.ParentResourceId IN ( 409 )
                              ) THEN 2
                         WHEN (
                              t1.ChildResourceId = 719
                              OR t1.ParentResourceId IN ( 719 )
                              ) THEN 3
                         WHEN (
                              t1.ChildResourceId = 720
                              OR t1.ParentResourceId IN ( 720 )
                              ) THEN 4
                         WHEN (
                              t1.ChildResourceId = 721
                              OR t1.ParentResourceId IN ( 721 )
                              ) THEN 5
                         WHEN (
                              t1.ChildResourceId = 725
                              OR t1.ParentResourceId IN ( 725 )
                              ) THEN 6
                         WHEN (
                              t1.ChildResourceId = 722
                              OR t1.ParentResourceId IN ( 722 )
                              ) THEN 7
                         WHEN (
                              t1.ChildResourceId = 723
                              OR t1.ParentResourceId IN ( 723 )
                              ) THEN 8
                         WHEN (
                              t1.ChildResourceId = 724
                              OR t1.ParentResourceId IN ( 724 )
                              ) THEN 9
                         --IPEDS UPDATE          
                         WHEN (
                              t1.ChildResourceId = 789
                              OR t1.ParentResourceId IN ( 789 )
                              ) THEN 10
                         WHEN (
                              t1.ChildResourceId = 839
                              OR t1.ParentResourceId IN ( 839 )
                              ) THEN 11
                         WHEN (
                              t1.ChildResourceId = 844
                              OR t1.ParentResourceId IN ( 844 )
                              ) THEN 12
                         WHEN (
                              t1.ChildResourceId = 845
                              OR t1.ParentResourceId IN ( 845 )
                              ) THEN 13
                         WHEN (
                              t1.ChildResourceId = 846
                              OR t1.ParentResourceId IN ( 846 )
                              ) THEN 14
                         WHEN (
                              t1.ChildResourceId = 847
                              OR t1.ParentResourceId IN ( 847 )
                              ) THEN 15
                         WHEN (
                              t1.ChildResourceId = 848
                              OR t1.ParentResourceId IN ( 847 )
                              ) THEN 16
                         ELSE 17
                    END AS GroupSortOrder
                   ,CASE WHEN ChildResourceId = 409 THEN 1
                         ELSE 2
                    END AS FirstSortOrder

                   --( SELECT TOP 1          
                   --            HierarchyIndex          
                   --  FROM      dbo.syNavigationNodes          
                   --  WHERE     ResourceId = ChildResourceId          
                   --) AS FirstSortOrder,          
                   ,CASE WHEN (
                              ChildResourceId IN ( 409 )
                              OR ParentResourceId IN ( 409 )
                              ) THEN 1
                         ELSE 2
                    END AS DisplaySequence
                   ,ResourceTypeID
             FROM   (
                    SELECT DISTINCT NNChild.ResourceId AS ChildResourceId
                          ,RChild.Resource AS ChildResource
                          ,RChild.ResourceURL AS ChildResourceURL
                          ,CASE WHEN (
                                     NNParent.ResourceId IN ( 689 )
                                     OR NNChild.ResourceId IN ( 409, 719, 720, 721, 725, 722, 723, 724, 789, 839 )
                                     ) THEN NULL
                                ELSE NNParent.ResourceId
                           END AS ParentResourceId
                          ,RParent.Resource AS ParentResource
                          ,RParentModule.Resource AS MODULE
                          ,RChild.ResourceTypeID
                    FROM   syResources RChild
                    INNER JOIN syNavigationNodes NNChild ON RChild.ResourceID = NNChild.ResourceId
                    INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                    INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                    LEFT OUTER JOIN (
                                    SELECT *
                                    FROM   syResources
                                    WHERE  ResourceTypeID = 1
                                    ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                    WHERE  RChild.ResourceTypeID IN ( 2, 5, 8 )
                           AND (
                               RChild.ChildTypeId IS NULL
                               OR RChild.ChildTypeId = 5
                               )
                           AND ( RChild.ResourceID NOT IN ( 691, 690, 729, 730, 736, 678, 692, 727, 472, 474 )) -- 394, 472, 474, 711 ,712   
                           AND (
                               @showNaccasReports = 'no'
                               AND RChild.ResourceID NOT IN ( 844, 845, 846, 847, 848 )
                               OR @showNaccasReports = 'yes'
                               )
                           AND (
                               NNParent.ParentId IN (
                                                    SELECT HierarchyId
                                                    FROM   syNavigationNodes
                                                    WHERE  ResourceId = 26
                                                    )
                               OR NNChild.ParentId IN (
                                                      SELECT HierarchyId
                                                      FROM   syNavigationNodes
                                                      WHERE  CASE WHEN @showNaccasReports = 'no'
                                                                       AND ResourceId IN ( 409, 719, 720, 721, 722, 723, 724, 725, 789, 839 ) THEN 1
                                                                  WHEN @showNaccasReports = 'yes'
                                                                       AND ResourceId IN ( 409, 719, 720, 721, 722, 723, 724, 725, 789, 839, 845, 846, 847, 848 ) THEN
                                                                      1
                                                             END = 1
                                                      )
                                  AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
                               )
                    ) t1
             UNION ALL
             SELECT 194 AS ModuleResourceId
                   ,394 AS TabId
                   ,ResourceID AS ChildResourceId
                   ,Resource AS ChildResource
                   ,(
                    SELECT MAX(AccessLevel)
                    FROM   syRlsResLvls
                    WHERE  RoleId = @RoleId
                           AND ResourceID = syResources.ResourceID
                    ) AS AccessLevel
                   ,ResourceURL AS ChildResourceURL
                   ,NULL AS ParentResourceId
                   ,NULL AS ParentResource
                   ,1 AS GroupSortOrder
                   ,NULL AS FirstSortOrder
                   ,NULL AS DisplaySequence
                   ,ResourceTypeID
             FROM   syResources
             WHERE  ResourceID = 194
             UNION
             SELECT 194 AS ModuleResourceId
                   ,394 AS TabId
                   ,ChildResourceId
                   ,ChildResource
                   ,(
                    SELECT MAX(AccessLevel)
                    FROM   syRlsResLvls
                    WHERE  RoleId = @RoleId
                           AND ResourceID = t1.ChildResourceId
                    ) AS AccessLevel
                   ,ChildResourceURL
                   ,ParentResourceId
                   ,ParentResource
                   ,CASE WHEN (
                              t1.ChildResourceId = 737
                              OR t1.ParentResourceId IN ( 737 )
                              ) THEN 2
                         ELSE CASE WHEN (
                                        t1.ChildResourceId = 739
                                        OR t1.ParentResourceId IN ( 739 )
                                        ) THEN 3
                                   ELSE CASE WHEN (
                                                  t1.ChildResourceId = 738
                                                  OR t1.ParentResourceId IN ( 738 )
                                                  ) THEN 4
                                             ELSE CASE WHEN (
                                                            t1.ChildResourceId = 740
                                                            OR t1.ParentResourceId IN ( 740 )
                                                            ) THEN 5
                                                       ELSE CASE WHEN (
                                                                      t1.ChildResourceId = 741
                                                                      OR t1.ParentResourceId IN ( 741 )
                                                                      ) THEN 6
                                                                 ELSE CASE WHEN (
                                                                                t1.ChildResourceId = 742
                                                                                OR t1.ParentResourceId IN ( 742 )
                                                                                ) THEN 7
                                                                      END
                                                            END
                                                  END
                                        END
                              END
                    END AS GroupSortOrder
                   ,CASE WHEN ChildResourceId = 737 THEN 1
                         ELSE 2
                    END AS FirstSortOrder
                   ,CASE WHEN (
                              ChildResourceId IN ( 737, 738, 740, 741, 742 )
                              OR ParentResourceId IN ( 737, 738, 740, 741, 742 )
                              ) THEN 1
                         ELSE 2
                    END AS DisplaySequence
                   ,ResourceTypeID
             FROM   (
                    SELECT DISTINCT NNChild.ResourceId AS ChildResourceId
                          ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                ELSE RChild.Resource
                           END AS ChildResource
                          ,RChild.ResourceURL AS ChildResourceURL
                          ,CASE WHEN NNParent.ResourceId IN ( 194 )
                                     OR NNChild.ResourceId IN ( 737, 738, 739, 740, 741, 742 ) THEN NULL
                                ELSE NNParent.ResourceId
                           END AS ParentResourceId
                          ,RParent.Resource AS ParentResource
                          ,RParentModule.Resource AS MODULE
                          ,RChild.ResourceTypeID
                    FROM   syResources RChild
                    INNER JOIN syNavigationNodes NNChild ON RChild.ResourceID = NNChild.ResourceId
                    INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                    INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                    LEFT OUTER JOIN (
                                    SELECT *
                                    FROM   syResources
                                    WHERE  ResourceTypeID = 1
                                    ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                    WHERE  RChild.ResourceTypeID IN ( 2, 3, 8 )
                           AND (
                               RChild.ChildTypeId IS NULL
                               OR RChild.ChildTypeId = 3
                               )
                           --  AND  RChild.ChildTypeId           
                           AND ( NNParent.ParentId IN (
                                                      SELECT HierarchyId
                                                      FROM   syNavigationNodes
                                                      WHERE  ResourceId = 394
                                                             AND ParentId IN (
                                                                             SELECT HierarchyId
                                                                             FROM   syNavigationNodes
                                                                             WHERE  ResourceId = 194
                                                                             )
                                                      )
                               )
                    UNION
                    SELECT DISTINCT RChild.ResourceID AS ChildResourceId
                          ,RChild.Resource AS ChildResource
                          ,RChild.ResourceURL AS ChildResourceURL
                          ,CASE WHEN ( RChild.ResourceID IN ( 737, 738, 739, 740, 741, 742 )) THEN NULL
                                ELSE RChild.ResourceID
                           END AS ParentResourceId
                          ,NULL AS ParentResource
                          ,NULL AS MODULE
                          ,RChild.ResourceTypeID
                    FROM   syResources RChild
                    WHERE  RChild.ResourceID IN ( 737, 738, 739, 740, 741, 742 )
                    ) t1
             UNION ALL
             SELECT 300 AS ModuleResourceId
                   ,394 AS TabId
                   ,ResourceID AS ChildResourceId
                   ,Resource AS ChildResource
                   ,(
                    SELECT MAX(AccessLevel)
                    FROM   syRlsResLvls
                    WHERE  RoleId = @RoleId
                           AND ResourceID = syResources.ResourceID
                    ) AS AccessLevel
                   ,ResourceURL AS ChildResourceURL
                   ,NULL AS ParentResourceId
                   ,NULL AS ParentResource
                   ,1 AS GroupSortOrder
                   ,NULL AS FirstSortOrder
                   ,NULL AS DisplaySequence
                   ,ResourceTypeID
             FROM   syResources
             WHERE  ResourceID = 300
             UNION
             SELECT 300 AS ModuleResourceId
                   ,394 AS TabId
                   ,ChildResourceId
                   ,ChildResource
                   ,(
                    SELECT MAX(AccessLevel)
                    FROM   syRlsResLvls
                    WHERE  RoleId = @RoleId
                           AND ResourceID = t1.ChildResourceId
                    ) AS AccessLevel
                   ,ChildResourceURL
                   ,ParentResourceId
                   ,ParentResource
                   ,CASE WHEN (
                              t1.ChildResourceId = 737
                              OR t1.ParentResourceId IN ( 737 )
                              ) THEN 1
                         ELSE CASE WHEN (
                                        t1.ChildResourceId = 739
                                        OR t1.ParentResourceId IN ( 739 )
                                        ) THEN 2
                                   ELSE CASE WHEN (
                                                  t1.ChildResourceId = 738
                                                  OR t1.ParentResourceId IN ( 738 )
                                                  ) THEN 3
                                             ELSE CASE WHEN (
                                                            t1.ChildResourceId = 740
                                                            OR t1.ParentResourceId IN ( 740 )
                                                            ) THEN 4
                                                       ELSE CASE WHEN (
                                                                      t1.ChildResourceId = 741
                                                                      OR t1.ParentResourceId IN ( 741 )
                                                                      ) THEN 5
                                                                 ELSE CASE WHEN (
                                                                                t1.ChildResourceId = 742
                                                                                OR t1.ParentResourceId IN ( 742 )
                                                                                ) THEN 6
                                                                      END
                                                            END
                                                  END
                                        END
                              END
                    END AS GroupSortOrder
                   ,CASE WHEN ChildResourceId = 737 THEN 1
                         ELSE 2
                    END AS FirstSortOrder
                   ,CASE WHEN (
                              ChildResourceId IN ( 737, 739 )
                              OR ParentResourceId IN ( 737, 739 )
                              ) THEN 1
                         ELSE 2
                    END AS DisplaySequence
                   ,ResourceTypeID
             FROM   (
                    SELECT DISTINCT NNChild.ResourceId AS ChildResourceId
                          ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                ELSE RChild.Resource
                           END AS ChildResource
                          ,RChild.ResourceURL AS ChildResourceURL
                          ,CASE WHEN NNParent.ResourceId = 300
                                     OR NNChild.ResourceId IN ( 737, 739, 740, 741, 742 ) THEN NULL
                                ELSE NNParent.ResourceId
                           END AS ParentResourceId
                          ,RParent.Resource AS ParentResource
                          ,RParentModule.Resource AS MODULE
                          ,RChild.ResourceTypeID
                    FROM   syResources RChild
                    INNER JOIN syNavigationNodes NNChild ON RChild.ResourceID = NNChild.ResourceId
                    INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                    INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                    LEFT OUTER JOIN (
                                    SELECT *
                                    FROM   syResources
                                    WHERE  ResourceTypeID = 1
                                    ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                    WHERE  RChild.ResourceTypeID IN ( 2, 3, 8 )
                           AND (
                               RChild.ChildTypeId IS NULL
                               OR RChild.ChildTypeId = 3
                               )
                           AND ( NNParent.ParentId IN (
                                                      SELECT HierarchyId
                                                      FROM   syNavigationNodes
                                                      WHERE  ResourceId = 394
                                                             AND ParentId IN (
                                                                             SELECT HierarchyId
                                                                             FROM   syNavigationNodes
                                                                             WHERE  ResourceId = 300
                                                                             )
                                                      )
                               )
                    UNION
                    SELECT DISTINCT RChild.ResourceID AS ChildResourceId
                          ,RChild.Resource AS ChildResource
                          ,RChild.ResourceURL AS ChildResourceURL
                          ,CASE WHEN ( RChild.ResourceID IN ( 737, 738, 739, 740, 741, 742 )) THEN NULL
                                ELSE RChild.ResourceID
                           END AS ParentResourceId
                          ,NULL AS ParentResource
                          ,NULL AS MODULE
                          ,RChild.ResourceTypeID
                    FROM   syResources RChild
                    WHERE  RChild.ResourceID IN ( 737, 738, 739, 740, 741, 742 )
                    ) t1
             UNION
             SELECT 191 AS ModuleResourceId
                   ,394 AS TabId
                   ,ResourceID AS ChildResourceId
                   ,Resource AS ChildResource
                   ,(
                    SELECT MAX(AccessLevel)
                    FROM   syRlsResLvls
                    WHERE  RoleId = @RoleId
                           AND ResourceID = syResources.ResourceID
                    ) AS AccessLevel
                   ,ResourceURL AS ChildResourceURL
                   ,NULL AS ParentResourceId
                   ,NULL AS ParentResource
                   ,1 AS GroupSortOrder
                   ,NULL AS FirstSortOrder
                   ,NULL AS DisplaySequence
                   ,ResourceTypeID
             FROM   syResources
             WHERE  ResourceID = 191
             UNION
             SELECT 191 AS ModuleResourceId
                   ,394 AS TabId
                   ,ChildResourceId
                   ,ChildResource
                   ,(
                    SELECT MAX(AccessLevel)
                    FROM   syRlsResLvls
                    WHERE  RoleId = @RoleId
                           AND ResourceID = t1.ChildResourceId
                    ) AS AccessLevel
                   ,ChildResourceURL
                   ,ParentResourceId
                   ,ParentResource
                   ,CASE WHEN (
                              t1.ChildResourceId = 737
                              OR t1.ParentResourceId IN ( 737 )
                              ) THEN 1
                         ELSE CASE WHEN (
                                        t1.ChildResourceId = 739
                                        OR t1.ParentResourceId IN ( 739 )
                                        ) THEN 2
                                   ELSE CASE WHEN (
                                                  t1.ChildResourceId = 738
                                                  OR t1.ParentResourceId IN ( 738 )
                                                  ) THEN 3
                                             ELSE CASE WHEN (
                                                            t1.ChildResourceId = 740
                                                            OR t1.ParentResourceId IN ( 740 )
                                                            ) THEN 4
                                                       ELSE CASE WHEN (
                                                                      t1.ChildResourceId = 741
                                                                      OR t1.ParentResourceId IN ( 741 )
                                                                      ) THEN 5
                                                                 ELSE CASE WHEN (
                                                                                t1.ChildResourceId = 742
                                                                                OR t1.ParentResourceId IN ( 742 )
                                                                                ) THEN 6
                                                                      END
                                                            END
                                                  END
                                        END
                              END
                    END AS GroupSortOrder
                   ,CASE WHEN ChildResourceId = 737 THEN 1
                         ELSE 2
                    END AS FirstSortOrder
                   ,CASE WHEN (
                              ChildResourceId IN ( 737, 738, 740, 741, 742 )
                              OR ParentResourceId IN ( 737, 738, 740, 741, 742 )
                              ) THEN 1
                         ELSE 2
                    END AS DisplaySequence
                   ,ResourceTypeID
             FROM   (
                    SELECT DISTINCT NNChild.ResourceId AS ChildResourceId
                          ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                ELSE RChild.Resource
                           END AS ChildResource
                          ,RChild.ResourceURL AS ChildResourceURL
                          ,CASE WHEN (
                                     NNParent.ResourceId = 191
                                     OR NNChild.ResourceId IN ( 737, 738, 739, 740, 741, 742 )
                                     ) THEN NULL
                                ELSE NNParent.ResourceId
                           END AS ParentResourceId
                          ,RParent.Resource AS ParentResource
                          ,RParentModule.Resource AS MODULE
                          ,RChild.ResourceTypeID
                    FROM   syResources RChild
                    INNER JOIN syNavigationNodes NNChild ON RChild.ResourceID = NNChild.ResourceId
                    INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                    INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                    LEFT OUTER JOIN (
                                    SELECT *
                                    FROM   syResources
                                    WHERE  ResourceTypeID = 1
                                    ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                    WHERE  RChild.ResourceTypeID IN ( 2, 3, 8 )
                           AND (
                               RChild.ChildTypeId IS NULL
                               OR RChild.ChildTypeId = 3
                               )
                           AND ( NNParent.ParentId IN (
                                                      SELECT HierarchyId
                                                      FROM   syNavigationNodes
                                                      WHERE  ResourceId = 394
                                                             AND ParentId IN (
                                                                             SELECT HierarchyId
                                                                             FROM   syNavigationNodes
                                                                             WHERE  ResourceId = 191
                                                                             )
                                                      )
                               )
                    UNION
                    SELECT DISTINCT RChild.ResourceID AS ChildResourceId
                          ,RChild.Resource AS ChildResource
                          ,RChild.ResourceURL AS ChildResourceURL
                          ,CASE WHEN ( RChild.ResourceID IN ( 737, 738, 739, 740, 741, 742 )) THEN NULL
                                ELSE RChild.ResourceID
                           END AS ParentResourceId
                          ,NULL AS ParentResource
                          ,NULL AS MODULE
                          ,RChild.ResourceTypeID
                    FROM   syResources RChild
                    WHERE  RChild.ResourceID IN ( 737, 738, 739, 740, 741, 742 )
                    ) t1
             UNION
             SELECT 193 AS ModuleResourceId
                   ,394 AS TabId
                   ,ResourceID AS ChildResourceId
                   ,Resource AS ChildResource
                   ,(
                    SELECT MAX(AccessLevel)
                    FROM   syRlsResLvls
                    WHERE  RoleId = @RoleId
                           AND ResourceID = syResources.ResourceID
                    ) AS AccessLevel
                   ,ResourceURL AS ChildResourceURL
                   ,NULL AS ParentResourceId
                   ,NULL AS ParentResource
                   ,1 AS GroupSortOrder
                   ,NULL AS FirstSortOrder
                   ,NULL AS DisplaySequence
                   ,ResourceTypeID
             FROM   syResources
             WHERE  ResourceID = 193
             UNION
             SELECT 193 AS ModuleResourceId
                   ,394 AS TabId
                   ,ChildResourceId
                   ,ChildResource
                   ,(
                    SELECT MAX(AccessLevel)
                    FROM   syRlsResLvls
                    WHERE  RoleId = @RoleId
                           AND ResourceID = t1.ChildResourceId
                    ) AS AccessLevel
                   ,ChildResourceURL
                   ,ParentResourceId
                   ,ParentResource
                   ,CASE WHEN (
                              t1.ChildResourceId = 737
                              OR t1.ParentResourceId IN ( 737 )
                              ) THEN 2
                         ELSE CASE WHEN (
                                        t1.ChildResourceId = 739
                                        OR t1.ParentResourceId IN ( 739 )
                                        ) THEN 3
                                   ELSE CASE WHEN (
                                                  t1.ChildResourceId = 738
                                                  OR t1.ParentResourceId IN ( 738 )
                                                  ) THEN 4
                                             ELSE CASE WHEN (
                                                            t1.ChildResourceId = 740
                                                            OR t1.ParentResourceId IN ( 740 )
                                                            ) THEN 5
                                                       ELSE CASE WHEN (
                                                                      t1.ChildResourceId = 741
                                                                      OR t1.ParentResourceId IN ( 741 )
                                                                      ) THEN 6
                                                                 ELSE CASE WHEN (
                                                                                t1.ChildResourceId = 742
                                                                                OR t1.ParentResourceId IN ( 742 )
                                                                                ) THEN 7
                                                                      END
                                                            END
                                                  END
                                        END
                              END
                    END AS GroupSortOrder
                   ,CASE WHEN ChildResourceId = 737 THEN 1
                         ELSE 2
                    END AS FirstSortOrder
                   ,CASE WHEN (
                              ChildResourceId IN ( 737, 738, 740, 741, 742 )
                              OR ParentResourceId IN ( 737, 738, 740, 741, 742 )
                              ) THEN 1
                         ELSE 2
                    END AS DisplaySequence
                   ,ResourceTypeID
             FROM   (
                    SELECT DISTINCT NNChild.ResourceId AS ChildResourceId
                          ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                ELSE RChild.Resource
                           END AS ChildResource
                          ,RChild.ResourceURL AS ChildResourceURL
                          ,CASE WHEN (
                                     NNParent.ResourceId = 193
                                     OR NNChild.ResourceId IN ( 713, 735 )
                                     ) THEN NULL
                                ELSE NNParent.ResourceId
                           END AS ParentResourceId
                          ,RParent.Resource AS ParentResource
                          ,RParentModule.Resource AS MODULE
                          ,RChild.ResourceTypeID
                    FROM   syResources RChild
                    INNER JOIN syNavigationNodes NNChild ON RChild.ResourceID = NNChild.ResourceId
                    INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                    INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                    LEFT OUTER JOIN (
                                    SELECT *
                                    FROM   syResources
                                    WHERE  ResourceTypeID = 1
                                    ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                    WHERE  RChild.ResourceTypeID IN ( 2, 3, 8 )
                           AND (
                               RChild.ChildTypeId IS NULL
                               OR RChild.ChildTypeId = 3
                               )
                           AND ( NNParent.ParentId IN (
                                                      SELECT HierarchyId
                                                      FROM   syNavigationNodes
                                                      WHERE  ResourceId = 394
                                                             AND ParentId IN (
                                                                             SELECT HierarchyId
                                                                             FROM   syNavigationNodes
                                                                             WHERE  ResourceId = 193
                                                                             )
                                                      )
                               )
                    UNION
                    SELECT DISTINCT RChild.ResourceID AS ChildResourceId
                          ,RChild.Resource AS ChildResource
                          ,RChild.ResourceURL AS ChildResourceURL
                          ,CASE WHEN ( RChild.ResourceID IN ( 737, 738, 739, 741, 742 )) THEN NULL
                                ELSE RChild.ResourceID
                           END AS ParentResourceId
                          ,NULL AS ParentResource
                          ,NULL AS MODULE
                          ,RChild.ResourceTypeID
                    FROM   syResources RChild
                    WHERE  RChild.ResourceID IN ( 737, 738, 739, 740, 741, 742 )
                    ) t1
             UNION
             SELECT 193 AS ModuleResourceId
                   ,397 AS TabId
                   ,ResourceID AS ChildResourceId
                   ,Resource AS ChildResource
                   ,(
                    SELECT MAX(AccessLevel)
                    FROM   syRlsResLvls
                    WHERE  RoleId = @RoleId
                           AND ResourceID = syResources.ResourceID
                    ) AS AccessLevel
                   ,ResourceURL AS ChildResourceURL
                   ,NULL AS ParentResourceId
                   ,NULL AS ParentResource
                   ,1 AS GroupSortOrder
                   ,NULL AS FirstSortOrder
                   ,NULL AS DisplaySequence
                   ,ResourceTypeID
             FROM   syResources
             WHERE  ResourceID = 193
             UNION
             SELECT 193 AS ModuleResourceId
                   ,397 AS TabId
                   ,ChildResourceId
                   ,ChildResource
                   ,(
                    SELECT MAX(AccessLevel)
                    FROM   syRlsResLvls
                    WHERE  RoleId = @RoleId
                           AND ResourceID = t1.ChildResourceId
                    ) AS AccessLevel
                   ,ChildResourceURL
                   ,ParentResourceId
                   ,ParentResource
                   ,CASE WHEN (
                              t1.ChildResourceId = 737
                              OR t1.ParentResourceId IN ( 737 )
                              ) THEN 2
                         ELSE CASE WHEN (
                                        t1.ChildResourceId = 739
                                        OR t1.ParentResourceId IN ( 739 )
                                        ) THEN 3
                                   ELSE CASE WHEN (
                                                  t1.ChildResourceId = 738
                                                  OR t1.ParentResourceId IN ( 738 )
                                                  ) THEN 4
                                             ELSE CASE WHEN (
                                                            t1.ChildResourceId = 740
                                                            OR t1.ParentResourceId IN ( 740 )
                                                            ) THEN 5
                                                       ELSE CASE WHEN (
                                                                      t1.ChildResourceId = 741
                                                                      OR t1.ParentResourceId IN ( 741 )
                                                                      ) THEN 6
                                                                 ELSE 6
                                                            END
                                                  END
                                        END
                              END
                    END AS GroupSortOrder
                   ,CASE WHEN ChildResourceId = 737 THEN 1
                         ELSE 2
                    END AS FirstSortOrder
                   ,CASE WHEN (
                              ChildResourceId IN ( 737 )
                              OR ParentResourceId IN ( 737 )
                              ) THEN 1
                         ELSE 2
                    END AS DisplaySequence
                   ,ResourceTypeID
             FROM   (
                    SELECT DISTINCT NNChild.ResourceId AS ChildResourceId
                          ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                ELSE RChild.Resource
                           END AS ChildResource
                          ,RChild.ResourceURL AS ChildResourceURL
                          ,CASE WHEN (
                                     NNParent.ResourceId = 193
                                     OR NNChild.ResourceId IN ( 737 )
                                     ) THEN NULL
                                ELSE NNParent.ResourceId
                           END AS ParentResourceId
                          ,RParent.Resource AS ParentResource
                          ,RParentModule.Resource AS MODULE
                          ,RChild.ResourceTypeID
                    FROM   syResources RChild
                    INNER JOIN syNavigationNodes NNChild ON RChild.ResourceID = NNChild.ResourceId
                    INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                    INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                    LEFT OUTER JOIN (
                                    SELECT *
                                    FROM   syResources
                                    WHERE  ResourceTypeID = 1
                                    ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                    WHERE  RChild.ResourceTypeID IN ( 2, 3, 8 )
                           AND (
                               RChild.ChildTypeId IS NULL
                               OR RChild.ChildTypeId = 3
                               )
                           AND ( NNParent.ParentId IN (
                                                      SELECT HierarchyId
                                                      FROM   syNavigationNodes
                                                      WHERE  ResourceId = 397
                                                             AND ParentId IN (
                                                                             SELECT HierarchyId
                                                                             FROM   syNavigationNodes
                                                                             WHERE  ResourceId = 193
                                                                             )
                                                      )
                               )
                    UNION
                    SELECT DISTINCT RChild.ResourceID AS ChildResourceId
                          ,RChild.Resource AS ChildResource
                          ,RChild.ResourceURL AS ChildResourceURL
                          ,CASE WHEN ( RChild.ResourceID IN ( 737 )) THEN NULL
                                ELSE RChild.ResourceID
                           END AS ParentResourceId
                          ,NULL AS ParentResource
                          ,NULL AS MODULE
                          ,RChild.ResourceTypeID
                    FROM   syResources RChild
                    WHERE  RChild.ResourceID IN ( 737 )
                    ) t1
             UNION
             SELECT 192 AS ModuleResourceId
                   ,396 AS TabId
                   ,ResourceID AS ChildResourceId
                   ,Resource AS ChildResource
                   ,(
                    SELECT MAX(AccessLevel)
                    FROM   syRlsResLvls
                    WHERE  RoleId = @RoleId
                           AND ResourceID = syResources.ResourceID
                    ) AS AccessLevel
                   ,ResourceURL AS ChildResourceURL
                   ,NULL AS ParentResourceId
                   ,NULL AS ParentResource
                   ,1 AS GroupSortOrder
                   ,NULL AS FirstSortOrder
                   ,NULL AS DisplaySequence
                   ,ResourceTypeID
             FROM   syResources
             WHERE  ResourceID = 192
             UNION
             SELECT 192 AS ModuleResourceId
                   ,396 AS TabId
                   ,ChildResourceId
                   ,ChildResource
                   ,(
                    SELECT MAX(AccessLevel)
                    FROM   syRlsResLvls
                    WHERE  RoleId = @RoleId
                           AND ResourceID = t1.ChildResourceId
                    ) AS AccessLevel
                   ,ChildResourceURL
                   ,ParentResourceId
                   ,ParentResource
                   ,CASE WHEN (
                              ChildResourceId IN ( 713 )
                              OR ParentResourceId IN ( 713 )
                              ) THEN 1
                         ELSE 2
                    END AS GroupSortOrder
                   ,CASE WHEN (
                              ChildResourceId IN ( 713 )
                              OR ParentResourceId IN ( 713 )
                              ) THEN 1
                         ELSE 2
                    END AS FirstSortOrder
                   ,CASE WHEN (
                              ChildResourceId IN ( 713 )
                              OR ParentResourceId IN ( 713 )
                              ) THEN 1
                         ELSE 2
                    END AS DisplaySequence
                   ,ResourceTypeID
             FROM   (
                    SELECT DISTINCT NNChild.ResourceId AS ChildResourceId
                          ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                ELSE RChild.Resource
                           END AS ChildResource
                          ,RChild.ResourceURL AS ChildResourceURL
                          ,CASE WHEN (
                                     NNParent.ResourceId = 193
                                     OR NNChild.ResourceId IN ( 713, 735 )
                                     ) THEN NULL
                                ELSE NNParent.ResourceId
                           END AS ParentResourceId
                          ,RParent.Resource AS ParentResource
                          ,RParentModule.Resource AS MODULE
                          ,RChild.ResourceTypeID
                    FROM   syResources RChild
                    INNER JOIN syNavigationNodes NNChild ON RChild.ResourceID = NNChild.ResourceId
                    INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                    INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                    LEFT OUTER JOIN (
                                    SELECT *
                                    FROM   syResources
                                    WHERE  ResourceTypeID = 1
                                    ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                    WHERE  RChild.ResourceTypeID IN ( 2, 3, 8 )
                           AND (
                               RChild.ChildTypeId IS NULL
                               OR RChild.ChildTypeId = 3
                               )
                           AND ( NNParent.ParentId IN (
                                                      SELECT HierarchyId
                                                      FROM   syNavigationNodes
                                                      WHERE  ResourceId = 396
                                                             AND ParentId IN (
                                                                             SELECT HierarchyId
                                                                             FROM   syNavigationNodes
                                                                             WHERE  ResourceId = 192
                                                                             )
                                                      )
                               )
                    UNION
                    SELECT DISTINCT RChild.ResourceID AS ChildResourceId
                          ,RChild.Resource AS ChildResource
                          ,RChild.ResourceURL AS ChildResourceURL
                          ,CASE WHEN ( RChild.ResourceID IN ( 737 )) THEN NULL
                                ELSE RChild.ResourceID
                           END AS ParentResourceId
                          ,NULL AS ParentResource
                          ,NULL AS MODULE
                          ,RChild.ResourceTypeID
                    FROM   syResources RChild
                    WHERE  RChild.ResourceID IN ( 737 )
                    ) t1
             ) t2
    WHERE    t2.ModuleResourceId = @ModuleResourceId
             AND t2.TabId = @TabId
             -- DE7545 Conversion pages not to be shown in Manage Security          
             AND ChildResourceId NOT IN ( 530, 531, 287, 288 )
             AND
        -- Hide resources based on Configuration Settings              
        (
        -- The following expression means if showross... is set to false, hide               
        -- ResourceIds 541,542,532,534,535,538,543,539              
        -- (Not False) OR (Condition)            
        -- US4330 removed Resourceid 541             
        (
        ( NOT LTRIM(RTRIM(@ShowRossOnlyTabs)) = 0 )
        OR ( ChildResourceId NOT IN ( 542, 532, 538, 543 ))
        )
        AND
        -- The following expression means if showross... is set to true, hide               
        -- ResourceIds 142,375,330,476,508,102,107,237              
        -- (Not True) OR (Condition)              
        (
        ( NOT LTRIM(RTRIM(@ShowRossOnlyTabs)) = 1 )
        OR ( ChildResourceId NOT IN ( 142, 375, 330, 476, 508, 102, 107, 237, 230, 286, 200, 217, 290 ))
        )
        AND
        -- US4330 added              
        -- The following expression means if PostServices is set to false or showross is set to false, hide                 
        -- ResourceId 541, 508 if ShowRossOnlyTabs is set to false            
        -- (Not False) OR (Condition)                
        (
        (
        ( NOT LTRIM(RTRIM(@PostServicesByStudent)) = 0 )
        OR ( ChildResourceId NOT IN ( 541 ))
        )
        OR (
           ( NOT LTRIM(RTRIM(@ShowRossOnlyTabs)) = 0 )
           OR ( ChildResourceId NOT IN ( 541 ))
           )
        )
        AND (
            (
            ( NOT LTRIM(RTRIM(@PostServicesByClass)) = 0 )
            OR ( ChildResourceId NOT IN ( 508 ))
            )
            OR (
               ( NOT LTRIM(RTRIM(@ShowRossOnlyTabs)) = 0 )
               OR ( ChildResourceId NOT IN ( 508 ))
               )
            )
        AND
        ---- The following expression means if SchedulingMethod=regulartraditional, hide               
        ---- ResourceIds 91 and 497              
        ---- (Not True) OR (Condition)              
        (
        ( NOT LOWER(LTRIM(RTRIM(@SchedulingMethod))) = 'regulartraditional' )
        OR ( ChildResourceId NOT IN ( 91, 497 ))
        )
        AND
        -- The following expression means if TrackSAPAttendance=byday, hide               
        -- ResourceIds 633              
        -- (Not True) OR (Condition)              
        (
        ( NOT LOWER(LTRIM(RTRIM(@TrackSAPAttendance))) = 'byday' )
        OR ( ChildResourceId NOT IN ( 633, 327 ))
        )
        AND (
            ( NOT LOWER(LTRIM(RTRIM(@TrackSAPAttendance))) = 'byclass' )
            OR ( ChildResourceId NOT IN ( 539 ))
            )
        AND
        ---- The following expression means if @ShowCollegeOfCourtReporting is set to false, hide               
        ---- ResourceIds 614,615              
        ---- (Not False) OR (Condition)              
        (
        ( NOT LOWER(LTRIM(RTRIM(@ShowCollegeOfCourtReporting))) = 'no' )
        OR ( ChildResourceId NOT IN ( 614, 615 ))
        )
        AND
        -- The following expression means if @ShowCollegeOfCourtReporting is set to true, hide               
        -- ResourceIds 497              
        -- (Not True) OR (Condition)              
        (
        ( NOT LOWER(LTRIM(RTRIM(@ShowCollegeOfCourtReporting))) = 'yes' )
        OR ( ChildResourceId NOT IN ( 497 ))
        )
        AND
        -- The following expression means if FAMEESP is set to false, hide               
        -- ResourceIds 517,523, 525              
        -- (Not False) OR (Condition)              
        (
        ( NOT LOWER(LTRIM(RTRIM(@FameESP))) = 'no' )
        OR ( ChildResourceId NOT IN ( 517, 523, 525 ))
        )
        AND
        ---- The following expression means if EDExpress is set to false, hide               
        ---- ResourceIds 603,604,606,619              
        ---- (Not False) OR (Condition)              
        (
        ( NOT LOWER(LTRIM(RTRIM(@EdExpress))) = 'no' )
        OR ( ChildResourceId NOT IN ( 603, 604, 605, 606, 619 ))
        )
        AND
        ---- The following expression means if @GradeBookWeightingLevel is set to courselevel, hide               
        ---- ResourceIds 107,96,222              
        ---- (Not False) OR (Condition)              
        (
        ( NOT LOWER(LTRIM(RTRIM(@GradeBookWeightingLevel))) = 'courselevel' )
        OR ( ChildResourceId NOT IN ( 107, 96, 222 ))
        )
        AND (
            ( NOT LOWER(LTRIM(RTRIM(@GradeBookWeightingLevel))) = 'instructorlevel' )
            OR ( ChildResourceId NOT IN ( 476 ))
            )
        AND (
            ( NOT LOWER(LTRIM(RTRIM(@ShowExternshipTabs))) = 'no' )
            --OR ( ChildResourceId NOT IN ( 543, 538 ) )            
            OR ( ChildResourceId NOT IN ( 543 ))
            )
        )
    ORDER BY GroupSortOrder
            ,ParentResourceId
            ,ChildResource;

-- =========================================================================================================      
-- END  --  USP_ManageSecurity_Tabs_Permissions      
-- ========================================================================================================= 
GO
