SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_GetGBWResultsForInstructorLevel]
    (
     @stuEnrollId UNIQUEIDENTIFIER
    ,@clsSectionId UNIQUEIDENTIFIER
 

    )
AS
    SET NOCOUNT ON;
    SELECT  A.StuEnrollId
           ,A.ClsSectionId
           ,SUM(A.Score * B.Weight * .01) AS FinalScore
           ,SUM(B.Weight) AS TotalWeight
    FROM    arGrdBkResults A
           ,arGrdBkWgtDetails B
    WHERE   B.InstrGrdBkWgtDetailId = A.InstrGrdBkWgtDetailId
            AND StuEnrollId = @stuEnrollId
            AND ClsSectionId = @clsSectionId
    GROUP BY A.StuEnrollId
           ,A.ClsSectionId;



GO
