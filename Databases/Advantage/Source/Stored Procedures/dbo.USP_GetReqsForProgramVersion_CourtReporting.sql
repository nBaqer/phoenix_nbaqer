SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_GetReqsForProgramVersion_CourtReporting]
    (
     @CampusId UNIQUEIDENTIFIER
    ,@PrgVerId UNIQUEIDENTIFIER
    )
AS
    IF @PrgVerId = (
                     SELECT CAST(CAST(0 AS BINARY) AS UNIQUEIDENTIFIER)
                   )
        BEGIN
            SELECT  *
            FROM    arReqs
            WHERE   arReqs.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
                    AND arReqs.ReqTypeId = 1
                    AND arReqs.CampGrpId IN ( SELECT    CampGrpId
                                              FROM      syCmpGrpCmps
                                              WHERE     CampusId = @CampusId )
                    AND arReqs.ReqId NOT IN ( SELECT DISTINCT
                                                        ReqId
                                              FROM      arBridge_GradeComponentTypes_Courses ); 
        END;
    ELSE
        BEGIN
            SELECT  arReqs.*
            FROM    arReqs
                   ,arProgVerDef
            WHERE   arReqs.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
                    AND arReqs.ReqTypeId = 1
                    AND arReqs.ReqId = arProgVerDef.ReqId
                    AND arProgVerDef.PrgVerId = @PrgVerId
                    AND arReqs.CampGrpId IN ( SELECT    CampGrpId
                                              FROM      syCmpGrpCmps
                                              WHERE     CampusId = @CampusId )
                    AND arReqs.ReqId NOT IN ( SELECT DISTINCT
                                                        ReqId
                                              FROM      arBridge_GradeComponentTypes_Courses )
            UNION
            SELECT  T5.*
            FROM    arReqs t1
                   ,arProgVerDef t2
                   ,arReqGrpDef t4
                   ,arReqs T5
            WHERE   t1.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
                    AND T5.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
                    AND t1.ReqId = t4.GrpID
                    AND t1.ReqTypeId = 2
                    AND t1.ReqId = t2.ReqId
                    AND PrgVerId = @PrgVerId
                    AND t1.CampGrpId IN ( SELECT    CampGrpId
                                          FROM      syCmpGrpCmps
                                          WHERE     CampusId = @CampusId )
                    AND t4.ReqId = T5.ReqId
                    AND t1.ReqId NOT IN ( SELECT DISTINCT
                                                    ReqId
                                          FROM      arBridge_GradeComponentTypes_Courses ); 
        END;



GO
