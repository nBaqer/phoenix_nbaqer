SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[USP_FASAPChkResults_getListByCampusAndProgVer]
    (
        @PrgVerId VARCHAR(MAX)
       ,@StatusCodeId VARCHAR(MAX) = NULL
       ,@StartDate AS DATETIME = NULL
       ,@CampusId VARCHAR(MAX)
    )
AS
    BEGIN
        SELECT   DISTINCT s.StudentId
                ,fasap.Period
                ,fasap.ModDate
                ,fasap.GPA
                ,fasap.Comments
                ,CASE fasap.IsMakingSAP
                      WHEN 1 THEN 'Yes'
                      ELSE 'No'
                 END AS MakingSAP
                ,s.FirstName
                ,s.LastName
                ,pv.PrgVerId
                ,pv.PrgVerDescrip
                ,e.CampusId
                ,(
                 SELECT CampDescrip
                 FROM   syCampuses C
                 WHERE  C.CampusId = e.CampusId
                 ) AS CampDescrip
                ,pv.CampGrpId
                ,(
                 SELECT CG.CampGrpDescrip
                 FROM   dbo.syCampGrps CG
                 WHERE  CG.CampGrpId = pv.CampGrpId
                 ) AS CampusGroupDescrip
                ,(
                 SELECT tivs.Code
                 FROM   dbo.syTitleIVSapStatus tivs
                 WHERE  fasap.TitleIVStatusId = tivs.Id
                 ) AS TitleIVStatus
                ,'yes' AS SuppressDate
                ,CONVERT(VARCHAR(10), fasap.CheckPointDate, 101) AS CheckPointDate
                ,CONVERT(VARCHAR(50), CAST(ROUND(TrigValue / 100.0, 2) AS DECIMAL(16, 2))) + ' ' + TrigUnitTypDescrip + ' after the ' + TrigOffTypDescrip
                 + ISNULL(CONVERT(VARCHAR(50), TrigOffsetSeq), '') AS TriggerDescription
        FROM     dbo.arFASAPChkResults fasap
        JOIN     dbo.arStuEnrollments e ON e.StuEnrollId = fasap.StuEnrollId
        JOIN     dbo.arStudent s ON e.StudentId = s.StudentId
        JOIN     dbo.arSAPDetails d ON d.SAPDetailId = fasap.SAPDetailId
        JOIN     dbo.arPrgVersions pv ON pv.PrgVerId = e.PrgVerId
        JOIN     dbo.arTrigUnitTyps ON arTrigUnitTyps.TrigUnitTypId = d.TrigUnitTypId
        JOIN     dbo.arTrigOffsetTyps ON arTrigOffsetTyps.TrigOffsetTypId = d.TrigOffsetTypId
        --JOIN dbo.syCmpGrpCmps cgc ON cgc.CampusId = e.CampusId
        --JOIN dbo.syCampGrps cg ON cg.CampGrpId = cgc.CampGrpId
        WHERE    fasap.PreviewSapCheck = 0
                 AND (
                     @CampusId IS NULL
                     OR ( e.CampusId IN (
                                        SELECT Val
                                        FROM   MultipleValuesForReportParameters(@CampusId, ',', 1)
                                        )
                        )
                     )
                 AND (
                     @PrgVerId IS NULL
                     OR ( e.PrgVerId IN (
                                        SELECT Val
                                        FROM   MultipleValuesForReportParameters(@PrgVerId, ',', 1)
                                        )
                        )
                     )
                 AND (
                     @StatusCodeId IS NULL
                     OR ( e.StatusCodeId IN (
                                            SELECT Val
                                            FROM   MultipleValuesForReportParameters(@StatusCodeId, ',', 1)
                                            )
                        )
                     )
                 AND (
                     @StartDate IS NULL
                     OR e.StartDate <= @StartDate
                     )
        ORDER BY fasap.ModDate DESC;

    END;


GO
