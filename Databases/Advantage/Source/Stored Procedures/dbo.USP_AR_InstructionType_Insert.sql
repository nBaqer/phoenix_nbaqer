SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[USP_AR_InstructionType_Insert]
    (
     @InstructionTypeId UNIQUEIDENTIFIER
    ,@InstructionTypeCode VARCHAR(10)
    ,@InstructionTypeDescrip VARCHAR(50)
    ,@IsDefault BIT
    ,@UserName VARCHAR(50)
    )
AS /*----------------------------------------------------------------------------------------------------
	Author          :	Janet Robinson
    
    Create date		:	08/25/2011
    
	Procedure Name	:	[USP_AR_InstructionType_Insert]

	Objective		:	Inserts into table arInstructionType
	
	Parameters		:	Name						Type	Data Type				Required? 	
						=====						====	=========				=========
						@InstructionTypeId			In		UniqueIDENTIFIER		Required	
						@InstructionTypeCode		In		varchar					Required						
						@InstructionTypeDescrip		In		varchar					Required						
						@IsDefault					In		bit						Required
						@UserName					In		varchar					Required
	
	Output			:		CampGrpId defaults to 'All'	
						
*/-----------------------------------------------------------------------------------------------------

    BEGIN

--For Sql 2005
        DECLARE @StatusID AS UNIQUEIDENTIFIER;
        SET @StatusID = (
                          SELECT    StatusId
                          FROM      syStatuses
                          WHERE     Status = 'Active'
                        );

        DECLARE @CampGrpId AS UNIQUEIDENTIFIER;
        SET @CampGrpId = (
                           SELECT   CampGrpId
                           FROM     syCampGrps
                           WHERE    CampGrpDescrip = 'All'
                         );
-----------------------------------------

        INSERT  INTO arInstructionType
                (
                 InstructionTypeId
                ,InstructionTypeCode
                ,InstructionTypeDescrip
                ,StatusId
                ,CampGrpId
                ,IsDefault
                ,ModUser
                ,ModDate
                )
        VALUES  (
                 @InstructionTypeId
                ,@InstructionTypeCode
                ,@InstructionTypeDescrip
                ,@StatusID
                ,@CampGrpId
                ,@IsDefault
                ,@UserName
                ,GETDATE()
                );

    END;



GO
