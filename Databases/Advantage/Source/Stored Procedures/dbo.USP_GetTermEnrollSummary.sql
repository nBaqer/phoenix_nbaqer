SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jose Torres
-- Create date: 12/31/2014
-- Description: Get TermEnrollSummary for the given TermId and StuEnrollId
--              if the record do not exist it create the record and return empty string.
--
-- EXECUTE USP_GetTermEnrollSummary    @TermId = '', @StuEnrollId = '', @User = ''
--           
-- =============================================

CREATE PROCEDURE [dbo].[USP_GetTermEnrollSummary]
    @TermId UNIQUEIDENTIFIER
   ,@StuEnrollId UNIQUEIDENTIFIER
   ,@User NVARCHAR(50)
AS
    BEGIN
        IF NOT EXISTS ( SELECT  1
                        FROM    arTermEnrollSummary AS ATES
                        WHERE   ATES.TermId = @TermId
                                AND ATES.StuEnrollId = @StuEnrollId )
            BEGIN 
			-- Create new record
                INSERT  INTO dbo.arTermEnrollSummary
                        (
                         TESummId
                        ,TermId
                        ,StuEnrollId
                        ,DescripXTranscript
                        ,ModUser
                        ,ModDate
                        )
                VALUES  (
                         NEWID()      -- TESummId           - UNIQUEIDENTIFIER
                        ,@TermId      -- TermId             - UNIQUEIDENTIFIER
                        ,@StuEnrollId -- StuEnrollId        - UNIQUEIDENTIFIER
                        ,N''          -- DescripXTranscript - NVARCHAR(250) 
                        ,@User        -- ModUser            - NVARCHAR(250) 
                        ,GETDATE()    -- ModDate            - DATETIME
					    );
            END;
        SELECT  ATES.TESummId
               ,ATES.TermId
               ,ATES.StuEnrollId
               ,ATES.DescripXTranscript
               ,ATES.ModUser
               ,ATES.ModDate
        FROM    dbo.arTermEnrollSummary AS ATES
        WHERE   ATES.TermId = @TermId
                AND ATES.StuEnrollId = @StuEnrollId;
    END;




GO
