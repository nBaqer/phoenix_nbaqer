SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

--=================================================================================================
-- USP_LOA_Suspension_Daily_Update
--=================================================================================================
-- =============================================
-- Author:		Jose Torres
-- Create date: 04/07/2015
-- Description: Change of Status for enrollment that have planning go to LOA Or Suspensions for today or older
--              and return bacK to previous status LOA and suspensions that endDate was yesterday or olders
--              Update stuEnrollments, syStudentStatusChange, arStudentLOAs, and arStdSuspensions 
--
-- EXECUTE usp_LOA_Suspension_Daily_Update
--                  
--                  


CREATE PROCEDURE [dbo].[usp_LOA_Suspension_Daily_Update]
AS
    BEGIN
        DECLARE @Test AS INTEGER;
        DECLARE @UserName AS NVARCHAR(50);
        DECLARE @TransDate AS DATETIME;
        DECLARE @Error AS INTEGER;
        DECLARE @CurStudentLOAId AS UNIQUEIDENTIFIER;
        DECLARE @CurStuEnrollId AS UNIQUEIDENTIFIER;
        DECLARE @CurStartDate AS DATETIME;
        DECLARE @CurEndDate AS DATETIME;
        DECLARE @CurStatusCodeId AS UNIQUEIDENTIFIER;
        DECLARE @CurOrigStatusId AS UNIQUEIDENTIFIER;
        DECLARE @CurPrevStatusId AS UNIQUEIDENTIFIER;
        DECLARE @CurCampusId AS UNIQUEIDENTIFIER;
        DECLARE @CurStuSuspensionId AS UNIQUEIDENTIFIER;
		DECLARE @FirstDate AS DATETIME;
		DECLARE @LOAReturnDate AS DATETIME;

        DECLARE @InsertedTable AS TABLE
            (
                NewStudentStatusChangeId UNIQUEIDENTIFIER
            );

        DECLARE @LOAStatusCodeId AS UNIQUEIDENTIFIER;

        SET @UserName = 'SA';
        SET @TransDate = GETDATE();
        SET @Test = 0;
        BEGIN
            ------------------------------------------------------------------------------------------------------------
            -- Change Status for those enrollments which have an LOA scheduled to <= Current Date
            ------------------------------------------------------------------------------------------------------------


            DECLARE LOACursor CURSOR FOR
                SELECT ASLA.StudentLOAId
                      ,ASLA.StuEnrollId
                      ,ASLA.StartDate
                      ,ASLA.StatusCodeId
                      ,ASE.StatusCodeId
                      ,ASE.CampusId
                FROM   dbo.arStudentLOAs AS ASLA
                INNER JOIN dbo.arStuEnrollments AS ASE ON ASE.StuEnrollId = ASLA.StuEnrollId
                INNER JOIN dbo.syStatusCodes AS SSC ON SSC.StatusCodeId = ASE.StatusCodeId
                WHERE  ASLA.StartDate <= @TransDate
                       AND ASLA.EndDate > @TransDate
                       AND ASLA.StudentStatusChangeId IS NULL
                       AND ASLA.StatusCodeId IS NOT NULL
                       AND ASLA.LOAReturnDate IS NULL
                       AND SSC.SysStatusId <> 10;

            OPEN LOACursor;
            FETCH NEXT FROM LOACursor
            INTO @CurStudentLOAId
                ,@CurStuEnrollId
                ,@CurStartDate
                ,@CurStatusCodeId
                ,@CurOrigStatusId
                ,@CurCampusId;

            WHILE @@FETCH_STATUS = 0
                  AND @Test = 0
                BEGIN
                    SET @Error = 0;
                    DELETE FROM @InsertedTable;
                    -- INSERT NEW
                    BEGIN TRANSACTION ChangeStatusToLOA;
                    BEGIN TRY
                        IF @Error = 0
                            BEGIN
                                -- Update arStuEnrollmnets with  StatusCodeID from LOA
                                UPDATE ASE
                                SET    ASE.StatusCodeId = @CurStatusCodeId
                                      ,ASE.ModDate = @TransDate
                                      ,ASE.ModUser = @UserName
                                FROM   dbo.arStuEnrollments AS ASE
                                WHERE  ASE.StuEnrollId = @CurStuEnrollId;
                                SET @Error = @@ERROR;
                            END;
                        IF @Error = 0
                            BEGIN
                                -- insert syStudentStatusChange record
                                INSERT INTO dbo.syStudentStatusChanges (
                                                                       StudentStatusChangeId
                                                                      ,StuEnrollId
                                                                      ,OrigStatusId
                                                                      ,NewStatusId
                                                                      ,CampusId
                                                                      ,ModDate
                                                                      ,ModUser
                                                                      ,IsReversal
                                                                      ,DropReasonId
                                                                      ,DateOfChange
                                                                       )
                                OUTPUT Inserted.StudentStatusChangeId
                                INTO @InsertedTable
                                VALUES (NEWID()          -- StudentStatusChangeId - uniqueidentifier
                                       ,@CurStuEnrollId  -- StuEnrollId           - uniqueidentifier
                                       ,@CurOrigStatusId -- OrigStatusId          - uniqueidentifier
                                       ,@CurStatusCodeId -- NewStatusId           - uniqueidentifier
                                       ,@CurCampusId     -- CampusId              - uniqueidentifier
                                       ,@TransDate       -- ModDate               - datetime
                                       ,@UserName        -- ModUser               - varchar(50)
                                       ,0                -- IsReversal            - bit
                                       ,NULL             -- DropReasonId          - uniqueidentifier
                                       ,@CurStartDate    -- DateOfChange          - datetime
                                       );
                                SET @Error = @@ERROR;
                            END;
                        IF @Error = 0
                            BEGIN
                                -- Update LOA Table with  StudentStatusChangeId
                                UPDATE ASLA
                                SET    ASLA.StudentStatusChangeId = (
                                                                    SELECT TOP 1 NewStudentStatusChangeId
                                                                    FROM   @InsertedTable
                                                                    )
                                      ,ASLA.ModDate = @TransDate
                                      ,ASLA.ModUser = @UserName
                                FROM   dbo.arStudentLOAs AS ASLA
                                WHERE  ASLA.StudentLOAId = @CurStudentLOAId;
                                SET @Error = @@ERROR;
                            END;
                        IF @Error = 0
                            BEGIN
                                COMMIT TRANSACTION ChangeStatusToLOA;
                            END;
                        ELSE
                            BEGIN
                                ROLLBACK TRANSACTION ChangeStatusToLOA;
                            END;
                    END TRY
                    BEGIN CATCH
                        ROLLBACK TRANSACTION ChangeStatusToLOA;
                        SELECT ERROR_NUMBER() AS ErrorNumber
                              ,ERROR_SEVERITY() AS ErrorSeverity
                              ,ERROR_STATE() AS ErrorState
                              ,ERROR_PROCEDURE() AS ErrorProcedure
                              ,ERROR_LINE() AS ErrorLine
                              ,ERROR_MESSAGE() AS ErrorMessage;
                        SELECT ASLA.StudentLOAId
                              ,ASLA.StuEnrollId
                              ,ASLA.StartDate
                              ,ASLA.StatusCodeId
                              ,ASE.StatusCodeId
                              ,ASE.CampusId
                        FROM   dbo.arStudentLOAs AS ASLA
                        INNER JOIN dbo.arStuEnrollments AS ASE ON ASE.StuEnrollId = ASLA.StuEnrollId
                        WHERE  ASLA.StudentLOAId = @CurStudentLOAId;
                    END CATCH;

                    FETCH NEXT FROM LOACursor
                    INTO @CurStudentLOAId
                        ,@CurStuEnrollId
                        ,@CurStartDate
                        ,@CurStatusCodeId
                        ,@CurOrigStatusId
                        ,@CurCampusId;
                END;

            CLOSE LOACursor;
            DEALLOCATE LOACursor;

            ------------------------------------------------------------------------------------------------------------
            -- Back to Status for those enrollments in LOA Status which EndDate is < Current Date
            ------------------------------------------------------------------------------------------------------------

            DECLARE BackFromLOACursor CURSOR FOR
                SELECT ASLA.StudentLOAId
                      ,ASLA.StuEnrollId
                      ,ASLA.EndDate
                      ,ASE.StatusCodeId
                      ,ASE.CampusId
                      ,SSSC.OrigStatusId
                FROM   dbo.arStudentLOAs AS ASLA
                INNER JOIN dbo.arStuEnrollments AS ASE ON ASE.StuEnrollId = ASLA.StuEnrollId
                INNER JOIN dbo.syStudentStatusChanges AS SSSC ON SSSC.StudentStatusChangeId = ASLA.StudentStatusChangeId
                INNER JOIN dbo.syStatusCodes AS SSC ON SSC.StatusCodeId = ASE.StatusCodeId
                WHERE  CAST(ASLA.EndDate AS DATE) < CAST(@TransDate AS DATE)
                       AND SSC.SysStatusId = 10
                       AND ASLA.EndDate = (
                                          SELECT MAX(EndDate)
                                          FROM   dbo.arStudentLOAs SLOAS
                                          WHERE  SLOAS.StuEnrollId = ASE.StuEnrollId
                                          );

            OPEN BackFromLOACursor;
            FETCH NEXT FROM BackFromLOACursor
            INTO @CurStudentLOAId
                ,@CurStuEnrollId
                ,@CurEndDate
                ,@CurOrigStatusId
                ,@CurCampusId
                ,@CurPrevStatusId;

            WHILE @@FETCH_STATUS = 0
                  AND @Test = 0
                BEGIN
                    SET @Error = 0;
					SET @FirstDate = dbo.GetFirstAttendanceFromDate(@CurStuEnrollId, @CurEndDate)
					SET @LOAReturnDate = (SELECT CASE WHEN @FirstDate IS NOT NULL AND @FirstDate <= @TransDate THEN @FirstDate ELSE @TransDate END)
                    DELETE FROM @InsertedTable;
                    -- INSERT NEW
                    BEGIN TRANSACTION BackFromLOA;
                    BEGIN TRY
                        IF @Error = 0
                            BEGIN
                                -- Update arStuEnrollmnets with  StatusCodeID from LOA
                                UPDATE ASE
                                SET    ASE.StatusCodeId = @CurPrevStatusId
                                      ,ASE.ModDate = @TransDate
                                      ,ASE.ModUser = @UserName
                                FROM   dbo.arStuEnrollments AS ASE
                                WHERE  ASE.StuEnrollId = @CurStuEnrollId;
                                SET @Error = @@ERROR;
                            END;
                        IF @Error = 0
                            BEGIN
                                -- insert syStudentStatusChange record
                                INSERT INTO dbo.syStudentStatusChanges (
                                                                       StudentStatusChangeId
                                                                      ,StuEnrollId
                                                                      ,OrigStatusId
                                                                      ,NewStatusId
                                                                      ,CampusId
                                                                      ,ModDate
                                                                      ,ModUser
                                                                      ,IsReversal
                                                                      ,DropReasonId
                                                                      ,DateOfChange
                                                                       )
                                OUTPUT Inserted.StudentStatusChangeId
                                INTO @InsertedTable
                                VALUES (NEWID()          -- StudentStatusChangeId - uniqueidentifier
                                       ,@CurStuEnrollId  -- StuEnrollId           - uniqueidentifier
                                       ,@CurOrigStatusId -- OrigStatusId          - uniqueidentifier
                                       ,@CurPrevStatusId -- NewStatusId           - uniqueidentifier
                                       ,@CurCampusId     -- CampusId              - uniqueidentifier
                                       ,@TransDate       -- ModDate               - datetime
                                       ,@UserName        -- ModUser               - varchar(50)
                                       ,0                -- IsReversal            - bit
                                       ,NULL             -- DropReasonId          - uniqueidentifier
                                       ,@CurEndDate      -- DateOfChange          - datetime
                                       );
                                SET @Error = @@ERROR;
                            END;
                        IF @Error = 0
                            BEGIN
                                -- Update LOA Table with LOAReturnDate
                                UPDATE ASLA
                                SET    ASLA.LOAReturnDate = @LOAReturnDate
                                      ,ASLA.ModDate = @TransDate
                                      ,ASLA.ModUser = @UserName
                                FROM   dbo.arStudentLOAs AS ASLA
                                WHERE  ASLA.StudentLOAId = @CurStudentLOAId;
                                SET @Error = @@ERROR;
                            END;
                        IF @Error = 0
                            BEGIN
                                COMMIT TRANSACTION BackFromLOA;
                            END;
                        ELSE
                            BEGIN
                                ROLLBACK TRANSACTION BackFromLOA;
                            END;
                    END TRY
                    BEGIN CATCH
                        ROLLBACK TRANSACTION BackFromLOA;
                        SELECT ERROR_NUMBER() AS ErrorNumber
                              ,ERROR_SEVERITY() AS ErrorSeverity
                              ,ERROR_STATE() AS ErrorState
                              ,ERROR_PROCEDURE() AS ErrorProcedure
                              ,ERROR_LINE() AS ErrorLine
                              ,ERROR_MESSAGE() AS ErrorMessage;
                        SELECT ASLA.StudentLOAId
                              ,ASLA.StuEnrollId
                              ,ASLA.StartDate
                              ,ASLA.StatusCodeId
                              ,ASE.StatusCodeId
                              ,ASE.CampusId
                        FROM   dbo.arStudentLOAs AS ASLA
                        INNER JOIN dbo.arStuEnrollments AS ASE ON ASE.StuEnrollId = ASLA.StuEnrollId
                        WHERE  ASLA.StudentLOAId = @CurStudentLOAId;
                    END CATCH;

                    FETCH NEXT FROM BackFromLOACursor
                    INTO @CurStudentLOAId
                        ,@CurStuEnrollId
                        ,@CurEndDate
                        ,@CurOrigStatusId
                        ,@CurCampusId
                        ,@CurPrevStatusId;
                END;

            CLOSE BackFromLOACursor;
            DEALLOCATE BackFromLOACursor;



            ------------------------------------------------------------------------------------------------------------
            -- Change Status for those enrollments which have an Suspension scheduled to <= Current Date
            ------------------------------------------------------------------------------------------------------------
            SELECT @CurStatusCodeId = (
                                      SELECT TOP 1 SSC.StatusCodeId
                                      FROM   dbo.syStatusCodes AS SSC
                                      INNER JOIN dbo.sySysStatus AS SSS ON SSS.SysStatusId = SSC.SysStatusId
                                      WHERE  SSS.SysStatusId = 11
                                      );

            DECLARE SusCursor CURSOR FOR
                SELECT ASS.StuSuspensionId
                      ,ASS.StuEnrollId
                      ,ASS.StartDate
                      ,ASE.StatusCodeId
                      ,ASE.CampusId
                FROM   dbo.arStdSuspensions AS ASS
                INNER JOIN dbo.arStuEnrollments AS ASE ON ASE.StuEnrollId = ASS.StuEnrollId
                INNER JOIN dbo.syStatusCodes AS SSC ON SSC.StatusCodeId = ASE.StatusCodeId
                WHERE  ASS.StartDate <= @TransDate
                       AND ASS.EndDate > @TransDate
                       AND ASS.StudentStatusChangeId IS NULL
                       AND SSC.SysStatusId <> 11;

            OPEN SusCursor;
            FETCH NEXT FROM SusCursor
            INTO @CurStuSuspensionId
                ,@CurStuEnrollId
                ,@CurStartDate
                ,@CurOrigStatusId
                ,@CurCampusId;

            WHILE @@FETCH_STATUS = 0
                  AND @Test = 0
                BEGIN
                    SET @Error = 0;
                    DELETE FROM @InsertedTable;
                    -- INSERT NEW
                    BEGIN TRANSACTION ChangeStatusToSuspension;
                    BEGIN TRY
                        IF @Error = 0
                            BEGIN
                                -- Update arStuEnrollmnets with  StatusCodeID from LOA
                                UPDATE ASE
                                SET    ASE.StatusCodeId = @CurStatusCodeId
                                      ,ASE.ModDate = @TransDate
                                      ,ASE.ModUser = @UserName
                                FROM   dbo.arStuEnrollments AS ASE
                                WHERE  ASE.StuEnrollId = @CurStuEnrollId;
                                SET @Error = @@ERROR;
                            END;
                        IF @Error = 0
                            BEGIN
                                -- insert syStudentStatusChange record
                                INSERT INTO dbo.syStudentStatusChanges (
                                                                       StudentStatusChangeId
                                                                      ,StuEnrollId
                                                                      ,OrigStatusId
                                                                      ,NewStatusId
                                                                      ,CampusId
                                                                      ,ModDate
                                                                      ,ModUser
                                                                      ,IsReversal
                                                                      ,DropReasonId
                                                                      ,DateOfChange
                                                                       )
                                OUTPUT Inserted.StudentStatusChangeId
                                INTO @InsertedTable
                                VALUES (NEWID()          -- StudentStatusChangeId - uniqueidentifier
                                       ,@CurStuEnrollId  -- StuEnrollId           - uniqueidentifier
                                       ,@CurOrigStatusId -- OrigStatusId          - uniqueidentifier
                                       ,@CurStatusCodeId -- NewStatusId           - uniqueidentifier
                                       ,@CurCampusId     -- CampusId              - uniqueidentifier
                                       ,@TransDate       -- ModDate               - datetime
                                       ,@UserName        -- ModUser               - varchar(50)
                                       ,0                -- IsReversal            - bit
                                       ,NULL             -- DropReasonId          - uniqueidentifier
                                       ,@CurStartDate    -- DateOfChange          - datetime
                                       );
                                SET @Error = @@ERROR;
                            END;
                        IF @Error = 0
                            BEGIN
                                -- Update arStdSuspensions Table with  StudentStatusChangeId
                                UPDATE ASS
                                SET    ASS.StudentStatusChangeId = (
                                                                   SELECT TOP 1 NewStudentStatusChangeId
                                                                   FROM   @InsertedTable
                                                                   )
                                      ,ASS.ModDate = @TransDate
                                      ,ASS.ModUser = @UserName
                                FROM   dbo.arStdSuspensions AS ASS
                                WHERE  ASS.StuSuspensionId = @CurStuSuspensionId;
                                SET @Error = @@ERROR;
                            END;
                        IF @Error = 0
                            BEGIN
                                COMMIT TRANSACTION ChangeStatusToSuspension;
                            END;
                        ELSE
                            BEGIN
                                ROLLBACK TRANSACTION ChangeStatusToSuspension;
                            END;
                    END TRY
                    BEGIN CATCH
                        ROLLBACK TRANSACTION ChangeStatusToSuspension;
                        SELECT ERROR_NUMBER() AS ErrorNumber
                              ,ERROR_SEVERITY() AS ErrorSeverity
                              ,ERROR_STATE() AS ErrorState
                              ,ERROR_PROCEDURE() AS ErrorProcedure
                              ,ERROR_LINE() AS ErrorLine
                              ,ERROR_MESSAGE() AS ErrorMessage;
                        SELECT ASS.StuSuspensionId
                              ,ASS.StuEnrollId
                              ,ASS.StartDate
                        FROM   dbo.arStdSuspensions AS ASS
                        WHERE  ASS.StuSuspensionId = @CurStuSuspensionId;
                    END CATCH;

                    FETCH NEXT FROM SusCursor
                    INTO @CurStuSuspensionId
                        ,@CurStuEnrollId
                        ,@CurStartDate
                        ,@CurOrigStatusId
                        ,@CurCampusId;
                END;

            CLOSE SusCursor;
            DEALLOCATE SusCursor;

            ------------------------------------------------------------------------------------------------------------
            -- Back to Status for those enrollments in Suspention Status which EndDate is Not Null and < Current Date
            ------------------------------------------------------------------------------------------------------------

            DECLARE BackFromSusCursor CURSOR FOR
                SELECT ASS.StuSuspensionId
                      ,ASS.StuEnrollId
                      ,ASS.EndDate
                      ,ASE.StatusCodeId
                      ,ASE.CampusId
                      ,SSSC.OrigStatusId
                FROM   dbo.arStdSuspensions AS ASS
                INNER JOIN dbo.arStuEnrollments AS ASE ON ASE.StuEnrollId = ASS.StuEnrollId
                INNER JOIN dbo.syStudentStatusChanges AS SSSC ON SSSC.StudentStatusChangeId = ASS.StudentStatusChangeId
                INNER JOIN dbo.syStatusCodes AS SSC ON SSC.StatusCodeId = ASE.StatusCodeId
                WHERE  CAST(ASS.EndDate AS DATE) < CAST(@TransDate AS DATE)
                       AND SSC.SysStatusId = 11
                       AND ASS.EndDate = (
                                         SELECT MAX(EndDate)
                                         FROM   dbo.arStdSuspensions SS
                                         WHERE  SS.StuEnrollId = ASE.StuEnrollId
                                         );

            OPEN BackFromSusCursor;
            FETCH NEXT FROM BackFromSusCursor
            INTO @CurStuSuspensionId
                ,@CurStuEnrollId
                ,@CurEndDate
                ,@CurOrigStatusId
                ,@CurCampusId
                ,@CurPrevStatusId;

            WHILE @@FETCH_STATUS = 0
                BEGIN
                    SET @Error = 0;
                    DELETE FROM @InsertedTable;
                    -- INSERT NEW
                    BEGIN TRANSACTION BackFromSus;
                    BEGIN TRY
                        IF @Error = 0
                            BEGIN
                                -- Update arStuEnrollmnets with  StatusCodeID from arStdSuspensions
                                UPDATE ASE
                                SET    ASE.StatusCodeId = @CurPrevStatusId
                                      ,ASE.ModDate = @TransDate
                                      ,ASE.ModUser = @UserName
                                FROM   dbo.arStuEnrollments AS ASE
                                WHERE  ASE.StuEnrollId = @CurStuEnrollId;
                                SET @Error = @@ERROR;
                            END;
                        IF @Error = 0
                            BEGIN
                                -- insert syStudentStatusChange record
                                INSERT INTO dbo.syStudentStatusChanges (
                                                                       StudentStatusChangeId
                                                                      ,StuEnrollId
                                                                      ,OrigStatusId
                                                                      ,NewStatusId
                                                                      ,CampusId
                                                                      ,ModDate
                                                                      ,ModUser
                                                                      ,IsReversal
                                                                      ,DropReasonId
                                                                      ,DateOfChange
                                                                       )
                                OUTPUT Inserted.StudentStatusChangeId
                                INTO @InsertedTable
                                VALUES (NEWID()          -- StudentStatusChangeId - uniqueidentifier
                                       ,@CurStuEnrollId  -- StuEnrollId           - uniqueidentifier
                                       ,@CurOrigStatusId -- OrigStatusId          - uniqueidentifier
                                       ,@CurPrevStatusId -- NewStatusId           - uniqueidentifier
                                       ,@CurCampusId     -- CampusId              - uniqueidentifier
                                       ,@TransDate       -- ModDate               - datetime
                                       ,@UserName        -- ModUser               - varchar(50)
                                       ,0                -- IsReversal            - bit
                                       ,NULL             -- DropReasonId          - uniqueidentifier
                                       ,@CurEndDate      -- DateOfChange          - datetime
                                       );
                                SET @Error = @@ERROR;
                            END;
                        --IF @Error = 0
                        --    BEGIN
                        --        -- Update arStdSuspensions Table with LOAReturnDate
                        --    END
                        IF @Error = 0
                            BEGIN
                                COMMIT TRANSACTION BackFromSus;
                            END;
                        ELSE
                            BEGIN
                                ROLLBACK TRANSACTION BackFromSus;
                            END;
                    END TRY
                    BEGIN CATCH
                        ROLLBACK TRANSACTION BackFromSus;
                        SELECT ERROR_NUMBER() AS ErrorNumber
                              ,ERROR_SEVERITY() AS ErrorSeverity
                              ,ERROR_STATE() AS ErrorState
                              ,ERROR_PROCEDURE() AS ErrorProcedure
                              ,ERROR_LINE() AS ErrorLine
                              ,ERROR_MESSAGE() AS ErrorMessage;
                        SELECT ASS.StuSuspensionId
                              ,ASS.StuEnrollId
                              ,ASS.StartDate
                              ,ASS.EndDate
                        FROM   dbo.arStdSuspensions AS ASS
                        WHERE  ASS.StuSuspensionId = @CurStuSuspensionId;
                    END CATCH;

                    FETCH NEXT FROM BackFromSusCursor
                    INTO @CurStuSuspensionId
                        ,@CurStuEnrollId
                        ,@CurEndDate
                        ,@CurOrigStatusId
                        ,@CurCampusId
                        ,@CurPrevStatusId;
                END;

            CLOSE BackFromSusCursor;
            DEALLOCATE BackFromSusCursor;


        END;
    END;
--=================================================================================================
-- END  --  USP_LOA_Suspension_Daily_Update
--=================================================================================================
GO
