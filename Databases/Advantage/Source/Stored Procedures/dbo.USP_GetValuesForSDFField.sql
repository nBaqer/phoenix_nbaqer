SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_GetValuesForSDFField]
    (
     @SDFId UNIQUEIDENTIFIER  
    )
AS
    DECLARE @val VARCHAR(8000);  
    SET @val = (
                 SELECT ValList
                 FROM   sySdfValList
                 WHERE  SDFId = @SDFId
               );  
  
    SELECT  s AS ValueId
           ,s AS Descrip
    FROM    dbo.SplitColumnIntoRows(',',@val);   



GO
