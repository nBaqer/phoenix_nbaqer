SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[USP_DoesReqGroupMeetsConditionsforFinAid]
    (
     @StuEnrollID UNIQUEIDENTIFIER
    )
AS /*----------------------------------------------------------------------------------------------------
	Author          :	Saraswathi Lakshmanan
    
    Create date		:	28/09/2010
    
	Procedure Name	:	[USP_DoesReqGroupMeetsConditionsforFinAid]

	Objective		:	Does Req Groups meets condition for FinAid validation
		
	Parameters		:	Name			Type	Data Type			Required? 	
						=====			====	=========			=========	
						@StuEnrollId	IN		Uniqueuidentifier	Yes
					
	
	Output			:	Returns the requested details	
						
*/-----------------------------------------------------------------------------------------------------
    BEGIN
 
 
 --Declare @StuEnrollId as UniqueIdentifier
 --Declare @CampusID as UniqueIdentifier
 
 --Set @StuEnrollId='60DB3379-3DAD-4443-ABC2-F9E211C9AAB8'
 --Set @CampusID='2CD3849A-DECD-48C5-8C9E-3FBE11351437'
        DECLARE @StudentId AS UNIQUEIDENTIFIER;
        DECLARE @PrgVerId AS UNIQUEIDENTIFIER;
        DECLARE @CampusID AS UNIQUEIDENTIFIER;
 
        SET @StudentId = (
                           SELECT   StudentId
                           FROM     arStuEnrollments
                           WHERE    StuEnrollId = @StuEnrollID
                         );
        SET @PrgVerId = (
                          SELECT    PrgVerId
                          FROM      arStuEnrollments
                          WHERE     StuEnrollId = @StuEnrollID
                        );
        SET @CampusID = (
                          SELECT    CampusID
                          FROM      arStuEnrollments
                          WHERE     StuEnrollId = @StuEnrollID
                        );
 
 
        DECLARE @ActiveStatusID AS UNIQUEIDENTIFIER;
        SET @ActiveStatusID = (
                                SELECT  StatusID
                                FROM    syStatuses
                                WHERE   Status = 'Active'
                              );
 
 
 
        SELECT  ReqGrpId
               ,R5.Descrip
               ,R5.NumReqs
               ,R5.TestAttempted + R5.DocsAttempted + R5.TestOverRidden + R5.DocumentOverRidden AS AttemptedReqs
        FROM    (
                  SELECT  
   	DISTINCT                t1.ReqGrpId
                           ,t2.Descrip
                           ,t2.CampGrpId
                           ,t3.NumReqs AS Numreqs
                           ,(
                              SELECT    COUNT(*) AS TestAttempted
                              FROM      (
                                          SELECT DISTINCT
                                                    A2.adReqId
                                                   ,GETDATE() AS CurrentDate
                                                   ,A3.StartDate
                                                   ,A3.EndDate
                                          FROM      adReqGrpDef A1
                                                   ,adReqs A2
                                                   ,adReqsEffectiveDates A3
                                                   ,adReqLeadGroups A4
                                          WHERE     A1.adReqId = A2.adReqId
                                                    AND A2.adReqId = A3.adReqId
                                                    AND A2.ReqforFinancialAid = 1
                                                    AND A2.CampGrpId IN ( SELECT DISTINCT
                                                                                    t2.CampGrpId
                                                                          FROM      syCmpGrpCmps t1
                                                                                   ,syCampGrps t2
                                                                          WHERE     t1.CampGrpId = t2.CampGrpId
                                                                                    AND t1.CampusId = @CampusID
                                                                                    AND t2.StatusId = @ActiveStatusID )
                                                    AND A3.adReqEffectiveDateId = A4.adReqEffectiveDateId
                                                    AND A4.LeadGrpId IN ( SELECT    LeadGrpId
                                                                          FROM      adLeadByLeadGroups
                                                                          WHERE     StudentId = @StudentId
                                                                                    OR StuEnrollId = @StuEnrollID )
                                                    AND ReqGrpId = t1.ReqGrpId
                                                    AND A2.adreqTypeId IN ( 1 )
                                                    AND A2.StatusId = @ActiveStatusID
                                        ) R1
                                       ,adLeadEntranceTest R2
                              WHERE     R1.adReqId = R2.EntrTestId
                                        AND R2.StudentId = @StudentId
                                        AND R1.CurrentDate >= R1.StartDate
                                        AND LEN(R2.TestTaken) >= 4
                                        AND R2.Pass = 1
                                        AND R2.EntrTestId NOT IN ( SELECT DISTINCT
                                                                            EntrTestId
                                                                   FROM     adEntrTestOverRide
                                                                   WHERE    StudentId = @StudentId
                                                                            AND OverRide = 1 )
                                        AND (
                                              R1.CurrentDate <= R1.EndDate
                                              OR R1.EndDate IS NULL
                                            )
                            ) AS TestAttempted
                           ,(
                              SELECT    COUNT(*) AS DocsAttempted
                              FROM      (
                                          SELECT DISTINCT
                                                    A2.adReqId
                                                   ,GETDATE() AS CurrentDate
                                                   ,A3.StartDate
                                                   ,A3.EndDate
                                          FROM      adReqGrpDef A1
                                                   ,adReqs A2
                                                   ,adReqsEffectiveDates A3
                                                   ,adReqLeadGroups A4
                                          WHERE     A1.adReqId = A2.adReqId
                                                    AND A2.adReqId = A3.adReqId
                                                    AND A3.adReqEffectiveDateId = A4.adReqEffectiveDateId
                                                    AND A2.ReqforFinancialAid = 1
                                                    AND A2.CampGrpId IN ( SELECT DISTINCT
                                                                                    t2.CampGrpId
                                                                          FROM      syCmpGrpCmps t1
                                                                                   ,syCampGrps t2
                                                                          WHERE     t1.CampGrpId = t2.CampGrpId
                                                                                    AND t1.CampusId = @CampusID
                                                                                    AND t2.StatusId = @ActiveStatusID )
                                                    AND A4.LeadGrpId IN ( SELECT    LeadGrpId
                                                                          FROM      adLeadByLeadGroups
                                                                          WHERE     StudentId = @StudentId
                                                                                    OR StuEnrollId = @StuEnrollID )
                                                    AND ReqGrpId = t1.ReqGrpId
                                                    AND A2.adreqTypeId IN ( 3 )
                                                    AND A2.StatusId = @ActiveStatusID
                                        ) R1
                                       ,plStudentDocs R2
                                       ,syDocStatuses R3
                              WHERE     R1.adReqId = R2.DocumentId
                                        AND R2.StudentId = @StudentId
                                        AND R2.DocumentId NOT IN ( SELECT DISTINCT
                                                                            EntrTestId
                                                                   FROM     adEntrTestOverRide
                                                                   WHERE    StudentId = @StudentId
                                                                            AND OverRide = 1 )
                                        AND R2.DocStatusId = R3.DocStatusId
                                        AND R3.SysDocStatusId = 1
                                        AND R1.CurrentDate >= R1.StartDate
                                        AND (
                                              R1.CurrentDate <= R1.EndDate
                                              OR R1.EndDate IS NULL
                                            )
                            ) AS DocsAttempted
                           ,(
                              SELECT    COUNT(*) AS OverRideTestAttempted
                              FROM      (
                                          SELECT DISTINCT
                                                    A2.adReqId
                                                   ,GETDATE() AS CurrentDate
                                                   ,A3.StartDate
                                                   ,A3.EndDate
                                          FROM      adReqGrpDef A1
                                                   ,adReqs A2
                                                   ,adReqsEffectiveDates A3
                                                   ,adReqLeadGroups A4
                                          WHERE     A1.adReqId = A2.adReqId
                                                    AND A2.adReqId = A3.adReqId
                                                    AND A2.ReqforFinancialAid = 1
                                                    AND A3.adReqEffectiveDateId = A4.adReqEffectiveDateId
                                                    AND A2.CampGrpId IN ( SELECT DISTINCT
                                                                                    t2.CampGrpId
                                                                          FROM      syCmpGrpCmps t1
                                                                                   ,syCampGrps t2
                                                                          WHERE     t1.CampGrpId = t2.CampGrpId
                                                                                    AND t1.CampusId = @CampusID
                                                                                    AND t2.StatusId = @ActiveStatusID )
                                                    AND A4.LeadGrpId IN ( SELECT    LeadGrpId
                                                                          FROM      adLeadByLeadGroups
                                                                          WHERE     StudentId = @StudentId
                                                                                    OR StuEnrollId = @StuEnrollID )
                                                    AND ReqGrpId = t1.ReqGrpId
                                                    AND A2.adreqTypeId IN ( 1 )
                                                    AND A2.StatusId = @ActiveStatusID
                                        ) R1
                                       ,adEntrTestOverRide R2
                              WHERE     R1.adReqId = R2.EntrTestId
                                        AND R2.OVERRIDE = 1
                                        AND R2.StudentId = @StudentId
                                        AND R1.CurrentDate >= R1.StartDate
                                        AND (
                                              R1.CurrentDate <= R1.EndDate
                                              OR R1.EndDate IS NULL
                                            )
                            ) AS TestOverRidden
                           ,(
                              SELECT    COUNT(*) AS OverRideDocumentAttempted
                              FROM      (
                                          SELECT DISTINCT
                                                    A2.adReqId
                                                   ,GETDATE() AS CurrentDate
                                                   ,A3.StartDate
                                                   ,A3.EndDate
                                          FROM      adReqGrpDef A1
                                                   ,adReqs A2
                                                   ,adReqsEffectiveDates A3
                                                   ,adReqLeadGroups A4
                                          WHERE     A1.adReqId = A2.adReqId
                                                    AND A2.adReqId = A3.adReqId
                                                    AND A2.ReqforFinancialAid = 1
                                                    AND A3.adReqEffectiveDateId = A4.adReqEffectiveDateId
                                                    AND A2.CampGrpId IN ( SELECT DISTINCT
                                                                                    t2.CampGrpId
                                                                          FROM      syCmpGrpCmps t1
                                                                                   ,syCampGrps t2
                                                                          WHERE     t1.CampGrpId = t2.CampGrpId
                                                                                    AND t1.CampusId = @CampusID
                                                                                    AND t2.StatusId = @ActiveStatusID )
                                                    AND A4.LeadGrpId IN ( SELECT    LeadGrpId
                                                                          FROM      adLeadByLeadGroups
                                                                          WHERE     StudentId = @StudentId
                                                                                    OR StuEnrollId = @StuEnrollID )
                                                    AND ReqGrpId = t1.ReqGrpId
                                                    AND A2.adreqTypeId IN ( 3 )
                                                    AND A2.StatusId = @ActiveStatusID
                                        ) R1
                                       ,adEntrTestOverRide R2
                              WHERE     R1.adReqId = R2.EntrTestId
                                        AND R2.OVERRIDE = 1
                                        AND R2.StudentId = @StudentId
                                        AND R1.CurrentDate >= R1.StartDate
                                        AND (
                                              R1.CurrentDate <= R1.EndDate
                                              OR R1.EndDate IS NULL
                                            )
                            ) AS DocumentOverRidden
                  FROM      adPrgVerTestDetails t1
                           ,adReqGroups t2
                           ,adLeadGrpReqGroups t3
                  WHERE     t1.ReqGrpId = t2.ReqGrpId
                            AND t2.StatusId = @ActiveStatusID
                            AND t2.ReqGrpId = t3.ReqGrpId
                            AND t2.ReqGrpId NOT IN ( SELECT ReqGrpId
                                                     FROM   adReqGroups
                                                     WHERE  IsMandatoryReqGrp = 1
                                                            AND StatusId = @ActiveStatusID )
                            AND t1.PrgVerId = @PrgVerId
                            AND t3.LeadGrpId IN ( SELECT DISTINCT
                                                            LeadGrpId
                                                  FROM      adLeadByLeadGroups
                                                  WHERE     StudentId = @StudentId
                                                            OR StuEnrollId = @StuEnrollID )
                            AND (
                                  t1.adreqID IN ( SELECT    adreqId
                                                  FROM      adreqs
                                                  WHERE     ReqforFinancialAid = 1 )
                                  OR t1.adReqId IS NULL
                                )
                            AND t1.ReqGrpId IN ( SELECT ReqGrpId
                                                 FROM   dbo.adReqGrpDef
                                                       ,adreqs
                                                 WHERE  dbo.adReqGrpDef.adReqId = adreqs.adReqId
                                                        AND ReqforFinancialAid = 1 )
                  UNION
                  SELECT    'CFD29DFD-4FFF-400B-B152-4FABB12E6E7B' AS ReqGrpId
                           ,CASE WHEN (
                                        SELECT  COUNT(*)
                                        FROM    adReqGroups
                                        WHERE   IsMandatoryReqGrp = 1
                                                AND StatusId = @ActiveStatusID
                                      ) >= 1 THEN (
                                                    SELECT  Descrip
                                                    FROM    adReqGroups
                                                    WHERE   IsMandatoryReqGrp = 1
                                                            AND StatusId = @ActiveStatusID
                                                  )
                                 ELSE 'School Level Requirements'
                            END AS Descrip
                           ,t1.CampGrpId
                           ,(
                              SELECT    COUNT(*) AS NumReqs
                              FROM      (
                                          SELECT DISTINCT
                                                    A2.adReqId
                                                   ,GETDATE() AS CurrentDate
                                                   ,A3.StartDate
                                                   ,A3.EndDate
                                          FROM      adReqs A2
                                                   ,adReqsEffectiveDates A3
                                          WHERE     A2.adReqId = A3.adReqId
                                                    AND A3.MandatoryRequirement = 1
                                                    AND A2.ReqforFinancialAid = 1
                                                    AND A2.CampGrpId IN ( SELECT DISTINCT
                                                                                    t2.CampGrpId
                                                                          FROM      syCmpGrpCmps t1
                                                                                   ,syCampGrps t2
                                                                          WHERE     t1.CampGrpId = t2.CampGrpId
                                                                                    AND t1.CampusId = @CampusID
                                                                                    AND t2.StatusId = @ActiveStatusID )
                                                    AND A2.adreqTypeId IN ( 1,3 )
                                                    AND A2.StatusId = @ActiveStatusID
                                        ) R1
                              WHERE     R1.CurrentDate >= R1.StartDate
                                        AND (
                                              R1.CurrentDate <= R1.EndDate
                                              OR R1.EndDate IS NULL
                                            )
                            ) AS NumReqs
                           ,(
                              SELECT    COUNT(*) AS TestAttempted
                              FROM      (
                                          SELECT DISTINCT
                                                    A2.adReqId
                                                   ,GETDATE() AS CurrentDate
                                                   ,A3.StartDate
                                                   ,A3.EndDate
                                          FROM      adReqs A2
                                                   ,adReqsEffectiveDates A3
                                          WHERE     A2.adReqId = A3.adReqId
                                                    AND A3.MandatoryRequirement = 1
                                                    AND A2.ReqforFinancialAid = 1
                                                    AND A2.CampGrpId IN ( SELECT DISTINCT
                                                                                    t2.CampGrpId
                                                                          FROM      syCmpGrpCmps t1
                                                                                   ,syCampGrps t2
                                                                          WHERE     t1.CampGrpId = t2.CampGrpId
                                                                                    AND t1.CampusId = @CampusID
                                                                                    AND t2.StatusId = @ActiveStatusID )
                                                    AND A2.adreqTypeId IN ( 1 )
                                                    AND A2.StatusId = @ActiveStatusID
                                        ) R1
                                       ,adLeadEntranceTest R2
                              WHERE     R1.adReqId = R2.EntrTestId
                                        AND R2.StudentId = @StudentId
                                        AND R1.CurrentDate >= R1.StartDate
                                        AND LEN(R2.TestTaken) >= 4
                                        AND R2.Pass = 1
                                        AND R2.EntrTestId NOT IN ( SELECT DISTINCT
                                                                            EntrTestId
                                                                   FROM     adEntrTestOverRide
                                                                   WHERE    StudentId = @StudentId
                                                                            AND OverRide = 1 )
                                        AND (
                                              R1.CurrentDate <= R1.EndDate
                                              OR R1.EndDate IS NULL
                                            )
                            ) AS TestAttempted
                           ,(
                              SELECT    COUNT(*) AS DocsAttempted
                              FROM      (
                                          SELECT DISTINCT
                                                    A2.adReqId
                                                   ,GETDATE() AS CurrentDate
                                                   ,A3.StartDate
                                                   ,A3.EndDate
                                          FROM      adReqs A2
                                                   ,adReqsEffectiveDates A3
                                          WHERE     A2.adReqId = A3.adReqId
                                                    AND A3.MandatoryRequirement = 1
                                                    AND A2.ReqforFinancialAid = 1
                                                    AND A2.CampGrpId IN ( SELECT DISTINCT
                                                                                    t2.CampGrpId
                                                                          FROM      syCmpGrpCmps t1
                                                                                   ,syCampGrps t2
                                                                          WHERE     t1.CampGrpId = t2.CampGrpId
                                                                                    AND t1.CampusId = @CampusID
                                                                                    AND t2.StatusId = @ActiveStatusID )
                                                    AND A2.adreqTypeId IN ( 3 )
                                                    AND A2.StatusId = @ActiveStatusID
                                        ) R1
                                       ,plStudentDocs R2
                                       ,syDocStatuses R3
                              WHERE     R1.adReqId = R2.DocumentId
                                        AND R2.DocStatusId = R3.DocStatusId
                                        AND R3.SysDocStatusId = 1
                                        AND R2.StudentId = @StudentId
                                        AND R2.DocumentId NOT IN ( SELECT DISTINCT
                                                                            EntrTestId
                                                                   FROM     adEntrTestOverRide
                                                                   WHERE    StudentId = @StudentId
                                                                            AND OverRide = 1 )
                                        AND R1.CurrentDate >= R1.StartDate
                                        AND (
                                              R1.CurrentDate <= R1.EndDate
                                              OR R1.EndDate IS NULL
                                            )
                            ) AS DocsAttempted
                           ,(
                              SELECT    COUNT(*) AS OverRideTestAttempted
                              FROM      (
                                          SELECT DISTINCT
                                                    A2.adReqId
                                                   ,GETDATE() AS CurrentDate
                                                   ,A3.StartDate
                                                   ,A3.EndDate
                                          FROM      adReqs A2
                                                   ,adReqsEffectiveDates A3
                                          WHERE     A2.adReqId = A3.adReqId
                                                    AND A3.MandatoryRequirement = 1
                                                    AND A2.ReqforFinancialAid = 1
                                                    AND A2.CampGrpId IN ( SELECT DISTINCT
                                                                                    t2.CampGrpId
                                                                          FROM      syCmpGrpCmps t1
                                                                                   ,syCampGrps t2
                                                                          WHERE     t1.CampGrpId = t2.CampGrpId
                                                                                    AND t1.CampusId = @CampusID
                                                                                    AND t2.StatusId = @ActiveStatusID )
                                                    AND A2.adreqTypeId IN ( 1 )
                                                    AND A2.StatusId = @ActiveStatusID
                                        ) R1
                                       ,adEntrTestOverRide R2
                              WHERE     R1.adReqId = R2.EntrTestId
                                        AND R2.OVERRIDE = 1
                                        AND R2.StudentId = @StudentId
                                        AND R1.CurrentDate >= R1.StartDate
                                        AND (
                                              R1.CurrentDate <= R1.EndDate
                                              OR R1.EndDate IS NULL
                                            )
                            ) AS TestOverRidden
                           ,(
                              SELECT    COUNT(*) AS OverRideDocumentAttempted
                              FROM      (
                                          SELECT DISTINCT
                                                    A2.adReqId
                                                   ,GETDATE() AS CurrentDate
                                                   ,A3.StartDate
                                                   ,A3.EndDate
                                          FROM      adReqs A2
                                                   ,adReqsEffectiveDates A3
                                          WHERE     A2.adReqId = A3.adReqId
                                                    AND A3.MandatoryRequirement = 1
                                                    AND A2.ReqforFinancialAid = 1
                                                    AND A2.CampGrpId IN ( SELECT DISTINCT
                                                                                    t2.CampGrpId
                                                                          FROM      syCmpGrpCmps t1
                                                                                   ,syCampGrps t2
                                                                          WHERE     t1.CampGrpId = t2.CampGrpId
                                                                                    AND t1.CampusId = @CampusID
                                                                                    AND t2.StatusId = @ActiveStatusID )
                                                    AND A2.adreqTypeId IN ( 3 )
                                                    AND A2.StatusId = @ActiveStatusID
                                        ) R1
                                       ,adEntrTestOverRide R2
                              WHERE     R1.adReqId = R2.EntrTestId
                                        AND R2.OVERRIDE = 1
                                        AND R2.StudentId = @StudentId
                                        AND R1.CurrentDate >= R1.StartDate
                                        AND (
                                              R1.CurrentDate <= R1.EndDate
                                              OR R1.EndDate IS NULL
                                            )
                            ) AS DocumentOverRidden
                  FROM      adReqs t1
                           ,adReqsEffectiveDates t2
                  WHERE     t1.adReqId = t2.adReqId
                            AND t2.MandatoryRequirement = 1
                            AND t1.adreqTypeId IN ( 1,3 )
                            AND t1.ReqforFinancialAid = 1
                ) R5
        WHERE   R5.CampGrpId IN ( SELECT DISTINCT
                                            t2.CampGrpId
                                  FROM      syCmpGrpCmps t1
                                           ,syCampGrps t2
                                  WHERE     t1.CampGrpId = t2.CampGrpId
                                            AND t1.CampusId = @CampusID
                                            AND t2.StatusId = @ActiveStatusID );
   
         
    END;










GO
