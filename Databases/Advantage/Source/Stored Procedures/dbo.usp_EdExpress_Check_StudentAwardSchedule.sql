SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[usp_EdExpress_Check_StudentAwardSchedule]
    @StudentAwardId VARCHAR(50)
AS
    BEGIN
        SELECT  SAS.AwardScheduleId
               ,SAS.ExpectedDate
               ,SAS.Amount
               ,SAS.DisbursementNumber
               ,ISNULL(SAS.SequenceNumber,1) AS SequenceNumber
               ,CASE (
                       SELECT   COUNT(*)
                       FROM     saPmtDisbRel PDR
                       WHERE    PDR.AwardScheduleId = SAS.AwardScheduleId
                     )
                  WHEN 0 THEN 'N'
                  ELSE 'Y'
                END AS RelInd
               ,CASE (
                       (SELECT  COUNT(AwardScheduleId)
                        FROM    saRefunds R
                        WHERE   R.AwardScheduleId = SAS.AwardScheduleId)
                     )
                  WHEN 0 THEN 0
                  ELSE 1
                END AS IsRefund
        FROM    faStudentAwardSchedule SAS
        WHERE   SAS.StudentAwardId = @StudentAwardId
        ORDER BY DisbursementNumber;
    END;





GO
