SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
  
CREATE PROCEDURE [dbo].[usp_GetStudentSummaryDetailsPANumeric]
    (
     @stuEnrollId UNIQUEIDENTIFIER
    ,@cutOffDate DATETIME
    )
AS
    SET NOCOUNT ON;

    SELECT  SchedHours
           ,ActualHours
           ,RecordDate
           ,IsTardy AS Tardy
    FROM    arStudentClockAttendance
    WHERE   StuEnrollId = @stuEnrollId
            AND SchedHours IS NOT NULL
            AND SchedHours <> 0
            AND (
                  ActualHours IS NOT NULL
                  AND ActualHours <> 999.00
                  AND ActualHours <> 9999.00
                )
            AND IsTardy = 1
            AND RecordDate <= @cutOffDate
    UNION
    SELECT  Schedule
           ,Actual
           ,MeetDate
           ,Tardy
    FROM    atConversionAttendance
    WHERE   StuEnrollId = @stuEnrollId
            AND Tardy = 1
            AND Schedule IS NOT NULL
            AND Schedule <> 0
            AND (
                  Actual IS NOT NULL
                  AND Actual <> 999.00
                  AND Actual <> 9999.00
                )
            AND meetDate <= @cutOffDate;





GO
