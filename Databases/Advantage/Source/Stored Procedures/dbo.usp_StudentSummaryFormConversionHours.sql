SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_StudentSummaryFormConversionHours]
    (
     @stuEnrollId UNIQUEIDENTIFIER
    ,@cutOffDate DATETIME
    )
AS
    SET NOCOUNT ON;

     --Select Distinct 
     --                  SCA.StuEnrollId, 
     --                  (select Sum(Schedule) from atConversionAttendance 
     --                  where StuEnrollId = SCA.StuEnrollId and  (Actual is not null and Actual <> 999.00 and Actual <> 9999.00)) as SchedHours,
     --                  (select Sum(Actual) from atConversionAttendance 
     --                  where StuEnrollId = SCA.StuEnrollId and ( Actual <> 999.00 and Actual <> 9999.00)) as TotalPresentHours,
     --                  (select Sum(Schedule-Actual) from atConversionAttendance 
     --                  where StuEnrollId = SCA.StuEnrollId and  (Actual <> 999.00 and Actual <> 9999.00)and (ActualHours < SchedHours)) 
     --                  as TotalHoursAbsent,
     --                  (select Sum(Actual-Schedule) from atConversionAttendance 
     --                  where StuEnrollId = SCA.StuEnrollId and 
     --                  ((Actual <> 999.00 and Actual <> 9999.00) and (ActualHours >SchedHours)))
     --                  as TotalMakeUpHours,
     --                  (select Sum(t4.Total) from 
     --                  arStuEnrollments t1,arStudentSchedules t2, arProgSchedules t3, arProgScheduleDetails t4 
     --                  where t1.StuEnrollId = t2.StuEnrollId and t1.PrgVerId=t3.PrgVerId 
     --                  and t2.ScheduleId=t3.ScheduleId and t3.scheduleId=t4.ScheduleId and 
     --                  t2.StuEnrollId=SCA.StuEnrollId  and t4.total is not null) as ScheduleHoursByWeek, 
     --                 (select max(MeetDate) from atConversionAttendance 
     --                  where StuEnrollId = SCA.StuEnrollId and (Actual>=1.00 and Actual <> 999.00 and Actual <> 9999.00)) as LastDateAttended, 
     --                  (select count(*) from  atConversionAttendance where StuEnrollId = SCA.StuEnrollId and Tardy=1) as TardyCount 
     --                  from atConversionAttendance SCA 
     --                  where SCA.StuEnrollId=@stuEnrollId
	 --					and SCA.MeetDate <=@cutOffDate  order by Schedhours desc,TotalPresentHours desc 


GO
