SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_DictationTest_GetList]
AS
    SELECT DISTINCT
            t3.Descrip
           ,NULL AS NumberofTests
           ,NULL AS MinimumScore
           ,ROW_NUMBER() OVER ( ORDER BY t3.Descrip ) AS RowNumber
    FROM    arBridge_GradeComponentTypes_Courses t1
    INNER JOIN arGrdComponentTypes t3 ON t1.GrdComponentTypeid = t3.GrdComponentTypeid
    WHERE   t3.SysComponentTypeId = 612
    ORDER BY t3.Descrip;



GO
