SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_PendingGrads_Report]
    (
     @PrgVerType AS VARCHAR(10)
    ,@Amount AS DECIMAL
    ,@CmpGrpId AS VARCHAR(MAX) = NULL
    ,@PrgVerId AS VARCHAR(MAX) = NULL
    )
AS
    IF ( @PrgVerType = 'Credit' )
        BEGIN
            EXEC dbo.usp_PendingGrads_Credit_Report @Amount,@CmpGrpId,@PrgVerId;  -- varchar(50)
        END;
    ELSE
        BEGIN
            DECLARE @TrackSapAttendance AS VARCHAR(50);
            SET @TrackSapAttendance = (
                                        SELECT  dbo.GetAppSettingValue((
                                                                         SELECT SettingId
                                                                         FROM   dbo.syConfigAppSettings
                                                                         WHERE  KeyName = 'TrackSapAttendance'
                                                                       ),NULL)
                                      );
            --SET @TrackSapAttendance = ( SELECT  [Value]
            --                            FROM    dbo.syConfigAppSetValues
            --                            WHERE   SettingID = ( SELECT
            --                                                  SettingID
            --                                                  FROM
            --                                                  dbo.syConfigAppSettings
            --                                                  WHERE
            --                                                  KeyName = 'TrackSapAttendance'
            --                                                )
            --                          )
        
            IF LOWER(@TrackSapAttendance) = 'byday'
                EXEC dbo.usp_PendingGrads_Clock_Report_ByDay @Amount,@CmpGrpId,@PrgVerId;  -- varchar(50)
            ELSE
                EXEC dbo.usp_PendingGrads_Clock_Report_ByClass @Amount,@CmpGrpId,@PrgVerId;  -- varchar(50)
        
        END;
   
GO
