SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Ginzo, John
-- Create date: 05/09/2014
-- Description:	Check for Duplicate SSN in Lead and Student tables
-- =============================================
CREATE PROCEDURE [dbo].[CheckForDuplicateSSN]
    @SSN VARCHAR(50)
   ,@FirstName VARCHAR(250)
   ,@LastName VARCHAR(250)
AS
    BEGIN

        SET NOCOUNT ON;			

        DECLARE @tb1 TABLE
            (
             SSN VARCHAR(50)
            ,FirstName VARCHAR(250)
            ,LastName VARCHAR(250)
            ,Address1 VARCHAR(250)
            ,Address2 VARCHAR(250)
            ,City VARCHAR(250)
            ,State VARCHAR(250)
            ,ZIP VARCHAR(10)
            ,Status VARCHAR(50)
            );
	
        INSERT  INTO @tb1
                SELECT  st.SSN
                       ,st.FirstName
                       ,st.LastName
                       ,a.Address1
                       ,a.Address2
                       ,a.City
                       ,s.StateDescrip
                       ,a.Zip
                       ,ss.Status
                FROM    arStudent st
                LEFT JOIN arStudAddresses a ON st.StudentId = a.StudentId
                LEFT JOIN syStates s ON a.StateId = s.StatusId
                LEFT JOIN syStatuses ss ON st.StudentStatus = ss.StatusId
                WHERE   st.SSN = @SSN
                        AND dbo.RemoveVowels(@FirstName + @LastName) <> dbo.RemoveVowels(st.FirstName + st.LastName)
                        AND a.default1 = 1;

        INSERT  INTO @tb1
                SELECT  l.SSN
                       ,l.FirstName
                       ,l.LastName
                       ,l.Address1
                       ,l.Address2
                       ,l.City
                       ,s.StateDescrip
                       ,l.Zip
                       ,ss.StatusCode
                FROM    adLeads l
                LEFT JOIN syStates s ON l.StateId = s.StatusId
                LEFT JOIN syStatusCodes ss ON l.LeadStatus = ss.StatusCodeId
                WHERE   l.SSN = @SSN
                        AND dbo.RemoveVowels(@FirstName + @LastName) <> dbo.RemoveVowels(l.FirstName + l.LastName)
                        AND l.SSN NOT IN ( SELECT   SSN
                                           FROM     @tb1 );	


        SELECT  SSN
               ,FirstName
               ,LastName
               ,Address1
               ,Address2
               ,City
               ,State
               ,ZIP
               ,Status
        FROM    @tb1;

    END;



GO
