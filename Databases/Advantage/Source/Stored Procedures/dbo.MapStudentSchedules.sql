SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[MapStudentSchedules]
AS
    DECLARE @StuEnrollId UNIQUEIDENTIFIER
       ,@PrgVerId UNIQUEIDENTIFIER
       ,@Counter INT
       ,@ProgScheduleId UNIQUEIDENTIFIER;
    DECLARE GetDDL_Cursor CURSOR
    FOR
        SELECT DISTINCT
                StuEnrollId
               ,PrgVerId
        FROM    arStuEnrollments
        WHERE   PrgVerId IN ( SELECT DISTINCT
                                        PrgVerId
                              FROM      ( SELECT    PrgVerId
                                                   ,COUNT(*) AS Counter
                                          FROM      dbo.arProgSchedules
                                          GROUP BY  PrgVerId
                                          HAVING    COUNT(*) >= 1
                                        ) TB1 );

    OPEN GetDDL_Cursor;
    FETCH NEXT FROM GetDDL_Cursor INTO @StuEnrollId,@PrgVerId;
    WHILE @@FETCH_STATUS = 0
        BEGIN
            DECLARE @ScheduleId UNIQUEIDENTIFIER;
            SET @ScheduleId = ( SELECT TOP 1
                                        ScheduleId
                                FROM    arProgSchedules
                                WHERE   PrgVerId = @PrgVerId
                                ORDER BY MODDATE DESC
                              );
            IF NOT EXISTS ( SELECT  *
                            FROM    dbo.arStudentSchedules
                            WHERE   StuEnrollId = @StuEnrollId )
                BEGIN
                    INSERT  INTO arStudentSchedules
                            ( StuScheduleId
                            ,StuEnrollId
                            ,ScheduleId
                            ,Active
                            ,ModUser
                            ,ModDate
                            ,Source
                            )
                    VALUES  ( NEWID()
                            ,@StuEnrollId
                            ,@ScheduleId
                            ,1
                            ,'sa'
                            ,GETDATE()
                            ,'attendance'
                            );
                END;

            FETCH NEXT FROM GetDDL_Cursor INTO @StuEnrollId,@PrgVerId;
        END;
    CLOSE GetDDL_Cursor;
    DEALLOCATE GetDDL_Cursor;
GO
