SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ginzo, John
-- Create date: 11/03/2014
-- Description:	Get the available resources 
-- =============================================
CREATE PROCEDURE [dbo].[GetAvailableResourcesByUserAndCampus]
    @UserId UNIQUEIDENTIFIER
   ,@CampusId UNIQUEIDENTIFIER
AS
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;

        SELECT		DISTINCT
                ( RRL.ResourceID )
        FROM    syUsersRolesCampGrps URC
        INNER JOIN syRlsResLvls RRL ON URC.RoleId = RRL.RoleId
        INNER JOIN syCmpGrpCmps CGC ON URC.CampGrpId = CGC.CampGrpId
        WHERE   URC.UserId = @UserId
                AND CGC.CampusId = @CampusId
                AND RRL.AccessLevel > 0
        GROUP BY RRL.ResourceID
               ,CGC.CampusId
        ORDER BY ResourceID;

    END;

-----------------------------------------------------------------------
-- END NON-SCHEMA CHANGES
-----------------------------------------------------------------------



GO
