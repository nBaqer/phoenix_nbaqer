SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_ChargesByprogram_GetList]
    @CampusId UNIQUEIDENTIFIER
   ,@ProgramId UNIQUEIDENTIFIER
   ,@ProgramVersion VARCHAR(8000) = NULL
   ,@TermId VARCHAR(8000) = NULL
   ,@ChargingMethodId VARCHAR(8000) = NULL
   ,@TransStartDate DATETIME = NULL
   ,@TransEndDate DATETIME = NULL
AS
    SELECT DISTINCT
            S.StudentNumber AS StudentID
           ,S.FirstName AS FirstName
           ,S.LastName AS LastName
           ,T.TransDate
           ,T.TransAmount AS Amount
           ,PV.PrgVerDescrip
           ,PV.PrgVerId
           ,SE.StuEnrollId
           ,P.ProgDescrip
           ,BM.BillingMethodDescrip
           ,SUM(T.TransAmount) OVER ( PARTITION BY T.StuEnrollId ) AS AmountByEnrollment
           ,PP.PeriodNumber
           ,I.IncrementName
           ,IncrementValue
           ,CumulativeValue
           ,ChargeAmount
    FROM    saTransactions T
    INNER JOIN arStuEnrollments SE ON T.StuEnrollId = SE.StuEnrollId
    INNER JOIN saPmtPeriods PP ON PP.PmtPeriodId = T.PmtPeriodId
    INNER JOIN arStudent S ON S.StudentId = SE.StudentId
    INNER JOIN arPrgVersions PV ON PV.PrgVerId = SE.PrgVerId
    INNER JOIN arPrograms P ON P.ProgId = PV.ProgId
    INNER JOIN saBillingMethods BM ON PV.BillingMethodId = BM.BillingMethodId
    INNER JOIN saIncrements I ON I.BillingMethodId = BM.BillingMethodId
    WHERE   SE.CampusId = @CampusId
            AND PV.ProgId = @ProgramId
            AND (
                  @ProgramVersion IS NULL
                  OR PV.PrgVerId IN ( SELECT    Val
                                      FROM      MultipleValuesForReportParameters(@ProgramVersion,',',1) )
                )
            AND (
                  @TermId IS NULL
                  OR TermId IN ( SELECT Val
                                 FROM   MultipleValuesForReportParameters(@TermId,',',1) )
                )
            AND (
                  @ChargingMethodId IS NULL
                  OR PV.BillingMethodId IN ( SELECT Val
                                             FROM   MultipleValuesForReportParameters(@ChargingMethodId,',',1) )
                )
            AND (
                  @TransStartDate IS NULL
                  OR T.TransDate >= @TransStartDate
                )
            AND (
                  @TransEndDate IS NULL
                  OR T.TransDate <= @TransEndDate
                )
            AND T.PmtPeriodId IS NOT NULL
    ORDER BY S.FirstName
           ,S.LastName
           ,T.TransDate; 	



GO
