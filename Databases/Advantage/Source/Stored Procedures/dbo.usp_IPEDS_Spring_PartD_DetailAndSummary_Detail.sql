SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/***

EXEC [usp_IPEDS_Spring_PartD_DetailAndSummary_Detail] '3f5e839a-589a-4b2a-b258-35a1a8b3b819',NULL,'2010','ssn','07/01/2010','6/30/2011',
'academic',1,'e711cc07-3806-4e01-ac15-8788bbdc0e4f','10/15/2010'

EXEC [usp_IPEDS_Spring_PartD_DetailAndSummary_Detail] '3f5e839a-589a-4b2a-b258-35a1a8b3b819',NULL,'2009','ssn','07/01/2009','6/30/2010',
'academic',1,'e711cc07-3806-4e01-ac15-8788bbdc0e4f','10/15/2009'

EXEC [usp_IPEDS_Spring_PartD_DetailAndSummary_Detail] '3f5e839a-589a-4b2a-b258-35a1a8b3b819',NULL,'2008','ssn','07/01/2008','6/30/2009',
'academic',1,'e711cc07-3806-4e01-ac15-8788bbdc0e4f','10/15/2008'

EXEC [usp_IPEDS_Spring_PartD_DetailAndSummary_Detail] '3f5e839a-589a-4b2a-b258-35a1a8b3b819',NULL,'2010','ssn','07/01/2010','6/30/2011',
'program',1,'3610512B-96EA-4AA2-A319-CB9A0789F32E','06/30/2011'
*/

CREATE PROCEDURE [dbo].[usp_IPEDS_Spring_PartD_DetailAndSummary_Detail]
    @CampusId VARCHAR(50)
   ,@ProgId VARCHAR(4000) = NULL
   ,@CohortYear VARCHAR(10) = NULL
   ,@OrderBy VARCHAR(100)
   ,@StartDate DATETIME
   ,@EndDate DATETIME
   ,@SchoolType VARCHAR(50)
   ,@InstitutionType VARCHAR(50)
   ,@LargestProgramID VARCHAR(50) = NULL
   , --NEW DD Rev 12/12/11
    @EndDate_Academic DATETIME = NULL
AS
    DECLARE @ReturnValue VARCHAR(100)
       ,@Value VARCHAR(100);
    DECLARE @SSN VARCHAR(10)
       ,@FirstName VARCHAR(100)
       ,@LastName VARCHAR(100)
       ,@StudentNumber VARCHAR(50)
       ,@TransferredOut INT
       ,@Exclusions INT;
    DECLARE @CitizenShip_IPEDSValue INT
       ,@ProgramType_IPEDSValue INT
       ,@Gender_IPEDSValue INT
       ,@FullTimePartTime_IPEDSValue INT
       ,@StudentId UNIQUEIDENTIFIER; 
    DECLARE @AcademicEndDate DATETIME
       ,@AcademicStartDate DATETIME; 
    --12/05/2012 DE8824 - Academic year options should have a cutoff start date
    DECLARE @AcadInstFirstTimeStartDate DATETIME;
--DECLARE @LargestProgramID VARCHAR(50)  --NEW DD Rev 12/12/11

-- Check if School tracks grades by letter or numeric 
    SET @Value = (
                   SELECT TOP 1
                            Value
                   FROM     dbo.syConfigAppSetValues
                   WHERE    SettingId = 47
                 );


--Here, we're determining if the school type is academic. If so, we will change the new var
--@AcademicEndDate to the @EndDate_Academic, if not, than use @EndDate. We will use the new
--@AcademicEndDate variable to help select the student list.  DD Rev 01/10/12
    IF LOWER(@SchoolType) = 'academic'
        BEGIN
            SET @AcademicEndDate = @EndDate_Academic;
            SET @AcademicStartDate = @EndDate_Academic;
            --12/05/2012 DE8824 - Academic year options should have a cutoff start date
            SET @AcadInstFirstTimeStartDate = DATEADD(YEAR,-1,@EndDate_Academic); 
        END;
    ELSE
        BEGIN
            SET @AcademicEndDate = @EndDate;
            SET @AcademicStartDate = @StartDate;
        END;
	

--CREATE TABLE #LargestProgram
--(LargestProgramID VARCHAR(50), LargeProgramName VARCHAR(100), StudentCount INT)

--Here, we want to get the largest program from the programs selected.
	--BEGIN
	--	INSERT INTO #LargestProgram
	--		SELECT TOP 1 * FROM
	--			(
	--			SELECT t3.ProgId, t3.ProgDescrip, COUNT(t1.StuEnrollId) AS StudentCount
	--			FROM dbo.arStuEnrollments t1, dbo.arPrgVersions t2, dbo.arPrograms t3
	--			WHERE t1.PrgVerId = t2.PrgVerId AND t2.ProgId = t3.ProgId AND
	--			(@progId IS NULL or t3.ProgId IN (Select Val from [MultipleValuesForReportParameters](@ProgId,',',1))) AND
	--			LTRIM(RTRIM(t1.CampusId))= LTRIM(RTRIM(@CampusId)) AND 
	--  t1.StartDate<=@EndDate   
 --     and StuEnrollId not in  
 --     -- Exclude students who are Dropped out/Transferred/Graduated/No Start   
 --     -- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate  
 --     (  
 --      Select t1.StuEnrollId from arStuEnrollments t1,SyStatusCodes t2, dbo.arPrgVersions t3, dbo.arPrograms t4   
 --      where t1.StatusCodeId=t2.StatusCodeId and StartDate <=@EndDate and -- Student started before the end date range  
 --      LTRIM(RTRIM(t1.CampusId))= LTRIM(RTRIM(@CampusId)) and  
 --      t1.PrgVerId = t3.PrgVerId AND t3.ProgId = t4.ProgId AND
	--   (@progId IS NULL or t3.ProgId IN (Select Val from [MultipleValuesForReportParameters](@ProgId,',',1))) AND    
 --      t2.SysStatusId in (12,14,19,8) -- Dropped out/Transferred/Graduated/No Start  
 --      -- Date Determined or ExpGradDate or LDA falls before 08/31/2010  
 --      and (t1.DateDetermined<@StartDate or ExpGradDate<@StartDate or LDA<@StartDate)  
 --     )   
 --     -- If Student is enrolled in only one program version and if that program version   
 --     -- happens to be a continuing ed program exclude the student  
 --                       and t1.StudentId not in   
 --                                  (  
 --                                         Select Distinct StudentId from  
 --                                         (  
 --                                               select StudentId,Count(*) as RowCounter from arStuENrollments   
 --                                               where PrgVerId in (select PrgVerId from arPrgVersions where IsContinuingEd=1)  
 --                                               Group by StudentId Having Count(*)=1  
 --                                         ) dtStudent_ContinuingEd  
 --                                   )
	--			GROUP BY t3.ProgId, t3.ProgDescrip
	--			) D1
	--		ORDER BY D1.StudentCount DESC	
			
	--		SELECT * FROM #LargestProgram
			
 --   END
    
--SET @LargestProgramID = (SELECT TOP 1 LargestProgramID FROM #LargestProgram)



    IF @ProgId IS NOT NULL
        BEGIN
            SELECT  @ReturnValue = COALESCE(@ReturnValue,'') + ProgDescrip + ','
            FROM    arPrograms t1
            WHERE   t1.ProgId IN ( SELECT   Val
                                   FROM     MultipleValuesForReportParameters(@ProgId,',',1) );
            SET @ReturnValue = SUBSTRING(@ReturnValue,1,LEN(@ReturnValue) - 1);
        END;
    ELSE
        BEGIN
            SELECT  @ReturnValue = COALESCE(@ReturnValue,'') + ProgDescrip + ','
            FROM    arPrograms t1
            WHERE   t1.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
                    AND t1.CampGrpId IN ( SELECT    CampGrpId
                                          FROM      syCmpGrpCmps
                                          WHERE     CampusId = @CampusId );
            SET @ReturnValue = SUBSTRING(@ReturnValue,1,LEN(@ReturnValue) - 1);
        END;

/*********Get the list of students that will be shown in the report - Starts Here ******************/			
    CREATE TABLE #StudentsList
        (
         StudentId UNIQUEIDENTIFIER
        ,SSN VARCHAR(10)
        ,FirstName VARCHAR(100)
        ,LastName VARCHAR(100)
        ,MiddleName VARCHAR(50)
        ,StudentNumber VARCHAR(50)
        ,TransferredOut INT
        ,Exclusions INT
        ,CitizenShip_IPEDSValue INT
        ,ProgramType_IPEDSValue INT
        ,Gender_IPEDSValue INT
        ,FullTimePartTime_IPEDSValue INT
        ,GenderId UNIQUEIDENTIFIER
        ,RaceId UNIQUEIDENTIFIER
        ,CitizenId UNIQUEIDENTIFIER
        ,GenderDescription VARCHAR(50)
        ,RaceDescription VARCHAR(50)
        ,StudentGraduatedStatusCount INT
        ,ClockHourProgramCount INT
        ,StartDate DATETIME
        ,HoustingType UNIQUEIDENTIFIER
        );
		
-- The following table will store only Full Time First Time UnderGraduate Students
    CREATE TABLE #UnderGraduateStudentsWhoReceivedFinancialAid
        (
         StudentId UNIQUEIDENTIFIER
        ,FinancialAidType INT
        ,TitleIV BIT
        );


-- Get the list of UnderGraduate Students
-- Get the list of UnderGraduate Students
    IF @SchoolType = 'academic'
        AND @InstitutionType = '1' -- academic and private
        BEGIN

            PRINT 'academic';

            INSERT  INTO #StudentsList
                    SELECT DISTINCT
                            t1.StudentId
                           ,t1.SSN
                           ,t1.FirstName
                           ,t1.LastName
                           ,t1.MiddleName
                           ,t1.StudentNumber
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SQ1
                                       ,syStatusCodes SQ2
                                       ,dbo.sySysStatus SQ3
                              WHERE     SQ1.StatusCodeId = SQ2.StatusCodeId
                                        AND SQ2.SysStatusId = SQ3.SysStatusId
                                        AND SQ1.StudentId = t1.StudentId
                                        AND SQ3.SysStatusId = 19
                                        AND SQ1.TransferDate <= @AcademicEndDate
                            ) AS TransferredOut
                           ,(
					-- Check if the student was either dropped and if the drop reason is either
					-- deceased, active duty, foreign aid service, church mission
                              CASE WHEN (
                                          SELECT    COUNT(*)
                                          FROM      arStuEnrollments SQ1
                                                   ,syStatusCodes SQ2
                                                   ,dbo.sySysStatus SQ3
                                                   ,arDropReasons SQ44
                                          WHERE     SQ1.StatusCodeId = SQ2.StatusCodeId
                                                    AND SQ2.SysStatusId = SQ3.SysStatusId
                                                    AND SQ1.DropReasonId = SQ44.DropReasonId
                                                    AND SQ1.StudentId = t1.StudentId
                                                    AND SQ3.SysStatusId IN ( 12 ) -- Dropped
                                                    AND SQ1.DateDetermined <= @AcademicEndDate
                                                    AND SQ44.IPEDSValue IN ( 15,16,17,18,19 )
                                        ) >= 1 THEN 1
                                   ELSE 0
                              END ) AS Exclusions
                           ,t11.IPEDSValue AS CitizenShip_IPEDSValue
                           ,t9.IPEDSValue AS ProgramType_IPEDSValue
                           ,t3.IPEDSValue AS Gender_IPEDSValue
                           ,t10.IPEDSValue AS FullTimePartTime_IPEDSValue
                           ,t1.Gender
                           ,t1.Race
                           ,t1.Citizen
                           ,(
                              SELECT DISTINCT
                                        AgencyDescrip
                              FROM      syRptAgencyFldValues
                              WHERE     RptAgencyFldValId = t3.IPEDSValue
                            ) AS GenderDescription
                           ,(
                              SELECT DISTINCT
                                        AgencyDescrip
                              FROM      syRptAgencyFldValues
                              WHERE     RptAgencyFldValId = t4.IPEDSValue
                            ) AS RaceDescription
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SEC
                                       ,syStatusCodes SC
                              WHERE     SEC.StudentId = t1.StudentId
                                        AND SEC.StatusCodeId = SC.StatusCodeId
                                        AND SC.SysStatusId = 14
                            ) AS StudentGraduatedStatusCount
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SEC
                                       ,arPrograms P
                                       ,arPrgVersions PV
                              WHERE     SEC.PrgVerId = PV.PrgVerId
                                        AND P.ProgId = PV.ProgId
                                        AND SEC.StudentId = t1.StudentId
                                        AND P.ACId = 5
                            ) AS ClockHourProgramCount
                           ,t2.StartDate
                           ,t1.HousingId
                    FROM    adGenders t3
                    LEFT JOIN arStudent t1 ON t3.GenderId = t1.Gender
                    LEFT JOIN adEthCodes t4 ON t4.EthCodeId = t1.Race
                    INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                    INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                    INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                                 AND t6.SysStatusId NOT IN ( 8 ) -- Ignore "No Start" Students
                    INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                    INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                    INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                    LEFT JOIN arAttendTypes t10 ON t2.AttendTypeId = t10.AttendTypeId
                    LEFT JOIN dbo.adDegCertSeeking t12 ON t2.DegCertSeekingId = t12.DegCertSeekingId
                    INNER JOIN adCitizenships t11 ON t1.Citizen = t11.CitizenshipId
                    INNER JOIN dbo.saTransactions SA ON t2.StuEnrollId = SA.StuEnrollId
                    INNER JOIN saFundSources FS ON SA.FundSourceId = FS.FundSourceId
                            --DE8813 - Item 2
                            --INNER JOIN dbo.saTuitionCategories TC ON t2.TuitionCategoryId = TC.TuitionCategoryId
                    WHERE   LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                            AND (
                                  @ProgId IS NULL
                                  OR t8.ProgId IN ( SELECT  Val
                                                    FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                )
                            AND t10.IPEDSValue = 61
                            AND -- Full Time
                            t12.IPEDSValue = 11
                            AND -- First Time
                            t9.IPEDSValue = 58
                            AND SA.Voided = 0
                            AND FS.AdvFundSourceId NOT IN ( 1,19,23 )
                            AND -- Under Graduate
                           --12/05/2012 DE8824 - Academic year options should have a cutoff start date
                           --( t2.StartDate <= @AcademicEndDate )
                            (
                              t2.StartDate > @AcadInstFirstTimeStartDate
                              AND t2.StartDate <= @AcademicEndDate
                            )
                            AND FS.IPEDSValue IN ( 66,67,68 )  -- Federal,State/local, Institution Grants
                            AND ISNULL(FS.AdvFundSourceId,0) NOT IN ( 5,6,9 ) -- Exclude FWS and Plus Loan  -- FFEL PLUS (9)(added for DE8816)
						--AND TC.IPEDSValue IN (145,146) --Instate or In-district tuition
						--AND (SA.AwardstartDate<=@EndDate OR SA.AwardEndDate<=@EndDate)
                            AND t2.StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
						( SELECT    t1.StuEnrollId
                          FROM      arStuEnrollments t1
                                   ,syStatusCodes t2
                          WHERE     t1.StatusCodeId = t2.StatusCodeId
                                    AND StartDate <= @AcademicEndDate
                                    AND -- Student started before the end date range
                                    LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                    AND (
                                          @ProgId IS NULL
                                          OR t8.ProgId IN ( SELECT  Val
                                                            FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                        )
                                    AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                    AND (
                                          t1.DateDetermined < @AcademicStartDate
                                          OR ExpGradDate < @AcademicStartDate
                                          OR LDA < @AcademicStartDate
                                        ) ) 
                       -- If Student is enrolled in only one program version and if that program version 
					   -- happens to be a continuing ed program exclude the student
                            AND t2.StudentId NOT IN ( SELECT DISTINCT
                                                                StudentId
                                                      FROM      (
                                                                  SELECT    StudentId
                                                                           ,COUNT(*) AS RowCounter
                                                                  FROM      arStuEnrollments
                                                                  WHERE     PrgVerId IN ( SELECT    PrgVerId
                                                                                          FROM      arPrgVersions
                                                                                          WHERE     IsContinuingEd = 1 )
                                                                  GROUP BY  StudentId
                                                                  HAVING    COUNT(*) = 1
                                                                ) dtStudent_ContinuingEd );
        END;


    IF @SchoolType = 'program'
        BEGIN

            PRINT 'program';
            INSERT  INTO #StudentsList
                    SELECT DISTINCT
                            t1.StudentId
                           ,t1.SSN
                           ,t1.FirstName
                           ,t1.LastName
                           ,t1.MiddleName
                           ,t1.StudentNumber
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SQ1
                                       ,syStatusCodes SQ2
                                       ,dbo.sySysStatus SQ3
                              WHERE     SQ1.StatusCodeId = SQ2.StatusCodeId
                                        AND SQ2.SysStatusId = SQ3.SysStatusId
                                        AND SQ1.StudentId = t1.StudentId
                                        AND SQ3.SysStatusId = 19
                                        AND SQ1.TransferDate <= @AcademicEndDate
                            ) AS TransferredOut
                           ,(
					-- Check if the student was either dropped and if the drop reason is either
					-- deceased, active duty, foreign aid service, church mission
                              CASE WHEN (
                                          SELECT    COUNT(*)
                                          FROM      arStuEnrollments SQ1
                                                   ,syStatusCodes SQ2
                                                   ,dbo.sySysStatus SQ3
                                                   ,arDropReasons SQ44
                                          WHERE     SQ1.StatusCodeId = SQ2.StatusCodeId
                                                    AND SQ2.SysStatusId = SQ3.SysStatusId
                                                    AND SQ1.DropReasonId = SQ44.DropReasonId
                                                    AND SQ1.StudentId = t1.StudentId
                                                    AND SQ3.SysStatusId IN ( 12 ) -- Dropped
                                                    AND SQ1.DateDetermined <= @AcademicEndDate
                                                    AND SQ44.IPEDSValue IN ( 15,16,17,18,19 )
                                        ) >= 1 THEN 1
                                   ELSE 0
                              END ) AS Exclusions
                           ,t11.IPEDSValue AS CitizenShip_IPEDSValue
                           ,t9.IPEDSValue AS ProgramType_IPEDSValue
                           ,t3.IPEDSValue AS Gender_IPEDSValue
                           ,t10.IPEDSValue AS FullTimePartTime_IPEDSValue
                           ,t1.Gender
                           ,t1.Race
                           ,t1.Citizen
                           ,(
                              SELECT DISTINCT
                                        AgencyDescrip
                              FROM      syRptAgencyFldValues
                              WHERE     RptAgencyFldValId = t3.IPEDSValue
                            ) AS GenderDescription
                           ,(
                              SELECT DISTINCT
                                        AgencyDescrip
                              FROM      syRptAgencyFldValues
                              WHERE     RptAgencyFldValId = t4.IPEDSValue
                            ) AS RaceDescription
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SEC
                                       ,syStatusCodes SC
                              WHERE     SEC.StudentId = t1.StudentId
                                        AND SEC.StatusCodeId = SC.StatusCodeId
                                        AND SC.SysStatusId = 14
                            ) AS StudentGraduatedStatusCount
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SEC
                                       ,arPrograms P
                                       ,arPrgVersions PV
                              WHERE     SEC.PrgVerId = PV.PrgVerId
                                        AND P.ProgId = PV.ProgId
                                        AND SEC.StudentId = t1.StudentId
                                        AND P.ACId = 5
                            ) AS ClockHourProgramCount
                           ,t2.StartDate
                           ,t1.HousingId
                    FROM    adGenders t3
                    LEFT JOIN arStudent t1 ON t3.GenderId = t1.Gender
                    LEFT JOIN adEthCodes t4 ON t4.EthCodeId = t1.Race
                    INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                    INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                    INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                                 AND t6.SysStatusId NOT IN ( 8 ) -- Ignore "No Start" Students
                    INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                    INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                    INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                    LEFT JOIN arAttendTypes t10 ON t2.AttendTypeId = t10.AttendTypeId
                    LEFT JOIN dbo.adDegCertSeeking t12 ON t2.DegCertSeekingId = t12.DegCertSeekingId
                    INNER JOIN adCitizenships t11 ON t1.Citizen = t11.CitizenshipId
                    INNER JOIN dbo.saTransactions SA ON t2.StuEnrollId = SA.StuEnrollId
                    INNER JOIN saFundSources FS ON SA.FundSourceId = FS.FundSourceId
                    WHERE   LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                            AND (
                                  @ProgId IS NULL
                                  OR t8.ProgId IN ( SELECT  Val
                                                    FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                )
                            AND t10.IPEDSValue = 61
                            AND -- Full Time
                            t12.IPEDSValue = 11
                            AND -- First Time
                            t9.IPEDSValue = 58
                            AND SA.Voided = 0
                            AND FS.AdvFundSourceId NOT IN ( 1,19,23 )
                            AND -- Under Graduate
                            (
                              t2.StartDate >= @AcademicStartDate
                              AND t2.StartDate <= @AcademicEndDate
                            ) 
                        --(t2.StartDate<=@AcademicEndDate) 
                            AND t2.PrgVerId IN ( SELECT PrgVerId
                                                 FROM   arPrgVersions
                                                 WHERE  ProgId = @LargestProgramID ) --Added By DD Rev 12/12/11
                            AND FS.IPEDSValue IN ( 66,67,68 )  -- Federal,State/local, Institution Grants
                            AND ISNULL(FS.AdvFundSourceId,0) NOT IN ( 5,6,9 ) -- Exclude FWS and Plus Loan  -- FFEL PLUS (9)(added for DE8816)
						--AND (SA.AwardstartDate>=@StartDate AND SA.AwardEndDate<=@EndDate)
                            AND t2.StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
						( SELECT    t1.StuEnrollId
                          FROM      arStuEnrollments t1
                                   ,syStatusCodes t2
                          WHERE     t1.StatusCodeId = t2.StatusCodeId
                                    AND StartDate <= @AcademicEndDate
                                    AND -- Student started before the end date range
                                    LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                    AND (
                                          @ProgId IS NULL
                                          OR t8.ProgId IN ( SELECT  Val
                                                            FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                        )
                                    AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                    AND (
                                          t1.DateDetermined < @AcademicStartDate
                                          OR ExpGradDate < @AcademicStartDate
                                          OR LDA < @AcademicStartDate
                                        ) ) 
                       -- If Student is enrolled in only one program version and if that program version 
					   -- happens to be a continuing ed program exclude the student
                            AND t2.StudentId NOT IN ( SELECT DISTINCT
                                                                StudentId
                                                      FROM      (
                                                                  SELECT    StudentId
                                                                           ,COUNT(*) AS RowCounter
                                                                  FROM      arStuEnrollments
                                                                  WHERE     PrgVerId IN ( SELECT    PrgVerId
                                                                                          FROM      arPrgVersions
                                                                                          WHERE     IsContinuingEd = 1 )
                                                                  GROUP BY  StudentId
                                                                  HAVING    COUNT(*) = 1
                                                                ) dtStudent_ContinuingEd );
        END;

/*********Get the list of students that will be shown in the report - E
nds Here *****************8*/

-- Get all Full Time, First Time UnderGraduate Students
-- Use #StudentsList as it pulls UnderGraduate Students
    SELECT  RowNumber
           ,SSN
           ,StudentNumber
           ,StudentName
           ,Group3CountCY
           ,OnCampusCountCY
           ,OffCampusCountCY
           ,WithParentCountCY
           ,FederalGovCY
           ,StateLocalGovCY
           ,InstSourceCY
           ,CASE WHEN Group3CountCY >= 1 THEN 'X'
                 ELSE ''
            END AS Group3CountCYX
           ,CASE WHEN OnCampusCountCY >= 1 THEN 'X'
                 ELSE ''
            END AS OnCampusCountCYX
           ,CASE WHEN OffCampusCountCY >= 1 THEN 'X'
                 ELSE ''
            END AS OffCampusCountCYX
           ,CASE WHEN WithParentCountCY >= 1 THEN 'X'
                 ELSE ''
            END AS WithParentCountCYX
    INTO    #Result
    FROM    (
              SELECT    NEWID() AS RowNumber
                       ,dbo.UDF_FormatSSN(SSN) AS SSN
                       ,StudentNumber
                       ,LastName + ', ' + FirstName + ' ' + ISNULL(MiddleName,'') AS StudentName
                       ,(
                          SELECT    COUNT(DISTINCT t2.StudentId)
                          FROM      arStudent t2
                          WHERE     t2.StudentId = SL.StudentId
                        ) AS Group3CountCY
                       ,(
                          SELECT    COUNT(DISTINCT t1.StudentId)
                          FROM      arStudent t1
                          INNER JOIN arHousing t2 ON t1.HousingId = t2.HousingId
                          WHERE     t1.StudentId = SL.StudentId
                                    AND t2.IPEDSValue = 35
                        ) AS OnCampusCountCY
                       ,(
                          SELECT    COUNT(DISTINCT t1.StudentId)
                          FROM      arStudent t1
                          INNER JOIN arHousing t2 ON t1.HousingId = t2.HousingId
                          WHERE     t1.StudentId = SL.StudentId
                                    AND t2.IPEDSValue = 36
                        ) AS OffCampusCountCY
                       ,(
                          SELECT    COUNT(DISTINCT t1.StudentId)
                          FROM      arStudent t1
                          INNER JOIN arHousing t2 ON t1.HousingId = t2.HousingId
                          WHERE     t1.StudentId = SL.StudentId
                                    AND t2.IPEDSValue = 37
                        ) AS WithParentCountCY
                       ,(
                          SELECT    SUM(TransAmount * -1) AS NetAmount
                          FROM      dbo.saTransactions t1
                          INNER JOIN saFundSources t2 ON t1.FundSourceId = t2.FundSourceId
                          INNER JOIN arStuEnrollments t3 ON t1.StuEnrollId = t3.StuEnrollId
                                                            AND t3.StudentId = SL.StudentId
                          WHERE     t2.IPEDSValue = 66
                                    AND ( t1.TransDate BETWEEN @StartDate AND @EndDate )
                                    AND t1.Voided = 0
                                    AND ISNULL(t2.AdvFundSourceId,0) NOT IN ( 5,6,9 ) -- Exclude FWS and PLus Loan  -- FFEL PLUS (9)(added for DE8816)
                                    AND t2.AdvFundSourceId NOT IN ( 1,19,23 )
                                    AND t3.StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
						( SELECT    t1.StuEnrollId
                          FROM      arStuEnrollments t1
                                   ,syStatusCodes t2
                          WHERE     t1.StatusCodeId = t2.StatusCodeId
                                    AND StartDate <= @EndDate
                                    AND -- Student started before the end date range
                                    LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                    AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                    AND (
                                          t1.DateDetermined < @StartDate
                                          OR ExpGradDate < @StartDate
                                          OR LDA < @StartDate
                                        ) )
                        ) AS FederalGovCY
                       ,(
                          SELECT    SUM(TransAmount * -1) AS NetAmount
                          FROM      dbo.saTransactions t1
                          INNER JOIN saFundSources t2 ON t1.FundSourceId = t2.FundSourceId
                          INNER JOIN arStuEnrollments t3 ON t1.StuEnrollId = t3.StuEnrollId
                                                            AND t3.StudentId = SL.StudentId
                          WHERE     t2.IPEDSValue = 67
                                    AND ( t1.TransDate BETWEEN @StartDate AND @EndDate )
                                    AND t1.Voided = 0
                                    AND ISNULL(t2.AdvFundSourceId,0) NOT IN ( 5,6,9 ) -- Exclude FWS and PLus Loan  -- FFEL PLUS (9)(added for DE8816)
                                    AND t2.AdvFundSourceId NOT IN ( 1,19,23 )
                                    AND t3.StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
						( SELECT    t1.StuEnrollId
                          FROM      arStuEnrollments t1
                                   ,syStatusCodes t2
                          WHERE     t1.StatusCodeId = t2.StatusCodeId
                                    AND StartDate <= @EndDate
                                    AND -- Student started before the end date range
                                    LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                    AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                    AND (
                                          t1.DateDetermined < @StartDate
                                          OR ExpGradDate < @StartDate
                                          OR LDA < @StartDate
                                        ) )
                        ) AS StateLocalGovCY
                       ,(
                          SELECT    SUM(TransAmount * -1) AS NetAmount
                          FROM      dbo.saTransactions t1
                          INNER JOIN saFundSources t2 ON t1.FundSourceId = t2.FundSourceId
                          INNER JOIN arStuEnrollments t3 ON t1.StuEnrollId = t3.StuEnrollId
                                                            AND t3.StudentId = SL.StudentId
                          WHERE     t2.IPEDSValue = 68
                                    AND ( t1.TransDate BETWEEN @StartDate AND @EndDate )
                                    AND t1.Voided = 0
                                    AND ISNULL(t2.AdvFundSourceId,0) NOT IN ( 5,6,9 ) -- Exclude FWS and PLus Loan  -- FFEL PLUS (9)(added for DE8816)
                                    AND t2.AdvFundSourceId NOT IN ( 1,19,23 )
                                    AND t3.StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
						( SELECT    t1.StuEnrollId
                          FROM      arStuEnrollments t1
                                   ,syStatusCodes t2
                          WHERE     t1.StatusCodeId = t2.StatusCodeId
                                    AND StartDate <= @EndDate
                                    AND -- Student started before the end date range
                                    LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                    AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                    AND (
                                          t1.DateDetermined < @StartDate
                                          OR ExpGradDate < @StartDate
                                          OR LDA < @StartDate
                                        ) )
                        ) AS InstSourceCY
              FROM      #StudentsList SL
            ) dt
    ORDER BY CASE WHEN @OrderBy = 'SSN' THEN dt.SSN
             END
           ,CASE WHEN @OrderBy = 'LastName' THEN dt.StudentName
            END
           ,
		-- Updated 11/27/2012 - sort by student number not working
		--Case When @OrderBy='StudentNumber' Then Convert(int,StudentNumber)
            CASE WHEN @OrderBy = 'Student Number' THEN dt.StudentNumber
            END;

    SELECT  SSN
           ,StudentNumber
           ,StudentName
           ,Group3CountCY
           ,OnCampusCountCY
           ,OffCampusCountCY
           ,WithParentCountCY
           ,Group3CountCYX
           ,OnCampusCountCYX
           ,OffCampusCountCYX
           ,WithParentCountCYX
           ,SUM(FederalGovCY) AS FederalGovCY
           ,SUM(StateLocalGovCY) AS StateLocalGovCY
           ,SUM(InstSourceCY) AS InstSourceCY
    FROM    #Result
    GROUP BY SSN
           ,StudentNumber
           ,StudentName
           ,Group3CountCY
           ,OnCampusCountCY
           ,OffCampusCountCY
           ,WithParentCountCY
           ,Group3CountCYX
           ,OnCampusCountCYX
           ,OffCampusCountCYX
           ,WithParentCountCYX
    ORDER BY CASE WHEN @OrderBy = 'SSN' THEN SSN
             END
           ,CASE WHEN @OrderBy = 'LastName' THEN StudentName
            END
           ,
		-- Updated 11/27/2012 - sort by student number not working
		--Case When @OrderBy='StudentNumber' Then Convert(int,StudentNumber)
            CASE WHEN @OrderBy = 'Student Number' THEN StudentNumber
            END;

    DROP TABLE #UnderGraduateStudentsWhoReceivedFinancialAid;
    DROP TABLE #StudentsList;
    DROP TABLE #Result;
    


GO
