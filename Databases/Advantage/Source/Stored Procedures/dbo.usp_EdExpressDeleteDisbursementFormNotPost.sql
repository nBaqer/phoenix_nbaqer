SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[usp_EdExpressDeleteDisbursementFormNotPost]
    @ParentId VARCHAR(50)
   ,@DetailId VARCHAR(50)
AS
    BEGIN
        DECLARE @CountDetails INT;
        DELETE  FROM syEDExpNotPostPell_ACG_SMART_Teach_DisbursementTable
        WHERE   DetailId = @DetailId;
        SELECT  @CountDetails = COUNT(*)
        FROM    syEDExpNotPostPell_ACG_SMART_Teach_DisbursementTable
        WHERE   ParentId = @ParentId;
        IF @CountDetails = 0
            BEGIN
                DELETE  FROM syEDExpNotPostPell_ACG_SMART_Teach_StudentTable
                WHERE   ParentId = @ParentId;
            END;
    END;





GO
