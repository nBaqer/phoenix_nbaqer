SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jose Torres
-- Create date: 7/17/2012
-- Description:	Get the all active Campuses id and description
--              Used in Report
-- EXECUTE RS_GetActiveCampuses
-- =============================================
CREATE PROCEDURE [dbo].[RS_GetActiveCampuses]
AS
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;

        SELECT  C.CampusId
               ,C.CampDescrip
        FROM    syCampuses AS C
        INNER JOIN syStatuses AS ST ON C.StatusId = ST.StatusId
        WHERE   ST.Status = 'Active'
        ORDER BY C.CampDescrip;
    END;





GO
