SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_prerequisitesbyeffectivedate_deletelist]
    @ReqId UNIQUEIDENTIFIER
   ,@EffectiveDate DATETIME
AS
    SET NOCOUNT ON;
    DELETE  FROM arPrerequisites_GradeComponentTypes_Courses
    WHERE   EffectiveDate = @EffectiveDate
            AND ReqId = @ReqId;



GO
