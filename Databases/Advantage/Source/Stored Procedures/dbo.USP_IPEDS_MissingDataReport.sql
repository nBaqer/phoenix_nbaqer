SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[USP_IPEDS_MissingDataReport]
    @campusId AS VARCHAR(50)
   ,@ProgId AS VARCHAR(8000) = NULL
   ,--'112AE539-EF1F-4513-A795-04D360C254C3,C1B7EB2C-9E8D-490E-A04A-50544FCBA85D,F8DB1537-7F52-49B4-805F-6F2603BF44BD',
    @ShowSSN AS BIT = 0
   ,@OrderBY AS INT = 1
   ,@OffRepDate DATETIME
   ,@ExcludeStuBefThisDate DATETIME
AS
    BEGIN
	/************************************************************************************************************************************
		
			Business Rules for Missing Data Report
			
			1.	Exclude students whose current enrollment status is
					b.	No Start
			
			2.	If student’s current status is Dropped, Expelled or Terminated

					a.	Check if drop date is less than or equal to Official Reporting date (or date range)
							i.	If above condition is met and if student’s LDA falls before the “Exclude” date, 
							then exclude student. 

			3.	If student is currently in Grad status,
					a.	Check if expected grad date is less than or equal to Official Reporting date (or date range)
							i.	If above condition is met and if student’s expected Grad Date falls 
							before the “Exclude” date, then exclude student. 

	**************************************************************************************************************************************/
        SELECT  t.*
               ,(
                  SELECT    CASE WHEN t.Sex = 'X' THEN 1
                                 ELSE 0
                            END
                ) AS SexCount
               ,(
                  SELECT    CASE WHEN t.Race = 'X' THEN 1
                                 ELSE 0
                            END
                ) AS RaceCount
               ,(
                  SELECT    CASE WHEN t.Citizenship = 'X' THEN 1
                                 ELSE 0
                            END
                ) AS CitizenshipCount
               ,(
                  SELECT    CASE WHEN t.DegreeCertSeekingStatus = 'X' THEN 1
                                 ELSE 0
                            END
                ) AS DegreeCertSeekingStatusCount
               ,(
                  SELECT    CASE WHEN t.EnrollmentStatus = 'X' THEN 1
                                 ELSE 0
                            END
                ) AS EnrollmentStatusCount
               ,(
                  SELECT    CASE WHEN t.EducationAttempted = 'X' THEN 1
                                 ELSE 0
                            END
                ) AS EducationAttemptedCount
               ,(
                  SELECT    CASE WHEN t.Housing = 'X' THEN 1
                                 ELSE 0
                            END
                ) AS HousingCount
               ,(
                  SELECT    CASE WHEN t.BirthDate = 'X' THEN 1
                                 ELSE 0
                            END
                ) AS BirthDateCount
               ,
	--(select case when t.AdminCriteria='X' then 1 else 0 end) as AdminCriteriaCount,
                (
                  SELECT    CASE WHEN t.HSGrad_GEDDate = 'X' THEN 1
                                 ELSE 0
                            END
                ) AS HSGrad_GEDDateCount
               ,(
                  SELECT    CASE WHEN t.IncomeLevel = 'X' THEN 1
                                 ELSE 0
                            END
                ) AS IncomeLevelCount
               ,(
                  SELECT    CASE WHEN t.TuitionCategory = 'X' THEN 1
                                 ELSE 0
                            END
                ) AS TuitionCategoryCount
        FROM    (
                  SELECT    *
                  FROM      (
                              SELECT    CASE @ShowSSN
                                          WHEN 0 THEN S.StudentNumber
                                          ELSE CASE WHEN S.SSN IS NULL THEN ''
                                                    ELSE SUBSTRING(S.SSN,1,3) + '-' + SUBSTRING(S.SSN,4,2) + '-' + SUBSTRING(S.SSN,6,4)
                                               END
                                        END AS StudentIdentifier
                                       ,RTRIM(LTRIM(S.LastName)) + ', ' + RTRIM(LTRIM(S.FirstName)) + RTRIM(LTRIM(CASE ( ISNULL(S.MiddleName,'') )
                                                                                                                    WHEN '' THEN ''
                                                                                                                    ELSE ', ' + S.MiddleName
                                                                                                                  END)) AS StudentName
                                       ,ISNULL((
                                                 SELECT ''
                                                 FROM   adGenders G
                                                 WHERE  G.GenderId = S.Gender
                                               ),'X') AS Sex
                                       ,ISNULL((
                                                 SELECT ''
                                                 FROM   adEthCodes E
                                                 WHERE  E.EthCodeId = S.Race
                                               ),'X') AS Race
                                       ,ISNULL((
                                                 SELECT ''
                                                 FROM   adCitizenships C
                                                 WHERE  C.CitizenshipId = S.Citizen
                                               ),'X') AS Citizenship
                                       ,ISNULL((
                                                 SELECT ''
                                                 FROM   adDegCertSeeking D
                                                 WHERE  D.DegCertSeekingId = SE.DegCertSeekingId
                                               ),'X') AS DegreeCertSeekingStatus
                                       ,ISNULL((
                                                 SELECT ''
                                                 FROM   arAttendTypes SC
                                                 WHERE  SC.AttendTypeId = SE.AttendTypeId
                                               ),'X') AS EnrollmentStatus
                                       ,ISNULL((
                                                 SELECT ''
                                                 FROM   adEdLvls ED
                                                 WHERE  ED.EdLvlId = SE.EdLvlId
                                               ),'X') AS EducationAttempted
                                       ,ISNULL((
                                                 SELECT ''
                                                 FROM   arHousing H
                                                 WHERE  H.HousingId = S.HousingId
                                               ),'X') AS Housing
                                       ,ISNULL((
                                                 SELECT ''
                                                 FROM   syFamilyIncome FI
                                                 WHERE  FI.FamilyIncomeID = S.FamilyIncome
                                               ),'X') AS IncomeLevel
                                       ,ISNULL((
                                                 SELECT ''
                                                 FROM   saTuitionCategories TC
                                                 WHERE  TC.TuitionCategoryId = SE.TuitionCategoryId
                                               ),'X') AS TuitionCategory
                                       ,CASE ( ISNULL(DOB,'') )
                                          WHEN '' THEN 'X'
                                          ELSE ''
                                        END AS BirthDate
                                       ,
							--isnull((select '' from adAdminCriteria AC where AC.admincriteriaid =S.admincriteriaid),'X')as AdminCriteria,
                                        CASE ( ISNULL(SE.graduatedorreceiveddate,'') )
                                          WHEN '' THEN 'X'
                                          ELSE ''
                                        END AS HSGrad_GEDDate
                                       ,CASE WHEN SC.SysStatusId = 14 THEN SE.ExpGradDate
                                             WHEN ( SC.SysStatusId = 12 ) THEN (
                                                                                 SELECT MAX(LDA)
                                                                                 FROM   (
                                                                                          SELECT    MAX(AttendedDate) AS LDA
                                                                                          FROM      arExternshipAttendance
                                                                                          WHERE     StuEnrollId = SE.StuEnrollId
                                                                                          UNION ALL
                                                                                          SELECT    MAX(MeetDate) AS LDA
                                                                                          FROM      atClsSectAttendance
                                                                                          WHERE     StuEnrollId = SE.StuEnrollId
                                                                                                    AND Actual > 0
                                                                                          UNION ALL
                                                                                          SELECT    MAX(AttendanceDate) AS LDA
                                                                                          FROM      atAttendance
                                                                                          WHERE     EnrollId = SE.StuEnrollId
                                                                                                    AND Actual > 0
                                                                                          UNION ALL
                                                                                          SELECT    MAX(RecordDate) AS LDA
                                                                                          FROM      arStudentClockAttendance
                                                                                          WHERE     StuEnrollId = SE.StuEnrollId
                                                                                                    AND (
                                                                                                          ActualHours > 0
                                                                                                          AND ActualHours <> 99.00
                                                                                                          AND ActualHours <> 999.00
                                                                                                          AND ActualHours <> 9999.00
                                                                                                        )
                                                                                          UNION ALL
                                                                                          SELECT    MAX(MeetDate) AS LDA
                                                                                          FROM      atConversionAttendance
                                                                                          WHERE     StuEnrollId = SE.StuEnrollId
                                                                                                    AND (
                                                                                                          Actual > 0
                                                                                                          AND Actual <> 99.00
                                                                                                          AND Actual <> 999.00
                                                                                                          AND Actual <> 9999.00
                                                                                                        )
                                                                                        ) TR
                                                                               )
                                             ELSE SE.ExpGradDate
                                        END AS ExcludeDate
                                       ,SE.StartDate AS StartDate
                              FROM      arStudent S
                              LEFT OUTER JOIN arStuEnrollments SE ON S.StudentId = SE.StudentId
                              INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                              INNER JOIN arPrograms P ON P.ProgId = PV.ProgId
                              INNER JOIN syStatusCodes SC ON SE.StatusCodeId = SC.StatusCodeId
                              WHERE     (
                                          @ProgId IS NULL
                                          OR P.ProgId IN ( SELECT   strval
                                                           FROM     dbo.SPLIT(@ProgId) )
                                        )
                                        AND SE.CampusId = @campusId
                                        AND ( SC.SysStatusId <> 8 )
                                        AND SE.StartDate <= @OffRepDate
                                        AND SC.SysStatusId IN ( 12,14 )
                            ) t1
                  WHERE     ExcludeDate > @ExcludeStuBefThisDate
                  UNION
                  SELECT    CASE @ShowSSN
                              WHEN 0 THEN S.StudentNumber
                              ELSE CASE WHEN S.SSN IS NULL THEN ''
                                        ELSE SUBSTRING(S.SSN,1,3) + '-' + SUBSTRING(S.SSN,4,2) + '-' + SUBSTRING(S.SSN,6,4)
                                   END
                            END AS StudentIdentifier
                           ,RTRIM(LTRIM(S.LastName)) + ', ' + RTRIM(LTRIM(S.FirstName)) + RTRIM(LTRIM(CASE ( ISNULL(S.MiddleName,'') )
                                                                                                        WHEN '' THEN ''
                                                                                                        ELSE ', ' + S.MiddleName
                                                                                                      END)) AS StudentName
                           ,ISNULL((
                                     SELECT ''
                                     FROM   adGenders G
                                     WHERE  G.GenderId = S.Gender
                                   ),'X') AS Sex
                           ,ISNULL((
                                     SELECT ''
                                     FROM   adEthCodes E
                                     WHERE  E.EthCodeId = S.Race
                                   ),'X') AS Race
                           ,ISNULL((
                                     SELECT ''
                                     FROM   adCitizenships C
                                     WHERE  C.CitizenshipId = S.Citizen
                                   ),'X') AS Citizenship
                           ,ISNULL((
                                     SELECT ''
                                     FROM   adDegCertSeeking D
                                     WHERE  D.DegCertSeekingId = SE.DegCertSeekingId
                                   ),'X') AS DegreeCertSeekingStatus
                           ,ISNULL((
                                     SELECT ''
                                     FROM   arAttendTypes SC
                                     WHERE  SC.AttendTypeId = SE.AttendTypeId
                                   ),'X') AS EnrollmentStatus
                           ,ISNULL((
                                     SELECT ''
                                     FROM   adEdLvls ED
                                     WHERE  ED.EdLvlId = SE.EdLvlId
                                   ),'X') AS EducationAttempted
                           ,ISNULL((
                                     SELECT ''
                                     FROM   arHousing H
                                     WHERE  H.HousingId = S.HousingId
                                   ),'X') AS Housing
                           ,ISNULL((
                                     SELECT ''
                                     FROM   syFamilyIncome FI
                                     WHERE  FI.FamilyIncomeID = S.FamilyIncome
                                   ),'X') AS IncomeLevel
                           ,ISNULL((
                                     SELECT ''
                                     FROM   saTuitionCategories TC
                                     WHERE  TC.TuitionCategoryId = SE.TuitionCategoryId
                                   ),'X') AS TuitionCategory
                           ,CASE ( ISNULL(DOB,'') )
                              WHEN '' THEN 'X'
                              ELSE ''
                            END AS BirthDate
                           ,
				--isnull((select '' from adAdminCriteria AC where AC.admincriteriaid =S.admincriteriaid),'X')as AdminCriteria,
                            CASE ( ISNULL(SE.graduatedorreceiveddate,'') )
                              WHEN '' THEN 'X'
                              ELSE ''
                            END AS HSGrad_GEDDate
                           ,SE.ExpGradDate AS ExcludeDate
                           ,SE.StartDate AS StartDate
                  FROM      arStudent S
                  LEFT OUTER JOIN arStuEnrollments SE ON S.StudentId = SE.StudentId
                  INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                  INNER JOIN arPrograms P ON P.ProgId = PV.ProgId
                  INNER JOIN syStatusCodes SC ON SE.StatusCodeId = SC.StatusCodeId
                  WHERE     (
                              @ProgId IS NULL
                              OR P.ProgId IN ( SELECT   strval
                                               FROM     dbo.SPLIT(@ProgId) )
                            )
                            AND SE.CampusId = @campusId
                            AND ( SC.SysStatusId <> 8 )
                            AND SE.StartDate <= @OffRepDate
                            AND SC.SysStatusId NOT IN ( 12,14 )
                ) t
        WHERE   (
                  Sex = 'X'
                  OR Race = 'X'
                  OR Citizenship = 'X'
                  OR DegreeCertSeekingStatus = 'X'
                  OR EnrollmentStatus = 'X'
                  OR EducationAttempted = 'X'
                  OR Housing = 'X'
                  OR BirthDate = 'X' 
		--or AdminCriteria='X' 
                  OR HSGrad_GEDDate = 'X'
                  OR IncomeLevel = 'X'
                  OR TuitionCategory = 'X'
                ) 
		-- and ExcludeDate>@ExcludeStuBefThisDate            
GROUP BY        StudentIdentifier
               ,StudentName
               ,Sex
               ,Race
               ,Citizenship
               ,DegreeCertSeekingStatus
               ,EnrollmentStatus
               ,EducationAttempted
               ,Housing
               ,BirthDate
               ,HSGrad_GEDDate
               ,IncomeLevel
               ,TuitionCategory
               ,ExcludeDate
               ,StartDate
        ORDER BY CASE @OrderBY
                   WHEN 1 THEN t.StudentIdentifier
                   ELSE t.StudentName
                 END; 
    END;




GO
