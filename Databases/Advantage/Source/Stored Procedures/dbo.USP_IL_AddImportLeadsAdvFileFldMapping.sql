SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[USP_IL_AddImportLeadsAdvFileFldMapping]
    @MappingId UNIQUEIDENTIFIER
   ,@FieldNumber INT
   ,@ILFieldId INT
AS
    INSERT  INTO adImportLeadsAdvFldMappings
            (
             AdvFieldMapping
            ,MappingId
            ,FieldNumber
            ,ILFieldId
			)
            SELECT  NEWID()
                   ,@MappingId
                   ,@FieldNumber
                   ,@ILFieldId; 









GO
