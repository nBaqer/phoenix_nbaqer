SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[usp_EdExpress_HasDisbursementBeenPaid]
    @FAID VARCHAR(50)
   ,@DisbNum INTEGER
AS
    BEGIN
        SELECT  COUNT(sas.AwardScheduleId)
        FROM    faStudentAwardSchedule sas
               ,faStudentAwards sa
               ,saPmtDisbRel pdr
        WHERE   sas.StudentAwardId = sa.StudentAwardId
                AND sas.AwardScheduleId = pdr.AwardScheduleId
                AND sa.FA_Id = @FAID
                AND sas.DisbursementNumber = @DisbNum;
    END;





GO
