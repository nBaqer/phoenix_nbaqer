SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_GetCampusId_ForLead]
    @EntityId UNIQUEIDENTIFIER
AS
    SELECT TOP 1
            CampusId
    FROM    adLeads
    WHERE   LeadId = @EntityId;



GO
