SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jose Alfredo
-- Create date: 8/27/2015
-- Description:	Insert/Update New/Updated Column in resources
--              The column is register in syFields, caption
--				and required value are register also.
--				the fields is linked to a table and to 
--				specific resource. If the table does not exists be created
--				Be CAREFULLY if you enter a ntable name erroneous, the table
--              resource should be created
-- Conditions: 1 -Resource in syResources must exists
-- 
-- =============================================
CREATE PROCEDURE [dbo].[DEVELOPMENT_InsertUpdateFieldInResources]
 
   -- Fill with your fieldName (the column to be inserted in resources. This name must be unique
    @FieldName VARCHAR(50)
   ,
 -- Enter the type of field (see table syFieldsType)
 -- FldTypeId	FldType
 -- 2	Smallint
 -- 3	Int
 -- 5	Float
 -- 6	Money
 -- 11	Bit
 -- 17	TinyInt
 -- 72	Uniqueidentifier
 -- 129	Char
 -- 131	Decimal
 -- 135	Datetime
 -- 200	Varchar
    @FldTypeId INT
   
   -- Field Length. this is valid for text
    ,@FldLen INT = 36
   
   -- DLL table ID to be connected in the case of combobox 
   -- form 3.8 it is only use in Ad Hoc report. Otherwise 0
   --,@DDLId INT = 0
	-- No idea, leave NULL unless you know what to do
   ,@DerivedField BIT = NULL
	-- No idea leave NULL  unless you know what to do
   ,@SchlReq BIT = NULL
   ,   
   -- If the field should be included in the log 
   -- 1 log change 0 no log.
    @LogChanges BIT = 0
	-- If the field use any type of mask. Not used in 3.8 and upper.
   ,@Mask VARCHAR(50) = NULL
   ,    
	-- Begin personalitation FOR syFldCaptions ------------------------------------------------------------
    -- Caption to be show if the column is displayed in Advantage
    @FieldCaption VARCHAR(50) = ''
	--Language ID to select a specific language in table. Use 1 USA in moment
	-- it is the only
   ,@LanguageId INT = 1
   ,
-- Personalitation Table syTable
    -- Name of the table where the resource is
    @TblName VARCHAR(50) = ''
   ,
	-- Table Description
    @TblDescription VARCHAR(50) = ''
   -- Table PK ?
   --	,@TblPK INT

-- Begin personalitation FOR SyTblFlds ------------------------------------------------------------

	-- CategoryId
	-- 
	-- CategoryId	EntityID	Description
	-- 13940		394			General Information
	-- 13941		394			Course
	-- 13942		394			Programs
	-- 13945		394			Enrollment
	-- 13944		394			SAP
	-- 23950		395			General Information
	-- 23951		395			Advertisement
	-- 23952		395			Admissions Rep
	-- 23953		395			Education
	-- 33960		396			General Information
	-- 43970		397			General Information
	-- 13949		394			Placement Information
	-- 54340		434			General Information
	-- 1			394			User Defined
	-- 2			395			User Defined
	-- 3			396			User Defined
	-- 4			397			User Defined
	-- 5			434			User Defined
	-- 13946		394			Ledger
	-- 13947		394			Awards
	-- 13948		394			Payments
	-- 13949		394			Requirements
	-- 13950		394			Education
	-- 13951		394			Term
	-- 13952		394			Class
	-- 13953		394			Exit Interview
	-- 13954		394			Placement History
	-- 54341		395			Lead Transactions 
   ,@CategoryId INT = NULL
   ,
 -- has only sense in Ad Hoc reports. 
    @FKColDescrip VARCHAR(100) = ''
   ,
-- Begin personalitation FOR SyResTblFlds ------------------------------------------------------------
    -- Resource Id that link the field with a specific resource in Advantage.
	-- Can be a page, a type etc.
    @PageResourceId INT = 22
   ,
-- 1 is a required field 0 not.
    @Required BIT = 1
   ,
    -- Control name associate with the field
	-- Not used in 3.8 lead
    @ControlName VARCHAR(50) = NULL
 -- no required in lead info
-- End personalitation FOR SyResTblFlds ---
AS
    BEGIN
-- *****************************************************************************************************************
        BEGIN TRAN;
        SET NOCOUNT ON;
-- STEP 1 Determine if the table is registered, is not register it in syTables as first step
--		  If table exists the record is updated!!

	-- See If the table exists in syTables
        DECLARE @TableId INT = (
                                 SELECT TOP 1
                                        TblId
                                 FROM   syTables
                                 WHERE  TblName = @TblName
                               );
        IF @TableId IS NULL
            BEGIN -- insert table
                SET @TableId = (
                                 SELECT MAX(TblId) + 1
                                 FROM   syTables
                               );
                INSERT  INTO dbo.syTables
                        (
                         TblId
                        ,TblName
                        ,TblDescrip
                        ,TblPK
                        )
                VALUES  (
                         @TableId  -- TblId - int
                        ,@TblName  -- TblName - varchar(50)
                        ,@TblDescription  -- TblDescrip - varchar(100)
                        ,0 -- TblPK- int (provisional later this change to the real value)
                        );

            END;
        ELSE
            BEGIN
                UPDATE  dbo.syTables
                SET     TblName = @TblName
                       ,TblDescrip = @TblDescription
--                       ,TblPK = @TblPK
                WHERE   TblId = @TableId;
            END;

-- STEP II - syFields - Check if the field associated exists. should be only one.

    -- Insert statements for procedure here
	-- Check if exists the field
        DECLARE @FieldId INT = (
                                 SELECT TOP 1
                                        syFields.FldId
                                 FROM   syFields
                                 WHERE  FldName = @FieldName
                               );

        IF @FieldId IS NULL
            BEGIN -- INSERT 
                SET @FieldId = (
                                 SELECT MAX(FldId) + 1
                                 FROM   syFields
                               ); 
                INSERT  INTO syFields
                        (
                         FldId
                        ,FldName
                        ,FldTypeId
                        ,FldLen
--                        ,DDLId
                        ,DerivedFld
                        ,SchlReq
                        ,LogChanges
                        ,Mask
                        )
                VALUES  (
                         @FieldId
                        ,@FieldName
                        ,@FldTypeId
                        ,@FldLen
--                        ,@DDLId
                        ,@DerivedField
                        ,@SchlReq
                        ,@LogChanges
                        ,@Mask
                        );
            END;
        ELSE
            BEGIN
                UPDATE  syFields
                SET     FldName = @FieldName
                       ,FldTypeId = @FldTypeId
                       ,FldLen = @FldLen
--                       ,DDLId = @DDLId
                       ,DerivedFld = @DerivedField
                       ,SchlReq = @SchlReq
                       ,LogChanges = @LogChanges
                       ,Mask = @Mask
                WHERE   ( FldId = @FieldId );
            END;

 
-- STEP III: Add in syTblFlds the relation between the new column and the table that contain it************
 
      
        DECLARE @TblId INT = (
                               SELECT TOP 1
                                        TblId
                               FROM     syTables
                               WHERE    TblName = @TblName
                             );

        DECLARE @TblFldId INT = (
                                  SELECT TOP 1
                                            TblFldsId
                                  FROM      dbo.syTblFlds
                                  WHERE     TblId = @TblId
                                            AND FldId = @FieldId
                                );
     
-- If the relation does not exists 
 
        IF @TblFldId IS NULL
            BEGIN
                SET @TblFldId = (
                                  SELECT    MAX(TblFldsId) + 1
                                  FROM      syTblFlds
                                ); 

                INSERT  INTO dbo.syTblFlds
                        (
                         TblFldsId
                        ,TblId
                        ,FldId
                        ,CategoryId
                        ,FKColDescrip
				        )
                VALUES  (
                         @TblFldId  -- TblFldsId - int
                        ,@TblId  -- TblId - int
                        ,@FieldId  -- FldId - int
                        ,@CategoryId  -- CategoryId - int
                        ,@FKColDescrip  -- FKColDescrip - varchar(50)
				        );	
            END;
        ELSE
            BEGIN
                
                UPDATE  dbo.syTblFlds
                SET     TblId = @TblId
                       ,FldId = @FieldId
                       ,CategoryId = @CategoryId
                       ,FKColDescrip = @FKColDescrip
                WHERE   TblFldsId = @TblFldId;
            END;

--STEP IV: Add in syFldCaptions the caption for the field ********************************************
 
        DECLARE @PlacementFldId INT = (
                                        SELECT TOP 1
                                                syFields.FldId
                                        FROM    syFields
                                        --JOIN    dbo.syTblFlds ON syTblFlds.FldId = syFields.FldId
                                        --JOIN    dbo.syTables ON syTables.TblId = syTblFlds.TblId
                                        WHERE   FldName = @FieldName
                                        --        AND TblName = @TblName
                                      );
        DECLARE @PlacementCaption INT = (
                                          SELECT TOP 1
                                                    FldCapId
                                          FROM      syFldCaptions
                                          WHERE     FldId = @PlacementFldId
                                        );
--insert if not exists
        IF @PlacementCaption IS NULL
            BEGIN
                INSERT  INTO syFldCaptions
                        (
                         FldCapId
                        ,FldId
                        ,LangId
                        ,Caption
                        ,FldDescrip
                        )
                VALUES  (
                         (
                           SELECT   MAX(FldCapId)
                           FROM     syFldCaptions
                         ) + 1
                        ,@PlacementFldId
                        ,1
                        ,@FieldCaption
                        ,NULL
                        );
            END;
        ELSE
            BEGIN
                UPDATE  syFldCaptions
                SET     FldId = @PlacementFldId
                       ,LangId = @LanguageId
                       ,Caption = @FieldCaption
                       ,FldDescrip = NULL
                WHERE   FldCapId = @PlacementCaption;
            END;   	

 
-- STEP V: Add in syResTblFields the relation between the new column and the resource in syResources
 
        DECLARE @NewResTblId INT = (
                                     SELECT MAX(ResDefId)
                                     FROM   syResTblFlds
                                   ) + 1;
 
        DECLARE @TestResource INT = (
                                      SELECT    COUNT(*)
                                      FROM      dbo.syResTblFlds
                                      WHERE     ResourceId = @PageResourceId
                                                AND TblFldsId = @TblFldId
                                    );
        IF @TestResource = 0
            BEGIN
 
                INSERT  dbo.syResTblFlds
                        (
                         ResDefId
                        ,ResourceId
                        ,TblFldsId
                        ,Required
                        ,SchlReq
                        ,ControlName
                        ,UsePageSetup
				        )
                VALUES  (
                         @NewResTblId  -- ResDefId - int
                        ,@PageResourceId  -- ResourceId - smallint
                        ,@TblFldId  -- TblFldsId - int
                        ,@Required  -- Required - bit
                        ,0  -- SchlReq - bit
                        ,@ControlName  -- ControlName - varchar(50)
                        ,0  -- UsePageSetup - bit
				        );
            END;
        ELSE
            BEGIN
                SET @NewResTblId = (
                                     SELECT TOP 1
                                            ResDefId
                                     FROM   dbo.syResTblFlds
                                     WHERE  ResourceId = @PageResourceId
                                            AND TblFldsId = @TblFldId
                                   ); 
                UPDATE  dbo.syResTblFlds
                SET     ResourceId = @PageResourceId
                       ,TblFldsId = @TblFldId
                       ,Required = @Required
                       ,ControlName = @ControlName
                WHERE   ResDefId = @NewResTblId;
            END;
        COMMIT;
    END;

GO
