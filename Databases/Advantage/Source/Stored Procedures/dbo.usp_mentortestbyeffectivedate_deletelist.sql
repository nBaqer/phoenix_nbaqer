SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_mentortestbyeffectivedate_deletelist]
    @ReqId UNIQUEIDENTIFIER
   ,@EffectiveDate DATETIME
AS
    SET NOCOUNT ON;
    DELETE  FROM arMentor_GradeComponentTypes_Courses
    WHERE   EffectiveDate = @EffectiveDate
            AND ReqId = @ReqId;



GO
