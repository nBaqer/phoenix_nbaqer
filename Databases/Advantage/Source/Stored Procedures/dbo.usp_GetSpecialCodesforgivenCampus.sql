SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[usp_GetSpecialCodesforgivenCampus]
    (
     @CampusID AS VARCHAR(50)
    )
AS
    BEGIN 
  
        SELECT  TCSpecialCode AS SpecialCode
        FROM    dbo.arTimeClockSpecialCode
        WHERE   CampusID = @CampusID;
    END;






GO
