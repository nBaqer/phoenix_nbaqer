SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_GetAllActiveAndInActiveGradeScales] @campusId VARCHAR(50)
AS
    SET NOCOUNT ON;
    SELECT  T.GrdScaleId
           ,T.Descrip
           ,( CASE ST.Status
                WHEN 'Active' THEN 1
                ELSE 0
              END ) AS Status
    FROM    arGradeScales T
           ,syStatuses ST
    WHERE   T.StatusId = ST.StatusId
            AND (
                  T.CampGrpId IN ( SELECT   CampGrpId
                                   FROM     syCmpGrpCmps
                                   WHERE    CampusId = @campusId
                                            AND CampGrpId <> (
                                                               SELECT   CampGrpId
                                                               FROM     syCampGrps
                                                               WHERE    CampGrpDescrip = 'ALL'
                                                             ) )
                  OR CampGrpId = (
                                   SELECT   CampGrpId
                                   FROM     syCampGrps
                                   WHERE    CampGrpDescrip = 'ALL'
                                 )
                )
    ORDER BY Status
           ,T.Descrip; 




GO
