SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_GetLeadGroups]
    @campusId UNIQUEIDENTIFIER
   ,@showActiveOnly BIT
AS
    IF @showActiveOnly = 1
        BEGIN
            EXEC dbo.usp_GetActiveLeadGroups @campusId;
        END;
    ELSE
        BEGIN
            EXEC dbo.usp_GetActiveAndInActiveLeadGroups @campusId;
        END;



GO
