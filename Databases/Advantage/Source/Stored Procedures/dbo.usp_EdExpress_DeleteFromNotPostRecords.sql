SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[usp_EdExpress_DeleteFromNotPostRecords]
    @MsgType NVARCHAR(50)
   ,@FileName NVARCHAR(250)
AS
    DELETE  FROM syEDExpNotPostPell_ACG_SMART_Teach_StudentTable
    WHERE   FileName = @FileName; 
    DELETE  FROM syEDExpNotPostPell_ACG_SMART_Teach_DisbursementTable
    WHERE   FileName = @FileName; 
        




GO
