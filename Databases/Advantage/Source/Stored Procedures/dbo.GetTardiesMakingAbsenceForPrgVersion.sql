SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[GetTardiesMakingAbsenceForPrgVersion]
    (
     @strGuid UNIQUEIDENTIFIER
    ,@IsPrgVersion BIT
    )
AS
    BEGIN


        DECLARE @isTardiesMaking AS INT; 

        IF ( @IsPrgVersion > 0 )
            BEGIN   

                SET @isTardiesMaking = (
                                         SELECT TOP 1
                                                P.TardiesMakingAbsence
                                         FROM   arStuEnrollments E
                                         INNER JOIN arPrgVersions P ON E.PrgVerId = P.PrgVerId
                                         WHERE  E.StuEnrollId = @strGuid
                                                AND P.TrackTardies = 1
                                       );

                IF @isTardiesMaking IS NULL
                    BEGIN

                        RETURN (ISNULL(@isTardiesMaking,0));

                    END; 

                ELSE
                    RETURN @isTardiesMaking;

            END; 

        ELSE
            BEGIN  

                SET @isTardiesMaking = (
                                         SELECT TOP 1
                                                TardiesMakingAbsence
                                         FROM   arPrgVersions
                                         WHERE  PrgVerId = @strGuid
                                                AND TrackTardies = 1
                                       );

                IF @isTardiesMaking IS NULL
                    BEGIN

                        RETURN (ISNULL(@isTardiesMaking,0));

                    END; 

                ELSE
                    RETURN @isTardiesMaking;

            END;
    END;
 







GO
