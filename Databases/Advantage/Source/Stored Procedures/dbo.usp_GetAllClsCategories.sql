SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [dbo].[usp_GetAllClsCategories]
AS
    SELECT DISTINCT
            CategoryCode
           ,CategoryDesc
           ,isDefault
    FROM    arclassCategories; 
    






GO
