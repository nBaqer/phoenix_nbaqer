SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_GetInstructionTypes]
    @showActiveOnly BIT
   ,@campusId VARCHAR(50)
AS
    SET NOCOUNT ON;
    IF @showActiveOnly = 1
        BEGIN
            SELECT  IT.InstructionTypeId
                   ,IT.InstructionTypeDescrip
                   ,1 AS Status
            FROM    arInstructionType IT
                   ,syStatuses S
            WHERE   IT.StatusId = S.StatusId
                    AND IT.CampGrpId IN ( SELECT    CGC.CampGrpId
                                          FROM      syCmpGrpCmps CGC
                                                   ,syCampGrps CG
                                          WHERE     CGC.CampusId = @campusId
                                                    AND CGC.CampGrpId = CG.CampGrpId )
                    AND S.STATUS = 'Active'
            ORDER BY IT.InstructionTypeDescrip;
        END;
    ELSE
        BEGIN
            SELECT  IT.InstructionTypeId
                   ,IT.InstructionTypeDescrip
                   ,1 AS Status
            FROM    arInstructionType IT
                   ,syStatuses S
            WHERE   IT.StatusId = S.StatusId
                    AND IT.CampGrpId IN ( SELECT    CGC.CampGrpId
                                          FROM      syCmpGrpCmps CGC
                                                   ,syCampGrps CG
                                          WHERE     CGC.CampusId = @campusId
                                                    AND CGC.CampGrpId = CG.CampGrpId )
            ORDER BY IT.InstructionTypeDescrip;
        END;



GO
