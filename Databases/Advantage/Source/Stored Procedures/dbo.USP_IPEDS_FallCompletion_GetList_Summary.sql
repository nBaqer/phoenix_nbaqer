SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--sp_helptext USP_IPEDS_FallCompletion_GetList_Summary
--exec USP_IPEDS_FallCompletion_GetList_Summary '1BB47DF1-E6F9-4467-A5D6-8FA5495BB571',null,'09/01/2009','08/31/2010','test','Student Number'
CREATE PROCEDURE [dbo].[USP_IPEDS_FallCompletion_GetList_Summary]
    @CampusId VARCHAR(50)
   ,@ProgId VARCHAR(4000) = NULL
   ,@StartDate DATETIME = NULL
   ,@EndDate DATETIME = NULL
   ,@DateRangeText VARCHAR(100) = NULL
   ,@OrderBy VARCHAR(100)
AS
    BEGIN
	
        SELECT  RaceSequence
               ,Race
               ,NEWID() AS RowNumber
               ,SUM(MenCount) AS MenCount
               ,SUM(WomenCount) AS WomenCount
        FROM    (
                  SELECT    NEWID() AS RowNumber
                           ,StudentId
                           ,SSN
                           ,StudentNumber
                           ,StudentName
                           ,Men
                           ,Women
                           ,Race
                           ,Gender
                           ,GenderSequence
                           ,RaceSequence
                           ,CASE Men
                              WHEN 'X' THEN 1
                              ELSE 0
                            END AS MenCount
                           ,CASE Women
                              WHEN 'X' THEN 1
                              ELSE 0
                            END AS WomenCount
                           ,EnrollmentId
                  FROM      (
                              --	Select 
--	NULL as StudentId, NULL as SSN, NULL as StudentNumber, NULL as StudentName,'' as Men,'' as Women,
--  Case WHEN IPEDSValue is NULL Then 'Race/ethnicity unknown' else (select Distinct AgencyDescrip 
--                                                                   from syRptAgencyFldValues 
--                                                                   WHERE RptAgencyFldValId=IPEDSValue
--                                                                  ) 
--End as Race,
--	'Men' as Gender, 1 as GenderSequence,IPEDSSequence as RaceSequence
--	from 	
--	adEthCodes where EthCodeId not in
--	(
--		Select distinct t1.Race	
--		from			
--			arStudent t1 INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId  
--			INNER JOIN syStatusCodes t5 on t2.StatusCodeId=t5.StatusCodeId 
--			INNER JOIN sySysStatus t6 on t5.SysStatusId=t6.SysStatusId and t6.SysStatusId not in (7,8)
--			INNER JOIN arPrgVersions t7 on t2.PrgVerId = t7.PrgVerId 
--			INNER JOIN arPrograms t8 on t7.ProgId = t8.ProgId  
--			INNER JOIN arProgTypes t9 on t7.ProgTypId = t9.ProgTypId 
--			--LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
--			INNER JOIN adGenders t11 ON t1.Gender = t11.GenderId 
--		where
--			t2.CampusId= @CampusId and 
--			(@ProgId is null or t8.ProgId in (Select Val from [MultipleValuesForReportParameters](@ProgId,',',1))) 
--			--and	t9.IPEDSValue=59 and
--			--(LOWER(t10.IPEDSValue)=61 or LOWER(t10.IPEDSValue)=62) 
--			and t6.SysStatusId in (14)
--			and (t11.IPEDSValue=31 Or t11.IPEDSValue=30)
--			and t1.Race is not null 
--			--
--			--and t2.StartDate >= @StartDate and t2.StartDate <= @EndDate
--			and t2.ExpGradDate >= @StartDate and t2.ExpGradDate <= @EndDate
--			--
--	)
--	Union
--	Select 
--		t1.StudentId, t1.SSN,
--		t1.StudentNumber,
--		t1.LastName + ', ' + t1.FirstName + ' ' + ISNULL(t1.MiddleName,'') as StudentName,
--		Case when t3.IPEDSValue=30 Then 'X' else '' End as Men,
--		Case when t3.IPEDSValue=31 Then 'X' else '' End as Women,
--		Case WHEN t4.IPEDSValue is NULL Then 'Race/ethnicity unknown' else (select Distinct AgencyDescrip from syRptAgencyFldValues where RptAgencyFldValId=t4.IPEDSValue) End as Race,
--		t3.[IPEDSValue] as Gender,
--		t3.[IPEDSSequence] as GenderSequence,
--		t4.[IPEDSSequence] as RaceSequence
--from
--		adGenders t3 LEFT JOIN arStudent t1 on t3.GenderId=t1.Gender 
--		LEFT JOIN adEthCodes t4 on t4.EthCodeId=t1.Race  
--		INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId  
--		INNER JOIN syStatusCodes t5 on t2.StatusCodeId=t5.StatusCodeId 
--		INNER JOIN sySysStatus t6 on t5.SysStatusId=t6.SysStatusId and t6.SysStatusId not in (7,8)
--		INNER JOIN arPrgVersions t7 on t2.PrgVerId = t7.PrgVerId 
--		INNER JOIN arPrograms t8 on t7.ProgId = t8.ProgId  
--		INNER JOIN arProgTypes t9 on t7.ProgTypId = t9.ProgTypId 
--		LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId 
--where
--	t2.CampusId= @CampusId and 
--	(@ProgId is null or t8.ProgId in (Select Val from [MultipleValuesForReportParameters](@ProgId,',',1))) 
--	--and	t9.IPEDSValue=59 and
--	--(LOWER(t10.IPEDSValue)=61 or LOWER(t10.IPEDSValue)=62) and
--	and t6.SysStatusId in (14)
--	and (t3.IPEDSValue=30 or t3.IPEDSValue=31) and
--	t1.Race is not null
--	--and t2.StartDate >= @StartDate and t2.StartDate <= @EndDate
--	and t2.ExpGradDate >= @StartDate and t2.ExpGradDate <= @EndDate

-- Check if there are any Foreign Male Students who are Full-Time Undergraduates
		-- If there are no foreign students in that category, insert a blank record
		-- with race 'Nonresident Alien'
		-- This race does not exist in adEthCodes anymore 
		-- Needs to be deleted from adEthCodes
                              SELECT TOP 1
                                        NULL AS StudentId
                                       ,NULL AS SSN
                                       ,NULL AS StudentNumber
                                       ,NULL AS StudentName
                                       ,'' AS Men
                                       ,'' AS Women
                                       ,'Nonresident Alien' AS Race
                                       ,NULL AS Gender
                                       ,1 AS GenderSequence
                                       ,1 AS RaceSequence
                                       ,NULL AS EnrollmentId
                              FROM      arStudent
                              WHERE     (
                                          SELECT    COUNT(t1.FirstName)
				--from 
				--	adGenders t3 LEFT JOIN arStudent t1 on t3.GenderId=t1.Gender 
				--	INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
				--	INNER JOIN syStatusCodes t5 on t2.StatusCodeId=t5.StatusCodeId 
				--	INNER JOIN sySysStatus t6 on t5.SysStatusId=t6.SysStatusId and t6.SysStatusId not in (7,8)
				--	Inner Join adCitizenships t11 on t1.Citizen  = t11.CitizenshipId
				--	INNER JOIN arPrgVersions t7 on t2.PrgVerId = t7.PrgVerId 
				--	INNER JOIN arPrograms t8 on t7.ProgId = t8.ProgId   
				--	INNER JOIN arProgTypes t9 on t7.ProgTypId = t9.ProgTypId 
				--	LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId 
				--where  
				--		LTRIM(RTRIM(t2.CampusId))= LTRIM(RTRIM(@CampusId)) and 
				--		t11.IPEDSValue=65 and -- non citizen
				--		t9.IPEDSValue=58 and -- undergraduate
				--		t3.IPEDSValue=30 and --Men
				--		t10.IPEDSValue=61  -- Full Time
				--		and (@ProgId is null or t8.ProgId in (Select Val from [MultipleValuesForReportParameters](@ProgId,',',1))) 
                                          FROM      arStudent t1
                                          INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                                          INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                                          INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                                                       AND t6.SysStatusId NOT IN ( 7,8 )
                                          INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                                          INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                                          INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId 
				--LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                                          INNER JOIN adGenders t11 ON t1.Gender = t11.GenderId
                                          INNER JOIN adCitizenships t12 ON t1.Citizen = t12.CitizenshipId
                                          WHERE     t2.CampusId = @CampusId
                                                    AND (
                                                          @ProgId IS NULL
                                                          OR t8.ProgId IN ( SELECT  Val
                                                                            FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                                        )
                                                    AND t6.SysStatusId IN ( 14 )
                                                    AND (
                                                          t11.IPEDSValue = 31
                                                          OR t11.IPEDSValue = 30
                                                        )
                                                    AND t1.Race IS NOT NULL
                                                    AND t2.ExpGradDate >= @StartDate
                                                    AND t2.ExpGradDate <= @EndDate
                                                    AND t12.IPEDSValue = 65
                                        ) = 0
                              UNION
                              SELECT    t1.StudentId
                                       ,t1.SSN
                                       ,t1.StudentNumber
                                       ,t1.LastName + ', ' + t1.FirstName + ' ' + ISNULL(t1.MiddleName,'') AS StudentName
                                       ,CASE WHEN t3.IPEDSValue = 30 THEN 'X'
                                             ELSE ''
                                        END AS Men
                                       ,CASE WHEN t3.IPEDSValue = 31 THEN 'X'
                                             ELSE ''
                                        END AS Women
                                       ,'Nonresident Alien' AS Race
                                       ,t3.IPEDSValue AS Gender
                                       ,t3.IPEDSSequence AS GenderSequence
                                       ,1 AS RaceSequence
                                       ,NULL AS EnrollmentId
                              FROM      adGenders t3
                              LEFT JOIN arStudent t1 ON t3.GenderId = t1.Gender
                              LEFT JOIN adEthCodes t4 ON t4.EthCodeId = t1.Race
                              INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                              INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                              INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                                           AND t6.SysStatusId NOT IN ( 7,8 )
                              INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                              INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                              INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                              LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                              INNER JOIN adCitizenships t12 ON t1.Citizen = t12.CitizenshipId
                              WHERE     t2.CampusId = @CampusId
                                        AND (
                                              @ProgId IS NULL
                                              OR t8.ProgId IN ( SELECT  Val
                                                                FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                            )
                                        AND t6.SysStatusId IN ( 14 )
                                        AND (
                                              t3.IPEDSValue = 30
                                              OR t3.IPEDSValue = 31
                                            )
                                        AND t1.Race IS NOT NULL
                                        AND t2.ExpGradDate >= @StartDate
                                        AND t2.ExpGradDate <= @EndDate
                                        AND t12.IPEDSValue = 65
                              UNION
                              SELECT    NULL AS StudentId
                                       ,NULL AS SSN
                                       ,NULL AS StudentNumber
                                       ,NULL AS StudentName
                                       ,'' AS Men
                                       ,'' AS Women
                                       ,CASE WHEN IPEDSValue IS NULL THEN 'Race/ethnicity unknown'
                                             ELSE (
                                                    SELECT DISTINCT
                                                            AgencyDescrip
                                                    FROM    syRptAgencyFldValues
                                                    WHERE   RptAgencyFldValId = IPEDSValue
                                                  )
                                        END AS Race
                                       ,NULL AS Gender
                                       ,1 AS GenderSequence
                                       ,IPEDSSequence AS RaceSequence
                                       ,NULL AS EnrollmentId
                              FROM      adEthCodes
                              WHERE     EthCodeDescrip <> 'Nonresident Alien'
                                        AND EthCodeId NOT IN ( SELECT DISTINCT
                                                                        t1.Race
                                                               FROM     arStudent t1
                                                               INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                                                               INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                                                               INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                                                                            AND t6.SysStatusId NOT IN ( 7,8 )
                                                               INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                                                               INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                                                               INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId 
			--LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                                                               INNER JOIN adGenders t11 ON t1.Gender = t11.GenderId
                                                               INNER JOIN adCitizenships t12 ON t1.Citizen = t12.CitizenshipId
                                                               WHERE    t2.CampusId = @CampusId
                                                                        AND (
                                                                              @ProgId IS NULL
                                                                              OR t8.ProgId IN ( SELECT  Val
                                                                                                FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                                                            )
                                                                        AND t6.SysStatusId IN ( 14 )
                                                                        AND (
                                                                              t11.IPEDSValue = 31
                                                                              OR t11.IPEDSValue = 30
                                                                            )
                                                                        AND t1.Race IS NOT NULL
                                                                        AND t2.ExpGradDate >= @StartDate
                                                                        AND t2.ExpGradDate <= @EndDate
                                                                        AND t12.IPEDSValue <> 65 )
                              UNION
                              SELECT    t1.StudentId
                                       ,t1.SSN
                                       ,t1.StudentNumber
                                       ,t1.LastName + ', ' + t1.FirstName + ' ' + ISNULL(t1.MiddleName,'') AS StudentName
                                       ,CASE WHEN t3.IPEDSValue = 30 THEN 'X'
                                             ELSE ''
                                        END AS Men
                                       ,CASE WHEN t3.IPEDSValue = 31 THEN 'X'
                                             ELSE ''
                                        END AS Women
                                       ,CASE WHEN t4.IPEDSValue IS NULL THEN 'Race/ethnicity unknown'
                                             ELSE (
                                                    SELECT DISTINCT
                                                            AgencyDescrip
                                                    FROM    syRptAgencyFldValues
                                                    WHERE   RptAgencyFldValId = t4.IPEDSValue
                                                  )
                                        END AS Race
                                       ,t3.IPEDSValue AS Gender
                                       ,t3.IPEDSSequence AS GenderSequence
                                       ,t4.IPEDSSequence AS RaceSequence
                                       ,t2.EnrollmentId AS EnrollmentId
                              FROM      adGenders t3
                              LEFT JOIN arStudent t1 ON t3.GenderId = t1.Gender
                              LEFT JOIN adEthCodes t4 ON t4.EthCodeId = t1.Race
                              INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                              INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                              INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                                           AND t6.SysStatusId NOT IN ( 7,8 )
                              INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                              INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                              INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                              LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                              INNER JOIN adCitizenships t12 ON t1.Citizen = t12.CitizenshipId
                              WHERE     t2.CampusId = @CampusId
                                        AND (
                                              @ProgId IS NULL
                                              OR t8.ProgId IN ( SELECT  Val
                                                                FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                            )
                                        AND t6.SysStatusId IN ( 14 )
                                        AND (
                                              t3.IPEDSValue = 30
                                              OR t3.IPEDSValue = 31
                                            )
                                        AND t1.Race IS NOT NULL
                                        AND t2.ExpGradDate >= @StartDate
                                        AND t2.ExpGradDate <= @EndDate
                                        AND t12.IPEDSValue <> 65
                            ) dt 
	-- Full Time Graduate --
	--Order by 
	--GenderSequence, RaceSequence,
	--Case when @OrderBy='SSN' Then SSN end,
	--Case when @OrderBy='LastName' Then StudentName end,
	--Case When @OrderBy='StudentNumber' Then Convert(int,StudentNumber) end
                ) dt2
        GROUP BY RaceSequence
               ,Race
        ORDER BY RaceSequence; 
    END;

GO
