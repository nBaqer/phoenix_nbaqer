SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- =========================================================================================================
-- USP_TR_Sub03_StudentInfo 
-- =========================================================================================================
CREATE PROCEDURE [dbo].[USP_TR_Sub03_StudentInfo]
    @StuEnrollIdList NVARCHAR(MAX) = NULL
   ,@ShowMultipleEnrollments BIT = 0
   ,@StatusList NVARCHAR(MAX) = NULL
   ,@OrderBy NVARCHAR(1000) = NULL
AS
    BEGIN
        DECLARE @CoursesTakenTableName NVARCHAR(200);
        DECLARE @GPASummaryTableName NVARCHAR(200);
        -- StuEnrollIdList plus double Enrollments
        DECLARE @StuEnrollIdListWithDoubleEnrollments NVARCHAR(MAX);

        CREATE TABLE #Temp1
            (
                StudentId NVARCHAR(50)
               ,EnrollAcademicType NVARCHAR(50)
               ,StuEnrollIdList NVARCHAR(MAX)
            );
        CREATE TABLE #Temp2
            (
                StudentId UNIQUEIDENTIFIER
               ,SSN NVARCHAR(50)
               ,StuFirstName NVARCHAR(50)
               ,StuLastName NVARCHAR(50)
               ,StuMiddleName NVARCHAR(50)
               ,StuDOB DATETIME
               ,StudentNumber NVARCHAR(50)
               ,StuEmail NVARCHAR(50)
               ,StuAddress1 NVARCHAR(250)
               ,StuAddress2 NVARCHAR(250)
               ,StuCity NVARCHAR(250)
               ,StuStateDescrip NVARCHAR(100)
               ,StuZIP NVARCHAR(50)
               ,StuCountryDescrip NVARCHAR(50)
               ,StuPhone NVARCHAR(50)
               ,EnrollAcademicType NVARCHAR(50)
               ,StuEnrollIdList NVARCHAR(MAX)
            );

        IF @ShowMultipleEnrollments = 0
            BEGIN
                SET @StatusList = NULL;

                INSERT INTO #Temp1
                            SELECT     ASE.StudentId
                                      ,CASE WHEN (
                                                 APV.Credits > 0.0
                                                 AND APV.Hours > 0
                                                 )
                                                 OR (APV.IsContinuingEd = 1) THEN 'Credits-ClockHours'
                                            WHEN APV.Credits > 0.0 THEN 'Credits'
                                            WHEN APV.Hours > 0.0 THEN 'ClockHours'
                                            ELSE ''
                                       END AcademicType
                                      ,CONVERT(NVARCHAR(50), ASE.StuEnrollId) AS StuEnrollIdList
                            FROM       arStuEnrollments ASE
                            INNER JOIN syStatusCodes SC ON SC.StatusCodeId = ASE.StatusCodeId
                            INNER JOIN arStudent AS AST ON AST.StudentId = ASE.StudentId
                            -- To get the Academic Type for each selected enrollment we go to Program version here
                            INNER JOIN arPrgVersions AS APV ON APV.PrgVerId = ASE.PrgVerId
                            WHERE      (
                                       @StuEnrollIdList IS NULL
                                       OR ASE.StuEnrollId IN (
                                                             SELECT Val
                                                             FROM   MultipleValuesForReportParameters(@StuEnrollIdList, ',', 1)
                                                             )
                                       )
                            ORDER BY   ASE.StudentId
                                      ,ASE.StuEnrollId;
            END;
        ELSE
            BEGIN
                -- if the parameter ShowMultipleEnrollmnets is True we will update the table #Temp1 with all enrollments for student selected
                INSERT INTO #Temp1
                            SELECT     ASE.StudentId
                                      ,CASE WHEN (
             APV.Credits > 0.0
                                                 AND APV.Hours > 0
                                                 )
                                                 OR (APV.IsContinuingEd = 1) THEN 'Credits-ClockHours'
                                            WHEN APV.Credits > 0.0 THEN 'Credits'
                                            WHEN APV.Hours > 0.0 THEN 'ClockHours'
                                            ELSE ''
                                       END AcademicType
                                      ,CONVERT(NVARCHAR(50), ASE.StuEnrollId) AS StuEnrollIdList
                            FROM       arStuEnrollments ASE
                            INNER JOIN syStatusCodes SC ON SC.StatusCodeId = ASE.StatusCodeId
                            INNER JOIN arStudent AS AST ON AST.StudentId = ASE.StudentId
                            -- To get the Academic Type for each selected enrollment we go to Program version here
                            INNER JOIN arPrgVersions AS APV ON APV.PrgVerId = ASE.PrgVerId
                            WHERE      ASE.StudentId IN (
                                                        SELECT     DISTINCT AST.StudentId
                                                        FROM       arStudent AS AST
                                                        INNER JOIN arStuEnrollments AS ASE1 ON ASE1.StudentId = AST.StudentId
                                                        WHERE      (
                                                                   @StuEnrollIdList IS NULL
                                                                   OR ASE1.StuEnrollId IN (
                                                                                          SELECT Val
                                                                                          FROM   MultipleValuesForReportParameters(@StuEnrollIdList, ',', 1)
                                                                                          )
                                                                   )
                                                        )
                            ORDER BY   ASE.StudentId
                                      ,ASE.StuEnrollId;
            END;

        IF (@ShowMultipleEnrollments = 1)
            -- if the parameter ShowMultipleEnrollmnets is True we will update the table #Temp1 with all enrollments for student selected
            -- but T1 table must be filtered for unique studentId
            BEGIN
                -- Create stuEnrollIdList for all student selected
                UPDATE     T4
                SET        T4.StuEnrollIdList = T2.List
                FROM       #Temp1 AS T4
                INNER JOIN (
                           SELECT      T1.StudentId
                                      ,T1.EnrollAcademicType
                                      ,LEFT(T3.List, LEN(T3.List) - 1) AS List
                           FROM        #Temp1 AS T1
                           CROSS APPLY (
                                       SELECT CONVERT(VARCHAR(50), T2.StuEnrollIdList) + ','
                                       FROM   #Temp1 AS T2
                                       WHERE  T2.StudentId = T1.StudentId
                                              AND T2.EnrollAcademicType = T1.EnrollAcademicType
                                       FOR XML PATH('')
                                       ) T3(List)
                           ) T2 ON T2.StudentId = T4.StudentId
                                   AND T2.EnrollAcademicType = T4.EnrollAcademicType;

            END;

        SELECT @StuEnrollIdListWithDoubleEnrollments = COALESCE(@StuEnrollIdListWithDoubleEnrollments + ', ', '') + T1.StuEnrollIdList
        FROM   #Temp1 AS T1;

        EXECUTE dbo.USP_TR_Sub03_PrepareGPA @StuEnrollIdList = @StuEnrollIdListWithDoubleEnrollments
                     ,@ShowMultipleEnrollments = @ShowMultipleEnrollments
                                           ,@CoursesTakenTableName = @CoursesTakenTableName OUTPUT
                                           ,@GPASummaryTableName = @GPASummaryTableName OUTPUT;


        INSERT INTO #Temp2 (
                           StudentId
                          ,SSN
                          ,StuFirstName
                          ,StuLastName
                          ,StuMiddleName
                          ,StuDOB
                          ,StudentNumber
                          ,StuEmail
                          ,StuAddress1
                          ,StuAddress2
                          ,StuCity
                          ,StuStateDescrip
                          ,StuZIP
                          ,StuCountryDescrip
                          ,StuPhone
                          ,EnrollAcademicType
                          ,StuEnrollIdList
                           )
                    SELECT          AST.StudentId AS StudentId
                                   ,AST.SSN AS SSN
                                   ,AST.FirstName AS StuFirstName
                                   ,AST.LastName AS StuLastName
                                   ,AST.MiddleName AS StuMiddleName
                                   ,AST.DOB AS StuDOB
                                   ,AST.StudentNumber AS StudentNumber
                                   ,AST.HomeEmail AS StuEmail
                                   ,ASA.Address1 AS StuAddress1
                                   ,ASA.Address2 AS StuAddress2
                                   ,ASA.City AS StuCity
                                   ,ASA.StateDescrip AS StuStateDescrip
                                   ,ASA.Zip AS StuZIP
                                   ,ASA.CountryDescrip AS StuCountryDescrip
                                   ,ASP.Phone AS StuPhone
                                   ,T.EnrollAcademicType
                                   ,T.StuEnrollIdList
                    FROM            arStudent AS AST
                    INNER JOIN      (
                                    SELECT DISTINCT T1.StudentId
                                          ,T1.EnrollAcademicType
                                          ,T1.StuEnrollIdList
                                    FROM   #Temp1 AS T1
                                    ) AS T ON T.StudentId = AST.StudentId
                    LEFT OUTER JOIN (
                                    SELECT          ASA1.StudentId
                                                   ,ASA1.Address1
                                                   ,ASA1.Address2
                                                   ,ASA1.City
                                                   ,SS2.StateDescrip
                                                   ,ASA1.Zip
                                                   ,AC.CountryDescrip
                                                   ,SS.Status
                                    FROM            arStudAddresses AS ASA1
                                    INNER JOIN      syStatuses AS SS ON SS.StatusId = ASA1.StatusId
                                    LEFT OUTER JOIN adCountries AS AC ON AC.CountryId = ASA1.CountryId
                                    LEFT OUTER JOIN syStates AS SS2 ON SS2.StateId = ASA1.StateId
                                    WHERE           SS.Status = 'Active'
                                                    AND ASA1.default1 = 1
                                    ) AS ASA ON ASA.StudentId = AST.StudentId
                    LEFT OUTER JOIN (
                                    SELECT     ASP1.StudentId
                                              ,ASP1.Phone
                                              ,ASP1.StatusId
                                              ,SS1.Status
FROM       arStudentPhone AS ASP1
                                    INNER JOIN syStatuses AS SS1 ON SS1.StatusId = ASP1.StatusId
                                    WHERE      SS1.Status = 'Active'
                                               AND ASP1.default1 = 1
                                    ) AS ASP ON ASP.StudentId = AST.StudentId;

        -- SELECT AND SORT     
        SELECT   StudentId
                ,SSN
                ,StuFirstName
                ,StuLastName
                ,StuMiddleName
                ,StuDOB
                ,StudentNumber
                ,StuEmail
                ,StuAddress1
                ,StuAddress2
                ,StuCity
                ,StuStateDescrip
                ,StuZIP
                ,StuCountryDescrip
                ,StuPhone
                ,EnrollAcademicType
                ,StuEnrollIdList
                ,@CoursesTakenTableName AS CoursesTakenTableName
                ,@GPASummaryTableName AS GPASummaryTableName
        FROM     #Temp2
        ORDER BY CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'LastName Asc,FirstName Asc,MiddleName Asc' THEN (RANK() OVER (ORDER BY StuLastName
                                                                                                                           ,StuFirstName
                                                                                                                           ,StuMiddleName
                                                                                                                  )
                                                                                                     )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'LastName Asc,FirstName Asc,MiddleName Desc' THEN (RANK() OVER (ORDER BY StuLastName
                                                                                                                            ,StuFirstName
                                                                                                                            ,StuMiddleName DESC
                                                                                                                   )
                                                                                                      )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'LastName Asc,FirstName Desc,MiddleName Asc' THEN (RANK() OVER (ORDER BY StuLastName
                                                                                                                            ,StuFirstName DESC
                                                                                                                            ,StuMiddleName
                                                                                                                   )
                                                                                                      )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'LastName Asc,FirstName Desc,MiddleName Desc' THEN (RANK() OVER (ORDER BY StuLastName
                                                                                                                             ,StuFirstName DESC
                                                                                                                             ,StuMiddleName DESC
                                                                                                                    )
                                                                                                       )
                 END
                --LD
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'LastName Desc,FirstName Asc,MiddleName Asc' THEN (RANK() OVER (ORDER BY StuLastName DESC
    ,StuFirstName
                                                                                                                            ,StuMiddleName
                                                                                                                   )
                                                                                                      )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'LastName Desc,FirstName Asc,MiddleName Desc' THEN (RANK() OVER (ORDER BY StuLastName DESC
                                                                                                                             ,StuFirstName
                                                                                                                             ,StuMiddleName DESC
                                                                                                                    )
                                                                                                       )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'LastName Desc,FirstName Desc,MiddleName Asc' THEN (RANK() OVER (ORDER BY StuLastName DESC
                                                                                                                             ,StuFirstName DESC
                                                                                                                             ,StuMiddleName
                                                                                                                    )
                                                                                                       )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'LastName Desc,FirstName Desc,MiddleName Desc' THEN (RANK() OVER (ORDER BY StuLastName DESC
                                                                                                                              ,StuFirstName DESC
                                                                                                                              ,StuMiddleName DESC
                                                                                                                     )
                                                                                                        )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'LastName Asc,MiddleName Asc,FirstName Asc' THEN (RANK() OVER (ORDER BY StuLastName ASC
                                                                                                                           ,StuMiddleName ASC
                                                                                                                           ,StuFirstName ASC
                                                                                                                  )
                                                                                                     )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'LastName Asc,MiddleName Asc,FirstName Desc' THEN (RANK() OVER (ORDER BY StuLastName ASC
                                                                                                                            ,StuMiddleName ASC
                                                                                                                            ,StuFirstName DESC
                                                                                                                   )
                                                                                                      )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'LastName Asc,MiddleName Desc,FirstName Asc' THEN (RANK() OVER (ORDER BY StuLastName ASC
                                                       ,StuMiddleName DESC
                                                                                                                            ,StuFirstName ASC
                                                                                                                   )
                                                                                                      )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'LastName Asc,MiddleName Desc,FirstName Desc' THEN (RANK() OVER (ORDER BY StuLastName ASC
                                                                                                                             ,StuMiddleName DESC
                                                                                                                             ,StuFirstName DESC
                                                                                                                    )
                                                                                                       )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'LastName Desc,MiddleName Asc,FirstName Asc' THEN (RANK() OVER (ORDER BY StuLastName DESC
                                                                                                                            ,StuMiddleName ASC
                                                                                                                            ,StuFirstName ASC
                                                                                                                   )
                                                                                                      )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'LastName Desc,MiddleName Asc,FirstName Desc' THEN (RANK() OVER (ORDER BY StuLastName DESC
                                                                                                                             ,StuMiddleName ASC
                                                                                                                             ,StuFirstName DESC
                                                                                                                    )
                                                                                                       )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'LastName Desc,MiddleName Desc,FirstName Asc' THEN (RANK() OVER (ORDER BY StuLastName DESC
                                                                                                                             ,StuMiddleName DESC
                                                                                                                             ,StuFirstName ASC
                                                                                                                    )
                                                                                                       )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'LastName Desc,MiddleName Desc,FirstName Desc' THEN (RANK() OVER (ORDER BY StuLastName DESC
                                                                                                                              ,StuMiddleName DESC
                                                                                                                              ,StuFirstName DESC
                                                                                                                     )
                                                                                                        )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'FirstName Asc,LastName Asc,MiddleName Asc' THEN (RANK() OVER (ORDER BY StuFirstName ASC
                                                                                                                           ,StuLastName ASC
                                                                                                                           ,StuMiddleName ASC
                                                                                                                  )
                                                                                                     )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'FirstName Asc,LastName Asc,MiddleName Desc' THEN (RANK() OVER (ORDER BY StuFirstName ASC
                                                                                                                            ,StuLastName ASC
                                                                                                                            ,StuMiddleName DESC
                                                                                                                   )
                                                                                                      )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'FirstName Asc,LastName Desc,MiddleName Asc' THEN (RANK() OVER (ORDER BY StuFirstName ASC
                                                                                                                            ,StuLastName DESC
                                                                                                                            ,StuMiddleName ASC
                                                                                                                   )
                                                                                                      )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'FirstName Asc,LastName Desc,MiddleName Desc' THEN (RANK() OVER (ORDER BY StuFirstName ASC
                                                                                                                             ,StuLastName DESC
                                                                                                                             ,StuMiddleName DESC
                                                                                                                    )
                                                                                                       )
                 END
                --LD
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'FirstName Desc,LastName Asc,MiddleName Asc' THEN (RANK() OVER (ORDER BY StuFirstName DESC
                                                                                                                            ,StuLastName ASC
                                                                                                                            ,StuMiddleName ASC
                                                                                                                   )
                                                                                                      )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'FirstName Desc,LastName Asc,MiddleName Desc' THEN (RANK() OVER (ORDER BY StuFirstName DESC
                                                                                                                             ,StuLastName ASC
                                                                                                                             ,StuMiddleName DESC
                                                                                                                    )
                                                                                                       )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'FirstName Desc,LastName Desc,MiddleName Asc' THEN (RANK() OVER (ORDER BY StuFirstName DESC
                                                                                                                             ,StuLastName DESC
                                                                                                                             ,StuMiddleName ASC
                                                                                                                    )
                                                                                                       )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'FirstName Desc,LastName Desc,StuMiddleName Desc' THEN (RANK() OVER (ORDER BY StuFirstName DESC
                                                                                                                                 ,StuLastName DESC
                                                                                                                                 ,StuMiddleName DESC
                                                                                                                        )
                                                                                                           )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'FirstName Asc,MiddleName Asc,LastName Asc' THEN (RANK() OVER (ORDER BY StuFirstName ASC
                                                                                                                           ,StuMiddleName ASC
                                                                                                                           ,StuLastName ASC
                                                                                                                  )
                                                                                                     )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'FirstName Asc,MiddleName Asc,LastName Desc' THEN (RANK() OVER (ORDER BY StuFirstName ASC
                                                                                                                            ,StuMiddleName ASC
                                                                                                                            ,StuLastName DESC
                                                                                                                   )
                                                                                                      )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'FirstName Asc,MiddleName Desc,LastName Asc' THEN (RANK() OVER (ORDER BY StuFirstName ASC
                                                                                                                            ,StuMiddleName DESC
                                                                                                                            ,StuLastName ASC
                                                                                                                   )
                                                                                                      )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'FirstName Asc,MiddleName Desc,LastName Desc' THEN (RANK() OVER (ORDER BY StuFirstName ASC
                                                                                                                             ,StuMiddleName DESC
                                                                                                                             ,StuLastName DESC
                                                                                                                    )
     )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'FirstName Desc,MiddleName Asc,LastName Asc' THEN (RANK() OVER (ORDER BY StuFirstName DESC
                                                                                                                            ,StuMiddleName ASC
                                                                                                                            ,StuLastName ASC
                                                                                                                   )
                                                                                                      )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'FirstName Desc,MiddleName Asc,LastName Desc' THEN (RANK() OVER (ORDER BY StuFirstName DESC
                                                                                                                             ,StuMiddleName ASC
                                                                                                                             ,StuLastName DESC
                                                                                                                    )
                                                                                                       )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'FirstName Desc,MiddleName Desc,LastName Asc' THEN (RANK() OVER (ORDER BY StuFirstName DESC
                                                                                                                             ,StuMiddleName DESC
                                                                                                                             ,StuLastName ASC
                                                                                                                    )
                                                                                                       )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'FirstName Desc,MiddleName Desc,LastName Desc' THEN (RANK() OVER (ORDER BY StuFirstName DESC
                                                                                                                              ,StuMiddleName DESC
                                                                                                                              ,StuLastName DESC
                                                                                                                     )
                                                                                                        )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'MiddleName Asc,LastName Asc,FirstName Asc' THEN (RANK() OVER (ORDER BY StuMiddleName ASC
                                                                                                                           ,StuLastName ASC
                                                                                                                           ,StuFirstName ASC
                                                                                                                  )
                                                                                                     )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'MiddleName Asc,LastName Asc,FirstName Desc' THEN (RANK() OVER (ORDER BY StuMiddleName ASC
                                                                                                                            ,StuLastName ASC
                                                                                                                            ,StuFirstName DESC
                                                                                                                   )
                                                                         )
                 END

                -- Start here
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'MiddleName Asc,LastName Desc,FirstName Asc' THEN (RANK() OVER (ORDER BY StuMiddleName ASC
                                                                                                                            ,StuLastName DESC
                                                                                                                            ,StuFirstName ASC
                                                                                                                   )
                                                                                                      )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'MiddleName Asc,LastName Desc,FirstName Desc' THEN (RANK() OVER (ORDER BY StuMiddleName ASC
                                                                                                                             ,StuLastName DESC
                                                                                                                             ,StuFirstName DESC
                                                                                                                    )
                                                                                                       )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'MiddleName Desc,LastName Asc,FirstName Asc' THEN (RANK() OVER (ORDER BY StuMiddleName DESC
                                                                                                                            ,StuLastName ASC
                                                                                                                            ,StuFirstName ASC
                                                                                                                   )
                                                                                                      )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'MiddleName Desc,LastName Asc,FirstName Desc' THEN (RANK() OVER (ORDER BY StuMiddleName DESC
                                                                                                                             ,StuLastName ASC
                                                                                                                             ,StuFirstName DESC
                                                                                                                    )
                                                                                                       )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'MiddleName Desc,LastName Desc,FirstName Asc' THEN (RANK() OVER (ORDER BY StuMiddleName DESC
                                                                                                                             ,StuLastName DESC
                                                                                                                             ,StuFirstName ASC
                                                                                                                    )
                                                                                                       )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'MiddleName Desc,LastName Desc,FirstName Desc' THEN (RANK() OVER (ORDER BY StuMiddleName DESC
                                                                                                                              ,StuLastName DESC
                                                                                                                              ,StuFirstName DESC
                                                                               )
                                                                                                        )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'MiddleName Asc,FirstName Asc,LastName Asc' THEN (RANK() OVER (ORDER BY StuMiddleName ASC
                                                                                                                           ,StuFirstName ASC
                                                                                                                           ,StuLastName ASC
                                                                                                                  )
                                                                                                     )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'MiddleName Asc,FirstName Asc,LastName Desc' THEN (RANK() OVER (ORDER BY StuMiddleName ASC
                                                                                                                            ,StuFirstName ASC
                                                                                                                            ,StuLastName DESC
                                                                                                                   )
                                                                                                      )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'MiddleName Asc,FirstName Desc,LastName Asc' THEN (RANK() OVER (ORDER BY StuMiddleName ASC
                                                                                                                            ,StuFirstName DESC
                                                                                                                            ,StuLastName ASC
                                                                                                                   )
                                                                                                      )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'MiddleName Asc,FirstName Desc,LastName Desc' THEN (RANK() OVER (ORDER BY StuMiddleName ASC
                                                                                                                             ,StuFirstName DESC
                                                                                                                             ,StuLastName DESC
                                                                                                                    )
                                                                                                       )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'MiddleName Desc,FirstName Asc,LastName Asc' THEN (RANK() OVER (ORDER BY StuMiddleName DESC
                                                                                                                            ,StuFirstName ASC
                                                                                                                            ,StuLastName ASC
                                                                                                                   )
                                                                                                      )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'MiddleName Desc,FirstName Asc,LastName Desc' THEN (RANK() OVER (ORDER BY StuMiddleName DESC
                                                                                                                             ,StuFirstName ASC
 ,StuLastName DESC
                                                                                                                    )
                                                                                                       )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'MiddleName Desc,FirstName Desc,LastName Asc' THEN (RANK() OVER (ORDER BY StuMiddleName DESC
                                                                                                                             ,StuFirstName DESC
                                                                                                                             ,StuLastName ASC
                                                                                                                    )
                                                                                                       )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'MiddleName Desc,FirstName Desc,LastName Desc' THEN (RANK() OVER (ORDER BY StuMiddleName DESC
                                                                                                                              ,StuFirstName DESC
                                                                                                                              ,StuLastName DESC
                                                                                                                     )
                                                                                                        )
                 END
                ,StudentId;
    END;
-- =========================================================================================================
-- END  --  USP_TR_Sub03_StudentInfo 
-- =========================================================================================================

GO
