SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_GetActiveAndInActiveLeadGroups]
    @campusId UNIQUEIDENTIFIER
AS
    SET NOCOUNT ON;
    SELECT  L.Descrip
           ,L.LeadGrpId
           ,L.StatusId
           ,( CASE ST.Status
                WHEN 'Active' THEN 1
                ELSE 0
              END ) AS Status
    FROM    adLeadGroups L
           ,syStatuses ST
    WHERE   L.StatusId = ST.StatusId
            AND L.UseForScheduling = 1
            AND L.CampGrpId IN ( SELECT DISTINCT
                                        CampGrpId
                                 FROM   syCmpGrpCmps
                                 WHERE  CampusId = @campusId
                                        AND CampGrpId NOT IN ( SELECT DISTINCT
                                                                        CampGrpId
                                                               FROM     syCampGrps
                                                               WHERE    CampGrpDescrip = 'ALL' )
                                 UNION
                                 SELECT DISTINCT
                                        CampGrpId
                                 FROM   syCampGrps
                                 WHERE  CampGrpDescrip = 'ALL' )
    ORDER BY Status
           ,L.Descrip; 




GO
