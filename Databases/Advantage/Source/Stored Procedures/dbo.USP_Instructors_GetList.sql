SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[USP_Instructors_GetList]
    @CampusId UNIQUEIDENTIFIER
AS
    SELECT DISTINCT
            s1.fullname AS FullName
           ,s1.UserId AS UserId
    FROM    syUsers s1
           ,syUsersRolesCampGrps s2
           ,syRoles s3
           ,sySysRoles s4
           ,syCmpGrpCmps s5
           ,syStatuses s6
    WHERE   s1.UserId = s2.UserId
            AND s2.RoleId = s3.RoleId
            AND s3.SysRoleId = s4.SysRoleId
            AND s2.CampGrpId = s5.CampGrpId
            AND s4.StatusId = s6.StatusId
            AND s5.CampusId = @CampusId
            AND s1.AccountActive = 1
            AND s4.SysRoleId = 2
            AND s6.Status = 'Active'
    ORDER BY fullname;

--select * from syResources where ResourceId=394
--
----select * from syResources where resourceId > 610
--select * from syNavigationNOdes where HierarchyId='6EFF3486-9707-4EBB-B758-A1C6D86B406C'
--select * from syNavigationNodes where ResourceId=543
--delete from arPostScoreDictationSpeedTest




GO
