SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
   
CREATE PROCEDURE [dbo].[usp_SA_GetAppliedPaymentsReport]
    @CampGrpId AS VARCHAR(8000)
   ,@PrgVerId AS VARCHAR(8000) = NULL
   ,@ChargeCode AS VARCHAR(8000) = NULL
   ,@EnrollmentStatus AS VARCHAR(8000) = NULL
   ,@StartDate AS DATETIME
   ,@EndDate AS DATETIME
   ,@StudentIdentifier AS VARCHAR(50)
AS /*----------------------------------------------------------------------------------------------------    
    Author : Vijay Ramteke    
        
    Create date : 09/24/2010    
        
    Procedure Name : usp_SA_GetAppliedPaymentsReport    
    
    Objective : Get Applied Payments Details For Students    
        
    Parameters : Name Type Data Type Required?     
        
    Output : Returns Applied Payments Reports DataSet For Students    
                            
*/-----------------------------------------------------------------------------------------------------    
    
    BEGIN    
    
        SELECT  StudentName
               ,LastName
               ,FirstName
               ,MiddleName
               ,StudentIdentifier
               ,PrgVerDescrip
               ,TransDate
               ,ChagreCode
               ,PaymentAmount
               ,AppliedPayment
               ,CampDescrip
               ,StatusCodeDescrip
               ,CampGrpDescrip
        FROM    (
                  SELECT    '' AS StudentName
                           ,S.LastName
                           ,S.FirstName
                           ,S.MiddleName
                           ,CASE @StudentIdentifier
                              WHEN 'StudentId' THEN S.StudentNumber
                              WHEN 'EnrollmentId' THEN SE.EnrollmentId
                              ELSE S.SSN
                            END AS StudentIdentifier
                           ,PV.PrgVerDescrip
                           ,T.TransDate
                           ,TC.TransCodeDescrip AS ChagreCode
                           ,T.TransAmount AS PaymentAmount
                           ,T.TransAmount AS AppliedPayment
                           ,T.TransactionId
                           ,SE.CampusId
                           ,(
                              SELECT    C.CampDescrip
                              FROM      syCampuses C
                              WHERE     C.CampusId = SE.CampusId
                            ) AS CampDescrip
                           ,SC.StatusCodeDescrip
                           ,CG.CampGrpDescrip
                           ,T.TransReference
                  FROM      arStudent S
                  INNER JOIN arStuEnrollments SE ON S.StudentId = SE.StudentId
                  INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                  INNER JOIN saTransactions T ON SE.StuEnrollId = T.StuEnrollId
                  LEFT OUTER JOIN saTransCodes TC ON TC.TransCodeId = T.TransCodeId
                  INNER JOIN syStatusCodes SC ON SE.StatusCodeId = SC.StatusCodeId
                  INNER JOIN syCampuses C ON C.CampusId = SE.CampusId
                  INNER JOIN syCmpGrpCmps CGC ON C.CampusId = CGC.CampusId
                  INNER JOIN syCampGrps CG ON CGC.CampGrpId = CG.CampGrpId
                  WHERE     T.TransTypeId = 2
                            AND T.Voided = 0
                            AND T.TransCodeId IS NOT NULL
                            AND SE.CampusId IN ( SELECT DISTINCT
                                                        CGC.CampusId
                                                 FROM   syCmpGrpCmps CGC
                                                 WHERE  CGC.CampGrpId IN ( SELECT   strval
                                                                           FROM     dbo.SPLIT(@CampGrpId) ) )
                            AND (
                                  @PrgVerId IS NULL
                                  OR SE.PrgVerId IN ( SELECT    strval
                                                      FROM      dbo.SPLIT(@PrgVerId) )
                                )
                            AND (
                                  TC.TransCodeId IN ( SELECT    strval
                                                      FROM      dbo.SPLIT(@ChargeCode) )
                                  OR @ChargeCode IS NULL
                                )
                            AND (
                                  @EnrollmentStatus IS NULL
                                  OR SE.StatusCodeId IN ( SELECT    strval
                                                          FROM      dbo.SPLIT(@EnrollmentStatus) )
                                )
                            AND T.TransDate >= @StartDate
                            AND T.TransDate <= @EndDate    
-- New Code Added By Vijay Ramteke On January 27, 2011 --    
                            AND TC.SysTransCodeId NOT IN ( 11,12,13 )    
-- New Code Added By Vijay Ramteke On January 27, 2011 --    
                  UNION
                  SELECT    '' AS StudentName
                           ,S.LastName
                           ,S.FirstName
                           ,S.MiddleName
                           ,CASE @StudentIdentifier
                              WHEN 'StudentId' THEN S.StudentNumber
                              WHEN 'EnrollmentId' THEN SE.EnrollmentId
                              ELSE S.SSN
                            END AS StudentIdentifier
                           ,PV.PrgVerDescrip
                           ,T.TransDate
                           ,TC.TransCodeDescrip AS ChagreCode
                           ,T.TransAmount AS PaymentAmount
                           ,AP.Amount AS AppliedPayment
                           ,T.TransactionId
                           ,SE.CampusId
                           ,(
                              SELECT    C.CampDescrip
                              FROM      syCampuses C
                              WHERE     C.CampusId = SE.CampusId
                            ) AS CampDescrip
                           ,SC.StatusCodeDescrip
                           ,CG.CampGrpDescrip
                           ,T.TransReference
                  FROM      arStudent S
                           ,arStuEnrollments SE
                           ,arPrgVersions PV
                           ,saTransactions T
                           ,saAppliedPayments AP
                           ,saTransactions TI
                           ,saTransCodes TC
                           ,syStatusCodes SC
                           ,syCampuses C
                           ,syCampGrps CG
                           ,syCmpGrpCmps CGC
                  WHERE     S.StudentId = SE.StudentId
                            AND SE.PrgVerId = PV.PrgVerId
                            AND SE.StuEnrollId = T.StuEnrollId
                            AND SE.StatusCodeId = SC.StatusCodeId
                            AND C.CampusId = CGC.CampusId
                            AND CGC.CampGrpId = CG.CampGrpId
                            AND C.CampusId = SE.CampusId
                            AND T.TransTypeId = 2
                            AND T.Voided = 0
                            AND AP.TransactionId = T.TransactionId
                            AND AP.ApplyToTransId = TI.TransactionId
                            AND TI.TransCodeId = TC.TransCodeId
                            AND T.TransCodeId IS NULL
                            AND SE.CampusId IN ( SELECT DISTINCT
                                                        CGC.CampusId
                                                 FROM   syCmpGrpCmps CGC
                                                 WHERE  CGC.CampGrpId IN ( SELECT   strval
                                                                           FROM     dbo.SPLIT(@CampGrpId) ) )
                            AND (
                                  @PrgVerId IS NULL
                                  OR SE.PrgVerId IN ( SELECT    strval
                                                      FROM      dbo.SPLIT(@PrgVerId) )
                                )
                            AND (
                                  TC.TransCodeId IN ( SELECT    strval
                                                      FROM      dbo.SPLIT(@ChargeCode) )
                                  OR @ChargeCode IS NULL
                                )
                            AND (
                                  @EnrollmentStatus IS NULL
                                  OR SE.StatusCodeId IN ( SELECT    strval
                                                          FROM      dbo.SPLIT(@EnrollmentStatus) )
                                )
                            AND T.TransDate >= @StartDate
                            AND T.TransDate <= @EndDate    
-- New Code Added By Vijay Ramteke On January 27, 2011 --    
                            AND TC.SysTransCodeId NOT IN ( 11,12,13 )    
-- New Code Added By Vijay Ramteke On January 27, 2011 --    
                ) TT
        WHERE   CampGrpDescrip IN ( SELECT  CampGrpDescrip
                                    FROM    syCampGrps CG
                                    WHERE   CG.CampGrpId IN ( SELECT    strval
                                                              FROM      dbo.SPLIT(@CampGrpId) ) )
        GROUP BY LastName
               ,FirstName
               ,MiddleName
               ,StudentIdentifier
               ,PrgVerDescrip
               ,TransDate
               ,ChagreCode
               ,PaymentAmount
               ,AppliedPayment
               ,CampDescrip
               ,StatusCodeDescrip
               ,CampGrpDescrip
               ,StudentName
               ,TransReference
               ,TransactionId
        ORDER BY LastName
               ,TransDate;    
    
    END;    
    
    



GO
