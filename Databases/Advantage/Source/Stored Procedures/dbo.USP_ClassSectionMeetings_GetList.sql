SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_ClassSectionMeetings_GetList]
    @ClsSectionId VARCHAR(50)
AS
    SELECT DISTINCT
            t1.PeriodDescrip
           ,(
              SELECT    RIGHT(CONVERT(VARCHAR,TimeIntervalDescrip,100),7)
              FROM      cmTimeInterval
              WHERE     TimeIntervalId = t1.StartTimeId
            ) AS StartTime
           ,(
              SELECT    RIGHT(CONVERT(VARCHAR,TimeIntervalDescrip,100),7)
              FROM      cmTimeInterval
              WHERE     TimeIntervalId = t1.EndTimeId
            ) AS EndTime
           ,t3.InstructionTypeDescrip
           ,ClsSectionId
    FROM    syPeriods t1
    INNER JOIN arClsSectMeetings t2 ON t1.PeriodId = t2.PeriodId
    INNER JOIN arInstructionType t3 ON t2.InstructionTypeId = t3.InstructionTypeId
    WHERE   --t2.ClsSectionId='B3D2D1DF-A182-46E6-BAD7-0060929AB83A'
            t2.ClsSectionId = @ClsSectionId;



GO
