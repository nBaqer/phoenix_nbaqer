SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =========================================================================================================
-- Usp_PR_Sub5_ComponentsByCourse_GivenComponentTypeId
-- =========================================================================================================
CREATE PROCEDURE [dbo].[Usp_PR_Sub5_ComponentsByCourse_GivenComponentTypeId]
    @StuEnrollIdList VARCHAR(MAX),
    @TermId VARCHAR(50) = NULL,
    @SysComponentTypeIdList VARCHAR(50) = NULL,
    @ReqId VARCHAR(50),
    @SysComponentTypeId VARCHAR(10) = NULL,
    @ShowIncomplete BIT = 1
AS
BEGIN
    DECLARE @StuEnrollCampusId UNIQUEIDENTIFIER;
    DECLARE @SetGradeBookAt VARCHAR(50);
    DECLARE @CharInt INT;
    DECLARE @Counter AS INT;
    DECLARE @times AS INT;
    DECLARE @TermStartDate1 AS DATETIME;
    DECLARE @Score AS DECIMAL(18, 2);
    DECLARE @GrdCompDescrip AS VARCHAR(50);


    DECLARE @curId AS UNIQUEIDENTIFIER;
    DECLARE @curReqId AS UNIQUEIDENTIFIER;
    DECLARE @curStuEnrollId AS UNIQUEIDENTIFIER;
    DECLARE @curDescrip AS VARCHAR(50);
    DECLARE @curNumber AS INT;
    DECLARE @curGrdComponentTypeId AS INT;
    DECLARE @curMinResult AS DECIMAL(18, 2);
    DECLARE @curGrdComponentDescription AS VARCHAR(50);
    DECLARE @curClsSectionId AS UNIQUEIDENTIFIER;
    DECLARE @curRownumber AS INT;
		 DECLARE @curRowSeq AS INT;
    DECLARE @ExecutedSproc BIT = 0;

    CREATE TABLE #Temp21
    (
        Id UNIQUEIDENTIFIER,
        TermId UNIQUEIDENTIFIER,
        ReqId UNIQUEIDENTIFIER,
        GradeBookDescription VARCHAR(50),
        Number INT,
        GradeBookSysComponentTypeId INT,
        GradeBookScore DECIMAL(18, 2),
        MinResult DECIMAL(18, 2),
        GradeComponentDescription VARCHAR(50),
        ClsSectionId UNIQUEIDENTIFIER,
        StuEnrollId UNIQUEIDENTIFIER,
        RowNumber INT
			,Seq int
    );
    CREATE TABLE #Temp22
    (
        ReqId UNIQUEIDENTIFIER,
        EffectiveDate DATETIME
    );

    IF @SysComponentTypeIdList IS NULL
    BEGIN
        SET @SysComponentTypeIdList = '501,544,502,499,503,500,533';
    END;

    SET @StuEnrollCampusId = COALESCE(
                             (
                                 SELECT TOP 1
                                        ASE.CampusId
                                 FROM dbo.arStuEnrollments AS ASE
                                 WHERE EXISTS
                                 (
                                     SELECT Val
                                     FROM dbo.MultipleValuesForReportParameters(@StuEnrollIdList, ',', 1)
                                     WHERE Val = ASE.StuEnrollId
                                 )
                             ),
                             NULL
                                     );
    SET @SetGradeBookAt = (dbo.GetAppSettingValueByKeyName('GradeBookWeightingLevel', @StuEnrollCampusId));
    IF (@SetGradeBookAt IS NOT NULL)
    BEGIN
        SET @SetGradeBookAt = LOWER(LTRIM(RTRIM(@SetGradeBookAt)));
    END;


    SET @CharInt =
    (
        SELECT CHARINDEX(@SysComponentTypeId, @SysComponentTypeIdList)
    );

    IF @SetGradeBookAt = 'instructorlevel'
    BEGIN
        SELECT CASE
                   WHEN GradeBookDescription IS NULL THEN
                       GradeComponentDescription
                   ELSE
                       GradeBookDescription
               END AS GradeBookDescription,
               MinResult,
               GradeBookScore,
               GradeComponentDescription,
               GradeBookSysComponentTypeId,
               StuEnrollId,
               rownumber
        FROM
        (
            SELECT AR2.ReqId,
                   AT.TermId,
                   CASE
                       WHEN AGBWD.Descrip IS NULL THEN
                           AGCT.Descrip
                       ELSE
                           AGBWD.Descrip
                   END AS GradeBookDescription,
                   (CASE
                        WHEN AGCT.SysComponentTypeId IN ( 500, 503, 544 ) THEN
                            AGBWD.Number
                        ELSE
                    (
                        SELECT MIN(MinVal)
                        FROM arGradeScaleDetails GSD,
                             arGradeSystemDetails GSS
                        WHERE GSD.GrdSysDetailId = GSS.GrdSysDetailId
                              AND GSS.IsPass = 1
                              AND GSD.GrdScaleId = ACS.GrdScaleId
                    )
                    END
                   ) AS MinResult,
                   AGBWD.Required,
                   AGBWD.MustPass,
                   ISNULL(AGCT.SysComponentTypeId, 0) AS GradeBookSysComponentTypeId,
                   AGBWD.Number,
                   SR.Resource AS GradeComponentDescription,
                   AGBWD.InstrGrdBkWgtDetailId,
                   ASE.StuEnrollId,
                   0 AS IsExternShip,
                   (CASE AGCT.SysComponentTypeId
                        WHEN 544 THEN
                        (
                            SELECT SUM(HoursAttended)
                            FROM arExternshipAttendance
                            WHERE StuEnrollId = ASE.StuEnrollId
                        )
                        ELSE
                    (
                        SELECT SUM(ISNULL(Score, 0))
                        FROM arGrdBkResults
                        WHERE StuEnrollId = ASE.StuEnrollId
                              AND InstrGrdBkWgtDetailId = AGBWD.InstrGrdBkWgtDetailId
                              AND ClsSectionId = ACS.ClsSectionId
                    )
                    END
                   ) AS GradeBookScore,
				   AGBWD.Seq,
                   ROW_NUMBER() OVER (PARTITION BY ASE.StuEnrollId,
                                                   AT.TermId,
                                                   AR2.ReqId
                                      ORDER BY AGCT.SysComponentTypeId,
                                               AGBWD.Descrip
                                     ) AS rownumber
            FROM arGrdBkWgtDetails AS AGBWD
                INNER JOIN arGrdBkWeights AS AGBW
                    ON AGBW.InstrGrdBkWgtId = AGBWD.InstrGrdBkWgtId
                INNER JOIN arClassSections AS ACS
                    ON ACS.InstrGrdBkWgtId = AGBW.InstrGrdBkWgtId
                INNER JOIN arResults AS AR
                    ON AR.TestId = ACS.ClsSectionId
                INNER JOIN arGrdComponentTypes AS AGCT
                    ON AGCT.GrdComponentTypeId = AGBWD.GrdComponentTypeId
                INNER JOIN arStuEnrollments AS ASE
                    ON ASE.StuEnrollId = AR.StuEnrollId
                INNER JOIN arTerm AS AT
                    ON AT.TermId = ACS.TermId
                INNER JOIN arReqs AS AR2
                    ON AR2.ReqId = ACS.ReqId
                INNER JOIN syResources AS SR
                    ON SR.ResourceID = AGCT.SysComponentTypeId
                LEFT JOIN arGrdBkResults AS AGBR
                    ON AGBR.StuEnrollId = ASE.StuEnrollId
                       AND AGBR.InstrGrdBkWgtDetailId = AGBWD.InstrGrdBkWgtDetailId
                       AND AGBR.ClsSectionId = ACS.ClsSectionId
            WHERE AR.StuEnrollId IN
                  (
                      SELECT Val
                      FROM MultipleValuesForReportParameters(@StuEnrollIdList, ',', 1)
                  )
                  AND AT.TermId = @TermId
                  AND AR2.ReqId = @ReqId
                  AND
                  (
                      @SysComponentTypeIdList IS NULL
                      OR AGCT.SysComponentTypeId IN
                         (
                             SELECT Val
                             FROM MultipleValuesForReportParameters(@SysComponentTypeIdList, ',', 1)
                         )
                  )
            UNION
            SELECT AR2.ReqId,
                   AT.TermId,
                   CASE
                       WHEN AGBW.Descrip IS NULL THEN
                       (
                           SELECT Resource
                           FROM syResources
                           WHERE ResourceID = AGCT.SysComponentTypeId
                       )
                       ELSE
                           AGBW.Descrip
                   END AS GradeBookDescription,
                   (CASE
                        WHEN AGCT.SysComponentTypeId IN ( 500, 503, 544 ) THEN
                            AGBWD.Number
                        ELSE
                    (
                        SELECT MIN(MinVal)
                        FROM arGradeScaleDetails GSD,
                             arGradeSystemDetails GSS
                        WHERE GSD.GrdSysDetailId = GSS.GrdSysDetailId
                              AND GSS.IsPass = 1
                              AND GSD.GrdScaleId = ACS.GrdScaleId
                    )
                    END
                   ) AS MinResult,
                   AGBWD.Required,
                   AGBWD.MustPass,
                   ISNULL(AGCT.SysComponentTypeId, 0) AS GradeBookSysComponentTypeId,
                   AGBWD.Number,
                   (
                       SELECT Resource
                       FROM syResources
                       WHERE ResourceID = AGCT.SysComponentTypeId
                   ) AS GradeComponentDescription,
                   AGBWD.InstrGrdBkWgtDetailId,
                   ASE.StuEnrollId,
                   0 AS IsExternShip,
                   (CASE AGCT.SysComponentTypeId
                        WHEN 544 THEN
                        (
                            SELECT SUM(HoursAttended)
                            FROM arExternshipAttendance
                            WHERE StuEnrollId = ASE.StuEnrollId
                        )
                        ELSE
                    (
                        SELECT SUM(ISNULL(Score, 0))
                        FROM arGrdBkResults
                        WHERE StuEnrollId = ASE.StuEnrollId
                              AND InstrGrdBkWgtDetailId = AGBWD.InstrGrdBkWgtDetailId
                              AND ClsSectionId = ACS.ClsSectionId
                    )
                    END
                   ) AS GradeBookScore,
				   AGBWD.seq,
                   ROW_NUMBER() OVER (PARTITION BY ASE.StuEnrollId,
                                                   AT.TermId,
                                                   AR2.ReqId
                                      ORDER BY AGCT.SysComponentTypeId,
                                               AGCT.Descrip
                                     ) AS rownumber
            FROM arGrdBkConversionResults GBCR
                INNER JOIN arStuEnrollments AS ASE
                    ON ASE.StuEnrollId = GBCR.StuEnrollId
                INNER JOIN adLeads AS AST
                    ON AST.StudentId = ASE.StudentId
                INNER JOIN arPrgVersions AS APV
                    ON APV.PrgVerId = ASE.PrgVerId
                INNER JOIN arTerm AS AT
                    ON AT.TermId = GBCR.TermId
                INNER JOIN arReqs AS AR2
                    ON AR2.ReqId = GBCR.ReqId
                INNER JOIN arGrdComponentTypes AS AGCT
                    ON AGCT.GrdComponentTypeId = GBCR.GrdComponentTypeId
                INNER JOIN arGrdBkWgtDetails AS AGBWD
                    ON AGBWD.GrdComponentTypeId = AGCT.GrdComponentTypeId
                       AND AGBWD.GrdComponentTypeId = GBCR.GrdComponentTypeId
                INNER JOIN arGrdBkWeights AS AGBW
                    ON AGBW.InstrGrdBkWgtId = AGBWD.InstrGrdBkWgtId
                       AND AGBW.ReqId = GBCR.ReqId
                INNER JOIN
                (
                    SELECT ReqId,
                           MAX(EffectiveDate) AS EffectiveDate
                    FROM arGrdBkWeights AS AGBW
                    GROUP BY ReqId
                ) AS MaxEffectiveDatesByCourse
                    ON GBCR.ReqId = MaxEffectiveDatesByCourse.ReqId
                INNER JOIN
                (
                    SELECT Resource,
                           ResourceID
                    FROM syResources
                    WHERE ResourceTypeID = 10
                ) SYRES
                    ON SYRES.ResourceID = AGCT.SysComponentTypeId
                INNER JOIN syCampuses AS SC
                    ON SC.CampusId = ASE.CampusId
                INNER JOIN arClassSections AS ACS
                    ON ACS.TermId = AT.TermId
                       AND ACS.ReqId = AR2.ReqId
                LEFT JOIN arGrdBkResults AS AGBR
                    ON AGBR.StuEnrollId = ASE.StuEnrollId
                       AND AGBR.InstrGrdBkWgtDetailId = AGBWD.InstrGrdBkWgtDetailId
                       AND AGBR.ClsSectionId = ACS.ClsSectionId
            WHERE MaxEffectiveDatesByCourse.EffectiveDate <= AT.StartDate
                  AND ASE.StuEnrollId IN
                      (
                          SELECT Val
                          FROM MultipleValuesForReportParameters(@StuEnrollIdList, ',', 1)
                      )
                  AND AT.TermId = @TermId
                  AND AR2.ReqId = @ReqId
                  AND
                  (
                      @SysComponentTypeIdList IS NULL
                      OR AGCT.SysComponentTypeId IN
                         (
                             SELECT Val
                             FROM MultipleValuesForReportParameters(@SysComponentTypeIdList, ',', 1)
                         )
                  )
        ) dt
        WHERE (
                  @SysComponentTypeId IS NULL
                  OR dt.GradeBookSysComponentTypeId = @SysComponentTypeId
              )
              AND
              (
                  @ShowIncomplete = 1
                  OR
                  (
                      @ShowIncomplete = 0
                      AND dt.GradeBookScore IS NOT NULL
                  )
              )
        ORDER BY seq,GradeComponentDescription,
                 rownumber,
                 GradeBookDescription;
    END;
    ELSE
    BEGIN

        SET @TermStartDate1 =
        (
            SELECT AT.StartDate FROM dbo.arTerm AS AT WHERE AT.TermId = @TermId
        );
        INSERT INTO #Temp22
        SELECT AGBW.ReqId,
               MAX(AGBW.EffectiveDate) AS EffectiveDate
        FROM dbo.arGrdBkWeights AS AGBW
            INNER JOIN dbo.syCreditSummary AS SCS
                ON SCS.ReqId = AGBW.ReqId
        WHERE SCS.TermId = @TermId
              AND SCS.StuEnrollId IN
                  (
                      SELECT Val
                      FROM dbo.MultipleValuesForReportParameters(@StuEnrollIdList, ',', 1)
                  )
              AND AGBW.EffectiveDate <= @TermStartDate1
        GROUP BY AGBW.ReqId;


        DECLARE getUsers_Cursor CURSOR FOR
        SELECT dt.ID,
               dt.ReqId,
               dt.Descrip,
               dt.Number,
               dt.SysComponentTypeId,
               dt.MinResult,
               dt.GradeComponentDescription,
               dt.ClsSectionId,
               dt.StuEnrollId,
               ROW_NUMBER() OVER (PARTITION BY dt.StuEnrollId,
                                               @TermId,
                                               dt.SysComponentTypeId
                                  ORDER BY dt.SysComponentTypeId,
                                           dt.Descrip
                                 ) AS rownumber,
								 dt.Seq
        FROM
        (
            SELECT ISNULL(AGBWD.InstrGrdBkWgtDetailId, NEWID()) AS ID,
                   AR.ReqId,
                   AGBWD.Descrip,
                   AGBWD.Number,
                   AGCT.SysComponentTypeId,
                   (CASE
                        WHEN AGCT.SysComponentTypeId IN ( 500, 503, 544 ) THEN
                            AGBWD.Number
                        ELSE
                    (
                        SELECT MIN(AGSD.MinVal) AS MinVal
                        FROM dbo.arGradeScaleDetails AS AGSD
                            INNER JOIN dbo.arGradeSystemDetails AS AGSDetails
                                ON AGSDetails.GrdSysDetailId = AGSD.GrdSysDetailId
                        WHERE AGSDetails.IsPass = 1
                              AND AGSD.GrdScaleId = ACS.GrdScaleId
                    )
                    END
                   ) AS MinResult,
                   SR.Resource AS GradeComponentDescription,
                   ACS.ClsSectionId,
                   AGBR.StuEnrollId,
				   AGBWD.Seq
            FROM dbo.arGrdBkResults AS AGBR
                INNER JOIN dbo.arGrdBkWgtDetails AS AGBWD
                    ON AGBWD.InstrGrdBkWgtDetailId = AGBR.InstrGrdBkWgtDetailId
                INNER JOIN dbo.arGrdBkWeights AS AGBW
                    ON AGBW.InstrGrdBkWgtId = AGBWD.InstrGrdBkWgtId
                INNER JOIN dbo.arGrdComponentTypes AS AGCT
                    ON AGCT.GrdComponentTypeId = AGBWD.GrdComponentTypeId
                INNER JOIN dbo.syResources AS SR
                    ON SR.ResourceID = AGCT.SysComponentTypeId
                --                             INNER JOIN #Temp22 AS T2 ON T2.ReqId = AGBW.ReqId
                INNER JOIN dbo.arReqs AS AR
                    ON AR.ReqId = AGBW.ReqId
                INNER JOIN dbo.arClassSections AS ACS
                    ON ACS.ReqId = AR.ReqId
                       AND ACS.ClsSectionId = AGBR.ClsSectionId
                INNER JOIN dbo.arResults AS AR2
                    ON AR2.TestId = ACS.ClsSectionId
                       AND AR2.StuEnrollId = AGBR.StuEnrollId --AND AR2.TestId = AGBR.ClsSectionId

            WHERE (
                      @StuEnrollIdList IS NULL
                      OR AGBR.StuEnrollId IN
                         (
                             SELECT Val
                             FROM dbo.MultipleValuesForReportParameters(@StuEnrollIdList, ',', 1)
                         )
                  )
                  --                                        AND T2.EffectiveDate = AGBW.EffectiveDate
                  AND AGBWD.Number > 0
                  AND AR.ReqId = @ReqId
        ) dt
        ORDER BY SysComponentTypeId,
                 rownumber;

        DECLARE @schoolTypeSetting VARCHAR(50);
        SET @schoolTypeSetting = dbo.GetAppSettingValueByKeyName('gradesformat', NULL);


        OPEN getUsers_Cursor;
        FETCH NEXT FROM getUsers_Cursor
        INTO @curId,
             @curReqId,
             @curDescrip,
             @curNumber,
             @curGrdComponentTypeId,
             @curMinResult,
             @curGrdComponentDescription,
             @curClsSectionId,
             @curStuEnrollId,
             @curRownumber,
			 @curRowSeq
        SET @Counter = 0;
        WHILE @@FETCH_STATUS = 0
        BEGIN
            --PRINT @Number; 

            IF @schoolTypeSetting = 'Numeric'
            BEGIN
                SET @times = 1;

                SET @GrdCompDescrip = @curDescrip;
                IF (@curGrdComponentTypeId <> 500 AND @curGrdComponentTypeId <> 503)
                BEGIN
                    SET @Score =
                    (
                        SELECT TOP 1
                               Score
                        FROM arGrdBkResults
                        WHERE StuEnrollId = @curStuEnrollId
                              AND InstrGrdBkWgtDetailId = @curId
                              AND ResNum = @times
                    );
                    IF @Score IS NULL
                    BEGIN
                        SET @Score =
                        (
                            SELECT TOP 1
                                   Score
                            FROM arGrdBkResults
                            WHERE StuEnrollId = @curStuEnrollId
                                  AND InstrGrdBkWgtDetailId = @curId
                                  AND ResNum = (@times - 1)
                        );
                    END;

                    IF @curGrdComponentTypeId = 544
                    BEGIN
                        SET @Score =
                        (
                            SELECT SUM(HoursAttended)
                            FROM arExternshipAttendance
                            WHERE StuEnrollId = @curStuEnrollId
                        );
                    END;
                    INSERT INTO #Temp21
                    VALUES
                    (@curId, @TermId, @curReqId, @GrdCompDescrip, @curNumber, @curGrdComponentTypeId, @Score,
                     @curMinResult, @curGrdComponentDescription, @curClsSectionId, @curStuEnrollId, @curRownumber, @curRowSeq);
                END;
                ELSE
                BEGIN
                    IF (@curGrdComponentTypeId = 500 OR @curGrdComponentTypeId = 503)
                       AND @ExecutedSproc = 0
                    BEGIN


                        DECLARE @Services TABLE
                        (
                            Descrip VARCHAR(50),
                            Required DECIMAL(18, 2) NULL,
                            Completed INT NULL,
                            Remaining DECIMAL(18, 2) NULL,
                            Average DECIMAL(18, 2) NULL,
                            EnrollDate DATETIME NULL,
                            InstrGrdBkWgtDetailId UNIQUEIDENTIFIER,
                            ClsSectionId UNIQUEIDENTIFIER,
							Seq INT NULL
                        );


                        INSERT INTO @Services
                        (
                            Descrip,
                            Required,
                            Completed,
                            Remaining,
                            Average,
                            EnrollDate,
                            InstrGrdBkWgtDetailId,
                            ClsSectionId,
							Seq
                        )
                        EXEC dbo.usp_GetClassroomWorkServices @StuEnrollId = @curStuEnrollId,   -- uniqueidentifier
                                                              @ClsSectionId = @curClsSectionId, -- uniqueidentifier
                                                              @GradeBookComponentType = @curGrdComponentTypeId;    -- int



                        INSERT INTO #Temp21
                        SELECT @curId,
                               @TermId,
                               @curReqId,
                                LTRIM(RTRIM(Descrip)) + ' (' + CAST(Completed AS VARCHAR(10)) + '/'
                               +( CASE WHEN @curGrdComponentTypeId = 500 THEN CAST(CONVERT(INT,Required)  AS VARCHAR(10)) ELSE CAST(Required  AS VARCHAR(10)) END) + ')',
                               @curNumber,
                               @curGrdComponentTypeId,
                               Average,
                               [Required],
                               @curGrdComponentDescription,
                               @curClsSectionId,
                               @curStuEnrollId,
                               @curRownumber,
							   Seq
                        FROM @Services;


                        SET @ExecutedSproc = 1;
                    END;
                END;



            END;
            ELSE
            BEGIN

                SET @times = 1;
                WHILE @times <= @curNumber
                BEGIN
                    --PRINT @times; 
                    IF @curNumber > 1
                    BEGIN
                        SET @GrdCompDescrip = @curDescrip + CAST(@times AS CHAR);

                        IF @curGrdComponentTypeId = 544
                        BEGIN
                            SET @Score =
                            (
                                SELECT SUM(HoursAttended)
                                FROM arExternshipAttendance
                                WHERE StuEnrollId = @curStuEnrollId
                            );
                        END;
                        ELSE
                        BEGIN
                            SET @Score =
                            (
                                SELECT Score
                                FROM arGrdBkResults
                                WHERE StuEnrollId = @curStuEnrollId
                                      AND InstrGrdBkWgtDetailId = @curId
                                      AND ResNum = @times
                                      AND ClsSectionId = @curClsSectionId
                            );
                        END;


                        SET @curRownumber = @times;
                    END;
                    ELSE
                    BEGIN
                        SET @GrdCompDescrip = @curDescrip;
                        SET @Score =
                        (
                            SELECT TOP 1
                                   Score
                            FROM arGrdBkResults
                            WHERE StuEnrollId = @curStuEnrollId
                                  AND InstrGrdBkWgtDetailId = @curId
                                  AND ResNum = @times
                        );
                        IF @Score IS NULL
                        BEGIN
                            SET @Score =
                            (
                                SELECT TOP 1
                                       Score
                                FROM arGrdBkResults
                                WHERE StuEnrollId = @curStuEnrollId
                                      AND InstrGrdBkWgtDetailId = @curId
                                      AND ResNum = (@times - 1)
                            );
                        END;

                        IF @curGrdComponentTypeId = 544
                        BEGIN
                            SET @Score =
                            (
                                SELECT SUM(HoursAttended)
                                FROM arExternshipAttendance
                                WHERE StuEnrollId = @curStuEnrollId
                            );
                        END;

                    END;
                    --PRINT @Score; 
                    INSERT INTO #Temp21
                    VALUES
                    (@curId, @TermId, @curReqId, @GrdCompDescrip, @curNumber, @curGrdComponentTypeId, @Score,
                     @curMinResult, @curGrdComponentDescription, @curClsSectionId, @curStuEnrollId, @curRownumber);

                    SET @times = @times + 1;
                END;

            END;
            FETCH NEXT FROM getUsers_Cursor
            INTO @curId,
                 @curReqId,
                 @curDescrip,
                 @curNumber,
                 @curGrdComponentTypeId,
                 @curMinResult,
                 @curGrdComponentDescription,
                 @curClsSectionId,
                 @curStuEnrollId,
                 @curRownumber,
				 @curRowSeq
        END;
        CLOSE getUsers_Cursor;
        DEALLOCATE getUsers_Cursor;

        SET @CharInt =
        (
            SELECT CHARINDEX(@SysComponentTypeId, @SysComponentTypeIdList)
        );

        IF (@SysComponentTypeIdList IS NULL)
           OR (@CharInt >= 1)
        BEGIN
            SELECT Id,
                   --, StuEnrollId 
                   TermId,
                   GradeBookDescription,
                   Number,
                   GradeBookSysComponentTypeId,
                   GradeBookScore,
                   MinResult,
                   GradeComponentDescription,
                   RowNumber,
                   ClsSectionId
				      ,Seq
            FROM #Temp21
            WHERE GradeBookSysComponentTypeId = @SysComponentTypeId
			AND
              (
                  @ShowIncomplete = 1
                  OR
                  (
                      @ShowIncomplete = 0
                      AND GradeBookScore IS NOT NULL
                  )
              )
            UNION
            SELECT GBWD.InstrGrdBkWgtDetailId,
                   --, SE.StuEnrollId 
                   T.TermId,
                   GCT.Descrip AS GradeBookDescription,
                   GBWD.Number,
                   GCT.SysComponentTypeId,
                   GBCR.Score,
                   GBCR.MinResult,
                   SYRES.Resource,
                   ROW_NUMBER() OVER (PARTITION BY --SE.StuEnrollId,  
                                          T.TermId,
                                          GCT.SysComponentTypeId
                                      ORDER BY GCT.SysComponentTypeId,
                                               GCT.Descrip
                                     ) AS rownumber,
                   (
                       SELECT TOP 1
                              ClsSectionId
                       FROM arClassSections
                       WHERE TermId = T.TermId
                             AND ReqId = R.ReqId
                   ) AS ClsSectionId
				      ,GBWD.Seq
            FROM arGrdBkConversionResults GBCR
                INNER JOIN arStuEnrollments SE
                    ON GBCR.StuEnrollId = SE.StuEnrollId
                INNER JOIN
                (SELECT StudentId, FirstName, LastName, MiddleName FROM adLeads) S
                    ON S.StudentId = SE.StudentId
                INNER JOIN arPrgVersions PV
                    ON SE.PrgVerId = PV.PrgVerId
                INNER JOIN arTerm T
                    ON GBCR.TermId = T.TermId
                INNER JOIN arReqs R
                    ON GBCR.ReqId = R.ReqId
                INNER JOIN arGrdComponentTypes GCT
                    ON GCT.GrdComponentTypeId = GBCR.GrdComponentTypeId
                INNER JOIN arGrdBkWgtDetails GBWD
                    ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
                       AND GBWD.GrdComponentTypeId = GBCR.GrdComponentTypeId
                INNER JOIN arGrdBkWeights GBW
                    ON GBWD.InstrGrdBkWgtId = GBW.InstrGrdBkWgtId
                       AND GBCR.ReqId = GBW.ReqId
                INNER JOIN
                (
                    SELECT ReqId,
                           MAX(EffectiveDate) AS EffectiveDate
                    FROM arGrdBkWeights
                    GROUP BY ReqId
                ) AS MaxEffectiveDatesByCourse
                    ON GBCR.ReqId = MaxEffectiveDatesByCourse.ReqId
                INNER JOIN
                (
                    SELECT Resource,
                           ResourceID
                    FROM syResources
                    WHERE ResourceTypeID = 10
                ) SYRES
                    ON SYRES.ResourceID = GCT.SysComponentTypeId
                INNER JOIN syCampuses C
                    ON SE.CampusId = C.CampusId
            WHERE MaxEffectiveDatesByCourse.EffectiveDate <= T.StartDate
                  AND SE.StuEnrollId IN
                      (
                          SELECT Val
                          FROM MultipleValuesForReportParameters(@StuEnrollIdList, ',', 1)
                      )
                  AND T.TermId = @TermId
                  AND R.ReqId = @ReqId
                  AND
                  (
                      @SysComponentTypeIdList IS NULL
                      OR GCT.SysComponentTypeId IN
                         (
                             SELECT Val
                             FROM MultipleValuesForReportParameters(@SysComponentTypeIdList, ',', 1)
                         )
                  )
                  AND
                  (
                      @SysComponentTypeId IS NULL
                      OR GCT.SysComponentTypeId = @SysComponentTypeId
                  )
				  AND
              (
                  @ShowIncomplete = 1
                  OR
                  (
                      @ShowIncomplete = 0
                      AND GBCR.Score IS NOT NULL
                  )
              )
            ORDER BY GradeBookSysComponentTypeId,
			seq,
                     GradeBookDescription,
                     RowNumber;
        END;
    END;
    DROP TABLE #Temp22;
    DROP TABLE #Temp21;
END;
-- =========================================================================================================
-- END  --  Usp_PR_Sub5_ComponentsByCourse_GivenComponentTypeId
-- =========================================================================================================






GO
