SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[USP_AR_FeeLevels_GetList]
AS /*----------------------------------------------------------------------------------------------------
	Author          :	Saraswathi Lakshmanan
    
    Create date		:	05/05/2010
    
	Procedure Name	:	[USP_AR_FeeLevels_GetList]

	Objective		:	Get the fee levels associated to programs
	
	Parameters		:	Name			Type	Data Type			Required? 	
						=====			====	=========			=========	
						
	Output			:	Returns the fee Id and Descriptions		
						
*/-----------------------------------------------------------------------------------------------------
    BEGIN

        SELECT  FeeLevelId
               ,Description
        FROM    saFeeLevels
        WHERE   saFeeLevels.FeeLevelId IN ( 1,2,3 );
		-- for R2T4 saFeeLevels.FeeLevelId in (1,2,3,4,5)
    END;




GO
