SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[USP_AD_CopyLeadTransactions]
    @LeadId UNIQUEIDENTIFIER
   ,@StuEnrollId UNIQUEIDENTIFIER
   ,@CampusId UNIQUEIDENTIFIER
AS
    BEGIN



        BEGIN TRY

	

            SET NOCOUNT ON;

            BEGIN

		

                BEGIN TRANSACTION;

			

                DECLARE @PostCCPayAuto BIT;

                DECLARE @PosEFTPayAuto BIT;

                DECLARE @Cnt INT;

                DECLARE @LeadTranCount INT;

                DECLARE @PaymentType INT;

                DECLARE @TransTypeId INT;

                DECLARE @TranId UNIQUEIDENTIFIER;

                DECLARE @TranIdApp UNIQUEIDENTIFIER;

                DECLARE @TranChrg UNIQUEIDENTIFIER;

                DECLARE @TranIdRev UNIQUEIDENTIFIER;

                DECLARE @CreatedDate DATETIME;

                DECLARE @TransCodeId UNIQUEIDENTIFIER;

                DECLARE @TransDescrip VARCHAR(50);

                DECLARE @RevReason VARCHAR(250);

                DECLARE @Voided BIT;

                DECLARE @Amount DECIMAL(18,2);

                SET @Cnt = 1;		

                SET @LeadTranCount = 0;

                SET @PaymentType = 0;

				

                IF (
                     SELECT COUNT(*)
                     FROM   syConfigAppSetValues
                     WHERE  SettingId = 60
                            AND CampusId = @CampusId
                   ) >= 1
                    BEGIN

                        SET @PostCCPayAuto = (
                                               SELECT   Value
                                               FROM     dbo.syConfigAppSetValues
                                               WHERE    SettingId = 60
                                                        AND CampusId = @CampusId
                                             );  

									

                    END; 

                ELSE
                    BEGIN

                        SET @PostCCPayAuto = (
                                               SELECT   Value
                                               FROM     dbo.syConfigAppSetValues
                                               WHERE    SettingId = 60
                                                        AND CampusId IS NULL
                                             );

                    END;

				

                IF (
                     SELECT COUNT(*)
                     FROM   syConfigAppSetValues
                     WHERE  SettingId = 61
                            AND CampusId = @CampusId
                   ) >= 1
                    BEGIN

                        SET @PosEFTPayAuto = (
                                               SELECT   Value
                                               FROM     dbo.syConfigAppSetValues
                                               WHERE    SettingId = 61
                                                        AND CampusId = @CampusId
                                             );  

									

                    END; 

                ELSE
                    BEGIN

                        SET @PosEFTPayAuto = (
                                               SELECT   Value
                                               FROM     dbo.syConfigAppSetValues
                                               WHERE    SettingId = 61
                                                        AND CampusId IS NULL
                                             );

                    END;

				

                DECLARE @AppliedPaymentsData TABLE
                    (
                     RowCnt INT IDENTITY(1,1)
                    ,TransactionId UNIQUEIDENTIFIER NOT NULL
                    ,ApplyToTransId UNIQUEIDENTIFIER NOT NULL
                    ,TransAmount DECIMAL(18,2) NOT NULL
                    ,ModUser VARCHAR(50) NULL
                    ,ModDate DATETIME NULL
                    );

			

                DECLARE @ReversedTransactionsData TABLE
                    (
                     RowCnt INT IDENTITY(1,1)
                    ,TransactionId UNIQUEIDENTIFIER NOT NULL
                    ,RevTranId UNIQUEIDENTIFIER NOT NULL
                    ,ModUser VARCHAR(50) NULL
                    ,ModDate DATETIME NULL
                    );

				

                DECLARE @LeadTransactionsData TABLE
                    (
                     RowCnt INT IDENTITY(1,1)
                    ,TransactionId UNIQUEIDENTIFIER NOT NULL
                    ,TransCodeId UNIQUEIDENTIFIER NOT NULL
                    ,TransReference VARCHAR(50) NULL
                    ,TransDescrip VARCHAR(50) NOT NULL
                    ,TransAmount DECIMAL(19,4) NOT NULL
                    ,TransDate DATETIME NOT NULL
                    ,TransTypeId INT NOT NULL
                    ,CreatedDate DATETIME NOT NULL
                    ,Voided BIT NULL
                    ,TransModUser VARCHAR(50) NOT NULL
                    ,TransModDate DATETIME NULL
                    ,CampusId UNIQUEIDENTIFIER NULL
                    ,ReversalReason VARCHAR(250) NULL
                    ,PaymentTypeId INT NULL
                    ,CheckNumber VARCHAR(50) NULL
                    ,PayModUser VARCHAR(50) NULL
                    ,PayModDate DATETIME NULL
                    ,PaymentReference INT NULL
                    ,NewChargeTransID UNIQUEIDENTIFIER NULL
                    ,DisplaySequence INT
                    ,SecondDisplaySequence INT
                    ,IsDeposited BIT
                    );

					

                INSERT  INTO @LeadTransactionsData
                        (
                         TransactionId
                        ,TransCodeId
                        ,TransReference
                        ,TransDescrip
                        ,TransAmount
                        ,TransDate
                        ,TransTypeId
                        ,CreatedDate
                        ,Voided
                        ,TransModUser
                        ,TransModDate
                        ,CampusId
                        ,ReversalReason
                        ,PaymentTypeId
                        ,CheckNumber
                        ,PayModUser
                        ,PayModDate
                        ,PaymentReference
                        ,DisplaySequence
                        ,SecondDisplaySequence
                        ,IsDeposited				

                        )
                        SELECT  T.TransactionId
                               ,TransCodeId
                               ,TransReference
                               ,TransDescrip
                               ,TransAmount
                               ,TransDate
                               ,TransTypeId
                               ,CreatedDate
                               ,Voided
                               ,T.ModUser
                               ,T.ModDate
                               ,CampusId
                               ,ReversalReason
                               ,PaymentTypeId
                               ,CheckNumber
                               ,P.ModUser
                               ,P.ModDate
                               ,PaymentReference
                               ,T.DisplaySequence
                               ,T.SecondDisplaySequence
                               ,P.IsDeposited
                        FROM    dbo.adLeadTransactions T
                        LEFT OUTER JOIN dbo.adLeadPayments P ON P.TransactionId = T.TransactionId
                        WHERE   LeadId = @LeadId
                        ORDER BY T.DisplaySequence
                               ,T.SecondDisplaySequence
                               ,T.TransTypeId;

						

                SET @LeadTranCount = (
                                       SELECT   COUNT(*)
                                       FROM     @LeadTransactionsData
                                     );

			

                WHILE ( @Cnt < ( @LeadTranCount + 1 ) )
                    BEGIN

                        SET @TranId = NEWID();

				        -- insert applicant fee transaction into saTransactions	

                        INSERT  INTO dbo.saTransactions
                                (
                                 TransactionId
                                ,StuEnrollId
                                ,CampusId
                                ,TransDate
                                ,TransCodeId
                                ,TransReference
                                ,TransDescrip
                                ,TransAmount
                                ,TransTypeId
                                ,IsPosted
                                ,CreateDate
                                ,IsAutomatic
                                ,ModUser
                                ,ModDate
                                ,Voided
                                ,DisplaySequence
                                ,SecondDisplaySequence 

                                )
                                SELECT  @TranId
                                       ,@StuEnrollId
                                       ,CampusId
                                       ,TransDate
                                       ,TransCodeId
                                       ,TransReference
                                       ,TransDescrip
                                       ,TransAmount
                                       ,CASE TransTypeId
                                          WHEN 3 THEN 1
                                          ELSE TransTypeId
                                        END
                                       ,1
                                       ,GETDATE()
                                       ,0
                                       ,TransModUser
                                       ,TransModDate
                                       ,Voided
                                       ,DisplaySequence
                                       ,SecondDisplaySequence
                                FROM    @LeadTransactionsData
                                WHERE   RowCnt = @Cnt;

						

				

				

					

                        UPDATE  @LeadTransactionsData
                        SET     NewChargeTransID = @TranId
                        WHERE   RowCnt = @Cnt;	

				

                        SET @PaymentType = (
                                             SELECT PaymentTypeId
                                             FROM   @LeadTransactionsData
                                             WHERE  RowCnt = @Cnt
                                           );

                        IF @PaymentType <> 0
                            BEGIN

								-- insert payment data into saPayments

                                INSERT  INTO dbo.saPayments
                                        (
                                         TransactionId
                                        ,PaymentTypeId
                                        ,CheckNumber
                                        ,ScheduledPayment
                                        ,IsDeposited
                                        ,ModUser
                                        ,ModDate 

                                        )
                                        SELECT  @TranId
                                               ,PaymentTypeId
                                               ,CheckNumber
                                               ,0
                                               ,CASE PaymentTypeId
                                                  WHEN 1 THEN IsDeposited
                                                  WHEN 2 THEN 0
                                                  WHEN 5 THEN 0
                                                  WHEN 6 THEN 0
                                                  WHEN 3 THEN @PostCCPayAuto
                                                  WHEN 4 THEN @PosEFTPayAuto
                                                  ELSE 0
                                                END
                                               ,PayModUser
                                               ,PayModDate
                                        FROM    @LeadTransactionsData
                                        WHERE   RowCnt = @Cnt;

					

					

								-- write applied payment if not voided

                                SET @Voided = (
                                                SELECT  Voided
                                                FROM    @LeadTransactionsData
                                                WHERE   RowCnt = @Cnt
                                              );

                                IF @Voided = 0
                                    BEGIN

										-- get charge transaction id if not voided

                                        SET @CreatedDate = (
                                                             SELECT CreatedDate
                                                             FROM   @LeadTransactionsData
                                                             WHERE  RowCnt = @Cnt
                                                           );

                                        SET @TransCodeId = (
                                                             SELECT TransCodeId
                                                             FROM   @LeadTransactionsData
                                                             WHERE  RowCnt = @Cnt
                                                           );

                                        SET @TransDescrip = (
                                                              SELECT    TransDescrip
                                                              FROM      @LeadTransactionsData
                                                              WHERE     RowCnt = @Cnt
                                                            );

                                        SET @TranChrg = (
                                                          SELECT    NewChargeTransID
                                                          FROM      @LeadTransactionsData
                                                          WHERE     TransTypeId = 0
                                                                    AND CreatedDate = @CreatedDate
                                                                    AND TransCodeId = @TransCodeId
                                                                    AND TransDescrip = @TransDescrip
                                                        );

						

                                        SET @TranIdApp = NEWID();

							

										-- insert payment data into saAppliedPayments

                                        INSERT  INTO dbo.saAppliedPayments
                                                (
                                                 AppliedPmtId
                                                ,TransactionId
                                                ,ApplyToTransId
                                                ,Amount
                                                ,ModUser
                                                ,ModDate 

                                                )
                                                SELECT  @TranIdApp
                                                       ,@TranId
                                                       ,@TranChrg
                                                       ,ABS(TransAmount)
                                                       ,PayModUser
                                                       ,PayModDate
                                                FROM    @LeadTransactionsData
                                                WHERE   RowCnt = @Cnt;

						

                                    END;

						

                                SET @RevReason = (
                                                   SELECT   ReversalReason
                                                   FROM     @LeadTransactionsData
                                                   WHERE    RowCnt = @Cnt
                                                 ); 

                                IF LEN(@RevReason) > 0
                                    BEGIN

							

                                        SET @TranIdRev = NEWID();

								

										-- insert reversal into saReversalTransactions

                                        INSERT  INTO dbo.saReversedTransactions
                                                (
                                                 TransactionId
                                                ,ReversedTransactionId
                                                ,ModUser
                                                ,ModDate
                                                ,ReversalReason

                                                )
                                                SELECT  @TranIdRev
                                                       ,@TranId
                                                       ,'sa'
                                                       ,GETDATE()
                                                       ,ReversalReason
                                                FROM    @LeadTransactionsData
                                                WHERE   RowCnt = @Cnt;

								

                                    END;		

							

                            END;

					

						-- write saAppliedPayments record for adjustment	

                        SET @TransTypeId = (
                                             SELECT TransTypeId
                                             FROM   @LeadTransactionsData
                                             WHERE  RowCnt = @Cnt
                                           );

                        SET @Amount = (
                                        SELECT  TransAmount
                                        FROM    @LeadTransactionsData
                                        WHERE   RowCnt = @Cnt
                                      );

                        IF @TransTypeId = 1
                            AND @Amount > 0
                            BEGIN 

					

								-- match charge transaction for adjustment

                                SET @CreatedDate = (
                                                     SELECT CreatedDate
                                                     FROM   @LeadTransactionsData
                                                     WHERE  RowCnt = @Cnt
                                                   );

                                SET @TransCodeId = (
                                                     SELECT TransCodeId
                                                     FROM   @LeadTransactionsData
                                                     WHERE  RowCnt = @Cnt
                                                   );

                                SET @TransDescrip = (
                                                      SELECT    TransDescrip
                                                      FROM      @LeadTransactionsData
                                                      WHERE     RowCnt = @Cnt
                                                    );

                                SET @TranChrg = (
                                                  SELECT    NewChargeTransID
                                                  FROM      @LeadTransactionsData
                                                  WHERE     TransTypeId = 1
                                                            AND CreatedDate = @CreatedDate
                                                            AND TransCodeId = @TransCodeId
                                                            AND TransDescrip = @TransDescrip
                                                            AND TransAmount = ( @Amount * -1 )
                                                );

					

                                SET @TranIdApp = NEWID();

						

								-- insert adjustment data into saAppliedPayments

                                INSERT  INTO dbo.saAppliedPayments
                                        (
                                         AppliedPmtId
                                        ,TransactionId
                                        ,ApplyToTransId
                                        ,Amount
                                        ,ModUser
                                        ,ModDate 

                                        )
                                        SELECT  @TranIdApp
                                               ,@TranChrg
                                               ,@TranId
                                               ,ABS(TransAmount)
                                               ,TransModUser
                                               ,TransModDate
                                        FROM    @LeadTransactionsData
                                        WHERE   RowCnt = @Cnt;

					

					

                            END;

														

                        SET @Cnt += 1;

                    END;

					

				-- set isEnrolled to true

                UPDATE  dbo.adLeadTransactions
                SET     isEnrolled = 1
                WHERE   LeadId = @LeadId;

					       

                COMMIT TRANSACTION;
                RETURN 1;
		

		

            END;  			  

        END TRY

		

        BEGIN CATCH

	

            ROLLBACK TRANSACTION;
            RETURN 0;
		

        END CATCH;	  

    END;
GO
