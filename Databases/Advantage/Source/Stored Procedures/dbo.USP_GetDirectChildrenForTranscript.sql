SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[USP_GetDirectChildrenForTranscript]
    (
     @stuEnrollid UNIQUEIDENTIFIER
    )
AS
    SET NOCOUNT ON;
    SELECT DISTINCT
            t400.ReqId AS ReqId
           ,t3.Descrip AS Req
           ,t3.Code AS Code
           ,t3.Credits
           ,t3.Hours
           ,t3.ReqTypeId
           ,t400.Credits AS DefCredits
           ,t600.Credits AS ProgCredits
           ,t600.Hours AS ProgHours
           ,t600.PrgVerDescrip
           ,t400.ReqSeq
           ,t600.IsContinuingEd
           ,t600.PrgVerId
           ,t3.FinAidCredits
           ,t100.StuEnrollId 
        /** New code added by kamalesh ahuja on march 08 2010 **/
           ,t3.Hours AS ScheduledHours
        /**/
    FROM    arStuEnrollments t100
    INNER JOIN arProgVerDef t400 ON t100.PrgVerId = t400.PrgVerId
    INNER JOIN arReqs t3 ON t3.ReqId = t400.ReqId
    INNER JOIN arStudent t500 ON t100.StudentId = t500.StudentId
    INNER JOIN arPrgVersions t600 ON t100.PrgVerId = t600.PrgVerId
                                     AND t100.StuEnrollId = @stuEnrollid
    ORDER BY t400.ReqSeq; 




GO
