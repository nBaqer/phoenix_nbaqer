SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_StudentRegisteredInDictationTestCourseByEnrollment_getcount]
    @studentid UNIQUEIDENTIFIER
   ,@stuenrollid UNIQUEIDENTIFIER
AS
    SELECT  COUNT(*) AS isRegistered
    FROM    arStudent t1
    INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
    INNER JOIN arResults t3 ON t2.StuEnrollId = t3.StuEnrollId
    INNER JOIN arClassSections t4 ON t3.TestId = t4.ClsSectionId
    INNER JOIN arBridge_GradeComponentTypes_Courses t5 ON t4.ReqId = t5.ReqId
    WHERE   t1.StudentId = @studentid
            AND t2.StuEnrollId = @stuenrollid;



GO
