SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_FERPAEntityInfo_Update]
    (
     @FERPAEntityID UNIQUEIDENTIFIER
    ,@FERPAEntityCode VARCHAR(12)
    ,@StatusId UNIQUEIDENTIFIER
    ,@FERPAEntityDescrip VARCHAR(50)
    ,@CampGrpId UNIQUEIDENTIFIER
    ,@ModUser VARCHAR(50)
    ,@moddate DATETIME
    )
AS
    SET NOCOUNT ON;
    UPDATE  arFERPAEntity
    SET     FERPAEntityCode = @FERPAEntityCode
           ,StatusId = @StatusId
           ,FERPAEntityDescrip = @FERPAEntityDescrip
           ,CampGrpId = @CampGrpId
           ,ModUser = @ModUser
           ,moddate = @moddate
    WHERE   FERPAEntityID = @FERPAEntityID;



GO
