SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[usp_UpdateAllPunchesToDuplicateandResetActualHrs]
    (
     @StuEnrollId AS VARCHAR(50)
    ,@SpecialCode AS VARCHAR(10)
    ,@PunchTime AS DATETIME
			
	)
AS
    BEGIN 
	
        DECLARE @PunchType AS VARCHAR(2);
		
        SET @PunchType = (
                           SELECT TOP 1
                                    TCSPunchType
                           FROM     dbo.arTimeClockSpecialCode
                           WHERE    TCSpecialCode = @SpecialCode
                         );
        IF @PunchType IS NULL
            BEGIN
                SET @PunchType = 1;
            END;                
        DECLARE @ClsSectMeetingID AS VARCHAR(50);

        SET @ClsSectMeetingID = (
                                  SELECT    ClsSectMeetingID
                                  FROM      dbo.arStudentTimeClockPunches
                                  WHERE     StuEnrollId = @StuEnrollId
                                            AND PunchTime = @PunchTime
                                            AND PunchType = @PunchType
                                );

        IF NOT @ClsSectMeetingID IS NULL
            BEGIN

                UPDATE  arStudentTimeClockPunches
                SET     Status = 5
                WHERE   StuEnrollId = @StuEnrollId
                        AND CONVERT(VARCHAR,PunchTime,101) = CONVERT(VARCHAR,@PunchTime,101)
                        AND ClsSectMeetingID = @ClsSectMeetingID;
						

                UPDATE  arStudentTimeClockPunches
                SET     Status = 8
                WHERE   StuEnrollId = @StuEnrollId
                        AND PunchTime = @PunchTime
                        AND PunchType = @PunchType
                        AND ClsSectMeetingID = @ClsSectMeetingID;
 
                DELETE  FROM arStudentTimeClockPunches
                WHERE   StuEnrollId = @StuEnrollId
                        AND PunchTime = @PunchTime
                        AND ClsSectMeetingID = @ClsSectMeetingID
                        AND FromSystem = 1;
  
                IF EXISTS ( SELECT  *
                            FROM    dbo.arStudentTimeClockPunches
                            WHERE   StuEnrollId = @StuEnrollId
                                    AND PunchTime = @PunchTime
                                    AND PunchType = @PunchType )
                    BEGIN
						
  
                        UPDATE  atClsSectAttendance
                        SET     Actual = 9999.0
                               ,Tardy = 0
                        WHERE   StuEnrollId = @StuEnrollId
                                AND ClsSectMeetingId = @ClsSectMeetingID
                                AND CONVERT(VARCHAR,MeetDate,101) = CONVERT(VARCHAR,@PunchTime,101);
  
                    END;

            END;
    END;







GO
