SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_CCR_CheckIfCourseHasAPassingGrade]
    @StuEnrollId UNIQUEIDENTIFIER
   ,@ReqId UNIQUEIDENTIFIER
AS
    SELECT  COUNT(t1.ModUser) AS RowCounter
    FROM    arResults t1
           ,arGradeSystemDetails t2
           ,arGradeSystems t3
           ,arClassSections t4
    WHERE   t1.GrdSysDetailId = t2.GrdSysDetailId
            AND t2.GrdSystemId = t3.GrdSystemId
            AND t1.TestId = t4.ClsSectionId
            AND t1.StuEnrollId = @StuEnrollId
            AND t4.ReqId = @ReqId
            AND t2.IsPass = 1;



GO
