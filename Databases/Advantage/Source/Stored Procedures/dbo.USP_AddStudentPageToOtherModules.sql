SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

--Added BY Theresa G ON 7/20/12 TO MERGE the 2.11SP5 project INTO streamlineUI  End

--------------------------------------------------------------------------------------------------------------------------------  
---DE4503 QA: Bug:We need to show only the student pages that are relevant to the client. Theresa G 07/24/2012 Start  
------------------------------------------------------------------------------------------------------------------------------  
--exec USP_AddStudentPageToOtherModules 412    
--exec USP_AddStudentPageToOtherModules 412    
CREATE PROCEDURE [dbo].[USP_AddStudentPageToOtherModules]
    @SchoolEnumerator INT
AS
    DECLARE @ShowRossOnlyTabs BIT
       ,@SchedulingMethod VARCHAR(50)
       ,@TrackSAPAttendance VARCHAR(50)
       ,@PostServicesByStudent BIT
       ,@PostServicesByClass BIT;
    DECLARE @ShowCollegeOfCourtReporting VARCHAR(5)
       ,@FameESP VARCHAR(5)
       ,@EdExpress VARCHAR(5);     
    DECLARE @GradeBookWeightingLevel VARCHAR(20)
       ,@ShowExternshipTabs VARCHAR(5);    
	
    DECLARE @AR_ParentId UNIQUEIDENTIFIER
       ,@PL_ParentId UNIQUEIDENTIFIER
       ,@FA_ParentId UNIQUEIDENTIFIER;    
    DECLARE @FAC_ParentId UNIQUEIDENTIFIER
       ,@SA_ParentId UNIQUEIDENTIFIER
       ,@AD_ParentId UNIQUEIDENTIFIER;    
	 
    SET @AD_ParentId = (
                         SELECT t1.HierarchyId
                         FROM   syNavigationNodes t1
                         INNER JOIN syNavigationNodes t2 ON t1.ParentId = t2.HierarchyId
                         WHERE  t1.ResourceId = 395
                                AND t2.ResourceId = 189
                       );    
		 
    SET @ShowRossOnlyTabs = (
                              SELECT    VALUE
                              FROM      dbo.syConfigAppSetValues
                              WHERE     SettingId = 68
                                        AND CampusId IS NULL
                            );    
	
	
    SET @ShowCollegeOfCourtReporting = (
                                         SELECT VALUE
                                         FROM   dbo.syConfigAppSetValues
                                         WHERE  SettingId = 118
                                                AND CampusId IS NULL
                                       );      
			
    SET @ShowExternshipTabs = (
                                SELECT  VALUE
                                FROM    dbo.syConfigAppSetValues
                                WHERE   SettingId = 71
                                        AND CampusId IS NULL
                              );  
							  
    SET @PostServicesByStudent = (
                                   SELECT   VALUE
                                   FROM     dbo.syConfigAppSetValues
                                   WHERE    SettingId = (
                                                          SELECT    SettingId
                                                          FROM      dbo.syConfigAppSettings
                                                          WHERE     KeyName = 'PostServicesByStudent'
                                                        )
                                            AND CampusId IS NULL
                                 ); 
    SET @PostServicesByClass = (
                                 SELECT VALUE
                                 FROM   dbo.syConfigAppSetValues
                                 WHERE  SettingId = (
                                                      SELECT    SettingId
                                                      FROM      dbo.syConfigAppSettings
                                                      WHERE     KeyName = 'PostServicesByClass'
                                                    )
                                        AND CampusId IS NULL
                               );  							  
    SELECT DISTINCT
            ChildResourceId
           ,ChildResource
           ,ResourceTypeId
           ,ParentResourceId
           ,ParentResource
           ,GroupSortOrder
           ,TabId
           ,CASE WHEN (
                        SELECT  COUNT(*)
                        FROM    syNavigationNodes navPage
                        INNER JOIN syNavigationNodes navSubMenu ON navPage.ParentId = navSubMenu.HierarchyId
                        INNER JOIN syNavigationNodes navStudentModule ON navSubMenu.ParentId = navStudentModule.HierarchyId
                        INNER JOIN syNavigationNodes navModule ON navStudentModule.ParentId = navModule.HierarchyId
                        WHERE   navPage.ResourceId = ChildResourceId
                                AND    
 --navSubMenu.ResourceId=740 and     
                                navModule.ResourceId = 26
                      ) >= 1 THEN 1
                 ELSE 0
            END AS isPageInAR
           ,CASE WHEN (
                        SELECT  COUNT(*)
                        FROM    syNavigationNodes navPage
                        INNER JOIN syNavigationNodes navSubMenu ON navPage.ParentId = navSubMenu.HierarchyId
                        INNER JOIN syNavigationNodes navStudentModule ON navSubMenu.ParentId = navStudentModule.HierarchyId
                        INNER JOIN syNavigationNodes navModule ON navStudentModule.ParentId = navModule.HierarchyId
                        WHERE   navPage.ResourceId = ChildResourceId
                                AND    
 --navSubMenu.ResourceId=740 and     
                                navModule.ResourceId = 300
                      ) >= 1 THEN 1
                 ELSE 0
            END AS isPageInFAC
           ,CASE WHEN (
                        SELECT  COUNT(*)
                        FROM    syNavigationNodes navPage
                        INNER JOIN syNavigationNodes navSubMenu ON navPage.ParentId = navSubMenu.HierarchyId
                        INNER JOIN syNavigationNodes navStudentModule ON navSubMenu.ParentId = navStudentModule.HierarchyId
                        INNER JOIN syNavigationNodes navModule ON navStudentModule.ParentId = navModule.HierarchyId
                        WHERE   navPage.ResourceId = ChildResourceId
                                AND    
 --navSubMenu.ResourceId=740 and     
                                navModule.ResourceId = 191
                      ) >= 1 THEN 1
                 ELSE 0
            END AS isPageInFA
           ,CASE WHEN (
                        SELECT  COUNT(*)
                        FROM    syNavigationNodes navPage
                        INNER JOIN syNavigationNodes navSubMenu ON navPage.ParentId = navSubMenu.HierarchyId
                        INNER JOIN syNavigationNodes navStudentModule ON navSubMenu.ParentId = navStudentModule.HierarchyId
                        INNER JOIN syNavigationNodes navModule ON navStudentModule.ParentId = navModule.HierarchyId
                        WHERE   navPage.ResourceId = ChildResourceId
                                AND    
 --navSubMenu.ResourceId=740 and     
                                navModule.ResourceId = 193
                      ) >= 1 THEN 1
                 ELSE 0
            END AS isPageInPL
           ,CASE WHEN (
                        SELECT  COUNT(*)
                        FROM    syNavigationNodes navPage
                        INNER JOIN syNavigationNodes navSubMenu ON navPage.ParentId = navSubMenu.HierarchyId
                        INNER JOIN syNavigationNodes navStudentModule ON navSubMenu.ParentId = navStudentModule.HierarchyId
                        INNER JOIN syNavigationNodes navModule ON navStudentModule.ParentId = navModule.HierarchyId
                        WHERE   navPage.ResourceId = ChildResourceId
                                AND    
 --navSubMenu.ResourceId=740 and     
                                navModule.ResourceId = 194
                      ) >= 1 THEN 1
                 ELSE 0
            END AS isPageInSA
           ,CASE WHEN (
                        SELECT  COUNT(*)
                        FROM    syNavigationNodes navPage
                        INNER JOIN syNavigationNodes navSubMenu ON navPage.ParentId = navSubMenu.HierarchyId
                        INNER JOIN syNavigationNodes navStudentModule ON navSubMenu.ParentId = navStudentModule.HierarchyId
                        INNER JOIN syNavigationNodes navModule ON navStudentModule.ParentId = navModule.HierarchyId
                        WHERE   navPage.ResourceId = ChildResourceId
                                AND    
 --navSubMenu.ResourceId=740 and     
                                navModule.ResourceId = 26
                                AND navPage.IsShipped = 1
                      ) >= 1 THEN 1
                 ELSE 0
            END AS isPageShippedWithAR
           ,CASE WHEN (
                        SELECT  COUNT(*)
                        FROM    syNavigationNodes navPage
                        INNER JOIN syNavigationNodes navSubMenu ON navPage.ParentId = navSubMenu.HierarchyId
                        INNER JOIN syNavigationNodes navStudentModule ON navSubMenu.ParentId = navStudentModule.HierarchyId
                        INNER JOIN syNavigationNodes navModule ON navStudentModule.ParentId = navModule.HierarchyId
                        WHERE   navPage.ResourceId = ChildResourceId
                                AND    
 --navSubMenu.ResourceId=740 and     
                                navModule.ResourceId = 300
                                AND navPage.IsShipped = 1
                      ) >= 1 THEN 1
                 ELSE 0
            END AS isPageShippedWithFAC
           ,CASE WHEN (
                        SELECT  COUNT(*)
                        FROM    syNavigationNodes navPage
                        INNER JOIN syNavigationNodes navSubMenu ON navPage.ParentId = navSubMenu.HierarchyId
                        INNER JOIN syNavigationNodes navStudentModule ON navSubMenu.ParentId = navStudentModule.HierarchyId
                        INNER JOIN syNavigationNodes navModule ON navStudentModule.ParentId = navModule.HierarchyId
                        WHERE   navPage.ResourceId = ChildResourceId
                                AND    
 --navSubMenu.ResourceId=740 and     
                                navModule.ResourceId = 191
                                AND navPage.IsShipped = 1
                      ) >= 1 THEN 1
                 ELSE 0
            END AS isPageShippedWithFA
           ,CASE WHEN (
                        SELECT  COUNT(*)
                        FROM    syNavigationNodes navPage
                        INNER JOIN syNavigationNodes navSubMenu ON navPage.ParentId = navSubMenu.HierarchyId
                        INNER JOIN syNavigationNodes navStudentModule ON navSubMenu.ParentId = navStudentModule.HierarchyId
                        INNER JOIN syNavigationNodes navModule ON navStudentModule.ParentId = navModule.HierarchyId
                        WHERE   navPage.ResourceId = ChildResourceId
                                AND    
 --navSubMenu.ResourceId=740 and     
                                navModule.ResourceId = 193
                                AND navPage.IsShipped = 1
                      ) >= 1 THEN 1
                 ELSE 0
            END AS isPageShippedWithPL
           ,CASE WHEN (
                        SELECT  COUNT(*)
                        FROM    syNavigationNodes navPage
                        INNER JOIN syNavigationNodes navSubMenu ON navPage.ParentId = navSubMenu.HierarchyId
                        INNER JOIN syNavigationNodes navStudentModule ON navSubMenu.ParentId = navStudentModule.HierarchyId
                        INNER JOIN syNavigationNodes navModule ON navStudentModule.ParentId = navModule.HierarchyId
                        WHERE   navPage.ResourceId = ChildResourceId
                                AND    
 --navSubMenu.ResourceId=740 and     
                                navModule.ResourceId = 194
                                AND navPage.IsShipped = 1
                      ) >= 1 THEN 1
                 ELSE 0
            END AS isPageShippedWithSA
    FROM    (
              SELECT    26 AS ModuleResourceId
                       ,394 AS TabId
                       ,ResourceId AS ChildResourceId
                       ,Resource AS ChildResource
                       ,NULL AS AccessLevel
                       ,ResourceURL AS ChildResourceURL
                       ,NULL AS ParentResourceId
                       ,NULL AS ParentResource
                       ,1 AS GroupSortOrder
                       ,NULL AS FirstSortOrder
                       ,NULL AS DisplaySequence
                       ,ResourceTypeID
              FROM      syResources
              WHERE     ResourceId = 26
              UNION
              SELECT    26 AS ModuleResourceId
                       ,394 AS TabId
                       ,ChildResourceId
                       ,ChildResource
                       ,NULL AS AccessLevel
                       ,ChildResourceURL
                       ,ParentResourceId
                       ,ParentResource
                       ,CASE WHEN (
                                    t1.ChildResourceId = 737
                                    OR t1.ParentResourceId IN ( 737 )
                                  ) THEN 1
                             ELSE CASE WHEN (
                                              t1.ChildResourceId = 738
                                              OR t1.ParentResourceId IN ( 738 )
                                            ) THEN 2
                                       ELSE CASE WHEN (
                                                        t1.ChildResourceId = 739
                                                        OR t1.ParentResourceId IN ( 739 )
                                                      ) THEN 3
                                                 ELSE CASE WHEN (
                                                                  t1.ChildResourceId = 740
                                                                  OR t1.ParentResourceId IN ( 740 )
                                                                ) THEN 4
                                                           ELSE CASE WHEN (
                                                                            t1.ChildResourceId = 741
                                                                            OR t1.ParentResourceId IN ( 741 )
                                                                          ) THEN 5
                                                                     ELSE 6
                                                                END
                                                      END
                                            END
                                  END
                        END AS GroupSortOrder
                       ,CASE WHEN ChildResourceId = 738 THEN 1
                             ELSE 2
                        END AS FirstSortOrder
                       ,CASE WHEN (
                                    ChildResourceId IN ( 738 )
                                    OR ParentResourceId IN ( 738 )
                                  ) THEN 1
                             ELSE 2
                        END AS DisplaySequence
                       ,ResourceTypeID
              FROM      (
                          SELECT DISTINCT
                                    NNChild.ResourceId AS ChildResourceId
                                   ,CASE WHEN NNChild.ResourceId = 409 THEN 'IPEDS - General Reports'
                                         ELSE RChild.Resource
                                    END AS ChildResource
                                   ,RChild.ResourceURL AS ChildResourceURL
                                   ,CASE WHEN (
                                                NNParent.ResourceId IN ( 689 )
                                                OR NNChild.ResourceId IN ( 737,738,739,740,741,742 )
                                              ) THEN NULL
                                         ELSE NNParent.ResourceId
                                    END AS ParentResourceId
                                   ,RParent.Resource AS ParentResource
                                   ,RParentModule.Resource AS MODULE
                                   ,RChild.ResourceTypeID
                          FROM      syResources RChild
                          INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
                          INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                          INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                          LEFT OUTER JOIN (
                                            SELECT  *
                                            FROM    syResources
                                            WHERE   ResourceTypeId = 1
                                          ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                          WHERE     RChild.ResourceTypeId IN ( 2,3,8 )
                                    AND (
                                          RChild.ChildTypeId IS NULL
                                          OR RChild.ChildTypeId = 3
                                        )
                                    AND ( NNParent.ParentId IN ( SELECT HierarchyId
                                                                 FROM   syNavigationNodes
                                                                 WHERE  ResourceId = 394
                                                                        AND ParentId IN ( SELECT    HierarchyId
                                                                                          FROM      syNavigationNodes
                                                                                          WHERE     ResourceId = 26 ) ) )    
	--Look at SchoolSelectedOptions function in App_Code/CommonWebUtilities to     
	--check how @schoolEnumerator value is built    
	--The value is dynamically built based on school config setting    
	--we perform a bitwise AND operation of two integer values    
                                    AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
                          UNION
                          SELECT DISTINCT
                                    RChild.ResourceId AS ChildResourceId
                                   ,RChild.Resource AS ChildResource
                                   ,RChild.ResourceURL AS ChildResourceURL
                                   ,CASE WHEN ( RChild.ResourceId IN ( 737,738,739,740,741,742 ) ) THEN NULL
                                         ELSE RChild.ResourceId
                                    END AS ParentResourceId
                                   ,NULL AS ParentResource
                                   ,NULL AS MODULE
                                   ,RChild.ResourceTypeID
                          FROM      syResources RChild
                          WHERE     RChild.ResourceId IN ( 737,738,739,740,741,742 )
                        ) t1
              UNION ALL
              SELECT    194 AS ModuleResourceId
                       ,394 AS TabId
                       ,ResourceId AS ChildResourceId
                       ,Resource AS ChildResource
                       ,NULL AS AccessLevel
                       ,ResourceURL AS ChildResourceURL
                       ,NULL AS ParentResourceId
                       ,NULL AS ParentResource
                       ,1 AS GroupSortOrder
                       ,NULL AS FirstSortOrder
                       ,NULL AS DisplaySequence
                       ,ResourceTypeID
              FROM      syResources
              WHERE     ResourceId = 194
              UNION
              SELECT    194 AS ModuleResourceId
                       ,394 AS TabId
                       ,ChildResourceId
                       ,ChildResource
                       ,NULL AS AccessLevel
                       ,ChildResourceURL
                       ,ParentResourceId
                       ,ParentResource
                       ,CASE WHEN (
                                    t1.ChildResourceId = 737
                                    OR t1.ParentResourceId IN ( 737 )
                                  ) THEN 1
                             ELSE CASE WHEN (
                                              t1.ChildResourceId = 738
                                              OR t1.ParentResourceId IN ( 738 )
                                            ) THEN 2
                                       ELSE CASE WHEN (
                                                        t1.ChildResourceId = 739
                                                        OR t1.ParentResourceId IN ( 739 )
                                                      ) THEN 3
                                                 ELSE CASE WHEN (
                                                                  t1.ChildResourceId = 740
                                                                  OR t1.ParentResourceId IN ( 740 )
                                                                ) THEN 4
                                                           ELSE CASE WHEN (
                                                                            t1.ChildResourceId = 741
                                                                            OR t1.ParentResourceId IN ( 741 )
                                                                          ) THEN 5
                                                                     ELSE 6
                                                                END
                                                      END
                                            END
                                  END
                        END AS GroupSortOrder
                       ,CASE WHEN ChildResourceId = 737 THEN 1
                             ELSE 2
                        END AS FirstSortOrder
                       ,CASE WHEN (
                                    ChildResourceId IN ( 737,738,740,741 )
                                    OR ParentResourceId IN ( 737,738,740,741 )
                                  ) THEN 1
                             ELSE 2
                        END AS DisplaySequence
                       ,ResourceTypeID
              FROM      (
                          SELECT DISTINCT
                                    NNChild.ResourceId AS ChildResourceId
                                   ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                         ELSE RChild.Resource
                                    END AS ChildResource
                                   ,RChild.ResourceURL AS ChildResourceURL
                                   ,CASE WHEN NNParent.ResourceId IN ( 194 )
                                              OR NNChild.ResourceId IN ( 737,738,739,740,741,742 ) THEN NULL
                                         ELSE NNParent.ResourceId
                                    END AS ParentResourceId
                                   ,RParent.Resource AS ParentResource
                                   ,RParentModule.Resource AS MODULE
                                   ,RChild.ResourceTypeID
                          FROM      syResources RChild
                          INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
                          INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                          INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                          LEFT OUTER JOIN (
                                            SELECT  *
                                            FROM    syResources
                                            WHERE   ResourceTypeId = 1
                                          ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                          WHERE     RChild.ResourceTypeId IN ( 2,3,8 )
                                    AND (
                                          RChild.ChildTypeId IS NULL
                                          OR RChild.ChildTypeId = 3
                                        )
                                    AND ( NNParent.ParentId IN ( SELECT HierarchyId
                                                                 FROM   syNavigationNodes
                                                                 WHERE  ResourceId = 394
                                                                        AND ParentId IN ( SELECT    HierarchyId
                                                                                          FROM      syNavigationNodes
                                                                                          WHERE     ResourceId = 194 ) ) )    
	--Look at SchoolSelectedOptions function in App_Code/CommonWebUtilities to     
	--check how @schoolEnumerator value is built    
	--The value is dynamically built based on school config setting    
	--we perform a bitwise AND operation of two integer values    
                                    AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
                          UNION
                          SELECT DISTINCT
                                    RChild.ResourceId AS ChildResourceId
                                   ,RChild.Resource AS ChildResource
                                   ,RChild.ResourceURL AS ChildResourceURL
                                   ,CASE WHEN ( RChild.ResourceId IN ( 737,738,739,740,741,742 ) ) THEN NULL
                                         ELSE RChild.ResourceId
                                    END AS ParentResourceId
                                   ,NULL AS ParentResource
                                   ,NULL AS MODULE
                                   ,RChild.ResourceTypeID
                          FROM      syResources RChild
                          WHERE     RChild.ResourceId IN ( 737,738,739,740,741,742 )
                        ) t1
              UNION ALL
              SELECT    300 AS ModuleResourceId
                       ,394 AS TabId
                       ,ResourceId AS ChildResourceId
                       ,Resource AS ChildResource
                       ,NULL AS AccessLevel
                       ,ResourceURL AS ChildResourceURL
                       ,NULL AS ParentResourceId
                       ,NULL AS ParentResource
                       ,1 AS GroupSortOrder
                       ,NULL AS FirstSortOrder
                       ,NULL AS DisplaySequence
                       ,ResourceTypeID
              FROM      syResources
              WHERE     ResourceId = 300
              UNION
              SELECT    300 AS ModuleResourceId
                       ,394 AS TabId
                       ,ChildResourceId
                       ,ChildResource
                       ,NULL AS AccessLevel
                       ,ChildResourceURL
                       ,ParentResourceId
                       ,ParentResource
                       ,CASE WHEN (
                                    t1.ChildResourceId = 737
                                    OR t1.ParentResourceId IN ( 737 )
                                  ) THEN 1
                             ELSE CASE WHEN (
                                              t1.ChildResourceId = 738
                                              OR t1.ParentResourceId IN ( 738 )
                                            ) THEN 2
                                       ELSE CASE WHEN (
                                                        t1.ChildResourceId = 739
                                                        OR t1.ParentResourceId IN ( 739 )
                                                      ) THEN 3
                                                 ELSE CASE WHEN (
                                                                  t1.ChildResourceId = 740
                                                                  OR t1.ParentResourceId IN ( 740 )
                                                                ) THEN 4
                                                           ELSE CASE WHEN (
                                                                            t1.ChildResourceId = 741
                                                                            OR t1.ParentResourceId IN ( 741 )
                                                                          ) THEN 5
                                                                     ELSE 6
                                                                END
                                                      END
                                            END
                                  END
                        END AS GroupSortOrder
                       ,CASE WHEN ChildResourceId = 737 THEN 1
                             ELSE 2
                        END AS FirstSortOrder
                       ,CASE WHEN (
                                    ChildResourceId IN ( 737,739 )
                                    OR ParentResourceId IN ( 737,739 )
                                  ) THEN 1
                             ELSE 2
                        END AS DisplaySequence
                       ,ResourceTypeID
              FROM      (
                          SELECT DISTINCT
                                    NNChild.ResourceId AS ChildResourceId
                                   ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                         ELSE RChild.Resource
                                    END AS ChildResource
                                   ,RChild.ResourceURL AS ChildResourceURL
                                   ,CASE WHEN NNParent.ResourceId = 300
                                              OR NNChild.ResourceId IN ( 737,739,740,741 ) THEN NULL
                                         ELSE NNParent.ResourceId
                                    END AS ParentResourceId
                                   ,RParent.Resource AS ParentResource
                                   ,RParentModule.Resource AS MODULE
                                   ,RChild.ResourceTypeID
                          FROM      syResources RChild
                          INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
                          INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                          INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                          LEFT OUTER JOIN (
                                            SELECT  *
                                            FROM    syResources
                                            WHERE   ResourceTypeId = 1
                                          ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                          WHERE     RChild.ResourceTypeId IN ( 2,3,8 )
                                    AND (
                                          RChild.ChildTypeId IS NULL
                                          OR RChild.ChildTypeId = 3
                                        )
                                    AND ( NNParent.ParentId IN ( SELECT HierarchyId
                                                                 FROM   syNavigationNodes
                                                                 WHERE  ResourceId = 394
                                                                        AND ParentId IN ( SELECT    HierarchyId
                                                                                          FROM      syNavigationNodes
                                                                                          WHERE     ResourceId = 300 ) ) )    
	--Look at SchoolSelectedOptions function in App_Code/CommonWebUtilities to     
	--check how @schoolEnumerator value is built    
	--The value is dynamically built based on school config setting    
	--we perform a bitwise AND operation of two integer values    
                                    AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
                          UNION
                          SELECT DISTINCT
                                    RChild.ResourceId AS ChildResourceId
                                   ,RChild.Resource AS ChildResource
                                   ,RChild.ResourceURL AS ChildResourceURL
                                   ,CASE WHEN ( RChild.ResourceId IN ( 737,738,739,740,741,742 ) ) THEN NULL
                                         ELSE RChild.ResourceId
                                    END AS ParentResourceId
                                   ,NULL AS ParentResource
                                   ,NULL AS MODULE
                                   ,RChild.ResourceTypeID
                          FROM      syResources RChild
                          WHERE     RChild.ResourceId IN ( 737,738,739,740,741,742 )
                        ) t1
              UNION
              SELECT    191 AS ModuleResourceId
                       ,394 AS TabId
                       ,ResourceId AS ChildResourceId
                       ,Resource AS ChildResource
                       ,NULL AS AccessLevel
                       ,ResourceURL AS ChildResourceURL
                       ,NULL AS ParentResourceId
                       ,NULL AS ParentResource
                       ,1 AS GroupSortOrder
                       ,NULL AS FirstSortOrder
                       ,NULL AS DisplaySequence
                       ,ResourceTypeID
              FROM      syResources
              WHERE     ResourceId = 191
              UNION
              SELECT    191 AS ModuleResourceId
                       ,394 AS TabId
                       ,ChildResourceId
                       ,ChildResource
                       ,NULL AS AccessLevel
                       ,ChildResourceURL
                       ,ParentResourceId
                       ,ParentResource
                       ,CASE WHEN (
                                    t1.ChildResourceId = 737
                                    OR t1.ParentResourceId IN ( 737 )
                                  ) THEN 1
                             ELSE CASE WHEN (
                                              t1.ChildResourceId = 738
                                              OR t1.ParentResourceId IN ( 738 )
                                            ) THEN 2
                                       ELSE CASE WHEN (
                                                        t1.ChildResourceId = 739
                                                        OR t1.ParentResourceId IN ( 739 )
                                                      ) THEN 3
                                                 ELSE CASE WHEN (
                                                                  t1.ChildResourceId = 740
                                                                  OR t1.ParentResourceId IN ( 740 )
                                                                ) THEN 4
                                                           ELSE CASE WHEN (
                                                                            t1.ChildResourceId = 741
                                                                            OR t1.ParentResourceId IN ( 741 )
                                                                          ) THEN 5
                                                                     ELSE 6
                                                                END
                                                      END
                                            END
                                  END
                        END AS GroupSortOrder
                       ,CASE WHEN ChildResourceId = 737 THEN 1
                             ELSE 2
                        END AS FirstSortOrder
                       ,CASE WHEN (
                                    ChildResourceId IN ( 737,738,740,741 )
                                    OR ParentResourceId IN ( 737,738,740,741 )
                                  ) THEN 1
                             ELSE 2
                        END AS DisplaySequence
                       ,ResourceTypeID
              FROM      (
                          SELECT DISTINCT
                                    NNChild.ResourceId AS ChildResourceId
                                   ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                         ELSE RChild.Resource
                                    END AS ChildResource
                                   ,RChild.ResourceURL AS ChildResourceURL
                                   ,CASE WHEN (
                                                NNParent.ResourceId = 191
                                                OR NNChild.ResourceId IN ( 737,738,739,740,741,742 )
                                              ) THEN NULL
                                         ELSE NNParent.ResourceId
                                    END AS ParentResourceId
                                   ,RParent.Resource AS ParentResource
                                   ,RParentModule.Resource AS MODULE
                                   ,RChild.ResourceTypeID
                          FROM      syResources RChild
                          INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
                          INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                          INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                          LEFT OUTER JOIN (
                                            SELECT  *
                                            FROM    syResources
                                            WHERE   ResourceTypeId = 1
                                          ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                          WHERE     RChild.ResourceTypeId IN ( 2,3,8 )
                                    AND (
                                          RChild.ChildTypeId IS NULL
                                          OR RChild.ChildTypeId = 3
                                        )
                                    AND ( NNParent.ParentId IN ( SELECT HierarchyId
                                                                 FROM   syNavigationNodes
                                                                 WHERE  ResourceId = 394
                                                                        AND ParentId IN ( SELECT    HierarchyId
                                                                                          FROM      syNavigationNodes
                                                                                          WHERE     ResourceId = 191 ) ) )    
	--Look at SchoolSelectedOptions function in App_Code/CommonWebUtilities to     
	--check how @schoolEnumerator value is built    
	--The value is dynamically built based on school config setting    
	--we perform a bitwise AND operation of two integer values    
                                    AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
                          UNION
                          SELECT DISTINCT
                                    RChild.ResourceId AS ChildResourceId
                                   ,RChild.Resource AS ChildResource
                                   ,RChild.ResourceURL AS ChildResourceURL
                                   ,CASE WHEN ( RChild.ResourceId IN ( 737,738,739,740,741,742 ) ) THEN NULL
                                         ELSE RChild.ResourceId
                                    END AS ParentResourceId
                                   ,NULL AS ParentResource
                                   ,NULL AS MODULE
                                   ,RChild.ResourceTypeID
                          FROM      syResources RChild
                          WHERE     RChild.ResourceId IN ( 737,738,739,740,741,742 )
                        ) t1
              UNION
              SELECT    193 AS ModuleResourceId
                       ,394 AS TabId
                       ,ResourceId AS ChildResourceId
                       ,Resource AS ChildResource
                       ,NULL AS AccessLevel
                       ,ResourceURL AS ChildResourceURL
                       ,NULL AS ParentResourceId
                       ,NULL AS ParentResource
                       ,1 AS GroupSortOrder
                       ,NULL AS FirstSortOrder
                       ,NULL AS DisplaySequence
                       ,ResourceTypeID
              FROM      syResources
              WHERE     ResourceId = 193
              UNION
              SELECT    193 AS ModuleResourceId
                       ,394 AS TabId
                       ,ChildResourceId
                       ,ChildResource
                       ,NULL AS AccessLevel
                       ,ChildResourceURL
                       ,ParentResourceId
                       ,ParentResource
                       ,CASE WHEN (
                                    t1.ChildResourceId = 737
                                    OR t1.ParentResourceId IN ( 737 )
                                  ) THEN 1
                             ELSE CASE WHEN (
                                              t1.ChildResourceId = 738
                                              OR t1.ParentResourceId IN ( 738 )
                                            ) THEN 2
                                       ELSE CASE WHEN (
                                                        t1.ChildResourceId = 739
                                                        OR t1.ParentResourceId IN ( 739 )
                                                      ) THEN 3
                                                 ELSE CASE WHEN (
                                                                  t1.ChildResourceId = 740
                                                                  OR t1.ParentResourceId IN ( 740 )
                                                                ) THEN 4
                                                           ELSE CASE WHEN (
                                                                            t1.ChildResourceId = 741
                                                                            OR t1.ParentResourceId IN ( 741 )
                                                                          ) THEN 5
                                                                     ELSE 6
                                                                END
                                                      END
                                            END
                                  END
                        END AS GroupSortOrder
                       ,CASE WHEN ChildResourceId = 737 THEN 1
                             ELSE 2
                        END AS FirstSortOrder
                       ,CASE WHEN (
                                    ChildResourceId IN ( 737,738,740,741,742 )
                                    OR ParentResourceId IN ( 737,738,740,741,742 )
                                  ) THEN 1
                             ELSE 2
                        END AS DisplaySequence
                       ,ResourceTypeID
              FROM      (
                          SELECT DISTINCT
                                    NNChild.ResourceId AS ChildResourceId
                                   ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                         ELSE RChild.Resource
                                    END AS ChildResource
                                   ,RChild.ResourceURL AS ChildResourceURL
                                   ,CASE WHEN (
                                                NNParent.ResourceId = 193
                                                OR NNChild.ResourceId IN ( 713,735 )
                                              ) THEN NULL
                                         ELSE NNParent.ResourceId
                                    END AS ParentResourceId
                                   ,RParent.Resource AS ParentResource
                                   ,RParentModule.Resource AS MODULE
                                   ,RChild.ResourceTypeID
                          FROM      syResources RChild
                          INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
                          INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                          INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                          LEFT OUTER JOIN (
                                            SELECT  *
                                            FROM    syResources
                                            WHERE   ResourceTypeId = 1
                                          ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                          WHERE     RChild.ResourceTypeId IN ( 2,3,8 )
                                    AND (
                                          RChild.ChildTypeId IS NULL
                                          OR RChild.ChildTypeId = 3
                                        )
                                    AND ( NNParent.ParentId IN ( SELECT HierarchyId
                                                                 FROM   syNavigationNodes
                                                                 WHERE  ResourceId = 394
                                                                        AND ParentId IN ( SELECT    HierarchyId
                                                                                          FROM      syNavigationNodes
                                                                                          WHERE     ResourceId = 193 ) ) )    
	--Look at SchoolSelectedOptions function in App_Code/CommonWebUtilities to     
	--check how @schoolEnumerator value is built    
	--The value is dynamically built based on school config setting    
	--we perform a bitwise AND operation of two integer values    
                                    AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
                          UNION
                          SELECT DISTINCT
                                    RChild.ResourceId AS ChildResourceId
                                   ,RChild.Resource AS ChildResource
                                   ,RChild.ResourceURL AS ChildResourceURL
                                   ,CASE WHEN ( RChild.ResourceId IN ( 737,738,739,741,740,742 ) ) THEN NULL
                                         ELSE RChild.ResourceId
                                    END AS ParentResourceId
                                   ,NULL AS ParentResource
                                   ,NULL AS MODULE
                                   ,RChild.ResourceTypeID
                          FROM      syResources RChild
                          WHERE     RChild.ResourceId IN ( 737,738,739,740,741,742 )
                        ) t1
              UNION
              SELECT    193 AS ModuleResourceId
                       ,397 AS TabId
                       ,ResourceId AS ChildResourceId
                       ,Resource AS ChildResource
                       ,NULL AS AccessLevel
                       ,ResourceURL AS ChildResourceURL
                       ,NULL AS ParentResourceId
                       ,NULL AS ParentResource
                       ,1 AS GroupSortOrder
                       ,NULL AS FirstSortOrder
                       ,NULL AS DisplaySequence
                       ,ResourceTypeID
              FROM      syResources
              WHERE     ResourceId = 193
              UNION
              SELECT    193 AS ModuleResourceId
                       ,397 AS TabId
                       ,ChildResourceId
                       ,ChildResource
                       ,NULL AS AccessLevel
                       ,ChildResourceURL
                       ,ParentResourceId
                       ,ParentResource
                       ,CASE WHEN (
                                    t1.ChildResourceId = 737
                                    OR t1.ParentResourceId IN ( 737 )
                                  ) THEN 1
                             ELSE CASE WHEN (
                                              t1.ChildResourceId = 738
                                              OR t1.ParentResourceId IN ( 738 )
                                            ) THEN 2
                                       ELSE CASE WHEN (
                                                        t1.ChildResourceId = 739
                                                        OR t1.ParentResourceId IN ( 739 )
                                                      ) THEN 3
                                                 ELSE CASE WHEN (
                                                                  t1.ChildResourceId = 740
                                                                  OR t1.ParentResourceId IN ( 740 )
                                                                ) THEN 4
                                                           ELSE CASE WHEN (
                                                                            t1.ChildResourceId = 741
                                                                            OR t1.ParentResourceId IN ( 741 )
                                                                          ) THEN 5
                                                                     ELSE 6
                                                                END
                                                      END
                                            END
                                  END
                        END AS GroupSortOrder
                       ,CASE WHEN ChildResourceId = 737 THEN 1
                             ELSE 2
                        END AS FirstSortOrder
                       ,CASE WHEN (
                                    ChildResourceId IN ( 737 )
                                    OR ParentResourceId IN ( 737 )
                                  ) THEN 1
                             ELSE 2
                        END AS DisplaySequence
                       ,ResourceTypeID
              FROM      (
                          SELECT DISTINCT
                                    NNChild.ResourceId AS ChildResourceId
                                   ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                         ELSE RChild.Resource
                                    END AS ChildResource
                                   ,RChild.ResourceURL AS ChildResourceURL
                                   ,CASE WHEN (
                                                NNParent.ResourceId = 193
                                                OR NNChild.ResourceId IN ( 737 )
                                              ) THEN NULL
                                         ELSE NNParent.ResourceId
                                    END AS ParentResourceId
                                   ,RParent.Resource AS ParentResource
                                   ,RParentModule.Resource AS MODULE
                                   ,RChild.ResourceTypeID
                          FROM      syResources RChild
                          INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
                          INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                          INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                          LEFT OUTER JOIN (
                                            SELECT  *
                                            FROM    syResources
                                            WHERE   ResourceTypeId = 1
                                          ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                          WHERE     RChild.ResourceTypeId IN ( 2,3,8 )
                                    AND (
                                          RChild.ChildTypeId IS NULL
                                          OR RChild.ChildTypeId = 3
                                        )
                                    AND ( NNParent.ParentId IN ( SELECT HierarchyId
                                                                 FROM   syNavigationNodes
                                                                 WHERE  ResourceId = 397
                                                                        AND ParentId IN ( SELECT    HierarchyId
                                                                                          FROM      syNavigationNodes
                                                                                          WHERE     ResourceId = 193 ) ) )    
	--Look at SchoolSelectedOptions function in App_Code/CommonWebUtilities to     
	--check how @schoolEnumerator value is built    
	--The value is dynamically built based on school config setting    
	--we perform a bitwise AND operation of two integer values    
                                    AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
                          UNION
                          SELECT DISTINCT
                                    RChild.ResourceId AS ChildResourceId
                                   ,RChild.Resource AS ChildResource
                                   ,RChild.ResourceURL AS ChildResourceURL
                                   ,CASE WHEN ( RChild.ResourceId IN ( 737 ) ) THEN NULL
                                         ELSE RChild.ResourceId
                                    END AS ParentResourceId
                                   ,NULL AS ParentResource
                                   ,NULL AS MODULE
                                   ,RChild.ResourceTypeID
                          FROM      syResources RChild
                          WHERE     RChild.ResourceId IN ( 737 )
                        ) t1
              UNION
              SELECT    192 AS ModuleResourceId
                       ,396 AS TabId
                       ,ResourceId AS ChildResourceId
                       ,Resource AS ChildResource
                       ,NULL AS AccessLevel
                       ,ResourceURL AS ChildResourceURL
                       ,NULL AS ParentResourceId
                       ,NULL AS ParentResource
                       ,1 AS GroupSortOrder
                       ,NULL AS FirstSortOrder
                       ,NULL AS DisplaySequence
                       ,ResourceTypeID
              FROM      syResources
              WHERE     ResourceId = 192
              UNION
              SELECT    192 AS ModuleResourceId
                       ,396 AS TabId
                       ,ChildResourceId
                       ,ChildResource
                       ,NULL AS AccessLevel
                       ,ChildResourceURL
                       ,ParentResourceId
                       ,ParentResource
                       ,CASE WHEN (
                                    ChildResourceId IN ( 713 )
                                    OR ParentResourceId IN ( 713 )
                                  ) THEN 1
                             ELSE 2
                        END AS GroupSortOrder
                       ,CASE WHEN (
                                    ChildResourceId IN ( 713 )
                                    OR ParentResourceId IN ( 713 )
                                  ) THEN 1
                             ELSE 2
                        END AS FirstSortOrder
                       ,CASE WHEN (
                                    ChildResourceId IN ( 713 )
                                    OR ParentResourceId IN ( 713 )
                                  ) THEN 1
                             ELSE 2
                        END AS DisplaySequence
                       ,ResourceTypeID
              FROM      (
                          SELECT DISTINCT
                                    NNChild.ResourceId AS ChildResourceId
                                   ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                         ELSE RChild.Resource
                                    END AS ChildResource
                                   ,RChild.ResourceURL AS ChildResourceURL
                                   ,CASE WHEN (
                                                NNParent.ResourceId = 193
                                                OR NNChild.ResourceId IN ( 713,735 )
                                              ) THEN NULL
                                         ELSE NNParent.ResourceId
                                    END AS ParentResourceId
                                   ,RParent.Resource AS ParentResource
                                   ,RParentModule.Resource AS MODULE
                                   ,RChild.ResourceTypeID
                          FROM      syResources RChild
                          INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
                          INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                          INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                          LEFT OUTER JOIN (
                                            SELECT  *
                                            FROM    syResources
                                            WHERE   ResourceTypeId = 1
                                          ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                          WHERE     RChild.ResourceTypeId IN ( 2,3,8 )
                                    AND (
                                          RChild.ChildTypeId IS NULL
                                          OR RChild.ChildTypeId = 3
                                        )
                                    AND ( NNParent.ParentId IN ( SELECT HierarchyId
                                                                 FROM   syNavigationNodes
                                                                 WHERE  ResourceId = 396
                                                                        AND ParentId IN ( SELECT    HierarchyId
                                                                                          FROM      syNavigationNodes
                                                                                          WHERE     ResourceId = 192 ) ) )    
	--Look at SchoolSelectedOptions function in App_Code/CommonWebUtilities to     
	--check how @schoolEnumerator value is built    
	--The value is dynamically built based on school config setting    
	--we perform a bitwise AND operation of two integer values    
                                    AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
                          UNION
                          SELECT DISTINCT
                                    RChild.ResourceId AS ChildResourceId
                                   ,RChild.Resource AS ChildResource
                                   ,RChild.ResourceURL AS ChildResourceURL
                                   ,CASE WHEN ( RChild.ResourceId IN ( 737 ) ) THEN NULL
                                         ELSE RChild.ResourceId
                                    END AS ParentResourceId
                                   ,NULL AS ParentResource
                                   ,NULL AS MODULE
                                   ,RChild.ResourceTypeID
                          FROM      syResources RChild
                          WHERE     RChild.ResourceId IN ( 737 )
                        ) t1
            ) t2
    WHERE   --t2.ModuleResourceId=@ModuleResourceId AND    
            t2.TabId = 394
            AND ChildResourceId NOT IN ( 26,300,191,193,195,194,287,288 )
            AND (
                  -- The following expression means if showross... is set to false, hide       
	 -- ResourceIds 541,542,532,534,535,538,543,539      
	 -- (Not False) OR (Condition)   
	 -- US4330 removed Resourceid 541   
				  (
                    (
                      NOT LTRIM(RTRIM(@ShowRossOnlyTabs)) = 0
                    )
                    OR ( ChildResourceId NOT IN ( 542,532,538 ) )
                  )
                  AND      
	 -- The following expression means if showross... is set to true, hide       
	 -- ResourceIds 142,375,330,476,508,102,107,237      
	 -- (Not True) OR (Condition)      
                  (
                    (
                      NOT LTRIM(RTRIM(@ShowRossOnlyTabs)) = 1
                    )
                    OR ( ChildResourceId NOT IN ( 142,375,330,476,508,102,107,237,230,286,200,217,290 ) )
                  )
                  AND     
	 -- US4330 added		  
	 -- The following expression means if PostServices is set to false or showross is set to false, hide            
	 -- ResourceId 541 , 508    
	 -- (Not False) OR (Condition)      
                  (
                    (
                      (
                        NOT LTRIM(RTRIM(@PostServicesByStudent)) = 0
                      )
                      OR ( ChildResourceId NOT IN ( 541 ) )
                    )
                    OR (
                         (
                           NOT LTRIM(RTRIM(@ShowRossOnlyTabs)) = 0
                         )
                         OR ( ChildResourceId NOT IN ( 541 ) )
                       )
                  )
                  AND (
                        (
                          (
                            NOT LTRIM(RTRIM(@PostServicesByClass)) = 0
                          )
                          OR ( ChildResourceId NOT IN ( 508 ) )
                        )
                        OR (
                             (
                               NOT LTRIM(RTRIM(@ShowRossOnlyTabs)) = 0
                             )
                             OR ( ChildResourceId NOT IN ( 508 ) )
                           )
                      )
                  AND
					  				  
		   ---- The following expression means if @ShowCollegeOfCourtReporting is set to false, hide         
		   ---- ResourceIds 614,615        
		   ---- (Not False) OR (Condition)        
                  (
                    (
                      NOT LOWER(LTRIM(RTRIM(@ShowCollegeOfCourtReporting))) = 'no'
                    )
                    OR ( ChildResourceId NOT IN ( 614,615 ) )
                  )
                  AND (
                        (
                          NOT LOWER(LTRIM(RTRIM(@ShowExternshipTabs))) = 'no'
                        )      
			   --OR ( NNChild.ResourceId NOT IN ( 543, 538 ) )      
                        OR ( ChildResourceId NOT IN ( 543 ) )
                      )
                )
    ORDER BY GroupSortOrder
           ,ParentResourceId
           ,ChildResource;  
			



GO
