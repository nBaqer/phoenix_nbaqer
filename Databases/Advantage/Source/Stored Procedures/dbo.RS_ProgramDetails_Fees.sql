SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[RS_ProgramDetails_Fees]
	-- Add the parameters for the stored procedure here
    @PrgVerId UNIQUEIDENTIFIER = 'FAFEC069-D168-467A-9BD0-D8B7F831C812' -- Electrolysis
AS
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;

        SELECT  unit.UnitDescrip
               ,transc.TransCodeDescrip
               ,fee.Amount
               ,tui.TuitionCategoryDescrip
               ,sche.RateScheduleDescrip
        FROM    saProgramVersionFees fee
        LEFT JOIN syProgramUnitTypes unit ON unit.UnitId = fee.UnitId
        LEFT JOIN saTransCodes transc ON transc.TransCodeId = fee.TransCodeId
        LEFT JOIN saTuitionCategories tui ON tui.TuitionCategoryId = fee.TuitionCategoryId
        LEFT JOIN saRateSchedules sche ON sche.RateScheduleId = fee.RateScheduleId
        JOIN    syStatuses sta ON sta.StatusId = fee.StatusId
        WHERE   fee.PrgVerId = @PrgVerId
                AND sta.StatusCode = 'A'
        ORDER BY transc.TransCodeDescrip
               ,fee.Amount DESC;
    END;



GO
