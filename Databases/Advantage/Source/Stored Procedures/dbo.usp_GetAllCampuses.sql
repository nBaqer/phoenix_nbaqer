SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_GetAllCampuses]
AS
    SET NOCOUNT ON;      
    SELECT  CampusId
           ,CampDescrip
    FROM    syCampuses
           ,syStatuses
    WHERE   syCampuses.StatusId = syStatuses.StatusId
            AND LOWER(Status) = 'active'
    ORDER BY CampDescrip;      



GO
