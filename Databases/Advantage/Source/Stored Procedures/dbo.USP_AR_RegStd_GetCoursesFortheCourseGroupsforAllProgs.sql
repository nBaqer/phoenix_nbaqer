SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[USP_AR_RegStd_GetCoursesFortheCourseGroupsforAllProgs]
AS /*----------------------------------------------------------------------------------------------------
	Author          :	Saraswathi Lakshmanan
    
    Create date		:	01/28/2010
    
	Procedure Name	:	USP_AR_RegStd_GetCoursesFortheCourseGroupsforAllProgs

	Objective		:	get the courses and their Course Groups
	
	Parameters		:	Name			Type	Data Type			
						=====			====	=========			
						
							
	
	Output			:	Returns the ReqID 
	
		
						
*/-----------------------------------------------------------------------------------------------------

--Stored procedure created by Saraswathi Lakhsmanan on 1/25/10
--To fix issue 17548: ENH: All: Register Students Page is Taking Too Long to Populate Available Students 
/*
Create a variable table to store the ReqId and GrpId
The first part will find all the first level of ReqId and GrpIds for a given ProgramId
the second part- Which has the recursive insert  will get all the Reqids for the given Grpid.
i.e the Courses for the course groups are found. This is multilevel- a course group can have another crourse group and courses.
*/

/* Modified by Saraswathi On feb 17 2010*/
--For all rpograms need not do the loop structure multiple times
/*DECLARE @VariableReqtable Table 
	(
		ReqId uniqueidentifier,
		GrpID uniqueIdentifier,
		ID int identity
	) 
INSERT INTO 
	@VariableReqtable 
	Select 
		arReqGrpDef.Reqid,
		arReqGrpDef.GrpID 
	from 
		arReqGrpDef inner Join (
								Select 
									t3.ReqId 
								from 
									arReqs t3 inner Join arProgVerDef t4 on t3.ReqId = t4.ReqId
									inner Join arPrgVersions t6 on t4.Prgverid=t6.PrgVerID 
								WHERE 
									t3.ReqTypeId = 2 AND 
									--t6.ProgId = @ProgId AND For all programs
									t6.IsContinuingEd = 0 
								)Table1 on Table1.ReqID=arReqGrpDef.GrpId
	
Declare @MaxRowCounter Int,@RowCounter int
Set @MaxRowCounter=(Select max(ID) from @VariableReqtable)
Set @RowCounter=1
while (@RowCounter<= @MaxRowCounter)
Begin
     INSERT INTO 
		@VariableReqtable 
			Select 
				Reqid,
				GrpId 
			from 
				arReqGrpDef 
			where 
				GrpID in(	
						Select 
							ReqID 
						from 
							@VariableReqtable 
						where 
							ID=@RowCounter
						) 
   
      Set @RowCounter=@RowCounter+1
      Set @MaxRowCounter=(Select max(ID) from @VariableReqtable)
End

 
Select Reqid from @VariableReqtable
union 
Select GrpId from @VariableReqtable
*/
    SELECT  arReqGrpDef.Reqid
           ,arReqGrpDef.GrpID
    FROM    arReqGrpDef
    INNER JOIN (
                 SELECT t3.ReqId
                 FROM   arReqs t3
                 INNER JOIN arProgVerDef t4 ON t3.ReqId = t4.ReqId
                 INNER JOIN arPrgVersions t6 ON t4.Prgverid = t6.PrgVerID
                 WHERE  t3.ReqTypeId = 2
                        AND 
									--t6.ProgId = @ProgId AND For all programs
                        t6.IsContinuingEd = 0
               ) Table1 ON Table1.ReqID = arReqGrpDef.GrpId;




GO
