SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ginzo, John
-- Create date: 10/17/2014
-- Description:	Retrieve the entity name based on entity type
-- =============================================
CREATE PROCEDURE [dbo].[GetEntityNameByEntityType]
    @EntityId UNIQUEIDENTIFIER
   ,@EntityType VARCHAR(50)
AS
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;

        IF @EntityType = 'LEAD'
            BEGIN
                SELECT  ( FirstName + ' ' + LastName ) AS FullName
                FROM    adLeads
                WHERE   LeadId = @EntityId;
            END;
        ELSE
            IF @EntityType = 'STUDENT'
                BEGIN
                    SELECT  ( FirstName + ' ' + LastName ) AS FullName
                    FROM    arStudent
                    WHERE   StudentId = @EntityId;
                END; 
            ELSE
                IF @EntityType = 'EMPLOYER'
                    BEGIN
                        SELECT  ( EmployerDescrip ) AS FullName
                        FROM    dbo.plEmployers
                        WHERE   EmployerId = @EntityId;
                    END; 
                ELSE
                    IF @EntityType = 'EMPLOYEE'
                        BEGIN
                            SELECT  ( FirstName + ' ' + LastName ) AS FullName
                            FROM    dbo.hrEmployees
                            WHERE   EmpId = @EntityId;
                        END; 
                    ELSE
                        BEGIN
                            SELECT  '' AS FullName;
                        END;
    END;




GO
