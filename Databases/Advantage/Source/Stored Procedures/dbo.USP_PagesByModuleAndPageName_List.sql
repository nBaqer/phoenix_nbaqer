SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_PagesByModuleAndPageName_List]
    @ModuleId INT = 0
   ,@PageName VARCHAR(50) = NULL
AS
    SELECT DISTINCT
            t5.resourceId
           ,t5.Resource
    FROM    syNavigationNodes t1
    INNER JOIN syNavigationNodes t2 ON t1.ParentId = t2.HierarchyId
    INNER JOIN syNavigationNodes t3 ON t2.ParentId = t3.HierarchyId
    INNER JOIN syNavigationNodes t4 ON t3.ParentId = t4.HierarchyId
    INNER JOIN syResources t5 ON t1.ResourceId = t5.ResourceID
    WHERE   (
              @ModuleId = 0
              OR t4.ResourceId = @ModuleId
            )
            AND (
                  @PageName IS NULL
                  OR t5.Resource LIKE '' + @PageName + '%'
                )
            AND t5.ResourceTypeID = 4
    ORDER BY t5.Resource;



GO
