SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_CoursesFromBridgeTable_GetList]
    @GrdCompTypeId UNIQUEIDENTIFIER
AS
    SELECT DISTINCT
            ReqId
    FROM    arBridge_GradeComponentTypes_Courses
    WHERE   GrdComponentTypeId = @GrdCompTypeId;



GO
