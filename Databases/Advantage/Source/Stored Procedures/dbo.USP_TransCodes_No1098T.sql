SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_TransCodes_No1098T]
    @CampusId UNIQUEIDENTIFIER
AS
    DECLARE @ApplicantFeeId INT;
    SET @ApplicantFeeId = (
                            SELECT TOP 1
                                    SysTransCodeId
                            FROM    dbo.saSysTransCodes
                            WHERE   LOWER(Description) = 'applicant fee'
                          ); 

    SELECT  DISTINCT
            P.TransCodeCode
           ,P.TransCodeDescrip
    FROM    dbo.saTransCodes P
    INNER JOIN dbo.syCmpGrpCmps CGC ON CGC.CampGrpId = P.CampGrpId
    INNER JOIN syCampuses C ON C.CampusId = CGC.CampusId
    WHERE   P.Is1098T = 0
            AND P.SysTransCodeId <> @ApplicantFeeId
            AND P.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
            AND C.CampusId = @CampusId
    ORDER BY TransCodeDescrip;
GO
