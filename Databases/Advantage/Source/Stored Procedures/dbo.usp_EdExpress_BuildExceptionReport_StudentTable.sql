SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[usp_EdExpress_BuildExceptionReport_StudentTable]
    @ExceptionReportId NVARCHAR(50)
   ,@DbIn NVARCHAR(10)
   ,@Filter NVARCHAR(10)
   ,@AwardAmount DECIMAL(19,4)
   ,@AwardId NVARCHAR(50)
   ,@GrantType NVARCHAR(5)
   ,@AddDate NVARCHAR(20)
   ,@AddTime NVARCHAR(20)
   ,@OriginalSSN NVARCHAR(20)
   ,@UpdateDate NVARCHAR(20)
   ,@UpdateTime NVARCHAR(20)
   ,@OriginationStatus NVARCHAR(5)
   ,@AcademicYearId NVARCHAR(50)
   ,@AwardYearStartDate NVARCHAR(20)
   ,@AwardYearEndDate NVARCHAR(20)
   ,@ModDate NVARCHAR(20)
   ,@ErrorMsg NVARCHAR(500)
   ,@FileName NVARCHAR(250)
   ,@ExceptionGUID NVARCHAR(50)
   ,@MsgType NVARCHAR(50)
   ,@LoanFees NVARCHAR(50) = '0.00'
AS
    IF @MsgType = 'PELL'
        BEGIN
            INSERT  INTO syEDExpressExceptionReportPell_ACG_SMART_Teach_StudentTable
                    (
                     ExceptionReportId
                    ,DbIn
                    ,Filter
                    ,AwardAmount
                    ,AwardId
                    ,GrantType
                    ,AddDate
                    ,AddTime
                    ,OriginalSSN
                    ,UpdateDate
                    ,UpdateTime
                    ,OriginationStatus
                    ,AcademicYearId
                    ,AwardYearStartDate
                    ,AwardYearEndDate
                    ,ModDate
                    ,ErrorMsg
                    ,FileName
                    ,ExceptionGUID
                    )
            VALUES  (
                     @ExceptionReportId
                    ,@DbIn
                    ,@Filter
                    ,@AwardAmount
                    ,@AwardId
                    ,@GrantType
                    ,@AddDate
                    ,@AddTime
                    ,@OriginalSSN
                    ,@UpdateDate
                    ,@UpdateTime
                    ,@OriginationStatus
                    ,@AcademicYearId
                    ,@AwardYearStartDate
                    ,@AwardYearEndDate
                    ,@ModDate
                    ,@ErrorMsg
                    ,@FileName
                    ,@ExceptionGUID
                    );
            
        END;
    ELSE
        BEGIN
            INSERT  INTO syEDExpressExceptionReportDirectLoan_StudentTable
                    (
                     ExceptionReportId
                    ,DbIn
                    ,Filter
                    ,AddDate
                    ,AddTime
                    ,LoanAmtApproved
                    ,LoanFeePer
                    ,LoanId
                    ,LoanPeriodEndDate
                    ,LoanPeriodStartDate
                    ,LoanStatus
                    ,LoanType
                    ,MPNStatus
                    ,OriginalSSN
                    ,UpdateDate
                    ,UpdateTime
                    ,AcademicYearId
                    ,ModDate
                    ,ErrorMsg
                    ,FileName
                    ,ExceptionGUID
                    )
            VALUES  (
                     @ExceptionReportId
                    ,@DbIn
                    ,@Filter
                    ,@AddDate
                    ,@AddTime
                    ,@AwardAmount
                    ,@LoanFees
                    ,@AwardId
                    ,@AwardYearEndDate
                    ,@AwardYearStartDate
                    ,''
                    ,@GrantType
                    ,''
                    ,@OriginalSSN
                    ,@UpdateDate
                    ,@UpdateTime
                    ,@AcademicYearId
                    ,@ModDate
                    ,@ErrorMsg
                    ,@FileName
                    ,@ExceptionGUID
                    );
        END;





GO
