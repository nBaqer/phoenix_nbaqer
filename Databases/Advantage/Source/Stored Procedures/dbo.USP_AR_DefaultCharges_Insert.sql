SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[USP_AR_DefaultCharges_Insert]
    (
     @PrgVerID UNIQUEIDENTIFIER
    ,@FeeLevelID TINYINT
    ,@SysTransCodeID INT
    ,@PeriodCanChange BIT
    ,@UserName VARCHAR(50)
    )
AS /*----------------------------------------------------------------------------------------------------
	Author          :	Saraswathi Lakshmanan
    
    Create date		:	05/05/2010
    
	Procedure Name	:	[USP_AR_DefaultCharges_Insert]

	Objective		:	Inserts into table saPrgVerDefaultChargePeriods
	
	Parameters		:	Name				Type	Data Type				Required? 	
						=====				====	=========				=========	
						@PrgVerID			In		UniqueIDENTIFIER		Required
						@FeeLevelID			In		TinyInt					Required
						@SysTransCodeID		In		Integer					Required
						@PeriodCanChange	In		Bit						Required
						@UserName				In		Bit						Required
	
	Output			:			
						
*/-----------------------------------------------------------------------------------------------------

    BEGIN

        INSERT  INTO saPrgVerDefaultChargePeriods
                (
                 PrgVerPmtId
                ,FeeLevelID
                ,SysTransCodeID
                ,PrgVerID
                ,PeriodCanChange
                ,ModUser
                ,ModDate
                )
        VALUES  (
                 NEWID()
                ,@FeeLevelID
                ,@SysTransCodeID
                ,@PrgVerID
                ,@PeriodCanChange
                ,@UserName
                ,GETDATE()
                );

    END;




GO
