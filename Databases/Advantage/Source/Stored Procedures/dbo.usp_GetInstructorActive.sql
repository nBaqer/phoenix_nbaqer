SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_GetInstructorActive]
AS
    SET NOCOUNT ON; 
    SELECT  InstructorID
           ,InstructorDescrip
           ,AccountActive
    FROM    rptInstructor I
    LEFT JOIN syUsers U ON I.InstructorID = U.UserId
    WHERE   ISNULL(U.AccountActive,0) = 1
    ORDER BY InstructorDescrip;
 







GO
