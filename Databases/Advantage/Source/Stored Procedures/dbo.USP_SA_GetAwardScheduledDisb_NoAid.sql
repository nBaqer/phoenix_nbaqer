SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[USP_SA_GetAwardScheduledDisb_NoAid]
    (
     @StuEnrollID UNIQUEIDENTIFIER
    ,@AwardTypeID UNIQUEIDENTIFIER
    --New Variable Added On June 09, 2010 For Mantis Id 18950--
    ,@CutoffDate DATETIME
    --New Variable Added On June 09, 2010 For Mantis Id 18950--
    )
AS /*----------------------------------------------------------------------------------------------------
    Author : Saraswathi Lakshmanan
    
    Create date : 04/12/2010
    
    Procedure Name : [USP_SA_GetAwardScheduledDisb]

    Objective : Get the award Schedule Disb
    
    Parameters : Name Type Data Type Required? 
                        ===== ==== ========= ========= 
                        @StuEnrollID In Uniqueidentifier Required
                        @AwardTypeID In Uniqueidentifier Required
    
    Output : returns all the Schedule Disb details
                        
*/-----------------------------------------------------------------------------------------------------

    BEGIN
-- GetAwardScheduledDisb
        SELECT  ScheduleId
               ,ScheduleDescrip
               ,AcademicYearDescrip
               ,StartDate
               ,EndDate
               ,ExpectedDate
               ,Amount
               ,Balance
               ,IsAwardDisb
               ,IsInDB
               ,T.ModDate
               ,T.AwardTypeId
        FROM    (
                  SELECT    AwardScheduleId AS ScheduleId
                           ,SA.AwardStartDate
                           ,SA.AwardTypeId
                           ,(
                              SELECT    FundSourceDescrip
                              FROM      saFundSources
                              WHERE     FundSourceId = SA.AwardTypeId
                            ) AS ScheduleDescrip
                           ,(
                              SELECT    AcademicYearDescrip
                              FROM      saAcademicYears
                              WHERE     AcademicYearId = SA.AcademicYearId
                            ) AS AcademicYearDescrip
                           ,SA.AwardStartDate AS StartDate
                           ,SA.AwardEndDate AS EndDate
                           ,ExpectedDate
                           ,Amount
                           ,Amount - COALESCE((
                                                SELECT  SUM(Amount)
                                                FROM    saPmtDisbRel PDR
                                                       ,saTransactions T
                                                WHERE   AwardScheduleId = SAS.AwardScheduleId
                                                        AND PDR.TransactionId = T.TransactionId
                                                        AND T.Voided = 0
                                                GROUP BY PDR.AwardScheduleId
                                              ),0) + COALESCE((
                                                                SELECT  SUM(TransAmount)
                                                                FROM    saTransactions T
                                                                       ,saRefunds R
                                                                WHERE   R.AwardScheduleId = SAS.AwardScheduleId
                                                                        AND R.TransactionId = T.TransactionId
                                                                        AND T.Voided = 0
                                                                GROUP BY R.AwardScheduleId
                                                              ),0) AS Balance
                           ,1 AS IsAwardDisb
                           ,0 AS IsInDB
                           ,SAS.ModDate
                  FROM      faStudentAwards SA
                           ,faStudentAwardSchedule SAS
                  WHERE     SA.StuEnrollId = @StuEnrollID
                            AND (
                                  @AwardTypeID IS NULL
                                  OR SA.AwardTypeId = @AwardTypeID
                                )
                            AND SA.StudentAwardId = SAS.StudentAwardId
                ) T 
--New Variable Added On June 09, 2010 For Mantis Id 18950--
               ,saFundSources F
--New Variable Added On June 09, 2010 For Mantis Id 18950--
        WHERE   Balance > 0 
    --New Variable Added On June 09, 2010 For Mantis Id 18950--
                AND T.AwardTypeId = F.FundSourceId
                AND (
                      F.CutoffDate > T.AwardStartDate
                      OR F.CutoffDate IS NULL
                    )
                AND F.AdvFundSourceId = 1
    --New Variable Added On June 09, 2010 For Mantis Id 18950-- 
ORDER BY        ScheduleDescrip
               ,AcademicYearDescrip
               ,StartDate
               ,EndDate
               ,ExpectedDate; 

    END;




GO
