SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[Usp_PR_Sub3_Terms_forAllEnrolmnents]
    @StuEnrollIdList VARCHAR(MAX)
   ,@TermId VARCHAR(MAX) = NULL
   ,@TermStartDate VARCHAR(50) = NULL
   ,@TermStartDateModifier VARCHAR(10) = NULL
   ,@ShowAllEnrollments BIT = 0
AS
    DECLARE @StuEnrollCampusId UNIQUEIDENTIFIER;
    DECLARE @GradesFormat VARCHAR(50);
    DECLARE @GPAMethod VARCHAR(50);
    IF @TermStartDate IS NULL
       OR @TermStartDate = ''
        BEGIN
            SET @TermStartDate = CONVERT(VARCHAR(10), GETDATE(), 120);
        END;
    SET @StuEnrollCampusId = COALESCE((
                                      SELECT TOP 1 ASE.CampusId
                                      FROM   arStuEnrollments AS ASE
                                      WHERE  ASE.StuEnrollId IN (
                                                                SELECT Val
                                                                FROM   MultipleValuesForReportParameters(@StuEnrollIdList, ',', 1)
                                                                )
                                      )
                                     ,NULL
                                     );
    SET @GradesFormat = dbo.GetAppSettingValueByKeyName('GradesFormat', @StuEnrollCampusId);
    IF ( @GradesFormat IS NOT NULL )
        BEGIN
            SET @GradesFormat = LOWER(LTRIM(RTRIM(@GradesFormat)));
        END;
    SET @GPAMethod = dbo.GetAppSettingValueByKeyName('GPAMethod', @StuEnrollCampusId);
    IF ( @GPAMethod IS NOT NULL )
        BEGIN
            SET @GPAMethod = LOWER(LTRIM(RTRIM(@GPAMethod)));
        END;

    DECLARE @TermCourseDataVar TABLE
        (
            StudentId UNIQUEIDENTIFIER
           ,TermId UNIQUEIDENTIFIER
           ,CourseId UNIQUEIDENTIFIER
           ,CreditsEarned DECIMAL(18, 2)
           ,CreditsAttempted DECIMAL(18, 2)
           ,Completed BIT
           ,FACreditsEarned DECIMAL(18, 2)
        );


    DECLARE @TermDataVar TABLE
        (
            PrgVerId UNIQUEIDENTIFIER
           ,PrgVerDescrip VARCHAR(100)
           ,PrgVersionTrackCredits BIT
           ,TermId UNIQUEIDENTIFIER
           ,TermDescription VARCHAR(100)
           ,TermStartDate DATETIME
           ,TermEndDate DATETIME
           ,CourseId UNIQUEIDENTIFIER
           ,CourseCode VARCHAR(100)
           ,CourseDescription VARCHAR(100)
           ,CourseCodeDescription VARCHAR(100)
           ,CourseCredits DECIMAL(18, 2)
           ,CourseFinAidCredits DECIMAL(18, 2)
           ,MinVal DECIMAL(18, 2)
           ,CourseScore DECIMAL(18, 2)
           ,StuEnrollId UNIQUEIDENTIFIER
           ,CreditsAttempted DECIMAL(18, 2)
           ,CreditsEarned DECIMAL(18, 2)
           ,Completed BIT
           ,CurrentScore DECIMAL(18, 2)
           ,FinalScore DECIMAL(18, 2)
           ,FinalGrade VARCHAR(10)
           ,WeightedAverage_GPA DECIMAL(18, 2)
           ,SimpleAverage_GPA DECIMAL(18, 2)
           ,CampusId UNIQUEIDENTIFIER
           ,CampDescrip VARCHAR(100)
           ,FirstName VARCHAR(100)
           ,LastName VARCHAR(100)
           ,MiddleName VARCHAR(100)
           ,TermGPA_Simple DECIMAL(18, 2)
           ,TermGPA_Weighted DECIMAL(18, 2)
           ,Term_CreditsAttempted DECIMAL(18, 2)
           ,Term_CreditsEarned DECIMAL(18, 2)
           ,termAverage DECIMAL(18, 2)
           ,GrdBkWgtDetailsCount INT
           ,ClockHourProgram BIT
           ,GradesFormat VARCHAR(50)
           ,GPAMethod VARCHAR(50)
           ,StudentId UNIQUEIDENTIFIER
           ,WasRegisteredByProgramType BIT
           ,rownumber1 INT
        );


    INSERT INTO @TermDataVar
                SELECT dt.PrgVerId
                      ,dt.PrgVerDescrip
                      ,dt.PrgVersionTrackCredits
                      ,dt.TermId
                      ,dt.TermDescription
                      ,dt.TermStartDate
                      ,dt.TermEndDate
                      ,dt.CourseId
                      ,dt.CourseCode
                      ,dt.CourseDescription
                      ,dt.CourseCodeDescription
                      ,dt.CourseCredits
                      ,dt.CourseFinAidCredits
                      ,dt.MinVal
                      ,dt.CourseScore
                      ,dt.StuEnrollId
                      ,dt.CreditsAttempted
                      ,dt.CreditsEarned
                      ,dt.Completed
                      ,dt.CurrentScore
                      ,dt.FinalScore
                      ,dt.FinalGrade
                      ,dt.WeightedAverage_GPA
                      ,dt.SimpleAverage_GPA
                      ,dt.CampusId
                      ,dt.CampDescrip
                      ,dt.FirstName
                      ,dt.LastName
                      ,dt.MiddleName
                      ,dt.TermGPA_Simple
                      ,dt.TermGPA_Weighted
                      ,dt.Term_CreditsAttempted
                      ,dt.Term_CreditsEarned
                      ,dt.termAverage
                      ,dt.GrdBkWgtDetailsCount
                      ,dt.ClockHourProgram
                      ,@GradesFormat AS GradesFormat
                      ,@GPAMethod AS GPAMethod
                      ,dt.StudentId AS StudentId
                      ,dt.WasRegisteredByProgramType AS WasRegisteredByProgramType
                      ,ROW_NUMBER() OVER ( PARTITION BY TermId
                                           ORDER BY TermStartDate
                                                   ,TermEndDate
                                                   ,TermDescription
                                         ) AS rownumber1
                FROM   (
                       SELECT     DISTINCT PV.PrgVerId AS PrgVerId
                                 ,PV.PrgVerDescrip AS PrgVerDescrip
                                 ,CASE WHEN ( PV.Credits > 0.0 ) THEN 1
                                       ELSE 0
                                  END AS PrgVersionTrackCredits
                                 ,T.TermId AS TermId
                                 ,T.TermDescrip AS TermDescription
                                 ,T.StartDate AS TermStartDate
                                 ,T.EndDate AS TermEndDate
                                 ,CS.ReqId AS CourseId
                                 ,R.Code AS CourseCode
                                 ,R.Descrip AS CourseDescription
                                 ,'(' + R.Code + ')' + R.Descrip AS CourseCodeDescription
                                 ,R.Credits AS CourseCredits
                                 ,CASE WHEN ( @ShowAllEnrollments = 1 ) THEN R.FinAidCredits
                                       ELSE (
                                            SELECT SUM(SCS1.FACreditsEarned)
                                            FROM   syCreditSummary AS SCS1
                                            WHERE  SCS1.StuEnrollId = SCS.StuEnrollId
                                                   AND SCS1.TermId = SCS.TermId
                                            )
                                  END AS CourseFinAidCredits
                                 ,(
                                  SELECT MIN(MinVal)
                                  FROM   arGradeScaleDetails GCD
                                        ,arGradeSystemDetails GSD
                                  WHERE  GCD.GrdSysDetailId = GSD.GrdSysDetailId
                                         AND GSD.IsPass = 1
                                         AND GCD.GrdScaleId = CS.GrdScaleId
                                  ) AS MinVal
                                 ,RES.Score AS CourseScore
                                 ,SE.StuEnrollId AS StuEnrollId
                                 --, NULL AS MinResult
                                 --, NULL AS GradeComponentDescription -- Student data   
                                 ,SCS.CreditsAttempted AS CreditsAttempted
                                 ,SCS.CreditsEarned AS CreditsEarned
                                 ,SCS.Completed AS Completed
                                 ,SCS.CurrentScore AS CurrentScore
                                 ,SCS.CurrentGrade AS CurrentGrade
                                 ,SCS.FinalScore AS FinalScore
                                 ,SCS.FinalGrade AS FinalGrade
                                 ,( CASE WHEN (
                                              SELECT SUM(SCS2.Count_WeightedAverage_Credits)
                                              FROM   syCreditSummary AS SCS2
                                              WHERE  SCS2.StuEnrollId = SE.StuEnrollId
                                                     AND SCS2.TermId = T.TermId
                                              ) > 0 THEN (
                                                         SELECT SUM(SCS2.Product_WeightedAverage_Credits_GPA) / SUM(SCS2.Count_WeightedAverage_Credits)
                                                         FROM   syCreditSummary AS SCS2
                                                         WHERE  SCS2.StuEnrollId = SE.StuEnrollId
                                                                AND SCS2.TermId = T.TermId
                                                         )
                                         ELSE 0
                                    END
                                  ) AS WeightedAverage_GPA
                                 ,( CASE WHEN (
                                              SELECT SUM(Count_SimpleAverage_Credits)
                                              FROM   syCreditSummary
                                              WHERE  StuEnrollId = SE.StuEnrollId
                                                     AND TermId = T.TermId
                                              ) > 0 THEN (
                                                         SELECT SUM(Product_SimpleAverage_Credits_GPA) / SUM(Count_SimpleAverage_Credits)
                                                         FROM   syCreditSummary
                                                         WHERE  StuEnrollId = SE.StuEnrollId
                                                                AND TermId = T.TermId
                                                         )
                                         ELSE 0
                                    END
                                  ) AS SimpleAverage_GPA
                                 --, NULL AS WeightedAverage_CumGPA
                                 --, NULL AS SimpleAverage_CumGPA
                                 ,C.CampusId AS CampusId
                                 ,C.CampDescrip AS CampDescrip
                                 --, NULL AS rownumber
                                 ,AST.FirstName AS FirstName
                                 ,AST.LastName AS LastName
                                 ,AST.MiddleName AS MiddleName
                                 -- Newly added fields
                                 ,SCS.TermGPA_Simple AS TermGPA_Simple
                                 ,SCS.TermGPA_Weighted AS TermGPA_Weighted
                                 --, NULL AS newcol
                                 ,(
                                  SELECT SUM(ISNULL(CreditsAttempted, 0))
                                  FROM   syCreditSummary
                                  WHERE  TermId = T.TermId
                                         AND StuEnrollId = SE.StuEnrollId
                                  ) AS Term_CreditsAttempted
                                 ,(
                                  SELECT SUM(ISNULL(CreditsEarned, 0))
                                  FROM   syCreditSummary
                                  WHERE  TermId = T.TermId
                                         AND StuEnrollId = SE.StuEnrollId
                                  ) AS Term_CreditsEarned
                                 --Newly added ends here
                                 ,(
                                  SELECT TOP 1 Average
                                  FROM   syCreditSummary
                                  WHERE  StuEnrollId = SE.StuEnrollId
                                         AND TermId = T.TermId
                                  ) AS termAverage
                                 ,(
                                  SELECT COUNT(*) AS GrdBkWgtDetailsCount
                                  FROM   arGrdBkResults
                                  WHERE  StuEnrollId = SE.StuEnrollId
                                         AND ClsSectionId = RES.TestId
                                  ) AS GrdBkWgtDetailsCount
                                 ,CASE WHEN P.ACId = 5 THEN 'True'
                                       ELSE 'False'
                                  END AS ClockHourProgram
                                 ,AST.StudentId
                                 ,CASE WHEN PV.ProgramRegistrationType IS NOT NULL
                                            AND PV.ProgramRegistrationType = 1 THEN 1
                                       ELSE 0
                                  END AS WasRegisteredByProgramType
                       FROM       arClassSections CS
                       INNER JOIN arResults GBR ON CS.ClsSectionId = GBR.TestId
                       INNER JOIN arStuEnrollments SE ON GBR.StuEnrollId = SE.StuEnrollId
                       INNER JOIN adLeads AS AST ON AST.StudentId = SE.StudentId
                       INNER JOIN syCampuses C ON SE.CampusId = C.CampusId
                       INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                       INNER JOIN arPrograms P ON P.ProgId = PV.ProgId
                       INNER JOIN arTerm T ON CS.TermId = T.TermId
                       INNER JOIN arReqs R ON CS.ReqId = R.ReqId
                       INNER JOIN arResults RES ON RES.StuEnrollId = GBR.StuEnrollId
                                                   AND RES.TestId = CS.ClsSectionId
                       LEFT JOIN  syCreditSummary SCS ON SE.StuEnrollId = SCS.StuEnrollId
                                                         AND T.TermId = SCS.TermId
                                                         AND R.ReqId = SCS.ReqId
                       WHERE -- SE.StuEnrollId = @StuEnrollId
                                  SE.StuEnrollId IN (
                                                    SELECT Val
                                                    FROM   MultipleValuesForReportParameters(@StuEnrollIdList, ',', 1)
                                                    )
                                  AND (
                                      @TermStartDate IS NULL
                                      OR @TermStartDateModifier IS NULL
                                      OR T.StartDate IS NULL
                                      OR (
                                         (
                                         ( @TermStartDateModifier <> '=' )
                                         OR ( T.StartDate = @TermStartDate )
                                         )
                                         AND (
                                             ( @TermStartDateModifier <> '>' )
                                             OR ( T.StartDate > @TermStartDate )
                                             )
                                         AND (
                                             ( @TermStartDateModifier <> '<' )
                                             OR ( T.StartDate < @TermStartDate )
                                             )
                                         AND (
                                             ( @TermStartDateModifier <> '>=' )
                                             OR ( T.StartDate >= @TermStartDate )
                                             )
                                         AND (
                                             ( @TermStartDateModifier <> '<=' )
                                             OR ( T.StartDate <= @TermStartDate )
                                             )
                                         )
                                      )
                                  AND (
                                      @TermId IS NULL
                                      OR T.TermId IN (
                                                     SELECT Val
                                                     FROM   MultipleValuesForReportParameters(@TermId, ',', 1)
                                                     )
                                      )
                       UNION
                       SELECT     DISTINCT PV.PrgVerId AS PrgVerId
                                 ,PV.PrgVerDescrip AS PrgVerDescrip
                                 ,CASE WHEN ( PV.Credits > 0.0 ) THEN 1
                                       ELSE 0
                                  END AS PrgVersionTrackCredits
                                 ,T.TermId AS TermId
                                 ,T.TermDescrip AS TermDescription
                                 ,T.StartDate AS TermStartDate
                                 ,T.EndDate AS TermEndDate
                                 ,GBCR.ReqId AS CourseId
                                 ,R.Code AS CourseCode
                                 ,R.Descrip AS CourseDescrip
                                 ,'(' + R.Code + ' ) ' + R.Descrip AS CourseCodeDescrip
                                 ,R.Credits AS CourseCredits
                                 ,CASE WHEN ( @ShowAllEnrollments = 1 ) THEN R.FinAidCredits
                                       ELSE (
                                            SELECT SUM(SCS1.FACreditsEarned)
                                            FROM   syCreditSummary AS SCS1
                                            WHERE  SCS1.StuEnrollId = SCS.StuEnrollId
                                                   AND SCS1.TermId = SCS.TermId
                                            )
                                  END AS CourseFinAidCredits
                                 ,(
                                  SELECT MIN(MinVal)
                                  FROM   arGradeScaleDetails GCD
                                        ,arGradeSystemDetails GSD
                                  WHERE  GCD.GrdSysDetailId = GSD.GrdSysDetailId
                                         AND GSD.IsPass = 1
                                  ) AS MinVal
                                 ,GBCR.Score AS CourseScore
                                 ,SE.StuEnrollId AS StuEnrollId
                                 --, NULL AS MinResult
                                 --, NULL AS GradeComponentDescription -- Student data    
                                 ,SCS.CreditsAttempted AS CreditsAttempted
                                 ,SCS.CreditsEarned AS CreditsEarned
                                 ,SCS.Completed AS Completed
                                 ,SCS.CurrentScore AS CurrentScore
                                 ,SCS.CurrentGrade AS CurrentGrade
                                 ,SCS.FinalScore AS FinalScore
                                 ,SCS.FinalGrade AS FinalGrade
                                 ,( CASE WHEN (
                                              SELECT SUM(Count_WeightedAverage_Credits)
                                              FROM   syCreditSummary
                                              WHERE  StuEnrollId = SE.StuEnrollId
                                                     AND TermId = T.TermId
                                              ) > 0 THEN (
                                                         SELECT SUM(Product_WeightedAverage_Credits_GPA) / SUM(Count_WeightedAverage_Credits)
                                                         FROM   syCreditSummary
                                                         WHERE  StuEnrollId = SE.StuEnrollId
                                                                AND TermId = T.TermId
                                                         )
                                         ELSE 0
                                    END
                                  ) AS WeightedAverage_GPA
                                 ,( CASE WHEN (
                                              SELECT SUM(Count_SimpleAverage_Credits)
                                              FROM   syCreditSummary
                                              WHERE  StuEnrollId = SE.StuEnrollId
                                                     AND TermId = T.TermId
                                              ) > 0 THEN (
                                                         SELECT SUM(Product_SimpleAverage_Credits_GPA) / SUM(Count_SimpleAverage_Credits)
                                                         FROM   syCreditSummary
                                                         WHERE  StuEnrollId = SE.StuEnrollId
                                                                AND TermId = T.TermId
                                                         )
                                         ELSE 0
                                    END
                                  ) AS SimpleAverage_GPA
                                 --, NULL AS WeightedAverage_CumGPA
                                 --, NULL AS SimpleAverage_CumGPA
                                 ,C.CampusId
                                 ,C.CampDescrip
                                 --, NULL AS rownumber
                                 ,AST.FirstName AS FirstName
                                 ,AST.LastName AS LastName
                                 ,AST.MiddleName AS MiddleName
                                 -- Newly added fields
                                 ,SCS.TermGPA_Simple AS TermGPA_Simple
                                 ,SCS.TermGPA_Weighted AS TermGPA_Weighted
                                 --, NULL AS newcol
                                 ,(
                                  SELECT SUM(ISNULL(CreditsAttempted, 0))
                                  FROM   syCreditSummary
                                  WHERE  TermId = T.TermId
                                         AND StuEnrollId = SE.StuEnrollId
                                  ) AS Term_CreditsAttempted
                                 ,(
                                  SELECT SUM(ISNULL(CreditsEarned, 0))
                                  FROM   syCreditSummary
                                  WHERE  TermId = T.TermId
                                         AND StuEnrollId = SE.StuEnrollId
                                  ) AS Term_CreditsEarned
                                 --Newly added ends here
                                 ,(
                                  SELECT TOP 1 Average
                                  FROM   syCreditSummary
                                  WHERE  StuEnrollId = SE.StuEnrollId
                                         AND TermId = T.TermId
                                  ) AS termAverage
                                 ,(
                                  SELECT COUNT(*) AS GrdBkWgtDetailsCount
                                  FROM   arGrdBkConversionResults
                                  WHERE  StuEnrollId = SE.StuEnrollId
                                         AND TermId = T.TermId
                                         AND ReqId = R.ReqId
                                  ) AS GrdBkWgtDetailsCount
                                 ,CASE WHEN P.ACId = 5 THEN 'True'
                                       ELSE 'False'
                                  END AS ClockHourProgram
                                 ,AST.StudentId
                                 ,CASE WHEN PV.ProgramRegistrationType IS NOT NULL
                                            AND PV.ProgramRegistrationType = 1 THEN 1
                                       ELSE 0
                                  END AS WasRegisteredByProgramType
                       FROM       arTransferGrades GBCR
                       INNER JOIN arStuEnrollments SE ON GBCR.StuEnrollId = SE.StuEnrollId
                       INNER JOIN adLeads AS AST ON AST.StudentId = SE.StudentId
                       INNER JOIN syCampuses C ON SE.CampusId = C.CampusId
                       INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                       INNER JOIN arPrograms P ON P.ProgId = PV.ProgId
                       INNER JOIN arTerm T ON GBCR.TermId = T.TermId
                       INNER JOIN arReqs R ON GBCR.ReqId = R.ReqId
                       --INNER JOIN arResults AR ON GBCR.StuEnrollId = AR.StuEnrollId
                       LEFT JOIN  syCreditSummary SCS ON SE.StuEnrollId = SCS.StuEnrollId
                                                         AND T.TermId = SCS.TermId
                                                         AND R.ReqId = SCS.ReqId
                       WHERE -- SE.StuEnrollId = @StuEnrollId
                                  SE.StuEnrollId IN (
                                                    SELECT Val
                                                    FROM   MultipleValuesForReportParameters(@StuEnrollIdList, ',', 1)
                                                    )
                                  AND (
                                      @TermStartDate IS NULL
                                      OR @TermStartDateModifier IS NULL
                                      OR T.StartDate IS NULL
                                      OR (
                                         (
                                         ( @TermStartDateModifier <> '=' )
                                         OR ( T.StartDate = @TermStartDate )
                                         )
                                         AND (
                                             ( @TermStartDateModifier <> '>' )
                                             OR ( T.StartDate > @TermStartDate )
                                             )
                                         AND (
                                             ( @TermStartDateModifier <> '<' )
                                             OR ( T.StartDate < @TermStartDate )
                                             )
                                         AND (
                                             ( @TermStartDateModifier <> '>=' )
                                             OR ( T.StartDate >= @TermStartDate )
                                             )
                                         AND (
                                             ( @TermStartDateModifier <> '<=' )
                                             OR ( T.StartDate <= @TermStartDate )
                                             )
                                         )
                                      )
                                  AND (
                                      @TermId IS NULL
                                      OR T.TermId IN (
                                                     SELECT Val
                                                     FROM   MultipleValuesForReportParameters(@TermId, ',', 1)
                                                     )
                                      )
                       ) dt;

    --SELECT * FROM @TermDataVar

    IF @ShowAllEnrollments = 1
        BEGIN
            INSERT INTO @TermCourseDataVar
                        SELECT DISTINCT StudentId
                              ,TermId
                              ,CourseId
                              ,CreditsEarned
                              ,CreditsAttempted
                              ,Completed
                              ,CourseFinAidCredits
                        FROM   @TermDataVar
                        WHERE  Completed = 1;

            SELECT   PrgVerId
                    ,PrgVerDescrip
                    ,PrgVersionTrackCredits
                    ,TermId
                    ,TermDescription
                    ,TermStartDate
                    ,TermEndDate
                    ,CourseId
                    ,CourseCode
                    ,CourseDescription
                    ,CourseCodeDescription
                    ,CourseCredits
                    ,(
                     SELECT   SUM(FACreditsEarned)
                     FROM     @TermCourseDataVar
                     WHERE    TermId = [@TermDataVar].TermId
                     GROUP BY StudentId
                     ) AS CourseFinAidCredits
                    --,(SELECT SUM(SCS1.FACreditsEarned)
                    --                         FROM   syCreditSummary AS SCS1
                    --                         WHERE  SCS1.StuEnrollId = '0235B76B-B4AB-40C1-85C1-64ED042D1205' 
                    --                                AND SCS1.TermId = 'E7430FF3-B021-4189-805D-5B54544D9615')
                    ,MinVal
                    ,CourseScore
                    ,StuEnrollId
                    ,CreditsAttempted
                    ,CreditsEarned
                    ,Completed
                    ,CurrentScore
                    ,FinalScore
                    ,FinalGrade
                    ,WeightedAverage_GPA
                    ,SimpleAverage_GPA
                    ,CampusId
                    ,CampDescrip
                    ,FirstName
                    ,LastName
                    ,MiddleName
                    ,TermGPA_Simple
                    ,TermGPA_Weighted
                    ,(
                     SELECT   SUM(CreditsAttempted)
                     FROM     @TermCourseDataVar
                     WHERE    TermId = [@TermDataVar].TermId
                     GROUP BY StudentId
                     ) AS Term_CreditsAttempted
                    ,(
                     SELECT   SUM(CreditsEarned)
                     FROM     @TermCourseDataVar
                     WHERE    TermId = [@TermDataVar].TermId
                     GROUP BY StudentId
                     ) AS Term_CreditsEarned
                    ,CASE WHEN (
                               SELECT TOP 1 P.ACId
                               FROM   dbo.arStuEnrollments E
                               JOIN   dbo.arPrgVersions PV ON PV.PrgVerId = E.PrgVerId
                               JOIN   dbo.arPrograms P ON P.ProgId = PV.ProgId
                               WHERE  E.StuEnrollId = StuEnrollId
                               ) = 5
                               AND @GradesFormat = 'numeric' THEN dbo.CalculateStudentAverage(StuEnrollId, NULL, NULL, NULL, TermId,NULL)
                          ELSE termAverage
                     END AS termAverage
                    ,GrdBkWgtDetailsCount
                    ,ClockHourProgram
                    ,GradesFormat
                    ,GPAMethod
                    ,WasRegisteredByProgramType
                    ,rownumber1
            FROM     @TermDataVar
            WHERE    rownumber1 = 1
            ORDER BY PrgVerDescrip
                    ,TermStartDate
                    ,TermEndDate
                    ,TermDescription
                    ,FinalGrade DESC
                    ,FinalScore DESC
                    ,CourseCode;
        END;
    ELSE
        BEGIN
            SELECT   PrgVerId
                    ,PrgVerDescrip
                    ,PrgVersionTrackCredits
                    ,TermId
                    ,TermDescription
                    ,TermStartDate
                    ,TermEndDate
                    ,CourseId
                    ,CourseCode
                    ,CourseDescription
                    ,CourseCodeDescription
                    ,CourseCredits
                    ,CourseFinAidCredits
                    ,MinVal
                    ,CourseScore
                    ,StuEnrollId
                    ,CreditsAttempted
                    ,CreditsEarned
                    ,Completed
                    ,CurrentScore
                    ,FinalScore
                    ,FinalGrade
                    ,WeightedAverage_GPA
                    ,SimpleAverage_GPA
                    ,CampusId
                    ,CampDescrip
                    ,FirstName
                    ,LastName
                    ,MiddleName
                    ,TermGPA_Simple
                    ,TermGPA_Weighted
                    ,Term_CreditsAttempted
                    ,Term_CreditsEarned
                    ,CASE WHEN (
                               SELECT TOP 1 P.ACId
                               FROM   dbo.arStuEnrollments E
                               JOIN   dbo.arPrgVersions PV ON PV.PrgVerId = E.PrgVerId
                               JOIN   dbo.arPrograms P ON P.ProgId = PV.ProgId
                               WHERE  E.StuEnrollId = StuEnrollId
                               ) = 5
                               AND @GradesFormat = 'numeric' THEN dbo.CalculateStudentAverage(StuEnrollId, NULL, NULL, NULL, TermId,NULL)
                          ELSE termAverage
                     END AS termAverage
                    ,GrdBkWgtDetailsCount
                    ,ClockHourProgram
                    ,GradesFormat
                    ,GPAMethod
                    ,StudentId
                    ,WasRegisteredByProgramType
                    ,rownumber1
            FROM     @TermDataVar
            WHERE    rownumber1 = 1
            ORDER BY PrgVerDescrip
                    ,TermStartDate
                    ,TermEndDate
                    ,TermDescription
                    ,FinalGrade DESC
                    ,FinalScore DESC
                    ,CourseCode;
        END;

--SELECT * FROM @TermCourseDataVar








GO
