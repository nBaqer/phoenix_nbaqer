SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_GetLastEntityUserWorkedWith_GetList]
    @UserId UNIQUEIDENTIFIER
   ,@MRUTypeId INT
   ,@CampusId UNIQUEIDENTIFIER
AS --At times Top 1 picks row 2 when all moddate are the same
   --need to use Row_Number function to get the right Top 1
    IF LTRIM(RTRIM(@MRUTypeId)) = 2
        BEGIN
            SELECT TOP 1
                    ChildId
            FROM    (
                      SELECT    ChildId
                               ,CampusId
                               ,ModDate
                               ,ROW_NUMBER() OVER ( ORDER BY ModDate DESC ) AS RowNumber
                      FROM      syMRUs
                      WHERE     UserId = @UserId
                                AND MRUTypeId = @MRUTypeId
                    ) t1
            INNER JOIN plEmployers t2 ON t1.ChildId = t2.EmployerId
            INNER JOIN syCampgrps t3 ON t2.CampgrpId = t3.CampgrpId
            WHERE   (
                      LOWER(t3.CampGrpDescrip) = 'all'
                      OR t2.CampGrpId IN ( SELECT DISTINCT
                                                    CampGrpId
                                           FROM     syCmpGrpCmps
                                           WHERE    CampusId = @CampusId )
                    )
				--AND 
				--t1.CampusId in (select Distinct CampusId 
				--				from syCmpGrpCmps 
				--				where CampGrpId=t2.CampGrpId)
            ORDER BY RowNumber ASC;
        END;
    ELSE
        BEGIN
            SELECT TOP 1
                    ChildId
            FROM    (
                      SELECT    ChildId
                               ,ModDate
                               ,ROW_NUMBER() OVER ( ORDER BY ModDate DESC ) AS RowNumber
                      FROM      syMRUs
                      WHERE     UserId = @UserId
                                AND MRUTypeId = @MRUTypeId
                                AND CampusId = @CampusId
                    ) t1
            ORDER BY RowNumber ASC;
        END;



GO
