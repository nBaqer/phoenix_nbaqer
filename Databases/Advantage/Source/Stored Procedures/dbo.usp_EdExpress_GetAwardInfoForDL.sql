SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[usp_EdExpress_GetAwardInfoForDL] @LoanId NVARCHAR(50)
AS
    SELECT  StuEnrollId
           ,AcademicYearId
           ,GrossAmount
           ,LoanFees
           ,CONVERT(VARCHAR,AwardStartDate,101) AS AwardStartDate
           ,CONVERT(VARCHAR,AwardEndDate,101) AS AwardEndDate
    FROM    faStudentAwards
    WHERE   FA_Id = @LoanId;




GO
