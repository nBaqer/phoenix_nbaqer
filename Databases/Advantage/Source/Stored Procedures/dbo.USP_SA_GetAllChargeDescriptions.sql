SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[USP_SA_GetAllChargeDescriptions]
    (
     @ShowActive NVARCHAR(50)
    ,@CampusId UNIQUEIDENTIFIER
    )
AS /*----------------------------------------------------------------------------------------------------
	Author          :	Saraswathi Lakshmanan
    
    Create date		:	04/12/2010
    
	Procedure Name	:	[USP_SA_GetAllChargeDescriptions]

	Objective		:	Get the Payment descriptions
	
	Parameters		:	Name			Type	Data Type			Required? 	
						=====			====	=========			=========	
						@ShowActive		In		nvarchar			Required
						@CampusId       In      uniqueidentifier    required
	
	Output			:	Returns the descriptions other than  whose systransCodeID is 11,12,13,14,15,16 			
						
*/-----------------------------------------------------------------------------------------------------

    BEGIN
        DECLARE @ShowActiveValue NVARCHAR(20);
        IF LOWER(@ShowActive) = 'true'
            BEGIN
                SET @ShowActiveValue = 'Active';
            END;
        ELSE
            BEGIN
                SET @ShowActiveValue = 'InActive';
            END;

        SELECT  TC.TransCodeId
               ,TC.TransCodeCode
               ,TC.TransCodeDescrip
               ,TC.StatusId
               ,TC.CampGrpId
               ,ST.StatusId
               ,ST.Status
        FROM    saTransCodes TC
               ,syStatuses ST
        WHERE   TC.StatusId = ST.StatusId
                AND SysTransCodeId NOT IN ( 11,12,13,14,15,16,18 )
                AND ST.Status = @ShowActiveValue
                AND TC.CampGrpId IN ( SELECT    CampGrpId
                                      FROM      syCmpGrpCmps
                                      WHERE     CampusId = @CampusId )
        ORDER BY TC.TransCodeDescrip;
	
    END;




GO
