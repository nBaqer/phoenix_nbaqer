SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- Add page to Manage Security
CREATE PROCEDURE [dbo].[USP_ManageSecurity_ExportReport]
    @RoleId VARCHAR(50)
   ,@ModuleResourceId INT
   ,@tabid INT = 0
   ,@ResourceTypeId INT
   ,@CampusId VARCHAR(50)
   ,@SchoolEnumerator INT
AS --Declare local variables      
    DECLARE @ShowRossOnlyTabs BIT
       ,@SchedulingMethod VARCHAR(50)
       ,@TrackSAPAttendance VARCHAR(50);      
    DECLARE @ShowCollegeOfCourtReporting VARCHAR(5)
       ,@FameESP VARCHAR(5)
       ,@EdExpress VARCHAR(5);       
    DECLARE @GradeBookWeightingLevel VARCHAR(20)
       ,@ShowExternshipTabs VARCHAR(5)
       ,@TimeClockClassSection VARCHAR(5)
       ,@SettingId INT;
	   
 -- Get Values      
    IF (
         SELECT COUNT(*)
         FROM   syConfigAppSetValues
         WHERE  SettingId = 68
                AND CampusId = @CampusId
       ) >= 1
        BEGIN    
            SET @ShowRossOnlyTabs = (
                                      SELECT    Value
                                      FROM      dbo.syConfigAppSetValues
                                      WHERE     SettingId = 68
                                                AND CampusId = @CampusId
                                    );      
			
        END;     
    ELSE
        BEGIN    
            SET @ShowRossOnlyTabs = (
                                      SELECT    Value
                                      FROM      dbo.syConfigAppSetValues
                                      WHERE     SettingId = 68
                                                AND CampusId IS NULL
                                    );    
        END;    
	 
	 
		
    IF (
         SELECT COUNT(*)
         FROM   syConfigAppSetValues
         WHERE  SettingId = 65
                AND CampusId = @CampusId
       ) >= 1
        BEGIN    
            SET @SchedulingMethod = (
                                      SELECT    Value
                                      FROM      dbo.syConfigAppSetValues
                                      WHERE     SettingId = 65
                                                AND CampusId = @CampusId
                                    );      
			
        END;     
    ELSE
        BEGIN    
            SET @SchedulingMethod = (
                                      SELECT    Value
                                      FROM      dbo.syConfigAppSetValues
                                      WHERE     SettingId = 65
                                                AND CampusId IS NULL
                                    );    
        END;                          
									
		
									  
    IF (
         SELECT COUNT(*)
         FROM   syConfigAppSetValues
         WHERE  SettingId = 72
                AND CampusId = @CampusId
       ) >= 1
        BEGIN    
            SET @TrackSAPAttendance = (
                                        SELECT  Value
                                        FROM    dbo.syConfigAppSetValues
                                        WHERE   SettingId = 72
                                                AND CampusId = @CampusId
                                      );      
			
        END;     
    ELSE
        BEGIN    
            SET @TrackSAPAttendance = (
                                        SELECT  Value
                                        FROM    dbo.syConfigAppSetValues
                                        WHERE   SettingId = 72
                                                AND CampusId IS NULL
                                      );    
        END;     
		   
		  
    IF (
         SELECT COUNT(*)
         FROM   syConfigAppSetValues
         WHERE  SettingId = 118
                AND CampusId = @CampusId
       ) >= 1
        BEGIN    
            SET @ShowCollegeOfCourtReporting = (
                                                 SELECT Value
                                                 FROM   dbo.syConfigAppSetValues
                                                 WHERE  SettingId = 118
                                                        AND CampusId = @CampusId
                                               );      
			
        END;     
    ELSE
        BEGIN    
            SET @ShowCollegeOfCourtReporting = (
                                                 SELECT Value
                                                 FROM   dbo.syConfigAppSetValues
                                                 WHERE  SettingId = 118
                                                        AND CampusId IS NULL
                                               );    
        END;     
		 
						   
    IF (
         SELECT COUNT(*)
         FROM   syConfigAppSetValues
         WHERE  SettingId = 37
                AND CampusId = @CampusId
       ) >= 1
        BEGIN    
            SET @FameESP = (
                             SELECT Value
                             FROM   dbo.syConfigAppSetValues
                             WHERE  SettingId = 37
                                    AND CampusId = @CampusId
                           );      
			
        END;     
    ELSE
        BEGIN    
            SET @FameESP = (
                             SELECT Value
                             FROM   dbo.syConfigAppSetValues
                             WHERE  SettingId = 37
                                    AND CampusId IS NULL
                           );    
        END;     
		 
							 
    IF (
         SELECT COUNT(*)
         FROM   syConfigAppSetValues
         WHERE  SettingId = 91
                AND CampusId = @CampusId
       ) >= 1
        BEGIN    
            SET @EdExpress = (
                               SELECT   Value
                               FROM     dbo.syConfigAppSetValues
                               WHERE    SettingId = 91
                                        AND CampusId = @CampusId
                             );      
			
        END;     
    ELSE
        BEGIN    
            SET @EdExpress = (
                               SELECT   Value
                               FROM     dbo.syConfigAppSetValues
                               WHERE    SettingId = 91
                                        AND CampusId IS NULL
                             );    
        END;    
			
									   
										   
    IF (
         SELECT COUNT(*)
         FROM   syConfigAppSetValues
         WHERE  SettingId = 43
                AND CampusId = @CampusId
       ) >= 1
        BEGIN    
            SET @GradeBookWeightingLevel = (
                                             SELECT Value
                                             FROM   dbo.syConfigAppSetValues
                                             WHERE  SettingId = 43
                                                    AND CampusId = @CampusId
                                           );      
	 
        END;     
    ELSE
        BEGIN    
            SET @GradeBookWeightingLevel = (
                                             SELECT Value
                                             FROM   dbo.syConfigAppSetValues
                                             WHERE  SettingId = 43
                                                    AND CampusId IS NULL
                                           );    
        END;    
		   
									  
					   
    IF (
         SELECT COUNT(*)
         FROM   syConfigAppSetValues
         WHERE  SettingId = 71
                AND CampusId = @CampusId
       ) >= 1
        BEGIN    
            SET @ShowExternshipTabs = (
                                        SELECT  Value
                                        FROM    dbo.syConfigAppSetValues
                                        WHERE   SettingId = 71
                                                AND CampusId = @CampusId
                                      );      
			
        END;     
    ELSE
        BEGIN    
            SET @ShowExternshipTabs = (
                                        SELECT  Value
                                        FROM    dbo.syConfigAppSetValues
                                        WHERE   SettingId = 71
                                                AND CampusId IS NULL
                                      );    
        END;    
				
				
    SET @SettingId = (
                       SELECT   SettingId
                       FROM     syConfigAppSettings
                       WHERE    KeyName LIKE '%TimeClockClassSection%'
                     );       
    IF (
         SELECT COUNT(*)
         FROM   syConfigAppSetValues
         WHERE  SettingId = @SettingId
                AND CampusId = @CampusId
       ) >= 1
        BEGIN    
            SET @TimeClockClassSection = (
                                           SELECT   LOWER(Value)
                                           FROM     dbo.syConfigAppSetValues
                                           WHERE    SettingId = @SettingId
                                                    AND CampusId = @CampusId
                                         );      
			
        END;     
    ELSE
        BEGIN    
            SET @TimeClockClassSection = (
                                           SELECT   LOWER(Value)
                                           FROM     dbo.syConfigAppSetValues
                                           WHERE    SettingId = @SettingId
                                                    AND CampusId IS NULL
                                         );    
        END;    
		
		
	                                                                     
                                                                     
                                                                     
                                             
-- IPEDS reports
    IF @ResourceTypeId = 689
        BEGIN
            SELECT DISTINCT
                    *
                   ,CASE WHEN AccessLevel = 15 THEN 'Full Access'
                         ELSE CASE WHEN AccessLevel = 12 THEN 'Add and Edit Only'
                                   ELSE CASE WHEN AccessLevel = 10 THEN 'Edit and Delete Only'
                                             ELSE CASE WHEN AccessLevel = 6 THEN 'Add and Delete Only'
                                                       ELSE CASE WHEN AccessLevel = 4 THEN 'Add Only'
                                                                 ELSE CASE WHEN AccessLevel = 8 THEN 'Edit Only'
                                                                           ELSE CASE WHEN AccessLevel = 1 THEN 'Display Only'
                                                                                     ELSE NULL
                                                                                END
                                                                      END
                                                            END
                                                  END
                                        END
                              END
                    END AS PermissionText
                   ,NULL AS TabId
            FROM    (
                      SELECT    26 AS ModuleResourceId
                               ,689 AS TabId
                               ,ResourceID AS ChildResourceId
                               ,Resource AS ChildResource
                               ,(
                                  SELECT    MAX(AccessLevel)
                                  FROM      syRlsResLvls
                                  WHERE     RoleId = @RoleId
                                            AND ResourceID = syResources.ResourceID
                                ) AS AccessLevel
                               ,ResourceURL AS ChildResourceURL
                               ,NULL AS ParentResourceId
                               ,NULL AS ParentResource
                               ,1 AS GroupSortOrder
                               ,NULL AS FirstSortOrder
                               ,NULL AS DisplaySequence
                               ,ResourceTypeID
                      FROM      syResources
                      WHERE     ResourceID = 26
                      UNION
                      SELECT    26 AS ModuleResourceId
                               ,689 AS TabId
                               ,ChildResourceId
                               ,ChildResource
                               ,(
                                  SELECT    MAX(AccessLevel)
                                  FROM      syRlsResLvls
                                  WHERE     RoleId = @RoleId
                                            AND ResourceID = t1.ChildResourceId
                                ) AS AccessLevel
                               ,ChildResourceURL
                               ,ParentResourceId
                               ,ParentResource
                               ,CASE WHEN (
                                            t1.ChildResourceId = 409
                                            OR t1.ParentResourceId IN ( 409 )
                                          ) THEN 2
                                     WHEN (
                                            t1.ChildResourceId = 719
                                            OR t1.ParentResourceId IN ( 719 )
                                          ) THEN 3
                                     WHEN (
                                            t1.ChildResourceId = 720
                                            OR t1.ParentResourceId IN ( 720 )
                                          ) THEN 4
                                     WHEN (
                                            t1.ChildResourceId = 721
                                            OR t1.ParentResourceId IN ( 721 )
                                          ) THEN 5
                                     WHEN (
                                            t1.ChildResourceId = 725
                                            OR t1.ParentResourceId IN ( 725 )
                                          ) THEN 6
                                     WHEN (
                                            t1.ChildResourceId = 722
                                            OR t1.ParentResourceId IN ( 722 )
                                          ) THEN 7
                                     WHEN (
                                            t1.ChildResourceId = 723
                                            OR t1.ParentResourceId IN ( 723 )
                                          ) THEN 8
                                     WHEN (
                                            t1.ChildResourceId = 724
                                            OR t1.ParentResourceId IN ( 724 )
                                          ) THEN 9
                                     WHEN (
                                            t1.ChildResourceId = 789
                                            OR t1.ParentResourceId IN ( 789 )
                                          ) THEN 10
                                     ELSE 11
                                END AS GroupSortOrder
                               ,CASE WHEN ChildResourceId = 409 THEN 1
                                     ELSE 2
                                END AS FirstSortOrder
                               ,
						
						--( SELECT TOP 1
						--            HierarchyIndex
						--  FROM      dbo.syNavigationNodes
						--  WHERE     ResourceId = ChildResourceId
						--) AS FirstSortOrder,
                                CASE WHEN (
                                            ChildResourceId IN ( 409 )
                                            OR ParentResourceId IN ( 409 )
                                          ) THEN 1
                                     ELSE 2
                                END AS DisplaySequence
                               ,ResourceTypeID
                      FROM      (
                                  SELECT DISTINCT
                                            NNChild.ResourceId AS ChildResourceId
                                           ,RChild.Resource AS ChildResource
                                           ,RChild.ResourceURL AS ChildResourceURL
                                           ,CASE WHEN (
                                                        NNParent.ResourceId IN ( 689 )
                                                        OR NNChild.ResourceId IN ( 409,719,720,721,725,722,723,724,789 )
                                                      ) THEN NULL
                                                 ELSE NNParent.ResourceId
                                            END AS ParentResourceId
                                           ,RParent.Resource AS ParentResource
                                           ,RParentModule.Resource AS MODULE
                                           ,RChild.ResourceTypeID
                                  FROM      syResources RChild
                                  INNER JOIN syNavigationNodes NNChild ON RChild.ResourceID = NNChild.ResourceId
                                  INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                                  INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                                  LEFT OUTER JOIN (
                                                    SELECT  *
                                                    FROM    syResources
                                                    WHERE   ResourceTypeID = 1
                                                  ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                                  WHERE     RChild.ResourceTypeID IN ( 2,5,8 )
                                            AND (
                                                  RChild.ChildTypeId IS NULL
                                                  OR RChild.ChildTypeId = 5
                                                )
                                            AND ( RChild.ResourceID NOT IN ( 691,690,729,730,736,678,692,727,472,474,473 ) )   -- 394, 472, 474, 711 ,712
                                            AND (
                                                  NNParent.ParentId IN ( SELECT HierarchyId
                                                                         FROM   syNavigationNodes
                                                                         WHERE  ResourceId = 26 )
                                                  OR NNChild.ParentId IN ( SELECT   HierarchyId
                                                                           FROM     syNavigationNodes
                                                                           WHERE    ResourceId IN ( 409,719,720,721,722,723,724,725,789 ) )
                                                  AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
                                                )
                                ) t1
                    ) t2
            ORDER BY GroupSortOrder
                   ,ParentResourceId
                   ,ChildResource;
	 
        END;	
	-- Popup pages starts here			
    IF @ResourceTypeId = 999
        BEGIN
            SELECT DISTINCT
                    *
                   ,CASE WHEN AccessLevel = 15 THEN 'Full Access'
                         ELSE CASE WHEN AccessLevel = 12 THEN 'Add and Edit Only'
                                   ELSE CASE WHEN AccessLevel = 10 THEN 'Edit and Delete Only'
                                             ELSE CASE WHEN AccessLevel = 6 THEN 'Add and Delete Only'
                                                       ELSE CASE WHEN AccessLevel = 4 THEN 'Add Only'
                                                                 ELSE CASE WHEN AccessLevel = 8 THEN 'Edit Only'
                                                                           ELSE CASE WHEN AccessLevel = 1 THEN 'Display Only'
                                                                                     ELSE NULL
                                                                                END
                                                                      END
                                                            END
                                                  END
                                        END
                              END
                    END AS PermissionText
                   ,NULL AS TabId1
            FROM    (
                      SELECT DISTINCT
                                NNChild.ResourceId AS ChildResourceId
                               ,RChild.Resource AS ChildResource
                               ,(
                                  SELECT    AccessLevel
                                  FROM      syRlsResLvls
                                  WHERE     RoleId = @RoleId
                                            AND ResourceID = RChild.ResourceID
                                ) AS AccessLevel
                               ,RChild.ResourceURL AS ChildResourceURL
                               ,CASE WHEN (
                                            NNParent.ResourceId IN ( 689 )
                                            OR NNChild.ResourceId IN ( 737,738,739,740,741,742 )
                                            OR RChild.ResourceID IN ( 6,38,40,41,43,44,84,85,94,169,174,213,238,277,290,295,317,321,322,336,340,373,380,506,578 )
                                          ) THEN NULL
                                     WHEN NNChild.ResourceId = 475 THEN 38
                                     WHEN NNChild.ResourceId = 61 THEN 41
                                     ELSE NNParent.ResourceId
                                END AS ParentResourceId
                               ,CASE WHEN NNChild.ResourceId IN ( 6,38,40,41,43,44,61,84,85,94,169,174,213,238,277,290,295,317,321,322,336,340,373,380,475,506,
                                                                  578 ) THEN NULL
                                     ELSE RParent.Resource
                                END AS ParentResource
                               ,GroupSortOrder = CASE WHEN (
                                                             NNChild.ResourceId = 6
                                                             OR NNParent.ResourceId = 6
                                                           ) THEN 1
                                                      WHEN (
                                                             NNChild.ResourceId = 38
                                                             OR NNParent.ResourceId = 38
                                                           ) THEN 2
                                                      WHEN (
                                                             NNChild.ResourceId = 40
                                                             OR NNParent.ResourceId = 40
                                                           ) THEN 3
                                                      WHEN (
                                                             NNChild.ResourceId = 41
                                                             OR NNParent.ResourceId = 41
                                                           ) THEN 4
                                                      WHEN (
                                                             NNChild.ResourceId = 43
                                                             OR NNParent.ResourceId = 43
                                                           ) THEN 5
                                                      WHEN (
                                                             NNChild.ResourceId = 44
                                                             OR NNParent.ResourceId = 44
                                                           ) THEN 6
                                                      WHEN (
                                                             NNChild.ResourceId = 61
                                                             OR NNParent.ResourceId = 61
                                                           ) THEN 7
                                                      WHEN (
                                                             NNChild.ResourceId = 84
                                                             OR NNParent.ResourceId = 84
                                                           ) THEN 8
                                                      WHEN (
                                                             NNChild.ResourceId = 85
                                                             OR NNParent.ResourceId = 85
                                                           ) THEN 9
                                                      WHEN (
                                                             NNChild.ResourceId = 94
                                                             OR NNParent.ResourceId = 94
                                                           ) THEN 10
                                                      WHEN (
                                                             NNChild.ResourceId = 169
                                                             OR NNParent.ResourceId = 169
                                                           ) THEN 11
                                                      WHEN (
                                                             NNChild.ResourceId = 174
                                                             OR NNParent.ResourceId = 174
                                                           ) THEN 12
                                                      WHEN (
                                                             NNChild.ResourceId = 213
                                                             OR NNParent.ResourceId = 213
                                                           ) THEN 13
                                                      WHEN (
                                                             NNChild.ResourceId = 238
                                                             OR NNParent.ResourceId = 238
                                                           ) THEN 14
                                                      WHEN (
                                                             NNChild.ResourceId = 277
                                                             OR NNParent.ResourceId = 277
                                                           ) THEN 15
                                                      WHEN (
                                                             NNChild.ResourceId = 290
                                                             OR NNParent.ResourceId = 290
                                                           ) THEN 16
                                                      WHEN (
                                                             NNChild.ResourceId = 295
                                                             OR NNParent.ResourceId = 295
                                                           ) THEN 17
                                                      WHEN (
                                                             NNChild.ResourceId = 317
                                                             OR NNParent.ResourceId = 317
                                                           ) THEN 18
                                                      WHEN (
                                                             NNChild.ResourceId = 321
                                                             OR NNParent.ResourceId = 321
                                                           ) THEN 19
                                                      WHEN (
                                                             NNChild.ResourceId = 322
                                                             OR NNParent.ResourceId = 322
                                                           ) THEN 20
                                                      WHEN (
                                                             NNChild.ResourceId = 336
                                                             OR NNParent.ResourceId = 336
                                                           ) THEN 21
                                                      WHEN (
                                                             NNChild.ResourceId = 340
                                                             OR NNParent.ResourceId = 340
                                                           ) THEN 22
                                                      WHEN (
                                                             NNChild.ResourceId = 373
                                                             OR NNParent.ResourceId = 373
                                                           ) THEN 23
                                                      WHEN (
                                                             NNChild.ResourceId = 380
                                                             OR NNParent.ResourceId = 380
                                                           ) THEN 24
                                                      WHEN (
                                                             NNChild.ResourceId = 475
                                                             OR NNParent.ResourceId = 475
                                                           ) THEN 25
                                                      WHEN (
                                                             NNChild.ResourceId = 506
                                                             OR NNParent.ResourceId = 506
                                                           ) THEN 26
                                                      WHEN (
                                                             NNChild.ResourceId = 578
                                                             OR NNParent.ResourceId = 578
                                                           ) THEN 27
                                                      ELSE 28
                                                 END
                               ,FirstSortOrder = CASE WHEN (
                                                             NNChild.ResourceId = 6
                                                             OR NNParent.ResourceId = 6
                                                           ) THEN 1
                                                      WHEN (
                                                             NNChild.ResourceId = 38
                                                             OR NNParent.ResourceId = 38
                                                           ) THEN 2
                                                      WHEN (
                                                             NNChild.ResourceId = 40
                                                             OR NNParent.ResourceId = 40
                                                           ) THEN 3
                                                      WHEN (
                                                             NNChild.ResourceId = 41
                                                             OR NNParent.ResourceId = 41
                                                           ) THEN 4
                                                      WHEN (
                                                             NNChild.ResourceId = 43
                                                             OR NNParent.ResourceId = 43
                                                           ) THEN 5
                                                      WHEN (
                                                             NNChild.ResourceId = 44
                                                             OR NNParent.ResourceId = 44
                                                           ) THEN 6
                                                      WHEN (
                                                             NNChild.ResourceId = 61
                                                             OR NNParent.ResourceId = 61
                                                           ) THEN 7
                                                      WHEN (
                                                             NNChild.ResourceId = 84
                                                             OR NNParent.ResourceId = 84
                                                           ) THEN 8
                                                      WHEN (
                                                             NNChild.ResourceId = 85
                                                             OR NNParent.ResourceId = 85
                                                           ) THEN 9
                                                      WHEN (
                                                             NNChild.ResourceId = 94
                                                             OR NNParent.ResourceId = 94
                                                           ) THEN 10
                                                      WHEN (
                                                             NNChild.ResourceId = 169
                                                             OR NNParent.ResourceId = 169
                                                           ) THEN 11
                                                      WHEN (
                                                             NNChild.ResourceId = 174
                                                             OR NNParent.ResourceId = 174
                                                           ) THEN 12
                                                      WHEN (
                                                             NNChild.ResourceId = 213
                                                             OR NNParent.ResourceId = 213
                                                           ) THEN 13
                                                      WHEN (
                                                             NNChild.ResourceId = 238
                                                             OR NNParent.ResourceId = 238
                                                           ) THEN 14
                                                      WHEN (
                                                             NNChild.ResourceId = 277
                                                             OR NNParent.ResourceId = 277
                                                           ) THEN 15
                                                      WHEN (
                                                             NNChild.ResourceId = 290
                                                             OR NNParent.ResourceId = 290
                                                           ) THEN 16
                                                      WHEN (
                                                             NNChild.ResourceId = 295
                                                             OR NNParent.ResourceId = 295
                                                           ) THEN 17
                                                      WHEN (
                                                             NNChild.ResourceId = 317
                                                             OR NNParent.ResourceId = 317
                                                           ) THEN 18
                                                      WHEN (
                                                             NNChild.ResourceId = 321
                                                             OR NNParent.ResourceId = 321
                                                           ) THEN 19
                                                      WHEN (
                                                             NNChild.ResourceId = 322
                                                             OR NNParent.ResourceId = 322
                                                           ) THEN 20
                                                      WHEN (
                                                             NNChild.ResourceId = 336
                                                             OR NNParent.ResourceId = 336
                                                           ) THEN 21
                                                      WHEN (
                                                             NNChild.ResourceId = 340
                                                             OR NNParent.ResourceId = 340
                                                           ) THEN 22
                                                      WHEN (
                                                             NNChild.ResourceId = 373
                                                             OR NNParent.ResourceId = 373
                                                           ) THEN 23
                                                      WHEN (
                                                             NNChild.ResourceId = 380
                                                             OR NNParent.ResourceId = 380
                                                           ) THEN 24
                                                      WHEN (
                                                             NNChild.ResourceId = 475
                                                             OR NNParent.ResourceId = 475
                                                           ) THEN 25
                                                      WHEN (
                                                             NNChild.ResourceId = 506
                                                             OR NNParent.ResourceId = 506
                                                           ) THEN 26
                                                      WHEN (
                                                             NNChild.ResourceId = 578
                                                             OR NNParent.ResourceId = 578
                                                           ) THEN 27
                                                      ELSE 28
                                                 END
                               ,DisplaySequence = CASE WHEN (
                                                              NNChild.ResourceId = 6
                                                              OR NNParent.ResourceId = 6
                                                            ) THEN 1
                                                       WHEN (
                                                              NNChild.ResourceId = 38
                                                              OR NNParent.ResourceId = 38
                                                            ) THEN 2
                                                       WHEN (
                                                              NNChild.ResourceId = 40
                                                              OR NNParent.ResourceId = 40
                                                            ) THEN 3
                                                       WHEN (
                                                              NNChild.ResourceId = 41
                                                              OR NNParent.ResourceId = 41
                                                            ) THEN 4
                                                       WHEN (
                                                              NNChild.ResourceId = 43
                                                              OR NNParent.ResourceId = 43
                                                            ) THEN 5
                                                       WHEN (
                                                              NNChild.ResourceId = 44
                                                              OR NNParent.ResourceId = 44
                                                            ) THEN 6
                                                       WHEN (
                                                              NNChild.ResourceId = 61
                                                              OR NNParent.ResourceId = 61
                                                            ) THEN 7
                                                       WHEN (
                                                              NNChild.ResourceId = 84
                                                              OR NNParent.ResourceId = 84
                                                            ) THEN 8
                                                       WHEN (
                                                              NNChild.ResourceId = 85
                                                              OR NNParent.ResourceId = 85
                                                            ) THEN 9
                                                       WHEN (
                                                              NNChild.ResourceId = 94
                                                              OR NNParent.ResourceId = 94
                                                            ) THEN 10
                                                       WHEN (
                                                              NNChild.ResourceId = 169
                                                              OR NNParent.ResourceId = 169
                                                            ) THEN 11
                                                       WHEN (
                                                              NNChild.ResourceId = 174
                                                              OR NNParent.ResourceId = 174
                                                            ) THEN 12
                                                       WHEN (
                                                              NNChild.ResourceId = 213
                                                              OR NNParent.ResourceId = 213
                                                            ) THEN 13
                                                       WHEN (
                                                              NNChild.ResourceId = 238
                                                              OR NNParent.ResourceId = 238
                                                            ) THEN 14
                                                       WHEN (
                                                              NNChild.ResourceId = 277
                                                              OR NNParent.ResourceId = 277
                                                            ) THEN 15
                                                       WHEN (
                                                              NNChild.ResourceId = 290
                                                              OR NNParent.ResourceId = 290
                                                            ) THEN 16
                                                       WHEN (
                                                              NNChild.ResourceId = 295
                                                              OR NNParent.ResourceId = 295
                                                            ) THEN 17
                                                       WHEN (
                                                              NNChild.ResourceId = 317
                                                              OR NNParent.ResourceId = 317
                                                            ) THEN 18
                                                       WHEN (
                                                              NNChild.ResourceId = 321
                                                              OR NNParent.ResourceId = 321
                                                            ) THEN 19
                                                       WHEN (
                                                              NNChild.ResourceId = 322
                                                              OR NNParent.ResourceId = 322
                                                            ) THEN 20
                                                       WHEN (
                                                              NNChild.ResourceId = 336
                                                              OR NNParent.ResourceId = 336
                                                            ) THEN 21
                                                       WHEN (
                                                              NNChild.ResourceId = 340
                                                              OR NNParent.ResourceId = 340
                                                            ) THEN 22
                                                       WHEN (
                                                              NNChild.ResourceId = 373
                                                              OR NNParent.ResourceId = 373
                                                            ) THEN 23
                                                       WHEN (
                                                              NNChild.ResourceId = 380
                                                              OR NNParent.ResourceId = 380
                                                            ) THEN 24
                                                       WHEN (
                                                              NNChild.ResourceId = 475
                                                              OR NNParent.ResourceId = 475
                                                            ) THEN 25
                                                       WHEN (
                                                              NNChild.ResourceId = 506
                                                              OR NNParent.ResourceId = 506
                                                            ) THEN 26
                                                       WHEN (
                                                              NNChild.ResourceId = 578
                                                              OR NNParent.ResourceId = 578
                                                            ) THEN 27
                                                       ELSE 28
                                                  END
                               ,CASE WHEN RChild.ResourceID IN ( 6,38,40,41,43,44,84,85,94,169,174,213,238,277,290,295,317,321,322,336,340,373,380,506,578 )
                                     THEN 2
                                     WHEN RChild.ResourceID IN ( 287,288,475,65,64,475,61 ) THEN 4
                                     ELSE RChild.ResourceTypeID
                                END AS ResourceTypeId
                               ,NULL AS TabId
                      FROM      syResources RChild
                      INNER JOIN syNavigationNodes NNChild ON RChild.ResourceID = NNChild.ResourceId
                      INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                      INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                      INNER JOIN syNavigationNodes NNSubMenuParent ON NNParent.ParentId = NNSubMenuParent.HierarchyId
                      INNER JOIN syNavigationNodes NNModuleParent ON NNSubMenuParent.ParentId = NNModuleParent.HierarchyId
                      WHERE     NNChild.IsPopupWindow = 1
                                AND RChild.ResourceID NOT IN ( 578,293,357,294,295 )
                      UNION
                      SELECT    ResourceID
                               ,Resource
                               ,NULL AS AccessLevel
                               ,ResourceURL
                               ,NULL AS ParentResourceId
                               ,NULL AS ParentResource
                               ,GroupSortOrder = CASE WHEN (
                                                             ResourceID = 6
                                                             OR ResourceID = 6
                                                           ) THEN 1
                                                      WHEN (
                                                             ResourceID = 38
                                                             OR ResourceID = 38
                                                           ) THEN 2
                                                      WHEN (
                                                             ResourceID = 40
                                                             OR ResourceID = 40
                                                           ) THEN 3
                                                      WHEN (
                                                             ResourceID = 41
                                                             OR ResourceID = 41
                                                           ) THEN 4
                                                      WHEN (
                                                             ResourceID = 43
                                                             OR ResourceID = 43
                                                           ) THEN 5
                                                      WHEN (
                                                             ResourceID = 44
                                                             OR ResourceID = 44
                                                           ) THEN 6
                                                      WHEN (
                                                             ResourceID = 61
                                                             OR ResourceID = 61
                                                           ) THEN 7
                                                      WHEN (
                                                             ResourceID = 84
                                                             OR ResourceID = 84
                                                           ) THEN 8
                                                      WHEN (
                                                             ResourceID = 85
                                                             OR ResourceID = 85
                                                           ) THEN 9
                                                      WHEN (
                                                             ResourceID = 94
                                                             OR ResourceID = 94
                                                           ) THEN 10
                                                      WHEN (
                                                             ResourceID = 169
                                                             OR ResourceID = 169
                                                           ) THEN 11
                                                      WHEN (
                                                             ResourceID = 174
                                                             OR ResourceID = 174
                                                           ) THEN 12
                                                      WHEN (
                                                             ResourceID = 213
                                                             OR ResourceID = 213
                                                           ) THEN 13
                                                      WHEN (
                                                             ResourceID = 238
                                                             OR ResourceID = 238
                                                           ) THEN 14
                                                      WHEN (
                                                             ResourceID = 277
                                                             OR ResourceID = 277
                                                           ) THEN 15
                                                      WHEN (
                                                             ResourceID = 290
                                                             OR ResourceID = 290
                                                           ) THEN 16
                                                      WHEN (
                                                             ResourceID = 295
                                                             OR ResourceID = 295
                                                           ) THEN 17
                                                      WHEN (
                                                             ResourceID = 317
                                                             OR ResourceID = 317
                                                           ) THEN 18
                                                      WHEN (
                                                             ResourceID = 321
                                                             OR ResourceID = 321
                                                           ) THEN 19
                                                      WHEN (
                                                             ResourceID = 322
                                                             OR ResourceID = 322
                                                           ) THEN 20
                                                      WHEN (
                                                             ResourceID = 336
                                                             OR ResourceID = 336
                                                           ) THEN 21
                                                      WHEN (
                                                             ResourceID = 340
                                                             OR ResourceID = 340
                                                           ) THEN 22
                                                      WHEN (
                                                             ResourceID = 373
                                                             OR ResourceID = 373
                                                           ) THEN 23
                                                      WHEN (
                                                             ResourceID = 380
                                                             OR ResourceID = 380
                                                           ) THEN 24
                                                      WHEN (
                                                             ResourceID = 475
                                                             OR ResourceID = 475
                                                           ) THEN 25
                                                      WHEN (
                                                             ResourceID = 506
                                                             OR ResourceID = 506
                                                           ) THEN 26
                                                      WHEN (
                                                             ResourceID = 578
                                                             OR ResourceID = 578
                                                           ) THEN 27
                                                 END
                               ,FirstSortOrder = CASE WHEN (
                                                             ResourceID = 6
                                                             OR ResourceID = 6
                                                           ) THEN 1
                                                      WHEN (
                                                             ResourceID = 38
                                                             OR ResourceID = 38
                                                           ) THEN 2
                                                      WHEN (
                                                             ResourceID = 40
                                                             OR ResourceID = 40
                                                           ) THEN 3
                                                      WHEN (
                                                             ResourceID = 41
                                                             OR ResourceID = 41
                                                           ) THEN 4
                                                      WHEN (
                                                             ResourceID = 43
                                                             OR ResourceID = 43
                                                           ) THEN 5
                                                      WHEN (
                                                             ResourceID = 44
                                                             OR ResourceID = 44
                                                           ) THEN 6
                                                      WHEN (
                                                             ResourceID = 61
                                                             OR ResourceID = 61
                                                           ) THEN 7
                                                      WHEN (
                                                             ResourceID = 84
                                                             OR ResourceID = 84
                                                           ) THEN 8
                                                      WHEN (
                                                             ResourceID = 85
                                                             OR ResourceID = 85
                                                           ) THEN 9
                                                      WHEN (
                                                             ResourceID = 94
                                                             OR ResourceID = 94
                                                           ) THEN 10
                                                      WHEN (
                                                             ResourceID = 169
                                                             OR ResourceID = 169
                                                           ) THEN 11
                                                      WHEN (
                                                             ResourceID = 174
                                                             OR ResourceID = 174
                                                           ) THEN 12
                                                      WHEN (
                                                             ResourceID = 213
                                                             OR ResourceID = 213
                                                           ) THEN 13
                                                      WHEN (
                                                             ResourceID = 238
                                                             OR ResourceID = 238
                                                           ) THEN 14
                                                      WHEN (
                                                             ResourceID = 277
                                                             OR ResourceID = 277
                                                           ) THEN 15
                                                      WHEN (
                                                             ResourceID = 290
                                                             OR ResourceID = 290
                                                           ) THEN 16
                                                      WHEN (
                                                             ResourceID = 295
                                                             OR ResourceID = 295
                                                           ) THEN 17
                                                      WHEN (
                                                             ResourceID = 317
                                                             OR ResourceID = 317
                                                           ) THEN 18
                                                      WHEN (
                                                             ResourceID = 321
                                                             OR ResourceID = 321
                                                           ) THEN 19
                                                      WHEN (
                                                             ResourceID = 322
                                                             OR ResourceID = 322
                                                           ) THEN 20
                                                      WHEN (
                                                             ResourceID = 336
                                                             OR ResourceID = 336
                                                           ) THEN 21
                                                      WHEN (
                                                             ResourceID = 340
                                                             OR ResourceID = 340
                                                           ) THEN 22
                                                      WHEN (
                                                             ResourceID = 373
                                                             OR ResourceID = 373
                                                           ) THEN 23
                                                      WHEN (
                                                             ResourceID = 380
                                                             OR ResourceID = 380
                                                           ) THEN 24
                                                      WHEN (
                                                             ResourceID = 475
                                                             OR ResourceID = 475
                                                           ) THEN 25
                                                      WHEN (
                                                             ResourceID = 506
                                                             OR ResourceID = 506
                                                           ) THEN 26
                                                      WHEN (
                                                             ResourceID = 578
                                                             OR ResourceID = 578
                                                           ) THEN 27
                                                 END
                               ,DisplaySequence = CASE WHEN ( ResourceID = 6 ) THEN 1
                                                       WHEN ( ResourceID = 38 ) THEN 2
                                                       WHEN ( ResourceID = 40 ) THEN 3
                                                       WHEN ( ResourceID = 41 ) THEN 4
                                                       WHEN (
                                                              ResourceID = 43
                                                              OR ResourceID = 43
                                                            ) THEN 5
                                                       WHEN (
                                                              ResourceID = 44
                                                              OR ResourceID = 44
                                                            ) THEN 6
                                                       WHEN (
                                                              ResourceID = 61
                                                              OR ResourceID = 61
                                                            ) THEN 7
                                                       WHEN (
                                                              ResourceID = 84
                                                              OR ResourceID = 84
                                                            ) THEN 8
                                                       WHEN (
                                                              ResourceID = 85
                                                              OR ResourceID = 85
                                                            ) THEN 9
                                                       WHEN (
                                                              ResourceID = 94
                                                              OR ResourceID = 94
                                                            ) THEN 10
                                                       WHEN (
                                                              ResourceID = 169
                                                              OR ResourceID = 169
                                                            ) THEN 11
                                                       WHEN (
                                                              ResourceID = 174
                                                              OR ResourceID = 174
                                                            ) THEN 12
                                                       WHEN (
                                                              ResourceID = 213
                                                              OR ResourceID = 213
                                                            ) THEN 13
                                                       WHEN (
                                                              ResourceID = 238
                                                              OR ResourceID = 238
                                                            ) THEN 14
                                                       WHEN (
                                                              ResourceID = 277
                                                              OR ResourceID = 277
                                                            ) THEN 15
                                                       WHEN (
                                                              ResourceID = 290
                                                              OR ResourceID = 290
                                                            ) THEN 16
                                                       WHEN (
                                                              ResourceID = 295
                                                              OR ResourceID = 295
                                                            ) THEN 17
                                                       WHEN (
                                                              ResourceID = 317
                                                              OR ResourceID = 317
                                                            ) THEN 18
                                                       WHEN (
                                                              ResourceID = 321
                                                              OR ResourceID = 321
                                                            ) THEN 19
                                                       WHEN (
                                                              ResourceID = 322
                                                              OR ResourceID = 322
                                                            ) THEN 20
                                                       WHEN (
                                                              ResourceID = 336
                                                              OR ResourceID = 336
                                                            ) THEN 21
                                                       WHEN (
                                                              ResourceID = 340
                                                              OR ResourceID = 340
                                                            ) THEN 22
                                                       WHEN (
                                                              ResourceID = 373
                                                              OR ResourceID = 373
                                                            ) THEN 23
                                                       WHEN (
                                                              ResourceID = 380
                                                              OR ResourceID = 380
                                                            ) THEN 24
                                                       WHEN (
                                                              ResourceID = 475
                                                              OR ResourceID = 475
                                                            ) THEN 25
                                                       WHEN (
                                                              ResourceID = 506
                                                              OR ResourceID = 506
                                                            ) THEN 26
                                                       WHEN (
                                                              ResourceID = 578
                                                              OR ResourceID = 578
                                                            ) THEN 27
                                                  END
                               ,2 AS ResourceTypeId
                               ,NULL AS TabId
                      FROM      syResources 
		--where ResourceId in (6,38,40,41,43,44,61,84,85,94,169,174,213,238,277,290,295,317,321,322,336,340,373,380,506,578)
                      WHERE     ResourceID IN ( 6,38,40,41,43,44,61,169,174,213,336,380,506,578 )
                    ) tbl1
            ORDER BY GroupSortOrder
                   ,ParentResourceId
                   ,ChildResource;
        END;
-- Popup pages end here


    IF @ResourceTypeId = 3
        AND @tabid = 0
        BEGIN
            SELECT DISTINCT
                    *
                   ,
	--CASE WHEN AccessLevel=15 THEN 1 ELSE 0 END AS FullPermission,
	--CASE WHEN AccessLevel IN (14,13,12,11,10,9,8) THEN 1 ELSE 0 END AS EditPermission,
	--CASE WHEN AccessLevel IN (14,13,12,7,6,5,4) THEN 1 ELSE 0 END AS AddPermission,
	--CASE WHEN AccessLevel IN (14,11,10,7,6,2) THEN 1 ELSE 0 END AS DeletePermission,
	--CASE WHEN AccessLevel IN (13,11,9,7,5,3,1) THEN 1 ELSE 0 END AS ReadPermission,
	--NULL as TabId
                    CASE WHEN AccessLevel = 15 THEN 'Full Access'
                         ELSE CASE WHEN AccessLevel = 12 THEN 'Add and Edit Only'
                                   ELSE CASE WHEN AccessLevel = 10 THEN 'Edit and Delete Only'
                                             ELSE CASE WHEN AccessLevel = 6 THEN 'Add and Delete Only'
                                                       ELSE CASE WHEN AccessLevel = 4 THEN 'Add Only'
                                                                 ELSE CASE WHEN AccessLevel = 8 THEN 'Edit Only'
                                                                           ELSE CASE WHEN AccessLevel = 1 THEN 'Display Only'
                                                                                     ELSE NULL
                                                                                END
                                                                      END
                                                            END
                                                  END
                                        END
                              END
                    END AS PermissionText
                   ,NULL AS TabId
            FROM    (
                      --Admissions
                      SELECT    189 AS ModuleResourceId
                               ,ResourceID AS ChildResourceId
                               ,Resource AS ChildResource
                               ,(
                                  SELECT    AccessLevel
                                  FROM      syRlsResLvls
                                  WHERE     RoleId = @RoleId
                                            AND ResourceID = syResources.ResourceID
                                ) AS AccessLevel
                               ,ResourceURL AS ChildResourceURL
                               ,NULL AS ParentResourceId
                               ,NULL AS ParentResource
                               ,1 AS GroupSortOrder
                               ,NULL AS FirstSortOrder
                               ,NULL AS DisplaySequence
                               ,ResourceTypeID
                               ,3 AS ChildTypeId
                      FROM      syResources
                      WHERE     ResourceID = 189
                      UNION
                      SELECT    189 AS ModuleResourceId
                               ,NNChild.ResourceId AS ChildResourceId
                               ,CASE WHEN NNChild.ResourceId = 395 THEN 'Lead Tabs'
                                     ELSE CASE WHEN NNChild.ResourceId = 398 THEN 'Add/View Leads'
                                               ELSE RChild.Resource
                                          END
                                END AS ChildResource
                               ,(
                                  SELECT    AccessLevel
                                  FROM      syRlsResLvls
                                  WHERE     RoleId = @RoleId
                                            AND ResourceID = NNChild.ResourceId
                                ) AS AccessLevel
                               ,RChild.ResourceURL AS ChildResourceURL
                               ,CASE WHEN NNParent.ResourceId IN ( 687,688,689 ) THEN NULL
                                     ELSE NNParent.ResourceId
                                END AS ParentResourceId
                               ,RParent.Resource AS ParentResource
                               ,CASE WHEN (
                                            NNChild.ResourceId = 398
                                            OR NNParent.ResourceId = 398
                                          ) THEN 2
                                     ELSE CASE WHEN (
                                                      NNChild.ResourceId = 790  --US3272
                                                      OR NNParent.ResourceId = 790
                                                    ) THEN 3
                                               ELSE 4
                                          END
                                END AS GroupSortOrder
                               ,CASE WHEN (
                                            NNChild.ResourceId = 398
                                            OR NNParent.ResourceId = 398
                                          ) THEN 2
                                     ELSE CASE WHEN (
                                                      NNChild.ResourceId = 790  --US3272
                                                      OR NNParent.ResourceId = 790
                                                    ) THEN 3
                                               ELSE 4
                                          END
                                END AS FirstSortOrder
                               ,CASE WHEN (
                                            NNChild.ResourceId IN ( 398 )
                                            OR NNParent.ResourceId IN ( 398 )
                                          ) THEN 1
                                     ELSE CASE WHEN (
                                                      NNChild.ResourceId IN ( 790 )  --US3272
                                                      OR NNParent.ResourceId IN ( 790 )
                                                    ) THEN 2
                                               ELSE 3
                                          END
                                END AS DisplaySequence
                               ,RChild.ResourceTypeID
                               ,RChild.ChildTypeId
	--NNChild.HierarchyIndex AS SecondSortOrder,
	--RParentModule.Resource AS Module
                      FROM      syResources RChild
                      INNER JOIN syNavigationNodes NNChild ON RChild.ResourceID = NNChild.ResourceId
                      INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                      INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                      LEFT OUTER JOIN (
                                        SELECT  *
                                        FROM    syResources
                                        WHERE   ResourceTypeID = 1
                                      ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                      WHERE     RChild.ResourceTypeID IN ( 2,3,8 )
                                AND (
                                      RChild.ChildTypeId IS NULL
                                      OR RChild.ChildTypeId = 3
                                    )
                                AND ( RChild.ResourceID NOT IN ( 395 ) )
                                AND ( NNParent.ResourceId NOT IN ( 395 ) )
                                AND (
                                      NNParent.ParentId IN ( SELECT HierarchyId
                                                             FROM   syNavigationNodes
                                                             WHERE  ResourceId = 189 )
                                      OR NNChild.ParentId IN ( SELECT   HierarchyId
                                                               FROM     syNavigationNodes
                                                               WHERE    ResourceId IN ( 694,398,790 ) )  --US3272
                                    )
							 --DE8343
                                AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
-- Acdemic Records
                      UNION
                      SELECT    26 AS ModuleResourceId
                               ,ResourceID AS ChildResourceId
                               ,Resource AS ChildResource
                               ,(
                                  SELECT DISTINCT
                                            AccessLevel
                                  FROM      syRlsResLvls
                                  WHERE     RoleId = @RoleId
                                            AND ResourceID = syResources.ResourceID
                                ) AS AccessLevel
                               ,ResourceURL AS ChildResourceURL
                               ,NULL AS ParentResourceId
                               ,NULL AS ParentResource
                               ,1 AS GroupSortOrder
                               ,NULL AS FirstSortOrder
                               ,NULL AS DisplaySequence
                               ,ResourceTypeID
                               ,3 AS ChildTypeId
                      FROM      syResources
                      WHERE     ResourceID = 26
                      UNION
                      SELECT    26 AS ModuleResourceId
                               ,ChildResourceId
                               ,ChildResource
                               ,(
                                  SELECT DISTINCT
                                            AccessLevel
                                  FROM      syRlsResLvls
                                  WHERE     RoleId = @RoleId
                                            AND ResourceID = ChildResourceId
                                ) AS AccessLevel
                               ,ChildResourceURL
                               ,ParentResourceId
                               ,ParentResource
                               ,CASE WHEN (
                                            ChildResourceId = 190
                                            OR ParentResourceId = 190
                                          ) THEN 2
                                     ELSE CASE WHEN (
                                                      ChildResourceId = 129
                                                      OR ParentResourceId = 129
                                                    ) THEN 3
                                               ELSE CASE WHEN (
                                                                ChildResourceId = 71
                                                                OR ParentResourceId = 71
                                                              ) THEN 4
                                                         ELSE CASE WHEN (
                                                                          ChildResourceId = 693
                                                                          OR ParentResourceId = 693
                                                                        ) THEN 5
                                                                   ELSE CASE WHEN (
                                                                                    ChildResourceId = 160
                                                                                    OR ParentResourceId = 160
                                                                                  ) THEN 6
                                                                             ELSE CASE WHEN (
                                                                                              ChildResourceId = 132
                                                                                              OR ParentResourceId = 132
                                                                                            ) THEN 7
                                                                                       ELSE 8
                                                                                  END
                                                                        END
                                                              END
                                                    END
                                          END
                                END AS GroupSortOrder
                               ,(
                                  SELECT TOP 1
                                            HierarchyIndex
                                  FROM      dbo.syNavigationNodes
                                  WHERE     ResourceId = ChildResourceId
                                ) AS FirstSortOrder
                               ,CASE WHEN (
                                            ChildResourceId IN ( 190,129,317,71 )
                                            OR ParentResourceId IN ( 190,129,317,71 )
                                          ) THEN 1
                                     ELSE 2
                                END AS DisplaySequence
                               ,ResourceTypeID
                               ,ChildTypeId
                      FROM      (
                                  SELECT DISTINCT
                                            NNChild.ResourceId AS ChildResourceId
                                           ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                                 ELSE RChild.Resource
                                            END AS ChildResource
                                           ,RChild.ResourceURL AS ChildResourceURL
                                           ,CASE WHEN NNParent.ResourceId = 687 THEN NULL
                                                 ELSE NNParent.ResourceId
                                            END AS ParentResourceId
                                           ,RParent.Resource AS ParentResource
                                           ,RParentModule.Resource AS MODULE
                                           ,RChild.ResourceTypeID
                                           ,RChild.ChildTypeId
				--NNParent.ParentId
                                  FROM      syResources RChild
                                  INNER JOIN syNavigationNodes NNChild ON RChild.ResourceID = NNChild.ResourceId
                                  INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                                  INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                                  LEFT OUTER JOIN (
                                                    SELECT  *
                                                    FROM    syResources
                                                    WHERE   ResourceTypeID = 1
                                                  ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                                  WHERE     RChild.ResourceTypeID IN ( 2,3,8 )
                                            AND (
                                                  RChild.ChildTypeId IS NULL
                                                  OR RChild.ChildTypeId = 3
                                                )
                                            AND ( RChild.ResourceID <> 394 )
                                            AND ( NNParent.ResourceId NOT IN ( 394 ) )
                                            AND (
                                                  NNParent.ParentId IN ( SELECT HierarchyId
                                                                         FROM   syNavigationNodes
                                                                         WHERE  ResourceId = 26 )
                                                  OR NNChild.ParentId IN ( SELECT   HierarchyId
                                                                           FROM     syNavigationNodes
                                                                           WHERE    ResourceId IN ( 693,71,129,132,160,190 ) )
                                                )
											 --DE8343
                                            AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
                                ) t5
--Student Accounts
                      UNION
                      SELECT    194 AS ModuleResourceId
                               ,ResourceID AS ChildResourceId
                               ,Resource AS ChildResource
                               ,(
                                  SELECT    AccessLevel
                                  FROM      syRlsResLvls
                                  WHERE     RoleId = @RoleId
                                            AND ResourceID = syResources.ResourceID
                                ) AS AccessLevel
                               ,ResourceURL AS ChildResourceURL
                               ,NULL AS ParentResourceId
                               ,NULL AS ParentResource
                               ,1 AS GroupSortOrder
                               ,NULL AS FirstSortOrder
                               ,NULL AS DisplaySequence
                               ,ResourceTypeID
                               ,3 AS ChildTypeId
                      FROM      syResources
                      WHERE     ResourceID = 194
                      UNION
                      SELECT    194 AS ModuleResourceId
                               ,ChildResourceId
                               ,ChildResource
                               ,(
                                  SELECT DISTINCT
                                            AccessLevel
                                  FROM      syRlsResLvls
                                  WHERE     RoleId = @RoleId
                                            AND ResourceID = ChildResourceId
                                ) AS AccessLevel
                               ,ChildResourceURL
                               ,ParentResourceId
                               ,ParentResource
                               ,CASE WHEN (
                                            ChildResourceId = 695
                                            OR ParentResourceId = 695
                                          ) THEN 2
                                     ELSE CASE WHEN (
                                                      ChildResourceId = 403
                                                      OR ParentResourceId = 403
                                                    ) THEN 3
                                               ELSE 4
                                          END
                                END AS GroupSortOrder
                               ,(
                                  SELECT TOP 1
                                            HierarchyIndex
                                  FROM      dbo.syNavigationNodes
                                  WHERE     ResourceId = ChildResourceId
                                ) AS FirstSortOrder
                               ,CASE WHEN (
                                            ChildResourceId IN ( 695 )
                                            OR ParentResourceId IN ( 695 )
                                          ) THEN 1
                                     ELSE 2
                                END AS DisplaySequence
                               ,ResourceTypeID
                               ,ChildTypeId
                      FROM      (
                                  SELECT DISTINCT
                                            NNChild.ResourceId AS ChildResourceId
                                           ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                                 ELSE RChild.Resource
                                            END AS ChildResource
                                           ,RChild.ResourceURL AS ChildResourceURL
                                           ,CASE WHEN NNParent.ResourceId IN ( 194 )
                                                      OR NNChild.ResourceId IN ( 695,403,698 ) THEN NULL
                                                 ELSE NNParent.ResourceId
                                            END AS ParentResourceId
                                           ,RParent.Resource AS ParentResource
                                           ,RParentModule.Resource AS MODULE
                                           ,RChild.ResourceTypeID
                                           ,RChild.ChildTypeId
				--NNParent.ParentId
                                  FROM      syResources RChild
                                  INNER JOIN syNavigationNodes NNChild ON RChild.ResourceID = NNChild.ResourceId
                                  INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                                  INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                                  LEFT OUTER JOIN (
                                                    SELECT  *
                                                    FROM    syResources
                                                    WHERE   ResourceTypeID = 1
                                                  ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                                  WHERE     RChild.ResourceTypeID IN ( 2,3,8 )
                                            AND (
                                                  RChild.ChildTypeId IS NULL
                                                  OR RChild.ChildTypeId = 3
                                                )
                                            AND (
                                                  NNParent.ParentId IN ( SELECT HierarchyId
                                                                         FROM   syNavigationNodes
                                                                         WHERE  ResourceId = 194 )
                                                  OR NNChild.ParentId IN ( SELECT   HierarchyId
                                                                           FROM     syNavigationNodes
                                                                           WHERE    ResourceId IN ( 403,695,698 ) )
                                                )
                                            AND ( RChild.ResourceID <> 394 )
                                            AND ( NNParent.ResourceId NOT IN ( 394 ) )
											 --DE8343
                                            AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
                                ) t1 
--Faculty
                      UNION
                      SELECT    300 AS ModuleResourceId
                               ,ResourceID AS ChildResourceId
                               ,Resource AS ChildResource
                               ,(
                                  SELECT    AccessLevel
                                  FROM      syRlsResLvls
                                  WHERE     RoleId = @RoleId
                                            AND ResourceID = syResources.ResourceID
                                ) AS AccessLevel
                               ,ResourceURL AS ChildResourceURL
                               ,NULL AS ParentResourceId
                               ,NULL AS ParentResource
                               ,1 AS GroupSortOrder
                               ,NULL AS FirstSortOrder
                               ,NULL AS DisplaySequence
                               ,ResourceTypeID
                               ,3 AS ChildTypeId
                      FROM      syResources
                      WHERE     ResourceID = 300
                      UNION
                      SELECT    300 AS ModuleResourceId
                               ,ChildResourceId
                               ,ChildResource
                               ,(
                                  SELECT DISTINCT
                                            AccessLevel
                                  FROM      syRlsResLvls
                                  WHERE     RoleId = @RoleId
                                            AND ResourceID = ChildResourceId
                                ) AS AccessLevel
                               ,ChildResourceURL
                               ,ParentResourceId
                               ,ParentResource
                               ,CASE WHEN (
                                            ChildResourceId = 697
                                            OR ParentResourceId = 697
                                          ) THEN 2
                                     ELSE 3
                                END AS GroupSortOrder
                               ,(
                                  SELECT TOP 1
                                            HierarchyIndex
                                  FROM      dbo.syNavigationNodes
                                  WHERE     ResourceId = ChildResourceId
                                ) AS FirstSortOrder
                               ,1 AS DisplaySequence
                               ,ResourceTypeID
                               ,ChildTypeId
                      FROM      (
                                  SELECT DISTINCT
                                            NNChild.ResourceId AS ChildResourceId
                                           ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                                 ELSE RChild.Resource
                                            END AS ChildResource
                                           ,RChild.ResourceURL AS ChildResourceURL
                                           ,CASE WHEN NNParent.ResourceId = 300
                                                      OR NNChild.ResourceId = 697 THEN NULL
                                                 ELSE NNParent.ResourceId
                                            END AS ParentResourceId
                                           ,RParent.Resource AS ParentResource
                                           ,RParentModule.Resource AS MODULE
                                           ,RChild.ResourceTypeID
                                           ,RChild.ChildTypeId
				--NNParent.ParentId
                                  FROM      syResources RChild
                                  INNER JOIN syNavigationNodes NNChild ON RChild.ResourceID = NNChild.ResourceId
                                  INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                                  INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                                  LEFT OUTER JOIN (
                                                    SELECT  *
                                                    FROM    syResources
                                                    WHERE   ResourceTypeID = 1
                                                  ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                                  WHERE     RChild.ResourceTypeID IN ( 2,3,8 )
                                            AND (
                                                  RChild.ChildTypeId IS NULL
                                                  OR RChild.ChildTypeId = 3
                                                )
                                            AND ( RChild.ResourceID <> 394 )
                                            AND ( NNParent.ResourceId NOT IN ( 394 ) )
                                            AND (
                                                  NNParent.ParentId IN ( SELECT HierarchyId
                                                                         FROM   syNavigationNodes
                                                                         WHERE  ResourceId = 300 )
                                                  OR NNChild.ParentId IN ( SELECT   HierarchyId
                                                                           FROM     syNavigationNodes
                                                                           WHERE    ResourceId IN ( 697 ) )
                                                )
											 --DE8343
                                            AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
                                ) t3
                      UNION
--Financial Aid
                      SELECT    191 AS ModuleResourceId
                               ,ResourceID AS ChildResourceId
                               ,Resource AS ChildResource
                               ,(
                                  SELECT    AccessLevel
                                  FROM      syRlsResLvls
                                  WHERE     RoleId = @RoleId
                                            AND ResourceID = syResources.ResourceID
                                ) AS AccessLevel
                               ,ResourceURL AS ChildResourceURL
                               ,NULL AS ParentResourceId
                               ,NULL AS ParentResource
                               ,1 AS GroupSortOrder
                               ,NULL AS FirstSortOrder
                               ,NULL AS DisplaySequence
                               ,ResourceTypeID
                               ,ChildTypeId
                      FROM      syResources
                      WHERE     ResourceID = 191
                      UNION
                      SELECT    191 AS ModuleResourceId
                               ,ChildResourceId
                               ,ChildResource
                               ,(
                                  SELECT DISTINCT
                                            AccessLevel
                                  FROM      syRlsResLvls
                                  WHERE     RoleId = @RoleId
                                            AND ResourceID = ChildResourceId
                                ) AS AccessLevel
                               ,ChildResourceURL
                               ,ParentResourceId
                               ,ParentResource
                               ,CASE WHEN (
                                            ChildResourceId = 700
                                            OR ParentResourceId = 700
                                          ) THEN 2
                                     ELSE CASE WHEN (
                                                      ChildResourceId = 698
                                                      OR ParentResourceId = 698
                                                    ) THEN 3
                                               ELSE 4
                                          END
                                END AS GroupSortOrder
                               ,(
                                  SELECT TOP 1
                                            HierarchyIndex
                                  FROM      dbo.syNavigationNodes
                                  WHERE     ResourceId = ChildResourceId
                                ) AS FirstSortOrder
                               ,CASE WHEN (
                                            ChildResourceId IN ( 698 )
                                            OR ParentResourceId IN ( 698 )
                                          ) THEN 1
                                     ELSE 2
                                END AS DisplaySequence
                               ,ResourceTypeID
                               ,ChildTypeId
                      FROM      (
                                  SELECT DISTINCT
                                            NNChild.ResourceId AS ChildResourceId
                                           ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                                 ELSE RChild.Resource
                                            END AS ChildResource
                                           ,RChild.ResourceURL AS ChildResourceURL
                                           ,CASE WHEN (
                                                        NNParent.ResourceId = 191
                                                        OR NNChild.ResourceId = 698
                                                        OR NNChild.ResourceId = 699
                                                        OR NNChild.ResourceId = 700
                                                      ) THEN NULL
                                                 ELSE NNParent.ResourceId
                                            END AS ParentResourceId
                                           ,RParent.Resource AS ParentResource
                                           ,RParentModule.Resource AS MODULE
                                           ,RChild.ResourceTypeID
                                           ,RChild.ChildTypeId
				--NNParent.ParentId
                                  FROM      syResources RChild
                                  INNER JOIN syNavigationNodes NNChild ON RChild.ResourceID = NNChild.ResourceId
                                  INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                                  INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                                  LEFT OUTER JOIN (
                                                    SELECT  *
                                                    FROM    syResources
                                                    WHERE   ResourceTypeID = 1
                                                  ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                                  WHERE     RChild.ResourceTypeID IN ( 2,3,8 )
                                            AND (
                                                  RChild.ChildTypeId IS NULL
                                                  OR RChild.ChildTypeId = 3
                                                )
                                            AND ( RChild.ResourceID <> 394 )
                                            AND ( NNParent.ResourceId NOT IN ( 394 ) )
                                            AND (
                                                  NNParent.ParentId IN ( SELECT HierarchyId
                                                                         FROM   syNavigationNodes
                                                                         WHERE  ResourceId = 191 )
                                                  OR NNChild.ParentId IN ( SELECT   HierarchyId
                                                                           FROM     syNavigationNodes
                                                                           WHERE    ResourceId IN ( 700,698,699 ) )
                                                )
									 --DE8343
                                            AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
                                ) t4
--Human Resources Starts Here
                      UNION
                      SELECT    192 AS ModuleResourceId
                               ,ResourceID AS ChildResourceId
                               ,Resource AS ChildResource
                               ,(
                                  SELECT    AccessLevel
                                  FROM      syRlsResLvls
                                  WHERE     RoleId = @RoleId
                                            AND ResourceID = syResources.ResourceID
                                ) AS AccessLevel
                               ,ResourceURL AS ChildResourceURL
                               ,NULL AS ParentResourceId
                               ,NULL AS ParentResource
                               ,1 AS GroupSortOrder
                               ,NULL AS FirstSortOrder
                               ,NULL AS DisplaySequence
                               ,ResourceTypeID
                               ,3 AS ChildTypeId
                      FROM      syResources
                      WHERE     ResourceID = 192
                      UNION
                      SELECT    192 AS ModuleResourceId
                               ,NNChild.ResourceId AS ChildResourceId
                               ,
	--CASE WHEN NNChild.ResourceId=395 THEN 'Lead Tabs' ELSE 
	--CASE WHEN NNChild.ResourceId=398 THEN 'Add/View Leads'	ELSE RChild.Resource END 
	--END
                                RChild.Resource AS ChildResource
                               ,(
                                  SELECT    AccessLevel
                                  FROM      syRlsResLvls
                                  WHERE     RoleId = @RoleId
                                            AND ResourceID = NNChild.ResourceId
                                ) AS AccessLevel
                               ,RChild.ResourceURL AS ChildResourceURL
                               ,CASE WHEN NNParent.ResourceId IN ( 687,688,689 ) THEN NULL
                                     ELSE NNParent.ResourceId
                                END AS ParentResourceId
                               ,RParent.Resource AS ParentResource
                               ,CASE WHEN (
                                            NNChild.ResourceId = 708
                                            OR NNParent.ResourceId = 708
                                          ) THEN 2
                                     ELSE 3
                                END AS GroupSortOrder
                               ,CASE WHEN (
                                            NNChild.ResourceId = 708
                                            OR NNParent.ResourceId = 708
                                          ) THEN 2
                                     ELSE 3
                                END AS FirstSortOrder
                               ,
	--CASE WHEN (NNChild.ResourceId IN (398) OR NNParent.ResourceId IN (398)) THEN 1 ELSE 2 END AS DisplaySequence,
                                1 AS DisplaySequence
                               ,RChild.ResourceTypeID
                               ,RChild.ChildTypeId
	--NNChild.HierarchyIndex AS SecondSortOrder,
	--RParentModule.Resource AS Module
                      FROM      syResources RChild
                      INNER JOIN syNavigationNodes NNChild ON RChild.ResourceID = NNChild.ResourceId
                      INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                      INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                      LEFT OUTER JOIN (
                                        SELECT  *
                                        FROM    syResources
                                        WHERE   ResourceTypeID = 1
                                      ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                      WHERE     RChild.ResourceTypeID IN ( 2,3,8 )
                                AND (
                                      RChild.ChildTypeId IS NULL
                                      OR RChild.ChildTypeId = 3
                                    )
                                AND ( RChild.ResourceID NOT IN ( 396 ) )
                                AND ( NNParent.ResourceId NOT IN ( 396 ) )
                                AND (
                                      NNParent.ParentId IN ( SELECT HierarchyId
                                                             FROM   syNavigationNodes
                                                             WHERE  ResourceId = 192 )
                                      OR NNChild.ParentId IN ( SELECT   HierarchyId
                                                               FROM     syNavigationNodes
                                                               WHERE    ResourceId IN ( 708 ) )
                                    )
                      UNION
--Placement
                      SELECT    193 AS ModuleResourceId
                               ,ResourceID AS ChildResourceId
                               ,Resource AS ChildResource
                               ,(
                                  SELECT    AccessLevel
                                  FROM      syRlsResLvls
                                  WHERE     RoleId = @RoleId
                                            AND ResourceID = syResources.ResourceID
                                ) AS AccessLevel
                               ,ResourceURL AS ChildResourceURL
                               ,NULL AS ParentResourceId
                               ,NULL AS ParentResource
                               ,1 AS GroupSortOrder
                               ,NULL AS FirstSortOrder
                               ,NULL AS DisplaySequence
                               ,ResourceTypeID
                               ,3 AS ChildTypeId
                      FROM      syResources
                      WHERE     ResourceID = 193
                      UNION
                      SELECT    193 AS ModuleResourceId
                               ,ChildResourceId
                               ,ChildResource
                               ,(
                                  SELECT DISTINCT
                                            AccessLevel
                                  FROM      syRlsResLvls
                                  WHERE     RoleId = @RoleId
                                            AND ResourceID = ChildResourceId
                                ) AS AccessLevel
                               ,ChildResourceURL
                               ,ParentResourceId
                               ,ParentResource
                               ,CASE WHEN (
                                            ChildResourceId = 696
                                            OR ParentResourceId = 696
                                          ) THEN 2
                                     ELSE 3
                                END AS GroupSortOrder
                               ,(
                                  SELECT TOP 1
                                            HierarchyIndex
                                  FROM      dbo.syNavigationNodes
                                  WHERE     ResourceId = ChildResourceId
                                ) AS FirstSortOrder
                               ,CASE WHEN (
                                            ChildResourceId IN ( 696 )
                                            OR ParentResourceId IN ( 696 )
                                          ) THEN 1
                                     ELSE 2
                                END AS DisplaySequence
                               ,ResourceTypeID
                               ,ChildTypeId
                      FROM      (
                                  SELECT DISTINCT
                                            NNChild.ResourceId AS ChildResourceId
                                           ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                                 ELSE CASE WHEN NNChild.ResourceId = 397 THEN 'Employer Tabs'
                                                           ELSE RChild.Resource
                                                      END
                                            END AS ChildResource
                                           ,RChild.ResourceURL AS ChildResourceURL
                                           ,CASE WHEN (
                                                        NNParent.ResourceId = 193
                                                        OR NNChild.ResourceId IN ( 696,404 )
                                                      ) THEN NULL
                                                 ELSE NNParent.ResourceId
                                            END AS ParentResourceId
                                           ,RParent.Resource AS ParentResource
                                           ,RParentModule.Resource AS MODULE
                                           ,RChild.ResourceTypeID
                                           ,RChild.ChildTypeId
				--NNParent.ParentId
                                  FROM      syResources RChild
                                  INNER JOIN syNavigationNodes NNChild ON RChild.ResourceID = NNChild.ResourceId
                                  INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                                  INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                                  LEFT OUTER JOIN (
                                                    SELECT  *
                                                    FROM    syResources
                                                    WHERE   ResourceTypeID = 1
                                                  ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                                  WHERE     RChild.ResourceTypeID IN ( 2,3,8 )
                                            AND (
                                                  RChild.ChildTypeId IS NULL
                                                  OR RChild.ChildTypeId = 3
                                                )
                                            AND ( RChild.ResourceID <> 394 )
                                            AND ( NNParent.ResourceId NOT IN ( 394,397 ) )
                                            AND (
                                                  NNParent.ParentId IN ( SELECT HierarchyId
                                                                         FROM   syNavigationNodes
                                                                         WHERE  ResourceId = 193 )
                                                  OR NNChild.ParentId IN ( SELECT   HierarchyId
                                                                           FROM     syNavigationNodes
                                                                           WHERE    ResourceId IN ( 404,696 ) )
                                                )
									 --DE8343
                                            AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
                                ) t5
		--- DE7659 - Added for 2nd item in the defect - No item displayed in the System - common tasks                        
-- SYSTEM
                      UNION
                      SELECT    195 AS ModuleResourceId
                               ,ResourceID AS ChildResourceId
                               ,Resource AS ChildResource
                               ,(
                                  SELECT    AccessLevel
                                  FROM      syRlsResLvls
                                  WHERE     RoleId = @RoleId
                                            AND ResourceID = syResources.ResourceID
                                ) AS AccessLevel
                               ,ResourceURL AS ChildResourceURL
                               ,NULL AS ParentResourceId
                               ,NULL AS ParentResource
                               ,1 AS GroupSortOrder
                               ,NULL AS FirstSortOrder
                               ,NULL AS DisplaySequence
                               ,ResourceTypeID
                               ,ChildTypeId
                      FROM      syResources
                      WHERE     ResourceID = 195
                      UNION
                      SELECT    195 AS ModuleResourceId
                               ,ChildResourceId
                               ,ChildResource
                               ,(
                                  SELECT DISTINCT
                                            AccessLevel
                                  FROM      syRlsResLvls
                                  WHERE     RoleId = @RoleId
                                            AND ResourceID = ChildResourceId
                                ) AS AccessLevel
                               ,ChildResourceURL
                               ,ParentResourceId
                               ,ParentResource
                               ,CASE WHEN (
                                            ChildResourceId = 701
                                            OR ParentResourceId = 701
                                          ) THEN 2
                                     ELSE 3
                                END AS GroupSortOrder
                               ,(
                                  SELECT TOP 1
                                            HierarchyIndex
                                  FROM      dbo.syNavigationNodes
                                  WHERE     ResourceId = ChildResourceId
                                ) AS FirstSortOrder
                               ,CASE WHEN (
                                            ChildResourceId IN ( 701 )
                                            OR ParentResourceId IN ( 701 )
                                          ) THEN 1
                                     ELSE 2
                                END AS DisplaySequence
                               ,ResourceTypeID
                               ,ChildTypeId
                      FROM      (
                                  SELECT DISTINCT
                                            NNChild.ResourceId AS ChildResourceId
                                           ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                                 ELSE RChild.Resource
                                            END AS ChildResource
                                           ,RChild.ResourceURL AS ChildResourceURL
                                           ,CASE WHEN NNParent.ResourceId IN ( 195 )
                                                      OR NNChild.ResourceId IN ( 701,407 ) THEN NULL
                                                 ELSE NNParent.ResourceId
                                            END AS ParentResourceId
                                           ,RParent.Resource AS ParentResource
                                           ,RParentModule.Resource AS MODULE
                                           ,RChild.ResourceTypeID
                                           ,RChild.ChildTypeId
				--NNParent.ParentId
                                  FROM      syResources RChild
                                  INNER JOIN syNavigationNodes NNChild ON RChild.ResourceID = NNChild.ResourceId
                                  INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                                  INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                                  LEFT OUTER JOIN (
                                                    SELECT  *
                                                    FROM    syResources
                                                    WHERE   ResourceTypeID = 1
                                                  ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                                  WHERE     RChild.ResourceTypeID IN ( 2,3,8 )
                                            AND (
                                                  RChild.ChildTypeId IS NULL
                                                  OR RChild.ChildTypeId = 3
                                                )
                                            AND (
                                                  NNParent.ParentId IN ( SELECT HierarchyId
                                                                         FROM   syNavigationNodes
                                                                         WHERE  ResourceId = 195 )
                                                  OR NNChild.ParentId IN ( SELECT   HierarchyId
                                                                           FROM     syNavigationNodes
                                                                           WHERE    ResourceId IN ( 701,407 ) )
                                                )
								   --DE7659
                                            AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
                                ) t6
                    ) t2
            WHERE   t2.ModuleResourceId = @ModuleResourceId
                    AND (
                          t2.ResourceTypeID = 3
                          OR (
                               t2.ResourceTypeID = 2
                               AND t2.ChildTypeId = 3
                             )
                        )
						-- DE7659
                    AND -- Hide resources based on Configuration Settings
                    (
                      -- The following expression means if showross... is set to false, hide   
	 -- ResourceIds 541,542,532,534,535,538,543,539  
	 -- (Not False) OR (Condition)  
                      (
                        (
                          NOT LTRIM(RTRIM(@ShowRossOnlyTabs)) = 0
                        )
                        OR ( ChildResourceId NOT IN ( 541,542,532,538,543,539 ) )
                      )
                      AND  
	 -- The following expression means if showross... is set to true, hide   
	 -- ResourceIds 142,375,330,476,508,102,107,237  
	 -- (Not True) OR (Condition)  
                      (
                        (
                          NOT LTRIM(RTRIM(@ShowRossOnlyTabs)) = 1
                        )
                        OR ( ChildResourceId NOT IN ( 142,375,330,476,508,102,107,237,217,290 ) )
                      )
                      AND  
	 ---- The following expression means if SchedulingMethod=regulartraditional, hide   
	 ---- ResourceIds 91 and 497  
	 ---- (Not True) OR (Condition)  
                      (
                        (
                          NOT LOWER(LTRIM(RTRIM(@SchedulingMethod))) = 'regulartraditional'
                        )
                        OR ( ChildResourceId NOT IN ( 91,497 ) )
                      )
                      AND  
	 -- The following expression means if TrackSAPAttendance=byday, hide   
	 -- ResourceIds 633  
	 -- (Not True) OR (Condition)  
				 -- ( ( NOT LOWER(LTRIM(RTRIM(@TrackSAPAttendance))) = 'byday'
					--)
					--OR ( ChildResourceId NOT IN ( 633 ) )
				 -- )
				 -- AND  				 
	 ---- The following expression means if @ShowCollegeOfCourtReporting is set to false, hide   
	 ---- ResourceIds 614,615  
	 ---- (Not False) OR (Condition)  
                      (
                        (
                          NOT LOWER(LTRIM(RTRIM(@ShowCollegeOfCourtReporting))) = 'no'
                        )
                        OR ( ChildResourceId NOT IN ( 614,615 ) )
                      )
                      AND  
	 -- The following expression means if @ShowCollegeOfCourtReporting is set to true, hide   
	 -- ResourceIds 497  
	 -- (Not True) OR (Condition)  
                      (
                        (
                          NOT LOWER(LTRIM(RTRIM(@ShowCollegeOfCourtReporting))) = 'yes'
                        )
                        OR ( ChildResourceId NOT IN ( 497 ) )
                      )
                      AND  
	 -- The following expression means if FAMEESP is set to false, hide   
	 -- ResourceIds 517,523, 525  
	 -- (Not False) OR (Condition)  
                      (
                        (
                          NOT LOWER(LTRIM(RTRIM(@FameESP))) = 'no'
                        )
                        OR ( ChildResourceId NOT IN ( 517,523,525,699 ) )
                      )
                      AND  
	 ---- The following expression means if EDExpress is set to false, hide   
	 ---- ResourceIds 603,604,606,619  
	 ---- (Not False) OR (Condition)  
                      (
                        (
                          NOT LOWER(LTRIM(RTRIM(@EdExpress))) = 'no'
                        )
                        OR ( ChildResourceId NOT IN ( 603,604,605,606,619,698 ) )
                      )
                      AND  
	 ---- The following expression means if @GradeBookWeightingLevel is set to courselevel, hide   
	 ---- ResourceIds 107,96,222  
	 ---- (Not False) OR (Condition)  
                      (
                        (
                          NOT LOWER(LTRIM(RTRIM(@GradeBookWeightingLevel))) = 'courselevel'
                        )
                        OR ( ChildResourceId NOT IN ( 107,96,222 ) )
                      )
                      AND (
                            (
                              NOT LOWER(LTRIM(RTRIM(@GradeBookWeightingLevel))) = 'instructorlevel'
                            )
                            OR ( ChildResourceId NOT IN ( 476 ) )
                          )
                      AND (
                            (
                              NOT LOWER(LTRIM(RTRIM(@ShowExternshipTabs))) = 'no'
                            )
                            OR ( ChildResourceId NOT IN ( 543,538 ) )
                          )
	 --            AND
	 --            ---- If user is not a power user (sa or support or super), hide Setup Required Fields (234)
				 ------ (Not False) OR (Condition)    
	 --             ((NOT @IsUserAPowerUser = 0) OR (ChildResourceId NOT IN (234)))   
                    )
            ORDER BY GroupSortOrder
                   ,ParentResourceId
                   ,ChildResource;
        END;

    IF @ResourceTypeId = 4
        AND @tabid = 0
        BEGIN
            SELECT DISTINCT
                    *
                   ,CASE WHEN AccessLevel = 15 THEN 'Full Access'
                         ELSE CASE WHEN AccessLevel = 12 THEN 'Add and Edit Only'
                                   ELSE CASE WHEN AccessLevel = 10 THEN 'Edit and Delete Only'
                                             ELSE CASE WHEN AccessLevel = 6 THEN 'Add and Delete Only'
                                                       ELSE CASE WHEN AccessLevel = 4 THEN 'Add Only'
                                                                 ELSE CASE WHEN AccessLevel = 8 THEN 'Edit Only'
                                                                           ELSE CASE WHEN AccessLevel = 1 THEN 'Display Only'
                                                                                     ELSE NULL
                                                                                END
                                                                      END
                                                            END
                                                  END
                                        END
                              END
                    END AS PermissionText
                   ,NULL AS TabId
            FROM    (
                      SELECT    189 AS ModuleResourceId
                               ,ResourceID AS ChildResourceId
                               ,Resource AS ChildResource
                               ,(
                                  SELECT    AccessLevel
                                  FROM      syRlsResLvls
                                  WHERE     RoleId = @RoleId
                                            AND ResourceID = syResources.ResourceID
                                ) AS AccessLevel
                               ,ResourceURL AS ChildResourceURL
                               ,NULL AS ParentResourceId
                               ,NULL AS ParentResource
                               ,1 AS GroupSortOrder
                               ,NULL AS FirstSortOrder
                               ,NULL AS DisplaySequence
                               ,ResourceTypeID
                               ,4 AS ChildTypeId
                      FROM      syResources
                      WHERE     ResourceID = 189
                      UNION
                      SELECT    189 AS ModuleResourceId
                               ,NNChild.ResourceId AS ChildResourceId
                               ,CASE WHEN NNChild.ResourceId = 395 THEN 'Lead Tabs'
                                     ELSE CASE WHEN NNChild.ResourceId = 398 THEN 'Add/View Leads'
                                               ELSE RChild.Resource
                                          END
                                END AS ChildResource
                               ,(
                                  SELECT    AccessLevel
                                  FROM      syRlsResLvls
                                  WHERE     RoleId = @RoleId
                                            AND ResourceID = NNChild.ResourceId
                                ) AS AccessLevel
                               ,RChild.ResourceURL AS ChildResourceURL
                               ,CASE WHEN NNParent.ResourceId IN ( 687,688,689 ) THEN NULL
                                     ELSE NNParent.ResourceId
                                END AS ParentResourceId
                               ,RParent.Resource AS ParentResource
                               ,CASE WHEN (
                                            NNChild.ResourceId = 400
                                            OR NNParent.ResourceId = 400
                                          ) THEN 2
                                     ELSE CASE WHEN (
                                                      NNChild.ResourceId = 702
                                                      OR NNParent.ResourceId = 702
                                                    ) THEN 3
                                               ELSE CASE WHEN (
                                                                NNChild.ResourceId = 401
                                                                OR NNParent.ResourceId = 401
                                                              ) THEN 4
                                                         ELSE 5
                                                    END
                                          END
                                END AS GroupSortOrder
                               ,CASE WHEN NNChild.ResourceId = 398 THEN 1
                                     ELSE CASE WHEN NNChild.ResourceId = 395 THEN 2
                                               ELSE 3
                                          END
                                END AS FirstSortOrder
                               ,CASE WHEN (
                                            NNChild.ResourceId IN ( 400,702 )
                                            OR NNParent.ResourceId IN ( 400,702 )
                                          ) THEN 1
                                     ELSE 2
                                END AS DisplaySequence
                               ,RChild.ResourceTypeID
                               ,RChild.ChildTypeId
                      FROM      syResources RChild
                      INNER JOIN syNavigationNodes NNChild ON RChild.ResourceID = NNChild.ResourceId
                      INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                      INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                      LEFT OUTER JOIN (
                                        SELECT  *
                                        FROM    syResources
                                        WHERE   ResourceTypeID = 1
                                      ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                      WHERE     RChild.ResourceTypeID IN ( 2,4,8 )
                                AND (
                                      RChild.ChildTypeId IS NULL
                                      OR RChild.ChildTypeId = 4
                                    )
                                AND ( RChild.ResourceID NOT IN ( 395 ) )
                                AND ( NNParent.ResourceId NOT IN ( 395 ) )
                                AND (
                                      NNParent.ParentId IN ( SELECT HierarchyId
                                                             FROM   syNavigationNodes
                                                             WHERE  ResourceId = 189 )
                                      OR NNChild.ParentId IN ( SELECT   HierarchyId
                                                               FROM     syNavigationNodes
                                                               WHERE    ResourceId IN ( 702,400,401,399 ) )
                                    )
									 --DE8343
                                AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
                      UNION
                      SELECT    26 AS ModuleResourceId
                               ,ResourceID AS ChildResourceId
                               ,Resource AS ChildResource
                               ,(
                                  SELECT    AccessLevel
                                  FROM      syRlsResLvls
                                  WHERE     RoleId = @RoleId
                                            AND ResourceID = syResources.ResourceID
                                ) AS AccessLevel
                               ,ResourceURL AS ChildResourceURL
                               ,NULL AS ParentResourceId
                               ,NULL AS ParentResource
                               ,1 AS GroupSortOrder
                               ,NULL AS FirstSortOrder
                               ,NULL AS DisplaySequence
                               ,ResourceTypeID
                               ,4 AS ChildTypeId
                      FROM      syResources
                      WHERE     ResourceID = 26
                      UNION
                      SELECT    26 AS ModuleResourceId
                               ,ChildResourceId
                               ,ChildResource
                               ,(
                                  SELECT    AccessLevel
                                  FROM      syRlsResLvls
                                  WHERE     RoleId = @RoleId
                                            AND ResourceID = t1.ChildResourceId
                                ) AS AccessLevel
                               ,ChildResourceURL
                               ,ParentResourceId
                               ,ParentResource
                               ,GroupSortOrder
                               ,(
                                  SELECT TOP 1
                                            HierarchyIndex
                                  FROM      dbo.syNavigationNodes
                                  WHERE     ResourceId = ChildResourceId
                                ) AS FirstSortOrder
                               ,CASE WHEN (
                                            ChildResourceId IN ( 703,468 )
                                            OR ParentResourceId IN ( 703,468 )
                                          ) THEN 1
                                     ELSE 2
                                END AS DisplaySequence
                               ,ResourceTypeID
                               ,ChildTypeId
                      FROM      (
                                  SELECT DISTINCT
                                            NNChild.ResourceId AS ChildResourceId
                                           ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                                 ELSE RChild.Resource
                                            END AS ChildResource
                                           ,RChild.ResourceURL AS ChildResourceURL
                                           ,CASE WHEN (
                                                        NNParent.ResourceId = 687
                                                        OR NNChild.ResourceId IN ( 468,469,621,471,470,703 )
                                                      ) THEN NULL
                                                 ELSE NNParent.ResourceId
                                            END AS ParentResourceId
                                           ,RParent.Resource AS ParentResource
                                           ,RParentModule.Resource AS MODULE
                                           ,CASE WHEN (
                                                        NNChild.ResourceId = 468
                                                        OR NNParent.ResourceId = 468
                                                      ) THEN 2
                                                 ELSE CASE WHEN (
                                                                  NNChild.ResourceId = 703
                                                                  OR NNParent.ResourceId = 703
                                                                ) THEN 3
                                                           ELSE CASE WHEN (
                                                                            NNChild.ResourceId = 469
                                                                            OR NNParent.ResourceId = 469
                                                                          ) THEN 4
                                                                     ELSE CASE WHEN (
                                                                                      NNChild.ResourceId = 621
                                                                                      OR NNParent.ResourceId = 621
                                                                                    ) THEN 5
                                                                               ELSE CASE WHEN (
                                                                                                NNChild.ResourceId = 471
                                                                                                OR NNParent.ResourceId = 471
                                                                                              ) THEN 6
                                                                                         ELSE 7
                                                                                    END
                                                                          END
                                                                END
                                                      END
                                            END AS GroupSortOrder
                                           ,RChild.ResourceTypeID
                                           ,RChild.ChildTypeId
                                  FROM      syResources RChild
                                  INNER JOIN syNavigationNodes NNChild ON RChild.ResourceID = NNChild.ResourceId
                                  INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                                  INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                                  LEFT OUTER JOIN (
                                                    SELECT  *
                                                    FROM    syResources
                                                    WHERE   ResourceTypeID = 1
                                                  ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                                  WHERE     RChild.ResourceTypeID IN ( 2,4,8 )
                                            AND (
                                                  RChild.ChildTypeId IS NULL
                                                  OR RChild.ChildTypeId = 4
                                                )
                                            AND ( RChild.ResourceID <> 394 )
                                            AND ( NNChild.ResourceId <> 769 )
                                            AND ( NNParent.ResourceId NOT IN ( 394 ) )
                                            AND (
                                                  NNParent.ParentId IN ( SELECT HierarchyId
                                                                         FROM   syNavigationNodes
                                                                         WHERE  ResourceId = 26 )
                                                  OR NNChild.ParentId IN ( SELECT   HierarchyId
                                                                           FROM     syNavigationNodes
                                                                           WHERE    ResourceId IN ( 703,468,469,621,471,470 ) )
                                                )
												 --DE8343
                                            AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
                                ) t1
                      UNION
                      SELECT    194 AS ModuleResourceId
                               ,ResourceID AS ChildResourceId
                               ,Resource AS ChildResource
                               ,(
                                  SELECT    AccessLevel
                                  FROM      syRlsResLvls
                                  WHERE     RoleId = @RoleId
                                            AND ResourceID = syResources.ResourceID
                                ) AS AccessLevel
                               ,ResourceURL AS ChildResourceURL
                               ,NULL AS ParentResourceId
                               ,NULL AS ParentResource
                               ,1 AS GroupSortOrder
                               ,NULL AS FirstSortOrder
                               ,NULL AS DisplaySequence
                               ,ResourceTypeID
                               ,4 AS ChildTypeId
                      FROM      syResources
                      WHERE     ResourceID = 194
                      UNION
                      SELECT    194 AS ModuleResourceId
                               ,ChildResourceId
                               ,ChildResource
                               ,(
                                  SELECT    AccessLevel
                                  FROM      syRlsResLvls
                                  WHERE     RoleId = @RoleId
                                            AND ResourceID = t3.ChildResourceId
                                ) AS AccessLevel
                               ,ChildResourceURL
                               ,ParentResourceId
                               ,ParentResource
                               ,GroupSortOrder
                               ,(
                                  SELECT TOP 1
                                            HierarchyIndex
                                  FROM      dbo.syNavigationNodes
                                  WHERE     ResourceId = ChildResourceId
                                ) AS FirstSortOrder
                               ,CASE WHEN (
                                            ChildResourceId IN ( 705 )
                                            OR ParentResourceId IN ( 705 )
                                          ) THEN 1
                                     ELSE 2
                                END AS DisplaySequence
                               ,ResourceTypeID
                               ,ChildTypeId
                      FROM      (
                                  SELECT DISTINCT
                                            NNChild.ResourceId AS ChildResourceId
                                           ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                                 ELSE RChild.Resource
                                            END AS ChildResource
                                           ,RChild.ResourceURL AS ChildResourceURL
                                           ,CASE WHEN NNParent.ResourceId IN ( 194 )
                                                      OR NNChild.ResourceId IN ( 705 ) THEN NULL
                                                 ELSE NNParent.ResourceId
                                            END AS ParentResourceId
                                           ,RParent.Resource AS ParentResource
                                           ,RParentModule.Resource AS MODULE
                                           ,CASE WHEN (
                                                        NNChild.ResourceId = 705
                                                        OR NNParent.ResourceId = 705
                                                      ) THEN 2
                                                 ELSE 3
                                            END AS GroupSortOrder
                                           ,RChild.ResourceTypeID
                                           ,RChild.ChildTypeId
                                  FROM      syResources RChild
                                  INNER JOIN syNavigationNodes NNChild ON RChild.ResourceID = NNChild.ResourceId
                                  INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                                  INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                                  LEFT OUTER JOIN (
                                                    SELECT  *
                                                    FROM    syResources
                                                    WHERE   ResourceTypeID = 1
                                                  ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                                  WHERE     RChild.ResourceTypeID IN ( 2,4,8 )
                                            AND (
                                                  RChild.ChildTypeId IS NULL
                                                  OR RChild.ChildTypeId = 4
                                                )
                                            AND (
                                                  NNParent.ParentId IN ( SELECT HierarchyId
                                                                         FROM   syNavigationNodes
                                                                         WHERE  ResourceId = 194 )
                                                  OR NNChild.ParentId IN ( SELECT   HierarchyId
                                                                           FROM     syNavigationNodes
                                                                           WHERE    ResourceId IN ( 705 ) )
                                                )
                                            AND ( RChild.ResourceID <> 394 )
                                            AND ( NNParent.ResourceId NOT IN ( 394,277 ) )
					-- The following condition uses Bitwise Operator
									--DE8343
                                            AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
                                ) t3
                      UNION
                      SELECT    300 AS ModuleResourceId
                               ,ResourceID AS ChildResourceId
                               ,Resource AS ChildResource
                               ,(
                                  SELECT    AccessLevel
                                  FROM      syRlsResLvls
                                  WHERE     RoleId = @RoleId
                                            AND ResourceID = syResources.ResourceID
                                ) AS AccessLevel
                               ,ResourceURL AS ChildResourceURL
                               ,NULL AS ParentResourceId
                               ,NULL AS ParentResource
                               ,1 AS GroupSortOrder
                               ,NULL AS FirstSortOrder
                               ,NULL AS DisplaySequence
                               ,ResourceTypeID
                               ,4 AS ChildTypeId
                      FROM      syResources
                      WHERE     ResourceID = 300
                      UNION
                      SELECT    300 AS ModuleResourceId
                               ,ChildResourceId
                               ,ChildResource
                               ,(
                                  SELECT    AccessLevel
                                  FROM      syRlsResLvls
                                  WHERE     RoleId = @RoleId
                                            AND ResourceID = t3.ChildResourceId
                                ) AS AccessLevel
                               ,ChildResourceURL
                               ,ParentResourceId
                               ,ParentResource
                               ,GroupSortOrder
                               ,(
                                  SELECT TOP 1
                                            HierarchyIndex
                                  FROM      dbo.syNavigationNodes
                                  WHERE     ResourceId = ChildResourceId
                                ) AS FirstSortOrder
                               ,1 AS DisplaySequence
                               ,ResourceTypeID
                               ,ChildTypeId
                      FROM      (
                                  SELECT DISTINCT
                                            NNChild.ResourceId AS ChildResourceId
                                           ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                                 ELSE RChild.Resource
                                            END AS ChildResource
                                           ,RChild.ResourceURL AS ChildResourceURL
                                           ,CASE WHEN NNParent.ResourceId = 300
                                                      OR NNChild.ResourceId = 697 THEN NULL
                                                 ELSE NNParent.ResourceId
                                            END AS ParentResourceId
                                           ,RParent.Resource AS ParentResource
                                           ,RParentModule.Resource AS MODULE
                                           ,CASE WHEN (
                                                        NNChild.ResourceId = 705
                                                        OR NNParent.ResourceId = 705
                                                      ) THEN 2
                                                 ELSE 3
                                            END AS GroupSortOrder
                                           ,RChild.ResourceTypeID
                                           ,RChild.ChildTypeId
                                  FROM      syResources RChild
                                  INNER JOIN syNavigationNodes NNChild ON RChild.ResourceID = NNChild.ResourceId
                                  INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                                  INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                                  LEFT OUTER JOIN (
                                                    SELECT  *
                                                    FROM    syResources
                                                    WHERE   ResourceTypeID = 1
                                                  ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                                  WHERE     RChild.ResourceTypeID IN ( 2,4,8 )
                                            AND (
                                                  RChild.ChildTypeId IS NULL
                                                  OR RChild.ChildTypeId = 4
                                                )
                                            AND ( RChild.ResourceID <> 394 )
                                            AND ( NNParent.ResourceId NOT IN ( 394,687 ) )
                                            AND ( NNParent.ParentId IN ( SELECT HierarchyId
                                                                         FROM   syNavigationNodes
                                                                         WHERE  ResourceId = 300 ) )
										--DE8343
                                            AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
                                ) t3
                      UNION
                      SELECT    191 AS ModuleResourceId
                               ,ResourceID AS ChildResourceId
                               ,Resource AS ChildResource
                               ,(
                                  SELECT    AccessLevel
                                  FROM      syRlsResLvls
                                  WHERE     RoleId = @RoleId
                                            AND ResourceID = syResources.ResourceID
                                ) AS AccessLevel
                               ,ResourceURL AS ChildResourceURL
                               ,NULL AS ParentResourceId
                               ,NULL AS ParentResource
                               ,1 AS GroupSortOrder
                               ,NULL AS FirstSortOrder
                               ,NULL AS DisplaySequence
                               ,ResourceTypeID
                               ,4 AS ChildTypeId
                      FROM      syResources
                      WHERE     ResourceID = 191
                      UNION
                      SELECT    191 AS ModuleResourceId
                               ,ChildResourceId
                               ,ChildResource
                               ,(
                                  SELECT    AccessLevel
                                  FROM      syRlsResLvls
                                  WHERE     RoleId = @RoleId
                                            AND ResourceID = t3.ChildResourceId
                                ) AS AccessLevel
                               ,ChildResourceURL
                               ,ParentResourceId
                               ,ParentResource
                               ,GroupSortOrder
                               ,(
                                  SELECT TOP 1
                                            HierarchyIndex
                                  FROM      dbo.syNavigationNodes
                                  WHERE     ResourceId = ChildResourceId
                                ) AS FirstSortOrder
                               ,1 AS DisplaySequence
                               ,ResourceTypeID
                               ,ChildTypeId
                      FROM      (
                                  SELECT DISTINCT
                                            NNChild.ResourceId AS ChildResourceId
                                           ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                                 ELSE RChild.Resource
                                            END AS ChildResource
                                           ,RChild.ResourceURL AS ChildResourceURL
                                           ,CASE WHEN (
                                                        NNParent.ResourceId = 191
                                                        OR NNChild.ResourceId = 706
                                                      ) THEN NULL
                                                 ELSE NNParent.ResourceId
                                            END AS ParentResourceId
                                           ,RParent.Resource AS ParentResource
                                           ,RParentModule.Resource AS MODULE
                                           ,CASE WHEN (
                                                        NNChild.ResourceId = 706
                                                        OR NNParent.ResourceId = 706
                                                      ) THEN 2
                                                 ELSE 3
                                            END AS GroupSortOrder
                                           ,RChild.ResourceTypeID
                                           ,RChild.ChildTypeId
                                  FROM      syResources RChild
                                  INNER JOIN syNavigationNodes NNChild ON RChild.ResourceID = NNChild.ResourceId
                                  INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                                  INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                                  LEFT OUTER JOIN (
                                                    SELECT  *
                                                    FROM    syResources
                                                    WHERE   ResourceTypeID = 1
                                                  ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                                  WHERE     RChild.ResourceTypeID IN ( 2,4,8 )
                                            AND (
                                                  RChild.ChildTypeId IS NULL
                                                  OR RChild.ChildTypeId = 4
                                                )
                                            AND ( RChild.ResourceID <> 394 )
                                            AND ( NNParent.ResourceId NOT IN ( 394 ) )
                                            AND (
                                                  NNParent.ParentId IN ( SELECT HierarchyId
                                                                         FROM   syNavigationNodes
                                                                         WHERE  ResourceId = 191 )
                                                  OR NNChild.ParentId IN ( SELECT   HierarchyId
                                                                           FROM     syNavigationNodes
                                                                           WHERE    ResourceId IN ( 706 ) )
                                                )
											--DE8343
                                            AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
                                ) t3
                      UNION
                      SELECT    193 AS ModuleResourceId
                               ,ResourceID AS ChildResourceId
                               ,Resource AS ChildResource
                               ,(
                                  SELECT    AccessLevel
                                  FROM      syRlsResLvls
                                  WHERE     RoleId = @RoleId
                                            AND ResourceID = syResources.ResourceID
                                ) AS AccessLevel
                               ,ResourceURL AS ChildResourceURL
                               ,NULL AS ParentResourceId
                               ,NULL AS ParentResource
                               ,1 AS GroupSortOrder
                               ,NULL AS FirstSortOrder
                               ,NULL AS DisplaySequence
                               ,ResourceTypeID
                               ,4 AS ChildTypeId
                      FROM      syResources
                      WHERE     ResourceID = 193
                      UNION
                      SELECT    193 AS ModuleResourceId
                               ,ChildResourceId
                               ,ChildResource
                               ,(
                                  SELECT    AccessLevel
                                  FROM      syRlsResLvls
                                  WHERE     RoleId = @RoleId
                                            AND ResourceID = t3.ChildResourceId
                                ) AS AccessLevel
                               ,ChildResourceURL
                               ,ParentResourceId
                               ,ParentResource
                               ,GroupSortOrder
                               ,(
                                  SELECT TOP 1
                                            HierarchyIndex
                                  FROM      dbo.syNavigationNodes
                                  WHERE     ResourceId = ChildResourceId
                                ) AS FirstSortOrder
                               ,CASE WHEN (
                                            ChildResourceId IN ( 704 )
                                            OR ParentResourceId IN ( 704 )
                                          ) THEN 1
                                     ELSE 2
                                END AS DisplaySequence
                               ,ResourceTypeID
                               ,ChildTypeId
                      FROM      (
                                  SELECT DISTINCT
                                            NNChild.ResourceId AS ChildResourceId
                                           ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                                 ELSE RChild.Resource
                                            END AS ChildResource
                                           ,RChild.ResourceURL AS ChildResourceURL
                                           ,CASE WHEN (
                                                        NNParent.ResourceId = 193
                                                        OR NNChild.ResourceId IN ( 704,405,406 )
                                                      ) THEN NULL
                                                 ELSE NNParent.ResourceId
                                            END AS ParentResourceId
                                           ,RParent.Resource AS ParentResource
                                           ,RParentModule.Resource AS MODULE
                                           ,CASE WHEN (
                                                        NNChild.ResourceId = 704
                                                        OR NNParent.ResourceId = 704
                                                      ) THEN 2
                                                 WHEN (
                                                        NNChild.ResourceId = 405
                                                        OR NNParent.ResourceId = 405
                                                      ) THEN 3
                                                 ELSE 4
                                            END AS GroupSortOrder
                                           ,RChild.ResourceTypeID
                                           ,RChild.ChildTypeId
                                  FROM      syResources RChild
                                  INNER JOIN syNavigationNodes NNChild ON RChild.ResourceID = NNChild.ResourceId
                                  INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                                  INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                                  LEFT OUTER JOIN (
                                                    SELECT  *
                                                    FROM    syResources
                                                    WHERE   ResourceTypeID = 1
                                                  ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                                  WHERE     RChild.ResourceTypeID IN ( 2,4,8 )
                                            AND (
                                                  RChild.ChildTypeId IS NULL
                                                  OR RChild.ChildTypeId = 4
                                                )
                                            AND ( RChild.ResourceID <> 394 )
                                            AND ( NNParent.ResourceId NOT IN ( 394 ) )
                                            AND (
                                                  NNParent.ParentId IN ( SELECT HierarchyId
                                                                         FROM   syNavigationNodes
                                                                         WHERE  ResourceId = 193 )
                                                  OR NNChild.ParentId IN ( SELECT   HierarchyId
                                                                           FROM     syNavigationNodes
                                                                           WHERE    ResourceId IN ( 694,405,406,704 ) )
                                                )
											--DE8343
                                            AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
                                ) t3
                      UNION
                      SELECT    195 AS ModuleResourceId
                               ,ResourceID AS ChildResourceId
                               ,Resource AS ChildResource
                               ,(
                                  SELECT    AccessLevel
                                  FROM      syRlsResLvls
                                  WHERE     RoleId = @RoleId
                                            AND ResourceID = syResources.ResourceID
                                ) AS AccessLevel
                               ,ResourceURL AS ChildResourceURL
                               ,NULL AS ParentResourceId
                               ,NULL AS ParentResource
                               ,1 AS GroupSortOrder
                               ,NULL AS FirstSortOrder
                               ,NULL AS DisplaySequence
                               ,ResourceTypeID
                               ,4 AS ChildTypeId
                      FROM      syResources
                      WHERE     ResourceID = 195
                      UNION
                      SELECT    195 AS ModuleResourceId
                               ,ChildResourceId
                               ,ChildResource
                               ,(
                                  SELECT    AccessLevel
                                  FROM      syRlsResLvls
                                  WHERE     RoleId = @RoleId
                                            AND ResourceID = t3.ChildResourceId
                                ) AS AccessLevel
                               ,ChildResourceURL
                               ,ParentResourceId
                               ,ParentResource
                               ,GroupSortOrder
                               ,(
                                  SELECT TOP 1
                                            HierarchyIndex
                                  FROM      dbo.syNavigationNodes
                                  WHERE     ResourceId = ChildResourceId
                                ) AS FirstSortOrder
                               ,CASE WHEN (
                                            ChildResourceId IN ( 707 )
                                            OR ParentResourceId IN ( 707 )
                                          ) THEN 1
                                     ELSE 2
                                END AS DisplaySequence
                               ,ResourceTypeID
                               ,ChildTypeId
                      FROM      (
                                  SELECT DISTINCT
                                            NNChild.ResourceId AS ChildResourceId
                                           ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                                 ELSE RChild.Resource
                                            END AS ChildResource
                                           ,RChild.ResourceURL AS ChildResourceURL
                                           ,CASE WHEN (
                                                        NNParent.ResourceId = 195
                                                        OR NNChild.ResourceId IN ( 707,463,429,408 )
                                                      ) THEN NULL
                                                 ELSE NNParent.ResourceId
                                            END AS ParentResourceId
                                           ,RParent.Resource AS ParentResource
                                           ,RParentModule.Resource AS MODULE
                                           ,CASE WHEN (
                                                        NNChild.ResourceId = 707
                                                        OR NNParent.ResourceId = 707
                                                      ) THEN 2
                                                 WHEN (
                                                        NNChild.ResourceId = 463
                                                        OR NNParent.ResourceId = 463
                                                      ) THEN 3
                                                 WHEN (
                                                        NNChild.ResourceId = 408
                                                        OR NNParent.ResourceId = 408
                                                      ) THEN 4
                                                 WHEN (
                                                        NNChild.ResourceId = 429
                                                        OR NNParent.ResourceId = 429
                                                      ) THEN 5
                                                 ELSE 6
                                            END AS GroupSortOrder
                                           ,RChild.ResourceTypeID
                                           ,RChild.ChildTypeId
                                  FROM      syResources RChild
                                  INNER JOIN syNavigationNodes NNChild ON RChild.ResourceID = NNChild.ResourceId
                                  INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                                  INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                                  LEFT OUTER JOIN (
                                                    SELECT  *
                                                    FROM    syResources
                                                    WHERE   ResourceTypeID = 1
                                                  ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                                  WHERE     RChild.ResourceTypeID IN ( 2,4,8 )
                                            AND (
                                                  RChild.ChildTypeId IS NULL
                                                  OR RChild.ChildTypeId = 4
                                                )
                                            AND ( RChild.ResourceID <> 394 )
                                            AND ( NNParent.ResourceId NOT IN ( 394 ) )
                                            AND (
                                                  NNParent.ParentId IN ( SELECT HierarchyId
                                                                         FROM   syNavigationNodes
                                                                         WHERE  ResourceId = 195 )
                                                  OR NNChild.ParentId IN ( SELECT   HierarchyId
                                                                           FROM     syNavigationNodes
                                                                           WHERE    ResourceId IN ( 707,463,429,408 ) )
                                                )
										--DE8343
                                            AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
                                ) t3
                      UNION
                      SELECT    192 AS ModuleResourceId
                               ,ResourceID AS ChildResourceId
                               ,Resource AS ChildResource
                               ,(
                                  SELECT    AccessLevel
                                  FROM      syRlsResLvls
                                  WHERE     RoleId = @RoleId
                                            AND ResourceID = syResources.ResourceID
                                ) AS AccessLevel
                               ,ResourceURL AS ChildResourceURL
                               ,NULL AS ParentResourceId
                               ,NULL AS ParentResource
                               ,1 AS GroupSortOrder
                               ,NULL AS FirstSortOrder
                               ,NULL AS DisplaySequence
                               ,ResourceTypeID
                               ,4 AS ChildTypeId
                      FROM      syResources
                      WHERE     ResourceID = 192
                      UNION
                      SELECT    192 AS ModuleResourceId
                               ,ChildResourceId
                               ,ChildResource
                               ,(
                                  SELECT    AccessLevel
                                  FROM      syRlsResLvls
                                  WHERE     RoleId = @RoleId
                                            AND ResourceID = t3.ChildResourceId
                                ) AS AccessLevel
                               ,ChildResourceURL
                               ,ParentResourceId
                               ,ParentResource
                               ,GroupSortOrder
                               ,(
                                  SELECT TOP 1
                                            HierarchyIndex
                                  FROM      dbo.syNavigationNodes
                                  WHERE     ResourceId = ChildResourceId
                                ) AS FirstSortOrder
                               ,CASE WHEN (
                                            ChildResourceId IN ( 709 )
                                            OR ParentResourceId IN ( 709 )
                                          ) THEN 1
                                     ELSE 2
                                END AS DisplaySequence
                               ,ResourceTypeID
                               ,ChildTypeId
                      FROM      (
                                  SELECT DISTINCT
                                            NNChild.ResourceId AS ChildResourceId
                                           ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                                 ELSE RChild.Resource
                                            END AS ChildResource
                                           ,RChild.ResourceURL AS ChildResourceURL
                                           ,CASE WHEN (
                                                        NNParent.ResourceId = 192
                                                        OR NNChild.ResourceId IN ( 688,709 )
                                                      ) THEN NULL
                                                 ELSE NNParent.ResourceId
                                            END AS ParentResourceId
                                           ,RParent.Resource AS ParentResource
                                           ,RParentModule.Resource AS MODULE
                                           ,CASE WHEN (
                                                        NNChild.ResourceId = 709
                                                        OR NNParent.ResourceId = 709
                                                      ) THEN 2
                                                 ELSE 3
                                            END AS GroupSortOrder
                                           ,RChild.ResourceTypeID
                                           ,RChild.ChildTypeId
                                  FROM      syResources RChild
                                  INNER JOIN syNavigationNodes NNChild ON RChild.ResourceID = NNChild.ResourceId
                                  INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                                  INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                                  LEFT OUTER JOIN (
                                                    SELECT  *
                                                    FROM    syResources
                                                    WHERE   ResourceTypeID = 1
                                                  ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                                  WHERE     RChild.ResourceTypeID IN ( 2,4,8 )
                                            AND (
                                                  RChild.ChildTypeId IS NULL
                                                  OR RChild.ChildTypeId = 4
                                                )
                                            AND ( RChild.ResourceID <> 394 )
                                            AND ( NNParent.ResourceId NOT IN ( 394 ) )
                                            AND (
                                                  NNParent.ParentId IN ( SELECT HierarchyId
                                                                         FROM   syNavigationNodes
                                                                         WHERE  ResourceId = 192 )
                                                  OR NNChild.ParentId IN ( SELECT   HierarchyId
                                                                           FROM     syNavigationNodes
                                                                           WHERE    ResourceId IN ( 709 ) )
                                                )
										--DE8343
                                            AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
                                ) t3
                    ) t2
            WHERE   t2.ModuleResourceId = @ModuleResourceId
                    AND (
                          t2.ResourceTypeID = 4
                          OR (
                               t2.ResourceTypeID = 2
                               AND t2.ChildTypeId = 4
                             )
                        )
                    AND
		   -- Hide resources based on Configuration Settings      
                    (
                      -- The following expression means if showross... is set to false, hide       
	 -- ResourceIds 541,542,532,534,535,538,543,539      
	 -- (Not False) OR (Condition)      
                      (
                        ( @ShowRossOnlyTabs <> 0 )
                        OR ( ChildResourceId NOT IN ( 541,542,532,538,543,539 ) )
                      )
                      AND      
	 -- The following expression means if showross... is set to true, hide       
	 -- ResourceIds 142,375,330,476,508,102,107,237      
	 -- (Not True) OR (Condition)      
                      (
                        ( @ShowRossOnlyTabs <> 1 )
                        OR ( ChildResourceId NOT IN ( 142,375,330,476,508,102,107,237 ) )
                      )
                      AND      
	 ---- The following expression means if SchedulingMethod=regulartraditional, hide       
	 ---- ResourceIds 91 and 497      
	 ---- (Not True) OR (Condition)      
                      (
                        (
                          NOT LOWER(LTRIM(RTRIM(@SchedulingMethod))) = 'regulartraditional'
                        )
                        OR ( ChildResourceId NOT IN ( 91,497 ) )
                      )
                      AND      
	 -- The following expression means if TrackSAPAttendance=byday, hide       
	 -- ResourceIds 633      
	 -- (Not True) OR (Condition)      
				 -- ( ( NOT LOWER(LTRIM(RTRIM(@TrackSAPAttendance))) = 'byday'
					--)
					--OR ( ChildResourceId NOT IN ( 633 ) )
				 -- )
				 -- AND 				   
	 ---- The following expression means if @ShowCollegeOfCourtReporting is set to false, hide       
	 ---- ResourceIds 614,615      
	 ---- (Not False) OR (Condition)      
                      (
                        (
                          NOT LOWER(LTRIM(RTRIM(@ShowCollegeOfCourtReporting))) = 'no'
                        )
                        OR ( ChildResourceId NOT IN ( 614,615 ) )
                      )
                      AND      
	 -- The following expression means if @ShowCollegeOfCourtReporting is set to true, hide       
	 -- ResourceIds 497      
	 -- (Not True) OR (Condition)      
                      (
                        (
                          NOT LOWER(LTRIM(RTRIM(@ShowCollegeOfCourtReporting))) = 'yes'
                        )
                        OR ( ChildResourceId NOT IN ( 497 ) )
                      )
                      AND      
	 -- The following expression means if FAMEESP is set to false, hide       
	 -- ResourceIds 517,523, 525      
	 -- (Not False) OR (Condition)      
                      (
                        (
                          NOT LOWER(LTRIM(RTRIM(@FameESP))) = 'no'
                        )
                        OR ( ChildResourceId NOT IN ( 517,523,525 ) )
                      )
                      AND      
	 ---- The following expression means if EDExpress is set to false, hide       
	 ---- ResourceIds 603,604,606,619      
	 ---- (Not False) OR (Condition)      
                      (
                        (
                          NOT LOWER(LTRIM(RTRIM(@EdExpress))) = 'no'
                        )
                        OR ( ChildResourceId NOT IN ( 603,604,605,606,619 ) )
                      )
                      AND    
	 ---- The following expression means if TimeClockClassSection is set to false, hide       
	 ---- ResourceIds 7      
	 ---- (Not False) OR (Condition)      
                      (
                        (
                          NOT LOWER(LTRIM(RTRIM(@TimeClockClassSection))) = 'no'
                        )
                        OR ( ChildResourceId NOT IN ( 7 ) )
                      )
                      AND        
	 ---- The following expression means if @GradeBookWeightingLevel is set to courselevel, hide       
	 ---- ResourceIds 107,96,222      
	 ---- (Not False) OR (Condition)      
                      (
                        (
                          NOT LOWER(LTRIM(RTRIM(@GradeBookWeightingLevel))) = 'courselevel'
                        )
                        OR ( ChildResourceId NOT IN ( 107,96,222 ) )
                      )
                      AND (
                            (
                              NOT LOWER(LTRIM(RTRIM(@GradeBookWeightingLevel))) = 'instructorlevel'
                            )
                            OR ( ChildResourceId NOT IN ( 476 ) )
                          )
                      AND (
                            (
                              NOT LOWER(LTRIM(RTRIM(@ShowExternshipTabs))) = 'no'
                            )
                            OR ( ChildResourceId NOT IN ( 543,538 ) )
                          )
                      AND    
				 --If Time Clock Exception is set to no and user is not support    
				 -- then hide setup punch codes    
				 -- IF A THEN B ==> (NOT A) or (B)    
				 -- A Part: If ConfigSetting TimeClockClassSection is set to NO (AND) User is not a Power User (support)    
				 -- B Part: Hide Resource Setup Punch Codes (769)    
                      (
                        (
                          NOT @TimeClockClassSection = 'no'
				  
					  --AND NOT @IsPowerUser = 0
                        )
                        OR ( ChildResourceId NOT IN ( 769 ) )
                      )
                      AND ( ChildResourceId NOT IN ( 616 ) )
                      AND ( ChildResourceId NOT IN ( 326 ) )
				  
				  --AND ( ( NOT @IsSupportORSuper = 0
				  --      )
				  --      OR ( ChildResourceId NOT IN ( 616 ) )
				  --    )
				  --AND    
	 ---- If user is not a power user (sa or support or super), hide Setup Student page Navigation (326)  
	 ---- (Not False) OR (Condition)      
				  --( ( NOT @IsUserAPowerUser = 0
				  --  )
				  --  OR ( ChildResourceId NOT IN ( 326 ) )
				  --)
                    )
            ORDER BY GroupSortOrder
                   ,ParentResourceId
                   ,ChildResource;
        END;
    IF @ResourceTypeId = 5
        AND @tabid = 0
        BEGIN	
            SELECT DISTINCT
                    *
                   ,CASE WHEN AccessLevel = 15 THEN 'Full Access'
                         ELSE CASE WHEN AccessLevel = 12 THEN 'Add and Edit Only'
                                   ELSE CASE WHEN AccessLevel = 10 THEN 'Edit and Delete Only'
                                             ELSE CASE WHEN AccessLevel = 6 THEN 'Add and Delete Only'
                                                       ELSE CASE WHEN AccessLevel = 4 THEN 'Add Only'
                                                                 ELSE CASE WHEN AccessLevel = 8 THEN 'Edit Only'
                                                                           ELSE CASE WHEN AccessLevel = 1 THEN 'Display Only'
                                                                                     ELSE NULL
                                                                                END
                                                                      END
                                                            END
                                                  END
                                        END
                              END
                    END AS PermissionText
                   ,NULL AS TabId
            FROM    (
                      SELECT    189 AS ModuleResourceId
                               ,ResourceID AS ChildResourceId
                               ,Resource AS ChildResource
                               ,(
                                  SELECT    AccessLevel
                                  FROM      syRlsResLvls
                                  WHERE     RoleId = @RoleId
                                            AND ResourceID = syResources.ResourceID
                                ) AS AccessLevel
                               ,ResourceURL AS ChildResourceURL
                               ,NULL AS ParentResourceId
                               ,NULL AS ParentResource
                               ,1 AS GroupSortOrder
                               ,NULL AS FirstSortOrder
                               ,NULL AS DisplaySequence
                               ,ResourceTypeID
                               ,5 AS ChildTypeId
                      FROM      syResources
                      WHERE     ResourceID = 189
                      UNION
                      SELECT    189 AS ModuleResourceId
                               ,NNChild.ResourceId AS ChildResourceId
                               ,CASE WHEN NNChild.ResourceId = 395 THEN 'Lead Tabs'
                                     ELSE CASE WHEN NNChild.ResourceId = 398 THEN 'Add/View Leads'
                                               ELSE RChild.Resource
                                          END
                                END AS ChildResource
                               ,(
                                  SELECT    AccessLevel
                                  FROM      syRlsResLvls
                                  WHERE     RoleId = @RoleId
                                            AND ResourceID = NNChild.ResourceId
                                ) AS AccessLevel
                               ,RChild.ResourceURL AS ChildResourceURL
                               ,CASE WHEN (
                                            NNParent.ResourceId IN ( 689 )
                                            OR NNChild.ResourceId IN ( 710,402,617,734,794 )
                                          ) THEN NULL
                                     ELSE NNParent.ResourceId
                                END AS ParentResourceId
                               ,RParent.Resource AS ParentResource
                               ,CASE WHEN (
                                            NNChild.ResourceId IN ( 710 )
                                            OR NNParent.ResourceId IN ( 710 )
                                          ) THEN 2
                                     ELSE CASE WHEN (
                                                      NNChild.ResourceId IN ( 617 )
                                                      OR NNParent.ResourceId IN ( 617 )
                                                    ) THEN 3
                                               ELSE CASE WHEN (
                                                                NNChild.ResourceId IN ( 402 )
                                                                OR NNParent.ResourceId IN ( 402 )
                                                              ) THEN 4
                                                         ELSE CASE WHEN (
                                                                          NNChild.ResourceId IN ( 794 )
                                                                          OR NNParent.ResourceId IN ( 794 )
                                                                        ) THEN 5
                                                                   ELSE 6
                                                              END
                                                    END
                                          END
                                END AS GroupSortOrder
                               ,CASE WHEN NNChild.ResourceId = 398 THEN 1
                                     ELSE CASE WHEN NNChild.ResourceId = 395 THEN 2
                                               ELSE 3
                                          END
                                END AS FirstSortOrder
                               ,CASE WHEN (
                                            NNChild.ResourceId IN ( 710,734 )
                                            OR NNParent.ResourceId IN ( 710,734 )
                                          ) THEN 1
                                     ELSE 2
                                END AS DisplaySequence
                               ,RChild.ResourceTypeID
                               ,RChild.ChildTypeId
                      FROM      syResources RChild
                      INNER JOIN syNavigationNodes NNChild ON RChild.ResourceID = NNChild.ResourceId
                      INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                      INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                      LEFT OUTER JOIN (
                                        SELECT  *
                                        FROM    syResources
                                        WHERE   ResourceTypeID = 1
                                      ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                      WHERE     RChild.ResourceTypeID IN ( 2,5,8 )
                                AND (
                                      RChild.ChildTypeId IS NULL
                                      OR RChild.ChildTypeId = 5
                                    )
                                AND ( RChild.ResourceID NOT IN ( 395 ) )
                                AND ( NNParent.ResourceId NOT IN ( 395 ) )
                                AND (
                                      NNParent.ParentId IN ( SELECT HierarchyId
                                                             FROM   syNavigationNodes
                                                             WHERE  ResourceId = 189 )
                                      OR NNChild.ParentId IN ( SELECT   HierarchyId
                                                               FROM     syNavigationNodes
                                                               WHERE    ResourceId IN ( 710,402,617,734,794 ) )
                                    )
									--DE8343
                                AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
                      UNION
                      SELECT    26 AS ModuleResourceId
                               ,ResourceID AS ChildResourceId
                               ,Resource AS ChildResource
                               ,(
                                  SELECT    AccessLevel
                                  FROM      syRlsResLvls
                                  WHERE     RoleId = @RoleId
                                            AND ResourceID = syResources.ResourceID
                                ) AS AccessLevel
                               ,ResourceURL AS ChildResourceURL
                               ,NULL AS ParentResourceId
                               ,NULL AS ParentResource
                               ,1 AS GroupSortOrder
                               ,NULL AS FirstSortOrder
                               ,NULL AS DisplaySequence
                               ,ResourceTypeID
                               ,5 AS ChildTypeId
                      FROM      syResources
                      WHERE     ResourceID = 26
                      UNION
                      SELECT    26 AS ModuleResourceId
                               ,ChildResourceId
                               ,ChildResource
                               ,(
                                  SELECT    AccessLevel
                                  FROM      syRlsResLvls
                                  WHERE     RoleId = @RoleId
                                            AND ResourceID = t1.ChildResourceId
                                ) AS AccessLevel
                               ,ChildResourceURL
                               ,ParentResourceId
                               ,ParentResource
                               ,GroupSortOrder
                               ,(
                                  SELECT TOP 1
                                            HierarchyIndex
                                  FROM      dbo.syNavigationNodes
                                  WHERE     ResourceId = ChildResourceId
                                ) AS FirstSortOrder
                               ,
								--CASE WHEN ( ChildResourceId IN ( 691, 729, 730)
								--            OR ParentResourceId IN ( 690, 691,
								--                              729, 730)
								--          ) THEN 1
								--     ELSE CASE WHEN ( ChildResourceId IN (727,
								--                              692, 736 )
								--                      OR ParentResourceId IN (
								--                      727, 736, 692 )
								--                    ) THEN 2
								--               ELSE CASE WHEN ( ChildResourceId IN (
								--                              409 )
								--                              OR ParentResourceId = 409
								--                              ) THEN 3
								--                         ELSE CASE
								--                              WHEN ( ChildResourceId IN (
								--             719, 720, 721,
								--                              725 )
								--                              OR ParentResourceId IN (
								--                              719, 720, 721,
								--                       725 )
								--               ) THEN 4
								--                              ELSE CASE
								--                              WHEN ( ChildResourceId IN (
								--                              722, 723, 724 )
								--                              OR ParentResourceId IN (
								--                              722, 723, 724 )
								--                              ) THEN 5
								--                              ELSE 6
								--                              END
								--                              END
								--                    END
								--          END
								--END AS DisplaySequence ,
                                CASE WHEN (
                                            ChildResourceId IN ( 690,691,729,730 )
                                            OR ParentResourceId IN ( 690,691,729,730 )
                                          ) THEN 1
                                     ELSE CASE WHEN (
                                                      ChildResourceId IN ( 727,692,736,678 )
                                                      OR ParentResourceId IN ( 727,736,692,678 )
                                                    ) THEN 2
                                               ELSE 3
                                          END
                                END AS DisplaySequence
                               ,ResourceTypeID
                               ,ChildTypeId
                      FROM      (
                                  SELECT DISTINCT
                                            NNChild.ResourceId AS ChildResourceId
                                           ,CASE WHEN NNChild.ResourceId = 409 THEN 'IPEDS - General Reports'
                                                 ELSE RChild.Resource
                                            END AS ChildResource
                                           ,RChild.ResourceURL AS ChildResourceURL
                                           ,CASE WHEN (
                                                        NNParent.ResourceId IN ( 689 )
                                                        OR NNChild.ResourceId IN ( 409,472,474,712,719,720,721,722,723,724,725,736,789 )
                                                      ) THEN NULL
                                                 ELSE NNParent.ResourceId
                                            END AS ParentResourceId
                                           ,RParent.Resource AS ParentResource
                                           ,RParentModule.Resource AS MODULE
                                           ,CASE WHEN (
                                                        NNChild.ResourceId IN ( 691 )
                                                        OR NNParent.ResourceId IN ( 691 )
                                                      ) THEN 2
                                                 WHEN (
                                                        NNChild.ResourceId IN ( 690 )
                                                        OR NNParent.ResourceId IN ( 690 )
                                                      ) THEN 3
                                                 WHEN (
                                                        NNChild.ResourceId IN ( 729 )
                                                        OR NNParent.ResourceId IN ( 729 )
                                                      ) THEN 4
                                                 WHEN (
                                                        NNChild.ResourceId IN ( 730 )
                                                        OR NNParent.ResourceId IN ( 730 )
                                                      ) THEN 5
                                                 WHEN (
                                                        NNChild.ResourceId IN ( 736 )
                                                        OR NNParent.ResourceId IN ( 736 )
                                                      ) THEN 6
                                                 WHEN (
                                                        NNChild.ResourceId IN ( 678 )
                                                        OR NNParent.ResourceId IN ( 678 )
                                                      ) THEN 7
                                                 WHEN (
                                                        NNChild.ResourceId IN ( 692 )
                                                        OR NNParent.ResourceId IN ( 692 )
                                                      ) THEN 8
                                                 WHEN (
                                                        NNChild.ResourceId IN ( 727 )
                                                        OR NNParent.ResourceId IN ( 727 )
                                                      ) THEN 9
                                                 ELSE 10
                                            END AS GroupSortOrder
                                           ,RChild.ResourceTypeID
                                           ,RChild.ChildTypeId
                                  FROM      syResources RChild
                                  INNER JOIN syNavigationNodes NNChild ON RChild.ResourceID = NNChild.ResourceId
                                  INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                                  INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                                  LEFT OUTER JOIN (
                                                    SELECT  *
                                                    FROM    syResources
                                                    WHERE   ResourceTypeID = 1
                                                  ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                                  WHERE     RChild.ResourceTypeID IN ( 2,5,8 )
                                            AND (
                                                  RChild.ChildTypeId IS NULL
                                                  OR RChild.ChildTypeId = 5
                                                )
				--AND (NNParent.ResourceId IN (689,409,711,472,474,712))
                                            AND ( RChild.ResourceID NOT IN ( 394,472,474,711,472,474,409,719,720,721,722,723,724,725,789 ) )
                                            AND (
                                                  NNParent.ParentId IN ( SELECT HierarchyId
                                                                         FROM   syNavigationNodes
                                                                         WHERE  ResourceId = 26 )
                                                  OR NNChild.ParentId IN (
                                                  SELECT    HierarchyId
                                                  FROM      syNavigationNodes
                                                  WHERE     --ResourceId IN (472,474,409,719,720,721,722,723,724,725,690,691,692,727,729,730,736)
                                                            ResourceId IN ( 678,691,690,692,727,729,736,730 ) )
                                                )
										--DE8343
                                            AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
                                ) t1
                      UNION
                      SELECT    194 AS ModuleResourceId
                               ,ResourceID AS ChildResourceId
                               ,Resource AS ChildResource
                               ,(
                                  SELECT    AccessLevel
                                  FROM      syRlsResLvls
                                  WHERE     RoleId = @RoleId
                                            AND ResourceID = syResources.ResourceID
                                ) AS AccessLevel
                               ,ResourceURL AS ChildResourceURL
                               ,NULL AS ParentResourceId
                               ,NULL AS ParentResource
                               ,1 AS GroupSortOrder
                               ,NULL AS FirstSortOrder
                               ,NULL AS DisplaySequence
                               ,ResourceTypeID
                               ,5 AS ChildTypeId
                      FROM      syResources
                      WHERE     ResourceID = 194
                      UNION
                      SELECT    194 AS ModuleResourceId
                               ,ChildResourceId
                               ,ChildResource
                               ,(
                                  SELECT    AccessLevel
                                  FROM      syRlsResLvls
                                  WHERE     RoleId = @RoleId
                                            AND ResourceID = t1.ChildResourceId
                                ) AS AccessLevel
                               ,ChildResourceURL
                               ,ParentResourceId
                               ,ParentResource
                               ,GroupSortOrder
                               ,GroupSortOrder AS FirstSortOrder
                               ,CASE WHEN (
                                            ChildResourceId IN ( 731,732 )
                                            OR ParentResourceId IN ( 731,732 )
                                          ) THEN 1
                                     ELSE 2
                                END AS DisplaySequence
                               ,ResourceTypeID
                               ,ChildTypeId
                      FROM      (
                                  SELECT DISTINCT
                                            NNChild.ResourceId AS ChildResourceId
                                           ,CASE WHEN NNChild.ResourceId = 409 THEN 'IPEDS - General Reports'
                                                 ELSE RChild.Resource
                                            END AS ChildResource
                                           ,RChild.ResourceURL AS ChildResourceURL
                                           ,CASE WHEN NNParent.ResourceId IN ( 194 )
                                                      OR NNChild.ResourceId IN ( 731,732,733 ) THEN NULL
                                                 ELSE NNParent.ResourceId
                                            END AS ParentResourceId
                                           ,RParent.Resource AS ParentResource
                                           ,RParentModule.Resource AS MODULE
                                           ,CASE WHEN (
                                                        NNChild.ResourceId IN ( 732 )
                                                        OR NNParent.ResourceId IN ( 732 )
                                                      ) THEN 2
                                                 ELSE CASE WHEN (
                                                                  NNChild.ResourceId IN ( 731 )
                                                                  OR NNParent.ResourceId IN ( 731 )
                                                                ) THEN 3
                                                           ELSE 4
                                                      END
                                            END AS GroupSortOrder
                                           ,RChild.ResourceTypeID
                                           ,RChild.ChildTypeId
                                  FROM      syResources RChild
                                  INNER JOIN syNavigationNodes NNChild ON RChild.ResourceID = NNChild.ResourceId
                                  INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                                  INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                                  LEFT OUTER JOIN (
                                                    SELECT  *
                                                    FROM    syResources
                                                    WHERE   ResourceTypeID = 1
                                                  ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                                  WHERE     RChild.ResourceTypeID IN ( 2,5,8 )
                                            AND (
                                                  RChild.ChildTypeId IS NULL
                                                  OR RChild.ChildTypeId = 5
                                                )
				--AND (NNParent.ResourceId IN (689,409,711,472,474,712))
                                            AND ( RChild.ResourceID NOT IN ( 394 ) )
                                            AND (
                                                  NNParent.ParentId IN ( SELECT HierarchyId
                                                                         FROM   syNavigationNodes
                                                                         WHERE  ResourceId = 194 )
                                                  OR NNChild.ParentId IN ( SELECT   HierarchyId
                                                                           FROM     syNavigationNodes
                                                                           WHERE    ResourceId IN ( 731,732,733 ) )
                                                )
										--DE8343
                                            AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
                                ) t1
                      UNION
                      SELECT    300 AS ModuleResourceId
                               ,ResourceID AS ChildResourceId
                               ,Resource AS ChildResource
                               ,(
                                  SELECT    AccessLevel
                                  FROM      syRlsResLvls
                                  WHERE     RoleId = @RoleId
                                            AND ResourceID = syResources.ResourceID
                                ) AS AccessLevel
                               ,ResourceURL AS ChildResourceURL
                               ,NULL AS ParentResourceId
                               ,NULL AS ParentResource
                               ,1 AS GroupSortOrder
                               ,NULL AS FirstSortOrder
                               ,NULL AS DisplaySequence
                               ,ResourceTypeID
                               ,5 AS ChildTypeId
                      FROM      syResources
                      WHERE     ResourceID = 300
                      UNION
                      SELECT    300 AS ModuleResourceId
                               ,ChildResourceId
                               ,ChildResource
                               ,(
                                  SELECT    AccessLevel
                                  FROM      syRlsResLvls
                                  WHERE     RoleId = @RoleId
                                            AND ResourceID = t1.ChildResourceId
                                ) AS AccessLevel
                               ,ChildResourceURL
                               ,ParentResourceId
                               ,ParentResource
                               ,GroupSortOrder
                               ,GroupSortOrder AS FirstSortOrder
                               ,1 AS DisplaySequence
                               ,ResourceTypeID
                               ,ChildTypeId
                      FROM      (
                                  SELECT DISTINCT
                                            NNChild.ResourceId AS ChildResourceId
                                           ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                                 ELSE RChild.Resource
                                            END AS ChildResource
                                           ,RChild.ResourceURL AS ChildResourceURL
                                           ,CASE WHEN NNParent.ResourceId = 300
                                                      OR NNChild.ResourceId = 715 THEN NULL
												  -- Had to hard code the parentresourceid to 715 for DE7532 on 7/25/2012 B. Shanblatt
                                                 ELSE 715
                                            END AS ParentResourceId
                                           ,
											--RParent.Resource AS ParentResource ,
											-- Had to hard code the parentresource to General Reports for DE7532 on 7/25/2012 B. Shanblatt
                                            'General Reports' AS ParentResource
                                           ,RParentModule.Resource AS MODULE
                                           ,CASE WHEN (
                                                        NNChild.ResourceId IN ( 715 )
                                                        OR NNParent.ResourceId IN ( 715,691 )
                                                      ) THEN 2
                                                 ELSE 3
                                            END AS GroupSortOrder
                                           ,RChild.ResourceTypeID
                                           ,RChild.ChildTypeId
                                  FROM      syResources RChild
                                  INNER JOIN syNavigationNodes NNChild ON RChild.ResourceID = NNChild.ResourceId
                                  INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                                  INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                                  LEFT OUTER JOIN (
                                                    SELECT  *
                                                    FROM    syResources
                                                    WHERE   ResourceTypeID = 1
                                                  ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                                  WHERE     RChild.ResourceTypeID IN ( 2,5,8 )
                                            AND (
                                                  RChild.ChildTypeId IS NULL
                                                  OR RChild.ChildTypeId = 5
                                                )
				--AND (NNParent.ResourceId IN (689,409,711,472,474,712))
											-- Added resource 325, 329, 366, 492, 554, 588, 633 for DE7532 on 7/25/2012 B. Shanblatt
                                            AND ( RChild.ResourceID NOT IN ( 394,325,329,366,492,554,633 ) )
                                            AND (
                                                  NNParent.ParentId IN ( SELECT HierarchyId
                                                                         FROM   syNavigationNodes
                                                                         WHERE  ResourceId = 300 )
                                                  OR NNChild.ParentId IN ( SELECT   HierarchyId
                                                                           FROM     syNavigationNodes
												    -- Added resource 691 for DE7532 on 7/25/2012 B. Shanblatt 
                                                                           WHERE    ResourceId IN ( 715,691 ) )
                                                )
										--DE8343
                                            AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
                                ) t1
                      UNION
                      SELECT    191 AS ModuleResourceId
                               ,ResourceID AS ChildResourceId
                               ,Resource AS ChildResource
                               ,(
                                  SELECT    AccessLevel
                                  FROM      syRlsResLvls
                                  WHERE     RoleId = @RoleId
                                            AND ResourceID = syResources.ResourceID
                                ) AS AccessLevel
                               ,ResourceURL AS ChildResourceURL
                               ,NULL AS ParentResourceId
                               ,NULL AS ParentResource
                               ,1 AS GroupSortOrder
                               ,NULL AS FirstSortOrder
                               ,NULL AS DisplaySequence
                               ,ResourceTypeID
                               ,5 AS ChildTypeId
                      FROM      syResources
                      WHERE     ResourceID = 191
                      UNION
                      SELECT    191 AS ModuleResourceId
                               ,ChildResourceId
                               ,ChildResource
                               ,(
                                  SELECT    AccessLevel
                                  FROM      syRlsResLvls
                                  WHERE     RoleId = @RoleId
                                            AND ResourceID = t1.ChildResourceId
                                ) AS AccessLevel
                               ,ChildResourceURL
                               ,ParentResourceId
                               ,ParentResource
                               ,GroupSortOrder
                               ,GroupSortOrder AS FirstSortOrder
                               ,CASE WHEN (
                                            ChildResourceId IN ( 716 )
                                            OR ParentResourceId IN ( 716 )
                                          ) THEN 1
                                     ELSE 2
                                END AS DisplaySequence
                               ,ResourceTypeID
                               ,ChildTypeId
                      FROM      (
                                  SELECT DISTINCT
                                            NNChild.ResourceId AS ChildResourceId
                                           ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                                 ELSE RChild.Resource
                                            END AS ChildResource
                                           ,RChild.ResourceURL AS ChildResourceURL
                                           ,CASE WHEN (
                                                        NNParent.ResourceId = 191
                                                        OR NNChild.ResourceId = 716
                                                      ) THEN NULL
                                                 ELSE NNParent.ResourceId
                                            END AS ParentResourceId
                                           ,RParent.Resource AS ParentResource
                                           ,RParentModule.Resource AS MODULE
                                           ,CASE WHEN (
                                                        NNChild.ResourceId IN ( 716 )
                                                        OR NNParent.ResourceId IN ( 716 )
                                                      ) THEN 2
                                                 ELSE 3
                                            END AS GroupSortOrder
                                           ,RChild.ResourceTypeID
                                           ,RChild.ChildTypeId
                                  FROM      syResources RChild
                                  INNER JOIN syNavigationNodes NNChild ON RChild.ResourceID = NNChild.ResourceId
                                  INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                                  INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                                  LEFT OUTER JOIN (
                                                    SELECT  *
                                                    FROM    syResources
                                                    WHERE   ResourceTypeID = 1
                                                  ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                                  WHERE     RChild.ResourceTypeID IN ( 2,5,8 )
                                            AND (
                                                  RChild.ChildTypeId IS NULL
                                                  OR RChild.ChildTypeId = 5
                                                )
				--AND (NNParent.ResourceId IN (689,409,711,472,474,712))
                                            AND ( RChild.ResourceID NOT IN ( 394 ) )
                                            AND (
                                                  NNParent.ParentId IN ( SELECT HierarchyId
                                                                         FROM   syNavigationNodes
                                                                         WHERE  ResourceId = 191 )
                                                  OR NNChild.ParentId IN ( SELECT   HierarchyId
                                                                           FROM     syNavigationNodes
                                                                           WHERE    ResourceId IN ( 716 ) )
                                                )
										--DE8343
                                            AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
                                ) t1
                      UNION
                      SELECT    193 AS ModuleResourceId
                               ,ResourceID AS ChildResourceId
                               ,Resource AS ChildResource
                               ,(
                                  SELECT    AccessLevel
                                  FROM      syRlsResLvls
                                  WHERE     RoleId = @RoleId
                                            AND ResourceID = syResources.ResourceID
                                ) AS AccessLevel
                               ,ResourceURL AS ChildResourceURL
                               ,NULL AS ParentResourceId
                               ,NULL AS ParentResource
                               ,1 AS GroupSortOrder
                               ,NULL AS FirstSortOrder
                               ,NULL AS DisplaySequence
                               ,ResourceTypeID
                               ,5 AS ChildTypeId
                      FROM      syResources
                      WHERE     ResourceID = 193
                      UNION
                      SELECT    193 AS ModuleResourceId
                               ,ChildResourceId
                               ,ChildResource
                               ,(
                                  SELECT    AccessLevel
                                  FROM      syRlsResLvls
                                  WHERE     RoleId = @RoleId
                                            AND ResourceID = t1.ChildResourceId
                                ) AS AccessLevel
                               ,ChildResourceURL
                               ,ParentResourceId
                               ,ParentResource
                               ,GroupSortOrder
                               ,GroupSortOrder AS FirstSortOrder
                               ,CASE WHEN (
                                            ChildResourceId IN ( 713 )
                                            OR ParentResourceId IN ( 713 )
                                          ) THEN 1
                                     ELSE 2
                                END AS DisplaySequence
                               ,ResourceTypeID
                               ,ChildTypeId
                      FROM      (
                                  SELECT DISTINCT
                                            NNChild.ResourceId AS ChildResourceId
                                           ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                                 ELSE RChild.Resource
                                            END AS ChildResource
                                           ,RChild.ResourceURL AS ChildResourceURL
                                           ,CASE WHEN (
                                                        NNParent.ResourceId = 193
                                                        OR NNChild.ResourceId IN ( 713,735 )
                                                      ) THEN NULL
                                                 ELSE NNParent.ResourceId
                                            END AS ParentResourceId
                                           ,RParent.Resource AS ParentResource
                                           ,RParentModule.Resource AS MODULE
                                           ,CASE WHEN (
                                                        NNChild.ResourceId IN ( 713 )
                                                        OR NNParent.ResourceId IN ( 713 )
                                                      ) THEN 2
                                                 ELSE 3
                                            END AS GroupSortOrder
                                           ,RChild.ResourceTypeID
                                           ,RChild.ChildTypeId
                                  FROM      syResources RChild
                                  INNER JOIN syNavigationNodes NNChild ON RChild.ResourceID = NNChild.ResourceId
                                  INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                                  INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                                  LEFT OUTER JOIN (
                                                    SELECT  *
                                                    FROM    syResources
                                                    WHERE   ResourceTypeID = 1
                                                  ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                                  WHERE     RChild.ResourceTypeID IN ( 2,5,8 )
                                            AND (
                                                  RChild.ChildTypeId IS NULL
                                                  OR RChild.ChildTypeId = 5
                                                )
				--AND (NNParent.ResourceId IN (689,409,711,472,474,712))
                                            AND ( RChild.ResourceID NOT IN ( 394 ) )
                                            AND (
                                                  NNParent.ParentId IN ( SELECT HierarchyId
                                                                         FROM   syNavigationNodes
                                                                         WHERE  ResourceId = 193 )
                                                  OR NNChild.ParentId IN ( SELECT   HierarchyId
                                                                           FROM     syNavigationNodes
                                                                           WHERE    ResourceId IN ( 713,735 ) )
                                                )
										--DE8343
                                            AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
                                ) t1
                      UNION
                      SELECT    195 AS ModuleResourceId
                               ,ResourceID AS ChildResourceId
                               ,Resource AS ChildResource
                               ,(
                                  SELECT    AccessLevel
                                  FROM      syRlsResLvls
                                  WHERE     RoleId = @RoleId
                                            AND ResourceID = syResources.ResourceID
                                ) AS AccessLevel
                               ,ResourceURL AS ChildResourceURL
                               ,NULL AS ParentResourceId
                               ,NULL AS ParentResource
                               ,1 AS GroupSortOrder
                               ,NULL AS FirstSortOrder
                               ,NULL AS DisplaySequence
                               ,ResourceTypeID
                               ,5 AS ChildTypeId
                      FROM      syResources
                      WHERE     ResourceID = 195
                      UNION
                      SELECT    195 AS ModuleResourceId
                               ,ChildResourceId
                               ,ChildResource
                               ,(
                                  SELECT    AccessLevel
                                  FROM      syRlsResLvls
                                  WHERE     RoleId = @RoleId
                                            AND ResourceID = t1.ChildResourceId
                                ) AS AccessLevel
                               ,ChildResourceURL
                               ,ParentResourceId
                               ,ParentResource
                               ,GroupSortOrder
                               ,GroupSortOrder AS FirstSortOrder
                               ,CASE WHEN (
                                            ChildResourceId IN ( 717 )
                                            OR ParentResourceId IN ( 717 )
                                          ) THEN 1
                                     ELSE 2
                                END AS DisplaySequence
                               ,ResourceTypeID
                               ,ChildTypeId
                      FROM      (
                                  SELECT DISTINCT
                                            NNChild.ResourceId AS ChildResourceId
                                           ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                                 ELSE RChild.Resource
                                            END AS ChildResource
                                           ,RChild.ResourceURL AS ChildResourceURL
                                           ,CASE WHEN (
                                                        NNParent.ResourceId = 195
                                                        OR NNChild.ResourceId IN ( 717 )
                                                      ) THEN NULL
                                                 ELSE NNParent.ResourceId
                                            END AS ParentResourceId
                                           ,RParent.Resource AS ParentResource
                                           ,RParentModule.Resource AS MODULE
                                           ,CASE WHEN (
                                                        NNChild.ResourceId IN ( 717 )
                                                        OR NNParent.ResourceId IN ( 717 )
                                                      ) THEN 2
                                                 ELSE 3
                                            END AS GroupSortOrder
                                           ,RChild.ResourceTypeID
                                           ,RChild.ChildTypeId
                                  FROM      syResources RChild
                                  INNER JOIN syNavigationNodes NNChild ON RChild.ResourceID = NNChild.ResourceId
                                  INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                                  INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                                  LEFT OUTER JOIN (
                                                    SELECT  *
                                                    FROM    syResources
                                                    WHERE   ResourceTypeID = 1
                                                  ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                                  WHERE     RChild.ResourceTypeID IN ( 2,5,8 )
                                            AND (
                                                  RChild.ChildTypeId IS NULL
                                                  OR RChild.ChildTypeId = 5
                                                )
				--AND (NNParent.ResourceId IN (689,409,711,472,474,712))
                                            AND ( RChild.ResourceID NOT IN ( 394 ) )
                                            AND (
                                                  NNParent.ParentId IN ( SELECT HierarchyId
                                                                         FROM   syNavigationNodes
                                                                         WHERE  ResourceId = 195 )
                                                  OR NNChild.ParentId IN ( SELECT   HierarchyId
                                                                           FROM     syNavigationNodes
                                                                           WHERE    ResourceId IN ( 717 ) )
                                                )
										--DE8343
                                            AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
                                ) t1
                    ) t2
            WHERE   t2.ModuleResourceId = @ModuleResourceId
                    AND (
                          t2.ResourceTypeID = 5
                          OR (
                               t2.ResourceTypeID = 2
                               AND t2.ChildTypeId = 5
                             )
                        )
					-- Hide resources based on Configuration Settings  
                    AND (
                          -- The following expression means if showross... is set to false, hide   
	 -- ResourceIds 541,542,532,534,535,538,543,539  
	 -- (Not False) OR (Condition)  
                          (
                            ( @ShowRossOnlyTabs <> 0 )
                            OR ( ChildResourceId NOT IN ( 541,542,532,534,535,538,543,539 ) )
                          )
                          AND  
	 -- The following expression means if showross... is set to true, hide   
	 -- ResourceIds 142,375,330,476,508,102,107,237  
	 -- (Not True) OR (Condition)  
                          (
                            ( @ShowRossOnlyTabs <> 1 )
                            OR ( ChildResourceId NOT IN ( 142,375,330,476,508,102,107,237 ) )
                          )
                          AND  
	 ---- The following expression means if SchedulingMethod=regulartraditional, hide   
	 ---- ResourceIds 91 and 497  
	 ---- (Not True) OR (Condition)  
                          (
                            (
                              NOT LOWER(LTRIM(RTRIM(@SchedulingMethod))) = 'regulartraditional'
                            )
                            OR ( ChildResourceId NOT IN ( 91,497 ) )
                          )
                          AND  
	 -- The following expression means if TrackSAPAttendance=byday, hide   
	 -- ResourceIds 585,586,589,590,677,678,770,670  
	 -- (Not True) OR (Condition) 
					-- DE8640 
				 -- ( ( NOT LOWER(LTRIM(RTRIM(@TrackSAPAttendance))) = 'byday'
					--)
					--OR ( ChildResourceId NOT IN ( 586, 589, 590, 670 ) )
				 -- )
				 -- AND  
                          (
                            (
                              NOT LOWER(LTRIM(RTRIM(@TrackSAPAttendance))) = 'byclass'
                            )
                            OR ( ChildResourceId NOT IN ( 633 ) )
                          )
                          AND  
	 ---- The following expression means if @ShowCollegeOfCourtReporting is set to false, hide   
	 ---- ResourceIds 614,615  
	 ---- (Not False) OR (Condition)  
                          (
                            (
                              NOT LOWER(LTRIM(RTRIM(@ShowCollegeOfCourtReporting))) = 'no'
                            )
                            OR ( ChildResourceId NOT IN ( 614,615,729 ) )
                          )
                          AND  
	 -- The following expression means if @ShowCollegeOfCourtReporting is set to true, hide   
	 -- ResourceIds 497  
	 -- (Not True) OR (Condition)  
                          (
                            (
                              NOT LOWER(LTRIM(RTRIM(@ShowCollegeOfCourtReporting))) = 'yes'
                            )
                            OR ( ChildResourceId NOT IN ( 497 ) )
                          )
                          AND  
	 -- The following expression means if FAMEESP is set to false, hide   
	 -- ResourceIds 517,523, 525  
	 -- (Not False) OR (Condition)  
                          (
                            (
                              NOT LOWER(LTRIM(RTRIM(@FameESP))) = 'no'
                            )
                            OR ( ChildResourceId NOT IN ( 517,523,525 ) )
                          )
                          AND  
	 ---- The following expression means if EDExpress is set to false, hide   
	 ---- ResourceIds 603,604,606,619  
	 ---- (Not False) OR (Condition)  
                          (
                            (
                              NOT LOWER(LTRIM(RTRIM(@EdExpress))) = 'no'
                            )
                            OR ( ChildResourceId NOT IN ( 603,604,605,619 ) )
                          )
                          AND  
	 ---- The following expression means if @GradeBookWeightingLevel is set to courselevel, hide   
	 ---- ResourceIds 107,96,222  
	 ---- (Not False) OR (Condition)  
                          (
                            (
                              NOT LOWER(LTRIM(RTRIM(@GradeBookWeightingLevel))) = 'courselevel'
                            )
                            OR ( ChildResourceId NOT IN ( 107,96,222 ) )
                          )
                          AND (
                                (
                                  NOT LOWER(LTRIM(RTRIM(@GradeBookWeightingLevel))) = 'instructorlevel'
                                )
                                OR ( ChildResourceId NOT IN ( 476 ) )
                              )
                          AND (
                                (
                                  NOT LOWER(LTRIM(RTRIM(@ShowExternshipTabs))) = 'no'
                                )
                                OR ( ChildResourceId NOT IN ( 543,538 ) )
                              )
                        )
            ORDER BY GroupSortOrder
                   ,ParentResourceId
                   ,ChildResource;
        END;
    IF @tabid > 0
        BEGIN
            SELECT  ModuleResourceId
                   ,ChildResourceId
                   ,ChildResource
                   ,AccessLevel
                   ,ChildResourceURL
                   ,ParentResourceId
                   ,ParentResource
                   ,GroupSortOrder
                   ,FirstSortOrder
                   ,DisplaySequence
                   ,ResourceTypeID
                   ,ChildTypeId
                   ,CASE WHEN AccessLevel = 15 THEN 'Full Access'
                         ELSE CASE WHEN AccessLevel = 12 THEN 'Add and Edit Only'
                                   ELSE CASE WHEN AccessLevel = 10 THEN 'Edit and Delete Only'
                                             ELSE CASE WHEN AccessLevel = 6 THEN 'Add and Delete Only'
                                                       ELSE CASE WHEN AccessLevel = 4 THEN 'Add Only'
                                                                 ELSE CASE WHEN AccessLevel = 8 THEN 'Edit Only'
                                                                           ELSE CASE WHEN AccessLevel = 1 THEN 'Display Only'
                                                                                     ELSE NULL
                                                                                END
                                                                      END
                                                            END
                                                  END
                                        END
                              END
                    END AS PermissionText
                   ,TabId
            FROM    (
                      SELECT    189 AS ModuleResourceId
                               ,395 AS TabId
                               ,ResourceID AS ChildResourceId
                               ,Resource AS ChildResource
                               ,(
                                  SELECT    AccessLevel
                                  FROM      syRlsResLvls
                                  WHERE     RoleId = @RoleId
                                            AND ResourceID = syResources.ResourceID
                                ) AS AccessLevel
                               ,ResourceURL AS ChildResourceURL
                               ,NULL AS ParentResourceId
                               ,NULL AS ParentResource
                               ,1 AS GroupSortOrder
                               ,NULL AS FirstSortOrder
                               ,NULL AS DisplaySequence
                               ,ResourceTypeID
                               ,ChildTypeId
                      FROM      syResources
                      WHERE     ResourceID = 189
                      UNION
                      SELECT    189 AS ModuleResourceId
                               ,395 AS TabId
                               ,NNChild.ResourceId AS ChildResourceId
                               ,CASE WHEN NNChild.ResourceId = 395 THEN 'Lead Tabs'
                                     ELSE CASE WHEN NNChild.ResourceId = 398 THEN 'Add/View Leads'
                                               ELSE RChild.Resource
                                          END
                                END AS ChildResource
                               ,(
                                  SELECT    AccessLevel
                                  FROM      syRlsResLvls
                                  WHERE     RoleId = @RoleId
                                            AND ResourceID = RChild.ResourceID
                                ) AS AccessLevel
                               ,RChild.ResourceURL AS ChildResourceURL
                               ,CASE WHEN (
                                            NNParent.ResourceId IN ( 395 )
                                            OR NNChild.ResourceId IN ( 737,740,741,792 )
                                          ) THEN NULL
                                     ELSE NNParent.ResourceId
                                END AS ParentResourceId
                               ,RParent.Resource AS ParentResource
                               ,CASE WHEN (
                                            NNChild.ResourceId = 737
                                            OR NNParent.ResourceId IN ( 737 )
                                          ) THEN 1
                                     ELSE CASE WHEN (
                                                      NNChild.ResourceId = 740
                                                      OR NNParent.ResourceId IN ( 740 )
                                                    ) THEN 2
                                               ELSE CASE WHEN (
                                                                NNChild.ResourceId = 741
                                                                OR NNParent.ResourceId IN ( 741 )
                                                              ) THEN 3
                                                         ELSE 4
                                                    END
                                          END
                                END AS GroupSortOrder
                               ,CASE WHEN NNChild.ResourceId = 737 THEN 1
                                     ELSE CASE WHEN NNChild.ResourceId = 395 THEN 2
                                               ELSE 3
                                          END
                                END AS FirstSortOrder
                               ,CASE WHEN (
                                            NNChild.ResourceId IN ( 737 )
                                            OR NNParent.ResourceId IN ( 737 )
                                          ) THEN 1
                                     ELSE 2
                                END AS DisplaySequence
                               ,RChild.ResourceTypeID
                               ,RChild.ChildTypeId
                      FROM      syResources RChild
                      INNER JOIN syNavigationNodes NNChild ON RChild.ResourceID = NNChild.ResourceId
                      INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                      INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                      LEFT OUTER JOIN (
                                        SELECT  *
                                        FROM    syResources
                                        WHERE   ResourceTypeID = 1
                                      ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                      WHERE     RChild.ResourceTypeID IN ( 2,3,8 )
                                AND (
                                      RChild.ChildTypeId IS NULL
                                      OR RChild.ChildTypeId = 3
                                    )
                                AND ( RChild.ResourceID NOT IN ( 394 ) )
                                AND (
                                      NNParent.ParentId IN ( SELECT HierarchyId
                                                             FROM   syNavigationNodes
                                                             WHERE  ResourceId = 395 )
                                      OR NNChild.HierarchyId IN ( SELECT DISTINCT
                                                                            HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId IN ( 737,740,741,792 )
                                                                            AND ParentId IN ( SELECT    HierarchyId
                                                                                              FROM      syNavigationNodes
                                                                                              WHERE     ResourceId = 395
                                                                                                        AND ParentId IN ( SELECT    HierarchyId
                                                                                                                          FROM      syNavigationNodes
                                                                                                                          WHERE     ResourceId = 189 ) ) )
                                    )
									--DE8343
                                AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
                      UNION ALL
                      SELECT    26 AS ModuleResourceId
                               ,394 AS TabId
                               ,ResourceID AS ChildResourceId
                               ,Resource AS ChildResource
                               ,(
                                  SELECT    AccessLevel
                                  FROM      syRlsResLvls
                                  WHERE     RoleId = @RoleId
                                            AND ResourceID = syResources.ResourceID
                                ) AS AccessLevel
                               ,ResourceURL AS ChildResourceURL
                               ,NULL AS ParentResourceId
                               ,NULL AS ParentResource
                               ,1 AS GroupSortOrder
                               ,NULL AS FirstSortOrder
                               ,NULL AS DisplaySequence
                               ,ResourceTypeID
                               ,ChildTypeId
                      FROM      syResources
                      WHERE     ResourceID = 26
                      UNION
                      SELECT    26 AS ModuleResourceId
                               ,394 AS TabId
                               ,ChildResourceId
                               ,ChildResource
                               ,(
                                  SELECT    AccessLevel
                                  FROM      syRlsResLvls
                                  WHERE     RoleId = @RoleId
                                            AND ResourceID = t1.ChildResourceId
                                ) AS AccessLevel
                               ,ChildResourceURL
                               ,ParentResourceId
                               ,ParentResource
                               ,CASE WHEN (
                                            t1.ChildResourceId = 737
                                            OR t1.ParentResourceId IN ( 737 )
                                          ) THEN 2
                                     ELSE CASE WHEN (
                                                      t1.ChildResourceId = 739
                                                      OR t1.ParentResourceId IN ( 739 )
                                                    ) THEN 3
                                               ELSE CASE WHEN (
                                                                t1.ChildResourceId = 738
                                                                OR t1.ParentResourceId IN ( 738 )
                                                              ) THEN 4
                                                         ELSE CASE WHEN (
                                                                          t1.ChildResourceId = 740
                                                                          OR t1.ParentResourceId IN ( 740 )
                                                                        ) THEN 5
                                                                   ELSE CASE WHEN (
                                                                                    t1.ChildResourceId = 741
                                                                                    OR t1.ParentResourceId IN ( 741 )
                                                                                  ) THEN 6
                                                                             ELSE 7
                                                                        END
                                                              END
                                                    END
                                          END
                                END AS GroupSortOrder
                               ,CASE WHEN ChildResourceId = 738 THEN 1
                                     ELSE 2
                                END AS FirstSortOrder
                               ,CASE WHEN (
                                            ChildResourceId IN ( 738 )
                                            OR ParentResourceId IN ( 738 )
                                          ) THEN 1
                                     ELSE 2
                                END AS DisplaySequence
                               ,ResourceTypeID
                               ,ChildTypeId
                      FROM      (
                                  SELECT DISTINCT
                                            NNChild.ResourceId AS ChildResourceId
                                           ,CASE WHEN NNChild.ResourceId = 409 THEN 'IPEDS - General Reports'
                                                 ELSE RChild.Resource
                                            END AS ChildResource
                                           ,RChild.ResourceURL AS ChildResourceURL
                                           ,CASE WHEN (
                                                        NNParent.ResourceId IN ( 689 )
                                                        OR NNChild.ResourceId IN ( 737,738,739,740,741,742 )
                                                      ) THEN NULL
                                                 ELSE NNParent.ResourceId
                                            END AS ParentResourceId
                                           ,RParent.Resource AS ParentResource
                                           ,RParentModule.Resource AS MODULE
                                           ,RChild.ResourceTypeID
                                           ,RChild.ChildTypeId
                                  FROM      syResources RChild
                                  INNER JOIN syNavigationNodes NNChild ON RChild.ResourceID = NNChild.ResourceId
                                  INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                                  INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                                  LEFT OUTER JOIN (
                                                    SELECT  *
                                                    FROM    syResources
                                                    WHERE   ResourceTypeID = 1
                                                  ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                                  WHERE     RChild.ResourceTypeID IN ( 2,3,8 )
                                            AND (
                                                  RChild.ChildTypeId IS NULL
                                                  OR RChild.ChildTypeId = 3
                                                )
                                            AND ( NNParent.ParentId IN ( SELECT HierarchyId
                                                                         FROM   syNavigationNodes
                                                                         WHERE  ResourceId = 394
                                                                                AND ParentId IN ( SELECT    HierarchyId
                                                                                                  FROM      syNavigationNodes
                                                                                                  WHERE     ResourceId = 26 ) ) )
                                  UNION
                                  SELECT DISTINCT
                                            RChild.ResourceID AS ChildResourceId
                                           ,RChild.Resource AS ChildResource
                                           ,RChild.ResourceURL AS ChildResourceURL
                                           ,CASE WHEN ( RChild.ResourceID IN ( 737,738,739,740,741,742 ) ) THEN NULL
                                                 ELSE RChild.ResourceID
                                            END AS ParentResourceId
                                           ,NULL AS ParentResource
                                           ,NULL AS MODULE
                                           ,RChild.ResourceTypeID
                                           ,RChild.ChildTypeId
                                  FROM      syResources RChild
                                  WHERE     RChild.ResourceID IN ( 737,738,739,740,741,742 )
										--DE8343
                                            AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
                                ) t1
                      UNION ALL
                      SELECT    194 AS ModuleResourceId
                               ,394 AS TabId
                               ,ResourceID AS ChildResourceId
                               ,Resource AS ChildResource
                               ,(
                                  SELECT    AccessLevel
                                  FROM      syRlsResLvls
                                  WHERE     RoleId = @RoleId
                                            AND ResourceID = syResources.ResourceID
                                ) AS AccessLevel
                               ,ResourceURL AS ChildResourceURL
                               ,NULL AS ParentResourceId
                               ,NULL AS ParentResource
                               ,1 AS GroupSortOrder
                               ,NULL AS FirstSortOrder
                               ,NULL AS DisplaySequence
                               ,ResourceTypeID
                               ,ChildTypeId
                      FROM      syResources
                      WHERE     ResourceID = 194
                      UNION
                      SELECT    194 AS ModuleResourceId
                               ,394 AS TabId
                               ,ChildResourceId
                               ,ChildResource
                               ,(
                                  SELECT    AccessLevel
                                  FROM      syRlsResLvls
                                  WHERE     RoleId = @RoleId
                                            AND ResourceID = t1.ChildResourceId
                                ) AS AccessLevel
                               ,ChildResourceURL
                               ,ParentResourceId
                               ,ParentResource
                               ,CASE WHEN (
                                            t1.ChildResourceId = 737
                                            OR t1.ParentResourceId IN ( 737 )
                                          ) THEN 1
                                     ELSE CASE WHEN (
                                                      t1.ChildResourceId = 739
                                                      OR t1.ParentResourceId IN ( 739 )
                                                    ) THEN 2
                                               ELSE CASE WHEN (
                                                                t1.ChildResourceId = 738
                                                                OR t1.ParentResourceId IN ( 738 )
                                                              ) THEN 3
                                                         ELSE CASE WHEN (
                                                                          t1.ChildResourceId = 740
                                                                          OR t1.ParentResourceId IN ( 740 )
                                                                        ) THEN 4
                                                                   ELSE CASE WHEN (
                                                                                    t1.ChildResourceId = 741
                                                                                    OR t1.ParentResourceId IN ( 741 )
                                                                                  ) THEN 5
                                                                             ELSE CASE WHEN (
                                                                                              t1.ChildResourceId = 742
                                                                                              OR t1.ParentResourceId IN ( 742 )
                                                                                            ) THEN 6
                                                                                  END
                                                                        END
                                                              END
                                                    END
                                          END
                                END AS GroupSortOrder
                               ,CASE WHEN ChildResourceId = 737 THEN 1
                                     ELSE 2
                                END AS FirstSortOrder
                               ,CASE WHEN (
                                            ChildResourceId IN ( 737,738,740,741 )
                                            OR ParentResourceId IN ( 737,738,740,741 )
                                          ) THEN 1
                                     ELSE 2
                                END AS DisplaySequence
                               ,ResourceTypeID
                               ,ChildTypeId
                      FROM      (
                                  SELECT DISTINCT
                                            NNChild.ResourceId AS ChildResourceId
                                           ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                                 ELSE RChild.Resource
                                            END AS ChildResource
                                           ,RChild.ResourceURL AS ChildResourceURL
                                           ,CASE WHEN NNParent.ResourceId IN ( 194 )
                                                      OR NNChild.ResourceId IN ( 737,738,739,740,741,742 ) THEN NULL
                                                 ELSE NNParent.ResourceId
                                            END AS ParentResourceId
                                           ,RParent.Resource AS ParentResource
                                           ,RParentModule.Resource AS MODULE
                                           ,RChild.ResourceTypeID
                                           ,RChild.ChildTypeId
                                  FROM      syResources RChild
                                  INNER JOIN syNavigationNodes NNChild ON RChild.ResourceID = NNChild.ResourceId
                                  INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                                  INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                                  LEFT OUTER JOIN (
                                                    SELECT  *
                                                    FROM    syResources
                                                    WHERE   ResourceTypeID = 1
                                                  ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                                  WHERE     RChild.ResourceTypeID IN ( 2,3,8 )
                                            AND (
                                                  RChild.ChildTypeId IS NULL
                                                  OR RChild.ChildTypeId = 3
                                                )
                                            AND ( NNParent.ParentId IN ( SELECT HierarchyId
                                                                         FROM   syNavigationNodes
                                                                         WHERE  ResourceId = 394
                                                                                AND ParentId IN ( SELECT    HierarchyId
                                                                                                  FROM      syNavigationNodes
                                                                                                  WHERE     ResourceId = 194 ) ) )
                                  UNION
                                  SELECT DISTINCT
                                            RChild.ResourceID AS ChildResourceId
                                           ,RChild.Resource AS ChildResource
                                           ,RChild.ResourceURL AS ChildResourceURL
                                           ,CASE WHEN ( RChild.ResourceID IN ( 737,738,739,740,741,742 ) ) THEN NULL
                                                 ELSE RChild.ResourceID
                                            END AS ParentResourceId
                                           ,NULL AS ParentResource
                                           ,NULL AS MODULE
                                           ,RChild.ResourceTypeID
                                           ,RChild.ChildTypeId
                                  FROM      syResources RChild
                                  WHERE     RChild.ResourceID IN ( 737,738,739,740,741,742 )
										--DE8343
                                            AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
                                ) t1
                      UNION ALL
                      SELECT    300 AS ModuleResourceId
                               ,394 AS TabId
                               ,ResourceID AS ChildResourceId
                               ,Resource AS ChildResource
                               ,(
                                  SELECT    AccessLevel
                                  FROM      syRlsResLvls
                                  WHERE     RoleId = @RoleId
                                            AND ResourceID = syResources.ResourceID
                                ) AS AccessLevel
                               ,ResourceURL AS ChildResourceURL
                               ,NULL AS ParentResourceId
                               ,NULL AS ParentResource
                               ,1 AS GroupSortOrder
                               ,NULL AS FirstSortOrder
                               ,NULL AS DisplaySequence
                               ,ResourceTypeID
                               ,ChildTypeId
                      FROM      syResources
                      WHERE     ResourceID = 300
                      UNION
                      SELECT    300 AS ModuleResourceId
                               ,394 AS TabId
                               ,ChildResourceId
                               ,ChildResource
                               ,(
                                  SELECT    AccessLevel
                                  FROM      syRlsResLvls
                                  WHERE     RoleId = @RoleId
                                            AND ResourceID = t1.ChildResourceId
                                ) AS AccessLevel
                               ,ChildResourceURL
                               ,ParentResourceId
                               ,ParentResource
                               ,CASE WHEN (
                                            t1.ChildResourceId = 737
                                            OR t1.ParentResourceId IN ( 737 )
                                          ) THEN 1
                                     ELSE CASE WHEN (
                                                      t1.ChildResourceId = 739
                                                      OR t1.ParentResourceId IN ( 739 )
                                                    ) THEN 2
                                               ELSE CASE WHEN (
                                                                t1.ChildResourceId = 738
                                                                OR t1.ParentResourceId IN ( 738 )
                                                              ) THEN 3
                                                         ELSE CASE WHEN (
                                                                          t1.ChildResourceId = 740
                                                                          OR t1.ParentResourceId IN ( 740 )
                                                                        ) THEN 4
                                                                   ELSE CASE WHEN (
                                                                                    t1.ChildResourceId = 741
                                                                                    OR t1.ParentResourceId IN ( 741 )
                                                                                  ) THEN 5
                                                                             ELSE CASE WHEN (
                                                                                              t1.ChildResourceId = 742
                                                                                              OR t1.ParentResourceId IN ( 742 )
                                                                                            ) THEN 6
                                                                                  END
                                                                        END
                                                              END
                                                    END
                                          END
                                END AS GroupSortOrder
                               ,CASE WHEN ChildResourceId = 737 THEN 1
                                     ELSE 2
                                END AS FirstSortOrder
                               ,CASE WHEN (
                                            ChildResourceId IN ( 737,739 )
                                            OR ParentResourceId IN ( 737,739 )
                                          ) THEN 1
                                     ELSE 2
                                END AS DisplaySequence
                               ,ResourceTypeID
                               ,ChildTypeId
                      FROM      (
                                  SELECT DISTINCT
                                            NNChild.ResourceId AS ChildResourceId
                                           ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                                 ELSE RChild.Resource
                                            END AS ChildResource
                                           ,RChild.ResourceURL AS ChildResourceURL
                                           ,CASE WHEN NNParent.ResourceId = 300
                                                      OR NNChild.ResourceId IN ( 737,739,740,741 ) THEN NULL
                                                 ELSE NNParent.ResourceId
                                            END AS ParentResourceId
                                           ,RParent.Resource AS ParentResource
                                           ,RParentModule.Resource AS MODULE
                                           ,RChild.ResourceTypeID
                                           ,RChild.ChildTypeId
                                  FROM      syResources RChild
                                  INNER JOIN syNavigationNodes NNChild ON RChild.ResourceID = NNChild.ResourceId
                                  INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                                  INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                                  LEFT OUTER JOIN (
                                                    SELECT  *
                                                    FROM    syResources
                                                    WHERE   ResourceTypeID = 1
                                                  ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                                  WHERE     RChild.ResourceTypeID IN ( 2,3,8 )
                                            AND (
                                                  RChild.ChildTypeId IS NULL
                                                  OR RChild.ChildTypeId = 3
                                                )
                                            AND ( NNParent.ParentId IN ( SELECT HierarchyId
                                                                         FROM   syNavigationNodes
                                                                         WHERE  ResourceId = 394
                                                                                AND ParentId IN ( SELECT    HierarchyId
                                                                                                  FROM      syNavigationNodes
                                                                                                  WHERE     ResourceId = 300 ) ) )
                                  UNION
                                  SELECT DISTINCT
                                            RChild.ResourceID AS ChildResourceId
                                           ,RChild.Resource AS ChildResource
                                           ,RChild.ResourceURL AS ChildResourceURL
                                           ,CASE WHEN ( RChild.ResourceID IN ( 737,739,740,741 ) ) THEN NULL
                                                 ELSE RChild.ResourceID
                                            END AS ParentResourceId
                                           ,NULL AS ParentResource
                                           ,NULL AS MODULE
                                           ,RChild.ResourceTypeID
                                           ,RChild.ChildTypeId
                                  FROM      syResources RChild
                                  WHERE     RChild.ResourceID IN ( 737,738,739,740,741,742 )
										--DE8343
                                            AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
                                ) t1
                      UNION
                      SELECT    191 AS ModuleResourceId
                               ,394 AS TabId
                               ,ResourceID AS ChildResourceId
                               ,Resource AS ChildResource
                               ,(
                                  SELECT    AccessLevel
                                  FROM      syRlsResLvls
                                  WHERE     RoleId = @RoleId
                                            AND ResourceID = syResources.ResourceID
                                ) AS AccessLevel
                               ,ResourceURL AS ChildResourceURL
                               ,NULL AS ParentResourceId
                               ,NULL AS ParentResource
                               ,1 AS GroupSortOrder
                               ,NULL AS FirstSortOrder
                               ,NULL AS DisplaySequence
                               ,ResourceTypeID
                               ,ChildTypeId
                      FROM      syResources
                      WHERE     ResourceID = 191
                      UNION
                      SELECT    191 AS ModuleResourceId
                               ,394 AS TabId
                               ,ChildResourceId
                               ,ChildResource
                               ,(
                                  SELECT    AccessLevel
                                  FROM      syRlsResLvls
                                  WHERE     RoleId = @RoleId
                                            AND ResourceID = t1.ChildResourceId
                                ) AS AccessLevel
                               ,ChildResourceURL
                               ,ParentResourceId
                               ,ParentResource
                               ,CASE WHEN (
                                            t1.ChildResourceId = 737
                                            OR t1.ParentResourceId IN ( 737 )
                                          ) THEN 1
                                     ELSE CASE WHEN (
                                                      t1.ChildResourceId = 739
                                                      OR t1.ParentResourceId IN ( 739 )
                                                    ) THEN 2
                                               ELSE CASE WHEN (
                                                                t1.ChildResourceId = 738
                                                                OR t1.ParentResourceId IN ( 738 )
                                                              ) THEN 3
                                                         ELSE CASE WHEN (
                                                                          t1.ChildResourceId = 740
                                                                          OR t1.ParentResourceId IN ( 740 )
                                                                        ) THEN 4
                                                                   ELSE CASE WHEN (
                                                                                    t1.ChildResourceId = 741
                                                                                    OR t1.ParentResourceId IN ( 741 )
                                                                                  ) THEN 5
                                                                             ELSE CASE WHEN (
                                                                                              t1.ChildResourceId = 742
                                                                                              OR t1.ParentResourceId IN ( 742 )
                                                                                            ) THEN 6
                                                                                  END
                                                                        END
                                                              END
                                                    END
                                          END
                                END AS GroupSortOrder
                               ,CASE WHEN ChildResourceId = 737 THEN 1
                                     ELSE 2
                                END AS FirstSortOrder
                               ,CASE WHEN (
                                            ChildResourceId IN ( 737,738,740,741 )
                                            OR ParentResourceId IN ( 737,738,740,741 )
                                          ) THEN 1
                                     ELSE 2
                                END AS DisplaySequence
                               ,ResourceTypeID
                               ,ChildTypeId
                      FROM      (
                                  SELECT DISTINCT
                                            NNChild.ResourceId AS ChildResourceId
                                           ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                                 ELSE RChild.Resource
                                            END AS ChildResource
                                           ,RChild.ResourceURL AS ChildResourceURL
                                           ,CASE WHEN (
                                                        NNParent.ResourceId = 191
                                                        OR NNChild.ResourceId IN ( 737,738,739,740,741,742 )
                                                      ) THEN NULL
                                                 ELSE NNParent.ResourceId
                                            END AS ParentResourceId
                                           ,RParent.Resource AS ParentResource
                                           ,RParentModule.Resource AS MODULE
                                           ,RChild.ResourceTypeID
                                           ,RChild.ChildTypeId
                                  FROM      syResources RChild
                                  INNER JOIN syNavigationNodes NNChild ON RChild.ResourceID = NNChild.ResourceId
                                  INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                                  INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                                  LEFT OUTER JOIN (
                                                    SELECT  *
                                                    FROM    syResources
                                                    WHERE   ResourceTypeID = 1
                                                  ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                                  WHERE     RChild.ResourceTypeID IN ( 2,3,8 )
                                            AND (
                                                  RChild.ChildTypeId IS NULL
                                                  OR RChild.ChildTypeId = 3
                                                )
                                            AND ( NNParent.ParentId IN ( SELECT HierarchyId
                                                                         FROM   syNavigationNodes
                                                                         WHERE  ResourceId = 394
                                                                                AND ParentId IN ( SELECT    HierarchyId
                                                                                                  FROM      syNavigationNodes
                                                                                                  WHERE     ResourceId = 191 ) ) )
                                  UNION
                                  SELECT DISTINCT
                                            RChild.ResourceID AS ChildResourceId
                                           ,RChild.Resource AS ChildResource
                                           ,RChild.ResourceURL AS ChildResourceURL
                                           ,CASE WHEN ( RChild.ResourceID IN ( 737,738,739,740,741,742 ) ) THEN NULL
                                                 ELSE RChild.ResourceID
                                            END AS ParentResourceId
                                           ,NULL AS ParentResource
                                           ,NULL AS MODULE
                                           ,RChild.ResourceTypeID
                                           ,RChild.ChildTypeId
                                  FROM      syResources RChild
                                  WHERE     RChild.ResourceID IN ( 737,738,739,740,741,742 )
										--DE8343
                                            AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
                                ) t1
                      UNION
                      SELECT    193 AS ModuleResourceId
                               ,394 AS TabId
                               ,ResourceID AS ChildResourceId
                               ,Resource AS ChildResource
                               ,(
                                  SELECT    AccessLevel
                                  FROM      syRlsResLvls
                                  WHERE     RoleId = @RoleId
                                            AND ResourceID = syResources.ResourceID
                                ) AS AccessLevel
                               ,ResourceURL AS ChildResourceURL
                               ,NULL AS ParentResourceId
                               ,NULL AS ParentResource
                               ,1 AS GroupSortOrder
                               ,NULL AS FirstSortOrder
                               ,NULL AS DisplaySequence
                               ,ResourceTypeID
                               ,ChildTypeId
                      FROM      syResources
                      WHERE     ResourceID = 193
                      UNION
                      SELECT    193 AS ModuleResourceId
                               ,394 AS TabId
                               ,ChildResourceId
                               ,ChildResource
                               ,(
                                  SELECT    AccessLevel
                                  FROM      syRlsResLvls
                                  WHERE     RoleId = @RoleId
                                            AND ResourceID = t1.ChildResourceId
                                ) AS AccessLevel
                               ,ChildResourceURL
                               ,ParentResourceId
                               ,ParentResource
                               ,CASE WHEN (
                                            t1.ChildResourceId = 737
                                            OR t1.ParentResourceId IN ( 737 )
                                          ) THEN 2
                                     ELSE CASE WHEN (
                                                      t1.ChildResourceId = 739
                                                      OR t1.ParentResourceId IN ( 739 )
                                                    ) THEN 3
                                               ELSE CASE WHEN (
                                                                t1.ChildResourceId = 738
                                                                OR t1.ParentResourceId IN ( 738 )
                                                              ) THEN 4
                                                         ELSE CASE WHEN (
                                                                          t1.ChildResourceId = 740
                                                                          OR t1.ParentResourceId IN ( 740 )
                                                                        ) THEN 5
                                                                   ELSE CASE WHEN (
                                                                                    t1.ChildResourceId = 741
                                                                                    OR t1.ParentResourceId IN ( 741 )
                                                                                  ) THEN 6
                                                                             ELSE CASE WHEN (
                                                                                              t1.ChildResourceId = 742
                                                                                              OR t1.ParentResourceId IN ( 742 )
                                                                                            ) THEN 7
                                                                                  END
                                                                        END
                                                              END
                                                    END
                                          END
                                END AS GroupSortOrder
                               ,CASE WHEN ChildResourceId = 737 THEN 1
                                     ELSE 2
                                END AS FirstSortOrder
                               ,CASE WHEN (
                                            ChildResourceId IN ( 737,738,740,741 )
                                            OR ParentResourceId IN ( 737,738,740,741 )
                                          ) THEN 1
                                     ELSE 2
                                END AS DisplaySequence
                               ,ResourceTypeID
                               ,ChildTypeId
                      FROM      (
                                  SELECT DISTINCT
                                            NNChild.ResourceId AS ChildResourceId
                                           ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                                 ELSE RChild.Resource
                                            END AS ChildResource
                                           ,RChild.ResourceURL AS ChildResourceURL
                                           ,CASE WHEN (
                                                        NNParent.ResourceId = 193
                                                        OR NNChild.ResourceId IN ( 713,735 )
                                                      ) THEN NULL
                                                 ELSE NNParent.ResourceId
                                            END AS ParentResourceId
                                           ,RParent.Resource AS ParentResource
                                           ,RParentModule.Resource AS MODULE
                                           ,RChild.ResourceTypeID
                                           ,RChild.ChildTypeId
                                  FROM      syResources RChild
                                  INNER JOIN syNavigationNodes NNChild ON RChild.ResourceID = NNChild.ResourceId
                                  INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                                  INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                                  LEFT OUTER JOIN (
                                                    SELECT  *
                                                    FROM    syResources
                                                    WHERE   ResourceTypeID = 1
                                                  ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                                  WHERE     RChild.ResourceTypeID IN ( 2,3,8 )
                                            AND (
                                                  RChild.ChildTypeId IS NULL
                                                  OR RChild.ChildTypeId = 3
                                                )
                                            AND ( NNParent.ParentId IN ( SELECT HierarchyId
                                                                         FROM   syNavigationNodes
                                                                         WHERE  ResourceId = 394
                                                                                AND ParentId IN ( SELECT    HierarchyId
                                                                                                  FROM      syNavigationNodes
                                                                                                  WHERE     ResourceId = 193 ) ) )
                                  UNION
                                  SELECT DISTINCT
                                            RChild.ResourceID AS ChildResourceId
                                           ,RChild.Resource AS ChildResource
                                           ,RChild.ResourceURL AS ChildResourceURL
                                           ,CASE WHEN ( RChild.ResourceID IN ( 737,738,739,741 ) ) THEN NULL
                                                 ELSE RChild.ResourceID
                                            END AS ParentResourceId
                                           ,NULL AS ParentResource
                                           ,NULL AS MODULE
                                           ,RChild.ResourceTypeID
                                           ,RChild.ChildTypeId
                                  FROM      syResources RChild
                                  WHERE     RChild.ResourceID IN ( 737,738,739,740,741,742 )
										--DE8343
                                            AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
                                ) t1
                      UNION
                      SELECT    193 AS ModuleResourceId
                               ,397 AS TabId
                               ,ResourceID AS ChildResourceId
                               ,Resource AS ChildResource
                               ,(
                                  SELECT    AccessLevel
                                  FROM      syRlsResLvls
                                  WHERE     RoleId = @RoleId
                                            AND ResourceID = syResources.ResourceID
                                ) AS AccessLevel
                               ,ResourceURL AS ChildResourceURL
                               ,NULL AS ParentResourceId
                               ,NULL AS ParentResource
                               ,1 AS GroupSortOrder
                               ,NULL AS FirstSortOrder
                               ,NULL AS DisplaySequence
                               ,ResourceTypeID
                               ,ChildTypeId
                      FROM      syResources
                      WHERE     ResourceID = 193
                      UNION
                      SELECT    193 AS ModuleResourceId
                               ,397 AS TabId
                               ,ChildResourceId
                               ,ChildResource
                               ,(
                                  SELECT    AccessLevel
                                  FROM      syRlsResLvls
                                  WHERE     RoleId = @RoleId
                                            AND ResourceID = t1.ChildResourceId
                                ) AS AccessLevel
                               ,ChildResourceURL
                               ,ParentResourceId
                               ,ParentResource
                               ,CASE WHEN (
                                            t1.ChildResourceId = 737
                                            OR t1.ParentResourceId IN ( 737 )
                                          ) THEN 2
                                     ELSE CASE WHEN (
                                                      t1.ChildResourceId = 739
                                                      OR t1.ParentResourceId IN ( 739 )
                                                    ) THEN 3
                                               ELSE CASE WHEN (
                                                                t1.ChildResourceId = 738
                                                                OR t1.ParentResourceId IN ( 738 )
                                                              ) THEN 4
                                                         ELSE CASE WHEN (
                                                                          t1.ChildResourceId = 740
                                                                          OR t1.ParentResourceId IN ( 740 )
                                                                        ) THEN 5
                                                                   ELSE CASE WHEN (
                                                                                    t1.ChildResourceId = 741
                                                                                    OR t1.ParentResourceId IN ( 741 )
                                                                                  ) THEN 6
                                                                             ELSE 6
                                                                        END
                                                              END
                                                    END
                                          END
                                END AS GroupSortOrder
                               ,CASE WHEN ChildResourceId = 737 THEN 1
                                     ELSE 2
                                END AS FirstSortOrder
                               ,CASE WHEN (
                                            ChildResourceId IN ( 737 )
                                            OR ParentResourceId IN ( 737 )
                                          ) THEN 1
                                     ELSE 2
                                END AS DisplaySequence
                               ,ResourceTypeID
                               ,ChildTypeId
                      FROM      (
                                  SELECT DISTINCT
                                            NNChild.ResourceId AS ChildResourceId
                                           ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                                 ELSE RChild.Resource
                                            END AS ChildResource
                                           ,RChild.ResourceURL AS ChildResourceURL
                                           ,CASE WHEN (
                                                        NNParent.ResourceId = 193
                                                        OR NNChild.ResourceId IN ( 737 )
                                                      ) THEN NULL
                                                 ELSE NNParent.ResourceId
                                            END AS ParentResourceId
                                           ,RParent.Resource AS ParentResource
                                           ,RParentModule.Resource AS MODULE
                                           ,RChild.ResourceTypeID
                                           ,RChild.ChildTypeId
                                  FROM      syResources RChild
                                  INNER JOIN syNavigationNodes NNChild ON RChild.ResourceID = NNChild.ResourceId
                                  INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                                  INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                                  LEFT OUTER JOIN (
                                                    SELECT  *
                                                    FROM    syResources
                                                    WHERE   ResourceTypeID = 1
                                                  ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                                  WHERE     RChild.ResourceTypeID IN ( 2,3,8 )
                                            AND (
                                                  RChild.ChildTypeId IS NULL
                                                  OR RChild.ChildTypeId = 3
                                                )
                                            AND ( NNParent.ParentId IN ( SELECT HierarchyId
                                                                         FROM   syNavigationNodes
                                                                         WHERE  ResourceId = 397
                                                                                AND ParentId IN ( SELECT    HierarchyId
                                                                                                  FROM      syNavigationNodes
                                                                                                  WHERE     ResourceId = 193 ) ) )
                                  UNION
                                  SELECT DISTINCT
                                            RChild.ResourceID AS ChildResourceId
                                           ,RChild.Resource AS ChildResource
                                           ,RChild.ResourceURL AS ChildResourceURL
                                           ,CASE WHEN ( RChild.ResourceID IN ( 737 ) ) THEN NULL
                                                 ELSE RChild.ResourceID
                                            END AS ParentResourceId
                                           ,NULL AS ParentResource
                                           ,NULL AS MODULE
                                           ,RChild.ResourceTypeID
                                           ,RChild.ChildTypeId
                                  FROM      syResources RChild
                                  WHERE     RChild.ResourceID IN ( 737 )
										--DE8343
                                            AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
                                ) t1
                      UNION
                      SELECT    192 AS ModuleResourceId
                               ,396 AS TabId
                               ,ResourceID AS ChildResourceId
                               ,Resource AS ChildResource
                               ,(
                                  SELECT    AccessLevel
                                  FROM      syRlsResLvls
                                  WHERE     RoleId = @RoleId
                                            AND ResourceID = syResources.ResourceID
                                ) AS AccessLevel
                               ,ResourceURL AS ChildResourceURL
                               ,NULL AS ParentResourceId
                               ,NULL AS ParentResource
                               ,1 AS GroupSortOrder
                               ,NULL AS FirstSortOrder
                               ,NULL AS DisplaySequence
                               ,ResourceTypeID
                               ,ChildTypeId
                      FROM      syResources
                      WHERE     ResourceID = 192
                      UNION
                      SELECT    192 AS ModuleResourceId
                               ,396 AS TabId
                               ,ChildResourceId
                               ,ChildResource
                               ,(
                                  SELECT    AccessLevel
                                  FROM      syRlsResLvls
                                  WHERE     RoleId = @RoleId
                                            AND ResourceID = t1.ChildResourceId
                                ) AS AccessLevel
                               ,ChildResourceURL
                               ,ParentResourceId
                               ,ParentResource
                               ,CASE WHEN (
                                            ChildResourceId IN ( 713 )
                                            OR ParentResourceId IN ( 713 )
                                          ) THEN 1
                                     ELSE 2
                                END AS GroupSortOrder
                               ,CASE WHEN (
                                            ChildResourceId IN ( 713 )
                                            OR ParentResourceId IN ( 713 )
                                          ) THEN 1
                                     ELSE 2
                                END AS FirstSortOrder
                               ,CASE WHEN (
                                            ChildResourceId IN ( 713 )
                                            OR ParentResourceId IN ( 713 )
                                          ) THEN 1
                                     ELSE 2
                                END AS DisplaySequence
                               ,ResourceTypeID
                               ,ChildTypeId
                      FROM      (
                                  SELECT DISTINCT
                                            NNChild.ResourceId AS ChildResourceId
                                           ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                                 ELSE RChild.Resource
                                            END AS ChildResource
                                           ,RChild.ResourceURL AS ChildResourceURL
                                           ,CASE WHEN (
                                                        NNParent.ResourceId = 193
                                                        OR NNChild.ResourceId IN ( 713,735 )
                                                      ) THEN NULL
                                                 ELSE NNParent.ResourceId
                                            END AS ParentResourceId
                                           ,RParent.Resource AS ParentResource
                                           ,RParentModule.Resource AS MODULE
                                           ,RChild.ResourceTypeID
                                           ,RChild.ChildTypeId
                                  FROM      syResources RChild
                                  INNER JOIN syNavigationNodes NNChild ON RChild.ResourceID = NNChild.ResourceId
                                  INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                                  INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                                  LEFT OUTER JOIN (
                                                    SELECT  *
                                                    FROM    syResources
                                                    WHERE   ResourceTypeID = 1
                                                  ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                                  WHERE     RChild.ResourceTypeID IN ( 2,3,8 )
                                            AND (
                                                  RChild.ChildTypeId IS NULL
                                                  OR RChild.ChildTypeId = 3
                                                )
                                            AND ( NNParent.ParentId IN ( SELECT HierarchyId
                                                                         FROM   syNavigationNodes
                                                                         WHERE  ResourceId = 396
                                                                                AND ParentId IN ( SELECT    HierarchyId
                                                                                                  FROM      syNavigationNodes
                                                                                                  WHERE     ResourceId = 192 ) ) )
                                  UNION
                                  SELECT DISTINCT
                                            RChild.ResourceID AS ChildResourceId
                                           ,RChild.Resource AS ChildResource
                                           ,RChild.ResourceURL AS ChildResourceURL
                                           ,CASE WHEN ( RChild.ResourceID IN ( 737 ) ) THEN NULL
                                                 ELSE RChild.ResourceID
                                            END AS ParentResourceId
                                           ,NULL AS ParentResource
                                           ,NULL AS MODULE
                                           ,RChild.ResourceTypeID
                                           ,RChild.ChildTypeId
                                  FROM      syResources RChild
                                  WHERE     RChild.ResourceID IN ( 737 )
									--DE8343
                                            AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
                                ) t1
                    ) t2
            WHERE   t2.ModuleResourceId = @ModuleResourceId
                    AND 
--(@TabId is NULL OR t2.TabId=@TabId)
                    t2.TabId = @tabid
					-- DE7545 Conversion pages not to be shown in Manage Security
                    AND ChildResourceId NOT IN ( 530,531,287,288 )
                    AND
			-- Hide resources based on Configuration Settings    
                    (
                      -- The following expression means if showross... is set to false, hide     
	 -- ResourceIds 541,542,532,534,535,538,543,539    
	 -- (Not False) OR (Condition)    
                      (
                        (
                          NOT LTRIM(RTRIM(@ShowRossOnlyTabs)) = 0
                        )
                        OR ( ChildResourceId NOT IN ( 541,542,532,538,543 ) )
                      )
                      AND    
	 -- The following expression means if showross... is set to true, hide     
	 -- ResourceIds 142,375,330,476,508,102,107,237    
	 -- (Not True) OR (Condition)    
                      (
                        (
                          NOT LTRIM(RTRIM(@ShowRossOnlyTabs)) = 1
                        )
                        OR ( ChildResourceId NOT IN ( 142,375,330,476,508,102,107,237,230,286,200,217,290 ) )
                      )
                      AND    
	 ---- The following expression means if SchedulingMethod=regulartraditional, hide     
	 ---- ResourceIds 91 and 497    
	 ---- (Not True) OR (Condition)    
                      (
                        (
                          NOT LOWER(LTRIM(RTRIM(@SchedulingMethod))) = 'regulartraditional'
                        )
                        OR ( ChildResourceId NOT IN ( 91,497 ) )
                      )
                      AND    
	 -- The following expression means if TrackSAPAttendance=byday, hide     
	 -- ResourceIds 633    
	 -- (Not True) OR (Condition)    
                      (
                        (
                          NOT LOWER(LTRIM(RTRIM(@TrackSAPAttendance))) = 'byday'
                        )
                        OR ( ChildResourceId NOT IN ( 327 ) )
                      )
                      AND (
                            (
                              NOT LOWER(LTRIM(RTRIM(@TrackSAPAttendance))) = 'byclass'
                            )
                            OR ( ChildResourceId NOT IN ( 539 ) )
                          )
                      AND  
	 ---- The following expression means if @ShowCollegeOfCourtReporting is set to false, hide     
	 ---- ResourceIds 614,615    
  ---- (Not False) OR (Condition)    
                      (
                        (
                          NOT LOWER(LTRIM(RTRIM(@ShowCollegeOfCourtReporting))) = 'no'
                        )
                        OR ( ChildResourceId NOT IN ( 614,615 ) )
                      )
                      AND    
	 -- The following expression means if @ShowCollegeOfCourtReporting is set to true, hide     
	 -- ResourceIds 497    
	 -- (Not True) OR (Condition)    
                      (
                        (
                          NOT LOWER(LTRIM(RTRIM(@ShowCollegeOfCourtReporting))) = 'yes'
                        )
                        OR ( ChildResourceId NOT IN ( 497 ) )
                      )
                      AND    
	 -- The following expression means if FAMEESP is set to false, hide     
	 -- ResourceIds 517,523, 525    
	 -- (Not False) OR (Condition)    
                      (
                        (
                          NOT LOWER(LTRIM(RTRIM(@FameESP))) = 'no'
                        )
                        OR ( ChildResourceId NOT IN ( 517,523,525 ) )
                      )
                      AND    
	 ---- The following expression means if EDExpress is set to false, hide     
	 ---- ResourceIds 603,604,606,619    
	 ---- (Not False) OR (Condition)    
                      (
                        (
                          NOT LOWER(LTRIM(RTRIM(@EdExpress))) = 'no'
                        )
                        OR ( ChildResourceId NOT IN ( 603,604,605,606,619 ) )
                      )
                      AND    
	 ---- The following expression means if @GradeBookWeightingLevel is set to courselevel, hide     
	 ---- ResourceIds 107,96,222    
	 ---- (Not False) OR (Condition)    
                      (
                        (
                          NOT LOWER(LTRIM(RTRIM(@GradeBookWeightingLevel))) = 'courselevel'
                        )
                        OR ( ChildResourceId NOT IN ( 107,96,222 ) )
                      )
                      AND (
                            (
                              NOT LOWER(LTRIM(RTRIM(@GradeBookWeightingLevel))) = 'instructorlevel'
                            )
                            OR ( ChildResourceId NOT IN ( 476 ) )
                          )
                      AND (
                            (
                              NOT LOWER(LTRIM(RTRIM(@ShowExternshipTabs))) = 'no'
                            )  
						--OR ( ChildResourceId NOT IN ( 543, 538 ) )  
                            OR ( ChildResourceId NOT IN ( 543 ) )
                          )
                    )
            ORDER BY GroupSortOrder
                   ,ParentResourceId
                   ,ChildResource;
        END;



GO
