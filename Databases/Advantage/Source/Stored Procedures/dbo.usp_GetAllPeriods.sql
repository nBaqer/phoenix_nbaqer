SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_GetAllPeriods]
    @showActiveOnly BIT
   ,@campusId VARCHAR(50)
AS
    SET NOCOUNT ON;
    IF @showActiveOnly = 1
        BEGIN
            EXEC usp_GetAllActivePeriods @campusId;
        END;
    ELSE
        BEGIN
            EXEC usp_GetAllActiveAndInActivePeriods @campusId;
        END;





GO
