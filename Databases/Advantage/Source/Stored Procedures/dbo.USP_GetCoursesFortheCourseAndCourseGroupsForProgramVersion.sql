SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


  
CREATE PROCEDURE [dbo].[USP_GetCoursesFortheCourseAndCourseGroupsForProgramVersion]
    (
     @prgVerID UNIQUEIDENTIFIER
    ,@UnitTypeId UNIQUEIDENTIFIER  
    )
AS
    SET NOCOUNT ON;  
    DECLARE @rtn INTEGER;  
    DECLARE @VariableReqtable TABLE
        (
         ReqId UNIQUEIDENTIFIER
        ,GrpID UNIQUEIDENTIFIER
        ,ID INT IDENTITY
        );   
  
    INSERT  INTO @VariableReqtable
            SELECT  ReqId
                   ,GrpID
            FROM    arReqGrpDef
            WHERE   GrpID IN ( SELECT   t3.ReqId
                               FROM     arReqs t3
                               INNER JOIN arProgVerDef t4 ON t3.ReqId = t4.ReqId
                                                             AND t3.ReqTypeId = 2
                               INNER JOIN arPrgVersions t6 ON t4.Prgverid = t6.PrgVerID
                                                              AND t6.PrgVerID = @prgVerID );  
  
    DECLARE @MaxRowCounter INT
       ,@RowCounter INT;  
  
    SET @MaxRowCounter = (
                           SELECT   MAX(ID)
                           FROM     @VariableReqtable
                         );  
  
    SET @RowCounter = 1;  
  
    WHILE ( @RowCounter <= @MaxRowCounter )
        BEGIN  
  
            INSERT  INTO @VariableReqtable
                    SELECT  ReqId
                           ,GrpID
                    FROM    arReqGrpDef
                    WHERE   GrpID IN ( SELECT   ReqId
                                       FROM     @VariableReqtable
                                       WHERE    ID = @RowCounter );   
  
     
  
            SET @RowCounter = @RowCounter + 1;  
  
            SET @MaxRowCounter = (
                                   SELECT   MAX(ID)
                                   FROM     @VariableReqtable
                                 );  
  
        END;  
      
 
    SELECT  T.Descrip
    FROM    (
              SELECT   
  DISTINCT              dbo.arReqs.ReqId
                       ,dbo.arReqs.UnitTypeId
                       ,dbo.arReqs.Descrip  
    
  /**/
              FROM      arReqs
              INNER JOIN arReqGrpDef ON arReqs.ReqId = arReqGrpDef.reqid
                                        AND arReqs.ReqTypeId = 1
              INNER JOIN arAttUnitType ON dbo.arReqs.UnitTypeId = dbo.arAttUnitType.UnitTypeId
                                          AND dbo.arAttUnitType.UnitTypeId <> @UnitTypeId
                                          AND dbo.arAttUnitType.UnitTypeDescrip <> 'None'
                                          AND arReqs.reqid IN ( SELECT  ReqId
                                                                FROM    @VariableReqtable
                                                                UNION
                                                                SELECT  GrpID
                                                                FROM    @VariableReqtable )
              UNION
              SELECT    B.reqid
                       ,B.UnitTypeId
                       ,B.Descrip
              FROM      dbo.arProgVerDef A
                       ,arReqs B
                       ,dbo.arAttUnitType C
              WHERE     PrgVerId = @prgVerID
                        AND A.ReqId = B.ReqId
                        AND ReqTypeId = 1
                        AND B.UnitTypeId = C.UnitTypeId
                        AND C.UnitTypeId <> @UnitTypeId
                        AND C.UnitTypeDescrip <> 'None'
            ) T;  
             
    --SELECT  @rtn  



GO
