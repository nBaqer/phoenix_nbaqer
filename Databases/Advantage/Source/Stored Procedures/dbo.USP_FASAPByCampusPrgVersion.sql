SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_FASAPByCampusPrgVersion]
    (
     @CampId UNIQUEIDENTIFIER
    ,@ProgVerID UNIQUEIDENTIFIER = NULL
	)
AS
    IF @ProgVerID IS NULL
        BEGIN
            SELECT  DISTINCT
                    t1.StuEnrollId
            FROM    arStuEnrollments t1
            INNER JOIN arPrgVersions PV ON t1.PrgVerId = PV.PrgVerId
            INNER JOIN arSAP t4 ON t4.SapId = PV.FASAPId
            INNER JOIN syStatusCodes t2 ON t1.StatusCodeId = t2.StatusCodeId
            INNER JOIN sySysStatus t3 ON t3.statusId = t2.StatusId
            INNER JOIN syCmpGrpCmps t5 ON t5.campusid = t1.campusid
            WHERE   t5.CampusId = @CampId
                    AND t1.StartDate <= GETDATE()
                    AND t3.SysStatusId IN ( 9,13,20,22 )
                    AND t4.FASAPPolicy = 1; 
        END;
    ELSE
        BEGIN          
            SELECT  DISTINCT
                    t1.StuEnrollId
            FROM    arStuEnrollments t1
            INNER JOIN arPrgVersions PV ON t1.PrgVerId = PV.PrgVerId
            INNER JOIN arSAP t4 ON t4.SapId = PV.FASAPId
            INNER JOIN syStatusCodes t2 ON t1.StatusCodeId = t2.StatusCodeId
            INNER JOIN sySysStatus t3 ON t3.statusId = t2.StatusId
            INNER JOIN syCmpGrpCmps t5 ON t5.campusid = t1.campusid
            WHERE   t5.CampusId = @CampId
                    AND t1.PrgVerId = @ProgVerID
                    AND t1.StartDate <= GETDATE()
                    AND t3.SysStatusId IN ( 9,13,20,22 )
                    AND t4.FASAPPolicy = 1; 
        END;





GO
