SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[USP_AR_GetPrereqClsSectsWOutStuEnrollid]
    (
     @PreReqID UNIQUEIDENTIFIER
    )
AS /*----------------------------------------------------------------------------------------------------
	Author          :	Saraswathi Lakshmanan
    
    Create date		:	02/03/2010
    
	Procedure Name	:	USP_AR_GetPrereqClsSectsWOutStuEnrollid

	Objective		:	To get the prerequisite Courses for a classsection 
	
	Parameters		:	Name			Type	Data Type			Required? 	
						=====			====	=========			=========	
						@PreReqID		In		UniqueIdentifier	Required	
						
*/-----------------------------------------------------------------------------------------------------

    BEGIN

        SELECT  b.PreCoReqId
               ,c.ClsSectionId
        FROM    arClassSections c
        INNER JOIN arCourseReqs b ON c.ReqId = b.PreCoReqId
        WHERE   b.CourseReqTypId = 1
                AND b.PreCoReqId = @PreReqID; 

    END;



GO
