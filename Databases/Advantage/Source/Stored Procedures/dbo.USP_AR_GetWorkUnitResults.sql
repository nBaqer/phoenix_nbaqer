SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[USP_AR_GetWorkUnitResults]
    (
     @StuEnrollId UNIQUEIDENTIFIER
    ,@ReqId UNIQUEIDENTIFIER
    )
AS /*----------------------------------------------------------------------------------------------------
	Author          :	Saraswathi Lakshmanan
    
    Create date		:	02/03/2010
    
	Procedure Name	:	USP_AR_GetWorkUnitResults

	Objective		:	get the Work Unit details
	
	Parameters		:	Name			Type	Data Type			Required? 	
						=====			====	=========			=========	
						@StuEnrollId	In		Uniqueidentifier	Required	
						@ReqID			In		Uniqueidentifier	Required
	
	Output			:	returns the work Unit details
	
	Calling procedure	: 	DoesStdHavePassingGrdForNumeric			
						
*/-----------------------------------------------------------------------------------------------------

/*--To fix issue 17548: ENH: All: Register Students Page is Taking Too Long to Populate Available Students 
This query is used get the work units
*/

    BEGIN
        SELECT  GBCR.ReqId
               ,GBCR.TermId
               ,GCT.Descrip
               ,GBCR.Score
               ,GBCR.MinResult
               ,GBCR.Required
               ,GBCR.MustPass
               ,( CASE WHEN Score > MinResult THEN NULL
                       WHEN Score = MinResult THEN 0
                       WHEN MinResult > Score THEN ( MinResult - Score )
                  END ) AS Remaining
        FROM    arGrdBkConversionResults GBCR
        INNER JOIN arGrdComponentTypes GCT ON GBCR.GrdComponentTypeId = GCT.GrdComponentTypeId
        WHERE   GBCR.StuEnrollId = @StuEnrollId
                AND ReqId = @ReqId
        UNION
        SELECT  ReqId
               ,TermId
               ,Descrip
               ,Score
               ,MinResult
               ,ISNULL(Required,0) AS Required
               ,ISNULL(MustPass,0) AS MustPass
               ,( CASE WHEN Score > MinResult THEN NULL
                       WHEN Score = MinResult THEN 0
                       WHEN MinResult > Score THEN ( MinResult - Score )
                  END ) AS Remaining
        FROM    (
                  SELECT    CS.ReqId
                           ,CS.TermId
                           ,GCT.Descrip
                           ,( CASE GCT.SysComponentTypeId
                                WHEN 544 THEN (
                                                SELECT  SUM(HoursAttended)
                                                FROM    arExternshipAttendance
                                                WHERE   StuEnrollId = @StuEnrollId
                                              )
                                ELSE GBRS.Score
                              END ) AS Score
                           ,( CASE GCT.SysComponentTypeId
                                WHEN 500 THEN GBWD.Number
                                WHEN 503 THEN GBWD.Number
                                ELSE (
                                       SELECT   MIN(MinVal)
                                       FROM     arGradeScaleDetails GSD
                                       INNER JOIN arGradeSystemDetails GSS ON GSD.GrdSysDetailId = GSS.GrdSysDetailId
                                       WHERE    GSS.IsPass = 1
                                     )
                              END ) AS MinResult
                           ,GBWD.Required
                           ,GBWD.MustPass
                  FROM      arGrdBkResults GBRS
                  INNER JOIN arClassSections CS ON GBRS.ClsSectionId = CS.ClsSectionId
                  INNER JOIN arGrdBkWgtDetails GBWD ON GBRS.InstrGrdBkWgtDetailId = GBWD.InstrGrdBkWgtDetailId
                  INNER JOIN arGrdComponentTypes GCT ON GBWD.GrdComponentTypeId = GCT.GrdComponentTypeId
                  WHERE     GBRS.StuEnrollId = @StuEnrollId
                            AND ReqId = @ReqId
                ) P; 

    END;



GO
