SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_GetAllActiveGradeScales] @campusId VARCHAR(50)
AS
    SET NOCOUNT ON;
    SELECT  T.GrdScaleId
           ,T.Descrip
           ,1 AS Status
    FROM    arGradeScales T
           ,syStatuses ST
    WHERE   T.StatusId = ST.StatusId
            AND ST.Status = 'Active'
            AND (
                  T.CampGrpId IN ( SELECT   CampGrpId
                                   FROM     syCmpGrpCmps
                                   WHERE    CampusId = @campusId
                                            AND CampGrpId <> (
                                                               SELECT   CampGrpId
                                                               FROM     syCampGrps
                                                               WHERE    CampGrpDescrip = 'ALL'
                                                             ) )
                  OR CampGrpId = (
                                   SELECT   CampGrpId
                                   FROM     syCampGrps
                                   WHERE    CampGrpDescrip = 'ALL'
                                 )
                )
    ORDER BY Status
           ,T.Descrip; 





GO
