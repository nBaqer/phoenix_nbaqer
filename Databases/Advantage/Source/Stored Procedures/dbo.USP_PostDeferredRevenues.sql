SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_PostDeferredRevenues] @StrXML NTEXT
AS
    DECLARE @hDoc INT;         
        --Prepare input values as an XML document         
    EXEC sp_xml_preparedocument @hDoc OUTPUT,@StrXML;          

    INSERT  INTO dbo.saDeferredRevenues
            (
             DefRevenueId
            ,DefRevenueDate
            ,TransactionId
            ,Type
            ,Source
            ,Amount
            ,IsPosted
            ,ModUser
            ,ModDate
            )
            SELECT  NEWID()
                   ,DefRevenueDate
                   ,TransactionId
                   ,Type
                   ,Source
                   ,Amount
                   ,IsPosted
                   ,ModUser
                   ,ModDate
            FROM    OPENXML(@hDoc,'/dsDeferredRevenues/dtDeferredRevenues',1)         
                    WITH (DefRevenueId UNIQUEIDENTIFIER,
						  DefRevenueDate DATETIME,
						  TransactionId UNIQUEIDENTIFIER,
						  Type BIT,Source TINYINT,
						  Amount MONEY,
						  IsPosted BIT, 
						  ModUser VARCHAR(50),
						  ModDate DATETIME);

    EXEC sp_xml_removedocument @hDoc;
GO
