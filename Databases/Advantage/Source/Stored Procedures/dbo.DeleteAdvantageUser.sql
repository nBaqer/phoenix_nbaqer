SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--------------------------------------------------------------------------------------------------------------------------------
--END Troy: DE12613:Production - High - Advantage - MAU - Students not appearing on the IPEDS report
----------------------------------------------------------------------------------------------------------------------------------
-- ==========================================================================================================
-- Description:	Delete an Advantage User
-- Modified 3/25/2016 JGUIRADO introduce RETURN in each
--          if clause to end the procedure.
-- ==========================================================================================================
CREATE PROCEDURE [dbo].[DeleteAdvantageUser]
    @UserId UNIQUEIDENTIFIER
   ,@ResultId INT OUTPUT
   ,@ErrorId INT OUTPUT
AS
    BEGIN
	
        SET NOCOUNT ON;

        BEGIN TRAN DELETE_USER;

                DELETE  FROM syUsers
                WHERE   UserId = @UserId;
                IF @@ERROR <> 0
                    BEGIN 
                        ROLLBACK TRAN DELETE_USER;
                        SET @ResultId = 2;
                        SET @ErrorId = 2;
                        RETURN;
                    END; 
	
                DELETE  FROM TenantAuthDB.dbo.TenantUsers
                WHERE   UserId = @UserId;
                IF @@ERROR <> 0
                    BEGIN 
                        ROLLBACK TRAN DELETE_USER;
                        SET @ResultId = 3; 
                        SET @ErrorId = @@ERROR;
                        RETURN;
                    END;

                DELETE  FROM TenantAuthDB.dbo.aspnet_Profile
                WHERE   UserId = @UserId;
                IF @@ERROR <> 0
                    BEGIN 
                        ROLLBACK TRAN DELETE_USER;
                        SET @ResultId = 4; 
                        SET @ErrorId = @@ERROR;
                        RETURN;
                    END;
	
                DELETE  FROM TenantAuthDB.dbo.aspnet_UsersInRoles
                WHERE   UserId = @UserId;
                IF @@ERROR <> 0
                    BEGIN 
                        ROLLBACK TRAN DELETE_USER;
                        SET @ResultId = 5; 
                        SET @ErrorId = @@ERROR;
                        RETURN;
                    END;

                DELETE  FROM TenantAuthDB.dbo.aspnet_PersonalizationPerUser
                WHERE   UserId = @UserId;
                IF @@ERROR <> 0
                    BEGIN 
                        ROLLBACK TRAN DELETE_USER;
                        SET @ResultId = 6; 
                        SET @ErrorId = @@ERROR;
                        RETURN;
                    END;

                DELETE  FROM TenantAuthDB.dbo.aspnet_Membership
                WHERE   UserId = @UserId;
                IF @@ERROR <> 0
                    BEGIN 
                        ROLLBACK TRAN DELETE_USER;
                        SET @ResultId = 7; 
                        SET @ErrorId = @@ERROR;
                        RETURN;
                    END;
	
                DELETE  FROM TenantAuthDB.dbo.aspnet_Users
                WHERE   UserId = @UserId;
                IF @@ERROR <> 0
                    BEGIN 
                        ROLLBACK TRAN DELETE_USER;
                        SET @ResultId = 8; 
                        SET @ErrorId = @@ERROR;
                        RETURN;
                    END;

                COMMIT TRAN DELETE_USER;

                SET @ResultId = 1;


            END;



GO
