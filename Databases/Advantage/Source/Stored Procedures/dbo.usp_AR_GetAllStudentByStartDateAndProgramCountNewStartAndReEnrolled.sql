SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_AR_GetAllStudentByStartDateAndProgramCountNewStartAndReEnrolled]
    @StartDate DATETIME
   ,@CampGrpId AS VARCHAR(8000)
   ,@ProgVerId AS VARCHAR(8000) = NULL
AS /*----------------------------------------------------------------------------------------------------
    Author : Vijay Ramteke
    
    Create date : 09/13/2010
    
    Procedure Name : usp_AR_GetAllStudentByStartDateAndProgramCountNewStartAndReEnrolled

    Objective : Get All The Student New Start And Reenrolled Count By Start Date And Program Version
    
    Parameters : Name Type Data Type Required? 
    
    Output : Returns All The Student New Start And Reenrolled Count By Start Date And Program Version for Report Dataset
                        
*/-----------------------------------------------------------------------------------------------------

    BEGIN

        SELECT  S.StudentId
               ,SE.PrgVerId
               ,SE.CampusId
        INTO    #TempStudent
        FROM    arStudent S
               ,arStuEnrollments SE
               ,syCmpGrpCmps E
               ,syCampuses F
               ,syCampGrps
        WHERE   S.StudentId = SE.StudentId
                AND CAST(SE.StartDate AS DATE) = CAST(@StartDate AS DATE)
                AND (
                      SE.PrgVerId IN ( SELECT   strval
                                       FROM     dbo.SPLIT(@ProgVerId) )
                      OR @ProgVerId IS NULL
                    )
                AND SE.CampusId IN ( SELECT DISTINCT
                                            t1.CampusId
                                     FROM   syCmpGrpCmps t1
                                     WHERE  t1.CampGrpId IN ( SELECT    strval
                                                              FROM      dbo.SPLIT(@CampGrpId) ) )
                AND SE.CampusId = F.CampusId
                AND F.CampusId = E.CampusId
                AND E.CampGrpId = syCampGrps.CampGrpId
                AND syCampGrps.CampGrpId IN ( SELECT DISTINCT
                                                        t1.CampGrpId
                                              FROM      syCmpGrpCmps t1
                                              WHERE     t1.CampGrpId IN ( SELECT    strval
                                                                          FROM      dbo.SPLIT(@CampGrpId) ) );


        SELECT  COUNT(SE.StudentId) AS ReEnrollCount
               ,SE.StudentId
               ,SE.PrgVerId
               ,SE.CampusId
        INTO    #TempReEnrollCount
        FROM    arStuEnrollments SE
               ,#TempStudent TS
               ,syCmpGrpCmps E
               ,syCampuses F
               ,syCampGrps
        WHERE   TS.StudentId = SE.StudentId
                AND TS.PrgVerId = SE.PrgVerId
                AND (
                      SE.PrgVerId IN ( SELECT   strval
                                       FROM     dbo.SPLIT(@ProgVerId) )
                      OR @ProgVerId IS NULL
                    )
                AND CAST(SE.StartDate AS DATE) <= CAST(@StartDate AS DATE)
                AND SE.CampusId = F.CampusId
                AND F.CampusId = E.CampusId
                AND E.CampGrpId = syCampGrps.CampGrpId
                AND syCampGrps.CampGrpId IN ( SELECT DISTINCT
                                                        t1.CampGrpId
                                              FROM      syCmpGrpCmps t1
                                              WHERE     t1.CampGrpId IN ( SELECT    strval
                                                                          FROM      dbo.SPLIT(@CampGrpId) ) )
        GROUP BY SE.PrgVerId
               ,SE.StudentId
               ,SE.CampusId
        HAVING  COUNT(SE.StudentId) > 1;



        SELECT  S.StudentId
               ,SE.PrgVerId
               ,SE.CampusId
        INTO    #TempStudentReEnroll
        FROM    arStudent S
               ,arStuEnrollments SE
               ,syCmpGrpCmps E
               ,syCampuses F
               ,syCampGrps
        WHERE   S.StudentId = SE.StudentId
                AND CAST(SE.StartDate AS DATE) = CAST(@StartDate AS DATE)
                AND (
                      SE.PrgVerId IN ( SELECT   strval
                                       FROM     dbo.SPLIT(@ProgVerId) )
                      OR @ProgVerId IS NULL
                    )
                AND SE.CampusId IN ( SELECT DISTINCT
                                            t1.CampusId
                                     FROM   syCmpGrpCmps t1
                                     WHERE  t1.CampGrpId IN ( SELECT    strval
                                                              FROM      dbo.SPLIT(@CampGrpId) ) )
                AND SE.ReEnrollmentDate IS NOT NULL
                AND SE.StudentId NOT IN ( SELECT    StudentId
                                          FROM      #TempReEnrollCount )
                AND SE.CampusId = F.CampusId
                AND F.CampusId = E.CampusId
                AND E.CampGrpId = syCampGrps.CampGrpId
                AND syCampGrps.CampGrpId IN ( SELECT DISTINCT
                                                        t1.CampGrpId
                                              FROM      syCmpGrpCmps t1
                                              WHERE     t1.CampGrpId IN ( SELECT    strval
                                                                          FROM      dbo.SPLIT(@CampGrpId) ) );

        DECLARE @NewStartCount INT
           ,@ReEnrollCount INT;

        SELECT  SUM(CountNew) AS CountNew
               ,SUM(CountReenrolls) AS CountReenrolls
               ,PrgVerId
               ,CampusId
        FROM    (
                  SELECT    PrgVerId
                           ,CampusId
                           ,CountNew - CountReenrolls AS CountNew
                           ,CountReenrolls
                  FROM      (
                              SELECT    COUNT(StudentId) AS CountNew
                                       ,0 AS CountReenrolls
                                       ,PrgVerId
                                       ,CampusId
                              FROM      #TempStudent
                              GROUP BY  PrgVerId
                                       ,CampusId
                              UNION ALL
                              SELECT    0 AS CountNew
                                       ,COUNT(ReEnrollCount) AS CountReenrolls
                                       ,PrgVerId
                                       ,CampusId
                              FROM      #TempReEnrollCount
                              GROUP BY  PrgVerId
                                       ,CampusId
                                       ,ReEnrollCount
                              UNION ALL
                              SELECT    0 AS CountNew
                                       ,COUNT(StudentId) AS CountReenrolls
                                       ,PrgVerId
                                       ,CampusId
                              FROM      #TempStudentReEnroll
                              GROUP BY  PrgVerId
                                       ,CampusId
                            ) tt
                ) ttt
        GROUP BY PrgVerId
               ,CampusId
        ORDER BY PrgVerId;


        DROP TABLE #TempStudent;
        DROP TABLE #TempReEnrollCount;
        DROP TABLE #TempStudentReEnroll;


    END;




GO
