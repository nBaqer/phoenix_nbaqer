SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[USP_AR_DefaultCharges_Delete]
    (
     @PrgVerPmtId UNIQUEIDENTIFIER
    )
AS /*----------------------------------------------------------------------------------------------------
	Author          :	Saraswathi Lakshmanan
    
    Create date		:	05/05/2010
    
	Procedure Name	:	[USP_AR_DefaultCharges_Delete]

	Objective		:	deletes record from table saPrgVerDefaultChargePeriods
	
	Parameters		:	Name				Type	Data Type				Required? 	
						=====				====	=========				=========	
						@PrgVerPmtId		In		UniqueIDENTIFIER		Required
						
	Output			:			
						
*/-----------------------------------------------------------------------------------------------------

    BEGIN

        DELETE  saPrgVerDefaultChargePeriods
        WHERE   PrgVerPmtId = @PrgVerPmtId;
		 
    END;




GO
