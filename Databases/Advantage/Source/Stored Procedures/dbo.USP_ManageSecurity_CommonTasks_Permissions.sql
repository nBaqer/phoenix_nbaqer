SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-----------------------------------------------------------------------------------------  
-- US3331 Ability to add UDF as param and required fields in adhoc reports.  
--End - script added by Theresa G on 9/21/12  
-----------------------------------------------------------------------------------------  
  
------------------------------------------------------------------------------------------------------------------------------  
-- US3272 - Security : Assign permissions for users to Post Applicant Fees - START - GM - 09/21/2012  
------------------------------------------------------------------------------------------------------------------------------  
------------------------------------------------------------------------------------------------------------------------------  
---DE4503 QA: Bug:We need to show only the student pages that are relevant to the client. Theresa G 06/01/2012 End  
------------------------------------------------------------------------------------------------------------------------------  
------------------------------------------------------------------------------------------------------------------------------  
---DE7659 QA:  Issue on the Manage Security page with the listing of pages. Geetha M 06/04/2012 Start. 
------------------------------------------------------------------------------------------------------------------------------  
------------------------------------------------------------------------------------------------------------------------------  
--USP_ManageSecurity_CommonTasks_Permissions  
------------------------------------------------------------------------------------------------------------------------------  
  
CREATE PROCEDURE [dbo].[USP_ManageSecurity_CommonTasks_Permissions]  
    @RoleId VARCHAR(50)  
   ,@ModuleResourceId INT  
   ,  
   -- DE7659  
    @SchoolEnumerator INT  
   ,@CampusId UNIQUEIDENTIFIER  
AS --DECLARE @RoleId UNIQUEIDENTIFIER  
--SET @RoleId='BB0E90D6-4D77-4362-95FA-B2D34260A4A1'  
--'1d1eab90-4260-4803-b5e5-0054f85de550'  
--194  
-- declare local variables  
 -- DE7659  
 --DECLARE @CampusId UNIQUEIDENTIFIER  
   
    DECLARE @ShowRossOnlyTabs BIT  
       ,@SchedulingMethod VARCHAR(50)  
       ,@TrackSAPAttendance VARCHAR(50);  
    DECLARE @ShowCollegeOfCourtReporting VARCHAR(5)  
       ,@FameESP VARCHAR(5)  
       ,@EdExpress VARCHAR(5);   
    DECLARE @GradeBookWeightingLevel VARCHAR(20)  
       ,@ShowExternshipTabs VARCHAR(5);  
-- Get Values  
    IF (  
         SELECT COUNT(*)  
         FROM   syConfigAppSetValues  
         WHERE  SettingId = 68  
                AND CampusId = @CampusId  
       ) >= 1  
        BEGIN  
            SET @ShowRossOnlyTabs = (  
                                      SELECT    Value  
                                      FROM      dbo.syConfigAppSetValues  
                                      WHERE     SettingId = 68  
                                                AND CampusId = @CampusId  
                                    );    
          
        END;   
    ELSE  
        BEGIN  
            SET @ShowRossOnlyTabs = (  
                                      SELECT    Value  
                                      FROM      dbo.syConfigAppSetValues  
                                      WHERE     SettingId = 68  
                                                AND CampusId IS NULL  
                                    );  
        END;  
   
   
   
    IF (  
         SELECT COUNT(*)  
         FROM   syConfigAppSetValues  
         WHERE  SettingId = 65  
                AND CampusId = @CampusId  
       ) >= 1  
        BEGIN  
            SET @SchedulingMethod = (  
                                      SELECT    Value  
                                      FROM      dbo.syConfigAppSetValues  
                                      WHERE     SettingId = 65  
                                                AND CampusId = @CampusId  
                                    );    
          
        END;   
    ELSE  
        BEGIN  
            SET @SchedulingMethod = (  
                                      SELECT    Value  
                                      FROM      dbo.syConfigAppSetValues  
                                      WHERE     SettingId = 65  
                                                AND CampusId IS NULL  
                                    );  
        END;                        
          
   
            
    IF (  
         SELECT COUNT(*)  
         FROM   syConfigAppSetValues  
         WHERE  SettingId = 72  
                AND CampusId = @CampusId  
       ) >= 1  
        BEGIN  
            SET @TrackSAPAttendance = (  
                                        SELECT  Value  
                                        FROM    dbo.syConfigAppSetValues  
                                        WHERE   SettingId = 72  
                                                AND CampusId = @CampusId  
                                      );    
          
        END;   
    ELSE  
        BEGIN  
            SET @TrackSAPAttendance = (  
                                        SELECT  Value  
                                        FROM    dbo.syConfigAppSetValues  
                                        WHERE   SettingId = 72  
                                                AND CampusId IS NULL  
                                      );  
        END;   
      
               
    IF (  
         SELECT COUNT(*)  
         FROM   syConfigAppSetValues  
         WHERE  SettingId = 118  
                AND CampusId = @CampusId  
       ) >= 1  
        BEGIN  
            SET @ShowCollegeOfCourtReporting = (  
                                                 SELECT Value  
                                                 FROM   dbo.syConfigAppSetValues  
                                                 WHERE  SettingId = 118  
                                                        AND CampusId = @CampusId  
                                               );    
          
        END;   
    ELSE  
        BEGIN  
            SET @ShowCollegeOfCourtReporting = (  
                                                 SELECT Value  
                                                 FROM   dbo.syConfigAppSetValues  
                                                 WHERE  SettingId = 118  
                                                        AND CampusId IS NULL  
                                               );  
        END;   
    
          
    IF (  
         SELECT COUNT(*)  
         FROM   syConfigAppSetValues  
         WHERE  SettingId = 37  
                AND CampusId = @CampusId  
       ) >= 1  
        BEGIN  
            SET @FameESP = (  
                             SELECT Value  
                             FROM   dbo.syConfigAppSetValues  
                             WHERE  SettingId = 37  
                                    AND CampusId = @CampusId  
                           );    
          
        END;   
    ELSE  
        BEGIN  
            SET @FameESP = (  
                             SELECT Value  
                             FROM   dbo.syConfigAppSetValues  
                             WHERE  SettingId = 37  
                                    AND CampusId IS NULL  
                           );  
        END;   
    
         
    IF (  
         SELECT COUNT(*)  
         FROM   syConfigAppSetValues  
         WHERE  SettingId = 91  
                AND CampusId = @CampusId  
       ) >= 1  
        BEGIN  
            SET @EdExpress = (  
                               SELECT   Value  
                               FROM     dbo.syConfigAppSetValues  
                               WHERE    SettingId = 91  
                                        AND CampusId = @CampusId  
                             );    
          
        END;   
    ELSE  
        BEGIN  
            SET @EdExpress = (  
                               SELECT   Value  
                               FROM     dbo.syConfigAppSetValues  
                               WHERE    SettingId = 91  
                                        AND CampusId IS NULL  
                             );  
        END;  
    
             
              
    IF (  
         SELECT COUNT(*)  
         FROM   syConfigAppSetValues  
         WHERE  SettingId = 43  
                AND CampusId = @CampusId  
       ) >= 1  
        BEGIN  
            SET @GradeBookWeightingLevel = (  
                                             SELECT Value  
                                             FROM   dbo.syConfigAppSetValues  
                                             WHERE  SettingId = 43  
                                                    AND CampusId = @CampusId  
                                           );    
          
        END;   
    ELSE  
        BEGIN  
            SET @GradeBookWeightingLevel = (  
                                             SELECT Value  
                                             FROM   dbo.syConfigAppSetValues  
                                             WHERE  SettingId = 43  
                                                    AND CampusId IS NULL  
                                           );  
        END;  
      
    PRINT @GradeBookWeightingLevel;                           
            
    IF (  
         SELECT COUNT(*)  
         FROM   syConfigAppSetValues  
         WHERE  SettingId = 71  
                AND CampusId = @CampusId  
       ) >= 1  
        BEGIN  
            SET @ShowExternshipTabs = (  
                                        SELECT  Value  
                                        FROM    dbo.syConfigAppSetValues  
                                        WHERE   SettingId = 71  
                                                AND CampusId = @CampusId  
                                      );    
          
        END;   
    ELSE  
        BEGIN  
            SET @ShowExternshipTabs = (  
                                        SELECT  Value  
                                        FROM    dbo.syConfigAppSetValues  
                                        WHERE   SettingId = 71  
                                                AND CampusId IS NULL  
                                      );  
        END;  
  
            
            
    SELECT  *  
           ,CASE WHEN AccessLevel = 15 THEN 1  
                 ELSE 0  
            END AS FullPermission  
           ,CASE WHEN AccessLevel IN ( 14,13,12,11,10,9,8 ) THEN 1  
                 ELSE 0  
            END AS EditPermission  
           ,CASE WHEN AccessLevel IN ( 14,13,12,7,6,5,4 ) THEN 1  
                 ELSE 0  
            END AS AddPermission  
           ,CASE WHEN AccessLevel IN ( 14,11,10,7,6,2 ) THEN 1  
                 ELSE 0  
            END AS DeletePermission  
           ,CASE WHEN AccessLevel IN ( 13,11,9,7,5,3,1 ) THEN 1  
                 ELSE 0  
            END AS ReadPermission  
           ,NULL AS TabId  
    FROM    (  
              --Admissions  
     SELECT    189 AS ModuleResourceId  
                       ,ResourceID AS ChildResourceId  
                       ,Resource AS ChildResource  
                       ,(  
                          SELECT    AccessLevel  
                          FROM      syRlsResLvls  
                          WHERE     RoleId = @RoleId  
                                    AND ResourceID = syResources.ResourceID  
                        ) AS AccessLevel  
                       ,ResourceURL AS ChildResourceURL  
                       ,NULL AS ParentResourceId  
                       ,NULL AS ParentResource  
                       ,1 AS GroupSortOrder  
                       ,NULL AS FirstSortOrder  
                       ,NULL AS DisplaySequence  
                       ,ResourceTypeID  
              FROM      syResources  
              WHERE     ResourceID = 189  
              UNION  
              SELECT    189 AS ModuleResourceId  
                       ,NNChild.ResourceId AS ChildResourceId  
                       ,CASE WHEN NNChild.ResourceId = 395 THEN 'Lead Tabs'  
                             ELSE CASE WHEN NNChild.ResourceId = 398 THEN 'Add/View Leads'  
                  ELSE RChild.Resource  
                                  END  
                        END AS ChildResource  
                       ,(  
                          SELECT    AccessLevel  
                          FROM      syRlsResLvls  
                          WHERE     RoleId = @RoleId  
                                    AND ResourceID = NNChild.ResourceId  
                        ) AS AccessLevel  
                       ,RChild.ResourceURL AS ChildResourceURL  
                       ,CASE WHEN NNParent.ResourceId IN ( 687,688,689 ) THEN NULL  
                             ELSE NNParent.ResourceId  
                        END AS ParentResourceId  
                       ,RParent.Resource AS ParentResource  
                       ,CASE WHEN (  
                                    NNChild.ResourceId = 398  
                                    OR NNParent.ResourceId = 398  
                                  ) THEN 2  
                             ELSE CASE WHEN (  
                                              NNChild.ResourceId = 790  --US3272  
                                              OR NNParent.ResourceId = 790  
                                            ) THEN 3  
                                       ELSE 4  
                                  END  
                        END AS GroupSortOrder  
                       ,CASE WHEN (  
                                    NNChild.ResourceId = 694  
                                    OR NNParent.ResourceId = 694  
                                  ) THEN 2  
                             ELSE 3  
                        END AS FirstSortOrder  
                       ,CASE WHEN (  
                                    NNChild.ResourceId IN ( 398 )  
                                    OR NNParent.ResourceId IN ( 398 )  
                                  ) THEN 1  
                             ELSE CASE WHEN (  
                                              NNChild.ResourceId IN ( 790 )  --US3272  
                                              OR NNParent.ResourceId IN ( 790 )  
                                            ) THEN 2  
                                       ELSE 3  
                                  END  
                        END AS DisplaySequence  
                       ,RChild.ResourceTypeID  
 --NNChild.HierarchyIndex AS SecondSortOrder,  
 --RParentModule.Resource AS Module  
              FROM      syResources RChild  
              INNER JOIN syNavigationNodes NNChild ON RChild.ResourceID = NNChild.ResourceId  
              INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId  
              INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID  
              LEFT OUTER JOIN (  
                                SELECT  *  
                                FROM    syResources  
                                WHERE   ResourceTypeID = 1  
                              ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID  
              WHERE     RChild.ResourceTypeID IN ( 2,3,8 )  
                        AND (  
                              RChild.ChildTypeId IS NULL  
                              OR RChild.ChildTypeId = 3  
                            )  
                        AND ( RChild.ResourceID NOT IN ( 395 ) )  
                        AND ( NNParent.ResourceId NOT IN ( 395 ) )  
                        AND (  
                              NNParent.ParentId IN ( SELECT HierarchyId  
                                                     FROM   syNavigationNodes  
                                                     WHERE  ResourceId = 189 )  
                              OR NNChild.ParentId IN ( SELECT   HierarchyId  
                                                       FROM     syNavigationNodes  
                                                       WHERE    ResourceId IN ( 694,398,790 ) )   -- US3272  
                            )  
        --DE7659  
                        AND ( RChild.UsedIn & @SchoolEnumerator > 0 )  
-- Acdemic Records  
              UNION  
              SELECT    26 AS ModuleResourceId  
                       ,ResourceID AS ChildResourceId  
                       ,Resource AS ChildResource  
                       ,(  
                          SELECT DISTINCT  
                                    AccessLevel  
                          FROM      syRlsResLvls  
                          WHERE     RoleId = @RoleId  
                                    AND ResourceID = syResources.ResourceID  
                        ) AS AccessLevel  
                       ,ResourceURL AS ChildResourceURL  
                       ,NULL AS ParentResourceId  
                       ,NULL AS ParentResource  
                       ,1 AS GroupSortOrder  
                       ,NULL AS FirstSortOrder  
                       ,NULL AS DisplaySequence  
                       ,ResourceTypeID  
              FROM      syResources  
              WHERE     ResourceID = 26  
              UNION  
              SELECT    26 AS ModuleResourceId  
                       ,ChildResourceId  
                       ,ChildResource  
                       ,(  
                          SELECT DISTINCT  
                                    AccessLevel  
                          FROM      syRlsResLvls  
                          WHERE     RoleId = @RoleId  
                                    AND ResourceID = ChildResourceId  
                        ) AS AccessLevel  
                       ,ChildResourceURL  
                       ,ParentResourceId  
                       ,ParentResource  
                       ,CASE WHEN (  
                                    ChildResourceId = 190  
                                    OR ParentResourceId = 190  
                                  ) THEN 2  
                             ELSE CASE WHEN (  
                                              ChildResourceId = 129  
                                              OR ParentResourceId = 129  
                                            ) THEN 3  
                                       ELSE CASE WHEN (  
                                                        ChildResourceId = 71  
                                                        OR ParentResourceId = 71  
                                                      ) THEN 4  
                                                 ELSE CASE WHEN (  
                                                                  ChildResourceId = 693  
                                                                  OR ParentResourceId = 693  
                                                                ) THEN 5  
                                                           ELSE CASE WHEN (  
                                                                            ChildResourceId = 160  
                                                                            OR ParentResourceId = 160  
                                                                          ) THEN 6  
                                                                     ELSE CASE WHEN (  
                                                                                      ChildResourceId = 132  
                                                                                      OR ParentResourceId = 132  
                                                                                    ) THEN 7  
                                                                               ELSE 8  
                                                                          END  
                                                                END  
                                                      END  
                                            END  
                                  END  
                        END AS GroupSortOrder  
                       ,(  
                          SELECT TOP 1  
                                    HierarchyIndex  
                          FROM      dbo.syNavigationNodes  
                    WHERE     ResourceId = ChildResourceId  
                        ) AS FirstSortOrder  
                       ,CASE WHEN (  
                                    ChildResourceId IN ( 190,129,317,71 )  
                                    OR ParentResourceId IN ( 190,129,317,71 )  
                                  ) THEN 1  
                             ELSE 2  
                        END AS DisplaySequence  
                       ,ResourceTypeID  
              FROM      (  
                          SELECT DISTINCT  
                                    NNChild.ResourceId AS ChildResourceId  
                                   ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'  
                                         ELSE RChild.Resource  
                                    END AS ChildResource  
                                   ,RChild.ResourceURL AS ChildResourceURL  
                                   ,CASE WHEN NNParent.ResourceId = 687 THEN NULL  
                                         ELSE NNParent.ResourceId  
                                    END AS ParentResourceId  
                                   ,RParent.Resource AS ParentResource  
                                   ,RParentModule.Resource AS MODULE  
                                   ,RChild.ResourceTypeID  
    --NNParent.ParentId  
                          FROM      syResources RChild  
                          INNER JOIN syNavigationNodes NNChild ON RChild.ResourceID = NNChild.ResourceId  
                          INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId  
                          INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID  
                          LEFT OUTER JOIN (  
                                            SELECT  *  
                                            FROM    syResources  
                                            WHERE   ResourceTypeID = 1  
                                          ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID  
                          WHERE     RChild.ResourceTypeID IN ( 2,3,8 )  
                                    AND (  
                                          RChild.ChildTypeId IS NULL  
                                          OR RChild.ChildTypeId = 3  
                                        )  
                                    AND ( RChild.ResourceID <> 394 )  
                                    AND ( NNParent.ResourceId NOT IN ( 394 ) )  
                                    AND (  
                                          NNParent.ParentId IN ( SELECT HierarchyId  
                                                                 FROM   syNavigationNodes  
                                                                 WHERE  ResourceId = 26 )  
                                          OR NNChild.ParentId IN ( SELECT   HierarchyId  
                                                                   FROM     syNavigationNodes  
                                                                   WHERE    ResourceId IN ( 693,71,129,132,160,190,804,860) )  
                                        )  
         --DE7659  
                                    AND ( RChild.UsedIn & @SchoolEnumerator > 0 )  
                        ) t5  
--Student Accounts  
              UNION  
              SELECT    194 AS ModuleResourceId  
                       ,ResourceID AS ChildResourceId  
                       ,Resource AS ChildResource  
                       ,(  
                          SELECT    AccessLevel  
                          FROM      syRlsResLvls  
                          WHERE     RoleId = @RoleId  
                                    AND ResourceID = syResources.ResourceID  
                        ) AS AccessLevel  
                       ,ResourceURL AS ChildResourceURL  
                       ,NULL AS ParentResourceId  
                       ,NULL AS ParentResource  
                       ,1 AS GroupSortOrder  
           ,NULL AS FirstSortOrder  
                       ,NULL AS DisplaySequence  
                       ,ResourceTypeID  
              FROM      syResources  
              WHERE     ResourceID = 194  
              UNION  
              SELECT    194 AS ModuleResourceId  
                       ,ChildResourceId  
                       ,ChildResource  
                       ,(  
                          SELECT DISTINCT  
                                    AccessLevel  
                          FROM      syRlsResLvls  
                          WHERE     RoleId = @RoleId  
                                    AND ResourceID = ChildResourceId  
                        ) AS AccessLevel  
                       ,ChildResourceURL  
                       ,ParentResourceId  
                       ,ParentResource  
                       ,CASE WHEN (  
                                    ChildResourceId = 695  
                                    OR ParentResourceId = 695  
                                  ) THEN 2  
                             ELSE CASE WHEN (  
                                              ChildResourceId = 403  
                                              OR ParentResourceId = 403  
                                            ) THEN 3  
                                       ELSE 4  
                                  END  
                        END AS GroupSortOrder  
                       ,(  
                          SELECT TOP 1  
                                    HierarchyIndex  
                          FROM      dbo.syNavigationNodes  
                          WHERE     ResourceId = ChildResourceId  
                        ) AS FirstSortOrder  
                       ,CASE WHEN (  
                                    ChildResourceId IN ( 695 )  
                                    OR ParentResourceId IN ( 695 )  
                                  ) THEN 1  
                             ELSE 2  
                        END AS DisplaySequence  
                       ,ResourceTypeID  
              FROM      (  
                          SELECT DISTINCT  
                                    NNChild.ResourceId AS ChildResourceId  
                                   ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'  
                                         ELSE RChild.Resource  
                                    END AS ChildResource  
                                   ,RChild.ResourceURL AS ChildResourceURL  
                                   ,CASE WHEN NNParent.ResourceId IN ( 194 )  
                                              OR NNChild.ResourceId IN ( 695,403,698 ) THEN NULL  
                                         ELSE NNParent.ResourceId  
                                    END AS ParentResourceId  
                                   ,RParent.Resource AS ParentResource  
                                   ,RParentModule.Resource AS MODULE  
                                   ,RChild.ResourceTypeID  
    --NNParent.ParentId  
                          FROM      syResources RChild  
                          INNER JOIN syNavigationNodes NNChild ON RChild.ResourceID = NNChild.ResourceId  
                          INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId  
                          INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID  
                          LEFT OUTER JOIN (  
                                            SELECT  *  
                                            FROM    syResources  
                                            WHERE   ResourceTypeID = 1  
                                          ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID  
                          WHERE     RChild.ResourceTypeID IN ( 2,3,8 )  
                                    AND (  
                                          RChild.ChildTypeId IS NULL  
                                          OR RChild.ChildTypeId = 3  
                                        )  
                                 AND (  
                                          NNParent.ParentId IN ( SELECT HierarchyId  
                                                                 FROM   syNavigationNodes  
                                                                 WHERE  ResourceId = 194 )  
                                          OR NNChild.ParentId IN ( SELECT   HierarchyId  
                                                                   FROM     syNavigationNodes  
                                                                   WHERE    ResourceId IN ( 403,695,698 ) )  
                                        )  
                                    AND ( RChild.ResourceID <> 394 )  
                                    AND ( NNParent.ResourceId NOT IN ( 394 ) )  
         --DE7659  
                                    AND ( RChild.UsedIn & @SchoolEnumerator > 0 )  
                        ) t1   
--Faculty  
              UNION  
              SELECT    300 AS ModuleResourceId  
                       ,ResourceID AS ChildResourceId  
                       ,Resource AS ChildResource  
                       ,(  
                          SELECT    AccessLevel  
                          FROM      syRlsResLvls  
                          WHERE     RoleId = @RoleId  
                                    AND ResourceID = syResources.ResourceID  
                        ) AS AccessLevel  
                       ,ResourceURL AS ChildResourceURL  
                       ,NULL AS ParentResourceId  
                       ,NULL AS ParentResource  
                       ,1 AS GroupSortOrder  
                       ,NULL AS FirstSortOrder  
                       ,NULL AS DisplaySequence  
                       ,ResourceTypeID  
              FROM      syResources  
              WHERE     ResourceID = 300  
              UNION  
              SELECT    300 AS ModuleResourceId  
                       ,ChildResourceId  
                       ,ChildResource  
                       ,(  
                          SELECT DISTINCT  
                                    AccessLevel  
                          FROM      syRlsResLvls  
                          WHERE     RoleId = @RoleId  
                                    AND ResourceID = ChildResourceId  
                        ) AS AccessLevel  
                       ,ChildResourceURL  
                       ,ParentResourceId  
                       ,ParentResource  
                       ,CASE WHEN (  
                                    ChildResourceId = 697  
                                    OR ParentResourceId = 697  
                                  ) THEN 2  
                             ELSE 3  
                        END AS GroupSortOrder  
                       ,(  
                          SELECT TOP 1  
                                    HierarchyIndex  
                          FROM      dbo.syNavigationNodes  
                          WHERE     ResourceId = ChildResourceId  
                        ) AS FirstSortOrder  
                       ,1 AS DisplaySequence  
                       ,ResourceTypeID  
              FROM      (  
                          SELECT DISTINCT  
                                    NNChild.ResourceId AS ChildResourceId  
                                   ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'  
                                         ELSE RChild.Resource  
                                    END AS ChildResource  
                                   ,RChild.ResourceURL AS ChildResourceURL  
                                   ,CASE WHEN NNParent.ResourceId = 300  
                                              OR NNChild.ResourceId = 697 THEN NULL  
                                         ELSE NNParent.ResourceId  
                                    END AS ParentResourceId  
                                   ,RParent.Resource AS ParentResource  
                                   ,RParentModule.Resource AS MODULE  
                                   ,RChild.ResourceTypeID  
    --NNParent.ParentId  
                          FROM      syResources RChild  
                          INNER JOIN syNavigationNodes NNChild ON RChild.ResourceID = NNChild.ResourceId  
                          INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId  
                          INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID  
                          LEFT OUTER JOIN (  
                                            SELECT  *  
                                            FROM    syResources  
                                            WHERE   ResourceTypeID = 1  
                                          ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID  
                          WHERE     RChild.ResourceTypeID IN ( 2,3,8 )  
                                    AND (  
                                          RChild.ChildTypeId IS NULL  
                                          OR RChild.ChildTypeId = 3  
                                        )  
                                    AND ( RChild.ResourceID <> 394 )  
                                    AND ( NNParent.ResourceId NOT IN ( 394 ) )  
                                    AND (  
                                          NNParent.ParentId IN ( SELECT HierarchyId  
                                                                 FROM   syNavigationNodes  
                                                                 WHERE  ResourceId = 300 )  
                                          OR NNChild.ParentId IN ( SELECT   HierarchyId  
                                                                   FROM     syNavigationNodes  
                                                                   WHERE    ResourceId IN ( 697 ) )  
                                        )  
         --DE7659  
                                    AND ( RChild.UsedIn & @SchoolEnumerator > 0 )  
                        ) t3  
              UNION  
--Financial Aid  
              SELECT    191 AS ModuleResourceId  
                       ,ResourceID AS ChildResourceId  
                       ,Resource AS ChildResource  
                       ,(  
                          SELECT    AccessLevel  
                          FROM      syRlsResLvls  
                          WHERE     RoleId = @RoleId  
                                    AND ResourceID = syResources.ResourceID  
                        ) AS AccessLevel  
                       ,ResourceURL AS ChildResourceURL  
                       ,NULL AS ParentResourceId  
                       ,NULL AS ParentResource  
                       ,1 AS GroupSortOrder  
                       ,NULL AS FirstSortOrder  
                       ,NULL AS DisplaySequence  
                       ,ResourceTypeID  
              FROM      syResources  
              WHERE     ResourceID = 191  
              UNION  
              SELECT    191 AS ModuleResourceId  
                       ,ChildResourceId  
                       ,ChildResource  
                       ,(  
                          SELECT DISTINCT  
                                    AccessLevel  
                          FROM      syRlsResLvls  
                          WHERE     RoleId = @RoleId  
                                    AND ResourceID = ChildResourceId  
                        ) AS AccessLevel  
                       ,ChildResourceURL  
                       ,ParentResourceId  
                       ,ParentResource  
                       ,CASE WHEN (  
                                    ChildResourceId = 700  
                                    OR ParentResourceId = 700  
                                  ) THEN 2  
                             ELSE CASE WHEN (  
                                              ChildResourceId = 698  
                                              OR ParentResourceId = 698  
                                            ) THEN 3  
                                       ELSE 4  
                         END  
                        END AS GroupSortOrder  
                       ,(  
                          SELECT TOP 1  
                                    HierarchyIndex  
                          FROM      dbo.syNavigationNodes  
                          WHERE     ResourceId = ChildResourceId  
                        ) AS FirstSortOrder  
                       ,CASE WHEN (  
                                    ChildResourceId IN ( 698 )  
                                    OR ParentResourceId IN ( 698 )  
                                  ) THEN 1  
                             ELSE 2  
                        END AS DisplaySequence  
                       ,ResourceTypeID  
              FROM      (  
                          SELECT DISTINCT  
                                    NNChild.ResourceId AS ChildResourceId  
                                   ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'  
                                         ELSE RChild.Resource  
                                    END AS ChildResource  
                                   ,RChild.ResourceURL AS ChildResourceURL  
                                   ,CASE WHEN (  
                                                NNParent.ResourceId = 191  
                                                OR NNChild.ResourceId = 698  
                                                OR NNChild.ResourceId = 699  
                                                OR NNChild.ResourceId = 700  
                                              ) THEN NULL  
                                         ELSE NNParent.ResourceId  
                                    END AS ParentResourceId  
                                   ,RParent.Resource AS ParentResource  
                                   ,RParentModule.Resource AS MODULE  
                                   ,RChild.ResourceTypeID  
    --NNParent.ParentId  
                          FROM      syResources RChild  
                          INNER JOIN syNavigationNodes NNChild ON RChild.ResourceID = NNChild.ResourceId  
                          INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId  
                          INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID  
                          LEFT OUTER JOIN (  
                                            SELECT  *  
                                            FROM    syResources  
                                            WHERE   ResourceTypeID = 1  
                                          ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID  
                          WHERE     RChild.ResourceTypeID IN ( 2,3,8 )  
                                    AND (  
                                          RChild.ChildTypeId IS NULL  
                                          OR RChild.ChildTypeId = 3  
                                        )  
                                    AND ( RChild.ResourceID <> 394 )  
                                    AND ( NNParent.ResourceId NOT IN ( 394 ) )  
                                    AND (  
                                          NNParent.ParentId IN ( SELECT HierarchyId  
                                                                 FROM   syNavigationNodes  
                                                                 WHERE  ResourceId = 191 )  
                                          OR NNChild.ParentId IN ( SELECT   HierarchyId  
                                                                   FROM     syNavigationNodes  
                                                                   WHERE    ResourceId IN ( 700,698,699 ) )  
                                        )  
           --DE7659  
                                    AND ( RChild.UsedIn & @SchoolEnumerator > 0 )  
                        ) t4  
--Human Resources Starts Here  
              UNION  
              SELECT    192 AS ModuleResourceId  
                       ,ResourceID AS ChildResourceId  
                       ,Resource AS ChildResource  
                       ,(  
                          SELECT    AccessLevel  
                          FROM      syRlsResLvls  
                          WHERE     RoleId = @RoleId  
                                    AND ResourceID = syResources.ResourceID  
                        ) AS AccessLevel  
                       ,ResourceURL AS ChildResourceURL  
                       ,NULL AS ParentResourceId  
                       ,NULL AS ParentResource  
                       ,1 AS GroupSortOrder  
                       ,NULL AS FirstSortOrder  
                       ,NULL AS DisplaySequence  
                       ,ResourceTypeID  
              FROM      syResources  
              WHERE     ResourceID = 192  
              UNION  
              SELECT    192 AS ModuleResourceId  
                       ,NNChild.ResourceId AS ChildResourceId  
                       ,  
 --CASE WHEN NNChild.ResourceId=395 THEN 'Lead Tabs' ELSE   
 --CASE WHEN NNChild.ResourceId=398 THEN 'Add/View Leads' ELSE RChild.Resource END   
 --END  
                        RChild.Resource AS ChildResource  
                       ,(  
                          SELECT    AccessLevel  
                          FROM      syRlsResLvls  
                          WHERE     RoleId = @RoleId  
                                    AND ResourceID = NNChild.ResourceId  
                        ) AS AccessLevel  
                       ,RChild.ResourceURL AS ChildResourceURL  
                       ,CASE WHEN NNParent.ResourceId IN ( 687,688,689 ) THEN NULL  
                             ELSE NNParent.ResourceId  
                        END AS ParentResourceId  
                       ,RParent.Resource AS ParentResource  
                       ,CASE WHEN (  
                                    NNChild.ResourceId = 708  
                                    OR NNParent.ResourceId = 708  
                                  ) THEN 2  
                             ELSE 3  
                        END AS GroupSortOrder  
                       ,CASE WHEN (  
                                    NNChild.ResourceId = 708  
                                    OR NNParent.ResourceId = 708  
                                  ) THEN 2  
                             ELSE 3  
                        END AS FirstSortOrder  
                       ,  
 --CASE WHEN (NNChild.ResourceId IN (398) OR NNParent.ResourceId IN (398)) THEN 1 ELSE 2 END AS DisplaySequence,  
                        1 AS DisplaySequence  
                       ,RChild.ResourceTypeID  
 --NNChild.HierarchyIndex AS SecondSortOrder,  
 --RParentModule.Resource AS Module  
              FROM      syResources RChild  
              INNER JOIN syNavigationNodes NNChild ON RChild.ResourceID = NNChild.ResourceId  
              INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId  
              INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID  
              LEFT OUTER JOIN (  
                                SELECT  *  
                                FROM    syResources  
                                WHERE   ResourceTypeID = 1  
                              ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID  
              WHERE     RChild.ResourceTypeID IN ( 2,3,8 )  
                        AND (  
                              RChild.ChildTypeId IS NULL  
                              OR RChild.ChildTypeId = 3  
                            )  
                        AND ( RChild.ResourceID NOT IN ( 396 ) )  
                        AND ( NNParent.ResourceId NOT IN ( 396 ) )  
                        AND (  
                              NNParent.ParentId IN ( SELECT HierarchyId  
                                                     FROM   syNavigationNodes  
                                                     WHERE  ResourceId = 192 )  
                              OR NNChild.ParentId IN ( SELECT   HierarchyId  
                            FROM     syNavigationNodes  
                                                       WHERE    ResourceId IN ( 708 ) )  
                            )  
              UNION  
--Placement  
              SELECT    193 AS ModuleResourceId  
                       ,ResourceID AS ChildResourceId  
                       ,Resource AS ChildResource  
                       ,(  
                          SELECT    AccessLevel  
                          FROM      syRlsResLvls  
                          WHERE     RoleId = @RoleId  
                                    AND ResourceID = syResources.ResourceID  
                        ) AS AccessLevel  
                       ,ResourceURL AS ChildResourceURL  
                       ,NULL AS ParentResourceId  
                       ,NULL AS ParentResource  
                       ,1 AS GroupSortOrder  
                       ,NULL AS FirstSortOrder  
                       ,NULL AS DisplaySequence  
                       ,ResourceTypeID  
              FROM      syResources  
              WHERE     ResourceID = 193  
              UNION  
              SELECT    193 AS ModuleResourceId  
                       ,ChildResourceId  
                       ,ChildResource  
                       ,(  
                          SELECT DISTINCT  
                                    AccessLevel  
                          FROM      syRlsResLvls  
                          WHERE     RoleId = @RoleId  
                                    AND ResourceID = ChildResourceId  
                        ) AS AccessLevel  
                       ,ChildResourceURL  
                       ,ParentResourceId  
                       ,ParentResource  
                       ,CASE WHEN (  
                                    ChildResourceId = 696  
                                    OR ParentResourceId = 696  
                                  ) THEN 2  
                             ELSE 3  
                        END AS GroupSortOrder  
                       ,(  
                          SELECT TOP 1  
                                    HierarchyIndex  
                          FROM      dbo.syNavigationNodes  
                          WHERE     ResourceId = ChildResourceId  
                        ) AS FirstSortOrder  
                       ,CASE WHEN (  
                                    ChildResourceId IN ( 696 )  
                                    OR ParentResourceId IN ( 696 )  
                                  ) THEN 1  
                             ELSE 2  
                        END AS DisplaySequence  
                       ,ResourceTypeID  
              FROM      (  
                          SELECT DISTINCT  
                                    NNChild.ResourceId AS ChildResourceId  
                                   ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'  
                                         ELSE CASE WHEN NNChild.ResourceId = 397 THEN 'Employer Tabs'  
                                                   ELSE RChild.Resource  
                                              END  
                                    END AS ChildResource  
                                   ,RChild.ResourceURL AS ChildResourceURL  
                                   ,CASE WHEN (  
                                                NNParent.ResourceId = 193  
                                                OR NNChild.ResourceId IN ( 696,404 )  
                                              ) THEN NULL  
                                         ELSE NNParent.ResourceId  
                                    END AS ParentResourceId  
                                   ,RParent.Resource AS ParentResource  
                                   ,RParentModule.Resource AS MODULE  
                                   ,RChild.ResourceTypeID  
    --NNParent.ParentId  
                          FROM      syResources RChild  
                          INNER JOIN syNavigationNodes NNChild ON RChild.ResourceID = NNChild.ResourceId  
            INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId  
                          INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID  
                          LEFT OUTER JOIN (  
                                            SELECT  *  
                                            FROM    syResources  
                                            WHERE   ResourceTypeID = 1  
                                          ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID  
                          WHERE     RChild.ResourceTypeID IN ( 2,3,8 )  
                                    AND (  
                                          RChild.ChildTypeId IS NULL  
                                          OR RChild.ChildTypeId = 3  
                                        )  
                                    AND ( RChild.ResourceID <> 394 )  
                                    AND ( NNParent.ResourceId NOT IN ( 394,397 ) )  
                                    AND (  
                                          NNParent.ParentId IN ( SELECT HierarchyId  
                                                                 FROM   syNavigationNodes  
                                                                 WHERE  ResourceId = 193 )  
                                          OR NNChild.ParentId IN ( SELECT   HierarchyId  
                                                                   FROM     syNavigationNodes  
                                                                   WHERE    ResourceId IN ( 404,696 ) )  
                                        )  
           --DE7659  
                                    AND ( RChild.UsedIn & @SchoolEnumerator > 0 )  
                        ) t5  
     
--- DE7659 - Added for 2nd item in the defect - No item displayed in the System - common tasks                          
-- SYSTEM  
              UNION  
              SELECT    195 AS ModuleResourceId  
                       ,ResourceID AS ChildResourceId  
                       ,Resource AS ChildResource  
                       ,(  
                          SELECT    AccessLevel  
                          FROM      syRlsResLvls  
                          WHERE     RoleId = @RoleId  
                                    AND ResourceID = syResources.ResourceID  
                        ) AS AccessLevel  
                       ,ResourceURL AS ChildResourceURL  
                       ,NULL AS ParentResourceId  
                       ,NULL AS ParentResource  
                       ,1 AS GroupSortOrder  
                       ,NULL AS FirstSortOrder  
                       ,NULL AS DisplaySequence  
                       ,ResourceTypeID  
              FROM      syResources  
              WHERE     ResourceID = 195  
              UNION  
              SELECT    195 AS ModuleResourceId  
                       ,ChildResourceId  
                       ,ChildResource  
                       ,(  
                          SELECT DISTINCT  
                                    AccessLevel  
                          FROM      syRlsResLvls  
                          WHERE     RoleId = @RoleId  
                                    AND ResourceID = ChildResourceId  
                        ) AS AccessLevel  
                       ,ChildResourceURL  
                       ,ParentResourceId  
                       ,ParentResource  
                       ,CASE WHEN (  
                                    ChildResourceId = 701  
                                    OR ParentResourceId = 701  
                                  ) THEN 2  
                             ELSE 3  
                        END AS GroupSortOrder  
                       ,(  
                          SELECT TOP 1  
                                    HierarchyIndex  
                          FROM      dbo.syNavigationNodes  
                          WHERE     ResourceId = ChildResourceId  
                        ) AS FirstSortOrder  
    ,CASE WHEN (  
                                    ChildResourceId IN ( 701 )  
                                    OR ParentResourceId IN ( 701 )  
                                  ) THEN 1  
                             ELSE 2  
                        END AS DisplaySequence  
                       ,ResourceTypeID  
              FROM      (  
                          SELECT DISTINCT  
                                    NNChild.ResourceId AS ChildResourceId  
                                   ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'  
                                         ELSE RChild.Resource  
                                    END AS ChildResource  
                                   ,RChild.ResourceURL AS ChildResourceURL  
                                   ,CASE WHEN NNParent.ResourceId IN ( 195 )  
                                              OR NNChild.ResourceId IN ( 701,407 ) THEN NULL  
                                         ELSE NNParent.ResourceId  
                                    END AS ParentResourceId  
                                   ,RParent.Resource AS ParentResource  
                                   ,RParentModule.Resource AS MODULE  
                                   ,RChild.ResourceTypeID  
    --NNParent.ParentId  
                          FROM      syResources RChild  
                          INNER JOIN syNavigationNodes NNChild ON RChild.ResourceID = NNChild.ResourceId  
                          INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId  
                          INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID  
                          LEFT OUTER JOIN (  
                                            SELECT  *  
                                            FROM    syResources  
                                            WHERE   ResourceTypeID = 1  
                                          ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID  
                          WHERE     RChild.ResourceTypeID IN ( 2,3,8 )  
                                    AND (  
                                          RChild.ChildTypeId IS NULL  
                                          OR RChild.ChildTypeId = 3  
                                        )  
                                    AND (  
                                          NNParent.ParentId IN ( SELECT HierarchyId  
                                                                 FROM   syNavigationNodes  
                                                                 WHERE  ResourceId = 195 )  
                                          OR NNChild.ParentId IN ( SELECT   HierarchyId  
                                                                   FROM     syNavigationNodes  
                                                                   WHERE    ResourceId IN ( 701,407 ) )  
                                        )  
           --DE7659  
                                    AND ( RChild.UsedIn & @SchoolEnumerator > 0 )  
                        ) t6  
            ) t2  
    WHERE   t2.ModuleResourceId = @ModuleResourceId  
   -- DE7659  
            AND -- Hide resources based on Configuration Settings  
            (  
              -- The following expression means if showross... is set to false, hide     
  -- ResourceIds 541,542,532,534,535,538,543,539    
  -- (Not False) OR (Condition)    
      (  
                    (  
                      NOT LTRIM(RTRIM(@ShowRossOnlyTabs)) = 0  
                    )  
                    OR ( ChildResourceId NOT IN ( 541,542,532,534,535,538,543,539 ) )  
                  )  
              AND    
  -- The following expression means if showross... is set to true, hide     
  -- ResourceIds 142,375,330,476,508,102,107,237    
  -- (Not True) OR (Condition)    
              (  
                (  
                  NOT LTRIM(RTRIM(@ShowRossOnlyTabs)) = 1  
                )  
                OR ( ChildResourceId NOT IN ( 142,375,330,476,508,102,107,237,217,290 ) )  
              )  
              AND    
  ---- The following expression means if SchedulingMethod=regulartraditional, hide     
  ---- ResourceIds 91 and 497    
  ---- (Not True) OR (Condition)    
              (  
                (  
                  NOT LOWER(LTRIM(RTRIM(@SchedulingMethod))) = 'regulartraditional'  
                )  
                OR ( ChildResourceId NOT IN ( 91,497 ) )  
              )  
              AND    
  -- The following expression means if TrackSAPAttendance=byday, hide     
  -- ResourceIds 633    
  -- (Not True) OR (Condition)    
              (  
                (  
                  NOT LOWER(LTRIM(RTRIM(@TrackSAPAttendance))) = 'byday'  
                )  
                OR ( ChildResourceId NOT IN ( 633 ) )  
              )  
              AND    
  ---- The following expression means if @ShowCollegeOfCourtReporting is set to false, hide     
  ---- ResourceIds 614,615    
  ---- (Not False) OR (Condition)    
              (  
                (  
                  NOT LOWER(LTRIM(RTRIM(@ShowCollegeOfCourtReporting))) = 'no'  
                )  
                OR ( ChildResourceId NOT IN ( 614,615 ) )  
              )  
              AND    
  -- The following expression means if @ShowCollegeOfCourtReporting is set to true, hide     
  -- ResourceIds 497    
  -- (Not True) OR (Condition)    
              (  
                (  
                  NOT LOWER(LTRIM(RTRIM(@ShowCollegeOfCourtReporting))) = 'yes'  
                )  
                OR ( ChildResourceId NOT IN ( 497 ) )  
              )  
              AND    
  -- The following expression means if FAMEESP is set to false, hide     
  -- ResourceIds 517,523, 525    
  -- (Not False) OR (Condition)    
              (  
                (  
                  NOT LOWER(LTRIM(RTRIM(@FameESP))) = 'no'  
                )  
                OR ( ChildResourceId NOT IN ( 517,523,525,699 ) )  
              )  
              AND    
  ---- The following expression means if EDExpress is set to false, hide     
  ---- ResourceIds 603,604,606,619    
  ---- (Not False) OR (Condition)    
              (  
                (  
                  NOT LOWER(LTRIM(RTRIM(@EdExpress))) = 'no'  
                )  
                OR ( ChildResourceId NOT IN ( 603,604,605,606,619,698 ) )  
              )  
              AND    
  ---- The following expression means if @GradeBookWeightingLevel is set to courselevel, hide     
  ---- ResourceIds 107,96,222    
  ---- (Not False) OR (Condition)    
              (  
                (  
                  NOT LOWER(LTRIM(RTRIM(@GradeBookWeightingLevel))) = 'courselevel'  
                )  
                OR ( ChildResourceId NOT IN ( 107,96,222 ) )  
              )  
              AND (  
                    (  
                      NOT LOWER(LTRIM(RTRIM(@GradeBookWeightingLevel))) = 'instructorlevel'  
                    )  
                    OR ( ChildResourceId NOT IN ( 476 ) )  
                  )  
              AND (  
                    (  
                      NOT LOWER(LTRIM(RTRIM(@ShowExternshipTabs))) = 'no'  
                    )  
                    OR ( ChildResourceId NOT IN ( 543,538 ) )  
                  )  
  --            AND  
  --            ---- If user is not a power user (sa or support or super), hide Setup Required Fields (234)  
     ------ (Not False) OR (Condition)      
  --             ((NOT @IsUserAPowerUser = 0) OR (ChildResourceId NOT IN (234)))     
            )  
    ORDER BY GroupSortOrder  
           ,ParentResourceId  
           ,ChildResource;  
  
  
  
  
GO
