SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/***
EXEC usp_IPEDS_Spring_PartA_DetailAndSummary '3F5E839A-589A-4B2A-B258-35A1A8B3B819',NULL,'2010','ssn','07/01/2010','06/30/2011','academic',1,null,
'10/15/2010'

EXEC usp_IPEDS_Spring_PartE_DetailAndSummaryIncomeLevelAndGrants_CohortYear 
'3F5E839A-589A-4B2A-B258-35A1A8B3B819',NULL,'2010','ssn','07/01/2010','06/30/2011','academic','1','10/15/2010',null

EXEC [usp_IPEDS_Spring_PartE_DetailAndSummaryLivingArrangements_CohortYear] 
'3F5E839A-589A-4B2A-B258-35A1A8B3B819',NULL,'2010','ssn','07/01/2010','06/30/2011','academic','1','10/15/2010',null

EXEC usp_IPEDS_Spring_PartA_DetailAndSummary '3F5E839A-589A-4B2A-B258-35A1A8B3B819',NULL,'2009','ssn','07/01/2009','06/30/2010','academic',1,null,
'10/15/2009'

EXEC usp_IPEDS_Spring_PartE_DetailAndSummaryIncomeLevelAndGrants_CohortYearMinus1 
'3F5E839A-589A-4B2A-B258-35A1A8B3B819',NULL,'2010','ssn','07/01/2010','06/30/2011','academic','1','10/15/2010',null

EXEC usp_IPEDS_Spring_PartE_DetailAndSummaryLivingArrangements_CohortYearMinus1 
'3F5E839A-589A-4B2A-B258-35A1A8B3B819',NULL,'2010','ssn','07/01/2010','06/30/2010','academic','1','10/15/2010',null

EXEC usp_IPEDS_Spring_PartA_DetailAndSummary '3F5E839A-589A-4B2A-B258-35A1A8B3B819',NULL,'2008','ssn','07/01/2008','06/30/2009','academic',1,null,
'10/15/2008'

EXEC usp_IPEDS_Spring_PartE_DetailAndSummaryIncomeLevelAndGrants_CohortYearMinus2 
'3F5E839A-589A-4B2A-B258-35A1A8B3B819',NULL,'2010','ssn','07/01/2010','06/30/2011','academic','1','10/15/2010',null

EXEC usp_IPEDS_Spring_PartE_DetailAndSummaryLivingArrangements_CohortYearMinus2 
'3F5E839A-589A-4B2A-B258-35A1A8B3B819',NULL,'2010','ssn','07/01/2010','06/30/2010','academic','1','10/15/2010',null
***/
CREATE PROCEDURE [dbo].[usp_IPEDS_Spring_PartE_DetailAndSummaryIncomeLevelAndGrants_CohortYear]
    @CampusId VARCHAR(50)
   ,@ProgId VARCHAR(4000) = NULL
   ,@CohortYear VARCHAR(10) = NULL
   ,@OrderBy VARCHAR(100)
   ,@StartDate DATETIME
   ,@EndDate DATETIME
   ,@SchoolType VARCHAR(50)
   ,@InstitutionType VARCHAR(50)
   ,@EndDate_Academic DATETIME = NULL
   ,@LargeProgramId VARCHAR(50) = NULL
AS
    DECLARE @ReturnValue VARCHAR(100)
       ,@Value VARCHAR(100);
    DECLARE @SSN VARCHAR(10)
       ,@FirstName VARCHAR(100)
       ,@LastName VARCHAR(100)
       ,@StudentNumber VARCHAR(50)
       ,@TransferredOut INT
       ,@Exclusions INT;
    DECLARE @CitizenShip_IPEDSValue INT
       ,@ProgramType_IPEDSValue INT
       ,@Gender_IPEDSValue INT
       ,@FullTimePartTime_IPEDSValue INT
       ,@StudentId UNIQUEIDENTIFIER; 
    DECLARE @AcademicEndDate DATETIME
       ,@AcademicStartDate DATETIME;
    DECLARE @AcadInstFirstTimeStartDate DATETIME;
-- Check if School tracks grades by letter or numeric 
    SET @Value = (
                   SELECT TOP 1
                            Value
                   FROM     dbo.syConfigAppSetValues
                   WHERE    SettingId = 47
                 );

--Here, we're determining if the school type is academic. If so, we will change the new var
--@AcademicEndDate to the @EndDate_Academic, if not, than use @EndDate. We will use the new
--@AcademicEndDate variable to help select the student list.  DD Rev 01/10/12
    IF LOWER(@SchoolType) = 'academic'
        BEGIN
            SET @AcademicEndDate = @EndDate_Academic;
            SET @AcademicStartDate = @EndDate_Academic;
            --12/05/2012 DE8824 - Academic year options should have a cutoff start date
            SET @AcadInstFirstTimeStartDate = DATEADD(YEAR,-1,@EndDate_Academic); 
        END;
    ELSE
        BEGIN
            SET @AcademicEndDate = @EndDate;
            SET @AcademicStartDate = @StartDate;
        END;
	

    IF @ProgId IS NOT NULL
        BEGIN
            SELECT  @ReturnValue = COALESCE(@ReturnValue,'') + ProgDescrip + ','
            FROM    arPrograms t1
            WHERE   t1.ProgId IN ( SELECT   Val
                                   FROM     MultipleValuesForReportParameters(@ProgId,',',1) );
            SET @ReturnValue = SUBSTRING(@ReturnValue,1,LEN(@ReturnValue) - 1);
        END;
    ELSE
        BEGIN
            SELECT  @ReturnValue = COALESCE(@ReturnValue,'') + ProgDescrip + ','
            FROM    arPrograms t1
            WHERE   t1.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
                    AND t1.CampGrpId IN ( SELECT    CampGrpId
                                          FROM      syCmpGrpCmps
                                          WHERE     CampusId = @CampusId );
            SET @ReturnValue = SUBSTRING(@ReturnValue,1,LEN(@ReturnValue) - 1);
        END;

/*********Get the list of students that will be shown in the report - Starts Here ******************/			
    CREATE TABLE #StudentsList
        (
         StudentId UNIQUEIDENTIFIER
        ,SSN VARCHAR(10)
        ,FirstName VARCHAR(100)
        ,LastName VARCHAR(100)
        ,MiddleName VARCHAR(50)
        ,StudentNumber VARCHAR(50)
        ,TransferredOut INT
        ,Exclusions INT
        ,CitizenShip_IPEDSValue INT
        ,ProgramType_IPEDSValue INT
        ,Gender_IPEDSValue INT
        ,FullTimePartTime_IPEDSValue INT
        ,GenderId UNIQUEIDENTIFIER
        ,RaceId UNIQUEIDENTIFIER
        ,CitizenId UNIQUEIDENTIFIER
        ,GenderDescription VARCHAR(50)
        ,RaceDescription VARCHAR(50)
        ,StudentGraduatedStatusCount INT
        ,ClockHourProgramCount INT
        ,StartDate DATETIME
        ,HoustingType UNIQUEIDENTIFIER
        ,FamilyIncome UNIQUEIDENTIFIER
        );
		
-- The following table will store only Full Time First Time UnderGraduate Students
    CREATE TABLE #UnderGraduateStudentsWhoReceivedFinancialAid
        (
         StudentId UNIQUEIDENTIFIER
        ,FinancialAidType INT
        ,TitleIV BIT
        );


-- Get the list of UnderGraduate Students
    IF @SchoolType <> 'program'
        AND @InstitutionType = '1'
        BEGIN
            INSERT  INTO #StudentsList
                    SELECT DISTINCT
                            t1.StudentId
                           ,t1.SSN
                           ,t1.FirstName
                           ,t1.LastName
                           ,t1.MiddleName
                           ,t1.StudentNumber
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SQ1
                                       ,syStatusCodes SQ2
                                       ,dbo.sySysStatus SQ3
                              WHERE     SQ1.StatusCodeId = SQ2.StatusCodeId
                                        AND SQ2.SysStatusId = SQ3.SysStatusId
                                        AND SQ1.StudentId = t1.StudentId
                                        AND SQ3.SysStatusId = 19
                                        AND SQ1.TransferDate <= @AcademicEndDate
                            ) AS TransferredOut
                           ,(
					-- Check if the student was either dropped and if the drop reason is either
					-- deceased, activeduty, foreign aid service, church mission
                              CASE WHEN (
                                          SELECT    COUNT(*)
                                          FROM      arStuEnrollments SQ1
                                                   ,syStatusCodes SQ2
                                                   ,dbo.sySysStatus SQ3
                                                   ,arDropReasons SQ44
                                          WHERE     SQ1.StatusCodeId = SQ2.StatusCodeId
                                                    AND SQ2.SysStatusId = SQ3.SysStatusId
                                                    AND SQ1.DropReasonId = SQ44.DropReasonId
                                                    AND SQ1.StudentId = t1.StudentId
                                                    AND SQ3.SysStatusId IN ( 12 ) -- Dropped
                                                    AND SQ1.DateDetermined <= @AcademicEndDate
                                                    AND SQ44.IPEDSValue IN ( 15,16,17,18,19 )
                                        ) >= 1 THEN 1
                                   ELSE 0
                              END ) AS Exclusions
                           ,t11.IPEDSValue AS CitizenShip_IPEDSValue
                           ,t9.IPEDSValue AS ProgramType_IPEDSValue
                           ,t3.IPEDSValue AS Gender_IPEDSValue
                           ,t10.IPEDSValue AS FullTimePartTime_IPEDSValue
                           ,t1.Gender
                           ,t1.Race
                           ,t1.Citizen
                           ,(
                              SELECT DISTINCT
                                        AgencyDescrip
                              FROM      syRptAgencyFldValues
                              WHERE     RptAgencyFldValId = t3.IPEDSValue
                            ) AS GenderDescription
                           ,(
                              SELECT DISTINCT
                                        AgencyDescrip
                              FROM      syRptAgencyFldValues
                              WHERE     RptAgencyFldValId = t4.IPEDSValue
                            ) AS RaceDescription
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SEC
                                       ,syStatusCodes SC
                              WHERE     SEC.StudentId = t1.StudentId
                                        AND SEC.StatusCodeId = SC.StatusCodeId
                                        AND SC.SysStatusId = 14
                            ) AS StudentGraduatedStatusCount
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SEC
                                       ,arPrograms P
                                       ,arPrgVersions PV
                              WHERE     SEC.PrgVerId = PV.PrgVerId
                                        AND P.ProgId = PV.ProgId
                                        AND SEC.StudentId = t1.StudentId
                                        AND P.ACId = 5
                            ) AS ClockHourProgramCount
                           ,t2.StartDate
                           ,t1.HousingId
                           ,t1.FamilyIncome
                    FROM    adGenders t3
                    LEFT JOIN arStudent t1 ON t3.GenderId = t1.Gender
                    LEFT JOIN adEthCodes t4 ON t4.EthCodeId = t1.Race
                    INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                    INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                    INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                                 AND t6.SysStatusId NOT IN ( 8 ) -- Ignore "No Start" Students
                    INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                    INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                    INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                    LEFT JOIN arAttendTypes t10 ON t2.AttendTypeId = t10.AttendTypeId
                    LEFT JOIN dbo.adDegCertSeeking t12 ON t2.DegCertSeekingId = t12.DegCertSeekingId
                    INNER JOIN adCitizenships t11 ON t1.Citizen = t11.CitizenshipId
                    INNER JOIN dbo.saTransactions SA ON SA.StuEnrollId = t2.StuEnrollId
                    INNER JOIN dbo.saFundSources FS ON SA.FundSourceId = FS.FundSourceId
                            --DE8813 - Item 2
                            -- INNER JOIN dbo.saTuitionCategories TC ON t2.TuitionCategoryId = TC.TuitionCategoryId
                    WHERE   LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                            AND (
                                  @ProgId IS NULL
                                  OR t8.ProgId IN ( SELECT  Val
                                                    FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                )
                            AND t10.IPEDSValue = 61
                            AND -- Full Time
                            t12.IPEDSValue = 11
                            AND -- First Time
                            t9.IPEDSValue = 58
                            AND FS.AdvFundSourceId NOT IN ( 1,19,23 )
                            AND -- Under Graduate
                            --12/05/2012 DE8824 - Academic year options should have a cutoff start date
                            --( t2.StartDate <= @AcademicEndDate )
                            (
                              t2.StartDate > @AcadInstFirstTimeStartDate
                              AND t2.StartDate <= @AcademicEndDate
                            )
                            AND t2.StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
						( SELECT    t1.StuEnrollId
                          FROM      arStuEnrollments t1
                                   ,syStatusCodes t2
                          WHERE     t1.StatusCodeId = t2.StatusCodeId
                                    AND StartDate <= @AcademicEndDate
                                    AND 
							-- Student started before the end date range
                                    LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                    AND (
                                          @ProgId IS NULL
                                          OR t8.ProgId IN ( SELECT  Val
                                                            FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                        )
                                    AND t2.SysStatusId IN ( 12,14,19,8 ) 
							-- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                    AND (
                                          t1.DateDetermined < @AcademicStartDate
                                          OR ExpGradDate < @AcademicStartDate
                                          OR LDA < @AcademicStartDate
                                        ) ) 
                       -- If Student is enrolled in only one program version and if that program version 
                       -- happens to be a continuing ed program exclude the student
                            AND t2.StudentId NOT IN ( SELECT DISTINCT
                                                                StudentId
                                                      FROM      (
                                                                  SELECT    StudentId
                                                                           ,COUNT(*) AS RowCounter
                                                                  FROM      arStuEnrollments
                                                                  WHERE     PrgVerId IN ( SELECT    PrgVerId
                                                                                          FROM      arPrgVersions
                                                                                          WHERE     IsContinuingEd = 1 )
                                                                  GROUP BY  StudentId
                                                                  HAVING    COUNT(*) = 1
                                                                ) dtStudent_ContinuingEd )

                         
       --                  --Also check if the student has received any Title IV Funds (Group 4 Students)
       --                 AND t2.StuEnrollId IN 
       --                  (
							--SELECT DISTINCT t1.StuEnrollId FROM faStudentAwards t1,saFundSources t2 
							--WHERE t1.AwardTypeId=t2.FundSourceId AND t2.TitleIV=1 AND
							--t1.AwardStartDate>=@StartDate AND t1.AwardstartDate<=@EndDate
       --                  )
                         --AND TC.IPEDSValue IN (145,146) --Instate or In-district tuition
                            AND FS.TitleIV = 1
       --                  AND 
							--(
							--	(SA.AwardStartDate>=@StartDate AND SA.AwardstartDate<=@EndDate) 
							--	OR 
							--	(SA.AwardEndDate>=@StartDate AND SA.AwardEndDate<=@EndDate)
							--)
							-- FFEL PLUS (9)(added for DE8816)
                            AND ISNULL(FS.AdvFundSourceId,0) NOT IN ( 5,6,9 ); -- Exclude FWS and Plus Loan
                        --AND FS.IPEDSValue IN (66,67,68) -- Only Federal, State and Institution Grant
        END;
--SELECT * FROM #StudentsList WHERE SSN='022622355'
    IF @SchoolType = 'program' --and @InstitutionType='1'
        BEGIN

--Get student only from largest program for program reporter schools
            SET @ProgId = @LargeProgramId;

            INSERT  INTO #StudentsList
                    SELECT DISTINCT
                            t1.StudentId
                           ,t1.SSN
                           ,t1.FirstName
                           ,t1.LastName
                           ,t1.MiddleName
                           ,t1.StudentNumber
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SQ1
                                       ,syStatusCodes SQ2
                                       ,dbo.sySysStatus SQ3
                              WHERE     SQ1.StatusCodeId = SQ2.StatusCodeId
                                        AND SQ2.SysStatusId = SQ3.SysStatusId
                                        AND SQ1.StudentId = t1.StudentId
                                        AND SQ3.SysStatusId = 19
                                        AND SQ1.TransferDate <= @EndDate
                            ) AS TransferredOut
                           ,(
					-- Check if the student was either dropped and if the drop reason is either
					-- deceased, activeduty, foreign aid service, church mission
                              CASE WHEN (
                                          SELECT    COUNT(*)
                                          FROM      arStuEnrollments SQ1
                                                   ,syStatusCodes SQ2
                                                   ,dbo.sySysStatus SQ3
                                                   ,arDropReasons SQ44
                                          WHERE     SQ1.StatusCodeId = SQ2.StatusCodeId
                                                    AND SQ2.SysStatusId = SQ3.SysStatusId
                                                    AND SQ1.DropReasonId = SQ44.DropReasonId
                                                    AND SQ1.StudentId = t1.StudentId
                                                    AND SQ3.SysStatusId IN ( 12 ) -- Dropped
                                                    AND SQ1.DateDetermined <= @EndDate
                                                    AND SQ44.IPEDSValue IN ( 15,16,17,18,19 )
                                        ) >= 1 THEN 1
                                   ELSE 0
                              END ) AS Exclusions
                           ,t11.IPEDSValue AS CitizenShip_IPEDSValue
                           ,t9.IPEDSValue AS ProgramType_IPEDSValue
                           ,t3.IPEDSValue AS Gender_IPEDSValue
                           ,t10.IPEDSValue AS FullTimePartTime_IPEDSValue
                           ,t1.Gender
                           ,t1.Race
                           ,t1.Citizen
                           ,(
                              SELECT DISTINCT
                                        AgencyDescrip
                              FROM      syRptAgencyFldValues
                              WHERE     RptAgencyFldValId = t3.IPEDSValue
                            ) AS GenderDescription
                           ,(
                              SELECT DISTINCT
                                        AgencyDescrip
                              FROM      syRptAgencyFldValues
                              WHERE     RptAgencyFldValId = t4.IPEDSValue
                            ) AS RaceDescription
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SEC
                                       ,syStatusCodes SC
                              WHERE     SEC.StudentId = t1.StudentId
                                        AND SEC.StatusCodeId = SC.StatusCodeId
                                        AND SC.SysStatusId = 14
                            ) AS StudentGraduatedStatusCount
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SEC
                                       ,arPrograms P
                                       ,arPrgVersions PV
                              WHERE     SEC.PrgVerId = PV.PrgVerId
                                        AND P.ProgId = PV.ProgId
                                        AND SEC.StudentId = t1.StudentId
                                        AND P.ACId = 5
                            ) AS ClockHourProgramCount
                           ,t2.StartDate
                           ,t1.HousingId
                           ,t1.FamilyIncome
                    FROM    adGenders t3
                    LEFT JOIN arStudent t1 ON t3.GenderId = t1.Gender
                    LEFT JOIN adEthCodes t4 ON t4.EthCodeId = t1.Race
                    INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                    INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                    INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                                 AND t6.SysStatusId NOT IN ( 8 ) -- Ignore "No Start" Students
                    INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                    INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                    INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                    LEFT JOIN arAttendTypes t10 ON t2.AttendTypeId = t10.AttendTypeId
                    LEFT JOIN dbo.adDegCertSeeking t12 ON t2.DegCertSeekingId = t12.DegCertSeekingId
                    INNER JOIN adCitizenships t11 ON t1.Citizen = t11.CitizenshipId
                    INNER JOIN dbo.saTransactions SA ON SA.StuEnrollId = t2.StuEnrollId
                    INNER JOIN dbo.saFundSources FS ON SA.FundSourceId = FS.FundSourceId
                    WHERE   LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                            AND (
                                  @ProgId IS NULL
                                  OR t8.ProgId IN ( SELECT  Val
                                                    FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                )
                            AND t10.IPEDSValue = 61
                            AND -- Full Time
                            t12.IPEDSValue = 11
                            AND -- First Time
                            t9.IPEDSValue = 58
                            AND FS.AdvFundSourceId NOT IN ( 1,19,23 )
                            AND -- Under Graduate
                            (
                              t2.StartDate >= @StartDate
                              AND t2.StartDate <= @EndDate
                            )
                        --(t2.StartDate<=@EndDate)
                            AND t2.StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
						( SELECT    t1.StuEnrollId
                          FROM      arStuEnrollments t1
                                   ,syStatusCodes t2
                          WHERE     t1.StatusCodeId = t2.StatusCodeId
                                    AND StartDate <= @AcademicEndDate
                                    AND 
							-- Student started before the end date range
                                    LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                    AND (
                                          @ProgId IS NULL
                                          OR t8.ProgId IN ( SELECT  Val
                                                            FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                        )
                                    AND t2.SysStatusId IN ( 12,14,19,8 ) 
							-- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                    AND (
                                          t1.DateDetermined < @AcademicStartDate
                                          OR ExpGradDate < @AcademicStartDate
                                          OR LDA < @AcademicStartDate
                                        ) ) 
                       -- If Student is enrolled in only one program version and if that program version 
                       -- happens to be a continuing ed program exclude the student
                            AND t2.StudentId NOT IN ( SELECT DISTINCT
                                                                StudentId
                                                      FROM      (
                                                                  SELECT    StudentId
                                                                           ,COUNT(*) AS RowCounter
                                                                  FROM      arStuEnrollments
                                                                  WHERE     PrgVerId IN ( SELECT    PrgVerId
                                                                                          FROM      arPrgVersions
                                                                                          WHERE     IsContinuingEd = 1 )
                                                                  GROUP BY  StudentId
                                                                  HAVING    COUNT(*) = 1
                                                                ) dtStudent_ContinuingEd )

                         --AND
                         --Also check if the student has received any Title IV Funds (Group 4 Students)
       --                  t2.StuEnrollId IN 
       --                  (
							--SELECT DISTINCT t1.StuEnrollId FROM faStudentAwards t1,saFundSources t2 
							--WHERE t1.AwardTypeId=t2.FundSourceId AND t2.TitleIV=1 AND
							--t1.AwardStartDate>=@StartDate AND t1.AwardstartDate<=@EndDate
       --                  )
       --                   AND 
							--(
							--	(SA.AwardStartDate>=@StartDate AND SA.AwardstartDate<=@EndDate) 
							--	OR 
							--	(SA.AwardEndDate>=@StartDate AND SA.AwardEndDate<=@EndDate)
							--)
							-- FFEL PLUS (9)(added for DE8816)
                            AND ISNULL(FS.AdvFundSourceId,0) NOT IN ( 5,6,9 ) -- Exclude FWS and Plus Loan
                            AND FS.TitleIV = 1;
                        --AND FS.IPEDSValue IN (66,67,68) -- Only Federal, State and Institution Grant
        END;

                         
/*********Get the list of students tha
t will be shown in the report - Ends Here *****************8*/

-- Get all Full Time, First Time UnderGraduate Students
-- Use #StudentsList as it pulls UnderGraduate Students
--INSERT INTO #UnderGraduateStudentsWhoReceivedFinancialAid
--SELECT t1.StudentId,F
--inancialAidType,TitleIV FROM
--#StudentsList t1 INNER JOIN 
--			( 
--				SELECT 
--					SE.StudentId,
--				    (SAS.Amount + Coalesce((Select Sum(TransAmount) from saPmtDisbRel PDR, saTransactions T where AwardScheduleId=SAS.AwardScheduleId and PDR.TransactionId=
--T.TransactionId and T.Voided=0),0)) as Balance, 
--				    (Coalesce((Select Sum(TransAmount*-1) from saPmtDisbRel PDR, saTransactions T where AwardScheduleId=SAS.AwardScheduleId and PDR.TransactionId=T.TransactionId and T.Voided=0),0)) as Received,
				   
-- FS.IPEDSValue AS FinancialAidType,FS.TitleIV AS TitleIV
--				FROM   
--					faStudentAwardSchedule SAS INNER JOIN faStudentAwards SA ON SAS.StudentAwardId=SA.StudentAwardId
--					INNER JOIN dbo.arStuEnrollments SE ON SA.StuEnrollId=SE.StuEnrollId 
--					INNER
-- JOIN saFundSources FS ON SA.AwardTypeId=FS.FundSourceId
--					LEFT JOIN arAttendTypes t10 ON SE.attendtypeid = t10.AttendTypeId 
--					LEFT JOIN dbo.adDegCertSeeking t12 ON SE.DegCertSeekingId = t12.DegCertSeekingId
--				WHERE  
--					SE.StudentId IN (select
-- DISTINCT StudentId from #StudentsList) AND -- UnderGraduates
--					t10.IPEDSValue=61 AND -- Full Time 
--					t12.IPEDSValue=11 -- First Time
--			) 
--getStudentsThatReceivedFinancialAid ON t1.StudentId=getStudentsThatReceivedFinancialAid.StudentId 



    SELECT  RowNumber
           ,SSN
           ,StudentNumber
           ,StudentName
           ,CASE WHEN INC1 >= 1 THEN 'X'
                 ELSE ''
            END AS INC1X
           ,INC1
           ,GRNT1
           ,CASE WHEN GRNT1 >= 1 THEN 'X'
                 ELSE ''
            END AS GRNT1X
           ,INC1Aid
           ,CASE WHEN INC2 >= 1 THEN 'X'
                 ELSE ''
            END AS INC2X
           ,INC2
           ,GRNT2
           ,CASE WHEN GRNT2 >= 1 THEN 'X'
                 ELSE ''
            END AS GRNT2X
           ,INC2Aid
           ,CASE WHEN INC3 >= 1 THEN 'X'
                 ELSE ''
            END AS INC3X
           ,INC3
           ,GRNT3
           ,CASE WHEN GRNT3 >= 1 THEN 'X'
                 ELSE ''
            END AS GRNT3X
           ,INC3Aid
           ,CASE WHEN INC4 >= 1 THEN 'X'
                 ELSE ''
            END AS INC4X
           ,INC4
           ,GRNT4
           ,CASE WHEN GRNT4 >= 1 THEN 'X'
                 ELSE ''
            END AS GRNT4X
           ,INC4Aid
           ,CASE WHEN INC5 >= 1 THEN 'X'
                 ELSE ''
            END AS INC5X
           ,INC5
           ,GRNT5
           ,CASE WHEN GRNT5 >= 1 THEN 'X'
                 ELSE ''
            END AS GRNT5X
           ,INC5Aid
    INTO    #Result
    FROM    (
              SELECT    NEWID() AS RowNumber
                       ,dbo.UDF_FormatSSN(SSN) AS SSN
                       ,StudentNumber
                       ,LastName + ', ' + FirstName + ' ' + ISNULL(MiddleName,'') AS StudentName
                       ,
		--(SELECT COUNT(DISTINCT t1.StudentId) FROM arStudent t1 INNER JOIN syFamilyIncome t2 ON t1.FamilyIncome=t2.FamilyIncomeID 
		--where t1.StudentId=SL.StudentId AND t2.IPEDSValue=148) AS INC1,
                        (
                          SELECT    CASE WHEN SUM(TransAmount * -1) > 0 THEN 1
                                         ELSE 0
                                    END AS NetAmount
                          FROM      dbo.saTransactions t1
                          INNER JOIN saFundSources t2 ON t1.FundSourceId = t2.FundSourceId
                          INNER JOIN arStuEnrollments t3 ON t1.StuEnrollId = t3.StuEnrollId
                                                            AND t3.StudentId = SL.StudentId
                          INNER JOIN arStudent t4 ON t4.StudentId = t3.StudentId
                          INNER JOIN syFamilyIncome t5 ON t4.FamilyIncome = t5.FamilyIncomeID
                          WHERE     t2.TitleIV = 1
                                    AND t5.IPEDSValue = 148
                                    AND ISNULL(t2.AdvFundSourceId,0) NOT IN ( 5,6,9 )-- FFEL PLUS (9)(added for DE8816)
                                    AND ( t1.TransDate BETWEEN @StartDate AND @EndDate )
                                    AND t1.Voided = 0
                                    AND t2.AdvFundSourceId NOT IN ( 1,19,23 )
                                    AND t3.StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
						( SELECT    t1.StuEnrollId
                          FROM      arStuEnrollments t1
                                   ,syStatusCodes t2
                          WHERE     t1.StatusCodeId = t2.StatusCodeId
                                    AND StartDate <= @AcademicEndDate
                                    AND -- Student started before the end date range
                                    LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                    AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                    AND (
                                          t1.DateDetermined < @AcademicStartDate
                                          OR ExpGradDate < @AcademicStartDate
                                          OR LDA < @AcademicStartDate
                                        ) ) 
		--AND t2.IPEDSValue IN (66,67,68)
                        ) AS INC1
                       ,(
                          SELECT    CASE WHEN SUM(TransAmount * -1) > 0 THEN 1
                                         ELSE 0
                                    END AS NetAmount
                          FROM      dbo.saTransactions t1
                          INNER JOIN saFundSources t2 ON t1.FundSourceId = t2.FundSourceId
                          INNER JOIN arStuEnrollments t3 ON t1.StuEnrollId = t3.StuEnrollId
                                                            AND t3.StudentId = SL.StudentId
                          INNER JOIN arStudent t4 ON t4.StudentId = t3.StudentId
                          INNER JOIN syFamilyIncome t5 ON t4.FamilyIncome = t5.FamilyIncomeID
                          WHERE     t2.TitleIV = 1
                                    AND t5.IPEDSValue = 148
                                    AND ISNULL(t2.AdvFundSourceId,0) NOT IN ( 5,6,9 )-- FFEL PLUS (9)(added for DE8816)
                                    AND ( t1.TransDate BETWEEN @StartDate AND @EndDate )
                                    AND t1.Voided = 0
                                    AND t2.AdvFundSourceId NOT IN ( 1,19,23 )
                                    AND t3.StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
						( SELECT    t1.StuEnrollId
                          FROM      arStuEnrollments t1
                                   ,syStatusCodes t2
                          WHERE     t1.StatusCodeId = t2.StatusCodeId
                                    AND StartDate <= @AcademicEndDate
                                    AND -- Student started before the end date range
                                    LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                    AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                    AND (
                                          t1.DateDetermined < @AcademicStartDate
                                          OR ExpGradDate < @AcademicStartDate
                                          OR LDA < @AcademicStartDate
                                        ) )
                                    AND t2.IPEDSValue IN ( 66,67,68 )
                        ) AS GRNT1
                       ,(
                          SELECT    SUM(TransAmount * -1) AS NetAmount
                          FROM      dbo.saTransactions t1
                          INNER JOIN saFundSources t2 ON t1.FundSourceId = t2.FundSourceId
                          INNER JOIN arStuEnrollments t3 ON t1.StuEnrollId = t3.StuEnrollId
                                                            AND t3.StudentId = SL.StudentId
                          INNER JOIN arStudent t4 ON t4.StudentId = t3.StudentId
                          INNER JOIN syFamilyIncome t5 ON t4.FamilyIncome = t5.FamilyIncomeID
                          WHERE     t2.TitleIV = 1
                                    AND t5.IPEDSValue = 148
                                    AND ISNULL(t2.AdvFundSourceId,0) NOT IN ( 5,6,9 )-- FFEL PLUS (9)(added for DE8816)
                                    AND ( t1.TransDate BETWEEN @StartDate AND @EndDate )
                                    AND t1.Voided = 0
                                    AND t2.AdvFundSourceId NOT IN ( 1,19,23 )
                                    AND t3.StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
						( SELECT    t1.StuEnrollId
                          FROM      arStuEnrollments t1
                                   ,syStatusCodes t2
                          WHERE     t1.StatusCodeId = t2.StatusCodeId
                                    AND StartDate <= @AcademicEndDate
                                    AND -- Student started before the end date range
                                    LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                    AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                    AND (
                                          t1.DateDetermined < @AcademicStartDate
                                          OR ExpGradDate < @AcademicStartDate
                                          OR LDA < @AcademicStartDate
                                        ) )
                                    AND t2.IPEDSValue IN ( 66,67,68 )
                        ) AS INC1Aid
                       ,
				
		--(SELECT COUNT(DISTINCT t1.StudentId) FROM arStudent t1 INNER JOIN syFamilyIncome t2 ON t1.FamilyIncome=t2.FamilyIncomeID 
		--where t1.StudentId=SL.StudentId AND t2.IPEDSValue=149) AS INC2,
                        (
                          SELECT    CASE WHEN SUM(TransAmount * -1) > 0 THEN 1
                                         ELSE 0
                                    END AS NetAmount
                          FROM      dbo.saTransactions t1
                          INNER JOIN saFundSources t2 ON t1.FundSourceId = t2.FundSourceId
                          INNER JOIN arStuEnrollments t3 ON t1.StuEnrollId = t3.StuEnrollId
                                                            AND t3.StudentId = SL.StudentId
                          INNER JOIN arStudent t4 ON t4.StudentId = t3.StudentId
                          INNER JOIN syFamilyIncome t5 ON t4.FamilyIncome = t5.FamilyIncomeID
                          WHERE     t2.TitleIV = 1
                                    AND t5.IPEDSValue = 149
                                    AND ISNULL(t2.AdvFundSourceId,0) NOT IN ( 5,6,9 )-- FFEL PLUS (9)(added for DE8816)
                                    AND ( t1.TransDate BETWEEN @StartDate AND @EndDate )
                                    AND t1.Voided = 0
                                    AND t2.AdvFundSourceId NOT IN ( 1,19,23 )
                                    AND t3.StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
						( SELECT    t1.StuEnrollId
                          FROM      arStuEnrollments t1
                                   ,syStatusCodes t2
                          WHERE     t1.StatusCodeId = t2.StatusCodeId
                                    AND StartDate <= @AcademicEndDate
                                    AND -- Student started before the end date range
                                    LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                    AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                    AND (
                                          t1.DateDetermined < @AcademicStartDate
                                          OR ExpGradDate < @AcademicStartDate
                                          OR LDA < @AcademicStartDate
                                        ) ) 
			--AND t2.IPEDSValue IN (66,67,68)
                        ) AS INC2
                       ,(
                          SELECT    CASE WHEN SUM(TransAmount * -1) > 0 THEN 1
                                         ELSE 0
                                    END AS NetAmount
                          FROM      dbo.saTransactions t1
                          INNER JOIN saFundSources t2 ON t1.FundSourceId = t2.FundSourceId
                          INNER JOIN arStuEnrollments t3 ON t1.StuEnrollId = t3.StuEnrollId
                                                            AND t3.StudentId = SL.StudentId
                          INNER JOIN arStudent t4 ON t4.StudentId = t3.StudentId
                          INNER JOIN syFamilyIncome t5 ON t4.FamilyIncome = t5.FamilyIncomeID
                          WHERE     t2.TitleIV = 1
                                    AND t5.IPEDSValue = 149
                                    AND ISNULL(t2.AdvFundSourceId,0) NOT IN ( 5,6,9 ) -- FFEL PLUS (9)(added for DE8816)
                                    AND ( t1.TransDate BETWEEN @StartDate AND @EndDate )
                                    AND t1.Voided = 0
                                    AND t2.AdvFundSourceId NOT IN ( 1,19,23 )
                                    AND t3.StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
						( SELECT    t1.StuEnrollId
                          FROM      arStuEnrollments t1
                                   ,syStatusCodes t2
                          WHERE     t1.StatusCodeId = t2.StatusCodeId
                                    AND StartDate <= @AcademicEndDate
                                    AND -- Student started before the end date range
                                    LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                    AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                    AND (
                                          t1.DateDetermined < @AcademicStartDate
                                          OR ExpGradDate < @AcademicStartDate
                                          OR LDA < @AcademicStartDate
                                        ) )
                                    AND t2.IPEDSValue IN ( 66,67,68 )
                        ) AS GRNT2
                       ,(
                          SELECT    SUM(TransAmount * -1) AS NetAmount
                          FROM      dbo.saTransactions t1
                          INNER JOIN saFundSources t2 ON t1.FundSourceId = t2.FundSourceId
                          INNER JOIN arStuEnrollments t3 ON t1.StuEnrollId = t3.StuEnrollId
                                                            AND t3.StudentId = SL.StudentId
                          INNER JOIN arStudent t4 ON t4.StudentId = t3.StudentId
                          INNER JOIN syFamilyIncome t5 ON t4.FamilyIncome = t5.FamilyIncomeID
                          WHERE     t2.TitleIV = 1
                                    AND t5.IPEDSValue = 149
                                    AND ISNULL(t2.AdvFundSourceId,0) NOT IN ( 5,6,9 ) -- FFEL PLUS (9)(added for DE8816)
                                    AND ( t1.TransDate BETWEEN @StartDate AND @EndDate )
                                    AND t1.Voided = 0
                                    AND t2.AdvFundSourceId NOT IN ( 1,19,23 )
                                    AND t3.StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
						( SELECT    t1.StuEnrollId
                          FROM      arStuEnrollments t1
                                   ,syStatusCodes t2
                          WHERE     t1.StatusCodeId = t2.StatusCodeId
                                    AND StartDate <= @AcademicEndDate
                                    AND -- Student started before the end date range
                                    LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                    AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                    AND (
                                          t1.DateDetermined < @AcademicStartDate
                                          OR ExpGradDate < @AcademicStartDate
                                          OR LDA < @AcademicStartDate
                                        ) )
                                    AND t2.IPEDSValue IN ( 66,67,68 )
                        ) AS INC2Aid
                       ,	
		
		--(SELECT COUNT(DISTINCT t1.StudentId) FROM arStudent t1 INNER JOIN 
		--syFamilyIncome t2 ON t1.FamilyIncome=t2.FamilyIncomeID 
		--where t1.StudentId=SL.StudentId AND t2.IPEDSValue=151) AS INC3,
                        (
                          SELECT    CASE WHEN SUM(TransAmount * -1) > 0 THEN 1
                                         ELSE 0
                                    END AS NetAmount
                          FROM      dbo.saTransactions t1
                          INNER JOIN saFundSources t2 ON t1.FundSourceId = t2.FundSourceId
                          INNER JOIN arStuEnrollments t3 ON t1.StuEnrollId = t3.StuEnrollId
                                                            AND t3.StudentId = SL.StudentId
                          INNER JOIN arStudent t4 ON t4.StudentId = t3.StudentId
                          INNER JOIN syFamilyIncome t5 ON t4.FamilyIncome = t5.FamilyIncomeID
                          WHERE     t2.TitleIV = 1
                                    AND t5.IPEDSValue = 151
                                    AND ISNULL(t2.AdvFundSourceId,0) NOT IN ( 5,6,9 ) -- FFEL PLUS (9)(added for DE8816)
                                    AND ( t1.TransDate BETWEEN @StartDate AND @EndDate )
                                    AND t1.Voided = 0
                                    AND t2.AdvFundSourceId NOT IN ( 1,19,23 )
                                    AND t3.StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
						( SELECT    t1.StuEnrollId
                          FROM      arStuEnrollments t1
                                   ,syStatusCodes t2
                          WHERE     t1.StatusCodeId = t2.StatusCodeId
                                    AND StartDate <= @AcademicEndDate
                                    AND -- Student started before the end date range
                                    LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                    AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                    AND (
                                          t1.DateDetermined < @AcademicStartDate
                                          OR ExpGradDate < @AcademicStartDate
                                          OR LDA < @AcademicStartDate
                                        ) ) 
			--AND t2.IPEDSValue IN (66,67,68)
                        ) AS INC3
                       ,(
                          SELECT    CASE WHEN SUM(TransAmount * -1) > 0 THEN 1
                                         ELSE 0
                                    END AS NetAmount
                          FROM      dbo.saTransactions t1
                          INNER JOIN saFundSources t2 ON t1.FundSourceId = t2.FundSourceId
                          INNER JOIN arStuEnrollments t3 ON t1.StuEnrollId = t3.StuEnrollId
                                                            AND t3.StudentId = SL.StudentId
                          INNER JOIN arStudent t4 ON t4.StudentId = t3.StudentId
                          INNER JOIN syFamilyIncome t5 ON t4.FamilyIncome = t5.FamilyIncomeID
                          WHERE     t2.TitleIV = 1
                                    AND t5.IPEDSValue = 151
                                    AND ISNULL(t2.AdvFundSourceId,0) NOT IN ( 5,6,9 )-- FFEL PLUS (9)(added for DE8816)
                                    AND ( t1.TransDate BETWEEN @StartDate AND @EndDate )
                                    AND t1.Voided = 0
                                    AND t2.AdvFundSourceId NOT IN ( 1,19,23 )
                                    AND t3.StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
						( SELECT    t1.StuEnrollId
                          FROM      arStuEnrollments t1
                                   ,syStatusCodes t2
                          WHERE     t1.StatusCodeId = t2.StatusCodeId
                                    AND StartDate <= @AcademicEndDate
                                    AND -- Student started before the end date range
                                    LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                    AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                    AND (
                                          t1.DateDetermined < @AcademicStartDate
                                          OR ExpGradDate < @AcademicStartDate
                                          OR LDA < @AcademicStartDate
                                        ) )
                                    AND t2.IPEDSValue IN ( 66,67,68 )
                        ) AS GRNT3
                       ,(
                          SELECT    SUM(TransAmount * -1) AS NetAmount
                          FROM      dbo.saTransactions t1
                          INNER JOIN saFundSources t2 ON t1.FundSourceId = t2.FundSourceId
                          INNER JOIN arStuEnrollments t3 ON t1.StuEnrollId = t3.StuEnrollId
                                                            AND t3.StudentId = SL.StudentId
                          INNER JOIN arStudent t4 ON t4.StudentId = t3.StudentId
                          INNER JOIN syFamilyIncome t5 ON t4.FamilyIncome = t5.FamilyIncomeID
                          WHERE     t2.TitleIV = 1
                                    AND t5.IPEDSValue = 151
                                    AND ISNULL(t2.AdvFundSourceId,0) NOT IN ( 5,6,9 )-- FFEL PLUS (9)(added for DE8816)
                                    AND ( t1.TransDate BETWEEN @StartDate AND @EndDate )
                                    AND t1.Voided = 0
                                    AND t2.AdvFundSourceId NOT IN ( 1,19,23 )
                                    AND t3.StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
						( SELECT    t1.StuEnrollId
                          FROM      arStuEnrollments t1
                                   ,syStatusCodes t2
                          WHERE     t1.StatusCodeId = t2.StatusCodeId
                                    AND StartDate <= @AcademicEndDate
                                    AND -- Student started before the end date range
                                    LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                    AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                    AND (
                                          t1.DateDetermined < @AcademicStartDate
                                          OR ExpGradDate < @AcademicStartDate
                                          OR LDA < @AcademicStartDate
                                        ) )
                                    AND t2.IPEDSValue IN ( 66,67,68 )
                        ) AS INC3Aid
                       ,	
		
		--(SELECT COUNT(DISTINCT t1.StudentId) FROM arStudent t1 INNER JOIN syFamilyIncome t2 ON t1.FamilyIncome=t2.FamilyIncomeID 
		--where t1.StudentId=SL.StudentId AND t2.IPEDSValue=152) AS INC4,
                        (
                          SELECT    CASE WHEN SUM(TransAmount * -1) > 0 THEN 1
                                         ELSE 0
                                    END AS NetAmount
                          FROM      dbo.saTransactions t1
                          INNER JOIN saFundSources t2 ON t1.FundSourceId = t2.FundSourceId
                          INNER JOIN arStuEnrollments t3 ON t1.StuEnrollId = t3.StuEnrollId
                                                            AND t3.StudentId = SL.StudentId
                          INNER JOIN arStudent t4 ON t4.StudentId = t3.StudentId
                          INNER JOIN syFamilyIncome t5 ON t4.FamilyIncome = t5.FamilyIncomeID
                          WHERE     t2.TitleIV = 1
                                    AND t5.IPEDSValue = 152
                                    AND ISNULL(t2.AdvFundSourceId,0) NOT IN ( 5,6,9 ) -- FFEL PLUS (9)(added for DE8816)
                                    AND ( t1.TransDate BETWEEN @StartDate AND @EndDate )
                                    AND t1.Voided = 0
                                    AND t2.AdvFundSourceId NOT IN ( 1,19,23 )
                                    AND t3.StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
						( SELECT    t1.StuEnrollId
                          FROM      arStuEnrollments t1
                                   ,syStatusCodes t2
                          WHERE     t1.StatusCodeId = t2.StatusCodeId
                                    AND StartDate <= @AcademicEndDate
                                    AND -- Student started before the end date range
                                    LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                    AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                    AND (
                                          t1.DateDetermined < @AcademicStartDate
                                          OR ExpGradDate < @AcademicStartDate
                                          OR LDA < @AcademicStartDate
                                        ) ) 
			--AND t2.IPEDSValue IN (66,67,68)
                        ) AS INC4
                       ,(
                          SELECT    CASE WHEN SUM(TransAmount * -1) > 0 THEN 1
                                         ELSE 0
                                    END AS NetAmount
                          FROM      dbo.saTransactions t1
                          INNER JOIN saFundSources t2 ON t1.FundSourceId = t2.FundSourceId
                          INNER JOIN arStuEnrollments t3 ON t1.StuEnrollId = t3.StuEnrollId
                                                            AND t3.StudentId = SL.StudentId
                          INNER JOIN arStudent t4 ON t4.StudentId = t3.StudentId
                          INNER JOIN syFamilyIncome t5 ON t4.FamilyIncome = t5.FamilyIncomeID
                          WHERE     t2.TitleIV = 1
                                    AND t5.IPEDSValue = 152
                                    AND ISNULL(t2.AdvFundSourceId,0) NOT IN ( 5,6,9 )-- FFEL PLUS (9)(added for DE8816)
                                    AND ( t1.TransDate BETWEEN @StartDate AND @EndDate )
                                    AND t1.Voided = 0
                                    AND t2.AdvFundSourceId NOT IN ( 1,19,23 )
                                    AND t3.StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
						( SELECT    t1.StuEnrollId
                          FROM      arStuEnrollments t1
                                   ,syStatusCodes t2
                          WHERE     t1.StatusCodeId = t2.StatusCodeId
                                    AND StartDate <= @AcademicEndDate
                                    AND -- Student started before the end date range
                                    LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                    AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                    AND (
                                          t1.DateDetermined < @AcademicStartDate
                                          OR ExpGradDate < @AcademicStartDate
                                          OR LDA < @AcademicStartDate
                                        ) )
                                    AND t2.IPEDSValue IN ( 66,67,68 )
                        ) AS GRNT4
                       ,(
                          SELECT    SUM(TransAmount * -1) AS NetAmount
                          FROM      dbo.saTransactions t1
                          INNER JOIN saFundSources t2 ON t1.FundSourceId = t2.FundSourceId
                          INNER JOIN arStuEnrollments t3 ON t1.StuEnrollId = t3.StuEnrollId
                                                            AND t3.StudentId = SL.StudentId
                          INNER JOIN arStudent t4 ON t4.StudentId = t3.StudentId
                          INNER JOIN syFamilyIncome t5 ON t4.FamilyIncome = t5.FamilyIncomeID
                          WHERE     t2.TitleIV = 1
                                    AND t5.IPEDSValue = 152
                                    AND ISNULL(t2.AdvFundSourceId,0) NOT IN ( 5,6,9 )-- FFEL PLUS (9)(added for DE8816)
                                    AND ( t1.TransDate BETWEEN @StartDate AND @EndDate )
                                    AND t1.Voided = 0
                                    AND t2.AdvFundSourceId NOT IN ( 1,19,23 )
                                    AND t3.StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
						( SELECT    t1.StuEnrollId
                          FROM      arStuEnrollments t1
                                   ,syStatusCodes t2
                          WHERE     t1.StatusCodeId = t2.StatusCodeId
                                    AND StartDate <= @AcademicEndDate
                                    AND -- Student started before the end date range
                                    LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                    AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                    AND (
                                          t1.DateDetermined < @AcademicStartDate
                                          OR ExpGradDate < @AcademicStartDate
                                          OR LDA < @AcademicStartDate
                                        ) )
                                    AND t2.IPEDSValue IN ( 66,67,68 )
                        ) AS INC4Aid
                       ,	
		
		--(SELECT COUNT(DISTINCT t1.StudentId) FROM arStudent t1 INNER JOIN syFamilyIncome t2 ON t1.FamilyIncome=t2.FamilyIncomeID 
		--where t1.StudentId=SL.StudentId AND t2.IPEDSValue=153) AS INC5,
                        (
                          SELECT    CASE WHEN SUM(TransAmount * -1) > 0 THEN 1
                                         ELSE 0
                                    END AS NetAmount
                          FROM      dbo.saTransactions t1
                          INNER JOIN saFundSources t2 ON t1.FundSourceId = t2.FundSourceId
                          INNER JOIN arStuEnrollments t3 ON t1.StuEnrollId = t3.StuEnrollId
                                                            AND t3.StudentId = SL.StudentId
                          INNER JOIN arStudent t4 ON t4.StudentId = t3.StudentId
                          INNER JOIN syFamilyIncome t5 ON t4.FamilyIncome = t5.FamilyIncomeID
                          WHERE     t2.TitleIV = 1
                                    AND t5.IPEDSValue = 153
                                    AND ISNULL(t2.AdvFundSourceId,0) NOT IN ( 5,6,9 )-- FFEL PLUS (9)(added for DE8816)
                                    AND ( t1.TransDate BETWEEN @StartDate AND @EndDate )
                                    AND t1.Voided = 0
                                    AND t2.AdvFundSourceId NOT IN ( 1,19,23 )
                                    AND t3.StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
						( SELECT    t1.StuEnrollId
                          FROM      arStuEnrollments t1
                                   ,syStatusCodes t2
                          WHERE     t1.StatusCodeId = t2.StatusCodeId
                                    AND StartDate <= @AcademicEndDate
                                    AND -- Student started before the end date range
                                    LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                    AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                    AND (
                                          t1.DateDetermined < @AcademicStartDate
                                          OR ExpGradDate < @AcademicStartDate
                                          OR LDA < @AcademicStartDate
                                        ) ) 
			--AND t2.IPEDSValue IN (66,67,68)
                        ) AS INC5
                       ,(
                          SELECT    CASE WHEN SUM(TransAmount * -1) > 0 THEN 1
                                         ELSE 0
                                    END AS NetAmount
                          FROM      dbo.saTransactions t1
                          INNER JOIN saFundSources t2 ON t1.FundSourceId = t2.FundSourceId
                          INNER JOIN arStuEnrollments t3 ON t1.StuEnrollId = t3.StuEnrollId
                                                            AND t3.StudentId = SL.StudentId
                          INNER JOIN arStudent t4 ON t4.StudentId = t3.StudentId
                          INNER JOIN syFamilyIncome t5 ON t4.FamilyIncome = t5.FamilyIncomeID
                          WHERE     t2.TitleIV = 1
                                    AND t5.IPEDSValue = 153
                                    AND ISNULL(t2.AdvFundSourceId,0) NOT IN ( 5,6,9 )-- FFEL PLUS (9)(added for DE8816)
                                    AND ( t1.TransDate BETWEEN @StartDate AND @EndDate )
                                    AND t1.Voided = 0
                                    AND t2.AdvFundSourceId NOT IN ( 1,19,23 )
                                    AND t3.StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
						( SELECT    t1.StuEnrollId
                          FROM      arStuEnrollments t1
                                   ,syStatusCodes t2
                          WHERE     t1.StatusCodeId = t2.StatusCodeId
                                    AND StartDate <= @AcademicEndDate
                                    AND -- Student started before the end date range
                                    LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                    AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                    AND (
                                          t1.DateDetermined < @AcademicStartDate
                                          OR ExpGradDate < @AcademicStartDate
                                          OR LDA < @AcademicStartDate
                                        ) )
                                    AND t2.IPEDSValue IN ( 66,67,68 )
                        ) AS GRNT5
                       ,(
                          SELECT    SUM(TransAmount * -1) AS NetAmount
                          FROM      dbo.saTransactions t1
                          INNER JOIN saFundSources t2 ON t1.FundSourceId = t2.FundSourceId
                          INNER JOIN arStuEnrollments t3 ON t1.StuEnrollId = t3.StuEnrollId
                                                            AND t3.StudentId = SL.StudentId
                          INNER JOIN arStudent t4 ON t4.StudentId = t3.StudentId
                          INNER JOIN syFamilyIncome t5 ON t4.FamilyIncome = t5.FamilyIncomeID
                          WHERE     t2.TitleIV = 1
                                    AND t5.IPEDSValue = 153
                                    AND ISNULL(t2.AdvFundSourceId,0) NOT IN ( 5,6,9 )-- FFEL PLUS (9)(added for DE8816)
                                    AND ( t1.TransDate BETWEEN @StartDate AND @EndDate )
                                    AND t1.Voided = 0
                                    AND t2.AdvFundSourceId NOT IN ( 1,19,23 )
                                    AND t3.StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
						( SELECT    t1.StuEnrollId
                          FROM      arStuEnrollments t1
                                   ,syStatusCodes t2
                          WHERE     t1.StatusCodeId = t2.StatusCodeId
                                    AND StartDate <= @AcademicEndDate
                                    AND -- Student started before the end date range
                                    LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                    AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                    AND (
                                          t1.DateDetermined < @AcademicStartDate
                                          OR ExpGradDate < @AcademicStartDate
                                          OR LDA < @AcademicStartDate
                                        ) )
                                    AND t2.IPEDSValue IN ( 66,67,68 )
                        ) AS INC5Aid
              FROM      #StudentsList SL
            ) dt
    ORDER BY CASE WHEN @OrderBy = 'SSN' THEN dt.SSN
             END
           ,CASE WHEN @OrderBy = 'LastName' THEN dt.StudentName
            END
           ,
            -- Updated 11/27/2012 - sort by student number not working
			--Case When @OrderBy='StudentNumber' Then Convert(int,dt.StudentNumber)
            CASE WHEN @OrderBy = 'Student Number' THEN dt.StudentNumber
            END;
		
		
    CREATE TABLE #CohortYear
        (
         SSN_CohortYr VARCHAR(11)
        ,StudentNumber_CohortYr VARCHAR(50)
        ,StudentName_CohortYr VARCHAR(200)
        ,INC1X_CohortYr VARCHAR(1)
        ,GRNT1X_CohortYr VARCHAR(1)
        ,INC1_CohortYr INT
        ,GRNT1_CohortYr INT
        ,INC1Aid_CohortYr DECIMAL(18,2)
        ,INC2X_CohortYr VARCHAR(1)
        ,GRNT2X_CohortYr VARCHAR(1)
        ,INC2_CohortYr INT
        ,GRNT2_CohortYr INT
        ,INC2Aid_CohortYr DECIMAL(18,2)
        ,INC3X_CohortYr VARCHAR(1)
        ,GRNT3X_CohortYr VARCHAR(1)
        ,INC3_CohortYr INT
        ,GRNT3_CohortYr INT
        ,INC3Aid_CohortYr DECIMAL(18,2)
        ,INC4X_CohortYr VARCHAR(1)
        ,GRNT4X_CohortYr VARCHAR(1)
        ,INC4_CohortYr INT
        ,GRNT4_CohortYr INT
        ,INC4Aid_CohortYr DECIMAL(18,2)
        ,INC5X_CohortYr VARCHAR(1)
        ,GRNT5X_CohortYr VARCHAR(1)
        ,INC5_CohortYr INT
        ,GRNT5_CohortYr INT
        ,INC5Aid_CohortYr DECIMAL(18,2)
        );		
	
		
    INSERT  INTO #CohortYear
            SELECT  SSN
                   ,StudentNumber
                   ,StudentName
                   ,INC1X
                   ,GRNT1X
                   ,INC1
                   ,GRNT1
                   ,SUM(INC1Aid) AS INC1Aid
                   ,INC2X
                   ,GRNT2X
                   ,INC2
                   ,GRNT2
                   ,SUM(INC2Aid) AS INC2Aid
                   ,INC3X
                   ,GRNT3X
                   ,INC3
                   ,GRNT3
                   ,SUM(INC3Aid) AS INC3Aid
                   ,INC4X
                   ,GRNT4X
                   ,INC4
                   ,GRNT4
                   ,SUM(INC4Aid) AS INC4Aid
                   ,INC5X
                   ,GRNT5X
                   ,INC5
                   ,GRNT5
                   ,SUM(INC5Aid) AS INC5Aid
            FROM    #Result
            GROUP BY SSN
                   ,StudentNumber
                   ,StudentName
                   ,INC1X
                   ,GRNT1X
                   ,INC1
                   ,GRNT1
                   ,INC2X
                   ,GRNT2X
                   ,INC2
                   ,GRNT2
                   ,INC3X
                   ,GRNT3X
                   ,INC3
                   ,GRNT3
                   ,INC4X
                   ,GRNT4X
                   ,INC4
                   ,GRNT4
                   ,INC5X
                   ,GRNT5X
                   ,INC5
                   ,GRNT5
            ORDER BY CASE WHEN @OrderBy = 'SSN' THEN SSN
                     END
                   ,CASE WHEN @OrderBy = 'LastName' THEN StudentName
                    END
                   ,
			-- Updated 11/27/2012 - sort by student number not working
			--Case When @OrderBy='StudentNumber' Then Convert(int,StudentNumber)
                    CASE WHEN @OrderBy = 'Student Number' THEN StudentNumber
                    END;
    DROP TABLE #UnderGraduateStudentsWhoReceivedFinancialAid;
    DROP TABLE #StudentsList;



    SELECT  *
    FROM    #CohortYear;

    DROP TABLE #Result;
    DROP TABLE #CohortYear;



GO
