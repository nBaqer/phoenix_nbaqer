SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jose Torres
-- Create date: 7/17/2012
-- Description:	Get the all active Program Versions id and description
--              for a given CampusIds id and description
--              Used in Report
-- EXECUTE RS_GetActivePrgVesionsForaGivenCampusId  '253EAF31-86B0-4C91-8178-3BAA3EE1327D'
-- =============================================
CREATE PROCEDURE [dbo].[RS_GetActivePrgVesionsForaGivenCampusId]
-- Add the parameters for the stored procedure here
    @CampusId VARCHAR(40)
AS
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;

        SELECT  PV.PrgVerId
               ,PV.PrgVerDescrip
        FROM    arPrgVersions AS PV
        INNER JOIN syCmpGrpCmps AS CGC ON PV.CampGrpId = CGC.CampGrpId
        INNER JOIN syCampuses AS C ON CGC.CampusId = C.CampusId
        WHERE   C.CampusId = @CampusId
        ORDER BY PrgVerDescrip;
    END;




GO
