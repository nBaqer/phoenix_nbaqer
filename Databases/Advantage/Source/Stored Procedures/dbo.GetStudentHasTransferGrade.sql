SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ginzo, John
-- Create date: 06/09/2015
-- Description:	GetStudentHasTransferGrade
-- =============================================
CREATE PROCEDURE [dbo].[GetStudentHasTransferGrade]
    @StuEnrollId UNIQUEIDENTIFIER
   ,@clsSectionId UNIQUEIDENTIFIER
AS
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;    
        DECLARE @StudentId UNIQUEIDENTIFIER;       

        SET @StudentId = (
                           SELECT TOP 1
                                    StudentId
                           FROM     dbo.arStuEnrollments
                           WHERE    StuEnrollId = @StuEnrollId
                         );    

	
        DECLARE @reqId UNIQUEIDENTIFIER;
        SET @reqId = (
                       SELECT   ReqId
                       FROM     dbo.arClassSections
                       WHERE    ClsSectionId = @clsSectionId
                     );

        SELECT  COUNT(*) AS TransferCount
        FROM    dbo.arTransferGrades
        WHERE   ReqId = @reqId
                AND StuEnrollId IN ( SELECT StuEnrollId
                                     FROM   dbo.arStuEnrollments
                                     WHERE  StudentId = @StudentId );	

    END;




GO
