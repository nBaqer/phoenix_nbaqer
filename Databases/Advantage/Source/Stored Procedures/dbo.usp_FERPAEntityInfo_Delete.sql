SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_FERPAEntityInfo_Delete]
    @FERPAEntityID UNIQUEIDENTIFIER
AS
    SET NOCOUNT ON;
    DELETE  FROM arFERPAEntity
    WHERE   FERPAEntityID = @FERPAEntityID;



GO
