SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_AttemptedClass_ClassStartDate]
    @StuEnrollId UNIQUEIDENTIFIER
   ,@ClsSectionId UNIQUEIDENTIFIER
AS
    BEGIN
        SELECT TOP 1
                ClassStartDate
        FROM    (
                  SELECT TOP 1
                            Class.Startdate AS ClassStartDate
                  FROM      arresults results
                  INNER JOIN arClassSections Class ON results.testid = Class.ClsSectionId
                  INNER JOIN arTerm Term ON Class.TermId = Term.TermId
                  WHERE     results.stuenrollid = @StuEnrollId
                            AND results.TestId = @ClsSectionId
                  UNION
                  SELECT TOP 1
                            t4.startDate AS ClassStartDate
                  FROM      arTransferGrades t9
                  INNER JOIN arTerm Term ON t9.TermId = Term.termId
                  INNER JOIN arClassSections t4 ON t4.TermId = t9.TermId
                                                   AND t4.ReqId = t9.ReqId
                  WHERE     t9.StuEnrollId = @StuEnrollId
                            AND t4.ClsSectionId = @ClsSectionId
                ) tblDerived;
    END;



GO
