SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[RS_ProgramDetails_ProgVersion]
	-- Add the parameters for the stored procedure here
    @PrgVerId VARCHAR(38) = '5FEF3CF7-5B12-4942-A1F1-8B87E01997C9' -- Electrolysis (historical)
    --,@CampusId UNIQUEIDENTIFIER = 'DC42A60A-5EB9-49B6-ADF6-535966F2E34A' -- Hieleach
AS
    BEGIN
	
        SET NOCOUNT ON;
        DECLARE @PrgVerGuid UNIQUEIDENTIFIER = CAST(@PrgVerId AS UNIQUEIDENTIFIER);
        SELECT  pro.ProgDescrip
               ,cal.ACDescrip AS AttendanceUnitType       -- Attendance Unit Type
               ,ver.PrgVerDescrip
               ,ver.PrgVerCode
               ,ver.TotalCost
               ,ver.Weeks
               ,ver.PayPeriodPerAcYear
               ,ver.Credits
               ,ver.Terms
               ,ver.Hours
               ,ver.AcademicYearLength
               ,ver.AcademicYearWeeks
               ,sap.SAPDescrip
               ,deg.DegreeDescrip
               ,bill.BillingMethodDescrip
               ,grad.Descrip AS GradeSystem
        FROM    arPrgVersions ver
        JOIN    arPrograms pro ON pro.ProgId = ver.ProgId
        JOIN    arSAP sap ON sap.SAPId = ver.SAPId
        JOIN    arDegrees deg ON deg.DegreeId = ver.DegreeId
        JOIN    saBillingMethods bill ON bill.BillingMethodId = ver.BillingMethodId
        JOIN    syAcademicCalendars cal ON cal.ACId = pro.ACId
        JOIN    arGradeSystems grad ON grad.GrdSystemId = ver.GrdSystemId
        WHERE   ver.PrgVerId = @PrgVerId;
    END;




GO
