SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_GetAllActiveAndInActiveRooms] @campusId VARCHAR(50)
AS
    SET NOCOUNT ON;
    SELECT  a.BldgId
           ,b.RoomId
           ,b.Capacity
           ,c.CampusId
           ,b.Descrip
    FROM    arBuildings a
           ,arRooms b
           ,syCmpGrpCmps c
           ,SyStatuses S
    WHERE   a.BldgId = b.BldgId
            AND b.StatusId = S.StatusId
            AND a.CampGrpId = c.CampGrpId
            AND c.CampusId = @campusId
    ORDER BY b.Descrip; 





GO
