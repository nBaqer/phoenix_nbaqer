SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =========================================================================================================
-- USP_PR_Main_Sub1
-- =========================================================================================================
CREATE PROCEDURE [dbo].[Usp_PR_Main_Sub1]
    @StuEnrollId VARCHAR(MAX) = NULL
   ,@CampGrpId VARCHAR(MAX) = NULL
   ,@PrgVerId VARCHAR(MAX) = NULL
   ,@StatusCodeId VARCHAR(MAX) = NULL
   ,@TermId VARCHAR(MAX) = NULL
   ,@TermStartDate VARCHAR(50) = NULL
   ,@TermStartDateModifier VARCHAR(10) = NULL
   ,@StudentGrpId VARCHAR(MAX) = NULL
   ,@OrderBy VARCHAR(100) = NULL
   ,@ExpectedGradDateModifier VARCHAR(10) = NULL
   ,@ExpectedGradDate VARCHAR(50) = NULL
   ,@CampusId VARCHAR(MAX) = NULL
   ,@ShowAllEnrollments BIT = 0
AS
    BEGIN

        CREATE TABLE #Temp1
            (
             StuEnrollId UNIQUEIDENTIFIER
            ,EnrollmentId VARCHAR(50)
            ,EnrollStartDate DATETIME
            ,CampusId UNIQUEIDENTIFIER
            ,StudentId UNIQUEIDENTIFIER
            ,SSN VARCHAR(50)
            ,StuFirstName VARCHAR(50)
            ,StuLastName VARCHAR(50)
            ,StuMiddleName VARCHAR(50)
            ,StudentNumber VARCHAR(50)
            ,StuEmail VARCHAR(50)
            ,StuAddress1 VARCHAR(50)
            ,StuAddress2 VARCHAR(50)
            ,StuCity VARCHAR(50)
            ,StuStateDescrip VARCHAR(80)
            ,StuZIP VARCHAR(50)
            ,StuPhone VARCHAR(50)
            ,StuEnrollIdList VARCHAR(MAX)
            );
        CREATE TABLE #Temp2
            (
             StuEnrollId UNIQUEIDENTIFIER
            ,EnrollmentId VARCHAR(50)
            ,EnrollStartDate DATETIME
            ,CampusId UNIQUEIDENTIFIER
            ,StudentId UNIQUEIDENTIFIER
            ,SSN VARCHAR(50)
            ,StuFirstName VARCHAR(50)
            ,StuLastName VARCHAR(50)
            ,StuMiddleName VARCHAR(50)
            ,StudentNumber VARCHAR(50)
            ,StuEmail VARCHAR(50)
            ,StuAddress1 VARCHAR(50)
            ,StuAddress2 VARCHAR(50)
            ,StuCity VARCHAR(50)
            ,StuStateDescrip VARCHAR(80)
            ,StuZIP VARCHAR(50)
            ,StuPhone VARCHAR(50)
            ,StuEnrollIdList VARCHAR(MAX)
            );


        IF @TermStartDate IS NULL
            OR @TermStartDate = ''
            BEGIN
                SET @TermStartDate = CONVERT(VARCHAR(10),GETDATE(),120);
            END; 

        INSERT  INTO #Temp1
                SELECT  StuEnrollId
                       ,EnrollmentId
                       ,EnrollStartDate
                       ,CampusId
                       ,StudentId
                       ,dbo.UDF_FormatSSN(SSN) AS SSN
                       ,StuFirstName
                       ,StuLastName
                       ,StuMiddleName
                       ,StudentNumber
                       ,StuEmail
                       ,StuAddress1
                       ,StuAddress2
                       ,StuCity
                       ,StuStateDescrip
                       ,StuZIP
                       ,ISNULL(StuPhone,'') AS StuPhone
                       ,'' AS StuEnrollIdList
                FROM    (
                          --SELECT DISTINCT
                          SELECT    SE.StuEnrollId
                                   ,SE.EnrollmentId
                                   ,SE.StartDate AS EnrollStartDate
                                   ,SE.CampusId
                                   ,AST.StudentId
                                   ,AST.SSN
                                   ,AST.FirstName AS StuFirstName
                                   ,AST.LastName AS StuLastName
                                   ,AST.MiddleName AS StuMiddleName
                                   ,AST.StudentNumber AS StudentNumber
                                   ,AST.HomeEmail AS StuEmail
                                   ,ASA.Address1 AS StuAddress1
                                   ,ASA.Address2 AS StuAddress2
                                   ,ASA.City AS StuCity
                                   ,ASA.StateDescrip AS StuStateDescrip
                                   ,ASA.Zip AS StuZIP
                                   ,ASP.Phone AS StuPhone
                          FROM      arStuEnrollments SE
                          INNER JOIN syCmpGrpCmps CGC ON CGC.CampusId = SE.CampusId
                          INNER JOIN syStatusCodes SC ON SC.StatusCodeId = SE.StatusCodeId
                          INNER JOIN arPrgVersions PV ON PV.PrgVerId = SE.PrgVerId
                          INNER JOIN arStudent AS AST ON AST.StudentId = SE.StudentId
                          LEFT OUTER JOIN (
                                            SELECT  ASA1.StudentId
                                                   ,ASA1.Address1
                                                   ,ASA1.Address2
                                                   ,ASA1.City
                                                   ,SS2.StateDescrip
                                                   ,ASA1.Zip
                                                   ,SS.Status
                                            FROM    arStudAddresses AS ASA1
                                            INNER JOIN syStatuses AS SS ON SS.StatusId = ASA1.StatusId
                                            LEFT OUTER JOIN syStates AS SS2 ON SS2.StateId = ASA1.StateId
                                            WHERE   SS.Status = 'Active'
                                                    AND ASA1.default1 = 1
                                          ) AS ASA ON ASA.StudentId = AST.StudentId
                          LEFT OUTER JOIN (
                                            SELECT  ASP1.StudentId
                                                   ,ASP1.Phone
                                                   ,ASP1.StatusId
                                                   ,SS1.Status
                                            FROM    arStudentPhone AS ASP1
                                            INNER JOIN syStatuses AS SS1 ON SS1.StatusId = ASP1.StatusId
                                            WHERE   SS1.Status = 'Active'
                                                    AND ASP1.default1 = 1
                                          ) AS ASP ON ASP.StudentId = AST.StudentId
                          INNER JOIN arResults R ON R.StuEnrollId = SE.StuEnrollId
                          INNER JOIN arClassSections CS ON CS.ClsSectionId = R.TestId
                          INNER JOIN arTerm T ON T.TermId = CS.TermId
                          LEFT OUTER JOIN adLeadByLeadGroups LLG ON LLG.StuEnrollId = SE.StuEnrollId
                          WHERE     (
                                      @CampGrpId IS NULL
                                      OR CGC.CampGrpId IN ( SELECT  Val
                                                            FROM    MultipleValuesForReportParameters(@CampGrpId,',',1) )
                                    )
                                    AND (
                                          @StatusCodeId IS NULL
                                          OR SC.StatusCodeId IN ( SELECT    Val
                                                                  FROM      MultipleValuesForReportParameters(@StatusCodeId,',',1) )
                                        )
                                    AND (
                                          @PrgVerId IS NULL
                                          OR SE.PrgVerId IN ( SELECT    Val
                                                              FROM      MultipleValuesForReportParameters(@PrgVerId,',',1) )
                                        )
                                    AND
					-- if A Then B is equivalent to (Not A) or B
                                    (
                                      @TermStartDate IS NULL
                                      OR @TermStartDateModifier IS NULL
									  OR T.StartDate IS NULL
                                      OR (
                                           (
                                             ( @TermStartDateModifier <> '=' )
                                             OR ( T.StartDate = @TermStartDate )
                                           )
                                           AND (
                                                 ( @TermStartDateModifier <> '>' )
                                                 OR ( T.StartDate > @TermStartDate )
                                               )
                                           AND (
                                                 ( @TermStartDateModifier <> '<' )
                                                 OR ( T.StartDate < @TermStartDate )
                                               )
                                           AND (
                                                 ( @TermStartDateModifier <> '>=' )
                                                 OR ( T.StartDate >= @TermStartDate )
                                               )
                                           AND (
                                                 ( @TermStartDateModifier <> '<=' )
                                                 OR ( T.StartDate <= @TermStartDate )
                                               )
                                         )
                                    )
                                    AND (
                                          @ExpectedGradDate IS NULL
                                          OR @ExpectedGradDateModifier IS NULL
                                          OR (
                                               (
                                                 ( @ExpectedGradDateModifier <> '=' )
                                                 OR ( SE.ExpGradDate = @ExpectedGradDate )
                                               )
                                               AND (
                                                     ( @ExpectedGradDateModifier <> '>' )
                                                     OR ( SE.ExpGradDate > @ExpectedGradDate )
                                                   )
                                               AND (
                                                     ( @ExpectedGradDateModifier <> '<' )
                                                     OR ( SE.ExpGradDate < @ExpectedGradDate )
                                                   )
                                               AND (
                                                     ( @ExpectedGradDateModifier <> '>=' )
                                                     OR ( SE.ExpGradDate >= @ExpectedGradDate )
                                                   )
                                               AND (
                                                     ( @ExpectedGradDateModifier <> '<=' )
                                                     OR ( SE.ExpGradDate <= @ExpectedGradDate )
                                                   )
                                             )
                                        )
                                    AND (
                                          @StuEnrollId IS NULL
                                          OR SE.StuEnrollId IN ( SELECT Val
                                                                 FROM   MultipleValuesForReportParameters(@StuEnrollId,',',1) )
                                        )
                                    AND (
                                          @TermId IS NULL
                                          OR T.TermId IN ( SELECT   Val
                                                           FROM     MultipleValuesForReportParameters(@TermId,',',1) )
                                        )
                                    AND (
                                          @StudentGrpId IS NULL
                                          OR LLG.LeadGrpId IN ( SELECT  Val
                                                                FROM    MultipleValuesForReportParameters(@StudentGrpId,',',1) )
                                        )
                                    AND (
                                          @CampusId IS NULL
                                          OR SE.CampusId IN ( SELECT    Val
                                                              FROM      MultipleValuesForReportParameters(@CampusId,',',1) )
                                        )
                          UNION
				 --SELECT DISTINCT
                          SELECT    SE.StuEnrollId
                                   ,SE.EnrollmentId
                                   ,SE.StartDate AS EnrollStartDate
                                   ,SE.CampusId
                                   ,AST.StudentId
                                   ,AST.SSN
                                   ,AST.FirstName AS StuFirstName
                                   ,AST.LastName AS StuLastName
                                   ,AST.MiddleName AS StuMiddleName
                                   ,AST.StudentNumber AS StudentNumber
                                   ,AST.HomeEmail AS StuEmail
                                   ,ASA.Address1 AS StuAddress1
                                   ,ASA.Address2 AS StuAddress2
                                   ,ASA.City AS StuCity
                                   ,ASA.StateDescrip AS StuStateDescrip
                                   ,ASA.Zip AS StuZIP
                                   ,ASP.Phone AS StuPhone
                          FROM      arStuEnrollments SE
                          INNER JOIN syCmpGrpCmps CGC ON CGC.CampusId = SE.CampusId
                          INNER JOIN syStatusCodes SC ON SC.StatusCodeId = SE.StatusCodeId
                          INNER JOIN arPrgVersions PV ON PV.PrgVerId = SE.PrgVerId
                          INNER JOIN arStudent AS AST ON AST.StudentId = SE.StudentId
                          LEFT OUTER JOIN (
                                            SELECT  ASA1.StudentId
                                                   ,ASA1.Address1
                                                   ,ASA1.Address2
                                                   ,ASA1.City
                                                   ,SS2.StateDescrip
                                                   ,ASA1.Zip
                                                   ,SS.Status
                                            FROM    arStudAddresses AS ASA1
                                            INNER JOIN syStatuses AS SS ON SS.StatusId = ASA1.StatusId
                                            LEFT OUTER JOIN syStates AS SS2 ON SS2.StateId = ASA1.StateId
                                            WHERE   SS.Status = 'Active'
                                                    AND ASA1.default1 = 1
                                          ) AS ASA ON ASA.StudentId = AST.StudentId
                          LEFT OUTER JOIN (
                                            SELECT  ASP1.Phone
                                                   ,ASP1.StatusId
                                                   ,SS1.Status
                                            FROM    arStudentPhone AS ASP1
                                            INNER JOIN syStatuses AS SS1 ON SS1.StatusId = ASP1.StatusId
                                            WHERE   SS1.Status = 'Active'
                                                    AND ASP1.default1 = 1
                                          ) AS ASP ON ASP.StatusId = AST.StudentId
                          INNER JOIN arTransferGrades R ON R.StuEnrollId = SE.StuEnrollId
                          INNER JOIN arReqs RQ ON RQ.ReqId = R.ReqId
                          INNER JOIN arTerm T ON T.TermId = R.TermId
                          LEFT OUTER JOIN adLeadByLeadGroups LLG ON LLG.StuEnrollId = SE.StuEnrollId
                          WHERE     (
                                      @CampGrpId IS NULL
                                      OR CGC.CampGrpId IN ( SELECT  Val
                                                            FROM    MultipleValuesForReportParameters(@CampGrpId,',',1) )
                                    )
                                    AND (
                                          @StatusCodeId IS NULL
                                          OR SC.StatusCodeId IN ( SELECT    Val
                                                                  FROM      MultipleValuesForReportParameters(@StatusCodeId,',',1) )
                                        )
                                    AND (
                                          @PrgVerId IS NULL
                                          OR SE.PrgVerId IN ( SELECT    Val
                                                              FROM      MultipleValuesForReportParameters(@PrgVerId,',',1) )
                                        )
                                    AND
					-- if A Then B is equivalent to (Not A) or B
                                    (
                                      @TermStartDate IS NULL
                                      OR @TermStartDateModifier IS NULL
									  OR T.StartDate IS NULL
                                      OR (
                                           (
                                             ( @TermStartDateModifier <> '=' )
                                             OR ( T.StartDate = @TermStartDate )
                                           )
                                           AND (
                                                 ( @TermStartDateModifier <> '>' )
                                                 OR ( T.StartDate > @TermStartDate )
                                               )
                                           AND (
                                                 ( @TermStartDateModifier <> '<' )
                                                 OR ( T.StartDate < @TermStartDate )
                                               )
                                           AND (
                                                 ( @TermStartDateModifier <> '>=' )
                                                 OR ( T.StartDate >= @TermStartDate )
                                               )
                                           AND (
                                                 ( @TermStartDateModifier <> '<=' )
                                                 OR ( T.StartDate <= @TermStartDate )
                                               )
                                         )
                                    )
                                    AND (
                                          @ExpectedGradDate IS NULL
                                          OR @ExpectedGradDateModifier IS NULL
                                          OR (
                                               (
                                                 ( @ExpectedGradDateModifier <> '=' )
                                                 OR ( SE.ExpGradDate = @ExpectedGradDate )
                                               )
                                               AND (
                                                     ( @ExpectedGradDateModifier <> '>' )
                                                     OR ( SE.ExpGradDate > @ExpectedGradDate )
                                                   )
                                               AND (
                                                     ( @ExpectedGradDateModifier <> '<' )
                                                     OR ( SE.ExpGradDate < @ExpectedGradDate )
                                                   )
                                               AND (
                                                     ( @ExpectedGradDateModifier <> '>=' )
                                                     OR ( SE.ExpGradDate >= @ExpectedGradDate )
                                                   )
                                               AND (
                                                     ( @ExpectedGradDateModifier <> '<=' )
                                                     OR ( SE.ExpGradDate <= @ExpectedGradDate )
                                                   )
                                             )
                                        )
                                    AND (
                                          @StuEnrollId IS NULL
                                          OR SE.StuEnrollId IN ( SELECT Val
                                                                 FROM   MultipleValuesForReportParameters(@StuEnrollId,',',1) )
                                        )
                                    AND (
                                          @TermId IS NULL
                                          OR T.TermId IN ( SELECT   Val
                                                           FROM     MultipleValuesForReportParameters(@TermId,',',1) )
                                        )
                                    AND (
                                          @StudentGrpId IS NULL
                                          OR LLG.LeadGrpId IN ( SELECT  Val
                                                                FROM    MultipleValuesForReportParameters(@StudentGrpId,',',1) )
                                        )
                                    AND (
                                          @CampusId IS NULL
                                          OR SE.CampusId IN ( SELECT    Val
                                                              FROM      MultipleValuesForReportParameters(@CampusId,',',1) )
                                        )
                        ) dt
                ORDER BY dt.StudentId
                       ,dt.StuEnrollId;

		-- if the parameter ShowAllEnrollmnet is True we will update the table #Temp1 with all enrollments for student selected
        IF @ShowAllEnrollments = 1
            BEGIN
                INSERT  INTO #Temp2
                        SELECT  ASE.StuEnrollId
                               ,ASE.EnrollmentId
                               ,ASE.StartDate AS EnrollStartDate
                               ,ASE.CampusId
                               ,T.StudentId
                               ,T.SSN
                               ,T.StuFirstName
                               ,T.StuLastName
                               ,T.StuMiddleName
                               ,T.StudentNumber
                               ,T.StuEmail
                               ,T.StuAddress1
                               ,T.StuAddress2
                               ,T.StuCity
                               ,T.StuStateDescrip
                               ,T.StuZIP
                               ,ISNULL(T.StuPhone,'') AS StuPhone
                               ,'' AS StuEnrollIdList
                        FROM    #Temp1 AS T
                        INNER JOIN arStudent AS AST ON AST.StudentId = T.StudentId
                        INNER JOIN arStuEnrollments AS ASE ON ASE.StudentId = AST.StudentId;

                DELETE  #Temp1;
			   
                INSERT  INTO #Temp1
                        (
                         StuEnrollId
                        ,EnrollmentId
                        ,EnrollStartDate
                        ,CampusId
                        ,StudentId
                        ,SSN
                        ,StuFirstName
                        ,StuLastName
                        ,StuMiddleName
                        ,StudentNumber
                        ,StuEmail
                        ,StuAddress1
                        ,StuAddress2
                        ,StuCity
                        ,StuStateDescrip
                        ,StuZIP
                        ,StuPhone
                        ,StuEnrollIdList
						)
                        SELECT  StuEnrollId
                               ,EnrollmentId
                               ,EnrollStartDate
                               ,CampusId
                               ,StudentId
                               ,SSN
                               ,StuFirstName
                               ,StuLastName
                               ,StuMiddleName
                               ,StudentNumber
                               ,StuEmail
                               ,StuAddress1
                               ,StuAddress2
                               ,StuCity
                               ,StuStateDescrip
                               ,StuZIP
                               ,ISNULL(StuPhone,'') AS StuPhone
                               ,StuEnrollIdList
                        FROM    #Temp2;

                DELETE  #Temp2;
                DROP TABLE #Temp2;
            END; 
		--
		-- Create stuEnrollIdList for all student selected
        UPDATE  T4
        SET     T4.StuEnrollIdList = T2.List
        FROM    #Temp1 AS T4
        INNER JOIN (
                     SELECT T1.StudentId
                           ,List = LEFT(T3.List,LEN(T3.List) - 1)
                     FROM   #Temp1 AS T1
                     CROSS APPLY (
                                   SELECT   CONVERT(VARCHAR(50),T2.StuEnrollId) + ','
                                   FROM     #Temp1 AS T2
                                   WHERE    T2.StudentId = T1.StudentId
                                 FOR
                                   XML PATH('')
                                 ) T3 ( List )
                   ) T2 ON T2.StudentId = T4.StudentId;
 
        SELECT  T.StuEnrollId
               ,T.EnrollmentId
               ,T.EnrollStartDate
               ,T.CampusId
               ,T.StudentId
               ,T.SSN
               ,T.StuFirstName
               ,T.StuLastName
               ,T.StuMiddleName
               ,T.StudentNumber
               ,T.StuEmail
               ,T.StuAddress1
               ,T.StuAddress2
               ,T.StuCity
               ,T.StuStateDescrip
               ,T.StuZIP
               ,ISNULL(T.StuPhone,'') AS StuPhone
               ,T.StuEnrollIdList
        FROM    #Temp1 AS T
        ORDER BY --dt.CampDescrip
			  --, 
                CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'LastName Asc,FirstName Asc,MiddleName Asc'
                     THEN ( RANK() OVER ( ORDER BY StuLastName, StuFirstName, StuMiddleName ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'LastName Asc,FirstName Asc,MiddleName Desc'
                     THEN ( RANK() OVER ( ORDER BY StuLastName, StuFirstName, StuMiddleName DESC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'LastName Asc,FirstName Desc,MiddleName Asc'
                     THEN ( RANK() OVER ( ORDER BY StuLastName, StuFirstName DESC, StuMiddleName ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'LastName Asc,FirstName Desc,MiddleName Desc'
                     THEN ( RANK() OVER ( ORDER BY StuLastName, StuFirstName DESC, StuMiddleName DESC ) )
                END
               ,
			 --LD
                CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'LastName Desc,FirstName Asc,MiddleName Asc'
                     THEN ( RANK() OVER ( ORDER BY StuLastName DESC, StuFirstName, StuMiddleName ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'LastName Desc,FirstName Asc,MiddleName Desc'
                     THEN ( RANK() OVER ( ORDER BY StuLastName DESC, StuFirstName, StuMiddleName DESC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'LastName Desc,FirstName Desc,MiddleName Asc'
                     THEN ( RANK() OVER ( ORDER BY StuLastName DESC, StuFirstName DESC, StuMiddleName ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'LastName Desc,FirstName Desc,MiddleName Desc'
                     THEN ( RANK() OVER ( ORDER BY StuLastName DESC, StuFirstName DESC, StuMiddleName DESC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'LastName Asc,MiddleName Asc,FirstName Asc'
                     THEN ( RANK() OVER ( ORDER BY StuLastName ASC, StuMiddleName ASC, StuFirstName ASC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'LastName Asc,MiddleName Asc,FirstName Desc'
                     THEN ( RANK() OVER ( ORDER BY StuLastName ASC, StuMiddleName ASC, StuFirstName DESC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'LastName Asc,MiddleName Desc,FirstName Asc'
                     THEN ( RANK() OVER ( ORDER BY StuLastName ASC, StuMiddleName DESC, StuFirstName ASC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'LastName Asc,MiddleName Desc,FirstName Desc'
                     THEN ( RANK() OVER ( ORDER BY StuLastName ASC, StuMiddleName DESC, StuFirstName DESC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'LastName Desc,MiddleName Asc,FirstName Asc'
                     THEN ( RANK() OVER ( ORDER BY StuLastName DESC, StuMiddleName ASC, StuFirstName ASC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'LastName Desc,MiddleName Asc,FirstName Desc'
                     THEN ( RANK() OVER ( ORDER BY StuLastName DESC, StuMiddleName ASC, StuFirstName DESC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'LastName Desc,MiddleName Desc,FirstName Asc'
                     THEN ( RANK() OVER ( ORDER BY StuLastName DESC, StuMiddleName DESC, StuFirstName ASC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'LastName Desc,MiddleName Desc,FirstName Desc'
                     THEN ( RANK() OVER ( ORDER BY StuLastName DESC, StuMiddleName DESC, StuFirstName DESC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'FirstName Asc,LastName Asc,MiddleName Asc'
                     THEN ( RANK() OVER ( ORDER BY StuFirstName ASC, StuLastName ASC, StuMiddleName ASC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'FirstName Asc,LastName Asc,MiddleName Desc'
                     THEN ( RANK() OVER ( ORDER BY StuFirstName ASC, StuLastName ASC, StuMiddleName DESC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'FirstName Asc,LastName Desc,MiddleName Asc'
                     THEN ( RANK() OVER ( ORDER BY StuFirstName ASC, StuLastName DESC, StuMiddleName ASC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'FirstName Asc,LastName Desc,MiddleName Desc'
                     THEN ( RANK() OVER ( ORDER BY StuFirstName ASC, StuLastName DESC, StuMiddleName DESC ) )
                END
               ,
			 --LD
                CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'FirstName Desc,LastName Asc,MiddleName Asc'
                     THEN ( RANK() OVER ( ORDER BY StuFirstName DESC, StuLastName ASC, StuMiddleName ASC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'FirstName Desc,LastName Asc,MiddleName Desc'
                     THEN ( RANK() OVER ( ORDER BY StuFirstName DESC, StuLastName ASC, StuMiddleName DESC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'FirstName Desc,LastName Desc,MiddleName Asc'
                     THEN ( RANK() OVER ( ORDER BY StuFirstName DESC, StuLastName DESC, StuMiddleName ASC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'FirstName Desc,LastName Desc,StuMiddleName Desc'
                     THEN ( RANK() OVER ( ORDER BY StuFirstName DESC, StuLastName DESC, StuMiddleName DESC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'FirstName Asc,MiddleName Asc,LastName Asc'
                     THEN ( RANK() OVER ( ORDER BY StuFirstName ASC, StuMiddleName ASC, StuLastName ASC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'FirstName Asc,MiddleName Asc,LastName Desc'
                     THEN ( RANK() OVER ( ORDER BY StuFirstName ASC, StuMiddleName ASC, StuLastName DESC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'FirstName Asc,MiddleName Desc,LastName Asc'
                     THEN ( RANK() OVER ( ORDER BY StuFirstName ASC, StuMiddleName DESC, StuLastName ASC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'FirstName Asc,MiddleName Desc,LastName Desc'
                     THEN ( RANK() OVER ( ORDER BY StuFirstName ASC, StuMiddleName DESC, StuLastName DESC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'FirstName Desc,MiddleName Asc,LastName Asc'
                     THEN ( RANK() OVER ( ORDER BY StuFirstName DESC, StuMiddleName ASC, StuLastName ASC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'FirstName Desc,MiddleName Asc,LastName Desc'
                     THEN ( RANK() OVER ( ORDER BY StuFirstName DESC, StuMiddleName ASC, StuLastName DESC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'FirstName Desc,MiddleName Desc,LastName Asc'
                     THEN ( RANK() OVER ( ORDER BY StuFirstName DESC, StuMiddleName DESC, StuLastName ASC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'FirstName Desc,MiddleName Desc,LastName Desc'
                     THEN ( RANK() OVER ( ORDER BY StuFirstName DESC, StuMiddleName DESC, StuLastName DESC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'MiddleName Asc,LastName Asc,FirstName Asc'
                     THEN ( RANK() OVER ( ORDER BY StuMiddleName ASC, StuLastName ASC, StuFirstName ASC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'MiddleName Asc,LastName Asc,FirstName Desc'
                     THEN ( RANK() OVER ( ORDER BY StuMiddleName ASC, StuLastName ASC, StuFirstName DESC ) )
                END
               ,
			 
			 -- Start here
                CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'MiddleName Asc,LastName Desc,FirstName Asc'
                     THEN ( RANK() OVER ( ORDER BY StuMiddleName ASC, StuLastName DESC, StuFirstName ASC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'MiddleName Asc,LastName Desc,FirstName Desc'
                     THEN ( RANK() OVER ( ORDER BY StuMiddleName ASC, StuLastName DESC, StuFirstName DESC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'MiddleName Desc,LastName Asc,FirstName Asc'
                     THEN ( RANK() OVER ( ORDER BY StuMiddleName DESC, StuLastName ASC, StuFirstName ASC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'MiddleName Desc,LastName Asc,FirstName Desc'
                     THEN ( RANK() OVER ( ORDER BY StuMiddleName DESC, StuLastName ASC, StuFirstName DESC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'MiddleName Desc,LastName Desc,FirstName Asc'
                     THEN ( RANK() OVER ( ORDER BY StuMiddleName DESC, StuLastName DESC, StuFirstName ASC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'MiddleName Desc,LastName Desc,FirstName Desc'
                     THEN ( RANK() OVER ( ORDER BY StuMiddleName DESC, StuLastName DESC, StuFirstName DESC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'MiddleName Asc,FirstName Asc,LastName Asc'
                     THEN ( RANK() OVER ( ORDER BY StuMiddleName ASC, StuFirstName ASC, StuLastName ASC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'MiddleName Asc,FirstName Asc,LastName Desc'
                     THEN ( RANK() OVER ( ORDER BY StuMiddleName ASC, StuFirstName ASC, StuLastName DESC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'MiddleName Asc,FirstName Desc,LastName Asc'
                     THEN ( RANK() OVER ( ORDER BY StuMiddleName ASC, StuFirstName DESC, StuLastName ASC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'MiddleName Asc,FirstName Desc,LastName Desc'
                     THEN ( RANK() OVER ( ORDER BY StuMiddleName ASC, StuFirstName DESC, StuLastName DESC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'MiddleName Desc,FirstName Asc,LastName Asc'
                     THEN ( RANK() OVER ( ORDER BY StuMiddleName DESC, StuFirstName ASC, StuLastName ASC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'MiddleName Desc,FirstName Asc,LastName Desc'
                     THEN ( RANK() OVER ( ORDER BY StuMiddleName DESC, StuFirstName ASC, StuLastName DESC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'MiddleName Desc,FirstName Desc,LastName Asc'
                     THEN ( RANK() OVER ( ORDER BY StuMiddleName DESC, StuFirstName DESC, StuLastName ASC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'MiddleName Desc,FirstName Desc,LastName Desc'
                     THEN ( RANK() OVER ( ORDER BY StuMiddleName DESC, StuFirstName DESC, StuLastName DESC ) )
                END
               ,T.StudentId
               ,T.EnrollStartDate;        
    END;
-- =========================================================================================================
-- END  --  USP_PR_Main_Sub1
-- =========================================================================================================
GO
