SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[USP_ARProgramVersionChargeSequence_Insert]
    (
     @PrgVerId UNIQUEIDENTIFIER
    )
AS /*----------------------------------------------------------------------------------------------------
	Author          :	Saraswathi Lakshmanan
	
	Create date		:	27/05/2010
	
	Procedure Name	:	[USP_ARProgramVersionChargeSequence_Insert]

	Objective		:	Insert data into the table
	
	Parameters		:	Name			Type	Data Type			Required? 	
						=====			====	=========			=========	
						@PrgVerId	IN		Uniqueuidentifier	Yes
	
	Output			:	Returns the requested details	
						
*/-----------------------------------------------------------------------------------------------------
    BEGIN

        DECLARE @Credits_or_Hours AS DECIMAL;

        SET @Credits_or_Hours = (
                                  SELECT    MAX(CreditHourValues)
                                  FROM      (
                                              SELECT    credits AS CreditHourValues
                                              FROM      arprgversions
                                              WHERE     PrgVerId = @PrgVerId
                                              UNION
                                              SELECT    Hours AS CreditHourValues
                                              FROM      arprgversions
                                              WHERE     PrgVerId = @PrgVerId
                                            ) AS res
                                );

        IF @Credits_or_Hours = 0
            BEGIN
                RETURN; 
            END;
 

        DECLARE @Academicyearlength AS DECIMAL;
        DECLARE @payPeriodperAcademicYear AS DECIMAL;
        DECLARE @OnePaymentperiod AS DECIMAL;
        DECLARE @NUmberofpaymentPeriods AS DECIMAL;
        DECLARE @NumberofAcademicyears AS DECIMAL;
        SET @Academicyearlength = (
                                    SELECT  AcademicYearLength
                                    FROM    arPrgVersions
                                    WHERE   PrgVerId = @PrgVerId
                                  );

        SET @payPeriodperAcademicYear = (
                                          SELECT    PayPeriodPerAcYear
                                          FROM      arPrgVersions
                                          WHERE     PrgVerId = @PrgVerId
                                        );

        IF @Academicyearlength > 0
            BEGIN
                SET @NumberofAcademicyears = CEILING(@Credits_or_Hours / @Academicyearlength);
            END;
        ELSE
            BEGIN
                SET @NumberofAcademicyears = 0;
            END;
	
	
        IF @payPeriodperAcademicYear > 0
            BEGIN
                SET @OnePaymentperiod = ( @Academicyearlength / @payPeriodperAcademicYear );
                IF @OnePaymentperiod > 0
                    BEGIN
                        SET @NUmberofpaymentPeriods = CEILING(@Credits_or_Hours / @OnePaymentperiod);
                    END;
            END;
        ELSE
            BEGIN
                SET @NUmberofpaymentPeriods = 0;
            END; 


        DECLARE @i AS INT;
        SET @i = 1;
        WHILE ( @i <= @NumberofAcademicyears )
            BEGIN
                IF NOT EXISTS ( SELECT  *
                                FROM    arPrgChargePeriodSeq
                                WHERE   PrgVerId = @PrgVerId
                                        AND FeeLevelID = 4
                                        AND Sequence = @i )
                    BEGIN
	
                        INSERT  INTO arPrgChargePeriodSeq
                                (
                                 PrgChrPeriodSeqId
                                ,PrgVerId
                                ,FeeLevelID
                                ,Sequence
                                ,StartValue
                                ,Endvalue
                                )
                        VALUES  (
                                 NEWID()
                                ,@PrgVerId
                                ,4
                                ,@i
                                ,@i
                                ,@i
                                );
                    END;
                ELSE
                    BEGIN
                        UPDATE  arPrgChargePeriodSeq
                        SET     StartValue = @i
                               ,Endvalue = @i
                        WHERE   PrgVerId = @PrgVerId
                                AND FeeLevelID = 4
                                AND Sequence = @i;
                    END;
	
	
                SET @i = @i + 1;
            END;

        DECLARE @J AS INT;
        SET @J = 1;
        DECLARE @Startvalue AS DECIMAL;
        DECLARE @EndValue AS DECIMAL;

        WHILE ( @J <= @NUmberofpaymentPeriods )
            BEGIN
                IF @J = 1
                    BEGIN
                        SET @Startvalue = 1;
                        SET @EndValue = @OnePaymentperiod;
                    END;
                ELSE
                    BEGIN
                        SET @Startvalue = ( @OnePaymentperiod * ( @J - 1 ) ) + 1;
                        SET @EndValue = @OnePaymentperiod * @J;
                        IF @EndValue > @Credits_or_Hours
                            BEGIN
                                SET @EndValue = @Credits_or_Hours;
                            END;	
                    END;
                IF NOT EXISTS ( SELECT  *
                                FROM    arPrgChargePeriodSeq
                                WHERE   PrgVerId = @PrgVerId
                                        AND FeeLevelID = 5
                                        AND Sequence = @J )
                    BEGIN
                        INSERT  INTO arPrgChargePeriodSeq
                                (
                                 PrgChrPeriodSeqId
                                ,PrgVerId
                                ,FeeLevelID
                                ,Sequence
                                ,StartValue
                                ,Endvalue
                                )
                        VALUES  (
                                 NEWID()
                                ,@PrgVerId
                                ,5
                                ,@J
                                ,@Startvalue
                                ,@EndValue
                                );
                    END;
                ELSE
                    BEGIN
                        UPDATE  arPrgChargePeriodSeq
                        SET     StartValue = @Startvalue
                               ,Endvalue = @EndValue
                        WHERE   PrgVerId = @PrgVerId
                                AND FeeLevelID = 5
                                AND Sequence = @J;
                    END;
		
                SET @J = @J + 1;
            END;
    END;




GO
