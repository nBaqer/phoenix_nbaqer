SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[Usp_Advantage_MaintenanceNavigation_GetList]
    @SchoolEnumerator INT
AS
    BEGIN
	-- declare local variables
        DECLARE @ShowRossOnlyTabs BIT
           ,@SchedulingMethod VARCHAR(50)
           ,@TrackSAPAttendance VARCHAR(50);
        DECLARE @ShowCollegeOfCourtReporting VARCHAR(5)
           ,@FameESP VARCHAR(5)
           ,@EdExpress VARCHAR(5); 
        DECLARE @GradeBookWeightingLevel VARCHAR(20)
           ,@ShowExternshipTabs VARCHAR(5);
        DECLARE @AdmissionsResourceId INT
           ,@AcademicRecordsResourceId INT
           ,@StudentAccountsResourceId INT;
        DECLARE @FacultyResourceId INT
           ,@FinancialAidResourceId INT
           ,@PlacementResourceId INT;
        DECLARE @HumanResourcesResourceId INT
           ,@SystemResourceId INT; 
	--DECLARE @SchoolEnumerator INT
	--SET @SchoolEnumerator=1689
	
	-- Set Module ResourceIds
        SET @AdmissionsResourceId = 189;
        SET @AcademicRecordsResourceId = 26;
        SET @StudentAccountsResourceId = 194;
        SET @FacultyResourceId = 300;
        SET @FinancialAidResourceId = 191;
        SET @PlacementResourceId = 193;
        SET @HumanResourcesResourceId = 192;
        SET @SystemResourceId = 195;
	
	-- Get Values
        SET @ShowRossOnlyTabs = (
                                  SELECT    VALUE
                                  FROM      dbo.syConfigAppSetValues
                                  WHERE     SettingId = 68
                                );
        SET @SchedulingMethod = (
                                  SELECT    VALUE
                                  FROM      dbo.syConfigAppSetValues
                                  WHERE     SettingId = 65
                                );
        SET @TrackSAPAttendance = (
                                    SELECT  VALUE
                                    FROM    dbo.syConfigAppSetValues
                                    WHERE   SettingId = 72
                                  );
        SET @ShowCollegeOfCourtReporting = (
                                             SELECT VALUE
                                             FROM   dbo.syConfigAppSetValues
                                             WHERE  SettingId = 118
                                           );
        SET @FameESP = (
                         SELECT VALUE
                         FROM   dbo.syConfigAppSetValues
                         WHERE  SettingId = 37
                       );
        SET @EdExpress = (
                           SELECT   VALUE
                           FROM     dbo.syConfigAppSetValues
                           WHERE    SettingId = 91
                         );
        SET @GradeBookWeightingLevel = (
                                         SELECT VALUE
                                         FROM   dbo.syConfigAppSetValues
                                         WHERE  SettingId = 43
                                       );
        SET @ShowExternshipTabs = (
                                    SELECT  VALUE
                                    FROM    dbo.syConfigAppSetValues
                                    WHERE   SettingId = 71
                                  );

	-- The first column always refers to the Module Resource Id (@AdmissionsResourceId or @AcademicRecordsResourceId or .....)
	/******************** Admissions *****************************/
        SELECT  ModuleResourceId
               ,ChildResourceId
               ,ChildResource AS ChildResourceDescription
               ,ChildResourceURL
               ,ParentResourceId
        FROM    (
                  SELECT    @AdmissionsResourceId AS ModuleResourceId
                           ,@AdmissionsResourceId AS ChildResourceId
                           ,'Admissions' AS ChildResource
                           ,NULL AS ChildResourceURL
                           ,NULL ParentResourceId
                           ,
				--,NULL AS ParentResource,
                            1 AS ModuleSortOrder
                  UNION
                  SELECT    @AdmissionsResourceId AS ModuleResourceId
                           ,CONVERT(INT,RChild.ResourceId) AS ChildResourceId
                           ,RChild.Resource AS ChildResource
                           ,RChild.ResourceURL AS ChildResourceURL
                           ,CONVERT(INT,RParent.ResourceID) AS ParentResourceId
                           ,1 AS ModuleSortOrder
                  FROM      syResources RChild
                  INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
                  INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                  INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                  LEFT OUTER JOIN (
                                    SELECT  *
                                    FROM    syResources
                                    WHERE   ResourceTypeId = 1
                                  ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                  WHERE     RChild.ResourceTypeId IN ( 2,4 )
                            AND (
                                  RChild.ChildTypeId IS NULL
                                  OR RChild.ChildTypeId = 4
                                )
                            AND (
                                  NNParent.ResourceId IN ( 189,337,38,400,399,401 )
                                  OR 
					---- A maintenance page  Tab may be part of AR,Financial Aid, Student Accounts or Placement
					---- So the following condition is neccessary to bring in only items tied to AD
                                  ( NNParent.Parentid IN ( SELECT   HierarchyId
                                                           FROM     syNavigationNodes
                                                           WHERE    ResourceId = 189 ) )
                                )
                            AND NNParent.ResourceId NOT IN ( 469,38 )
                            AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
                  UNION ALL
                  SELECT    @AcademicRecordsResourceId AS ModuleResourceId
                           ,@AcademicRecordsResourceId AS ChildResourceId
                           ,'Academic Records' AS ChildResource
                           ,NULL AS ChildResourceURL
                           ,NULL ParentResourceId
                           ,2 AS ModuleSortOrder
                  UNION
                  SELECT    @AcademicRecordsResourceId AS ModuleResourceId
                           ,CONVERT(INT,RChild.ResourceId) AS ChildResourceId
                           ,RChild.Resource AS ChildResource
                           ,RChild.ResourceURL AS ChildResourceURL
                           ,CONVERT(INT,RParent.ResourceID) AS ParentResourceId
                           ,2 AS ModuleSortOrder
                  FROM      syResources RChild
                  INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
                  INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                  INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                  LEFT OUTER JOIN (
                                    SELECT  *
                                    FROM    syResources
                                    WHERE   ResourceTypeId = 1
                                  ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                  WHERE     RChild.ResourceTypeId IN ( 2,4 )
                            AND (
                                  RChild.ChildTypeId IS NULL
                                  OR RChild.ChildTypeId = 4
                                )
                            AND (
                                  NNParent.ResourceId IN ( 26,468,469,621,471,470 )
                                  OR 
					---- A maintenance page  Tab may be part of AR,Financial Aid, Student Accounts or Placement
					---- So the following condition is neccessary to bring in only items tied to AD
                                  ( NNParent.Parentid IN ( SELECT   HierarchyId
                                                           FROM     syNavigationNodes
                                                           WHERE    ResourceId = 26 ) )
                                )
                            AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
                  UNION ALL
                  SELECT    @StudentAccountsResourceId AS ModuleResourceId
                           ,@StudentAccountsResourceId AS ChildResourceId
                           ,'Student Accounts' AS ChildResource
                           ,NULL AS ChildResourceURL
                           ,NULL ParentResourceId
                           ,3 AS ModuleSortOrder
                  UNION
                  SELECT    @StudentAccountsResourceId AS ModuleResourceId
                           ,CONVERT(INT,RChild.ResourceId) AS ChildResourceId
                           ,RChild.Resource AS ChildResource
                           ,RChild.ResourceURL AS ChildResourceURL
                           ,CONVERT(INT,RParent.ResourceID) AS ParentResourceId
                           ,3 AS ModuleSortOrder
                  FROM      syResources RChild
                  INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
                  INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                  INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                  LEFT OUTER JOIN (
                                    SELECT  *
                                    FROM    syResources
                                    WHERE   ResourceTypeId = 1
                                  ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                  WHERE     RChild.ResourceTypeId IN ( 2,4 )
                            AND (
                                  RChild.ChildTypeId IS NULL
                                  OR RChild.ChildTypeId = 4
                                )
                            AND (
                                  NNParent.ResourceId IN ( 194 )
                                  OR 
					---- A maintenance page  Tab may be part of AR,Financial Aid, Student Accounts or Placement
					---- So the following condition is neccessary to bring in only items tied to AD
                                  ( NNParent.Parentid IN ( SELECT   HierarchyId
                                                           FROM     syNavigationNodes
                                                           WHERE    ResourceId = 194 ) )
                                )
                            AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
                  UNION ALL
                  SELECT    @FacultyResourceId AS ModuleResourceId
                           ,@FacultyResourceId AS ChildResourceId
                           ,'Faculty' AS ChildResource
                           ,NULL AS ChildResourceURL
                           ,NULL ParentResourceId
                           ,4 AS ModuleSortOrder
                  UNION
                  SELECT    @FacultyResourceId AS ModuleResourceId
                           ,CONVERT(INT,RChild.ResourceId) AS ChildResourceId
                           ,RChild.Resource AS ChildResource
                           ,RChild.ResourceURL AS ChildResourceURL
                           ,CONVERT(INT,RParent.ResourceID) AS ParentResourceId
                           ,4 AS ModuleSortOrder
                  FROM      syResources RChild
                  INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
                  INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                  INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                  LEFT OUTER JOIN (
                                    SELECT  *
                                    FROM    syResources
                                    WHERE   ResourceTypeId = 1
                                  ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                  WHERE     RChild.ResourceTypeId IN ( 2,4 )
                            AND (
                                  RChild.ChildTypeId IS NULL
                                  OR RChild.ChildTypeId = 4
                                )
                            AND (
                                  NNParent.ResourceId IN ( 300 )
                                  OR 
					---- A maintenance page  Tab may be part of AR,Financial Aid, Student Accounts or Placement
					---- So the following condition is neccessary to bring in only items tied to AD
                                  ( NNParent.Parentid IN ( SELECT   HierarchyId
                                                           FROM     syNavigationNodes
                                                           WHERE    ResourceId = 300 ) )
                                )
                            AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
                  UNION ALL
                  SELECT    @FinancialAidResourceId AS ModuleResourceId
                           ,@FinancialAidResourceId AS ChildResourceId
                           ,'FinancialAid' AS ChildResource
                           ,NULL AS ChildResourceURL
                           ,NULL ParentResourceId
                           ,5 AS ModuleSortOrder
                  UNION
                  SELECT    @FinancialAidResourceId AS ModuleResourceId
                           ,CONVERT(INT,RChild.ResourceId) AS ChildResourceId
                           ,RChild.Resource AS ChildResource
                           ,RChild.ResourceURL AS ChildResourceURL
                           ,CONVERT(INT,RParent.ResourceID) AS ParentResourceId
                           ,5 AS ModuleSortOrder
                  FROM      syResources RChild
                  INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
                  INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                  INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                  LEFT OUTER JOIN (
                                    SELECT  *
                                    FROM    syResources
                                    WHERE   ResourceTypeId = 1
                                  ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                  WHERE     RChild.ResourceTypeId IN ( 2,4 )
                            AND (
                                  RChild.ChildTypeId IS NULL
                                  OR RChild.ChildTypeId = 4
                                )
                            AND (
                                  NNParent.ResourceId IN ( 191 )
                                  OR 
					---- A maintenance page  Tab may be part of AR,Financial Aid, Student Accounts or Placement
					---- So the following condition is neccessary to bring in only items tied to AD
                                  ( NNParent.Parentid IN ( SELECT   HierarchyId
                                                           FROM     syNavigationNodes
                                                           WHERE    ResourceId = 191 ) )
                                )
                            AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
                  UNION ALL
                  SELECT    @PlacementResourceId AS ModuleResourceId
                           ,@PlacementResourceId AS ChildResourceId
                           ,'Placement' AS ChildResource
                           ,NULL AS ChildResourceURL
                           ,NULL ParentResourceId
                           ,6 AS ModuleSortOrder
                  UNION
                  SELECT    @PlacementResourceId AS ModuleResourceId
                           ,CONVERT(INT,RChild.ResourceId) AS ChildResourceId
                           ,RChild.Resource AS ChildResource
                           ,RChild.ResourceURL AS ChildResourceURL
                           ,CONVERT(INT,RParent.ResourceID) AS ParentResourceId
                           ,6 AS ModuleSortOrder
                  FROM      syResources RChild
                  INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
                  INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                  INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                  LEFT OUTER JOIN (
                                    SELECT  *
                                    FROM    syResources
                                    WHERE   ResourceTypeId = 1
                                  ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                  WHERE     RChild.ResourceTypeId IN ( 2,4 )
                            AND (
                                  RChild.ChildTypeId IS NULL
                                  OR RChild.ChildTypeId = 4
                                )
                            AND (
                                  NNParent.ResourceId IN ( 193,404,406 )
                                  OR 
					---- A maintenance page  Tab may be part of AR,Financial Aid, Student Accounts or Placement
					---- So the following condition is neccessary to bring in only items tied to AD
                                  ( NNParent.Parentid IN ( SELECT   HierarchyId
                                                           FROM     syNavigationNodes
                                                           WHERE    ResourceId = 193 ) )
                                )
                            AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
                  UNION ALL
                  SELECT    @HumanResourcesResourceId AS ModuleResourceId
                           ,@HumanResourcesResourceId AS ChildResourceId
                           ,'Human Resources' AS ChildResource
                           ,NULL AS ChildResourceURL
                           ,NULL ParentResourceId
                           ,7 AS ModuleSortOrder
                  UNION
                  SELECT    @HumanResourcesResourceId AS ModuleResourceId
                           ,CONVERT(INT,RChild.ResourceId) AS ChildResourceId
                           ,RChild.Resource AS ChildResource
                           ,RChild.ResourceURL AS ChildResourceURL
                           ,CONVERT(INT,RParent.ResourceID) AS ParentResourceId
                           ,7 AS ModuleSortOrder
                  FROM      syResources RChild
                  INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
                  INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                  INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                  LEFT OUTER JOIN (
                                    SELECT  *
                                    FROM    syResources
                                    WHERE   ResourceTypeId = 1
                                  ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                  WHERE     RChild.ResourceTypeId IN ( 2,4 )
                            AND (
                                  RChild.ChildTypeId IS NULL
                                  OR RChild.ChildTypeId = 4
                                )
                            AND (
                                  NNParent.ResourceId IN ( 192 )
                                  OR 
					---- A maintenance page  Tab may be part of AR,Financial Aid, Student Accounts or Placement
					---- So the following condition is neccessary to bring in only items tied to AD
                                  ( NNParent.Parentid IN ( SELECT   HierarchyId
                                                           FROM     syNavigationNodes
                                                           WHERE    ResourceId = 192 ) )
                                )
                            AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
                  UNION
                  SELECT    @SystemResourceId AS ModuleResourceId
                           ,@SystemResourceId AS ChildResourceId
                           ,'System' AS ChildResource
                           ,NULL AS ChildResourceURL
                           ,NULL ParentResourceId
                           ,8 AS ModuleSortOrder
                  UNION
                  SELECT    @SystemResourceId AS ModuleResourceId
                           ,CONVERT(INT,RChild.ResourceId) AS ChildResourceId
                           ,RChild.Resource AS ChildResource
                           ,RChild.ResourceURL AS ChildResourceURL
                           ,CONVERT(INT,RParent.ResourceID) AS ParentResourceId
                           ,8 AS ModuleSortOrder
                  FROM      syResources RChild
                  INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
                  INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                  INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                  LEFT OUTER JOIN (
                                    SELECT  *
                                    FROM    syResources
                                    WHERE   ResourceTypeId = 1
                                  ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                  WHERE     RChild.ResourceTypeId IN ( 2,4 )
                            AND (
                                  RChild.ChildTypeId IS NULL
                                  OR RChild.ChildTypeId = 4
                                )
                            AND (
                                  NNParent.ResourceId IN ( 195 )
                                  OR 
					---- A maintenance page  Tab may be part of AR,Financial Aid, Student Accounts or Placement
					---- So the following condition is neccessary to bring in only items tied to AD
                                  ( NNParent.Parentid IN ( SELECT   HierarchyId
                                                           FROM     syNavigationNodes
                                                           WHERE    ResourceId = 195 ) )
                                )
                            AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
                ) MainQuery
        WHERE   -- Hide resources based on Configuration Settings
                (
                  -- The following expression means if showross... is set to false, hide 
					-- ResourceIds 541,542,532,534,535,538,543,539
					-- (Not False) OR (Condition)
				  (
                    ( @ShowRossOnlyTabs <> 0 )
                    OR ( ChildResourceId NOT IN ( 541,542,532,534,535,538,543,539 ) )
                  )
                  AND
					-- The following expression means if showross... is set to true, hide 
					-- ResourceIds 142,375,330,476,508,102,107,237
					-- (Not True) OR (Condition)
                  (
                    ( @ShowRossOnlyTabs <> 1 )
                    OR ( ChildResourceId NOT IN ( 142,375,330,476,508,102,107,237 ) )
                  )
                  AND
					---- The following expression means if SchedulingMethod=regulartraditional, hide 
					---- ResourceIds 91 and 497
					---- (Not True) OR (Condition)
                  (
                    (
                      NOT LOWER(LTRIM(RTRIM(@SchedulingMethod))) = 'regulartraditional'
                    )
                    OR ( ChildResourceId NOT IN ( 91,497 ) )
                  )
                  AND
					-- The following expression means if TrackSAPAttendance=byday, hide 
					-- ResourceIds 633
					-- (Not True) OR (Condition)
                  (
                    (
                      NOT LOWER(LTRIM(RTRIM(@TrackSAPAttendance))) = 'byday'
                    )
                    OR ( ChildResourceId NOT IN ( 633 ) )
                  )
                  AND
					---- The following expression means if @ShowCollegeOfCourtReporting is set to false, hide 
					---- ResourceIds 614,615
					---- (Not False) OR (Condition)
                  (
                    (
                      NOT LOWER(LTRIM(RTRIM(@ShowCollegeOfCourtReporting))) = 'no'
                    )
                    OR ( ChildResourceId NOT IN ( 614,615 ) )
                  )
                  AND
					-- The following expression means if @ShowCollegeOfCourtReporting is set to true, hide 
					-- ResourceIds 497
					-- (Not True) OR (Condition)
                  (
                    (
                      NOT LOWER(LTRIM(RTRIM(@ShowCollegeOfCourtReporting))) = 'yes'
                    )
                    OR ( ChildResourceId NOT IN ( 497 ) )
                  )
                  AND
					-- The following expression means if FAMEESP is set to false, hide 
					-- ResourceIds 517,523, 525
					-- (Not False) OR (Condition)
                  (
                    (
                      NOT LOWER(LTRIM(RTRIM(@FameESP))) = 'no'
                    )
                    OR ( ChildResourceId NOT IN ( 517,523,525 ) )
                  )
                  AND
					---- The following expression means if EDExpress is set to false, hide 
					---- ResourceIds 603,604,606,619
					---- (Not False) OR (Condition)
                  (
                    (
                      NOT LOWER(LTRIM(RTRIM(@EdExpress))) = 'no'
                    )
                    OR ( ChildResourceId NOT IN ( 603,604,605,606,619 ) )
                  )
                  AND
					---- The following expression means if @GradeBookWeightingLevel is set to courselevel, hide 
					---- ResourceIds 107,96,222
					---- (Not False) OR (Condition)
                  (
                    (
                      NOT LOWER(LTRIM(RTRIM(@GradeBookWeightingLevel))) = 'courselevel'
                    )
                    OR ( ChildResourceId NOT IN ( 107,96,222 ) )
                  )
                  AND (
                        (
                          NOT LOWER(LTRIM(RTRIM(@GradeBookWeightingLevel))) = 'instructorlevel'
                        )
                        OR ( ChildResourceId NOT IN ( 476 ) )
                      )
                  AND (
                        (
                          NOT LOWER(LTRIM(RTRIM(@ShowExternshipTabs))) = 'no'
                        )
                        OR ( ChildResourceId NOT IN ( 543,538 ) )
                      )
                )
        ORDER BY ModuleSortOrder
               ,ModuleResourceId
               ,ChildResource
               ,ParentResourceId; 
    END; 		



GO
