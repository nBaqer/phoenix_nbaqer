SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ginzo, John
-- Create date: 08/04/2015
-- Description:	update student schedule and badges
-- =============================================
CREATE PROCEDURE [dbo].[UpdateAssignStudentSchedule]
    @StuEnrollId UNIQUEIDENTIFIER
   ,@ScheduleId UNIQUEIDENTIFIER = NULL
   ,@Source VARCHAR(50)
   ,@Active BIT
   ,@ModUser VARCHAR(50)
   ,@BadgeNumber VARCHAR(50) = NULL
AS
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;

        BEGIN TRAN UPDATESCHEDULE;

        IF @BadgeNumber IS NOT NULL
            BEGIN
                UPDATE  arStuEnrollments
                SET     BadgeNumber = @BadgeNumber
                WHERE   StuEnrollId = @StuEnrollId;
            END;


        IF @ScheduleId IS NULL
            BEGIN
                DELETE  FROM dbo.arStudentSchedules
                WHERE   StuEnrollId = @StuEnrollId;	

                IF @@error <> 0
                    BEGIN
                        ROLLBACK TRAN UPDATESCHEDULE;
                        RETURN -1;
                    END;
            END;		

        ELSE
            BEGIN
			
                IF NOT EXISTS ( SELECT  @StuEnrollId
                                FROM    dbo.arStudentSchedules
                                WHERE   StuEnrollId = @StuEnrollId )
                    BEGIN
                        INSERT  INTO arStudentSchedules
                                (
                                 StuEnrollId
                                ,ScheduleId
                                ,Source
                                ,Active
                                ,ModUser
                                ,ModDate
								)
                        VALUES  (
                                 @StuEnrollId
                                ,@ScheduleId
                                ,'attendance'
                                ,@Active
                                ,@ModUser
                                ,GETDATE()
                                ); 

                        IF @@error <> 0
                            BEGIN
                                ROLLBACK TRAN UPDATESCHEDULE;
                                RETURN -1;
                            END;
                    END;
                ELSE
                    BEGIN
                        UPDATE  arStudentSchedules
                        SET     ScheduleId = @ScheduleId
                               ,Source = 'attendance'
                               ,Active = 1
                               ,ModUser = @ModUser
                               ,ModDate = GETDATE()
                        WHERE   StuEnrollId = @StuEnrollId;

                        IF @@error <> 0
                            BEGIN
                                ROLLBACK TRAN UPDATESCHEDULE;
                                RETURN -1;
                            END;
                    END;

		
            END;

        COMMIT TRAN UPDATESCHEDULE;
        RETURN 0;

    END;




GO
