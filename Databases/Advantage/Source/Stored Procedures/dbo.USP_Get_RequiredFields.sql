SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_Get_RequiredFields]
AS
    SELECT  ModuleResourceId
           ,ChildResourceId
           ,ChildResource
           ,ResourceTypeId
           ,NULL AS TabId
    FROM    (
              --Admissions
			  SELECT    189 AS ModuleResourceId
                       ,ResourceId AS ChildResourceId
                       ,Resource AS ChildResource
                       ,
	---- (SELECT AccessLevel FROM syRlsResLvls WHERE RoleId=@RoleId AND ResourceId=syResources.ResourceId) AS AccessLevel,
                        ResourceURL AS ChildResourceURL
                       ,NULL AS ParentResourceId
                       ,NULL AS ParentResource
                       ,1 AS GroupSortOrder
                       ,NULL AS FirstSortOrder
                       ,NULL AS DisplaySequence
                       ,ResourceTypeID
                       ,(
                          SELECT TOP 1
                                    t1.AllowSchlReqFlds
                          FROM      syResources t1
                          WHERE     t1.ResourceId = ResourceId
                        ) AS AllowSchlReqFlds
              FROM      syResources
              WHERE     ResourceId = 189
              UNION
              SELECT    189 AS ModuleResourceId
                       ,NNChild.ResourceId AS ChildResourceId
                       ,CASE WHEN NNChild.ResourceId = 395 THEN 'Lead Tabs'
                             ELSE CASE WHEN NNChild.ResourceId = 398 THEN 'Add/View Leads'
                                       ELSE RChild.Resource
                                  END
                        END AS ChildResource
                       ,
	---- (SELECT AccessLevel FROM syRlsResLvls WHERE RoleId=@RoleId AND ResourceId=NNChild.ResourceId) AS AccessLevel,
                        RChild.ResourceURL AS ChildResourceURL
                       ,CASE WHEN NNParent.ResourceId IN ( 687,688,689 ) THEN NULL
                             ELSE NNParent.ResourceId
                        END AS ParentResourceId
                       ,RParent.Resource AS ParentResource
                       ,CASE WHEN (
                                    NNChild.ResourceId = 694
                                    OR NNParent.ResourceId = 694
                                  ) THEN 2
                             ELSE 3
                        END AS GroupSortOrder
                       ,CASE WHEN (
                                    NNChild.ResourceId = 694
                                    OR NNParent.ResourceId = 694
                                  ) THEN 2
                             ELSE 3
                        END AS FirstSortOrder
                       ,CASE WHEN (
                                    NNChild.ResourceId IN ( 398 )
                                    OR NNParent.ResourceId IN ( 398 )
                                  ) THEN 1
                             ELSE 2
                        END AS DisplaySequence
                       ,RChild.ResourceTypeID
                       ,(
                          SELECT    AllowSchlReqFlds
                          FROM      syResources
                          WHERE     ResourceId = NNChild.ResourceId
                        ) AS AllowSchlReqFlds
	--NNChild.HierarchyIndex AS SecondSortOrder,
	--RParentModule.Resource AS Module
              FROM      syResources RChild
              INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
              INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
              INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
              LEFT OUTER JOIN (
                                SELECT  *
                                FROM    syResources
                                WHERE   ResourceTypeId = 1
                              ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
              WHERE     RChild.ResourceTypeId IN ( 2,3,8 )
                        AND (
                              RChild.ChildTypeId IS NULL
                              OR RChild.ChildTypeId = 3
                            )
                        AND ( RChild.ResourceId NOT IN ( 395 ) )
                        AND ( NNParent.ResourceId NOT IN ( 395 ) )
                        AND (
                              NNParent.ParentId IN ( SELECT HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = 189 )
                              OR NNChild.ParentId IN ( SELECT   HierarchyId
                                                       FROM     syNavigationNodes
                                                       WHERE    ResourceId IN ( 694,398 ) )
                            )
-- Acdemic Records
              UNION
              SELECT    26 AS ModuleResourceId
                       ,ResourceId AS ChildResourceId
                       ,Resource AS ChildResource
                       ,
	-- (SELECT AccessLevel FROM syRlsResLvls WHERE RoleId=@RoleId AND ResourceId=syResources.ResourceId) AS AccessLevel,
                        ResourceURL AS ChildResourceURL
                       ,NULL AS ParentResourceId
                       ,NULL AS ParentResource
                       ,1 AS GroupSortOrder
                       ,NULL AS FirstSortOrder
                       ,NULL AS DisplaySequence
                       ,ResourceTypeID
                       ,(
                          SELECT TOP 1
                                    t1.AllowSchlReqFlds
                          FROM      syResources t1
                          WHERE     t1.ResourceId = ResourceId
                        ) AS AllowSchlReqFlds
              FROM      syResources
              WHERE     ResourceId = 26
              UNION
              SELECT    26 AS ModuleResourceId
                       ,ChildResourceId
                       ,ChildResource
                       ,
	-- (SELECT AccessLevel FROM syRlsResLvls WHERE RoleId=@RoleId AND ResourceId=ChildResourceId) AS AccessLevel,
                        ChildResourceURL
                       ,ParentResourceId
                       ,ParentResource
                       ,CASE WHEN (
                                    ChildResourceId = 693
                                    OR ParentResourceId = 693
                                  ) THEN 2
                             ELSE CASE WHEN (
                                              ChildResourceId = 132
                                              OR ParentResourceId = 132
                                            ) THEN 3
                                       ELSE CASE WHEN (
                                                        ChildResourceId = 160
                                                        OR ParentResourceId = 160
                                                      ) THEN 4
                                                 ELSE CASE WHEN (
                                                                  ChildResourceId = 129
                                                                  OR ParentResourceId = 129
                                                                ) THEN 5
                                                           ELSE CASE WHEN (
                                                                            ChildResourceId = 71
                                                                            OR ParentResourceId = 71
                                                                          ) THEN 6
                                                                     ELSE CASE WHEN (
                                                                                      ChildResourceId = 190
                                                                                      OR ParentResourceId = 190
                                                                                    ) THEN 7
                                                                               ELSE 8
                                                                          END
                                                                END
                                                      END
                                            END
                                  END
                        END AS GroupSortOrder
                       ,(
                          SELECT TOP 1
                                    HierarchyIndex
                          FROM      dbo.syNavigationNodes
                          WHERE     ResourceId = ChildResourceId
                        ) AS FirstSortOrder
                       ,CASE WHEN (
                                    ChildResourceId IN ( 190,129,317,71 )
                                    OR ParentResourceId IN ( 190,129,317,71 )
                                  ) THEN 1
                             ELSE 2
                        END AS DisplaySequence
                       ,ResourceTypeID
                       ,(
                          SELECT    AllowSchlReqFlds
                          FROM      syResources
                          WHERE     ResourceId = ChildResourceId
                        ) AS AllowSchlReqFlds
              FROM      (
                          SELECT DISTINCT
                                    NNChild.ResourceId AS ChildResourceId
                                   ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                         ELSE RChild.Resource
                                    END AS ChildResource
                                   ,RChild.ResourceURL AS ChildResourceURL
                                   ,CASE WHEN NNParent.ResourceId = 687 THEN NULL
                                         ELSE NNParent.ResourceId
                                    END AS ParentResourceId
                                   ,RParent.Resource AS ParentResource
                                   ,RParentModule.Resource AS MODULE
                                   ,RChild.ResourceTypeId
				--NNParent.ParentId
                          FROM      syResources RChild
                          INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
                          INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                          INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                          LEFT OUTER JOIN (
                                            SELECT  *
                                            FROM    syResources
                                            WHERE   ResourceTypeId = 1
                                          ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                          WHERE     RChild.ResourceTypeId IN ( 2,3,8 )
                                    AND (
                                          RChild.ChildTypeId IS NULL
                                          OR RChild.ChildTypeId = 3
                                        )
                                    AND ( RChild.ResourceId <> 394 )
                                    AND ( NNParent.ResourceId NOT IN ( 394 ) )
                                    AND (
                                          NNParent.Parentid IN ( SELECT HierarchyId
                                                                 FROM   syNavigationNodes
                                                                 WHERE  ResourceId = 26 )
                                          OR NNChild.ParentId IN ( SELECT   HierarchyId
                                                                   FROM     syNavigationNodes
                                                                   WHERE    ResourceId IN ( 693,71,129,132,160,190 ) )
                                        )
                        ) t5
--Student Accounts
              UNION
              SELECT    194 AS ModuleResourceId
                       ,ResourceId AS ChildResourceId
                       ,Resource AS ChildResource
                       ,
	-- (SELECT AccessLevel FROM syRlsResLvls WHERE RoleId=@RoleId AND ResourceId=syResources.ResourceId) AS AccessLevel,
                        ResourceURL AS ChildResourceURL
                       ,NULL AS ParentResourceId
                       ,NULL AS ParentResource
                       ,1 AS GroupSortOrder
                       ,NULL AS FirstSortOrder
                       ,NULL AS DisplaySequence
                       ,ResourceTypeID
                       ,(
                          SELECT TOP 1
                                    t1.AllowSchlReqFlds
                          FROM      syResources t1
                          WHERE     t1.ResourceId = ResourceId
                        ) AS AllowSchlReqFlds
              FROM      syResources
              WHERE     ResourceId = 194
              UNION
              SELECT    194 AS ModuleResourceId
                       ,ChildResourceId
                       ,ChildResource
                       ,
	-- (SELECT AccessLevel FROM syRlsResLvls WHERE RoleId=@RoleId AND ResourceId=ChildResourceId) AS AccessLevel,
                        ChildResourceURL
                       ,ParentResourceId
                       ,ParentResource
                       ,CASE WHEN (
                                    ChildResourceId = 695
                                    OR ParentResourceId = 695
                                  ) THEN 2
                             ELSE CASE WHEN (
                                              ChildResourceId = 403
                                              OR ParentResourceId = 403
                                            ) THEN 3
                                       ELSE 4
                                  END
                        END AS GroupSortOrder
                       ,(
                          SELECT TOP 1
                                    HierarchyIndex
                          FROM      dbo.syNavigationNodes
                          WHERE     ResourceId = ChildResourceId
                        ) AS FirstSortOrder
                       ,CASE WHEN (
                                    ChildResourceId IN ( 695 )
                                    OR ParentResourceId IN ( 695 )
                                  ) THEN 1
                             ELSE 2
                        END AS DisplaySequence
                       ,ResourceTypeID
                       ,(
                          SELECT    AllowSchlReqFlds
                          FROM      syResources
                          WHERE     ResourceId = ChildResourceId
                        ) AS AllowSchlReqFlds
              FROM      (
                          SELECT DISTINCT
                                    NNChild.ResourceId AS ChildResourceId
                                   ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                         ELSE RChild.Resource
                                    END AS ChildResource
                                   ,RChild.ResourceURL AS ChildResourceURL
                                   ,CASE WHEN NNParent.ResourceId IN ( 194 )
                                              OR NNChild.ResourceId IN ( 695,403,698 ) THEN NULL
                                         ELSE NNParent.ResourceId
                                    END AS ParentResourceId
                                   ,RParent.Resource AS ParentResource
                                   ,RParentModule.Resource AS MODULE
                                   ,RChild.ResourceTypeID
				--NNParent.ParentId
                          FROM      syResources RChild
                          INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
                          INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                          INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                          LEFT OUTER JOIN (
                                            SELECT  *
                                            FROM    syResources
                                            WHERE   ResourceTypeId = 1
                                          ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                          WHERE     RChild.ResourceTypeId IN ( 2,3,8 )
                                    AND (
                                          RChild.ChildTypeId IS NULL
                                          OR RChild.ChildTypeId = 3
                                        )
                                    AND (
                                          NNParent.Parentid IN ( SELECT HierarchyId
                                                                 FROM   syNavigationNodes
                                                                 WHERE  ResourceId = 194 )
                                          OR NNChild.ParentId IN ( SELECT   HierarchyId
                                                                   FROM     syNavigationNodes
                                                                   WHERE    ResourceId IN ( 403,695,698 ) )
                                        )
                                    AND ( RChild.ResourceId <> 394 )
                                    AND ( NNParent.ResourceId NOT IN ( 394 ) )
                        ) t1 
--Faculty
              UNION
              SELECT    300 AS ModuleResourceId
                       ,ResourceId AS ChildResourceId
                       ,Resource AS ChildResource
                       ,
	-- (SELECT AccessLevel FROM syRlsResLvls WHERE RoleId=@RoleId AND ResourceId=syResources.ResourceId) AS AccessLevel,
                        ResourceURL AS ChildResourceURL
                       ,NULL AS ParentResourceId
                       ,NULL AS ParentResource
                       ,1 AS GroupSortOrder
                       ,NULL AS FirstSortOrder
                       ,NULL AS DisplaySequence
                       ,ResourceTypeID
                       ,(
                          SELECT TOP 1
                                    t1.AllowSchlReqFlds
                          FROM      syResources t1
                          WHERE     t1.ResourceId = ResourceId
                        ) AS AllowSchlReqFlds
              FROM      syResources
              WHERE     ResourceId = 300
              UNION
              SELECT    300 AS ModuleResourceId
                       ,ChildResourceId
                       ,ChildResource
                       ,
	---- (SELECT AccessLevel FROM syRlsResLvls WHERE RoleId=@RoleId AND ResourceId=ChildResourceId) AS AccessLevel,
                        ChildResourceURL
                       ,ParentResourceId
                       ,ParentResource
                       ,CASE WHEN (
                                    ChildResourceId = 697
                                    OR ParentResourceId = 697
                                  ) THEN 2
                             ELSE 3
                        END AS GroupSortOrder
                       ,(
                          SELECT TOP 1
                                    HierarchyIndex
                          FROM      dbo.syNavigationNodes
                          WHERE     ResourceId = ChildResourceId
                        ) AS FirstSortOrder
                       ,1 AS DisplaySequence
                       ,ResourceTypeId
                       ,(
                          SELECT    AllowSchlReqFlds
                          FROM      syResources
                          WHERE     ResourceId = ChildResourceId
                        ) AS AllowSchlReqFlds
              FROM      (
                          SELECT DISTINCT
                                    NNChild.ResourceId AS ChildResourceId
                                   ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                         ELSE RChild.Resource
                                    END AS ChildResource
                                   ,RChild.ResourceURL AS ChildResourceURL
                                   ,CASE WHEN NNParent.ResourceId = 300
                                              OR NNChild.ResourceId = 697 THEN NULL
                                         ELSE NNParent.ResourceId
                                    END AS ParentResourceId
                                   ,RParent.Resource AS ParentResource
                                   ,RParentModule.Resource AS MODULE
                                   ,RChild.ResourceTypeId
				--NNParent.ParentId
                          FROM      syResources RChild
                          INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
                          INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                          INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                          LEFT OUTER JOIN (
                                            SELECT  *
                                            FROM    syResources
                                            WHERE   ResourceTypeId = 1
                                          ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                          WHERE     RChild.ResourceTypeId IN ( 2,3,8 )
                                    AND (
                                          RChild.ChildTypeId IS NULL
                                          OR RChild.ChildTypeId = 3
                                        )
                                    AND ( RChild.ResourceId <> 394 )
                                    AND ( NNParent.ResourceId NOT IN ( 394 ) )
                                    AND (
                                          NNParent.Parentid IN ( SELECT HierarchyId
                                                                 FROM   syNavigationNodes
                                                                 WHERE  ResourceId = 300 )
                                          OR NNChild.ParentId IN ( SELECT   HierarchyId
                                                                   FROM     syNavigationNodes
                                                                   WHERE    ResourceId IN ( 697 ) )
                                        )
                        ) t3
              UNION
--Financial Aid
              SELECT    191 AS ModuleResourceId
                       ,ResourceId AS ChildResourceId
                       ,Resource AS ChildResource
                       ,
	-- (SELECT AccessLevel FROM syRlsResLvls WHERE RoleId=@RoleId AND ResourceId=syResources.ResourceId) AS AccessLevel,
                        ResourceURL AS ChildResourceURL
                       ,NULL AS ParentResourceId
                       ,NULL AS ParentResource
                       ,1 AS GroupSortOrder
                       ,NULL AS FirstSortOrder
                       ,NULL AS DisplaySequence
                       ,ResourceTypeID
                       ,(
                          SELECT TOP 1
                                    t1.AllowSchlReqFlds
                          FROM      syResources t1
                          WHERE     t1.ResourceId = ResourceId
                        ) AS AllowSchlReqFlds
              FROM      syResources
              WHERE     ResourceId = 191
              UNION
              SELECT    191 AS ModuleResourceId
                       ,ChildResourceId
                       ,ChildResource
                       ,
	-- (SELECT AccessLevel FROM syRlsResLvls WHERE RoleId=@RoleId AND ResourceId=ChildResourceId) AS AccessLevel,
                        ChildResourceURL
                       ,ParentResourceId
                       ,ParentResource
                       ,CASE WHEN (
                                    ChildResourceId = 700
                                    OR ParentResourceId = 700
                                  ) THEN 2
                             ELSE CASE WHEN (
                                              ChildResourceId = 698
                                              OR ParentResourceId = 698
                                            ) THEN 3
                                       ELSE 4
                                  END
                        END AS GroupSortOrder
                       ,(
                          SELECT TOP 1
                                    HierarchyIndex
                          FROM      dbo.syNavigationNodes
                          WHERE     ResourceId = ChildResourceId
                        ) AS FirstSortOrder
                       ,CASE WHEN (
                                    ChildResourceId IN ( 698 )
                                    OR ParentResourceId IN ( 698 )
                                  ) THEN 1
                             ELSE 2
                        END AS DisplaySequence
                       ,ResourceTypeId
                       ,(
                          SELECT    AllowSchlReqFlds
                          FROM      syResources
                          WHERE     ResourceId = ChildResourceId
                        ) AS AllowSchlReqFlds
              FROM      (
                          SELECT DISTINCT
                                    NNChild.ResourceId AS ChildResourceId
                                   ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                         ELSE RChild.Resource
                                    END AS ChildResource
                                   ,RChild.ResourceURL AS ChildResourceURL
                                   ,CASE WHEN (
                                                NNParent.ResourceId = 191
                                                OR NNChild.ResourceId = 698
                                                OR NNChild.ResourceId = 699
                                                OR NNChild.ResourceId = 700
                                              ) THEN NULL
                                         ELSE NNParent.ResourceId
                                    END AS ParentResourceId
                                   ,RParent.Resource AS ParentResource
                                   ,RParentModule.Resource AS MODULE
                                   ,RChild.ResourceTypeID
				--NNParent.ParentId
                          FROM      syResources RChild
                          INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
                          INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                          INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                          LEFT OUTER JOIN (
                                            SELECT  *
                                            FROM    syResources
                                            WHERE   ResourceTypeId = 1
                                          ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                          WHERE     RChild.ResourceTypeId IN ( 2,3,8 )
                                    AND (
                                          RChild.ChildTypeId IS NULL
                                          OR RChild.ChildTypeId = 3
                                        )
                                    AND ( RChild.ResourceId <> 394 )
                                    AND ( NNParent.ResourceId NOT IN ( 394 ) )
                                    AND (
                                          NNParent.Parentid IN ( SELECT HierarchyId
                                                                 FROM   syNavigationNodes
                                                                 WHERE  ResourceId = 191 )
                                          OR NNChild.ParentId IN ( SELECT   HierarchyId
                                                                   FROM     syNavigationNodes
                                                                   WHERE    ResourceId IN ( 700,698,699 ) )
                                        )
                        ) t4
--Human Resources Starts Here
              UNION
              SELECT    192 AS ModuleResourceId
                       ,ResourceId AS ChildResourceId
                       ,Resource AS ChildResource
                       ,
	-- (SELECT AccessLevel FROM syRlsResLvls WHERE RoleId=@RoleId AND ResourceId=syResources.ResourceId) AS AccessLevel,
                        ResourceURL AS ChildResourceURL
                       ,NULL AS ParentResourceId
                       ,NULL AS ParentResource
                       ,1 AS GroupSortOrder
                       ,NULL AS FirstSortOrder
                       ,NULL AS DisplaySequence
                       ,ResourceTypeID
                       ,(
                          SELECT TOP 1
                                    t1.AllowSchlReqFlds
                          FROM      syResources t1
                          WHERE     t1.ResourceId = ResourceId
                        ) AS AllowSchlReqFlds
              FROM      syResources
              WHERE     ResourceId = 192
              UNION
              SELECT    192 AS ModuleResourceId
                       ,NNChild.ResourceId AS ChildResourceId
                       ,
	--CASE WHEN NNChild.ResourceId=395 THEN 'Lead Tabs' ELSE 
	--CASE WHEN NNChild.ResourceId=398 THEN 'Add/View Leads'	ELSE RChild.Resource END 
	--END
                        RChild.Resource AS ChildResource
                       ,
	-- (SELECT AccessLevel FROM syRlsResLvls WHERE RoleId=@RoleId AND ResourceId=NNChild.ResourceId) AS AccessLevel,
                        RChild.ResourceURL AS ChildResourceURL
                       ,CASE WHEN NNParent.ResourceId IN ( 687,688,689 ) THEN NULL
                             ELSE NNParent.ResourceId
                        END AS ParentResourceId
                       ,RParent.Resource AS ParentResource
                       ,CASE WHEN (
                                    NNChild.ResourceId = 708
                                    OR NNParent.ResourceId = 708
                                  ) THEN 2
                             ELSE 3
                        END AS GroupSortOrder
                       ,CASE WHEN (
                                    NNChild.ResourceId = 708
                                    OR NNParent.ResourceId = 708
                                  ) THEN 2
                             ELSE 3
                        END AS FirstSortOrder
                       ,
	--CASE WHEN (NNChild.ResourceId IN (398) OR NNParent.ResourceId IN (398)) THEN 1 ELSE 2 END AS DisplaySequence,
                        1 AS DisplaySequence
                       ,RChild.ResourceTypeID
                       ,(
                          SELECT    AllowSchlReqFlds
                          FROM      syResources
                          WHERE     ResourceId = NNChild.ResourceId
                        ) AS AllowSchlReqFlds
	--NNChild.HierarchyIndex AS SecondSortOrder,
	--RParentModule.Resource AS Module
              FROM      syResources RChild
              INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
              INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
              INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
              LEFT OUTER JOIN (
                                SELECT  *
                                FROM    syResources
                                WHERE   ResourceTypeId = 1
                              ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
              WHERE     RChild.ResourceTypeId IN ( 2,3,8 )
                        AND (
                              RChild.ChildTypeId IS NULL
                              OR RChild.ChildTypeId = 3
                            )
                        AND ( RChild.ResourceId NOT IN ( 396 ) )
                        AND ( NNParent.ResourceId NOT IN ( 396 ) )
                        AND (
                              NNParent.ParentId IN ( SELECT HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = 192 )
                              OR NNChild.ParentId IN ( SELECT   HierarchyId
                                                       FROM     syNavigationNodes
                                                       WHERE    ResourceId IN ( 708 ) )
                            )
              UNION
--Placement
              SELECT    193 AS ModuleResourceId
                       ,ResourceId AS ChildResourceId
                       ,Resource AS ChildResource
                       ,
	-- (SELECT AccessLevel FROM syRlsResLvls WHERE RoleId=@RoleId AND ResourceId=syResources.ResourceId) AS AccessLevel,
                        ResourceURL AS ChildResourceURL
                       ,NULL AS ParentResourceId
                       ,NULL AS ParentResource
                       ,1 AS GroupSortOrder
                       ,NULL AS FirstSortOrder
                       ,NULL AS DisplaySequence
                       ,ResourceTypeID
                       ,(
                          SELECT TOP 1
                                    t1.AllowSchlReqFlds
                          FROM      syResources t1
                          WHERE     t1.ResourceId = ResourceId
                        ) AS AllowSchlReqFlds
              FROM      syResources
              WHERE     ResourceId = 193
              UNION
              SELECT    193 AS ModuleResourceId
                       ,ChildResourceId
                       ,ChildResource
                       ,
	-- (SELECT AccessLevel FROM syRlsResLvls WHERE RoleId=@RoleId AND ResourceId=ChildResourceId) AS AccessLevel,
                        ChildResourceURL
                       ,ParentResourceId
                       ,ParentResource
                       ,CASE WHEN (
                                    ChildResourceId = 696
                                    OR ParentResourceId = 696
                                  ) THEN 2
                             ELSE 3
                        END AS GroupSortOrder
                       ,(
                          SELECT TOP 1
                                    HierarchyIndex
                          FROM      dbo.syNavigationNodes
                          WHERE     ResourceId = ChildResourceId
                        ) AS FirstSortOrder
                       ,CASE WHEN (
                                    ChildResourceId IN ( 696 )
                                    OR ParentResourceId IN ( 696 )
                                  ) THEN 1
                             ELSE 2
                        END AS DisplaySequence
                       ,ResourceTypeID
                       ,(
                          SELECT    AllowSchlReqFlds
                          FROM      syResources
                          WHERE     ResourceId = ChildResourceId
                        ) AS AllowSchlReqFlds
              FROM      (
                          SELECT DISTINCT
                                    NNChild.ResourceId AS ChildResourceId
                                   ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                         ELSE CASE WHEN NNChild.ResourceId = 397 THEN 'Employer Tabs'
                                                   ELSE RChild.Resource
                                              END
                                    END AS ChildResource
                                   ,RChild.ResourceURL AS ChildResourceURL
                                   ,CASE WHEN (
                                                NNParent.ResourceId = 193
                                                OR NNChild.ResourceId IN ( 696,404 )
                                              ) THEN NULL
                                         ELSE NNParent.ResourceId
                                    END AS ParentResourceId
                                   ,RParent.Resource AS ParentResource
                                   ,RParentModule.Resource AS MODULE
                                   ,RChild.ResourceTypeID
				--NNParent.ParentId
                          FROM      syResources RChild
                          INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
                          INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                          INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                          LEFT OUTER JOIN (
                                            SELECT  *
                                            FROM    syResources
                                            WHERE   ResourceTypeId = 1
                                          ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                          WHERE     RChild.ResourceTypeId IN ( 2,3,8 )
                                    AND (
                                          RChild.ChildTypeId IS NULL
                                          OR RChild.ChildTypeId = 3
                                        )
                                    AND ( RChild.ResourceId <> 394 )
                                    AND ( NNParent.ResourceId NOT IN ( 394,397 ) )
                                    AND (
                                          NNParent.Parentid IN ( SELECT HierarchyId
                                                                 FROM   syNavigationNodes
                                                                 WHERE  ResourceId = 193 )
                                          OR NNChild.ParentId IN ( SELECT   HierarchyId
                                                                   FROM     syNavigationNodes
                                                                   WHERE    ResourceId IN ( 404,696 ) )
                                        )
                        ) t5
            ) t2
    WHERE   --(@ModuleResourceId is null or t2.ModuleResourceId=@ModuleResourceId)  and 
            ( AllowSchlReqFlds = 1 )
            AND ParentResourceId IS NOT NULL
    UNION
-- Maintenance
    SELECT  ModuleResourceId
           ,ChildResourceId
           ,ChildResource
           ,ResourceTypeId
           ,NULL AS TabId
    FROM    (
              SELECT    189 AS ModuleResourceId
                       ,ResourceId AS ChildResourceId
                       ,Resource AS ChildResource
                       ,(
                          SELECT TOP 1
                                    t1.AllowSchlReqFlds
                          FROM      syResources t1
                          WHERE     t1.ResourceId = ResourceId
                        ) AS AllowSchlReqFlds
                       ,ResourceURL AS ChildResourceURL
                       ,NULL AS ParentResourceId
                       ,NULL AS ParentResource
                       ,1 AS GroupSortOrder
                       ,NULL AS FirstSortOrder
                       ,NULL AS DisplaySequence
                       ,ResourceTypeID
              FROM      syResources
              WHERE     ResourceId = 189
              UNION
              SELECT    189 AS ModuleResourceId
                       ,NNChild.ResourceId AS ChildResourceId
                       ,CASE WHEN NNChild.ResourceId = 395 THEN 'Lead Tabs'
                             ELSE CASE WHEN NNChild.ResourceId = 398 THEN 'Add/View Leads'
                                       ELSE RChild.Resource
                                  END
                        END AS ChildResource
                       ,(
                          SELECT TOP 1
                                    t1.AllowSchlReqFlds
                          FROM      syResources t1
                          WHERE     t1.ResourceId = NNChild.ResourceId
                        ) AS AllowSchlReqFlds
                       ,RChild.ResourceURL AS ChildResourceURL
                       ,CASE WHEN NNParent.ResourceId IN ( 687,688,689 ) THEN NULL
                             ELSE NNParent.ResourceId
                        END AS ParentResourceId
                       ,RParent.Resource AS ParentResource
                       ,CASE WHEN (
                                    NNChild.ResourceId = 702
                                    OR NNParent.ResourceId = 702
                                  ) THEN 2
                             ELSE CASE WHEN (
                                              NNChild.ResourceId = 400
                                              OR NNParent.ResourceId = 400
                                            ) THEN 3
                                       ELSE CASE WHEN (
                                                        NNChild.ResourceId = 401
                                                        OR NNParent.ResourceId = 401
                                                      ) THEN 4
                                                 ELSE 5
                                            END
                                  END
                        END AS GroupSortOrder
                       ,CASE WHEN NNChild.ResourceId = 398 THEN 1
                             ELSE CASE WHEN NNChild.ResourceId = 395 THEN 2
                                       ELSE 3
                                  END
                        END AS FirstSortOrder
                       ,CASE WHEN (
                                    NNChild.ResourceId IN ( 400,702 )
                                    OR NNParent.ResourceId IN ( 400,702 )
                                  ) THEN 1
                             ELSE 2
                        END AS DisplaySequence
                       ,RChild.ResourceTypeID
              FROM      syResources RChild
              INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
              INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
              INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
              LEFT OUTER JOIN (
                                SELECT  *
                                FROM    syResources
                                WHERE   ResourceTypeId = 1
                              ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
              WHERE     RChild.ResourceTypeId IN ( 2,4,8 )
                        AND (
                              RChild.ChildTypeId IS NULL
                              OR RChild.ChildTypeId = 4
                            )
                        AND ( RChild.ResourceId NOT IN ( 395 ) )
                        AND ( NNParent.ResourceId NOT IN ( 395 ) )
                        AND (
                              NNParent.ParentId IN ( SELECT HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = 189 )
                              OR NNChild.ParentId IN ( SELECT   HierarchyId
                                                       FROM     syNavigationNodes
                                                       WHERE    ResourceId IN ( 702,400,401,399 ) )
                            )
              UNION
              SELECT    26 AS ModuleResourceId
                       ,ResourceId AS ChildResourceId
                       ,Resource AS ChildResource
                       ,(
                          SELECT TOP 1
                                    t1.AllowSchlReqFlds
                          FROM      syResources t1
                          WHERE     t1.ResourceId = ResourceId
                        ) AS AllowSchlReqFlds
                       ,ResourceURL AS ChildResourceURL
                       ,NULL AS ParentResourceId
                       ,NULL AS ParentResource
                       ,1 AS GroupSortOrder
                       ,NULL AS FirstSortOrder
                       ,NULL AS DisplaySequence
                       ,ResourceTypeID
              FROM      syResources
              WHERE     ResourceId = 26
              UNION
              SELECT    26 AS ModuleResourceId
                       ,ChildResourceId
                       ,ChildResource
                       ,(
                          SELECT TOP 1
                                    t2.AllowSchlReqFlds
                          FROM      syResources t2
                          WHERE     t2.ResourceId = t1.ChildResourceId
                        ) AS AllowSchlReqFlds
                       ,ChildResourceURL
                       ,ParentResourceId
                       ,ParentResource
                       ,GroupSortOrder
                       ,(
                          SELECT TOP 1
                                    HierarchyIndex
                          FROM      dbo.syNavigationNodes
                          WHERE     ResourceId = ChildResourceId
                        ) AS FirstSortOrder
                       ,CASE WHEN (
                                    ChildResourceId IN ( 703,468 )
                                    OR ParentResourceId IN ( 703,468 )
                                  ) THEN 1
                             ELSE 2
                        END AS DisplaySequence
                       ,ResourceTypeId
              FROM      (
                          SELECT DISTINCT
                                    NNChild.ResourceId AS ChildResourceId
                                   ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                         ELSE RChild.Resource
                                    END AS ChildResource
                                   ,RChild.ResourceURL AS ChildResourceURL
                                   ,CASE WHEN (
                                                NNParent.ResourceId = 687
                                                OR NNChild.ResourceId IN ( 468,469,621,471,470,703 )
                                              ) THEN NULL
                                         ELSE NNParent.ResourceId
                                    END AS ParentResourceId
                                   ,RParent.Resource AS ParentResource
                                   ,RParentModule.Resource AS MODULE
                                   ,CASE WHEN (
                                                NNChild.ResourceId = 703
                                                OR NNParent.ResourceId = 703
                                              ) THEN 2
                                         ELSE CASE WHEN (
                                                          NNChild.ResourceId = 468
                                                          OR NNParent.ResourceId = 468
                                                        ) THEN 3
                                                   ELSE CASE WHEN (
                                                                    NNChild.ResourceId = 469
                                                                    OR NNParent.ResourceId = 469
                                                                  ) THEN 4
                                                             ELSE CASE WHEN (
                                                                              NNChild.ResourceId = 621
                                                                              OR NNParent.ResourceId = 621
                                                                            ) THEN 5
                                                                       ELSE CASE WHEN (
                                                                                        NNChild.ResourceId = 471
                                                                                        OR NNParent.ResourceId = 471
                                                                                      ) THEN 6
                                                                                 ELSE 7
                                                                            END
                                                                  END
                                                        END
                                              END
                                    END AS GroupSortOrder
                                   ,RChild.ResourceTypeID
				--NNParent.ParentId
                          FROM      syResources RChild
                          INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
                          INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                          INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                          LEFT OUTER JOIN (
                                            SELECT  *
                                            FROM    syResources
                                            WHERE   ResourceTypeId = 1
                                          ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                          WHERE     RChild.ResourceTypeId IN ( 2,4,8 )
                                    AND (
                                          RChild.ChildTypeId IS NULL
                                          OR RChild.ChildTypeId = 4
                                        )
                                    AND ( RChild.ResourceId <> 394 )
                                    AND ( NNChild.ResourceId <> 769 )
                                    AND ( NNParent.ResourceId NOT IN ( 394 ) )
                                    AND (
                                          NNParent.ParentId IN ( SELECT HierarchyId
                                                                 FROM   syNavigationNodes
                                                                 WHERE  ResourceId = 26 )
                                          OR NNChild.ParentId IN ( SELECT   HierarchyId
                                                                   FROM     syNavigationNodes
                                                                   WHERE    ResourceId IN ( 703,468,469,621,471,470 ) )
                                        )
                        ) t1
              UNION
              SELECT    194 AS ModuleResourceId
                       ,ResourceId AS ChildResourceId
                       ,Resource AS ChildResource
                       ,(
                          SELECT TOP 1
                                    t1.AllowSchlReqFlds
                          FROM      syResources t1
                          WHERE     t1.ResourceId = ResourceId
                        ) AS AllowSchlReqFlds
                       ,ResourceURL AS ChildResourceURL
                       ,NULL AS ParentResourceId
                       ,NULL AS ParentResource
                       ,1 AS GroupSortOrder
                       ,NULL AS FirstSortOrder
                       ,NULL AS DisplaySequence
                       ,ResourceTypeID
              FROM      syResources
              WHERE     ResourceId = 194
              UNION
              SELECT    194 AS ModuleResourceId
                       ,ChildResourceId
                       ,ChildResource
                       ,(
                          SELECT TOP 1
                                    t2.AllowSchlReqFlds
                          FROM      syResources t2
                          WHERE     t2.ResourceId = t3.ChildResourceId
                        ) AS AllowSchlReqFlds
                       ,ChildResourceURL
                       ,ParentResourceId
                       ,ParentResource
                       ,GroupSortOrder
                       ,(
                          SELECT TOP 1
                                    HierarchyIndex
                          FROM      dbo.syNavigationNodes
                          WHERE     ResourceId = ChildResourceId
                        ) AS FirstSortOrder
                       ,CASE WHEN (
                                    ChildResourceId IN ( 705 )
                                    OR ParentResourceId IN ( 705 )
                                  ) THEN 1
                             ELSE 2
                        END AS DisplaySequence
                       ,ResourceTypeID
              FROM      (
                          SELECT DISTINCT
                                    NNChild.ResourceId AS ChildResourceId
                                   ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                         ELSE RChild.Resource
                                    END AS ChildResource
                                   ,RChild.ResourceURL AS ChildResourceURL
                                   ,CASE WHEN NNParent.ResourceId IN ( 194 )
                                              OR NNChild.ResourceId IN ( 705 ) THEN NULL
                                         ELSE NNParent.ResourceId
                                    END AS ParentResourceId
                                   ,RParent.Resource AS ParentResource
                                   ,RParentModule.Resource AS MODULE
                                   ,CASE WHEN (
                                                NNChild.ResourceId = 705
                                                OR NNParent.ResourceId = 705
                                              ) THEN 2
                                         ELSE 3
                                    END AS GroupSortOrder
                                   ,RChild.ResourceTypeID
                          FROM      syResources RChild
                          INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
                          INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                          INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                          LEFT OUTER JOIN (
                                            SELECT  *
                                            FROM    syResources
                                            WHERE   ResourceTypeId = 1
                                          ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                          WHERE     RChild.ResourceTypeId IN ( 2,4,8 )
                                    AND (
                                          RChild.ChildTypeId IS NULL
                                          OR RChild.ChildTypeId = 4
                                        )
                                    AND (
                                          NNParent.ParentId IN ( SELECT HierarchyId
                                                                 FROM   syNavigationNodes
                                                                 WHERE  ResourceId = 194 )
                                          OR NNChild.ParentId IN ( SELECT   HierarchyId
                                                                   FROM     syNavigationNodes
                                                                   WHERE    ResourceId IN ( 705 ) )
                                        )
                                    AND ( RChild.ResourceId <> 394 )
                                    AND ( NNParent.ResourceId NOT IN ( 394,277 ) )
					-- The following condition uses Bitwise Operator
                        ) t3
              UNION
              SELECT    300 AS ModuleResourceId
                       ,ResourceId AS ChildResourceId
                       ,Resource AS ChildResource
                       ,(
                          SELECT TOP 1
                                    t1.AllowSchlReqFlds
                          FROM      syResources t1
                          WHERE     t1.ResourceId = ResourceId
                        ) AS AllowSchlReqFlds
                       ,ResourceURL AS ChildResourceURL
                       ,NULL AS ParentResourceId
                       ,NULL AS ParentResource
                       ,1 AS GroupSortOrder
                       ,NULL AS FirstSortOrder
                       ,NULL AS DisplaySequence
                       ,ResourceTypeID
              FROM      syResources
              WHERE     ResourceId = 300
              UNION
              SELECT    300 AS ModuleResourceId
                       ,ChildResourceId
                       ,ChildResource
                       ,(
                          SELECT TOP 1
                                    t2.AllowSchlReqFlds
                          FROM      syResources t2
                          WHERE     t2.ResourceId = t3.ChildResourceId
                        ) AS AllowSchlReqFlds
                       ,ChildResourceURL
                       ,ParentResourceId
                       ,ParentResource
                       ,GroupSortOrder
                       ,(
                          SELECT TOP 1
                                    HierarchyIndex
                          FROM      dbo.syNavigationNodes
                          WHERE     ResourceId = ChildResourceId
                        ) AS FirstSortOrder
                       ,1 AS DisplaySequence
                       ,ResourceTypeID
              FROM      (
                          SELECT DISTINCT
                                    NNChild.ResourceId AS ChildResourceId
                                   ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                         ELSE RChild.Resource
                                    END AS ChildResource
                                   ,RChild.ResourceURL AS ChildResourceURL
                                   ,CASE WHEN NNParent.ResourceId = 300
                                              OR NNChild.ResourceId = 697 THEN NULL
                                         ELSE NNParent.ResourceId
                                    END AS ParentResourceId
                                   ,RParent.Resource AS ParentResource
                                   ,RParentModule.Resource AS MODULE
                                   ,CASE WHEN (
                                                NNChild.ResourceId = 705
                                                OR NNParent.ResourceId = 705
                                              ) THEN 2
                                         ELSE 3
                                    END AS GroupSortOrder
                                   ,RChild.ResourceTypeID
                          FROM      syResources RChild
                          INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
                          INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                          INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                          LEFT OUTER JOIN (
                                            SELECT  *
                                            FROM    syResources
                                            WHERE   ResourceTypeId = 1
                                          ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                          WHERE     RChild.ResourceTypeId IN ( 2,4,8 )
                                    AND (
                                          RChild.ChildTypeId IS NULL
                                          OR RChild.ChildTypeId = 4
                                        )
                                    AND ( RChild.ResourceId <> 394 )
                                    AND ( NNParent.ResourceId NOT IN ( 394,687 ) )
                                    AND ( NNParent.ParentId IN ( SELECT HierarchyId
                                                                 FROM   syNavigationNodes
                                                                 WHERE  ResourceId = 300 ) )
                        ) t3
              UNION
              SELECT    191 AS ModuleResourceId
                       ,ResourceId AS ChildResourceId
                       ,Resource AS ChildResource
                       ,(
                          SELECT TOP 1
                                    t1.AllowSchlReqFlds
                          FROM      syResources t1
                          WHERE     t1.ResourceId = ResourceId
                        ) AS AllowSchlReqFlds
                       ,ResourceURL AS ChildResourceURL
                       ,NULL AS ParentResourceId
                       ,NULL AS ParentResource
                       ,1 AS GroupSortOrder
                       ,NULL AS FirstSortOrder
                       ,NULL AS DisplaySequence
                       ,ResourceTypeID
              FROM      syResources
              WHERE     ResourceId = 191
              UNION
              SELECT    191 AS ModuleResourceId
                       ,ChildResourceId
                       ,ChildResource
                       ,(
                          SELECT TOP 1
                                    t2.AllowSchlReqFlds
                          FROM      syResources t2
                          WHERE     t2.ResourceId = t3.ChildResourceId
                        ) AS AllowSchlReqFlds
                       ,ChildResourceURL
                       ,ParentResourceId
                       ,ParentResource
                       ,GroupSortOrder
                       ,(
                          SELECT TOP 1
                                    HierarchyIndex
                          FROM      dbo.syNavigationNodes
                          WHERE     ResourceId = ChildResourceId
                        ) AS FirstSortOrder
                       ,1 AS DisplaySequence
                       ,ResourceTypeID
              FROM      (
                          SELECT DISTINCT
                                    NNChild.ResourceId AS ChildResourceId
                                   ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                         ELSE RChild.Resource
                                    END AS ChildResource
                                   ,RChild.ResourceURL AS ChildResourceURL
                                   ,CASE WHEN (
                                                NNParent.ResourceId = 191
                                                OR NNChild.ResourceId = 706
                                              ) THEN NULL
                                         ELSE NNParent.ResourceId
                                    END AS ParentResourceId
                                   ,RParent.Resource AS ParentResource
                                   ,RParentModule.Resource AS MODULE
                                   ,CASE WHEN (
                                                NNChild.ResourceId = 706
                                                OR NNParent.ResourceId = 706
                                              ) THEN 2
                                         ELSE 3
                                    END AS GroupSortOrder
                                   ,RChild.ResourceTypeID
                          FROM      syResources RChild
                          INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
                          INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                          INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                          LEFT OUTER JOIN (
                                            SELECT  *
                                            FROM    syResources
                                            WHERE   ResourceTypeId = 1
                                          ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                          WHERE     RChild.ResourceTypeId IN ( 2,4,8 )
                                    AND (
                                          RChild.ChildTypeId IS NULL
                                          OR RChild.ChildTypeId = 4
                                        )
                                    AND ( RChild.ResourceId <> 394 )
                                    AND ( NNParent.ResourceId NOT IN ( 394 ) )
                                    AND (
                                          NNParent.ParentId IN ( SELECT HierarchyId
                                                                 FROM   syNavigationNodes
                                                                 WHERE  ResourceId = 191 )
                                          OR NNChild.ParentId IN ( SELECT   HierarchyId
                                                                   FROM     syNavigationNodes
                                                                   WHERE    ResourceId IN ( 706 ) )
                                        )
                        ) t3
              UNION
              SELECT    193 AS ModuleResourceId
                       ,ResourceId AS ChildResourceId
                       ,Resource AS ChildResource
                       ,(
                          SELECT TOP 1
                                    t1.AllowSchlReqFlds
                          FROM      syResources t1
                          WHERE     t1.ResourceId = ResourceId
                        ) AS AllowSchlReqFlds
                       ,ResourceURL AS ChildResourceURL
                       ,NULL AS ParentResourceId
                       ,NULL AS ParentResource
                       ,1 AS GroupSortOrder
                       ,NULL AS FirstSortOrder
                       ,NULL AS DisplaySequence
                       ,ResourceTypeID
              FROM      syResources
              WHERE     ResourceId = 193
              UNION
              SELECT    193 AS ModuleResourceId
                       ,ChildResourceId
                       ,ChildResource
                       ,(
                          SELECT TOP 1
                                    t1.AllowSchlReqFlds
                          FROM      syResources t1
                          WHERE     t1.ResourceId = t3.ChildResourceId
                        ) AS AllowSchlReqFlds
                       ,ChildResourceURL
                       ,ParentResourceId
                       ,ParentResource
                       ,GroupSortOrder
                       ,(
                          SELECT TOP 1
                                    HierarchyIndex
                          FROM      dbo.syNavigationNodes
                          WHERE     ResourceId = ChildResourceId
                        ) AS FirstSortOrder
                       ,CASE WHEN (
                                    ChildResourceId IN ( 704 )
                                    OR ParentResourceId IN ( 704 )
                                  ) THEN 1
                             ELSE 2
                        END AS DisplaySequence
                       ,ResourceTypeID
              FROM      (
                          SELECT DISTINCT
                                    NNChild.ResourceId AS ChildResourceId
                                   ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                         ELSE RChild.Resource
                                    END AS ChildResource
                                   ,RChild.ResourceURL AS ChildResourceURL
                                   ,CASE WHEN (
                                                NNParent.ResourceId = 193
                                                OR NNChild.ResourceId IN ( 704,405,406 )
                                              ) THEN NULL
                                         ELSE NNParent.ResourceId
                                    END AS ParentResourceId
                                   ,RParent.Resource AS ParentResource
                                   ,RParentModule.Resource AS MODULE
                                   ,CASE WHEN (
                                                NNChild.ResourceId = 704
                                                OR NNParent.ResourceId = 704
                                              ) THEN 2
                                         ELSE 3
                                    END AS GroupSortOrder
                                   ,RChild.ResourceTypeID
                          FROM      syResources RChild
                          INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
                          INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                          INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                          LEFT OUTER JOIN (
                                            SELECT  *
                                            FROM    syResources
                                            WHERE   ResourceTypeId = 1
                                          ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                          WHERE     RChild.ResourceTypeId IN ( 2,4,8 )
                                    AND (
                                          RChild.ChildTypeId IS NULL
                                          OR RChild.ChildTypeId = 4
                                        )
                                    AND ( RChild.ResourceId <> 394 )
                                    AND ( NNParent.ResourceId NOT IN ( 394 ) )
                                    AND (
                                          NNParent.ParentId IN ( SELECT HierarchyId
                                                                 FROM   syNavigationNodes
                                                                 WHERE  ResourceId = 193 )
                                          OR NNChild.ParentId IN ( SELECT   HierarchyId
                                                                   FROM     syNavigationNodes
                                                                   WHERE    ResourceId IN ( 694,405,406,704 ) )
                                        )
                        ) t3
              UNION
              SELECT    195 AS ModuleResourceId
                       ,ResourceId AS ChildResourceId
                       ,Resource AS ChildResource
                       ,(
                          SELECT TOP 1
                                    t1.AllowSchlReqFlds
                          FROM      syResources t1
                          WHERE     t1.ResourceId = ResourceId
                        ) AS AllowSchlReqFlds
                       ,ResourceURL AS ChildResourceURL
                       ,NULL AS ParentResourceId
                       ,NULL AS ParentResource
                       ,1 AS GroupSortOrder
                       ,NULL AS FirstSortOrder
                       ,NULL AS DisplaySequence
                       ,ResourceTypeID
              FROM      syResources
              WHERE     ResourceId = 195
              UNION
              SELECT    195 AS ModuleResourceId
                       ,ChildResourceId
                       ,ChildResource
                       ,(
                          SELECT TOP 1
                                    t1.AllowSchlReqFlds
                          FROM      syResources t1
                          WHERE     t1.ResourceId = t3.ChildResourceId
                        ) AS AllowSchlReqFlds
                       ,ChildResourceURL
                       ,ParentResourceId
                       ,ParentResource
                       ,GroupSortOrder
                       ,(
                          SELECT TOP 1
                                    HierarchyIndex
                          FROM      dbo.syNavigationNodes
                          WHERE     ResourceId = ChildResourceId
                        ) AS FirstSortOrder
                       ,CASE WHEN (
                                    ChildResourceId IN ( 707 )
                                    OR ParentResourceId IN ( 707 )
                                  ) THEN 1
                             ELSE 2
                        END AS DisplaySequence
                       ,ResourceTypeID
              FROM      (
                          SELECT DISTINCT
                                    NNChild.ResourceId AS ChildResourceId
                                   ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                         ELSE RChild.Resource
                                    END AS ChildResource
                                   ,RChild.ResourceURL AS ChildResourceURL
                                   ,CASE WHEN (
                                                NNParent.ResourceId = 195
                                                OR NNChild.ResourceId IN ( 707,463,429,408 )
                                              ) THEN NULL
                                         ELSE NNParent.ResourceId
                                    END AS ParentResourceId
                                   ,RParent.Resource AS ParentResource
                                   ,RParentModule.Resource AS MODULE
                                   ,CASE WHEN (
                                                NNChild.ResourceId = 707
                                                OR NNParent.ResourceId = 707
                                              ) THEN 2
                                         ELSE 3
                                    END AS GroupSortOrder
                                   ,RChild.ResourceTypeID
                          FROM      syResources RChild
                          INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
                          INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                          INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                          LEFT OUTER JOIN (
                                            SELECT  *
                                            FROM    syResources
                                            WHERE   ResourceTypeId = 1
                                          ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                          WHERE     RChild.ResourceTypeId IN ( 2,4,8 )
                                    AND (
                                          RChild.ChildTypeId IS NULL
                                          OR RChild.ChildTypeId = 4
                                        )
                                    AND ( RChild.ResourceId <> 394 )
                                    AND ( NNParent.ResourceId NOT IN ( 394 ) )
                                    AND (
                                          NNParent.ParentId IN ( SELECT HierarchyId
                                                                 FROM   syNavigationNodes
                                                                 WHERE  ResourceId = 195 )
                                          OR NNChild.ParentId IN ( SELECT   HierarchyId
                                                                   FROM     syNavigationNodes
                                                                   WHERE    ResourceId IN ( 707,463,429,408 ) )
                                        )
                        ) t3
              UNION
              SELECT    192 AS ModuleResourceId
                       ,ResourceId AS ChildResourceId
                       ,Resource AS ChildResource
                       ,(
                          SELECT TOP 1
                                    t1.AllowSchlReqFlds
                          FROM      syResources t1
                          WHERE     t1.ResourceId = ResourceId
                        ) AS AllowSchlReqFlds
                       ,ResourceURL AS ChildResourceURL
                       ,NULL AS ParentResourceId
                       ,NULL AS ParentResource
                       ,1 AS GroupSortOrder
                       ,NULL AS FirstSortOrder
                       ,NULL AS DisplaySequence
                       ,ResourceTypeID
              FROM      syResources
              WHERE     ResourceId = 192
              UNION
              SELECT    192 AS ModuleResourceId
                       ,ChildResourceId
                       ,ChildResource
                       ,(
                          SELECT TOP 1
                                    t1.AllowSchlReqFlds
                          FROM      syResources t1
                          WHERE     t1.ResourceId = t3.ChildResourceId
                        ) AS AllowSchlReqFlds
                       ,ChildResourceURL
                       ,ParentResourceId
                       ,ParentResource
                       ,GroupSortOrder
                       ,(
                          SELECT TOP 1
                                    HierarchyIndex
                          FROM      dbo.syNavigationNodes
                          WHERE     ResourceId = ChildResourceId
                        ) AS FirstSortOrder
                       ,CASE WHEN (
                                    ChildResourceId IN ( 709 )
                                    OR ParentResourceId IN ( 709 )
                                  ) THEN 1
                             ELSE 2
                        END AS DisplaySequence
                       ,ResourceTypeID
              FROM      (
                          SELECT DISTINCT
                                    NNChild.ResourceId AS ChildResourceId
                                   ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                         ELSE RChild.Resource
                                    END AS ChildResource
                                   ,RChild.ResourceURL AS ChildResourceURL
                                   ,CASE WHEN (
                                                NNParent.ResourceId = 192
                                                OR NNChild.ResourceId IN ( 688,709 )
                                              ) THEN NULL
                                         ELSE NNParent.ResourceId
                                    END AS ParentResourceId
                                   ,RParent.Resource AS ParentResource
                                   ,RParentModule.Resource AS MODULE
                                   ,CASE WHEN (
                                                NNChild.ResourceId = 709
                                                OR NNParent.ResourceId = 709
                                              ) THEN 2
                                         ELSE 3
                                    END AS GroupSortOrder
                                   ,RChild.ResourceTypeID
                          FROM      syResources RChild
                          INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
                          INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                          INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                          LEFT OUTER JOIN (
                                            SELECT  *
                                            FROM    syResources
                                            WHERE   ResourceTypeId = 1
                                          ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                          WHERE     RChild.ResourceTypeId IN ( 2,4,8 )
                                    AND (
                                          RChild.ChildTypeId IS NULL
                                          OR RChild.ChildTypeId = 4
                                        )
                                    AND ( RChild.ResourceId <> 394 )
                                    AND ( NNParent.ResourceId NOT IN ( 394 ) )
                                    AND (
                                          NNParent.ParentId IN ( SELECT HierarchyId
                                                                 FROM   syNavigationNodes
                                                                 WHERE  ResourceId = 192 )
                                          OR NNChild.ParentId IN ( SELECT   HierarchyId
                                                                   FROM     syNavigationNodes
                                                                   WHERE    ResourceId IN ( 709 ) )
                                        )
                        ) t3
            ) t2
    WHERE   --(@ModuleResourceId is null or t2.ModuleResourceId=@ModuleResourceId)  and 
            ( AllowSchlReqFlds = 1 )
            AND ParentResourceId IS NOT NULL
    UNION
-- Reports
    SELECT  ModuleResourceId
           ,ChildResourceId
           ,ChildResource
           ,ResourceTypeId
           ,NULL AS TabId
    FROM    (
              SELECT    189 AS ModuleResourceId
                       ,ResourceId AS ChildResourceId
                       ,Resource AS ChildResource
                       ,(
                          SELECT TOP 1
                                    t1.AllowSchlReqFlds
                          FROM      syResources t1
                          WHERE     t1.ResourceId = ResourceId
                        ) AS AllowSchlReqFlds
                       ,ResourceURL AS ChildResourceURL
                       ,NULL AS ParentResourceId
                       ,NULL AS ParentResource
                       ,1 AS GroupSortOrder
                       ,NULL AS FirstSortOrder
                       ,NULL AS DisplaySequence
                       ,ResourceTypeID
              FROM      syResources
              WHERE     ResourceId = 189
              UNION
              SELECT    189 AS ModuleResourceId
                       ,NNChild.ResourceId AS ChildResourceId
                       ,CASE WHEN NNChild.ResourceId = 395 THEN 'Lead Tabs'
                             ELSE CASE WHEN NNChild.ResourceId = 398 THEN 'Add/View Leads'
                                       ELSE RChild.Resource
                                  END
                        END AS ChildResource
                       ,(
                          SELECT TOP 1
                                    t1.AllowSchlReqFlds
                          FROM      syResources t1
                          WHERE     t1.ResourceId = NNChild.ResourceId
                        ) AS AllowSchlReqFlds
                       ,RChild.ResourceURL AS ChildResourceURL
                       ,CASE WHEN (
                                    NNParent.ResourceId IN ( 689 )
                                    OR NNChild.ResourceId IN ( 710,402,617,734 )
                                  ) THEN NULL
                             ELSE NNParent.ResourceId
                        END AS ParentResourceId
                       ,RParent.Resource AS ParentResource
                       ,CASE WHEN (
                                    NNChild.ResourceId IN ( 710 )
                                    OR NNParent.ResourceId IN ( 710 )
                                  ) THEN 2
                             ELSE CASE WHEN (
                                              NNChild.ResourceId IN ( 617 )
                                              OR NNParent.ResourceId IN ( 617 )
                                            ) THEN 3
                                       ELSE CASE WHEN (
                                                        NNChild.ResourceId IN ( 402 )
                                                        OR NNParent.ResourceId IN ( 402 )
                                                      ) THEN 4
                                                 ELSE 5
                                            END
                                  END
                        END AS GroupSortOrder
                       ,CASE WHEN NNChild.ResourceId = 398 THEN 1
                             ELSE CASE WHEN NNChild.ResourceId = 395 THEN 2
                                       ELSE 3
                                  END
                        END AS FirstSortOrder
                       ,CASE WHEN (
                                    NNChild.ResourceId IN ( 710,734 )
                                    OR NNParent.ResourceId IN ( 710,734 )
                                  ) THEN 1
                             ELSE 2
                        END AS DisplaySequence
                       ,RChild.ResourceTypeID
              FROM      syResources RChild
              INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
              INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
              INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
              LEFT OUTER JOIN (
                                SELECT  *
                                FROM    syResources
                                WHERE   ResourceTypeId = 1
                              ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
              WHERE     RChild.ResourceTypeId IN ( 2,5,8 )
                        AND (
                              RChild.ChildTypeId IS NULL
                              OR RChild.ChildTypeId = 5
                            )
                        AND ( RChild.ResourceId NOT IN ( 395 ) )
                        AND ( NNParent.ResourceId NOT IN ( 395 ) )
                        AND (
                              NNParent.ParentId IN ( SELECT HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = 189 )
                              OR NNChild.ParentId IN ( SELECT   HierarchyId
                                                       FROM     syNavigationNodes
                                                       WHERE    ResourceId IN ( 710,402,617,734 ) )
                            )
              UNION
              SELECT    26 AS ModuleResourceId
                       ,ResourceId AS ChildResourceId
                       ,Resource AS ChildResource
                       ,(
                          SELECT TOP 1
                                    t1.AllowSchlReqFlds
                          FROM      syResources t1
                          WHERE     t1.ResourceId = ResourceId
                        ) AS AllowSchlReqFlds
                       ,ResourceURL AS ChildResourceURL
                       ,NULL AS ParentResourceId
                       ,NULL AS ParentResource
                       ,1 AS GroupSortOrder
                       ,NULL AS FirstSortOrder
                       ,NULL AS DisplaySequence
                       ,ResourceTypeID
              FROM      syResources
              WHERE     ResourceId = 26
              UNION
              SELECT    26 AS ModuleResourceId
                       ,ChildResourceId
                       ,ChildResource
                       ,(
                          SELECT TOP 1
                                    t5.AllowSchlReqFlds
                          FROM      syResources t5
                          WHERE     t5.ResourceId = t1.ChildResourceId
                        ) AS AllowSchlReqFlds
                       ,ChildResourceURL
                       ,ParentResourceId
                       ,ParentResource
                       ,GroupSortOrder
                       ,(
                          SELECT TOP 1
                                    HierarchyIndex
                          FROM      dbo.syNavigationNodes
                          WHERE     ResourceId = ChildResourceId
                        ) AS FirstSortOrder
                       ,CASE WHEN (
                                    ChildResourceId IN ( 691,729,730 )
                                    OR ParentResourceId IN ( 690,691,729,730 )
                                  ) THEN 1
                             ELSE CASE WHEN (
                                              ChildResourceId IN ( 727,692,736 )
                                              OR ParentResourceId IN ( 727,736,692 )
                                            ) THEN 2
                                       ELSE CASE WHEN (
                                                        ChildResourceId IN ( 409 )
                                                        OR ParentResourceId = 409
                                                      ) THEN 3
                                                 ELSE CASE WHEN (
                                                                  ChildResourceId IN ( 719,720,721,725 )
                                                                  OR ParentResourceId IN ( 719,720,721,725 )
                                                                ) THEN 4
                                                           ELSE CASE WHEN (
                                                                            ChildResourceId IN ( 722,723,724 )
                                                                            OR ParentResourceId IN ( 722,723,724 )
                                                                          ) THEN 5
                                                                     ELSE 6
                                                                END
                                                      END
                                            END
                                  END
                        END AS DisplaySequence
                       ,ResourceTypeID
              FROM      (
                          SELECT DISTINCT
                                    NNChild.ResourceId AS ChildResourceId
                                   ,CASE WHEN NNChild.ResourceId = 409 THEN 'IPEDS - General Reports'
                                         ELSE RChild.Resource
                                    END AS ChildResource
                                   ,RChild.ResourceURL AS ChildResourceURL
                                   ,CASE WHEN (
                                                NNParent.ResourceId IN ( 689 )
                                                OR NNChild.ResourceId IN ( 409,472,474,712,719,720,721,722,723,724,725,736 )
                                              ) THEN NULL
                                         ELSE NNParent.ResourceId
                                    END AS ParentResourceId
                                   ,RParent.Resource AS ParentResource
                                   ,RParentModule.Resource AS MODULE
                                   ,CASE WHEN (
                                                NNChild.ResourceId IN ( 691 )
                                                OR NNParent.ResourceId IN ( 691 )
                                              ) THEN 2
                                         ELSE CASE WHEN (
                                                          NNChild.ResourceId IN ( 690 )
                                                          OR NNParent.ResourceId IN ( 690 )
                                                        ) THEN 3
                                                   ELSE CASE WHEN (
                                                                    NNChild.ResourceId IN ( 736 )
                                                                    OR NNParent.ResourceId IN ( 736 )
                                                                  ) THEN 4
                                                             ELSE CASE WHEN (
                                                                              NNChild.ResourceId IN ( 729 )
                                                                              OR NNParent.ResourceId IN ( 729 )
                                                                            ) THEN 5
                                                                       ELSE CASE WHEN (
                                                                                        NNChild.ResourceId IN ( 692 )
                                                                                        OR NNParent.ResourceId IN ( 692 )
                                                                                      ) THEN 6
                                                                                 ELSE CASE WHEN (
                                                                                                  NNChild.ResourceId IN ( 727 )
                                                                                                  OR NNParent.ResourceId IN ( 727 )
                                                                                                ) THEN 7
                                                                                           ELSE 8
                                                                                      END
                                                                            END
                                                                  END
                                                        END
                                              END
                                    END AS GroupSortOrder
                                   ,RChild.ResourceTypeId
                          FROM      syResources RChild
                          INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
                          INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                          INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                          LEFT OUTER JOIN (
                                            SELECT  *
                                            FROM    syResources
                                            WHERE   ResourceTypeId = 1
                                          ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                          WHERE     RChild.ResourceTypeId IN ( 2,5,8 )
                                    AND (
                                          RChild.ChildTypeId IS NULL
                                          OR RChild.ChildTypeId = 5
                                        )
				--AND (NNParent.ResourceId IN (689,409,711,472,474,712))
                                    AND ( RChild.ResourceId NOT IN ( 394,472,474,711,472,474,409,719,720,721,722,723,724,725 ) )
                                    AND (
                                          NNParent.Parentid IN ( SELECT HierarchyId
                                                                 FROM   syNavigationNodes
                                                                 WHERE  ResourceId = 26 )
                                          OR NNChild.ParentId IN (
                                          SELECT    HierarchyId
                                          FROM      syNavigationNodes
                                          WHERE     --ResourceId IN (472,474,409,719,720,721,722,723,724,725,690,691,692,727,729,730,736)
                                                    ResourceId IN ( 691,690,692,727,729,736,730 ) )
                                        )
                        ) t1
              UNION
              SELECT    194 AS ModuleResourceId
                       ,ResourceId AS ChildResourceId
                       ,Resource AS ChildResource
                       ,(
                          SELECT TOP 1
                                    t1.AllowSchlReqFlds
                          FROM      syResources t1
                          WHERE     t1.ResourceId = ResourceId
                        ) AS AllowSchlReqFlds
                       ,ResourceURL AS ChildResourceURL
                       ,NULL AS ParentResourceId
                       ,NULL AS ParentResource
                       ,1 AS GroupSortOrder
                       ,NULL AS FirstSortOrder
                       ,NULL AS DisplaySequence
                       ,ResourceTypeID
              FROM      syResources
              WHERE     ResourceId = 194
              UNION
              SELECT    194 AS ModuleResourceId
                       ,ChildResourceId
                       ,ChildResource
                       ,(
                          SELECT TOP 1
                                    t5.AllowSchlReqFlds
                          FROM      syResources t5
                          WHERE     t5.ResourceId = t1.ChildResourceId
                        ) AS AllowSchlReqFlds
                       ,ChildResourceURL
                       ,ParentResourceId
                       ,ParentResource
                       ,GroupSortOrder
                       ,GroupSortOrder AS FirstSortOrder
                       ,CASE WHEN (
                                    ChildResourceId IN ( 731,732 )
                                    OR ParentResourceId IN ( 731,732 )
                                  ) THEN 1
                             ELSE 2
                        END AS DisplaySequence
                       ,ResourceTypeID
              FROM      (
                          SELECT DISTINCT
                                    NNChild.ResourceId AS ChildResourceId
                                   ,CASE WHEN NNChild.ResourceId = 409 THEN 'IPEDS - General Reports'
                                         ELSE RChild.Resource
                                    END AS ChildResource
                                   ,RChild.ResourceURL AS ChildResourceURL
                                   ,CASE WHEN NNParent.ResourceId IN ( 194 )
                                              OR NNChild.ResourceId IN ( 731,732,733 ) THEN NULL
                                         ELSE NNParent.ResourceId
                                    END AS ParentResourceId
                                   ,RParent.Resource AS ParentResource
                                   ,RParentModule.Resource AS MODULE
                                   ,CASE WHEN (
                                                NNChild.ResourceId IN ( 731 )
                                                OR NNParent.ResourceId IN ( 731 )
                                              ) THEN 2
                                         ELSE 3
                                    END AS GroupSortOrder
                                   ,RChild.ResourceTypeId
                          FROM      syResources RChild
                          INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
                          INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                          INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                          LEFT OUTER JOIN (
                                            SELECT  *
                                            FROM    syResources
                                            WHERE   ResourceTypeId = 1
                                          ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                          WHERE     RChild.ResourceTypeId IN ( 2,5,8 )
                                    AND (
                                          RChild.ChildTypeId IS NULL
                                          OR RChild.ChildTypeId = 5
                                        )
				--AND (NNParent.ResourceId IN (689,409,711,472,474,712))
                                    AND ( RChild.ResourceId NOT IN ( 394 ) )
                                    AND (
                                          NNParent.Parentid IN ( SELECT HierarchyId
                                                                 FROM   syNavigationNodes
                                                                 WHERE  ResourceId = 194 )
                                          OR NNChild.ParentId IN ( SELECT   HierarchyId
                                                                   FROM     syNavigationNodes
                                                                   WHERE    ResourceId IN ( 731,732,733 ) )
                                        )
                        ) t1
              UNION
              SELECT    300 AS ModuleResourceId
                       ,ResourceId AS ChildResourceId
                       ,Resource AS ChildResource
                       ,(
                          SELECT TOP 1
                                    t1.AllowSchlReqFlds
                          FROM      syResources t1
                          WHERE     t1.ResourceId = ResourceId
                        ) AS AllowSchlReqFlds
                       ,ResourceURL AS ChildResourceURL
                       ,NULL AS ParentResourceId
                       ,NULL AS ParentResource
                       ,1 AS GroupSortOrder
                       ,NULL AS FirstSortOrder
                       ,NULL AS DisplaySequence
                       ,ResourceTypeID
              FROM      syResources
              WHERE     ResourceId = 300
              UNION
              SELECT    300 AS ModuleResourceId
                       ,ChildResourceId
                       ,ChildResource
                       ,(
                          SELECT TOP 1
                                    t5.AllowSchlReqFlds
                          FROM      syResources t5
                          WHERE     t5.ResourceId = t1.ChildResourceId
                        ) AS AllowSchlReqFlds
                       ,ChildResourceURL
                       ,ParentResourceId
                       ,ParentResource
                       ,GroupSortOrder
                       ,GroupSortOrder AS FirstSortOrder
                       ,1 AS DisplaySequence
                       ,ResourceTypeID
              FROM      (
                          SELECT DISTINCT
                                    NNChild.ResourceId AS ChildResourceId
                                   ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                         ELSE RChild.Resource
                                    END AS ChildResource
                                   ,RChild.ResourceURL AS ChildResourceURL
                                   ,CASE WHEN NNParent.ResourceId = 300
                                              OR NNChild.ResourceId = 715 THEN NULL
                                         ELSE NNParent.ResourceId
                                    END AS ParentResourceId
                                   ,RParent.Resource AS ParentResource
                                   ,RParentModule.Resource AS MODULE
                                   ,CASE WHEN (
                                                NNChild.ResourceId IN ( 715 )
                                                OR NNParent.ResourceId IN ( 715 )
                                              ) THEN 2
                                         ELSE 3
                                    END AS GroupSortOrder
                                   ,RChild.ResourceTypeId
                          FROM      syResources RChild
                          INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
                          INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                          INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                          LEFT OUTER JOIN (
                                            SELECT  *
                                            FROM    syResources
                                            WHERE   ResourceTypeId = 1
                                          ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                          WHERE     RChild.ResourceTypeId IN ( 2,5,8 )
                                    AND (
                                          RChild.ChildTypeId IS NULL
                                          OR RChild.ChildTypeId = 5
                                        )
				--AND (NNParent.ResourceId IN (689,409,711,472,474,712))
                                    AND ( RChild.ResourceId NOT IN ( 394 ) )
                                    AND (
                                          NNParent.Parentid IN ( SELECT HierarchyId
                                                                 FROM   syNavigationNodes
                                                                 WHERE  ResourceId = 300 )
                                          OR NNChild.ParentId IN ( SELECT   HierarchyId
                                                                   FROM     syNavigationNodes
                                                                   WHERE    ResourceId IN ( 715 ) )
                                        )
                        ) t1
              UNION
              SELECT    191 AS ModuleResourceId
                       ,ResourceId AS ChildResourceId
                       ,Resource AS ChildResource
                       ,(
                          SELECT TOP 1
                                    t1.AllowSchlReqFlds
                          FROM      syResources t1
                          WHERE     t1.ResourceId = ResourceId
                        ) AS AllowSchlReqFlds
                       ,ResourceURL AS ChildResourceURL
                       ,NULL AS ParentResourceId
                       ,NULL AS ParentResource
                       ,1 AS GroupSortOrder
                       ,NULL AS FirstSortOrder
                       ,NULL AS DisplaySequence
                       ,ResourceTypeID
              FROM      syResources
              WHERE     ResourceId = 191
              UNION
              SELECT    191 AS ModuleResourceId
                       ,ChildResourceId
                       ,ChildResource
                       ,(
                          SELECT TOP 1
                                    t5.AllowSchlReqFlds
                          FROM      syResources t5
                          WHERE     t5.ResourceId = t1.ChildResourceId
                        ) AS AllowSchlReqFlds
                       ,ChildResourceURL
                       ,ParentResourceId
                       ,ParentResource
                       ,GroupSortOrder
                       ,GroupSortOrder AS FirstSortOrder
                       ,CASE WHEN (
                                    ChildResourceId IN ( 716 )
                                    OR ParentResourceId IN ( 716 )
                                  ) THEN 1
                             ELSE 2
                        END AS DisplaySequence
                       ,ResourceTypeID
              FROM      (
                          SELECT DISTINCT
                                    NNChild.ResourceId AS ChildResourceId
                                   ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                         ELSE RChild.Resource
                                    END AS ChildResource
                                   ,RChild.ResourceURL AS ChildResourceURL
                                   ,CASE WHEN (
                                                NNParent.ResourceId = 191
                                                OR NNChild.ResourceId = 716
                                              ) THEN NULL
                                         ELSE NNParent.ResourceId
                                    END AS ParentResourceId
                                   ,RParent.Resource AS ParentResource
                                   ,RParentModule.Resource AS MODULE
                                   ,CASE WHEN (
                                                NNChild.ResourceId IN ( 716 )
                                                OR NNParent.ResourceId IN ( 716 )
                                              ) THEN 2
                                         ELSE 3
                                    END AS GroupSortOrder
                                   ,RChild.ResourceTypeId
                          FROM      syResources RChild
                          INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
                          INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                          INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                          LEFT OUTER JOIN (
                                            SELECT  *
                                            FROM    syResources
                                            WHERE   ResourceTypeId = 1
                                          ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                          WHERE     RChild.ResourceTypeId IN ( 2,5,8 )
                                    AND (
                                          RChild.ChildTypeId IS NULL
                                          OR RChild.ChildTypeId = 5
                                        )
				--AND (NNParent.ResourceId IN (689,409,711,472,474,712))
                                    AND ( RChild.ResourceId NOT IN ( 394 ) )
                                    AND (
                                          NNParent.Parentid IN ( SELECT HierarchyId
                                                                 FROM   syNavigationNodes
                                                                 WHERE  ResourceId = 191 )
                                          OR NNChild.ParentId IN ( SELECT   HierarchyId
                                                                   FROM     syNavigationNodes
                                                                   WHERE    ResourceId IN ( 716 ) )
                                        )
                        ) t1
              UNION
              SELECT    193 AS ModuleResourceId
                       ,ResourceId AS ChildResourceId
                       ,Resource AS ChildResource
                       ,(
                          SELECT TOP 1
                                    t1.AllowSchlReqFlds
                          FROM      syResources t1
                          WHERE     t1.ResourceId = ResourceId
                        ) AS AllowSchlReqFlds
                       ,ResourceURL AS ChildResourceURL
                       ,NULL AS ParentResourceId
                       ,NULL AS ParentResource
                       ,1 AS GroupSortOrder
                       ,NULL AS FirstSortOrder
                       ,NULL AS DisplaySequence
                       ,ResourceTypeID
              FROM      syResources
              WHERE     ResourceId = 193
              UNION
              SELECT    193 AS ModuleResourceId
                       ,ChildResourceId
                       ,ChildResource
                       ,(
                          SELECT TOP 1
                                    t5.AllowSchlReqFlds
                          FROM      syResources t5
                          WHERE     t5.ResourceId = t1.ChildResourceId
                        ) AS AllowSchlReqFlds
                       ,ChildResourceURL
                       ,ParentResourceId
                       ,ParentResource
                       ,GroupSortOrder
                       ,GroupSortOrder AS FirstSortOrder
                       ,CASE WHEN (
                                    ChildResourceId IN ( 713 )
                                    OR ParentResourceId IN ( 713 )
                                  ) THEN 1
                             ELSE 2
                        END AS DisplaySequence
                       ,ResourceTypeID
              FROM      (
                          SELECT DISTINCT
                                    NNChild.ResourceId AS ChildResourceId
                                   ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                         ELSE RChild.Resource
                                    END AS ChildResource
                                   ,RChild.ResourceURL AS ChildResourceURL
                                   ,CASE WHEN (
                                                NNParent.ResourceId = 193
                                                OR NNChild.ResourceId IN ( 713,735 )
                                              ) THEN NULL
                                         ELSE NNParent.ResourceId
                                    END AS ParentResourceId
                                   ,RParent.Resource AS ParentResource
                                   ,RParentModule.Resource AS MODULE
                                   ,CASE WHEN (
                                                NNChild.ResourceId IN ( 713 )
                                                OR NNParent.ResourceId IN ( 713 )
                                              ) THEN 2
                                         ELSE 3
                                    END AS GroupSortOrder
                                   ,RChild.ResourceTypeId
                          FROM      syResources RChild
                          INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
                          INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                          INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                          LEFT OUTER JOIN (
                                            SELECT  *
                                            FROM    syResources
                                            WHERE   ResourceTypeId = 1
                                          ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                          WHERE     RChild.ResourceTypeId IN ( 2,5,8 )
                                    AND (
                                          RChild.ChildTypeId IS NULL
                                          OR RChild.ChildTypeId = 5
                                        )
				--AND (NNParent.ResourceId IN (689,409,711,472,474,712))
                                    AND ( RChild.ResourceId NOT IN ( 394 ) )
                                    AND (
                                          NNParent.Parentid IN ( SELECT HierarchyId
                                                                 FROM   syNavigationNodes
                                                                 WHERE  ResourceId = 193 )
                                          OR NNChild.ParentId IN ( SELECT   HierarchyId
                                                                   FROM     syNavigationNodes
                                                                   WHERE    ResourceId IN ( 713,735 ) )
                                        )
                        ) t1
              UNION
              SELECT    195 AS ModuleResourceId
                       ,ResourceId AS ChildResourceId
                       ,Resource AS ChildResource
                       ,(
                          SELECT TOP 1
                                    t1.AllowSchlReqFlds
                          FROM      syResources t1
                          WHERE     t1.ResourceId = ResourceId
                        ) AS AllowSchlReqFlds
                       ,ResourceURL AS ChildResourceURL
                       ,NULL AS ParentResourceId
                       ,NULL AS ParentResource
                       ,1 AS GroupSortOrder
                       ,NULL AS FirstSortOrder
                       ,NULL AS DisplaySequence
                       ,ResourceTypeID
              FROM      syResources
              WHERE     ResourceId = 195
              UNION
              SELECT    195 AS ModuleResourceId
                       ,ChildResourceId
                       ,ChildResource
                       ,(
                          SELECT TOP 1
                                    t5.AllowSchlReqFlds
                          FROM      syResources t5
                          WHERE     t5.ResourceId = t1.ChildResourceId
                        ) AS AllowSchlReqFlds
                       ,ChildResourceURL
                       ,ParentResourceId
                       ,ParentResource
                       ,GroupSortOrder
                       ,GroupSortOrder AS FirstSortOrder
                       ,CASE WHEN (
                                    ChildResourceId IN ( 717 )
                                    OR ParentResourceId IN ( 717 )
                                  ) THEN 1
                             ELSE 2
                        END AS DisplaySequence
                       ,ResourceTypeID
              FROM      (
                          SELECT DISTINCT
                                    NNChild.ResourceId AS ChildResourceId
                                   ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                         ELSE RChild.Resource
                                    END AS ChildResource
                                   ,RChild.ResourceURL AS ChildResourceURL
                                   ,CASE WHEN (
                                                NNParent.ResourceId = 195
                                                OR NNChild.ResourceId IN ( 717 )
                                              ) THEN NULL
                                         ELSE NNParent.ResourceId
                                    END AS ParentResourceId
                                   ,RParent.Resource AS ParentResource
                                   ,RParentModule.Resource AS MODULE
                                   ,CASE WHEN (
                                                NNChild.ResourceId IN ( 717 )
                                                OR NNParent.ResourceId IN ( 717 )
                                              ) THEN 2
                                         ELSE 3
                                    END AS GroupSortOrder
                                   ,RChild.ResourceTypeId
                          FROM      syResources RChild
                          INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
                          INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                          INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                          LEFT OUTER JOIN (
                                            SELECT  *
                                            FROM    syResources
                                            WHERE   ResourceTypeId = 1
                                          ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                          WHERE     RChild.ResourceTypeId IN ( 2,5,8 )
                                    AND (
                                          RChild.ChildTypeId IS NULL
                                          OR RChild.ChildTypeId = 5
                                        )
				--AND (NNParent.ResourceId IN (689,409,711,472,474,712))
                                    AND ( RChild.ResourceId NOT IN ( 394 ) )
                                    AND (
                                          NNParent.Parentid IN ( SELECT HierarchyId
                                                                 FROM   syNavigationNodes
                                                                 WHERE  ResourceId = 195 )
                                          OR NNChild.ParentId IN ( SELECT   HierarchyId
                                                                   FROM     syNavigationNodes
                                                                   WHERE    ResourceId IN ( 717 ) )
                                        )
                        ) t1
            ) t2
    WHERE   --(@ModuleResourceId is null or t2.ModuleResourceId=@ModuleResourceId)  
            ( AllowSchlReqFlds = 1 )
            AND ParentResourceId IS NOT NULL
    UNION
--Tabs
    SELECT  ModuleResourceId
           ,ChildResourceId
           ,CASE WHEN ChildResourceId = 52 THEN 'Employee Info'
                 ELSE CASE WHEN ChildResourceId = 170 THEN 'Lead Info'
                           ELSE CASE WHEN ChildResourceId = 203 THEN 'Student Info'
                                     ELSE CASE WHEN ChildResourceId = 79 THEN 'Employer Info'
                                               ELSE ChildResource
                                          END
                                END
                      END
            END AS ChildResource
           ,ResourceTypeId
           ,TabId AS TabId
    FROM    (
              SELECT    189 AS ModuleResourceId
                       ,395 AS TabId
                       ,ResourceId AS ChildResourceId
                       ,Resource AS ChildResource
                       ,(
                          SELECT TOP 1
                                    t1.AllowSchlReqFlds
                          FROM      syResources t1
                          WHERE     t1.ResourceId = ResourceId
                        ) AS AllowSchlReqFlds
                       ,ResourceURL AS ChildResourceURL
                       ,NULL AS ParentResourceId
                       ,NULL AS ParentResource
                       ,1 AS GroupSortOrder
                       ,NULL AS FirstSortOrder
                       ,NULL AS DisplaySequence
                       ,ResourceTypeID
              FROM      syResources
              WHERE     ResourceId = 189
              UNION
              SELECT    189 AS ModuleResourceId
                       ,395 AS TabId
                       ,NNChild.ResourceId AS ChildResourceId
                       ,CASE WHEN NNChild.ResourceId = 395 THEN 'Lead Tabs'
                             ELSE CASE WHEN NNChild.ResourceId = 398 THEN 'Add/View Leads'
                                       ELSE RChild.Resource
                                  END
                        END AS ChildResource
                       ,(
                          SELECT TOP 1
                                    t5.AllowSchlReqFlds
                          FROM      syResources t5
                          WHERE     t5.ResourceId = NNChild.ResourceId
                        ) AS AllowSchlReqFlds
                       ,RChild.ResourceURL AS ChildResourceURL
                       ,CASE WHEN (
                                    NNParent.ResourceId IN ( 395 )
                                    OR NNChild.ResourceId IN ( 737,740,741 )
                                  ) THEN NULL
                             ELSE NNParent.ResourceId
                        END AS ParentResourceId
                       ,RParent.Resource AS ParentResource
                       ,CASE WHEN (
                                    NNChild.ResourceId = 737
                                    OR NNParent.ResourceId IN ( 737 )
                                  ) THEN 1
                             ELSE CASE WHEN (
                                              NNChild.ResourceId = 740
                                              OR NNParent.ResourceId IN ( 740 )
                                            ) THEN 2
                                       ELSE 3
                                  END
                        END AS GroupSortOrder
                       ,CASE WHEN NNChild.ResourceId = 737 THEN 1
                             ELSE CASE WHEN NNChild.ResourceId = 395 THEN 2
                                       ELSE 3
                                  END
                        END AS FirstSortOrder
                       ,CASE WHEN (
                                    NNChild.ResourceId IN ( 737 )
                                    OR NNParent.ResourceId IN ( 737 )
                                  ) THEN 1
                             ELSE 2
                        END AS DisplaySequence
                       ,RChild.ResourceTypeID
              FROM      syResources RChild
              INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
              INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
              INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
              LEFT OUTER JOIN (
                                SELECT  *
                                FROM    syResources
                                WHERE   ResourceTypeId = 1
                              ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
              WHERE     RChild.ResourceTypeId IN ( 2,3,8 )
                        AND (
                              RChild.ChildTypeId IS NULL
                              OR RChild.ChildTypeId = 3
                            )
                        AND ( RChild.ResourceId NOT IN ( 394 ) )
                        AND (
                              NNParent.ParentId IN ( SELECT HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = 395 )
                              OR NNChild.HierarchyId IN ( SELECT DISTINCT
                                                                    HierarchyId
                                                          FROM      syNavigationNodes
                                                          WHERE     ResourceId IN ( 737,740,741 )
                                                                    AND ParentId IN ( SELECT    HierarchyId
                                                                                      FROM      syNavigationNodes
                                                                                      WHERE     ResourceId = 395
                                                                                                AND ParentId IN ( SELECT    HierarchyId
                                                                                                                  FROM      syNavigationNodes
                                                                                                                  WHERE     ResourceId = 189 ) ) )
                            )
              UNION ALL
              SELECT    26 AS ModuleResourceId
                       ,394 AS TabId
                       ,ResourceId AS ChildResourceId
                       ,Resource AS ChildResource
                       ,(
                          SELECT TOP 1
                                    t1.AllowSchlReqFlds
                          FROM      syResources t1
                          WHERE     t1.ResourceId = ResourceId
                        ) AS AllowSchlReqFlds
                       ,ResourceURL AS ChildResourceURL
                       ,NULL AS ParentResourceId
                       ,NULL AS ParentResource
                       ,1 AS GroupSortOrder
                       ,NULL AS FirstSortOrder
                       ,NULL AS DisplaySequence
                       ,ResourceTypeID
              FROM      syResources
              WHERE     ResourceId = 26
              UNION
              SELECT    26 AS ModuleResourceId
                       ,394 AS TabId
                       ,ChildResourceId
                       ,ChildResource
                       ,(
                          SELECT TOP 1
                                    t5.AllowSchlReqFlds
                          FROM      syResources t5
                          WHERE     t5.ResourceId = t1.ChildResourceId
                        ) AS AllowSchlReqFlds
                       ,ChildResourceURL
                       ,ParentResourceId
                       ,ParentResource
                       ,CASE WHEN (
                                    t1.ChildResourceId = 737
                                    OR t1.ParentResourceId IN ( 737 )
                                  ) THEN 2
                             ELSE CASE WHEN (
                                              t1.ChildResourceId = 738
                                              OR t1.ParentResourceId IN ( 738 )
                                            ) THEN 3
                                       ELSE CASE WHEN (
                                                        t1.ChildResourceId = 739
                                                        OR t1.ParentResourceId IN ( 739 )
                                                      ) THEN 4
                                                 ELSE CASE WHEN (
                                                                  t1.ChildResourceId = 740
                                                                  OR t1.ParentResourceId IN ( 740 )
                                                                ) THEN 5
                                                           ELSE CASE WHEN (
                                                                            t1.ChildResourceId = 741
                                                                            OR t1.ParentResourceId IN ( 741 )
                                                                          ) THEN 6
                                                                     ELSE 7
                                                                END
                                                      END
                                            END
                                  END
                        END AS GroupSortOrder
                       ,CASE WHEN ChildResourceId = 738 THEN 1
                             ELSE 2
                        END AS FirstSortOrder
                       ,CASE WHEN (
                                    ChildResourceId IN ( 738 )
                                    OR ParentResourceId IN ( 738 )
                                  ) THEN 1
                             ELSE 2
                        END AS DisplaySequence
                       ,ResourceTypeID
              FROM      (
                          SELECT DISTINCT
                                    NNChild.ResourceId AS ChildResourceId
                                   ,CASE WHEN NNChild.ResourceId = 409 THEN 'IPEDS - General Reports'
                                         ELSE RChild.Resource
                                    END AS ChildResource
                                   ,RChild.ResourceURL AS ChildResourceURL
                                   ,CASE WHEN (
                                                NNParent.ResourceId IN ( 689 )
                                                OR NNChild.ResourceId IN ( 737,738,739,740,741,742 )
                                              ) THEN NULL
                                         ELSE NNParent.ResourceId
                                    END AS ParentResourceId
                                   ,RParent.Resource AS ParentResource
                                   ,RParentModule.Resource AS MODULE
                                   ,RChild.ResourceTypeID
                          FROM      syResources RChild
                          INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
                          INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                          INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                          LEFT OUTER JOIN (
                                            SELECT  *
                                            FROM    syResources
                                            WHERE   ResourceTypeId = 1
                                          ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                          WHERE     RChild.ResourceTypeId IN ( 2,3,8 )
                                    AND (
                                          RChild.ChildTypeId IS NULL
                                          OR RChild.ChildTypeId = 3
                                        )
                                    AND ( NNParent.ParentId IN ( SELECT HierarchyId
                                                                 FROM   syNavigationNodes
                                                                 WHERE  ResourceId = 394
                                                                        AND ParentId IN ( SELECT    HierarchyId
                                                                                          FROM      syNavigationNodes
                                                                                          WHERE     ResourceId = 26 ) ) )
                          UNION
                          SELECT DISTINCT
                                    RChild.ResourceId AS ChildResourceId
                                   ,RChild.Resource AS ChildResource
                                   ,RChild.ResourceURL AS ChildResourceURL
                                   ,CASE WHEN ( RChild.ResourceId IN ( 737,738,739,740,741,742 ) ) THEN NULL
                                         ELSE RChild.ResourceId
                                    END AS ParentResourceId
                                   ,NULL AS ParentResource
                                   ,NULL AS MODULE
                                   ,RChild.ResourceTypeID
                          FROM      syResources RChild
                          WHERE     RChild.ResourceId IN ( 737,738,739,740,741,742 )
                        ) t1
              UNION ALL
              SELECT    194 AS ModuleResourceId
                       ,394 AS TabId
                       ,ResourceId AS ChildResourceId
                       ,Resource AS ChildResource
                       ,(
                          SELECT TOP 1
                                    t1.AllowSchlReqFlds
                          FROM      syResources t1
                          WHERE     t1.ResourceId = ResourceId
                        ) AS AllowSchlReqFlds
                       ,ResourceURL AS ChildResourceURL
                       ,NULL AS ParentResourceId
                       ,NULL AS ParentResource
                       ,1 AS GroupSortOrder
                       ,NULL AS FirstSortOrder
                       ,NULL AS DisplaySequence
                       ,ResourceTypeID
              FROM      syResources
              WHERE     ResourceId = 194
              UNION
              SELECT    194 AS ModuleResourceId
                       ,394 AS TabId
                       ,ChildResourceId
                       ,ChildResource
                       ,(
                          SELECT TOP 1
                                    t5.AllowSchlReqFlds
                          FROM      syResources t5
                          WHERE     t5.ResourceId = t1.ChildResourceId
                        ) AS AllowSchlReqFlds
                       ,ChildResourceURL
                       ,ParentResourceId
                       ,ParentResource
                       ,CASE WHEN (
                                    t1.ChildResourceId = 737
                                    OR t1.ParentResourceId IN ( 737 )
                                  ) THEN 1
                             ELSE CASE WHEN (
                                              t1.ChildResourceId = 738
                                              OR t1.ParentResourceId IN ( 738 )
                                            ) THEN 2
                                       ELSE CASE WHEN (
                                                        t1.ChildResourceId = 739
                                                        OR t1.ParentResourceId IN ( 739 )
                                                      ) THEN 3
                                                 ELSE CASE WHEN (
                                                                  t1.ChildResourceId = 740
                                                                  OR t1.ParentResourceId IN ( 740 )
                                                                ) THEN 4
                                                           ELSE CASE WHEN (
                                                                            t1.ChildResourceId = 741
                                                                            OR t1.ParentResourceId IN ( 741 )
                                                                          ) THEN 5
                                                                     ELSE 6
                                                                END
                                                      END
                                            END
                                  END
                        END AS GroupSortOrder
                       ,CASE WHEN ChildResourceId = 737 THEN 1
                             ELSE 2
                        END AS FirstSortOrder
                       ,CASE WHEN (
                                    ChildResourceId IN ( 737,738,740,741 )
                                    OR ParentResourceId IN ( 737,738,740,741 )
                                  ) THEN 1
                             ELSE 2
                        END AS DisplaySequence
                       ,ResourceTypeID
              FROM      (
                          SELECT DISTINCT
                                    NNChild.ResourceId AS ChildResourceId
                                   ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                         ELSE RChild.Resource
                                    END AS ChildResource
                                   ,RChild.ResourceURL AS ChildResourceURL
                                   ,CASE WHEN NNParent.ResourceId IN ( 194 )
                                              OR NNChild.ResourceId IN ( 737,738,739,740,741,742 ) THEN NULL
                                         ELSE NNParent.ResourceId
                                    END AS ParentResourceId
                                   ,RParent.Resource AS ParentResource
                                   ,RParentModule.Resource AS MODULE
                                   ,RChild.ResourceTypeID
                          FROM      syResources RChild
                          INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
                          INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                          INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                          LEFT OUTER JOIN (
                                            SELECT  *
                                            FROM    syResources
                                            WHERE   ResourceTypeId = 1
                                          ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                          WHERE     RChild.ResourceTypeId IN ( 2,3,8 )
                                    AND (
                                          RChild.ChildTypeId IS NULL
                                          OR RChild.ChildTypeId = 3
                                        )
                                    AND ( NNParent.ParentId IN ( SELECT HierarchyId
                                                                 FROM   syNavigationNodes
                                                                 WHERE  ResourceId = 394
                                                                        AND ParentId IN ( SELECT    HierarchyId
                                                                                          FROM      syNavigationNodes
                                                                                          WHERE     ResourceId = 194 ) ) )
                          UNION
                          SELECT DISTINCT
                                    RChild.ResourceId AS ChildResourceId
                                   ,RChild.Resource AS ChildResource
                                   ,RChild.ResourceURL AS ChildResourceURL
                                   ,CASE WHEN ( RChild.ResourceId IN ( 737,738,739,740,741,742 ) ) THEN NULL
                                         ELSE RChild.ResourceId
                                    END AS ParentResourceId
                                   ,NULL AS ParentResource
                                   ,NULL AS MODULE
                                   ,RChild.ResourceTypeID
                          FROM      syResources RChild
                          WHERE     RChild.ResourceId IN ( 737,738,739,740,741,742 )
                        ) t1
              UNION ALL
              SELECT    300 AS ModuleResourceId
                       ,394 AS TabId
                       ,ResourceId AS ChildResourceId
                       ,Resource AS ChildResource
                       ,(
                          SELECT TOP 1
                                    t1.AllowSchlReqFlds
                          FROM      syResources t1
                          WHERE     t1.ResourceId = ResourceId
                        ) AS AllowSchlReqFlds
                       ,ResourceURL AS ChildResourceURL
                       ,NULL AS ParentResourceId
                       ,NULL AS ParentResource
                       ,1 AS GroupSortOrder
                       ,NULL AS FirstSortOrder
                       ,NULL AS DisplaySequence
                       ,ResourceTypeID
              FROM      syResources
              WHERE     ResourceId = 300
              UNION
              SELECT    300 AS ModuleResourceId
                       ,394 AS TabId
                       ,ChildResourceId
                       ,ChildResource
                       ,(
                          SELECT TOP 1
                                    t5.AllowSchlReqFlds
                          FROM      syResources t5
                          WHERE     t5.ResourceId = t1.ChildResourceId
                        ) AS AllowSchlReqFlds
                       ,ChildResourceURL
                       ,ParentResourceId
                       ,ParentResource
                       ,CASE WHEN (
                                    t1.ChildResourceId = 737
                                    OR t1.ParentResourceId IN ( 737 )
                                  ) THEN 1
                             ELSE CASE WHEN (
                                              t1.ChildResourceId = 738
                                              OR t1.ParentResourceId IN ( 738 )
                                            ) THEN 2
                                       ELSE CASE WHEN (
                                                        t1.ChildResourceId = 739
                                                        OR t1.ParentResourceId IN ( 739 )
                                                      ) THEN 3
                                                 ELSE CASE WHEN (
                                                                  t1.ChildResourceId = 740
                                                                  OR t1.ParentResourceId IN ( 740 )
                                                                ) THEN 4
                                                           ELSE CASE WHEN (
                                                                            t1.ChildResourceId = 741
                                                                            OR t1.ParentResourceId IN ( 741 )
                                                                          ) THEN 5
                                                                     ELSE 6
                                                                END
                                                      END
                                            END
                                  END
                        END AS GroupSortOrder
                       ,CASE WHEN ChildResourceId = 737 THEN 1
                             ELSE 2
                        END AS FirstSortOrder
                       ,CASE WHEN (
                                    ChildResourceId IN ( 737,739 )
                                    OR ParentResourceId IN ( 737,739 )
                                  ) THEN 1
                             ELSE 2
                        END AS DisplaySequence
                       ,ResourceTypeID
              FROM      (
                          SELECT DISTINCT
                                    NNChild.ResourceId AS ChildResourceId
                                   ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                         ELSE RChild.Resource
                                    END AS ChildResource
                                   ,RChild.ResourceURL AS ChildResourceURL
                                   ,CASE WHEN NNParent.ResourceId = 300
                                              OR NNChild.ResourceId IN ( 737,739,740,741 ) THEN NULL
                                         ELSE NNParent.ResourceId
                                    END AS ParentResourceId
                                   ,RParent.Resource AS ParentResource
                                   ,RParentModule.Resource AS MODULE
                                   ,RChild.ResourceTypeID
                          FROM      syResources RChild
                          INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
                          INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                          INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                          LEFT OUTER JOIN (
                                            SELECT  *
                                            FROM    syResources
                                            WHERE   ResourceTypeId = 1
                                          ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                          WHERE     RChild.ResourceTypeId IN ( 2,3,8 )
                                    AND (
                                          RChild.ChildTypeId IS NULL
                                          OR RChild.ChildTypeId = 3
                                        )
                                    AND ( NNParent.ParentId IN ( SELECT HierarchyId
                                                                 FROM   syNavigationNodes
                                                                 WHERE  ResourceId = 394
                                                                        AND ParentId IN ( SELECT    HierarchyId
                                                                                          FROM      syNavigationNodes
                                                                                          WHERE     ResourceId = 300 ) ) )
                          UNION
                          SELECT DISTINCT
                                    RChild.ResourceId AS ChildResourceId
                                   ,RChild.Resource AS ChildResource
                                   ,RChild.ResourceURL AS ChildResourceURL
                                   ,CASE WHEN ( RChild.ResourceId IN ( 737,739,740,741 ) ) THEN NULL
                                         ELSE RChild.ResourceId
                                    END AS ParentResourceId
                                   ,NULL AS ParentResource
                                   ,NULL AS MODULE
                                   ,RChild.ResourceTypeID
                          FROM      syResources RChild
                          WHERE     RChild.ResourceId IN ( 737,738,739,740,741,742 )
                        ) t1
              UNION
              SELECT    191 AS ModuleResourceId
                       ,394 AS TabId
                       ,ResourceId AS ChildResourceId
                       ,Resource AS ChildResource
                       ,(
                          SELECT TOP 1
                                    t1.AllowSchlReqFlds
                          FROM      syResources t1
                          WHERE     t1.ResourceId = ResourceId
                        ) AS AllowSchlReqFlds
                       ,ResourceURL AS ChildResourceURL
                       ,NULL AS ParentResourceId
                       ,NULL AS ParentResource
                       ,1 AS GroupSortOrder
                       ,NULL AS FirstSortOrder
                       ,NULL AS DisplaySequence
                       ,ResourceTypeID
              FROM      syResources
              WHERE     ResourceId = 191
              UNION
              SELECT    191 AS ModuleResourceId
                       ,394 AS TabId
                       ,ChildResourceId
                       ,ChildResource
                       ,(
                          SELECT TOP 1
                                    t5.AllowSchlReqFlds
                          FROM      syResources t5
                          WHERE     t5.ResourceId = t1.ChildResourceId
                        ) AS AllowSchlReqFlds
                       ,ChildResourceURL
                       ,ParentResourceId
                       ,ParentResource
                       ,CASE WHEN (
                                    t1.ChildResourceId = 737
                                    OR t1.ParentResourceId IN ( 737 )
                                  ) THEN 1
                             ELSE CASE WHEN (
                                              t1.ChildResourceId = 738
                                              OR t1.ParentResourceId IN ( 738 )
                                            ) THEN 2
                                       ELSE CASE WHEN (
                                                        t1.ChildResourceId = 739
                                                        OR t1.ParentResourceId IN ( 739 )
                                                      ) THEN 3
                                                 ELSE CASE WHEN (
                                                                  t1.ChildResourceId = 740
                                                                  OR t1.ParentResourceId IN ( 740 )
                                                                ) THEN 4
                                                           ELSE CASE WHEN (
                                                                            t1.ChildResourceId = 741
                                                                            OR t1.ParentResourceId IN ( 741 )
                                                                          ) THEN 5
                                                                     ELSE 6
                                                                END
                                                      END
                                            END
                                  END
                        END AS GroupSortOrder
                       ,CASE WHEN ChildResourceId = 737 THEN 1
                             ELSE 2
                        END AS FirstSortOrder
                       ,CASE WHEN (
                                    ChildResourceId IN ( 737,738,740,741 )
                                    OR ParentResourceId IN ( 737,738,740,741 )
                                  ) THEN 1
                             ELSE 2
                        END AS DisplaySequence
                       ,ResourceTypeID
              FROM      (
                          SELECT DISTINCT
                                    NNChild.ResourceId AS ChildResourceId
                                   ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                         ELSE RChild.Resource
                                    END AS ChildResource
                                   ,RChild.ResourceURL AS ChildResourceURL
                                   ,CASE WHEN (
                                                NNParent.ResourceId = 191
                                                OR NNChild.ResourceId IN ( 737,738,739,740,741,742 )
                                              ) THEN NULL
                                         ELSE NNParent.ResourceId
                                    END AS ParentResourceId
                                   ,RParent.Resource AS ParentResource
                                   ,RParentModule.Resource AS MODULE
                                   ,RChild.ResourceTypeID
                          FROM      syResources RChild
                          INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
                          INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                          INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                          LEFT OUTER JOIN (
                                            SELECT  *
                                            FROM    syResources
                                            WHERE   ResourceTypeId = 1
                                          ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                          WHERE     RChild.ResourceTypeId IN ( 2,3,8 )
                                    AND (
                                          RChild.ChildTypeId IS NULL
                                          OR RChild.ChildTypeId = 3
                                        )
                                    AND ( NNParent.ParentId IN ( SELECT HierarchyId
                                                                 FROM   syNavigationNodes
                                                                 WHERE  ResourceId = 394
                                                                        AND ParentId IN ( SELECT    HierarchyId
                                                                                          FROM      syNavigationNodes
                                                                                          WHERE     ResourceId = 191 ) ) )
                          UNION
                          SELECT DISTINCT
                                    RChild.ResourceId AS ChildResourceId
                                   ,RChild.Resource AS ChildResource
                                   ,RChild.ResourceURL AS ChildResourceURL
                                   ,CASE WHEN ( RChild.ResourceId IN ( 737,738,739,740,741,742 ) ) THEN NULL
                                         ELSE RChild.ResourceId
                                    END AS ParentResourceId
                                   ,NULL AS ParentResource
                                   ,NULL AS MODULE
                                   ,RChild.ResourceTypeID
                          FROM      syResources RChild
                          WHERE     RChild.ResourceId IN ( 737,738,739,740,741,742 )
                        ) t1
              UNION
              SELECT    193 AS ModuleResourceId
                       ,394 AS TabId
                       ,ResourceId AS ChildResourceId
                       ,Resource AS ChildResource
                       ,(
                          SELECT TOP 1
                                    t1.AllowSchlReqFlds
                          FROM      syResources t1
                          WHERE     t1.ResourceId = ResourceId
                        ) AS AllowSchlReqFlds
                       ,ResourceURL AS ChildResourceURL
                       ,NULL AS ParentResourceId
                       ,NULL AS ParentResource
                       ,1 AS GroupSortOrder
                       ,NULL AS FirstSortOrder
                       ,NULL AS DisplaySequence
                       ,ResourceTypeID
              FROM      syResources
              WHERE     ResourceId = 193
              UNION
              SELECT    193 AS ModuleResourceId
                       ,394 AS TabId
                       ,ChildResourceId
                       ,ChildResource
                       ,(
                          SELECT TOP 1
                                    t5.AllowSchlReqFlds
                          FROM      syResources t5
                          WHERE     t5.ResourceId = t1.ChildResourceId
                        ) AS AllowSchlReqFlds
                       ,ChildResourceURL
                       ,ParentResourceId
                       ,ParentResource
                       ,CASE WHEN (
                                    t1.ChildResourceId = 737
                                    OR t1.ParentResourceId IN ( 737 )
                                  ) THEN 2
                             ELSE CASE WHEN (
                                              t1.ChildResourceId = 738
                                              OR t1.ParentResourceId IN ( 738 )
                                            ) THEN 3
                                       ELSE CASE WHEN (
                                                        t1.ChildResourceId = 739
                                                        OR t1.ParentResourceId IN ( 739 )
                                                      ) THEN 4
                                                 ELSE CASE WHEN (
                                                                  t1.ChildResourceId = 740
                                                                  OR t1.ParentResourceId IN ( 740 )
                                                                ) THEN 5
                                                           ELSE CASE WHEN (
                                                                            t1.ChildResourceId = 741
                                                                            OR t1.ParentResourceId IN ( 741 )
                                                                          ) THEN 6
                                                                     ELSE 7
                                                                END
                                                      END
                                            END
                                  END
                        END AS GroupSortOrder
                       ,CASE WHEN ChildResourceId = 737 THEN 1
                             ELSE 2
                        END AS FirstSortOrder
                       ,CASE WHEN (
                                    ChildResourceId IN ( 737,738,740,741 )
                                    OR ParentResourceId IN ( 737,738,740,741 )
                                  ) THEN 1
                             ELSE 2
                        END AS DisplaySequence
                       ,ResourceTypeID
              FROM      (
                          SELECT DISTINCT
                                    NNChild.ResourceId AS ChildResourceId
                                   ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                         ELSE RChild.Resource
                                    END AS ChildResource
                                   ,RChild.ResourceURL AS ChildResourceURL
                                   ,CASE WHEN (
                                                NNParent.ResourceId = 193
                                                OR NNChild.ResourceId IN ( 713,735 )
                                              ) THEN NULL
                                         ELSE NNParent.ResourceId
                                    END AS ParentResourceId
                                   ,RParent.Resource AS ParentResource
                                   ,RParentModule.Resource AS MODULE
                                   ,RChild.ResourceTypeID
                          FROM      syResources RChild
                          INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
                          INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                          INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                          LEFT OUTER JOIN (
                                            SELECT  *
                                            FROM    syResources
                                            WHERE   ResourceTypeId = 1
                                          ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                          WHERE     RChild.ResourceTypeId IN ( 2,3,8 )
                                    AND (
                                          RChild.ChildTypeId IS NULL
                                          OR RChild.ChildTypeId = 3
                                        )
                                    AND ( NNParent.ParentId IN ( SELECT HierarchyId
                                                                 FROM   syNavigationNodes
                                                                 WHERE  ResourceId = 394
                                                                        AND ParentId IN ( SELECT    HierarchyId
                                                                                          FROM      syNavigationNodes
                                                                                          WHERE     ResourceId = 193 ) ) )
                          UNION
                          SELECT DISTINCT
                                    RChild.ResourceId AS ChildResourceId
                                   ,RChild.Resource AS ChildResource
                                   ,RChild.ResourceURL AS ChildResourceURL
                                   ,CASE WHEN ( RChild.ResourceId IN ( 737,738,739,741 ) ) THEN NULL
                                         ELSE RChild.ResourceId
                                    END AS ParentResourceId
                                   ,NULL AS ParentResource
                                   ,NULL AS MODULE
                                   ,RChild.ResourceTypeID
                          FROM      syResources RChild
                          WHERE     RChild.ResourceId IN ( 737,738,739,740,741,742 )
                        ) t1
              UNION
              SELECT    193 AS ModuleResourceId
                       ,397 AS TabId
                       ,ResourceId AS ChildResourceId
                       ,Resource AS ChildResource
                       ,(
                          SELECT TOP 1
                                    t1.AllowSchlReqFlds
                          FROM      syResources t1
                          WHERE     t1.ResourceId = ResourceId
                        ) AS AllowSchlReqFlds
                       ,ResourceURL AS ChildResourceURL
                       ,NULL AS ParentResourceId
                       ,NULL AS ParentResource
                       ,1 AS GroupSortOrder
                       ,NULL AS FirstSortOrder
                       ,NULL AS DisplaySequence
                       ,ResourceTypeID
              FROM      syResources
              WHERE     ResourceId = 193
              UNION
              SELECT    193 AS ModuleResourceId
                       ,397 AS TabId
                       ,ChildResourceId
                       ,ChildResource
                       ,(
                          SELECT TOP 1
                                    t5.AllowSchlReqFlds
                          FROM      syResources t5
                          WHERE     t5.ResourceId = t1.ChildResourceId
                        ) AS AllowSchlReqFlds
                       ,ChildResourceURL
                       ,ParentResourceId
                       ,ParentResource
                       ,CASE WHEN (
                                    t1.ChildResourceId = 737
                                    OR t1.ParentResourceId IN ( 737 )
                                  ) THEN 2
                             ELSE CASE WHEN (
                                              t1.ChildResourceId = 738
                                              OR t1.ParentResourceId IN ( 738 )
                                            ) THEN 3
                                       ELSE CASE WHEN (
                                                        t1.ChildResourceId = 739
                                                        OR t1.ParentResourceId IN ( 739 )
                                                      ) THEN 4
                                                 ELSE CASE WHEN (
                                                                  t1.ChildResourceId = 740
                                                                  OR t1.ParentResourceId IN ( 740 )
                                                                ) THEN 5
                                                           ELSE CASE WHEN (
                                                                            t1.ChildResourceId = 741
                                                                            OR t1.ParentResourceId IN ( 741 )
                                                                          ) THEN 6
                                                                     ELSE 6
                                                                END
                                                      END
                                            END
                                  END
                        END AS GroupSortOrder
                       ,CASE WHEN ChildResourceId = 737 THEN 1
                             ELSE 2
                        END AS FirstSortOrder
                       ,CASE WHEN (
                                    ChildResourceId IN ( 737 )
                                    OR ParentResourceId IN ( 737 )
                                  ) THEN 1
                             ELSE 2
                        END AS DisplaySequence
                       ,ResourceTypeID
              FROM      (
                          SELECT DISTINCT
                                    NNChild.ResourceId AS ChildResourceId
                                   ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                         ELSE RChild.Resource
                                    END AS ChildResource
                                   ,RChild.ResourceURL AS ChildResourceURL
                                   ,CASE WHEN (
                                                NNParent.ResourceId = 193
                                                OR NNChild.ResourceId IN ( 737 )
                                              ) THEN NULL
                                         ELSE NNParent.ResourceId
                                    END AS ParentResourceId
                                   ,RParent.Resource AS ParentResource
                                   ,RParentModule.Resource AS MODULE
                                   ,RChild.ResourceTypeID
                          FROM      syResources RChild
                          INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
                          INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                          INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                          LEFT OUTER JOIN (
                                            SELECT  *
                                            FROM    syResources
                                            WHERE   ResourceTypeId = 1
                                          ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                          WHERE     RChild.ResourceTypeId IN ( 2,3,8 )
                                    AND (
                                          RChild.ChildTypeId IS NULL
                                          OR RChild.ChildTypeId = 3
                                        )
                                    AND ( NNParent.ParentId IN ( SELECT HierarchyId
                                                                 FROM   syNavigationNodes
                                                                 WHERE  ResourceId = 397
                                                                        AND ParentId IN ( SELECT    HierarchyId
                                                                                          FROM      syNavigationNodes
                                                                                          WHERE     ResourceId = 193 ) ) )
                          UNION
                          SELECT DISTINCT
                                    RChild.ResourceId AS ChildResourceId
                                   ,RChild.Resource AS ChildResource
                                   ,RChild.ResourceURL AS ChildResourceURL
                                   ,CASE WHEN ( RChild.ResourceId IN ( 737 ) ) THEN NULL
                                         ELSE RChild.ResourceId
                                    END AS ParentResourceId
                                   ,NULL AS ParentResource
                                   ,NULL AS MODULE
                                   ,RChild.ResourceTypeID
                          FROM      syResources RChild
                          WHERE     RChild.ResourceId IN ( 737 )
                        ) t1
              UNION
              SELECT    192 AS ModuleResourceId
                       ,396 AS TabId
                       ,ResourceId AS ChildResourceId
                       ,Resource AS ChildResource
                       ,(
                          SELECT TOP 1
                                    t1.AllowSchlReqFlds
                          FROM      syResources t1
                          WHERE     t1.ResourceId = ResourceId
                        ) AS AllowSchlReqFlds
                       ,ResourceURL AS ChildResourceURL
                       ,NULL AS ParentResourceId
                       ,NULL AS ParentResource
                       ,1 AS GroupSortOrder
                       ,NULL AS FirstSortOrder
                       ,NULL AS DisplaySequence
                       ,ResourceTypeID
              FROM      syResources
              WHERE     ResourceId = 192
              UNION
              SELECT    192 AS ModuleResourceId
                       ,396 AS TabId
                       ,ChildResourceId
                       ,ChildResource
                       ,(
                          SELECT TOP 1
                                    t5.AllowSchlReqFlds
                          FROM      syResources t5
                          WHERE     t5.ResourceId = t1.ChildResourceId
                        ) AS AllowSchlReqFlds
                       ,ChildResourceURL
                       ,ParentResourceId
                       ,ParentResource
                       ,CASE WHEN (
                                    ChildResourceId IN ( 713 )
                                    OR ParentResourceId IN ( 713 )
                                  ) THEN 1
                             ELSE 2
                        END AS GroupSortOrder
                       ,CASE WHEN (
                                    ChildResourceId IN ( 713 )
                                    OR ParentResourceId IN ( 713 )
                                  ) THEN 1
                             ELSE 2
                        END AS FirstSortOrder
                       ,CASE WHEN (
                                    ChildResourceId IN ( 713 )
                                    OR ParentResourceId IN ( 713 )
                                  ) THEN 1
                             ELSE 2
                        END AS DisplaySequence
                       ,ResourceTypeID
              FROM      (
                          SELECT DISTINCT
                                    NNChild.ResourceId AS ChildResourceId
                                   ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                         ELSE RChild.Resource
                                    END AS ChildResource
                                   ,RChild.ResourceURL AS ChildResourceURL
                                   ,CASE WHEN (
                                                NNParent.ResourceId = 193
                                                OR NNChild.ResourceId IN ( 713,735 )
                                              ) THEN NULL
                                         ELSE NNParent.ResourceId
                                    END AS ParentResourceId
                                   ,RParent.Resource AS ParentResource
                                   ,RParentModule.Resource AS MODULE
                                   ,RChild.ResourceTypeID
                          FROM      syResources RChild
                          INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
                          INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                          INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                          LEFT OUTER JOIN (
                                            SELECT  *
                                            FROM    syResources
                                            WHERE   ResourceTypeId = 1
                                          ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                          WHERE     RChild.ResourceTypeId IN ( 2,3,8 )
                                    AND (
                                          RChild.ChildTypeId IS NULL
                                          OR RChild.ChildTypeId = 3
                                        )
                                    AND ( NNParent.ParentId IN ( SELECT HierarchyId
                                                                 FROM   syNavigationNodes
                                                                 WHERE  ResourceId = 396
                                                                        AND ParentId IN ( SELECT    HierarchyId
                                                                                          FROM      syNavigationNodes
                                                                                          WHERE     ResourceId = 192 ) ) )
                          UNION
                          SELECT DISTINCT
                                    RChild.ResourceId AS ChildResourceId
                                   ,RChild.Resource AS ChildResource
                                   ,RChild.ResourceURL AS ChildResourceURL
                                   ,CASE WHEN ( RChild.ResourceId IN ( 737 ) ) THEN NULL
                                         ELSE RChild.ResourceId
                                    END AS ParentResourceId
                                   ,NULL AS ParentResource
                                   ,NULL AS MODULE
                                   ,RChild.ResourceTypeID
                          FROM      syResources RChild
                          WHERE     RChild.ResourceId IN ( 737 )
                        ) t1
            ) t2
    WHERE   --(@ModuleResourceId is null or t2.ModuleResourceId=@ModuleResourceId) AND 
--t2.TabId=@TabId and 
            ( AllowSchlReqFlds = 1 )
            AND ParentResourceId IS NOT NULL
    ORDER BY ModuleResourceId
           ,ChildResource
           ,TabId;



GO
