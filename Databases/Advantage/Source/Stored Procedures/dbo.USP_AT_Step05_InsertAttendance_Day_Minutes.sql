SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--=================================================================================================
-- USP_AT_Step05_InsertAttendance_Day_Minutes
--=================================================================================================
CREATE PROCEDURE [dbo].[USP_AT_Step05_InsertAttendance_Day_Minutes]
    (
        @StuEnrollIdList AS VARCHAR(MAX) = NULL
    )
AS --
    -- Step 5  --  InsertAttendance_Class_Minutes  -- UnitTypeDescrip = ('Minutes') 
    --         --  TrackSapAttendance = 'byday'
    BEGIN -- Step 5  --  InsertAttendance_Day_Minutes
        DECLARE @StuEnrollId UNIQUEIDENTIFIER
               ,@MeetDate DATETIME
               ,@WeekDay VARCHAR(15)
               ,@StartDate DATETIME
               ,@EndDate DATETIME
               ,@PeriodDescrip VARCHAR(50)
               ,@Actual DECIMAL(18, 2)
               ,@Excused DECIMAL(18, 2)
               ,@ClsSectionId UNIQUEIDENTIFIER
               ,@Absent DECIMAL(18, 2)
               ,@ScheduledMinutes DECIMAL(18, 2)
               ,@TardyMinutes DECIMAL(18, 2)
               ,@tardy DECIMAL(18, 2)
               ,@tracktardies INT
               ,@tardiesMakingAbsence INT
               ,@PrgVerId UNIQUEIDENTIFIER
               ,@rownumber INT
               ,@IsTardy BIT
               ,@ActualRunningScheduledHours DECIMAL(18, 2)
               ,@ActualRunningPresentHours DECIMAL(18, 2)
               ,@ActualRunningAbsentHours DECIMAL(18, 2)
               ,@ActualRunningTardyHours DECIMAL(18, 2)
               ,@ActualRunningMakeupHours DECIMAL(18, 2)
               ,@PrevStuEnrollId UNIQUEIDENTIFIER
               ,@intTardyBreakPoint INT
               ,@AdjustedRunningPresentHours DECIMAL(18, 2)
               ,@AdjustedRunningAbsentHours DECIMAL(18, 2)
               ,@ActualRunningScheduledDays DECIMAL(18, 2);



        DECLARE @attendanceSummary TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,ClsSectionId UNIQUEIDENTIFIER
               ,MeetDate DATETIME
               ,ActualHours DECIMAL(18, 2)
               ,ScheduledMinutes DECIMAL(18, 2)
               ,Absent DECIMAL(18, 2)
               ,isTardy BIT
               ,ActualRunningScheduledHours DECIMAL(18, 2)
               ,ActualRunningPresentHours DECIMAL(18, 2)
               ,ActualRunningAbsentHours DECIMAL(18, 2)
               ,ActualRunningMakeupHours DECIMAL(18, 2)
               ,ActualRunningTardyHours DECIMAL(18, 2)
               ,TrackTardies BIT
               ,TardiesMakingAbsence INT
               ,PrgVerId UNIQUEIDENTIFIER
               ,RowNumber INT
            );

        DECLARE @MyEnrollments TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
            );
        INSERT INTO @MyEnrollments
                    SELECT enrollments.StuEnrollId
                    FROM   dbo.arStuEnrollments enrollments
                    WHERE  (
                           @StuEnrollIdList IS NULL
                           OR ( enrollments.StuEnrollId IN (
                                                           SELECT zz.Val
                                                           FROM   MultipleValuesForReportParameters(@StuEnrollIdList, ',', 1) zz
                                                           )
                              )
                           );

        INSERT INTO @attendanceSummary (
                                       StuEnrollId
                                      ,ClsSectionId
                                      ,MeetDate
                                      ,ActualHours
                                      ,ScheduledMinutes
                                      ,Absent
                                      ,isTardy
                                      ,ActualRunningScheduledHours
                                      ,ActualRunningPresentHours
                                      ,ActualRunningAbsentHours
                                      ,ActualRunningMakeupHours
                                      ,ActualRunningTardyHours
                                      ,TrackTardies
                                      ,TardiesMakingAbsence
                                      ,PrgVerId
                                      ,RowNumber
                                       )
                    SELECT *
                    FROM   (
                           SELECT     t1.StuEnrollId
                                     ,NULL AS ClsSectionId
                                     ,t1.RecordDate AS MeetDate
                                     ,t1.ActualHours
                                     ,t1.SchedHours AS ScheduledMinutes
                                     ,CASE WHEN (
                                                (
                                                t1.SchedHours >= 1
                                                AND t1.SchedHours NOT IN ( 999, 9999 )
                                                )
                                                AND t1.ActualHours = 0
                                                ) THEN t1.SchedHours
                                           ELSE 0
                                      END AS Absent
                                     ,t1.isTardy
                                     ,(
                                      SELECT SUM(SchedHours)
                                      FROM   arStudentClockAttendance
                                      WHERE  StuEnrollId = t1.StuEnrollId
                                             AND RecordDate <= t1.RecordDate
                                             AND (
                                                 t1.SchedHours >= 1
                                                 AND t1.SchedHours NOT IN ( 999, 9999 )
                                                 AND t1.ActualHours NOT IN ( 999, 9999 )
                                                 )
                                      ) AS ActualRunningScheduledHours
                                     ,(
                                      SELECT SUM(ActualHours)
                                      FROM   arStudentClockAttendance
                                      WHERE  StuEnrollId = t1.StuEnrollId
                                             AND RecordDate <= t1.RecordDate
                                             AND (
                                                 t1.SchedHours >= 1
                                                 AND t1.SchedHours NOT IN ( 999, 9999 )
                                                 )
                                             AND ActualHours >= 1
                                             AND ActualHours NOT IN ( 999, 9999 )
                                      ) AS ActualRunningPresentHours
                                     ,(
                                      SELECT COUNT(ActualHours)
                                      FROM   arStudentClockAttendance
                                      WHERE  StuEnrollId = t1.StuEnrollId
                                             AND RecordDate <= t1.RecordDate
                                             AND (
                                                 t1.SchedHours >= 1
                                                 AND t1.SchedHours NOT IN ( 999, 9999 )
                                                 )
                                             AND ActualHours = 0
                                             AND ActualHours NOT IN ( 999, 9999 )
                                      ) AS ActualRunningAbsentHours
                                     ,(
                                      SELECT SUM(ActualHours)
                                      FROM   arStudentClockAttendance
                                      WHERE  StuEnrollId = t1.StuEnrollId
                                             AND RecordDate <= t1.RecordDate
                                             AND SchedHours = 0
                                             AND ActualHours >= 1
                                             AND ActualHours NOT IN ( 999, 9999 )
                                      ) AS ActualRunningMakeupHours
                                     ,(
                                      SELECT SUM(SchedHours - ActualHours)
                                      FROM   arStudentClockAttendance
                                      WHERE  StuEnrollId = t1.StuEnrollId
                                             AND RecordDate <= t1.RecordDate
                                             AND (
                                                 (
                                                 t1.SchedHours >= 1
                                                 AND t1.SchedHours NOT IN ( 999, 9999 )
                                                 )
                                                 AND ActualHours >= 1
                                                 AND ActualHours NOT IN ( 999, 9999 )
                                                 )
                                             AND isTardy = 1
                                      ) AS ActualRunningTardyHours
                                     ,t3.TrackTardies
                                     ,t3.TardiesMakingAbsence
                                     ,t3.PrgVerId
                                     ,ROW_NUMBER() OVER ( ORDER BY t1.RecordDate ) AS RowNumber
                           FROM       arStudentClockAttendance t1
                           INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                           INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                           INNER JOIN arAttUnitType AS AAUT ON AAUT.UnitTypeId = t3.UnitTypeId
                           INNER JOIN @MyEnrollments ON [@MyEnrollments].StuEnrollId = t1.StuEnrollId
                           WHERE --t3.UnitTypeId IN ( 'A1389C74-0BB9-4BBF-A47F-68428BE7FA4D' ) -- UnitTypeDescrip = ('Minutes') 
                                      AAUT.UnitTypeDescrip IN ( 'Minutes' )
                                      AND t1.ActualHours <> 9999.00
                           ) minAttendance;
        DELETE FROM dbo.syStudentAttendanceSummary
        WHERE StuEnrollId IN (
                             SELECT DISTINCT StuEnrollId
                             FROM   @attendanceSummary
                             );
        DECLARE GetAttendance_Cursor CURSOR FOR
            SELECT   *
            FROM     @attendanceSummary
            ORDER BY StuEnrollId
                    ,MeetDate;
        OPEN GetAttendance_Cursor;
        DECLARE @ActualHours DECIMAL(18, 2)
               ,@SchedHours DECIMAL(18, 2);
        FETCH NEXT FROM GetAttendance_Cursor
        INTO @StuEnrollId
            ,@ClsSectionId
            ,@MeetDate
            ,@Actual
            ,@ScheduledMinutes
            ,@Absent
            ,@IsTardy
            ,@ActualRunningScheduledHours
            ,@ActualRunningPresentHours
            ,@ActualRunningAbsentHours
            ,@ActualRunningMakeupHours
            ,@ActualRunningTardyHours
            ,@tracktardies
            ,@tardiesMakingAbsence
            ,@PrgVerId
            ,@rownumber;

        SET @ActualRunningPresentHours = 0;
        SET @ActualRunningPresentHours = 0;
        SET @ActualRunningAbsentHours = 0;
        SET @ActualRunningTardyHours = 0;
        SET @ActualRunningMakeupHours = 0;
        SET @intTardyBreakPoint = 0;
        SET @AdjustedRunningPresentHours = 0;
        SET @AdjustedRunningAbsentHours = 0;
        SET @ActualRunningScheduledDays = 0;
        WHILE @@FETCH_STATUS = 0
            BEGIN

                IF @PrevStuEnrollId <> @StuEnrollId
                    BEGIN
                        SET @ActualRunningPresentHours = 0;
                        SET @ActualRunningAbsentHours = 0;
                        SET @intTardyBreakPoint = 0;
                        SET @ActualRunningTardyHours = 0;
                        SET @AdjustedRunningPresentHours = 0;
                        SET @AdjustedRunningAbsentHours = 0;
                        SET @ActualRunningScheduledDays = 0;
                    END;

                IF (
                   @ScheduledMinutes >= 1
                   AND (
                       @Actual <> 9999
                       AND @Actual <> 999
                       )
                   AND (
                       @ScheduledMinutes <> 9999
                       AND @Actual <> 999
                       )
                   )
                    BEGIN
                        SET @ActualRunningScheduledDays = ISNULL(@ActualRunningScheduledDays, 0) + ISNULL(@ScheduledMinutes, 0);
                    END;
                ELSE
                    BEGIN
                        SET @ActualRunningScheduledDays = ISNULL(@ActualRunningScheduledDays, 0);
                    END;

                IF (
                   @Actual <> 9999
                   AND @Actual <> 999
                   )
                    BEGIN
                        SET @ActualRunningPresentHours = @ActualRunningPresentHours + @Actual;
                    END;
                SET @ActualRunningAbsentHours = @ActualRunningAbsentHours + @Absent;
                -- Absent hours
                --1. sched = 5, Actual = 2 then Ab = (5-3) = 2
                IF (
                   @Actual > 0
                   AND @Actual < @ScheduledMinutes
                   )
                    BEGIN
                        SET @ActualRunningAbsentHours = @ActualRunningAbsentHours + ( @ScheduledMinutes - @Actual );
                    END;
                -- Make up hours
                --sched=5, Actual =7, makeup= 2,ab = 0
                --sched=0, Actual =7, makeup= 7,ab = 0
                IF (
                   @Actual > 0
                   AND @ScheduledMinutes > 0
                   AND @Actual > @ScheduledMinutes
                   AND (
                       @Actual <> 9999
                       AND @Actual <> 999
                       )
                   )
                    BEGIN
                        SET @ActualRunningMakeupHours = @ActualRunningMakeupHours + ( @Actual - @ScheduledMinutes );
                    END;
                --		if (@Actual>0 and @ScheduledMinutes=0 and (@Actual <> 9999.00 and @Actual <> 999.00))
                --		begin
                --			set @ActualRunningMakeupHours = @ActualRunningMakeupHours + (@Actual-@ScheduledMinutes)
                --		end
                IF (
                   @Actual <> 9999
                   AND @Actual <> 999
                   )
                    BEGIN
                        SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours + @Actual;
                    END;
                IF (
                   @Actual = 0
                   AND @ScheduledMinutes >= 1
                   AND @ScheduledMinutes NOT IN ( 999, 9999 )
                   )
                    BEGIN
                        SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Absent;
                    END;
                -- Absent hours
                --1. sched = 5, Actual = 2 then Ab = (5-3) = 2
                IF (
                   @Absent = 0
                   AND @ActualRunningAbsentHours > 0
                   AND ( @Actual < @ScheduledMinutes )
                   )
                    BEGIN
                        SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + ( @ScheduledMinutes - @Actual );
                    END;
                IF (
                   @Actual > 0
                   AND @Actual < @ScheduledMinutes
                   AND (
                       @Actual <> 9999.00
                       AND @Actual <> 999.00
                       )
                   )
                    BEGIN
                        SET @ActualRunningTardyHours = @ActualRunningTardyHours + ( @ScheduledMinutes - @Actual );
                    END;

                IF @tracktardies = 1
                   AND (
                       @TardyMinutes > 0
                       OR @IsTardy = 1
                       )
                    BEGIN
                        SET @intTardyBreakPoint = @intTardyBreakPoint + 1;
                    END;
                IF (
                   @tracktardies = 1
                   AND @intTardyBreakPoint = @tardiesMakingAbsence
                   )
                    BEGIN
                        SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - @Actual;
                        SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @ScheduledMinutes; --@TardyMinutes
                        SET @intTardyBreakPoint = 0;
                    END;

                INSERT INTO syStudentAttendanceSummary (
                                                       StuEnrollId
                                                      ,ClsSectionId
                                                      ,StudentAttendedDate
                                                      ,ScheduledDays
                                                      ,ActualDays
                                                      ,ActualRunningScheduledDays
                                                      ,ActualRunningPresentDays
                                                      ,ActualRunningAbsentDays
                                                      ,ActualRunningMakeupDays
                                                      ,ActualRunningTardyDays
                                                      ,AdjustedPresentDays
                                                      ,AdjustedAbsentDays
                                                      ,AttendanceTrackType
                                                      ,ModUser
                                                      ,ModDate
                                                       )
                VALUES ( @StuEnrollId, @ClsSectionId, @MeetDate, @ScheduledMinutes, @Actual, @ActualRunningScheduledDays, @ActualRunningPresentHours
                        ,@ActualRunningAbsentHours, @ActualRunningMakeupHours, @ActualRunningTardyHours, @AdjustedRunningPresentHours
                        ,@AdjustedRunningAbsentHours, 'Post Attendance by Class', 'sa', GETDATE());

                UPDATE syStudentAttendanceSummary
                SET    tardiesmakingabsence = @tardiesMakingAbsence
                WHERE  StuEnrollId = @StuEnrollId;
                --end
                SET @PrevStuEnrollId = @StuEnrollId;

                FETCH NEXT FROM GetAttendance_Cursor
                INTO @StuEnrollId
                    ,@ClsSectionId
                    ,@MeetDate
                    ,@Actual
                    ,@ScheduledMinutes
                    ,@Absent
                    ,@IsTardy
                    ,@ActualRunningScheduledHours
                    ,@ActualRunningPresentHours
                    ,@ActualRunningAbsentHours
                    ,@ActualRunningMakeupHours
                    ,@ActualRunningTardyHours
                    ,@tracktardies
                    ,@tardiesMakingAbsence
                    ,@PrgVerId
                    ,@rownumber;
            END;
        CLOSE GetAttendance_Cursor;
        DEALLOCATE GetAttendance_Cursor;

    END;
--=================================================================================================
-- END  --  USP_AT_Step05_InsertAttendance_Day_Minutes
--=================================================================================================
GO
