SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_SystemComponentTypes_Update]
    @GrdComponentTypeId UNIQUEIDENTIFIER
   ,@SystemComponentTypeId INT
AS
    UPDATE  arGrdComponentTypes
    SET     SysComponentTypeId = @SystemComponentTypeId
    WHERE   GrdComponentTypeId = @GrdComponentTypeId;



GO
