SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[USP_PeriodsForEachClass_GetList]
    @TermId UNIQUEIDENTIFIER
   ,@ReqId UNIQUEIDENTIFIER
AS
    BEGIN
        SELECT DISTINCT
                t5.PeriodDescrip
        FROM    arClassSections CS
        INNER JOIN arClsSectMeetings m ON CS.ClsSectionId = m.ClsSectionId
        INNER JOIN syPeriods t5 ON m.PeriodId = t5.PeriodId
        INNER JOIN syPeriodsWorkDays t6 ON t5.PeriodId = t6.PeriodId
        INNER JOIN plWorkDays t7 ON t6.WorkDayId = t7.WorkDaysId
        WHERE   CS.TermId = @TermId    --'D3E2BD0D-F053-4B97-97AF-A7BE3BAAD77D' 
                AND CS.ReqId = @ReqId;      --'36FB7399-ECEC-4BD8-8E6A-C479A0D6042C'
    END;



GO
