SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[RS_StateBoard_Report]
	-- Add the parameters for the stored procedure here
    @CampusId AS VARCHAR(38) = '253EAF31-86B0-4C91-8178-3BAA3EE1327D'
   ,@ReportDate AS DATETIME = '7/02/2014'
   ,@PrgVerId AS VARCHAR(4000) = NULL --'1B36473D-AE8C-4DCC-B04C-0585BFBC64AC'
AS
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;

	-- This conversion is for make happy the SSRS
        DECLARE @CampusGuid UNIQUEIDENTIFIER = CAST(@CampusId AS UNIQUEIDENTIFIER);
	
        DECLARE @tmpConsolidatedAttendance TABLE
            (
             StuEnrollId UNIQUEIDENTIFIER
            ,AttendanceThisMonth DECIMAL(18,2)
            ,AttendanceLastMonth DECIMAL(18,2)
            ,AttendanceTotal DECIMAL(18,2)
            );

        DECLARE @ReportMonthLastDay DATETIME; 
        DECLARE @BeforeMonthLastDay DATETIME;
 --   SET @ReportMonthLastDay = DATEADD(DAY,-1, CAST(CAST(YEAR(@ReportDate) AS VARCHAR(4)) + '-' 
 --                           + CAST(MONTH (DATEADD(MONTH,1,@ReportDate)) AS VARCHAR(5)) + '-' 
 --                           + CAST(1 AS varchar) + ' ' + CAST('23:59:59' AS VARCHAR(8)) AS DATETIME));
 --   SET @BeforeMonthLastDay = DATEADD(MONTH, -1, @ReportMonthLastDay);
        SET @ReportMonthLastDay = DATEADD(SECOND,-1,DATEADD(MONTH,DATEDIFF(MONTH,0,@ReportDate) + 1,0));
        SET @BeforeMonthLastDay = DATEADD(SECOND,-1,DATEADD(MONTH,DATEDIFF(MONTH,0,DATEADD(MONTH,-1,@ReportMonthLastDay)) + 1,0));
	

        INSERT  INTO @tmpConsolidatedAttendance
                SELECT DISTINCT
                        StuEnrollId AS StuEnrollId
                       ,(
                          SELECT    ISNULL(SUM(ISNULL(Actual,0)),0)
                          FROM      atClsSectAttendance t2
                          WHERE     Actual <> 9999
                                    AND YEAR(MeetDate) = YEAR(@ReportMonthLastDay)
                                    AND MONTH(MeetDate) = MONTH(@ReportMonthLastDay)
                                    AND t2.StuEnrollId = t1.StuEnrollId
                        ) AS AttendanceThisMonth
                       ,(
                          SELECT    ISNULL(SUM(ISNULL(Actual,0)),0)
                          FROM      atClsSectAttendance t2
                          WHERE     Actual <> 9999 
											 --AND YEAR(MeetDate)=YEAR(@MonthBefore) 
											 --AND MONTH(MeetDate)=MONTH(@MonthBefore)
                                    AND MeetDate <= @BeforeMonthLastDay
                                    AND t2.StuEnrollId = t1.StuEnrollId
                        ) AS AttendanceLastMonth
                       ,(
                          SELECT    ISNULL(SUM(ISNULL(Actual,0)),0)
                          FROM      atClsSectAttendance t2
                          WHERE     Actual <> 9999
													--AND MeetDate <= DATEADD(DAY,-1, DATEADD(MONTH,1,@ReportDate))
                                    AND MeetDate <= @ReportMonthLastDay
                                    AND t2.StuEnrollId = t1.StuEnrollId
                        ) AS AttendanceTotal
                FROM    atClsSectAttendance t1
	--WHERE t1.StuEnrollId = '4D5D3104-8711-4DF6-A509-58A17E4A6FAA'
                ORDER BY StuEnrollId;

	--SELECT * FROM @tmpConsolidatedAttendance

	-- Second part
        SELECT  stu.LastName AS Last
               ,( ISNULL(stu.FirstName,'') + ' ' + ISNULL(stu.MiddleName,'') ) AS First
               ,( RIGHT(ISNULL(stu.SSN,''),4) ) AS [Last 4 of SS#]
               ,en.EnrollDate AS EnrollDate
               ,CASE WHEN AAT.IPEDSValue = 61 THEN 'Full Time'
                     WHEN AAT.IPEDSValue = 62 THEN 'Part Time'
                     ELSE 'Full Time'
                END AS TimeType
               ,( tmp.AttendanceThisMonth / 60 ) AS AttendanceThisMonth
               ,( tmp.AttendanceLastMonth / 60 ) AS AttendancePastMonth
               ,( tmp.AttendanceTotal / 60 ) AS AttendanceTotal
               ,prg.ProgDescrip AS CourseStudiedDescription
               ,prg.ProgCode AS [Course of Study]
        FROM    @tmpConsolidatedAttendance AS tmp
        INNER JOIN dbo.arStuEnrollments AS en ON en.StuEnrollId = tmp.StuEnrollId
        INNER JOIN dbo.arStudent AS stu ON stu.StudentId = en.StudentId
        INNER JOIN arPrgVersions AS prgve ON prgve.PrgVerId = en.PrgVerId
        INNER JOIN dbo.arPrograms AS prg ON prg.ProgId = prgve.ProgId
        INNER JOIN arAttendTypes AS AAT ON AAT.AttendTypeId = en.attendtypeid
        INNER JOIN dbo.arAttUnitType AS AAUT ON AAUT.UnitTypeId = prgve.UnitTypeId
        WHERE   en.CampusId = @CampusId
                AND tmp.AttendanceThisMonth > 0
                AND (
                      @PrgVerId IS NULL
                      OR LTRIM(RTRIM(en.PrgVerId)) IN ( SELECT  Val
                                                        FROM    MultipleValuesForReportParameters(@PrgVerId,',',1) )
                    )
                AND AAUT.UnitTypeDescrip = 'Clock Hours'
        ORDER BY Last
               ,First
               ,[Course of Study];  
    END;





GO
