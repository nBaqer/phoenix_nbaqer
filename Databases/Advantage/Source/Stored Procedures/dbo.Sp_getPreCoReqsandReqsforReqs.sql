SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[Sp_getPreCoReqsandReqsforReqs]
    (
     @CampusId UNIQUEIDENTIFIER
    ,@ReqId UNIQUEIDENTIFIER
    ,@PrgVerId UNIQUEIDENTIFIER
    )
AS
    DECLARE @preCoreqid1 UNIQUEIDENTIFIER;

--get the children for the given Reqid
--create a variable table type and insert the selected courses into that table
    DECLARE @VariableReqtable TABLE
        (
         ReqId UNIQUEIDENTIFIER
        ,preCoReqId UNIQUEIDENTIFIER
        ,ID INT IDENTITY
        ); 
    INSERT  INTO @VariableReqtable
            SELECT  ReqId
                   ,preCoReqId
            FROM    arCourseReqs
            WHERE   ReqId = @ReqId
                    AND (
                          PrgVerId = @PrgVerId
                          OR PrgVerId IS NULL
                        );
    DECLARE @MaxRowCounter INT
       ,@RowCounter INT;
    SET @MaxRowCounter = (
                           SELECT   MAX(ID)
                           FROM     @VariableReqtable
                         );
    SET @RowCounter = 1;
    WHILE ( @RowCounter <= @MaxRowCounter )
        BEGIN
            INSERT  INTO @VariableReqtable
                    SELECT  ReqId
                           ,preCoReqId
                    FROM    arCourseReqs
                    WHERE   ReqId IN ( SELECT   preCoReqId
                                       FROM     @VariableReqtable
                                       WHERE    ID = @RowCounter )
                            AND (
                                  PrgVerId = @PrgVerId
                                  OR PrgVerId IS NULL
                                ); 
            SET @RowCounter = @RowCounter + 1;
            SET @MaxRowCounter = (
                                   SELECT   MAX(ID)
                                   FROM     @VariableReqtable
                                 );
        END;
------get the parents for the given ReqId
--create a variable table type and insert the selected courses into that table
    DECLARE @VariableReqtable1 TABLE
        (
         ReqId UNIQUEIDENTIFIER
        ,preCoReqId UNIQUEIDENTIFIER
        ,ID INT IDENTITY
        ); 
    INSERT  INTO @VariableReqtable1
            SELECT  ReqId
                   ,preCoReqId
            FROM    arCourseReqs
            WHERE   preCoReqId = @ReqId
                    AND (
                          PrgVerId = @PrgVerId
                          OR PrgVerId IS NULL
                        );
    SET @MaxRowCounter = (
                           SELECT   MAX(ID)
                           FROM     @VariableReqtable1
                         );
    SET @RowCounter = 1;
    WHILE ( @RowCounter <= @MaxRowCounter )
        BEGIN
            INSERT  INTO @VariableReqtable1
                    SELECT  ReqId
                           ,preCoReqId
                    FROM    arCourseReqs
                    WHERE   preCoReqId IN ( SELECT  ReqId
                                            FROM    @VariableReqtable1
                                            WHERE   ID = @RowCounter )
                            AND (
                                  PrgVerId = @PrgVerId
                                  OR PrgVerId IS NULL
                                ); 
            SET @RowCounter = @RowCounter + 1;
            SET @MaxRowCounter = (
                                   SELECT   MAX(ID)
                                   FROM     @VariableReqtable1
                                 );
        END;


--Get all the children and parent Courses for the Course given
    IF @PrgVerId = (
                     SELECT CAST(CAST(0 AS BINARY) AS UNIQUEIDENTIFIER)
                   )
        BEGIN
            SELECT  *
            FROM    arReqs
            WHERE   ReqId IN ( SELECT   ReqId
                               FROM     @VariableReqtable
                               UNION
                               SELECT   preCoReqId
                               FROM     @VariableReqtable
                               UNION
                               SELECT   ReqId
                               FROM     @VariableReqtable1
                               UNION
                               SELECT   preCoReqId
                               FROM     @VariableReqtable1 )
                    AND arReqs.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
                    AND arReqs.ReqTypeId = 1
                    AND arReqs.ReqId <> @ReqId
                    AND arReqs.CampGrpId IN ( SELECT    CampGrpId
                                              FROM      syCmpGrpCmps
                                              WHERE     CampusId = @CampusId ); 
        END;
    ELSE
        BEGIN
            SELECT  arReqs.*
            FROM    arReqs
                   ,arProgVerDef
            WHERE   arReqs.ReqId IN ( SELECT    ReqId
                                      FROM      @VariableReqtable
                                      UNION
                                      SELECT    preCoReqId
                                      FROM      @VariableReqtable
                                      UNION
                                      SELECT    ReqId
                                      FROM      @VariableReqtable1
                                      UNION
                                      SELECT    preCoReqId
                                      FROM      @VariableReqtable1 )
                    AND arReqs.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
                    AND arReqs.ReqTypeId = 1
                    AND arReqs.ReqId <> @ReqId
                    AND arReqs.ReqId = arProgVerDef.ReqId
                    AND arProgVerDef.PrgVerId = @PrgVerId
                    AND arReqs.CampGrpId IN ( SELECT    CampGrpId
                                              FROM      syCmpGrpCmps
                                              WHERE     CampusId = @CampusId )
            UNION
            SELECT  t5.*
            FROM    arReqs t1
                   ,arProgVerDef t2
                   ,arReqGrpDef t4
                   ,arReqs t5
            WHERE   t4.ReqId IN ( SELECT    ReqId
                                  FROM      @VariableReqtable
                                  UNION
                                  SELECT    preCoReqId
                                  FROM      @VariableReqtable
                                  UNION
                                  SELECT    ReqId
                                  FROM      @VariableReqtable1
                                  UNION
                                  SELECT    preCoReqId
                                  FROM      @VariableReqtable1 )
                    AND t4.GrpId = t2.ReqId
                    AND t2.PrgVerId = @PrgVerId
                    AND t2.ReqId = t1.ReqId
                    AND t5.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
                    AND t5.ReqTypeId = 1
                    AND t5.ReqId <> @ReqId
                    AND t4.ReqId = t5.ReqId
                    AND t5.CampGrpId IN ( SELECT    CampGrpId
                                          FROM      syCmpGrpCmps
                                          WHERE     CampusId = @CampusId ); 
        END;


GO
