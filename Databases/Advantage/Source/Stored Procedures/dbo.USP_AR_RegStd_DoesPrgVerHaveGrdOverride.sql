SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[USP_AR_RegStd_DoesPrgVerHaveGrdOverride]
    (
     @PrgVerID UNIQUEIDENTIFIER
    ,@ReqID UNIQUEIDENTIFIER
    )
AS /*----------------------------------------------------------------------------------------------------
	Author          :	Saraswathi Lakshmanan
    
    Create date		:	02/04/2010
    
	Procedure Name	:	USP_AR_RegStd_DoesPrgVerHaveGrdOverride

	Objective		:	Finds if the req has a grade override
	
	Parameters		:	Name			Type	Data Type			Required? 	
						=====			====	=========			=========	
						@PrgVerID		In		UniqueIdentifier
						@ReqID			In		Uniqueidentifier
	
	Output			:	Returns the count	
						
*/-----------------------------------------------------------------------------------------------------
/*
Procedure created by Saraswathi Lakshmanan on Jan 29 2010
--To fix issue 17548: ENH: All: Register Students Page is Taking Too Long to Populate Available Students 
*/

    BEGIN
        SELECT  COUNT(*) AS Count
               ,GrdSysDetailId
        FROM    arProgVerDef
        WHERE   ReqId = @ReqID
                AND PrgVerId = @PrgVerID
                AND GrdSysDetailId IS NOT NULL
        GROUP BY GrdSysDetailId; 
    END;




GO
