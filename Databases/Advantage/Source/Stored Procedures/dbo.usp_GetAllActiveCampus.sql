SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_GetAllActiveCampus] @campusId VARCHAR(50)
AS
    SET NOCOUNT ON;
    SELECT DISTINCT
            T.CampusId
           ,T.CampDescrip
           ,1 AS Status
    FROM    syCampuses T
           ,syStatuses ST
    WHERE   T.StatusId = ST.StatusId
            AND ST.Status = 'Active'
            AND T.CampusId = @campusId
    ORDER BY Status
           ,T.CampDescrip;



GO
