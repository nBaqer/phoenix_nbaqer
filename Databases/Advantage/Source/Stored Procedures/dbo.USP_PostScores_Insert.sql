SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_PostScores_Insert]
    @EffectiveDate DATETIME
   ,@RuleValues NTEXT
AS
    DECLARE @hDoc INT;
    --Prepare input values as an XML document
    EXEC sp_xml_preparedocument @hDoc OUTPUT,@RuleValues;

    -- Insert records into Bridge Table 
    INSERT  INTO arPostScoreDictationSpeedTest
            SELECT DISTINCT
                    id
                   ,stuenrollid
                   ,termid
                   ,@EffectiveDate
                   ,grdcomponenttypeid
                   ,accuracy
                   ,speed
                   ,GrdSysDetailId
                   ,InstructorId
                   ,ModUser
                   ,GETDATE()
                   ,istestmentorproctored
            FROM    OPENXML(@hDoc,'/NewDataSet/PostScores',1) 
                WITH (
                     id UNIQUEIDENTIFIER,stuenrollid UNIQUEIDENTIFIER,termid UNIQUEIDENTIFIER,
                     datepassed DATETIME,grdcomponenttypeid UNIQUEIDENTIFIER,
                     accuracy DECIMAL,speed DECIMAL,GrdSysDetailId UNIQUEIDENTIFIER,
                     InstructorId UNIQUEIDENTIFIER,ModUser VARCHAR(50),ModDate DATETIME,istestmentorproctored VARCHAR(10));
    
    EXEC sp_xml_removedocument @hDoc;



GO
