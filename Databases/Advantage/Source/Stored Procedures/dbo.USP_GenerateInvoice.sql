SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Edwin Sosa
-- Create date: 11/19/2018
-- Description:	A Stored procedure for generating the invoice for an enrollment.
-- =============================================
CREATE PROCEDURE [dbo].[USP_GenerateInvoice]
    -- Add the parameters for the stored procedure here
    @campusId UNIQUEIDENTIFIER = NULL
   ,@stuEnrollIdList VARCHAR(MAX) = NULL
   ,@refDate DATETIME = GETDATE
AS
    BEGIN
        -- SET NOCOUNT ON added to prevent extra result sets from
        -- interfering with SELECT statements.
        SET NOCOUNT ON;



        DECLARE @InvoicePayments TABLE
            (
                Firstname VARCHAR(100)
               ,MiddleName VARCHAR(75)
               ,LastName VARCHAR(100)
               ,StudentNumber NVARCHAR(50)
               ,Amount DECIMAL(18, 2)
               ,Description VARCHAR(50)
               ,DueDate DATETIME
               ,Item INT
            );

        DECLARE @PreAdjustedPayments TABLE
            (
                PrevDescription VARCHAR(100)
               ,PostDescription VARCHAR(100)
               ,Firstname VARCHAR(100)
               ,MiddleName VARCHAR(75)
               ,LastName VARCHAR(100)
               ,StudentNumber NVARCHAR(50)
               ,Amount DECIMAL(18, 2)
               ,Description VARCHAR(50)
               ,DueDate DATETIME
               ,Item INT
               ,TotalCredit DECIMAL(18, 2)
               ,GroupNo INT
            );

        INSERT INTO @InvoicePayments (
                                     Firstname
                                    ,MiddleName
                                    ,LastName
                                    ,StudentNumber
                                    ,Amount
                                    ,Description
                                    ,DueDate
                                    ,Item
                                     )
                    (SELECT     AST.FirstName
                               ,AST.MiddleName
                               ,AST.LastName
                               ,AST.StudentNumber
                               ,( ISNULL(FSPPS.Amount, 0) - ISNULL(SPDR2.AmountPaid, 0)) AS Amount
                               ,CASE WHEN ( ISNULL(FSPPS.Amount, 0) - ISNULL(SPDR2.AmountPaid, 0) < 0 ) THEN 'Payment Credit'
                                     ELSE 'Payment Due'
                                END AS Description
                               ,FSPPS.ExpectedDate AS DueDate
                               ,ROW_NUMBER() OVER ( PARTITION BY FSPPS.PaymentPlanId
                                                    ORDER BY FSPPS.ExpectedDate
                                                  ) AS Item
                     FROM       faStudentPaymentPlans AS FSPP
                     INNER JOIN faStuPaymentPlanSchedule AS FSPPS ON FSPPS.PaymentPlanId = FSPP.PaymentPlanId
                     INNER JOIN arStuEnrollments AS ASE ON ASE.StuEnrollId = FSPP.StuEnrollId
                     INNER JOIN arStudent AS AST ON AST.StudentId = ASE.StudentId
                     INNER JOIN syStatusCodes AS SSC ON SSC.StatusCodeId = ASE.StatusCodeId
                     INNER JOIN arPrgVersions AS APV ON APV.PrgVerId = ASE.PrgVerId
                     LEFT JOIN  (
                                SELECT CASE WHEN amountsPaid.AmountPaid < 0 THEN amountsPaid.AmountPaid * -1
                                            ELSE amountsPaid.AmountPaid
                                       END AS AmountPaid
                                      ,amountsPaid.PayPlanScheduleId
                                FROM   (
                                       SELECT     SUM(ISNULL(SPDR.Amount, 0)) AS AmountPaid
                                                 ,SPDR.PayPlanScheduleId
                                       FROM       saPmtDisbRel AS SPDR
                                       INNER JOIN saTransactions AS ST ON ST.TransactionId = SPDR.TransactionId
                                       INNER JOIN faStuPaymentPlanSchedule AS FSPPS2 ON FSPPS2.PayPlanScheduleId = SPDR.PayPlanScheduleId
                                       WHERE      ST.Voided = 0
                                       GROUP BY   SPDR.PayPlanScheduleId
                                       ) AS amountsPaid
                                ) AS SPDR2 ON SPDR2.PayPlanScheduleId = FSPPS.PayPlanScheduleId
                     LEFT JOIN  (
                                SELECT T.StudentId
                                      ,T.StdAddressId
                                      ,T.Address
                                      ,T.City
                                      ,T.Zip
                                      ,T.StateDescrip
                                FROM   (
                                       SELECT     ASA.StudentId
                                                 ,ASA.StdAddressId
                                                 ,( Address1 + ' ' + ISNULL(Address2, '')) AS Address
                                                 ,ASA.City
                                                 ,ASA.Zip
                                                 ,ISNULL(SS2.StateDescrip, '') AS StateDescrip
                                                 ,ROW_NUMBER() OVER ( PARTITION BY ASA.StudentId
                                                                      ORDER BY ASA.default1 DESC
                                                                    ) AS RowNumber
                                       FROM       arStudAddresses AS ASA
                                       INNER JOIN syStatuses AS SS ON SS.StatusId = ASA.StatusId
                                       LEFT JOIN  syStates AS SS2 ON SS2.StateId = ASA.StateId
                                       WHERE      SS.StatusCode = 'A'
                                       ) AS T
                                WHERE  T.RowNumber = 1
                                ) AS ASA2 ON ASA2.StudentId = AST.StudentId
                     WHERE      FSPPS.ExpectedDate <= @refDate -- RefDate  
                                AND ASE.CampusId = @campusId -- CampusId 
                                AND (
                                    @stuEnrollIdList IS NULL
                                    OR ( ASE.StuEnrollId IN (
                                                            SELECT Val
                                                            FROM   MultipleValuesForReportParameters(@stuEnrollIdList, ',', 1)
                                                            )
                                       )
                                    )
                                AND ( ISNULL(FSPPS.Amount, 0) - ISNULL(SPDR2.AmountPaid, 0)) <> 0);



        WITH cte
        AS ( SELECT tmp.PrevDescript
                   ,tmp.PostDescript
                   ,CASE WHEN ( tmp.PrevDescript ) = Description THEN 0
                         ELSE 1
                    END AS IsChange
                   ,tmp.Firstname
                   ,tmp.MiddleName
                   ,tmp.LastName
                   ,tmp.StudentNumber
                   ,tmp.Amount
                   ,tmp.Description
                   ,tmp.DueDate
                   ,tmp.Item
             FROM   (
                    SELECT      ilag.Description AS PrevDescript
                               ,ilead.Description AS PostDescript
                               ,i.Firstname
                               ,i.MiddleName
                               ,i.LastName
                               ,i.StudentNumber
                               ,i.Amount
                               ,i.Description
                               ,i.DueDate
                               ,i.Item
                    FROM        @InvoicePayments i
                    OUTER APPLY (
                                SELECT   TOP 1 i2.Description
                                FROM     @InvoicePayments i2
                                WHERE    i2.Item < i.Item
                                ORDER BY i2.Item DESC
                                ) ilag
                    OUTER APPLY (
                                SELECT   TOP 1 i2.Description
                                FROM     @InvoicePayments i2
                                WHERE    i2.Item > i.Item
                                ORDER BY i2.Item ASC
                                ) ilead
                    ) tmp )
            ,PaymentGroupings
        AS ( SELECT t.*
                   ,(
                    SELECT SUM(IsChange)
                    FROM   cte
                    WHERE  Item <= t.Item
                    ) GroupNo
             FROM   cte t )
            ,CreditAmounts
        AS ( SELECT   PaymentGroupings.GroupNo
                     ,ISNULL(SUM(Amount), 0) TotalCredit
             FROM     PaymentGroupings
             GROUP BY PaymentGroupings.GroupNo )
        INSERT INTO @PreAdjustedPayments (
                                         PrevDescription
                                        ,PostDescription
                                        ,Firstname
                                        ,MiddleName
                                        ,LastName
                                        ,StudentNumber
                                        ,Amount
                                        ,Description
                                        ,DueDate
                                        ,Item
                                        ,TotalCredit
                                        ,GroupNo
                                         )
                    (SELECT PaymentGroupings.PrevDescript
                           ,PaymentGroupings.PostDescript
                           ,Firstname
                           ,MiddleName
                           ,LastName
                           ,StudentNumber
                           ,PaymentGroupings.Amount
                           ,PaymentGroupings.Description
                           ,PaymentGroupings.DueDate
                           ,PaymentGroupings.Item
                           ,CASE WHEN PaymentGroupings.Description = 'Payment Due' THEN 0 ELSE CreditAmounts.TotalCredit END  AS TotalCredit
                           ,PaymentGroupings.GroupNo
                     FROM   PaymentGroupings
                     JOIN   CreditAmounts ON CreditAmounts.GroupNo = PaymentGroupings.GroupNo);


        SELECT Firstname
              ,MiddleName
              ,LastName
              ,StudentNumber
              ,CASE WHEN Description = 'Payment Due'
                         AND AdjustedInvoice.PrevDescription = 'Payment Credit' THEN AdjustedInvoice.AdjustedAmount
                    ELSE AdjustedInvoice.Amount
               END AS Amount
              ,Description
              ,DueDate
              ,Item
        FROM   (
               SELECT *
                     ,CASE WHEN (
                                Description = 'Payment Due'
                                AND PreAdjustment.PrevDescription = 'Payment Credit'
                                ) THEN ( Amount + PreAdjustment.LagCredit)
                           ELSE 0
                      END AS AdjustedAmount
               FROM   (
                      SELECT      *
                      FROM        @PreAdjustedPayments p
                      OUTER APPLY (
                                  SELECT   TOP 1 pap.TotalCredit AS LagCredit
                                  FROM     @PreAdjustedPayments pap
                                  WHERE    pap.Item < p.Item
                                  ORDER BY pap.Item DESC
                                  ) lagCredit
                      ) AS PreAdjustment
               ) AS AdjustedInvoice;

    END;
GO
