SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
   
CREATE PROCEDURE [dbo].[USP_GetStudentGridTestRequirementDetailsByEffectiveDates]
    (
     @StudentId UNIQUEIDENTIFIER
    ,@CampusID UNIQUEIDENTIFIER    
    )
AS /*----------------------------------------------------------------------------------------------------    
 Author          : Saraswathi Lakshmanan    
        
    Create date  : 28/09/2010    
        
 Procedure Name : USP_GetStudentGridTestRequirementDetailsByEffectiveDates    
    
 Objective  : Get test document requirements    
      
 Parameters  : Name   Type Data Type   Required?      
      =====   ==== =========   =========     
      @StuEnrollId IN  Uniqueuidentifier Yes    
         
     
 Output   : Returns the requested details     
          
*/-----------------------------------------------------------------------------------------------------    
     
    BEGIN    
     
     
 --   DECLARE @StuEnrollId AS UNIQUEIDENTIFIER    
 ----Declare @CampusID as UniqueIdentifier    
     
 --   SET @StuEnrollId = '9DBE5406-AD32-4799-9FCA-000C1AC33754'    
 --Set @CampusID='2CD3849A-DECD-48C5-8C9E-3FBE11351437'    
        DECLARE @StudentStartDate AS DATETIME;    
        SET @StudentStartDate = (
                                  SELECT    MIN(StartDate)
                                  FROM      arStuEnrollments
                                  WHERE     StudentID = @StudentId
                                );    
       
                               
        DECLARE @ActiveStatusID AS UNIQUEIDENTIFIER;    
        SET @ActiveStatusID = (
                                SELECT  StatusID
                                FROM    syStatuses
                                WHERE   Status = 'Active'
                              );     
     
    
    
    
        (
          SELECT  DISTINCT
                    adReqId
                   ,Descrip
                   ,ReqGrpId
                   ,StartDate
                   ,EndDate
                   ,ActualScore
                   ,TestTaken
                   ,Comments
                   ,CASE WHEN override >= 1 THEN 'True'
                         ELSE 'False'
                    END AS OverRide
                   ,CASE WHEN (
                                SELECT  COUNT(*)
                                FROM    adPrgVerTestDetails
                                WHERE   adReqId = R2.adReqId
                                        AND PrgVerId IN ( SELECT    PrgVerId
                                                          FROM      arStuEnrollments
                                                          WHERE     StudentId = @StudentId )
                              ) >= 1 THEN (
                                            SELECT  DISTINCT
                                                    MinScore
                                            FROM    adPrgVerTestDetails
                                            WHERE   adReqId = R2.adReqId
                                                    AND PrgVerId IN ( SELECT    PrgVerId
                                                                      FROM      arStuEnrollments
                                                                      WHERE     StudentId = @StudentId )
                                          )
                         ELSE MinScore
                    END AS MinScore
                   ,Required
                   ,reqforEnrollment
                   ,reqforFinancialAid
                   ,reqforGraduation
                   ,ModuleName
                   ,CASE WHEN (
                                TestTaken IS NOT NULL
                                AND ActualScore >= 1
                              ) THEN 1
                         ELSE 0
                    END AS TestTakenCount
                   ,Pass
          FROM      (
                      SELECT      
     DISTINCT                   adReqId
                               ,Descrip
                               ,ReqGrpId
                               ,StartDate
                               ,EndDate
                               ,ActualScore
                               ,TestTaken
                               ,Comments
                               ,override
                               ,Minscore
                               ,Required
                               ,reqforEnrollment
                               ,reqforFinancialAid
                               ,reqforGraduation
                               ,ModuleName
                               ,CASE WHEN ActualScore >= Minscore THEN 'True'
                                     ELSE 'False'
                                END AS Pass
                               ,CampGrpId
                      FROM      (
                                  SELECT DISTINCT
                                            t1.adReqId
                                           ,t1.Descrip
                                           ,CASE WHEN (
                                                        SELECT  COUNT(*)
                                                        FROM    adReqGroups
                                                        WHERE   IsMandatoryReqGrp = 1
                                                      ) >= 1 THEN (
                                                                    SELECT  ReqGrpId
                                                                    FROM    adReqGroups
                                                                    WHERE   IsMandatoryReqGrp = 1
                                                                  )
                                                 ELSE '00000000-0000-0000-0000-000000000000'
                                            END AS ReqGrpId
                                           ,@StudentStartDate AS CurrentDate
                                           ,t2.StartDate
                                           ,t2.EndDate
                                           ,(
                                              SELECT    MAX(ActualScore)
                                              FROM      adLeadEntranceTest
                                              WHERE     EntrTestId = t1.adReqId
                                                        AND StudentId = @StudentId
                                            ) AS ActualScore
                                           ,(
                                              SELECT TOP 1
                                                        TestTaken
                                              FROM      adLeadEntranceTest
                                              WHERE     EntrTestId = t1.adReqId
                                                        AND StudentId = @StudentId
                                            ) AS TestTaken
                                           ,(
                                              SELECT TOP 1
                                                        Comments
                                              FROM      adLeadEntranceTest
                                              WHERE     EntrTestId = t1.adReqId
                                                        AND StudentId = @StudentId
                                            ) AS Comments
                                           ,(
                                              SELECT TOP 1
                                                        override
                                              FROM      adEntrTestOverRide
                                              WHERE     EntrTestId = t1.adReqId
                                                        AND StudentId = @StudentId
                                            ) AS override
                                           ,t2.Minscore
                                           ,1 AS Required
                                           ,reqforEnrollment
                                           ,reqforFinancialAid
                                           ,reqforGraduation
                                           ,(
                                              SELECT    ModuleName
                                              FROM      dbo.syModules A
                                              WHERE     A.ModuleID = t1.moduleID
                                            ) AS ModuleName
                                           ,t1.CampGrpId
                                  FROM      adReqs t1
                                           ,adReqsEffectiveDates t2
                                  WHERE     t1.adReqId = t2.adReqId
                                            AND t2.MandatoryRequirement = 1
                                            AND t1.adReqTypeId IN ( 1 )
                                            AND t1.StatusId = @ActiveStatusID
                                ) R1
                      WHERE     R1.CurrentDate >= R1.StartDate
                                AND (
                                      R1.CurrentDate <= R1.EndDate
                                      OR R1.EndDate IS NULL
                                    )
                      UNION
                      SELECT      
         DISTINCT               adReqId
                               ,Descrip
                               ,reqGrpId
                               ,StartDate
                               ,EndDate
                               ,ActualScore
                               ,TestTaken
                               ,Comments
                               ,override
                               ,MinScore
                               ,Required
                               ,reqforEnrollment
                               ,reqforFinancialAid
                               ,reqforGraduation
                               ,ModuleName
                               ,CASE WHEN ActualScore >= MinScore THEN 'True'
                                     ELSE 'False'
                                END AS Pass
                               ,CampGrpId
                      FROM      (
                                  SELECT DISTINCT
                                            t1.adReqId
                                           ,t1.Descrip
                                           ,'00000000-0000-0000-0000-000000000000' AS reqGrpId
                                           ,@StudentStartDate AS CurrentDate
                                           ,t2.StartDate
                                           ,t2.EndDate
                                           ,(
                                              SELECT    MAX(ActualScore)
                                              FROM      adLeadEntranceTest
                                              WHERE     EntrTestId = t1.adReqId
                                                        AND StudentId = @StudentId
                                            ) AS ActualScore
                                           ,(
                                              SELECT TOP 1
                                                        TestTaken
                                              FROM      adLeadEntranceTest
                                              WHERE     EntrTestId = t1.adReqId
                                                        AND StudentId = @StudentId
                                            ) AS TestTaken
                                           ,(
                                              SELECT TOP 1
                                                        Comments
                                              FROM      adLeadEntranceTest
                                              WHERE     EntrTestId = t1.adReqId
                                                        AND StudentId = @StudentId
                                            ) AS Comments
                                           ,(
                                              SELECT TOP 1
                                                        override
                                              FROM      adEntrTestOverRide
                                              WHERE     EntrTestId = t1.adReqId
                                                        AND StudentId = @StudentId
                                            ) AS override
                                           ,    
                                       --     t2.Minscore ,    
                                            CASE WHEN (
                                                        SELECT  COUNT(*)
                                                        FROM    adPrgVerTestDetails
                                                        WHERE   adReqId = t1.adReqId
                                                                AND PrgVerId IN ( SELECT    PrgVerId
                                                                                  FROM      arStuEnrollments
                                                                                  WHERE     StudentId = @StudentId )
                                                      ) >= 1 THEN (
                                                                    SELECT    
                                                              DISTINCT      MinScore
                                                                    FROM    adPrgVerTestDetails
                                                                    WHERE   adReqId = t1.adReqId
                                                                            AND PrgVerId IN ( SELECT    PrgVerId
                                                                                              FROM      arStuEnrollments
                                                                                              WHERE     StudentId = @StudentId )
                                                                  )
                                                 ELSE t2.Minscore
                                            END AS MinScore
                                           ,1 AS Required
                                           ,reqforEnrollment
                                           ,reqforFinancialAid
                                           ,reqforGraduation
                                           ,(
                                              SELECT    ModuleName
                                              FROM      dbo.syModules A
                                              WHERE     A.ModuleID = t1.moduleID
                                            ) AS ModuleName
                                           ,t1.CampGrpId
                                  FROM      adReqs t1
                                           ,adReqsEffectiveDates t2
                                           ,adReqLeadGroups t3
                                           ,adPrgVerTestDetails t5
                                  WHERE     t1.adReqId = t2.adReqId
                                            AND t2.MandatoryRequirement <> 1
                                            AND t1.adreqTypeId IN ( 1 )
                                            AND t2.adReqEffectiveDateId = t3.adReqEffectiveDateId
                                            AND t1.adReqId = t5.adReqId
                                            AND t5.PrgVerId IN ( SELECT PrgVerId
                                                                 FROM   arStuEnrollments
                                                                 WHERE  StudentId = @StudentId )    
                                            --AND t3.LeadGrpId IN (    
                                            --SELECT DISTINCT    
                                            --        LeadGrpId    
                                            --FROM    adLeadByLeadGroups    
                                            --WHERE   StuEnrollId IN (    
                                            --        SELECT  StuEnrollId    
                                            --        FROM    arStuEnrollments    
                                            --        WHERE   StudentId = @StudentID ) )    
                                            AND t5.adReqId IS NOT NULL
                                            AND t1.StatusId = @ActiveStatusID
                                ) R1
                      WHERE     R1.CurrentDate >= R1.StartDate
                                AND (
                                      R1.CurrentDate <= R1.EndDate
                                      OR R1.EndDate IS NULL
                                    )
                      UNION
                      SELECT      
            DISTINCT            adReqId
                               ,Descrip
                               ,reqGrpId
                               ,StartDate
                               ,EndDate
                               ,ActualScore
                               ,TestTaken
                               ,Comments
                               ,override
                               ,Minscore
                               ,Required
                               ,reqforEnrollment
                               ,reqforFinancialAid
                               ,reqforGraduation
                               ,ModuleName
                               ,CASE WHEN ActualScore >= Minscore THEN 'True'
                                     ELSE 'False'
                                END AS Pass
                               ,CampGrpId
                      FROM      (
                                  SELECT DISTINCT
                                            t1.adReqId
                                           ,t1.Descrip
                                           ,'00000000-0000-0000-0000-000000000000' AS reqGrpId
                                           ,@StudentStartDate AS CurrentDate
                                           ,t2.StartDate
                                           ,t2.EndDate
                                           ,(
                                              SELECT    MAX(ActualScore)
                                              FROM      adLeadEntranceTest
                                              WHERE     EntrTestId = t1.adReqId
                                                        AND StudentId = @StudentId
                                            ) AS ActualScore
                                           ,(
                                              SELECT TOP 1
                                                        TestTaken
                                              FROM      adLeadEntranceTest
                                              WHERE     EntrTestId = t1.adReqId
                                                        AND StudentId = @StudentId
                                            ) AS TestTaken
                                           ,(
                                              SELECT TOP 1
                                                        Comments
                                              FROM      adLeadEntranceTest
                                              WHERE     EntrTestId = t1.adReqId
                                                        AND StudentId = @StudentId
                                            ) AS Comments
                                           ,(
                                              SELECT TOP 1
                                                        override
                                              FROM      adEntrTestOverRide
                                              WHERE     EntrTestId = t1.adReqId
                                                        AND StudentId = @StudentId
                                            ) AS override
                                           ,t2.Minscore
                                           ,ISNULL(t6.IsRequired,0) AS Required
                                           ,reqforEnrollment
                                           ,reqforFinancialAid
                                           ,reqforGraduation
                                           ,(
                                              SELECT    ModuleName
                                              FROM      dbo.syModules A
                                              WHERE     A.ModuleID = t1.moduleID
                                            ) AS ModuleName
                                           ,t1.CampGrpId
                                  FROM      adReqs t1
                                           ,adReqsEffectiveDates t2
                                           ,adReqLeadGroups t3
                                           ,adPrgVerTestDetails t5
                                           ,adReqGrpDef t6
                                           ,adLeadByLeadGroups t7
                                  WHERE     t1.adReqId = t2.adReqId
                                            AND t1.adreqTypeId IN ( 1 )
                                            AND t2.adReqEffectiveDateId = t3.adReqEffectiveDateId
                                            AND t5.ReqGrpId = t6.ReqGrpId
                                            AND t3.LeadGrpId = t7.LeadGrpId
                                            AND t6.LeadGrpId = t7.LeadGrpId
                                            AND t1.adReqId = t6.adReqId
                                            AND t5.ReqGrpId IS NOT NULL
                                            AND t7.StuEnrollId IN ( SELECT  StuEnrollId
                                                                    FROM    arStuEnrollments
                                                                    WHERE   StudentId = @StudentId )
                                            AND t5.PrgVerId IN ( SELECT PrgVerId
                                                                 FROM   arStuEnrollments
                                                                 WHERE  StudentId = @StudentId )
                                            AND t1.StatusId = @ActiveStatusID
                                ) R1
                      WHERE     R1.CurrentDate >= R1.StartDate
                                AND (
                                      R1.CurrentDate <= R1.EndDate
                                      OR R1.EndDate IS NULL
                                    )
                      UNION
                      SELECT  DISTINCT
                                adReqId
                               ,Descrip
                               ,ReqGrpId
                               ,StartDate
                               ,EndDate
                               ,ActualScore
                               ,TestTaken
                               ,Comments
                               ,override
                               ,Minscore
                               ,Required
                               ,reqforEnrollment
                               ,reqforFinancialAid
                               ,reqforGraduation
                               ,ModuleName
                               ,CASE WHEN ActualScore >= Minscore THEN 'True'
                                     ELSE 'False'
                                END AS Pass
                               ,CampGrpId
                      FROM      (
                                  SELECT  DISTINCT
                                            t1.adReqId
                                           ,t1.Descrip
                                           ,'00000000-0000-0000-0000-000000000000' AS ReqGrpId
                                           ,@StudentStartDate AS CurrentDate
                                           ,t2.StartDate
                                           ,t2.EndDate
                                           ,(
                                              SELECT    MAX(ActualScore)
                                              FROM      adLeadEntranceTest
                                              WHERE     EntrTestId = t1.adReqId
                                                        AND StudentId = @StudentId
                                            ) AS ActualScore
                                           ,(
                                              SELECT TOP 1
                                                        TestTaken
                                              FROM      adLeadEntranceTest
                                              WHERE     EntrTestId = t1.adReqId
                                                        AND StudentId = @StudentId
                                            ) AS TestTaken
                                           ,(
                                              SELECT TOP 1
                                                        Comments
                                              FROM      adLeadEntranceTest
                                              WHERE     EntrTestId = t1.adReqId
                                                        AND StudentId = @StudentId
                                            ) AS Comments
                                           ,(
                                              SELECT TOP 1
                                                        override
                                              FROM      adEntrTestOverRide
                                              WHERE     EntrTestId = t1.adReqId
                                                        AND StudentId = @StudentId
                                            ) AS override
                                           ,t2.Minscore
                                           ,ISNULL(t3.IsRequired,0) AS Required
                                           ,reqforEnrollment
                                           ,reqforFinancialAid
                                           ,reqforGraduation
                                           ,(
                                              SELECT    ModuleName
                                              FROM      dbo.syModules A
                                              WHERE     A.ModuleID = t1.moduleID
                                            ) AS ModuleName
                                           ,t1.CampGrpId
                                  FROM      adReqs t1
                                           ,adReqsEffectiveDates t2
                                           ,adReqLeadGroups t3
                                  WHERE     t1.adReqId = t2.adReqId
                                            AND t2.adReqEffectiveDateId = t3.adReqEffectiveDateId
                                            AND t1.StatusId = @ActiveStatusID
                                            AND t1.adReqId NOT IN ( SELECT DISTINCT
                                                                            s1.adReqId
                                                                    FROM    adReqGrpDef s1
                                                                           ,adPrgVerTestDetails s2
                                                                    WHERE   s1.ReqGrpId = s2.ReqGrpId
                                                                            AND s2.PrgVerId IN ( SELECT PrgVerId
                                                                                                 FROM   arStuEnrollments
                                                                                                 WHERE  StudentId = @StudentId )
                                                                            AND s2.adReqId IS NULL )
                                            AND t3.LeadGrpId IN ( SELECT    LeadGrpId
                                                                  FROM      adLeadByLeadGroups
                                                                  WHERE     StuEnrollId IN ( SELECT StuEnrollId
                                                                                             FROM   arStuEnrollments
                                                                                             WHERE  StudentId = @StudentId ) )
                                            AND t1.adReqTypeId IN ( 1 )
                                            AND t2.MandatoryRequirement <> 1
                                            AND t1.adReqId NOT IN ( SELECT DISTINCT
                                                                            adReqId
                                                                    FROM    adPrgVerTestDetails
                                                                    WHERE   adReqId IS NOT NULL
                                                                            AND PrgVerId IN ( SELECT    PrgVerId
                                                                                              FROM      arStuEnrollments
                                                                                              WHERE     StudentId = @StudentId ) )
                                ) R1
                      WHERE     R1.CurrentDate >= R1.StartDate
                                AND (
                                      R1.CurrentDate <= R1.EndDate
                                      OR R1.EndDate IS NULL
                                    )
                    ) R2
          WHERE     R2.CampGrpId IN ( SELECT DISTINCT
                                                t2.CampGrpId
                                      FROM      syCmpGrpCmps t1
                                               ,syCampGrps t2
                                      WHERE     t1.CampGrpId = t2.CampGrpId
                                                AND t1.CampusId = @CampusID
                                                AND t2.StatusId = @ActiveStatusID )
                    AND (
                          reqforEnrollment = 1
                          OR reqforGraduation = 1
                        )
        )
        UNION
        (
          SELECT  DISTINCT
                    adReqId
                   ,Descrip
                   ,ReqGrpId
                   ,StartDate
                   ,EndDate
                   ,ActualScore
                   ,TestTaken
                   ,Comments
                   ,CASE WHEN override >= 1 THEN 'True'
                         ELSE 'False'
                    END AS OverRide
                   ,CASE WHEN (
                                SELECT  COUNT(*)
                                FROM    adPrgVerTestDetails
                                WHERE   adReqId = R2.adReqId
                                        AND PrgVerId IN ( SELECT    PrgVerId
                                                          FROM      arStuEnrollments
                                                          WHERE     StudentId = @StudentId )
                              ) >= 1 THEN (
                                            SELECT  DISTINCT
                                                    MinScore
                                            FROM    adPrgVerTestDetails
                                            WHERE   adReqId = R2.adReqId
                                                    AND PrgVerId IN ( SELECT    PrgVerId
                                                                      FROM      arStuEnrollments
                                                                      WHERE     StudentId = @StudentId )
                                          )
                         ELSE MinScore
                    END AS MinScore
                   ,Required
                   ,reqforEnrollment
                   ,reqforFinancialAid
                   ,reqforGraduation
                   ,ModuleName
                   ,CASE WHEN (
                                TestTaken IS NOT NULL
                                AND ActualScore >= 1
                              ) THEN 1
                         ELSE 0
                    END AS TestTakenCount
                   ,Pass
          FROM      (
                      SELECT      
     DISTINCT                   adReqId
                               ,Descrip
                               ,ReqGrpId
                               ,StartDate
                               ,EndDate
                               ,ActualScore
                               ,TestTaken
                               ,Comments
                               ,override
                               ,Minscore
                               ,Required
                               ,reqforEnrollment
                               ,reqforFinancialAid
                               ,reqforGraduation
                               ,ModuleName
                               ,CASE WHEN ActualScore >= Minscore THEN 'True'
                                     ELSE 'False'
                                END AS Pass
                               ,CampGrpId
                      FROM      (
                                  SELECT DISTINCT
                                            t1.adReqId
                                           ,t1.Descrip
                                           ,CASE WHEN (
                                                        SELECT  COUNT(*)
                                                        FROM    adReqGroups
                                                        WHERE   IsMandatoryReqGrp = 1
                                                      ) >= 1 THEN (
                                                                    SELECT  ReqGrpId
                                                                    FROM    adReqGroups
                                                                    WHERE   IsMandatoryReqGrp = 1
                                                                  )
                                                 ELSE '00000000-0000-0000-0000-000000000000'
                                            END AS ReqGrpId
                                           ,GETDATE() AS CurrentDate
                                           ,t2.StartDate
                                           ,t2.EndDate
                                           ,(
                                              SELECT    MAX(ActualScore)
                                              FROM      adLeadEntranceTest
                                              WHERE     EntrTestId = t1.adReqId
                                                        AND StudentId = @StudentId
                                            ) AS ActualScore
                                           ,(
                                              SELECT TOP 1
                                                        TestTaken
                                              FROM      adLeadEntranceTest
                                              WHERE     EntrTestId = t1.adReqId
                                                        AND StudentId = @StudentId
                                            ) AS TestTaken
                                           ,(
                                              SELECT TOP 1
                                                        Comments
                                              FROM      adLeadEntranceTest
                                              WHERE     EntrTestId = t1.adReqId
                                                        AND StudentId = @StudentId
                                            ) AS Comments
                                           ,(
                                              SELECT TOP 1
                                                        override
                                              FROM      adEntrTestOverRide
                                              WHERE     EntrTestId = t1.adReqId
                                                        AND StudentId = @StudentId
                                            ) AS override
                                           ,t2.Minscore
                                           ,1 AS Required
                                           ,reqforEnrollment
                                           ,reqforFinancialAid
                                           ,reqforGraduation
                                           ,(
                                              SELECT    ModuleName
                                              FROM      dbo.syModules A
                                              WHERE     A.ModuleID = t1.moduleID
                                            ) AS ModuleName
                                           ,t1.CampGrpId
                                  FROM      adReqs t1
                                           ,adReqsEffectiveDates t2
                                  WHERE     t1.adReqId = t2.adReqId
                                            AND t2.MandatoryRequirement = 1
                                            AND t1.adReqTypeId IN ( 1 )
                                            AND t1.StatusId = @ActiveStatusID
                                ) R1
                      WHERE     R1.CurrentDate >= R1.StartDate
                                AND (
                                      R1.CurrentDate <= R1.EndDate
                                      OR R1.EndDate IS NULL
                                    )
                      UNION
                      SELECT      
         DISTINCT               adReqId
                               ,Descrip
                               ,reqGrpId
                               ,StartDate
                               ,EndDate
                               ,ActualScore
                               ,TestTaken
                               ,Comments
                               ,override
                               ,MinScore
                               ,Required
                               ,reqforEnrollment
                               ,reqforFinancialAid
                               ,reqforGraduation
                               ,ModuleName
                               ,CASE WHEN ActualScore >= MinScore THEN 'True'
                                     ELSE 'False'
                                END AS Pass
                               ,CampGrpId
                      FROM      (
                                  SELECT DISTINCT
                                            t1.adReqId
                                           ,t1.Descrip
                                           ,'00000000-0000-0000-0000-000000000000' AS reqGrpId
                                           ,GETDATE() AS CurrentDate
                                           ,t2.StartDate
                                           ,t2.EndDate
                                           ,(
                                              SELECT    MAX(ActualScore)
                                              FROM      adLeadEntranceTest
                                              WHERE     EntrTestId = t1.adReqId
                                                        AND StudentId = @StudentId
                                            ) AS ActualScore
                                           ,(
                                              SELECT TOP 1
                                                        TestTaken
                                              FROM      adLeadEntranceTest
                                              WHERE     EntrTestId = t1.adReqId
                                                        AND StudentId = @StudentId
                                            ) AS TestTaken
                                           ,(
                                              SELECT TOP 1
                                                        Comments
                                              FROM      adLeadEntranceTest
                                              WHERE     EntrTestId = t1.adReqId
                                                        AND StudentId = @StudentId
                                            ) AS Comments
                                           ,(
                                              SELECT TOP 1
                                                        override
                                              FROM      adEntrTestOverRide
                                              WHERE     EntrTestId = t1.adReqId
                                                        AND StudentId = @StudentId
                                            ) AS override
                                           ,    
                                          --  t2.Minscore ,    
                                            CASE WHEN (
                                                        SELECT  COUNT(*)
                                                        FROM    adPrgVerTestDetails
                                                        WHERE   adReqId = t1.adReqId
                                                                AND PrgVerId IN ( SELECT    PrgVerId
                                                                                  FROM      arStuEnrollments
                                                                                  WHERE     StudentId = @StudentId )
                                                      ) >= 1 THEN (
                                                                    SELECT    
                                                              DISTINCT      MinScore
                                                                    FROM    adPrgVerTestDetails
                                                                    WHERE   adReqId = t1.adReqId
                                                                            AND PrgVerId IN ( SELECT    PrgVerId
                                                                                              FROM      arStuEnrollments
                                                                                              WHERE     StudentId = @StudentId )
                                                                  )
                                                 ELSE t2.Minscore
                                            END AS MinScore
                                           ,1 AS Required
                                           ,reqforEnrollment
                                           ,reqforFinancialAid
                                           ,reqforGraduation
                                           ,(
                                              SELECT    ModuleName
                                              FROM      dbo.syModules A
                                              WHERE     A.ModuleID = t1.moduleID
                                            ) AS ModuleName
                                           ,t1.CampGrpId
                                  FROM      adReqs t1
                                           ,adReqsEffectiveDates t2
                                           ,adReqLeadGroups t3
                                           ,adPrgVerTestDetails t5
                                  WHERE     t1.adReqId = t2.adReqId
                                            AND t2.MandatoryRequirement <> 1
                                            AND t1.adreqTypeId IN ( 1 )
                                            AND t2.adReqEffectiveDateId = t3.adReqEffectiveDateId
                                            AND t1.adReqId = t5.adReqId
                                            AND t5.PrgVerId IN ( SELECT PrgVerId
                                                                 FROM   arStuEnrollments
                                                                 WHERE  StudentId = @StudentId )    
                                            --AND t3.LeadGrpId IN (    
                                            --SELECT DISTINCT    
                                            --        LeadGrpId    
                                            --FROM    adLeadByLeadGroups    
                                            --WHERE   StuEnrollId IN (    
                                            --        SELECT  StuEnrollId    
                                            --        FROM    arStuEnrollments    
                                            --        WHERE   StudentId = @StudentID ) )    
                                            AND t5.adReqId IS NOT NULL
                                            AND t1.StatusId = @ActiveStatusID
                                ) R1
                      WHERE     R1.CurrentDate >= R1.StartDate
                                AND (
                                      R1.CurrentDate <= R1.EndDate
                                      OR R1.EndDate IS NULL
                                    )
                      UNION
                      SELECT      
            DISTINCT            adReqId
                               ,Descrip
                               ,reqGrpId
                               ,StartDate
                               ,EndDate
                               ,ActualScore
                               ,TestTaken
                               ,Comments
                               ,override
                               ,Minscore
                               ,Required
                               ,reqforEnrollment
                               ,reqforFinancialAid
                               ,reqforGraduation
                               ,ModuleName
                               ,CASE WHEN ActualScore >= Minscore THEN 'True'
                                     ELSE 'False'
                                END AS Pass
                               ,CampGrpId
                      FROM      (
                                  SELECT DISTINCT
                                            t1.adReqId
                                           ,t1.Descrip
                                           ,'00000000-0000-0000-0000-000000000000' AS reqGrpId
                                           ,GETDATE() AS CurrentDate
                                           ,t2.StartDate
                                           ,t2.EndDate
                                           ,(
                                              SELECT    MAX(ActualScore)
                                              FROM      adLeadEntranceTest
                                              WHERE     EntrTestId = t1.adReqId
                                                        AND StudentId = @StudentId
                                            ) AS ActualScore
                                           ,(
                                              SELECT TOP 1
                                                        TestTaken
                                              FROM      adLeadEntranceTest
                                              WHERE     EntrTestId = t1.adReqId
                                                        AND StudentId = @StudentId
                                            ) AS TestTaken
                                           ,(
                                              SELECT TOP 1
                                                        Comments
                                              FROM      adLeadEntranceTest
                                              WHERE     EntrTestId = t1.adReqId
                                                        AND StudentId = @StudentId
                                            ) AS Comments
                                           ,(
                                              SELECT TOP 1
                                                        override
                                              FROM      adEntrTestOverRide
                                              WHERE     EntrTestId = t1.adReqId
                                                        AND StudentId = @StudentId
                                            ) AS override
                                           ,t2.Minscore
                                           ,ISNULL(t6.IsRequired,0) AS Required
                                           ,reqforEnrollment
                                           ,reqforFinancialAid
                                           ,reqforGraduation
                                           ,(
                                              SELECT    ModuleName
                                              FROM      dbo.syModules A
                                              WHERE     A.ModuleID = t1.moduleID
                                            ) AS ModuleName
                                           ,t1.CampGrpId
                                  FROM      adReqs t1
                                           ,adReqsEffectiveDates t2
                                           ,adReqLeadGroups t3
                                           ,adPrgVerTestDetails t5
                                           ,adReqGrpDef t6
                                           ,adLeadByLeadGroups t7
                                  WHERE     t1.adReqId = t2.adReqId
                                            AND t1.adreqTypeId IN ( 1 )
                                            AND t2.adReqEffectiveDateId = t3.adReqEffectiveDateId
                                            AND t5.ReqGrpId = t6.ReqGrpId
                                            AND t3.LeadGrpId = t7.LeadGrpId
                                            AND t6.LeadGrpId = t7.LeadGrpId
                                            AND t1.adReqId = t6.adReqId
                                            AND t5.ReqGrpId IS NOT NULL
                                            AND t7.StuEnrollId IN ( SELECT  StuEnrollId
                                                                    FROM    arStuEnrollments
                                                                    WHERE   StudentId = @StudentId )
                                            AND t5.PrgVerId IN ( SELECT PrgVerId
                                                                 FROM   arStuEnrollments
                                                                 WHERE  StudentId = @StudentId )
                                            AND t1.StatusId = @ActiveStatusID
                                ) R1
                      WHERE     R1.CurrentDate >= R1.StartDate
                                AND (
                                      R1.CurrentDate <= R1.EndDate
                                      OR R1.EndDate IS NULL
                                    )
                      UNION
                      SELECT  DISTINCT
                                adReqId
                               ,Descrip
                               ,ReqGrpId
                               ,StartDate
                               ,EndDate
                               ,ActualScore
                               ,TestTaken
                               ,Comments
                               ,override
                               ,Minscore
                               ,Required
                               ,reqforEnrollment
                               ,reqforFinancialAid
                               ,reqforGraduation
                               ,ModuleName
                               ,CASE WHEN ActualScore >= Minscore THEN 'True'
                                     ELSE 'False'
                                END AS Pass
                               ,CampGrpId
                      FROM      (
                                  SELECT  DISTINCT
                                            t1.adReqId
                                           ,t1.Descrip
                                           ,'00000000-0000-0000-0000-000000000000' AS ReqGrpId
                                           ,GETDATE() AS CurrentDate
                                           ,t2.StartDate
                                           ,t2.EndDate
                                           ,(
                                              SELECT    MAX(ActualScore)
                                              FROM      adLeadEntranceTest
                                              WHERE     EntrTestId = t1.adReqId
                                                        AND StudentId = @StudentId
                                            ) AS ActualScore
                                           ,(
                                              SELECT TOP 1
                                                        TestTaken
                                              FROM      adLeadEntranceTest
                                              WHERE     EntrTestId = t1.adReqId
                                                        AND StudentId = @StudentId
                                            ) AS TestTaken
                                           ,(
                                              SELECT TOP 1
                                                        Comments
                                              FROM      adLeadEntranceTest
                                              WHERE     EntrTestId = t1.adReqId
                                                        AND StudentId = @StudentId
                                            ) AS Comments
                                           ,(
                                              SELECT TOP 1
                                                        override
                                              FROM      adEntrTestOverRide
                                              WHERE     EntrTestId = t1.adReqId
                                                        AND StudentId = @StudentId
                                            ) AS override
                                           ,t2.Minscore
                                           ,ISNULL(t3.IsRequired,0) AS Required
                                           ,reqforEnrollment
                                           ,reqforFinancialAid
                                           ,reqforGraduation
                                           ,(
                                              SELECT    ModuleName
                                              FROM      dbo.syModules A
                                              WHERE     A.ModuleID = t1.moduleID
                                            ) AS ModuleName
                                           ,t1.CampGrpId
                                  FROM      adReqs t1
                                           ,adReqsEffectiveDates t2
                                           ,adReqLeadGroups t3
                                  WHERE     t1.adReqId = t2.adReqId
                                            AND t2.adReqEffectiveDateId = t3.adReqEffectiveDateId
                                            AND t1.StatusId = @ActiveStatusID
                                            AND t1.adReqId NOT IN ( SELECT DISTINCT
                                                                            s1.adReqId
                                                                    FROM    adReqGrpDef s1
                                                                           ,adPrgVerTestDetails s2
                                                                    WHERE   s1.ReqGrpId = s2.ReqGrpId
                                                                            AND s2.PrgVerId IN ( SELECT PrgVerId
                                                                                                 FROM   arStuEnrollments
                                                                                                 WHERE  StudentId = @StudentId )
                                                                            AND s2.adReqId IS NULL )
                                            AND t3.LeadGrpId IN ( SELECT    LeadGrpId
                                                                  FROM      adLeadByLeadGroups
                                                                  WHERE     StuEnrollId IN ( SELECT StuEnrollId
                                                                                             FROM   arStuEnrollments
                                                                                             WHERE  StudentId = @StudentId ) )
                                            AND t1.adReqTypeId IN ( 1 )
                                            AND t2.MandatoryRequirement <> 1
                                            AND t1.adReqId NOT IN ( SELECT DISTINCT
                                                                            adReqId
                                                                    FROM    adPrgVerTestDetails
                                                                    WHERE   adReqId IS NOT NULL
                                                                            AND PrgVerId IN ( SELECT    PrgVerId
                                                                                              FROM      arStuEnrollments
                                                                                              WHERE     StudentId = @StudentId ) )
                                ) R1
                      WHERE     R1.CurrentDate >= R1.StartDate
                                AND (
                                      R1.CurrentDate <= R1.EndDate
                                      OR R1.EndDate IS NULL
                                    )
                    ) R2
          WHERE     R2.CampGrpId IN ( SELECT DISTINCT
                                                t2.CampGrpId
                                      FROM      syCmpGrpCmps t1
                                               ,syCampGrps t2
                                      WHERE     t1.CampGrpId = t2.CampGrpId
                                                AND t1.CampusId = @CampusID
                                                AND t2.StatusId = @ActiveStatusID )
                    AND ( reqforfinancialAid = 1 )
        );    
                                                                           
       
    END;       



GO
