SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- select * from adLeadTransactions where LeadId='B1B7B57C-1B2B-474A-B529-30668799C1D8' order by DisplaySequence, SecondDisplaySequence,TransTypeId
-- exec USP_ApplicantLedger_GetList 'B1B7B57C-1B2B-474A-B529-30668799C1D8'
--Select getdate()
--select substring(getdate(),1,1)
--select *,Case when transTypeId=1 and TransAmount < 0 Then 'Adjusted Charge' else (select Distinct Description from saTransTypes where TransTypeId=LT.TransTypeId) end as TransactionType from adLeadTransactions LT
CREATE PROCEDURE [dbo].[USP_ApplicantLedger_GetList]
    @LeadId UNIQUEIDENTIFIER
AS --Set @LeadId='43D6FD75-5453-4F22-9283-BC7A213128FB' 
    SELECT  *
    FROM    (
              SELECT    LT.TransactionId
                       ,LT.LeadId
                       ,LT.TransCodeId
                       ,LT.isEnrolled
                       ,(
                          SELECT DISTINCT
                                    TransCodeDescrip
                          FROM      saTransCodes
                          WHERE     TransCodeId = LT.TransCodeId
                        ) AS TransactionCode
                       ,LT.TransDescrip AS TransDescription
                       ,CASE WHEN TransTypeId = 1
                                  AND TransAmount < 0 THEN 'Charge (Adjusted)'
                             ELSE CASE WHEN TransTypeId = 1
                                            AND TransAmount > 0 THEN 'Payment (Adjusted)'
                                       ELSE (
                                              SELECT DISTINCT
                                                        Description
                                              FROM      saTransTypes
                                              WHERE     TransTypeId = LT.TransTypeId
                                            )
                                  END
                        END AS TransactionType
                       ,LT.ModUser AS Postedby
                       ,LT.TransAmount AS TransactionAmount
                       ,CONVERT(CHAR(10),LT.CreatedDate,101) AS TransDate
                       ,TransReference
                       ,(
                          SELECT TOP 1
                                    CheckNumber
                          FROM      adLeadPayments
                          WHERE     TransactionId = LT.TransactionId
                        ) AS DocumentId
                       ,LT.Voided AS Voided
                       ,LT.ReversalReason
                       ,LT.DisplaySequence
                       ,LT.SecondDisplaySequence
                       ,LT.TransTypeId
              FROM      adLeadTransactions LT
              WHERE     LT.LeadId = @LeadId
                        AND LT.TransTypeId IN ( 0,1,2 )
                        AND LT.Voided = 0
              UNION
              SELECT    LT.TransactionId
                       ,LT.LeadId
                       ,LT.TransCodeId
                       ,LT.isEnrolled
                       ,(
                          SELECT DISTINCT
                                    TransCodeDescrip
                          FROM      saTransCodes
                          WHERE     TransCodeId = LT.TransCodeId
                        ) AS TransactionCode
                       ,LT.TransDescrip AS TransDescription
                       ,CASE WHEN TransTypeId = 0
                                  AND TransAmount > 0 THEN 'Charge'
                             ELSE 'Payment (Voided)'
                        END AS TransactionType
                       ,LT.ModUser AS Postedby
                       ,LT.TransAmount AS TransactionAmount
                       ,CONVERT(CHAR(10),LT.CreatedDate,101) AS TransDate
                       ,TransReference
                       ,(
                          SELECT TOP 1
                                    CheckNumber
                          FROM      adLeadPayments
                          WHERE     TransactionId = LT.TransactionId
                        ) AS DocumentId
                       ,LT.Voided AS Voided
                       ,LT.ReversalReason
                       ,LT.DisplaySequence
                       ,LT.SecondDisplaySequence
                       ,LT.TransTypeId
              FROM      adLeadTransactions LT
              WHERE     LT.LeadId = @LeadId
                        AND LT.TransTypeId IN ( 0,2 )
                        AND LT.Voided = 1
            ) DT
  --and LT.TransDescrip not like '%- Adjustment Chrg' 
ORDER BY    DT.DisplaySequence
           ,DT.SecondDisplaySequence
           ,DT.TransTypeId;



GO
