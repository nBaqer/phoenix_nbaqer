SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[FA_Advantage_Report_GetStudentPaymentPlanSchedules] 
	-- Add the parameters for the stored procedure here
    @paymentPlanId UNIQUEIDENTIFIER = '5E47400D-7C7E-47EF-B405-0041054F81B1'
   ,@voided BIT = 0
AS
    BEGIN
-- GET PAYMENT PLAN SCHEDULES.........
        SELECT  *
               ,( CASE WHEN Balance <= 0.00 THEN 'True'
                       ELSE 'False'
                  END ) AS WasReceived
        FROM    (
                  SELECT    PPS.PayPlanScheduleId
                           ,PPS.PaymentPlanId
                           ,PPS.ExpectedDate
                           ,PPS.Amount
                           ,PPS.Reference
                           ,( PPS.Amount + COALESCE((
                                                      SELECT    SUM(PDR.Amount) * -1
                                                      FROM      saPmtDisbRel PDR
                                                      INNER JOIN saTransactions T ON PayPlanScheduleId = PPS.PayPlanScheduleId
                                                                                     AND PDR.TransactionId = T.TransactionId
                                                      WHERE     T.Voided = @voided
                                                    ),0) ) AS Balance
                           ,( COALESCE((
                                         SELECT SUM(PDR.Amount)
                                         FROM   saPmtDisbRel PDR
                                         INNER JOIN saTransactions T ON PayPlanScheduleId = PPS.PayPlanScheduleId
                                                                        AND PDR.TransactionId = T.TransactionId
                                         WHERE  T.Voided = @voided
                                       ),0) ) AS Received
                           ,0.00 AS RefundAmount
                           ,(
                              SELECT    CONVERT(VARCHAR(10),T.TransDate,101) + ' '
                              FROM      saPmtDisbRel PDR
                              INNER JOIN saTransactions T ON PayPlanScheduleId = PPS.PayPlanScheduleId
                                                             AND T.TransactionId = PDR.TransactionId
                            FOR
                              XML PATH('')
                            ) AS ReceivedData
                  FROM      faStuPaymentPlanSchedule PPS
                  WHERE     PPS.PaymentPlanId = @paymentPlanId
                ) T1
        ORDER BY PaymentPlanId
               ,ExpectedDate;
 
    END;

GO
