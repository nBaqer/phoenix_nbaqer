SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_DictationComponents_GetCount]
    @ReqId UNIQUEIDENTIFIER
AS
    SELECT  COUNT(*) AS RowsCount
    FROM    arBridge_GradeComponentTypes_Courses t1
    INNER JOIN arGrdComponentTypes t3 ON t1.GrdComponentTypeid = t3.GrdComponentTypeid
    WHERE   t3.SysComponentTypeId = 612
            AND t1.ReqId = @ReqId;



GO
