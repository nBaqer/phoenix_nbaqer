SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_GradeComponentsMappedToDictationSpeedTestComponent_GetList]
    @SysComponentTypeId INT
AS
    SELECT DISTINCT
            GrdComponentTypeId
           ,Descrip
    FROM    arGrdComponentTypes
    WHERE   SysComponentTypeId = @SysComponentTypeId
    ORDER BY Descrip;



GO
