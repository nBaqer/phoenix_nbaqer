SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_FERPAPolicyInfo_Insert]
    (
     @FERPAEntityID UNIQUEIDENTIFIER
    ,@FERPACategoryID UNIQUEIDENTIFIER
    ,@StudentId UNIQUEIDENTIFIER
    ,@ModUser VARCHAR(50)
    ,@moddate DATETIME
    )
AS
    SET NOCOUNT ON;
    INSERT  INTO arFerpaPolicy
    VALUES  ( NEWID(),@FERPAEntityID,@FERPACategoryID,@StudentId,@ModUser,@moddate );



GO
