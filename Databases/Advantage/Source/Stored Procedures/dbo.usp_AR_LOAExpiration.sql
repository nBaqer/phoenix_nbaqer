SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





-- =============================================
-- Author: Vijay Ramteke
-- Create date: July 09, 2010
-- Description: Pending LOA Expiration
-- =============================================
CREATE PROCEDURE [dbo].[usp_AR_LOAExpiration]
    @CampGrpId VARCHAR(8000)
   ,@ProgramIds VARCHAR(8000) = NULL
   ,@StartDate AS DATETIME
   ,@EndDate AS DATETIME
   ,@StudentIdentifier VARCHAR(100)
AS
    BEGIN

        SELECT  StuEnrollId
               ,ST.StatusCodeId
               ,PrgVerId
               ,StartDate
               ,StudentId
               ,EnrollmentId
               ,CampusId
        INTO    #tempEnrollments
        FROM    arStuEnrollments ST
               ,syStatusCodes SCT
        WHERE   ST.StatusCodeId = SCT.StatusCodeId
                AND SCT.SysStatusId IN ( 10 )
                AND CampusId IN ( (
                                    SELECT DISTINCT
                                            t1.CampusId
                                    FROM    sycmpgrpcmps t1
                                    WHERE   t1.campgrpid IN ( SELECT    strval
                                                              FROM      dbo.SPLIT(@CampGrpId) )
                                  )
                             )
                AND (
                      @ProgramIds IS NULL
                      OR PrgVerId IN ( SELECT   strval
                                       FROM     dbo.SPLIT(@ProgramIds) )
                    );

        SELECT  LastName
               ,FirstName
               ,MiddleName
               ,StudentIdentifier
               ,StuEnrollId
               ,StatusCodeDescrip
               ,PrgVerDescrip
               ,PrgVerId
               ,StartDate
               ,LDA
               ,LOAStartDate
               ,LOAEndDate
               ,CONVERT(VARCHAR,@StartDate,101) + ' And ' + CONVERT(VARCHAR,@EndDate,101) AS DateRange
               ,Campus
        FROM    (
                  SELECT    RTRIM(LTRIM(S.LastName)) AS LastName
                           ,RTRIM(LTRIM(S.FirstName)) AS FirstName
                           ,RTRIM(LTRIM(ISNULL(S.MiddleName,''))) AS MiddleName
                           ,SE.StuEnrollId
                           ,SC.StatusCodeDescrip
                           ,RTRIM(LTRIM(PV.PrgVerDescrip)) + ' ( ' + RTRIM(LTRIM(PV.PrgVerCode)) + ' )' AS PrgVerDescrip
                           ,SE.PrgVerId
                           ,SE.StartDate
                           ,SLOA.StartDate AS LOAStartDate
                           ,SLOA.EndDate AS LOAEndDate
                           ,CASE @StudentIdentifier
                              WHEN 'StudentId' THEN RTRIM(LTRIM(S.StudentNumber))
                              WHEN 'EnrollmentId' THEN RTRIM(LTRIM(SE.EnrollmentId))
                              ELSE S.SSN
                            END AS StudentIdentifier
                           ,(
                              SELECT    MAX(LDA)
                              FROM      (
                                          SELECT    MAX(AttendedDate) AS LDA
                                          FROM      arExternshipAttendance
                                          WHERE     StuEnrollId = SE.StuEnrollId
                                          UNION ALL
                                          SELECT    MAX(MeetDate) AS LDA
                                          FROM      atClsSectAttendance
                                          WHERE     StuEnrollId = SE.StuEnrollId
                                                    AND Actual >= 1
                                          UNION ALL
                                          SELECT    MAX(AttendanceDate) AS LDA
                                          FROM      atAttendance
                                          WHERE     EnrollId = SE.StuEnrollId
                                                    AND Actual >= 1
                                          UNION ALL
                                          SELECT    MAX(RecordDate) AS LDA
                                          FROM      arStudentClockAttendance
                                          WHERE     StuEnrollId = SE.StuEnrollId
                                                    AND (
                                                          ActualHours >= 1.00
                                                          AND ActualHours <> 99.00
                                                          AND ActualHours <> 999.00
                                                          AND ActualHours <> 9999.00
                                                        )
                                          UNION ALL
                                          SELECT    MAX(MeetDate) AS LDA
                                          FROM      atConversionAttendance
                                          WHERE     StuEnrollId = SE.StuEnrollId
                                                    AND (
                                                          Actual >= 1.00
                                                          AND Actual <> 99.00
                                                          AND Actual <> 999.00
                                                          AND Actual <> 9999.00
                                                        )
                                        ) TR
                            ) AS LDA
                           ,(
                              SELECT    CampDescrip
                              FROM      syCampuses C
                              WHERE     C.CampusId = SE.CampusId
                            ) AS Campus
                  FROM      arStudent S
                           ,#tempEnrollments SE
                           ,syStatusCodes SC
                           ,arPrgVersions PV
                           ,arStudentLOAs SLOA
                  WHERE     S.StudentId = SE.StudentId
                            AND SE.StatusCodeId = SC.StatusCodeId
                            AND SE.PrgVerId = PV.PrgVerId
                            AND SLOA.StuEnrollId = SE.StuEnrollId
                            AND SLOA.EndDate >= @StartDate
                            AND SLOA.EndDate <= @EndDate
                ) T
        ORDER BY LastName
               ,FirstName
               ,LDA ASC;


        DROP TABLE #tempEnrollments;

    END;






GO
