SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_GetFERPAEntityInfo]
    @FERPAEntityID UNIQUEIDENTIFIER
AS
    SET NOCOUNT ON;
    SELECT  B.StatusId
           ,(
              SELECT    Status
              FROM      syStatuses
              WHERE     StatusId = B.StatusId
            ) AS Status
           ,B.FERPAEntityCode
           ,B.FERPAEntityDEscrip
           ,B.CampGrpId
           ,(
              SELECT    CampGrpDescrip
              FROM      syCampGrps
              WHERE     CampGrpId = B.CampGrpId
            ) AS CampGrpDescrip
    FROM    arFERPAEntity B
    WHERE   B.FERPAEntityID = @FERPAEntityID;




GO
