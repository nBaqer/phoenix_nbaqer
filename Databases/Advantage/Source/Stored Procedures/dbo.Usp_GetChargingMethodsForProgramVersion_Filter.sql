SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[Usp_GetChargingMethodsForProgramVersion_Filter]
    @CampusId UNIQUEIDENTIFIER
   ,@PrgVerId UNIQUEIDENTIFIER = NULL
   ,@selectedBillingMethods VARCHAR(8000)
AS
    BEGIN
        SELECT DISTINCT
                t5.BillingMethodId AS BillingMethodId
               ,t5.BillingMethodDescrip + ' (' + t5.BillingMethodCode + ')' AS Descrip
        FROM    syCampGrps AS t1
        INNER JOIN syCmpGrpCmps AS t2 ON t1.CampGrpId = t2.CampGrpId
        INNER JOIN syCampuses AS t3 ON t2.CampusId = t3.CampusId
        INNER JOIN saBillingMethods AS t5 ON t1.CampGrpId = t5.CampGrpId
        INNER JOIN syStatuses AS t4 ON t5.StatusId = t4.StatusId
        INNER JOIN arPrgVersions AS t6 ON t6.BillingMethodId = t5.BillingMethodId
        WHERE   t3.CampusId = @CampusId
                AND (
                      @PrgVerId IS NULL
                      OR t6.PrgVerId = @PrgVerId
                    )
                AND t5.BillingMethodId NOT IN ( @selectedBillingMethods );
    END;
GO
