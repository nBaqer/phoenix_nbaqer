SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_GetAllActiveCourses] @campusId VARCHAR(50)
---[dbo].[usp_GetAllActiveCourses] '436bf45e-5420-4c1f-8687-cd32a60d64be'
AS
    SET NOCOUNT ON;
    SELECT  T.ReqId
           ,'(' + T.Code + ') ' + T.Descrip AS Descrip
           ,1 AS STATUS
           ,T.Descrip AS ShortDescr
           ,T.Code
    FROM    arReqs T
    INNER JOIN syStatuses S ON T.StatusId = S.StatusId
    WHERE   S.Status = 'Active'
            AND T.CampGrpId IN ( SELECT CampGrpId
                                 FROM   syCmpGrpCmps
                                 WHERE  CampusId = @campusId )
            AND ReqTypeId = 1
    ORDER BY T.Descrip
           ,T.Code;     

-----------------------------------------------------------------------------------------
--DE9082/DE9080 - Course code should be displayed on 'Register Student'  &  ' Enter Class Sections with Periods' pages. - NZ - 2/11/13
-- END 
-----------------------------------------------------------------------------------------

-----------------------------------------------------------------------------------------
--DE9072 QA: Change the IPEDS report names shown on the landing page to include more of the actual title.
--Start added by Theresa G on 2/12/13
-----------------------------------------------------------------------------------------



GO
