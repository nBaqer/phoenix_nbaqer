SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_SystemComponentTypes_GetList]
AS
    SELECT DISTINCT
            ResourceID
           ,Resource
    FROM    syResources
    WHERE   ResourceTypeID = 10
    ORDER BY Resource;



GO
