SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_GetClsSectionScheduleInfo]
    (
     @ClsSectMeetingID VARCHAR(50)
    )
AS
    BEGIN   
      
        SELECT DISTINCT
                CS.ClsSectionId
               ,ClsSectMeetingId
               ,RQ.Code
               ,RQ.Descrip
               ,RQ.UnitTypeId
               ,CSM.StartDate
               ,CSM.EndDate
        FROM    dbo.arClassSections CS
               ,dbo.arClsSectMeetings CSM
               ,dbo.arReqs RQ
               ,arResults AR
        WHERE   RQ.ReqId = CS.ReqId
                AND AR.TestId = CS.ClsSectionId
                AND CS.ClsSectionId = CSM.ClsSectionId
                AND CSM.ClsSectMeetingId = @ClsSectMeetingID;  
  
  
    END;  
---------------------------------------------------------------------------------------------------------------------------------
--DE8578 QA: Posted attendance on Alternate period is not shown in the attendance grid on the Enter class section attendance page
--END - Store Proc Update Missed in 3.0SP2 Geetha 11/19/2012
---------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------------
--US3454 Open New Fields for AdHoc
--Start Bruce Shanblatt 11.26.2012
------------------------------------------------------------------------------------------------------------------------------



GO
