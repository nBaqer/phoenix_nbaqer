SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
----------------------------------------------------------------------------------------------------------------------
--US4477 Scheduled Completed Hours Report Enhancements
--End added by Theresa G on Oct 7 2013
----------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------
-- US4479 Apportioning Report Enhancements
-- Start added by Balaji on Oct 8 2013
----------------------------------------------------------------------------------------------------------------------
--exec USP_SA_GetApportionedCredits 'F111E9DC-E138-418C-8CD5-C2701A2CD54D','887C7690-B8CE-4068-98EB-831B40A0D07A','72B5E261-5F3F-48D0-AB55-B32F60EA18C9','1/1/2010'
CREATE PROCEDURE [dbo].[USP_SA_GetApportionedCredits]
    @campgrpid UNIQUEIDENTIFIER
   ,@prgverid UNIQUEIDENTIFIER
   ,@enrollmentstatus UNIQUEIDENTIFIER
   ,@asofdate DATETIME
AS /*----------------------------------------------------------------------------------------------------
	Author          :	Bruce Shanblatt
    
    Create date		:	07/30/2013
    
	Procedure Name	:	USP_SA_GetApportionedCredits
	
	Parameters		:	Name			     Type	Data Type	            Required? 	
						=====			     ====	=========	            =========	
						@CampGrpId	     In		UNIQUEIDENTIFIER		Required	
						@prgverid      In		UNIQUEIDENTIFIER		Required	
						@enrollmentstatus    In		VARCHAR(25)      		Required	
						@asofdate            In		DATETIME        		Required	
						
*/-----------------------------------------------------------------------------------------------------
--query processing variables
    DECLARE @RowsToProcess INT;
    DECLARE @CurrentRow INT;
    DECLARE @eid UNIQUEIDENTIFIER;
    DECLARE @fsid UNIQUEIDENTIFIER;
    DECLARE @enrollstatdescrip VARCHAR(20);

    DECLARE @selectTable TABLE
        (
         RowID INT NOT NULL
                   PRIMARY KEY
                   IDENTITY(1,1)
        ,studentid UNIQUEIDENTIFIER
        ,studentname VARCHAR(50)
        ,transamount DECIMAL(19,4)
        ,appprtionedcreditay1 DECIMAL(19,4)
        ,appprtionedcreditay2 DECIMAL(19,4)
        ,enrollstatus VARCHAR(20)
        ,sapattendancepercent DECIMAL(19,2)
        ,fundsourceid UNIQUEIDENTIFIER
        ,fundsourcedesc VARCHAR(50)
        ,transactiondate DATETIME
        ,reenrolldate VARCHAR(12)
        ,ssn VARCHAR(12)
        ,campgrpdescrip VARCHAR(30)
        ,Prgverdescrip VARCHAR(30)
        ,asofdate DATE
        );

    SET @enrollstatdescrip = (
                               SELECT   StatusCodeDescrip
                               FROM     dbo.syStatusCodes
                               WHERE    StatusCodeId = @enrollmentstatus
                             );

--load the table variable with the data 
    INSERT  INTO @selectTable
            SELECT DISTINCT
	--StudentEnrollments.StuEnrollId, 
                    Student.StudentId
                   ,Student.FirstName + ' ' + Student.LastName AS StudentName
                   ,StudentTransactions.TransAmount
                   ,Student.ApportionedCreditBalanceAY1
                   ,Student.ApportionedCreditBalanceAY2
                   ,@enrollstatdescrip
                   ,'1.0'
                   ,StudentTransactions.FundSourceId
                   ,' '
                   ,StudentTransactions.TransDate
                   ,
	--CONVERT(VARCHAR(10),StudentEnrollments.ReEnrollmentDate,101),
                    StudentEnrollments.ReEnrollmentDate
                   ,Student.SSN
                   ,(
                      SELECT    CampGrpDescrip
                      FROM      dbo.syCampGrps
                      WHERE     CampGrpId = @campgrpid
                    )
                   ,(
                      SELECT    PrgVerDescrip
                      FROM      dbo.arPrgVersions
                      WHERE     PrgVerId = @prgverid
                    )
                   ,@asofdate
            FROM    dbo.arStuEnrollments StudentEnrollments
            INNER JOIN dbo.arStudent Student ON StudentEnrollments.StudentId = Student.StudentId
            INNER JOIN dbo.saTransactions StudentTransactions ON StudentEnrollments.StuEnrollId = StudentTransactions.StuEnrollId
            INNER JOIN dbo.saTransCodes TransactionCodes ON StudentTransactions.TransCodeId = TransactionCodes.TransCodeId
            WHERE   StudentEnrollments.StuEnrollId IN (
                    SELECT DISTINCT
                            StudentTransactions.StuEnrollId
                    FROM    dbo.saTransactions
                    WHERE   StudentTransactions.StuEnrollId IN (
                            SELECT  StudentEnrollments.StuEnrollId
                            FROM    dbo.arStuEnrollments StudentEnrollments
                            WHERE   StudentEnrollments.CampusId IN ( SELECT CampusGroupsBridge.CampusId
                                                                     FROM   dbo.syCmpGrpCmps CampusGroupsBridge
                                                                     WHERE  CampusGroupsBridge.CampGrpId = @campgrpid
                                                                            AND StudentEnrollments.PrgVerId = @prgverid
                                                                            AND StudentEnrollments.StatusCodeId = @enrollmentstatus ) ) )
                    AND TransactionCodes.IsInstCharge = 1
                    AND StudentTransactions.TransDate <= @asofdate;

--loop through the table variable and insert the SAP attendance percentage and the fund source description	
    SET @RowsToProcess = @@ROWCOUNT;
    SET @CurrentRow = 0;
    WHILE @CurrentRow < @RowsToProcess
        BEGIN
            SET @CurrentRow = @CurrentRow + 1;
            SELECT  @eid = studentid
                   ,@fsid = fundsourceid
            FROM    @selectTable
            WHERE   RowID = @CurrentRow;
            UPDATE  @selectTable
            SET     sapattendancepercent = (
                                             SELECT TOP 1
                                                    SAPResults.Attendance
                                             FROM   dbo.arSAPChkResults SAPResults
                                             INNER JOIN arStuEnrollments Enrollments ON SAPResults.StuEnrollId = Enrollments.StuEnrollId
                                             WHERE  Enrollments.StudentId = @eid
                                             ORDER BY SAPResults.Period DESC
                                           )
                   ,fundsourcedesc = (
                                       SELECT   FundSources.FundSourceDescrip
                                       FROM     dbo.saFundSources FundSources
                                       WHERE    FundSources.FundSourceId = @fsid
                                     )
            WHERE   RowID = @CurrentRow;
        END;

    DECLARE @SettingId INT;
    SET @SettingId = (
                       SELECT TOP 1
                                SettingId
                       FROM     syConfigAppSettings
                       WHERE    KeyName LIKE 'SchoolName%'
                     );


    SELECT  *
           ,(
              SELECT TOP 1
                        Value
              FROM      syConfigAppSetValues
              WHERE     SettingId = @SettingId
            ) AS SchoolName
    FROM    @selectTable;



GO
