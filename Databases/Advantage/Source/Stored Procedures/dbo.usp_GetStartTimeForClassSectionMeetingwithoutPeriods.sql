SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_GetStartTimeForClassSectionMeetingwithoutPeriods]
    (
     @ClsSectMeetingID VARCHAR(50)
    )
AS
    BEGIN 
    
        SELECT  CONVERT(VARCHAR,CT.TimeIntervalDescrip,108)
        FROM    dbo.arClsSectMeetings CSM
               ,dbo.cmTimeInterval CT
        WHERE   CSM.ClsSectMeetingId = @ClsSectMeetingID
                AND CSM.TimeIntervalId = CT.TimeIntervalId;                
                
    END;



GO
