SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- Procedure To Drop Foreign Key Constraints
CREATE PROCEDURE [dbo].[DropForeignKeyConstraints]
AS
    DECLARE @FKConstraintName VARCHAR(100)
       ,@FKTableName VARCHAR(100)
       ,@FKColumnName VARCHAR(100);
    DECLARE @PKTableName VARCHAR(100)
       ,@PKColumnName VARCHAR(100);
    DECLARE @SQLStatement VARCHAR(500);
    DECLARE GetDDL_Cursor CURSOR
    FOR
        SELECT  KCU1.CONSTRAINT_NAME AS 'FK_CONSTRAINT_NAME'
               ,KCU1.TABLE_NAME AS 'FK_TABLE_NAME'
               ,KCU1.COLUMN_NAME AS 'FK_COLUMN_NAME'
               ,KCU2.TABLE_NAME AS 'UQ_TABLE_NAME'
               ,KCU2.COLUMN_NAME AS 'UQ_COLUMN_NAME'
        FROM    INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS RC
        JOIN    INFORMATION_SCHEMA.KEY_COLUMN_USAGE KCU1 ON KCU1.CONSTRAINT_CATALOG = RC.CONSTRAINT_CATALOG
                                                            AND KCU1.CONSTRAINT_SCHEMA = RC.CONSTRAINT_SCHEMA
                                                            AND KCU1.CONSTRAINT_NAME = RC.CONSTRAINT_NAME
        JOIN    INFORMATION_SCHEMA.KEY_COLUMN_USAGE KCU2 ON KCU2.CONSTRAINT_CATALOG = RC.UNIQUE_CONSTRAINT_CATALOG
                                                            AND KCU2.CONSTRAINT_SCHEMA = RC.UNIQUE_CONSTRAINT_SCHEMA
                                                            AND KCU2.CONSTRAINT_NAME = RC.UNIQUE_CONSTRAINT_NAME
                                                            AND KCU2.ORDINAL_POSITION = KCU1.ORDINAL_POSITION;

    OPEN GetDDL_Cursor;

    FETCH NEXT FROM GetDDL_Cursor
INTO @FKConstraintName,@FKTableName,@FKColumnName,@PKTableName,@PKColumnName;

    WHILE @@FETCH_STATUS = 0
        BEGIN
            SET @SQLStatement = '';
            SET @SQLStatement = 'Alter table ' + @FKTableName + ' drop constraint ' + @FKConstraintName;
            PRINT @SQLStatement;
            FETCH NEXT FROM GetDDL_Cursor
      INTO @FKConstraintName,@FKTableName,@FKColumnName,@PKTableName,@PKColumnName;
        END;
    CLOSE GetDDL_Cursor;
    DEALLOCATE GetDDL_Cursor;



GO
