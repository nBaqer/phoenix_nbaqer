SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_GetLeadDocsStatus]
    @FirstName VARCHAR(50)
   ,@LastName VARCHAR(50)
   ,@Email VARCHAR(50)
AS
    BEGIN  
      
        DECLARE @LeadID VARCHAR(50);  
      
        SET @LeadID = (
                        SELECT  LeadId
                        FROM    adLeads
                        WHERE   FirstName = @FirstName
                                AND LastName = @LastName
                                AND (
                                      HomeEmail = @Email
                                      OR WorkEmail = @Email
                                    )
                      );  
      
        DECLARE @CampusGrps TABLE
            (
             CampusGrpId VARCHAR(50)
            );  
        INSERT  INTO @CampusGrps
                SELECT DISTINCT
                        CampGrpId
                FROM    dbo.syCmpGrpCmps
                WHERE   CampusId IN ( SELECT TOP 1
                                                CampusId
                                      FROM      adLeads
                                      WHERE     FirstName = @FirstName
                                                AND LastName = @LastName
                                                AND (
                                                      HomeEmail = @Email
                                                      OR WorkEmail = @Email
                                                    ) );  
      
        DECLARE @LeadDocs TABLE
            (
             DocumentId VARCHAR(50)
            ,DocumentDescrip VARCHAR(50)
            ,StartDate DATETIME
            ,EndDate DATETIME
            ,DocSubmittedCount INT
            ,Required INT
            ,DocStatusDescrip VARCHAR(50)
            ,DateReceived DATETIME
            );  
      
        INSERT  INTO @LeadDocs
                SELECT  adReqId AS DocumentId
                       ,Descrip AS DocumentDescrip
                       ,StartDate
                       ,EndDate
                       ,DocSubmittedCount
                       ,Required
                       ,CASE WHEN DocStatusDescrip = 'Approved' THEN 'Approved'
                             ELSE 'Currently Due'
                        END AS DocStatusDescrip
                       ,(
                          SELECT DISTINCT
                                    ReceiveDate
                          FROM      adLeadDocsReceived
                          WHERE     DocumentId = adReqId
                                    AND LeadId = @LeadID
                        ) AS DateReceived
                FROM    (
                          SELECT  DISTINCT
                                    adReqId
                                   ,Descrip
                                   ,StartDate
                                   ,EndDate
                                   ,Required
                                   ,DocSubmittedCount
                                   ,DocStatusDescrip
                                   ,CampGrpId
                                   ,COUNT(*) OVER ( PARTITION BY adReqId ) AS DupCount
                          FROM      (
                                      SELECT DISTINCT
                                                t1.adReqId
                                               ,t1.Descrip
                                               ,GETDATE() AS CurrentDate
                                               ,t2.StartDate
                                               ,t2.EndDate
                                               ,(
                                                  SELECT    COUNT(*)
                                                  FROM      adLeadDocsReceived
                                                  WHERE     DocumentId = t1.adReqId
                                                            AND LeadId = @LeadID
                                                ) AS DocSubmittedCount
                                               ,1 AS Required
                                               ,(
                                                  SELECT DISTINCT
                                                            s1.DocStatusDescrip
                                                  FROM      sySysDocStatuses s1
                                                           ,syDocStatuses s2
                                                           ,adLeadDocsReceived s3
                                                  WHERE     s1.SysDocStatusId = s2.SysDocStatusId
                                                            AND s2.DocStatusId = s3.DocStatusId
                                                            AND s3.LeadId = @LeadID
                                                            AND s3.DocumentId = t1.adReqId
                                                ) AS DocStatusDescrip
                                               ,t1.CampGrpId
                                      FROM      adReqs t1
                                               ,adReqsEffectiveDates t2
                                      WHERE     t1.adReqId = t2.adReqId
                                                AND t1.ReqforEnrollment = 1
                                                AND t2.MandatoryRequirement = 1
                                                AND t1.adReqTypeId IN ( 3 )
                                                AND t1.StatusId = (
                                                                    SELECT TOP 1
                                                                            StatusId
                                                                    FROM    dbo.syStatuses
                                                                    WHERE   Status = 'Active'
                                                                  )
                                    ) R1
                          WHERE     R1.CurrentDate >= R1.StartDate
                                    AND (
                                          R1.CurrentDate <= R1.EndDate
                                          OR R1.EndDate IS NULL
                                        )
                          UNION
                          SELECT    adReqId
                                   ,Descrip
                                   ,StartDate
                                   ,EndDate
                                   ,Required
                                   ,DocSubmittedCount
                                   ,DocStatusDescrip
                                   ,CampGrpId
                                   ,COUNT(*) OVER ( PARTITION BY adReqId ) AS DupCount
                          FROM      (
                                      SELECT DISTINCT
                                                t1.adReqId
                                               ,t1.Descrip
                                               ,GETDATE() AS CurrentDate
                                               ,t2.StartDate
                                               ,t2.EndDate
                                               ,(
                                                  SELECT    COUNT(*)
                                                  FROM      adLeadDocsReceived
                                                  WHERE     DocumentId = t1.adReqId
                                                            AND LeadId = @LeadID
                                                ) AS DocSubmittedCount
                                               ,ISNULL(t3.IsRequired,0) AS Required
                                               ,(
                                                  SELECT DISTINCT
                                                            s1.DocStatusDescrip
                                                  FROM      sySysDocStatuses s1
                                                           ,syDocStatuses s2
                                                           ,adLeadDocsReceived s3
                                                  WHERE     s1.SysDocStatusId = s2.SysDocStatusId
                                                            AND s2.DocStatusId = s3.DocStatusId
                                                            AND s3.LeadId = @LeadID
                                                            AND s3.DocumentId = t1.adReqId
                                                ) AS DocStatusDescrip
                                               ,t1.CampGrpId
                                      FROM      adReqs t1
                                               ,adReqsEffectiveDates t2
                                               ,adReqLeadGroups t3
                                      WHERE     t1.adReqId = t2.adReqId
                                                AND t2.adReqEffectiveDateId = t3.adReqEffectiveDateId
                                                AND t1.ReqforEnrollment = 1
                                                AND t3.LeadGrpId IN ( SELECT    LeadGrpId
                                                                      FROM      adLeadByLeadGroups
                                                                      WHERE     LeadId = @LeadID )
                                                AND t1.adReqTypeId IN ( 3 )
                                                AND t2.MandatoryRequirement <> 1
                                                AND t1.StatusId = (
                                                                    SELECT TOP 1
                                                                            StatusId
                                                                    FROM    dbo.syStatuses
                                                                    WHERE   Status = 'Active'
                                                                  )
                                    ) R1
                          WHERE     R1.CurrentDate >= R1.StartDate
                                    AND (
                                          R1.CurrentDate <= R1.EndDate
                                          OR R1.EndDate IS NULL
                                        )
                        ) R2
                WHERE   (
                          (
                            DupCount > 1
                            AND Required = 1
                          )
                          OR (
                               DupCount = 1
                               AND Required IN ( 0,1 )
                             )
                        )
                        AND R2.CampGrpId IN ( SELECT DISTINCT
                                                        CampGrpId
                                              FROM      @CampusGrps )
                ORDER BY R2.Descrip;  
      
        IF (
             SELECT COUNT(*)
             FROM   @LeadDocs
             WHERE  DocStatusDescrip = 'Currently Due'
                    AND Required = 1
           ) > 0
            BEGIN  
                SELECT DISTINCT
                        DocumentDescrip
                       ,DocStatusDescrip
                FROM    @LeadDocs
                WHERE   DocStatusDescrip = 'Currently Due'
                        AND Required = 1;  
            END;  
        ELSE
            IF (
                 SELECT COUNT(*)
                 FROM   @LeadDocs
                 WHERE  DocStatusDescrip = 'Approved'
                        AND Required = 1
               ) > 0
                BEGIN  
                    SELECT DISTINCT
                            DocumentDescrip
                           ,DocStatusDescrip
                    FROM    @LeadDocs
                    WHERE   DocStatusDescrip = 'Approved'
                            AND Required = 1;  
                END;  
    END;  
      
GO
