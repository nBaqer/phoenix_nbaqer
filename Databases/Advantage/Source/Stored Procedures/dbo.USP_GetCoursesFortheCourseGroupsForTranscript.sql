SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[USP_GetCoursesFortheCourseGroupsForTranscript]
    (
     @stuEnrollid UNIQUEIDENTIFIER
    )
AS
    SET NOCOUNT ON;
    DECLARE @VariableReqtable TABLE
        (
         ReqId UNIQUEIDENTIFIER
        ,GrpID UNIQUEIDENTIFIER
        ,ID INT IDENTITY
        ); 

    INSERT  INTO @VariableReqtable
            SELECT  ReqId
                   ,GrpID
            FROM    arReqGrpDef
            WHERE   GrpID IN ( SELECT   t3.ReqId
                               FROM     arReqs t3
                               INNER JOIN arProgVerDef t4 ON t3.ReqId = t4.ReqId
                                                             AND t3.ReqTypeId = 2
                               INNER JOIN arPrgVersions t6 ON t4.Prgverid = t6.PrgVerID
                                                              AND t6.PrgVerID = (
                                                                                  SELECT    prgVerID
                                                                                  FROM      arStuEnrollments
                                                                                  WHERE     StuEnrollId = @stuEnrollid
                                                                                ) );

    DECLARE @MaxRowCounter INT
       ,@RowCounter INT;

    SET @MaxRowCounter = (
                           SELECT   MAX(ID)
                           FROM     @VariableReqtable
                         );

    SET @RowCounter = 1;

    WHILE ( @RowCounter <= @MaxRowCounter )
        BEGIN

            INSERT  INTO @VariableReqtable
                    SELECT  ReqId
                           ,GrpID
                    FROM    arReqGrpDef
                    WHERE   GrpID IN ( SELECT   ReqId
                                       FROM     @VariableReqtable
                                       WHERE    ID = @RowCounter ); 

   

            SET @RowCounter = @RowCounter + 1;

            SET @MaxRowCounter = (
                                   SELECT   MAX(ID)
                                   FROM     @VariableReqtable
                                 );

        END;
    SELECT  arReqGrpDef.GrpId
           ,arReqs.ReqId
           ,arReqs.Descrip AS Req
           ,arReqs.Code AS Code
           ,arReqs.Credits
           ,arReqs.ReqTypeId
           ,arReqGrpDef.ReqSeq
           ,arReqs.Hours
           ,(
              SELECT    PrgVerId
              FROM      arStuEnrollments
              WHERE     StuEnrollId = @stuEnrollid
            ) AS PrgVerId
           ,arReqs.FinAidCredits
           ,@stuEnrollid AS stuEnrollId
		/** New code added by kamalesh ahuja on march 08 2010 **/
           ,arReqs.Hours AS ScheduledHours
		/**/
    FROM    arReqs
    INNER JOIN arReqGrpDef ON arReqs.ReqId = arReqGrpDef.reqid
                              AND arReqs.ReqTypeId = 1
                              AND arReqs.reqid IN ( SELECT  ReqId
                                                    FROM    @VariableReqtable
                                                    UNION
                                                    SELECT  GrpID
                                                    FROM    @VariableReqtable )
    ORDER BY arReqGrpDef.ReqSeq; 



GO
