SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_BindMentorProctoredTestRequirement_GetList]
    @EffectiveDate DATETIME
   ,@ReqId UNIQUEIDENTIFIER
AS
    SELECT DISTINCT
            MentorRequirement
           ,MentorOperator
           ,CONVERT(CHAR(36),GrdComponentTypeId) + ':' + RTRIM(CONVERT(VARCHAR(20),Speed)) AS MentorValue
           ,Speed
           ,OperatorSequence
    FROM    arMentor_GradeComponentTypes_Courses
    WHERE   EffectiveDate = @EffectiveDate
            AND ReqId = @ReqId
    ORDER BY OperatorSequence;



GO
