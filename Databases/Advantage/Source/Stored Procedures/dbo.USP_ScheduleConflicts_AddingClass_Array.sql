SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[USP_ScheduleConflicts_AddingClass_Array]
    @CurrentClassSectionId UNIQUEIDENTIFIER
   ,@ClsMeetingStartDate VARCHAR(8000)
   ,@ClsMeetingEndDate VARCHAR(8000)
   ,@ClsMeetingRoomId VARCHAR(8000)
   ,@InstructorId UNIQUEIDENTIFIER
   ,@CurrentTermId UNIQUEIDENTIFIER
   ,@CurrentCourseId UNIQUEIDENTIFIER
   ,@CurrentPeriodId VARCHAR(8000)
   ,@CurrentCampusId UNIQUEIDENTIFIER
AS
    BEGIN


        DECLARE @ClassesWithOverlappingDates TABLE
            (
             CourseDescrip VARCHAR(50)
            ,ClsSectionDescrip VARCHAR(50)
            ,ClsSectionId UNIQUEIDENTIFIER
            ,RoomId UNIQUEIDENTIFIER
            ,StartDate DATETIME
            ,EndDate DATETIME
            );

        DECLARE @ClassesWithOverlappingDays TABLE
            (
             StartDate DATETIME
            ,EndDate DATETIME
            ,WorkDaysDescrip VARCHAR(50)
            ,StartTime VARCHAR(50)
            ,EndTime VARCHAR(50)
            ,ViewOrder INT
            ,ClsSection VARCHAR(50)
            ,CourseDescrip VARCHAR(50)
            ,ClsSectionId VARCHAR(50)
            ,CourseId VARCHAR(50)
            ,RoomId VARCHAR(50)
            ,InstructorId VARCHAR(50)
            );

        DECLARE @ClassesWithOverlappingTimeInterval TABLE
            (
             StartDate DATETIME
            ,EndDate DATETIME
            ,WorkDaysDescrip VARCHAR(50)
            ,StartTime VARCHAR(50)
            ,EndTime VARCHAR(50)
            ,ViewOrder INT
            ,ClsSection VARCHAR(50)
            ,CourseDescrip VARCHAR(50)
            ,ClsSectionId VARCHAR(50)
            ,CourseId VARCHAR(50)
            ,RoomId VARCHAR(50)
            ,InstructorId VARCHAR(50)
            );

        DECLARE @ClassesWithSameRoomOrInstructor TABLE
            (
             StartDate DATETIME
            ,EndDate DATETIME
            ,WorkDaysDescrip VARCHAR(50)
            ,StartTime VARCHAR(50)
            ,EndTime VARCHAR(50)
            ,ViewOrder INT
            ,ClsSection VARCHAR(50)
            ,CourseDescrip VARCHAR(50)
            ,ClsSectionId VARCHAR(50)
            ,CourseId VARCHAR(50)
            ,RoomId VARCHAR(50)
            ,InstructorId VARCHAR(50)
            );

        DECLARE @ClassMeetingStartDate TABLE
            (
             RowNumber INT IDENTITY(1,1)
            ,StartDate DATETIME
            );
        DECLARE @ClassMeetingEndDate TABLE
            (
             RowNumber INT IDENTITY(1,1)
            ,EndDate DATETIME
            );
        DECLARE @ClassMeetingRoomId TABLE
            (
             RowNumber INT IDENTITY(1,1)
            ,RoomId UNIQUEIDENTIFIER
            );
        DECLARE @ClassMeetingPeriodId TABLE
            (
             RowNumber INT IDENTITY(1,1)
            ,PeriodId UNIQUEIDENTIFIER
            );
        DECLARE @ClassMeetingArray TABLE
            (
             CurrentCourseId UNIQUEIDENTIFIER
            ,StartDate DATETIME
            ,EndDate DATETIME
            ,RoomId UNIQUEIDENTIFIER
            ,PeriodId UNIQUEIDENTIFIER
            ,InstructorId UNIQUEIDENTIFIER
            );

        INSERT  INTO @ClassMeetingStartDate
                (
                 StartDate
                )
                SELECT  Val
                FROM    MultipleValuesForReportParameters(@ClsMeetingStartDate,',',1); 
        INSERT  INTO @ClassMeetingEndDate
                (
                 EndDate
                )
                SELECT  Val
                FROM    MultipleValuesForReportParameters(@ClsMeetingEndDate,',',1);

        INSERT  INTO @ClassMeetingRoomId
                (
                 RoomId
                )
                SELECT  Val
                FROM    MultipleValuesForReportParameters(@ClsMeetingRoomId,',',1); 

        INSERT  INTO @ClassMeetingPeriodId
                (
                 PeriodId
                )
                SELECT  Val
                FROM    MultipleValuesForReportParameters(@CurrentPeriodId,',',1); 

        INSERT  INTO @ClassMeetingArray
                SELECT DISTINCT
                        @CurrentCourseId
                       ,StartDate
                       ,EndDate
                       ,RoomId
                       ,PeriodId
                       ,@InstructorId
                FROM    @ClassMeetingStartDate t1
                INNER JOIN @ClassMeetingEndDate t2 ON t1.RowNumber = t2.RowNumber
                LEFT OUTER JOIN @ClassMeetingRoomId t3 ON t2.RowNumber = t3.RowNumber
                LEFT OUTER JOIN @ClassMeetingPeriodId t4 ON t3.RowNumber = t4.RowNumber;



/**************************** Business Rules for Schedule Conflicts *************
There are 3 layers involved in this stored proc
a. Level 1 - Check if class you are trying to add to advantage via class sections page,
			 class section with periods page has the same start date/end date or 
			 overlapping date ranges.
			 Example: 
					  English Class is offered from 03/01/2012 - 05/01/2012
					  Maths Class is offered from 03/05/2012 - 05/14/2012

b. Level 2 - If a conflict is identified in Level 1, then check if 
			 both classes are scheduled on same day. It is perfectly normal
			 for two classes to fall in the same date range but on different dates.
			 So this check is neccessary.
			 Example: 
					English class (03/01/2012-05/01/2012) - Mon, Tues
					Maths Class (03/05/2012 - 05/14/2012) - Mon, Wed

			In the above cases, both classes have overlapping date ranges and 
			they both offer classes on Mon

c. Level 3 - If conflict is identified in Level 2, then check if both classes are 
			 offered at the same time or during overlapping times. Its quite normal
			 for two classes to be offered on the same day but different times 
			 (ex:day and eve classes). Also, there is a probability for 
			 two classes to have overlapping time

			 Example: 
				English class - Mon - 8AM - 1PM
				Maths Class - Mon - 8AM - 3.30PM

**************************************************************************/

--@ClsMeetingStartDate DATETIME,
--@ClsMeetingEndDate DATETIME,
--@ClsMeetingPeriodId UNIQUEIDENTIFIER,
--@InstructorId UNIQUEIDENTIFIER

        INSERT  INTO @ClassesWithOverlappingDates
                SELECT  *
                FROM    (
                          SELECT DISTINCT
                                    A1.Descrip
                                   ,A1.ClsSection
                                   ,A1.ClsSectionId
                                   ,A1.RoomId
                                   ,A1.StartDate
                                   ,A1.EndDate
                          FROM      (
                                      -- Get the class startdate and enddate of classes that already exist for this term
			-- exclude the current class
                                      SELECT DISTINCT
                                                t3.*
                                               ,t4.Descrip
                                               ,t2.ClsSection
                                               ,t2.TermId
                                      FROM      arClassSections t2
                                               ,arClsSectMeetings t3
                                               ,arReqs t4
                                      WHERE     t2.ClsSectionId = t3.ClsSectionId
                                                AND t2.ReqId = t4.ReqId
                                                AND t2.CampusId = @CurrentCampusId
                                                AND t2.ClsSectionId <> @CurrentClassSectionId
                                    ) A1
                                   ,(
                                      -- Get the class startdate and enddate that is being currently put in
         SELECT DISTINCT
                t5.StartDate AS StartDate
               ,t5.EndDate AS EndDate
               ,@ClsMeetingRoomId AS RoomId
               ,@CurrentClassSectionId AS ClsSectionId
               ,@CurrentTermId AS TermId
               ,t4.Descrip
         FROM   arReqs t4
         INNER JOIN @ClassMeetingArray t5 ON t4.ReqId = t5.CurrentCourseId
                                    ) A2
                          WHERE     A1.StartDate <= A2.EndDate
                                    AND A1.EndDate >= A2.StartDate
                                    AND A1.ClsSectionId <> A2.ClsSectionId
                                    --AND -- Different classes
                                    --A1.RoomId = A2.RoomId
                                    --AND -- Same room
                                    --A1.TermId = A2.TermId -- Same Term
                        ) dt;
-- TEST Eliminate in production
--        SELECT  *
--        FROM    @ClassesWithOverlappingDates

	-- If there are classes with overlapping days in same term
	-- Get the days on which they overlap

        INSERT  INTO @ClassesWithOverlappingDays
                SELECT DISTINCT
                        A1.StartDate
                       ,A1.EndDate
                       ,A1.WorkDaysDescrip
                       ,A1.StartTime
                       ,A1.EndTime
                       ,A1.ViewOrder
                       ,A1.ClsSection
                       ,A1.Descrip AS CourseDescrip
                       ,A1.ClsSectionId
                       ,A1.ReqId
                       ,A1.RoomId
                       ,A1.InstructorId
                FROM    (
                          SELECT DISTINCT
                                    t3.ClsSectMeetingId
                                   ,t3.StartDate
                                   ,t3.EndDate
                                   ,t4.Descrip
                                   ,t2.ClsSection
                                   ,t2.TermId
                                   ,(
                                      SELECT    CONVERT(VARCHAR,TimeIntervalDescrip,108)
                                      FROM      dbo.cmTimeInterval
                                      WHERE     TimeIntervalId = t5.StartTimeId
                                    ) AS StartTime
                                   ,(
                                      SELECT    CONVERT(VARCHAR,TimeIntervalDescrip,108)
                                      FROM      dbo.cmTimeInterval
                                      WHERE     TimeIntervalId = t5.EndTimeId
                                    ) AS EndTime
                                   ,t7.WorkDaysDescrip
                                   ,t7.ViewOrder
                                   ,t2.ClsSectionId
                                   ,t4.ReqId
                                   ,t3.RoomId
                                   ,t2.InstructorId
                          FROM      arClassSections t2
                                   ,arClsSectMeetings t3
                                   ,arReqs t4
                                   ,syPeriods t5
                                   ,dbo.syPeriodsWorkDays t6
                                   ,plWorkDays t7
                          WHERE     t2.ClsSectionId = t3.ClsSectionId
                                    AND t2.ReqId = t4.ReqId
                                    AND t3.PeriodId = t5.PeriodId
                                    AND t5.PeriodId = t6.PeriodId
                                    AND t6.WorkDayId = t7.WorkDaysId
                                    AND t2.ClsSectionId IN ( SELECT DISTINCT
                                                                    ClsSectionId
                                                             FROM   @ClassesWithOverlappingDates )
                        ) A1
                       ,(
                          SELECT DISTINCT
                                    t4.StartDate AS StartDate
                                   ,t4.EndDate AS EndDate
                                   ,t4.RoomId AS RoomId
                                   ,@CurrentClassSectionId AS ClsSectionId
                                   ,@CurrentTermId AS TermId
                                   ,t3.Descrip AS Descrip
                                   ,(
                                      SELECT    CONVERT(VARCHAR,TimeIntervalDescrip,108)
                                      FROM      dbo.cmTimeInterval
                                      WHERE     TimeIntervalId = t5.StartTimeId
                                    ) AS StartTime
                                   ,(
                                      SELECT    CONVERT(VARCHAR,TimeIntervalDescrip,108)
                                      FROM      dbo.cmTimeInterval
                                      WHERE     TimeIntervalId = t5.EndTimeId
                                    ) AS EndTime
                                   ,t7.WorkDaysDescrip
                                   ,t7.ViewOrder
                          FROM      --(SELECT * FROM syPeriods WHERE PeriodId=@CurrentPeriodId) t5, dbo.syPeriodsWorkDays t6, plWorkDays t7
                                    arReqs t3
                          INNER JOIN @ClassMeetingArray t4 ON t3.ReqId = t4.CurrentCourseId
                          INNER JOIN syPeriods t5 ON t4.PeriodId = t5.PeriodId
                          INNER JOIN syPeriodsWorkDays t6 ON t5.PeriodId = t6.PeriodId
                          INNER JOIN plWorkDays t7 ON t6.WorkDayId = t7.WorkDaysId
                        ) A2
                WHERE   A1.WorkDaysDescrip = A2.WorkDaysDescrip
                ORDER BY A1.ViewOrder
                       ,A1.ClsSection
                       ,A1.StartDate
                       ,A1.EndDate
                       ,A1.StartTime
                       ,A1.EndTime;

-- Diagnostic eliminate in production
--        SELECT  *
--        FROM    @ClassesWithOverlappingDays

    -- If classes overlap on a day check the timings to see if there is a overlap

        INSERT  INTO @ClassesWithOverlappingTimeInterval
                SELECT  A2.*
                FROM    (
                          SELECT DISTINCT
                                    t4.StartDate AS StartDate
                                   ,t4.EndDate AS EndDate
                                   ,t4.RoomId AS RoomId
                                   ,@CurrentClassSectionId AS ClsSectionId
                                   ,@CurrentTermId AS TermId
                                   ,t3.Descrip AS Descrip
                                   ,(
                                      SELECT    CONVERT(VARCHAR,TimeIntervalDescrip,108)
                                      FROM      dbo.cmTimeInterval
                                      WHERE     TimeIntervalId = t5.StartTimeId
                                    ) AS StartTime
                                   ,(
                                      SELECT    CONVERT(VARCHAR,TimeIntervalDescrip,108)
                                      FROM      dbo.cmTimeInterval
                                      WHERE     TimeIntervalId = t5.EndTimeId
                                    ) AS EndTime
                                   ,t7.WorkDaysDescrip
                                   ,t7.ViewOrder
                          FROM      --(SELECT * FROM syPeriods WHERE PeriodId=@CurrentPeriodId) t5, dbo.syPeriodsWorkDays t6, plWorkDays t7
                                    arReqs t3
                          INNER JOIN @ClassMeetingArray t4 ON t3.ReqId = t4.CurrentCourseId
                          INNER JOIN arClsSectMeetings m ON t4.PeriodId = m.PeriodId
                          INNER JOIN syPeriods t5 ON t4.PeriodId = t5.PeriodId
                          INNER JOIN syPeriodsWorkDays t6 ON t5.PeriodId = t6.PeriodId
                          INNER JOIN plWorkDays t7 ON t6.WorkDayId = t7.WorkDaysId
                        ) A1
                       ,@ClassesWithOverlappingDays A2
                WHERE   --(A1.StartDate>=A2.StartDate OR A1.StartDate <= A2.EndDate) AND
                        (
                          A1.StartDate <= A2.EndDate
                          AND A1.EndDate >= A2.StartDate
                        )
                        AND ( A1.WorkDaysDescrip = A2.WorkDaysDescrip )
                        AND (
                              A1.StartTime < A2.EndTime
                              AND A1.EndTime > A2.StartTime
                            );

  
  --We have overlapping times so we want to check if
  -------(1) We have another meeting for the same room
						--OR
  -------(2) We have the instructor scheduled for another meeting
        INSERT  INTO @ClassesWithSameRoomOrInstructor
                SELECT  *
                FROM    @ClassesWithOverlappingTimeInterval
                WHERE   (
                          RoomId = @ClsMeetingRoomId
                          OR InstructorId = @InstructorId
                        );




  --Return Results
        SELECT DISTINCT
                CourseId
               ,CourseDescrip
               ,ClsSectionId
               ,ClsSection
               ,CONVERT(VARCHAR,StartDate,101) + '- ' + CONVERT(VARCHAR,EndDate,101) AS ClassPeriod
               ,WorkDaysDescrip + ' (' + StartTime + ' - ' + EndTime + ')' AS MeetingSchedule
               ,ViewOrder
               ,StartDate
               ,EndDate
        FROM    @ClassesWithSameRoomOrInstructor
        ORDER BY CourseDescrip
               ,ClsSection
               ,StartDate
               ,EndDate
               ,ViewOrder;
 
    END;


GO
