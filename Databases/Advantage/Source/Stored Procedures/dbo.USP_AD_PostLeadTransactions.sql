SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--Select * from adLeads where firstname='Robert' and lastname='Baker'
--select * from adLeadTransactions where LeadId='CB5AA904-6F20-4A61-AD40-1D5F103AE556'

CREATE PROCEDURE [dbo].[USP_AD_PostLeadTransactions]
    @LeadTransactionId UNIQUEIDENTIFIER
   ,@LeadId UNIQUEIDENTIFIER
   ,@TransCodeId UNIQUEIDENTIFIER
   ,@TransReference VARCHAR(50)
   ,@TransDescrip VARCHAR(50)
   ,@TransAmount DECIMAL(19,4)
   ,@TransTypeId INT
   ,@IsEnrolled BIT
   ,@TransDate DATETIME
   ,@IsVoided BIT
   ,@CampusId UNIQUEIDENTIFIER
   ,@PaymentTransactionId UNIQUEIDENTIFIER
   ,@PaymentTypeId INT
   ,@CheckNumber VARCHAR(50)
   ,@CreatedDate DATETIME
   ,@ModUser VARCHAR(50)
   ,@ModDate DATETIME
   ,@PaymentReference INT OUTPUT
AS
    BEGIN
		
        BEGIN TRY
	
            BEGIN TRANSACTION;
		
            DECLARE @DisplaySequence INT
               ,@SecondDisplaySequence INT;
            SET @DisplaySequence = (
                                     SELECT MAX(ISNULL(DisplaySequence,0))
                                     FROM   adLeadTransactions
                                   );
            SET @DisplaySequence = @DisplaySequence + 1;
            SET @SecondDisplaySequence = @DisplaySequence + 1;
	
		-- insert applicant fee transaction		
            INSERT  INTO dbo.adLeadTransactions
                    (
                     TransactionId
                    ,LeadId
                    ,TransCodeId
                    ,TransReference
                    ,TransDescrip
                    ,TransAmount
                    ,TransDate
                    ,TransTypeId
                    ,isEnrolled
                    ,CreatedDate
                    ,Voided
                    ,ModUser
                    ,ModDate
                    ,CampusId
                    ,DisplaySequence
                    ,SecondDisplaySequence
		            )
            VALUES  (
                     @LeadTransactionId
                    ,@LeadId
                    ,@TransCodeId
                    ,@TransReference
                    ,@TransDescrip
                    ,@TransAmount
                    ,@TransDate
                    ,@TransTypeId
                    ,@IsEnrolled
                    ,@CreatedDate
                    ,@IsVoided
                    ,@ModUser
                    ,@ModDate
                    ,@CampusId
                    ,@DisplaySequence
                    ,@SecondDisplaySequence
				    );
		
            SET @DisplaySequence = (
                                     SELECT MAX(ISNULL(DisplaySequence,0))
                                     FROM   adLeadTransactions
                                   );
		--Set @DisplaySequence =(select Top 1 IsNULL(DisplaySequence,0) from adLeadTransactions where TransactionId=(Select Top 1 MapTransactionId from adLeadTransactions where TransactionId=@TransactionId))
            SET @DisplaySequence = @DisplaySequence + 1;
            SET @SecondDisplaySequence = @DisplaySequence + 1;
		
		-- insert payment transaction		
            INSERT  INTO dbo.adLeadTransactions
                    (
                     TransactionId
                    ,LeadId
                    ,TransCodeId
                    ,TransReference
                    ,TransDescrip
                    ,TransAmount
                    ,TransDate
                    ,TransTypeId
                    ,isEnrolled
                    ,CreatedDate
                    ,Voided
                    ,ModUser
                    ,ModDate
                    ,CampusId
                    ,DisplaySequence
                    ,SecondDisplaySequence
                    ,MapTransactionId
		            )
            VALUES  (
                     @PaymentTransactionId
                    ,@LeadId
                    ,@TransCodeId
                    ,@TransReference
                    ,@TransDescrip
                    ,( @TransAmount * -1 )
                    ,@TransDate
                    ,2
                    ,@IsEnrolled
                    ,@CreatedDate
                    ,@IsVoided
                    ,@ModUser
                    ,@ModDate
                    ,@CampusId
                    ,@DisplaySequence
                    ,@DisplaySequence
                    ,@LeadTransactionId
				    );
				 
            INSERT  INTO dbo.adLeadPayments
                    (
                     TransactionId
                    ,PaymentTypeId
                    ,CheckNumber
                    ,ModUser
                    ,ModDate
		            )
            VALUES  (
                     @PaymentTransactionId
                    ,@PaymentTypeId
                    ,@CheckNumber
                    ,@ModUser
                    ,@ModDate
		            );
		        
            COMMIT TRANSACTION;
		
            SET @PaymentReference = (
                                      SELECT    PaymentReference
                                      FROM      dbo.adLeadPayments
                                      WHERE     TransactionId = @PaymentTransactionId
                                    );
		     			  
        END TRY
		
        BEGIN CATCH
	
            ROLLBACK TRANSACTION;
		
        END CATCH;	  
    END;



GO
