SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[USP_AR_HasStudentAttemptedReqswithCoReqs]
    (
     @StuEnrollId UNIQUEIDENTIFIER
    ,@CoReqID UNIQUEIDENTIFIER
    )
AS /*----------------------------------------------------------------------------------------------------
	Author          :	Saraswathi Lakshmanan
    
    Create date		:	02/03/2010
    
	Procedure Name	:	USP_AR_HasStudentAttemptedReqswithCoReqs

	Objective		:	find if the Student has attempted the Reqs and the prereqs
	
	Parameters		:	Name			Type	Data Type	Required? 	
						=====			====	=========	=========	
						@StuEnrollId	In		Varchar		Required	
						@CoReqID		In		varChar		Required
	
	Output			:	Returns the rowcount					
						
*/-----------------------------------------------------------------------------------------------------

    BEGIN
	
        SELECT  SUM(Num)
        FROM    (
                  SELECT    COALESCE((
                                       SELECT   COUNT(*) AS Count
                                       FROM     arResults a
                                       WHERE    a.StuEnrollId IN ( SELECT   StuEnrollId
                                                                   FROM     arStuEnrollments
                                                                   WHERE    StudentId IN ( SELECT   StudentId
                                                                                           FROM     arStuEnrollments
                                                                                           WHERE    StuEnrollId = @StuEnrollId ) )
                                                AND a.TestId IN ( SELECT    c.ClsSectionId
                                                                  FROM      arClassSections c
                                                                  WHERE     c.ReqId = @CoReqID )
                                     ),0) AS Num
                  UNION ALL
                  SELECT    COALESCE((
                                       SELECT   COUNT(*) AS Count
                                       FROM     arTransferGrades t9
                                       WHERE    t9.StuEnrollId = @StuEnrollId
                                                AND t9.ReqId = @CoReqID
                                     ),0) AS Num
                ) R1; 

    END;



GO
