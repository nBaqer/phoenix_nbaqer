SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_SystemComponent_GetSingle]
    @GrdComponentTypeId VARCHAR(36)
   ,@returnvalue INT OUTPUT
AS
    SET @returnvalue = (
                         SELECT TOP 1
                                SysComponentTypeId
                         FROM   arGrdComponentTypes
                         WHERE  GrdComponentTypeId = @GrdComponentTypeId
                       );
    IF @returnvalue > 0
        BEGIN
            SET @returnvalue = @returnvalue;
        END; 
    ELSE
        BEGIN
            SET @returnvalue = 0;
        END;



GO
