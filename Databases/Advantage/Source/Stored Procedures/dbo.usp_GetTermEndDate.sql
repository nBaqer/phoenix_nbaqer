SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_GetTermEndDate]
    (
     @termId UNIQUEIDENTIFIER
    )
AS
    SET NOCOUNT ON; 
    SELECT  Enddate
    FROM    arTerm
    WHERE   TermId = @termId;
    
    
    



GO
