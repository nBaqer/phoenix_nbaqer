SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Jose Torres
-- Create date: 11/15/2014
-- Description: Update selected PrgVersion with BillingMethodId
--
-- EXECUTE USP_PrgVersionsWithBillingMethod_Update 
--                  @strXML = '<NewDataSet><BMPV BMPVId="3565e019-0f55-4914-8acd-b4107372abde" BillingMehodId="3ff5b744-1724-4c12-b043-c2c7e12c564d" PrgVerId="086f6e36-022d-4288-8cbc-03f4bf4c69e4" />
--                             <BMPV BMPVId="2565e019-0f55-4914-8acd-b4107372abde" BillingMehodId="3ff5b744-1724-4c12-b043-c2c7e12c564d" PrgVerId="386f6e36-022d-4288-8cbc-03f4bf4c69e4" /></NewDataSet>' 
--                  @BillingMethodID = N'F335D044-F05B-4F4F-8590-39552C6FDC3F'
-- =============================================

CREATE PROCEDURE [dbo].[USP_PrgVersionsWithBillingMethod_Update]
    @strXML NTEXT
   ,@BillingMethodID NVARCHAR(50)
AS
    BEGIN
        DECLARE @hDoc INT;
        DECLARE @temTable AS TABLE
            (
             BMPVId VARCHAR(50)
            ,BillingMehodId VARCHAR(50)
            ,PrgVerId VARCHAR(50)
            );
	
	--Prepare input values as an XML document
        EXEC sp_xml_preparedocument @hDoc OUTPUT,@strXML;
	
        INSERT  INTO @temTable
                SELECT  BMPVId
                       ,BillingMehodId
                       ,PrgVerId
                FROM    OPENXML(@hDoc,'/NewDataSet/BMPV',1) 
	WITH (BMPVId VARCHAR(50), BillingMehodId VARCHAR(50), PrgVerId VARCHAR(50));

	-- BillingMethodID = Null for  all records that was un marked from the list    
        UPDATE  APV
        SET     APV.BillingMethodId = NULL
	--SELECT * 
        FROM    arPrgVersions AS APV
        LEFT OUTER JOIN @temTable AS TT ON APV.PrgVerId = TT.PrgVerId
                                           AND APV.BillingMethodId = TT.BillingMehodId
        WHERE   APV.BillingMethodId = @BillingMethodID
                AND TT.BillingMehodId IS NULL;

	-- BillingMethodID = TT.BillingMethodID all records in the list that BillingMenthodId are Null
        UPDATE  APV
        SET     APV.BillingMethodId = TT.BillingMehodId
	--SELECT *
        FROM    arPrgVersions AS APV
        INNER JOIN @temTable AS TT ON APV.PrgVerId = TT.PrgVerId
        WHERE   APV.BillingMethodId IS NULL;

        EXEC sp_xml_removedocument @hDoc;
    END;





GO
