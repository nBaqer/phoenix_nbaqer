SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_SAPrequirementtext_getlist]
    @SAPDetailId UNIQUEIDENTIFIER
AS
    SELECT DISTINCT
            GrdComponentTypeId
           ,CONVERT(CHAR(36),GrdComponentTypeId) + ':' + RTRIM(CONVERT(VARCHAR(20),Speed)) AS MentorReq
           ,RTRIM(CONVERT(VARCHAR(10),Speed)) + ' wpm in ' + (
                                                               SELECT   Descrip
                                                               FROM     arGrdComponentTypes
                                                               WHERE    GrdComponentTypeId = t1.GrdComponentTypeId
                                                             ) AS MentorRequirement
           ,Operator AS MentorOperator
           ,Speed
           ,MentorOperatorOrder = CASE Operator
                                    WHEN 'Select' THEN 0
                                    WHEN 'And' THEN 1
                                    WHEN 'Or' THEN 2
                                    ELSE 3
                                  END
           ,OperatorSequence
    FROM    arSAP_ShortHandSkillRequirement t1
    WHERE   SAPDetailId = @SAPDetailId
    ORDER BY OperatorSequence ASC
           ,Speed ASC;



GO
