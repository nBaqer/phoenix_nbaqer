SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--=================================================================================================
-- USP_GenerateStudentSequence
--=================================================================================================
CREATE PROCEDURE [dbo].[USP_GenerateStudentSequence]
AS
    BEGIN
        SET NOCOUNT ON;
        DECLARE @stuId INT;
        SET @stuId = (
                     SELECT ISNULL(MAX(Student_SeqID), 0) AS Student_SeqID
                     FROM   syGenerateStudentID
                     ) + 1;
        INSERT INTO syGenerateStudentID (
                                        Student_SeqID
                                       ,ModDate
                                        )
        VALUES ( @stuId, GETDATE());
        SELECT @stuId AS SeqId;
    END;
--=================================================================================================
-- END  --  USP_GenerateStudentSequence
--=================================================================================================
GO
