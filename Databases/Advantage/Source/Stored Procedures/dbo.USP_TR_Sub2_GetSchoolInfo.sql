SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[USP_TR_Sub2_GetSchoolInfo]
    @StuEnrollIdList VARCHAR(MAX) = NULL
AS
    BEGIN
        SELECT  SC.SchoolName
               ,SC.CampusId
               ,SC.CampDescrip AS CampusName
               ,SC.CampCode AS CampusCode
               ,SC.Address1
               ,SC.Address2
               ,SC.City
               ,ISNULL(SS.StateDescrip,'') AS State
               ,ISNULL(SS.StateCode,'') AS StateCode
               ,SC.Zip AS ZIP
               ,ISNULL(AC.CountryDescrip,'') AS Country
               ,SC.Phone1
               ,SC.Phone2
               ,SC.Phone3
               ,SC.Fax
               ,SC.Website
        FROM    syCampuses AS SC
        INNER JOIN arStuEnrollments AS ASE ON ASE.CampusId = SC.CampusId
        LEFT JOIN adCountries AS AC ON AC.CountryId = SC.CountryId
        LEFT JOIN syStates AS SS ON SS.StateId = SC.StateId
        WHERE   ASE.StuEnrollId IN ( SELECT Val
                                     FROM   MultipleValuesForReportParameters(@StuEnrollIdList,',',1) );

    END;

GO
