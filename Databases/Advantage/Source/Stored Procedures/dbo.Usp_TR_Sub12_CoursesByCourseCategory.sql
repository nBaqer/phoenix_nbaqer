SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =========================================================================================================
-- Usp_TR_Sub12_CoursesByCourseCategory
-- =========================================================================================================
--exec Usp_TR_Sub12_CoursesByCourseCategory '421D28C4-5577-43AB-BEB5-348111782763,F90AE025-CF1A-4921-A874-B8622745835C,2B77DD79-0CF4-4D89-B9CB-CE586CE574A9'
CREATE PROCEDURE [dbo].[Usp_TR_Sub12_CoursesByCourseCategory]
    @StuEnrollIdList VARCHAR(MAX)
   ,@TermId VARCHAR(50) = NULL
AS --''''''''''''''''''''''''''''' This section of the stored proc calculates the GPA '''''''''''''''''''''''''''''''
--''''''''''''''''''''''''''''' Term GPA and Program version GPA '''''''''''''''''''''''''''''''''''''''''''''''''
    BEGIN

		
        DECLARE @CumulativeGPA_Range DECIMAL(18,2)
           ,@GradeCourseRepetitionsMethod VARCHAR(50);
        DECLARE @cumSimpleCourseCredits DECIMAL(18,2)
           ,@cumSimple_GPA_Credits DECIMAL(18,2)
           ,@cumSimpleGPA DECIMAL(18,2)
           ,@cumSimpleCourseCredits_OverAll DECIMAL(18,2)
           ,@cumSimple_GPA_Credits_OverAll DECIMAL(18,2)
           ,@cumSimpleGPA_OverAll DECIMAL(18,2);
        DECLARE @times INT
           ,@counter INT
           ,@StudentId VARCHAR(50)
           ,@StuEnrollId VARCHAR(50);
        DECLARE @cumCourseCredits DECIMAL(18,2)
           ,@cumWeighted_GPA_Credits DECIMAL(18,2)
           ,@cumWeightedGPA DECIMAL(18,2);
        DECLARE @EquivCourse_SA_CC INT
           ,@EquivCourse_SA_GPA DECIMAL(18,2)
           ,@EquivCourse_SA_CC_OverAll INT
           ,@EquivCourse_SA_GPA_OverAll DECIMAL(18,2);

        CREATE TABLE #CoursesNotRepeated
            (
             StuEnrollId UNIQUEIDENTIFIER
            ,TermId UNIQUEIDENTIFIER
            ,TermDescrip VARCHAR(100)
            ,ReqId UNIQUEIDENTIFIER
            ,ReqDescrip VARCHAR(100)
            ,ClsSectionId VARCHAR(50)
            ,CreditsEarned DECIMAL(18,2)
            ,CreditsAttempted DECIMAL(18,2)
            ,CurrentScore DECIMAL(18,2)
            ,CurrentGrade VARCHAR(10)
            ,FinalScore DECIMAL(18,2)
            ,FinalGrade VARCHAR(10)
            ,Completed BIT
            ,FinalGPA DECIMAL(18,2)
            ,Product_WeightedAverage_Credits_GPA DECIMAL(18,2)
            ,Count_WeightedAverage_Credits DECIMAL(18,2)
            ,Product_SimpleAverage_Credits_GPA DECIMAL(18,2)
            ,Count_SimpleAverage_Credits DECIMAL(18,2)
            ,ModUser VARCHAR(50)
            ,ModDate DATETIME
            ,TermGPA_Simple DECIMAL(18,2)
            ,TermGPA_Weighted DECIMAL(18,2)
            ,coursecredits DECIMAL(18,2)
            ,CumulativeGPA DECIMAL(18,2)
            ,CumulativeGPA_Simple DECIMAL(18,2)
            ,FACreditsEarned DECIMAL(18,2)
            ,Average DECIMAL(18,2)
            ,CumAverage DECIMAL(18,2)
            ,TermStartDate DATETIME
            ,rownumber INT NULL
            ,StudentId UNIQUEIDENTIFIER
            );

        SET @GradeCourseRepetitionsMethod = (
                                              SELECT    Value
                                              FROM      dbo.syConfigAppSetValues t1
                                              INNER JOIN dbo.syConfigAppSettings t2 ON t1.SettingId = t2.SettingId
                                                                                       AND t2.KeyName = 'GradeCourseRepetitionsMethod'
                                            );

        SET @cumSimpleGPA = 0;


		  --SELECT  *
    --                   ,0 AS rownumber
    --                   ,( SELECT TOP 1
    --                                StudentId
    --                      FROM      arStuEnrollments
    --                      WHERE     StuEnrollId = dbo.syCreditSummary.StuEnrollId
    --                    ) AS StudentId
    --            FROM    syCreditSummary
    --            WHERE   StuEnrollId IN (Select Val from [MultipleValuesForReportParameters](@StuEnrollIdList,',',1))
				--		AND (FinalScore IS NOT NULL OR FinalGrade IS NOT NULL)
    --                    AND ReqId IN ( SELECT   ReqId
    --                                   FROM     ( SELECT   ReqId
    --                                                       ,ReqDescrip
    --                                                       --,TermId
    --                                                       ,StuEnrollId
    --                                                       ,COUNT(*) AS counter
    --                                              FROM      syCreditSummary
    --                                              WHERE     StuEnrollId IN (Select Val from [MultipleValuesForReportParameters](@StuEnrollIdList,',',1))
    --                                              GROUP BY  ReqId
    --                                                       ,ReqDescrip
    --                                                       --,TermId
    --                                                       ,StuEnrollId
    --                                              HAVING    COUNT(*) = 1
    --                                            ) dt );


		

        CREATE TABLE #getStudentGPAbyTerms
            (
             StuEnrollId UNIQUEIDENTIFIER
            ,TermId UNIQUEIDENTIFIER
            ,TermDescrip VARCHAR(50)
            ,TermCredits DECIMAL(18,2) NULL
            ,TermSimpleGPA DECIMAL(18,2) NULL
            ,TermWeightedGPA DECIMAL(18,2) NULL
            ,TermStartDate DATETIME
            );

        INSERT  INTO #getStudentGPAbyTerms
                SELECT DISTINCT
                        StuEnrollId
                       ,T.TermId
                       ,T.TermDescrip
                       ,NULL
                       ,NULL
                       ,NULL
                       ,T.StartDate
                FROM    arResults R
                INNER JOIN arClassSections CS ON R.TestId = CS.ClsSectionId
                INNER JOIN arTerm T ON CS.TermId = T.TermId
                WHERE   R.StuEnrollId IN ( SELECT   Val
                                           FROM     MultipleValuesForReportParameters(@StuEnrollIdList,',',1) )
                UNION
                SELECT DISTINCT
                        StuEnrollId
                       ,T.TermId
                       ,T.TermDescrip
                       ,NULL
                       ,NULL
                       ,NULL
                       ,T.StartDate
                FROM    arTransferGrades TG
                INNER JOIN arTerm T ON TG.TermId = T.TermId
                WHERE   TG.StuEnrollId IN ( SELECT  Val
                                            FROM    MultipleValuesForReportParameters(@StuEnrollIdList,',',1) );

		--SELECT * FROM #getStudentGPAbyTerms WHERE StuEnrollID='95094539-B06E-4323-9F1C-AC1EF0B22F2F'

        CREATE TABLE #getStudentGPAByProgramVersion
            (
             LastName VARCHAR(50)
            ,FirstName VARCHAR(50)
            ,StudentId UNIQUEIDENTIFIER
            ,StudentNumber VARCHAR(50)
            ,StuEnrollID UNIQUEIDENTIFIER
            ,PrgVerId UNIQUEIDENTIFIER
            ,ProgramVersion VARCHAR(50)
            ,ProgramVersion_SimpleGPA DECIMAL(18,2)
            ,ProgramVersion_WeightedGPA DECIMAL(18,2)
            ,OverAll_SimpleGPA DECIMAL(18,2)
            ,OverAll_WeightedGPA DECIMAL(18,2)
            ,SSN VARCHAR(11)
            ,EnrollmentID VARCHAR(50)
            ,MiddleName VARCHAR(50)
            ,TermId UNIQUEIDENTIFIER
            ,TermDescription VARCHAR(50)
            ,rowNumber INT
            );

        INSERT  INTO #getStudentGPAByProgramVersion
                SELECT DISTINCT
                        S.LastName
                       ,S.FirstName
                       ,S.StudentId
                       ,S.StudentNumber
                       ,SE.StuEnrollId
                       ,PV.PrgVerId
                       ,PV.PrgVerDescrip
                       ,NULL AS ProgramVersion_SimpleGPA
                       ,NULL AS ProgramVersion_WeightedGPA
                       ,NULL AS OverAll_SimpleGPA
                       ,NULL AS OverAll_WeightedGPA
                       ,S.SSN
                       ,SE.EnrollmentId
                       ,S.MiddleName
                       ,GST.TermId
                       ,GST.TermDescrip
                       ,ROW_NUMBER() OVER ( PARTITION BY SE.StuEnrollId ORDER BY GST.TermStartDate DESC ) AS RowNumber
                FROM    arStudent S
                INNER JOIN arStuEnrollments SE ON S.StudentId = SE.StudentId
                INNER JOIN dbo.arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                INNER JOIN #getStudentGPAbyTerms GST ON GST.StuEnrollId = SE.StuEnrollId
                LEFT OUTER JOIN dbo.adLeadByLeadGroups LLG ON SE.StuEnrollId = LLG.StuEnrollId
                WHERE   SE.StuEnrollId IN ( SELECT  Val
                                            FROM    MultipleValuesForReportParameters(@StuEnrollIdList,',',1) );
						



 
        INSERT  INTO #CoursesNotRepeated
                SELECT  StuEnrollId
                       ,TermId
                       ,TermDescrip
                       ,ReqId
                       ,ReqDescrip
                       ,ClsSectionId
                       ,CreditsEarned
                       ,CreditsAttempted
                       ,CurrentScore
                       ,CurrentGrade
                       ,FinalScore
                       ,FinalGrade
                       ,Completed
                       ,FinalGPA
                       ,Product_WeightedAverage_Credits_GPA
                       ,Count_WeightedAverage_Credits
                       ,Product_SimpleAverage_Credits_GPA
                       ,Count_SimpleAverage_Credits
                       ,ModUser
                       ,ModDate
                       ,TermGPA_Simple
                       ,TermGPA_Weighted
                       ,coursecredits
                       ,CumulativeGPA
                       ,CumulativeGPA_Simple
                       ,FACreditsEarned
                       ,Average
                       ,CumAverage
                       ,TermStartDate
                       ,0 AS rownumber
                       ,(
                          SELECT TOP 1
                                    StudentId
                          FROM      arStuEnrollments
                          WHERE     StuEnrollId = dbo.syCreditSummary.StuEnrollId
                        ) AS StudentId
                FROM    syCreditSummary
                WHERE   StuEnrollId IN ( SELECT DISTINCT
                                                StuEnrollID
                                         FROM   #getStudentGPAByProgramVersion )
                        AND (
                              FinalScore IS NOT NULL
                              OR FinalGrade IS NOT NULL
                            )
                        AND ReqId IN ( SELECT   ReqId
                                       FROM     (
                                                  SELECT    ReqId
                                                           ,ReqDescrip
                                                           --,TermId
                                                           ,StuEnrollId
                                                           ,COUNT(*) AS counter
                                                  FROM      syCreditSummary
                                                  WHERE     StuEnrollId IN ( SELECT DISTINCT
                                                                                    StuEnrollID
                                                                             FROM   #getStudentGPAByProgramVersion )
                                                  GROUP BY  ReqId
                                                           ,ReqDescrip
                                                           --,TermId
                                                           ,StuEnrollId
                                                  HAVING    COUNT(*) = 1
                                                ) dt );

		--Select * from #CoursesNotRepeated where ReqId='485FBB54-202D-4A46-A353-3F9C76A275E4'
		--Select * from syCreditSummary

        IF LOWER(@GradeCourseRepetitionsMethod) = 'latest'
            BEGIN
                INSERT  INTO #CoursesNotRepeated
                        SELECT  dt2.StuEnrollId
                               ,dt2.TermId
                               ,dt2.TermDescrip
                               ,dt2.ReqId
                               ,dt2.ReqDescrip
                               ,dt2.ClsSectionId
                               ,dt2.CreditsEarned
                               ,dt2.CreditsAttempted
                               ,dt2.CurrentScore
                               ,dt2.CurrentGrade
                               ,dt2.FinalScore
                               ,dt2.FinalGrade
                               ,dt2.Completed
                               ,dt2.FinalGPA
                               ,dt2.Product_WeightedAverage_Credits_GPA
                               ,dt2.Count_WeightedAverage_Credits
                               ,dt2.Product_SimpleAverage_Credits_GPA
                               ,dt2.Count_SimpleAverage_Credits
                               ,dt2.ModUser
                               ,dt2.ModDate
                               ,dt2.TermGPA_Simple
                               ,dt2.TermGPA_Weighted
                               ,dt2.coursecredits
                               ,dt2.CumulativeGPA
                               ,dt2.CumulativeGPA_Simple
                               ,dt2.FACreditsEarned
                               ,dt2.Average
                               ,dt2.CumAverage
                               ,dt2.TermStartDate
                               ,dt2.RowNumber
                               ,dt2.StudentId
                        FROM    (
                                  SELECT    StuEnrollId
                                           ,TermId
                                           ,TermDescrip
                                           ,ReqId
                                           ,ReqDescrip
                                           ,ClsSectionId
                                           ,CreditsEarned
                                           ,CreditsAttempted
                                           ,CurrentScore
                                           ,CurrentGrade
                                           ,FinalScore
                                           ,FinalGrade
                                           ,Completed
                                           ,FinalGPA
                                           ,Product_WeightedAverage_Credits_GPA
                                           ,Count_WeightedAverage_Credits
                                           ,Product_SimpleAverage_Credits_GPA
                                           ,Count_SimpleAverage_Credits
                                           ,ModUser
                                           ,ModDate
                                           ,TermGPA_Simple
                                           ,TermGPA_Weighted
                                           ,coursecredits
                                           ,CumulativeGPA
                                           ,CumulativeGPA_Simple
                                           ,FACreditsEarned
                                           ,Average
                                           ,CumAverage
                                           ,TermStartDate
                                           ,ROW_NUMBER() OVER ( PARTITION BY ReqId ORDER BY TermStartDate DESC ) AS RowNumber
                                           ,(
                                              SELECT TOP 1
                                                        StudentId
                                              FROM      arStuEnrollments
                                              WHERE     StuEnrollId = dbo.syCreditSummary.StuEnrollId
                                            ) AS StudentId
                                  FROM      syCreditSummary
                                  WHERE     StuEnrollId IN ( SELECT StuEnrollID
                                                             FROM   #getStudentGPAByProgramVersion )
                                            AND (
                                                  FinalScore IS NOT NULL
                                                  OR FinalGrade IS NOT NULL
                                                )
                                            AND ReqId IN ( SELECT   ReqId
                                                           FROM     (
                                                                      SELECT    ReqId
                                                                               ,ReqDescrip
                                                               --,TermId
                                                                               ,StuEnrollId
                                                                               ,COUNT(*) AS Counter
                                                                      FROM      syCreditSummary
                                                                      WHERE     StuEnrollId IN ( SELECT StuEnrollID
                                                                                                 FROM   #getStudentGPAByProgramVersion )
                                                                      GROUP BY  ReqId
                                                                               ,ReqDescrip
                                           --,TermId
                                                                               ,StuEnrollId
                                                                      HAVING    COUNT(*) > 1
                                                                    ) dt )
                                ) dt2
                        WHERE   RowNumber = 1
                        ORDER BY TermStartDate DESC;
            END;

        IF LOWER(@GradeCourseRepetitionsMethod) = 'best'
            BEGIN
                INSERT  INTO #CoursesNotRepeated
                        SELECT  dt2.StuEnrollId
                               ,dt2.TermId
                               ,dt2.TermDescrip
                               ,dt2.ReqId
                               ,dt2.ReqDescrip
                               ,dt2.ClsSectionId
                               ,dt2.CreditsEarned
                               ,dt2.CreditsAttempted
                               ,dt2.CurrentScore
                               ,dt2.CurrentGrade
                               ,dt2.FinalScore
                               ,dt2.FinalGrade
                               ,dt2.Completed
                               ,dt2.FinalGPA
                               ,dt2.Product_WeightedAverage_Credits_GPA
                               ,dt2.Count_WeightedAverage_Credits
                               ,dt2.Product_SimpleAverage_Credits_GPA
                               ,dt2.Count_SimpleAverage_Credits
                               ,dt2.ModUser
                               ,dt2.ModDate
                               ,dt2.TermGPA_Simple
                               ,dt2.TermGPA_Weighted
                               ,dt2.coursecredits
                               ,dt2.CumulativeGPA
                               ,dt2.CumulativeGPA_Simple
                               ,dt2.FACreditsEarned
                               ,dt2.Average
                               ,dt2.CumAverage
                               ,dt2.TermStartDate
                               ,dt2.RowNumber
                               ,dt2.StudentId
                        FROM    (
                                  SELECT    StuEnrollId
                                           ,TermId
                                           ,TermDescrip
                                           ,ReqId
                                           ,ReqDescrip
                                           ,ClsSectionId
                                           ,CreditsEarned
                                           ,CreditsAttempted
                                           ,CurrentScore
                                           ,CurrentGrade
                                           ,FinalScore
                                           ,FinalGrade
                                           ,Completed
                                           ,FinalGPA
                                           ,Product_WeightedAverage_Credits_GPA
                                           ,Count_WeightedAverage_Credits
                                           ,Product_SimpleAverage_Credits_GPA
                                           ,Count_SimpleAverage_Credits
                                           ,ModUser
                                           ,ModDate
                                           ,TermGPA_Simple
                                           ,TermGPA_Weighted
                                           ,coursecredits
                                           ,CumulativeGPA
                                           ,CumulativeGPA_Simple
                                           ,FACreditsEarned
                                           ,Average
                                           ,CumAverage
                                           ,TermStartDate
                                           ,ROW_NUMBER() OVER ( PARTITION BY ReqId ORDER BY FinalGPA DESC ) AS RowNumber
                                           ,(
                                              SELECT TOP 1
                                                        StudentId
                                              FROM      arStuEnrollments
                                              WHERE     StuEnrollId = dbo.syCreditSummary.StuEnrollId
                                            ) AS StudentId
                                  FROM      syCreditSummary
                                  WHERE     StuEnrollId IN ( SELECT StuEnrollID
                                                             FROM   #getStudentGPAByProgramVersion )
                                            AND (
                                                  FinalScore IS NOT NULL
                                                  OR FinalGrade IS NOT NULL
                                                )
                                            AND ReqId IN ( SELECT   ReqId
                                                           FROM     (
                                                                      SELECT    ReqId
                                                                               ,ReqDescrip
                                                               --,TermId
                                                                               ,StuEnrollId
                                                                               ,COUNT(*) AS Counter
                                                                      FROM      syCreditSummary
                                                                      WHERE     StuEnrollId IN ( SELECT StuEnrollID
                                                                                                 FROM   #getStudentGPAByProgramVersion )
                                                                      GROUP BY  ReqId
                                                                               ,ReqDescrip
                                                               --,TermId
                                                                               ,StuEnrollId
                                                                      HAVING    COUNT(*) > 1
                                                                    ) dt )
                                ) dt2
                        WHERE   RowNumber = 1; 
            END;

        --Select * from #CoursesNotRepeated where ReqId='485FBB54-202D-4A46-A353-3F9C76A275E4'

        IF LOWER(@GradeCourseRepetitionsMethod) = 'average'
            BEGIN
                INSERT  INTO #CoursesNotRepeated
                        SELECT  dt2.StuEnrollId
                               ,dt2.TermId
                               ,dt2.TermDescrip
                               ,dt2.ReqId
                               ,dt2.ReqDescrip
                               ,dt2.ClsSectionId
                               ,dt2.CreditsEarned
                               ,dt2.CreditsAttempted
                               ,dt2.CurrentScore
                               ,dt2.CurrentGrade
                               ,dt2.FinalScore
                               ,dt2.FinalGrade
                               ,dt2.Completed
                               ,dt2.FinalGPA
                               ,dt2.Product_WeightedAverage_Credits_GPA
                               ,dt2.Count_WeightedAverage_Credits
                               ,dt2.Product_SimpleAverage_Credits_GPA
                               ,dt2.Count_SimpleAverage_Credits
                               ,dt2.ModUser
                               ,dt2.ModDate
                               ,dt2.TermGPA_Simple
                               ,dt2.TermGPA_Weighted
                               ,dt2.coursecredits
                               ,dt2.CumulativeGPA
                               ,dt2.CumulativeGPA_Simple
                               ,dt2.FACreditsEarned
                               ,dt2.Average
                               ,dt2.CumAverage
                               ,dt2.TermStartDate
                               ,dt2.RowNumber
                               ,dt2.StudentId
                        FROM    (
                                  SELECT    StuEnrollId
                                           ,TermId
                                           ,TermDescrip
                                           ,ReqId
                                           ,ReqDescrip
                                           ,ClsSectionId
                                           ,CreditsEarned
                                           ,CreditsAttempted
                                           ,CurrentScore
                                           ,CurrentGrade
                                           ,FinalScore
                                           ,FinalGrade
                                           ,Completed
                                           ,FinalGPA
                                           ,Product_WeightedAverage_Credits_GPA
                                           ,Count_WeightedAverage_Credits
                                           ,Product_SimpleAverage_Credits_GPA
                                           ,Count_SimpleAverage_Credits
                                           ,ModUser
                                           ,ModDate
                                           ,TermGPA_Simple
                                           ,TermGPA_Weighted
                                           ,coursecredits
                                           ,CumulativeGPA
                                           ,CumulativeGPA_Simple
                                           ,FACreditsEarned
                                           ,Average
                                           ,CumAverage
                                           ,TermStartDate
                                           ,ROW_NUMBER() OVER ( PARTITION BY ReqId ORDER BY FinalGPA DESC ) AS RowNumber
                                           ,(
                                              SELECT TOP 1
                                                        StudentId
                                              FROM      arStuEnrollments
                                              WHERE     StuEnrollId = dbo.syCreditSummary.StuEnrollId
                                            ) AS StudentId
                                  FROM      syCreditSummary
                                  WHERE     StuEnrollId IN ( SELECT StuEnrollID
                                                             FROM   #getStudentGPAByProgramVersion )
                                            AND (
                                                  FinalScore IS NOT NULL
                                                  OR FinalGrade IS NOT NULL
                                                )
                                            AND ReqId IN ( SELECT   ReqId
                                                           FROM     (
                                                                      SELECT    ReqId
                                                                               ,ReqDescrip
                                                               --,TermId
                                                                               ,StuEnrollId
                                                                               ,COUNT(*) AS Counter
                                                                      FROM      syCreditSummary
                                                                      WHERE     StuEnrollId IN ( SELECT StuEnrollID
                                                                                                 FROM   #getStudentGPAByProgramVersion )
                                                                      GROUP BY  ReqId
                                                                               ,ReqDescrip
                                                               --,TermId
                                                                               ,StuEnrollId
                                                                      HAVING    COUNT(*) > 1
                                                                    ) dt )
                                ) dt2; 
            END;
	


        DECLARE @curTermId UNIQUEIDENTIFIER;
        DECLARE getNodes_Cursor CURSOR FAST_FORWARD FORWARD_ONLY
        FOR
            SELECT  DISTINCT
                    StuEnrollId
                   ,TermId
            FROM    #CoursesNotRepeated;
	  

        OPEN getNodes_Cursor;
        FETCH NEXT FROM getNodes_Cursor
				INTO @StuEnrollId,@curTermId;
        WHILE @@FETCH_STATUS = 0
            BEGIN
				----- Program Version GPA Starts Here-----------------------------------------------------------------------
                SET @cumSimpleCourseCredits = (
                                                SELECT  COUNT(coursecredits)
                                                FROM    #CoursesNotRepeated
                                                WHERE   StuEnrollId IN ( @StuEnrollId )
                                                        AND TermId IN ( @curTermId )
                                                        AND FinalGPA IS NOT NULL
                                              );


                SET @cumSimple_GPA_Credits = (
                                               SELECT   SUM(FinalGPA)
                                               FROM     #CoursesNotRepeated
                                               WHERE    StuEnrollId IN ( @StuEnrollId )
                                                        AND TermId IN ( @curTermId )
                                                        AND FinalGPA IS NOT NULL
                                             );

                SET @EquivCourse_SA_CC = (
                                           SELECT   ISNULL(COUNT(Credits),0) AS Credits
                                           FROM     arReqs
                                           WHERE    ReqId IN ( SELECT   CE.EquivReqId
                                                               FROM     arCourseEquivalent CE
                                                               INNER JOIN dbo.arClassSections CS ON CE.ReqId = CS.ReqId
                                                               INNER JOIN arResults R ON CS.ClsSectionId = R.TestId
                                                               INNER JOIN syCreditSummary CSS ON CSS.StuEnrollId = R.StuEnrollId
                                                                                                 AND CSS.ReqId = CS.ReqId
                                                               WHERE    R.StuEnrollId IN ( @StuEnrollId )
                                                                        AND CS.TermId IN ( @curTermId )
                                                                        AND R.GrdSysDetailId IS NOT NULL
                                                                        AND CSS.FinalGPA IS NOT NULL )
                                         );

                SET @EquivCourse_SA_GPA = (
                                            SELECT  SUM(ISNULL(GSD.GPA,0.00))
                                            FROM    arCourseEquivalent CE
                                            INNER JOIN dbo.arClassSections CS ON CE.ReqId = CS.ReqId
                                            INNER JOIN arResults R ON CS.ClsSectionId = R.TestId
                                            INNER JOIN arGradeSystemDetails GSD ON R.GrdSysDetailId = GSD.GrdSysDetailId
                                            INNER JOIN syCreditSummary CSS ON CSS.StuEnrollId = R.StuEnrollId
                                                                              AND CSS.ReqId = CS.ReqId
                                            WHERE   R.StuEnrollId IN ( @StuEnrollId )
                                                    AND CS.TermId IN ( @curTermId )
                                                    AND R.GrdSysDetailId IS NOT NULL
                                                    AND CSS.FinalGPA IS NOT NULL
                                          );


                SET @cumSimpleCourseCredits = @cumSimpleCourseCredits + ISNULL(@EquivCourse_SA_CC,0.00);
                SET @cumSimple_GPA_Credits = @cumSimple_GPA_Credits + ISNULL(@EquivCourse_SA_GPA,0.00);

	
                IF @cumSimpleCourseCredits >= 1
                    BEGIN
                        SET @cumSimpleGPA = @cumSimple_GPA_Credits / @cumSimpleCourseCredits;
                    END; 

                SET @cumWeightedGPA = 0;
                SET @cumCourseCredits = (
                                          SELECT    SUM(coursecredits)
                                          FROM      #CoursesNotRepeated
                                          WHERE     StuEnrollId IN ( @StuEnrollId )
                                                    AND TermId IN ( @curTermId )
                                                    AND FinalGPA IS NOT NULL
                                        );
                SET @cumWeighted_GPA_Credits = (
                                                 SELECT SUM(coursecredits * FinalGPA)
                                                 FROM   #CoursesNotRepeated
                                                 WHERE  StuEnrollId IN ( @StuEnrollId )
                                                        AND TermId IN ( @curTermId )
                                                        AND FinalGPA IS NOT NULL
                                               );

                DECLARE @doesThisCourseHaveAEquivalentCourse INT
                   ,@EquivCourse_WGPA_CC1 INT
                   ,@EquivCourse_WGPA_GPA1 DECIMAL(18,2);
	
                SET @EquivCourse_WGPA_CC1 = (
                                              SELECT    SUM(ISNULL(Credits,0)) AS Credits
                                              FROM      arReqs
                                              WHERE     ReqId IN ( SELECT   CE.EquivReqId
                                                                   FROM     arCourseEquivalent CE
                                                                   INNER JOIN dbo.arClassSections CS ON CE.ReqId = CS.ReqId
                                                                   INNER JOIN arResults R ON CS.ClsSectionId = R.TestId
                                                                   INNER JOIN syCreditSummary CSS ON CSS.StuEnrollId = R.StuEnrollId
                                                                                                     AND CSS.ReqId = CS.ReqId
                                                                   WHERE    R.StuEnrollId IN ( @StuEnrollId )
                                                                            AND R.GrdSysDetailId IS NOT NULL
                                                                            AND CS.TermId IN ( @curTermId )
                                                                            AND CSS.FinalGPA IS NOT NULL )
                                            );
		
                SET @EquivCourse_WGPA_GPA1 = (
                                               SELECT   SUM(GSD.GPA * R1.Credits)
                                               FROM     arCourseEquivalent CE
                                               INNER JOIN dbo.arClassSections CS ON CE.ReqId = CS.ReqId
                                               INNER JOIN arResults R ON CS.ClsSectionId = R.TestId
                                               INNER JOIN arGradeSystemDetails GSD ON R.GrdSysDetailId = GSD.GrdSysDetailId
                                               INNER JOIN arReqs R1 ON R1.ReqId = CE.ReqId
                                               INNER JOIN syCreditSummary CSS ON CSS.StuEnrollId = R.StuEnrollId
                                                                                 AND CSS.ReqId = CS.ReqId
                                               WHERE    R.StuEnrollId IN ( @StuEnrollId )
                                                        AND R.GrdSysDetailId IS NOT NULL
                                                        AND CS.TermId IN ( @curTermId )
                                                        AND CSS.FinalGPA IS NOT NULL
                                             );

		
                SET @cumCourseCredits = @cumCourseCredits + ISNULL(@EquivCourse_WGPA_CC1,0.00);
                SET @cumWeighted_GPA_Credits = @cumWeighted_GPA_Credits + ISNULL(@EquivCourse_WGPA_GPA1,0.00);

                IF @cumCourseCredits >= 1
                    BEGIN
                        SET @cumWeightedGPA = @cumWeighted_GPA_Credits / @cumCourseCredits;
                    END; 	

                DECLARE @TermCredits DECIMAL(18,2);
                SET @TermCredits = (
                                     SELECT SUM(CreditsEarned)
                                     FROM   #CoursesNotRepeated
                                     WHERE  StuEnrollId IN ( @StuEnrollId )
                                            AND TermId IN ( @curTermId )
                                            AND FinalGPA IS NOT NULL
                                   );

                UPDATE  #getStudentGPAbyTerms
                SET     TermCredits = @TermCredits
                       ,TermSimpleGPA = @cumSimpleGPA
                       ,TermWeightedGPA = @cumWeightedGPA
                WHERE   StuEnrollId = @StuEnrollId
                        AND TermId = @curTermId;

						 
                FETCH NEXT FROM getNodes_Cursor
				INTO @StuEnrollId,@curTermId;
            END;
        CLOSE getNodes_Cursor;
        DEALLOCATE getNodes_Cursor;
    --END

        DECLARE getNodes_Cursor CURSOR FAST_FORWARD FORWARD_ONLY
        FOR
            SELECT  StuEnrollID
            FROM    #getStudentGPAByProgramVersion;
	  

        OPEN getNodes_Cursor;
        FETCH NEXT FROM getNodes_Cursor
				INTO @StuEnrollId;
        WHILE @@FETCH_STATUS = 0
            BEGIN
				----- Program Version GPA Starts Here-----------------------------------------------------------------------
                SET @cumSimpleCourseCredits = (
                                                SELECT  COUNT(coursecredits)
                                                FROM    #CoursesNotRepeated
                                                WHERE   StuEnrollId IN ( @StuEnrollId )
                                                        AND FinalGPA IS NOT NULL
                                              );

			--Print '@cumSimpleCourseCredits'
			--Print @cumSimpleCourseCredits

                SET @cumSimpleCourseCredits_OverAll = (
                                                        SELECT  COUNT(coursecredits)
                                                        FROM    #CoursesNotRepeated
                                                        WHERE   StudentId IN ( SELECT   StudentId
                                                                               FROM     arStuEnrollments
                                                                               WHERE    StuEnrollId = @StuEnrollId )
                                                                AND FinalGPA IS NOT NULL
                                                      );


                SET @cumSimple_GPA_Credits = (
                                               SELECT   SUM(FinalGPA)
                                               FROM     #CoursesNotRepeated
                                               WHERE    StuEnrollId IN ( @StuEnrollId )
                                                        AND FinalGPA IS NOT NULL
                                             );

                SET @cumSimple_GPA_Credits_OverAll = (
                                                       SELECT   SUM(FinalGPA)
                                                       FROM     #CoursesNotRepeated
                                                       WHERE    StudentId IN ( SELECT   StudentId
                                                                               FROM     arStuEnrollments
                                                                               WHERE    StuEnrollId = @StuEnrollId )
                                                                AND FinalGPA IS NOT NULL
                                                     );

			
	
                SET @EquivCourse_SA_CC = (
                                           SELECT   ISNULL(COUNT(Credits),0) AS Credits
                                           FROM     arReqs
                                           WHERE    ReqId IN ( SELECT   CE.EquivReqId
                                                               FROM     arCourseEquivalent CE
                                                               INNER JOIN dbo.arClassSections CS ON CE.ReqId = CS.ReqId
                                                               INNER JOIN arResults R ON CS.ClsSectionId = R.TestId
                                                               INNER JOIN syCreditSummary CSS ON CSS.StuEnrollId = R.StuEnrollId
                                                                                                 AND CSS.ReqId = CS.ReqId
                                                               WHERE    R.StuEnrollId IN ( @StuEnrollId )
                                                                        AND R.GrdSysDetailId IS NOT NULL
                                                                        AND CSS.FinalGPA IS NOT NULL )
                                         );

			


                SET @EquivCourse_SA_CC_OverAll = (
                                                   SELECT   ISNULL(COUNT(Credits),0) AS Credits
                                                   FROM     arReqs
                                                   WHERE    ReqId IN ( SELECT   CE.EquivReqId
                                                                       FROM     arCourseEquivalent CE
                                                                       INNER JOIN dbo.arClassSections CS ON CE.ReqId = CS.ReqId
                                                                       INNER JOIN arResults R ON CS.ClsSectionId = R.TestId
                                                                       INNER JOIN dbo.arStuEnrollments SE ON R.StuEnrollId = SE.StuEnrollId
                                                                       INNER JOIN syCreditSummary CSS ON CSS.StuEnrollId = R.StuEnrollId
                                                                                                         AND CSS.ReqId = CS.ReqId
                                                                       WHERE    SE.StudentId IN ( SELECT    StudentId
                                                                                                  FROM      arStuEnrollments
                                                                                                  WHERE     StuEnrollId = @StuEnrollId )
                                                                                AND R.GrdSysDetailId IS NOT NULL
                                                                                AND CSS.FinalGPA IS NOT NULL )
                                                 );
		
                SET @EquivCourse_SA_GPA = (
                                            SELECT  SUM(ISNULL(GSD.GPA,0.00))
                                            FROM    arCourseEquivalent CE
                                            INNER JOIN dbo.arClassSections CS ON CE.ReqId = CS.ReqId
                                            INNER JOIN arResults R ON CS.ClsSectionId = R.TestId
                                            INNER JOIN arGradeSystemDetails GSD ON R.GrdSysDetailId = GSD.GrdSysDetailId
                                            INNER JOIN syCreditSummary CSS ON CSS.StuEnrollId = R.StuEnrollId
                                                                              AND CSS.ReqId = CS.ReqId
                                            WHERE   R.StuEnrollId IN ( @StuEnrollId )
                                                    AND R.GrdSysDetailId IS NOT NULL
                                                    AND CSS.FinalGPA IS NOT NULL
                                          );

                SET @EquivCourse_SA_GPA_OverAll = (
                                                    SELECT  SUM(ISNULL(GSD.GPA,0.00))
                                                    FROM    arCourseEquivalent CE
                                                    INNER JOIN dbo.arClassSections CS ON CE.ReqId = CS.ReqId
                                                    INNER JOIN arResults R ON CS.ClsSectionId = R.TestId
                                                    INNER JOIN arGradeSystemDetails GSD ON R.GrdSysDetailId = GSD.GrdSysDetailId
                                                    INNER JOIN dbo.arStuEnrollments SE ON R.StuEnrollId = SE.StuEnrollId
                                                    INNER JOIN syCreditSummary CSS ON CSS.StuEnrollId = R.StuEnrollId
                                                                                      AND CSS.ReqId = CS.ReqId
                                                    WHERE   StudentId IN ( SELECT   StudentId
                                                                           FROM     arStuEnrollments
                                                                           WHERE    StuEnrollId = @StuEnrollId )
                                                            AND R.GrdSysDetailId IS NOT NULL
                                                            AND CSS.FinalGPA IS NOT NULL
                                                  );
			

                SET @cumSimpleCourseCredits = @cumSimpleCourseCredits + ISNULL(@EquivCourse_SA_CC,0.00);
                SET @cumSimple_GPA_Credits = @cumSimple_GPA_Credits + ISNULL(@EquivCourse_SA_GPA,0.00);

                SET @cumSimpleCourseCredits_OverAll = @cumSimpleCourseCredits_OverAll + ISNULL(@EquivCourse_SA_CC_OverAll,0.00);
                SET @cumSimple_GPA_Credits_OverAll = @cumSimple_GPA_Credits_OverAll + ISNULL(@EquivCourse_SA_GPA_OverAll,0.00);
			
                IF @cumSimpleCourseCredits >= 1
                    BEGIN
                        SET @cumSimpleGPA = @cumSimple_GPA_Credits / @cumSimpleCourseCredits;
                        SET @cumSimpleGPA_OverAll = @cumSimple_GPA_Credits_OverAll / @cumSimpleCourseCredits_OverAll;
                    END; 
			
--			Print 'NWH New'	
--			Print @cumSimpleCourseCredits
--			PRINT @cumSimple_GPA_Credits
--			PRINT @EquivCourse_SA_CC
--			Print @EquivCourse_SA_GPA
--			print 'beforePPPRP'	
--			PRINT        @cumSimpleCourseCredits
--Print @cumSimple_GPA_Credits
--			Print '@cumSimpleGPA='
--			Print @cumSimpleGPA

			
                SET @cumWeightedGPA = 0;
                SET @cumCourseCredits = (
                                          SELECT    SUM(coursecredits)
                                          FROM      #CoursesNotRepeated
                                          WHERE     StuEnrollId IN ( @StuEnrollId )
                                                    AND FinalGPA IS NOT NULL
                                        );
                SET @cumWeighted_GPA_Credits = (
                                                 SELECT SUM(coursecredits * FinalGPA)
                                                 FROM   #CoursesNotRepeated
                                                 WHERE  StuEnrollId IN ( @StuEnrollId )
                                                        AND FinalGPA IS NOT NULL
                                               );
			--Print 'New values='
			--Print @cumCourseCredits
			--Print @cumWeighted_GPA_Credits

                DECLARE @cumCourseCredits_OverAll DECIMAL(18,2)
                   ,@cumWeighted_GPA_Credits_OverAll DECIMAL(18,2);

                SET @cumCourseCredits_OverAll = (
                                                  SELECT    SUM(coursecredits)
                                                  FROM      #CoursesNotRepeated
                                                  WHERE     StudentId IN ( SELECT   StudentId
                                                                           FROM     arStuEnrollments
                                                                           WHERE    StuEnrollId = @StuEnrollId )
                                                            AND FinalGPA IS NOT NULL
                                                );

                SET @cumWeighted_GPA_Credits_OverAll = (
                                                         SELECT SUM(coursecredits * FinalGPA)
                                                         FROM   #CoursesNotRepeated
                                                         WHERE  StudentId IN ( SELECT   StudentId
                                                                               FROM     arStuEnrollments
                                                                               WHERE    StuEnrollId = @StuEnrollId )
                                                                AND FinalGPA IS NOT NULL
                                                       );


                DECLARE @EquivCourse_WGPA_CC1_OverAll DECIMAL(18,2)
                   ,@EquivCourse_WGPA_GPA1_OverAll DECIMAL(18,2)
                   ,@cumWeightedGPA_OverAll DECIMAL(18,2);
		
                SET @EquivCourse_WGPA_CC1 = (
                                              SELECT    SUM(ISNULL(Credits,0)) AS Credits
                                              FROM      arReqs
                                              WHERE     ReqId IN ( SELECT   CE.EquivReqId
                                                                   FROM     arCourseEquivalent CE
                                                                   INNER JOIN dbo.arClassSections CS ON CE.ReqId = CS.ReqId
                                                                   INNER JOIN arResults R ON CS.ClsSectionId = R.TestId
                                                                   INNER JOIN syCreditSummary CSS ON CSS.StuEnrollId = R.StuEnrollId
                                                                                                     AND CSS.ReqId = CS.ReqId
                                                                   WHERE    R.StuEnrollId IN ( @StuEnrollId )
                                                                            AND R.GrdSysDetailId IS NOT NULL
                                                                            AND CSS.FinalGPA IS NOT NULL )
                                            );
		

                SET @EquivCourse_WGPA_GPA1 = (
                                               SELECT   SUM(GSD.GPA * R1.Credits)
                                               FROM     arCourseEquivalent CE
                                               INNER JOIN dbo.arClassSections CS ON CE.ReqId = CS.ReqId
                                               INNER JOIN arResults R ON CS.ClsSectionId = R.TestId
                                               INNER JOIN arGradeSystemDetails GSD ON R.GrdSysDetailId = GSD.GrdSysDetailId
                                               INNER JOIN arReqs R1 ON R1.ReqId = CE.ReqId
                                               INNER JOIN syCreditSummary CSS ON CSS.StuEnrollId = R.StuEnrollId
                                                                                 AND CSS.ReqId = CS.ReqId
                                               WHERE    R.StuEnrollId IN ( @StuEnrollId )
                                                        AND R.GrdSysDetailId IS NOT NULL
                                                        AND CSS.FinalGPA IS NOT NULL
                                             );

                SET @EquivCourse_WGPA_CC1_OverAll = (
                                                      SELECT    SUM(ISNULL(Credits,0)) AS Credits
                                                      FROM      arReqs
                                                      WHERE     ReqId IN ( SELECT   CE.EquivReqId
                                                                           FROM     arCourseEquivalent CE
                                                                           INNER JOIN dbo.arClassSections CS ON CE.ReqId = CS.ReqId
                                                                           INNER JOIN arResults R ON CS.ClsSectionId = R.TestId
                                                                           INNER JOIN dbo.arStuEnrollments SE ON R.StuEnrollId = SE.StuEnrollId
                                                                           INNER JOIN syCreditSummary CSS ON CSS.StuEnrollId = R.StuEnrollId
                                                                                                             AND CSS.ReqId = CS.ReqId
                                                                           WHERE    StudentId IN ( SELECT   StudentId
                                                                                                   FROM     arStuEnrollments
                                                                                                   WHERE    StuEnrollId = @StuEnrollId )
                                                                                    AND R.GrdSysDetailId IS NOT NULL
                                                                                    AND CSS.FinalGPA IS NOT NULL )
                                                    );

                SET @EquivCourse_WGPA_GPA1_OverAll = (
                                                       SELECT   SUM(GSD.GPA * R1.Credits)
                                                       FROM     arCourseEquivalent CE
                                                       INNER JOIN dbo.arClassSections CS ON CE.ReqId = CS.ReqId
                                                       INNER JOIN arResults R ON CS.ClsSectionId = R.TestId
                                                       INNER JOIN arGradeSystemDetails GSD ON R.GrdSysDetailId = GSD.GrdSysDetailId
                                                       INNER JOIN arReqs R1 ON R1.ReqId = CE.ReqId
                                                       INNER JOIN dbo.arStuEnrollments SE ON R.StuEnrollId = SE.StuEnrollId
                                                       INNER JOIN syCreditSummary CSS ON CSS.StuEnrollId = R.StuEnrollId
                                                                                         AND CSS.ReqId = CS.ReqId
                                                       WHERE    StudentId IN ( SELECT   StudentId
                                                                               FROM     arStuEnrollments
                                                                               WHERE    StuEnrollId = @StuEnrollId )
                                                                AND R.GrdSysDetailId IS NOT NULL
                                                                AND CSS.FinalGPA IS NOT NULL
                                                     );  


                SET @cumCourseCredits = @cumCourseCredits + ISNULL(@EquivCourse_WGPA_CC1,0.00);
                SET @cumWeighted_GPA_Credits = @cumWeighted_GPA_Credits + ISNULL(@EquivCourse_WGPA_GPA1,0.00);

                SET @cumCourseCredits_OverAll = @cumCourseCredits_OverAll + ISNULL(@EquivCourse_WGPA_CC1_OverAll,0.00);
                SET @cumWeighted_GPA_Credits_OverAll = @cumWeighted_GPA_Credits_OverAll + ISNULL(@EquivCourse_WGPA_GPA1_OverAll,0.00);

                IF @cumCourseCredits >= 1
                    BEGIN
                        SET @cumWeightedGPA = @cumWeighted_GPA_Credits / @cumCourseCredits;
                        SET @cumWeightedGPA_OverAll = @cumWeighted_GPA_Credits_OverAll / @cumCourseCredits_OverAll;
                    END; 	

                UPDATE  #getStudentGPAByProgramVersion
                SET     ProgramVersion_SimpleGPA = @cumSimpleGPA
                       ,ProgramVersion_WeightedGPA = @cumWeightedGPA
                       ,OverAll_SimpleGPA = @cumSimpleGPA_OverAll
                       ,OverAll_WeightedGPA = @cumWeightedGPA_OverAll
                WHERE   StuEnrollID = @StuEnrollId;

                FETCH NEXT FROM getNodes_Cursor
				INTO @StuEnrollId;
            END;
        CLOSE getNodes_Cursor;
        DEALLOCATE getNodes_Cursor;

        CREATE TABLE #GPASummary
            (
             StuEnrollId UNIQUEIDENTIFIER
            ,TermId UNIQUEIDENTIFIER
            ,TermDescrip VARCHAR(100)
            ,TermSimpleGPA DECIMAL(18,2)
            ,TermWeightedGPA DECIMAL(18,2)
            ,ProgramVersionSimpleGPA DECIMAL(18,2)
            ,ProgramVersionWeightedGPA DECIMAL(18,2)
            ,StudentSimpleGPA DECIMAL(18,2)
            ,StudentWeightedGPA DECIMAL(18,2)
            );

        INSERT  INTO #GPASummary
                SELECT DISTINCT
                        SGT.StuEnrollId
                       ,SGT.TermId
                       ,TermDescrip
                       ,TermSimpleGPA
                       ,TermWeightedGPA
                       ,ProgramVersion_SimpleGPA AS ProgramVersionSimpleGPA
                       ,ProgramVersion_WeightedGPA AS ProgramVersionWeightedGPA
                       ,OverAll_SimpleGPA AS StudentSimpleGPA
                       ,OverAll_WeightedGPA AS StudentWeightedGPA
                FROM    #getStudentGPAbyTerms SGT
                INNER JOIN #getStudentGPAByProgramVersion SGPV ON SGT.StuEnrollId = SGPV.StuEnrollID;

        DROP TABLE #CoursesNotRepeated;
        DROP TABLE #getStudentGPAbyTerms;
        DROP TABLE #getStudentGPAByProgramVersion;
    END;
--''''''''''''''''''''''''''''' End: This section of the stored proc calculates the GPA '''''''''''''''''''''''''''''''


    DECLARE @GradesFormat AS VARCHAR(50);
    DECLARE @GPAMethod AS VARCHAR(50);
    DECLARE @GradeBookAt AS VARCHAR(50);
    DECLARE @StuEnrollCampusId AS UNIQUEIDENTIFIER;
    DECLARE @TermStartDate1 AS DATETIME; 
    DECLARE @Score AS DECIMAL(18,2);
    DECLARE @GrdCompDescrip AS VARCHAR(50);

    DECLARE @curId AS UNIQUEIDENTIFIER;
    DECLARE @curReqId AS UNIQUEIDENTIFIER;
    DECLARE @curStuEnrollId AS UNIQUEIDENTIFIER;
    DECLARE @curDescrip AS VARCHAR(50);
    DECLARE @curNumber AS INT;
    DECLARE @curGrdComponentTypeId AS INT;
    DECLARE @curMinResult AS DECIMAL(18,2); 
    DECLARE @curGrdComponentDescription AS VARCHAR(50);   
    DECLARE @curClsSectionId AS UNIQUEIDENTIFIER;
    DECLARE @curRownumber AS INT;
			
    SET @StuEnrollCampusId = COALESCE((
                                        SELECT TOP 1
                                                ASE.CampusId
                                        FROM    arStuEnrollments AS ASE
                                        WHERE   ASE.StuEnrollId IN ( SELECT Val
                                                                     FROM   MultipleValuesForReportParameters(@StuEnrollIdList,',',1) )
                                      ),NULL);
    SET @GradesFormat = dbo.GetAppSettingValueByKeyName('GradesFormat',@StuEnrollCampusId);
    IF ( @GradesFormat IS NOT NULL )
        BEGIN
            SET @GradesFormat = LOWER(LTRIM(RTRIM(@GradesFormat)));
        END;
    SET @GPAMethod = dbo.GetAppSettingValueByKeyName('GPAMethod',@StuEnrollCampusId);
    IF ( @GPAMethod IS NOT NULL )
        BEGIN
            SET @GPAMethod = LOWER(LTRIM(RTRIM(@GPAMethod)));
        END;
    SET @GradeBookAt = dbo.GetAppSettingValueByKeyName('GradeBookWeightingLevel',@StuEnrollCampusId);
    IF ( @GradeBookAt IS NOT NULL )
        BEGIN
            SET @GradeBookAt = LOWER(LTRIM(RTRIM(@GradeBookAt)));
        END;


	
    SELECT  dt1.PrgVerId
           ,dt1.PrgVerDescrip
           ,dt1.PrgVersionTrackCredits
           ,dt1.TermId
           ,dt1.TermDescription
           ,dt1.TermStartDate
           ,dt1.TermEndDate
           ,dt1.CourseId
           ,dt1.CouseStartDate
           ,dt1.CourseCode
           ,dt1.CourseDescription
           ,dt1.CourseCodeDescription
           ,dt1.CourseCredits
           ,dt1.CourseFinAidCredits
           ,dt1.MinVal
           ,dt1.CourseScore
           ,dt1.StuEnrollId
           ,dt1.CreditsAttempted
           ,dt1.CreditsEarned
           ,dt1.Completed
           ,dt1.CurrentScore
           ,dt1.CurrentGrade
           ,dt1.FinalScore
           ,dt1.FinalGrade
           ,dt1.FinalGPA
           ,dt1.ScheduleDays
           ,CASE WHEN dt1.IsCreditsEarned = 1 THEN dt1.ActualDay
                 ELSE 0.00
            END AS ActualDay
		--, dt1.CampusId
		-- dt1.CampDescrip
           ,dt1.GrdBkWgtDetailsCount
           ,dt1.ClockHourProgram
           ,dt1.GradesFormat
           ,dt1.GPAMethod
           ,GS.TermSimpleGPA
           ,GS.TermWeightedGPA
           ,GS.ProgramVersionSimpleGPA
           ,GS.ProgramVersionWeightedGPA
           ,GS.StudentSimpleGPA
           ,GS.StudentWeightedGPA
           ,dt1.RowNumber
           ,dt1.CourseCategoryDescrip
           ,dt1.CourseCategoryId
           ,(
              SELECT    ISNULL(Credits,0)
              FROM      arReqs AS AR
              WHERE     AR.ReqId = dt1.CourseId
                        AND AR.CourseCategoryId = dt1.CourseCategoryId
            ) AS CreditsByCourseCategory
           ,(
              SELECT    ISNULL(Hours,0)
              FROM      arReqs AS AR
              WHERE     AR.ReqId = dt1.CourseId
                        AND AR.CourseCategoryId = dt1.CourseCategoryId
            ) AS HoursByCourseCategory
           ,dt1.IsCreditsEarned
    FROM    (
              SELECT    dt.PrgVerId
                       ,dt.PrgVerDescrip
                       ,dt.PrgVersionTrackCredits
                       ,dt.TermId
                       ,dt.TermDescription
                       ,dt.TermStartDate
                       ,dt.TermEndDate
                       ,dt.CourseId
                       ,dt.CouseStartDate
                       ,dt.CourseCode
                       ,dt.CourseDescription
                       ,dt.CourseCodeDescription
                       ,dt.CourseCredits
                       ,dt.CourseFinAidCredits
                       ,dt.MinVal
                       ,dt.CourseScore
                       ,dt.StuEnrollId
                       ,dt.CreditsAttempted
                       ,dt.CreditsEarned
                       ,dt.Completed
                       ,dt.CurrentScore
                       ,dt.CurrentGrade
                       ,dt.FinalScore
                       ,dt.FinalGrade
                       ,dt.FinalGPA
                       ,dt.ScheduleDays
                       ,dt.ActualDay
                       ,dt.GrdBkWgtDetailsCount
                       ,dt.ClockHourProgram
                       ,@GradesFormat AS GradesFormat
                       ,@GPAMethod AS GPAMethod
                       ,ROW_NUMBER() OVER ( PARTITION BY TermId,CourseId ORDER BY TermStartDate, TermEndDate, TermDescription, CourseDescription ) AS RowNumber
                       ,dt.CourseCategoryDescrip
                       ,dt.CourseCategoryId
                       ,dt.IsCreditsEarned
              FROM      (
                          SELECT DISTINCT
                                    PV.PrgVerId AS PrgVerId
                                   ,PV.PrgVerDescrip AS PrgVerDescrip
                                   ,CASE WHEN ( PV.Credits > 0.0 ) THEN 1
                                         ELSE 0
                                    END AS PrgVersionTrackCredits
                                   ,T.TermId AS TermId
                                   ,T.TermDescrip AS TermDescription
                                   ,T.StartDate AS TermStartDate
                                   ,T.EndDate AS TermEndDate
                                   ,CS.ReqId AS CourseId
                                   ,CS.StartDate AS CouseStartDate
                                   ,R.Code AS CourseCode
                                   ,R.Descrip AS CourseDescription
                                   ,'(' + R.Code + ') ' + R.Descrip AS CourseCodeDescription
                                   ,R.Credits AS CourseCredits
                                   ,R.FinAidCredits AS CourseFinAidCredits
                                   ,(
                                      SELECT    MIN(MinVal)
                                      FROM      arGradeScaleDetails GCD
                                               ,arGradeSystemDetails GSD
                                      WHERE     GCD.GrdSysDetailId = GSD.GrdSysDetailId
                                                AND GSD.IsPass = 1
                                                AND GCD.GrdScaleId = CS.GrdScaleId
                                    ) AS MinVal
                                   ,RES.Score AS CourseScore
                                   ,SE.StuEnrollId AS StuEnrollId
                                   ,ISNULL(SCS.CreditsAttempted,0) AS CreditsAttempted
                                   ,ISNULL(SCS.CreditsEarned,0) AS CreditsEarned
                                   ,SCS.Completed AS Completed
                                   ,SCS.CurrentScore AS CurrentScore
                                   ,SCS.CurrentGrade AS CurrentGrade
                                   ,SCS.FinalScore AS FinalScore
                                   ,SCS.FinalGrade AS FinalGrade
                                   ,SCS.FinalGPA AS FinalGPA
						  --, SSAS.ScheduledDays AS ScheduleDays
						  --, SSAS.ActualDays AS ActualDay
                                   ,R.Hours AS ScheduleDays
                                   ,R.Hours AS ActualDay
                                   ,(
                                      SELECT    COUNT(*) AS GrdBkWgtDetailsCount
                                      FROM      arGrdBkResults
                                      WHERE     StuEnrollId = SE.StuEnrollId
                                                AND ClsSectionId = RES.TestId
                                    ) AS GrdBkWgtDetailsCount
                                   ,CASE WHEN P.ACId = 5 THEN 'True'
                                         ELSE 'False'
                                    END AS ClockHourProgram
                                   ,ISNULL(CC.CourseCategoryId,'00000000-0000-0000-0000-000000000000') AS CourseCategoryId
                                   ,CC.Descrip AS CourseCategoryDescrip
                                   ,GSD.IsCreditsEarned AS IsCreditsEarned
                          FROM      arClassSections CS
                          INNER JOIN arResults RES ON CS.ClsSectionId = RES.TestId
                          INNER JOIN arStuEnrollments SE ON RES.StuEnrollId = SE.StuEnrollId
                          INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                          INNER JOIN arPrograms P ON P.ProgId = PV.ProgId
                          INNER JOIN arTerm T ON CS.TermId = T.TermId
                          INNER JOIN arReqs AS R ON CS.ReqId = R.ReqId
                          LEFT JOIN syCreditSummary SCS ON SCS.StuEnrollId = SE.StuEnrollId
                                                           AND SCS.TermId = T.TermId
                                                           AND SCS.ReqId = R.ReqId
                                                           AND SCS.ClsSectionId = CS.ClsSectionId
                          LEFT JOIN syStudentAttendanceSummary AS SSAS ON SSAS.ClsSectionId = CS.ClsSectionId
                                                                          AND SSAS.StuEnrollId = SE.StuEnrollId
                          LEFT JOIN arCourseCategories CC ON R.CourseCategoryId = CC.CourseCategoryId
                          LEFT JOIN arGradeSystemDetails GSD ON RES.GrdSysDetailId = GSD.GrdSysDetailId
                          WHERE     SE.StuEnrollId IN ( SELECT  Val
                                                        FROM    MultipleValuesForReportParameters(@StuEnrollIdList,',',1) )
                                    AND (
                                          @TermId IS NULL
                                          OR T.TermId = @TermId
                                        )
							--AND R.IsAttendanceOnly = 0
                            -- Nota: 2016-01-05 JT: As per DE12275 Case 986512 Do not check for Completed but check for FinalsGPA is not null
                            --AND SCS.Completed = 1  
                            --AND SCS.FinalGPA IS NOT NULL
							-- Show the courses only if it has a grade   Letter or numenic always have GPAFinal not null 
							--AND (ISNULL(SCS.FinalScore, 0) > 0 OR ISNULL(SCS.FinalGrade, 0)
                                    AND (
                                          RES.GrdSysDetailId IS NOT NULL
                                          OR RES.Score IS NOT NULL
                                        )
                          UNION
                          SELECT DISTINCT
                                    PV.PrgVerId AS PrgVerId
                                   ,PV.PrgVerDescrip AS PrgVerDescrip
                                   ,CASE WHEN ( PV.Credits > 0.0 ) THEN 1
                                         ELSE 0
                                    END AS PrgVersionTrackCredits
                                   ,T.TermId AS TermId
                                   ,T.TermDescrip AS TermDescription
                                   ,T.StartDate AS TermStartDate
                                   ,T.EndDate AS TermEndDate
                                   ,GBCR.ReqId AS CourseId
                                   ,T.StartDate AS CouseStartDate   -- tranfered
                                   ,R.Code AS CourseCode
                                   ,R.Descrip AS CourseDescrip
                                   ,'(' + R.Code + ') ' + R.Descrip AS CourseCodeDescrip
                                   ,R.Credits AS CourseCredits
                                   ,R.FinAidCredits AS CourseFinAidCredits
                                   ,(
                                      SELECT    MIN(MinVal)
                                      FROM      arGradeScaleDetails GCD
                                               ,arGradeSystemDetails GSD
                                      WHERE     GCD.GrdSysDetailId = GSD.GrdSysDetailId
                                                AND GSD.IsPass = 1
                                    ) AS MinVal
                                   ,ISNULL(GBCR.Score,0)
                                   ,SE.StuEnrollId
                                   ,ISNULL(SCS.CreditsAttempted,0) AS CreditsAttempted
                                   ,ISNULL(SCS.CreditsEarned,0) AS CreditsEarned
                                   ,SCS.Completed AS Completed
                                   ,SCS.CurrentScore AS CurrentScore
                                   ,SCS.CurrentGrade AS CurrentGrade
                                   ,SCS.FinalScore AS FinalScore
                                   ,SCS.FinalGrade AS FinalGrade
                                   ,SCS.FinalGPA AS FinalGPA
                                   ,R.Hours AS ScheduleDays
                                   ,R.Hours AS ActualDay
						  --, NULL AS ScheduleDays -- SSAS.ScheduledDays AS ScheduleDays
						  --, NULL AS ActualDays   --SSAS.ActualDays    AS ActualDays
                                   ,(
                                      SELECT    COUNT(*) AS GrdBkWgtDetailsCount
                                      FROM      arGrdBkConversionResults
                                      WHERE     StuEnrollId = SE.StuEnrollId
                                                AND TermId = GBCR.TermId
                                                AND ReqId = GBCR.ReqId
                                    ) AS GrdBkWgtDetailsCount
                                   ,CASE WHEN P.ACId = 5 THEN 'True'
                                         ELSE 'False'
                                    END AS ClockHourProgram
                                   ,ISNULL(CC.CourseCategoryId,'00000000-0000-0000-0000-000000000000') AS CourseCategoryId
                                   ,CC.Descrip AS CourseCategoryDescrip
                                   ,GSD.IsCreditsEarned AS IsCreditsEarned
                          FROM      arTransferGrades GBCR
                          INNER JOIN arStuEnrollments SE ON GBCR.StuEnrollId = SE.StuEnrollId
                          INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                          INNER JOIN arPrograms P ON P.ProgId = PV.ProgId
                          INNER JOIN arTerm T ON GBCR.TermId = T.TermId
                          INNER JOIN arReqs R ON GBCR.ReqId = R.ReqId
                          LEFT JOIN syCreditSummary SCS ON SE.StuEnrollId = SCS.StuEnrollId
                                                           AND T.TermId = SCS.TermId
                                                           AND R.ReqId = SCS.ReqId
                          LEFT JOIN arCourseCategories CC ON R.CourseCategoryId = CC.CourseCategoryId
                          LEFT JOIN arGradeSystemDetails GSD ON GBCR.GrdSysDetailId = GSD.GrdSysDetailId
                          WHERE     SE.StuEnrollId IN ( SELECT  Val
                                                        FROM    MultipleValuesForReportParameters(@StuEnrollIdList,',',1) )
                                    AND (
                                          @TermId IS NULL
                                          OR T.TermId = @TermId
                                        )
							--AND R.IsAttendanceOnly = 0
                            -- Nota: 2016-01-05 JT: As per DE12275 Case 986512 Do not check for Completed but check for FinalsGPA is not null
                            --AND SCS.Completed = 1  
                            --AND SCS.FinalGPA IS NOT NULL
							--AND SCS.Completed = 1
							-- Show the courses only if it has a grade   Letter or numenic always have GPAFinal not null 
							--AND (ISNULL(SCS.FinalScore, 0) > 0 OR ISNULL(SCS.FinalGrade, 0)
                                    AND ( GBCR.GrdSysDetailId IS NOT NULL )
                        ) dt
            ) dt1
    INNER JOIN #GPASummary GS ON dt1.StuEnrollId = GS.StuEnrollId
                                 AND dt1.TermId = GS.TermId
	--WHERE   dt1.RowNumber = 1
    ORDER BY dt1.CourseCategoryDescrip
           ,dt1.TermStartDate
           ,dt1.TermDescription
           ,dt1.CourseDescription;
          --, CASE WHEN @TermId IS NOT NULL
          --       THEN (RANK() OVER (ORDER BY dt1.TermStartDate, dt1.TermDescription, dt1.CourseDescription))
          --       ELSE (RANK() OVER (ORDER BY dt1.CouseStartDate, dt1.CourseDescription))
          --  END

--Drop table #CoursesNotRepeated
--	Drop table #getStudentGPAbyTerms
--	Drop table #getStudentGPAByProgramVersion
    DROP TABLE #GPASummary;

-- =========================================================================================================
-- Usp_TR_Sub12_CoursesByCourseCategory
-- =========================================================================================================

GO
