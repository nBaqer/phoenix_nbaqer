SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[DeleteDuplicateGrdBkResults]
AS
    DECLARE @counter AS INT;
    DECLARE @ClsSectionId UNIQUEIDENTIFIER;
    DECLARE @InstrGrdBkWgtDetailId UNIQUEIDENTIFIER;
    DECLARE @StuEnrollId UNIQUEIDENTIFIER;
    DECLARE @GrdBkResultId UNIQUEIDENTIFIER;
    DECLARE @times INT;
    DECLARE getUsers_Cursor CURSOR
    FOR
        SELECT  ClsSectionId
               ,InstrGrdBkWgtDetailId
               ,StuEnrollId
               ,COUNT(*)
        FROM    arGrdBkResults
        GROUP BY ClsSectionId
               ,InstrGrdBkWgtDetailId
               ,StuEnrollId
        HAVING  COUNT(*) > 1;
    OPEN getUsers_Cursor;
    FETCH NEXT FROM getUsers_Cursor
INTO @ClsSectionId,@InstrGrdBkWgtDetailId,@StuEnrollId,@counter;

    WHILE @@FETCH_STATUS = 0
        BEGIN
            SET @times = @counter - 1;
            WHILE @times > 0
                BEGIN
                    SET @GrdBkResultId = (
                                           SELECT TOP 1
                                                    GrdBkResultId
                                           FROM     arGrdBkResults
                                           WHERE    ClsSectionId = @ClsSectionId
                                                    AND StuEnrollId = @StuEnrollId
                                                    AND InstrGrdBkWgtDetailId = @InstrGrdBkWgtDetailId
                                         );
                    DELETE  FROM arGrdBkResults
                    WHERE   GrdBkResultId = @GrdBkResultId;
                    SET @times = @times - 1;
                END;
            FETCH NEXT FROM getUsers_Cursor
INTO @ClsSectionId,@InstrGrdBkWgtDetailId,@StuEnrollId,@counter;
        END;
    CLOSE getUsers_Cursor;
    DEALLOCATE getUsers_Cursor;



GO
