SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--=================================================================================================
-- USP_getTerms_byCampusDate
--=================================================================================================
CREATE PROCEDURE [dbo].[USP_getTerms_byCampusDate]
    @Campus UNIQUEIDENTIFIER
   ,@WeekEndDate DATE
   ,@ShowInactive BIT
AS
    BEGIN  
        DECLARE @ClassesThatFallWithinDateRange TABLE
            (
             TermId UNIQUEIDENTIFIER
            );  
  
        INSERT  INTO @ClassesThatFallWithinDateRange
                SELECT DISTINCT
                        TermId
                FROM    dbo.arClassSections ARC
                WHERE   @WeekEndDate BETWEEN ARC.StartDate AND ARC.EndDate
                        AND ARC.CampusId = @Campus;  
      
        SELECT DISTINCT
                T.TermId
               ,T.TermCode + ' - ' + T.TermDescrip AS TermDescrip
               ,StartDate
        FROM    arTerm T
        INNER JOIN @ClassesThatFallWithinDateRange CWR ON T.TermId = CWR.TermId
        WHERE   (
                  @ShowInactive = 0
                  AND T.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
                )
                OR @ShowInactive = 1
        ORDER BY StartDate DESC
               ,TermDescrip;    
    END;  
--=================================================================================================
-- END  --  USP_getTerms_byCampusDate
--=================================================================================================
GO
