SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[USP_ClsSectAttendance_Insert]
    (
     @StuEnrollId UNIQUEIDENTIFIER
    ,@ClsSectionID UNIQUEIDENTIFIER
    ,@ClsSectMeetingID UNIQUEIDENTIFIER
    ,@MeetDate DATETIME
    ,@Actual DECIMAL(18)
    ,@Tardy BIT
    ,@Excused BIT
    ,@Scheduled DECIMAL(18)
    )
AS /*----------------------------------------------------------------------------------------------------
	Author          :	Saraswathi Lakshmanan
    
    Create date		:	09/13/2011
    
	Procedure Name	:	[USP_ClsSectAttendance_Insert]

	Objective		:	Inserts into table atClsSectAttendance
	
					
*/-----------------------------------------------------------------------------------------------------

    BEGIN

        IF EXISTS ( SELECT  *
                    FROM    dbo.atClsSectAttendance
                    WHERE   meetdate = @MeetDate
                            AND StuEnrollId = @StuEnrollId
                            AND ClsSectionId = @ClsSectionID
                            AND ClsSectMeetingId = @ClsSectMeetingID )
            BEGIN
                DELETE  FROM dbo.atClsSectAttendance
                WHERE   meetdate = @MeetDate
                        AND StuEnrollId = @StuEnrollId
                        AND ClsSectionId = @ClsSectionID
                        AND ClsSectMeetingId = @ClsSectMeetingID;
	
            END;
	
        INSERT  INTO dbo.atClsSectAttendance
                (
                 ClsSectAttId
                ,StudentId
                ,ClsSectionId
                ,ClsSectMeetingId
                ,MeetDate
                ,Actual
                ,Tardy
                ,Excused
                ,Comments
                ,StuEnrollId
                ,Scheduled
	            )
        VALUES  (
                 NEWID()
                , -- ClsSectAttId - uniqueidentifier
                 NULL
                , -- StudentId - uniqueidentifier
                 @ClsSectionID
                , -- ClsSectionId - uniqueidentifier
                 @ClsSectMeetingID
                , -- ClsSectMeetingId - uniqueidentifier
                 @MeetDate
                , -- MeetDate - datetime
                 @Actual
                , -- Actual - decimal
                 @Tardy
                , -- Tardy - bit
                 @Excused
                , -- Excused - bit
                 NULL
                , -- Comments - varchar(100)
                 @StuEnrollId
                , -- StuEnrollId - uniqueidentifier
                 @Scheduled  -- Scheduled - decimal
	            );
	
	
    END;





GO
