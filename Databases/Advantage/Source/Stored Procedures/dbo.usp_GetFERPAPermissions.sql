SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_GetFERPAPermissions]
    (
     @ResourceId SMALLINT
    ,@studentID UNIQUEIDENTIFIER
    )
AS
    SET NOCOUNT ON;
    IF @ResourceId = 0
        BEGIN
            DECLARE @AnyPolicy AS INT;
            SET @AnyPolicy = 0;
            SET @AnyPolicy = (
                               SELECT   COUNT(*)
                               FROM     arFERPAPolicy
                               WHERE    studentid = @studentID
                             );
            IF @AnyPolicy > 0
                SELECT DISTINCT
                        'Student Only' AS FerPaEntityDescrip
                FROM    arFERPAPolicy
                WHERE   studentid = @studentID;
        END;
    ELSE
        BEGIN
            SELECT DISTINCT
                    D.FerPaEntityDescrip AS FerPaEntityDescrip
            FROM    arferpapolicy A
                   ,arFERPaCategory B
                   ,arFERPAPage C
                   ,arFERPAEntity D
            WHERE   A.FERPACategoryID = B.FERPACategoryID
                    AND A.FERPACategoryID = C.FERPACategoryID
                    AND C.Resourceid = @ResourceId
                    AND A.FerpaEntityId = D.FerpaEntityId
                    AND A.studentid = @studentID;
        END;






GO
