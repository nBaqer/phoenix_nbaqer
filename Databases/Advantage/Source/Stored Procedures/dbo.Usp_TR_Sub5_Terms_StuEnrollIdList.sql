SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =========================================================================================================
-- Usp_TR_Sub5_Terms_StuEnrollIdList 
-- =========================================================================================================
CREATE PROCEDURE [dbo].[Usp_TR_Sub5_Terms_StuEnrollIdList]
    @StuEnrollIdList VARCHAR(MAX)
AS --SET @StuEnrollIdList = '0235B76B-B4AB-40C1-85C1-64ED042D1205,5A979811-C4B4-4A89-8696-6DE71E2EC3AE'


    DECLARE @StuEnrollCampusId UNIQUEIDENTIFIER;
    DECLARE @GradesFormat VARCHAR(50);
    DECLARE @GPAMethod VARCHAR(50);

    SET @StuEnrollCampusId = COALESCE((
                                        SELECT TOP 1
                                                ASE.CampusId
                                        FROM    arStuEnrollments AS ASE
                                        WHERE   ASE.StuEnrollId IN ( SELECT Val
                                                                     FROM   MultipleValuesForReportParameters(@StuEnrollIdList,',',1) )
                                      ),NULL);
    SET @GradesFormat = dbo.GetAppSettingValueByKeyName('GradesFormat',@StuEnrollCampusId);
    IF ( @GradesFormat IS NOT NULL )
        BEGIN
            SET @GradesFormat = LOWER(LTRIM(RTRIM(@GradesFormat)));
        END;
    SET @GPAMethod = dbo.GetAppSettingValueByKeyName('GPAMethod',@StuEnrollCampusId);
    IF ( @GPAMethod IS NOT NULL )
        BEGIN
            SET @GPAMethod = LOWER(LTRIM(RTRIM(@GPAMethod)));
        END;

    DECLARE @TermCourseDataVar TABLE
        (
         TermId UNIQUEIDENTIFIER
        ,CourseId UNIQUEIDENTIFIER
        ,Completed BIT
        );
	
  
    DECLARE @TermDataVar TABLE
        (
         PrgVerId UNIQUEIDENTIFIER
        ,PrgVerDescrip VARCHAR(100)
        ,PrgVersionTrackCredits BIT
        ,TermId UNIQUEIDENTIFIER
        ,TermDescription VARCHAR(100)
        ,TermStartDate DATETIME
        ,TermEndDate DATETIME
        ,CourseId UNIQUEIDENTIFIER
        ,CourseCode VARCHAR(100)
        ,CourseDescription VARCHAR(100)
        ,CourseCodeDescription VARCHAR(100)
        ,CourseCredits DECIMAL(18,2)
        ,MinVal DECIMAL(18,2)
        ,CourseScore DECIMAL(18,2)
        ,StuEnrollId UNIQUEIDENTIFIER
        ,Completed BIT
        ,CurrentScore DECIMAL(18,2)
        ,FinalScore DECIMAL(18,2)
        ,FinalGrade VARCHAR(10)
        ,WeightedAverage_GPA DECIMAL(18,2)
        ,SimpleAverage_GPA DECIMAL(18,2)
        ,TermGPA_Simple DECIMAL(18,2)
        ,TermGPA_Weighted DECIMAL(18,2)
        ,Term_CreditsAttempted DECIMAL(18,2)
        ,Term_CreditsEarned DECIMAL(18,2)
        ,termAverage DECIMAL(18,2)
        ,GrdBkWgtDetailsCount INT
        ,ClockHourProgram BIT
        ,DescripXTranscript NVARCHAR(250)
        ,GradesFormat VARCHAR(50)
        ,GPAMethod VARCHAR(50)
        ,RowNumber INTEGER
        ,RowNumber_RepeatedCourse INTEGER
        );


    INSERT  INTO @TermDataVar
            SELECT  dt.PrgVerId
                   ,dt.PrgVerDescrip
                   ,dt.PrgVersionTrackCredits
                   ,dt.TermId
                   ,dt.TermDescription
                   ,dt.TermStartDate
                   ,dt.TermEndDate
                   ,dt.CourseId
                   ,dt.CourseCode
                   ,dt.CourseDescription
                   ,dt.CourseCodeDescription
                   ,dt.CourseCredits
                   ,dt.MinVal
                   ,dt.CourseScore
                   ,dt.StuEnrollId
                   ,dt.Completed
                   ,dt.CurrentScore
                   ,dt.FinalScore
                   ,dt.FinalGrade
                   ,dt.WeightedAverage_GPA
                   ,dt.SimpleAverage_GPA
                   ,dt.TermGPA_Simple
                   ,dt.TermGPA_Weighted
                   ,dt.Term_CreditsAttempted
                   ,dt.Term_CreditsEarned
                   ,dt.termAverage
                   ,dt.GrdBkWgtDetailsCount
                   ,dt.ClockHourProgram
                   ,dt.DescripXTranscript
                   ,@GradesFormat AS GradesFormat
                   ,@GPAMethod AS GPAMethod
                   ,ROW_NUMBER() OVER ( PARTITION BY TermId,CourseId ORDER BY TermStartDate, TermEndDate, TermDescription ) AS RowNumber
                    -- In ordert filere repeated courses by double enrollment
                   ,ROW_NUMBER() OVER ( PARTITION BY CourseId ORDER BY TermStartDate, TermEndDate, TermDescription, CourseDescription ) AS RowNumber_RepeatedCourse
            FROM    (
                      SELECT DISTINCT
                                PV.PrgVerId AS PrgVerId
                               ,PV.PrgVerDescrip AS PrgVerDescrip
                               ,CASE WHEN ( PV.Credits > 0.0 ) THEN 1
                                     ELSE 0
                                END AS PrgVersionTrackCredits
                               ,T.TermId AS TermId
                               ,T.TermDescrip AS TermDescription
                               ,T.StartDate AS TermStartDate
                               ,T.EndDate AS TermEndDate
                               ,CS.ReqId AS CourseId
                               ,R.Code AS CourseCode
                               ,R.Descrip AS CourseDescription
                               ,'(' + R.Code + ')' + R.Descrip AS CourseCodeDescription
                               ,R.Credits AS CourseCredits
                               ,(
                                  SELECT    MIN(GCD.MinVal)
                                  FROM      arGradeScaleDetails GCD
                                  INNER JOIN arGradeSystemDetails GSD ON GSD.GrdSysDetailId = GCD.GrdSysDetailId
                                  WHERE     GSD.IsPass = 1
                                            AND GCD.GrdScaleId = CS.GrdScaleId
                                ) AS MinVal
                               ,RES.Score AS CourseScore
                               ,SE.StuEnrollId AS StuEnrollId
                               ,SCS.Completed AS Completed
                               ,SCS.CurrentScore AS CurrentScore
                               ,SCS.CurrentGrade AS CurrentGrade
                               ,SCS.FinalScore AS FinalScore
                               ,SCS.FinalGrade AS FinalGrade
                               ,( CASE WHEN (
                                              SELECT    SUM(SCS2.Count_WeightedAverage_Credits)
                                              FROM      syCreditSummary AS SCS2
                                              WHERE     SCS2.StuEnrollId = SE.StuEnrollId
                                                        AND SCS2.TermId = T.TermId
                                            ) > 0 THEN (
                                                         SELECT SUM(SCS2.Product_WeightedAverage_Credits_GPA) / SUM(SCS2.Count_WeightedAverage_Credits)
                                                         FROM   syCreditSummary AS SCS2
                                                         WHERE  SCS2.StuEnrollId = SE.StuEnrollId
                                                                AND SCS2.TermId = T.TermId
                                                       )
                                       ELSE 0
                                  END ) AS WeightedAverage_GPA
                               ,( CASE WHEN (
                                              SELECT    SUM(Count_SimpleAverage_Credits)
                                              FROM      syCreditSummary
                                              WHERE     StuEnrollId = SE.StuEnrollId
                                                        AND TermId = T.TermId
                                            ) > 0 THEN (
                                                         SELECT SUM(Product_SimpleAverage_Credits_GPA) / SUM(Count_SimpleAverage_Credits)
                                                         FROM   syCreditSummary
                                                         WHERE  StuEnrollId = SE.StuEnrollId
                                                                AND TermId = T.TermId
                                                       )
                                       ELSE 0
                                  END ) AS SimpleAverage_GPA
                               ,SCS.TermGPA_Simple AS TermGPA_Simple
                               ,SCS.TermGPA_Weighted AS TermGPA_Weighted
                               ,(
                                  SELECT    SUM(ISNULL(CreditsAttempted,0))
                                  FROM      syCreditSummary
                                  WHERE     TermId = T.TermId
                                            AND StuEnrollId = SE.StuEnrollId
                                ) AS Term_CreditsAttempted
                               ,(
                                  SELECT    SUM(ISNULL(CreditsEarned,0))
                                  FROM      syCreditSummary
                                  WHERE     TermId = T.TermId
                                            AND StuEnrollId = SE.StuEnrollId
                                ) AS Term_CreditsEarned
                               ,(
                                  SELECT TOP 1
                                            Average
                                  FROM      syCreditSummary
                                  WHERE     StuEnrollId = SE.StuEnrollId
                                            AND TermId = T.TermId
                                ) AS termAverage
                               ,(
                                  SELECT    COUNT(*) AS GrdBkWgtDetailsCount
                                  FROM      arGrdBkResults AS AGBR
                                  WHERE     AGBR.StuEnrollId = SE.StuEnrollId
                                            AND AGBR.ClsSectionId = RES.TestId
                                ) AS GrdBkWgtDetailsCount
                               ,CASE WHEN P.ACId = 5 THEN 'True'
                                     ELSE 'False'
                                END AS ClockHourProgram
                               ,ISNULL(ATES.DescripXTranscript,'') AS DescripXTranscript
                      FROM      arClassSections CS
                      INNER JOIN arResults RES ON CS.ClsSectionId = RES.TestId
                      INNER JOIN arStuEnrollments SE ON RES.StuEnrollId = SE.StuEnrollId
                      INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                      INNER JOIN arPrograms P ON P.ProgId = PV.ProgId
                      INNER JOIN arTerm T ON CS.TermId = T.TermId
                      INNER JOIN arReqs R ON CS.ReqId = R.ReqId
                      LEFT JOIN arTermEnrollSummary AS ATES ON ATES.TermId = T.TermId
                                                               AND ATES.StuEnrollId = SE.StuEnrollId
                      LEFT JOIN syCreditSummary SCS ON SE.StuEnrollId = SCS.StuEnrollId
                                                       AND T.TermId = SCS.TermId
                                                       AND R.ReqId = SCS.ReqId
                      WHERE     SE.StuEnrollId IN ( SELECT  Val
                                                    FROM    MultipleValuesForReportParameters(@StuEnrollIdList,',',1) )
                    -- Nota: 2016-01-05 JT: As per DE12275 Case 986512 Do not check for Completed but check for FinalsGPA is not null
                    --AND SCS.Completed = 1  
                    --AND SCS.FinalGPA IS NOT NULL
					-- Show the courses only if it has a grade   Letter or numenic always have GPAFinal not null 
					--AND (ISNULL(SCS.FinalScore, 0) > 0 OR ISNULL(SCS.FinalGrade, 0)
					--AND SCS.FinalGPA IS NOT NULL
                                AND (
                                      RES.GrdSysDetailId IS NOT NULL
                                      OR RES.Score IS NOT NULL
                                    )
                      UNION
                      SELECT DISTINCT
                                PV.PrgVerId AS PrgVerId
                               ,PV.PrgVerDescrip AS PrgVerDescrip
                               ,CASE WHEN ( PV.Credits > 0.0 ) THEN 1
                                     ELSE 0
                                END AS PrgVersionTrackCredits
                               ,T.TermId AS TermId
                               ,T.TermDescrip AS TermDescription
                               ,T.StartDate AS TermStartDate
                               ,T.EndDate AS TermEndDate
                               ,GBCR.ReqId AS CourseId
                               ,R.Code AS CourseCode
                               ,R.Descrip AS CourseDescrip
                               ,'(' + R.Code + ' ) ' + R.Descrip AS CourseCodeDescrip
                               ,R.Credits AS CourseCredits
                               ,(
                                  SELECT    MIN(GCD.MinVal)
                                  FROM      arGradeScaleDetails GCD
                                  INNER JOIN arGradeSystemDetails GSD ON GSD.GrdSysDetailId = GCD.GrdSysDetailId
                                  WHERE     GSD.IsPass = 1
                                ) AS MinVal
                               ,GBCR.Score AS CourseScore
                               ,SE.StuEnrollId AS StuEnrollId
                               ,SCS.Completed AS Completed
                               ,SCS.CurrentScore AS CurrentScore
                               ,SCS.CurrentGrade AS CurrentGrade
                               ,SCS.FinalScore AS FinalScore
                               ,SCS.FinalGrade AS FinalGrade
                               ,( CASE WHEN (
                                              SELECT    SUM(Count_WeightedAverage_Credits)
                                              FROM      syCreditSummary
                                              WHERE     StuEnrollId = SE.StuEnrollId
                                                        AND TermId = T.TermId
                                            ) > 0 THEN (
                                                         SELECT SUM(Product_WeightedAverage_Credits_GPA) / SUM(Count_WeightedAverage_Credits)
                                                         FROM   syCreditSummary
                                                         WHERE  StuEnrollId = SE.StuEnrollId
                                                                AND TermId = T.TermId
                                                       )
                                       ELSE 0
                                  END ) AS WeightedAverage_GPA
                               ,( CASE WHEN (
                                              SELECT    SUM(Count_SimpleAverage_Credits)
                                              FROM      syCreditSummary
                                              WHERE     StuEnrollId = SE.StuEnrollId
                                                        AND TermId = T.TermId
                                            ) > 0 THEN (
                                                         SELECT SUM(Product_SimpleAverage_Credits_GPA) / SUM(Count_SimpleAverage_Credits)
                                                         FROM   syCreditSummary
                                                         WHERE  StuEnrollId = SE.StuEnrollId
                                                                AND TermId = T.TermId
                                                       )
                                       ELSE 0
                                  END ) AS SimpleAverage_GPA
                               ,SCS.TermGPA_Simple AS TermGPA_Simple
                               ,SCS.TermGPA_Weighted AS TermGPA_Weighted
                               ,(
                                  SELECT    SUM(ISNULL(CreditsAttempted,0))
                                  FROM      syCreditSummary
                                  WHERE     TermId = T.TermId
                                            AND StuEnrollId = SE.StuEnrollId
                                ) AS Term_CreditsAttempted
                               ,(
                                  SELECT    SUM(ISNULL(CreditsEarned,0))
                                  FROM      syCreditSummary
                                  WHERE     TermId = T.TermId
                                            AND StuEnrollId = SE.StuEnrollId
                                ) AS Term_CreditsEarned
                               ,(
                                  SELECT TOP 1
                                            Average
                                  FROM      syCreditSummary
                                  WHERE     StuEnrollId = SE.StuEnrollId
                                            AND TermId = T.TermId
                                ) AS termAverage
                               ,(
                                  SELECT    COUNT(*) AS GrdBkWgtDetailsCount
                                  FROM      arGrdBkConversionResults
                                  WHERE     StuEnrollId = SE.StuEnrollId
                                            AND TermId = T.TermId
                                            AND ReqId = R.ReqId
                                ) AS GrdBkWgtDetailsCount
                               ,CASE WHEN P.ACId = 5 THEN 'True'
                                     ELSE 'False'
                                END AS ClockHourProgram
                               ,ISNULL(ATES.DescripXTranscript,'') AS DescripXTranscript
                      FROM      arTransferGrades GBCR
                      INNER JOIN arStuEnrollments SE ON GBCR.StuEnrollId = SE.StuEnrollId
                      INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                      INNER JOIN arPrograms P ON P.ProgId = PV.ProgId
                      INNER JOIN arTerm T ON GBCR.TermId = T.TermId
                      INNER JOIN arReqs R ON GBCR.ReqId = R.ReqId
                      LEFT JOIN arTermEnrollSummary AS ATES ON ATES.TermId = T.TermId
                                                               AND ATES.StuEnrollId = SE.StuEnrollId
                      LEFT JOIN syCreditSummary SCS ON SE.StuEnrollId = SCS.StuEnrollId
                                                       AND T.TermId = SCS.TermId
                                                       AND R.ReqId = SCS.ReqId
                      WHERE     SE.StuEnrollId IN ( SELECT  Val
                                                    FROM    MultipleValuesForReportParameters(@StuEnrollIdList,',',1) )
                    -- Nota: 2016-01-05 JT: As per DE12275 Case 986512 Do not check for Completed but check for FinalsGPA is not null
                    --AND SCS.Completed = 1  
                    --AND SCS.FinalGPA IS NOT NULL
					-- Show the courses only if it has a grade   Letter or numenic always have GPAFinal not null 
					--AND (ISNULL(SCS.FinalScore, 0) > 0 OR ISNULL(SCS.FinalGrade, 0)
					--AND SCS.FinalGPA IS NOT NULL
                                AND ( GBCR.GrdSysDetailId IS NOT NULL )
                    ) dt;


    SELECT  PrgVerId
           ,PrgVerDescrip
           ,PrgVersionTrackCredits
           ,TermId
           ,TermDescription
           ,TermStartDate
           ,TermEndDate
           ,CourseId
           ,CourseCode
           ,CourseDescription
           ,CourseCodeDescription
           ,CourseCredits
           ,MinVal
           ,CourseScore
           ,StuEnrollId
           ,Completed
           ,CurrentScore
           ,FinalScore
           ,FinalGrade
           ,WeightedAverage_GPA
           ,SimpleAverage_GPA
           ,TermGPA_Simple
           ,TermGPA_Weighted
           ,Term_CreditsAttempted
           ,Term_CreditsEarned
           ,termAverage
           ,GrdBkWgtDetailsCount
           ,ClockHourProgram
           ,DescripXTranscript
           ,GradesFormat
           ,GPAMethod
           ,RowNumber
           ,RowNumber_RepeatedCourse
    FROM    @TermDataVar
    WHERE   RowNumber_RepeatedCourse = 1   -- In ordert filere repeated courses by double enrollment
    ORDER BY PrgVerDescrip
           ,TermStartDate
           ,TermEndDate
           ,TermDescription
           ,FinalGrade DESC
           ,FinalScore DESC
           ,CourseCode;

-- =========================================================================================================
-- END  --  Usp_TR_Sub5_Terms_StuEnrollIdList 
-- =========================================================================================================


GO
