SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[USP_FL_GetExceptionReport]
AS
    SELECT  *
    FROM    syFameESPExceptionReport
    ORDER BY SSN
           ,moddate;




GO
