SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[usp_EdExpress_DeleteNotPostedParentRecordsWithNoChildRecords]
AS
    DELETE  FROM syEDExpNotPostPell_ACG_SMART_Teach_StudentTable
    WHERE   NOT EXISTS ( SELECT *
                         FROM   syEDExpNotPostPell_ACG_SMART_Teach_DisbursementTable d
                         WHERE  d.ParentId = syEDExpNotPostPell_ACG_SMART_Teach_StudentTable.ParentId );
	
	



GO
