SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
  CREATE PROCEDURE [dbo].[usp_highschool_getlist]
    @selectedIndex INT = 2
   ,@HsName VARCHAR(50) = NULL
   ,@InstitutionType INT = 0
  AS
    DECLARE @StatusIndex VARCHAR(10) = NULL;

    IF @selectedIndex = 0
        BEGIN
            SET @StatusIndex = 'Active';
        END;

    IF @selectedIndex = 1
        BEGIN
            SET @StatusIndex = 'Inactive';
        END;
   
    SELECT  CCT.HSCode
           ,CCT.HSId
           ,CCT.HSName
           ,ST.Status
           ,CCT.StatusId
    FROM    syInstitutions CCT
           ,syStatuses ST
    WHERE   CCT.StatusId = ST.StatusId
            AND LevelID = ( CASE @InstitutionType
                              WHEN 1 THEN 1
                              ELSE 2
                            END )
            AND (
                  @StatusIndex IS NULL
                  OR ST.Status = @StatusIndex
                )
            AND (
                  @HsName IS NULL
                  OR HSName LIKE '%' + @HsName + '%'
                )
    ORDER BY CCT.HSName; 

GO
