SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_WinterIPEDS_FallPartD_PTFTUG_GetList_Summary]
    @CampusId VARCHAR(50)
   ,@ProgId VARCHAR(4000) = NULL
   ,@StartDate DATETIME = NULL
   ,@EndDate DATETIME = NULL
   ,@DateRangeText VARCHAR(100) = NULL
   ,@OrderBy VARCHAR(100)
AS
    DECLARE @ReturnValue VARCHAR(100);
    DECLARE @ContinuingStartDate DATETIME;
    DECLARE @Value VARCHAR(50);
    SET @Value = (
                   SELECT   dbo.GetAppSettingValue(47,@CampusId)
                 );
    SET @ContinuingStartDate = @StartDate;
-- For Academic Year Reporter, the End Date should be 10/15/Cohort Year
-- and Start Date should be blank
    IF SUBSTRING(LOWER(@DateRangeText),1,10) = 'enrollment'
        BEGIN
            SET @EndDate = @StartDate; -- it will always be 10/15/cohort year example: for cohort year 2009, it is 10/15/2009
            SET @StartDate = @StartDate; -- it will always be 10/15/cohort year example: for cohort year 2009, it is 10/15/2009
        END;

    IF @ProgId IS NOT NULL
        BEGIN
            SELECT  @ReturnValue = COALESCE(@ReturnValue,'') + ProgDescrip + ','
            FROM    arPrograms t1
            WHERE   t1.ProgId IN ( SELECT   Val
                                   FROM     MultipleValuesForReportParameters(@ProgId,',',1) );
            SET @ReturnValue = SUBSTRING(@ReturnValue,1,LEN(@ReturnValue) - 1);
        END;
    ELSE
        BEGIN
            SELECT  @ReturnValue = COALESCE(@ReturnValue,'') + ProgDescrip + ','
            FROM    arPrograms t1
            WHERE   t1.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
                    AND t1.CampGrpId IN ( SELECT    CampGrpId
                                          FROM      syCmpGrpCmps
                                          WHERE     CampusId = @CampusId );
            SET @ReturnValue = SUBSTRING(@ReturnValue,1,LEN(@ReturnValue) - 1);
        END;

    CREATE TABLE #FallPartB4FTUG
        (
         RowNumber UNIQUEIDENTIFIER
        ,NonDegCertSeeking VARCHAR(10)
        ,NonDegCertSeekingCount INT
        ,FirstTimeAndUG VARCHAR(10)
        ,FirstTimeAndUGCount INT
        ,StudentId UNIQUEIDENTIFIER
        ); 

    INSERT  INTO #FallPartB4FTUG
            SELECT  NEWID() AS RowNumber
                   ,CASE WHEN NonDegCertSeeking >= 1 THEN 'X'
                         ELSE ''
                    END AS NonDegCertSeekingTime
                   ,CASE WHEN NonDegCertSeeking >= 1 THEN 1
                         ELSE 0
                    END AS NonDegCertSeekingCount
                   ,CASE WHEN FirstTimeAndUG >= 1 THEN 'X'
                         ELSE ''
                    END AS FirstTimeAndUG
                   ,CASE WHEN FirstTimeAndUG >= 1 THEN 1
                         ELSE 0
                    END AS FirstTimeAndUGCount
                   ,StudentId
            FROM    (
                      SELECT 
				--(Select Count(*) from arStuEnrollments SQ1,adDegCertSeeking  SQ2
				--	where 
				--		SQ1.StuEnrollId = t2.StuEnrollId and 
				--		SQ1.StartDate<=@EndDate and 
				--		SQ1.degcertseekingid = SQ2.DegCertSeekingId  and
				--		SQ2.IPEDSValue=10
				--) 
                                1 AS NonDegCertSeeking
                               ,
				
				 --When Academic year is chosen since it is an as of date and if both the enrollments 
				 --started before that date then student is not marked under First time. This option is pretty straightforward.
                                CASE WHEN ( SUBSTRING(LOWER(@DateRangeText),1,10) = 'enrollment' ) THEN --IF Academic Program
                                          (
                                            SELECT  COUNT(*)
                                            FROM    arStuEnrollments SQ1
                                                   ,adDegCertSeeking SQ2
                                                   ,arPrgVersions SQ3
                                                   ,arProgTypes SQ4
                                            WHERE   SQ1.StuEnrollId = t2.StuEnrollId
                                                    AND SQ3.PrgVerId = SQ1.PrgVerId
                                                    AND SQ3.ProgTypId = SQ4.ProgTypId
                                                    AND SQ1.StartDate <= @EndDate
                                                    AND SQ1.degcertseekingid = SQ2.DegCertSeekingId
                                                    AND SQ2.IPEDSValue = 11
                                                    AND SQ4.IPEDSValue = 58
                                                    AND
							--When Academic year is chosen since it is an as of date and 
							--if both the enrollments started before that date then student is not marked under First time
                                                    SQ1.StudentId NOT IN ( SELECT DISTINCT
                                                                                    StudentId
                                                                           FROM     (
                                                                                      SELECT    StudentId
                                                                                               ,COUNT(*) AS RowCounter
                                                                                      FROM      arStuEnrollments t9
                                                                                      WHERE     t9.StartDate <= @EndDate
                                                                                      GROUP BY  StudentId
                                                                                      HAVING    COUNT(*) > 1
                                                                                    ) dtMultipleEnrollments )
                                          )
                                     ELSE (
                                            SELECT  COUNT(*)
                                            FROM    arStuEnrollments SQ1
                                                   ,adDegCertSeeking SQ2
                                                   ,arPrgVersions SQ3
                                                   ,arProgTypes SQ4
                                            WHERE   SQ1.StuEnrollId = t2.StuEnrollId
                                                    AND SQ3.PrgVerId = SQ1.PrgVerId
                                                    AND SQ3.ProgTypId = SQ4.ProgTypId
                                                    AND (
                                                          SQ1.StartDate >= @StartDate
                                                          AND SQ1.StartDate <= @EndDate
                                                        )
                                                    AND SQ1.degcertseekingid = SQ2.DegCertSeekingId
                                                    AND SQ2.IPEDSValue = 11
                                                    AND SQ4.IPEDSValue = 58
                                          )
                                END AS FirstTimeAndUG
                               ,t1.StudentId
                      FROM      arStudent t1
                      INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                      INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                      INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                                   AND t6.SysStatusId NOT IN ( 8 )
                      INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                      INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                      INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                      LEFT JOIN arAttendTypes t10 ON t2.AttendTypeId = t10.AttendTypeId
                      INNER JOIN adCitizenships t12 ON t1.Citizen = t12.CitizenshipId
                      INNER JOIN adDegCertSeeking t17 ON t2.DegCertSeekingId = t17.DegCertSeekingId
                      WHERE     t2.CampusId = LTRIM(RTRIM(@CampusId))
                                AND (
                                      @ProgId IS NULL
                                      OR t8.ProgId IN ( SELECT  Val
                                                        FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                    )
                                AND t9.IPEDSValue = 58
                                AND ----- Undergraduate
                                t10.IPEDSValue IN ( 61,62 )
                                AND ----PartTime And Full Time
                                t17.IPEDSValue = 10 --Bring in only non-degree cert seeking id
                                AND t2.StartDate <= @EndDate
                                AND StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
						( SELECT    t1.StuEnrollId
                          FROM      arStuEnrollments t1
                                   ,syStatusCodes t2
                          WHERE     t1.StatusCodeId = t2.StatusCodeId
                                    AND StartDate <= @EndDate
                                    AND -- Student started before the end date range
                                    LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                    AND (
                                          @ProgId IS NULL
                                          OR t8.ProgId IN ( SELECT  Val
                                                            FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                        )
                                    AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                    AND (
                                          t1.DateDetermined < @StartDate
                                          OR ExpGradDate < @StartDate
                                          OR LDA < @StartDate
                                        ) )
                                                  	-- If Student is enrolled in only program version and if that program version 
						-- happens to be a continuing ed program exclude the student
                                AND t2.StudentId NOT IN ( SELECT DISTINCT
                                                                    StudentId
                                                          FROM      (
                                                                      SELECT    StudentId
                                                                               ,COUNT(*) AS RowCounter
                                                                      FROM      arStuEnrollments
                                                                      WHERE     PrgVerId IN ( SELECT    PrgVerId
                                                                                              FROM      arPrgVersions
                                                                                              WHERE     IsContinuingEd = 1 )
                                                                      GROUP BY  StudentId
                                                                      HAVING    COUNT(*) = 1
                                                                    ) dtStudent_ContinuingEd )
                                AND t2.StuEnrollId IN (
                                --SELECT TOP 1
                                --        StuEnrollId
                                --FROM    arStuEnrollments A1 ,
                                --        arPrgVersions A2 ,
                                --        arProgTypes A3
                                --WHERE   A1.PrgVerId = A2.PrgVerId
                                --        AND A2.ProgTypId = A3.ProgTypId
                                --        AND A1.StudentId = t1.StudentId
                                --        AND A3.IPEDSValue = 58
                                --ORDER BY StartDate ,
                                --        EnrollDate ASC
                                SELECT  dbo.GetIPEDS_Spring_GetEnrollmentfromMultipleEnrollment(@CampusId,@ProgId,@EndDate,'undergraduate',t1.StudentId) )
                    ) dt;

    IF ( SUBSTRING(LOWER(@DateRangeText),1,10) <> 'enrollment' ) -- Declare Cursor only for Program Reporter
        BEGIN
            DECLARE @StudentId_Cursor UNIQUEIDENTIFIER
               ,@MultipleEnrollments_Count INT
               ,@MultipleEnrollmentsInDateRange_Count INT;
            DECLARE getUsers_Cursor CURSOR
            FOR
                SELECT  StudentId
                FROM    #FallPartB4FTUG;
            OPEN getUsers_Cursor;
            FETCH NEXT FROM getUsers_Cursor
	INTO @StudentId_Cursor;
            WHILE @@FETCH_STATUS = 0
                BEGIN

                    SET @MultipleEnrollments_Count = (
                                                       SELECT DISTINCT
                                                                RowCounter
                                                       FROM     (
                                                                  --Multiple enrollments that falls between 08/01 - 10/31
                                                              SELECT    StudentId
                                                                       ,COUNT(*) AS RowCounter
                                                              FROM      arStuEnrollments t9
                                                              WHERE     t9.StudentId = @StudentId_Cursor
                                                              GROUP BY  StudentId
                                                              HAVING    COUNT(*) > 1
                                                                ) dtMultipleEnrollments
                                                     );

                    SET @MultipleEnrollmentsInDateRange_Count = (
                                                                  SELECT DISTINCT
                                                                            RowCounter
                                                                  FROM      (
                                                                              --Multiple enrollments that falls between 08/01 - 10/31
                                                              SELECT    StudentId
                                                                       ,COUNT(*) AS RowCounter
                                                              FROM      arStuEnrollments t9
                                                              WHERE     t9.StudentId = @StudentId_Cursor
                                                                        AND t9.StartDate >= @StartDate
                                                                        AND t9.StartDate <= @EndDate
                                                              GROUP BY  StudentId
                                                              HAVING    COUNT(*) > 1
                                                                            ) dtMultipleEnrollments
                                                                );
										
		-- If student has multiple enrollments and if both enrollments are with in the date range
                    IF ( @MultipleEnrollments_Count >= 1 )
                        AND ( @MultipleEnrollmentsInDateRange_Count >= 1 )
                        BEGIN
                            UPDATE  #FallPartB4FTUG
                            SET     FirstTimeAndUG = 'X'
                                   ,FirstTimeAndUGCount = 1
                            WHERE   StudentId = @StudentId_Cursor;
                        END;
	
		--If one of the enrollment’s start date is within the report date range and the other one is before this report date range then the student is not marked under the First time column.
                    IF ( @MultipleEnrollments_Count >= 1 )
                        AND ( @MultipleEnrollmentsInDateRange_Count <= 1 )
                        BEGIN
                            UPDATE  #FallPartB4FTUG
                            SET     FirstTimeAndUG = ''
                                   ,FirstTimeAndUGCount = 0
                            WHERE   StudentId = @StudentId_Cursor;
                        END;
		
                    FETCH NEXT FROM getUsers_Cursor
		INTO @StudentId_Cursor;
                END;
            CLOSE getUsers_Cursor;
            DEALLOCATE getUsers_Cursor;
        END;
    SELECT  SUM(NonDegCertSeekingCount) AS NonDegCertSeekingCount
           ,SUM(FirstTimeAndUGCount) AS FirstTimeAndUGCount
    FROM    (
              SELECT    CASE ( NonDegCertSeeking )
                          WHEN 'X' THEN 1
                          ELSE 0
                        END NonDegCertSeekingCount
                       ,CASE ( FirstTimeAndUG )
                          WHEN 'X' THEN 1
                          ELSE 0
                        END FirstTimeAndUGCount
              FROM      #FallPartB4FTUG
            ) dt;
    DROP TABLE #FallPartB4FTUG;



GO
