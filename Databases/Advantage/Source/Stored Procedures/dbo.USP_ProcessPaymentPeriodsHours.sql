SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--================================================================================================= 
-- USP_ProcessPaymentPeriodsHours 
--================================================================================================= 
-- ============================================= 
-- Author:		Ginzo, John 
-- Create date: 11/17/2014 
-- ============================================= 
CREATE PROCEDURE [dbo].[USP_ProcessPaymentPeriodsHours]
AS
    BEGIN
        -- SET NOCOUNT ON added to prevent extra result sets from 
        -- interfering with SELECT statements. 
        SET NOCOUNT ON;


        BEGIN TRAN PAYMENTPERIODTRANSACTION;

        ----------------------------------------------------------------- 
        --Table to store Student population 
        ----------------------------------------------------------------- 
        DECLARE @MasterStudentPopulation TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,ClsSectionId VARCHAR(250)
               ,MaxDate DATETIME
               ,ScheduledHours DECIMAL
               ,MakeUpHours DECIMAL
               ,ActualHours DECIMAL
               ,TermId UNIQUEIDENTIFIER
               ,PrgVerId UNIQUEIDENTIFIER
               ,PrgVerCode VARCHAR(250)
               ,StartDate DATETIME
               ,EndDate DATETIME
            );

        DECLARE @MasterStudentPopulationRunningTotal TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,ClsSectionId VARCHAR(250)
               ,PrgVerId UNIQUEIDENTIFIER
               ,MeetingDate DATETIME
               ,TermId UNIQUEIDENTIFIER
               ,ScheduledHoursRunningTotal DECIMAL(18, 2)
               ,ActualHoursRunningTotal DECIMAL(18, 2)
               ,IsExcused BIT
               ,ExcusedHours DECIMAL(18, 2)
               ,ScheduledDays DECIMAL(18, 2)
               ,ActualDays DECIMAL(18, 2)
            );


        ----------------------------------------------------------------- 
        -- TABLE FOR HOURS DATA 
        ----------------------------------------------------------------- 
        DECLARE @HoursScheduledActual TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,PrgVerId UNIQUEIDENTIFIER
               ,ScheduledHours DECIMAL
               ,ActualHours DECIMAL
               ,MakeUpHours DECIMAL
            );


        DECLARE @HoursExcused TABLE
            (
                PmtPeriodId UNIQUEIDENTIFIER
               ,StuEnrollId UNIQUEIDENTIFIER
               ,PrgVerId UNIQUEIDENTIFIER
               ,ExcusedHours DECIMAL(18, 2)
            );

        DECLARE @TransactionDates TABLE
            (
                PmtPeriodId UNIQUEIDENTIFIER
               ,StuEnrollId UNIQUEIDENTIFIER
               ,TransactionDate DATETIME
            );
        ----------------------------------------------------------------- 
        -- Insert student population into table 
        ----------------------------------------------------------------- 
        INSERT INTO @MasterStudentPopulation
                    SELECT     DISTINCT SE.StuEnrollId
                                       ,CS.ClsSectionId
                                       ,MD.MaxDate
                                       ,SH.ScheduledHours
                                       ,MUH.MakeUpHours
                                       ,AH.ActualHours
                                       ,TT.TermId
                                       ,PV.PrgVerId
                                       ,PV.PrgVerCode
                                       ,TT.StartDate
                                       ,TT.EndDate
                    FROM       dbo.arStuEnrollments SE
                    INNER JOIN dbo.syStudentAttendanceSummary SA ON SE.StuEnrollId = SA.StuEnrollId
                    INNER JOIN (
                               SELECT   StuEnrollId
                                       ,MAX(ActualRunningScheduledDays) AS ScheduledHours
                               FROM     dbo.syStudentAttendanceSummary
                               GROUP BY StuEnrollId
                               ) SH ON SA.StuEnrollId = SH.StuEnrollId
                    INNER JOIN (
                               SELECT   StuEnrollId
                                       ,MAX(ActualRunningPresentDays) AS ActualHours
                               FROM     dbo.syStudentAttendanceSummary
                               GROUP BY StuEnrollId
                               ) AH ON SE.StuEnrollId = AH.StuEnrollId
                    INNER JOIN (
                               SELECT   StuEnrollId
                                       ,MAX(ActualRunningMakeupDays) AS MakeUpHours
                               FROM     dbo.syStudentAttendanceSummary
                               GROUP BY StuEnrollId
                               ) MUH ON SE.StuEnrollId = MUH.StuEnrollId
                    INNER JOIN (
                               SELECT   StuEnrollId
                                       ,MAX(StudentAttendedDate) AS MaxDate
                               FROM     dbo.syStudentAttendanceSummary
                               GROUP BY StuEnrollId
                               ) MD ON SE.StuEnrollId = MD.StuEnrollId
                    INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                    INNER JOIN dbo.arClassSections CS ON SA.ClsSectionId = CS.ClsSectionId
                    INNER JOIN dbo.arTerm TT ON CS.TermId = TT.TermId
                    INNER JOIN dbo.syStatusCodes SC ON SE.StatusCodeId = SC.StatusCodeId
                    INNER JOIN dbo.syStatuses ST ON PV.StatusId = ST.StatusId
                    WHERE      SC.SysStatusId IN ( 9, 20 )
								--removed due to this condition being required for by term or course charges and auto post is for payment period charging
                               --AND 1 = dbo.UDF_ShouldClassBeChargedOnThisEnrollment(SE.StuEnrollId, CS.ClsSectionId) --Added by Troy as part of fix for US8005 
                               AND SE.DisableAutoCharge <> 1 --Exclude enrollments with auto charge disabled  
                               AND PV.ProgramRegistrationType = 0
                    --AND pv.PrgVerId='5E8BFC2C-FE62-4220-BDC6-51C7941F5AE0' 
                    --AND se.StuEnrollId='71A812F7-972D-4AB6-971C-3CFD02A3086B' 

                    UNION
                    SELECT     DISTINCT SE.StuEnrollId
                                       ,CS.ClsSectionId
                                       ,MD.MaxDate
                                       ,SH.ScheduledHours
                                       ,MUH.MakeUpHours
                                       ,AH.ActualHours
                                       ,TT.TermId
                                       ,PV.PrgVerId
                                       ,PV.PrgVerCode
                                       ,TT.StartDate
                                       ,TT.EndDate
                    FROM       dbo.arStuEnrollments SE
                    INNER JOIN dbo.syStudentAttendanceSummary SA ON SE.StuEnrollId = SA.StuEnrollId
                    INNER JOIN (
                               SELECT   StuEnrollId
                                       ,MAX(ActualRunningScheduledDays) AS ScheduledHours
                               FROM     dbo.syStudentAttendanceSummary
                               GROUP BY StuEnrollId
                               ) SH ON SA.StuEnrollId = SH.StuEnrollId
                    INNER JOIN (
                               SELECT   StuEnrollId
                                       ,MAX(ActualRunningPresentDays) AS ActualHours
                               FROM     dbo.syStudentAttendanceSummary
                               GROUP BY StuEnrollId
                               ) AH ON SE.StuEnrollId = AH.StuEnrollId
                    INNER JOIN (
                               SELECT   StuEnrollId
                                       ,MAX(ActualRunningMakeupDays) AS MakeUpHours
                               FROM     dbo.syStudentAttendanceSummary
                               GROUP BY StuEnrollId
                               ) MUH ON SE.StuEnrollId = MUH.StuEnrollId
                    INNER JOIN (
                               SELECT   StuEnrollId
                                       ,MAX(StudentAttendedDate) AS MaxDate
                               FROM     dbo.syStudentAttendanceSummary
                               GROUP BY StuEnrollId
                               ) MD ON SE.StuEnrollId = MD.StuEnrollId
                    INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                    INNER JOIN dbo.arTerm TT ON TT.ProgramVersionId = PV.PrgVerId
                    INNER JOIN dbo.arProgVerDef pvd ON pvd.PrgVerId = PV.PrgVerId
                    INNER JOIN dbo.arClassSections CS ON CS.ProgramVersionDefinitionId = pvd.ProgVerDefId
                    INNER JOIN dbo.syStatusCodes SC ON SE.StatusCodeId = SC.StatusCodeId
                    INNER JOIN dbo.syStatuses ST ON PV.StatusId = ST.StatusId
                    WHERE      SC.SysStatusId IN ( 9, 20 )
							   --removed due to this condition being required for by term or course charges and auto post is for payment period charging
                               --AND 1 = dbo.UDF_ShouldClassBeChargedOnThisEnrollment(SE.StuEnrollId, CS.ClsSectionId) --Added by Troy as part of fix for US8005 
                               AND SE.DisableAutoCharge <> 1 --Exclude enrollments with auto charge disabled  
                               AND PV.ProgramRegistrationType = 1;
        --AND pv.PrgVerId='5E8BFC2C-FE62-4220-BDC6-51C7941F5AE0' 
        --AND se.StuEnrollId='71A812F7-972D-4AB6-971C-3CFD02A3086B' 


        -- Get Running Totals for Current Term 
        INSERT INTO @MasterStudentPopulationRunningTotal
                    SELECT     DISTINCT SAS.StuEnrollId
                                       ,SAS.ClsSectionId
                                       ,SE.PrgVerId
                                       ,SAS.StudentAttendedDate
                                       ,(
                                        SELECT     TOP 1 TermId
                                        FROM       arResults R
                                        INNER JOIN arClassSections CS ON R.TestId = CS.ClsSectionId
                                        WHERE      R.StuEnrollId = SAS.StuEnrollId
                                                   AND CS.ClsSectionId = ClsSectionId
                                        ) AS termId
                                       ,SAS.ActualRunningScheduledDays
                                       ,SAS.AdjustedPresentDays
                                       ,SAS.IsExcused
                                       ,( CASE WHEN IsExcused = 1 THEN ( SAS.ScheduledDays - SAS.ActualDays )
                                               ELSE 0
                                          END
                                        ) AS ExcusedHours
                                       ,SAS.ScheduledDays
                                       ,SAS.ActualDays
                    FROM       syStudentAttendanceSummary SAS
                    INNER JOIN dbo.arStuEnrollments SE ON SAS.StuEnrollId = SE.StuEnrollId
                    WHERE      SAS.StuEnrollId IN (
                                                  SELECT DISTINCT StuEnrollId
                                                  FROM   @MasterStudentPopulation
                                                  )
                    ORDER BY   SAS.StuEnrollId
                              ,StudentAttendedDate;


        IF @@ERROR <> 0
            BEGIN
                ROLLBACK TRAN PAYMENTPERIODTRANSACTION;
                RAISERROR('Error creating Student Population.', 16, 1);
                RETURN;
            END;






        ----------------------------------------------------------------- 
        -- insert hours data into table 
        ----------------------------------------------------------------- 
        INSERT INTO @HoursScheduledActual
                    SELECT   StuEnrollId
                            ,PrgVerId
                            ,ScheduledHours
                            ,ActualHours
                            ,MakeUpHours
                    FROM     @MasterStudentPopulation
                    GROUP BY StuEnrollId
                            ,PrgVerId
                            ,ScheduledHours
                            ,ActualHours
                            ,MakeUpHours;


        IF @@ERROR <> 0
            BEGIN
                ROLLBACK TRAN PAYMENTPERIODTRANSACTION;
                RAISERROR('Error creating hours data.', 16, 1);
                RETURN;
            END;




        ----------------------------------------------------------------------------------- 
        -- Cursor to handle Excused Hours 
        ----------------------------------------------------------------------------------- 
        DECLARE @pmtperiodid UNIQUEIDENTIFIER;
        DECLARE @CumulativeValue DECIMAL;
        DECLARE @StuEnrollId UNIQUEIDENTIFIER;
        DECLARE @NextCumulativeValue DECIMAL;




        DECLARE db_cursor CURSOR FOR
            SELECT     p.PmtPeriodId
                      ,p.CumulativeValue
                      ,e.StuEnrollId
                      ,(
                       SELECT   TOP 1 child.CumulativeValue
                       FROM     saPmtPeriods child
                       WHERE    child.IncrementId = p.IncrementId
                                AND child.PeriodNumber = p.PeriodNumber + 1
                       ORDER BY p.PeriodNumber ASC
                               ,p.CumulativeValue ASC
                       ) AS NextCumulativeValue
            FROM       dbo.saPmtPeriods p
            INNER JOIN dbo.saIncrements I ON p.IncrementId = I.IncrementId
            INNER JOIN dbo.saBillingMethods B ON I.BillingMethodId = B.BillingMethodId
            INNER JOIN dbo.arPrgVersions prg ON B.BillingMethodId = prg.BillingMethodId
            INNER JOIN dbo.arStuEnrollments e ON prg.PrgVerId = e.PrgVerId
            WHERE      I.IncrementType = 0
            ORDER BY   e.StuEnrollId
                      ,e.PrgVerId
                      ,I.IncrementId
                      ,p.PeriodNumber;

        DECLARE @FloorValue DECIMAL;
        SET @FloorValue = 0;

        DECLARE @currentEnrollId UNIQUEIDENTIFIER;
        SET @currentEnrollId = NEWID();

        DECLARE @curCumValue DECIMAL;
        SET @curCumValue = 0;

        OPEN db_cursor;

        FETCH NEXT FROM db_cursor
        INTO @pmtperiodid
            ,@CumulativeValue
            ,@StuEnrollId
            ,@NextCumulativeValue;


        WHILE @@FETCH_STATUS = 0
            BEGIN

                IF @StuEnrollId <> @currentEnrollId
                    BEGIN
                        SET @FloorValue = 0;
                        SET @curCumValue = @CumulativeValue;
                        SET @currentEnrollId = @StuEnrollId;
                    END;
                ELSE
                    BEGIN
                        SET @FloorValue = @curCumValue;
                    END;


                --SELECT @FloorValue,@CumulativeValue,@currentEnrollId,@StuEnrollId 

                INSERT INTO @HoursExcused
                            SELECT   @pmtperiodid AS pmtperiodid
                                    ,StuEnrollId AS StuEnrollId
                                    ,PrgVerId
                                    ,SUM(ExcusedHours) AS ExcusedHours
                            FROM     @MasterStudentPopulationRunningTotal
                            WHERE    StuEnrollId = @StuEnrollId
                                     AND ActualHoursRunningTotal > @FloorValue
                                     AND (
                                         ( ActualHoursRunningTotal <= @CumulativeValue )
                                         OR ( ActualHoursRunningTotal
                                     BETWEEN @CumulativeValue AND @NextCumulativeValue
                                            )
                                         OR ( ActualHoursRunningTotal >= @CumulativeValue )
                                         )
                            GROUP BY StuEnrollId
                                    ,PrgVerId;



                IF @@ERROR <> 0
                    BEGIN
                        ROLLBACK TRAN PAYMENTPERIODTRANSACTION;
                        RAISERROR('Error creating excused hours data.', 16, 1);
                        RETURN;
                    END;



                FETCH NEXT FROM db_cursor
                INTO @pmtperiodid
                    ,@CumulativeValue
                    ,@StuEnrollId
                    ,@NextCumulativeValue;

            END;

        CLOSE db_cursor;
        DEALLOCATE db_cursor;
        ----------------------------------------------------------------------------------- 

        --for testing 
        --SELECT * FROM @MasterStudentPopulation 
        --SELECT * FROM @MasterStudentPopulationRunningTotal 
        --SELECT * FROM @HoursScheduledActual	 
        --SELECT * FROM @HoursExcused 




        DECLARE @BatchNumber INT;
        SET @BatchNumber = (
                           SELECT ISNULL(MAX(PmtPeriodBatchHeaderId), 0) + 1
                           FROM   saPmtPeriodBatchHeaders
                           );

        INSERT INTO saPmtPeriodBatchHeaders (
                                            BatchName
                                            )
                    SELECT 'Batch ' + CAST(@BatchNumber AS VARCHAR(20)) + ' (Hours) - ' + LEFT(CONVERT(VARCHAR, GETDATE(), 101), 10) + ' '
                           + LEFT(CONVERT(VARCHAR, GETDATE(), 108), 10);


        IF @@ERROR <> 0
            BEGIN
                ROLLBACK TRAN PAYMENTPERIODTRANSACTION;
                RAISERROR('Error creating batch header.', 16, 1);
                RETURN;
            END;

        DECLARE @InsertedHeaderId INT;
        SET @InsertedHeaderId = SCOPE_IDENTITY();





        INSERT INTO saPmtPeriodBatchItems (
                                          PmtPeriodId
                                         ,PmtPeriodBatchHeaderId
                                         ,StuEnrollId
                                         ,ChargeAmount
                                         ,CreditsHoursValue
                                         ,TransactionReference
                                         ,TransactionDescription
                                         ,ModUser
                                         ,ModDate
                                         ,TermId
                                          )
                    SELECT     DISTINCT ( p.PmtPeriodId )
                                       ,@InsertedHeaderId
                                       ,e.StuEnrollId
                                       ,p.ChargeAmount
                                       ,( CASE WHEN i.IncrementType = 0 THEN CAE.ActualHours + ( ISNULL(HE.ExcusedHours,0) * i.ExcAbscenesPercent )
                                               WHEN i.IncrementType = 1 THEN CAE.ScheduledHours
                                               ELSE 0
                                          END
                                        ) AS CreditsHoursValue
                                       ,'Payment Period ' + CAST(p.PeriodNumber AS VARCHAR(5)) AS TransactionReference
                                       ,tc.TransCodeDescrip + ': ' + CAST(p.CumulativeValue AS VARCHAR(20)) + ' ' + i.IncrementName AS TransactionDescription
                                       ,'AutoPost' AS ModUser
                                       ,GETDATE() AS ModDate
                                       ,( CASE WHEN i.IncrementType = 0 THEN
                                               (
                                               SELECT TOP 1 TermId
                                               FROM   @MasterStudentPopulationRunningTotal
                                               -- INNER JOIN dbo.arPrgVersions pv ON pv.PrgVerId = [@MasterStudentPopulationRunningTotal].PrgVerId
                                               WHERE  StuEnrollId = e.StuEnrollId
                                                      -- AND ( pv.ProgramRegistrationType = 1 )
                                                      AND ActualHoursRunningTotal >= p.CumulativeValue
                                               )
                                               WHEN i.IncrementType = 1 THEN
                                               (
                                               SELECT TOP 1 TermId
                                               FROM   @MasterStudentPopulationRunningTotal
                                               --INNER JOIN dbo.arPrgVersions pv ON pv.PrgVerId = [@MasterStudentPopulationRunningTotal].PrgVerId
                                               WHERE  StuEnrollId = e.StuEnrollId
                                                      --AND ( pv.ProgramRegistrationType = 1 )
                                                      AND ScheduledHoursRunningTotal >= p.CumulativeValue
                                               )
                                               ELSE NULL
                                          END
                                        ) AS TermId
                    FROM       saPmtPeriods p
                    INNER JOIN dbo.saIncrements i ON p.IncrementId = i.IncrementId
                    INNER JOIN dbo.saBillingMethods B ON i.BillingMethodId = B.BillingMethodId
                    INNER JOIN dbo.arPrgVersions prg ON B.BillingMethodId = prg.BillingMethodId
                    INNER JOIN dbo.arStuEnrollments e ON prg.PrgVerId = e.PrgVerId
                    --INNER JOIN dbo.arStudent st ON e.StudentId = st.StudentId
                    INNER JOIN @HoursScheduledActual CAE ON e.StuEnrollId = CAE.StuEnrollId
                    LEFT JOIN  @HoursExcused HE ON p.PmtPeriodId = HE.PmtPeriodId
                                                   AND e.StuEnrollId = HE.StuEnrollId
                                                   AND e.PrgVerId = HE.PrgVerId
                    LEFT JOIN  dbo.saTransCodes tc ON tc.TransCodeId = p.TransactionCodeId
                    WHERE      B.BillingMethod = 3
                               AND e.StartDate >= i.EffectiveDate
                               AND ( CASE WHEN i.IncrementType = 0 THEN CAE.ActualHours + ( ISNULL(HE.ExcusedHours,0) * i.ExcAbscenesPercent ) + CAE.MakeUpHours
                                          WHEN i.IncrementType = 1 THEN CAE.ScheduledHours
                                          ELSE 0
                                     END
                                   ) >= p.CumulativeValue
                               AND e.StuEnrollId NOT IN (
                                                        SELECT StuEnrollId
                                                        FROM   dbo.saTransactions
                                                        WHERE  StuEnrollId = e.StuEnrollId
                                                               AND PmtPeriodId = p.PmtPeriodId
                                                               OR p.PeriodNumber = PaymentPeriodNumber
                                                        )
                               AND e.StuEnrollId NOT IN (
                                                        SELECT     i.StuEnrollId
                                                        FROM       dbo.saPmtPeriodBatchItems i
                                                        INNER JOIN saPmtPeriodBatchHeaders h ON i.PmtPeriodBatchHeaderId = h.PmtPeriodBatchHeaderId
                                                        WHERE      StuEnrollId = e.StuEnrollId
                                                                   AND PmtPeriodId = p.PmtPeriodId
                                                                   AND h.IsPosted = 0
                                                        );


        IF @@ERROR <> 0
            BEGIN
                ROLLBACK TRAN PAYMENTPERIODTRANSACTION;
                RAISERROR('Error creating batch items.', 16, 1);
                RETURN;
            END;



        ----------------------------------------------------------------- 
        -- UPDATE Batch Item count in header table 
        ----------------------------------------------------------------- 
        UPDATE saPmtPeriodBatchHeaders
        SET    BatchItemCount = (
                                SELECT     COUNT(*)
                                FROM       saPmtPeriodBatchHeaders h
                                INNER JOIN saPmtPeriodBatchItems i ON h.PmtPeriodBatchHeaderId = i.PmtPeriodBatchHeaderId
                                WHERE      h.PmtPeriodBatchHeaderId = @InsertedHeaderId
                                )
        WHERE  PmtPeriodBatchHeaderId = @InsertedHeaderId;

        IF @@ERROR <> 0
            BEGIN
                ROLLBACK TRAN PAYMENTPERIODTRANSACTION;
                RAISERROR('Error updating batch count.', 16, 1);
                RETURN;
            END;
        ----------------------------------------------------------------- 

        ----------------------------------------------------------------- 
        -- GET AutoPost config setting 
        ----------------------------------------------------------------- 
        DECLARE @IsAutoPost VARCHAR(50);
        SET @IsAutoPost = (
                          SELECT     TOP 1 v.Value
                          FROM       dbo.syConfigAppSettings c
                          INNER JOIN dbo.syConfigAppSetValues v ON c.SettingId = v.SettingId
                          WHERE      c.KeyName = 'PaymentPeriodAutoPost'
                          );
        ----------------------------------------------------------------- 



        ------------------------------------------------------------------- 
        ---- INSERT Transactions from Batch 
        ------------------------------------------------------------------- 
        IF @IsAutoPost = 'yes'
            BEGIN

                IF OBJECTPROPERTY(OBJECT_ID('Trigger_Insert_Check_saTransactions'), 'IsTrigger') = 1
                    BEGIN
                        ALTER TABLE dbo.saTransactions DISABLE TRIGGER Trigger_Insert_Check_saTransactions;
                    END;



                INSERT INTO @TransactionDates (
                                              PmtPeriodId
                                             ,StuEnrollId
                                             ,TransactionDate
                                              )
                            SELECT      ppCumulative.PmtPeriodId
                                       ,ppCumulative.StuEnrollId
                                       ,transDate.StudentAttendedDate
                            FROM        (
                                        SELECT     DISTINCT pp.PmtPeriodId
                                                           ,StuEnrollId
                                                           ,pp.CumulativeValue
                                        FROM       dbo.saPmtPeriodBatchItems ppbi
                                        INNER JOIN dbo.saPmtPeriods pp ON pp.PmtPeriodId = ppbi.PmtPeriodId
                                        WHERE      ppbi.PmtPeriodBatchHeaderId = @InsertedHeaderId
                                        ) ppCumulative
                            CROSS APPLY (
                                        SELECT   TOP 1 sas.StudentAttendedDate
                                        FROM     dbo.syStudentAttendanceSummary sas
                                        WHERE    sas.StuEnrollId = ppCumulative.StuEnrollId
                                                 AND (( sas.ActualRunningPresentDays + sas.ActualRunningMakeupDays ) >= ppCumulative.CumulativeValue )
                                        ORDER BY sas.StudentAttendedDate
                                        ) AS transDate;



                INSERT INTO dbo.saTransactions (
                                               StuEnrollId
                                              ,TermId
                                              ,CampusId
                                              ,TransDate
                                              ,TransCodeId
                                              ,TransReference
                                              ,AcademicYearId
                                              ,TransDescrip
                                              ,TransAmount
                                              ,TransTypeId
                                              ,IsPosted
                                              ,CreateDate
                                              ,BatchPaymentId
                                              ,ViewOrder
                                              ,IsAutomatic
                                              ,ModUser
                                              ,ModDate
                                              ,Voided
                                              ,FeeLevelId
                                              ,FeeId
                                              ,PaymentCodeId
                                              ,FundSourceId
                                              ,StuEnrollPayPeriodId
                                              ,DisplaySequence
                                              ,SecondDisplaySequence
                                              ,PmtPeriodId
                                              ,PaymentPeriodNumber
                                               )
                            SELECT     BI.StuEnrollId AS StuEnrollId
                                      ,BI.TermId AS TermId
                                      ,SE.CampusId
                                      ,COALESCE((
                                                SELECT TransactionDate
                                                FROM   @TransactionDates
                                                WHERE  PmtPeriodId = BI.PmtPeriodId
                                                       AND StuEnrollId = BI.StuEnrollId
                                                )
                                               ,GETDATE()
                                               ,SE.StartDate
                                               ,SE.ExpStartDate
                                               ) --transaction date
                                      ,PP.TransactionCodeId AS TranscodeId
                                      ,BI.TransactionReference
                                      ,NULL AS AcademicYearId
                                                 --??? HOW TO GET THIS?, 
                                      ,BI.TransactionDescription
                                      ,BI.ChargeAmount
                                      ,(
                                       SELECT TOP 1 TransTypeId
                                       FROM   dbo.saTransTypes
                                       WHERE  Description LIKE 'Charge%'
                                       ) AS TransTypeId
                                      ,1 AS IsPosted
                                      ,GETDATE() AS CreatedDate
                                      ,NULL AS BatchPaymentId
                                      ,NULL AS ViewOrder
                                      ,0 AS IsAutomatic
                                      ,'AutoPost' AS ModUser
                                      ,GETDATE() AS ModDate
                                      ,0 AS Voided
                                      ,NULL AS FeeLevelId
                                      ,NULL AS FeeId
                                      ,NULL AS PaymentCodeId
                                      ,NULL AS FundSourceId
                                      ,NULL AS StuEnrollPayPeriodId
                                      ,NULL AS DisplaySequence
                                      ,NULL AS SecondDisplaySequence
                                      ,BI.PmtPeriodId AS PmtPeriodId
                                      ,PP.PeriodNumber
                            FROM       saPmtPeriodBatchItems BI
                            INNER JOIN saPmtPeriodBatchHeaders BH ON BI.PmtPeriodBatchHeaderId = BH.PmtPeriodBatchHeaderId
                            INNER JOIN dbo.arStuEnrollments SE ON BI.StuEnrollId = SE.StuEnrollId
                            INNER JOIN dbo.saPmtPeriods PP ON PP.PmtPeriodId = BI.PmtPeriodId
                            LEFT JOIN  dbo.saTransCodes tc ON tc.TransCodeId = PP.TransactionCodeId
                            WHERE      BH.PmtPeriodBatchHeaderId = @InsertedHeaderId
                                       AND BI.IncludedInPost = 1
                                       AND SE.StuEnrollId NOT IN (
                                                                 SELECT StuEnrollId
                                                                 FROM   dbo.saTransactions
                                                                 WHERE  StuEnrollId = SE.StuEnrollId
                                                                        AND PmtPeriodId = BI.PmtPeriodId
                                                                        OR PaymentPeriodNumber = PP.PeriodNumber
                                                                 )
                            ORDER BY   SE.StuEnrollId
                                      ,BI.TransactionReference;


                IF @@ERROR <> 0
                    BEGIN
                        ROLLBACK TRAN PAYMENTPERIODTRANSACTION;
                        RAISERROR('Error inserting transactions.', 16, 1);
                        RETURN;
                    END;
                IF OBJECTPROPERTY(OBJECT_ID('Trigger_Insert_Check_saTransactions'), 'IsTrigger') = 1
                    BEGIN
                        ALTER TABLE dbo.saTransactions ENABLE TRIGGER Trigger_Insert_Check_saTransactions;
                    END;


                UPDATE     saPmtPeriodBatchItems
                SET        TransactionId = T.TransactionId
                FROM       dbo.saTransactions T
                INNER JOIN dbo.saPmtPeriodBatchItems BI ON T.PmtPeriodId = BI.PmtPeriodId
                                                           AND T.StuEnrollId = BI.StuEnrollId
                INNER JOIN dbo.saPmtPeriodBatchHeaders BH ON BI.PmtPeriodBatchHeaderId = BH.PmtPeriodBatchHeaderId
                WHERE      BH.PmtPeriodBatchHeaderId = @InsertedHeaderId;

                IF @@ERROR <> 0
                    BEGIN
                        ROLLBACK TRAN PAYMENTPERIODTRANSACTION;
                        RAISERROR('Error updating transaction ids in batch.', 16, 1);
                        RETURN;
                    END;
                ----------------------------------------------------------------- 

                ----------------------------------------------------------------- 
                -- Set Batch to IsPosted 
                ----------------------------------------------------------------- 
                UPDATE dbo.saPmtPeriodBatchHeaders
                SET    IsPosted = 1
                      ,PostedDate = GETDATE()
                WHERE  PmtPeriodBatchHeaderId = @InsertedHeaderId;

                IF @@ERROR <> 0
                    BEGIN
                        ROLLBACK TRAN PAYMENTPERIODTRANSACTION;
                        RAISERROR('Error updating batch is posted flag.', 16, 1);
                        RETURN;
                    END;
                ----------------------------------------------------------------- 


                ----------------------------------------------------------------- 
                -- Set IsCharged flag for payment periods from this batch 
                ----------------------------------------------------------------- 
                UPDATE     dbo.saPmtPeriods
                SET        IsCharged = 1
                FROM       saPmtPeriodBatchItems I
                INNER JOIN dbo.saPmtPeriods P ON I.PmtPeriodId = P.PmtPeriodId
                WHERE      I.PmtPeriodBatchHeaderId = @InsertedHeaderId;

                IF @@ERROR <> 0
                    BEGIN
                        ROLLBACK TRAN PAYMENTPERIODTRANSACTION;
                        RAISERROR('Error updating batch is posted flag.', 16, 1);
                        RETURN;
                    END;
            END;


        ----------------------------------------------------------------- 
        -- Set IsBillingMethodCharged flag for Program Version from this batch 
        ----------------------------------------------------------------- 
        UPDATE     P
        SET        P.IsBillingMethodCharged = 1
        FROM       dbo.arPrgVersions P
        INNER JOIN dbo.arStuEnrollments E ON P.PrgVerId = E.PrgVerId
        INNER JOIN saPmtPeriodBatchItems I ON E.StuEnrollId = I.StuEnrollId
        INNER JOIN dbo.saPmtPeriodBatchHeaders H ON I.PmtPeriodBatchHeaderId = H.PmtPeriodBatchHeaderId
        WHERE      H.PmtPeriodBatchHeaderId = @InsertedHeaderId
                   AND I.TransactionId IS NOT NULL
                   AND I.IncludedInPost = 1;


        IF @@ERROR <> 0
            BEGIN
                ROLLBACK TRAN PAYMENTPERIODTRANSACTION;
                RAISERROR('Error updating program version ischarged flag.', 16, 1);
                RETURN;
            END;
        -----------------------------------------------------------------	 


        COMMIT TRAN PAYMENTPERIODTRANSACTION;
    -----------------------------------------------------------------	 



    END;
--================================================================================================= 
-- END  --  USP_ProcessPaymentPeriodsHours 
--================================================================================================= 

GO
