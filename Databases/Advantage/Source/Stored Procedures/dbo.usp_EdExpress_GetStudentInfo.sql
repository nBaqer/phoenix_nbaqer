SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[usp_EdExpress_GetStudentInfo]
    @SSN NVARCHAR(50)
   ,@AwardId NVARCHAR(50)
AS
    DECLARE @StuEnrollId VARCHAR(50);
    SET @StuEnrollId = NULL;
        
    SET @StuEnrollId = (
                         SELECT TOP 1
                                SE.StuEnrollId
                         FROM   arStudent S
                               ,arStuEnrollments SE
                               ,faStudentAwards SA
                         WHERE  S.StudentId = SE.StudentId
                                AND SE.StuEnrollId = SA.StuEnrollId
                                AND S.SSN = @SSN
                                AND (
                                      SA.LoanId IN ( @AwardId )
                                      OR SA.FA_Id IN ( @AwardId )
                                    )
                         ORDER BY SE.EnrollDate
                       );
        
    SELECT  S.FirstName
           ,S.LastName
           ,SE.StuEnrollId
           ,SE.CampusId
           ,SS.InSchool
           ,SS.SysStatusId
           ,C.CampDescrip
    FROM    arStudent S
           ,arStuEnrollments SE
           ,syStatusCodes SC
           ,sySysStatus SS
           ,syCampuses C
    WHERE   S.StudentId = SE.StudentId
            AND SE.StatusCodeId = SC.StatusCodeId
            AND SC.SysStatusId = SS.SysStatusId
            AND C.CampusId = SE.CampusId
            AND SE.StudentId IN ( SELECT    StudentId
                                  FROM      arStudent
                                  WHERE     SSN = @SSN )
            AND (
                  @StuEnrollId IS NULL
                  OR StuEnrollId IN ( @StuEnrollId )
                )
    ORDER BY EnrollDate DESC;



GO
