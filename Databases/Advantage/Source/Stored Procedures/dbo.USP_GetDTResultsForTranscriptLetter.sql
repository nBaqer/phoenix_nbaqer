SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[USP_GetDTResultsForTranscriptLetter]
    (
     @stuEnrollid UNIQUEIDENTIFIER
    ,@termId VARCHAR(8000) = NULL
    ,@clsStartDate DATETIME
    ,@clsEndDate DATETIME
    )
AS
    SET NOCOUNT ON;
    SELECT  *
    FROM    (
              SELECT DISTINCT
                        t4.TermId
                       ,t3.TermDescrip
                       ,ISNULL(ATES.DescripXTranscript,'') AS DescripXTranscript
                       ,t4.ReqId
                       ,t2.Code
                       ,t2.Descrip AS Descrip
                       ,t2.Credits
                       ,t2.Hours
                       ,t2.CourseCategoryId
                       ,t3.StartDate AS StartDate
                       ,t3.EndDate AS EndDate
                       ,t4.StartDate AS ClassStartDate
                       ,t4.EndDate AS ClassEndDate
                       ,t1.GrdSysDetailId
                       ,t1.TestId
                       ,t1.ResultId
                       ,(
                          SELECT    Grade
                          FROM      arGradeSystemDetails
                          WHERE     GrdSysDetailId = t1.GrdSysDetailId
                        ) AS Grade
                       ,(
                          SELECT    IsPass
                          FROM      arGradeSystemDetails
                          WHERE     GrdSysDetailId = t1.GrdSysDetailId
                        ) AS IsPass
                       ,(
                          SELECT    ISNULL(GPA,0)
                          FROM      arGradeSystemDetails
                          WHERE     GrdSysDetailId = t1.GrdSysDetailId
                        ) AS GPA
                       ,(
                          SELECT    IsCreditsAttempted
                          FROM      arGradeSystemDetails
                          WHERE     GrdSysDetailId = t1.GrdSysDetailId
                        ) AS IsCreditsAttempted
                       ,(
                          SELECT    IsCreditsEarned
                          FROM      arGradeSystemDetails
                          WHERE     GrdSysDetailId = t1.GrdSysDetailId
                        ) AS IsCreditsEarned
                       ,(
                          SELECT    IsInGPA
                          FROM      arGradeSystemDetails
                          WHERE     GrdSysDetailId = t1.GrdSysDetailId
                        ) AS IsInGPA
                       ,(
                          SELECT    IsDrop
                          FROM      arGradeSystemDetails
                          WHERE     GrdSysDetailId = t1.GrdSysDetailId
                        ) AS IsDrop
                       ,(
                          SELECT    Descrip
                          FROM      arCourseCategories
                          WHERE     CourseCategoryId = t2.CourseCategoryId
                        ) AS CourseCategory
                       ,t1.Score
                       ,t2.FinAidCredits
                       ,CASE ( t2.IsExternship )
                          WHEN 1 THEN (
                                        SELECT  MAX(AttendedDate)
                                        FROM    arExternshipAttendance
                                        WHERE   arExternshipAttendance.StuEnrollId = t1.StuEnrollId
                                      )
                          ELSE t4.EndDate
                        END AS DateIssue
                       ,t1.DateDetermined AS DropDate
                       ,t2.Hours AS ScheduledHours
                       ,0 AS IsCourseALab
                       ,0.00 AS decCredits
                       ,(
                          SELECT    IsTransferGrade
                          FROM      arGradeSystemDetails
                          WHERE     GrdSysDetailId = t1.GrdSysDetailId
                        ) AS IsTransferGrade
              FROM      arResults t1
              INNER JOIN arClassSections t4 ON t1.TestId = t4.ClsSectionId
                                               AND (
                                                     t1.GrdSysDetailId IS NOT NULL
                                                     OR (
                                                          t1.GrdSysDetailId IS NULL
                                                          AND t1.isClinicsSatisfied = 1
                                                        )
                                                   )
                                               AND t1.StuEnrollId = @stuEnrollid
                                               AND t4.StartDate >= @clsStartDate
                                               AND t4.EndDate <= @clsEndDate
              INNER JOIN arReqs t2 ON t4.ReqId = t2.ReqId
              INNER JOIN arClassSectionTerms t5 ON t4.ClsSectionId = t5.ClsSectionId
              INNER JOIN arTerm t3 ON t5.TermId = t3.TermId
                                      AND (
                                            @termId IS NULL
                                            OR t3.TermId IN ( SELECT    strval
                                                              FROM      dbo.SPLIT(@termId) )
                                          )
              INNER JOIN arStuEnrollments t6 ON t1.StuEnrollId = t6.StuEnrollId
              LEFT JOIN arTermEnrollSummary AS ATES ON t3.TermId = ATES.TermId
                                                       AND t1.StuEnrollId = ATES.StuEnrollId
              WHERE     t2.IsAttendanceOnly = 0
              UNION
              SELECT 
			DISTINCT    t10.TermId
                       ,t30.TermDescrip
                       ,ISNULL(ATES.DescripXTranscript,'') AS DescripXTranscript
                       ,t10.ReqId
                       ,t20.Code
                       ,t20.Descrip AS Descrip
                       ,t20.Credits
                       ,t20.Hours
                       ,t20.CourseCategoryId
                       ,t30.StartDate AS StartDate
                       ,t30.EndDate AS EndDate
                       ,'1/1/1900' AS ClassStartDate
                       ,'1/1/1900' AS ClassEndDate
                       ,t10.GrdSysDetailId
                       ,'{00000000-0000-0000-0000-000000000000}' AS TestId
                       ,t10.TransferId AS ResultId
                       ,(
                          SELECT    Grade
                          FROM      arGradeSystemDetails
                          WHERE     GrdSysDetailId = t10.GrdSysDetailId
                        ) AS Grade
                       ,(
                          SELECT    IsPass
                          FROM      arGradeSystemDetails
                          WHERE     GrdSysDetailId = t10.GrdSysDetailId
                        ) AS IsPass
                       ,(
                          SELECT    ISNULL(GPA,0)
                          FROM      arGradeSystemDetails
                          WHERE     GrdSysDetailId = t10.GrdSysDetailId
                        ) AS GPA
                       ,(
                          SELECT    IsCreditsAttempted
                          FROM      arGradeSystemDetails
                          WHERE     GrdSysDetailId = t10.GrdSysDetailId
                        ) AS IsCreditsAttempted
                       ,(
                          SELECT    IsCreditsEarned
                          FROM      arGradeSystemDetails
                          WHERE     GrdSysDetailId = t10.GrdSysDetailId
                        ) AS IsCreditsEarned
                       ,(
                          SELECT    IsInGPA
                          FROM      arGradeSystemDetails
                          WHERE     GrdSysDetailId = t10.GrdSysDetailId
                        ) AS IsInGPA
                       ,(
                          SELECT    IsDrop
                          FROM      arGradeSystemDetails
                          WHERE     GrdSysDetailId = t10.GrdSysDetailId
                        ) AS IsDrop
                       ,(
                          SELECT    Descrip
                          FROM      arCourseCategories
                          WHERE     CourseCategoryId = t20.CourseCategoryId
                        ) AS CourseCategory
                       ,t10.Score
                       ,t20.FinAidCredits
                       ,CASE ( t20.IsExternship )
                          WHEN 1 THEN (
                                        SELECT  MAX(AttendedDate)
                                        FROM    arExternshipAttendance
                                        WHERE   arExternshipAttendance.StuEnrollId = t10.StuEnrollId
                                      )
                          ELSE t30.EndDate
                        END AS DateIssue
                       ,t30.EndDate AS DropDate
                       ,t20.Hours AS ScheduledHours
                       ,0 AS IsCourseALab
                       ,0.00 AS decCredits
                       ,(
                          SELECT    IsTransferGrade
                          FROM      arGradeSystemDetails
                          WHERE     GrdSysDetailId = t10.GrdSysDetailId
                        ) AS IsTransferGrade
              FROM      arTransferGrades t10
              INNER JOIN arReqs t20 ON t10.ReqId = t20.ReqId
                                       AND (
                                             t10.GrdSysDetailId IS NOT NULL
                                             OR (
                                                  t10.GrdSysDetailId IS NULL
                                                  AND t10.isClinicsSatisfied = 1
                                                )
                                           )
                                       AND t10.StuEnrollId = @stuEnrollid
              INNER JOIN arTerm t30 ON t10.TermId = t30.TermId
                                       AND (
                                             @termId IS NULL
                                             OR t30.TermId IN ( SELECT  strval
                                                                FROM    dbo.SPLIT(@termId) )
                                           )
                                       AND t30.StartDate >= @clsStartDate
                                       AND t30.EndDate <= @clsEndDate
              LEFT JOIN arTermEnrollSummary AS ATES ON t30.TermId = ATES.TermId
                                                       AND t10.StuEnrollId = ATES.StuEnrollId
              WHERE     t20.IsAttendanceOnly = 0
            ) R
    ORDER BY StartDate DESC
           ,EndDate
           ,TermDescrip;



GO
