SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_RegistrationInSHCourses_getcount]
    @stuenrollid UNIQUEIDENTIFIER
   ,@coursecode VARCHAR(50)
   ,@SHRepeatLimit INT
   ,@ReqId UNIQUEIDENTIFIER
   ,@ValidForregistration VARCHAR(5) OUTPUT
AS
    DECLARE @RegCount INT
       ,@RowCounter INT
       ,@CountCourseRegistered INT;
    SET @RowCounter = (
                        SELECT  COUNT(t1.ModUser)
                        FROM    arResults t1
                        INNER JOIN arClassSections t2 ON t1.TestId = t2.ClsSectionId
                        INNER JOIN arReqs t3 ON t2.ReqId = t3.ReqId
                        WHERE   t1.StuEnrollId = @stuenrollid
                                AND t3.Code LIKE @coursecode + '%'
                      );

    IF @RowCounter >= @SHRepeatLimit --If the student was already registered in a course more than 4 times
        BEGIN
            SET @CountCourseRegistered = (
                                           SELECT   COUNT(t1.ModUser)
                                           FROM     arResults t1
                                           INNER JOIN arClassSections t2 ON t1.TestId = t2.ClsSectionId
                                           INNER JOIN arReqs t3 ON t2.ReqId = t3.ReqId
                                           WHERE    t1.StuEnrollId = @stuenrollid
                                                    AND t3.ReqId = @ReqId
                                                    AND t3.Code LIKE @coursecode + '%'
                                         );

        -- If Student was already registered in the course, then don't let the student to repeat the 
        -- course, as allowing the student to repeat will violate the total number of semesters : 11
            IF @CountCourseRegistered >= 1
                BEGIN
                    SET @ValidForregistration = 'False'; -- Student has already been registered once, not possible to repeat the course 
                END;
            ELSE
                BEGIN
                    SET @ValidForregistration = 'True'; -- Student can be registered
                END;
        END;
    IF @RowCounter < @SHRepeatLimit
        BEGIN
            SET @ValidForregistration = 'True'; -- Student can be registered
        END;
    RETURN;



GO
