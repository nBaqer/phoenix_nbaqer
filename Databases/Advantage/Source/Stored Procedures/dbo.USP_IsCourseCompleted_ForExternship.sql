SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_IsCourseCompleted_ForExternship]
    @StuEnrollId UNIQUEIDENTIFIER
AS
    DECLARE @IsCourseCompleted BIT;
    DECLARE @ExternshipAttendanceNumber INT;
    DECLARE @AttemptedExternshipHours DECIMAL(18,2)
       ,@MinAttendanceNeeded DECIMAL(18,2);

-- Get the minimum externship hours required
    SET @ExternshipAttendanceNumber = (
                                        SELECT TOP 1
                                                CourseGradeBookDetails.Number
                                        FROM    arGrdBkWeights CourseGradeBook
                                        INNER JOIN arGrdBkWgtDetails CourseGradeBookDetails ON CourseGradeBook.InstrGrdBkWgtId = CourseGradeBookDetails.InstrGrdBkWgtId
                                        INNER JOIN arGrdComponentTypes ComponentTypes ON CourseGradeBookDetails.GrdComponentTypeId = ComponentTypes.GrdComponentTypeId
                                        WHERE   CourseGradeBook.ReqId IN ( SELECT DISTINCT
                                                                                    ReqId
                                                                           FROM     arClassSections CS
                                                                           INNER JOIN arResults R ON CS.ClsSectionId = R.TestId
                                                                                                     AND R.StuEnrollId = @StuEnrollId )
                                                AND ComponentTypes.SysComponentTypeId IN ( 544 )
                                      );


    DECLARE getGradeBookResultsForLabWorkorLabHours CURSOR
    FOR
        SELECT DISTINCT
                t1.StuEnrollId
               ,SUM(t1.HoursAttended) AS Score
               ,@ExternshipAttendanceNumber AS Number
        FROM    arExternshipAttendance t1
        INNER JOIN arGrdComponentTypes t3 ON t1.GrdComponentTypeId = t3.GrdComponentTypeId
        WHERE   StuEnrollId = @StuEnrollId
                AND t3.SysComponentTypeId IN ( 544 )
        GROUP BY t1.StuEnrollId;
		
		
    OPEN getGradeBookResultsForLabWorkorLabHours;

    FETCH NEXT FROM getGradeBookResultsForLabWorkorLabHours
INTO @StuEnrollId,@AttemptedExternshipHours,@MinAttendanceNeeded;

    SET @IsCourseCompleted = 0; -- by default False

    WHILE @@FETCH_STATUS = 0
        BEGIN
		
            IF @AttemptedExternshipHours >= @MinAttendanceNeeded
                BEGIN
                    SET @IsCourseCompleted = 1;
                END;
		
            IF @AttemptedExternshipHours < @MinAttendanceNeeded
                BEGIN
                    SET @IsCourseCompleted = 0; 
                    BREAK; --Exit while loop if any one of component was not completed
                END;
       
            FETCH NEXT FROM getGradeBookResultsForLabWorkorLabHours
	INTO @StuEnrollId,@AttemptedExternshipHours,@MinAttendanceNeeded;
        END;
    CLOSE getGradeBookResultsForLabWorkorLabHours;
    DEALLOCATE getGradeBookResultsForLabWorkorLabHours;

--Print 'Before Applying course component'
--Print @IsCourseCompleted

    DECLARE @ClsSectionId UNIQUEIDENTIFIER;
    SET @ClsSectionId = (
                          SELECT TOP 1
                                    CS.ClsSectionId
                          FROM      arClassSections CS
                          INNER JOIN arResults R ON CS.ClsSectionId = R.TestId
                          INNER JOIN arGrdBkWeights CourseGradeBook ON CS.ReqId = CourseGradeBook.ReqId
                          INNER JOIN arGrdBkWgtDetails CourseGradeBookDetails ON CourseGradeBook.InstrGrdBkWgtId = CourseGradeBookDetails.InstrGrdBkWgtId
                          INNER JOIN arGrdComponentTypes ComponentTypes ON CourseGradeBookDetails.GrdComponentTypeId = ComponentTypes.GrdComponentTypeId
                                                                           AND R.StuEnrollId = @StuEnrollId
                                                                           AND ComponentTypes.SysComponentTypeId IN ( 544 )
                        );


    IF @IsCourseCompleted = 1
        BEGIN
            DECLARE @IsComponentAnExam INT
               ,@IsFinalGradePosted INT;
		
		-- Among the components assigned to course see if there are any exam components
            SET @IsComponentAnExam = (
                                       SELECT   COUNT(*) AS RowCounter
                                       FROM     arGrdBkWeights CourseGradeBook
                                       INNER JOIN arGrdBkWgtDetails CourseGradeBookDetails ON CourseGradeBook.InstrGrdBkWgtId = CourseGradeBookDetails.InstrGrdBkWgtId
                                       INNER JOIN arGrdComponentTypes ComponentTypes ON CourseGradeBookDetails.GrdComponentTypeId = ComponentTypes.GrdComponentTypeId
                                       WHERE    CourseGradeBook.ReqId IN ( SELECT DISTINCT
                                                                                    ReqId
                                                                           FROM     arClassSections CS
                                                                           INNER JOIN arResults R ON CS.ClsSectionId = R.TestId
                                                                                                     AND R.StuEnrollId = @StuEnrollId )
                                                AND ComponentTypes.SysComponentTypeId IN ( 501,533 )
                                     );
		
		
		
		
	
		--Print 'ExamComponent='
		--Print @IsComponentAnExam
		--If there is an exam component check if there is a final grade available on
		--the course in arResults table
            IF @IsComponentAnExam >= 1
                BEGIN
                    SET @IsFinalGradePosted = (
                                                SELECT  COUNT(*)
                                                FROM    arResults
                                                WHERE   StuEnrollId = @StuEnrollId
                                                        AND TestId = @ClsSectionId
                                                        AND (
                                                              GrdSysDetailId IS NOT NULL
                                                              OR Score IS NOT NULL
                                                            )
                                              );
				
				--Print 'Final Grade Posted='
				--Print @IsFinalGradePosted
				
				-- If student has a final grade on the course set CourseCompleted Flag to 1					 
                    IF @IsFinalGradePosted >= 1
                        BEGIN
                            SET @IsCourseCompleted = 1;
                            UPDATE  arResults
                            SET     IsCourseCompleted = 1
                            WHERE   StuEnrollId = @StuEnrollId
                                    AND TestId = @ClsSectionId;			
                        END; 		
                    ELSE
                        BEGIN
                            SET @IsCourseCompleted = 0;
                            UPDATE  arResults
                            SET     IsCourseCompleted = 0
                            WHERE   StuEnrollId = @StuEnrollId
                                    AND TestId = @ClsSectionId;			
                        END;
                END;
            ELSE
                BEGIN
                    SET @IsCourseCompleted = 1;
                    UPDATE  arResults
                    SET     IsCourseCompleted = 1
                    WHERE   StuEnrollId = @StuEnrollId
                            AND TestId = @ClsSectionId;			
                END;
        END;
    ELSE
        BEGIN
		--sET @IsCourseCompleted=0
            UPDATE  arResults
            SET     IsCourseCompleted = 0
            WHERE   StuEnrollId = @StuEnrollId
                    AND TestId = @ClsSectionId;
        END;
	--Print @IsCourseCompleted



GO
