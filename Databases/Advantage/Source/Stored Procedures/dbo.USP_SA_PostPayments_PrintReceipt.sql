SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--=================================================================================================
-- USP_SA_PostPayments_PrintReceipt
--=================================================================================================
CREATE PROCEDURE [dbo].[USP_SA_PostPayments_PrintReceipt]
    @CampusId UNIQUEIDENTIFIER
   ,@TransactionId UNIQUEIDENTIFIER
AS
    SET NOCOUNT ON;  
    BEGIN  
   
        DECLARE @StudentReceiptData TABLE
            (
             SchoolName VARCHAR(100) NOT NULL
            ,SchoolAddress1 VARCHAR(200) NOT NULL
            ,SchoolAddress2 VARCHAR(200) NULL
            ,SchoolCity VARCHAR(50) NOT NULL
            ,SchoolState VARCHAR(50) NOT NULL
            ,SchoolZip VARCHAR(20) NOT NULL
            ,SchoolCountry VARCHAR(50) NULL
            ,StudentName VARCHAR(100) NOT NULL
            ,StudentAddress1 VARCHAR(200) NULL
            ,StudentAddress2 VARCHAR(200) NULL
            ,StudentCity VARCHAR(50) NULL
            ,StudentState VARCHAR(50) NULL
            ,StudentZip VARCHAR(20) NULL
            ,StudentCountry VARCHAR(50) NULL
            ,TransDate VARCHAR(10) NOT NULL
            ,TransDesc VARCHAR(50) NULL
            ,PaymentTypeDesc VARCHAR(50) NOT NULL
            ,CheckNumber VARCHAR(50) NULL
            ,TransAmount DECIMAL(19,4) NOT NULL
            );  
    
        DECLARE @SchoolData TABLE
            (
             SchoolName VARCHAR(100) NOT NULL
            ,SchoolAddress1 VARCHAR(200) NOT NULL
            ,SchoolAddress2 VARCHAR(200) NULL
            ,SchoolCity VARCHAR(50) NOT NULL
            ,SchoolState VARCHAR(50) NULL
            ,SchoolZip VARCHAR(20) NOT NULL
            ,SchoolCountry VARCHAR(50) NULL
            );  
    
        DECLARE @StudentData TABLE
            (
             StudentName VARCHAR(100) NOT NULL
            ,StudentAddress1 VARCHAR(200) NULL
            ,StudentAddress2 VARCHAR(200) NULL
            ,StudentCity VARCHAR(50) NULL
            ,StudentState VARCHAR(50) NULL
            ,StudentZip VARCHAR(20) NULL
            ,StudentCountry VARCHAR(50) NULL
            );  
    
      
  --Get School Address  
        DECLARE @AddressToBePrintedInReceipts VARCHAR(50);  
        SET @AddressToBePrintedInReceipts = (
                                              SELECT    Value
                                              FROM      syConfigAppSetValues v
                                              INNER JOIN syConfigAppSettings s ON s.SettingId = v.SettingId
                                              WHERE     KeyName = 'AddressToBePrintedInReceipts'
                                            );  
    
        BEGIN  
            IF @AddressToBePrintedInReceipts = 'CorporateAddress'
                BEGIN  
                    INSERT  INTO @SchoolData
                            (
                             SchoolName
                            ,SchoolAddress1
                            ,SchoolAddress2
                            ,SchoolCity
                            ,SchoolState
                            ,SchoolZip
                            ,SchoolCountry       
                            )
                            SELECT  (
                                      SELECT    Value
                                      FROM      dbo.syConfigAppSetValues v
                                      INNER JOIN dbo.syConfigAppSettings s ON s.SettingId = v.SettingId
                                      WHERE     KeyName LIKE 'CorporateName'
                                    ) AS SchoolName
                                   ,(
                                      SELECT    Value
                                      FROM      dbo.syConfigAppSetValues v
                                      INNER JOIN dbo.syConfigAppSettings s ON s.SettingId = v.SettingId
                                      WHERE     KeyName LIKE 'CorporateAddress1'
                                    ) AS SchoolAddress1
                                   ,(
                                      SELECT    Value
                                      FROM      dbo.syConfigAppSetValues v
                                      INNER JOIN dbo.syConfigAppSettings s ON s.SettingId = v.SettingId
                                      WHERE     KeyName LIKE 'CorporateAddress2'
                                    ) AS SchoolAddress2
                                   ,(
                                      SELECT    Value
                                      FROM      dbo.syConfigAppSetValues v
                                      INNER JOIN dbo.syConfigAppSettings s ON s.SettingId = v.SettingId
                                      WHERE     KeyName LIKE 'CorporateCity'
                                    ) AS SchoolCity
                                   ,(
                                      SELECT    Value
                                      FROM      dbo.syConfigAppSetValues v
                                      INNER JOIN dbo.syConfigAppSettings s ON s.SettingId = v.SettingId
                                      WHERE     KeyName LIKE 'CorporateState'
                                    ) AS SchoolState
                                   ,(
                                      SELECT    Value
                                      FROM      dbo.syConfigAppSetValues v
                                      INNER JOIN dbo.syConfigAppSettings s ON s.SettingId = v.SettingId
                                      WHERE     KeyName LIKE 'CorporateZip'
                                    ) AS SchoolZip
                                   ,(
                                      SELECT    Value
                                      FROM      dbo.syConfigAppSetValues v
                                      INNER JOIN dbo.syConfigAppSettings s ON s.SettingId = v.SettingId
                                      WHERE     KeyName LIKE 'CorporateCountry'
                                    ) AS SchoolCountry;  
                END;  
      
            IF @AddressToBePrintedInReceipts = 'CampusAddress'
                BEGIN     
                    INSERT  INTO @SchoolData
                            (
                             SchoolName
                            ,SchoolAddress1
                            ,SchoolAddress2
                            ,SchoolCity
                            ,SchoolState
                            ,SchoolZip
                            ,SchoolCountry    
                            )
                            SELECT  (
                                      SELECT    CampDescrip
                                      FROM      dbo.syCampuses
                                      WHERE     CampusId = @CampusId
                                    ) AS SchoolName
                                   ,(
                                      SELECT    Address1
                                      FROM      dbo.syCampuses
                                      WHERE     CampusId = @CampusId
                                    ) AS SchoolAddress1
                                   ,(
                                      SELECT    Address2
                                      FROM      dbo.syCampuses
                                      WHERE     CampusId = @CampusId
                                    ) AS SchoolAddress2
                                   ,(
                                      SELECT    City
                                      FROM      dbo.syCampuses
                                      WHERE     CampusId = @CampusId
                                    ) AS SchoolCity
                                   ,(
                                      SELECT    StateCode
                                      FROM      syStates s
                                      INNER JOIN syCampuses c ON s.StateId = c.StateId
                                      WHERE     CampusId = @CampusId
                                    ) AS SchoolState
                                   ,(
                                      SELECT    Zip
                                      FROM      dbo.syCampuses
                                      WHERE     CampusId = @CampusId
                                    ) AS SchoolZip
                                   ,(
                                      SELECT    CountryCode
                                      FROM      dbo.adCountries s
                                      INNER JOIN syCampuses c ON s.CountryId = c.CountryId
                                      WHERE     CampusId = @CampusId
                                    ) AS SchoolCountry;  
                END;   
    
        END;    
    
  --Get Student Address  
    
        DECLARE @DefaultStuAddressId UNIQUEIDENTIFIER;  
        SET @DefaultStuAddressId = ISNULL((
                                            SELECT TOP 1
                                                    adLeadAddressId
                                            FROM    saTransactions T
                                                   ,arStuEnrollments SE
                                                   ,adLeadAddresses SA
                                                   ,syStatuses ST
                                                   ,adLeads L
                                            WHERE   T.StuEnrollId = SE.StuEnrollId
                                                    AND L.StudentId = SE.StudentId
                                                    AND L.LeadId = SA.LeadId
                                                    AND SA.StatusId = ST.StatusId
                                                    AND ST.Status = 'Active'
                                                    AND TransactionId = @TransactionId
                                            ORDER BY IsShowOnLeadPage DESC
                                          ),'00000000-0000-0000-0000-000000000000');  
  
        IF @DefaultStuAddressId <> '00000000-0000-0000-0000-000000000000'
            BEGIN  
                INSERT  INTO @StudentData
                        (
                         StudentName
                        ,StudentAddress1
                        ,StudentAddress2
                        ,StudentCity
                        ,StudentState
                        ,StudentZip
                        ,StudentCountry   
                        )
                        SELECT  (
                                  SELECT    ( LastName + ', ' + FirstName ) AS StudentName
                                  FROM      adLeads
                                  WHERE     StudentId = (
                                                          SELECT    StudentId
                                                          FROM      arStuEnrollments S
                                                          INNER JOIN saTransactions t ON S.StuEnrollId = t.StuEnrollId
                                                          WHERE     t.TransactionId = @TransactionId
                                                        )
                                ) AS StudentName
                               ,( CASE CCT.AddressApto
                                    WHEN '' THEN CCT.Address1
                                    ELSE CCT.AddressApto + ' ' + CCT.Address1
                                  END ) AS StudentAddress1
                               ,CCT.Address2 AS StudentAddress2
                               ,CCT.City AS StudentCity
                               ,(
                                  SELECT    StateCode
                                  FROM      syStates
                                  WHERE     StateId = CCT.StateId
                                ) AS StudentState
                               ,CCT.ZipCode AS StudentZip
                               ,(
                                  SELECT    CountryCode
                                  FROM      adCountries
                                  WHERE     CountryId = CCT.CountryId
                                ) AS StudentCountry
                        FROM    adLeadAddresses CCT
                        WHERE   CCT.adLeadAddressId = @DefaultStuAddressId;   
    
            END;   
      
        IF @DefaultStuAddressId = '00000000-0000-0000-0000-000000000000'
            BEGIN  
                INSERT  INTO @StudentData
                        (
                         StudentName
                        ,StudentAddress1
                        ,StudentAddress2
                        ,StudentCity
                        ,StudentState
                        ,StudentZip
                        ,StudentCountry   
                        )
                        SELECT  (
                                  SELECT    ( LastName + ', ' + FirstName ) AS StudentName
                                  FROM      adLeads
                                  WHERE     StudentId = (
                                                          SELECT    StudentId
                                                          FROM      arStuEnrollments S
                                                          INNER JOIN saTransactions t ON S.StuEnrollId = t.StuEnrollId
                                                          WHERE     t.TransactionId = @TransactionId
                                                        )
                                ) AS StudentName
                               ,'' AS StudentAddress1
                               ,'' AS StudentAddress2
                               ,'' AS StudentCity
                               ,'' AS StudentState
                               ,'' AS StudentZip
                               ,'' AS StudentCountry;          
      
      
            END;        
     
    
    
    
        BEGIN  
            INSERT  INTO @StudentReceiptData
                    (
                     SchoolName
                    ,SchoolAddress1
                    ,SchoolAddress2
                    ,SchoolCity
                    ,SchoolState
                    ,SchoolZip
                    ,SchoolCountry
                    ,StudentName
                    ,StudentAddress1
                    ,StudentAddress2
                    ,StudentCity
                    ,StudentState
                    ,StudentZip
                    ,StudentCountry
                    ,TransDate
                    ,TransDesc
                    ,PaymentTypeDesc
                    ,CheckNumber
                    ,TransAmount      
                    )
                    SELECT  SchoolName AS SchoolName
                           ,SchoolAddress1 AS SchoolAddress1
                           ,SchoolAddress2 AS SchoolAddress2
                           ,SchoolCity AS SchoolCity
                           ,SchoolState AS SchoolState
                           ,SchoolZip AS SchoolZip
                           ,SchoolCountry AS SchoolCountry
                           ,StudentName AS StudentName
                           ,StudentAddress1 AS StudentAddress1
                           ,StudentAddress2 AS StudentAddress2
                           ,StudentCity AS StudentCity
                           ,StudentState AS StudentState
                           ,StudentZip AS StudentZip
                           ,StudentCountry AS StudentCountry
                           ,CONVERT(VARCHAR(10),TransDate,101) AS TransDate
                           ,(
                              SELECT    TransCodeDescrip
                              FROM      dbo.saTransCodes
                              WHERE     TransCodeId = T.TransCodeId
                                        AND T.TransactionId = @TransactionId
                            ) AS TransDesc
                           ,(
                              SELECT    Description
                              FROM      dbo.saPaymentTypes PT
                              INNER JOIN dbo.saPayments P ON PT.PaymentTypeId = P.PaymentTypeId
                              WHERE     P.TransactionId = @TransactionId
                            ) AS PaymentTypeDesc
                           ,P.CheckNumber AS CheckNumber
                           ,T.TransAmount AS TransAmount
                    FROM    @SchoolData
                           ,@StudentData
                           ,dbo.saTransactions T
                           ,dbo.saPayments P
                    WHERE   T.TransactionId = @TransactionId
                            AND T.Voided = 0
                            AND P.TransactionId = @TransactionId;  
     
        END;  
    
        SELECT  SchoolName
               ,SchoolAddress1
               ,SchoolAddress2
               ,SchoolCity
               ,SchoolState
               ,SchoolZip
               ,CASE SchoolCountry
                  WHEN 'USA' THEN ''
                  ELSE SchoolCountry
                END AS SchoolCountry
               ,StudentName
               ,StudentAddress1
               ,StudentAddress2
               ,StudentCity
               ,StudentState
               ,StudentZip
               ,CASE StudentCountry
                  WHEN 'USA' THEN ''
                  ELSE StudentCountry
                END AS StudentCountry
               ,TransDate
               ,TransDesc
               ,PaymentTypeDesc
               ,CheckNumber
               ,TransAmount
        FROM    @StudentReceiptData;  
    
    END;  
--=================================================================================================
-- END  --  USP_SA_PostPayments_PrintReceipt
--=================================================================================================   
GO
