SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =========================================================================================================
-- USP_IPEDS_FALL_DisabilityService
-- =========================================================================================================
CREATE PROCEDURE [dbo].[USP_IPEDS_FALL_DisabilityService]
--EXEC USP_IPEDS_FALL_DISABILITY '3F5E839A-589A-4B2A-B258-35A1A8B3B819',NULL,NULL,'10/31/2016',NULL,'SSN',NULL 
    @CampusId VARCHAR(50)
   ,@ProgIdList VARCHAR(MAX) = NULL                 -- Program Id List
   ,@StartDate DATETIME = NULL
   ,@EndDate DATETIME = NULL
   ,@ShowSSN BIT
   ,@OrderBy VARCHAR(100)                           -- 'StudentIdentifier' OR 'StudentName'      
AS
    BEGIN
		
		/* Get a list of students who are out of school as of report end date. These students will be excluded from the report */
        DECLARE @StudentsInOutofSchoolStatusAsOfReportEndDate TABLE
            (
             StuEnrollId UNIQUEIDENTIFIER
            );
        INSERT  INTO @StudentsInOutofSchoolStatusAsOfReportEndDate
                SELECT  ASE1.StuEnrollId
                FROM    arStuEnrollments AS ASE1
                INNER JOIN syStatusCodes AS SSC1 ON SSC1.StatusCodeId = ASE1.StatusCodeId  -- t2
                INNER JOIN arPrgVersions AS APV1 ON APV1.PrgVerId = ASE1.PrgVerId
                INNER JOIN arPrograms AS AP1 ON AP1.ProgId = APV1.ProgId
                WHERE   ASE1.StartDate <= @EndDate -- Student enrolled as of report end date
                        AND LTRIM(RTRIM(ASE1.CampusId)) = LTRIM(RTRIM(@CampusId))
                        AND (
                              @ProgIdList IS NULL
                              OR AP1.ProgId IN ( SELECT Val
                                                 FROM   MultipleValuesForReportParameters(@ProgIdList,',',1) )
                            )
                        AND SSC1.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
												-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                        AND (
                              ASE1.DateDetermined < @StartDate
                              OR ASE1.ExpGradDate < @StartDate
                              OR ASE1.LDA < @StartDate
                            );



	
		/* List of students who will show up in report */
        SELECT  List.StudentIdentifier
               ,List.StudentName
               ,List.DisabledStudent
               ,List.IPEDSSequence
        FROM    (
                  SELECT  DISTINCT
                            CASE WHEN ( @ShowSSN = 1 ) THEN AST.SSN
                                 ELSE AST.StudentNumber
                            END AS StudentIdentifier
                           ,( LTRIM(RTRIM(ISNULL(AST.LastName,''))) + CASE WHEN ( AST.FirstName IS NOT NULL ) THEN ', ' + LTRIM(RTRIM(ISNULL(AST.FirstName,'')))
                                                                           ELSE CASE WHEN ( AST.MiddleName IS NOT NULL ) THEN ','
                                                                                     ELSE ''
                                                                                END
                                                                      END + CASE WHEN ( AST.MiddleName IS NOT NULL )
                                                                                 THEN ' ' + LTRIM(RTRIM(SUBSTRING(ISNULL(AST.MiddleName,''),1,1)))
                                                                                 ELSE ''
                                                                            END ) AS StudentName
                           ,CASE WHEN ( ASE.IsDisabled = 1 ) THEN 'X'
                                 ELSE ''
                            END AS DisabledStudent
                           ,ISNULL(AG.IPEDSSequence,1) AS IPEDSSequence
                  FROM      arStudent AS AST
                  LEFT JOIN adGenders AS AG ON AST.Gender = AG.GenderId
                  INNER JOIN arStuEnrollments AS ASE ON AST.StudentId = ASE.StudentId
                  LEFT JOIN adDegCertSeeking AS ADCS ON ADCS.DegCertSeekingId = ASE.degcertseekingid ---- For Degree Seeking --
                  INNER JOIN syStatusCodes AS SSC ON ASE.StatusCodeId = SSC.StatusCodeId
                  INNER JOIN sySysStatus AS SSS ON SSC.SysStatusId = SSS.SysStatusId
                  INNER JOIN arPrgVersions AS APV ON APV.PrgVerId = ASE.PrgVerId
                  INNER JOIN arPrograms AS AP ON AP.ProgId = APV.ProgId
							--Areas Of Intrest --
                  INNER JOIN arPrgGrp AS APG ON APG.PrgGrpId = APV.PrgGrpId 
						    --Areas Of Intrest --
                  INNER JOIN arProgTypes AS APT ON APV.ProgTypId = APT.ProgTypId
                  LEFT JOIN arAttendTypes AS AAT ON ASE.attendtypeid = AAT.AttendTypeId
                  WHERE     ASE.CampusId = @CampusId
                            AND (
                                  @ProgIdList IS NULL
                                  OR AP.ProgId IN ( SELECT  Val
                                                    FROM    MultipleValuesForReportParameters(@ProgIdList,',',1) )
                                )
                            AND APT.IPEDSValue = 58							 -- Undergraduate
                            AND (
                                  AAT.IPEDSValue = 61
                                  OR AAT.IPEDSValue = 62
                                ) -- Part Time or Full Time
                            AND (
                                  AG.IPEDSValue = 30
                                  OR AG.IPEDSValue = 31
                                )   -- Men or Women
                            AND ASE.IsFirstTimeInSchool = 1					 -- First-time in school
                            AND SSS.SysStatusId NOT IN ( 8 )				 -- Exclude No Start Students
                            AND ASE.StartDate <= @EndDate					 -- Student who is enrolled before Report End Date
                            AND (
                                  ISNULL(ADCS.IPEDSValue,0) = 11
                                  OR ISNULL(ADCS.IPEDSValue,0) = 10
                                ) -- Degree/Cert Seeking and Non-Degree Certificate Seeking students     
                            AND StuEnrollId NOT IN ( SELECT StuEnrollId
                                                     FROM   @StudentsInOutofSchoolStatusAsOfReportEndDate )
                ) AS List
        ORDER BY CASE WHEN @OrderBy = 'StudentIdentifier' THEN List.StudentIdentifier
                      WHEN @OrderBy = 'StudentName' THEN List.StudentName
                 END; 
    END;
-- =========================================================================================================
-- END  --  USP_IPEDS_FALL_DisabilityService
-- =========================================================================================================


GO
