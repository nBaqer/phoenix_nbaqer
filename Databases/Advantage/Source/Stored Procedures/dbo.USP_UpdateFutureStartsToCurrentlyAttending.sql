SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

--=================================================================================================
-- USP_UpdateFutureStartsToCurrentlyAttending
--=================================================================================================
-- =============================================
-- Author:		Ginzo, John
-- Create date: 03/31/2015
-- Description:	Update statuses for future starts
--				to currently attending based on 
--				attendance posted or start date
--				in the past
-- =============================================
CREATE PROCEDURE [dbo].[USP_UpdateFutureStartsToCurrentlyAttending]
AS
    BEGIN
        -- SET NOCOUNT ON added to prevent extra result sets from
        -- interfering with SELECT statements.
        SET NOCOUNT ON;

        ------------------------------------------------
        -- GET currently attending status code
        ------------------------------------------------
        DECLARE @curAttending UNIQUEIDENTIFIER;
        DECLARE @transDate DATETIME = GETDATE();
        DECLARE @CampusGroupStatuses TABLE
            (
                CampGrpId UNIQUEIDENTIFIER NOT NULL
               ,StatusCodeId UNIQUEIDENTIFIER NOT NULL
            );

        SET @curAttending = (
                            SELECT     TOP 1 StatusCodeId
                            FROM       dbo.syStatusCodes sc
                            INNER JOIN dbo.sySysStatus ss ON sc.SysStatusId = ss.SysStatusId
                            WHERE      ss.SysStatusId = 9
                            );
        ------------------------------------------------


        ------------------------------------------------
        -- GET currently attending status code
        ------------------------------------------------
        CREATE TABLE #MinAttendance1
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,StudentAttendedDate DATETIME
            );



        INSERT INTO #MinAttendance1
                    SELECT   StuEnrollId
                            ,MAX(StudentAttendedDate) AS StudentAttendedDate
                    FROM     dbo.syStudentAttendanceSummary
                    WHERE    ActualDays != 9999.0
                    GROUP BY StuEnrollId;



        CREATE TABLE #EnrollmentsIsAttendanceTaking
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,IsAttendanceTaking BIT
               ,StartDate DATETIME
               ,StudentAttendedDate DATETIME
               ,ReEnrollDate DATETIME
            );

        INSERT INTO #EnrollmentsIsAttendanceTaking
                    SELECT     e.StuEnrollId
                              ,CASE WHEN ut.UnitTypeDescrip IS NULL
                                         OR ut.UnitTypeDescrip = 'None' THEN 0
                                    ELSE 1
                               END AS IsAttendanceTaking
                              ,ISNULL(e.StartDate, e.ExpStartDate) AS StartDate
                              ,ma.StudentAttendedDate
                              ,e.ReEnrollmentDate
                    FROM       dbo.arStuEnrollments e
                    INNER JOIN dbo.arPrgVersions pv ON e.PrgVerId = pv.PrgVerId
                    LEFT JOIN  dbo.arAttUnitType ut ON pv.UnitTypeId = ut.UnitTypeId
                    INNER JOIN dbo.syStatusCodes sc ON e.StatusCodeId = sc.StatusCodeId
                    INNER JOIN dbo.sySysStatus SS ON sc.SysStatusId = SS.SysStatusId
                    LEFT JOIN  #MinAttendance1 ma ON e.StuEnrollId = ma.StuEnrollId
                    WHERE      sc.SysStatusId = 7;


        CREATE TABLE #EnrollmentsToChange
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,ChangeToCurrentStatus BIT
               ,OriginalStatusId UNIQUEIDENTIFIER
               ,NewStatusId UNIQUEIDENTIFIER
               ,CampusId UNIQUEIDENTIFIER
               ,DateOfChange DATETIME
            );

        INSERT INTO #EnrollmentsToChange
                    SELECT     e.StuEnrollId
                              ,( CASE WHEN ISNULL(e.StartDate, e.ExpStartDate) < GETDATE() THEN 1
                                      WHEN ia.IsAttendanceTaking = 1 THEN
                              ( CASE WHEN ia.StudentAttendedDate IS NOT NULL THEN CASE WHEN ia.ReEnrollDate IS NULL THEN 1
                                                                                       --WHEN ia.ReEnrollDate > ia.StudentAttendedDate THEN 0
                                                                                       WHEN ia.ReEnrollDate IS NOT NULL
                                                                                            AND ia.ReEnrollDate <= GETDATE() THEN 1
                                                                                       ELSE 0
                                                                                  END
                                     ELSE 0
                                END
                              )
                                      ELSE CASE WHEN ia.ReEnrollDate IS NOT NULL THEN CASE WHEN ia.ReEnrollDate <= GETDATE() THEN 1
                                                                                           ELSE 0
                                                                                      END
                                                ELSE 0
                                           END
                                 END
                               ) AS ChangeToCurrentStatus
                              ,e.StatusCodeId
                              ,@curAttending
                              ,e.CampusId
                              ,CASE WHEN IsAttendanceTaking = 1 AND Ia.StudentAttendedDate IS NOT null THEN ia.StudentAttendedDate -- when is posting attendance
                                    WHEN e.StartDate <= @transDate AND ia.IsAttendanceTaking = 1 THEN @transDate --when running job and startdate has passed or is same as execution date
                                    ELSE ISNULL(e.StartDate, ExpStartDate)
                               END AS DateOfChange
                    FROM       arStuEnrollments e
                    INNER JOIN #EnrollmentsIsAttendanceTaking ia ON e.StuEnrollId = ia.StuEnrollId
                    INNER JOIN dbo.syStatusCodes sc ON e.StatusCodeId = sc.StatusCodeId
                    INNER JOIN dbo.sySysStatus SS ON sc.SysStatusId = SS.SysStatusId
                    WHERE      sc.SysStatusId = 7;
   
        INSERT INTO @CampusGroupStatuses
                    SELECT   pv.CampGrpId
                            ,StatusCodeId = dbo.GetCurrentlyAttendingDefaultStatusIdForCampusGroup(pv.CampGrpId)
                    FROM     #EnrollmentsToChange e
                    JOIN     dbo.arStuEnrollments b ON e.StuEnrollId = b.StuEnrollId
                    JOIN     dbo.arPrgVersions pv ON b.PrgVerId = pv.PrgVerId
                    WHERE    b.StuEnrollId IN (
                                              SELECT StuEnrollId
                                              FROM   #EnrollmentsToChange
                                              WHERE  ChangeToCurrentStatus = 1
                                              )
                    GROUP BY pv.CampGrpId;



        ------------------------------------------------
        -- update the necessary tables
        ------------------------------------------------
        BEGIN TRAN UpdateFutureStarts;

        DECLARE @curDateTime DATETIME; --datetime to use in updated
        SET @curDateTime = GETDATE();

        ------------------------------------------------
        -- update the status in enrollments
        ------------------------------------------------
        UPDATE e
        SET    StatusCodeId = cgs.StatusCodeId
              ,ModDate = @curDateTime
              ,ModUser = 'support'
        FROM   #EnrollmentsToChange etu
        JOIN   dbo.arStuEnrollments e ON e.StuEnrollId = etu.StuEnrollId
        JOIN   dbo.arPrgVersions pv ON pv.PrgVerId = e.PrgVerId
        JOIN   @CampusGroupStatuses cgs ON cgs.CampGrpId = pv.CampGrpId
        WHERE  e.StuEnrollId IN (
                                SELECT StuEnrollId
                                FROM   #EnrollmentsToChange
                                WHERE  ChangeToCurrentStatus = 1
                                );

        IF @@error <> 0
            BEGIN
                ROLLBACK TRAN UpdateFutureStarts;
                RETURN -1;
            END;
        ------------------------------------------------

        ------------------------------------------------
        -- update the status change history table
        ------------------------------------------------
        INSERT INTO dbo.syStudentStatusChanges (
                                               StudentStatusChangeId
                                              ,StuEnrollId
                                              ,OrigStatusId
                                              ,NewStatusId
                                              ,CampusId
                                              ,ModDate
                                              ,ModUser
                                              ,IsReversal
                                              ,DropReasonId
                                              ,DateOfChange
                                               )
                    SELECT NEWID()
                          ,ia.StuEnrollId
                          ,ia.OriginalStatusId
                          ,cgs.StatusCodeId
                          ,ia.CampusId
                          ,@curDateTime
                          ,'support'
                          ,0
                          ,NULL
                          ,ia.DateOfChange
                    FROM   #EnrollmentsToChange ia
                    JOIN   dbo.arStuEnrollments e ON e.StuEnrollId = ia.StuEnrollId
                    JOIN   dbo.arPrgVersions pv ON pv.PrgVerId = e.PrgVerId
                    JOIN   @CampusGroupStatuses cgs ON cgs.CampGrpId = pv.CampGrpId
                    WHERE  ia.ChangeToCurrentStatus = 1;


        IF @@error <> 0
            BEGIN
                ROLLBACK TRAN UpdateFutureStarts;
                RETURN -1;
            END;
        ------------------------------------------------

        ------------------------------------------------
        -- ALL GOOD - commit and drop temps
        ------------------------------------------------
        COMMIT TRAN UpdateFutureStarts;

        DROP TABLE #MinAttendance1;
        DROP TABLE #EnrollmentsIsAttendanceTaking;
        DROP TABLE #EnrollmentsToChange;
        ------------------------------------------------

        RETURN 0;
    END;
--=================================================================================================
-- END  --  USP_UpdateFutureStartsToCurrentlyAttending
--=================================================================================================

GO
