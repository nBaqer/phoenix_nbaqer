SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[USP_GetCampuIDForGivenEnrollment]
    @StuEnrollID UNIQUEIDENTIFIER
AS
    SELECT  CampusID
    FROM    arStuEnrollMents
    WHERE   StuEnrollId = @StuEnrollID;
        




GO
