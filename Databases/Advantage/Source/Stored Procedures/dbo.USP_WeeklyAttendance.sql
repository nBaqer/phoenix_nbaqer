SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--================================================================================================= 
-- USP_WeeklyAttendance 
--================================================================================================= 
CREATE PROCEDURE [dbo].[USP_WeeklyAttendance]
    @WeekStartDate DATETIME
   ,@WeekEndDate DATETIME
   ,@CampusID UNIQUEIDENTIFIER
   ,@TermId VARCHAR(8000) = NULL
   ,@CourseId VARCHAR(8000) = NULL
   ,@InstructorId VARCHAR(8000) = NULL
   ,@ShiftId VARCHAR(8000) = NULL
   ,@ClassId VARCHAR(8000) = NULL
   ,@EnrollmentId VARCHAR(8000) = NULL
AS
    BEGIN
        DECLARE @StudentIdentifier AS NVARCHAR(1000);
        DECLARE @TrackSapAttendance AS NVARCHAR(1000);
        DECLARE @AttendanceWindow TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,RecordDate DATETIME
               ,SchedHours DECIMAL(18, 2)
               ,ActualHours DECIMAL(18, 2)
               ,IsTardy BIT
            );
        SELECT     @StudentIdentifier = SCASV.Value
        FROM       syConfigAppSettings AS SCAS
        INNER JOIN syConfigAppSetValues AS SCASV ON SCASV.SettingId = SCAS.SettingId
        WHERE      SCAS.KeyName = 'StudentIdentifier'; -- @StudentIdentifier -->  'SSN', 'EnrollmentId', 'StudentId'            

        SET @TrackSapAttendance = dbo.GetAppSettingValueByKeyName('TrackSapAttendance', @CampusID);
        IF ( @TrackSapAttendance IS NOT NULL )
            BEGIN
                SET @TrackSapAttendance = LOWER(LTRIM(RTRIM(@TrackSapAttendance)));
            END;

        IF ( @TrackSapAttendance = 'byday' )
            BEGIN

                SELECT   C.StuEnrollId
                        ,C.StudentIdentifier
                        ,C.StudentName
                        ,CONVERT(DECIMAL(15, 2), SUM(C.Mon) / ActualDivisor) AS Mon
                        ,CONVERT(DECIMAL(15, 2), SUM(C.Tue) / ActualDivisor) AS Tue
                        ,CONVERT(DECIMAL(15, 2), SUM(C.Wed) / ActualDivisor) AS Wed
                        ,CONVERT(DECIMAL(15, 2), SUM(C.Thur) / ActualDivisor) AS Thur
                        ,CONVERT(DECIMAL(15, 2), SUM(C.Fri) / ActualDivisor) AS Fri
                        ,CONVERT(DECIMAL(15, 2), SUM(C.Sat) / ActualDivisor) AS Sat
                        ,CONVERT(DECIMAL(15, 2), SUM(C.Sun) / ActualDivisor) AS Sun
                        ,CONVERT(DECIMAL(15, 2), SUM(C.WeekTotal) / ActualDivisor) AS WeekTotal
                        ,CONVERT(DECIMAL(15, 2), SUM(C.NewTotal) / ActualDivisor) AS NewTotal
                        ,CONVERT(DECIMAL(15, 2), SUM(C.NewTotalSched) / ActualDivisor) AS NewTotalSched
                        ,CONVERT(DECIMAL(15, 2), SUM(C.NewTotalDaysAbsent) / ActualDivisor) AS NewTotalDaysAbsent
                        ,CONVERT(DECIMAL(15, 2), SUM(C.NewTotalMakeUpDays) / ActualDivisor) AS NewTotalMakeUpDays
                        ,CONVERT(DECIMAL(15, 2), SUM(C.NewNetDaysAbsent) / ActualDivisor) AS NewNetDaysAbsent
                        ,CONVERT(DECIMAL(15, 2), SUM(C.NewSchedDaysatLDA) / ActualDivisor) AS NewSchedDaysatLDA
                        ,LEFT(CONVERT(VARCHAR, C.LDA, 120), 10) AS LDA
                        --,C.CampGrpDescrip                       
                        ,C.AttTypeID
                        ,C.SuppressDate
                        ,NULL AS InstructorId
                        ,NULL AS ShiftId
                        ,StatusCodeId
                        ,StatusCodeDescrip
						,C.UnitTypeDescrip
                FROM     (
                         SELECT   B.*
                         FROM     (
                                  SELECT     A.StuEnrollId
                                            ,CASE WHEN @StudentIdentifier = 'SSN' THEN '***-**-' + SUBSTRING(S.SSN, 6, 4)
                                                  WHEN @StudentIdentifier = 'EnrollmentId' THEN SE.EnrollmentId
                                                  WHEN @StudentIdentifier = 'StudentId' THEN S.StudentNumber
                                                  ELSE S.StudentNumber
                                             END AS StudentIdentifier
                                            --S.SSN AS StudentIdentifier                   
                                            ,ISNULL((
                                                    SELECT LastName
                                                    FROM   arStudent
                                                    WHERE  StudentId IN (
                                                                        SELECT StudentId
                                                                        FROM   arStuEnrollments
                                                                        WHERE  StuEnrollId = A.StuEnrollId
                                                                        )
                                                    )
                                                   ,' '
                                                   ) + ',' + ISNULL((
                                                                    SELECT FirstName
                                                                    FROM   arStudent
                                                                    WHERE  StudentId IN (
                                                                                        SELECT StudentId
                                                                                        FROM   arStuEnrollments
                                                                                        WHERE  StuEnrollId = A.StuEnrollId
                                                                                        )
                                                                    )
                                                                   ,' '
                                                                   ) + ' ' + ISNULL((
                                                                                    SELECT MiddleName
                                                                                    FROM   arStudent
                                                                                    WHERE  StudentId IN (
                                                                                                        SELECT StudentId
                                                                                                        FROM   arStuEnrollments
                                                                                                        WHERE  StuEnrollId = A.StuEnrollId
                                                                                                        )
                                                                                    )
                                                                                   ,' '
                                                                                   ) + ' ' AS StudentName
                                            ,(
                                             SELECT SUM(ActualHours)
                                             FROM   dbo.arStudentClockAttendance
                                             WHERE  StuEnrollId = A.StuEnrollId
                                                    AND CONVERT(DATE, RecordDate, 101) = @WeekStartDate
                                                    AND ActualHours > 0
                                                    AND (
                                                        ActualHours <> 99.00
                                                        AND ActualHours <> 999.00
                                                        AND ActualHours <> 9999.00
                                                        )
                                             ) AS Mon
                                            ,(
                                             SELECT SUM(ActualHours)
                                             FROM   dbo.arStudentClockAttendance
                                             WHERE  StuEnrollId = A.StuEnrollId
                                                    AND CONVERT(DATE, RecordDate, 101) = DATEADD(DAY, 1, @WeekStartDate)
                                                    AND ActualHours > 0
                                                    AND (
                                                        ActualHours <> 99.00
                                                        AND ActualHours <> 999.00
                                                        AND ActualHours <> 9999.00
                                                        )
                                             ) AS Tue
                                            ,(
                                             SELECT SUM(ActualHours)
                                             FROM   dbo.arStudentClockAttendance
                                             WHERE  StuEnrollId = A.StuEnrollId
                                                    AND CONVERT(DATE, RecordDate, 101) = DATEADD(DAY, 2, @WeekStartDate)
                                                    AND ActualHours > 0
                                                    AND (
                                                        ActualHours <> 99.00
                                                        AND ActualHours <> 999.00
                                                        AND ActualHours <> 9999.00
                                                        )
                                             ) AS Wed
                                            ,(
                                             SELECT SUM(ActualHours)
                                             FROM   dbo.arStudentClockAttendance
                                             WHERE  StuEnrollId = A.StuEnrollId
                                                    AND CONVERT(DATE, RecordDate, 101) = DATEADD(DAY, 3, @WeekStartDate)
                                                    AND ActualHours > 0
                                                    AND (
                                                        ActualHours <> 99.00
                                                        AND ActualHours <> 999.00
                                                        AND ActualHours <> 9999.00
                                                        )
                                             ) AS Thur
                                            ,(
                                             SELECT SUM(ActualHours)
                                             FROM   dbo.arStudentClockAttendance
                                             WHERE  StuEnrollId = A.StuEnrollId
                                                    AND CONVERT(DATE, RecordDate, 101) = DATEADD(DAY, 4, @WeekStartDate)
                                                    AND ActualHours > 0
                                                    AND (
                                                        ActualHours <> 99.00
                                                        AND ActualHours <> 999.00
                                                        AND ActualHours <> 9999.00
                                                        )
                                             ) AS Fri
                                            ,(
                                             SELECT SUM(ActualHours)
                                             FROM   dbo.arStudentClockAttendance
                                             WHERE  StuEnrollId = A.StuEnrollId
                                                    AND CONVERT(DATE, RecordDate, 101) = DATEADD(DAY, 5, @WeekStartDate)
                                                    AND ActualHours > 0
                                                    AND (
                                                        ActualHours <> 99.00
                                                        AND ActualHours <> 999.00
                                                        AND ActualHours <> 9999.00
                                                        )
                                             ) AS Sat
                                            ,(
                                             SELECT SUM(ActualHours)
                                             FROM   dbo.arStudentClockAttendance
                                             WHERE  StuEnrollId = A.StuEnrollId
                                                    AND CONVERT(DATE, RecordDate, 101) = @WeekEndDate
                                                    AND ActualHours > 0
                                                    AND (
                                                        ActualHours <> 99.00
                                                        AND ActualHours <> 999.00
                                                        AND ActualHours <> 9999.00
                                                        )
                                             ) AS Sun
                                            ,ISNULL((
                                                    SELECT SUM(ActualHours)
                                                    FROM   dbo.arStudentClockAttendance
                                                    WHERE  ( CONVERT(DATE, RecordDate, 101)) <= @WeekEndDate
                                                           AND ( CONVERT(DATE, RecordDate, 101)) >= @WeekStartDate
                                                           AND StuEnrollId = A.StuEnrollId
                                                           AND ActualHours > 0
                                                           AND (
                                                               ActualHours <> 99.00
                                                               AND ActualHours <> 999.00
                                                               AND ActualHours <> 9999.00
                                                               )
                                                    )
                                                   ,0.00
                                                   ) AS WeekTotal
                                            ,ISNULL((
                                                    SELECT SUM(ActualHours)
                                                    FROM   dbo.arStudentClockAttendance
                                                    WHERE  ( CONVERT(DATE, RecordDate, 101)) <= @WeekEndDate
                                                           AND StuEnrollId = A.StuEnrollId
                                                           AND ActualHours > 0
                                                           AND (
                                                               ActualHours <> 99.00
                                                               AND ActualHours <> 999.00
                                                               AND ActualHours <> 9999.00
                                                               )
                                                    )
                                                   ,0.00
                                                   ) AS NewTotal
                                            ,ISNULL((
                                                    SELECT SUM(SchedHours)
                                                    FROM   dbo.arStudentClockAttendance
                                                    WHERE  ( CONVERT(DATE, RecordDate, 101)) <= @WeekEndDate
                                                           AND StuEnrollId = A.StuEnrollId
                                                           AND SchedHours > 0
                                                           AND ActualHours IS NOT NULL
                                                           AND (
                                                               ActualHours <> 99.00
                                                               AND ActualHours <> 999.00
                                                               AND ActualHours <> 9999.00
                                                               )
                                                    )
                                                   ,0.00
                                                   ) AS NewTotalSched
                                            ,ISNULL((
                                                    SELECT SUM(SchedHours - ActualHours)
                                                    FROM   dbo.arStudentClockAttendance
                                                    WHERE  ( CONVERT(DATE, RecordDate, 101)) <= @WeekEndDate
                                                           AND StuEnrollId = A.StuEnrollId
                                                           AND (
                                                               ActualHours <> 99.00
                                                               AND ActualHours <> 999.00
                                                               AND ActualHours <> 9999.00
                                                               )
                                                           AND SchedHours > 0
                                                           AND ActualHours IS NOT NULL
                                                           AND ( ActualHours < SchedHours )
                                                    )
                                                   ,0.00
                                                   ) AS NewTotalDaysAbsent
                                            ,ISNULL((
                                                    SELECT SUM(ActualHours - SchedHours)
                                                    FROM   dbo.arStudentClockAttendance
                                                    WHERE  ( CONVERT(DATE, RecordDate, 101)) <= @WeekEndDate
                                                           AND StuEnrollId = A.StuEnrollId
                                                           AND (
                                                               ActualHours <> 99.00
                                                               AND ActualHours <> 999.00
                                                               AND ActualHours <> 9999.00
                                                               )
                                                           AND SchedHours IS NOT NULL
                                                           AND ActualHours IS NOT NULL
                                                           AND ActualHours <> 0
                                                           AND ActualHours > SchedHours
                                                    )
                                                   ,0.00
                                                   ) AS NewTotalMakeUpDays
                                            ,ISNULL(((
                                                     SELECT SUM(SchedHours - ActualHours)
                                                     FROM   dbo.arStudentClockAttendance
                                                     WHERE  ( CONVERT(DATE, RecordDate, 101)) <= @WeekEndDate
                                                            AND StuEnrollId = A.StuEnrollId
                                                            AND (
                                                                ActualHours <> 99.00
                                                                AND ActualHours <> 999.00
                                                                AND ActualHours <> 9999.00
                                                                )
                                                            AND SchedHours > 0
                                                            AND ActualHours IS NOT NULL
                                                            AND ( ActualHours < SchedHours )
                                                     ) - (
                                                         SELECT SUM(ActualHours - SchedHours)
                                                         FROM   dbo.arStudentClockAttendance
                                                         WHERE  ( CONVERT(DATE, RecordDate, 101)) <= @WeekEndDate
                                                                AND StuEnrollId = A.StuEnrollId
                                                                AND (
                                                                    ActualHours <> 99.00
                                                                    AND ActualHours <> 999.00
                                                                    AND ActualHours <> 9999.00
                                                                    )
                                                                AND SchedHours IS NOT NULL
                                                                AND ActualHours IS NOT NULL
                                                                AND ActualHours <> 0
                                                                AND ActualHours > SchedHours
                                                         )
                                                    )
                                                   ,0.00
                                                   ) AS NewNetDaysAbsent
                                            ,ISNULL((
                                                    SELECT SUM(SchedHours)
                                                    FROM   dbo.arStudentClockAttendance
                                                    WHERE  StuEnrollId = A.StuEnrollId
                                                           AND (
                                                               ActualHours <> 99.00
                                                               AND ActualHours <> 999.00
                                                               AND ActualHours <> 9999.00
                                                               )
                                                           AND CONVERT(DATE, RecordDate, 101) <= dbo.GetLDA(A.StuEnrollId)
                                                    )
                                                   ,0.00
                                                   ) AS NewSchedDaysatLDA
                                            ,( dbo.GetLDA(A.StuEnrollId)) AS LDA
                                            --,CG.CampGrpDescrip                       
                                            ,F.CampDescrip
                                            ,(
                                             SELECT UnitTypeId
                                             FROM   arPrgVersions PV
                                             WHERE  PV.PrgVerId IN (
                                                                   SELECT PrgVerId
                                                                   FROM   arStuEnrollments
                                                                   WHERE  StuEnrollId = A.StuEnrollId
                                                                   )
                                             ) AS AttTypeID
                                            ,(
                                             SELECT     aut.UnitTypeDescrip
                                             FROM       arPrgVersions PV
                                             INNER JOIN dbo.arAttUnitType aut ON aut.UnitTypeId = PV.UnitTypeId
                                             WHERE      PV.PrgVerId IN (
                                                                       SELECT PrgVerId
                                                                       FROM   arStuEnrollments
                                                                       WHERE  StuEnrollId = A.StuEnrollId
                                                                       )
                                             ) AS UnitTypeDescrip
                                            ,'yes' AS SuppressDate
                                            ,SE.StatusCodeId
                                            ,SC.StatusCodeDescrip
                                            ,1 AS ActualDivisor
                                  FROM       dbo.arStudentClockAttendance A
                                  INNER JOIN arStuEnrollments SE ON SE.StuEnrollId = A.StuEnrollId
                                  INNER JOIN arStudent S ON S.StudentId = SE.StudentId
                                  INNER JOIN syCampuses F ON F.CampusId = SE.CampusId
                                  INNER JOIN dbo.syStatusCodes SC ON SE.StatusCodeId = SC.StatusCodeId
                                  WHERE      ( CONVERT(DATE, A.RecordDate, 101)) >= @WeekStartDate
                                             AND ( CONVERT(DATE, A.RecordDate, 101)) <= @WeekEndDate
                                             AND SE.CampusId = F.CampusId
                                             AND F.CampusId = @CampusID
                                             AND (
                                                 @EnrollmentId IS NULL
                                                 OR @EnrollmentId = ''
                                                 OR SE.StatusCodeId IN (
                                                                       SELECT strval
                                                                       FROM   dbo.SPLIT(@EnrollmentId)
                                                                       )
                                                 )
                                  ) B
                         GROUP BY B.StuEnrollId
                                 ,B.StudentName
                                 ,B.StudentIdentifier
                                 --,B.CampGrpDescrip                       
                                 ,B.CampDescrip
                                 ,B.Mon
                                 ,B.Tue
                                 ,B.Wed
                                 ,B.Thur
                                 ,B.Fri
                                 ,B.Sat
                                 ,B.Sun
                                 ,B.WeekTotal
                                 ,B.NewTotal
                                 ,B.NewTotalSched
                                 ,B.NewTotalDaysAbsent
                                 ,B.NewTotalMakeUpDays
                                 ,B.NewNetDaysAbsent
                                 ,B.NewSchedDaysatLDA
                                 ,B.LDA
                                 ,B.AttTypeID
                                 ,B.SuppressDate
                                 ,B.StatusCodeId
                                 ,B.StatusCodeDescrip
                                 ,B.ActualDivisor
								 ,B.UnitTypeDescrip
                         ) C
                GROUP BY C.StuEnrollId
                        ,C.StudentIdentifier
                        ,C.StudentName
                        ,C.LDA
                        --,C.CampGrpDescrip                       
                        ,C.AttTypeID
                        ,C.SuppressDate
                        ,C.StatusCodeId
                        ,C.StatusCodeDescrip
                        ,C.ActualDivisor
						,C.UnitTypeDescrip
                ORDER BY C.StudentName
                        ,C.StudentIdentifier;

            END;
        ELSE
            SELECT   C.StuEnrollId
                    ,C.StudentIdentifier
                    ,C.StudentName
                    ,CONVERT(DECIMAL(15, 2), SUM(C.Mon) / ActualDivisor) AS Mon
                    ,CONVERT(DECIMAL(15, 2), SUM(C.Tue) / ActualDivisor) AS Tue
                    ,CONVERT(DECIMAL(15, 2), SUM(C.Wed) / ActualDivisor) AS Wed
                    ,CONVERT(DECIMAL(15, 2), SUM(C.Thur) / ActualDivisor) AS Thur
                    ,CONVERT(DECIMAL(15, 2), SUM(C.Fri) / ActualDivisor) AS Fri
                    ,CONVERT(DECIMAL(15, 2), SUM(C.Sat) / ActualDivisor) AS Sat
                    ,CONVERT(DECIMAL(15, 2), SUM(C.Sun) / ActualDivisor) AS Sun
                    ,CONVERT(DECIMAL(15, 2), SUM(C.WeekTotal) / ActualDivisor) AS WeekTotal
                    ,CONVERT(DECIMAL(15, 2), SUM(C.NewTotal) / ActualDivisor) AS NewTotal
                    ,CONVERT(DECIMAL(15, 2), SUM(C.NewTotalSched) / ActualDivisor) AS NewTotalSched
                    ,CONVERT(DECIMAL(15, 2), SUM(C.NewTotalDaysAbsent) / ActualDivisor) AS NewTotalDaysAbsent
                    ,CONVERT(DECIMAL(15, 2), SUM(C.NewTotalMakeUpDays) / ActualDivisor) AS NewTotalMakeUpDays
                    ,CONVERT(DECIMAL(15, 2), SUM(C.NewNetDaysAbsent) / ActualDivisor) AS NewNetDaysAbsent
                    ,CONVERT(DECIMAL(15, 2), SUM(C.NewSchedDaysatLDA) / ActualDivisor) AS NewSchedDaysatLDA
                    ,C.LDA
                    --,C.CampGrpDescrip                       
                    ,C.AttTypeID
                    ,C.SuppressDate
                    ,C.ClsSectionId
                    ,C.ClsSection
                    ,Descrip
                    ,FullName
                    ,TermId
                    ,TermDescrip
                    ,ReqId
                    ,Code
                    ,InstructorId
                    ,ShiftId
                    ,StatusCodeId
                    ,StatusCodeDescrip
                    ,UnitTypeDescrip
            FROM     (
                     SELECT   B.*
                     FROM     (
                              SELECT     A.StuEnrollId
                                        ,CASE WHEN @StudentIdentifier = 'SSN' THEN '***-**-' + SUBSTRING(S.SSN, 6, 4)
                                              WHEN @StudentIdentifier = 'EnrollmentId' THEN SE.EnrollmentId
                                              WHEN @StudentIdentifier = 'StudentId' THEN S.StudentNumber
                                              ELSE S.StudentNumber
                                         END AS StudentIdentifier
                                        --S.SSN AS StudentIdentifier                   
                                        ,ISNULL((
                                                SELECT LastName
                                                FROM   arStudent
                                                WHERE  StudentId IN (
                                                                    SELECT StudentId
                                                                    FROM   arStuEnrollments
                                                                    WHERE  StuEnrollId = A.StuEnrollId
                                                                    )
                                                )
                                               ,' '
                                               ) + ',' + ISNULL((
                                                                SELECT FirstName
                                                                FROM   arStudent
                                                                WHERE  StudentId IN (
                                                                                    SELECT StudentId
                                                                                    FROM   arStuEnrollments
                                                                                    WHERE  StuEnrollId = A.StuEnrollId
                                                                                    )
                                                                )
                                                               ,' '
                                                               ) + ' ' + ISNULL((
                                                                                SELECT MiddleName
                                                                                FROM   arStudent
                                                                                WHERE  StudentId IN (
                                                                                                    SELECT StudentId
                                                                                                    FROM   arStuEnrollments
                                                                                                    WHERE  StuEnrollId = A.StuEnrollId
                                                                                                    )
                                                                                )
                                                                               ,' '
                                                                               ) + ' ' AS StudentName
                                        ,(
                                         SELECT SUM(Actual)
                                         FROM   atClsSectAttendance
                                         WHERE  StuEnrollId = A.StuEnrollId
                                                AND ClsSectionId = A.ClsSectionId
                                                AND CONVERT(DATE, MeetDate, 101) = @WeekStartDate
                                                AND Actual <> 999.0
                                                AND Actual <> 9999.0
                                         ) AS Mon
                                        ,(
                                         SELECT SUM(Actual)
                                         FROM   atClsSectAttendance
                                         WHERE  StuEnrollId = A.StuEnrollId
                                                AND ClsSectionId = A.ClsSectionId
                                                AND CONVERT(DATE, MeetDate, 101) = DATEADD(DAY, 1, @WeekStartDate)
                                                AND Actual <> 999.0
                                                AND Actual <> 9999.0
                                         ) AS Tue
                                        ,(
                                         SELECT SUM(Actual)
                                         FROM   atClsSectAttendance
                                         WHERE  StuEnrollId = A.StuEnrollId
                                                AND ClsSectionId = A.ClsSectionId
                                                AND CONVERT(DATE, MeetDate, 101) = DATEADD(DAY, 2, @WeekStartDate)
                                                AND Actual <> 999.0
                                                AND Actual <> 9999.0
                                         ) AS Wed
                                        ,(
                                         SELECT SUM(Actual)
                                         FROM   atClsSectAttendance
                                         WHERE  StuEnrollId = A.StuEnrollId
                                                AND ClsSectionId = A.ClsSectionId
                                                AND CONVERT(DATE, MeetDate, 101) = DATEADD(DAY, 3, @WeekStartDate)
                                                AND Actual <> 999.0
                                                AND Actual <> 9999.0
                                         ) AS Thur
                                        ,(
                                         SELECT SUM(Actual)
                                         FROM   atClsSectAttendance
                                         WHERE  StuEnrollId = A.StuEnrollId
                                                AND ClsSectionId = A.ClsSectionId
                                                AND CONVERT(DATE, MeetDate, 101) = DATEADD(DAY, 4, @WeekStartDate)
                                                AND Actual <> 999.0
                                                AND Actual <> 9999.0
                                         ) AS Fri
                                        ,(
                                         SELECT SUM(Actual)
                                         FROM   atClsSectAttendance
                                         WHERE  StuEnrollId = A.StuEnrollId
                                                AND ClsSectionId = A.ClsSectionId
                                                AND CONVERT(DATE, MeetDate, 101) = DATEADD(DAY, 5, @WeekStartDate)
                                                AND Actual <> 999.0
                                                AND Actual <> 9999.0
                                         ) AS Sat
                                        ,(
                                         SELECT SUM(Actual)
                                         FROM   atClsSectAttendance
                                         WHERE  StuEnrollId = A.StuEnrollId
                                                AND ClsSectionId = A.ClsSectionId
                                                AND CONVERT(DATE, MeetDate, 101) = @WeekEndDate
                                                AND Actual <> 999.0
                                                AND Actual <> 9999.0
                                         ) AS Sun
                                        ,ISNULL((
                                                SELECT SUM(Actual)
                                                FROM   atClsSectAttendance
                                                WHERE  ( CONVERT(DATE, MeetDate, 101)) <= @WeekEndDate
                                                       AND ( CONVERT(DATE, MeetDate, 101)) >= @WeekStartDate
                                                       AND StuEnrollId = A.StuEnrollId
                                                       AND ClsSectionId = A.ClsSectionId
                                                       AND Actual <> 999.0
                                                       AND Actual <> 9999.0
                                                )
                                               ,0.00
                                               ) AS WeekTotal
                                        ,ISNULL((
                                                SELECT SUM(Actual)
                                                FROM   atClsSectAttendance
                                                WHERE  ( CONVERT(DATE, MeetDate, 101)) <= @WeekEndDate
                                                       AND StuEnrollId = A.StuEnrollId
                                                       AND ClsSectionId = A.ClsSectionId
                                                       AND Actual <> 999.0
                                                       AND Actual <> 9999.0
                                                )
                                               ,0.00
                                               ) AS NewTotal
                                        ,ISNULL((
                                                SELECT SUM(Scheduled)
                                                FROM   atClsSectAttendance
                                                WHERE  ( CONVERT(DATE, MeetDate, 101)) <= @WeekEndDate
                                                       AND StuEnrollId = A.StuEnrollId
                                                       AND ClsSectionId = A.ClsSectionId
                                                       AND Actual <> 999.0
                                                       AND Actual <> 9999.0
                                                )
                                               ,0.00
                                               ) AS NewTotalSched
                                        ,ISNULL((
                                                SELECT SUM(Scheduled - Actual)
                                                FROM   atClsSectAttendance
                                                WHERE  ( CONVERT(DATE, MeetDate, 101)) <= @WeekEndDate
                                                       AND StuEnrollId = A.StuEnrollId
                                                       AND ClsSectionId = A.ClsSectionId
                                                       AND Actual <> 999.0
                                                       AND Actual <> 9999.0
                                                       AND Scheduled IS NOT NULL
                                                       AND Scheduled <> 0
                                                       AND Actual IS NOT NULL
                                                       AND Scheduled > Actual
                                                )
                                               ,0.00
                                               ) AS NewTotalDaysAbsent
                                        ,ISNULL((
                                                SELECT SUM(Actual - Scheduled)
                                                FROM   atClsSectAttendance
                                                WHERE  ( CONVERT(DATE, MeetDate, 101)) <= @WeekEndDate
                                                       AND StuEnrollId = A.StuEnrollId
                                                       AND ClsSectionId = A.ClsSectionId
                                                       AND Actual <> 999.0
                                                       AND Actual <> 9999.0
                                                       AND Scheduled IS NOT NULL
                                                       AND Actual IS NOT NULL
                                                       AND Actual <> 0
                                                       AND Scheduled < Actual
                                                )
                                               ,0.00
                                               ) AS NewTotalMakeUpDays
                                        ,ISNULL(((
                                                 SELECT SUM(Scheduled - Actual)
                                                 FROM   atClsSectAttendance
                                                 WHERE  ( CONVERT(DATE, MeetDate, 101)) <= @WeekEndDate
                                                        AND StuEnrollId = A.StuEnrollId
                                                        AND ClsSectionId = A.ClsSectionId
                                                        AND Actual <> 999.0
                                                        AND Actual <> 9999.0
                                                        AND Scheduled IS NOT NULL
                                                        AND Scheduled <> 0
                                                        AND Actual IS NOT NULL
                                                        AND Scheduled > Actual
                                                 ) - (
                                                     SELECT SUM(Actual - Scheduled)
                                                     FROM   atClsSectAttendance
                                                     WHERE  ( CONVERT(DATE, MeetDate, 101)) <= @WeekEndDate
                                                            AND StuEnrollId = A.StuEnrollId
                                                            AND ClsSectionId = A.ClsSectionId
                                                            AND Actual <> 999.0
                                                            AND Actual <> 9999.0
                                                            AND Scheduled IS NOT NULL
                                                            AND Actual IS NOT NULL
                                                            AND Actual <> 0
                                                            AND Scheduled < Actual
                                                     )
                                                )
                                               ,0.00
                                               ) AS NewNetDaysAbsent
                                        ,ISNULL((
                                                SELECT SUM(Scheduled)
                                                FROM   atClsSectAttendance
                                                WHERE  StuEnrollId = A.StuEnrollId
                                                       AND ClsSectionId = A.ClsSectionId
                                                       AND Actual <> 999.0
                                                       AND Actual <> 9999.0
                                                       AND CONVERT(DATE, MeetDate, 101) <= dbo.GetLDA(A.StuEnrollId)
                                                )
                                               ,0.00
                                               ) AS NewSchedDaysatLDA
                                        ,(
                                         SELECT CONVERT(VARCHAR, MAX(MeetDate), 101)
                                         FROM   atClsSectAttendance
                                         WHERE  StuEnrollId = A.StuEnrollId
                                                AND Actual <> 999.0
                                                AND Actual <> 9999.0
                                                AND Actual <> 0.00
                                         ) AS LDA
                                        --,CG.CampGrpDescrip                       
                                        ,F.CampDescrip
                                        ,(
                                         SELECT UnitTypeId
                                         FROM   arPrgVersions PV
                                         WHERE  PV.PrgVerId IN (
                                                               SELECT PrgVerId
                                                               FROM   arStuEnrollments
                                                               WHERE  StuEnrollId = A.StuEnrollId
                                                               )
                                         ) AS AttTypeID
                                        ,'yes' AS SuppressDate
                                        ,A.ClsSectionId
                                        ,CS.ClsSection
                                        ,R.Descrip
                                        ,U.FullName
                                        ,CS.TermId
                                        ,T.TermDescrip
                                        ,CS.ReqId
                                        ,R.Code
                                        ,CS.InstructorId
                                        ,CS.ShiftId
                                        ,SE.StatusCodeId
                                        ,SC.StatusCodeDescrip
                                        ,CASE WHEN UT.UnitTypeId = 'B937C92E-FD7A-455E-A731-527A9918C734' THEN 60
                                              ELSE 1
                                         END AS ActualDivisor
                                        ,UT.UnitTypeDescrip
                              FROM       atClsSectAttendance A
                              INNER JOIN arStuEnrollments SE ON SE.StuEnrollId = A.StuEnrollId
                              INNER JOIN arStudent S ON S.StudentId = SE.StudentId
                              INNER JOIN syCampuses F ON F.CampusId = SE.CampusId
                              INNER JOIN dbo.arClassSections CS ON CS.ClsSectionId = A.ClsSectionId
                              INNER JOIN dbo.arTerm T ON CS.TermId = T.TermId
                              INNER JOIN dbo.arReqs R ON R.ReqId = CS.ReqId
                              INNER JOIN dbo.syUsers U ON U.UserId = CS.InstructorId
                              INNER JOIN dbo.syStatusCodes SC ON SE.StatusCodeId = SC.StatusCodeId
                              INNER JOIN dbo.arAttUnitType UT ON R.UnitTypeId = UT.UnitTypeId
                              WHERE      ( CONVERT(DATE, MeetDate, 101)) >= @WeekStartDate
                                         AND ( CONVERT(DATE, MeetDate, 101)) <= @WeekEndDate
                                         AND SE.CampusId = F.CampusId
                                         AND A.ClsSectionId = CS.ClsSectionId
                                         AND F.CampusId = @CampusID
                                         AND (
                                             @TermId IS NULL
                                             OR @TermId = ''
                                             OR T.TermId IN (
                                                            SELECT strval
                                                            FROM   dbo.SPLIT(@TermId)
                                                            )
                                             )
                                         AND (
                                             @CourseId IS NULL
                                             OR @CourseId = ''
                                             OR R.ReqId IN (
                                                           SELECT strval
                                                           FROM   dbo.SPLIT(@CourseId)
                                                           )
                                             )
                                         AND (
                                             @InstructorId IS NULL
                                             OR @InstructorId = ''
                                             OR CS.InstructorId IN (
                                                                   SELECT strval
                                                                   FROM   dbo.SPLIT(@InstructorId)
                                                                   )
                                             )
                                         AND (
                                             @ShiftId IS NULL
                                             OR @ShiftId = ''
                                             OR CS.ShiftId IN (
                                                              SELECT strval
                                                              FROM   dbo.SPLIT(@ShiftId)
                                                              )
                                             )
                                         AND (
                                             @ClassId IS NULL
                                             OR @ClassId = ''
                                             OR CS.ClsSectionId IN (
                                                                   SELECT strval
                                                                   FROM   dbo.SPLIT(@ClassId)
                                                                   )
                                             )
                                         AND (
                                             @EnrollmentId IS NULL
                                             OR @EnrollmentId = ''
                                             OR SE.StatusCodeId IN (
                                                                   SELECT strval
                                                                   FROM   dbo.SPLIT(@EnrollmentId)
                                                                   )
                                             )
                              ) B
                     GROUP BY B.StuEnrollId
                             ,B.StudentName
                             ,B.StudentIdentifier
                             --,B.CampGrpDescrip                       
                             ,B.CampDescrip
                             ,B.Mon
                             ,B.Tue
                             ,B.Wed
                             ,B.Thur
                             ,B.Fri
                             ,B.Sat
                             ,B.Sun
                             ,B.WeekTotal
                             ,B.NewTotal
                             ,B.NewTotalSched
                             ,B.NewTotalDaysAbsent
                             ,B.NewTotalMakeUpDays
                             ,B.NewNetDaysAbsent
                             ,B.NewSchedDaysatLDA
                             ,B.LDA
                             ,B.AttTypeID
                             ,B.SuppressDate
                             ,B.ClsSectionId
                             ,B.ClsSection
                             ,B.Descrip
                             ,B.FullName
                             ,B.TermId
                             ,B.TermDescrip
                             ,B.ReqId
                             ,B.Code
                             ,B.InstructorId
                             ,B.ShiftId
                             ,B.StatusCodeId
                             ,B.StatusCodeDescrip
                             ,B.ActualDivisor
                             ,B.UnitTypeDescrip
                     ) C
            GROUP BY C.StuEnrollId
                    ,C.StudentIdentifier
                    ,C.StudentName
                    ,C.LDA
                    --,C.CampGrpDescrip                       
                    ,C.AttTypeID
                    ,C.SuppressDate
                    ,C.ClsSectionId
                    ,C.ClsSection
                    ,C.Descrip
                    ,C.FullName
                    ,TermId
                    ,TermDescrip
                    ,C.ReqId
                    ,C.Code
                    ,C.InstructorId
                    ,C.ShiftId
                    ,C.StatusCodeId
                    ,C.StatusCodeDescrip
                    ,C.ActualDivisor
                    ,C.UnitTypeDescrip
            ORDER BY C.StudentName
                    ,C.StudentIdentifier
                    ,C.ClsSectionId;
    END;
--================================================================================================= 
-- END  --  USP_WeeklyAttendance 
--================================================================================================= 
GO
