SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--EXEC Usp_MoveMaintenancePage_InModules_General 0,0,0,0,0,1,0,0,20
--GO 
CREATE PROCEDURE [dbo].[Usp_MoveMaintenancePage_InModules_General]
    @AD_ModuleId BIT = 0
   ,@AR_ModuleId BIT = 0
   ,@SA_ModuleId BIT = 0
   ,@PL_ModuleId BIT = 0
   ,@FA_ModuleId BIT = 0
   ,@HR_ModuleId BIT = 0
   ,@FAC_ModuleId BIT = 0
   ,@SY_ModuleId BIT = 0
   ,@MaintenanePageId INT
   ,@UserName VARCHAR(50)
AS
    DECLARE @ParentId UNIQUEIDENTIFIER
       ,@ResId INT
       ,@Maintenance_HierarchyId UNIQUEIDENTIFIER;
	-- If Item originally belongs to Admissions
    IF @MaintenanePageId IN ( 20,29,34,339,387,388,389,390,491,12,14,302,15,216,215,212,214,358,336,344,337,13,16,18,241,296,21,35,239,28,350,44,38 )
        BEGIN 
			-- Module (AD)
			-- If Maintenance Page is part of Admissions Module, don't let user add again or delete from Admissions Module
            IF ( @AD_ModuleId ) = 1
                AND @MaintenanePageId NOT IN ( 20,29,34,339,387,388,389,390,491,12,14,302,15,216,215,212,214,358,336,344,337,13,16,18,241,296,21,35,239,28,350,
                                               44,38 )
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 189 )
                                    );
                    SET @ResId = 702;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF NOT EXISTS ( SELECT TOP 1
                                            ResourceId
                                    FROM    syNavigationNodes
                                    WHERE   ResourceId = @MaintenanePageId
                                            AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            INSERT  INTO syNavigationNodes
                                    (
                                     HierarchyId
                                    ,HierarchyIndex
                                    ,ResourceId
                                    ,ParentId
                                    ,ModUser
                                    ,ModDate
                                    ,IsPopupWindow
									)
                            VALUES  (
                                     NEWID()
                                    ,1
                                    ,@MaintenanePageId
                                    ,@Maintenance_HierarchyId
                                    ,@UserName
                                    ,GETDATE()
                                    ,0
									);
                        END;
                END; 
			-- if AD is not selected, remove page from AD
			-- If Maintenance Page is part of Admissions Module, don't let user add again or delete from Admissions Module
            IF @AD_ModuleId = 0
                AND @MaintenanePageId NOT IN ( 20,29,34,339,387,388,389,390,491,12,14,302,15,216,215,212,214,358,336,344,337,13,16,18,241,296,21,35,239,28,350,
                                               44,38 )
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 189 )
                                    );
                    SET @ResId = 702;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF EXISTS ( SELECT TOP 1
                                        ResourceId
                                FROM    syNavigationNodes
                                WHERE   ResourceId = @MaintenanePageId
                                        AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            DELETE  FROM dbo.syNavigationNodes
                            WHERE   ResourceId = @MaintenanePageId
                                    AND ParentId = @Maintenance_HierarchyId;
                        END;
                END; 
			-- Module = AR
            IF @AR_ModuleId = 1 -- if AR is selected 
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 26 )
                                    );
                    SET @ResId = 703;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF NOT EXISTS ( SELECT TOP 1
                                            ResourceId
                                    FROM    syNavigationNodes
                                    WHERE   ResourceId = @MaintenanePageId
                                            AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            INSERT  INTO syNavigationNodes
                                    (
                                     HierarchyId
                                    ,HierarchyIndex
                                    ,ResourceId
                                    ,ParentId
                                    ,ModUser
                                    ,ModDate
                                    ,IsPopupWindow
									)
                            VALUES  (
                                     NEWID()
                                    ,1
                                    ,@MaintenanePageId
                                    ,@Maintenance_HierarchyId
                                    ,@UserName
                                    ,GETDATE()
                                    ,0
									);
                        END;
                END; 
            IF @AR_ModuleId = 0 -- if AR is not selected, remove page from AR
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 26 )
                                    );
                    SET @ResId = 703;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF EXISTS ( SELECT TOP 1
                                        ResourceId
                                FROM    syNavigationNodes
                                WHERE   ResourceId = @MaintenanePageId
                                        AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            DELETE  FROM dbo.syNavigationNodes
                            WHERE   ResourceId = @MaintenanePageId
                                    AND ParentId = @Maintenance_HierarchyId;
                        END;
                END; 
			-- Module = SA
            IF @SA_ModuleId = 1 -- if SA is selected 
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 194 )
                                    );
                    SET @ResId = 705;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF NOT EXISTS ( SELECT TOP 1
                                            ResourceId
                                    FROM    syNavigationNodes
                                    WHERE   ResourceId = @MaintenanePageId
                                            AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            INSERT  INTO syNavigationNodes
                                    (
                                     HierarchyId
                                    ,HierarchyIndex
                                    ,ResourceId
                                    ,ParentId
                                    ,ModUser
                                    ,ModDate
                                    ,IsPopupWindow
									)
                            VALUES  (
                                     NEWID()
                                    ,1
                                    ,@MaintenanePageId
                                    ,@Maintenance_HierarchyId
                                    ,@UserName
                                    ,GETDATE()
                                    ,0
									);
                        END;
                END; 
            IF @SA_ModuleId = 0 -- if AR is not selected, remove page from AR
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 194 )
                                    );
                    SET @ResId = 705;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF EXISTS ( SELECT TOP 1
                                        ResourceId
                                FROM    syNavigationNodes
                                WHERE   ResourceId = @MaintenanePageId
                                        AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            DELETE  FROM dbo.syNavigationNodes
                            WHERE   ResourceId = @MaintenanePageId
                                    AND ParentId = @Maintenance_HierarchyId;
                        END;
                END; 
			-- Placement
            IF @PL_ModuleId = 1 -- if SA is selected 
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 193 )
                                    );
                    SET @ResId = 704;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF NOT EXISTS ( SELECT TOP 1
                                            ResourceId
                                    FROM    syNavigationNodes
                                    WHERE   ResourceId = @MaintenanePageId
                                            AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            INSERT  INTO syNavigationNodes
                                    (
                                     HierarchyId
                                    ,HierarchyIndex
                                    ,ResourceId
                                    ,ParentId
                                    ,ModUser
                                    ,ModDate
                                    ,IsPopupWindow
									)
                            VALUES  (
                                     NEWID()
                                    ,1
                                    ,@MaintenanePageId
                                    ,@Maintenance_HierarchyId
                                    ,@UserName
                                    ,GETDATE()
                                    ,0
									);
                        END;
                END; 
            IF @PL_ModuleId = 0 -- if AR is not selected, remove page from AR
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 193 )
                                    );
                    SET @ResId = 704;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF EXISTS ( SELECT TOP 1
                                        ResourceId
                                FROM    syNavigationNodes
                                WHERE   ResourceId = @MaintenanePageId
                                        AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            DELETE  FROM dbo.syNavigationNodes
                            WHERE   ResourceId = @MaintenanePageId
                                    AND ParentId = @Maintenance_HierarchyId;
                        END;
                END; 
			-- Financial Aid
            IF @FA_ModuleId = 1 -- if SA is selected 
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 191 )
                                    );
                    SET @ResId = 706;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF NOT EXISTS ( SELECT TOP 1
                                            ResourceId
                                    FROM    syNavigationNodes
                                    WHERE   ResourceId = @MaintenanePageId
                                            AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            INSERT  INTO syNavigationNodes
                                    (
                                     HierarchyId
                                    ,HierarchyIndex
                                    ,ResourceId
                                    ,ParentId
                                    ,ModUser
                                    ,ModDate
                                    ,IsPopupWindow
									)
                            VALUES  (
                                     NEWID()
                                    ,1
                                    ,@MaintenanePageId
                                    ,@Maintenance_HierarchyId
                                    ,@UserName
                                    ,GETDATE()
                                    ,0
									);
                        END;
                END; 
            IF @FA_ModuleId = 0 -- if AR is not selected, remove page from AR
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 191 )
                                    );
                    SET @ResId = 706;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF EXISTS ( SELECT TOP 1
                                        ResourceId
                                FROM    syNavigationNodes
                                WHERE   ResourceId = @MaintenanePageId
                                        AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            DELETE  FROM dbo.syNavigationNodes
                            WHERE   ResourceId = @MaintenanePageId
                                    AND ParentId = @Maintenance_HierarchyId;
                        END;
                END; 
			-- 	System
            IF @SY_ModuleId = 1 -- if SA is selected 
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 195 )
                                    );
                    SET @ResId = 707;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF NOT EXISTS ( SELECT TOP 1
                                            ResourceId
                                    FROM    syNavigationNodes
                                    WHERE   ResourceId = @MaintenanePageId
                                            AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            INSERT  INTO syNavigationNodes
                                    (
                                     HierarchyId
                                    ,HierarchyIndex
                                    ,ResourceId
                                    ,ParentId
                                    ,ModUser
                                    ,ModDate
                                    ,IsPopupWindow
									)
                            VALUES  (
                                     NEWID()
                                    ,1
                                    ,@MaintenanePageId
                                    ,@Maintenance_HierarchyId
                                    ,@UserName
                                    ,GETDATE()
                                    ,0
									);
                        END;
                END; 
            IF @SY_ModuleId = 0 -- if AR is not selected, remove page from AR
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 195 )
                                    );
                    SET @ResId = 707;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF EXISTS ( SELECT TOP 1
                                        ResourceId
                                FROM    syNavigationNodes
                                WHERE   ResourceId = @MaintenanePageId
                                        AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            DELETE  FROM dbo.syNavigationNodes
                            WHERE   ResourceId = @MaintenanePageId
                                    AND ParentId = @Maintenance_HierarchyId;
                        END;
                END; 
			-- 	Human Resources
            IF @HR_ModuleId = 1 -- if SA is selected 
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 192 )
                                    );
                    SET @ResId = 709;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF NOT EXISTS ( SELECT TOP 1
                                            ResourceId
                                    FROM    syNavigationNodes
                                    WHERE   ResourceId = @MaintenanePageId
                                            AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            INSERT  INTO syNavigationNodes
                                    (
                                     HierarchyId
                                    ,HierarchyIndex
                                    ,ResourceId
                                    ,ParentId
                                    ,ModUser
                                    ,ModDate
                                    ,IsPopupWindow
									)
                            VALUES  (
                                     NEWID()
                                    ,1
                                    ,@MaintenanePageId
                                    ,@Maintenance_HierarchyId
                                    ,@UserName
                                    ,GETDATE()
                                    ,0
									);
                        END;
                END; 
            IF @HR_ModuleId = 0 -- if HR is not selected, remove page from HR
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 192 )
                                    );
                    SET @ResId = 709;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF EXISTS ( SELECT TOP 1
                                        ResourceId
                                FROM    syNavigationNodes
                                WHERE   ResourceId = @MaintenanePageId
                                        AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            DELETE  FROM dbo.syNavigationNodes
                            WHERE   ResourceId = @MaintenanePageId
                                    AND ParentId = @Maintenance_HierarchyId;
                        END;
                END; 
        END; 
		
		-- If Item Belongs to Academic Records
    IF @MaintenanePageId IN ( 4,6,10,39,43,66,67,280,297,316,323,505,552,581,627,40,14,161,1,11,100,101,350,44,38,623,624,361,150,149,5,289,41 )
        BEGIN 
			-- Module (AD)
			-- If Maintenance Page is part of Admissions Module, don't let user add again or delete from Admissions Module
            IF ( @AR_ModuleId ) = 1
                AND @MaintenanePageId NOT IN ( 4,6,10,39,43,66,67,280,297,316,323,505,552,581,627,40,14,161,1,11,100,101,350,44,38,623,624,361,150,149,5,289,41 )
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 26 )
                                    );
                    SET @ResId = 703;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF NOT EXISTS ( SELECT TOP 1
                                            ResourceId
                                    FROM    syNavigationNodes
                                    WHERE   ResourceId = @MaintenanePageId
                                            AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            INSERT  INTO syNavigationNodes
                                    (
                                     HierarchyId
                                    ,HierarchyIndex
                                    ,ResourceId
                                    ,ParentId
                                    ,ModUser
                                    ,ModDate
                                    ,IsPopupWindow
									)
                            VALUES  (
                                     NEWID()
                                    ,1
                                    ,@MaintenanePageId
                                    ,@Maintenance_HierarchyId
                                    ,@UserName
                                    ,GETDATE()
                                    ,0
									);
                        END;
                END; 
			-- if AD is not selected, remove page from AD
			-- If Maintenance Page is part of Admissions Module, don't let user add again or delete from Admissions Module
            IF @AR_ModuleId = 0
                AND @MaintenanePageId NOT IN ( 4,6,10,39,43,66,67,280,297,316,323,505,552,581,627,40,14,161,1,11,100,101,350,44,38,623,624,361,150,149,5,289,41 )
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 26 )
                                    );
                    SET @ResId = 703;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF EXISTS ( SELECT TOP 1
                                        ResourceId
                                FROM    syNavigationNodes
                                WHERE   ResourceId = @MaintenanePageId
                                        AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            DELETE  FROM dbo.syNavigationNodes
                            WHERE   ResourceId = @MaintenanePageId
                                    AND ParentId = @Maintenance_HierarchyId;
                        END;
                END; 
			-- Module = AD
            IF @AD_ModuleId = 1 -- if AD is selected 
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 189 )
                                    );
                    SET @ResId = 702;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF NOT EXISTS ( SELECT TOP 1
                                            ResourceId
                                    FROM    syNavigationNodes
                                    WHERE   ResourceId = @MaintenanePageId
                                            AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            INSERT  INTO syNavigationNodes
                                    (
                                     HierarchyId
                                    ,HierarchyIndex
                                    ,ResourceId
                                    ,ParentId
                                    ,ModUser
                                    ,ModDate
                                    ,IsPopupWindow
									)
                            VALUES  (
                                     NEWID()
                                    ,1
                                    ,@MaintenanePageId
                                    ,@Maintenance_HierarchyId
                                    ,@UserName
                                    ,GETDATE()
                                    ,0
									);
                        END;
                END; 
            IF @AD_ModuleId = 0 -- if AR is not selected, remove page from AR
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 189 )
                                    );
                    SET @ResId = 702;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF EXISTS ( SELECT TOP 1
                                        ResourceId
                                FROM    syNavigationNodes
                                WHERE   ResourceId = @MaintenanePageId
                                        AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            DELETE  FROM dbo.syNavigationNodes
                            WHERE   ResourceId = @MaintenanePageId
                                    AND ParentId = @Maintenance_HierarchyId;
                        END;
                END; 
			-- Module = SA
            IF @SA_ModuleId = 1 -- if SA is selected 
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 194 )
                                    );
                    SET @ResId = 705;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF NOT EXISTS ( SELECT TOP 1
                                            ResourceId
                                    FROM    syNavigationNodes
                                    WHERE   ResourceId = @MaintenanePageId
                                            AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            INSERT  INTO syNavigationNodes
                                    (
                                     HierarchyId
                                    ,HierarchyIndex
                                    ,ResourceId
                                    ,ParentId
                                    ,ModUser
                                    ,ModDate
                                    ,IsPopupWindow
									)
                            VALUES  (
                                     NEWID()
                                    ,1
                                    ,@MaintenanePageId
                                    ,@Maintenance_HierarchyId
                                    ,@UserName
                                    ,GETDATE()
                                    ,0
									);
                        END;
                END; 
            IF @SA_ModuleId = 0 -- if AR is not selected, remove page from AR
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 194 )
                                    );
                    SET @ResId = 705;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF EXISTS ( SELECT TOP 1
                                        ResourceId
                                FROM    syNavigationNodes
                                WHERE   ResourceId = @MaintenanePageId
                                        AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            DELETE  FROM dbo.syNavigationNodes
                            WHERE   ResourceId = @MaintenanePageId
                                    AND ParentId = @Maintenance_HierarchyId;
                        END;
                END; 
			-- Placement
            IF @PL_ModuleId = 1 -- if SA is selected 
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 193 )
                                    );
                    SET @ResId = 704;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF NOT EXISTS ( SELECT TOP 1
                                            ResourceId
                                    FROM    syNavigationNodes
                                    WHERE   ResourceId = @MaintenanePageId
                                            AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            INSERT  INTO syNavigationNodes
                                    (
                                     HierarchyId
                                    ,HierarchyIndex
                                    ,ResourceId
                                    ,ParentId
                                    ,ModUser
                                    ,ModDate
                                    ,IsPopupWindow
									)
                            VALUES  (
                                     NEWID()
                                    ,1
                                    ,@MaintenanePageId
                                    ,@Maintenance_HierarchyId
                                    ,@UserName
                                    ,GETDATE()
                                    ,0
									);
                        END;
                END; 
            IF @PL_ModuleId = 0 -- if AR is not selected, remove page from AR
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 193 )
                                    );
                    SET @ResId = 704;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF EXISTS ( SELECT TOP 1
                                        ResourceId
                                FROM    syNavigationNodes
                                WHERE   ResourceId = @MaintenanePageId
                                        AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            DELETE  FROM dbo.syNavigationNodes
                            WHERE   ResourceId = @MaintenanePageId
                                    AND ParentId = @Maintenance_HierarchyId;
                        END;
                END; 
			-- Financial Aid
            IF @FA_ModuleId = 1 -- if SA is selected 
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 191 )
                                    );
                    SET @ResId = 706;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF NOT EXISTS ( SELECT TOP 1
                                            ResourceId
                                    FROM    syNavigationNodes
                                    WHERE   ResourceId = @MaintenanePageId
                                            AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            INSERT  INTO syNavigationNodes
                                    (
                                     HierarchyId
                                    ,HierarchyIndex
                                    ,ResourceId
                                    ,ParentId
                                    ,ModUser
                                    ,ModDate
                                    ,IsPopupWindow
									)
                            VALUES  (
                                     NEWID()
                                    ,1
                                    ,@MaintenanePageId
                                    ,@Maintenance_HierarchyId
                                    ,@UserName
                                    ,GETDATE()
                                    ,0
									);
                        END;
                END; 
            IF @FA_ModuleId = 0 -- if AR is not selected, remove page from AR
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 191 )
                                    );
                    SET @ResId = 706;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF EXISTS ( SELECT TOP 1
                                        ResourceId
                                FROM    syNavigationNodes
                                WHERE   ResourceId = @MaintenanePageId
                                        AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            DELETE  FROM dbo.syNavigationNodes
                            WHERE   ResourceId = @MaintenanePageId
                                    AND ParentId = @Maintenance_HierarchyId;
                        END;
                END; 
			-- 	System
            IF @SY_ModuleId = 1 -- if SA is selected 
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 195 )
                                    );
                    SET @ResId = 707;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF NOT EXISTS ( SELECT TOP 1
                                            ResourceId
                                    FROM    syNavigationNodes
                                    WHERE   ResourceId = @MaintenanePageId
                                            AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            INSERT  INTO syNavigationNodes
                                    (
                                     HierarchyId
                                    ,HierarchyIndex
                                    ,ResourceId
                                    ,ParentId
                                    ,ModUser
                                    ,ModDate
                                    ,IsPopupWindow
									)
                            VALUES  (
                                     NEWID()
                                    ,1
                                    ,@MaintenanePageId
                                    ,@Maintenance_HierarchyId
                                    ,@UserName
                                    ,GETDATE()
                                    ,0
									);
                        END;
                END; 
            IF @SY_ModuleId = 0 -- if AR is not selected, remove page from AR
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 195 )
                                    );
                    SET @ResId = 707;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF EXISTS ( SELECT TOP 1
                                        ResourceId
                                FROM    syNavigationNodes
                                WHERE   ResourceId = @MaintenanePageId
                                        AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            DELETE  FROM dbo.syNavigationNodes
                            WHERE   ResourceId = @MaintenanePageId
                                    AND ParentId = @Maintenance_HierarchyId;
                        END;
                END; 
			-- 	Human Resources
            IF @HR_ModuleId = 1 -- if SA is selected 
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 192 )
                                    );
                    SET @ResId = 709;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF NOT EXISTS ( SELECT TOP 1
                                            ResourceId
                                    FROM    syNavigationNodes
                                    WHERE   ResourceId = @MaintenanePageId
                                            AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            INSERT  INTO syNavigationNodes
                                    (
                                     HierarchyId
                                    ,HierarchyIndex
                                    ,ResourceId
                                    ,ParentId
                                    ,ModUser
                                    ,ModDate
                                    ,IsPopupWindow
									)
                            VALUES  (
                                     NEWID()
                                    ,1
                                    ,@MaintenanePageId
                                    ,@Maintenance_HierarchyId
                                    ,@UserName
                                    ,GETDATE()
                                    ,0
									);
                        END;
                END; 
            IF @HR_ModuleId = 0 -- if HR is not selected, remove page from HR
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 192 )
                                    );
                    SET @ResId = 709;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF EXISTS ( SELECT TOP 1
                                        ResourceId
                                FROM    syNavigationNodes
                                WHERE   ResourceId = @MaintenanePageId
                                        AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            DELETE  FROM dbo.syNavigationNodes
                            WHERE   ResourceId = @MaintenanePageId
                                    AND ParentId = @Maintenance_HierarchyId;
                        END;
                END; 
        END; 
		
		/************* If Item Belongs to SA ***************************************/
    IF @MaintenanePageId IN ( 57,72,75,81,154,162,164,528,571,12,161,486,85 )
        BEGIN 
			-- Module (AD)
			-- If Maintenance Page is part of Admissions Module, don't let user add again or delete from Admissions Module
            IF ( @AD_ModuleId ) = 1
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 189 )
                                    );
                    SET @ResId = 702;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF NOT EXISTS ( SELECT TOP 1
                                            ResourceId
                                    FROM    syNavigationNodes
                                    WHERE   ResourceId = @MaintenanePageId
                                            AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            INSERT  INTO syNavigationNodes
                                    (
                                     HierarchyId
                                    ,HierarchyIndex
                                    ,ResourceId
                                    ,ParentId
                                    ,ModUser
                                    ,ModDate
                                    ,IsPopupWindow
									)
                            VALUES  (
                                     NEWID()
                                    ,1
                                    ,@MaintenanePageId
                                    ,@Maintenance_HierarchyId
                                    ,@UserName
                                    ,GETDATE()
                                    ,0
									);
                        END;
                END; 
			-- if AD is not selected, remove page from AD
			-- If Maintenance Page is part of Admissions Module, don't let user add again or delete from Admissions Module
            IF @AD_ModuleId = 0
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 189 )
                                    );
                    SET @ResId = 702;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF EXISTS ( SELECT TOP 1
                                        ResourceId
                                FROM    syNavigationNodes
                                WHERE   ResourceId = @MaintenanePageId
                                        AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            DELETE  FROM dbo.syNavigationNodes
                            WHERE   ResourceId = @MaintenanePageId
                                    AND ParentId = @Maintenance_HierarchyId;
                        END;
                END; 
			-- Module = AR
            IF @AR_ModuleId = 1 -- if AR is selected 
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 26 )
                                    );
                    SET @ResId = 703;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF NOT EXISTS ( SELECT TOP 1
                                            ResourceId
                                    FROM    syNavigationNodes
                                    WHERE   ResourceId = @MaintenanePageId
                                            AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            INSERT  INTO syNavigationNodes
                                    (
                                     HierarchyId
                                    ,HierarchyIndex
                                    ,ResourceId
                                    ,ParentId
                                    ,ModUser
                                    ,ModDate
                                    ,IsPopupWindow
									)
                            VALUES  (
                                     NEWID()
                                    ,1
                                    ,@MaintenanePageId
                                    ,@Maintenance_HierarchyId
                                    ,@UserName
                                    ,GETDATE()
                                    ,0
									);
                        END;
                END; 
            IF @AR_ModuleId = 0 -- if AR is not selected, remove page from AR
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 26 )
                                    );
                    SET @ResId = 703;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF EXISTS ( SELECT TOP 1
                                        ResourceId
                                FROM    syNavigationNodes
                                WHERE   ResourceId = @MaintenanePageId
                                        AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            DELETE  FROM dbo.syNavigationNodes
                            WHERE   ResourceId = @MaintenanePageId
                                    AND ParentId = @Maintenance_HierarchyId;
                        END;
                END; 
			-- Module = SA
            IF @SA_ModuleId = 1
                AND @MaintenanePageId NOT IN ( 57,72,75,81,154,162,164,528,571,12,161,486,85 ) -- if SA is selected 
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 194 )
                                    );
                    SET @ResId = 705;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF NOT EXISTS ( SELECT TOP 1
                                            ResourceId
                                    FROM    syNavigationNodes
                                    WHERE   ResourceId = @MaintenanePageId
                                            AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            INSERT  INTO syNavigationNodes
                                    (
                                     HierarchyId
                                    ,HierarchyIndex
                                    ,ResourceId
                                    ,ParentId
                                    ,ModUser
                                    ,ModDate
                                    ,IsPopupWindow
									)
                            VALUES  (
                                     NEWID()
                                    ,1
                                    ,@MaintenanePageId
                                    ,@Maintenance_HierarchyId
                                    ,@UserName
                                    ,GETDATE()
                                    ,0
									);
                        END;
                END; 
            IF @SA_ModuleId = 0
                AND @MaintenanePageId NOT IN ( 57,72,75,81,154,162,164,528,571,12,161,486,85 )  -- if AR is not selected, remove page from AR
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 194 )
                                    );
                    SET @ResId = 705;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF EXISTS ( SELECT TOP 1
                                        ResourceId
                                FROM    syNavigationNodes
                                WHERE   ResourceId = @MaintenanePageId
                                        AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            DELETE  FROM dbo.syNavigationNodes
                            WHERE   ResourceId = @MaintenanePageId
                                    AND ParentId = @Maintenance_HierarchyId;
                        END;
                END; 
			-- Placement
            IF @PL_ModuleId = 1 -- if SA is selected 
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 193 )
                                    );
                    SET @ResId = 704;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF NOT EXISTS ( SELECT TOP 1
                                            ResourceId
                                    FROM    syNavigationNodes
                                    WHERE   ResourceId = @MaintenanePageId
                                            AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            INSERT  INTO syNavigationNodes
                                    (
                                     HierarchyId
                                    ,HierarchyIndex
                                    ,ResourceId
                                    ,ParentId
                                    ,ModUser
                                    ,ModDate
                                    ,IsPopupWindow
									)
                            VALUES  (
                                     NEWID()
                                    ,1
                                    ,@MaintenanePageId
                                    ,@Maintenance_HierarchyId
                                    ,@UserName
                                    ,GETDATE()
                                    ,0
									);
                        END;
                END; 
            IF @PL_ModuleId = 0 -- if AR is not selected, remove page from AR
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 193 )
                                    );
                    SET @ResId = 704;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF EXISTS ( SELECT TOP 1
                                        ResourceId
                                FROM    syNavigationNodes
                                WHERE   ResourceId = @MaintenanePageId
                                        AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            DELETE  FROM dbo.syNavigationNodes
                            WHERE   ResourceId = @MaintenanePageId
                                    AND ParentId = @Maintenance_HierarchyId;
                        END;
                END; 
			-- Financial Aid
            IF @FA_ModuleId = 1 -- if SA is selected 
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 191 )
                                    );
                    SET @ResId = 706;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF NOT EXISTS ( SELECT TOP 1
                                            ResourceId
                                    FROM    syNavigationNodes
                                    WHERE   ResourceId = @MaintenanePageId
                                            AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            INSERT  INTO syNavigationNodes
                                    (
                                     HierarchyId
                                    ,HierarchyIndex
                                    ,ResourceId
                                    ,ParentId
                                    ,ModUser
                                    ,ModDate
                                    ,IsPopupWindow
									)
                            VALUES  (
                                     NEWID()
                                    ,1
                                    ,@MaintenanePageId
                                    ,@Maintenance_HierarchyId
                                    ,@UserName
                                    ,GETDATE()
                                    ,0
									);
                        END;
                END; 
            IF @FA_ModuleId = 0 -- if AR is not selected, remove page from AR
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 191 )
                                    );
                    SET @ResId = 706;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF EXISTS ( SELECT TOP 1
                                        ResourceId
                                FROM    syNavigationNodes
                                WHERE   ResourceId = @MaintenanePageId
                                        AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            DELETE  FROM dbo.syNavigationNodes
                            WHERE   ResourceId = @MaintenanePageId
                                    AND ParentId = @Maintenance_HierarchyId;
                        END;
                END; 
			-- 	System
            IF @SY_ModuleId = 1 -- if SA is selected 
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 195 )
                                    );
                    SET @ResId = 707;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF NOT EXISTS ( SELECT TOP 1
                                            ResourceId
                                    FROM    syNavigationNodes
                                    WHERE   ResourceId = @MaintenanePageId
                                            AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            INSERT  INTO syNavigationNodes
                                    (
                                     HierarchyId
                                    ,HierarchyIndex
                                    ,ResourceId
                                    ,ParentId
                                    ,ModUser
                                    ,ModDate
                                    ,IsPopupWindow
									)
                            VALUES  (
                                     NEWID()
                                    ,1
                                    ,@MaintenanePageId
                                    ,@Maintenance_HierarchyId
                                    ,@UserName
                                    ,GETDATE()
                                    ,0
									);
                        END;
                END; 
            IF @SY_ModuleId = 0 -- if AR is not selected, remove page from AR
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 195 )
                                    );
                    SET @ResId = 707;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF EXISTS ( SELECT TOP 1
                                        ResourceId
                                FROM    syNavigationNodes
                                WHERE   ResourceId = @MaintenanePageId
                                        AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            DELETE  FROM dbo.syNavigationNodes
                            WHERE   ResourceId = @MaintenanePageId
                                    AND ParentId = @Maintenance_HierarchyId;
                        END;
                END; 
			-- 	Human Resources
            IF @HR_ModuleId = 1 -- if SA is selected 
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 192 )
                                    );
                    SET @ResId = 709;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF NOT EXISTS ( SELECT TOP 1
                                            ResourceId
                                    FROM    syNavigationNodes
                                    WHERE   ResourceId = @MaintenanePageId
                                            AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            INSERT  INTO syNavigationNodes
                                    (
                                     HierarchyId
                                    ,HierarchyIndex
                                    ,ResourceId
                                    ,ParentId
                                    ,ModUser
                                    ,ModDate
                                    ,IsPopupWindow
									)
                            VALUES  (
                                     NEWID()
                                    ,1
                                    ,@MaintenanePageId
                                    ,@Maintenance_HierarchyId
                                    ,@UserName
                                    ,GETDATE()
                                    ,0
									);
                        END;
                END; 
            IF @HR_ModuleId = 0 -- if HR is not selected, remove page from HR
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 192 )
                                    );
                    SET @ResId = 709;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF EXISTS ( SELECT TOP 1
                                        ResourceId
                                FROM    syNavigationNodes
                                WHERE   ResourceId = @MaintenanePageId
                                        AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            DELETE  FROM dbo.syNavigationNodes
                            WHERE   ResourceId = @MaintenanePageId
                                    AND ParentId = @Maintenance_HierarchyId;
                        END;
                END; 
        END; 
		/************* If Item Belongs to Placement ***********************/
    IF @MaintenanePageId IN ( 88,99,282,304,24,27,25,93,93,37,302,15 )
        BEGIN 
			-- Module (AD)
			-- If Maintenance Page is part of Admissions Module, don't let user add again or delete from Admissions Module
            IF ( @AD_ModuleId ) = 1
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 189 )
                                    );
                    SET @ResId = 702;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF NOT EXISTS ( SELECT TOP 1
                                            ResourceId
                                    FROM    syNavigationNodes
                                    WHERE   ResourceId = @MaintenanePageId
                                            AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            INSERT  INTO syNavigationNodes
                                    (
                                     HierarchyId
                                    ,HierarchyIndex
                                    ,ResourceId
                                    ,ParentId
                                    ,ModUser
                                    ,ModDate
                                    ,IsPopupWindow
									)
                            VALUES  (
                                     NEWID()
                                    ,1
                                    ,@MaintenanePageId
                                    ,@Maintenance_HierarchyId
                                    ,@UserName
                                    ,GETDATE()
                                    ,0
									);
                        END;
                END; 
			-- if AD is not selected, remove page from AD
			-- If Maintenance Page is part of Admissions Module, don't let user add again or delete from Admissions Module
            IF @AD_ModuleId = 0
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 189 )
                                    );
                    SET @ResId = 702;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF EXISTS ( SELECT TOP 1
                                        ResourceId
                                FROM    syNavigationNodes
                                WHERE   ResourceId = @MaintenanePageId
                                        AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            DELETE  FROM dbo.syNavigationNodes
                            WHERE   ResourceId = @MaintenanePageId
                                    AND ParentId = @Maintenance_HierarchyId;
                        END;
                END; 
			-- Module = AR
            IF @AR_ModuleId = 1 -- if AR is selected 
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 26 )
                                    );
                    SET @ResId = 703;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF NOT EXISTS ( SELECT TOP 1
                                            ResourceId
                                    FROM    syNavigationNodes
                                    WHERE   ResourceId = @MaintenanePageId
                                            AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            INSERT  INTO syNavigationNodes
                                    (
                                     HierarchyId
                                    ,HierarchyIndex
                                    ,ResourceId
                                    ,ParentId
                                    ,ModUser
                                    ,ModDate
                                    ,IsPopupWindow
									)
                            VALUES  (
                                     NEWID()
                                    ,1
                                    ,@MaintenanePageId
                                    ,@Maintenance_HierarchyId
                                    ,@UserName
                                    ,GETDATE()
                                    ,0
									);
                        END;
                END; 
            IF @AR_ModuleId = 0 -- if AR is not selected, remove page from AR
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 26 )
                                    );
                    SET @ResId = 703;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF EXISTS ( SELECT TOP 1
                                        ResourceId
                                FROM    syNavigationNodes
                                WHERE   ResourceId = @MaintenanePageId
                                        AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            DELETE  FROM dbo.syNavigationNodes
                            WHERE   ResourceId = @MaintenanePageId
                                    AND ParentId = @Maintenance_HierarchyId;
                        END;
                END; 
			-- Module = SA
            IF @SA_ModuleId = 1  -- if SA is selected 
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 194 )
                                    );
                    SET @ResId = 705;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF NOT EXISTS ( SELECT TOP 1
                                            ResourceId
                                    FROM    syNavigationNodes
                                    WHERE   ResourceId = @MaintenanePageId
                                            AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            INSERT  INTO syNavigationNodes
                                    (
                                     HierarchyId
                                    ,HierarchyIndex
                                    ,ResourceId
                                    ,ParentId
                                    ,ModUser
                                    ,ModDate
                                    ,IsPopupWindow
									)
                            VALUES  (
                                     NEWID()
                                    ,1
                                    ,@MaintenanePageId
                                    ,@Maintenance_HierarchyId
                                    ,@UserName
                                    ,GETDATE()
                                    ,0
									);
                        END;
                END; 
            IF @SA_ModuleId = 0   -- if AR is not selected, remove page from AR
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 194 )
                                    );
                    SET @ResId = 705;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF EXISTS ( SELECT TOP 1
                                        ResourceId
                                FROM    syNavigationNodes
                                WHERE   ResourceId = @MaintenanePageId
                                        AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            DELETE  FROM dbo.syNavigationNodes
                            WHERE   ResourceId = @MaintenanePageId
                                    AND ParentId = @Maintenance_HierarchyId;
                        END;
                END; 
			-- Placement
            IF @PL_ModuleId = 1
                AND @MaintenanePageId NOT IN ( 88,99,282,304,24,27,25,93,93,37,302,15 ) -- if SA is selected 
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 193 )
                                    );
                    SET @ResId = 704;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF NOT EXISTS ( SELECT TOP 1
                                            ResourceId
                                    FROM    syNavigationNodes
                                    WHERE   ResourceId = @MaintenanePageId
                                            AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            INSERT  INTO syNavigationNodes
                                    (
                                     HierarchyId
                                    ,HierarchyIndex
                                    ,ResourceId
                                    ,ParentId
                                    ,ModUser
                                    ,ModDate
                                    ,IsPopupWindow
									)
                            VALUES  (
                                     NEWID()
                                    ,1
                                    ,@MaintenanePageId
                                    ,@Maintenance_HierarchyId
                                    ,@UserName
                                    ,GETDATE()
                                    ,0
									);
                        END;
                END; 
            IF @PL_ModuleId = 0
                AND @MaintenanePageId NOT IN ( 88,99,282,304,24,27,25,93,93,37,302,15 ) -- if AR is not selected, remove page from AR
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 193 )
                                    );
                    SET @ResId = 704;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF EXISTS ( SELECT TOP 1
                                        ResourceId
                                FROM    syNavigationNodes
                                WHERE   ResourceId = @MaintenanePageId
                                        AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            DELETE  FROM dbo.syNavigationNodes
                            WHERE   ResourceId = @MaintenanePageId
                                    AND ParentId = @Maintenance_HierarchyId;
                        END;
                END; 
			-- Financial Aid
            IF @FA_ModuleId = 1 -- if SA is selected 
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 191 )
                                    );
                    SET @ResId = 706;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF NOT EXISTS ( SELECT TOP 1
                                            ResourceId
                                    FROM    syNavigationNodes
                                    WHERE   ResourceId = @MaintenanePageId
                                            AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            INSERT  INTO syNavigationNodes
                                    (
                                     HierarchyId
                                    ,HierarchyIndex
                                    ,ResourceId
                                    ,ParentId
                                    ,ModUser
                                    ,ModDate
                                    ,IsPopupWindow
									)
                            VALUES  (
                                     NEWID()
                                    ,1
                                    ,@MaintenanePageId
                                    ,@Maintenance_HierarchyId
                                    ,@UserName
                                    ,GETDATE()
                                    ,0
									);
                        END;
                END; 
            IF @FA_ModuleId = 0 -- if AR is not selected, remove page from AR
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 191 )
                                    );
                    SET @ResId = 706;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF EXISTS ( SELECT TOP 1
                                        ResourceId
                                FROM    syNavigationNodes
                                WHERE   ResourceId = @MaintenanePageId
                                        AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            DELETE  FROM dbo.syNavigationNodes
                            WHERE   ResourceId = @MaintenanePageId
                                    AND ParentId = @Maintenance_HierarchyId;
                        END;
                END; 
			-- 	System
            IF @SY_ModuleId = 1 -- if SA is selected 
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 195 )
                                    );
                    SET @ResId = 707;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF NOT EXISTS ( SELECT TOP 1
                                            ResourceId
                                    FROM    syNavigationNodes
                                    WHERE   ResourceId = @MaintenanePageId
                                            AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            INSERT  INTO syNavigationNodes
                                    (
                                     HierarchyId
                                    ,HierarchyIndex
                                    ,ResourceId
                                    ,ParentId
                                    ,ModUser
                                    ,ModDate
                                    ,IsPopupWindow
									)
                            VALUES  (
                                     NEWID()
                                    ,1
                                    ,@MaintenanePageId
                                    ,@Maintenance_HierarchyId
                                    ,@UserName
                                    ,GETDATE()
                                    ,0
									);
                        END;
                END; 
            IF @SY_ModuleId = 0 -- if AR is not selected, remove page from AR
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 195 )
                                    );
                    SET @ResId = 707;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF EXISTS ( SELECT TOP 1
                                        ResourceId
                                FROM    syNavigationNodes
                                WHERE   ResourceId = @MaintenanePageId
                                        AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            DELETE  FROM dbo.syNavigationNodes
                            WHERE   ResourceId = @MaintenanePageId
                                    AND ParentId = @Maintenance_HierarchyId;
                        END;
                END; 
			-- 	Human Resources
            IF @HR_ModuleId = 1 -- if SA is selected 
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 192 )
                                    );
                    SET @ResId = 709;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF NOT EXISTS ( SELECT TOP 1
                                            ResourceId
                                    FROM    syNavigationNodes
                                    WHERE   ResourceId = @MaintenanePageId
                                            AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            INSERT  INTO syNavigationNodes
                                    (
                                     HierarchyId
                                    ,HierarchyIndex
                                    ,ResourceId
                                    ,ParentId
                                    ,ModUser
                                    ,ModDate
                                    ,IsPopupWindow
									)
                            VALUES  (
                                     NEWID()
                                    ,1
                                    ,@MaintenanePageId
                                    ,@Maintenance_HierarchyId
                                    ,@UserName
                                    ,GETDATE()
                                    ,0
									);
                        END;
                END; 
            IF @HR_ModuleId = 0 -- if HR is not selected, remove page from HR
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 192 )
                                    );
                    SET @ResId = 709;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF EXISTS ( SELECT TOP 1
                                        ResourceId
                                FROM    syNavigationNodes
                                WHERE   ResourceId = @MaintenanePageId
                                        AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            DELETE  FROM dbo.syNavigationNodes
                            WHERE   ResourceId = @MaintenanePageId
                                    AND ParentId = @Maintenance_HierarchyId;
                        END;
                END; 
        END; 
		
		/****************** IF Financial Aid Page is selected */
    IF @MaintenanePageId IN ( 139 )
        BEGIN 
			-- Module (AD)
			-- If Maintenance Page is part of Admissions Module, don't let user add again or delete from Admissions Module
            IF ( @AD_ModuleId ) = 1
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 189 )
                                    );
                    SET @ResId = 702;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF NOT EXISTS ( SELECT TOP 1
                                            ResourceId
                                    FROM    syNavigationNodes
                                    WHERE   ResourceId = @MaintenanePageId
                                            AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            INSERT  INTO syNavigationNodes
                                    (
                                     HierarchyId
                                    ,HierarchyIndex
                                    ,ResourceId
                                    ,ParentId
                                    ,ModUser
                                    ,ModDate
                                    ,IsPopupWindow
									)
                            VALUES  (
                                     NEWID()
                                    ,1
                                    ,@MaintenanePageId
                                    ,@Maintenance_HierarchyId
                                    ,@UserName
                                    ,GETDATE()
                                    ,0
									);
                        END;
                END; 
			-- if AD is not selected, remove page from AD
			-- If Maintenance Page is part of Admissions Module, don't let user add again or delete from Admissions Module
            IF @AD_ModuleId = 0
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 189 )
                                    );
                    SET @ResId = 702;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF EXISTS ( SELECT TOP 1
                                        ResourceId
                                FROM    syNavigationNodes
                                WHERE   ResourceId = @MaintenanePageId
                                        AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            DELETE  FROM dbo.syNavigationNodes
                            WHERE   ResourceId = @MaintenanePageId
                                    AND ParentId = @Maintenance_HierarchyId;
                        END;
                END; 
			-- Module = AR
            IF @AR_ModuleId = 1 -- if AR is selected 
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 26 )
                                    );
                    SET @ResId = 703;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF NOT EXISTS ( SELECT TOP 1
                                            ResourceId
                                    FROM    syNavigationNodes
                                    WHERE   ResourceId = @MaintenanePageId
                                            AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            INSERT  INTO syNavigationNodes
                                    (
                                     HierarchyId
                                    ,HierarchyIndex
                                    ,ResourceId
                                    ,ParentId
                                    ,ModUser
                                    ,ModDate
                                    ,IsPopupWindow
									)
                            VALUES  (
                                     NEWID()
                                    ,1
                                    ,@MaintenanePageId
                                    ,@Maintenance_HierarchyId
                                    ,@UserName
                                    ,GETDATE()
                                    ,0
									);
                        END;
                END; 
            IF @AR_ModuleId = 0 -- if AR is not selected, remove page from AR
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 26 )
                                    );
                    SET @ResId = 703;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF EXISTS ( SELECT TOP 1
                                        ResourceId
                                FROM    syNavigationNodes
                                WHERE   ResourceId = @MaintenanePageId
                                        AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            DELETE  FROM dbo.syNavigationNodes
                            WHERE   ResourceId = @MaintenanePageId
                                    AND ParentId = @Maintenance_HierarchyId;
                        END;
                END; 
			-- Module = SA
            IF @SA_ModuleId = 1  -- if SA is selected 
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 194 )
                                    );
                    SET @ResId = 705;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF NOT EXISTS ( SELECT TOP 1
                                            ResourceId
                                    FROM    syNavigationNodes
                                    WHERE   ResourceId = @MaintenanePageId
                                            AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            INSERT  INTO syNavigationNodes
                                    (
                                     HierarchyId
                                    ,HierarchyIndex
                                    ,ResourceId
                                    ,ParentId
                                    ,ModUser
                                    ,ModDate
                                    ,IsPopupWindow
									)
                            VALUES  (
                                     NEWID()
                                    ,1
                                    ,@MaintenanePageId
                                    ,@Maintenance_HierarchyId
                                    ,@UserName
                                    ,GETDATE()
                                    ,0
									);
                        END;
                END; 
            IF @SA_ModuleId = 0   -- if AR is not selected, remove page from AR
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 194 )
                                    );
                    SET @ResId = 705;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF EXISTS ( SELECT TOP 1
                                        ResourceId
                                FROM    syNavigationNodes
                                WHERE   ResourceId = @MaintenanePageId
                                        AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            DELETE  FROM dbo.syNavigationNodes
                            WHERE   ResourceId = @MaintenanePageId
                                    AND ParentId = @Maintenance_HierarchyId;
                        END;
                END; 
			-- Placement
            IF @PL_ModuleId = 1  -- if SA is selected 
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 193 )
                                    );
                    SET @ResId = 704;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF NOT EXISTS ( SELECT TOP 1
                                            ResourceId
                                    FROM    syNavigationNodes
                                    WHERE   ResourceId = @MaintenanePageId
                                            AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            INSERT  INTO syNavigationNodes
                                    (
                                     HierarchyId
                                    ,HierarchyIndex
                                    ,ResourceId
                                    ,ParentId
                                    ,ModUser
                                    ,ModDate
                                    ,IsPopupWindow
									)
                            VALUES  (
                                     NEWID()
                                    ,1
                                    ,@MaintenanePageId
                                    ,@Maintenance_HierarchyId
                                    ,@UserName
                                    ,GETDATE()
                                    ,0
									);
                        END;
                END; 
            IF @PL_ModuleId = 0  -- if AR is not selected, remove page from AR
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 193 )
                                    );
                    SET @ResId = 704;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF EXISTS ( SELECT TOP 1
                                        ResourceId
                                FROM    syNavigationNodes
                                WHERE   ResourceId = @MaintenanePageId
                                        AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            DELETE  FROM dbo.syNavigationNodes
                            WHERE   ResourceId = @MaintenanePageId
                                    AND ParentId = @Maintenance_HierarchyId;
                        END;
                END; 
			-- Financial Aid
            IF @FA_ModuleId = 1
                AND @MaintenanePageId NOT IN ( 139 ) -- if FA is selected 
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 191 )
                                    );
                    SET @ResId = 706;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF NOT EXISTS ( SELECT TOP 1
                                            ResourceId
                                    FROM    syNavigationNodes
                                    WHERE   ResourceId = @MaintenanePageId
                                            AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            INSERT  INTO syNavigationNodes
                                    (
                                     HierarchyId
                                    ,HierarchyIndex
                                    ,ResourceId
                                    ,ParentId
                                    ,ModUser
                                    ,ModDate
                                    ,IsPopupWindow
									)
                            VALUES  (
                                     NEWID()
                                    ,1
                                    ,@MaintenanePageId
                                    ,@Maintenance_HierarchyId
                                    ,@UserName
                                    ,GETDATE()
                                    ,0
									);
                        END;
                END; 
            IF @FA_ModuleId = 0
                AND @MaintenanePageId NOT IN ( 139 ) -- if FA is not selected, remove page from FA
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 191 )
                                    );
                    SET @ResId = 706;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF EXISTS ( SELECT TOP 1
                                        ResourceId
                                FROM    syNavigationNodes
                                WHERE   ResourceId = @MaintenanePageId
                                        AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            DELETE  FROM dbo.syNavigationNodes
                            WHERE   ResourceId = @MaintenanePageId
                                    AND ParentId = @Maintenance_HierarchyId;
                        END;
                END; 
			-- 	System
            IF @SY_ModuleId = 1 -- if SA is selected 
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 195 )
                                    );
                    SET @ResId = 707;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF NOT EXISTS ( SELECT TOP 1
                                            ResourceId
                                    FROM    syNavigationNodes
                                    WHERE   ResourceId = @MaintenanePageId
                                            AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            INSERT  INTO syNavigationNodes
                                    (
                                     HierarchyId
                                    ,HierarchyIndex
                                    ,ResourceId
                                    ,ParentId
                                    ,ModUser
                                    ,ModDate
                                    ,IsPopupWindow
									)
                            VALUES  (
                                     NEWID()
                                    ,1
                                    ,@MaintenanePageId
                                    ,@Maintenance_HierarchyId
                                    ,@UserName
                                    ,GETDATE()
                                    ,0
									);
                        END;
                END; 
            IF @SY_ModuleId = 0 -- if AR is not selected, remove page from AR
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 195 )
                                    );
                    SET @ResId = 707;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF EXISTS ( SELECT TOP 1
                                        ResourceId
                                FROM    syNavigationNodes
                                WHERE   ResourceId = @MaintenanePageId
                                        AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            DELETE  FROM dbo.syNavigationNodes
                            WHERE   ResourceId = @MaintenanePageId
                                    AND ParentId = @Maintenance_HierarchyId;
                        END;
                END; 
			-- 	Human Resources
            IF @HR_ModuleId = 1 -- if SA is selected 
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 192 )
                                    );
                    SET @ResId = 709;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF NOT EXISTS ( SELECT TOP 1
                                            ResourceId
                                    FROM    syNavigationNodes
                                    WHERE   ResourceId = @MaintenanePageId
                                            AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            INSERT  INTO syNavigationNodes
                                    (
                                     HierarchyId
                                    ,HierarchyIndex
                                    ,ResourceId
                                    ,ParentId
                                    ,ModUser
                                    ,ModDate
                                    ,IsPopupWindow
									)
                            VALUES  (
                                     NEWID()
                                    ,1
                                    ,@MaintenanePageId
                                    ,@Maintenance_HierarchyId
                                    ,@UserName
                                    ,GETDATE()
                                    ,0
									);
                        END;
                END; 
            IF @HR_ModuleId = 0 -- if HR is not selected, remove page from HR
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 192 )
                                    );
                    SET @ResId = 709;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF EXISTS ( SELECT TOP 1
                                        ResourceId
                                FROM    syNavigationNodes
                                WHERE   ResourceId = @MaintenanePageId
                                        AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            DELETE  FROM dbo.syNavigationNodes
                            WHERE   ResourceId = @MaintenanePageId
                                    AND ParentId = @Maintenance_HierarchyId;
                        END;
                END; 
        END; 
		/*********************** If Page is part of Systems Module *********************************/
    IF @MaintenanePageId IN ( 22,31,269,278,324,326,378,381,382,385,481,487,616,464,478,465,430,431,432,433,314,348,131 )
        BEGIN 
			-- Module (AD)
			-- If Maintenance Page is part of Admissions Module, don't let user add again or delete from Admissions Module
            IF ( @AD_ModuleId ) = 1
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 189 )
                                    );
                    SET @ResId = 702;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF NOT EXISTS ( SELECT TOP 1
                                            ResourceId
                                    FROM    syNavigationNodes
                                    WHERE   ResourceId = @MaintenanePageId
                                            AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            INSERT  INTO syNavigationNodes
                                    (
                                     HierarchyId
                                    ,HierarchyIndex
                                    ,ResourceId
                                    ,ParentId
                                    ,ModUser
                                    ,ModDate
                                    ,IsPopupWindow
									)
                            VALUES  (
                                     NEWID()
                                    ,1
                                    ,@MaintenanePageId
                                    ,@Maintenance_HierarchyId
                                    ,@UserName
                                    ,GETDATE()
                                    ,0
									);
                        END;
                END; 
			-- if AD is not selected, remove page from AD
			-- If Maintenance Page is part of Admissions Module, don't let user add again or delete from Admissions Module
            IF @AD_ModuleId = 0
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 189 )
                                    );
                    SET @ResId = 702;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF EXISTS ( SELECT TOP 1
                                        ResourceId
                                FROM    syNavigationNodes
                                WHERE   ResourceId = @MaintenanePageId
                                        AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            DELETE  FROM dbo.syNavigationNodes
                            WHERE   ResourceId = @MaintenanePageId
                                    AND ParentId = @Maintenance_HierarchyId;
                        END;
                END; 
			-- Module = AR
            IF @AR_ModuleId = 1 -- if AR is selected 
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 26 )
                                    );
                    SET @ResId = 703;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF NOT EXISTS ( SELECT TOP 1
                                            ResourceId
                                    FROM    syNavigationNodes
                                    WHERE   ResourceId = @MaintenanePageId
                                            AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            INSERT  INTO syNavigationNodes
                                    (
                                     HierarchyId
                                    ,HierarchyIndex
                                    ,ResourceId
                                    ,ParentId
                                    ,ModUser
                                    ,ModDate
                                    ,IsPopupWindow
									)
                            VALUES  (
                                     NEWID()
                                    ,1
                                    ,@MaintenanePageId
                                    ,@Maintenance_HierarchyId
                                    ,@UserName
                                    ,GETDATE()
                                    ,0
									);
                        END;
                END; 
            IF @AR_ModuleId = 0 -- if AR is not selected, remove page from AR
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 26 )
                                    );
                    SET @ResId = 703;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF EXISTS ( SELECT TOP 1
                                        ResourceId
                                FROM    syNavigationNodes
                                WHERE   ResourceId = @MaintenanePageId
                                        AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            DELETE  FROM dbo.syNavigationNodes
                            WHERE   ResourceId = @MaintenanePageId
                                    AND ParentId = @Maintenance_HierarchyId;
                        END;
                END; 
			-- Module = SA
            IF @SA_ModuleId = 1  -- if SA is selected 
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 194 )
                                    );
                    SET @ResId = 705;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF NOT EXISTS ( SELECT TOP 1
                                            ResourceId
                                    FROM    syNavigationNodes
                                    WHERE   ResourceId = @MaintenanePageId
                                            AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            INSERT  INTO syNavigationNodes
                                    (
                                     HierarchyId
                                    ,HierarchyIndex
                                    ,ResourceId
                                    ,ParentId
                                    ,ModUser
                                    ,ModDate
                                    ,IsPopupWindow
									)
                            VALUES  (
                                     NEWID()
                                    ,1
                                    ,@MaintenanePageId
                                    ,@Maintenance_HierarchyId
                                    ,@UserName
                                    ,GETDATE()
                                    ,0
									);
                        END;
                END; 
            IF @SA_ModuleId = 0   -- if AR is not selected, remove page from AR
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 194 )
                                    );
                    SET @ResId = 705;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF EXISTS ( SELECT TOP 1
                                        ResourceId
                                FROM    syNavigationNodes
                                WHERE   ResourceId = @MaintenanePageId
                                        AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            DELETE  FROM dbo.syNavigationNodes
                            WHERE   ResourceId = @MaintenanePageId
                                    AND ParentId = @Maintenance_HierarchyId;
                        END;
                END; 
			-- Placement
            IF @PL_ModuleId = 1  -- if SA is selected 
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 193 )
                                    );
                    SET @ResId = 704;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF NOT EXISTS ( SELECT TOP 1
                                            ResourceId
                                    FROM    syNavigationNodes
                                    WHERE   ResourceId = @MaintenanePageId
                                            AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            INSERT  INTO syNavigationNodes
                                    (
                                     HierarchyId
                                    ,HierarchyIndex
                                    ,ResourceId
                                    ,ParentId
                                    ,ModUser
                                    ,ModDate
                                    ,IsPopupWindow
									)
                            VALUES  (
                                     NEWID()
                                    ,1
                                    ,@MaintenanePageId
                                    ,@Maintenance_HierarchyId
                                    ,@UserName
                                    ,GETDATE()
                                    ,0
									);
                        END;
                END; 
            IF @PL_ModuleId = 0  -- if AR is not selected, remove page from AR
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 193 )
                                    );
                    SET @ResId = 704;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF EXISTS ( SELECT TOP 1
                                        ResourceId
                                FROM    syNavigationNodes
                                WHERE   ResourceId = @MaintenanePageId
                                        AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            DELETE  FROM dbo.syNavigationNodes
                            WHERE   ResourceId = @MaintenanePageId
                                    AND ParentId = @Maintenance_HierarchyId;
                        END;
                END; 
			-- Financial Aid
            IF @FA_ModuleId = 1  -- if FA is selected 
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 191 )
                                    );
                    SET @ResId = 706;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF NOT EXISTS ( SELECT TOP 1
                                            ResourceId
                                    FROM    syNavigationNodes
                                    WHERE   ResourceId = @MaintenanePageId
                                            AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            INSERT  INTO syNavigationNodes
                                    (
                                     HierarchyId
                                    ,HierarchyIndex
                                    ,ResourceId
                                    ,ParentId
                                    ,ModUser
                                    ,ModDate
                                    ,IsPopupWindow
									)
                            VALUES  (
                                     NEWID()
                                    ,1
                                    ,@MaintenanePageId
                                    ,@Maintenance_HierarchyId
                                    ,@UserName
                                    ,GETDATE()
                                    ,0
									);
                        END;
                END; 
            IF @FA_ModuleId = 0 -- if FA is not selected, remove page from FA
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 191 )
                                    );
                    SET @ResId = 706;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF EXISTS ( SELECT TOP 1
                                        ResourceId
                                FROM    syNavigationNodes
                                WHERE   ResourceId = @MaintenanePageId
                                        AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            DELETE  FROM dbo.syNavigationNodes
                            WHERE   ResourceId = @MaintenanePageId
                                    AND ParentId = @Maintenance_HierarchyId;
                        END;
                END; 
			-- 	System
            IF @SY_ModuleId = 1
                AND @MaintenanePageId NOT IN ( 22,31,269,278,324,326,378,381,382,385,481,487,616,464,478,465,430,431,432,433,314,348,131 )-- if SA is selected 
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 195 )
                                    );
                    SET @ResId = 707;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF NOT EXISTS ( SELECT TOP 1
                                            ResourceId
                                    FROM    syNavigationNodes
                                    WHERE   ResourceId = @MaintenanePageId
                                            AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            INSERT  INTO syNavigationNodes
                                    (
                                     HierarchyId
                                    ,HierarchyIndex
                                    ,ResourceId
                                    ,ParentId
                                    ,ModUser
                                    ,ModDate
                                    ,IsPopupWindow
									)
                            VALUES  (
                                     NEWID()
                                    ,1
                                    ,@MaintenanePageId
                                    ,@Maintenance_HierarchyId
                                    ,@UserName
                                    ,GETDATE()
                                    ,0
									);
                        END;
                END; 
            IF @SY_ModuleId = 0
                AND @MaintenanePageId NOT IN ( 22,31,269,278,324,326,378,381,382,385,481,487,616,464,478,465,430,431,432,433,314,348,131 ) -- if AR is not selected, remove page from AR
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 195 )
                                    );
                    SET @ResId = 707;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF EXISTS ( SELECT TOP 1
                                        ResourceId
                                FROM    syNavigationNodes
                                WHERE   ResourceId = @MaintenanePageId
                                        AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            DELETE  FROM dbo.syNavigationNodes
                            WHERE   ResourceId = @MaintenanePageId
                                    AND ParentId = @Maintenance_HierarchyId;
                        END;
                END; 
			-- 	Human Resources
            IF @HR_ModuleId = 1 -- if SA is selected 
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 192 )
                                    );
                    SET @ResId = 709;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF NOT EXISTS ( SELECT TOP 1
                                            ResourceId
                                    FROM    syNavigationNodes
                                    WHERE   ResourceId = @MaintenanePageId
                                            AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            INSERT  INTO syNavigationNodes
                                    (
                                     HierarchyId
                                    ,HierarchyIndex
                                    ,ResourceId
                                    ,ParentId
                                    ,ModUser
                                    ,ModDate
                                    ,IsPopupWindow
									)
                            VALUES  (
                                     NEWID()
                                    ,1
                                    ,@MaintenanePageId
                                    ,@Maintenance_HierarchyId
                                    ,@UserName
                                    ,GETDATE()
                                    ,0
									);
                        END;
                END; 
            IF @HR_ModuleId = 0 -- if HR is not selected, remove page from HR
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 192 )
                                    );
                    SET @ResId = 709;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF EXISTS ( SELECT TOP 1
                                        ResourceId
                                FROM    syNavigationNodes
                                WHERE   ResourceId = @MaintenanePageId
                                        AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            DELETE  FROM dbo.syNavigationNodes
                            WHERE   ResourceId = @MaintenanePageId
                                    AND ParentId = @Maintenance_HierarchyId;
                        END;
                END; 
        END; 
		/******************* If page is part of Human Resources ********************/
    IF @MaintenanePageId IN ( 298,299 )
        BEGIN 
			-- Module (AD)
			-- If Maintenance Page is part of Admissions Module, don't let user add again or delete from Admissions Module
            IF ( @AD_ModuleId ) = 1
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 189 )
                                    );
                    SET @ResId = 702;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF NOT EXISTS ( SELECT TOP 1
                                            ResourceId
                                    FROM    syNavigationNodes
                                    WHERE   ResourceId = @MaintenanePageId
                                            AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            INSERT  INTO syNavigationNodes
                                    (
                                     HierarchyId
                                    ,HierarchyIndex
                                    ,ResourceId
                                    ,ParentId
                                    ,ModUser
                                    ,ModDate
                                    ,IsPopupWindow
									)
                            VALUES  (
                                     NEWID()
                                    ,1
                                    ,@MaintenanePageId
                                    ,@Maintenance_HierarchyId
                                    ,@UserName
                                    ,GETDATE()
                                    ,0
									);
                        END;
                END; 
			-- if AD is not selected, remove page from AD
			-- If Maintenance Page is part of Admissions Module, don't let user add again or delete from Admissions Module
            IF @AD_ModuleId = 0
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 189 )
                                    );
                    SET @ResId = 702;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF EXISTS ( SELECT TOP 1
                                        ResourceId
                                FROM    syNavigationNodes
                                WHERE   ResourceId = @MaintenanePageId
                                        AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            DELETE  FROM dbo.syNavigationNodes
                            WHERE   ResourceId = @MaintenanePageId
                                    AND ParentId = @Maintenance_HierarchyId;
                        END;
                END; 
			-- Module = AR
            IF @AR_ModuleId = 1 -- if AR is selected 
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 26 )
                                    );
                    SET @ResId = 703;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF NOT EXISTS ( SELECT TOP 1
                                            ResourceId
                                    FROM    syNavigationNodes
                                    WHERE   ResourceId = @MaintenanePageId
                                            AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            INSERT  INTO syNavigationNodes
                                    (
                                     HierarchyId
                                    ,HierarchyIndex
                                    ,ResourceId
                                    ,ParentId
                                    ,ModUser
                                    ,ModDate
                                    ,IsPopupWindow
									)
                            VALUES  (
                                     NEWID()
                                    ,1
                                    ,@MaintenanePageId
                                    ,@Maintenance_HierarchyId
                                    ,@UserName
                                    ,GETDATE()
                                    ,0
									);
                        END;
                END; 
            IF @AR_ModuleId = 0 -- if AR is not selected, remove page from AR
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 26 )
                                    );
                    SET @ResId = 703;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF EXISTS ( SELECT TOP 1
                                        ResourceId
                                FROM    syNavigationNodes
                                WHERE   ResourceId = @MaintenanePageId
                                        AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            DELETE  FROM dbo.syNavigationNodes
                            WHERE   ResourceId = @MaintenanePageId
                                    AND ParentId = @Maintenance_HierarchyId;
                        END;
                END; 
			-- Module = SA
            IF @SA_ModuleId = 1  -- if SA is selected 
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 194 )
                                    );
                    SET @ResId = 705;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF NOT EXISTS ( SELECT TOP 1
                                            ResourceId
                                    FROM    syNavigationNodes
                                    WHERE   ResourceId = @MaintenanePageId
                                            AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            INSERT  INTO syNavigationNodes
                                    (
                                     HierarchyId
                                    ,HierarchyIndex
                                    ,ResourceId
                                    ,ParentId
                                    ,ModUser
                                    ,ModDate
                                    ,IsPopupWindow
									)
                            VALUES  (
                                     NEWID()
                                    ,1
                                    ,@MaintenanePageId
                                    ,@Maintenance_HierarchyId
                                    ,@UserName
                                    ,GETDATE()
                                    ,0
									);
                        END;
                END; 
            IF @SA_ModuleId = 0   -- if AR is not selected, remove page from AR
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 194 )
                                    );
                    SET @ResId = 705;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF EXISTS ( SELECT TOP 1
                                        ResourceId
                                FROM    syNavigationNodes
                                WHERE   ResourceId = @MaintenanePageId
                                        AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            DELETE  FROM dbo.syNavigationNodes
                            WHERE   ResourceId = @MaintenanePageId
                                    AND ParentId = @Maintenance_HierarchyId;
                        END;
                END; 
			-- Placement
            IF @PL_ModuleId = 1  -- if SA is selected 
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 193 )
                                    );
                    SET @ResId = 704;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF NOT EXISTS ( SELECT TOP 1
                                            ResourceId
                                    FROM    syNavigationNodes
                                    WHERE   ResourceId = @MaintenanePageId
                                            AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            INSERT  INTO syNavigationNodes
                                    (
                                     HierarchyId
                                    ,HierarchyIndex
                                    ,ResourceId
                                    ,ParentId
                                    ,ModUser
                                    ,ModDate
                                    ,IsPopupWindow
									)
                            VALUES  (
                                     NEWID()
                                    ,1
                                    ,@MaintenanePageId
                                    ,@Maintenance_HierarchyId
                                    ,@UserName
                                    ,GETDATE()
                                    ,0
									);
                        END;
                END; 
            IF @PL_ModuleId = 0  -- if AR is not selected, remove page from AR
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 193 )
                                    );
                    SET @ResId = 704;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF EXISTS ( SELECT TOP 1
                                        ResourceId
                                FROM    syNavigationNodes
                                WHERE   ResourceId = @MaintenanePageId
                                        AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            DELETE  FROM dbo.syNavigationNodes
                            WHERE   ResourceId = @MaintenanePageId
                                    AND ParentId = @Maintenance_HierarchyId;
                        END;
                END; 
			-- Financial Aid
            IF @FA_ModuleId = 1  -- if FA is selected 
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 191 )
                                    );
                    SET @ResId = 706;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF NOT EXISTS ( SELECT TOP 1
                                            ResourceId
                                    FROM    syNavigationNodes
                                    WHERE   ResourceId = @MaintenanePageId
                                            AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            INSERT  INTO syNavigationNodes
                                    (
                                     HierarchyId
                                    ,HierarchyIndex
                                    ,ResourceId
                                    ,ParentId
                                    ,ModUser
                                    ,ModDate
                                    ,IsPopupWindow
									)
                            VALUES  (
                                     NEWID()
                                    ,1
                                    ,@MaintenanePageId
                                    ,@Maintenance_HierarchyId
                                    ,@UserName
                                    ,GETDATE()
                                    ,0
									);
                        END;
                END; 
            IF @FA_ModuleId = 0 -- if FA is not selected, remove page from FA
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 191 )
                                    );
                    SET @ResId = 706;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF EXISTS ( SELECT TOP 1
                                        ResourceId
                                FROM    syNavigationNodes
                                WHERE   ResourceId = @MaintenanePageId
                                        AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            DELETE  FROM dbo.syNavigationNodes
                            WHERE   ResourceId = @MaintenanePageId
                                    AND ParentId = @Maintenance_HierarchyId;
                        END;
                END; 
			-- 	System
            IF @SY_ModuleId = 1 -- if SA is selected 
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 195 )
                                    );
                    SET @ResId = 707;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF NOT EXISTS ( SELECT TOP 1
                                            ResourceId
                                    FROM    syNavigationNodes
                                    WHERE   ResourceId = @MaintenanePageId
                                            AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            INSERT  INTO syNavigationNodes
                                    (
                                     HierarchyId
                                    ,HierarchyIndex
                                    ,ResourceId
                                    ,ParentId
                                    ,ModUser
                                    ,ModDate
                                    ,IsPopupWindow
									)
                            VALUES  (
                                     NEWID()
                                    ,1
                                    ,@MaintenanePageId
                                    ,@Maintenance_HierarchyId
                                    ,@UserName
                                    ,GETDATE()
                                    ,0
									);
                        END;
                END; 
            IF @SY_ModuleId = 0 -- if AR is not selected, remove page from AR
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 195 )
                                    );
                    SET @ResId = 707;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF EXISTS ( SELECT TOP 1
                                        ResourceId
                                FROM    syNavigationNodes
                                WHERE   ResourceId = @MaintenanePageId
                                        AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            DELETE  FROM dbo.syNavigationNodes
                            WHERE   ResourceId = @MaintenanePageId
                                    AND ParentId = @Maintenance_HierarchyId;
                        END;
                END; 
			-- 	Human Resources
            IF @HR_ModuleId = 1
                AND @MaintenanePageId NOT IN ( 298,299 ) -- if SA is selected 
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 192 )
                                    );
                    SET @ResId = 709;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF NOT EXISTS ( SELECT TOP 1
                                            ResourceId
                                    FROM    syNavigationNodes
                                    WHERE   ResourceId = @MaintenanePageId
                                            AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            INSERT  INTO syNavigationNodes
                                    (
                                     HierarchyId
                                    ,HierarchyIndex
                                    ,ResourceId
                                    ,ParentId
                                    ,ModUser
                                    ,ModDate
                                    ,IsPopupWindow
									)
                            VALUES  (
                                     NEWID()
                                    ,1
                                    ,@MaintenanePageId
                                    ,@Maintenance_HierarchyId
                                    ,@UserName
                                    ,GETDATE()
                                    ,0
									);
                        END;
                END; 
            IF @HR_ModuleId = 0
                AND @MaintenanePageId NOT IN ( 298,299 )-- if HR is not selected, remove page from HR
                BEGIN
                    SET @ParentId = (
                                      SELECT    HierarchyId
                                      FROM      syNavigationNodes
                                      WHERE     ResourceId = 688
                                                AND ParentId IN ( SELECT    HierarchyId
                                                                  FROM      syNavigationNodes
                                                                  WHERE     ResourceId = 192 )
                                    );
                    SET @ResId = 709;
                    SET @Maintenance_HierarchyId = (
                                                     SELECT TOP 1
                                                            HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = @ResId
                                                            AND ParentId = @ParentId
                                                   );
                    IF EXISTS ( SELECT TOP 1
                                        ResourceId
                                FROM    syNavigationNodes
                                WHERE   ResourceId = @MaintenanePageId
                                        AND ParentId = @Maintenance_HierarchyId )
                        BEGIN
                            DELETE  FROM dbo.syNavigationNodes
                            WHERE   ResourceId = @MaintenanePageId
                                    AND ParentId = @Maintenance_HierarchyId;
                        END;
                END; 
        END; 

GO
