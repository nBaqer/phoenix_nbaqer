SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[RS_TranscriptGetLogoAndOptions] 
	-- Add the parameters for the stored procedure here
    @ImageCode VARCHAR(30) = 'RSTRAN1'
   ,@IsOfficialTranscript BIT = 0
   ,@IsShowedSignatureLine BIT = 0
   ,@LegalDisclaimer VARCHAR(500) = NULL
   ,@SignatureLegalDisclaimer VARCHAR(50) = NULL
   ,@IsHeaderShowed BIT = 1
AS
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;

    -- Insert statements for procedure here
        SELECT  Image
               ,@IsOfficialTranscript AS IsOfficialTranscript
               ,@IsShowedSignatureLine AS IsShowedSignatureLine
               ,@LegalDisclaimer AS LegalDisclaimer
               ,@SignatureLegalDisclaimer AS SignatureLegalDisclaimer
               ,@IsHeaderShowed AS IsHeaderShowed
        FROM    sySchoolLogo
        WHERE   Imagecode = @ImageCode;
    END;




GO
