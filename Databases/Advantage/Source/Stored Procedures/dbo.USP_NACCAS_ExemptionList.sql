SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[USP_NACCAS_ExemptionList]
    @PrgIdList VARCHAR(MAX) = NULL
   ,@CampusId VARCHAR(50)
   ,@ReportYear INT
AS
    DECLARE @StartMonth INT = 1
           ,@StartDay INT = 1
           ,@EndMonth INT = 12
           ,@EndDay INT = 31
           ,@FutureStart INT = 7
           ,@CurrentlyAttending INT = 9
           ,@LOA INT = 10
           ,@Suspension INT = 11
           ,@Dropped INT = 12
           ,@Graduated INT = 14
           ,@Transfer INT = 19
           ,@DeceasedReason INT = 1
           ,@TempOrPermDisabledReason INT = 2
           ,@DeployedReason INT = 3
           ,@Transferred100Reason INT = 4
           ,@TransferredAccreditedReason INT = 5
           ,@EarlyWithdrawalReason INT = 6
           ,@MilitaryTransfer UNIQUEIDENTIFIER
           ,@CallToActiveDuty UNIQUEIDENTIFIER
           ,@Deceased UNIQUEIDENTIFIER
           ,@TemporarilyDisabled UNIQUEIDENTIFIER
           ,@PermanentlyDisabled UNIQUEIDENTIFIER
           ,@TransferredToEquivalent UNIQUEIDENTIFIER;
    BEGIN


        SET @MilitaryTransfer = (
                                SELECT TOP 1 NACCASDropReasonId
                                FROM   dbo.syNACCASDropReasons
                                WHERE  Name = 'Military Transfer'
                                );
        SET @CallToActiveDuty = (
                                SELECT TOP 1 NACCASDropReasonId
                                FROM   dbo.syNACCASDropReasons
                                WHERE  Name = 'Call to Active Duty'
                                );
        SET @Deceased = (
                        SELECT TOP 1 NACCASDropReasonId
                        FROM   dbo.syNACCASDropReasons
                        WHERE  Name = 'Deceased'
                        );
        SET @TemporarilyDisabled = (
                                   SELECT TOP 1 NACCASDropReasonId
                                   FROM   dbo.syNACCASDropReasons
                                   WHERE  Name = 'Temporarily disabled'
                                   );
        SET @PermanentlyDisabled = (
                                   SELECT TOP 1 NACCASDropReasonId
                                   FROM   dbo.syNACCASDropReasons
                                   WHERE  Name = 'Permanently disabled'
                                   );
        SET @TransferredToEquivalent = (
                                       SELECT TOP 1 NACCASDropReasonId
                                       FROM   dbo.syNACCASDropReasons
                                       WHERE  Name = 'Transferred to an equivalent program at another school with same accreditation'
                                       );

        DECLARE @Enrollments TABLE
            (
                PrgID UNIQUEIDENTIFIER
               ,PrgDescrip VARCHAR(50)
               ,ProgramHours DECIMAL(9, 2)
               ,ProgramCredits DECIMAL(18, 2)
               ,TotalTransferHours DECIMAL(18, 2)
               ,TransferredProgram UNIQUEIDENTIFIER
               ,TransferHoursFromProgram DECIMAL(18, 2)
               ,PrgVerId UNIQUEIDENTIFIER
               ,SSN VARCHAR(50)
               ,PhoneNumber VARCHAR(50)
               ,Email VARCHAR(100)
               ,Student VARCHAR(50)
               ,LeadId UNIQUEIDENTIFIER
               ,StuEnrollId UNIQUEIDENTIFIER
               ,EnrollmentID NVARCHAR(50)
               ,Status VARCHAR(80)
               ,SysStatusId INT
               ,DropReasonId UNIQUEIDENTIFIER
               ,StartDate DATETIME
               ,ContractedGradDate DATETIME
               ,ExpectedGradDate DATETIME
               ,ReEnrollmentDate DATETIME
               ,LDA DATETIME
               ,LicensureWrittenAllParts BIT
               ,LicensureLastPartWrittenOn DATETIME
               ,LicensurePassedAllParts BIT
               ,AllowsMoneyOwed BIT
               ,AllowsIncompleteReq BIT
               ,ThirdPartyContract BIT
               ,RN INT
               ,cnt INT
            );

        DECLARE @EnrollmentsWithLastStatus TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,SysStatusId INT
               ,DropReasonId UNIQUEIDENTIFIER
               ,StatusCodeDescription VARCHAR(80)
            );
        INSERT INTO @EnrollmentsWithLastStatus
                    SELECT lastStatusOfYear.StuEnrollId
                          ,lastStatusOfYear.SysStatusId
                          ,lastStatusOfYear.DropReasonId
                          ,lastStatusOfYear.StatusCodeDescrip
                    FROM   (
                           SELECT     StuEnrollId
                                     ,syStatusCodes.SysStatusId
                                     ,StatusCodeDescrip
                                     ,DateOfChange
                                     ,DropReasonId
                                     ,ROW_NUMBER() OVER ( PARTITION BY StuEnrollId
                                                          ORDER BY DateOfChange DESC
                                                        ) rn
                           FROM       dbo.syStudentStatusChanges
                           INNER JOIN dbo.syStatusCodes ON syStatusCodes.StatusCodeId = syStudentStatusChanges.NewStatusId
                           WHERE      DATEPART(YEAR, DateOfChange) <= ( @ReportYear - 1 )
                           ) lastStatusOfYear
                    WHERE  rn = 1;


        INSERT INTO @Enrollments
                    SELECT *
                          ,ROW_NUMBER() OVER ( PARTITION BY enr.LeadId
                                                           ,enr.PrgID
                                                           ,enr.SysStatusId
                                               ORDER BY enr.StartDate DESC
                                             ) RN
                          ,COUNT(1) OVER ( PARTITION BY enr.LeadId
                                                       ,enr.PrgVerId
                                         ) AS ReenrollmentCount
                    FROM   (
                           SELECT     Prog.ProgId AS PrgID
                                     ,Prog.ProgDescrip AS PrgDescrip
                                     ,PV.Hours AS ProgramHours
                                     ,PV.Credits AS ProgramCredits
                                     ,SE.TransferHours
                                     ,SE.TransferHoursFromThisSchoolEnrollmentId
                                     ,SE.TotalTransferHoursFromThisSchool
                                     ,PV.PrgVerId AS PrgVerId
                                     ,LD.SSN
                                     ,ISNULL((
                                             SELECT   TOP 1 Phone
                                             FROM     dbo.adLeadPhone
                                             WHERE    LeadId = LD.LeadId
                                                      AND (
                                                          IsBest = 1
                                                          OR IsShowOnLeadPage = 1
                                                          )
                                             ORDER BY Position
                                             )
                                            ,''
                                            ) AS PhoneNumber
                                     ,ISNULL((
                                             SELECT   TOP 1 EMail
                                             FROM     dbo.AdLeadEmail
                                             WHERE    LeadId = LD.LeadId
                                                      AND (
                                                          IsPreferred = 1
                                                          OR IsShowOnLeadPage = 1
                                                          )
                                             ORDER BY IsPreferred DESC
                                                     ,IsShowOnLeadPage DESC
                                             )
                                            ,''
                                            ) AS Email
                                     ,LD.LastName + ', ' + LD.FirstName + ' ' + ISNULL(LD.MiddleName, '') AS Student
                                     ,LD.LeadId
                                     ,SE.StuEnrollId
                                     ,LD.StudentNumber AS EnrollmentID
                                     ,[@EnrollmentsWithLastStatus].StatusCodeDescription AS Status
                                     ,[@EnrollmentsWithLastStatus].SysStatusId AS SysStatusId
                                     ,[@EnrollmentsWithLastStatus].DropReasonId AS DropReasonId
                                     ,SE.StartDate AS StartDate
                                     ,SE.ContractedGradDate AS ContractedGradDate
                                     ,dbo.GetGraduatedDate(SE.StuEnrollId) AS ExpectedGradDate
                                     ,SE.ReEnrollmentDate AS ReEnrollmentDate
                                     ,dbo.GetLDA(SE.StuEnrollId) AS LDA
                                     ,SE.LicensureWrittenAllParts AS LicensureWrittenAllParts
                                     ,SE.LicensureLastPartWrittenOn AS LicensureLastPartWrittenOn
                                     ,SE.LicensurePassedAllParts AS LicensurePassedAllParts
                                     ,camp.AllowGraduateAndStillOweMoney AS AllowsMoneyOwed
                                     ,camp.AllowGraduateWithoutFullCompletion AS AllowsIncompleteReq
                                     ,SE.ThirdPartyContract AS ThirdPartyContract
                           FROM       dbo.arStuEnrollments AS SE
                           INNER JOIN dbo.adLeads AS LD ON LD.LeadId = SE.LeadId
                           INNER JOIN dbo.arPrgVersions AS PV ON SE.PrgVerId = PV.PrgVerId
                           INNER JOIN dbo.arPrograms AS Prog ON Prog.ProgId = PV.ProgId
                           INNER JOIN dbo.syApprovedNACCASProgramVersion NaccasApproved ON NaccasApproved.ProgramVersionId = SE.PrgVerId
                           INNER JOIN dbo.syNaccasSettings NaccasSettings ON NaccasSettings.NaccasSettingId = NaccasApproved.NaccasSettingId
                           INNER JOIN dbo.syCampuses AS camp ON camp.CampusId = SE.CampusId
                           INNER JOIN @EnrollmentsWithLastStatus ON [@EnrollmentsWithLastStatus].StuEnrollId = SE.StuEnrollId
                           WHERE      SE.CampusId = @CampusId
                                      AND NaccasSettings.CampusId = @CampusId
                                      --that have a revised graduation date between January 1 of reporting year minus 1 year and December 31st of reporting year minus 1 year

                                      AND NaccasApproved.IsApproved = 1
                                      AND SE.ContractedGradDate
                                      BETWEEN CONVERT(DATETIME, CONVERT(VARCHAR(50), (( @ReportYear - 1 ) * 10000 + @StartMonth * 100 + @StartDay )), 112) AND CONVERT(
                                                                                                                                                                          DATETIME
                                                                                                                                                                         ,CONVERT(
                                                                                                                                                                                     VARCHAR(50)
                                                                                                                                                                                    ,(( @ReportYear
                                                                                                                                                                                        - 1
                                                                                                                                                                                      )
                                                                                                                                                                                      * 10000
                                                                                                                                                                                      + @EndMonth
                                                                                                                                                                                      * 100
                                                                                                                                                                                      + @EndDay
                                                                                                                                                                                     )
                                                                                                                                                                                 )
                                                                                                                                                                         ,112
                                                                                                                                                                      )
                           ) enr;



        WITH A
        AS (
           --Has dropped from a program whose length is less than one academic year of 900 hours after being enrolled for less than or equal to 15 calendar days
           --Has dropped from a program whose length is equal to or more than one academic year of 900 hours after being enrolled for less than or equal to 30 calendar DAYS
           SELECT se.PrgID
                 ,se.PrgDescrip
                 ,se.ProgramHours
                 ,se.ProgramCredits
                 ,se.SSN
                 ,se.PhoneNumber
                 ,se.Email
                 ,se.Student
                 ,se.LeadId
                 ,se.StuEnrollId
                 ,se.EnrollmentID
                 ,se.Status
                 ,se.SysStatusId
                 ,se.DropReasonId
                 ,se.StartDate
                 ,se.ContractedGradDate
                 ,se.ExpectedGradDate
                 ,se.LDA
                 ,se.LicensureWrittenAllParts
                 ,se.LicensureLastPartWrittenOn
                 ,se.LicensurePassedAllParts
                 ,se.AllowsMoneyOwed
                 ,se.AllowsIncompleteReq
                 ,@EarlyWithdrawalReason AS ReasonNumber
           FROM   @Enrollments se
           WHERE  se.SysStatusId IN ( @Dropped )
                  AND (
                      (
                      se.ProgramHours < 900
                      AND DATEDIFF(DAY, se.StartDate, se.LDA) <= 15
                      )
                      OR (
                         se.ProgramHours >= 900
                         AND DATEDIFF(DAY, se.StartDate, se.LDA) <= 30
                         )
                      ))
            ,B
        AS (
           --exists in naccas drop reason
           SELECT se.PrgID
                 ,se.PrgDescrip
                 ,se.ProgramHours
                 ,se.ProgramCredits
                 ,se.SSN
                 ,se.PhoneNumber
                 ,se.Email
                 ,se.Student
                 ,se.LeadId
                 ,se.StuEnrollId
                 ,se.EnrollmentID
                 ,se.Status
                 ,se.SysStatusId
                 ,se.DropReasonId
                 ,se.StartDate
                 ,se.ContractedGradDate
                 ,se.ExpectedGradDate
                 ,se.LDA
                 ,se.LicensureWrittenAllParts
                 ,se.LicensureLastPartWrittenOn
                 ,se.LicensurePassedAllParts
                 ,se.AllowsMoneyOwed
                 ,se.AllowsIncompleteReq
                 ,@DeployedReason AS ReasonNumber
           FROM   @Enrollments se
           WHERE  se.SysStatusId IN ( @Dropped )
                  AND ( EXISTS (
                               SELECT     *
                               FROM       dbo.syNACCASDropReasonsMapping nm
                               INNER JOIN dbo.syNaccasSettings ns ON ns.NaccasSettingId = nm.NaccasSettingId
                               WHERE      ns.CampusId = @CampusId
                                          AND se.DropReasonId = nm.ADVDropReasonId
                                          AND nm.NACCASDropReasonId IN ( @MilitaryTransfer, @CallToActiveDuty )
                               )
                      ))
            ,C
        AS (
           --exists in naccas drop reason
           SELECT se.PrgID
                 ,se.PrgDescrip
                 ,se.ProgramHours
                 ,se.ProgramCredits
                 ,se.SSN
                 ,se.PhoneNumber
                 ,se.Email
                 ,se.Student
                 ,se.LeadId
                 ,se.StuEnrollId
                 ,se.EnrollmentID
                 ,se.Status
                 ,se.SysStatusId
                 ,se.DropReasonId
                 ,se.StartDate
                 ,se.ContractedGradDate
                 ,se.ExpectedGradDate
                 ,se.LDA
                 ,se.LicensureWrittenAllParts
                 ,se.LicensureLastPartWrittenOn
                 ,se.LicensurePassedAllParts
                 ,se.AllowsMoneyOwed
                 ,se.AllowsIncompleteReq
                 ,@DeceasedReason AS ReasonNumber
           FROM   @Enrollments se
           WHERE  se.SysStatusId IN ( @Dropped )
                  AND ( EXISTS (
                               SELECT     *
                               FROM       dbo.syNACCASDropReasonsMapping nm
                               INNER JOIN dbo.syNaccasSettings ns ON ns.NaccasSettingId = nm.NaccasSettingId
                               WHERE      ns.CampusId = @CampusId
                                          AND se.DropReasonId = nm.ADVDropReasonId
                                          AND nm.NACCASDropReasonId IN ( @Deceased )
                               )
                      ))
            ,D
        AS (
           --exists in naccas drop reason
           SELECT se.PrgID
                 ,se.PrgDescrip
                 ,se.ProgramHours
                 ,se.ProgramCredits
                 ,se.SSN
                 ,se.PhoneNumber
                 ,se.Email
                 ,se.Student
                 ,se.LeadId
                 ,se.StuEnrollId
                 ,se.EnrollmentID
                 ,se.Status
                 ,se.SysStatusId
                 ,se.DropReasonId
                 ,se.StartDate
                 ,se.ContractedGradDate
                 ,se.ExpectedGradDate
                 ,se.LDA
                 ,se.LicensureWrittenAllParts
                 ,se.LicensureLastPartWrittenOn
                 ,se.LicensurePassedAllParts
                 ,se.AllowsMoneyOwed
                 ,se.AllowsIncompleteReq
                 ,@TempOrPermDisabledReason AS ReasonNumber
           FROM   @Enrollments se
           WHERE  se.SysStatusId IN ( @Dropped )
                  AND ( EXISTS (
                               SELECT     *
                               FROM       dbo.syNACCASDropReasonsMapping nm
                               INNER JOIN dbo.syNaccasSettings ns ON ns.NaccasSettingId = nm.NaccasSettingId
                               WHERE      ns.CampusId = @CampusId
                                          AND se.DropReasonId = nm.ADVDropReasonId
                                          AND nm.NACCASDropReasonId IN ( @TemporarilyDisabled, @PermanentlyDisabled )
                               )
                      ))
            ,E
        AS (
           --exists in naccas drop reason
           SELECT se.PrgID
                 ,se.PrgDescrip
                 ,se.ProgramHours
                 ,se.ProgramCredits
                 ,se.SSN
                 ,se.PhoneNumber
                 ,se.Email
                 ,se.Student
                 ,se.LeadId
                 ,se.StuEnrollId
                 ,se.EnrollmentID
                 ,se.Status
                 ,se.SysStatusId
                 ,se.DropReasonId
                 ,se.StartDate
                 ,se.ContractedGradDate
                 ,se.ExpectedGradDate
                 ,se.LDA
                 ,se.LicensureWrittenAllParts
                 ,se.LicensureLastPartWrittenOn
                 ,se.LicensurePassedAllParts
                 ,se.AllowsMoneyOwed
                 ,se.AllowsIncompleteReq
                 ,@TransferredAccreditedReason AS ReasonNumber
           FROM   @Enrollments se
           WHERE  se.SysStatusId IN ( @Dropped )
                  AND ( EXISTS (
                               SELECT     *
                               FROM       dbo.syNACCASDropReasonsMapping nm
                               INNER JOIN dbo.syNaccasSettings ns ON ns.NaccasSettingId = nm.NaccasSettingId
                               WHERE      ns.CampusId = @CampusId
                                          AND se.DropReasonId = nm.ADVDropReasonId
                                          AND nm.NACCASDropReasonId IN ( @TransferredToEquivalent )
                               )
                      ))
            ,F
        AS ( SELECT     pe.PrgID
                       ,pe.PrgDescrip
                       ,pe.ProgramHours
                       ,pe.ProgramCredits
                       ,pe.SSN
                       ,pe.PhoneNumber
                       ,pe.Email
                       ,pe.Student
                       ,pe.LeadId
                       ,pe.StuEnrollId
                       ,pe.EnrollmentID
                       ,pe.Status
                       ,pe.SysStatusId
                       ,pe.DropReasonId
                       ,pe.StartDate
                       ,pe.ContractedGradDate
                       ,pe.ExpectedGradDate
                       ,pe.LDA
                       ,pe.LicensureWrittenAllParts
                       ,pe.LicensureLastPartWrittenOn
                       ,pe.LicensurePassedAllParts
                       ,pe.AllowsMoneyOwed
                       ,pe.AllowsIncompleteReq
                       ,@Transferred100Reason AS ReasonNumber
             FROM       @Enrollments se
             INNER JOIN @Enrollments pe ON pe.StuEnrollId = se.TransferredProgram
             WHERE      (
                        (
                        se.TotalTransferHours IS NOT NULL
                        AND se.TotalTransferHours <> 0
                        )
                        AND (( se.TransferHoursFromProgram / pe.ProgramHours ) = 1 )
                        ))
        SELECT   *
        FROM     (
                 SELECT *
                       ,ROW_NUMBER() OVER ( PARTITION BY resultByProg.LeadId
                                            ORDER BY resultByProg.LeadId
                                          ) numOfReasons
                 FROM   (
                        SELECT result.PrgID
                              ,result.PrgDescrip
                              ,result.ProgramHours
                              ,result.ProgramCredits
                              ,result.SSN
                              ,result.Student
                              ,result.LeadId
                              ,result.StuEnrollId
                              ,result.EnrollmentID
                              ,result.Status
                              ,result.SysStatusId
                              ,result.StartDate
                              ,result.LDA
                              ,result.ReasonNumber
                        FROM   (
                               SELECT *
                               FROM   A
                               UNION
                               SELECT *
                               FROM   B
                               UNION
                               SELECT *
                               FROM   C
                               UNION
                               SELECT *
                               FROM   D
                               UNION
                               SELECT *
                               FROM   E
                               UNION
                               SELECT *
                               FROM   F
                               ) AS result
                        WHERE  (
                               @PrgIdList IS NULL
                               OR result.PrgID IN (
                                                  SELECT Val
                                                  FROM   MultipleValuesForReportParameters(@PrgIdList, ',', 1)
                                                  )
                               )
                        ) resultByProg
                 ) distinctResults
        WHERE    distinctResults.numOfReasons = 1
        ORDER BY distinctResults.Student;


    END;
GO
