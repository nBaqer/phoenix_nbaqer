SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[USP_IPEDS_ProgramList]
    @ProgId AS VARCHAR(8000) = NULL
   ,@CampusId VARCHAR(50)
AS
    BEGIN
        SELECT  ProgDescrip
               ,ProgId
        FROM    arPrograms
        JOIN    dbo.syStatuses sta ON sta.StatusId = arPrograms.StatusId
        WHERE   (
                  (
                    sta.StatusCode = 'A'
                    AND @ProgId IS NULL
                  )
                  OR ProgId IN ( SELECT Val
                                 FROM   MultipleValuesForReportParameters(@ProgId,',',1) )
                )
                AND ( CampGrpId IN ( SELECT CampGrpId
                                     FROM   syCmpGrpCmps
                                     WHERE  CampusId = @CampusId ) )
        ORDER BY ProgDescrip;
    END;
GO
