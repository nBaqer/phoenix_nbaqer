SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
  
  
  
  
  
-- =============================================  
-- Author: Vijay Ramteke  
-- Create date: July 09, 2010  
-- Description: Consecutive Absent Days  
-- =============================================  
CREATE PROCEDURE [dbo].[usp_AR_ConsecutiveAbsentDays]
    @CampGrpId VARCHAR(8000)
   ,@ProgramIds VARCHAR(8000) = NULL
   ,@ConsecutiveAbsentDays1 INT = 0
   ,@ConsecutiveAbsentDays2 INT = 0
   ,@CompOperator VARCHAR(100)
   ,@StudentIdentifier VARCHAR(100)
   ,@campusId UNIQUEIDENTIFIER
AS
    BEGIN  
        DECLARE @IsCampusgrpAll INT;  
      
        SELECT  StuEnrollId
               ,ST.StatusCodeId
               ,PrgVerId
               ,StartDate
               ,StudentId
               ,EnrollmentId
               ,ST.CampusId
        INTO    #tempEnrollments
        FROM    arStuEnrollments ST
               ,syStatusCodes SCT
        WHERE   ST.StatusCodeId = SCT.StatusCodeId
                AND SCT.SysStatusId IN ( 9,20 )
                AND ST.CampusId IN ( (
                                       SELECT DISTINCT
                                                t1.CampusId
                                       FROM     sycmpgrpcmps t1
                                       WHERE    t1.campgrpid IN ( SELECT    strval
                                                                  FROM      dbo.SPLIT(@CampGrpId) )
                                     )
                                )
                AND (
                      @ProgramIds IS NULL
                      OR PrgVerId IN ( SELECT   strval
                                       FROM     dbo.SPLIT(@ProgramIds) )
                    )
                AND ST.CampusId = @campusId;  
  
        SELECT  @IsCampusgrpAll = COUNT(*)
        FROM    syCampGrps
        WHERE   CampGrpId IN ( SELECT   strval
                               FROM     dbo.SPLIT(@CampGrpId) )
                AND CampGrpDescrip = 'All';  
  
        SELECT  *
        INTO    #TempRecords
        FROM    (
                  SELECT    RTRIM(LTRIM(S.LastName)) AS LastName
                           ,RTRIM(LTRIM(S.FirstName)) AS FirstName
                           ,RTRIM(LTRIM(ISNULL(S.MiddleName,''))) AS MiddleName
                           ,SE.StuEnrollId
                           ,SC.StatusCodeDescrip
                           ,RTRIM(LTRIM(PV.PrgVerDescrip)) + ' ( ' + RTRIM(LTRIM(PV.PrgVerCode)) + ' )' AS PrgVerDescrip
                           ,SE.PrgVerId
                           ,SE.StartDate
                           ,SE.CampusId
                           ,CASE @StudentIdentifier
                              WHEN 'StudentId' THEN S.StudentNumber
                              WHEN 'EnrollmentId' THEN SE.EnrollmentId
                              ELSE S.SSN
                            END AS StudentIdentifier
                           ,(
                              SELECT    MAX(AttendedDate)
                              FROM      arExternshipAttendance
                              WHERE     StuEnrollId = SE.StuEnrollId
                                        AND HoursAttended > 0
                            ) AS LDA
                           ,EA.AttendedDate AS RecordDate
                           ,EA.HoursAttended AS HoursAttended
                           ,CG.CampGrpId
                           ,CG.CampGrpDescrip
                  FROM      arStudent S
                           ,#tempEnrollments SE
                           ,syStatusCodes SC
                           ,arPrgVersions PV
                           ,arExternshipAttendance EA
                           ,syCampGrps CG
                           ,syCmpGrpCmps CGC
                  WHERE     S.StudentId = SE.StudentId
                            AND SE.StatusCodeId = SC.StatusCodeId
                            AND SE.PrgVerId = PV.PrgVerId
                            AND EA.StuEnrollId = SE.StuEnrollId
                            AND EA.HoursAttended = 0
                            AND SE.CampusId = CGC.CampusId
                            AND CGC.CampGrpId = CG.CampGrpId
                  UNION
                  SELECT    RTRIM(LTRIM(S.LastName)) AS LastName
                           ,RTRIM(LTRIM(S.FirstName)) AS FirstName
                           ,RTRIM(LTRIM(ISNULL(S.MiddleName,''))) AS MiddleName
                           ,SE.StuEnrollId
                           ,SC.StatusCodeDescrip
                           ,RTRIM(LTRIM(PV.PrgVerDescrip)) + ' ( ' + RTRIM(LTRIM(PV.PrgVerCode)) + ' )' AS PrgVerDescrip
                           ,SE.PrgVerId
                           ,SE.StartDate
                           ,SE.CampusId
                           ,CASE @StudentIdentifier
                              WHEN 'StudentId' THEN S.StudentNumber
                              WHEN 'EnrollmentId' THEN SE.EnrollmentId
                              ELSE S.SSN
                            END AS StudentIdentifier
                           ,(
                              SELECT    MAX(MeetDate)
                              FROM      atClsSectAttendance
                              WHERE     StuEnrollId = SE.StuEnrollId
                                        AND Actual > 0
                            ) AS LDA
                           ,CSA.MeetDate AS RecordDate
                           ,CSA.Actual AS HoursAttended
                           ,CG.CampGrpId
                           ,CG.CampGrpDescrip
                  FROM      arStudent S
                           ,#tempEnrollments SE
                           ,syStatusCodes SC
                           ,arPrgVersions PV
                           ,atClsSectAttendance CSA
                           ,syCampGrps CG
                           ,syCmpGrpCmps CGC
                  WHERE     S.StudentId = SE.StudentId
                            AND SE.StatusCodeId = SC.StatusCodeId
                            AND SE.PrgVerId = PV.PrgVerId
                            AND CSA.StuEnrollId = SE.StuEnrollId
                            AND CSA.Actual = 0
                            AND SE.CampusId = CGC.CampusId
                            AND CGC.CampGrpId = CG.CampGrpId
                  UNION
                  SELECT    RTRIM(LTRIM(S.LastName)) AS LastName
                           ,RTRIM(LTRIM(S.FirstName)) AS FirstName
                           ,RTRIM(LTRIM(ISNULL(S.MiddleName,''))) AS MiddleName
                           ,SE.StuEnrollId
                           ,SC.StatusCodeDescrip
                           ,RTRIM(LTRIM(PV.PrgVerDescrip)) + ' ( ' + RTRIM(LTRIM(PV.PrgVerCode)) + ' )' AS PrgVerDescrip
                           ,SE.PrgVerId
                           ,SE.StartDate
                           ,SE.CampusId
                           ,CASE @StudentIdentifier
                              WHEN 'StudentId' THEN S.StudentNumber
                              WHEN 'EnrollmentId' THEN SE.EnrollmentId
                              ELSE S.SSN
                            END AS StudentIdentifier
                           ,(
                              SELECT    MAX(AttendanceDate)
                              FROM      atAttendance
                              WHERE     EnrollId = SE.StuEnrollId
                                        AND Actual > 0
                            ) AS LDA
                           ,A.AttendanceDate AS RecordDate
                           ,A.Actual AS HoursAttended
                           ,CG.CampGrpId
                           ,CG.CampGrpDescrip
                  FROM      arStudent S
                           ,#tempEnrollments SE
                           ,syStatusCodes SC
                           ,arPrgVersions PV
                           ,atAttendance A
                           ,syCampGrps CG
                           ,syCmpGrpCmps CGC
                  WHERE     S.StudentId = SE.StudentId
                            AND SE.StatusCodeId = SC.StatusCodeId
                            AND SE.PrgVerId = PV.PrgVerId
                            AND A.EnrollId = SE.StuEnrollId
                            AND A.Actual = 0
                            AND SE.CampusId = CGC.CampusId
                            AND CGC.CampGrpId = CG.CampGrpId
                  UNION
                  SELECT    RTRIM(LTRIM(S.LastName)) AS LastName
                           ,RTRIM(LTRIM(S.FirstName)) AS FirstName
                           ,RTRIM(LTRIM(ISNULL(S.MiddleName,''))) AS MiddleName
                           ,SE.StuEnrollId
                           ,SC.StatusCodeDescrip
                           ,RTRIM(LTRIM(PV.PrgVerDescrip)) + ' ( ' + RTRIM(LTRIM(PV.PrgVerCode)) + ' )' AS PrgVerDescrip
                           ,SE.PrgVerId
                           ,SE.StartDate
                           ,SE.CampusId
                           ,CASE @StudentIdentifier
                              WHEN 'StudentId' THEN S.StudentNumber
                              WHEN 'EnrollmentId' THEN SE.EnrollmentId
                              ELSE S.SSN
                            END AS StudentIdentifier
                           ,(
                              SELECT    MAX(RecordDate)
                              FROM      arStudentClockAttendance
                              WHERE     StuEnrollId = SE.StuEnrollId
                                        AND (
                                              ActualHours > 0
                                              AND ActualHours <> 99.00
                                              AND ActualHours <> 999.00
                                              AND ActualHours <> 9999.00
                                            )
                            ) AS LDA
                           ,SCA.RecordDate AS RecordDate
                           ,SCA.ActualHours AS HoursAttended
                           ,CG.CampGrpId
                           ,CG.CampGrpDescrip
                  FROM      arStudent S
                           ,#tempEnrollments SE
                           ,syStatusCodes SC
                           ,arPrgVersions PV
                           ,arStudentClockAttendance SCA
                           ,syCampGrps CG
                           ,syCmpGrpCmps CGC
                  WHERE     S.StudentId = SE.StudentId
                            AND SE.StatusCodeId = SC.StatusCodeId
                            AND SE.PrgVerId = PV.PrgVerId
                            AND SCA.StuEnrollId = SE.StuEnrollId
                            AND SCA.ActualHours = 0
                            AND SE.CampusId = CGC.CampusId
                            AND CGC.CampGrpId = CG.CampGrpId
                  UNION
                  SELECT    RTRIM(LTRIM(S.LastName)) AS LastName
                           ,RTRIM(LTRIM(S.FirstName)) AS FirstName
                           ,RTRIM(LTRIM(ISNULL(S.MiddleName,''))) AS MiddleName
                           ,SE.StuEnrollId
                           ,SC.StatusCodeDescrip
                           ,RTRIM(LTRIM(PV.PrgVerDescrip)) + ' ( ' + RTRIM(LTRIM(PV.PrgVerCode)) + ' )' AS PrgVerDescrip
                           ,SE.PrgVerId
                           ,SE.StartDate
                           ,SE.CampusId
                           ,CASE @StudentIdentifier
                              WHEN 'StudentId' THEN S.StudentNumber
                              WHEN 'EnrollmentId' THEN SE.EnrollmentId
                              ELSE S.SSN
                            END AS StudentIdentifier
                           ,(
                              SELECT    MAX(MeetDate)
                              FROM      atConversionAttendance
                              WHERE     StuEnrollId = SE.StuEnrollId
                                        AND (
                                              Actual > 0
                                              AND Actual <> 99.00
                                              AND Actual <> 999.00
                                              AND Actual <> 9999.00
                                            )
                            ) AS LDA
                           ,CA.MeetDate AS RecordDate
                           ,CA.Actual AS HoursAttended
                           ,CG.CampGrpId
                           ,CG.CampGrpDescrip
                  FROM      arStudent S
                           ,#tempEnrollments SE
                           ,syStatusCodes SC
                           ,arPrgVersions PV
                           ,atConversionAttendance CA
                           ,syCampGrps CG
                           ,syCmpGrpCmps CGC
                  WHERE     S.StudentId = SE.StudentId
                            AND SE.StatusCodeId = SC.StatusCodeId
                            AND SE.PrgVerId = PV.PrgVerId
                            AND CA.StuEnrollId = SE.StuEnrollId
                            AND CA.Actual = 0
                            AND SE.CampusId = CGC.CampusId
                            AND CGC.CampGrpId = CG.CampGrpId
                ) T;  
  
  
        IF @IsCampusgrpAll > 0
            BEGIN  
                SELECT  LastName
                       ,FirstName
                       ,MiddleName
                       ,StuEnrollId
                       ,StatusCodeDescrip
                       ,PrgVerDescrip
                       ,PrgVerId
                       ,StartDate
                       ,CampusId
                       ,StudentIdentifier
                       ,(
                          SELECT    MAX(#TempRecords.LDA)
                          FROM      #TempRecords
                          WHERE     StuEnrollId = TT.StuEnrollId
                        ) AS LDA
                       ,CountRecordDate
                       ,(
                          SELECT    MAX(#TempRecords.RecordDate)
                          FROM      #TempRecords
                          WHERE     StuEnrollId = TT.StuEnrollId
                                    AND HoursAttended = 0
                        ) AS RecordDateMax
                       ,0 AS DayMissed
                       ,RecordDate
                       ,CampGrpId
                       ,CampGrpDescrip
                       ,(
                          SELECT    CampDescrip
                          FROM      syCampuses C
                          WHERE     C.CampusId = TT.CampusID
                        ) AS CampDescrip
                FROM    (
                          SELECT    LastName
                                   ,FirstName
                                   ,MiddleName
                                   ,StuEnrollId
                                   ,StatusCodeDescrip
                                   ,PrgVerDescrip
                                   ,PrgVerId
                                   ,StartDate
                                   ,CampusId
                                   ,StudentIdentifier
                                   ,LDA
                                   ,0 AS CountRecordDate
                                   ,RecordDate
                                   ,CampGrpId
                                   ,CampGrpDescrip
                          FROM      #TempRecords
                          WHERE     CampGrpDescrip = 'All'
                        ) TT
                ORDER BY LastName
                       ,FirstName
                       ,CampGrpDescrip
                       ,RecordDate;  
            END;  
        ELSE
            BEGIN  
                SELECT  LastName
                       ,FirstName
                       ,MiddleName
                       ,StuEnrollId
                       ,StatusCodeDescrip
                       ,PrgVerDescrip
                       ,PrgVerId
                       ,StartDate
                       ,CampusId
                       ,StudentIdentifier
                       ,(
                          SELECT    MAX(#TempRecords.LDA)
                          FROM      #TempRecords
                          WHERE     StuEnrollId = TT.StuEnrollId
                        ) AS LDA
                       ,CountRecordDate
                       ,(
                          SELECT    MAX(#TempRecords.RecordDate)
                          FROM      #TempRecords
                          WHERE     StuEnrollId = TT.StuEnrollId
                                    AND HoursAttended = 0
                        ) AS RecordDateMax
                       ,0 AS DayMissed
                       ,RecordDate
                       ,CampGrpId
                       ,CampGrpDescrip
                       ,(
                          SELECT    CampDescrip
                          FROM      syCampuses C
                          WHERE     C.CampusId = TT.CampusID
                        ) AS CampDescrip
                FROM    (
                          SELECT    LastName
                                   ,FirstName
                                   ,MiddleName
                                   ,StuEnrollId
                                   ,StatusCodeDescrip
                                   ,PrgVerDescrip
                                   ,PrgVerId
                                   ,StartDate
                                   ,CampusId
                                   ,StudentIdentifier
                                   ,LDA
                                   ,0 AS CountRecordDate
                                   ,RecordDate
                                   ,CampGrpId
                                   ,CampGrpDescrip
                          FROM      #TempRecords
                          WHERE     CampGrpDescrip <> 'All'
                        ) TT
                ORDER BY LastName
                       ,FirstName
                       ,CampGrpDescrip
                       ,RecordDate;  
  
            END;  
     
      
        DROP TABLE #tempEnrollments;  
        DROP TABLE #TempRecords;  
    END;  



GO
