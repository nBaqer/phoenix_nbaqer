SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[CheckIfDataExistsInExceptionReport]
    @SSN VARCHAR(12)
   ,@FAID VARCHAR(50)
   ,@Fund VARCHAR(50)
   ,@FileName VARCHAR(150)
AS
    SELECT  COUNT(*)
    FROM    syFameESPExceptionReport
    WHERE   SSN = @SSN
            AND FAID = @FAID
            AND Fund = @Fund
            AND FileName LIKE '" & @FileName & "%';



GO
