SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_CheckIfTestExistsForTheCourse]
    @TermId UNIQUEIDENTIFIER
   ,@StuEnrollId UNIQUEIDENTIFIER
AS
    SELECT DISTINCT
            COUNT(t5.ModUser) AS rowcounter
    FROM    arResults t1
           ,arClassSections t2
           ,arTerm t3
           ,arBridge_GradeComponentTypes_Courses t4
           ,arRules_GradeComponentTypes_Courses t5
           ,arReqs t6
    WHERE   t1.TestId = t2.ClsSectionId
            AND t2.TermId = t3.TermId
            AND t2.ReqId = t4.ReqId
            AND t4.GrdComponentTypeId_ReqId = t5.GrdComponentTypeId_ReqId
            AND t2.ReqId = t6.ReqId
            AND t3.TermId = @TermId
            AND t1.StuEnrollId = @StuEnrollId;



GO
