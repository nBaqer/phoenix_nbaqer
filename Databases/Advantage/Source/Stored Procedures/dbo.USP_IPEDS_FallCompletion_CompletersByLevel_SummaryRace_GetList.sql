SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[USP_IPEDS_FallCompletion_CompletersByLevel_SummaryRace_GetList]
    @CampusId VARCHAR(50)
   ,@ProgId VARCHAR(4000) = NULL
   ,@StartDate DATETIME = NULL
   ,@EndDate DATE = NULL
AS
    BEGIN
        SELECT  RaceSequence
               ,Race
               ,NEWID() AS RowNumber
               ,SUM(Seq1Count) AS Seq1Count
               ,SUM(Seq2Count) AS Seq2Count
               ,SUM(Seq3Count) AS Seq3Count
               ,SUM(Seq4Count) AS Seq4Count
               ,SUM(Seq5Count) AS Seq5Count
               ,SUM(Seq6Count) AS Seq6Count
               ,SUM(Seq7Count) AS Seq7Count
        FROM    (
                  SELECT    NEWID() AS RowNumber
                           ,StudentId
                           ,SSN
                           ,StudentNumber
                           ,StudentName
                           ,Seq1
                           ,Seq2
                           ,Seq3
                           ,Seq4
                           ,Seq5
                           ,Seq6
                           ,Seq7
                           ,Race
                           ,RaceSequence
                           ,CASE Seq1
                              WHEN 'X' THEN 1
                              ELSE 0
                            END AS Seq1Count
                           ,CASE Seq2
                              WHEN 'X' THEN 1
                              ELSE 0
                            END AS Seq2Count
                           ,CASE Seq3
                              WHEN 'X' THEN 1
                              ELSE 0
                            END AS Seq3Count
                           ,CASE Seq4
                              WHEN 'X' THEN 1
                              ELSE 0
                            END AS Seq4Count
                           ,CASE Seq5
                              WHEN 'X' THEN 1
                              ELSE 0
                            END AS Seq5Count
                           ,CASE Seq6
                              WHEN 'X' THEN 1
                              ELSE 0
                            END AS Seq6Count
                           ,CASE Seq7
                              WHEN 'X' THEN 1
                              ELSE 0
                            END AS Seq7Count
                  FROM      (
                              SELECT DISTINCT
                                        t1.StudentId
                                       ,t1.SSN
                                       ,t1.StudentNumber
                                       ,t1.LastName + ', ' + t1.FirstName + ' ' + ISNULL(t1.MiddleName,'') AS StudentName
                                       ,CASE WHEN t13.IPEDSValue = 154 THEN 'X'
                                             ELSE ''
                                        END AS Seq1
                                       ,CASE WHEN t13.IPEDSValue = 155 THEN 'X'
                                             ELSE ''
                                        END AS Seq2
                                       ,CASE WHEN t13.IPEDSValue = 156 THEN 'X'
                                             ELSE ''
                                        END AS Seq3
                                       ,CASE WHEN t13.IPEDSValue = 157 THEN 'X'
                                             ELSE ''
                                        END AS Seq4
                                       ,CASE WHEN t13.IPEDSValue = 158 THEN 'X'
                                             ELSE ''
                                        END AS Seq5
                                       ,CASE WHEN t13.IPEDSValue = 159 THEN 'X'
                                             ELSE ''
                                        END AS Seq6
                                       ,CASE WHEN t13.IPEDSValue = 160 THEN 'X'
                                             ELSE ''
                                        END AS Seq7
                                       ,'Nonresident Alien' AS Race
                                       ,1 AS RaceSequence
                              FROM      adGenders t3
                              LEFT JOIN arStudent t1 ON t3.GenderId = t1.Gender
                              LEFT JOIN adEthCodes t4 ON t4.EthCodeId = t1.Race
                              INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                              INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                              INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                                           AND t6.SysStatusId NOT IN ( 7,8 )
                              INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                              INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                              INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                              LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                              INNER JOIN adCitizenships t12 ON t1.Citizen = t12.CitizenshipId
                              LEFT JOIN arProgCredential t13 ON t8.CredentialLvlId = t13.CredentialId
				-- select * from arProgCredential order by IPEDSSequence
                              WHERE     t2.CampusId = @CampusId
                                        AND (
                                              (
                                                t8.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
                                                AND @ProgId IS NULL
                                              )
                                              OR t8.ProgId IN ( SELECT  Val
                                                                FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                            )
                                        AND t6.SysStatusId IN ( 14 )
                                        AND (
                                              t3.IPEDSValue = 30
                                              OR t3.IPEDSValue = 31
                                            )
                                        AND t1.Race IS NOT NULL
                                        AND t2.ExpGradDate >= @StartDate
                                        AND t2.ExpGradDate <= @EndDate
                                        AND t12.IPEDSValue = 65
                                        AND NOT t8.CredentialLvlId IS NULL
				--and t4.IPEDSSequence is not null
                                        AND t13.IPEDSSequence IS NOT NULL
                              UNION
                              SELECT    NULL AS StudentId
                                       ,NULL AS SSN
                                       ,NULL AS StudentNumber
                                       ,NULL AS StudentName
                                       ,'' AS Seq1
                                       ,'' AS Seq2
                                       ,'' AS Seq3
                                       ,'' AS Seq4
                                       ,'' AS Seq5
                                       ,'' AS Seq6
                                       ,'' AS Seq7
                                       ,'Nonresident Alien' AS Race
                                       ,1 AS RaceSequence
                              UNION
                              SELECT    NULL AS StudentId
                                       ,NULL AS SSN
                                       ,NULL AS StudentNumber
                                       ,NULL AS StudentName
                                       ,'' AS Seq1
                                       ,'' AS Seq2
                                       ,'' AS Seq3
                                       ,'' AS Seq4
                                       ,'' AS Seq5
                                       ,'' AS Seq6
                                       ,'' AS Seq7
                                       ,AgencyDescrip AS Race
                                       ,CASE WHEN RptAgencyFldValId = 26 THEN 2
                                             WHEN RptAgencyFldValId = 27 THEN 3
                                             WHEN RptAgencyFldValId = 23 THEN 4
                                             WHEN RptAgencyFldValId = 24 THEN 5
                                             WHEN RptAgencyFldValId = 82 THEN 6
                                             WHEN RptAgencyFldValId = 25 THEN 7
                                             WHEN RptAgencyFldValId = 83 THEN 8
                                             WHEN RptAgencyFldValId = 28 THEN 9
                                        END AS RaceSequence
                              FROM      syRptAgencyFldValues
                              WHERE     RptAgencyFldId = 15
                                        AND RptAgencyFldValId <> 29
				-- select * from syRptAgencyFldValues
                              UNION
                              SELECT DISTINCT
                                        t1.StudentId
                                       ,t1.SSN
                                       ,t1.StudentNumber
                                       ,t1.LastName + ', ' + t1.FirstName + ' ' + ISNULL(t1.MiddleName,'') AS StudentName
                                       ,CASE WHEN t13.IPEDSValue = 154 THEN 'X'
                                             ELSE ''
                                        END AS Seq1
                                       ,CASE WHEN t13.IPEDSValue = 155 THEN 'X'
                                             ELSE ''
                                        END AS Seq2
                                       ,CASE WHEN t13.IPEDSValue = 156 THEN 'X'
                                             ELSE ''
                                        END AS Seq3
                                       ,CASE WHEN t13.IPEDSValue = 157 THEN 'X'
                                             ELSE ''
                                        END AS Seq4
                                       ,CASE WHEN t13.IPEDSValue = 158 THEN 'X'
                                             ELSE ''
                                        END AS Seq5
                                       ,CASE WHEN t13.IPEDSValue = 159 THEN 'X'
                                             ELSE ''
                                        END AS Seq6
                                       ,CASE WHEN t13.IPEDSValue = 160 THEN 'X'
                                             ELSE ''
                                        END AS Seq7
                                       ,CASE WHEN t4.IPEDSValue IS NULL THEN 'Race/ethnicity unknown'
                                             ELSE (
                                                    SELECT DISTINCT
                                                            AgencyDescrip
                                                    FROM    syRptAgencyFldValues
                                                    WHERE   RptAgencyFldValId = t4.IPEDSValue
                                                  )
                                        END AS Race
                                       ,CASE WHEN t4.IPEDSValue IS NULL THEN 9
                                             ELSE ( t4.IPEDSSequence )
                                        END AS RaceSequence
                              FROM      adGenders t3
                              LEFT JOIN arStudent t1 ON t3.GenderId = t1.Gender
                              LEFT JOIN adEthCodes t4 ON t4.EthCodeId = t1.Race
                              INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                              INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                              INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                                           AND t6.SysStatusId NOT IN ( 7,8 )
                              INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                              INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                              INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                              LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                              INNER JOIN adCitizenships t12 ON t1.Citizen = t12.CitizenshipId
                              LEFT JOIN arProgCredential t13 ON t8.CredentialLvlId = t13.CredentialId
                              WHERE     t2.CampusId = @CampusId
                                        AND (
                                              (
                                                t8.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
                                                AND @ProgId IS NULL
                                              )
                                              OR t8.ProgId IN ( SELECT  Val
                                                                FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                            )
                                        AND t6.SysStatusId IN ( 14 )
                                        AND (
                                              t3.IPEDSValue = 30
                                              OR t3.IPEDSValue = 31
                                            )
                                        AND t1.Race IS NOT NULL
                                        AND t2.ExpGradDate >= @StartDate
                                        AND t2.ExpGradDate <= @EndDate
                                        AND t12.IPEDSValue <> 65
                                        AND NOT t8.CredentialLvlId IS NULL
                                        AND t4.IPEDSSequence IS NOT NULL
                                        AND t13.IPEDSSequence IS NOT NULL
                            ) dt
                ) dt2
        GROUP BY RaceSequence
               ,Race
        ORDER BY RaceSequence; 
    END;

GO
