SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[USP_TMGetUserTasksCalendarDefaultView]
    (
     @UserID AS VARCHAR(8000)
    )
AS
    BEGIN
        SELECT DISTINCT
                DefaultView
        FROM    tmUsersTaskDefaultView
        WHERE   UserID = @UserID;
    END;



GO
