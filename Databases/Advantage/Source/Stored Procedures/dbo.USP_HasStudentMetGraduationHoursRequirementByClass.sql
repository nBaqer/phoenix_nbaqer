SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[USP_HasStudentMetGraduationHoursRequirementByClass]
    (
     @StuEnrollId UNIQUEIDENTIFIER
    ,@CampusId UNIQUEIDENTIFIER
    )
AS
    SET NOCOUNT ON;
    BEGIN  
        --SET @StuEnrollId = '9AC571F7-4672-490F-A2F2-58EE9E7984A8'
        DECLARE @prgVerId UNIQUEIDENTIFIER;
        SET @prgVerId = (
                          SELECT    PrgVerId
                          FROM      dbo.arStuEnrollments
                          WHERE     StuEnrollId = @StuEnrollId
                        );
        DECLARE @InstructionTypeDescrip VARCHAR(50);
        DECLARE @cnt TINYINT;
        DECLARE @i TINYINT;
        DECLARE @ScheduledHrs DECIMAL(18,2);  
        DECLARE @ActualHrs1 DECIMAL(18,2);  
        DECLARE @ActualHrs2 DECIMAL(18,2); 
        SET @ActualHrs1 = 0;
        SET @ActualHrs2 = 0;
        
        DECLARE @UseImportedAttendance BIT;
        IF (
             SELECT COUNT(*)
             FROM   syConfigAppSetValues
             WHERE  SettingId = (
                                  SELECT    SettingId
                                  FROM      dbo.syConfigAppSettings
                                  WHERE     KeyName = 'UseImportedAttendance'
                                )
                    AND CampusId = @CampusId
           ) >= 1
            BEGIN
                SET @UseImportedAttendance = (
                                               SELECT   Value
                                               FROM     dbo.syConfigAppSetValues
                                               WHERE    SettingId = (
                                                                      SELECT    SettingId
                                                                      FROM      dbo.syConfigAppSettings
                                                                      WHERE     KeyName = 'UseImportedAttendance'
                                                                    )
                                                        AND CampusId = @CampusId
                                             );  
								
            END; 
        ELSE
            BEGIN
                SET @UseImportedAttendance = (
                                               SELECT   Value
                                               FROM     dbo.syConfigAppSetValues
                                               WHERE    SettingId = (
                                                                      SELECT    SettingId
                                                                      FROM      dbo.syConfigAppSettings
                                                                      WHERE     KeyName = 'UseImportedAttendance'
                                                                    )
                                                        AND CampusId IS NULL
                                             );
            END;
					
    
        DECLARE @arPrgVerInstructionType TABLE
            (
             PrgVerInstructionTypeId UNIQUEIDENTIFIER
            ,PrgVerId UNIQUEIDENTIFIER
            ,InstructionTypeId UNIQUEIDENTIFIER
            ,InstructionTypeDescrip VARCHAR(50)
            ,Hours DECIMAL(6,0)
            ,RowNumber INT
            );	

        INSERT  INTO @arPrgVerInstructionType
                (
                 PrgVerInstructionTypeId
                ,PrgVerId
                ,InstructionTypeId
                ,InstructionTypeDescrip
                ,Hours
                ,RowNumber
                )
                SELECT  PrgVerInstructionTypeId
                       ,PrgVerId
                       ,InstructionTypeId
                       ,(
                          SELECT    IT.InstructionTypeDescrip
                          FROM      arInstructionType IT
                          WHERE     IT.InstructionTypeId = PV.InstructionTypeId
                        ) AS InstructionTypeDescrip
                       ,Hours
                       ,ROW_NUMBER() OVER ( ORDER BY InstructionTypeId ) AS 'RowNumber'
                FROM    arPrgVerInstructionType PV
                WHERE   PrgVerId = @prgVerId;  
 

        SET @cnt = (
                     SELECT COUNT(*)
                     FROM   @arPrgVerInstructionType
                   );

        SET @i = 0;
        WHILE @i < @cnt
            BEGIN
                SET @i = ( @i + 1 );
                SET @ScheduledHrs = (
                                      SELECT    Hours
                                      FROM      @arPrgVerInstructionType
                                      WHERE     RowNumber = @i
                                    );
							 
                SET @InstructionTypeDescrip = (
                                                SELECT  InstructionTypeDescrip
                                                FROM    @arPrgVerInstructionType
                                                WHERE   RowNumber = @i
                                              );
				--select @InstructionTypeDescrip		
						 
                IF @UseImportedAttendance = 0
                    BEGIN                   
                        SET @ActualHrs1 = (
                                            SELECT  ISNULL(SUM(Actual),0) / 60
                                            FROM    dbo.atClsSectAttendance
                                                   ,dbo.arClsSectMeetings
                                                   ,@arPrgVerInstructionType
                                            WHERE   StuEnrollId = @StuEnrollId
                                                    AND atClsSectAttendance.ClsSectMeetingId = dbo.arClsSectMeetings.ClsSectMeetingId
                                                    AND [@arPrgVerInstructionType].InstructionTypeId = dbo.arClsSectMeetings.InstructionTypeId
                                                    AND [@arPrgVerInstructionType].RowNumber = @i
                                                    AND Actual <> 9999
                                                    AND Actual <> 999
                                          );
                    END;
                    
                ELSE
                    BEGIN
                        SET @ActualHrs1 = (
                                            SELECT  ISNULL(SUM(Actual),0) / 60
                                            FROM    dbo.atClsSectAttendance
                                                   ,dbo.arClsSectMeetings
                                                   ,@arPrgVerInstructionType
                                            WHERE   StuEnrollId = @StuEnrollId
                                                    AND atClsSectAttendance.ClsSectMeetingId = dbo.arClsSectMeetings.ClsSectMeetingId
                                                    AND [@arPrgVerInstructionType].InstructionTypeId = dbo.arClsSectMeetings.InstructionTypeId
                                                    AND [@arPrgVerInstructionType].RowNumber = @i
                                                    AND Actual <> 9999
                                                    AND Actual <> 999
                                          );
                        IF @InstructionTypeDescrip = 'Theory'
                            BEGIN
                                SET @ActualHrs2 = (
                                                    SELECT  ISNULL(SUM(ActualHours),0)
                                                    FROM    dbo.arStudentClockAttendance
                                                    WHERE   StuEnrollId = @StuEnrollId
                                                            AND ActualHours <> 9999
                                                            AND ActualHours <> 999
                                                            AND Converted = 1
                                                  );
								    
									--SELECT @ActualHrs1	 
									--SELECT @ActualHrs2        
                                SET @ActualHrs1 = @ActualHrs1 + @ActualHrs2;
								--SELECT @ActualHrs1
                            END;
					
                    END;
					
                         --SELECT @ActualHrs1
                         --SELECT @ActualHrs2                   
                         --SELECT @ScheduledHrs
                IF ( @ActualHrs1 < @ScheduledHrs )
                    BEGIN  
                        SELECT  'UnSatisfied';  
                        RETURN;
                    END; 
            END;
   
    END;  

GO
