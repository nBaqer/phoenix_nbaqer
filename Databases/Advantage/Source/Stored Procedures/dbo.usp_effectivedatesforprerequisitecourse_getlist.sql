SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_effectivedatesforprerequisitecourse_getlist]
    @ReqId UNIQUEIDENTIFIER
AS
    SELECT TOP 1
            t1.EffectiveDate
           ,t2.StartDate AS ClassStartDate
           ,DATEDIFF(DAY,t1.EffectiveDate,t2.StartDate) AS DaysSinceClassStarted
           ,t1.MinperCategory
           ,t1.MaxperCategory
           ,t1.MinperCombination
           ,t1.MaxperCombination
           ,t1.mentorproctored
           ,t1.ReqId
           ,t1.PreReqId
    FROM    arPrerequisites_GradeComponentTypes_Courses t1
           ,arClassSections t2
    WHERE   t1.ReqId = t2.ReqId
            AND t1.ReqId = @ReqId
            AND DATEDIFF(DAY,t1.EffectiveDate,t2.StartDate) >= 0
    ORDER BY DATEDIFF(DAY,t1.EffectiveDate,t2.StartDate) ASC;



GO
