SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_GetSAPDetailsCount]
    (
     @prgVerId UNIQUEIDENTIFIER = NULL
    )
AS
    SELECT  COUNT(*)
    FROM    dbo.arSAPDetails A
           ,dbo.arSAP B
           ,dbo.arPrgVersions C
    WHERE   A.SAPId = B.SAPId
            AND B.SAPId = C.SAPId
            AND (
                  @prgVerId IS NULL
                  OR C.PrgVerId = @prgVerId
                );



GO
