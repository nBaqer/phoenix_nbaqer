SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_GetEquivalentReqIdDS]
    (
     @reqId UNIQUEIDENTIFIER
    )
AS
    SET NOCOUNT ON;
    SELECT 
		DISTINCT
            EquivReqid
    FROM    arCourseEquivalent
    WHERE   Reqid = @reqId;



GO
