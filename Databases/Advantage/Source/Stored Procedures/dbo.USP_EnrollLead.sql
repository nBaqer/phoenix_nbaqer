SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =========================================================================================================          
-- USP_EnrollLead          
-- =========================================================================================================          
CREATE PROCEDURE [dbo].[USP_EnrollLead]
    (
        @leadId AS UNIQUEIDENTIFIER
       ,@campus UNIQUEIDENTIFIER
       ,@gender AS UNIQUEIDENTIFIER
       ,@familyIncome UNIQUEIDENTIFIER = NULL
       ,@housingType UNIQUEIDENTIFIER = NULL
       ,@educationLevel UNIQUEIDENTIFIER = NULL
       ,@adminCriteria UNIQUEIDENTIFIER = NULL
       ,@degSeekCert UNIQUEIDENTIFIER = NULL
       ,@attendType UNIQUEIDENTIFIER = NULL
       ,@race UNIQUEIDENTIFIER = NULL
       ,@citizenship UNIQUEIDENTIFIER = NULL
       ,@prgVersion UNIQUEIDENTIFIER = NULL
       ,@programId UNIQUEIDENTIFIER = NULL
       ,@areaId UNIQUEIDENTIFIER = NULL
       ,@prgVerType INT = 0
       ,@startDt DATETIME
       --,@studentId UNIQUEIDENTIFIER          
       ,@expectedStartDt DATETIME = NULL
       ,@contractGrdDt DATETIME
       ,@tuitionCategory UNIQUEIDENTIFIER
       ,@enrollDt DATETIME
       ,@transferhr DECIMAL(18, 2) = NULL
       ,@dob AS DATETIME = NULL
       ,@ssn AS VARCHAR(50) = NULL
       ,@depencytype UNIQUEIDENTIFIER = NULL
       ,@maritalstatus UNIQUEIDENTIFIER = NULL
       ,@nationality UNIQUEIDENTIFIER = NULL
       ,@geographicType UNIQUEIDENTIFIER = NULL
       ,@state UNIQUEIDENTIFIER = NULL
       ,@disabled BIT = NULL
       ,@disableAutoCharge BIT = NULL
       ,@thirdPartyContract BIT = 0
       ,@firsttimeSchool BIT = NULL
       ,@firsttimePostSec BIT = NULL
       ,@schedule UNIQUEIDENTIFIER = NULL
       ,@shift UNIQUEIDENTIFIER = NULL
       ,@midptDate DATETIME = NULL
       ,@transferDt DATETIME = NULL
       ,@entranceInterviewDt DATETIME = NULL
       ,@chargingMethod UNIQUEIDENTIFIER = NULL
       ,@fAAvisor UNIQUEIDENTIFIER = NULL
       ,@acedemicAdvisor UNIQUEIDENTIFIER = NULL
       ,@badgeNum VARCHAR(50) = NULL
       ,@studentNumber VARCHAR(50) = ''
       ,@modUser UNIQUEIDENTIFIER = NULL
       ,@enrollId VARCHAR(50) = NULL
    )
AS
    BEGIN
        SET NOCOUNT ON;
        DECLARE @transDate DATETIME;
        DECLARE @MyError AS INTEGER;
        DECLARE @Message AS NVARCHAR(MAX);
        DECLARE @modUserN VARCHAR(50)
               ,@studentStatusId UNIQUEIDENTIFIER
               ,@studentEnrolStatus UNIQUEIDENTIFIER
               ,@studentId UNIQUEIDENTIFIER
               ,@stuEnrollId UNIQUEIDENTIFIER
               ,@admissionRepId UNIQUEIDENTIFIER;
        SET @transDate = GETDATE();
        SET @studentStatusId = (
                               SELECT StatusId
                               FROM   syStatuses
                               WHERE  StatusCode = 'A'
                               );
        --,@originalStatus UNIQUEIDENTIFIER;           
        SET @studentId = NEWID();
        --set @studentEnrolStatus to system future start          
        SET @studentEnrolStatus = (
                                  SELECT     DISTINCT TOP 1 A.StatusCodeId
                                  FROM       syStatusCodes A
                                  INNER JOIN sySysStatus B ON B.SysStatusId = A.SysStatusId
                                  INNER JOIN syStatuses C ON C.StatusId = A.StatusId
                                  WHERE      C.Status = 'Active'
                                             AND B.SysStatusId = 7
                                             AND A.CampGrpId IN (
                                                                SELECT CampGrpId
                                                                FROM   dbo.syCmpGrpCmps
                                                                WHERE  CampusId = @campus
                                                                )
                                  );
        DECLARE @acCalendarID INT;
        SELECT @modUserN = UserName
        FROM   syUsers
        WHERE  UserId = @modUser;
        SET @MyError = 0; -- all good          
        SET @Message = N'';
        BEGIN TRANSACTION;
        BEGIN TRY
            --step 1: temperary insert into arstudent          
            DECLARE @lName VARCHAR(50)
                   ,@fName VARCHAR(50)
                   ,@mName VARCHAR(50);

            SET @lName = (
                         SELECT LastName
                         FROM   adLeads
                         WHERE  LeadId = @leadId
                         );
            SET @fName = (
                         SELECT FirstName
                         FROM   adLeads
                         WHERE  LeadId = @leadId
                         );
            SET @mName = (
                         SELECT MiddleName
                         FROM   adLeads
                         WHERE  LeadId = @leadId
                         );

            IF ( @MyError = 0 )
                BEGIN
                    --update the adleads table          
                    DECLARE @enrolLeadStatus UNIQUEIDENTIFIER;
                    SET @enrolLeadStatus = (
                                           SELECT   TOP 1 StatusCodeId
                                           FROM     syStatusCodes
                                           WHERE    SysStatusId = 6
                                                    AND StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
                                                    AND CampGrpId IN (
                                                                     SELECT CampGrpId
                                                                     FROM   dbo.syCmpGrpCmps
                                                                     WHERE  CampusId = @campus
                                                                     )
                                           ORDER BY ModDate
                                           );
                    DECLARE @stuNum VARCHAR(50);
                    SET @stuNum = (
                                  SELECT StudentNumber
                                  FROM   adLeads
                                  WHERE  LeadId = @leadId
                                  );
                    IF ( @stuNum = '' )
                        BEGIN
                            -- generate student number          
                            DECLARE @formatType INT
                                   ,@yrNo INT
                                   ,@monthNo INT
                                   ,@dateNo INT
                                   ,@lNameNo INT
                                   ,@fNameNo INT;
                            DECLARE @lNameStu VARCHAR(50)
                                   ,@fNameStu VARCHAR(50)
                                   ,@yr VARCHAR(10)
                                   ,@mon VARCHAR(10)
                                   ,@dt VARCHAR(10)
                                   ,@seqStartNo INT;
                            SELECT @formatType = CAST(FormatType AS INT)
                                  ,@yrNo = ISNULL(YearNumber, 0)
                                  ,@monthNo = ISNULL(MonthNumber, 0)
                                  ,@dateNo = ISNULL(DateNumber, 0)
                                  ,@lNameNo = ISNULL(LNameNumber, 0)
                                  ,@fNameNo = ISNULL(FNameNumber, 0)
                                  ,@seqStartNo = SeqStartingNumber
                            FROM   syStudentFormat;
                            IF ( @formatType = 1 )
                                BEGIN
                                    SET @stuNum = '';
                                END;
                            ELSE IF ( @formatType = 3 )
                                     BEGIN
                                         SET @lNameStu = SUBSTRING(@lName, 1, ISNULL(@lNameNo, 0));
                                         SET @fNameStu = SUBSTRING(@fName, 1, ISNULL(@fNameNo, 0));
                                         SET @yr = '';
                                         SET @yr = CASE @yrNo
                                                        WHEN 0 THEN ''
                                                        WHEN 4 THEN SUBSTRING(CAST(YEAR(GETDATE()) AS VARCHAR(10)), 1, 4)
                                                        WHEN 3 THEN SUBSTRING(CAST(YEAR(GETDATE()) AS VARCHAR(10)), 2, 3)
                                                        WHEN 2 THEN SUBSTRING(CAST(YEAR(GETDATE()) AS VARCHAR(10)), 3, 2)
                                                        WHEN 1 THEN SUBSTRING(CAST(YEAR(GETDATE()) AS VARCHAR(10)), 4, 1)
                                                   END;
                                         SET @mon = SUBSTRING(CAST(MONTH(GETDATE()) AS VARCHAR(10)), 1, @monthNo);
                                         SET @dt = SUBSTRING(CAST(DAY(GETDATE()) AS VARCHAR(10)), 1, @dateNo);
                                         IF (
                                            LEN(@mon) = 1
                                            AND @monthNo = 2
                                            )
                                             BEGIN
                                                 SET @mon = '0' + @mon;
                                             END;
                                         IF (
                                            LEN(@dt) = 1
                                            AND @dateNo = 2
                                            )
                                             BEGIN
                                                 SET @dt = '0' + @dt;
                                             END;
                                         SET NOCOUNT ON;
                                         DECLARE @stuId INT;
                                         SET @stuId = (
                                                      SELECT MAX(Student_SeqId) AS Student_SeqID
                                                      FROM   syGenerateStudentFormatID
                                                      ) + 1;
                                         INSERT INTO syGenerateStudentFormatID (
                                                                               Student_SeqId
                                                                              ,ModDate
                                                                               )
                                         VALUES ( @stuId, GETDATE());

                                         IF ( @yr = '' )
                                             BEGIN
                                                 SET @stuNum = @mon + @dt + @lNameStu + @fNameStu + CAST(@stuId AS VARCHAR(5));
                                             END;
                                         ELSE
                                             BEGIN
                                                 SET @stuNum = @yr + @mon + @dt + @lNameStu + @fNameStu + CAST(@stuId AS VARCHAR(5));
                                             END;
                                     --SELECT @stuNum          
                                     END;
                            ELSE IF ( @formatType = 4 )
                                     BEGIN
                                         SET @stuNum = @badgeNum;
                                     END;
                            ELSE
                                     BEGIN
                                         SET NOCOUNT ON;

                                         IF EXISTS (
                                                   SELECT *
                                                   FROM   syGenerateStudentSeq
                                                   )
                                             BEGIN
                                                 SET @stuId = (
                                                              SELECT MAX(Student_SeqId) AS Student_SeqID
                                                              FROM   syGenerateStudentSeq
                                                              ) + 1;
                                             END;
                                         ELSE
                                             BEGIN
                                                 SET @stuId = @seqStartNo;
                                             END;
                                         INSERT INTO syGenerateStudentSeq (
                                                                          Student_SeqId
                                                                         ,ModDate
                                                                          )
                                         VALUES ( @stuId, GETDATE());
                                         SET @stuNum = CAST(@stuId AS VARCHAR(10));
                                     END;

                        END;
                    UPDATE adLeads
                    SET    Gender = @gender
                          ,BirthDate = @dob                             --ISNULL(@dob,BirthDate)          
                          ,SSN = @ssn                                   --ISNULL(@ssn,SSN)          
                          ,DependencyTypeId = @depencytype              --ISNULL(@depencytype,DependencyTypeId)          
                          ,MaritalStatus = @maritalstatus               --ISNULL(@maritalstatus,MaritalStatus)          
                          ,FamilyIncome = @familyIncome
                          ,HousingId = @housingType
                          ,admincriteriaid = @adminCriteria
                          ,DegCertSeekingId = @degSeekCert
                          ,AttendTypeId = @attendType
                          ,Race = @race
                          ,Nationality = @nationality                   --ISNULL(@nationality,Nationality)          
                          ,Citizen = @citizenship
                          ,GeographicTypeId = @geographicType           --ISNULL(@geographicType,GeographicTypeId)          
                          ,IsDisabled = @disabled                       --ISNULL(@disabled,IsDisabled)          
                          ,IsFirstTimeInSchool = @firsttimeSchool       --ISNULL(@firsttimeSchool,IsFirstTimeInSchool)          
                          ,CampusId = @campus
                          ,IsFirstTimePostSecSchool = @firsttimePostSec --ISNULL(@firsttimePostSec,IsFirstTimePostSecSchool)          
                          ,PrgVerId = @prgVersion
                          ,ProgramID = @programId
                          ,AreaID = @areaId
                          ,ProgramScheduleId = @schedule                --ISNULL(@schedule,ProgramScheduleId)          
                          ,ShiftID = @shift                             --ISNULL(@shift,ShiftID)          
                          ,ExpectedStart = @expectedStartDt
                          ,EntranceInterviewDate = @entranceInterviewDt --ISNULL(@entranceInterviewDt,EntranceInterviewDate)          
                          ,StudentNumber = ISNULL(@stuNum, '')          --ISNULL(@studentNumber,StudentNumber)          
                          ,StudentStatusId = ISNULL(@studentStatusId, StudentStatusId)
                          ,StudentId = ISNULL(@studentId, StudentId)
                          ,ModUser = ISNULL(@modUserN, ModUser)
                          ,ModDate = @transDate
                          ,LeadStatus = @enrolLeadStatus                --update to enrolled          
                          ,PreviousEducation = @educationLevel
                          ,EnrollStateId = @state
                    WHERE  LeadId = @leadId;
                    SET @MyError = @@ERROR;
                    IF ( @MyError <> 0 )
                        BEGIN
                            SET @MyError = 1;
                            SET @Message = @Message + N'Failed to update in adleads';
                        END;
                    SET @admissionRepId = (
                                          SELECT AdmissionsRep
                                          FROM   adLeads
                                          WHERE  LeadId = @leadId
                                          );
                END;

            IF ( @MyError = 0 )
                BEGIN
                    --insert into arStuEnrollments          
                    SET @stuEnrollId = NEWID();
                    INSERT INTO arStuEnrollments (
                                                 StuEnrollId
                                                ,StudentId
                                                ,EnrollDate
                                                ,PrgVerId
                                                ,StartDate
                                                ,ExpStartDate
                                                ,MidPtDate
                                                ,TransferDate
                                                ,ShiftId
                                                ,BillingMethodId
                                                ,CampusId
                                                ,StatusCodeId
                                                ,ModDate
                                                ,ModUser
                                                ,EnrollmentId
                                                ,AcademicAdvisor
                                                ,LeadId
                                                ,TuitionCategoryId
                                                ,EdLvlId
                                                ,FAAdvisorId
                                                ,attendtypeid
                                                ,degcertseekingid
                                                ,BadgeNumber
                                                ,ContractedGradDate
                                                ,TransferHours
                                                ,PrgVersionTypeId
                                                ,IsDisabled
                                                ,DisableAutoCharge
                                                ,ThirdPartyContract
                                                ,IsFirstTimeInSchool
                                                ,IsFirstTimePostSecSchool
                                                ,EntranceInterviewDate
                                                ,AdmissionsRep
                                                ,ExpGradDate
                                                ,CohortStartDate
                                                 )
                    VALUES ( @stuEnrollId, @studentId             -- StudentId - uniqueidentifier          
                            ,@enrollDt                            -- EnrollDate - datetime          
                            ,@prgVersion                          -- PrgVerId - uniqueidentifier          
                            ,CONVERT(DATE, @startDt, 101)         -- StartDate - datetime          
                            ,CONVERT(DATE, @expectedStartDt, 101) -- ExpStartDate - datetime          
                            ,@midptDate                           -- MidPtDate - datetime                   
                            ,@transferDt                          -- TransferDate - datetime          
                            ,@shift                               -- ShiftId - uniqueidentifier          
                            ,@chargingMethod                      -- BillingMethodId - uniqueidentifier          
                            ,@campus                              -- CampusId - uniqueidentifier          
                            ,@studentEnrolStatus                  -- StatusCodeId - uniqueidentifier                   
                            ,@transDate                           -- ModDate - datetime          
                            ,@modUserN                            -- ModUser - varchar(50)          
                            ,@enrollId                            -- EnrollmentId - varchar(50)                   
                            ,@acedemicAdvisor                     -- AcademicAdvisor - uniqueidentifier          
                            ,@leadId                              -- LeadId - uniqueidentifier          
                            ,@tuitionCategory                     -- TuitionCategoryId - uniqueidentifier          
                            ,@educationLevel                      -- EdLvlId - uniqueidentifier          
                            ,@fAAvisor                            -- FAAdvisorId - uniqueidentifier          
                            ,@attendType                          -- attendtypeid - uniqueidentifier          
                            ,@degSeekCert                         -- degcertseekingid - uniqueidentifier          
                            ,@badgeNum                            -- BadgeNumber - varchar(50)          
                            ,@contractGrdDt                       -- ContractedGradDate - datetime          
                            ,@transferhr                          -- TransferHours - decimal          
                            ,@prgVerType                          -- PrgVersionTypeId - int          
                            ,@disabled                            -- IsDisabled - bit    
                            ,@disableAutoCharge                   -- DisableAutoCharge - bit    
                            ,@thirdPartyContract                  -- ThirdPartyContract - bit      
                            ,@firsttimeSchool                     -- IsFirstTimeInSchool - bit          
                            ,@firsttimePostSec                    -- IsFirstTimePostSecSchool - bit          
                            ,@entranceInterviewDt                 -- EntranceInterviewDate - datetime          
                            ,@admissionRepId                      -- AdmissionsRep uniqueidentifier          
                            ,@contractGrdDt                       -- ExpGradDate DateTime          
                            ,CONVERT(DATE, @expectedStartDt, 101) -- cohort start date                    
                        );


                    IF ((
                        SELECT TOP 1 RTRIM(LTRIM(FormatType))
                        FROM   dbo.syStudentFormat
                        ) = '4'
                       )
                        BEGIN
                            DECLARE @badgeId VARCHAR(50) = (
                                                           SELECT TOP 1 BadgeNumber
                                                           FROM   dbo.arStuEnrollments
                                                           WHERE  StuEnrollId = @stuEnrollId
                                                           );

                            UPDATE adLeads
                            SET    StudentNumber = @badgeId
                            WHERE  LeadId = @leadId;
                        END;


                    SET @MyError = @@ERROR;
                    IF ( @MyError <> 0 )
                        BEGIN
                            SET @MyError = 1;
                            SET @Message = @Message + N'Failed to insert in arStuEnrollments';
                        END;
                END;
            -- insert into arStudentSchedules if schedule is not null
            IF ( @MyError = 0 )
                BEGIN
                    IF @schedule IS NOT NULL
                        BEGIN
                            INSERT INTO arStudentSchedules (
                                                           StuScheduleId
                                                          ,StuEnrollId
                                                          ,ScheduleId
                                                          ,StartDate
                                                          ,EndDate
                                                          ,Active
                                                          ,ModUser
                                                          ,ModDate
                                                          ,Source
                                                           )
                            VALUES ( NEWID()      -- StuScheduleId - uniqueidentifier
                                    ,@stuEnrollId -- StuEnrollId - uniqueidentifier
                                    ,@schedule    -- ScheduleId - uniqueidentifier
                                    ,NULL         -- StartDate - smalldatetime
                                    ,NULL         -- EndDate - smalldatetime
                                    ,1            -- Active - bit
                                    ,@modUserN    -- ModUser - varchar(50)
                                    ,GETDATE()    -- ModDate - smalldatetime
                                    ,'attendance' -- Source - varchar(50)
                                );
                        END;
                    SET @MyError = @@ERROR;
                    IF ( @MyError <> 0 )
                        BEGIN
                            SET @MyError = 1;
                            SET @Message = @Message + N'Failed to insert in arStuEnrollments';
                        END;
                END;
            IF ( @MyError = 0 )
                BEGIN
                    IF @stuEnrollId IS NOT NULL
                        BEGIN
                            IF ( @MyError = 0 )
                                BEGIN
                                    --insert into syStudentStatusChanges          
                                    DECLARE @OriginalleadStatus UNIQUEIDENTIFIER;
                                    SET @OriginalleadStatus = (
                                                              SELECT LeadStatus
                                                              FROM   adLeads
                                                              WHERE  LeadId = @leadId
                                                              );
                                    INSERT INTO syStudentStatusChanges (
                                                                       StudentStatusChangeId
                                                                      ,StuEnrollId
                                                                      ,OrigStatusId
                                                                      ,NewStatusId
                                                                      ,CampusId
                                                                      ,ModDate
                                                                      ,ModUser
                                                                      ,IsReversal
                                                                      ,DropReasonId
                                                                      ,DateOfChange
                                                                      ,Lda
                                                                       )
                                    VALUES ( NEWID(), @stuEnrollId, @OriginalleadStatus, @studentEnrolStatus, @campus, @transDate, @modUserN, 0, NULL
                                            ,@transDate, NULL );
                                    SET @MyError = @@ERROR;
                                    IF ( @MyError <> 0 )
                                        BEGIN
                                            SET @MyError = 1;
                                            SET @Message = @Message + N'Failed to insert in syStudentStatusChanges';
                                        END;
                                END;
                            --insert into plStudentDocs          
                            INSERT INTO plStudentDocs (
                                                      StudentId
                                                     ,DocumentId
                                                     ,DocStatusId
                                                     ,ReceiveDate
                                                     ,ScannedId
                                                     ,ModDate
                                                     ,ModUser
                                                     ,ModuleID
                                                      )
                                        SELECT @studentId
                                              ,DocumentId
                                              ,DocStatusId
                                              ,ReceiveDate
                                              ,1
                                              ,@transDate
                                              ,@modUserN
                                              ,(
                                               SELECT DISTINCT ModuleId
                                               FROM   adReqs
                                               WHERE  adReqId = adLeadDocsReceived.DocumentId
                                               )
                                        FROM   adLeadDocsReceived
                                        WHERE  LeadId = @leadId;
                        END;
                    SET @MyError = @@ERROR;
                    IF ( @MyError <> 0 )
                        BEGIN
                            SET @MyError = 1;
                            SET @Message = @Message + N'Failed to insert in plStudentDocs';
                        END;
                END;
            IF ( @MyError = 0 )
                BEGIN
                    --update syDocumentHistory          
                    UPDATE syDocumentHistory
                    SET    StudentId = @studentId
                          ,ModUser = @modUserN
                          ,ModDate = @transDate
                    WHERE  LeadId = @leadId;
                    SET @MyError = @@ERROR;
                    IF ( @MyError <> 0 )
                        BEGIN
                            SET @MyError = 1;
                            SET @Message = @Message + N'Failed to update in syDocumentHistory';
                        END;
                END;
            --update adLeadByLeadGroups          
            IF ( @MyError = 0 )
                BEGIN
                    UPDATE adLeadByLeadGroups
                    SET    StuEnrollId = @stuEnrollId
                          ,ModUser = @modUserN
                          ,ModDate = @transDate
                    WHERE  LeadId = @leadId;
                    SET @MyError = @@ERROR;
                    IF ( @MyError <> 0 )
                        BEGIN
                            SET @MyError = 1;
                            SET @Message = @Message + N'Failed to update in adLeadByLeadGroups';
                        END;

                END;
            --insert in adLeadEntranceTest          
            IF ( @MyError = 0 )
                BEGIN

                    UPDATE adLeadEntranceTest
                    SET    StudentId = @studentId
                    WHERE  LeadId = @leadId;
                    SET @MyError = @@ERROR;
                    IF ( @MyError <> 0 )
                        BEGIN
                            SET @MyError = 1;
                            SET @Message = @Message + N'Failed to update in adLeadEntranceTest';

                        END;
                END;
            --copy all the lead requirements adEntrTestOverRide          
            IF ( @MyError = 0 )
                BEGIN

                    UPDATE adEntrTestOverRide
                    SET    StudentId = @studentId
                    WHERE  LeadId = @leadId;

                    SET @MyError = @@ERROR;
                    IF ( @MyError <> 0 )
                        BEGIN
                            SET @MyError = 1;
                            SET @Message = @Message + N'Failed to update in adEntrTestOverRide';
                        END;
                END;
            --exec USP_AD_CopyLeadTransactions          
            IF ( @MyError = 0 )
                BEGIN
                    DECLARE @rc INT;
                    EXECUTE @rc = dbo.USP_AD_CopyLeadTransactions @leadId
                                                                 ,@stuEnrollId
                                                                 ,@campus;
                    SET @MyError = @@ERROR;
                    IF (
                       @MyError <> 0
                       AND @rc = 0
                       )
                        BEGIN
                            SET @MyError = 1;
                            SET @Message = @Message + N'Failed to execute USP_AD_CopyLeadTransactions';
                        END;
                END;
            --exec fees related SP          
            SET @acCalendarID = (
                                SELECT COUNT(P.ACId)
                                FROM   arPrograms P
                                      ,arPrgVersions PV
                                      ,arStuEnrollments SE
                                WHERE  PV.PrgVerId = SE.PrgVerId
                                       AND SE.StuEnrollId = @stuEnrollId
                                       AND P.ProgId = PV.ProgId
                                       AND ACId = 5
                                );
            --IF @acCalendarID >= 1          
            --    BEGIN          
            --apply fees by progver for clock hr programs          
            INSERT INTO saTransactions (
                                       TransactionId
                                      ,StuEnrollId
                                      ,TermId
                                      ,CampusId
                                      ,TransDate
                                      ,TransCodeId
                                      ,TransReference
                                      ,TransDescrip
                                      ,TransAmount
                                      ,FeeLevelId
                                      ,AcademicYearId
                                      ,TransTypeId
                                      ,IsPosted
                                      ,FeeId
                                      ,CreateDate
                                      ,IsAutomatic
                                      ,ModUser
                                      ,ModDate
                                       )
                        SELECT     NEWID()
                                  ,@stuEnrollId
                                  ,NULL
                                  ,@campus
                                  ,SE.ExpStartDate
                                  ,PVF.TransCodeId
                                  ,PV.PrgVerDescrip AS TransDescrip
                                  ,ST.TransCodeDescrip + '-' + PV.PrgVerDescrip AS TransCodeDescrip
                                  ,CASE PVF.UnitId
                                        WHEN 0 THEN PVF.Amount * PV.Credits
                                        WHEN 1 THEN PVF.Amount * PV.Hours
                                        WHEN 2 THEN PVF.Amount
                                   END AS TransAmount
                                  ,2 -- program version fees          
                                  ,NULL
                                  ,0
                                  ,1
                                  ,PVF.PrgVerFeeId
                                  ,@transDate
                                  ,0
                                  ,@modUserN
                                  ,@transDate
                        FROM       saProgramVersionFees AS PVF
                        INNER JOIN arPrgVersions PV ON PV.PrgVerId = PVF.PrgVerId
                        INNER JOIN arStuEnrollments SE ON SE.PrgVerId = PVF.PrgVerId
                        INNER JOIN saTransCodes AS ST ON ST.TransCodeId = PVF.TransCodeId
                        WHERE      SE.StuEnrollId = @stuEnrollId
                                   AND PVF.TuitionCategoryId = SE.TuitionCategoryId
                                   AND RateScheduleId IS NULL;

            --charge Program Fees if applicable          
            IF ( @MyError = 0 )
                BEGIN
                    DECLARE @Result INT;
                    DECLARE @currdt DATETIME;
                    SET @currdt = @transDate;
                    EXEC USP_ApplyFeesByProgramVersion @stuEnrollId    -- uniqueidentifier          
                                                      ,@modUserN       -- varchar(100)          
                                                      ,@campus         -- uniqueidentifier          
                                                      ,@currdt         -- datetime          
                                                      ,@Result OUTPUT; -- int          
                    SET @MyError = @@ERROR;
                END;
            IF ( @MyError = 0 )
                BEGIN
                    IF ( @prgVersion IS NOT NULL )
                        BEGIN
                            EXEC dbo.USP_REGISTER_STUDENT_AUTOMATICALLY @prgVersion
                                                                       ,@stuEnrollId
                                                                       ,@modUserN;
                        END;
                    SET @MyError = @@ERROR;
                END;

            IF (
               @MyError = 0
               AND @Result = 1
               )
                BEGIN
                    COMMIT TRANSACTION;
                    SET @Message = N'Successful COMMIT TRANSACTION'; -- do not change this message. It is used for verification in C# code.          
                    SELECT @Message;
                END;
            ELSE
                BEGIN
                    ROLLBACK TRANSACTION;
                    SET @Message = @Message + N'rollback failed transaction';
                    SELECT @Message;
                END;
        END TRY
        BEGIN CATCH
            ROLLBACK TRANSACTION;
            SET @Message = @Message + N'Failed ROLLBACK TRANSACTION  Catching error';

            SET @Message = ERROR_MESSAGE() + @Message + N'try failed transaction';
            SELECT @Message;
        END CATCH;
    END;
-- =========================================================================================================          
-- END  --  USP_EnrollLead          
-- =========================================================================================================          

GO
