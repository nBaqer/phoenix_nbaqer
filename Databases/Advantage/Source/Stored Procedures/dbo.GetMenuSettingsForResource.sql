SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[GetMenuSettingsForResource] @ResourceId INT
AS
    BEGIN
        SET NOCOUNT ON;
        SELECT  MRUType
               ,HideStatusBar
        FROM    syMenuItems
        WHERE   ResourceId = 308;
    END;



GO
