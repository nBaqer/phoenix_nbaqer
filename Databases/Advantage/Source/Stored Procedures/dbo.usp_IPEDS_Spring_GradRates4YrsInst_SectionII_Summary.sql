SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_IPEDS_Spring_GradRates4YrsInst_SectionII_Summary]
    @CampusId VARCHAR(50)
   ,@ProgId VARCHAR(4000) = NULL
   ,@CohortYear VARCHAR(10) = NULL
   ,@CohortPossible VARCHAR(20) = NULL
   ,@OrderBy VARCHAR(100)
   ,@StartDate DATETIME
   ,@EndDate DATETIME
AS
    DECLARE @ReturnValue VARCHAR(100)
       ,@Value VARCHAR(100);    
    DECLARE @SSN VARCHAR(10)
       ,@FirstName VARCHAR(100)
       ,@LastName VARCHAR(100)
       ,@StudentNumber VARCHAR(50)
       ,@TransferredOut INT
       ,@Exclusions INT;    
    DECLARE @CitizenShip_IPEDSValue INT
       ,@ProgramType_IPEDSValue INT
       ,@Gender_IPEDSValue INT
       ,@FullTimePartTime_IPEDSValue INT
       ,@StudentId UNIQUEIDENTIFIER;     
    DECLARE @StatusDate DATETIME;
    SET @StatusDate = '08/31/' + CONVERT(CHAR(4),YEAR(GETDATE()) - 1);    
-- Check if School tracks grades by letter or numeric
    SET @Value = (
                   SELECT   dbo.GetAppSettingValue(47,@CampusId)
                 );
--SET @Value = (SELECT TOP 1 Value FROM dbo.syConfigAppSetValues WHERE SettingId=47)    
    
--if @CohortPossible='Full Year'    
--  begin    
--   -- the default report date ranges are 9/1/Cohort Year to 8/31/next year.     
--   set @StartDate = '9/1/'+@CohortYear    
--   set @EndDate = '8/31/'+Convert(varchar,(Convert(int,@CohortYear)+1))    
--  end    
-- else    
--  begin    
--   -- the default report date ranges are 9/1/Cohort Year to 12/15/Cohort year.     
--   set @StartDate = '9/1/'+@CohortYear    
--   set @EndDate = '12/15/'+@CohortYear    
--  end    
    
    IF @ProgId IS NOT NULL
        BEGIN    
            SELECT  @ReturnValue = COALESCE(@ReturnValue,'') + ProgDescrip + ','
            FROM    arPrograms t1
            WHERE   t1.ProgId IN ( SELECT   Val
                                   FROM     MultipleValuesForReportParameters(@ProgId,',',1) );    
            SET @ReturnValue = SUBSTRING(@ReturnValue,1,LEN(@ReturnValue) - 1);    
        END;    
    ELSE
        BEGIN    
            SELECT  @ReturnValue = COALESCE(@ReturnValue,'') + ProgDescrip + ','
            FROM    arPrograms t1
            WHERE   t1.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
                    AND t1.CampGrpId IN ( SELECT    CampGrpId
                                          FROM      syCmpGrpCmps
                                          WHERE     CampusId = @CampusId );    
            SET @ReturnValue = SUBSTRING(@ReturnValue,1,LEN(@ReturnValue) - 1);    
        END;    
    
-- Create a temp table to hold the final output of this stored proc    
    CREATE TABLE #GraduationRate
        (
         RowNumber UNIQUEIDENTIFIER
        ,SSN VARCHAR(50)
        ,StudentNumber VARCHAR(50)
        ,StudentName VARCHAR(100)
        ,Gender VARCHAR(50)
        ,Race VARCHAR(50)
        ,CopmPrg150Less2Yrs VARCHAR(10)
        ,CopmPrg1502YrsLess4Yrs VARCHAR(10)
        ,CopmPrg150BachelorOrEquivalent VARCHAR(10)
        ,CopmPrg4YrsOrLess VARCHAR(10)
        ,CopmPrg5Yrs VARCHAR(10)
        ,TransOut VARCHAR(10)
        ,Exclusions VARCHAR(10)
        ,StillInProg5YrsOrLonger VARCHAR(10)
        ,CopmPrg150Less2YrsCount INT
        ,CopmPrg1502YrsLess4YrsCount INT
        ,CopmPrg150BachelorOrEquivalentCount INT
        ,CopmPrg4YrsOrLessCount INT
        ,CopmPrg5YrsCount INT
        ,TransOutCount INT
        ,ExclusionsCount INT
        ,StillInProg5YrsOrLongerCount INT
        ,GenderSequence INT
        ,RaceSequence INT
        ,StudentId UNIQUEIDENTIFIER
        ,StuEnrollId UNIQUEIDENTIFIER
        );     
       
/*********Get the list of students that will be shown in the report - Starts Here ******************/       
    CREATE TABLE #StudentsList
        (
         StudentId UNIQUEIDENTIFIER
        ,StuEnrollId UNIQUEIDENTIFIER
        ,SSN VARCHAR(10)
        ,FirstName VARCHAR(100)
        ,LastName VARCHAR(100)
        ,MiddleName VARCHAR(50)
        ,StudentNumber VARCHAR(50)
        ,TransferredOut INT
        ,Exclusions INT
        ,CitizenShip_IPEDSValue INT
        ,ProgramType_IPEDSValue INT
        ,Gender_IPEDSValue INT
        ,FullTimePartTime_IPEDSValue INT
        ,GenderId UNIQUEIDENTIFIER
        ,RaceId UNIQUEIDENTIFIER
        ,CitizenId UNIQUEIDENTIFIER
        ,GenderDescription VARCHAR(50)
        ,RaceDescription VARCHAR(50)
        ,StudentGraduatedStatusCount INT
        ,ClockHourProgramCount INT
        );    
    
-- Get the list of FullTime, FirstTime, UnderGraduate Students    
-- Exclude students who are Transferred in to the institution 
    IF @CohortPossible = 'full year'
        BEGIN
            INSERT  INTO #StudentsList
                    SELECT DISTINCT
                            t1.StudentId
                           ,t2.StuEnrollId
                           ,t1.SSN
                           ,t1.FirstName
                           ,t1.LastName
                           ,t1.MiddleName
                           ,t1.StudentNumber
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SQ1
                                       ,syStatusCodes SQ2
                                       ,dbo.sySysStatus SQ3
                              WHERE     SQ1.StatusCodeId = SQ2.StatusCodeId
                                        AND SQ2.SysStatusId = SQ3.SysStatusId
                                        AND SQ1.StuEnrollId = t2.StuEnrollId
                                        AND SQ3.SysStatusId = 19
                                        AND --SQ1.TransferDate<=@EndDate AND 
                                        SQ1.StuEnrollId NOT IN ( SELECT DISTINCT
                                                                        StuEnrollId
                                                                 FROM   arTrackTransfer )
                                        AND SQ1.DateDetermined <= @StatusDate
                            ) AS TransferredOut
                           ,(    
     -- Check if the student was either dropped and if the drop reason is either    
     -- deceased, active duty, foreign aid service, church mission    
                              CASE WHEN (
                                          SELECT    COUNT(*)
                                          FROM      arStuEnrollments SQ1
                                                   ,syStatusCodes SQ2
                                                   ,dbo.sySysStatus SQ3
                                                   ,arDropReasons SQ44
                                          WHERE     SQ1.StatusCodeId = SQ2.StatusCodeId
                                                    AND SQ2.SysStatusId = SQ3.SysStatusId
                                                    AND SQ1.DropReasonId = SQ44.DropReasonId
                                                    AND SQ1.StuEnrollId = t2.StuEnrollId
                                                    AND SQ3.SysStatusId IN ( 12 ) -- Dropped    
                                                    AND SQ1.DateDetermined <= @StatusDate
                                                    AND SQ44.IPEDSValue IN ( 15,16,17,18,19 )
                                        ) >= 1 THEN 1
                                   ELSE 0
                              END ) AS Exclusions
                           ,t11.IPEDSValue AS CitizenShip_IPEDSValue
                           ,t9.IPEDSValue AS ProgramType_IPEDSValue
                           ,t3.IPEDSValue AS Gender_IPEDSValue
                           ,t10.IPEDSValue AS FullTimePartTime_IPEDSValue
                           ,t1.Gender
                           ,CASE WHEN (
                                        t1.Race IS NULL
                                        AND t11.IPEDSValue <> 65
                                      ) THEN (
                                               SELECT TOP 1
                                                        EthCodeId
                                               FROM     adEthCodes
                                               WHERE    EthCodeDescrip = 'Race/ethnicity unknown'
                                             )
                                 ELSE t1.Race
                            END AS Race
                           ,t1.Citizen
                           ,(
                              SELECT DISTINCT
                                        AgencyDescrip
                              FROM      syRptAgencyFldValues
                              WHERE     RptAgencyFldValId = t3.IPEDSValue
                            ) AS GenderDescription
                           ,CASE WHEN (
                                        t1.Race IS NULL
                                        AND t11.IPEDSValue <> 65
                                      ) THEN 'Race/ethnicity unknown'
                                 ELSE (
                                        SELECT DISTINCT
                                                AgencyDescrip
                                        FROM    syRptAgencyFldValues
                                        WHERE   RptAgencyFldValId = t4.IPEDSValue
                                      )
                            END AS RaceDescription
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SEC
                                       ,syStatusCodes SC
                              WHERE     SEC.StuEnrollId = t2.StuEnrollId
                                        AND SEC.StatusCodeId = SC.StatusCodeId
                                        AND SC.SysStatusId = 14
                                        AND ExpGradDate <= @StatusDate
                            ) AS StudentGraduatedStatusCount
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SEC
                                       ,arPrograms P
                                       ,arPrgVersions PV
                              WHERE     SEC.PrgVerId = PV.PrgVerId
                                        AND P.ProgId = PV.ProgId
                                        AND SEC.StuEnrollId = t2.StuEnrollId
                                        AND P.ACId = 5
                            ) AS ClockHourProgramCount
                    FROM    adGenders t3
                    LEFT JOIN arStudent t1 ON t3.GenderId = t1.Gender
                    LEFT JOIN adEthCodes t4 ON t4.EthCodeId = t1.Race
                    INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                    INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                    INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                                 AND t6.SysStatusId NOT IN ( 8 ) -- Ignore "No Start" Students    
                    INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                    INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                    INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                    LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                    LEFT JOIN dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                    INNER JOIN adCitizenships t11 ON t1.Citizen = t11.CitizenshipId
                    WHERE   LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                            AND (
                                  @ProgId IS NULL
                                  OR t8.ProgId IN ( SELECT  Val
                                                    FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                )
                            AND t10.IPEDSValue = 61
                            AND -- Full Time    
                            t12.IPEDSValue = 11
                            AND -- First Time    
                            t9.IPEDSValue = 58
                            AND -- Under Graduate    
                            (
                              t2.StartDate >= @StartDate
                              AND t2.StartDate <= @EndDate
                            ) -- Student Should Have Started Before the Report End Date  
                        
                         -- Show students with degree intent
                            AND t2.StuEnrollId IN ( SELECT  SQ1.StuEnrollId
                                                    FROM    arStuEnrollments SQ1
                                                           ,arPrgVersions SQ2
                                                           ,arDegrees SQ3
                                                    WHERE   SQ1.PrgVerId = SQ2.PrgVerId
                                                            AND SQ2.DegreeId = SQ3.DegreeId
                                                            AND (
                                                                  SQ3.IPEDSValue = 143
                                                                  OR SQ3.IPEDSValue IS NULL
                                                                ) --Bachelor  
                                                            AND SQ1.StuEnrollId = t2.StuEnrollId )
						  
                            -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
                            AND t2.StuEnrollId NOT IN ( SELECT  t1.StuEnrollId
                                                        FROM    arStuEnrollments t1
                                                               ,syStatusCodes t2
                                                        WHERE   t1.StatusCodeId = t2.StatusCodeId
                                                                AND StartDate <= @EndDate
                                                                AND -- Student started before the end date range
                                                                LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                                                AND (
                                                                      @ProgId IS NULL
                                                                      OR t8.ProgId IN ( SELECT  Val
                                                                                        FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                                                    )
                                                                AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                                                AND (
                                                                      t1.DateDetermined < @StartDate
                                                                      OR ExpGradDate < @StartDate
                                                                      OR LDA < @StartDate
                                                                    ) ) 
                        -- If Student is enrolled in only one program version and if that program version     
                       -- happens to be a continuing ed program exclude the student    
                            AND t2.StudentId NOT IN ( SELECT DISTINCT
                                                                StudentId
                                                      FROM      (
                                                                  SELECT    StudentId
                                                                           ,COUNT(*) AS RowCounter
                                                                  FROM      arStuEnrollments
                                                                  WHERE     PrgVerId IN ( SELECT    PrgVerId
                                                                                          FROM      arPrgVersions
                                                                                          WHERE     IsContinuingEd = 1 )
                                                                  GROUP BY  StudentId
                                                                  HAVING    COUNT(*) = 1
                                                                ) dtStudent_ContinuingEd )    
                                 
                        -- Exclude students who were Transferred in to your institution     
                        -- This was used in FALL Part B report and we can reuse it here    
                            AND StuEnrollId NOT IN (
                            SELECT DISTINCT
                                    SQ1.StuEnrollId
                            FROM    arStuEnrollments SQ1
                                   ,adDegCertSeeking SQ2
                            WHERE   SQ1.StuEnrollId = t2.StuEnrollId
                                    AND     
                                    -- To be considered for TransferIn, Student should be a First-Time Student    
                                    --SQ1.LeadId IS NOT NULL AND 
                                    SQ1.degcertseekingid = SQ2.DegCertSeekingId
                                    AND SQ2.IPEDSValue = 11
                                    AND (
                                          SQ1.TransferHours > 0
                                          OR SQ1.StuEnrollId = CASE WHEN @Value = 'letter'
                                                                    THEN (
                                                                           SELECT TOP 1
                                                                                    StuEnrollId
                                                                           FROM     arTransferGrades
                                                                           WHERE    StuEnrollId = SQ1.StuEnrollId
                                                                                    AND GrdSysDetailId IN ( SELECT  GrdSysDetailId
                                                                                                            FROM    arGradeSystemDetails
                                                                                                            WHERE   IsTransferGrade = 1 )
                                                                         )
                                                                    ELSE (
                                                                           SELECT TOP 1
                                                                                    StuEnrollId
                                                                           FROM     arTransferGrades
                                                                           WHERE    IsTransferred = 1
                                                                                    AND StuEnrollId = SQ1.StuEnrollId
                                                                         )
                                                               END
                                        )
                                    AND SQ1.StartDate < @EndDate
                                    AND NOT EXISTS ( SELECT StuEnrollId
                                                     FROM   arStuEnrollments
                                                     WHERE  StudentId = SQ1.StudentId
                                                            AND StartDate < SQ1.StartDate ) )
                            AND t2.StartDate IN ( SELECT    MIN(StartDate)
                                                  FROM      arStuEnrollments t2
                                                  INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                                                  INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                                                  INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                                                  LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                                                  LEFT JOIN dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                                                  WHERE     LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                                                            AND (
                                                                  @ProgId IS NULL
                                                                  OR t8.ProgId IN ( SELECT  Val
                                                                                    FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                                                )
                                                            AND t10.IPEDSValue = 61
                                                            AND -- Full Time  
                                                            t12.IPEDSValue = 11
                                                            AND -- First Time  
                                                            t9.IPEDSValue = 58
                                                            AND -- Under Graduate  
                                                            t2.StudentId = t1.StudentId
                                                            AND (
                                                                  t2.StartDate >= @StartDate
                                                                  AND t2.StartDate <= @EndDate
                                                                ) )
					 	-- If two enrollments fall on same start date pick any one 
                            AND t2.StuEnrollId IN (
                            SELECT TOP 1
                                    StuEnrollId
                            FROM    arStuEnrollments t2
                            INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                            INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                            INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                            LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                            LEFT JOIN dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                            WHERE   LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                                    AND (
                                          @ProgId IS NULL
                                          OR t8.ProgId IN ( SELECT  Val
                                                            FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                        )
                                    AND t10.IPEDSValue = 61
                                    AND -- Full Time  
                                    t12.IPEDSValue = 11
                                    AND -- First Time  
                                    t9.IPEDSValue = 58
                                    AND -- Under Graduate  
                                    t2.StudentId = t1.StudentId
                                    AND (
                                          t2.StartDate >= @StartDate
                                          AND t2.StartDate <= @EndDate
                                        )
                                    AND t2.StartDate IN ( SELECT    MIN(StartDate)
                                                          FROM      arStuEnrollments t2
                                                          INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                                                          INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                                                          INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                                                          LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                                                          LEFT JOIN dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                                                          WHERE     LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                                                                    AND (
                                                                          @ProgId IS NULL
                                                                          OR t8.ProgId IN ( SELECT  Val
                                                                                            FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                                                        )
                                                                    AND t10.IPEDSValue = 61
                                                                    AND -- Full Time  
                                                                    t12.IPEDSValue = 11
                                                                    AND -- First Time  
                                                                    t9.IPEDSValue = 58
                                                                    AND -- Under Graduate  
                                                                    t2.StudentId = t1.StudentId
                                                                    AND (
                                                                          t2.StartDate >= @StartDate
                                                                          AND t2.StartDate <= @EndDate
                                                                        ) ) );  
        END;
    IF @CohortPossible = 'fall'
        BEGIN
            INSERT  INTO #StudentsList
                    SELECT DISTINCT
                            t1.StudentId
                           ,t2.StuEnrollId
                           ,t1.SSN
                           ,t1.FirstName
                           ,t1.LastName
                           ,t1.MiddleName
                           ,t1.StudentNumber
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SQ1
                                       ,syStatusCodes SQ2
                                       ,dbo.sySysStatus SQ3
                              WHERE     SQ1.StatusCodeId = SQ2.StatusCodeId
                                        AND SQ2.SysStatusId = SQ3.SysStatusId
                                        AND SQ1.StuEnrollId = t2.StuEnrollId
                                        AND SQ3.SysStatusId = 19
                                        AND --SQ1.TransferDate<=@EndDate AND 
                                        SQ1.StuEnrollId NOT IN ( SELECT DISTINCT
                                                                        StuEnrollId
                                                                 FROM   arTrackTransfer )
                                        AND SQ1.DateDetermined <= @StatusDate
                            ) AS TransferredOut
                           ,(    
     -- Check if the student was either dropped and if the drop reason is either    
     -- deceased, active duty, foreign aid service, church mission    
                              CASE WHEN (
                                          SELECT    COUNT(*)
                                          FROM      arStuEnrollments SQ1
                                                   ,syStatusCodes SQ2
                                                   ,dbo.sySysStatus SQ3
                                                   ,arDropReasons SQ44
                                          WHERE     SQ1.StatusCodeId = SQ2.StatusCodeId
                                                    AND SQ2.SysStatusId = SQ3.SysStatusId
                                                    AND SQ1.DropReasonId = SQ44.DropReasonId
                                                    AND SQ1.StuEnrollId = t2.StuEnrollId
                                                    AND SQ3.SysStatusId IN ( 12 ) -- Dropped    
                                                    AND SQ1.DateDetermined <= @StatusDate
                                                    AND SQ44.IPEDSValue IN ( 15,16,17,18,19 )
                                        ) >= 1 THEN 1
                                   ELSE 0
                              END ) AS Exclusions
                           ,t11.IPEDSValue AS CitizenShip_IPEDSValue
                           ,t9.IPEDSValue AS ProgramType_IPEDSValue
                           ,t3.IPEDSValue AS Gender_IPEDSValue
                           ,t10.IPEDSValue AS FullTimePartTime_IPEDSValue
                           ,t1.Gender
                           ,CASE WHEN (
                                        t1.Race IS NULL
                                        AND t11.IPEDSValue <> 65
                                      ) THEN (
                                               SELECT TOP 1
                                                        EthCodeId
                                               FROM     adEthCodes
                                               WHERE    EthCodeDescrip = 'Race/ethnicity unknown'
                                             )
                                 ELSE t1.Race
                            END AS Race
                           ,t1.Citizen
                           ,(
                              SELECT DISTINCT
                                        AgencyDescrip
                              FROM      syRptAgencyFldValues
                              WHERE     RptAgencyFldValId = t3.IPEDSValue
                            ) AS GenderDescription
                           ,CASE WHEN (
                                        t1.Race IS NULL
                                        AND t11.IPEDSValue <> 65
                                      ) THEN 'Race/ethnicity unknown'
                                 ELSE (
                                        SELECT DISTINCT
                                                AgencyDescrip
                                        FROM    syRptAgencyFldValues
                                        WHERE   RptAgencyFldValId = t4.IPEDSValue
                                      )
                            END AS RaceDescription
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SEC
                                       ,syStatusCodes SC
                              WHERE     SEC.StuEnrollId = t2.StuEnrollId
                                        AND SEC.StatusCodeId = SC.StatusCodeId
                                        AND SC.SysStatusId = 14
                                        AND ExpGradDate <= @StatusDate
                            ) AS StudentGraduatedStatusCount
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SEC
                                       ,arPrograms P
                                       ,arPrgVersions PV
                              WHERE     SEC.PrgVerId = PV.PrgVerId
                                        AND P.ProgId = PV.ProgId
                                        AND SEC.StuEnrollId = t2.StuEnrollId
                                        AND P.ACId = 5
                            ) AS ClockHourProgramCount
                    FROM    adGenders t3
                    LEFT JOIN arStudent t1 ON t3.GenderId = t1.Gender
                    LEFT JOIN adEthCodes t4 ON t4.EthCodeId = t1.Race
                    INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                    INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                    INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                                 AND t6.SysStatusId NOT IN ( 8 ) -- Ignore "No Start" Students    
                    INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                    INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                    INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                    LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                    LEFT JOIN dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                    INNER JOIN adCitizenships t11 ON t1.Citizen = t11.CitizenshipId
                    WHERE   LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                            AND (
                                  @ProgId IS NULL
                                  OR t8.ProgId IN ( SELECT  Val
                                                    FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                )
                            AND t10.IPEDSValue = 61
                            AND -- Full Time    
                            t12.IPEDSValue = 11
                            AND -- First Time    
                            t9.IPEDSValue = 58
                            AND -- Under Graduate    
                            t2.StartDate <= @EndDate -- Student Should Have Started Before the Report End Date  
                       
                        -- Show students with degree intent
                            AND t2.StuEnrollId IN ( SELECT  SQ1.StuEnrollId
                                                    FROM    arStuEnrollments SQ1
                                                           ,arPrgVersions SQ2
                                                           ,arDegrees SQ3
                                                    WHERE   SQ1.PrgVerId = SQ2.PrgVerId
                                                            AND (
                                                                  (
                                                                    SQ2.DegreeId = SQ3.DegreeId
                                                                    AND SQ3.IPEDSValue = 143
                                                                  )
                                                                  OR SQ2.DegreeId IS NULL
                                                                  OR SQ3.IPEDSValue IS NULL
                                                                ) --Bachelor  
                                                            AND SQ1.StuEnrollId = t2.StuEnrollId )
						
                        -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
                            AND t2.StuEnrollId NOT IN ( SELECT  t1.StuEnrollId
                                                        FROM    arStuEnrollments t1
                                                               ,syStatusCodes t2
                                                        WHERE   t1.StatusCodeId = t2.StatusCodeId
                                                                AND StartDate <= @EndDate
                                                                AND -- Student started before the end date range
                                                                LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                                                AND (
                                                                      @ProgId IS NULL
                                                                      OR t8.ProgId IN ( SELECT  Val
                                                                                        FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                                                    )
                                                                AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                                                AND (
                                                                      t1.DateDetermined < @EndDate
                                                                      OR ExpGradDate < @EndDate
                                                                      OR LDA < @EndDate
                                                                    ) )   
                        -- If Student is enrolled in only one program version and if that program version     
                       -- happens to be a continuing ed program exclude the student    
                            AND t2.StudentId NOT IN ( SELECT DISTINCT
                                                                StudentId
                                                      FROM      (
                                                                  SELECT    StudentId
                                                                           ,COUNT(*) AS RowCounter
                                                                  FROM      arStuEnrollments
                                                                  WHERE     PrgVerId IN ( SELECT    PrgVerId
                                                                                          FROM      arPrgVersions
                                                                                          WHERE     IsContinuingEd = 1 )
                                                                  GROUP BY  StudentId
                                                                  HAVING    COUNT(*) = 1
                                                                ) dtStudent_ContinuingEd )    
                        -- Exclude students who were Transferred in to your institution     
                        -- This was used in FALL Part B report and we can reuse it here    
                            AND StuEnrollId NOT IN (
                            SELECT DISTINCT
                                    SQ1.StuEnrollId
                            FROM    arStuEnrollments SQ1
                                   ,adDegCertSeeking SQ2
                            WHERE   SQ1.StuEnrollId = t2.StuEnrollId
                                    AND     
                                    -- To be considered for TransferIn, Student should be a First-Time Student    
                                    --SQ1.LeadId IS NOT NULL AND 
                                    SQ1.degcertseekingid = SQ2.DegCertSeekingId
                                    AND SQ2.IPEDSValue = 11
                                    AND (
                                          SQ1.TransferHours > 0
                                          OR SQ1.StuEnrollId = CASE WHEN @Value = 'letter'
                                                                    THEN (
                                                                           SELECT TOP 1
                                                                                    StuEnrollId
                                                                           FROM     arTransferGrades
                                                                           WHERE    StuEnrollId = SQ1.StuEnrollId
                                                                                    AND GrdSysDetailId IN ( SELECT  GrdSysDetailId
                                                                                                            FROM    arGradeSystemDetails
                                                                                                            WHERE   IsTransferGrade = 1 )
                                                                         )
                                                                    ELSE (
                                                                           SELECT TOP 1
                                                                                    StuEnrollId
                                                                           FROM     arTransferGrades
                                                                           WHERE    IsTransferred = 1
                                                                                    AND StuEnrollId = SQ1.StuEnrollId
                                                                         )
                                                               END
                                        )
                                    AND SQ1.StartDate < @EndDate
                                    AND NOT EXISTS ( SELECT StuEnrollId
                                                     FROM   arStuEnrollments
                                                     WHERE  StudentId = SQ1.StudentId
                                                            AND StartDate < SQ1.StartDate ) )
                            AND t2.StartDate IN ( SELECT    MIN(StartDate)
                                                  FROM      arStuEnrollments t2
                                                  INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                                                  INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                                                  INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                                                  LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                                                  LEFT JOIN dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                                                  WHERE     LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                                                            AND (
                                                                  @ProgId IS NULL
                                                                  OR t8.ProgId IN ( SELECT  Val
                                                                                    FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                                                )
                                                            AND t10.IPEDSValue = 61
                                                            AND -- Full Time  
                                                            t12.IPEDSValue = 11
                                                            AND -- First Time  
                                                            t9.IPEDSValue = 58
                                                            AND -- Under Graduate  
                                                            t2.StudentId = t1.StudentId
                                                            AND ( t2.StartDate <= @EndDate ) )
							-- If two enrollments fall on same start date pick any one 
                            AND t2.StuEnrollId IN (
                            SELECT TOP 1
                                    StuEnrollId
                            FROM    arStuEnrollments t2
                            INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                            INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                            INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                            LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                            LEFT JOIN dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                            WHERE   LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                                    AND (
                                          @ProgId IS NULL
                                          OR t8.ProgId IN ( SELECT  Val
                                                            FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                        )
                                    AND t10.IPEDSValue = 61
                                    AND -- Full Time  
                                    t12.IPEDSValue = 11
                                    AND -- First Time  
                                    t9.IPEDSValue = 58
                                    AND -- Under Graduate  
                                    t2.StudentId = t1.StudentId
                                    AND ( t2.StartDate <= @EndDate )
                                    AND t2.StartDate IN ( SELECT    MIN(StartDate)
                                                          FROM      arStuEnrollments t2
                                                          INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                                                          INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                                                          INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                                                          LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                                                          LEFT JOIN dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                                                          WHERE     LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                                                                    AND (
                                                                          @ProgId IS NULL
                                                                          OR t8.ProgId IN ( SELECT  Val
                                                                                            FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                                                        )
                                                                    AND t10.IPEDSValue = 61
                                                                    AND -- Full Time  
                                                                    t12.IPEDSValue = 11
                                                                    AND -- First Time  
                                                                    t9.IPEDSValue = 58
                                                                    AND -- Under Graduate  
                                                                    t2.StudentId = t1.StudentId
                                                                    AND ( t2.StartDate <= @EndDate ) ) );    
        END;   

/*********Get the list of students that will be shown in the report - Ends Here *****************8*/       
    
    
--SELECT * FROM #StudentsList    
--Insert into #GraduationRate    
--Select     
--  NEWID() as RowNumber,dbo.UDF_FormatSSN(SSN) as SSN,StudentNumber,StudentName,    
--  GenderDescription,RaceDescription,    
--  Case When CopmPrg150Less2Yrs>=1 Then 'X' else '' end as CopmPrg150Less2Yrs,    
--  Case When CopmPrg1502YrsLess4Yrs>=1 Then 'X' else '' end as CopmPrg1502YrsLess4Yrs,    
--  CASE WHEN (CopmPrg150Less2Yrs>=1 OR CopmPrg1502YrsLess4Yrs>=1) THEN 'X' 
--  ELSE 
--	Case When CopmPrg150BachelorOrEquivalent>=1 Then 'X' else '' end 
--  END AS CopmPrg150BachelorOrEquivalent,  
--  Case When CopmPrg4YrsOrLess >=1 Then 'X' else '' end as CopmPrg4YrsOrLess,     
--  Case When CopmPrg5Yrs >=1 Then 'X' else '' end as CopmPrg5Yrs ,    
--   Case When (CopmPrg150Less2Yrs>=1 OR CopmPrg1502YrsLess4Yrs>=1 OR CopmPrg150BachelorOrEquivalent>=1 OR
--			CopmPrg150BachelorOrEquivalent>=1 OR CopmPrg4YrsOrLess >=1 OR CopmPrg5Yrs >=1) 
--			THEN '' 
--	ELSE 
--		CASE WHEN TransferredOut>=1 Then 'X' else '' end 
--	END 
--	as TransOut,  
--  Case When Exclusions>=1 Then 'X' else '' end as Exclusions,      
--  Case When StillInProg5YrsOrLonger>=1 Then 'X' else '' end as StillInProg5YrsOrLonger,    
--  Case When CopmPrg150Less2Yrs>=1 Then 1 else 0 end as CopmPrg150Less2YrsCount,    
--  Case When CopmPrg1502YrsLess4Yrs>=1 Then 1 else 0 end as CopmPrg1502YrsLess4YrsCount,    
--  CASE WHEN (CopmPrg150Less2Yrs>=1 OR CopmPrg1502YrsLess4Yrs>=1) THEN 1
--  ELSE 
--	Case When CopmPrg150BachelorOrEquivalent>=1 Then 1 else 0 end 
--  END as CopmPrg150BachelorOrEquivalentCount,    
--  Case When CopmPrg4YrsOrLess >=1 Then 1 else 0 end as CopmPrg4YrsOrLessCount,     
--  Case When CopmPrg5Yrs >=1 Then 1 else 0 end as CopmPrg5YrsCount ,    
--  Case When (CopmPrg150Less2Yrs>=1 OR CopmPrg1502YrsLess4Yrs>=1 OR CopmPrg150BachelorOrEquivalent>=1 OR
--			CopmPrg150BachelorOrEquivalent>=1 OR CopmPrg4YrsOrLess >=1 OR CopmPrg5Yrs >=1) 
--			THEN 0
--	ELSE 
--		CASE WHEN TransferredOut>=1 Then 1 else 0 end 
--	END 
--	as TransOutCount,    
--  Case When Exclusions>=1 Then 1 else 0 end as ExclusionsCount,      
--  Case When StillInProg5YrsOrLonger>=1 Then 1 else 0 end as StillInProg5YrsOrLongerCount,    
--  GenderSequence,RaceSequence,    
--  StudentId,StuEnrollId   
--from    
--(     
-- -- If none of the students are mapped to non-citizen, then insert a row for 'Nonresident Alien'     
-- -- Gender : Men and Women    
-- Select     
--  NULL as SSN, NULL as StudentNumber, NULL as StudentName,    
--  (select Distinct AgencyDescrip from syRptAgencyFldValues where RptAgencyFldValId=t2.IPEDSValue) as GenderDescription,    
--  'Nonresident Alien' as RaceDescription,    
--  0 as CopmPrg150Less2Yrs,    
--  0 as CopmPrg1502YrsLess4Yrs,     
--  0 as CopmPrg150BachelorOrEquivalent ,    
--  0 as CopmPrg4YrsOrLess,     
--  0 as CopmPrg5Yrs,    
--  0 as TransferredOut,    
--  0 as Exclusions,    
--  0 as StillInProg5YrsOrLonger,    
--  t2.IPEDSSequence as GenderSequence, 1 as RaceSequence,NULL AS StudentId,NULL AS StuEnrollId    
-- from    
--  (SELECT * FROM adGenders WHERE IPEDSValue IN (30,31)) t2    
-- WHERE    
--  ( --Check if there are any men in the student list whose citizenship is set to non citizen    
--    SELECT COUNT(t1.FirstName) FROM     
--    #StudentsList t1 Inner Join adCitizenships t11 on t1.Citizenid  = t11.CitizenshipId     
--    WHERE t11.IPEDSValue=65  AND t1.GenderId=t2.GenderId -- Non-Citizen    
--  ) = 0    
-- UNION     
-- -- Bring in Students whose Citizenship Type is set to non citizen     
-- -- Gender : Men and Women    
-- Select     
--  t1.SSN, t1.StudentNumber,t1.LastName + ', ' + t1.FirstName + ' ' + ISNULL(t1.MiddleName,'') as StudentName,    
--  (select Distinct AgencyDescrip from syRptAgencyFldValues where RptAgencyFldValId= t4.IPEDSValue) as GenderDescription,    
--  'Nonresident Alien' as Race,    
      
--  (SELECT CASE WHEN t1.StudentGraduatedStatusCount>=1 THEN -- Is Student Graduated?    
--      CASE WHEN t1.ClockHourProgramCount>=1 THEN -- Is the program a clock hour program?    
--       CASE WHEN (Select (Weeks/52) From arPrgVersions PVI Where PVI.PrgVerId=t3.PrgVerId) <2 THEN -- Is the Program a 2 year program    
--         --Did the student graduate within 150% (1.5) of the Program length in hours    
--        CASE WHEN ((SELECT ISNULL(SUM(SchedHours),0) FROM arStudentClockAttendance WHERE StuEnrollId=t3.StuEnrollId) <= (Select (Hours*1.5) From arPrgVersions PVI Where PVI.PrgVerId=t3.PrgVerId))    
--        THEN 1 ELSE 0 END     
--       ELSE 0 END    
--      ELSE     
--       CASE WHEN (Select (Weeks/52) From arPrgVersions PVI Where PVI.PrgVerId=t3.PrgVerId) <2 THEN -- Is the Program a 2 year program    
--        -- Did the student graduate within 150% (1.5) of the Weeks in Program in weeks    
--        CASE WHEN ((SELECT DATEDIFF(day,t3.StartDate,t3.ExpGradDate)) <=(Select (Weeks*7*1.5) From arPrgVersions PVI Where PVI.PrgVerId=t3.PrgVerId))    
--        THEN 1 ELSE 0 END     
--       ELSE 0 END    
--      END    
--    ELSE 0 END    
--   ) AS CopmPrg150Less2Yrs,    
--   (SELECT CASE WHEN t1.StudentGraduatedStatusCount>=1 THEN -- Is Student Graduated?    
--      CASE WHEN t1.ClockHourProgramCount>=1 THEN -- Is the program a clock hour program?    
--       -- Is the Program greater than 2 year and less than 4 years    
--       CASE WHEN (Select (Weeks/52) From arPrgVersions PVI Where PVI.PrgVerId=t3.PrgVerId) >=2 AND (Select (Weeks/52) From arPrgVersions PVI Where PVI.PrgVerId=t3.PrgVerId)<4 THEN     
--        -- Did the student graduate within 150% (1.5) of the Program length in hours    
--        CASE WHEN ((SELECT ISNULL(SUM(SchedHours),0) FROM arStudentClockAttendance WHERE StuEnrollId=t3.StuEnrollId)<=(Select (Hours*1.5) From arPrgVersions PVI Where PVI.PrgVerId=t3.PrgVerId))    
--        THEN 1 ELSE 0 END     
--       ELSE 0 END    
--      ELSE     
--       -- Is the Program greater than 2 year and less than 4 years    
--       CASE WHEN (Select (Weeks/52) From arPrgVersions PVI Where PVI.PrgVerId=t3.PrgVerId) >=2 AND (Select (Weeks/52) From arPrgVersions PVI Where PVI.PrgVerId=t3.PrgVerId) < 4 THEN     
--        -- Did the student graduate within 150% (1.5) of the Weeks in Program in weeks    
--        CASE WHEN ((SELECT DATEDIFF(day,t3.StartDate,t3.ExpGradDate))<=(Select (Weeks*7*1.5) From arPrgVersions PVI Where PVI.PrgVerId=t3.PrgVerId))    
--        THEN 1 ELSE 0 END     
--       ELSE 0 END    
--      END    
--    ELSE 0 END    
--   ) AS CopmPrg1502YrsLess4Yrs,    
       
--   (SELECT CASE WHEN t1.StudentGraduatedStatusCount>=1 THEN -- Is Student Graduated?    
--      CASE WHEN t1.ClockHourProgramCount>=1 THEN -- Is the program a clock hour program?    
--        -- Did the student graduate within 150% (1.5) of the Program length in hours    
--        CASE WHEN ((SELECT ISNULL(SUM(SchedHours),0) FROM arStudentClockAttendance WHERE StuEnrollId=t3.StuEnrollId)<=(Select (Hours*1.5) From arPrgVersions PVI     
--        INNER JOIN arDegrees D ON PVI.DegreeId=D.DegreeId      
--        Where PVI.PrgVerId=t3.PrgVerId And (D.IPEDSValue=143 or D.IPEDSValue Is NULL)))    
--        THEN 1 ELSE 0 END     
--       ELSE 0 END    
--      ELSE     
--        -- Did the student graduate within 150% (1.5) of the Weeks in Program in weeks    
--        CASE WHEN ((SELECT DATEDIFF(day,t3.StartDate,t3.ExpGradDate))<=(Select (Weeks*7*1.5) From arPrgVersions PVI     
--        INNER JOIN arDegrees D ON PVI.DegreeId=D.DegreeId    
--        Where PVI.PrgVerId=t3.PrgVerId And (D.IPEDSValue=143 or D.IPEDSValue Is NULL)))    
--        THEN 1 ELSE 0 END     
           
--        END    
--   ) AS CopmPrg150BachelorOrEquivalent,    
       
    
--  (SELECT CASE
-- WHEN t1.StudentGraduatedStatusCount=1 THEN -- Is Student Not Graduated?  
--      CASE WHEN t1.ClockHourProgramCount>=1 THEN -- Is the program a clock hour program?  
--       -- Is the Program less than 4 years  
--       CASE WHEN (Select (Weeks/52) From arPrgVersions PVI Where PVI.PrgVerId=t3.PrgVerId) <=4   
--       THEN 
--		 CASE WHEN ((SELECT ISNULL(SUM(SchedHours),0) FROM arStudentClockAttendance 
--        WHERE StuEnrollId=t3.StuEnrollId)<=(Select (Hours) From arPrgVersions PVI   
--        INNER JOIN arDegrees D ON PVI.DegreeId=D.DegreeId    
--        Where PVI.PrgVerId=t3.PrgVerId And (D.IPEDSValue=143 or D.IPEDSValue Is NULL)))  
--        THEN 1 ELSE 0 END   
--       ELSE 0 END   
--      ELSE   
--		CASE WHEN (Select (Weeks/52) From arPrgVersions PVI Where PVI.PrgVerId=t3.PrgVerId) <=4   
--        THEN 
--			CASE WHEN ((SELECT DATEDIFF(day,t3.StartDate,t3.ExpGradDate))<=(Select (Weeks*7) From arPrgVersions PVI 
--			INNER JOIN arDegrees D ON PVI.DegreeId=D.DegreeId  
--			Where PVI.PrgVerId=t3.PrgVerId And (D.IPEDSValue=143 or D.IPEDSValue Is NULL)))  
--	        THEN 1 ELSE 0 END   
--	    ELSE 0 END 
--      END  
--    ELSE 0 END  
--   )  CopmPrg4YrsOrLess,  
     
--   (SELECT CASE WHEN t1.StudentGraduatedStatusCount=1 THEN -- Is Student Not Graduated?  
--      CASE WHEN t1.ClockHourProgramCount>=1 THEN -- Is the program a clock hour program?  
--       -- Is the Program less than 5 years  
--       CASE WHEN (Select (Weeks/52) From arPrgVersions PVI Where PVI.PrgVerId=t3.PrgVerId) >4   
--       THEN 
--        CASE WHEN ((SELECT ISNULL(SUM(SchedHours),0) FROM arStudentClockAttendance 
--        WHERE StuEnrollId=t3.StuEnrollId)<=(Select (Hours) From arPrgVersions PVI   
--        INNER JOIN arDegrees D ON PVI.DegreeId=D.DegreeId    
--        Where PVI.PrgVerId=t3.PrgVerId And (D.IPEDSValue=143 or D.IPEDSValue Is NULL)))  
--        THEN 1 ELSE 0 END   
--       ELSE 0 END   
--      ELSE   
--       CASE WHEN ((Select (Weeks/52) From arPrgVersions PVI Where PVI.PrgVerId=t3.PrgVerId)>4)  
--       THEN 
--			CASE WHEN ((SELECT DATEDIFF(day,t3.StartDate,t3.ExpGradDate))<=(Select (Weeks*7) From arPrgVersions PVI 
--			INNER JOIN arDegrees D ON PVI.DegreeId=D.DegreeId  
--			Where PVI.PrgVerId=t3.PrgVerId And (D.IPEDSValue=143 or D.IPEDSValue Is NULL)
--			))  
--			THEN 1 ELSE 0 END 
--       ELSE 0 END   
--      END  
--    ELSE 0 END  
--   )  CopmPrg5Yrs,   
--     t1.TransferredOut,    
--   t1.Exclusions,     
--   (SELECT CASE WHEN t1.StudentGraduatedStatusCount=0 
--   AND 
--		t3.StatusCodeId IN --Student should be in a InSchool Status
--		(
--			(SELECT DISTINCT t1.StatusCodeId FROM syStatusCodes t1,dbo.sySysStatus t2 
--			WHERE t1.SysStatusId=t2.SysStatusId AND t2.InSchool=1)
--		)
--   THEN -- Is Student Not Graduated?    
--      CASE WHEN t1.ClockHourProgramCount>=1 THEN -- Is the program a clock hour program?    
--       -- Is the Program greater than 2 year and less than 4 years    
--       CASE WHEN (Select (Weeks/52) From arPrgVersions PVI Where PVI.PrgVerId=t3.PrgVerId) >=5     
--       THEN 1 ELSE 0 END     
--      ELSE     
--       CASE WHEN ((Select (Weeks/52) From arPrgVersions PVI Where PVI.PrgVerId=t3.PrgVerId)>=5)    
--       THEN 1 ELSE 0 END     
--      END    
--    ELSE 0 END    
--   ) AS StillInProg5YrsOrLonger,    
--  t4.IPEDSSequence AS GenderSequence,    
--  1 AS RaceSequence,t1.StudentId AS StudentId,t1.StuEnrollId AS StuEnrollId    
-- FROM     
--  (SELECT * FROM adGenders WHERE IPEDSValue IN (30,31)) t4 LEFT JOIN #StudentsList t1 ON t4.GenderId=t1.GenderId    
--  Inner Join adCitizenships t11 on t1.CitizenId  = t11.CitizenshipId    
--  INNER JOIN arStuEnrollments t3 ON t1.StuEnrollId=t3.StuEnrollId     
--  WHERE t11.IPEDSValue=65 --Non-Citizen    
-- UNION    
-- -- Get List of Races that is not tied to any students (Full Time, First Time, UnderGraduate Students)    
-- -- Gender : Men    
-- Select     
--  NULL as SSN, NULL as StudentNumber, NULL as StudentName,    
--  'Men' as GenderDescription,    
--  (select Distinct AgencyDescrip from syRptAgencyFldValues where RptAgencyFldValId=IPEDSValue) as Race,    
--  0 as CopmPrg150Less2Yrs,    
--  0 as CopmPrg1502YrsLess4Yrs,     
--  0 as CopmPrg150BachelorOrEquivalent ,    
--  0 as CopmPrg4YrsOrLess,     
--  0 as CopmPrg5Yrs,    
--  0 as TransferredOut,    
--  0 as Exclusions,    
--  0 as StillInProg5YrsOrLonger,    
--  1 as GenderSequence,IPEDSSequence as RaceSequence,NULL as StudentId,NULL AS StuEnrollId    
-- FROM      
--  adEthCodes     
-- where     
--  EthCodeDescrip <> 'Nonresident Alien' and    
--  EthCodeId not in    
--  (SELECT DISTINCT RaceId FROM #StudentsList t1 INNER JOIN adGenders t2 ON t1.GenderId=t2.GenderId AND t2.IPEDSValue=30)    
-- UNION    
-- -- Get List of Races that is not tied to any students (Full Time, First Time, UnderGraduate Students)    
-- -- Gender : Women    
-- Select     
--  NULL as SSN, NULL as StudentNumber, NULL as StudentName,    
--  'Women' as GenderDescription,    
--  (select Distinct AgencyDescrip from syRptAgencyFldValues where RptAgencyFldValId=IPEDSValue) as Race,    
--  0 as CopmPrg150Less2Yrs,    
--  0 as CopmPrg1502YrsLess4Yrs,     
--  0 as CopmPrg150BachelorOrEquivalent ,    
--  0 as CopmPrg4YrsOrLess,     
--  0 as CopmPrg5Yrs,    
--  0 as TransferredOut,    
--  0 as Exclusions,    
--  0 as StillInProg5YrsOrLonger,    
--  2 as GenderSequence,IPEDSSequence as RaceSequence,NULL as StudentId,NULL AS StuEnrollId   
-- FROM      
--  adEthCodes     
-- where     
--  EthCodeDescrip <> 'Nonresident Alien' and    
--  EthCodeId not in    
--  (SELECT DISTINCT RaceId FROM #StudentsList t1 INNER JOIN adGenders t2 ON t1.GenderId=t2.GenderId AND t2.IPEDSValue=31)    
--  UNION    
--  -- Get Students who belongs to a Race    
--  -- Gender : Men and Women    
--  Select     
--   t1.SSN, t1.StudentNumber,    
--   t1.LastName + ', ' + t1.FirstName + ' ' + ISNULL(t1.MiddleName,'') as StudentName,    
--   --t1.GenderDescription AS GenderDescription,    
--   (select Distinct AgencyDescrip from syRptAgencyFldValues where RptAgencyFldValId=t4.IPEDSValue) as GenderDescription,    
--   (select Distinct AgencyDescrip from syRptAgencyFldValues where RptAgencyFldValId=t2.IPEDSValue) AS RaceDescription,    
       
--   (SELECT CASE WHEN t1.StudentGraduatedStatusCount>=1 THEN -- Is Student Graduated?    
--      CASE WHEN t1.ClockHourProgramCount>=1 THEN -- Is the program a clock hour program?    
--       CASE WHEN (Select (Weeks/52) From arPrgVersions PVI Where PVI.PrgVerId=t3.PrgVerId) <2 THEN -- Is the Program a 2 year program    
--         --Did the student graduate within 150% (1.5) of the Program length in hours    
--        CASE WHEN ((SELECT ISNULL(SUM(SchedHours),0) FROM arStudentClockAttendance WHERE StuEnrollId=t3.StuEnrollId) <= (Select (Hours*1.5) From arPrgVersions PVI Where PVI.PrgVerId=t3.PrgVerId))    
--        THEN 1 ELSE 0 END     
--       ELSE 0 END    
--      ELSE     
--       CASE WHEN (Select (Weeks/52) From arPrgVersions PVI Where PVI.PrgVerId=t3.PrgVerId) <2 THEN -- Is the Program a 2 year program    
--        -- Did the student graduate within 150% (1.5) of the Weeks in Program in weeks    
--        CASE WHEN ((SELECT DATEDIFF(day,t3.StartDate,t3.ExpGradDate)) <=(Select (Weeks*7*1.5) From arPrgVersions PVI Where PVI.PrgVerId=t3.PrgVerId))    
--        THEN 1 ELSE 0 END     
--       ELSE 0 END    
--      END    
--    ELSE 0 END    
--   ) AS CopmPrg150Less2Yrs,    
--   (SELECT CASE WHEN t1.StudentGraduatedStatusCount>=1 THEN -- Is Student Graduated?    
--      CASE WHEN t1.ClockHourProgramCount>=1 THEN -- Is the program a clock hour program?    
--       -- Is the Program greater than 2 year and less than 4 years    
--       CASE WHEN (Select (Weeks/52) From arPrgVersions PVI Where PVI.PrgVerId=t3.PrgVerId) >=2 AND (Select (Weeks/52) From arPrgVersions PVI Where PVI.PrgVerId=t3.PrgVerId)<4 THEN     
--        -- Did the student graduate within 150% (1.5) of the Program length in hours    
--        CASE WHEN ((SELECT ISNULL(SUM(SchedHours),0) FROM arStudentClockAttendance WHERE StuEnrollId=t3.StuEnrollId)<=(Select (Hours*1.5) From arPrgVersions PVI Where PVI.PrgVerId=t3.PrgVerId))    
--        THEN 1 ELSE 0 END     
--       ELSE 0 END    
--      ELSE     
--       -- Is the Program greater than 2 year and less than 4 years    
--       CASE WHEN (Select (Weeks/52) From arPrgVersions PVI Where PVI.PrgVerId=t3.PrgVerId) >=2 AND (Select (Weeks/52) From arPrgVersions PVI Where PVI.PrgVerId=t3.PrgVerId) < 4 THEN     
--        -- Did the student graduate within 150% (1.5) of the Weeks in Program in weeks    
--        CASE WHEN ((SELECT DATEDIFF(day,t3.StartDate,t3.ExpGradDate))<=(Select (Weeks*7*1.5) From arPrgVersions PVI Where PVI.PrgVerId=t3.PrgVerId))    
--        THEN 1 ELSE 0 END     
--       ELSE 0 END    
--      END    
--    ELSE 0 END    
--   ) AS CopmPrg1502YrsLess4Yrs,    
--   (SELECT CASE WHEN t1.StudentGraduatedStatusCount>=1 THEN -- Is Student Graduated?    
--      CASE WHEN t1.ClockHourProgramCount>=1 THEN -- Is the program a clock hour program?    
--        -- Did the student graduate within 150% (1.5) of the Program length in hours    
--        CASE WHEN ((SELECT ISNULL(SUM(SchedHours),0) FROM arStudentClockAttendance WHERE StuEnrollId=t3.StuEnrollId)<=(Select (Hours*1.5) From arPrgVersions PVI     
--        INNER JOIN arDegrees D ON PVI.DegreeId=D.DegreeId      
--        Where PVI.PrgVerId=t3.PrgVerId And (D.IPEDSValue=143 or D.IPEDSValue Is NULL)))    
--        THEN 1 ELSE 0 END     
--       ELSE 0 END    
--      ELSE     
--        -- Did the student graduate within 150% (1.5) of the Weeks in Program in weeks    
--        CASE WHEN ((SELECT DATEDIFF(day,t3.StartDate,t3.ExpGradDate))<=(Select (Weeks*7*1.5) From arPrgVersions PVI     
--        INNER JOIN arDegrees D ON PVI.DegreeId=D.DegreeId    
--        Where PVI.PrgVerId=t3.PrgVerId And (D.IPEDSValue=143 or D.IPEDSValue Is NULL)))    
--        THEN 1 ELSE 0 END     
           
--        END    
--   ) AS CopmPrg150BachelorOrEquivalent,    
       
     
--   (SELECT CASE
-- WHEN t1.StudentGraduatedStatusCount=1 THEN -- Is Student Not Graduated?  
--      CASE WHEN t1.ClockHourProgramCount>=1 THEN -- Is the program a clock hour program?  
--       -- Is the Program less than 4 years  
--       CASE WHEN (Select (Weeks/52) From arPrgVersions PVI Where PVI.PrgVerId=t3.PrgVerId) <=4   
--       THEN 
--		 CASE WHEN ((SELECT ISNULL(SUM(SchedHours),0) FROM arStudentClockAttendance 
--        WHERE StuEnrollId=t3.StuEnrollId)<=(Select (Hours) From arPrgVersions PVI   
--        INNER JOIN arDegrees D ON PVI.DegreeId=D.DegreeId    
--        Where PVI.PrgVerId=t3.PrgVerId And (D.IPEDSValue=143 or D.IPEDSValue Is NULL)))  
--        THEN 1 ELSE 0 END   
--       ELSE 0 END   
--      ELSE   
--      CASE WHEN (Select (Weeks/52) From arPrgVersions PVI Where PVI.PrgVerId=t3.PrgVerId) <=4   
--        THEN 
--			CASE WHEN ((SELECT DATEDIFF(day,t3.StartDate,t3.ExpGradDate))<=(Select (Weeks*7) From arPrgVersions PVI 
--			INNER JOIN arDegrees D ON PVI.DegreeId=D.DegreeId  
--			Where PVI.PrgVerId=t3.PrgVerId And (D.IPEDSValue=143 or D.IPEDSValue Is NULL)))  
--	        THEN 1 ELSE 0 END   
--	    ELSE 0 END 
--      END  
--    ELSE 0 END  
--   )  CopmPrg4YrsOrLess,  
--   (SELECT CASE WHEN t1.StudentGraduatedStatusCount=1 THEN -- Is Student Not Graduated?  
--      CASE WHEN t1.ClockHourProgramCount>=1 THEN -- Is the program a clock hour program?  
--       -- Is the Program less than 5 years  
--       CASE WHEN (Select (Weeks/52) From arPrgVersions PVI Where PVI.PrgVerId=t3.PrgVerId) >4   
--       THEN 
--        CASE WHEN ((SELECT ISNULL(SUM(SchedHours),0) FROM arStudentClockAttendance 
--        WHERE StuEnrollId=t3.StuEnrollId)<=(Select (Hours) From arPrgVersions PVI   
--        INNER JOIN arDegrees D ON PVI.DegreeId=D.DegreeId    
--        Where PVI.PrgVerId=t3.PrgVerId And (D.IPEDSValue=143 or D.IPEDSValue Is NULL)))  
--        THEN 1 ELSE 0 END   
--       ELSE 0 END   
--      ELSE   
--       CASE WHEN ((Select (Weeks/52) From arPrgVersions PVI Where PVI.PrgVerId=t3.PrgVerId)>4)  
--       THEN 
--			CASE WHEN ((SELECT DATEDIFF(day,t3.StartDate,t3.ExpGradDate))<=(Select (Weeks*7) From arPrgVersions PVI 
--			INNER JOIN arDegrees D ON PVI.DegreeId=D.DegreeId  
--			Where PVI.PrgVerId=t3.PrgVerId And (D.IPEDSValue=143 or D.IPEDSValue Is NULL)
--			))  
--			THEN 1 ELSE 0 END 
--       ELSE 0 END   
--      END  
--    ELSE 0 END  
--   )  CopmPrg5Yrs,
--    t1.TransferredOut,    
--   t1.Exclusions,   
--   (SELECT CASE WHEN t1.StudentGraduatedStatusCount=0 
--   AND 
--		t3.StatusCodeId IN --Student should be in a InSchool Status
--		(
--			(SELECT DISTINCT t1.StatusCodeId FROM syStatusCodes t1,dbo.sySysStatus t2 
--			WHERE t1.SysStatusId=t2.SysStatusId AND t2.InSchool=1)
--		)
--   THEN -- Is Student Not Graduated?    
--      CASE WHEN t1.ClockHourProgramCount>=1 THEN -- Is the program a clock hour program?    
--       -- Is the Program greater than 2 year and less than 4 years    
--       CASE WHEN (Select (Weeks/52) From arPrgVersions PVI Where PVI.PrgVerId=t3.PrgVerId) >=5     
--       THEN 1 ELSE 0 END     
--      ELSE     
--       CASE WHEN ((Select (Weeks/52) From arPrgVersions PVI Where PVI.PrgVerId=t3.PrgVerId)>=5)    
--       THEN 1 ELSE 0 END     
--      END    
--    ELSE 0 END    
--   ) AS StillInProg5YrsOrLonger,    
--   t4.IPEDSSequence AS GenderSequence,    
--   t2.IPEDSSequence AS RaceSequence,    
--   t1.StudentId AS StudentId,t3.StuEnrollId AS StuEnrollId    
--  FROM    
--   (SELECT * FROM adGenders WHERE IPEDSValue IN (30,31)) t4 LEFT JOIN #StudentsList t1 ON t4.GenderId=t1.GenderId    
--   LEFT JOIN adEthCodes t2 ON t2.EthCodeId=t1.RaceId    
--   INNER JOIN arStuEnrollments t3 ON t1.StudentId=t3.StudentId  
--   AND t1.Citizenship_IPEDSValue<>65
--   WHERE 
--	t2.EthCodeDescrip <> 'Nonresident Alien'     
--) dt     
--Order by     
--  GenderSequence, RaceSequence,    
--  Case when @OrderBy='SSN' Then dt.SSN end,    
--  Case when @OrderBy='LastName' Then dt.StudentName end,    
--  Case When @OrderBy='StudentNumber' Then Convert(int,dt.StudentNumber) END   

    INSERT  INTO #GraduationRate
            SELECT  NEWID() AS RowNumber
                   ,dbo.UDF_FormatSSN(SSN) AS SSN
                   ,StudentNumber
                   ,StudentName
                   ,GenderDescription
                   ,RaceDescription
                   ,CASE WHEN CopmPrg150Less2Yrs >= 1 THEN 'X'
                         ELSE ''
                    END AS CopmPrg150Less2Yrs
                   ,CASE WHEN CopmPrg1502YrsLess4Yrs >= 1 THEN 'X'
                         ELSE ''
                    END AS CopmPrg1502YrsLess4Yrs
                   ,CASE WHEN (
                                CopmPrg150Less2Yrs >= 1
                                OR CopmPrg1502YrsLess4Yrs >= 1
                              ) THEN 'X'
                         ELSE CASE WHEN CopmPrg150BachelorOrEquivalent >= 1 THEN 'X'
                                   ELSE ''
                              END
                    END AS CopmPrg150BachelorOrEquivalent
                   ,CASE WHEN CopmPrg4YrsOrLess >= 1 THEN 'X'
                         ELSE ''
                    END AS CopmPrg4YrsOrLess
                   ,CASE WHEN CopmPrg5Yrs >= 1 THEN 'X'
                         ELSE ''
                    END AS CopmPrg5Yrs
                   ,CASE WHEN (
                                CopmPrg150Less2Yrs >= 1
                                OR CopmPrg1502YrsLess4Yrs >= 1
                                OR CopmPrg150BachelorOrEquivalent >= 1
                                OR CopmPrg150BachelorOrEquivalent >= 1
                                OR CopmPrg4YrsOrLess >= 1
                                OR CopmPrg5Yrs >= 1
                              ) THEN ''
                         ELSE CASE WHEN TransferredOut >= 1 THEN 'X'
                                   ELSE ''
                              END
                    END AS TransOut
                   ,CASE WHEN Exclusions >= 1 THEN 'X'
                         ELSE ''
                    END AS Exclusions
                   ,CASE WHEN StillInProg5YrsOrLonger >= 1 THEN 'X'
                         ELSE ''
                    END AS StillInProg5YrsOrLonger
                   ,CASE WHEN CopmPrg150Less2Yrs >= 1 THEN 1
                         ELSE 0
                    END AS CopmPrg150Less2YrsCount
                   ,CASE WHEN CopmPrg1502YrsLess4Yrs >= 1 THEN 1
                         ELSE 0
                    END AS CopmPrg1502YrsLess4YrsCount
                   ,CASE WHEN (
                                CopmPrg150Less2Yrs >= 1
                                OR CopmPrg1502YrsLess4Yrs >= 1
                              ) THEN 1
                         ELSE CASE WHEN CopmPrg150BachelorOrEquivalent >= 1 THEN 1
                                   ELSE 0
                              END
                    END AS CopmPrg150BachelorOrEquivalentCount
                   ,CASE WHEN CopmPrg4YrsOrLess >= 1 THEN 1
                         ELSE 0
                    END AS CopmPrg4YrsOrLessCount
                   ,CASE WHEN CopmPrg5Yrs >= 1 THEN 1
                         ELSE 0
                    END AS CopmPrg5YrsCount
                   ,CASE WHEN (
                                CopmPrg150Less2Yrs >= 1
                                OR CopmPrg1502YrsLess4Yrs >= 1
                                OR CopmPrg150BachelorOrEquivalent >= 1
                                OR CopmPrg150BachelorOrEquivalent >= 1
                                OR CopmPrg4YrsOrLess >= 1
                                OR CopmPrg5Yrs >= 1
                              ) THEN 0
                         ELSE CASE WHEN TransferredOut >= 1 THEN 1
                                   ELSE 0
                              END
                    END AS TransOutCount
                   ,CASE WHEN Exclusions >= 1 THEN 1
                         ELSE 0
                    END AS ExclusionsCount
                   ,CASE WHEN StillInProg5YrsOrLonger >= 1 THEN 1
                         ELSE 0
                    END AS StillInProg5YrsOrLongerCount
                   ,GenderSequence
                   ,RaceSequence
                   ,StudentId
                   ,StuEnrollId
            FROM    (
                      -- If none of the students are mapped to non-citizen, then insert a row for 'Nonresident Alien'   
 -- Gender : Men and Women  
                      SELECT    NULL AS SSN
                               ,NULL AS StudentNumber
                               ,NULL AS StudentName
                               ,(
                                  SELECT DISTINCT
                                            AgencyDescrip
                                  FROM      syRptAgencyFldValues
                                  WHERE     RptAgencyFldValId = t2.IPEDSValue
                                ) AS GenderDescription
                               ,'Nonresident Alien' AS RaceDescription
                               ,0 AS CopmPrg150Less2Yrs
                               ,0 AS CopmPrg1502YrsLess4Yrs
                               ,0 AS CopmPrg150BachelorOrEquivalent
                               ,0 AS CopmPrg4YrsOrLess
                               ,0 AS CopmPrg5Yrs
                               ,0 AS TransferredOut
                               ,0 AS Exclusions
                               ,0 AS StillInProg5YrsOrLonger
                               ,t2.IPEDSSequence AS GenderSequence
                               ,1 AS RaceSequence
                               ,NULL AS StudentId
                               ,NULL AS StuEnrollId
                      FROM      (
                                  SELECT    *
                                  FROM      adGenders
                                  WHERE     IPEDSValue IN ( 30,31 )
                                ) t2
                      WHERE     (
                                  --Check if there are any men in the student list whose citizenship is set to non citizen  
                                  SELECT    COUNT(t1.FirstName)
                                  FROM      #StudentsList t1
                                  INNER JOIN adCitizenships t11 ON t1.CitizenId = t11.CitizenshipId
                                  WHERE     t11.IPEDSValue = 65
                                            AND t1.GenderId = t2.GenderId -- Non-Citizen  
                                ) = 0
                      UNION   
 -- Bring in Students whose Citizenship Type is set to non citizen   
 -- Gender : Men and Women  
                      SELECT    t1.SSN
                               ,t1.StudentNumber
                               ,t1.LastName + ', ' + t1.FirstName + ' ' + ISNULL(t1.MiddleName,'') AS StudentName
                               ,(
                                  SELECT DISTINCT
                                            AgencyDescrip
                                  FROM      syRptAgencyFldValues
                                  WHERE     RptAgencyFldValId = t4.IPEDSValue
                                ) AS GenderDescription
                               ,'Nonresident Alien' AS Race
                               ,(
                                  SELECT    CASE WHEN t1.StudentGraduatedStatusCount >= 1 THEN -- Is Student Graduated?  
                                                      CASE WHEN t1.ClockHourProgramCount >= 1 THEN -- Is the program a clock hour program?  
                                                                CASE WHEN (
                                                                            SELECT  ( Weeks / 52 )
                                                                            FROM    arPrgVersions PVI
                                                                            WHERE   PVI.PrgVerId = t3.PrgVerId
                                                                          ) < 2 THEN -- Is the Program a 2 year program  
         --Did the student graduate within 150% (1.5) of the Program length in hours  
                                                                          CASE WHEN ( (
                                                                                        SELECT  ISNULL(SUM(SchedHours),0)
                                                                                        FROM    arStudentClockAttendance
                                                                                        WHERE   StuEnrollId = t3.StuEnrollId
                                                                                      ) <= (
                                                                                             SELECT ( Hours * 1.5 )
                                                                                             FROM   arPrgVersions PVI
                                                                                             WHERE  PVI.PrgVerId = t3.PrgVerId
                                                                                           ) ) THEN 1
                                                                               ELSE 0
                                                                          END
                                                                     ELSE 0
                                                                END
                                                           ELSE CASE WHEN (
                                                                            SELECT  ( Weeks / 52 )
                                                                            FROM    arPrgVersions PVI
                                                                            WHERE   PVI.PrgVerId = t3.PrgVerId
                                                                          ) < 2 THEN -- Is the Program a 2 year program  
        -- Did the student graduate within 150% (1.5) of the Weeks in Program in weeks  
                                                                          CASE WHEN ( (
                                                                                        SELECT  DATEDIFF(DAY,t3.StartDate,t3.ExpGradDate)
                                                                                      ) <= (
                                                                                             SELECT ( Weeks * 7 * 1.5 )
                                                                                             FROM   arPrgVersions PVI
                                                                                             WHERE  PVI.PrgVerId = t3.PrgVerId
                                                                                           ) ) THEN 1
                                                                               ELSE 0
                                                                          END
                                                                     ELSE 0
                                                                END
                                                      END
                                                 ELSE 0
                                            END
                                ) AS CopmPrg150Less2Yrs
                               ,(
                                  SELECT    CASE WHEN t1.StudentGraduatedStatusCount >= 1 THEN -- Is Student Graduated?  
                                                      CASE WHEN t1.ClockHourProgramCount >= 1 THEN -- Is the program a clock hour program?  
       -- Is the Program greater than 2 year and less than 4 years  
                                                                CASE WHEN (
                                                                            SELECT  ( Weeks / 52 )
                                                                            FROM    arPrgVersions PVI
                                                                            WHERE   PVI.PrgVerId = t3.PrgVerId
                                                                          ) >= 2
                                                                          AND (
                                                                                SELECT  ( Weeks / 52 )
                                                                                FROM    arPrgVersions PVI
                                                                                WHERE   PVI.PrgVerId = t3.PrgVerId
                                                                              ) < 4 THEN   
        -- Did the student graduate within 150% (1.5) of the Program length in hours  
                                                                          CASE WHEN ( (
                                                                                        SELECT  ISNULL(SUM(SchedHours),0)
                                                                                        FROM    arStudentClockAttendance
                                                                                        WHERE   StuEnrollId = t3.StuEnrollId
                                                                                      ) <= (
                                                                                             SELECT ( Hours * 1.5 )
                                                                                             FROM   arPrgVersions PVI
                                                                                             WHERE  PVI.PrgVerId = t3.PrgVerId
                                                                                           ) ) THEN 1
                                                                               ELSE 0
                                                                          END
                                                                     ELSE 0
                                                                END
                                                           ELSE   
       -- Is the Program greater than 2 year and less than 4 years  
                                                                CASE WHEN (
                                                                            SELECT  ( Weeks / 52 )
                                                                            FROM    arPrgVersions PVI
                                                                            WHERE   PVI.PrgVerId = t3.PrgVerId
                                                                          ) >= 2
                                                                          AND (
                                                                                SELECT  ( Weeks / 52 )
                                                                                FROM    arPrgVersions PVI
                                                                                WHERE   PVI.PrgVerId = t3.PrgVerId
                                                                              ) < 4 THEN   
        -- Did the student graduate within 150% (1.5) of the Weeks in Program in weeks  
                                                                          CASE WHEN ( (
                                                                                        SELECT  DATEDIFF(DAY,t3.StartDate,t3.ExpGradDate)
                                                                                      ) <= (
                                                                                             SELECT ( Weeks * 7 * 1.5 )
                                                                                             FROM   arPrgVersions PVI
                                                                                             WHERE  PVI.PrgVerId = t3.PrgVerId
                                                                                           ) ) THEN 1
                                                                               ELSE 0
                                                                          END
                                                                     ELSE 0
                                                                END
                                                      END
                                                 ELSE 0
                                            END
                                ) AS CopmPrg1502YrsLess4Yrs
                               ,(
                                  SELECT    CASE WHEN t1.StudentGraduatedStatusCount >= 1 THEN -- Is Student Graduated?  
                                                      CASE WHEN t1.ClockHourProgramCount >= 1 THEN -- Is the program a clock hour program?  
        -- Did the student graduate within 150% (1.5) of the Program length in hours  
                                                                CASE WHEN ( (
                                                                              SELECT    ISNULL(SUM(SchedHours),0)
                                                                              FROM      arStudentClockAttendance
                                                                              WHERE     StuEnrollId = t3.StuEnrollId
                                                                            ) <= (
                                                                                   SELECT   ( Hours * 1.5 )
                                                                                   FROM     arPrgVersions PVI
                                                                                   INNER JOIN arDegrees D ON PVI.DegreeId = D.DegreeId
                                                                                   WHERE    PVI.PrgVerId = t3.PrgVerId
                                                                                            AND (
                                                                                                  D.IPEDSValue = 143
                                                                                                  OR D.IPEDSValue IS NULL
                                                                                                )
                                                                                 ) ) THEN 1
                                                                     ELSE 0
                                                                END   
       -- ELSE 0 END  
                                                           ELSE   
        -- Did the student graduate within 150% (1.5) of the Weeks in Program in weeks          
                                                                CASE WHEN ( (
                                                                              SELECT    DATEDIFF(DAY,t3.StartDate,t3.ExpGradDate)
                                                                            ) <= (
                                                                                   SELECT   ( Weeks * 7 * 1.5 )
                                                                                   FROM     arPrgVersions PVI
                                                                                   INNER JOIN arDegrees D ON PVI.DegreeId = D.DegreeId
                                                                                   WHERE    PVI.PrgVerId = t3.PrgVerId
                                                                                            AND (
                                                                                                  D.IPEDSValue = 143
                                                                                                  OR D.IPEDSValue IS NULL
                                                                                                )
                                                                                 ) ) THEN 1
                                                                     ELSE 0
                                                                END
                                                      END
                                                 ELSE 0
                                            END
                                ) AS CopmPrg150BachelorOrEquivalent
                               ,(
                                  SELECT    CASE WHEN t1.StudentGraduatedStatusCount = 1 THEN -- Is Student Not Graduated?  
                                                      CASE WHEN t1.ClockHourProgramCount >= 1 THEN -- Is the program a clock hour program?  
       -- Is the Program less than 4 years  
       --CASE WHEN (Select (Weeks/52) From arPrgVersions PVI Where PVI.PrgVerId=t3.PrgVerId) <=4   
       --THEN 
                                                                CASE WHEN ( (
                                                                              SELECT    ISNULL(SUM(SchedHours),0)
                                                                              FROM      arStudentClockAttendance
                                                                              WHERE     StuEnrollId = t3.StuEnrollId
                                                                            ) <= (
                                                                                   SELECT   ( Hours )
                                                                                   FROM     arPrgVersions PVI
                                                                                   INNER JOIN arDegrees D ON PVI.DegreeId = D.DegreeId
                                                                                   WHERE    PVI.PrgVerId = t3.PrgVerId
                                                                                            AND (
                                                                                                  D.IPEDSValue = 143
                                                                                                  OR D.IPEDSValue IS NULL
                                                                                                )
                                                                                 ) ) THEN 1
                                                                     ELSE 0
                                                                END   
      -- ELSE 0 END   
                                                           ELSE   
		--CASE WHEN (Select (Weeks/52) From arPrgVersions PVI Where PVI.PrgVerId=t3.PrgVerId) <=4   
  --      THEN 
                                                                CASE WHEN ( (
                                                                              SELECT    DATEDIFF(ww,t3.StartDate,t3.ExpGradDate)
                                                                            ) <= 208 ) THEN 1
                                                                     ELSE 0
                                                                END
	    --ELSE 0 END   
                                                      END
                                                 ELSE 0
                                            END
                                ) CopmPrg4YrsOrLess
                               ,(
                                  SELECT    CASE WHEN t1.StudentGraduatedStatusCount = 1 THEN -- Is Student Not Graduated?  
                                                      CASE WHEN t1.ClockHourProgramCount >= 1 THEN -- Is the program a clock hour program?  
       -- Is the Program less than 5 years  
                                                                CASE WHEN (
                                                                            SELECT  ( Weeks / 52 )
                                                                            FROM    arPrgVersions PVI
                                                                            WHERE   PVI.PrgVerId = t3.PrgVerId
                                                                          ) > 4 THEN CASE WHEN ( (
                                                                                                   SELECT   ISNULL(SUM(SchedHours),0)
                                                                                                   FROM     arStudentClockAttendance
                                                                                                   WHERE    StuEnrollId = t3.StuEnrollId
                                                                                                 ) <= (
                                                                                                        SELECT  ( Hours )
                                                                                                        FROM    arPrgVersions PVI
                                                                                                        INNER JOIN arDegrees D ON PVI.DegreeId = D.DegreeId
                                                                                                        WHERE   PVI.PrgVerId = t3.PrgVerId
                                                                                                                AND (
                                                                                                                      D.IPEDSValue = 143
                                                                                                                      OR D.IPEDSValue IS NULL
                                                                                                                    )
                                                                                                      ) ) THEN 1
                                                                                          ELSE 0
                                                                                     END
                                                                     ELSE 0
                                                                END
                                                           ELSE   
       --CASE WHEN ((Select (Weeks/52) From arPrgVersions PVI Where PVI.PrgVerId=t3.PrgVerId)>4)  
       --THEN 
			-- 260 (52 Weeks * 5 Years)
                                                                CASE WHEN (
                                                                            (
                                                                              SELECT    DATEDIFF(ww,t3.StartDate,t3.ExpGradDate)
                                                                            ) > 208
                                                                            AND (
                                                                                  SELECT    DATEDIFF(ww,t3.StartDate,t3.ExpGradDate)
                                                                                ) <= 260
                                                                          ) THEN 1
                                                                     ELSE 0
                                                                END
       --ELSE 0 END   
                                                      END
                                                 ELSE 0
                                            END
                                ) CopmPrg5Yrs
                               ,t1.TransferredOut
                               ,t1.Exclusions
                               ,(
                                  SELECT    CASE WHEN t1.StudentGraduatedStatusCount = 0
                                                      AND t3.StatusCodeId IN --Student should be in a InSchool Status
		( (
            SELECT DISTINCT
                    t1.StatusCodeId
            FROM    syStatusCodes t1
                   ,dbo.sySysStatus t2
            WHERE   t1.SysStatusId = t2.SysStatusId
                    AND t2.InSchool = 1
          )
                                                                          ) THEN -- Is Student Not Graduated?  
                                                      CASE WHEN t1.ClockHourProgramCount >= 1 THEN -- Is the program a clock hour program?  
       -- Is the Program greater than 2 year and less than 4 years  
                                                                CASE WHEN (
                                                                            SELECT  ( Weeks / 52 )
                                                                            FROM    arPrgVersions PVI
                                                                            WHERE   PVI.PrgVerId = t3.PrgVerId
                                                                          ) >= 5 THEN 1
                                                                     ELSE 0
                                                                END
                                                           ELSE CASE WHEN ( (
                                                                              SELECT    ( Weeks / 52 )
                                                                              FROM      arPrgVersions PVI
                                                                              WHERE     PVI.PrgVerId = t3.PrgVerId
                                                                            ) >= 5 ) THEN 1
                                                                     ELSE 0
                                                                END
                                                      END
                                                 ELSE 0
                                            END
                                ) AS StillInProg5YrsOrLonger
                               ,t4.IPEDSSequence AS GenderSequence
                               ,1 AS RaceSequence
                               ,t1.StudentId AS StudentId
                               ,t1.StuEnrollId
                      FROM      (
                                  SELECT    *
                                  FROM      adGenders
                                  WHERE     IPEDSValue IN ( 30,31 )
                                ) t4
                      LEFT JOIN #StudentsList t1 ON t4.GenderId = t1.GenderId
                      INNER JOIN adCitizenships t11 ON t1.CitizenId = t11.CitizenshipId
                      INNER JOIN arStuEnrollments t3 ON t1.StuEnrollId = t3.StuEnrollId
                      WHERE     t11.IPEDSValue = 65 --Non-Citizen  
                      UNION  
 -- Get List of Races that is not tied to any students (Full Time, First Time, UnderGraduate Students)  
 -- Gender : Men  
                      SELECT    NULL AS SSN
                               ,NULL AS StudentNumber
                               ,NULL AS StudentName
                               ,'Men' AS GenderDescription
                               ,(
                                  SELECT DISTINCT
                                            AgencyDescrip
                                  FROM      syRptAgencyFldValues
                                  WHERE     RptAgencyFldValId = IPEDSValue
                                ) AS Race
                               ,0 AS CopmPrg150Less2Yrs
                               ,0 AS CopmPrg1502YrsLess4Yrs
                               ,0 AS CopmPrg150BachelorOrEquivalent
                               ,0 AS CopmPrg4YrsOrLess
                               ,0 AS CopmPrg5Yrs
                               ,0 AS TransferredOut
                               ,0 AS Exclusions
                               ,0 AS StillInProg5YrsOrLonger
                               ,1 AS GenderSequence
                               ,IPEDSSequence AS RaceSequence
                               ,NULL AS StudentId
                               ,NULL AS StuEnrollId
                      FROM      adEthCodes
                      WHERE     EthCodeDescrip <> 'Nonresident Alien'
                                AND EthCodeId NOT IN ( SELECT DISTINCT
                                                                RaceId
                                                       FROM     #StudentsList t1
                                                       INNER JOIN adGenders t2 ON t1.GenderId = t2.GenderId
                                                                                  AND t2.IPEDSValue = 30 )
                      UNION  
 -- Get List of Races that is not tied to any students (Full Time, First Time, UnderGraduate Students)  
 -- Gender : Women  
                      SELECT    NULL AS SSN
                               ,NULL AS StudentNumber
                               ,NULL AS StudentName
                               ,'Women' AS GenderDescription
                               ,(
                                  SELECT DISTINCT
                                            AgencyDescrip
                                  FROM      syRptAgencyFldValues
                                  WHERE     RptAgencyFldValId = IPEDSValue
                                ) AS Race
                               ,0 AS CopmPrg150Less2Yrs
                               ,0 AS CopmPrg1502YrsLess4Yrs
                               ,0 AS CopmPrg150BachelorOrEquivalent
                               ,0 AS CopmPrg4YrsOrLess
                               ,0 AS CopmPrg5Yrs
                               ,0 AS TransferredOut
                               ,0 AS Exclusions
                               ,0 AS StillInProg5YrsOrLonger
                               ,2 AS GenderSequence
                               ,IPEDSSequence AS RaceSequence
                               ,NULL AS StudentId
                               ,NULL AS StuEnrollId
                      FROM      adEthCodes
                      WHERE     EthCodeDescrip <> 'Nonresident Alien'
                                AND EthCodeId NOT IN ( SELECT DISTINCT
                                                                RaceId
                                                       FROM     #StudentsList t1
                                                       INNER JOIN adGenders t2 ON t1.GenderId = t2.GenderId
                                                                                  AND t2.IPEDSValue = 31 )
                      UNION  
  -- Get Students who belongs to a Race  
  -- Gender : Men and Women  
                      SELECT    t1.SSN
                               ,t1.StudentNumber
                               ,t1.LastName + ', ' + t1.FirstName + ' ' + ISNULL(t1.MiddleName,'') AS StudentName
                               ,  
   --t1.GenderDescription AS GenderDescription,  
                                (
                                  SELECT DISTINCT
                                            AgencyDescrip
                                  FROM      syRptAgencyFldValues
                                  WHERE     RptAgencyFldValId = t4.IPEDSValue
                                ) AS GenderDescription
                               ,(
                                  SELECT DISTINCT
                                            AgencyDescrip
                                  FROM      syRptAgencyFldValues
                                  WHERE     RptAgencyFldValId = t2.IPEDSValue
                                ) AS RaceDescription
                               ,(
                                  SELECT    CASE WHEN t1.StudentGraduatedStatusCount >= 1 THEN -- Is Student Graduated?  
                                                      CASE WHEN t1.ClockHourProgramCount >= 1 THEN -- Is the program a clock hour program?  
                                                                CASE WHEN (
                                                                            SELECT  ( Weeks / 52 )
                                                                            FROM    arPrgVersions PVI
                                                                            WHERE   PVI.PrgVerId = t3.PrgVerId
                                                                          ) < 2 THEN -- Is the Program a 2 year program  
         --Did the student graduate within 150% (1.5) of the Program length in hours  
                                                                          CASE WHEN ( (
                                                                                        SELECT  ISNULL(SUM(SchedHours),0)
                                                                                        FROM    arStudentClockAttendance
                                                                                        WHERE   StuEnrollId = t3.StuEnrollId
                                                                                      ) <= (
                                                                                             SELECT ( Hours * 1.5 )
                                                                                             FROM   arPrgVersions PVI
                                                                                             WHERE  PVI.PrgVerId = t3.PrgVerId
                                                                                           ) ) THEN 1
                                                                               ELSE 0
                                                                          END
                                                                     ELSE 0
                                                                END
                                                           ELSE CASE WHEN (
                                                                            SELECT  ( Weeks / 52 )
                                                                            FROM    arPrgVersions PVI
                                                                            WHERE   PVI.PrgVerId = t3.PrgVerId
                                                                          ) < 2 THEN -- Is the Program a 2 year program  
        -- Did the student graduate within 150% (1.5) of the Weeks in Program in weeks  
                                                                          CASE WHEN ( (
                                                                                        SELECT  DATEDIFF(DAY,t3.StartDate,t3.ExpGradDate)
                                                                                      ) <= (
                                                                                             SELECT ( Weeks * 7 * 1.5 )
                                                                                             FROM   arPrgVersions PVI
                                                                                             WHERE  PVI.PrgVerId = t3.PrgVerId
                                                                                           ) ) THEN 1
                                                                               ELSE 0
                                                                          END
                                                                     ELSE 0
                                                                END
                                                      END
                                                 ELSE 0
                                            END
                                ) AS CopmPrg150Less2Yrs
                               ,(
                                  SELECT    CASE WHEN t1.StudentGraduatedStatusCount >= 1 THEN -- Is Student Graduated?  
                                                      CASE WHEN t1.ClockHourProgramCount >= 1 THEN -- Is the program a clock hour program?  
       -- Is the Program greater than 2 year and less than 4 years  
                                                                CASE WHEN (
                                                                            SELECT  ( Weeks / 52 )
                                                                            FROM    arPrgVersions PVI
                                                                            WHERE   PVI.PrgVerId = t3.PrgVerId
                                                                          ) >= 2
                                                                          AND (
                                                                                SELECT  ( Weeks / 52 )
                                                                                FROM    arPrgVersions PVI
                                                                                WHERE   PVI.PrgVerId = t3.PrgVerId
                                                                              ) < 4 THEN   
        -- Did the student graduate within 150% (1.5) of the Program length in hours  
                                                                          CASE WHEN ( (
                                                                                        SELECT  ISNULL(SUM(SchedHours),0)
                                                                                        FROM    arStudentClockAttendance
                                                                                        WHERE   StuEnrollId = t3.StuEnrollId
                                                                                      ) <= (
                                                                                             SELECT ( Hours * 1.5 )
                                                                                             FROM   arPrgVersions PVI
                                                                                             WHERE  PVI.PrgVerId = t3.PrgVerId
                                                                                           ) ) THEN 1
                                                                               ELSE 0
                                                                          END
                                                                     ELSE 0
                                                                END
                                                           ELSE   
       -- Is the Program greater than 2 year and less than 4 years  
                                                                CASE WHEN (
                                                                            SELECT  ( Weeks / 52 )
                                                                            FROM    arPrgVersions PVI
                                                                            WHERE   PVI.PrgVerId = t3.PrgVerId
                                                                          ) >= 2
                                                                          AND (
                                                                                SELECT  ( Weeks / 52 )
                                                                                FROM    arPrgVersions PVI
                                                                                WHERE   PVI.PrgVerId = t3.PrgVerId
                                                                              ) < 4 THEN   
        -- Did the student graduate within 150% (1.5) of the Weeks in Program in weeks  
                                                                          CASE WHEN ( (
                                                                                        SELECT  DATEDIFF(DAY,t3.StartDate,t3.ExpGradDate)
                                                                                      ) <= (
                                                                                             SELECT ( Weeks * 7 * 1.5 )
                                                                                             FROM   arPrgVersions PVI
                                                                                             WHERE  PVI.PrgVerId = t3.PrgVerId
                                                                                           ) ) THEN 1
                                                                               ELSE 0
                                                                          END
                                                                     ELSE 0
                                                                END
                                                      END
                                                 ELSE 0
                                            END
                                ) AS CopmPrg1502YrsLess4Yrs
                               ,(
                                  SELECT    CASE WHEN t1.StudentGraduatedStatusCount >= 1 THEN -- Is Student Graduated?  
                                                      CASE WHEN t1.ClockHourProgramCount >= 1 THEN -- Is the program a clock hour program?  
        -- Did the student graduate within 150% (1.5) of the Program length in hours  
                                                                CASE WHEN ( (
                                                                              SELECT    ISNULL(SUM(SchedHours),0)
                                                                              FROM      arStudentClockAttendance
                                                                              WHERE     StuEnrollId = t3.StuEnrollId
                                                                            ) <= (
                                                                                   SELECT   ( Hours * 1.5 )
                                                                                   FROM     arPrgVersions PVI
                                                                                   INNER JOIN arDegrees D ON PVI.DegreeId = D.DegreeId
                                                                                   WHERE    PVI.PrgVerId = t3.PrgVerId
                                                                                            AND (
                                                                                                  D.IPEDSValue = 143
                                                                                                  OR D.IPEDSValue IS NULL
                                                                                                )
                                                                                 ) ) THEN 1
                                                                     ELSE 0
                                                                END   
      -- ELSE 0 END  
                                                           ELSE   
        -- Did the student graduate within 150% (1.5) of the Weeks in Program in weeks          
                                                                CASE WHEN ( (
                                                                              SELECT    DATEDIFF(DAY,t3.StartDate,t3.ExpGradDate)
                                                                            ) <= (
                                                                                   SELECT   ( Weeks * 7 * 1.5 )
                                                                                   FROM     arPrgVersions PVI
                                                                                   INNER JOIN arDegrees D ON PVI.DegreeId = D.DegreeId
                                                                                   WHERE    PVI.PrgVerId = t3.PrgVerId
                                                                                            AND (
                                                                                                  D.IPEDSValue = 143
                                                                                                  OR D.IPEDSValue IS NULL
                                                                                                )
                                                                                 ) ) THEN 1
                                                                     ELSE 0
                                                                END
                                                      END
                                                 ELSE 0
                                            END
                                ) AS CopmPrg150BachelorOrEquivalent
                               ,(
                                  SELECT    CASE WHEN t1.StudentGraduatedStatusCount = 1 THEN -- Is Student Not Graduated?  
                                                      CASE WHEN t1.ClockHourProgramCount >= 1 THEN -- Is the program a clock hour program?  
       -- Is the Program less than 4 years  
       --CASE WHEN (Select (Weeks/52) From arPrgVersions PVI Where PVI.PrgVerId=t3.PrgVerId) <=4   
       --THEN 
                                                                CASE WHEN ( (
                                                                              SELECT    ISNULL(SUM(SchedHours),0)
                                                                              FROM      arStudentClockAttendance
                                                                              WHERE     StuEnrollId = t3.StuEnrollId
                                                                            ) <= (
                                                                                   SELECT   ( Hours )
                                                                                   FROM     arPrgVersions PVI
                                                                                   INNER JOIN arDegrees D ON PVI.DegreeId = D.DegreeId
                                                                                   WHERE    PVI.PrgVerId = t3.PrgVerId
                                                                                            AND (
                                                                                                  D.IPEDSValue = 143
                                                                                                  OR D.IPEDSValue IS NULL
                                                                                                )
                                                                                 ) ) THEN 1
                                                                     ELSE 0
                                                                END   
       --ELSE 0 END   
                                                           ELSE  
		--CASE WHEN (Select (Weeks/52) From arPrgVersions PVI Where PVI.PrgVerId=t3.PrgVerId) <=4   
  --      THEN 
                                                                CASE WHEN ( (
                                                                              SELECT    DATEDIFF(ww,t3.StartDate,t3.ExpGradDate)
                                                                            ) <= 208 ) THEN 1
                                                                     ELSE 0
                                                                END
	    --ELSE 0 END 
                                                      END
                                                 ELSE 0
                                            END
                                ) CopmPrg4YrsOrLess
                               ,(
                                  SELECT    CASE WHEN t1.StudentGraduatedStatusCount = 1 THEN -- Is Student Not Graduated?  
                                                      CASE WHEN t1.ClockHourProgramCount >= 1 THEN -- Is the program a clock hour program?  
       -- Is the Program less than 5 years  
       --CASE WHEN (Select (Weeks/52) From arPrgVersions PVI Where PVI.PrgVerId=t3.PrgVerId)>4   
       --THEN 
                                                                CASE WHEN ( (
                                                                              SELECT    ISNULL(SUM(SchedHours),0)
                                                                              FROM      arStudentClockAttendance
                                                                              WHERE     StuEnrollId = t3.StuEnrollId
                                                                            ) <= (
                                                                                   SELECT   ( Hours )
                                                                                   FROM     arPrgVersions PVI
                                                                                   INNER JOIN arDegrees D ON PVI.DegreeId = D.DegreeId
                                                                                   WHERE    PVI.PrgVerId = t3.PrgVerId
                                                                                            AND (
                                                                                                  D.IPEDSValue = 143
                                                                                                  OR D.IPEDSValue IS NULL
                                                                                                )
                                                                                 ) ) THEN 1
                                                                     ELSE 0
                                                                END   
       --ELSE 0 END   
                                                           ELSE   
       --CASE WHEN ((Select (Weeks/52) From arPrgVersions PVI Where PVI.PrgVerId=t3.PrgVerId)>4)  
       --THEN 
			-- 260 (52 Weeks * 5 Years)
                                                                CASE WHEN (
                                                                            (
                                                                              SELECT    DATEDIFF(ww,t3.StartDate,t3.ExpGradDate)
                                                                            ) > 208
                                                                            AND (
                                                                                  SELECT    DATEDIFF(ww,t3.StartDate,t3.ExpGradDate)
                                                                                ) <= 260
                                                                          ) THEN 1
                                                                     ELSE 0
                                                                END
       --ELSE 0 END   
                                                      END
                                                 ELSE 0
                                            END
                                ) CopmPrg5Yrs
                               ,t1.TransferredOut
                               ,t1.Exclusions
                               ,(
                                  SELECT    CASE WHEN t1.StudentGraduatedStatusCount = 0
                                                      AND t3.StatusCodeId IN --Student should be in a InSchool Status
		( (
            SELECT DISTINCT
                    t1.StatusCodeId
            FROM    syStatusCodes t1
                   ,dbo.sySysStatus t2
            WHERE   t1.SysStatusId = t2.SysStatusId
                    AND t2.InSchool = 1
          )
                                                                          ) THEN -- Is Student Not Graduated?  
                                                      CASE WHEN t1.ClockHourProgramCount >= 1 THEN -- Is the program a clock hour program?  
       -- Is the Program greater than 2 year and less than 4 years  
                                                                CASE WHEN (
                                                                            SELECT  ( Weeks / 52 )
                                                                            FROM    arPrgVersions PVI
                                                                            WHERE   PVI.PrgVerId = t3.PrgVerId
                                                                          ) >= 5 THEN 1
                                                                     ELSE 0
                                                                END
                                                           ELSE CASE WHEN ( (
                                                                              SELECT    ( Weeks / 52 )
                                                                              FROM      arPrgVersions PVI
                                                                              WHERE     PVI.PrgVerId = t3.PrgVerId
                                                                            ) >= 5 ) THEN 1
                                                                     ELSE 0
                                                                END
                                                      END
                                                 ELSE 0
                                            END
                                ) AS StillInProg5YrsOrLonger
                               ,t4.IPEDSSequence AS GenderSequence
                               ,t2.IPEDSSequence AS RaceSequence
                               ,t1.StudentId AS StudentId
                               ,t1.StuEnrollId
                      FROM      (
                                  SELECT    *
                                  FROM      adGenders
                                  WHERE     IPEDSValue IN ( 30,31 )
                                ) t4
                      LEFT JOIN #StudentsList t1 ON t4.GenderId = t1.GenderId
                      LEFT JOIN adEthCodes t2 ON t2.EthCodeId = t1.RaceId
                      INNER JOIN arStuEnrollments t3 ON t1.StuEnrollId = t3.StuEnrollId
                                                        AND t1.CitizenShip_IPEDSValue <> 65
                      WHERE     t2.EthCodeDescrip <> 'Nonresident Alien'
                    ) dt
            ORDER BY GenderSequence
                   ,RaceSequence
                   ,CASE WHEN @OrderBy = 'SSN' THEN dt.SSN
                    END
                   ,CASE WHEN @OrderBy = 'LastName' THEN dt.StudentName
                    END
                   ,CASE WHEN @OrderBy = 'StudentNumber' THEN CONVERT(INT,dt.StudentNumber)
                    END;  
  
    CREATE TABLE #FinalStudentList
        (
         StudentId UNIQUEIDENTIFIER
        ,SSN VARCHAR(50)
        ,StudentNumber VARCHAR(50)
        ,StudentName VARCHAR(100)
        ,Gender VARCHAR(50)
        ,Race VARCHAR(50)
        ,CopmPrg150Less2Yrs VARCHAR(10)
        ,CopmPrg1502YrsLess4Yrs VARCHAR(10)
        ,CopmPrg150BachelorOrEquivalent VARCHAR(10)
        ,CopmPrg4YrsOrLess VARCHAR(10)
        ,CopmPrg5Yrs VARCHAR(10)
        ,TransOut VARCHAR(10)
        ,Exclusions VARCHAR(10)
        ,StillInProg5YrsOrLonger VARCHAR(10)
        ,CopmPrg150Less2YrsCount INT
        ,CopmPrg1502YrsLess4YrsCount INT
        ,CopmPrg150BachelorOrEquivalentCount INT
        ,CopmPrg4YrsOrLessCount INT
        ,CopmPrg5YrsCount INT
        ,TransOutCount INT
        ,ExclusionsCount INT
        ,StillInProg5YrsOrLongerCount INT
        ,GenderSequence INT
        ,RaceSequence INT
        );  


--INSERT INTO #FinalStudentList
--SELECT DISTINCT StudentId,SSN,StudentNumber,StudentName,Gender,Race,
--(CASE WHEN (SELECT COUNT(CopmPrg150Less2Yrs) FROM #GraduationRate WHERE StudentId=t2.StudentId AND CopmPrg150Less2Yrs='X')>=1 THEN 'X' else '' END) as CopmPrg150Less2Yrs, 
--(CASE WHEN (SELECT COUNT(CopmPrg1502YrsLess4Yrs) FROM #GraduationRate WHERE StudentId=t2.StudentId AND CopmPrg1502YrsLess4Yrs='X')>=1 THEN 'X' else '' END) as CopmPrg1502YrsLess4Yrs,
--(CASE WHEN (SELECT COUNT(CopmPrg150BachelorOrEquivalent) FROM #GraduationRate WHERE StudentId=t2.StudentId  AND CopmPrg150BachelorOrEquivalent='X')>=1 THEN 'X' else '' END) as CopmPrg150BachelorOrEquivalent,
--(CASE WHEN (SELECT COUNT(CopmPrg4YrsOrLess) FROM #GraduationRate WHERE StudentId=t2.StudentId AND CopmPrg4YrsOrLess='X')>=1 THEN 'X' else '' END) as CopmPrg4YrsOrLess,
--(CASE WHEN (SELECT COUNT(CopmPrg5Yrs) FROM #GraduationRate WHERE StudentId=t2.StudentId AND CopmPrg5Yrs='X')>=1 THEN 'X' else '' END) as CopmPrg5Yrs,
--(CASE WHEN (SELECT COUNT(TransOut) FROM #GraduationRate WHERE StudentId=t2.StudentId AND TransOut='X')>=1 THEN 'X' else '' END) as TransOut,
--(CASE WHEN (SELECT COUNT(Exclusions) FROM #GraduationRate WHERE StudentId=t2.StudentId AND Exclusions='X')>=1 THEN 'X' else '' END) as Exclusions,
--(CASE WHEN (SELECT COUNT(StillInProg5YrsOrLonger) FROM #GraduationRate WHERE StudentId=t2.StudentId AND StillInProg5YrsOrLonger='X')>=1 THEN 'X' else '' END) as StillInProg5YrsOrLonger,

--(CASE WHEN (SELECT COUNT(CopmPrg150Less2Yrs) FROM #GraduationRate WHERE StudentId=t2.StudentId AND CopmPrg150Less2Yrs='X')>=1 THEN 1 else 0 END) as CopmPrg150Less2YrsCount, 
--(CASE WHEN (SELECT COUNT(CopmPrg1502YrsLess4Yrs) FROM #GraduationRate WHERE StudentId=t2.StudentId AND CopmPrg1502YrsLess4Yrs='X')>=1 THEN 1 else 0 END) as CopmPrg1502YrsLess4YrsCount,
--(CASE WHEN (SELECT COUNT(CopmPrg150BachelorOrEquivalent) FROM #GraduationRate WHERE StudentId=t2.StudentId  AND CopmPrg150BachelorOrEquivalent='X')>=1 THEN 1 else 0 END) as CopmPrg150BachelorOrEquivalentCount,
--(CASE WHEN (SELECT COUNT(CopmPrg4YrsOrLess) FROM #GraduationRate WHERE StudentId=t2.StudentId AND CopmPrg4YrsOrLess='X')>=1 THEN 1 else 0 END) as CopmPrg4YrsOrLessCount,
--(CASE WHEN (SELECT COUNT(CopmPrg5Yrs) FROM #GraduationRate WHERE StudentId=t2.StudentId AND CopmPrg5Yrs='X')>=1 THEN 1 else 0 END) as CopmPrg5YrsCount,
--(CASE WHEN (SELECT COUNT(TransOut) FROM #GraduationRate WHERE StudentId=t2.StudentId AND TransOut='X')>=1 THEN 1 else 0 END) as TransOutCount,
--(CASE WHEN (SELECT COUNT(Exclusions) FROM #GraduationRate WHERE StudentId=t2.StudentId AND Exclusions='X')>=1 THEN 1 else 0 END) as Exclusions,
--(CASE WHEN (SELECT COUNT(StillInProg5YrsOrLonger) FROM #GraduationRate WHERE StudentId=t2.StudentId AND StillInProg5YrsOrLonger='X')>=1 THEN 1 else 0 END) as StillInProg5YrsOrLonger,
--GenderSequence,RaceSequence
--FROM #GraduationRate t2 
      
    SELECT  Race
           ,Gender
           ,GenderSequence
           ,RaceSequence
           ,SUM(CopmPrg150Less2YrsCount) AS CopmPrg150Less2YrsCount
           ,SUM(CopmPrg1502YrsLess4YrsCount) AS CopmPrg1502YrsLess4YrsCount
           ,SUM(CopmPrg150BachelorOrEquivalentCount) AS CopmPrg150BachelorOrEquivalentCount
           ,SUM(CopmPrg4YrsOrLessCount) AS CopmPrg4YrsOrLessCount
           ,SUM(CopmPrg5YrsCount) AS CopmPrg5YrsCount
           ,SUM(TransOutCount) AS TransOutCount
           ,SUM(ExclusionsCount) AS ExclusionsCount
           ,SUM(StillInProg5YrsOrLongerCount) AS StillInProg5YrsOrLongerCount
    FROM    #GraduationRate
    GROUP BY GenderSequence
           ,RaceSequence
           ,Gender
           ,Race
    ORDER BY GenderSequence
           ,RaceSequence
           ,Gender
           ,Race;   
  
    DROP TABLE #GraduationRate; 
    DROP TABLE #FinalStudentList;   
    DROP TABLE #StudentsList;


GO
