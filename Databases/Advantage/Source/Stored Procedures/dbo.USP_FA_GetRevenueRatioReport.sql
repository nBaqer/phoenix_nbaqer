SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE PROCEDURE [dbo].[USP_FA_GetRevenueRatioReport]
    @StartDate DATETIME
   ,@EndDate DATETIME
   ,@campGrpId AS VARCHAR(8000)
   ,@strStudentId AS VARCHAR(50)
AS /*----------------------------------------------------------------------------------------------------
    Author : Kamalesh Ahuja
    
    Create date : 06/21/2010
    
    Procedure Name : USP_FA_GetRevenueRatioReport

    Objective : Get Revenue Ratio 90/10 ratio) report
    
    Parameters : Name Type Data Type Required? 
                        ===== ==== ========= ========= 
                        
    
    Output : Returns the Revenue Ratio (90/10 ratio) report dataset 
                        
*/-----------------------------------------------------------------------------------------------------



    BEGIN

        SELECT  *
        INTO    #TempTrans
        FROM    satransactions
        WHERE   TransDate >= @StartDate
                AND TransDate <= @EndDate
                AND satransactions.StuEnrollId IN ( SELECT  StuEnrollId
                                                    FROM    arStuEnrollments
                                                    WHERE   arStuEnrollments.CampusId IN ( (
                                                                                             SELECT DISTINCT
                                                                                                    t1.campusid
                                                                                             FROM   sycmpgrpcmps t1
                                                                                             WHERE  t1.campgrpid IN ( SELECT    strval
                                                                                                                      FROM      dbo.SPLIT(@campGrpId) )
                                                                                           )
                                                                                      ) );
    
        SELECT  tt.*
               ,COALESCE((
                           SELECT   SUM(TransAmount)
                           FROM     #TempTrans T
                                   ,saRefunds R
                           WHERE    T.TransactionId = R.TransactionId
                                    AND R.RefundTypeId = 1
                                    AND R.StudentAwardId = tt.StudentAwardId
                                    AND T.TransDate >= @StartDate
                                    AND T.TransDate <= @EndDate
                         ),0.00) AS Refunded
        FROM    (
                  SELECT    AwardTypeId
                           ,StudentAwardId
                           ,FundSourceDescrip
                           ,TitleIV
                           ,StuEnrollId
                           ,LastName
                           ,FirstName
                           ,MiddleName
                           ,StudentIdentifier
                           ,LenderDescrip
                           ,CampGrpId
                           ,CampGrpDescrip
                           ,CampusId
                           ,CampDescrip
                           ,SUM(Received) AS Received
                           ,InstCharges
                           ,PmtPlanCount
                  FROM      (
                              SELECT    SA.AwardTypeId
                                       ,SA.StudentAwardId
                                       ,FS.FundSourceDescrip
                                       ,FS.TitleIV
                                       ,t1.StuEnrollId
                                       ,S.LastName
                                       ,S.FirstName
                                       ,S.MiddleName
                                       ,CASE @strStudentId
                                          WHEN 'StudentId' THEN S.StudentNumber
                                          WHEN 'EnrollmentId' THEN SE.EnrollmentId
                                          ELSE S.SSN
                                        END AS StudentIdentifier
                                       ,(
                                          SELECT    LenderDescrip
                                          FROM      faLenders
                                          WHERE     LenderId = SA.LenderId
                                        ) AS LenderDescrip
                                       ,CGC.CampGrpId
                                       ,syCampGrps.CampGrpDescrip
                                       ,SE.CampusId
                                       ,C.CampDescrip
                                       ,( CASE WHEN PDR.Amount > -t1.TransAmount THEN -t1.TransAmount
                                               ELSE PDR.Amount
                                          END ) AS Received
                                       ,0.00 AS InstCharges
                                       ,0 AS PmtPlanCount
                              FROM      #TempTrans t1
                                       ,saPayments P
                                       ,saPmtDisbRel PDR
                                       ,faStudentAwardSchedule SAS
                                       ,faStudentAwards SA
                                       ,saFundSources FS
                                       ,arStuEnrollments SE
                                       ,arStudent S
                                       ,syCmpGrpCmps CGC
                                       ,syCampGrps
                                       ,syCampuses C
                              WHERE     t1.voided = 0
                                        AND t1.TransactionId = P.TransactionId
                                        AND t1.TransactionId = PDR.TransactionId
                                        AND PDR.AwardScheduleId = SAS.AwardScheduleId
                                        AND SAS.StudentAwardId = SA.StudentAwardId
                                        AND SA.AwardTypeId = FS.FundSourceId
                                        AND FS.TitleIV = 1
                                        AND t1.StuEnrollId = SE.StuEnrollId
                                        AND SE.StudentId = S.StudentId
                                        AND CGC.CampusId = SE.CampusId
                                        AND syCampGrps.CampGrpId = CGC.CampGrpId
                                        AND C.CampusId = SE.CampusId
                                        AND t1.Voided = 0
                                        AND (
                                              ( t1.TransCodeId IN ( SELECT  TransCodeId
                                                                    FROM    saTransCodes TC
                                                                    WHERE   TC.TransCodeId = t1.TransCodeId
                                                                            AND TC.IsInstCharge = 1 ) )
                                              OR ( t1.TransCodeId IN ( SELECT   T2.TransCodeId
                                                                       FROM     saTransCodes TC
                                                                               ,#TempTrans T2
                                                                               ,saAppliedPayments AP1
                                                                       WHERE    AP1.TransactionId = t1.TransactionId
                                                                                AND AP1.AppliedPmtId = T2.TransactionId
                                                                                AND TC.TransCodeId = T2.TransCodeId
                                                                                AND TC.IsInstCharge = 1 ) )
                                            )
                                        AND syCampGrps.campgrpid IN ( SELECT DISTINCT
                                                                                t1.campgrpid
                                                                      FROM      sycmpgrpcmps t1
                                                                      WHERE     t1.campgrpid IN ( SELECT    strval
                                                                                                  FROM      dbo.SPLIT(@campGrpId) ) )
                                        AND t1.TransDate >= @StartDate
                                        AND t1.TransDate <= @EndDate 
 --And SE.StuEnrollId='B5A25B7F-D50C-4093-B04A-57D886892822' 
                            ) t
                  GROUP BY  AwardTypeId
                           ,StudentAwardId
                           ,FundSourceDescrip
                           ,TitleIV
                           ,StuEnrollId
                           ,LastName
                           ,FirstName
                           ,MiddleName
                           ,StudentIdentifier
                           ,LenderDescrip
                           ,CampGrpId
                           ,CampGrpDescrip
                           ,CampusId
                           ,CampDescrip
                           ,InstCharges
                           ,PmtPlanCount
                ) tt
        UNION
        SELECT  tt.*
               ,COALESCE((
                           SELECT   SUM(TransAmount)
                           FROM     #TempTrans T
                                   ,saRefunds R
                           WHERE    T.TransactionId = R.TransactionId
                                    AND R.RefundTypeId = 1
                                    AND R.StudentAwardId = tt.StudentAwardId
                                    AND T.TransDate >= @StartDate
                                    AND T.TransDate <= @EndDate
                         ),0.00) AS Refunded
        FROM    (
                  SELECT    AwardTypeId
                           ,StudentAwardId
                           ,FundSourceDescrip
                           ,TitleIV
                           ,StuEnrollId
                           ,LastName
                           ,FirstName
                           ,MiddleName
                           ,StudentIdentifier
                           ,LenderDescrip
                           ,CampGrpId
                           ,CampGrpDescrip
                           ,CampusId
                           ,CampDescrip
                           ,SUM(Received) AS Received
                           ,InstCharges
                           ,PmtPlanCount
                  FROM      (
                              SELECT    SA.AwardTypeId
                                       ,SA.StudentAwardId
                                       ,FS.FundSourceDescrip
                                       ,FS.TitleIV
                                       ,t1.StuEnrollId
                                       ,S.LastName
                                       ,S.FirstName
                                       ,S.MiddleName
                                       ,CASE @strStudentId
                                          WHEN 'StudentId' THEN S.StudentNumber
                                          WHEN 'EnrollmentId' THEN SE.EnrollmentId
                                          ELSE S.SSN
                                        END AS StudentIdentifier
                                       ,(
                                          SELECT    LenderDescrip
                                          FROM      faLenders
                                          WHERE     LenderId = SA.LenderId
                                        ) AS LenderDescrip
                                       ,CGC.CampGrpId
                                       ,syCampGrps.CampGrpDescrip
                                       ,SE.CampusId
                                       ,C.CampDescrip
                                       ,( CASE WHEN PDR.Amount > -t1.TransAmount THEN -t1.TransAmount
                                               ELSE PDR.Amount
                                          END ) AS Received
                                       ,0.00 AS InstCharges
                                       ,0 AS PmtPlanCount
                              FROM      #TempTrans t1
                                       ,saPayments P
                                       ,saPmtDisbRel PDR
                                       ,faStudentAwardSchedule SAS
                                       ,faStudentAwards SA
                                       ,saFundSources FS
                                       ,arStuEnrollments SE
                                       ,arStudent S
                                       ,syCmpGrpCmps CGC
                                       ,syCampGrps
                                       ,syCampuses C
                              WHERE     t1.voided = 0
                                        AND t1.TransactionId = P.TransactionId
                                        AND t1.TransactionId = PDR.TransactionId
                                        AND PDR.AwardScheduleId = SAS.AwardScheduleId
                                        AND SAS.StudentAwardId = SA.StudentAwardId
                                        AND SA.AwardTypeId = FS.FundSourceId
                                        AND FS.TitleIV = 0
                                        AND t1.StuEnrollId = SE.StuEnrollId
                                        AND SE.StudentId = S.StudentId
                                        AND CGC.CampusId = SE.CampusId
                                        AND syCampGrps.CampGrpId = CGC.CampGrpId
                                        AND C.CampusId = SE.CampusId
                                        AND t1.Voided = 0
                                        AND syCampGrps.campgrpid IN ( SELECT DISTINCT
                                                                                t1.campgrpid
                                                                      FROM      sycmpgrpcmps t1
                                                                      WHERE     t1.campgrpid IN ( SELECT    strval
                                                                                                  FROM      dbo.SPLIT(@campGrpId) ) )
                                        AND t1.TransDate >= @StartDate
                                        AND t1.TransDate <= @EndDate
                                        AND (
                                              ( t1.TransCodeId IN ( SELECT  TransCodeId
                                                                    FROM    saTransCodes TC
                                                                    WHERE   TC.TransCodeId = t1.TransCodeId
                                                                            AND TC.IsInstCharge = 1 ) )
                                              OR ( t1.TransCodeId IN ( SELECT   T2.TransCodeId
                                                                       FROM     saTransCodes TC
                                                                               ,#TempTrans T2
                                                                               ,saAppliedPayments AP1
                                                                       WHERE    AP1.TransactionId = t1.TransactionId
                                                                                AND AP1.AppliedPmtId = T2.TransactionId
                                                                                AND TC.TransCodeId = T2.TransCodeId
                                                                                AND TC.IsInstCharge = 1 ) )
                                            ) 
 --And SE.StuEnrollId='B5A25B7F-D50C-4093-B04A-57D886892822' 
                            ) t
                  GROUP BY  AwardTypeId
                           ,StudentAwardId
                           ,FundSourceDescrip
                           ,TitleIV
                           ,StuEnrollId
                           ,LastName
                           ,FirstName
                           ,MiddleName
                           ,StudentIdentifier
                           ,LenderDescrip
                           ,CampGrpId
                           ,CampGrpDescrip
                           ,CampusId
                           ,CampDescrip
                           ,InstCharges
                           ,PmtPlanCount
                ) tt
        UNION
        SELECT  AwardTypeId
               ,StudentAwardId
               ,FundSourceDescrip
               ,TitleIV
               ,StuEnrollId
               ,LastName
               ,FirstName
               ,MiddleName
               ,StudentIdentifier
               ,LenderDescrip
               ,CampGrpId
               ,CampGrpDescrip
               ,CampusId
               ,CampDescrip
               ,SUM(Received) AS Received
               ,InstCharges
               ,PmtPlanCount
               ,SUM(Refunded) AS Refunded
        FROM    (
                  SELECT    '00000000-0000-0000-0000-000000000000' AS AwardTypeId
                           ,'00000000-0000-0000-0000-000000000000' AS StudentAwardId
                           ,'Fund Source: Cash' AS FundSourceDescrip
                           ,0 AS TitleIV
                           ,t1.StuEnrollId
                           ,S.LastName
                           ,S.FirstName
                           ,S.MiddleName
                           ,CASE @strStudentId
                              WHEN 'StudentId' THEN S.StudentNumber
                              WHEN 'EnrollmentId' THEN SE.EnrollmentId
                              ELSE S.SSN
                            END AS StudentIdentifier
                           ,'' AS LenderDescrip
                           ,CGC.CampGrpId
                           ,syCampGrps.CampGrpDescrip
                           ,SE.CampusId
                           ,C.CampDescrip
                           ,0.00 AS Received
                           ,t1.TransAmount AS Refunded
                           ,0.00 AS InstCharges
                           ,0 AS PmtPlanCount
                  FROM      #TempTrans t1
                           ,saRefunds R
                           ,arStuEnrollments SE
                           ,arStudent S
                           ,syCmpGrpCmps CGC
                           ,syCampGrps
                           ,syCampuses C
                           ,saTransCodes TC
                  WHERE     t1.voided = 0
                            AND t1.TransactionId = R.TransactionId
                            AND t1.StuEnrollId = SE.StuEnrollId
                            AND R.RefundTypeId = 0
                            AND R.StudentAwardId IS NULL
                            AND SE.StudentId = S.StudentId
                            AND CGC.CampusId = SE.CampusId
                            AND syCampGrps.CampGrpId = CGC.CampGrpId
                            AND C.CampusId = SE.CampusId
                            AND TC.TransCodeId = t1.TransCodeId
                            AND TC.IsInstCharge = 1
                            AND syCampGrps.campgrpid IN ( SELECT DISTINCT
                                                                    t1.campgrpid
                                                          FROM      sycmpgrpcmps t1
                                                          WHERE     t1.campgrpid IN ( SELECT    strval
                                                                                      FROM      dbo.SPLIT(@campGrpId) ) )
                            AND t1.TransDate >= @StartDate
                            AND t1.TransDate <= @EndDate 
   --And SE.StuEnrollId='B5A25B7F-D50C-4093-B04A-57D886892822'
                ) t
        GROUP BY AwardTypeId
               ,StudentAwardId
               ,FundSourceDescrip
               ,TitleIV
               ,StuEnrollId
               ,LastName
               ,FirstName
               ,MiddleName
               ,StudentIdentifier
               ,LenderDescrip
               ,CampGrpId
               ,CampGrpDescrip
               ,CampusId
               ,CampDescrip
               ,PmtPlanCount
               ,InstCharges
        UNION
        SELECT  AwardTypeId
               ,StudentAwardId
               ,FundSourceDescrip
               ,TitleIV
               ,StuEnrollId
               ,LastName
               ,FirstName
               ,MiddleName
               ,StudentIdentifier
               ,LenderDescrip
               ,CampGrpId
               ,CampGrpDescrip
               ,CampusId
               ,CampDescrip
               ,SUM(Received) AS Received
               ,InstCharges
               ,PmtPlanCount
               ,0.00 AS Refunded
        FROM    (
                  SELECT    '00000000-0000-0000-0000-000000000000' AS AwardTypeId
                           ,'00000000-0000-0000-0000-000000000000' AS StudentAwardId
                           ,'Fund Source: Cash' AS FundSourceDescrip
                           ,0 AS TitleIV
                           ,t1.StuEnrollId
                           ,S.LastName
                           ,S.FirstName
                           ,S.MiddleName
                           ,CASE @strStudentId
                              WHEN 'StudentId' THEN S.StudentNumber
                              WHEN 'EnrollmentId' THEN SE.EnrollmentId
                              ELSE S.SSN
                            END AS StudentIdentifier
                           ,'' AS LenderDescrip
                           ,CGC.CampGrpId
                           ,syCampGrps.CampGrpDescrip
                           ,SE.CampusId
                           ,C.CampDescrip
                           ,-t1.TransAmount AS Received
                           ,0.00 AS Refunded
                           ,0.00 AS InstCharges
                           ,0 AS PmtPlanCount
                  FROM      #TempTrans t1
                           ,arStuEnrollments SE
                           ,arStudent S
                           ,syCmpGrpCmps CGC
                           ,syCampGrps
                           ,syCampuses C
                           ,saTransCodes TC
                           ,saPayments P
                  WHERE     t1.voided = 0
                            AND t1.StuEnrollId = SE.StuEnrollId
                            AND SE.StudentId = S.StudentId
                            AND CGC.CampusId = SE.CampusId
                            AND syCampGrps.CampGrpId = CGC.CampGrpId
                            AND C.CampusId = SE.CampusId
                            AND TC.TransCodeId = t1.TransCodeId
                            AND TC.IsInstCharge = 1
                            AND t1.TransactionId = P.TransactionId
                            AND P.ScheduledPayment = 0 
   --AND syCampGrps.CampGrpId in (select Distinct t1.CampGrpId from syCmpGrpCmps t1 
   --where t1.CampGrpId in ('0F811F4D-FA5E-45A7-B9C4-3B52A8FE1238'))
                            AND syCampGrps.campgrpid IN ( SELECT DISTINCT
                                                                    t1.campgrpid
                                                          FROM      sycmpgrpcmps t1
                                                          WHERE     t1.campgrpid IN ( SELECT    strval
                                                                                      FROM      dbo.SPLIT(@campGrpId) ) )
                            AND t1.TransDate >= @StartDate
                            AND t1.TransDate <= @EndDate 
   --And SE.StuEnrollId='B5A25B7F-D50C-4093-B04A-57D886892822'
                ) t
        GROUP BY AwardTypeId
               ,StudentAwardId
               ,FundSourceDescrip
               ,TitleIV
               ,StuEnrollId
               ,LastName
               ,FirstName
               ,MiddleName
               ,StudentIdentifier
               ,LenderDescrip
               ,CampGrpId
               ,CampGrpDescrip
               ,CampusId
               ,CampDescrip
               ,PmtPlanCount
               ,InstCharges
        ORDER BY FundSourceDescrip
               ,LastName
               ,FirstName;

        DROP TABLE #TempTrans;

    END;





GO
