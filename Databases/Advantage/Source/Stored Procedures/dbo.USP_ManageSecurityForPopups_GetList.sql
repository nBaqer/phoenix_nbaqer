SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_ManageSecurityForPopups_GetList]
    @RoleId UNIQUEIDENTIFIER
AS --Set @RoleId='57EFECAF-BA91-4D5F-BA4B-8C6489A0C3D2'
    SELECT DISTINCT
            NULL AS ChildResourceId
           ,'Apply permission to all popup pages' AS ChildResource
           ,NULL AS AccessLevel
           ,NULL AS ChildResourceURL
           ,NULL AS ParentResourceId
           ,NULL AS ParentResource
           ,1 AS GroupSortOrder
           ,1 AS FirstSortOrder
           ,1 AS DisplaySequence
           ,1 AS ResourceTypeId
           ,NULL AS TabId
    UNION
    SELECT DISTINCT
            NNChild.ResourceId AS ChildResourceId
           ,RChild.Resource AS ChildResource
           ,(
              SELECT    AccessLevel
              FROM      syRlsResLvls
              WHERE     RoleId = @RoleId
                        AND ResourceId = RChild.ResourceId
            ) AS AccessLevel
           ,RChild.ResourceURL AS ChildResourceURL
           ,CASE WHEN (
                        NNParent.ResourceId IN ( 689 )
                        OR NNChild.ResourceId IN ( 737,738,739,740,741,742 )
                        OR RChild.ResourceId IN ( 6,38,40,41,43,44,84,85,94,169,174,213,238,277,290,295,317,321,322,336,340,373,380,506,578 )
                      ) THEN NULL
                 WHEN NNChild.ResourceId = 475 THEN 38
                 WHEN NNChild.ResourceId = 61 THEN 41
                 ELSE NNParent.ResourceId
            END AS ParentResourceId
           ,CASE WHEN NNChild.ResourceId IN ( 6,38,40,41,43,44,61,84,85,94,169,174,213,238,277,290,295,317,321,322,336,340,373,380,475,506,578 ) THEN NULL
                 ELSE RParent.Resource
            END AS ParentResource
           ,GroupSortOrder = CASE WHEN (
                                         NNChild.ResourceId = 6
                                         OR NNParent.ResourceId = 6
                                       ) THEN 1
                                  WHEN (
                                         NNChild.ResourceId = 38
                                         OR NNParent.ResourceId = 38
                                       ) THEN 2
                                  WHEN (
                                         NNChild.ResourceId = 40
                                         OR NNParent.ResourceId = 40
                                       ) THEN 3
                                  WHEN (
                                         NNChild.ResourceId = 41
                                         OR NNParent.ResourceId = 41
                                       ) THEN 4
                                  WHEN (
                                         NNChild.ResourceId = 43
                                         OR NNParent.ResourceId = 43
                                       ) THEN 5
                                  WHEN (
                                         NNChild.ResourceId = 44
                                         OR NNParent.ResourceId = 44
                                       ) THEN 6
                                  WHEN (
                                         NNChild.ResourceId = 61
                                         OR NNParent.ResourceId = 61
                                       ) THEN 7
                                  WHEN (
                                         NNChild.ResourceId = 84
                                         OR NNParent.ResourceId = 84
                                       ) THEN 8
                                  WHEN (
                                         NNChild.ResourceId = 85
                                         OR NNParent.ResourceId = 85
                                       ) THEN 9
                                  WHEN (
                                         NNChild.ResourceId = 94
                                         OR NNParent.ResourceId = 94
                                       ) THEN 10
                                  WHEN (
                                         NNChild.ResourceId = 169
                                         OR NNParent.ResourceId = 169
                                       ) THEN 11
                                  WHEN (
                                         NNChild.ResourceId = 174
                                         OR NNParent.ResourceId = 174
                                       ) THEN 12
                                  WHEN (
                                         NNChild.ResourceId = 213
                                         OR NNParent.ResourceId = 213
                                       ) THEN 13
                                  WHEN (
                                         NNChild.ResourceId = 238
                                         OR NNParent.ResourceId = 238
                                       ) THEN 14
                                  WHEN (
                                         NNChild.ResourceId = 277
                                         OR NNParent.ResourceId = 277
                                       ) THEN 15
                                  WHEN (
                                         NNChild.ResourceId = 290
                                         OR NNParent.ResourceId = 290
                                       ) THEN 16
                                  WHEN (
                                         NNChild.ResourceId = 295
                                         OR NNParent.ResourceId = 295
                                       ) THEN 17
                                  WHEN (
                                         NNChild.ResourceId = 317
                                         OR NNParent.ResourceId = 317
                                       ) THEN 18
                                  WHEN (
                                         NNChild.ResourceId = 321
                                         OR NNParent.ResourceId = 321
                                       ) THEN 19
                                  WHEN (
                                         NNChild.ResourceId = 322
                                         OR NNParent.ResourceId = 322
                                       ) THEN 20
                                  WHEN (
                                         NNChild.ResourceId = 336
                                         OR NNParent.ResourceId = 336
                                       ) THEN 21
                                  WHEN (
                                         NNChild.ResourceId = 340
                                         OR NNParent.ResourceId = 340
                                       ) THEN 22
                                  WHEN (
                                         NNChild.ResourceId = 373
                                         OR NNParent.ResourceId = 373
                                       ) THEN 23
                                  WHEN (
                                         NNChild.ResourceId = 380
                                         OR NNParent.ResourceId = 380
                                       ) THEN 24
                                  WHEN (
                                         NNChild.ResourceId = 475
                                         OR NNParent.ResourceId = 475
                                       ) THEN 25
                                  WHEN (
                                         NNChild.ResourceId = 506
                                         OR NNParent.ResourceId = 506
                                       ) THEN 26
                                  WHEN (
                                         NNChild.ResourceId = 578
                                         OR NNParent.ResourceId = 578
                                       ) THEN 27
                                  ELSE 28
                             END
           ,FirstSortOrder = CASE WHEN (
                                         NNChild.ResourceId = 6
                                         OR NNParent.ResourceId = 6
                                       ) THEN 1
                                  WHEN (
                                         NNChild.ResourceId = 38
                                         OR NNParent.ResourceId = 38
                                       ) THEN 2
                                  WHEN (
                                         NNChild.ResourceId = 40
                                         OR NNParent.ResourceId = 40
                                       ) THEN 3
                                  WHEN (
                                         NNChild.ResourceId = 41
                                         OR NNParent.ResourceId = 41
                                       ) THEN 4
                                  WHEN (
                                         NNChild.ResourceId = 43
                                         OR NNParent.ResourceId = 43
                                       ) THEN 5
                                  WHEN (
                                         NNChild.ResourceId = 44
                                         OR NNParent.ResourceId = 44
                                       ) THEN 6
                                  WHEN (
                                         NNChild.ResourceId = 61
                                         OR NNParent.ResourceId = 61
                                       ) THEN 7
                                  WHEN (
                                         NNChild.ResourceId = 84
                                         OR NNParent.ResourceId = 84
                                       ) THEN 8
                                  WHEN (
                                         NNChild.ResourceId = 85
                                         OR NNParent.ResourceId = 85
                                       ) THEN 9
                                  WHEN (
                                         NNChild.ResourceId = 94
                                         OR NNParent.ResourceId = 94
                                       ) THEN 10
                                  WHEN (
                                         NNChild.ResourceId = 169
                                         OR NNParent.ResourceId = 169
                                       ) THEN 11
                                  WHEN (
                                         NNChild.ResourceId = 174
                                         OR NNParent.ResourceId = 174
                                       ) THEN 12
                                  WHEN (
                                         NNChild.ResourceId = 213
                                         OR NNParent.ResourceId = 213
                                       ) THEN 13
                                  WHEN (
                                         NNChild.ResourceId = 238
                                         OR NNParent.ResourceId = 238
                                       ) THEN 14
                                  WHEN (
                                         NNChild.ResourceId = 277
                                         OR NNParent.ResourceId = 277
                                       ) THEN 15
                                  WHEN (
                                         NNChild.ResourceId = 290
                                         OR NNParent.ResourceId = 290
                                       ) THEN 16
                                  WHEN (
                                         NNChild.ResourceId = 295
                                         OR NNParent.ResourceId = 295
                                       ) THEN 17
                                  WHEN (
                                         NNChild.ResourceId = 317
                                         OR NNParent.ResourceId = 317
                                       ) THEN 18
                                  WHEN (
                                         NNChild.ResourceId = 321
                                         OR NNParent.ResourceId = 321
                                       ) THEN 19
                                  WHEN (
                                         NNChild.ResourceId = 322
                                         OR NNParent.ResourceId = 322
                                       ) THEN 20
                                  WHEN (
                                         NNChild.ResourceId = 336
                                         OR NNParent.ResourceId = 336
                                       ) THEN 21
                                  WHEN (
                                         NNChild.ResourceId = 340
                                         OR NNParent.ResourceId = 340
                                       ) THEN 22
                                  WHEN (
                                         NNChild.ResourceId = 373
                                         OR NNParent.ResourceId = 373
                                       ) THEN 23
                                  WHEN (
                                         NNChild.ResourceId = 380
                                         OR NNParent.ResourceId = 380
                                       ) THEN 24
                                  WHEN (
                                         NNChild.ResourceId = 475
                                         OR NNParent.ResourceId = 475
                                       ) THEN 25
                                  WHEN (
                                         NNChild.ResourceId = 506
                                         OR NNParent.ResourceId = 506
                                       ) THEN 26
                                  WHEN (
                                         NNChild.ResourceId = 578
                                         OR NNParent.ResourceId = 578
                                       ) THEN 27
                                  ELSE 28
                             END
           ,DisplaySequence = CASE WHEN (
                                          NNChild.ResourceId = 6
                                          OR NNParent.ResourceId = 6
                                        ) THEN 1
                                   WHEN (
                                          NNChild.ResourceId = 38
                                          OR NNParent.ResourceId = 38
                                        ) THEN 2
                                   WHEN (
                                          NNChild.ResourceId = 40
                                          OR NNParent.ResourceId = 40
                                        ) THEN 3
                                   WHEN (
                                          NNChild.ResourceId = 41
                                          OR NNParent.ResourceId = 41
                                        ) THEN 4
                                   WHEN (
                                          NNChild.ResourceId = 43
                                          OR NNParent.ResourceId = 43
                                        ) THEN 5
                                   WHEN (
                                          NNChild.ResourceId = 44
                                          OR NNParent.ResourceId = 44
                                        ) THEN 6
                                   WHEN (
                                          NNChild.ResourceId = 61
                                          OR NNParent.ResourceId = 61
                                        ) THEN 7
                                   WHEN (
                                          NNChild.ResourceId = 84
                                          OR NNParent.ResourceId = 84
                                        ) THEN 8
                                   WHEN (
                                          NNChild.ResourceId = 85
                                          OR NNParent.ResourceId = 85
                                        ) THEN 9
                                   WHEN (
                                          NNChild.ResourceId = 94
                                          OR NNParent.ResourceId = 94
                                        ) THEN 10
                                   WHEN (
                                          NNChild.ResourceId = 169
                                          OR NNParent.ResourceId = 169
                                        ) THEN 11
                                   WHEN (
                                          NNChild.ResourceId = 174
                                          OR NNParent.ResourceId = 174
                                        ) THEN 12
                                   WHEN (
                                          NNChild.ResourceId = 213
                                          OR NNParent.ResourceId = 213
                                        ) THEN 13
                                   WHEN (
                                          NNChild.ResourceId = 238
                                          OR NNParent.ResourceId = 238
                                        ) THEN 14
                                   WHEN (
                                          NNChild.ResourceId = 277
                                          OR NNParent.ResourceId = 277
                                        ) THEN 15
                                   WHEN (
                                          NNChild.ResourceId = 290
                                          OR NNParent.ResourceId = 290
                                        ) THEN 16
                                   WHEN (
                                          NNChild.ResourceId = 295
                                          OR NNParent.ResourceId = 295
                                        ) THEN 17
                                   WHEN (
                                          NNChild.ResourceId = 317
                                          OR NNParent.ResourceId = 317
                                        ) THEN 18
                                   WHEN (
                                          NNChild.ResourceId = 321
                                          OR NNParent.ResourceId = 321
                                        ) THEN 19
                                   WHEN (
                                          NNChild.ResourceId = 322
                                          OR NNParent.ResourceId = 322
                                        ) THEN 20
                                   WHEN (
                                          NNChild.ResourceId = 336
                                          OR NNParent.ResourceId = 336
                                        ) THEN 21
                                   WHEN (
                                          NNChild.ResourceId = 340
                                          OR NNParent.ResourceId = 340
                                        ) THEN 22
                                   WHEN (
                                          NNChild.ResourceId = 373
                                          OR NNParent.ResourceId = 373
                                        ) THEN 23
                                   WHEN (
                                          NNChild.ResourceId = 380
                                          OR NNParent.ResourceId = 380
                                        ) THEN 24
                                   WHEN (
                                          NNChild.ResourceId = 475
                                          OR NNParent.ResourceId = 475
                                        ) THEN 25
                                   WHEN (
                                          NNChild.ResourceId = 506
                                          OR NNParent.ResourceId = 506
                                        ) THEN 26
                                   WHEN (
                                          NNChild.ResourceId = 578
                                          OR NNParent.ResourceId = 578
                                        ) THEN 27
                                   ELSE 28
                              END
           ,CASE WHEN RChild.ResourceId IN ( 6,38,40,41,43,44,84,85,94,169,174,213,238,277,290,295,317,321,322,336,340,373,380,506,578 ) THEN 2
                 WHEN RChild.ResourceId IN ( 287,288,475,65,64,475,61 ) THEN 4
                 ELSE RChild.ResourceTypeID
            END AS ResourceTypeId
           ,NULL AS TabId
    FROM    syResources RChild
    INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
    INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
    INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceId
    INNER JOIN syNavigationNodes NNSubMenuParent ON NNParent.ParentId = NNSubMenuParent.HierarchyId
    INNER JOIN syNavigationNodes NNModuleParent ON NNSubMenuParent.ParentId = NNModuleParent.HierarchyId
    WHERE   NNChild.IsPopupWindow = 1
            AND RChild.ResourceId NOT IN ( 578,293,357,294,295 )
    UNION
    SELECT  ResourceId
           ,Resource
           ,NULL AS AccessLevel
           ,ResourceURL
           ,NULL AS ParentResourceId
           ,NULL AS ParentResource
           ,GroupSortOrder = CASE WHEN (
                                         ResourceId = 6
                                         OR ResourceId = 6
                                       ) THEN 1
                                  WHEN (
                                         ResourceId = 38
                                         OR ResourceId = 38
                                       ) THEN 2
                                  WHEN (
                                         ResourceId = 40
                                         OR ResourceId = 40
                                       ) THEN 3
                                  WHEN (
                                         ResourceId = 41
                                         OR ResourceId = 41
                                       ) THEN 4
                                  WHEN (
                                         ResourceId = 43
                                         OR ResourceId = 43
                                       ) THEN 5
                                  WHEN (
                                         ResourceId = 44
                                         OR ResourceId = 44
                                       ) THEN 6
                                  WHEN (
                                         ResourceId = 61
                                         OR ResourceId = 61
                                       ) THEN 7
                                  WHEN (
                                         ResourceId = 84
                                         OR ResourceId = 84
                                       ) THEN 8
                                  WHEN (
                                         ResourceId = 85
                                         OR ResourceId = 85
                                       ) THEN 9
                                  WHEN (
                                         ResourceId = 94
                                         OR ResourceId = 94
                                       ) THEN 10
                                  WHEN (
                                         ResourceId = 169
                                         OR ResourceId = 169
                                       ) THEN 11
                                  WHEN (
                                         ResourceId = 174
                                         OR ResourceId = 174
                                       ) THEN 12
                                  WHEN (
                                         ResourceId = 213
                                         OR ResourceId = 213
                                       ) THEN 13
                                  WHEN (
                                         ResourceId = 238
                                         OR ResourceId = 238
                                       ) THEN 14
                                  WHEN (
                                         ResourceId = 277
                                         OR ResourceId = 277
                                       ) THEN 15
                                  WHEN (
                                         ResourceId = 290
                                         OR ResourceId = 290
                                       ) THEN 16
                                  WHEN (
                                         ResourceId = 295
                                         OR ResourceId = 295
                                       ) THEN 17
                                  WHEN (
                                         ResourceId = 317
                                         OR ResourceId = 317
                                       ) THEN 18
                                  WHEN (
                                         ResourceId = 321
                                         OR ResourceId = 321
                                       ) THEN 19
                                  WHEN (
                                         ResourceId = 322
                                         OR ResourceId = 322
                                       ) THEN 20
                                  WHEN (
                                         ResourceId = 336
                                         OR ResourceId = 336
                                       ) THEN 21
                                  WHEN (
                                         ResourceId = 340
                                         OR ResourceId = 340
                                       ) THEN 22
                                  WHEN (
                                         ResourceId = 373
                                         OR ResourceId = 373
                                       ) THEN 23
                                  WHEN (
                                         ResourceId = 380
                                         OR ResourceId = 380
                                       ) THEN 24
                                  WHEN (
                                         ResourceId = 475
                                         OR ResourceId = 475
                                       ) THEN 25
                                  WHEN (
                                         ResourceId = 506
                                         OR ResourceId = 506
                                       ) THEN 26
                                  WHEN (
                                         ResourceId = 578
                                         OR ResourceId = 578
                                       ) THEN 27
                             END
           ,FirstSortOrder = CASE WHEN (
                                         ResourceId = 6
                                         OR ResourceId = 6
                                       ) THEN 1
                                  WHEN (
                                         ResourceId = 38
                                         OR ResourceId = 38
                                       ) THEN 2
                                  WHEN (
                                         ResourceId = 40
                                         OR ResourceId = 40
                                       ) THEN 3
                                  WHEN (
                                         ResourceId = 41
                                         OR ResourceId = 41
                                       ) THEN 4
                                  WHEN (
                                         ResourceId = 43
                                         OR ResourceId = 43
                                       ) THEN 5
                                  WHEN (
                                         ResourceId = 44
                                         OR ResourceId = 44
                                       ) THEN 6
                                  WHEN (
                                         ResourceId = 61
                                         OR ResourceId = 61
                                       ) THEN 7
                                  WHEN (
                                         ResourceId = 84
                                         OR ResourceId = 84
                                       ) THEN 8
                                  WHEN (
                                         ResourceId = 85
                                         OR ResourceId = 85
                                       ) THEN 9
                                  WHEN (
                                         ResourceId = 94
                                         OR ResourceId = 94
                                       ) THEN 10
                                  WHEN (
                                         ResourceId = 169
                                         OR ResourceId = 169
                                       ) THEN 11
                                  WHEN (
                                         ResourceId = 174
                                         OR ResourceId = 174
                                       ) THEN 12
                                  WHEN (
                                         ResourceId = 213
                                         OR ResourceId = 213
                                       ) THEN 13
                                  WHEN (
                                         ResourceId = 238
                                         OR ResourceId = 238
                                       ) THEN 14
                                  WHEN (
                                         ResourceId = 277
                                         OR ResourceId = 277
                                       ) THEN 15
                                  WHEN (
                                         ResourceId = 290
                                         OR ResourceId = 290
                                       ) THEN 16
                                  WHEN (
                                         ResourceId = 295
                                         OR ResourceId = 295
                                       ) THEN 17
                                  WHEN (
                                         ResourceId = 317
                                         OR ResourceId = 317
                                       ) THEN 18
                                  WHEN (
                                         ResourceId = 321
                                         OR ResourceId = 321
                                       ) THEN 19
                                  WHEN (
                                         ResourceId = 322
                                         OR ResourceId = 322
                                       ) THEN 20
                                  WHEN (
                                         ResourceId = 336
                                         OR ResourceId = 336
                                       ) THEN 21
                                  WHEN (
                                         ResourceId = 340
                                         OR ResourceId = 340
                                       ) THEN 22
                                  WHEN (
                                         ResourceId = 373
                                         OR ResourceId = 373
                                       ) THEN 23
                                  WHEN (
                                         ResourceId = 380
                                         OR ResourceId = 380
                                       ) THEN 24
                                  WHEN (
                                         ResourceId = 475
                                         OR ResourceId = 475
                                       ) THEN 25
                                  WHEN (
                                         ResourceId = 506
                                         OR ResourceId = 506
                                       ) THEN 26
                                  WHEN (
                                         ResourceId = 578
                                         OR ResourceId = 578
                                       ) THEN 27
                             END
           ,DisplaySequence = CASE WHEN ( ResourceId = 6 ) THEN 1
                                   WHEN ( ResourceId = 38 ) THEN 2
                                   WHEN ( ResourceId = 40 ) THEN 3
                                   WHEN ( ResourceId = 41 ) THEN 4
                                   WHEN (
                                          ResourceId = 43
                                          OR ResourceId = 43
                                        ) THEN 5
                                   WHEN (
                                          ResourceId = 44
                                          OR ResourceId = 44
                                        ) THEN 6
                                   WHEN (
                                          ResourceId = 61
                                          OR ResourceId = 61
                                        ) THEN 7
                                   WHEN (
                                          ResourceId = 84
                                          OR ResourceId = 84
                                        ) THEN 8
                                   WHEN (
                                          ResourceId = 85
                                          OR ResourceId = 85
                                        ) THEN 9
                                   WHEN (
                                          ResourceId = 94
                                          OR ResourceId = 94
                                        ) THEN 10
                                   WHEN (
                                          ResourceId = 169
                                          OR ResourceId = 169
                                        ) THEN 11
                                   WHEN (
                                          ResourceId = 174
                                          OR ResourceId = 174
                                        ) THEN 12
                                   WHEN (
                                          ResourceId = 213
                                          OR ResourceId = 213
                                        ) THEN 13
                                   WHEN (
                                          ResourceId = 238
                                          OR ResourceId = 238
                                        ) THEN 14
                                   WHEN (
                                          ResourceId = 277
                                          OR ResourceId = 277
                                        ) THEN 15
                                   WHEN (
                                          ResourceId = 290
                                          OR ResourceId = 290
                                        ) THEN 16
                                   WHEN (
                                          ResourceId = 295
                                          OR ResourceId = 295
                                        ) THEN 17
                                   WHEN (
                                          ResourceId = 317
                                          OR ResourceId = 317
                                        ) THEN 18
                                   WHEN (
                                          ResourceId = 321
                                          OR ResourceId = 321
                                        ) THEN 19
                                   WHEN (
                                          ResourceId = 322
                                          OR ResourceId = 322
                                        ) THEN 20
                                   WHEN (
                                          ResourceId = 336
                                          OR ResourceId = 336
                                        ) THEN 21
                                   WHEN (
                                          ResourceId = 340
                                          OR ResourceId = 340
                                        ) THEN 22
                                   WHEN (
                                          ResourceId = 373
                                          OR ResourceId = 373
                                        ) THEN 23
                                   WHEN (
                                          ResourceId = 380
                                          OR ResourceId = 380
                                        ) THEN 24
                                   WHEN (
                                          ResourceId = 475
                                          OR ResourceId = 475
                                        ) THEN 25
                                   WHEN (
                                          ResourceId = 506
                                          OR ResourceId = 506
                                        ) THEN 26
                                   WHEN (
                                          ResourceId = 578
                                          OR ResourceId = 578
                                        ) THEN 27
                              END
           ,2 AS ResourceTypeId
           ,NULL AS TabId
    FROM    SyResources 
		--where ResourceId in (6,38,40,41,43,44,61,84,85,94,169,174,213,238,277,290,295,317,321,322,336,340,373,380,506,578)
    WHERE   ResourceId IN ( 6,38,40,41,43,44,61,169,174,213,336,380,506,578 )
    ORDER BY GroupSortOrder
           ,ParentResourceId
           ,ChildResource;



GO
