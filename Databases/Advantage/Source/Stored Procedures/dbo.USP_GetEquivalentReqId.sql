SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_GetEquivalentReqId]
    (
     @reqId UNIQUEIDENTIFIER
    )
AS
    SET NOCOUNT ON;
    SELECT  Reqid
    FROM    arCourseEquivalent
    WHERE   EquivReqid = @reqId;
 



GO
