SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
  
CREATE PROCEDURE [dbo].[usp_AR_GetAllStudentByStartDateAndProgramCountByStatusTotal]
    @StartDate DATETIME
   ,@CampGrpId AS VARCHAR(8000)
   ,@ProgVerId AS VARCHAR(8000) = NULL
AS /*---------------------------------------------------------------------------------------------------- 
    Author : Vijay Ramteke 
      
    Create date : 09/13/2010 
      
    Procedure Name : usp_AR_GetAllStudentByStartDateAndProgramCountByStatus 
  
    Objective : Get All The Student Count Status For Start Date And Program Version 
      
    Parameters : Name Type Data Type Required? 
      
    Output : Returns All The Student Count Status For Start Date And Program Version for Report Dataset 
                          
*/----------------------------------------------------------------------------------------------------- 
  
    BEGIN 
  
      
        CREATE TABLE #tempStatusCodes
            (
             EnrollmentStatusCount INT
            ,StatusCodeId UNIQUEIDENTIFIER
            ); 
  
        INSERT  INTO #tempStatusCodes
                SELECT  COUNT(SC.StatusCodeId) AS EnrollmentStatusCount
                       ,SE.StatusCodeId
                FROM    arStuEnrollments SE
                       ,syStatusCodes SC
                       ,dbo.sySysStatus SS
                       ,syCmpGrpCmps E
                       ,syCampuses F
                       ,syCampGrps
                WHERE   SE.StatusCodeId = SC.StatusCodeId
                        AND SC.SysStatusId = SS.SysStatusId
                        AND SS.StatusLevelId = 2
                        AND CAST(SE.StartDate AS DATE) = CAST(@StartDate AS DATE)
                        AND (
                              SE.PrgVerId IN ( SELECT   strval
                                               FROM     dbo.SPLIT(@ProgVerId) )
                              OR @ProgVerId IS NULL
                            )
                        AND SE.CampusId IN ( SELECT DISTINCT
                                                    t1.CampusId
                                             FROM   syCmpGrpCmps t1
                                             WHERE  t1.CampGrpId IN ( SELECT    strval
                                                                      FROM      dbo.SPLIT(@CampGrpId) ) )
                        AND SE.CampusId = F.CampusId
                        AND F.CampusId = E.CampusId
                        AND E.CampGrpId = syCampGrps.CampGrpId
                        AND syCampGrps.CampGrpId IN ( SELECT DISTINCT
                                                                t1.CampGrpId
                                                      FROM      syCmpGrpCmps t1
                                                      WHERE     t1.CampGrpId IN ( SELECT    strval
                                                                                  FROM      dbo.SPLIT(@CampGrpId) ) )
                GROUP BY SE.StatusCodeId; 
  
        SELECT  ISNULL(TSC.EnrollmentStatusCount,0) AS EnrollmentStatusCount
               ,SC.StatusCodeDescrip
        FROM    syStatusCodes SC
        LEFT OUTER JOIN #tempStatusCodes TSC ON SC.StatusCodeId = TSC.StatusCodeId
        LEFT OUTER JOIN dbo.sySysStatus SS ON SS.SysStatusId = SC.SysStatusId
        WHERE   SS.StatusLevelId = 2
        ORDER BY SC.StatusCodeDescrip; 
  
        DROP TABLE #tempStatusCodes; 
  
    END; 



GO
