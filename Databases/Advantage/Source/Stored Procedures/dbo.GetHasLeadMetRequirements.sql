SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ginzo, John
-- Create date: 11/25/2015
-- Description:	Get whether leads requirements are met
-- =============================================
CREATE PROCEDURE [dbo].[GetHasLeadMetRequirements]
    @LeadId UNIQUEIDENTIFIER
AS
    BEGIN

        SET NOCOUNT ON;       

		--------------------------------------------------------
		-- Variables to be used
		--------------------------------------------------------
        DECLARE @curReqId UNIQUEIDENTIFIER;
        DECLARE @curType VARCHAR(50);
        DECLARE @ExpectedCount INT = 0;
        DECLARE @ActualCount INT = 0;		
        DECLARE @IsApproved BIT = 0;
		--------------------------------------------------------


		--------------------------------------------------------
		-- Check School Level Requirements
		--------------------------------------------------------
        DECLARE CHECK_SCHOOL_LEVEL_COUNTS CURSOR
        FOR
            SELECT  r.adReqId
                   ,t.Descrip
            FROM    dbo.adReqs r
            INNER JOIN dbo.adReqsEffectiveDates ef ON r.adReqId = ef.adReqId
            INNER JOIN dbo.adReqTypes t ON r.adReqTypeId = t.adReqTypeId
            INNER JOIN dbo.syStatuses s ON r.StatusId = s.StatusId
            WHERE   ef.MandatoryRequirement = 1
                    AND r.ReqforEnrollment = 1
                    AND (
                          (
                            GETDATE() >= ef.StartDate
                            AND ef.EndDate IS NULL
                          )
                          OR ( GETDATE() BETWEEN ef.StartDate AND ef.EndDate )
                        )
                    AND s.StatusCode = 'A';

		

        OPEN CHECK_SCHOOL_LEVEL_COUNTS;
        FETCH NEXT FROM CHECK_SCHOOL_LEVEL_COUNTS INTO @curReqId,@curType;

        WHILE @@FETCH_STATUS = 0
            BEGIN
				

                SET @ExpectedCount = @ExpectedCount + 1;
                SET @IsApproved = (
                                    SELECT  dbo.GetIsRequirementApprovedOrOverridden(@curReqId,@curType,@LeadId)
                                  );

                IF @IsApproved = 1
                    BEGIN
                        SET @ActualCount = @ActualCount + 1;
                    END;                

                FETCH NEXT FROM CHECK_SCHOOL_LEVEL_COUNTS INTO @curReqId,@curType;

            END;

        CLOSE CHECK_SCHOOL_LEVEL_COUNTS;
        DEALLOCATE CHECK_SCHOOL_LEVEL_COUNTS;
		--------------------------------------------------------


		--------------------------------------------------------
		--Check Program Level Requirements
		--------------------------------------------------------
        DECLARE @PrgVerId UNIQUEIDENTIFIER;
        SET @PrgVerId = (
                          SELECT    prgverid
                          FROM      adleads
                          WHERE     leadid = @LeadId
                        );

        IF @PrgVerId IS NOT NULL
            BEGIN
			

                DECLARE CHECK_PROGRAM_LEVEL_COUNTS CURSOR
                FOR
                    SELECT DISTINCT
                            ( r.adReqId ) AS curReqId
                           ,t.Descrip AS curType
                    FROM    dbo.adPrgVerTestDetails pr
                    INNER JOIN dbo.adReqs r ON pr.adReqId = r.adReqId
                                               AND PrgVerId = @PrgVerId
                    INNER JOIN dbo.adReqsEffectiveDates ef ON r.adReqId = ef.adReqId
                    INNER JOIN dbo.adReqTypes t ON r.adReqTypeId = t.adReqTypeId
                    INNER JOIN dbo.syStatuses s ON r.StatusId = s.StatusId
                    WHERE   r.ReqforEnrollment = 1
                            AND s.StatusCode = 'A'
                            AND (
                                  (
                                    GETDATE() >= ef.StartDate
                                    AND ef.EndDate IS NULL
                                  )
                                  OR ( GETDATE() BETWEEN ef.StartDate AND ef.EndDate )
                                ); 

                OPEN CHECK_PROGRAM_LEVEL_COUNTS;
                FETCH NEXT FROM CHECK_PROGRAM_LEVEL_COUNTS INTO @curReqId,@curType;

                WHILE @@FETCH_STATUS = 0
                    BEGIN
                        SET @IsApproved = 0;

                        SET @ExpectedCount = @ExpectedCount + 1;
                        SET @IsApproved = (
                                            SELECT  dbo.GetIsRequirementApprovedOrOverridden(@curReqId,@curType,@LeadId)
                                          );

                        IF @IsApproved = 1
                            BEGIN
                                SET @ActualCount = @ActualCount + 1;
                            END;                

                        FETCH NEXT FROM CHECK_PROGRAM_LEVEL_COUNTS INTO @curReqId,@curType;

                    END;

                CLOSE CHECK_PROGRAM_LEVEL_COUNTS;
                DEALLOCATE CHECK_PROGRAM_LEVEL_COUNTS;
           

            END;
		--------------------------------------------------------

        
		
		
		--------------------------------------------------------
		--Check Lead Group Level Requirements
		--------------------------------------------------------
        DECLARE CHECK_LEADGROUP_LEVEL_COUNTS CURSOR
        FOR
            SELECT  r.adReqId AS curReqId
                   ,t.Descrip AS curType
            FROM    dbo.adLeadGroups lg
            INNER JOIN dbo.adLeadByLeadGroups lgb ON lg.LeadGrpId = lgb.LeadGrpId
                                                     AND lgb.LeadId = @LeadId
            INNER JOIN dbo.adReqLeadGroups rlg ON lgb.LeadGrpId = rlg.LeadGrpId
            INNER JOIN dbo.adReqsEffectiveDates ef ON rlg.adReqEffectiveDateId = ef.adReqEffectiveDateId
            INNER JOIN dbo.adReqs r ON ef.adReqId = r.adReqId
            INNER JOIN dbo.syStatuses s ON r.StatusId = s.StatusId
            INNER JOIN dbo.adReqTypes t ON r.adReqTypeId = t.adReqTypeId
            WHERE   r.ReqforEnrollment = 1
                    AND rlg.IsRequired = 1
                    AND s.StatusCode = 'A'
                    AND (
                          (
                            GETDATE() >= ef.StartDate
                            AND ef.EndDate IS NULL
                          )
                          OR ( GETDATE() BETWEEN ef.StartDate AND ef.EndDate )
                        ); 


        OPEN CHECK_LEADGROUP_LEVEL_COUNTS;
        FETCH NEXT FROM CHECK_LEADGROUP_LEVEL_COUNTS INTO @curReqId,@curType;

        WHILE @@FETCH_STATUS = 0
            BEGIN
                SET @IsApproved = 0;

                SET @ExpectedCount = @ExpectedCount + 1;
                SET @IsApproved = (
                                    SELECT  dbo.GetIsRequirementApprovedOrOverridden(@curReqId,@curType,@LeadId)
                                  );

                IF @IsApproved = 1
                    BEGIN
                        SET @ActualCount = @ActualCount + 1;
                    END;                

                FETCH NEXT FROM CHECK_LEADGROUP_LEVEL_COUNTS INTO @curReqId,@curType;

            END;

        CLOSE CHECK_LEADGROUP_LEVEL_COUNTS;
        DEALLOCATE CHECK_LEADGROUP_LEVEL_COUNTS;
		--------------------------------------------------------


		--------------------------------------------------------
		--Check Requirement Group Level Requirements
		--------------------------------------------------------
        DECLARE @CurGrpId UNIQUEIDENTIFIER;
        DECLARE @curNumReqs INT;

        DECLARE CHECK_REQGROUPS CURSOR
        FOR
            SELECT DISTINCT
                    ( grp.ReqGrpId ) AS curGrpId
                   ,lgrg.NumReqs AS curNumReqs
            FROM    dbo.adLeadGroups lg
            INNER JOIN dbo.adLeadByLeadGroups lgb ON lg.LeadGrpId = lgb.LeadGrpId
                                                     AND lgb.LeadId = @LeadId
            INNER JOIN dbo.adLeadGrpReqGroups lgrg ON lgb.LeadGrpId = lgrg.LeadGrpId
            INNER JOIN dbo.adReqGroups grp ON lgrg.ReqGrpId = grp.ReqGrpId;


        OPEN CHECK_REQGROUPS;
        FETCH NEXT FROM CHECK_REQGROUPS INTO @CurGrpId,@curNumReqs;

        SET @ExpectedCount = @ExpectedCount + @curNumReqs;

        DECLARE @numApproved INT = 0;

        WHILE @@FETCH_STATUS = 0
            BEGIN
					
                DECLARE CHECK_REQGROUPS_DETAILS CURSOR
                FOR
                    SELECT DISTINCT
                            ( r.adreqid ) AS curReqId
                           ,t.Descrip AS curType
                    FROM    adreqs r
                    INNER JOIN dbo.adReqTypes t ON r.adReqTypeId = t.adReqTypeId
                    INNER JOIN dbo.adReqsEffectiveDates ef ON r.adReqId = ef.adReqId
                    INNER JOIN dbo.adReqGrpDef grpdef ON r.adReqId = grpdef.adReqId
                    INNER JOIN dbo.adReqGroups grp ON grpdef.ReqGrpId = grp.ReqGrpId
                    WHERE   grpdef.ReqGrpId = @CurGrpId;

                SET @numApproved = 0;	

                OPEN CHECK_REQGROUPS_DETAILS;
                FETCH NEXT FROM CHECK_REQGROUPS_DETAILS INTO @curReqId,@curType;



                WHILE @@FETCH_STATUS = 0
                    BEGIN
                        SET @IsApproved = 0;

								
                        SET @IsApproved = (
                                            SELECT  dbo.GetIsRequirementApprovedOrOverridden(@curReqId,@curType,@LeadId)
                                          );

                        IF @IsApproved = 1
                            AND @numApproved <= @curNumReqs
                            BEGIN
                                SET @ActualCount = @ActualCount + 1;
                                SET @numApproved = @numApproved + 1;
                            END;                

                        FETCH NEXT FROM CHECK_REQGROUPS_DETAILS INTO @curReqId,@curType;
                    END;

                CLOSE CHECK_REQGROUPS_DETAILS;
                DEALLOCATE CHECK_REQGROUPS_DETAILS;

                FETCH NEXT FROM CHECK_REQGROUPS INTO @CurGrpId,@curNumReqs;
            END;				

        CLOSE CHECK_REQGROUPS;
        DEALLOCATE CHECK_REQGROUPS;
		






		--------------------------------------------------------
		-- Check is actual exceeds expected and output result
		--------------------------------------------------------
        SELECT  CASE WHEN @ActualCount >= @ExpectedCount THEN 1
                     ELSE 0
                END AS HasMetRequirments;
		--------------------------------------------------------
	


    END;
	

	

GO
