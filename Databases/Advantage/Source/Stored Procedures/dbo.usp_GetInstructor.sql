SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_GetInstructor]
AS
    SET NOCOUNT ON; 
    SELECT  InstructorID
           ,InstructorDescrip
    FROM    rptInstructor
    ORDER BY InstructorDescrip;



GO
