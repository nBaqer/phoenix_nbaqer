SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[USP_AR_GetCoursepreReqs]
    (
     @ClsSectID UNIQUEIDENTIFIER
    ,@PrgverID UNIQUEIDENTIFIER
    ,@CampusID UNIQUEIDENTIFIER
    )
AS /*---------------------------------------------------------------------------------------------------- 
 Author : Saraswathi Lakshmanan 
      
    Create date : 02/03/2010 
      
 Procedure Name : USP_AR_GetCoursepreReqs 
  
 Objective : To get the prerequisite Courses for a given Course 
   
 Parameters : Name Type Data Type Required? 
      ===== ==== ========= ========= 
      @ClsSectID In Varchar Required 
      @PrgverID In Varchar Required 
      @CampusID In varChar Required 
*/----------------------------------------------------------------------------------------------------- 
  
    BEGIN 
   
 --Select 
 -- arReqs.ReqID as PreCoReqId 
 --from 
 -- arReqs Inner Join arClassSections on arReqs.ReqId=arClassSections.ReqId 
 -- inner Join arProgVerDef on arReqs.ReqId=arProgVerDef.ReqId 
 -- inner Join syCmpGrpCmps on arReqs.CampGrpId=syCmpGrpCmps.CampGrpId 
 --where 
 -- arReqs.reqid in ( 
 -- Select PreCoreqId from arCourseReqs b inner Join arClassSections a on B.reqId=a.reqID 
 -- where (prgverId is Null or prgverid=@PrgverID) and b.CourseReqTypId = 1 
 -- ) 
 -- and arReqs.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' and 
 -- arReqs.ReqTypeId = 1 
 -- and arClassSections.ClsSectionId= @ClsSectID 
 -- and arProgVerDef.PrgVerId = @prgverID 
 -- and syCmpGrpCmps.CampusId = @CampusID 
  
  
        SELECT  arReqs.ReqID AS PreCoReqId
        FROM    arReqs
               ,arProgVerDef
        WHERE   arReqs.reqid IN ( SELECT    PreCoreqId
                                  FROM      arCourseReqs b
                                  WHERE     b.ReqId IN ( SELECT a.ReqId
                                                         FROM   arClassSections a
                                                         WHERE  a.ClsSectionId = @ClsSectID )
                                            AND (
                                                  prgverId IS NULL
                                                  OR prgverid = @PrgverID
                                                )
                                            AND b.CourseReqTypId = 1 )
                AND arReqs.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
                AND arReqs.ReqTypeId = 1
                AND (
                      arReqs.ReqId = arProgVerDef.ReqId
                      OR arReqs.ReqId IN ( SELECT   Reqid
                                           FROM     arReqGrpDef
                                           WHERE    GrpId IN ( SELECT   Reqid
                                                               FROM     arProgVerDef
                                                               WHERE    prgverid = @PrgverID ) )
                    )
                AND arProgVerDef.PrgVerId = @PrgverID
                AND arReqs.CampGrpId IN ( SELECT    CampGrpId
                                          FROM      syCmpGrpCmps
                                          WHERE     CampusId = @CampusID ); 
  
    END;



GO
