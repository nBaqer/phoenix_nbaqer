SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[usp_GetStartTimeForClassSectionMeetingwithPeriods]
    (
     @ClsSectMeetingID VARCHAR(50)
    )
AS
    BEGIN 
    
        SELECT  CONVERT(VARCHAR,CT.TimeIntervalDescrip,108)
        FROM    dbo.arClsSectMeetings CSM
               ,SyPeriods PR
               ,dbo.cmTimeInterval CT
        WHERE   CSM.PeriodId = PR.PeriodId
                AND CSM.ClsSectMeetingId = @ClsSectMeetingID
                AND PR.StartTimeId = CT.TimeIntervalId;                
                
    END;



GO
