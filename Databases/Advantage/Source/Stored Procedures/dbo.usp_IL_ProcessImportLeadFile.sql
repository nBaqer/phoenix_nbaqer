SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =========================================================================================================
-- usp_IL_ProcessImportLeadFile
-- =========================================================================================================
CREATE PROCEDURE [dbo].[usp_IL_ProcessImportLeadFile]
    @LeadId UNIQUEIDENTIFIER
   ,@FirstName VARCHAR(50)
   ,@LastName VARCHAR(50)
   ,@MiddleName VARCHAR(50)
   ,@SSN VARCHAR(50)
   ,@ModUser VARCHAR(50)
   ,@ModDate DATETIME
   ,@Phone VARCHAR(50)
   ,@HomeEmail VARCHAR(50)
   ,@Address1 VARCHAR(50)
   ,@Address2 VARCHAR(50)
   ,@City VARCHAR(50)
   ,@StateId UNIQUEIDENTIFIER
   ,@Zip VARCHAR(50)
   ,@LeadStatus UNIQUEIDENTIFIER
   ,@WorkEmail VARCHAR(50)
   ,@AddressType UNIQUEIDENTIFIER
   ,@Prefix UNIQUEIDENTIFIER
   ,@Suffix UNIQUEIDENTIFIER
   ,@BirthDate DATETIME
   ,@Sponsor UNIQUEIDENTIFIER
   ,@AdmissionsRep UNIQUEIDENTIFIER
   ,@AssignedDate DATETIME
   ,@Gender UNIQUEIDENTIFIER
   ,@Race UNIQUEIDENTIFIER
   ,@MaritalStatus UNIQUEIDENTIFIER
   ,@FamilyIncome UNIQUEIDENTIFIER
   ,@Children VARCHAR(50)
   ,@PhoneType UNIQUEIDENTIFIER
   ,@PhoneStatus UNIQUEIDENTIFIER
   ,@SourceCategoryId UNIQUEIDENTIFIER
   ,@SourceTypeID UNIQUEIDENTIFIER
   ,@SourceDate DATETIME
   ,@AreaID UNIQUEIDENTIFIER
   ,@ProgramID UNIQUEIDENTIFIER
   ,@ExpectedStart DATETIME
   ,@ShiftId UNIQUEIDENTIFIER
   ,@Nationality UNIQUEIDENTIFIER
   ,@Citizen UNIQUEIDENTIFIER
   ,@DrivLicStateId UNIQUEIDENTIFIER
   ,@DrivLicNumber VARCHAR(50)
   ,@AlienNumber VARCHAR(50)
   ,@Comments VARCHAR(240)
   ,@SourceAdvertisement UNIQUEIDENTIFIER
   ,@CampusId UNIQUEIDENTIFIER
   ,@PrgVerId UNIQUEIDENTIFIER
   ,@Country UNIQUEIDENTIFIER
   ,@County UNIQUEIDENTIFIER
   ,@PreviousEducation UNIQUEIDENTIFIER
   ,@AddressStatus UNIQUEIDENTIFIER
   ,@CreatedDate DATETIME
   ,@ForeignPhone BIT
   ,@ForeignZip BIT
   ,@LeadGrpId UNIQUEIDENTIFIER
   ,@DependencyTypeId UNIQUEIDENTIFIER
   ,@DegCertSeekingId UNIQUEIDENTIFIER
   ,@GeographicTypeId UNIQUEIDENTIFIER
   ,@HousingId UNIQUEIDENTIFIER
   ,@AdminCriteriaId UNIQUEIDENTIFIER
   ,@DateApplied DATETIME
   ,@AdvertisementNote VARCHAR(50)
   ,@Phone2 VARCHAR(50)
   ,@PhoneType2 UNIQUEIDENTIFIER
   ,@PhoneStatus2 UNIQUEIDENTIFIER
   ,@ForeignPhone2 VARCHAR(50)
   ,@DefaultPhone VARCHAR(50)
AS
    BEGIN
        DECLARE @Error AS INTEGER; 
        DECLARE @Message AS NVARCHAR(MAX);
        DECLARE @StudentIdDefault AS UNIQUEIDENTIFIER;

        DECLARE @ForeignPhoneForInsert BIT;  
        DECLARE @ForeignZipForInsert BIT;  
        DECLARE @ForeignPhone2ForInsert BIT;  
        
        DECLARE @DefaultAddressCode AS VARCHAR(12);
        DECLARE @StatusCodeActive AS VARCHAR(3);
        DECLARE @StatusIdActive AS UNIQUEIDENTIFIER;
        DECLARE @StatusCodeInactive AS VARCHAR(3);
        DECLARE @StatusIdInactive AS UNIQUEIDENTIFIER;
        DECLARE @CountryIdNational AS UNIQUEIDENTIFIER;
        DECLARE @HomeEmailTypeId UNIQUEIDENTIFIER;
        DECLARE @WorkEmailTypeId UNIQUEIDENTIFIER;
        DECLARE @DefaultPhoneTypeId AS UNIQUEIDENTIFIER;
        
        DECLARE @CountryCodeUSA AS VARCHAR(12);
        DECLARE @EMailTypeCodeHome AS VARCHAR(50);
        DECLARE @EMailTypeCodeWork AS VARCHAR(50);
        DECLARE @PhoneTypeCodeHome AS VARCHAR(12);
  
        SET @Message = '';
        SET @Error = 0;
        SET @StudentIdDefault = '00000000-0000-0000-0000-000000000000';
        SET @StatusCodeActive = 'A';
        SET @StatusCodeInactive = 'I';
        SET @CountryCodeUSA = 'USA';
        SET @DefaultAddressCode = 'Home'; 
        SET @EMailTypeCodeHome = 'Home';
        SET @EMailTypeCodeWork = 'Work';
        SET @PhoneTypeCodeHome = 'Home';

        IF @ForeignPhone = '0'
            BEGIN  
                SET @ForeignPhoneForInsert = 0;  
            END;  
        ELSE
            BEGIN  
                SET @ForeignPhoneForInsert = 1;  
            END;  
        IF @ForeignZip = '0'
            BEGIN  
                SET @ForeignZipForInsert = 0;  
            END;  
        ELSE
            BEGIN  
                SET @ForeignZipForInsert = 1;  
            END;   
        IF @ForeignPhone2 = '0'
            BEGIN  
                SET @ForeignPhone2ForInsert = 0;  
            END;  
        ELSE
            BEGIN  
                SET @ForeignPhone2ForInsert = 1;  
            END;  
        SET @StatusIdActive = (
                                SELECT TOP 1
                                        SS.StatusId
                                FROM    syStatuses AS SS
                                WHERE   SS.StatusCode = @StatusCodeActive
                              );
        SET @StatusIdInactive = (
                                  SELECT TOP 1
                                            SS.StatusId
                                  FROM      syStatuses AS SS
                                  WHERE     SS.StatusCode = @StatusCodeInactive
                                );
        SET @CountryIdNational = (
                                   SELECT TOP 1
                                            AC.CountryId
                                   FROM     adCountries AS AC
                                   WHERE    AC.CountryCode = @CountryCodeUSA
                                 );
        IF NOT EXISTS ( SELECT  1
                        FROM    plAddressTypes AS PAT
                        WHERE   PAT.AddressTypeId = @AddressType )
            BEGIN
                SELECT  @AddressType = PAT.AddressTypeId
                FROM    plAddressTypes AS PAT
                WHERE   PAT.AddressCode = @DefaultAddressCode;
            END;                                          
        SET @HomeEmailTypeId = (
                                 SELECT TOP 1
                                        SETY.EMailTypeId
                                 FROM   syEmailType AS SETY
                                 WHERE  SETY.EMailTypeCode = @EMailTypeCodeHome
                               );
        SET @WorkEmailTypeId = (
                                 SELECT TOP 1
                                        SETY.EMailTypeId
                                 FROM   syEmailType AS SETY
                                 WHERE  SETY.EMailTypeCode = @EMailTypeCodeWork
                               );
        SET @DefaultPhoneTypeId = (
                                    SELECT TOP 1
                                            SPT.PhoneTypeId
                                    FROM    syPhoneType AS SPT
                                    WHERE   SPT.PhoneTypeCode = @PhoneTypeCodeHome
                                  );

        SET @Address1 = ISNULL(@Address1,'');
        SET @Address2 = ISNULL(@Address2,'');
        SET @City = ISNULL(@City,''); 
        SET @Zip = ISNULL(@Zip,'');
        SET @Country = ISNULL(@Country,@CountryIdNational);
        SET @HomeEmail = ISNULL(@HomeEmail,'');
        SET @WorkEmail = ISNULL(@WorkEmail,'');
        SET @PhoneType = ISNULL(@PhoneType,@DefaultPhoneTypeId);
        SET @Phone = ISNULL(@Phone,'');
        SET @PhoneStatus = ISNULL(@PhoneStatus,@StatusIdActive);
        SET @PhoneType2 = ISNULL(@PhoneType2,@DefaultPhoneTypeId);
        SET @Phone2 = ISNULL(@Phone2,'');
        SET @PhoneStatus2 = ISNULL(@PhoneStatus2,@StatusIdActive);
        SET @ModUser = ISNULL(@ModUser,'ImportLeadUser');
        SET @ModDate = ISNULL(@ModDate,GETDATE());
        SET @Message = '';
        SET @Error = 0;

        BEGIN TRANSACTION ImportLead; 
        BEGIN TRY 
            IF ( @Error = 0 )               -- Insert Lead
                BEGIN                       -- Insert Lead
                    INSERT  INTO adLeads
                            (
                             LeadId
                            ,ProspectID
                            ,FirstName
                            ,LastName
                            ,MiddleName
                            ,SSN
                            ,ModUser
                            ,ModDate
                            ,Phone
                            ,HomeEmail
                            ,Address1
                            ,Address2
                            ,City
                            ,StateId
                            ,Zip
                            ,LeadStatus
                            ,WorkEmail
                            ,AddressType
                            ,Prefix
                            ,Suffix
                            ,BirthDate
                            ,Sponsor
                            ,AdmissionsRep
                            ,AssignedDate
                            ,Gender
                            ,Race
                            ,MaritalStatus
                            ,FamilyIncome
                            ,Children
                            ,PhoneType
                            ,PhoneStatus
                            ,SourceCategoryID
                            ,SourceTypeID
                            ,SourceDate
                            ,AreaID
                            ,ProgramID
                            ,ExpectedStart
                            ,ShiftID
                            ,Nationality
                            ,Citizen
                            ,DrivLicStateID
                            ,DrivLicNumber
                            ,AlienNumber
                            ,Comments
                            ,SourceAdvertisement
                            ,CampusId
                            ,PrgVerId
                            ,Country
                            ,County
                            ,Age
                            ,PreviousEducation
                            ,AddressStatus
                            ,CreatedDate
                            ,RecruitmentOffice
                            ,OtherState
                            ,ForeignPhone
                            ,ForeignZip
                            ,LeadgrpId
                            ,DependencyTypeId
                            ,DegCertSeekingId
                            ,GeographicTypeId
                            ,HousingId
                            ,admincriteriaid
                            ,DateApplied
                            ,InquiryTime
                            ,AdvertisementNote
                            ,Phone2
                            ,PhoneType2
                            ,PhoneStatus2
                            ,ForeignPhone2
                            ,DefaultPhone
                            ,CampaignId
                            ,ProgramOfInterest
                            ,CampusOfInterest
                            ,TransportationId
                            ,NickName
                            ,AttendTypeId
                            ,PreferredContactId
                            ,AddressApt
                            ,NoneEmail
                            ,HighSchoolId
                            ,HighSchoolGradDate
                            ,AttendingHs
                            ,IsDisabled
                            ,ProgramScheduleId
                            ,BestTime
                            ,DistanceToSchool
                            ,IsFirstTimeInSchool
                            ,IsFirstTimePostSecSchool
                            ,EntranceInterviewDate
                            ,StudentId
                            ,StudentNumber
                            ,StudentStatusId
                            ,ReasonNotEnrolledId
                            )
                    VALUES  (
                             @LeadId
                            ,''             -- ProspectID - varchar(50)
                            ,@FirstName
                            ,@LastName
                            ,@MiddleName
                            ,@SSN
                            ,@ModUser
                            ,@ModDate
                            ,''     -- @Phone
                            ,''     -- @HomeEmail
                            ,''     -- @Address1
                            ,''     -- @Address2
                            ,''     -- @City
                            ,NULL   -- @StateId
                            ,''     -- @Zip
                            ,@LeadStatus
                            ,''     -- @WorkEmail
                            ,NULL   -- @AddressType
                            ,@Prefix
                            ,@Suffix
                            ,@BirthDate
                            ,@Sponsor
                            ,@AdmissionsRep
                            ,@AssignedDate
                            ,@Gender
                            ,@Race
                            ,@MaritalStatus
                            ,@FamilyIncome
                            ,@Children
                            ,NULL   -- @PhoneType
                            ,NULL   -- @PhoneStatus
                            ,@SourceCategoryId
                            ,@SourceTypeID
                            ,@SourceDate
                            ,@AreaID
                            ,@ProgramID
                            ,@ExpectedStart
                            ,@ShiftId
                            ,@Nationality
                            ,@Citizen
                            ,@DrivLicStateId
                            ,@DrivLicNumber
                            ,@AlienNumber
                            ,@Comments
                            ,@SourceAdvertisement
                            ,@CampusId
                            ,@PrgVerId
                            ,NULL   -- @Country
                            ,NULL   -- @County
                            ,''             -- Age - varchar(3)
                            ,@PreviousEducation
                            ,NULL   -- @AddressStatus
                            ,@CreatedDate
                            ,''             -- RecruitmentOffice - varchar(50)
                            ,''             -- OtherState - varchar(50)
                            ,0   -- @ForeignPhoneForInsert
                            ,@ForeignZipForInsert
                            ,@LeadGrpId
                            ,@DependencyTypeId
                            ,@DegCertSeekingId
                            ,@GeographicTypeId
                            ,@HousingId
                            ,@AdminCriteriaId
                            ,@DateApplied
                            ,''  -- InquiryTime - varchar(50)
                            ,@AdvertisementNote
                            ,''     -- @Phone2
                            ,NULL   -- @PhoneType2
                            ,NULL   -- @PhoneStatus2
                            ,NULL   -- @ForeignPhone2ForInsert
                            ,0      -- @DefaultPhone  
                            ,NULL           -- CampaignId - int
                            ,''             -- ProgramOfInterest - varchar(100)
                            ,''             -- CampusOfInterest - varchar(100)
                            ,NULL           -- TransportationId - uniqueidentifier
                            ,''             -- NickName - varchar(50)
                            ,NULL           -- AttendTypeId - uniqueidentifier
                            ,0              -- PreferredContactId - int
                            ,''             -- AddressApt - varchar(20)
                            ,0              -- NoneEmail - bit
                            ,NULL           -- HighSchoolId - uniqueidentifier
                            ,GETDATE()      -- HighSchoolGradDate - datetime
                            ,NULL           -- AttendingHs - bit
                            ,NULL           -- IsDisabled - bit
                            ,NULL           -- ProgramScheduleId - uniqueidentifier
                            ,''             -- BestTime - varchar(20)
                            ,0              -- DistanceToSchool - int
                            ,0              -- IsFirstTimeInSchool - bit
                            ,0              -- IsFirstTimePostSecSchool - bit
                            ,GETDATE()      -- EntranceInterviewDate - datetime
                            ,@StudentIdDefault           -- StudentId - uniqueidentifier
                            ,N''            -- StudentNumber - nvarchar(50)
                            ,@StatusIdInactive           -- StudentStatusId - uniqueidentifier
                            ,NULL           -- ReasonNotEnrolledId - uniqueidentifier
                            );   
                    
                    IF ( @@ERROR = 0 )
                        BEGIN
                            SET @Error = 0;  
                        END;
                    ELSE
                        BEGIN
                            SET @Error = 1;
                            SET @Message = @Message + 'Failed inserting adLead ' + CONVERT(VARCHAR(50),@LeadId) + CHAR(10);
                        END;
                END;
            IF ( @Error = 0 )               -- insert adLeadAddresses
                BEGIN                       -- insert adLeadAddresses
                    IF ( @Address1 <> '' )
                        BEGIN               -- insert adLeadAddresses
                            INSERT  INTO adLeadAddresses
                                    (
                                     adLeadAddressId
                                    ,LeadId
                                    ,AddressTypeId
                                    ,Address1
                                    ,Address2
                                    ,City
                                    ,StateId
                                    ,ZipCode
                                    ,CountryId
                                    ,StatusId
                                    ,IsMailingAddress
                                    ,IsShowOnLeadPage
                                    ,ModDate
                                    ,ModUser
                                    ,State
                                    ,IsInternational
                                    ,CountyId
                                    ,ForeignCountyStr
                                    ,ForeignCountryStr
                                    ,AddressApto
                                    )
                            VALUES  (
                                     NEWID()                        -- adLeadAddressId - uniqueidentifier
                                    ,@LeadId                        -- LeadId - uniqueidentifier
                                    ,@AddressType                   -- AddressTypeId - uniqueidentifier
                                    ,@Address1                      -- Address1 - varchar(250)
                                    ,@Address2                      -- Address2 - varchar(250)
                                    ,@City                          -- City - varchar(250)
                                    ,@StateId                       -- StateId - uniqueidentifier
                                    ,@Zip                           -- ZipCode - varchar(10)
                                    ,@Country                       -- CountryId - uniqueidentifier
                                    ,@StatusIdActive                -- StatusId - uniqueidentifier
                                    ,1                              -- IsMailingAddress - bit
                                    ,1                              -- IsShowOnLeadPage - bit
                                    ,@ModDate                       -- ModDate - datetime
                                    ,@ModUser                       -- ModUser - varchar(50)
                                    ,''                             -- State - varchar(100)
                                    ,CASE WHEN ( @Country = @CountryIdNational ) THEN 0
                                          ELSE 1
                                     END                            -- IsInternational - bit
                                    ,@County                        -- CountyId - uniqueidentifier
                                    ,''                             -- ForeignCountyStr - varchar(100)
                                    ,''                             -- ForeignCountryStr - varchar(100)
                                    ,''                             -- AddressApto - varchar(20)
                                    );
               
                            IF ( @@ERROR = 0 )
                                BEGIN
                                    SET @Error = 0;  
                                END;
                            ELSE
                                BEGIN
                                    SET @Error = 1;
                                    SET @Message = @Message + 'Failed inserting adLeadAddreses ' + CONVERT(VARCHAR(50),@LeadId) + CHAR(10);
                                END;
                        END;
                END;
            IF ( @Error = 0 )               -- insert adLeadEmail HomeEmail
                BEGIN                       -- insert adLeadEmail HomeEmail
                    IF ( @HomeEmail <> '' )
                        BEGIN               -- insert adLeadEmail HomeEmail
                            INSERT  INTO AdLeadEmail
                                    (
                                     LeadEMailId
                                    ,LeadId
                                    ,EMail
                                    ,EMailTypeId
                                    ,IsPreferred
                                    ,IsPortalUserName
                                    ,ModUser
                                    ,ModDate
                                    ,StatusId
                                    ,IsShowOnLeadPage
                                    )
                            VALUES  (
                                     NEWID()            -- LeadEMailId - uniqueidentifier
                                    ,@LeadId            -- LeadId - uniqueidentifier
                                    ,@HomeEmail         -- EMail - varchar(100)
                                    ,@HomeEmailTypeId   -- EMailTypeId - uniqueidentifier
                                    ,1                  -- IsPreferred - bit
                                    ,0                  -- IsPortalUserName - bit
                                    ,@ModUser           -- ModUser - varchar(50)
                                    ,@ModDate           -- ModDate - datetime
                                    ,@StatusIdActive    -- StatusId - uniqueidentifier
                                    ,1                  -- IsShowOnLeadPage - bit
                                    );
                            
                            IF ( @@ERROR = 0 )
                                BEGIN
                                    SET @Error = 0;  
                                END;
                            ELSE
                                BEGIN
                                    SET @Error = 1;
                                    SET @Message = @Message + 'Failed inserting adLeadEmail  HomeEmail ' + CONVERT(VARCHAR(50),@LeadId) + CHAR(10);
                                END; 
                        END;
                END;
            IF ( @Error = 0 )               -- insert adLeadEmail WorkEmail
                BEGIN                       -- insert adLeadEmail WorkEmail
                    IF ( @WorkEmail <> '' )
                        BEGIN               -- insert adLeadEmail WorkEmail
                            INSERT  INTO AdLeadEmail
                                    (
                                     LeadEMailId
                                    ,LeadId
                                    ,EMail
                                    ,EMailTypeId
                                    ,IsPreferred
                                    ,IsPortalUserName
                                    ,ModUser
                                    ,ModDate
                                    ,StatusId
                                    ,IsShowOnLeadPage
                                    )
                            VALUES  (
                                     NEWID()            -- LeadEMailId - uniqueidentifier
                                    ,@LeadId            -- LeadId - uniqueidentifier
                                    ,@WorkEmail         -- EMail - varchar(100)
                                    ,@WorkEmailTypeId   -- EMailTypeId - uniqueidentifier
                                    ,CASE WHEN ISNULL(@HomeEmail,'') = '' THEN 1
                                          ELSE 0
                                     END                -- IsPreferred - bit
                                    ,0                  -- IsPortalUserName - bit
                                    ,@ModUser           -- ModUser - varchar(50)
                                    ,@ModDate           -- ModDate - datetime
                                    ,@StatusIdActive    -- StatusId - uniqueidentifier
                                    ,1                  -- IsShowOnLeadPage - bit
                                    );
                            
                            IF ( @@ERROR = 0 )
                                BEGIN
                                    SET @Error = 0;  
                                END;
                            ELSE
                                BEGIN
                                    SET @Error = 1;
                                    SET @Message = @Message + 'Failed inserting adLeadEmail  WorkEmail ' + CONVERT(VARCHAR(50),@LeadId) + CHAR(10);
                                END; 
                        END;
                END;
            IF ( @Error = 0 )               -- insert adLeadPhone  Phone
                BEGIN                       -- insert adLeadPhone  Phone
                    IF ( @Phone <> '' )
                        BEGIN               -- insert adLeadPhone  Phone
                            INSERT  INTO adLeadPhone
                                    (
                                     LeadPhoneId
                                    ,LeadId
                                    ,PhoneTypeId
                                    ,Phone
                                    ,ModDate
                                    ,ModUser
                                    ,Position
                                    ,Extension
                                    ,IsForeignPhone
                                    ,IsBest
                                    ,IsShowOnLeadPage
                                    ,StatusId
                                    )
                            VALUES  (
                                     NEWID()                    -- LeadPhoneId - uniqueidentifier
                                    ,@LeadId                    -- LeadId - uniqueidentifier
                                    ,@PhoneType                 -- PhoneTypeId - uniqueidentifier
                                    ,@Phone                     -- Phone - varchar(50)
                                    ,@ModDate                   -- ModDate - datetime
                                    ,@ModUser                   -- ModUser - varchar(50)
                                    ,1                          -- Position - int
                                    ,''                         -- Extension - varchar(10)
                                    ,@ForeignPhoneForInsert     -- IsForeignPhone - bit
                                    ,0                          -- IsBest - bit Default value = 0
                                    ,0                          -- IsShowOnLeadPage - bit Default value = 0
                                    ,@PhoneStatus               -- StatusId - uniqueidentifier
                                    );
                            
                            IF ( @@ERROR = 0 )
                                BEGIN
                                    SET @Error = 0;  
                                END;
                            ELSE
                                BEGIN
                                    SET @Error = 1;
                                    SET @Message = @Message + 'Failed inserting adLeadPhone  Phone ' + CONVERT(VARCHAR(50),@LeadId) + CHAR(10);
                                END; 
                        END;
                END;
            IF ( @Error = 0 )               -- insert adLeadPhone  Phone2
                BEGIN                       -- insert adLeadPhone  Phone2
                    IF ( @Phone2 <> '' )
                        BEGIN               -- insert adLeadPhone  Phone2
                            INSERT  INTO adLeadPhone
                                    (
                                     LeadPhoneId
                                    ,LeadId
                                    ,PhoneTypeId
                                    ,Phone
                                    ,ModDate
                                    ,ModUser
                                    ,Position
                                    ,Extension
                                    ,IsForeignPhone
                                    ,IsBest
                                    ,IsShowOnLeadPage
                                    ,StatusId
                                    )
                            VALUES  (
                                     NEWID()                    -- LeadPhoneId - uniqueidentifier
                                    ,@LeadId                    -- LeadId - uniqueidentifier
                                    ,@PhoneType2                -- PhoneTypeId - uniqueidentifier
                                    ,@Phone2                    -- Phone - varchar(50)
                                    ,@ModDate                   -- ModDate - datetime
                                    ,@ModUser                   -- ModUser - varchar(50)
                                    ,CASE WHEN ( @Phone = '' )  --    AND ( @CurDefaultPhone IS NULL OR @CurDefaultPhone = 2 ) 
                                               THEN 1
                                          ELSE 2
                                     END                        -- Position - int
                                    ,''                         -- Extension - varchar(10)
                                    ,@ForeignPhone2ForInsert    -- IsForeignPhone - bit
                                    ,0                          -- IsBest - bit Default value = 0
                                    ,0                          -- IsShowOnLeadPage - bit Default value = 0
                                    ,@PhoneStatus2              -- StatusId - uniqueidentifier
                                    ); 
                            
                            IF ( @@ERROR = 0 )
                                BEGIN
                                    SET @Error = 0;  
                                END;
                            ELSE
                                BEGIN
                                    SET @Error = 1;
                                    SET @Message = @Message + 'Failed inserting adLeadPhone  Phone2 ' + CONVERT(VARCHAR(50),@LeadId) + CHAR(10);
                                END; 
                        END;
                END;
            IF ( @Error = 0 )               -- insert adLeadByLeadGroups
                BEGIN
                    IF @LeadGrpId IS NOT NULL
                        BEGIN
                            INSERT  INTO adLeadByLeadGroups
                                    (
                                     LeadGrpLeadId
                                    ,LeadId
                                    ,LeadGrpId
                                    ,ModDate
                                    ,ModUser
                                    )
                            VALUES  (
                                     NEWID()
                                    ,@LeadId
                                    ,@LeadGrpId
                                    ,@ModDate
                                    ,@ModUser    
                                    );

                            IF ( @@ERROR = 0 )
                                BEGIN
                                    SET @Error = 0;  
                                END;
                            ELSE
                                BEGIN
                                    SET @Error = 1;
                                    SET @Message = @Message + 'Failed inserting adLeadByLeadGroups ' + CONVERT(VARCHAR(50),@LeadId) + CHAR(10);
                                END; 
                        END;
                END;
-- To Test
--  SET @Error = 1
            IF ( @Error = 0 )
                BEGIN
                    COMMIT TRANSACTION ImportLead;
                    SET @Message = @Message + 'Successful Transsaction -- COMMIT TRANSACTION ' + CONVERT(VARCHAR(50),@LeadId) + CHAR(10);
                END;
            ELSE
                BEGIN
                    ROLLBACK TRANSACTION ImportLead;
                    SET @Message = @Message + 'Failed Transaction -- ROLLBACK TRANSACTION ' + CONVERT(VARCHAR(50),@LeadId) + CHAR(10);  
                    SELECT  @Message;   
                END;
        END TRY
        BEGIN CATCH
            ROLLBACK TRANSACTION ImportLead;  
            SET @Message = @Message + 'Failed transsaction--  ROLLBACK TRANSACTION  Catching error ' + CONVERT(VARCHAR(50),@LeadId) + CHAR(10);    
            SELECT  ERROR_NUMBER() AS ErrorNumber
                   ,ERROR_SEVERITY() AS ErrorSeverity
                   ,ERROR_STATE() AS ErrorState
                   ,ERROR_PROCEDURE() AS ErrorProcedure
                   ,ERROR_LINE() AS ErrorLine
                   ,ERROR_MESSAGE() AS ErrorMessage;   
--            PRINT @Message;
        END CATCH;
--            PRINT @Message
    END;
-- =========================================================================================================
-- usp_IL_ProcessImportLeadFile
-- =========================================================================================================

GO
