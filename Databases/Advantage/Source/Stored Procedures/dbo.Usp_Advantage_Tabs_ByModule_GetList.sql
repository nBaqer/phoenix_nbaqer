SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[Usp_Advantage_Tabs_ByModule_GetList]
    @SchoolEnumerator INT
   ,@campusId UNIQUEIDENTIFIER
AS
    BEGIN    
 -- declare local variables    
        DECLARE @ShowRossOnlyTabs BIT
           ,@SchedulingMethod VARCHAR(50)
           ,@TrackSAPAttendance VARCHAR(50)
           ,@PostServicesByStudent BIT
           ,@PostServicesByClass BIT;    
        DECLARE @ShowCollegeOfCourtReporting VARCHAR(5)
           ,@FameESP VARCHAR(5)
           ,@EdExpress VARCHAR(5);     
        DECLARE @GradeBookWeightingLevel VARCHAR(20)
           ,@ShowExternshipTabs VARCHAR(5);    
	 
 -- Get Values    
        IF (
             SELECT COUNT(*)
             FROM   syConfigAppSetValues
             WHERE  SettingId = 68
                    AND CampusId = @campusId
           ) >= 1
            BEGIN  
                SET @ShowRossOnlyTabs = (
                                          SELECT    VALUE
                                          FROM      dbo.syConfigAppSetValues
                                          WHERE     SettingId = 68
                                                    AND CampusId = @campusId
                                        );    
		  
            END;   
        ELSE
            BEGIN  
                SET @ShowRossOnlyTabs = (
                                          SELECT    VALUE
                                          FROM      dbo.syConfigAppSetValues
                                          WHERE     SettingId = 68
                                                    AND CampusId IS NULL
                                        );  
            END;  
   
		--PRINT @ShowRossOnlyTabs  
	  
        IF (
             SELECT COUNT(*)
             FROM   syConfigAppSetValues
             WHERE  SettingId = 65
                    AND CampusId = @campusId
           ) >= 1
            BEGIN  
                SET @SchedulingMethod = (
                                          SELECT    VALUE
                                          FROM      dbo.syConfigAppSetValues
                                          WHERE     SettingId = 65
                                                    AND CampusId = @campusId
                                        );    
		  
            END;   
        ELSE
            BEGIN  
                SET @SchedulingMethod = (
                                          SELECT    VALUE
                                          FROM      dbo.syConfigAppSetValues
                                          WHERE     SettingId = 65
                                                    AND CampusId IS NULL
                                        );  
            END;                        
								  
	  
									
        IF (
             SELECT COUNT(*)
             FROM   syConfigAppSetValues
             WHERE  SettingId = 72
                    AND CampusId = @campusId
           ) >= 1
            BEGIN  
                SET @TrackSAPAttendance = (
                                            SELECT  VALUE
                                            FROM    dbo.syConfigAppSetValues
                                            WHERE   SettingId = 72
                                                    AND CampusId = @campusId
                                          );    
		  
            END;   
        ELSE
            BEGIN  
                SET @TrackSAPAttendance = (
                                            SELECT  VALUE
                                            FROM    dbo.syConfigAppSetValues
                                            WHERE   SettingId = 72
                                                    AND CampusId IS NULL
                                          );  
            END;   
		 
											 
        IF (
             SELECT COUNT(*)
             FROM   syConfigAppSetValues
             WHERE  SettingId = 118
                    AND CampusId = @campusId
           ) >= 1
            BEGIN  
                SET @ShowCollegeOfCourtReporting = (
                                                     SELECT VALUE
                                                     FROM   dbo.syConfigAppSetValues
                                                     WHERE  SettingId = 118
                                                            AND CampusId = @campusId
                                                   );    
		  
            END;   
        ELSE
            BEGIN  
                SET @ShowCollegeOfCourtReporting = (
                                                     SELECT VALUE
                                                     FROM   dbo.syConfigAppSetValues
                                                     WHERE  SettingId = 118
                                                            AND CampusId IS NULL
                                                   );  
            END;   
	   
						 
        IF (
             SELECT COUNT(*)
             FROM   syConfigAppSetValues
             WHERE  SettingId = 37
                    AND CampusId = @campusId
           ) >= 1
            BEGIN  
                SET @FameESP = (
                                 SELECT VALUE
                                 FROM   dbo.syConfigAppSetValues
                                 WHERE  SettingId = 37
                                        AND CampusId = @campusId
                               );    
		  
            END;   
        ELSE
            BEGIN  
                SET @FameESP = (
                                 SELECT VALUE
                                 FROM   dbo.syConfigAppSetValues
                                 WHERE  SettingId = 37
                                        AND CampusId IS NULL
                               );  
            END;   
	   
						   
        IF (
             SELECT COUNT(*)
             FROM   syConfigAppSetValues
             WHERE  SettingId = 91
                    AND CampusId = @campusId
           ) >= 1
            BEGIN  
                SET @EdExpress = (
                                   SELECT   VALUE
                                   FROM     dbo.syConfigAppSetValues
                                   WHERE    SettingId = 91
                                            AND CampusId = @campusId
                                 );    
		  
            END;   
        ELSE
            BEGIN  
                SET @EdExpress = (
                                   SELECT   VALUE
                                   FROM     dbo.syConfigAppSetValues
                                   WHERE    SettingId = 91
                                            AND CampusId IS NULL
                                 );  
            END;  
		  
									 
										 
        IF (
             SELECT COUNT(*)
             FROM   syConfigAppSetValues
             WHERE  SettingId = 43
                    AND CampusId = @campusId
           ) >= 1
            BEGIN  
                SET @GradeBookWeightingLevel = (
                                                 SELECT VALUE
                                                 FROM   dbo.syConfigAppSetValues
                                                 WHERE  SettingId = 43
                                                        AND CampusId = @campusId
                                               );    
		  
            END;   
        ELSE
            BEGIN  
                SET @GradeBookWeightingLevel = (
                                                 SELECT VALUE
                                                 FROM   dbo.syConfigAppSetValues
                                                 WHERE  SettingId = 43
                                                        AND CampusId IS NULL
                                               );  
            END;  
		 
									
									
        IF (
             SELECT COUNT(*)
             FROM   syConfigAppSetValues
             WHERE  SettingId = 71
                    AND CampusId = @campusId
           ) >= 1
            BEGIN  
                SET @ShowExternshipTabs = (
                                            SELECT  VALUE
                                            FROM    dbo.syConfigAppSetValues
                                            WHERE   SettingId = 71
                                                    AND CampusId = @campusId
                                          );    
		  
            END;   
        ELSE
            BEGIN  
                SET @ShowExternshipTabs = (
                                            SELECT  VALUE
                                            FROM    dbo.syConfigAppSetValues
                                            WHERE   SettingId = 71
                                                    AND CampusId IS NULL
                                          );  
            END;  
			  
        SET @PostServicesByStudent = (
                                       SELECT   VALUE
                                       FROM     dbo.syConfigAppSetValues
                                       WHERE    SettingId = (
                                                              SELECT TOP 1
                                                                        SettingId
                                                              FROM      dbo.syConfigAppSettings
                                                              WHERE     KeyName = 'PostServicesByStudent'
                                                            )
                                                AND CampusId IS NULL
                                     );  
        SET @PostServicesByClass = (
                                     SELECT VALUE
                                     FROM   dbo.syConfigAppSetValues
                                     WHERE  SettingId = (
                                                          SELECT TOP 1
                                                                    SettingId
                                                          FROM      dbo.syConfigAppSettings
                                                          WHERE     KeyName = 'PostServicesByClass'
                                                        )
                                            AND CampusId IS NULL
                                   ); 
							     	 
  -- Create a temptable for FinancialAid-StudentPages  
        SELECT  *
        INTO    #tmpFA_StudentPages
        FROM    (
                  SELECT DISTINCT
                            NNChild.ResourceId AS ChildResourceId
                           ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                 ELSE RChild.Resource
                            END AS ChildResource
                           ,RChild.ResourceURL AS ChildResourceURL
                           ,CASE WHEN (
                                        NNParent.ResourceId = 191
                                        OR NNChild.ResourceId IN ( 737,738,739,740,741,742 )
                                      ) THEN NULL
                                 ELSE NNParent.ResourceId
                            END AS ParentResourceId
                           ,RParent.Resource AS ParentResource
                           ,RParentModule.Resource AS MODULE    
	--NNParent.ParentId    
                  FROM      syResources RChild
                  INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
                  INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                  INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                  LEFT OUTER JOIN (
                                    SELECT  *
                                    FROM    syResources
                                    WHERE   ResourceTypeId = 1
                                  ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                  WHERE     RChild.ResourceTypeId IN ( 2,3,8 )
                            AND (
                                  RChild.ChildTypeId IS NULL
                                  OR RChild.ChildTypeId = 3
                                )
                            AND ( NNParent.ParentId IN ( SELECT HierarchyId
                                                         FROM   syNavigationNodes
                                                         WHERE  ResourceId = 394
                                                                AND ParentId IN ( SELECT    HierarchyId
                                                                                  FROM      syNavigationNodes
                                                                                  WHERE     ResourceId = 191 ) ) )
                            AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
                            AND  
										-- Hide resources based on Configuration Settings    
			   -- Hide resources based on Configuration Settings    
                            (
                              -- The following expression means if showross... is set to false, hide     
		   -- ResourceIds 541,542,532,534,535,538,543,539    
		   -- (Not False) OR (Condition)   
		   -- US4330 removed Resourceid 541 
							  (
                                (
                                  NOT LTRIM(RTRIM(@ShowRossOnlyTabs)) = 0
                                )
                                OR ( NNChild.ResourceId NOT IN ( 542,532,
														  --    534, 535, Commented on August 01 2012 for de8041
                                                                 538 --DECommented for DE8204--    , 543 
															   ) )
                              )
                              AND    
		   -- The following expression means if showross... is set to true, hide     
		   -- ResourceIds 142,375,330,476,508,102,107,237    
		   -- (Not True) OR (Condition)    
                              (
                                (
                                  NOT LTRIM(RTRIM(@ShowRossOnlyTabs)) = 1
                                )
                                OR ( NNChild.ResourceId NOT IN ( 142,375,330,476,508,102,107,237,230,286,200,217,290 ) )
                              )
                              AND   
		   -- US4330 added		  
		   -- The following expression means if PostServices is set to false or showross is set to false, hide           
		   -- ResourceId 541 , 508   
		   -- (Not False) OR (Condition)      
                              (
                                (
                                  (
                                    NOT LTRIM(RTRIM(@PostServicesByStudent)) = 0
                                  )
                                  OR ( NNChild.ResourceId NOT IN ( 541 ) )
                                )
                                OR (
                                     (
                                       NOT LTRIM(RTRIM(@ShowRossOnlyTabs)) = 0
                                     )
                                     OR ( NNChild.ResourceId NOT IN ( 541 ) )
                                   )
                              )
                              AND (
                                    (
                                      (
                                        NOT LTRIM(RTRIM(@PostServicesByClass)) = 0
                                      )
                                      OR ( NNChild.ResourceId NOT IN ( 508 ) )
                                    )
                                    OR (
                                         (
                                           NOT LTRIM(RTRIM(@ShowRossOnlyTabs)) = 0
                                         )
                                         OR ( NNChild.ResourceId NOT IN ( 508 ) )
                                       )
                                  )
                              AND 
						  						  
		   ---- The following expression means if SchedulingMethod=regulartraditional, hide     
		   ---- ResourceIds 91 and 497    
		   ---- (Not True) OR (Condition)    
                              (
                                (
                                  NOT LOWER(LTRIM(RTRIM(@SchedulingMethod))) = 'regulartraditional'
                                )
                                OR ( NNChild.ResourceId NOT IN ( 91,497 ) )
                              )
                              AND    
		   -- The following expression means if TrackSAPAttendance=byday, hide     
		   -- ResourceIds 633    
		   -- (Not True) OR (Condition)    
                              (
                                (
                                  NOT LOWER(LTRIM(RTRIM(@TrackSAPAttendance))) = 'byday'
                                )
                                OR ( NNChild.ResourceId NOT IN ( 633,327 ) )
                              )
                              AND (
                                    (
                                      NOT LOWER(LTRIM(RTRIM(@TrackSAPAttendance))) = 'byclass'
                                    )
                                    OR ( NNChild.ResourceId NOT IN ( 539 ) )
                                  )
                              AND    
		   ---- The following expression means if @ShowCollegeOfCourtReporting is set to false, hide     
		   ---- ResourceIds 614,615    
		   ---- (Not False) OR (Condition)    
                              (
                                (
                                  NOT LOWER(LTRIM(RTRIM(@ShowCollegeOfCourtReporting))) = 'no'
                                )
                                OR ( NNChild.ResourceId NOT IN ( 614,615 ) )
                              )
                              AND    
		   -- The following expression means if @ShowCollegeOfCourtReporting is set to true, hide     
		   -- ResourceIds 497    
		   -- (Not True) OR (Condition)    
                              (
                                (
                                  NOT LOWER(LTRIM(RTRIM(@ShowCollegeOfCourtReporting))) = 'yes'
                                )
                                OR ( NNChild.ResourceId NOT IN ( 497 ) )
                              )
                              AND    
		   -- The following expression means if FAMEESP is set to false, hide     
		   -- ResourceIds 517,523, 525    
		   -- (Not False) OR (Condition)    
                              (
                                (
                                  NOT LOWER(LTRIM(RTRIM(@FameESP))) = 'no'
                                )
                                OR ( NNChild.ResourceId NOT IN ( 517,523,525 ) )
                              )
                              AND    
		   ---- The following expression means if EDExpress is set to false, hide     
		   ---- ResourceIds 603,604,606,619    
		   ---- (Not False) OR (Condition)    
                              (
                                (
                                  NOT LOWER(LTRIM(RTRIM(@EdExpress))) = 'no'
                                )
                                OR ( NNChild.ResourceId NOT IN ( 603,604,605,606,619 ) )
                              )
                              AND    
		   ---- The following expression means if @GradeBookWeightingLevel is set to courselevel, hide     
		   ---- ResourceIds 107,96,222    
		   ---- (Not False) OR (Condition)    
                              (
                                (
                                  NOT LOWER(LTRIM(RTRIM(@GradeBookWeightingLevel))) = 'courselevel'
                                )
                                OR ( NNChild.ResourceId NOT IN ( 107,96,222 ) )
                              )
                              AND (
                                    (
                                      NOT LOWER(LTRIM(RTRIM(@GradeBookWeightingLevel))) = 'instructorlevel'
                                    )
                                    OR ( NNChild.ResourceId NOT IN ( 476 ) )
                                  )
                              AND (
                                    (
                                      NOT LOWER(LTRIM(RTRIM(@ShowExternshipTabs))) = 'no'
                                    )  
			   --OR ( NNChild.ResourceId NOT IN ( 543, 538 ) )  
                                    OR ( NNChild.ResourceId NOT IN ( 543 ) )
                                  )
                            )
                ) tblDerived;  
	
  --Create a temp table for StudentAccounts-StudentPages  
        SELECT  *
        INTO    #tmpSA_StudentPages
        FROM    (
                  SELECT DISTINCT
                            NNChild.ResourceId AS ChildResourceId
                           ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                 ELSE RChild.Resource
                            END AS ChildResource
                           ,RChild.ResourceURL AS ChildResourceURL
                           ,CASE WHEN NNParent.ResourceId IN ( 194 )
                                      OR NNChild.ResourceId IN ( 737,738,739,740,741,742 ) THEN NULL
                                 ELSE NNParent.ResourceId
                            END AS ParentResourceId
                           ,RParent.Resource AS ParentResource
                           ,RParentModule.Resource AS MODULE    
	--NNParent.ParentId    
                  FROM      syResources RChild
                  INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
                  INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                  INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                  LEFT OUTER JOIN (
                                    SELECT  *
                                    FROM    syResources
                                    WHERE   ResourceTypeId = 1
                                  ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                  WHERE     RChild.ResourceTypeId IN ( 2,3,8 )
                            AND (
                                  RChild.ChildTypeId IS NULL
                                  OR RChild.ChildTypeId = 3
                                )
                            AND ( NNParent.ParentId IN ( SELECT HierarchyId
                                                         FROM   syNavigationNodes
                                                         WHERE  ResourceId = 394
                                                                AND ParentId IN ( SELECT    HierarchyId
                                                                                  FROM      syNavigationNodes
                                                                                  WHERE     ResourceId = 194 ) ) )
                            AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
                            AND  
										-- Hide resources based on Configuration Settings    
			   -- Hide resources based on Configuration Settings    
                            (
                              -- The following expression means if showross... is set to false, hide     
		   -- ResourceIds 541,542,532,534,535,538,543,539    
		   -- (Not False) OR (Condition)   
		   -- US4330 removed Resourceid 541  
							  (
                                (
                                  NOT LTRIM(RTRIM(@ShowRossOnlyTabs)) = 0
                                )
                                OR ( NNChild.ResourceId NOT IN ( 542,532,
														   --   534, 535, Commented for DE8041
                                                                 538 --Commented for DE8204     , 543
															   ) )
                              )
                              AND    
		   -- The following expression means if showross... is set to true, hide     
		   -- ResourceIds 142,375,330,476,508,102,107,237    
		   -- (Not True) OR (Condition)    
                              (
                                (
                                  NOT LTRIM(RTRIM(@ShowRossOnlyTabs)) = 1
                                )
                                OR ( NNChild.ResourceId NOT IN ( 142,375,330,476,508,102,107,237,230,286,200,217,290 ) )
                              )
                              AND 
		   -- US4330 added		  
		   -- The following expression means if PostServices is set to false or showross is set to false, hide           
		   -- ResourceId 541 , 508   
		   -- (Not False) OR (Condition)      
                              (
                                (
                                  (
                                    NOT LTRIM(RTRIM(@PostServicesByStudent)) = 0
                                  )
                                  OR ( NNChild.ResourceId NOT IN ( 541 ) )
                                )
                                OR (
                                     (
                                       NOT LTRIM(RTRIM(@ShowRossOnlyTabs)) = 0
                                     )
                                     OR ( NNChild.ResourceId NOT IN ( 541 ) )
                                   )
                              )
                              AND (
                                    (
                                      (
                                        NOT LTRIM(RTRIM(@PostServicesByClass)) = 0
                                      )
                                      OR ( NNChild.ResourceId NOT IN ( 508 ) )
                                    )
                                    OR (
                                         (
                                           NOT LTRIM(RTRIM(@ShowRossOnlyTabs)) = 0
                                         )
                                         OR ( NNChild.ResourceId NOT IN ( 508 ) )
                                       )
                                  )
                              AND						     
		   ---- The following expression means if SchedulingMethod=regulartraditional, hide     
		   ---- ResourceIds 91 and 497    
		   ---- (Not True) OR (Condition)    
                              (
                                (
                                  NOT LOWER(LTRIM(RTRIM(@SchedulingMethod))) = 'regulartraditional'
                                )
                                OR ( NNChild.ResourceId NOT IN ( 91,497 ) )
                              )
                              AND    
		   -- The following expression means if TrackSAPAttendance=byday, hide     
		   -- ResourceIds 633    
		   -- (Not True) OR (Condition)    
                              (
                                (
                                  NOT LOWER(LTRIM(RTRIM(@TrackSAPAttendance))) = 'byday'
                                )
                                OR ( NNChild.ResourceId NOT IN ( 633,327 ) )
                              )
                              AND (
                                    (
                                      NOT LOWER(LTRIM(RTRIM(@TrackSAPAttendance))) = 'byclass'
                                    )
                                    OR ( NNChild.ResourceId NOT IN ( 539 ) )
                                  )
                              AND       
		   ---- The following expression means if @ShowCollegeOfCourtReporting is set to false, hide     
		   ---- ResourceIds 614,615    
		   ---- (Not False) OR (Condition)    
                              (
                                (
                                  NOT LOWER(LTRIM(RTRIM(@ShowCollegeOfCourtReporting))) = 'no'
                                )
                                OR ( NNChild.ResourceId NOT IN ( 614,615 ) )
                              )
                              AND    
		   -- The following expression means if @ShowCollegeOfCourtReporting is set to true, hide     
		   -- ResourceIds 497    
		   -- (Not True) OR (Condition)    
                              (
                                (
                                  NOT LOWER(LTRIM(RTRIM(@ShowCollegeOfCourtReporting))) = 'yes'
                                )
                                OR ( NNChild.ResourceId NOT IN ( 497 ) )
                              )
                              AND    
		   -- The following expression means if FAMEESP is set to false, hide     
		   -- ResourceIds 517,523, 525    
		   -- (Not False) OR (Condition)    
                              (
                                (
                                  NOT LOWER(LTRIM(RTRIM(@FameESP))) = 'no'
                                )
                                OR ( NNChild.ResourceId NOT IN ( 517,523,525 ) )
                              )
                              AND    
		   ---- The following expression means if EDExpress is set to false, hide     
		   ---- ResourceIds 603,604,606,619    
		   ---- (Not False) OR (Condition)    
                              (
                                (
                                  NOT LOWER(LTRIM(RTRIM(@EdExpress))) = 'no'
                                )
                                OR ( NNChild.ResourceId NOT IN ( 603,604,605,606,619 ) )
                              )
                              AND    
		   ---- The following expression means if @GradeBookWeightingLevel is set to courselevel, hide     
		   ---- ResourceIds 107,96,222    
		   ---- (Not False) OR (Condition)    
                              (
                                (
                                  NOT LOWER(LTRIM(RTRIM(@GradeBookWeightingLevel))) = 'courselevel'
                                )
                                OR ( NNChild.ResourceId NOT IN ( 107,96,222 ) )
                              )
                              AND (
                                    (
                                      NOT LOWER(LTRIM(RTRIM(@GradeBookWeightingLevel))) = 'instructorlevel'
                                    )
                                    OR ( NNChild.ResourceId NOT IN ( 476 ) )
                                  )
                              AND (
                                    (
                                      NOT LOWER(LTRIM(RTRIM(@ShowExternshipTabs))) = 'no'
                                    )  
			   --OR ( NNChild.ResourceId NOT IN ( 543, 538 ) )  
                                    OR ( NNChild.ResourceId NOT IN ( 543 ) )
                                  )
                            )
                ) tblDerived;  
	
  -- Create a temptable for Placement-Student Pages  
        SELECT  *
        INTO    #tmpPlacement_StudentPages
        FROM    (
                  SELECT DISTINCT
                            NNChild.ResourceId AS ChildResourceId
                           ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                 ELSE CASE WHEN NNChild.ResourceId = 397 THEN 'Employer Tabs'
                                           ELSE RChild.Resource
                                      END
                            END AS ChildResource
                           ,RChild.ResourceURL AS ChildResourceURL
                           ,CASE WHEN (
                                        NNParent.ResourceId = 193
                                        OR NNChild.ResourceId IN ( 713,735 )
                                      ) THEN NULL
                                 ELSE NNParent.ResourceId
                            END AS ParentResourceId
                           ,RParent.Resource AS ParentResource
                           ,RParentModule.Resource AS MODULE    
	--NNParent.ParentId    
                  FROM      syResources RChild
                  INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
                  INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                  INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                  LEFT OUTER JOIN (
                                    SELECT  *
                                    FROM    syResources
                                    WHERE   ResourceTypeId = 1
                                  ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                  WHERE     RChild.ResourceTypeId IN ( 2,3,8 )
                            AND (
                                  RChild.ChildTypeId IS NULL
                                  OR RChild.ChildTypeId = 3
                                )
                            AND ( NNParent.ParentId IN ( SELECT HierarchyId
                                                         FROM   syNavigationNodes
                                                         WHERE  ResourceId = 394
                                                                AND ParentId IN ( SELECT    HierarchyId
                                                                                  FROM      syNavigationNodes
                                                                                  WHERE     ResourceId = 193 ) ) )
                            AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
                            AND  
										-- Hide resources based on Configuration Settings    
			   -- Hide resources based on Configuration Settings    
                            (
                              -- The following expression means if showross... is set to false, hide     
		   -- ResourceIds 541,542,532,534,535,538,543,539    
		   -- (Not False) OR (Condition)  
		   -- US4330 removed Resourceid 541    
							  (
                                (
                                  NOT LTRIM(RTRIM(@ShowRossOnlyTabs)) = 0
                                )
                                OR ( NNChild.ResourceId NOT IN ( 542,532,
														   --   534, 535,Commented for DE8041
                                                                 538 --Commented for DE8204     ,   543
																) )
                              )
                              AND    
		   -- The following expression means if showross... is set to true, hide     
		   -- ResourceIds 142,375,330,476,508,102,107,237    
		   -- (Not True) OR (Condition)    
                              (
                                (
                                  NOT LTRIM(RTRIM(@ShowRossOnlyTabs)) = 1
                                )
                                OR ( NNChild.ResourceId NOT IN ( 142,375,330,476,508,102,107,237,230,286,200,217,290 ) )
                              )
                              AND  
		   -- US4330 added		  
		   -- The following expression means if PostServices is set to false or showross is set to false, hide           
		   -- ResourceId 541 , 508    
		   -- (Not False) OR (Condition)      
                              (
                                (
                                  (
                                    NOT LTRIM(RTRIM(@PostServicesByStudent)) = 0
                                  )
                                  OR ( NNChild.ResourceId NOT IN ( 541 ) )
                                )
                                OR (
                                     (
                                       NOT LTRIM(RTRIM(@ShowRossOnlyTabs)) = 0
                                     )
                                     OR ( NNChild.ResourceId NOT IN ( 541 ) )
                                   )
                              )
                              AND (
                                    (
                                      (
                                        NOT LTRIM(RTRIM(@PostServicesByClass)) = 0
                                      )
                                      OR ( NNChild.ResourceId NOT IN ( 508 ) )
                                    )
                                    OR (
                                         (
                                           NOT LTRIM(RTRIM(@ShowRossOnlyTabs)) = 0
                                         )
                                         OR ( NNChild.ResourceId NOT IN ( 508 ) )
                                       )
                                  )
                              AND						       
		   ---- The following expression means if SchedulingMethod=regulartraditional, hide     
		   ---- ResourceIds 91 and 497    
		   ---- (Not True) OR (Condition)    
                              (
                                (
                                  NOT LOWER(LTRIM(RTRIM(@SchedulingMethod))) = 'regulartraditional'
                                )
                                OR ( NNChild.ResourceId NOT IN ( 91,497 ) )
                              )
                              AND    
		   -- The following expression means if TrackSAPAttendance=byday, hide     
		   -- ResourceIds 633    
		   -- (Not True) OR (Condition)    
                              (
                                (
                                  NOT LOWER(LTRIM(RTRIM(@TrackSAPAttendance))) = 'byday'
                                )
                                OR ( NNChild.ResourceId NOT IN ( 633,327 ) )
                              )
                              AND (
                                    (
                                      NOT LOWER(LTRIM(RTRIM(@TrackSAPAttendance))) = 'byclass'
                                    )
                                    OR ( NNChild.ResourceId NOT IN ( 539 ) )
                                  )
                              AND      
		   ---- The following expression means if @ShowCollegeOfCourtReporting is set to false, hide     
		   ---- ResourceIds 614,615    
		   ---- (Not False) OR (Condition)    
                              (
                                (
                                  NOT LOWER(LTRIM(RTRIM(@ShowCollegeOfCourtReporting))) = 'no'
                                )
                                OR ( NNChild.ResourceId NOT IN ( 614,615 ) )
                              )
                              AND    
		   -- The following expression means if @ShowCollegeOfCourtReporting is set to true, hide     
		   -- ResourceIds 497    
		   -- (Not True) OR (Condition)    
                              (
                                (
                                  NOT LOWER(LTRIM(RTRIM(@ShowCollegeOfCourtReporting))) = 'yes'
                                )
                                OR ( NNChild.ResourceId NOT IN ( 497 ) )
                              )
                              AND    
		   -- The following expression means if FAMEESP is set to false, hide     
		   -- ResourceIds 517,523, 525    
		   -- (Not False) OR (Condition)    
                              (
                                (
                                  NOT LOWER(LTRIM(RTRIM(@FameESP))) = 'no'
                                )
                                OR ( NNChild.ResourceId NOT IN ( 517,523,525 ) )
                              )
                              AND    
		   ---- The following expression means if EDExpress is set to false, hide     
		   ---- ResourceIds 603,604,606,619    
		   ---- (Not False) OR (Condition)    
                              (
                                (
                                  NOT LOWER(LTRIM(RTRIM(@EdExpress))) = 'no'
                                )
                                OR ( NNChild.ResourceId NOT IN ( 603,604,605,606,619 ) )
                              )
                              AND    
		   ---- The following expression means if @GradeBookWeightingLevel is set to courselevel, hide     
		   ---- ResourceIds 107,96,222    
		   ---- (Not False) OR (Condition)    
                              (
                                (
                                  NOT LOWER(LTRIM(RTRIM(@GradeBookWeightingLevel))) = 'courselevel'
                                )
                                OR ( NNChild.ResourceId NOT IN ( 107,96,222 ) )
                              )
                              AND (
                                    (
                                      NOT LOWER(LTRIM(RTRIM(@GradeBookWeightingLevel))) = 'instructorlevel'
                                    )
                                    OR ( NNChild.ResourceId NOT IN ( 476 ) )
                                  )
                              AND (
                                    (
                                      NOT LOWER(LTRIM(RTRIM(@ShowExternshipTabs))) = 'no'
                                    )  
			   --OR ( NNChild.ResourceId NOT IN ( 543, 538 ) )  
                                    OR ( NNChild.ResourceId NOT IN ( 543 ) )
                                  )
                            )
                ) tblDerived;  
	
  -- Create a temp table for AR-StudentPages  
        SELECT  *
        INTO    #tmpAcademics_StudentPages
        FROM    (
                  SELECT DISTINCT
                            NNChild.ResourceId AS ChildResourceId
                           ,CASE WHEN NNChild.ResourceId = 409 THEN 'IPEDS - General Reports'
                                 ELSE RChild.Resource
                            END AS ChildResource
                           ,RChild.ResourceURL AS ChildResourceURL
                           ,CASE WHEN (
                                        NNParent.ResourceId IN ( 689 )
                                        OR NNChild.ResourceId IN ( 737,738,739,740,741,742 )
                                      ) THEN NULL
                                 ELSE NNParent.ResourceId
                            END AS ParentResourceId
                           ,RParent.Resource AS ParentResource
                           ,RParentModule.Resource AS MODULE
                  FROM      syResources RChild
                  INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
                  INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                  INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                  LEFT OUTER JOIN (
                                    SELECT  *
                                    FROM    syResources
                                    WHERE   ResourceTypeId = 1
                                  ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                  WHERE     RChild.ResourceTypeId IN ( 2,3,8 )
                            AND (
                                  RChild.ChildTypeId IS NULL
                                  OR RChild.ChildTypeId = 3
                                )
                            AND ( NNParent.ParentId IN ( SELECT HierarchyId
                                                         FROM   syNavigationNodes
                                                         WHERE  ResourceId = 394
                                                                AND ParentId IN ( SELECT    HierarchyId
                                                                                  FROM      syNavigationNodes
                                                                                  WHERE     ResourceId = 26 ) ) ) 
											--Uncommented for de8041  Aug 1 2012
                            AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
                            AND  
										-- Hide resources based on Configuration Settings    
			   -- Hide resources based on Configuration Settings    
                            (
                              -- The following expression means if showross... is set to false, hide     
		   -- ResourceIds 541,542,532,534,535,538,543,539    
		   -- (Not False) OR (Condition) 
		   -- US4330 removed Resourceid 541    
							  (
                                (
                                  NOT LTRIM(RTRIM(@ShowRossOnlyTabs)) = 0
                                )
                                OR ( NNChild.ResourceId NOT IN ( 542,532,
														   --   534, 535,  Commented for DE8041
                                                                 538 --Commented for DE8204    , 543 
															) )
                              )
                              AND    
		   -- The following expression means if showross... is set to true, hide     
		   -- ResourceIds 142,375,330,476,508,102,107,237    
		   -- (Not True) OR (Condition)    
                              (
                                (
                                  NOT LTRIM(RTRIM(@ShowRossOnlyTabs)) = 1
                                )
                                OR ( NNChild.ResourceId NOT IN ( 142,375,330,476,508,102,107,237,230,286,200,217,290 ) )
                              )
                              AND   
		   -- US4330 added		  
		   -- The following expression means if PostServices is set to false or showross is set to false, hide           
		   -- ResourceId 541 , 508    
		   -- (Not False) OR (Condition)      
                              (
                                (
                                  (
                                    NOT LTRIM(RTRIM(@PostServicesByStudent)) = 0
                                  )
                                  OR ( NNChild.ResourceId NOT IN ( 541 ) )
                                )
                                OR (
                                     (
                                       NOT LTRIM(RTRIM(@ShowRossOnlyTabs)) = 0
                                     )
                                     OR ( NNChild.ResourceId NOT IN ( 541 ) )
                                   )
                              )
                              AND (
                                    (
                                      (
                                        NOT LTRIM(RTRIM(@PostServicesByClass)) = 0
                                      )
                                      OR ( NNChild.ResourceId NOT IN ( 508 ) )
                                    )
                                    OR (
                                         (
                                           NOT LTRIM(RTRIM(@ShowRossOnlyTabs)) = 0
                                         )
                                         OR ( NNChild.ResourceId NOT IN ( 508 ) )
                                       )
                                  )
                              AND 						  
		   ---- The following expression means if SchedulingMethod=regulartraditional, hide     
		   ---- ResourceIds 91 and 497    
		   ---- (Not True) OR (Condition)    
                              (
                                (
                                  NOT LOWER(LTRIM(RTRIM(@SchedulingMethod))) = 'regulartraditional'
                                )
                                OR ( NNChild.ResourceId NOT IN ( 91,497 ) )
                              )
                              AND    
		   -- The following expression means if TrackSAPAttendance=byday, hide     
		   -- ResourceIds 633    
		   -- (Not True) OR (Condition)    
                              (
                                (
                                  NOT LOWER(LTRIM(RTRIM(@TrackSAPAttendance))) = 'byday'
                                )
                                OR ( NNChild.ResourceId NOT IN ( 633,327 ) )
                              )
                              AND (
                                    (
                                      NOT LOWER(LTRIM(RTRIM(@TrackSAPAttendance))) = 'byclass'
                                    )
                                    OR ( NNChild.ResourceId NOT IN ( 539 ) )
                                  )
                              AND    
		   ---- The following expression means if @ShowCollegeOfCourtReporting is set to false, hide     
		   ---- ResourceIds 614,615    
		   ---- (Not False) OR (Condition)    
                              (
                                (
                                  NOT LOWER(LTRIM(RTRIM(@ShowCollegeOfCourtReporting))) = 'no'
                                )
                                OR ( NNChild.ResourceId NOT IN ( 614,615 ) )
                              )
                              AND    
		   -- The following expression means if @ShowCollegeOfCourtReporting is set to true, hide     
		   -- ResourceIds 497    
		   -- (Not True) OR (Condition)    
                              (
                                (
                                  NOT LOWER(LTRIM(RTRIM(@ShowCollegeOfCourtReporting))) = 'yes'
                                )
                                OR ( NNChild.ResourceId NOT IN ( 497 ) )
                              )
                              AND    
		   -- The following expression means if FAMEESP is set to false, hide     
		   -- ResourceIds 517,523, 525    
		   -- (Not False) OR (Condition)    
                              (
                                (
                                  NOT LOWER(LTRIM(RTRIM(@FameESP))) = 'no'
                                )
                                OR ( NNChild.ResourceId NOT IN ( 517,523,525 ) )
                              )
                              AND    
		   ---- The following expression means if EDExpress is set to false, hide     
		   ---- ResourceIds 603,604,606,619    
		   ---- (Not False) OR (Condition)    
                              (
                                (
                                  NOT LOWER(LTRIM(RTRIM(@EdExpress))) = 'no'
                                )
                                OR ( NNChild.ResourceId NOT IN ( 603,604,605,606,619 ) )
                              )
                              AND    
		   ---- The following expression means if @GradeBookWeightingLevel is set to courselevel, hide     
		   ---- ResourceIds 107,96,222    
		   ---- (Not False) OR (Condition)    
                              (
                                (
                                  NOT LOWER(LTRIM(RTRIM(@GradeBookWeightingLevel))) = 'courselevel'
                                )
                                OR ( NNChild.ResourceId NOT IN ( 107,96,222 ) )
                              )
                              AND (
                                    (
                                      NOT LOWER(LTRIM(RTRIM(@GradeBookWeightingLevel))) = 'instructorlevel'
                                    )
                                    OR ( NNChild.ResourceId NOT IN ( 476 ) )
                                  )
                              AND (
                                    (
                                      NOT LOWER(LTRIM(RTRIM(@ShowExternshipTabs))) = 'no'
                                    )  
			   --OR ( NNChild.ResourceId NOT IN ( 543, 538 ) )  
                                    OR ( NNChild.ResourceId NOT IN ( 543 ) )
                                  )  
							 -- DE7545 - Conversion Page should not be shown in the menu  
                              AND NNChild.ResourceId NOT IN ( 530,531 )
                            )
                ) tblDerived;                                                   
	 
  --Select * from #tmpAcademics_StudentPages  
	
  -- Create a temp table to hold all FACULTY-Student Pages  
        SELECT  *
        INTO    #tmpFaculty_StudentPages
        FROM    (
                  SELECT DISTINCT
                            NNChild.ResourceId AS ChildResourceId
                           ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                 ELSE RChild.Resource
                            END AS ChildResource
                           ,RChild.ResourceURL AS ChildResourceURL
                           ,CASE WHEN NNParent.ResourceId = 300
                                      OR NNChild.ResourceId IN ( 737,739,740,741 ) THEN NULL
                                 ELSE NNParent.ResourceId
                            END AS ParentResourceId
                           ,RParent.Resource AS ParentResource
                           ,RParentModule.Resource AS MODULE    
		  --NNParent.ParentId    
                  FROM      syResources RChild
                  INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
                  INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                  INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                  LEFT OUTER JOIN (
                                    SELECT  *
                                    FROM    syResources
                                    WHERE   ResourceTypeId = 1
                                  ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                  WHERE     RChild.ResourceTypeId IN ( 2,3,8 )
                            AND (
                                  RChild.ChildTypeId IS NULL
                                  OR RChild.ChildTypeId = 3
                                )
                            AND ( NNParent.ParentId IN ( SELECT HierarchyId
                                                         FROM   syNavigationNodes
                                                         WHERE  ResourceId = 394
                                                                AND ParentId IN ( SELECT    HierarchyId
                                                                                  FROM      syNavigationNodes
                                                                                  WHERE     ResourceId = 300 ) ) )
                            AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
                            AND  
										-- Hide resources based on Configuration Settings    
			   -- Hide resources based on Configuration Settings    
                            (
                              -- The following expression means if showross... is set to false, hide     
		   -- ResourceIds 541,542,532,534,535,538,543,539    
		   -- (Not False) OR (Condition) 
		   -- US4330 removed Resourceid 541    
							  (
                                (
                                  NOT LTRIM(RTRIM(@ShowRossOnlyTabs)) = 0
                                )
                                OR ( NNChild.ResourceId NOT IN ( 542,532,
														   --   534, 535,  Commented for DE8041
                                                                 538 --Commented for DE8204    , 543 
															) )
                              )
                              AND    
		   -- The following expression means if showross... is set to true, hide     
		   -- ResourceIds 142,375,330,476,508,102,107,237    
		   -- (Not True) OR (Condition)    
                              (
                                (
                                  NOT LTRIM(RTRIM(@ShowRossOnlyTabs)) = 1
                                )
                                OR ( NNChild.ResourceId NOT IN ( 142,375,330,476,508,102,107,237,230,286,200,217,290 ) )
                              )
                              AND   
		   -- US4330 added		  
		   -- The following expression means if PostServices is set to false or showross is set to false, hide           
		   -- ResourceId 541  , 508   
		   -- (Not False) OR (Condition)      
                              (
                                (
                                  (
                                    NOT LTRIM(RTRIM(@PostServicesByStudent)) = 0
                                  )
                                  OR ( NNChild.ResourceId NOT IN ( 541 ) )
                                )
                                OR (
                                     (
                                       NOT LTRIM(RTRIM(@ShowRossOnlyTabs)) = 0
                                     )
                                     OR ( NNChild.ResourceId NOT IN ( 541 ) )
                                   )
                              )
                              AND (
                                    (
                                      (
                                        NOT LTRIM(RTRIM(@PostServicesByClass)) = 0
                                      )
                                      OR ( NNChild.ResourceId NOT IN ( 508 ) )
                                    )
                                    OR (
                                         (
                                           NOT LTRIM(RTRIM(@ShowRossOnlyTabs)) = 0
                                         )
                                         OR ( NNChild.ResourceId NOT IN ( 508 ) )
                                       )
                                  )
                              AND  						  
		   ---- The following expression means if SchedulingMethod=regulartraditional, hide     
		   ---- ResourceIds 91 and 497    
		   ---- (Not True) OR (Condition)    
                              (
                                (
                                  NOT LOWER(LTRIM(RTRIM(@SchedulingMethod))) = 'regulartraditional'
                                )
                                OR ( NNChild.ResourceId NOT IN ( 91,497 ) )
                              )
                              AND    
		   -- The following expression means if TrackSAPAttendance=byday, hide     
		   -- ResourceIds 633    
		   -- (Not True) OR (Condition)    
                              (
                                (
                                  NOT LOWER(LTRIM(RTRIM(@TrackSAPAttendance))) = 'byday'
                                )
                                OR ( NNChild.ResourceId NOT IN ( 633,327 ) )
                              )
                              AND (
                                    (
                                      NOT LOWER(LTRIM(RTRIM(@TrackSAPAttendance))) = 'byclass'
                                    )
                                    OR ( NNChild.ResourceId NOT IN ( 539 ) )
                                  )
                              AND    
		   ---- The following expression means if @ShowCollegeOfCourtReporting is set to false, hide     
		   ---- ResourceIds 614,615    
		   ---- (Not False) OR (Condition)    
                              (
                                (
                                  NOT LOWER(LTRIM(RTRIM(@ShowCollegeOfCourtReporting))) = 'no'
                                )
                                OR ( NNChild.ResourceId NOT IN ( 614,615 ) )
                              )
                              AND    
		   -- The following expression means if @ShowCollegeOfCourtReporting is set to true, hide     
		   -- ResourceIds 497    
		   -- (Not True) OR (Condition)    
                              (
                                (
                                  NOT LOWER(LTRIM(RTRIM(@ShowCollegeOfCourtReporting))) = 'yes'
                                )
                                OR ( NNChild.ResourceId NOT IN ( 497 ) )
                              )
                              AND    
		   -- The following expression means if FAMEESP is set to false, hide     
		   -- ResourceIds 517,523, 525    
		   -- (Not False) OR (Condition)    
                              (
                                (
                                  NOT LOWER(LTRIM(RTRIM(@FameESP))) = 'no'
                                )
                                OR ( NNChild.ResourceId NOT IN ( 517,523,525 ) )
                              )
                              AND    
		   ---- The following expression means if EDExpress is set to false, hide     
		   ---- ResourceIds 603,604,606,619    
		   ---- (Not False) OR (Condition)    
                              (
                                (
                                  NOT LOWER(LTRIM(RTRIM(@EdExpress))) = 'no'
                                )
                                OR ( NNChild.ResourceId NOT IN ( 603,604,605,606,619 ) )
                              )
                              AND    
		   ---- The following expression means if @GradeBookWeightingLevel is set to courselevel, hide     
		   ---- ResourceIds 107,96,222    
		   ---- (Not False) OR (Condition)    
                              (
                                (
                                  NOT LOWER(LTRIM(RTRIM(@GradeBookWeightingLevel))) = 'courselevel'
                                )
                                OR ( NNChild.ResourceId NOT IN ( 107,96,222 ) )
                              )
                              AND (
                                    (
                                      NOT LOWER(LTRIM(RTRIM(@GradeBookWeightingLevel))) = 'instructorlevel'
                                    )
                                    OR ( NNChild.ResourceId NOT IN ( 476 ) )
                                  )
                              AND (
                                    (
                                      NOT LOWER(LTRIM(RTRIM(@ShowExternshipTabs))) = 'no'
                                    )  
			   --OR ( NNChild.ResourceId NOT IN ( 543, 538 ) )  
                                    OR ( NNChild.ResourceId NOT IN ( 543 ) )
                                  )
                            )
                ) tblDerived;  
	
  --select * from #tmpFaculty_StudentPages  
	
 -- The first column always refers to the Module Resource Id (189 or 26 or .....)    
        SELECT  *
        FROM    (
                  SELECT    189 AS ModuleResourceId
                           ,395 AS TabId
                           ,NNChild.ResourceId AS ChildResourceId
                           ,CASE WHEN NNChild.ResourceId = 395 THEN 'Lead Tabs'
                                 ELSE CASE WHEN NNChild.ResourceId = 398 THEN 'Add/View Leads'
                                           ELSE RChild.Resource
                                      END
                            END AS ChildResource
                           ,RChild.ResourceURL AS ChildResourceURL
                           ,CASE WHEN (
                                        NNParent.ResourceId IN ( 395 )
                                        OR NNChild.ResourceId IN ( 737,740,741,792 )
                                      ) THEN NULL
                                 ELSE NNParent.ResourceId
                            END AS ParentResourceId
                           ,RParent.Resource AS ParentResource
                           ,CASE WHEN NNChild.ResourceId = 737 THEN 1
                                 ELSE CASE WHEN NNChild.ResourceId = 395 THEN 2
                                           ELSE 3
                                      END
                            END AS FirstSortOrder
                           ,CASE WHEN (
                                        NNChild.ResourceId IN ( 737,792 )
                                        OR NNParent.ResourceId IN ( 737,792 )
                                      ) THEN 1
                                 ELSE 2
                            END AS DisplaySequence
                  FROM      syResources RChild
                  INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
                  INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                  INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                  LEFT OUTER JOIN (
                                    SELECT  *
                                    FROM    syResources
                                    WHERE   ResourceTypeId = 1
                                  ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                  WHERE     RChild.ResourceTypeId IN ( 2,3,8 )
                            AND (
                                  RChild.ChildTypeId IS NULL
                                  OR RChild.ChildTypeId = 3
                                )
                            AND ( RChild.ResourceId NOT IN ( 394 ) )
                            AND (
                                  NNParent.ParentId IN ( SELECT HierarchyId
                                                         FROM   syNavigationNodes
                                                         WHERE  ResourceId = 395 )
                                  OR NNChild.HierarchyId IN ( SELECT DISTINCT
                                                                        HierarchyId
                                                              FROM      syNavigationNodes
                                                              WHERE     ResourceId IN ( 737,740,741,792 )
                                                                        AND ParentId IN ( SELECT    HierarchyId
                                                                                          FROM      syNavigationNodes
                                                                                          WHERE     ResourceId = 395
                                                                                                    AND ParentId IN ( SELECT    HierarchyId
                                                                                                                      FROM      syNavigationNodes
                                                                                                                      WHERE     ResourceId = 189 ) ) )
                                )
                  UNION ALL
                  SELECT    26 AS ModuleResourceId
                           ,394 AS TabId
                           ,ChildResourceId
                           ,ChildResource
                           ,ChildResourceURL
                           ,ParentResourceId
                           ,ParentResource
                           ,CASE WHEN ChildResourceId = 737 THEN 1
                                 ELSE 2
                            END AS FirstSortOrder
                           ,CASE WHEN (
                                        ChildResourceId IN ( 737,738,739 )
                                        OR ParentResourceId IN ( 737,738,739 )
                                      ) THEN 1
                                 ELSE 2
                            END AS DisplaySequence
                  FROM      (
                              SELECT    *
                              FROM      #tmpAcademics_StudentPages
                              UNION  
							  -- get all submenu headers   
							  -- only the ones that have child items  
                              SELECT DISTINCT
                                        RChild.ResourceId AS ChildResourceId
                                       ,RChild.Resource AS ChildResource
                                       ,RChild.ResourceURL AS ChildResourceURL
                                       ,CASE WHEN ( RChild.ResourceId IN ( 737,738,739,740,741,742 ) ) THEN NULL
                                             ELSE RChild.ResourceId
                                        END AS ParentResourceId
                                       ,NULL AS ParentResource
                                       ,NULL AS MODULE
                              FROM      syResources RChild
                              WHERE     RChild.ResourceId IN ( SELECT DISTINCT
                                                                        ParentResourceId
                                                               FROM     #tmpAcademics_StudentPages )
                            ) t1
                  UNION ALL
                  SELECT    194 AS ModuleResourceId
                           ,394 AS TabId
                           ,ChildResourceId
                           ,ChildResource
                           ,ChildResourceURL
                           ,ParentResourceId
                           ,ParentResource
                           ,CASE WHEN ChildResourceId = 737 THEN 1
                                 ELSE 2
                            END AS FirstSortOrder
                           ,CASE WHEN (
                                        ChildResourceId IN ( 737,738,739 )
                                        OR ParentResourceId IN ( 737,738,739 )
                                      ) THEN 1
                                 ELSE 2
                            END AS DisplaySequence
                  FROM      (
                              SELECT    *
                              FROM      #tmpSA_StudentPages
                              UNION
                              SELECT DISTINCT
                                        RChild.ResourceId AS ChildResourceId
                                       ,RChild.Resource AS ChildResource
                                       ,RChild.ResourceURL AS ChildResourceURL
                                       ,CASE WHEN ( RChild.ResourceId IN ( 737,738,739,740,741,742 ) ) THEN NULL
                                             ELSE RChild.ResourceId
                                        END AS ParentResourceId
                                       ,NULL AS ParentResource
                                       ,NULL AS MODULE
                              FROM      syResources RChild
                              WHERE     RChild.ResourceId IN ( SELECT DISTINCT
                                                                        ParentResourceId
                                                               FROM     #tmpSA_StudentPages )
                            ) t2
                  UNION ALL
                  SELECT    300 AS ModuleResourceId
                           ,394 AS TabId
                           ,ChildResourceId
                           ,ChildResource
                           ,ChildResourceURL
                           ,ParentResourceId
                           ,ParentResource
                           ,CASE WHEN ChildResourceId = 737 THEN 1
                                 ELSE 2
                            END AS FirstSortOrder
                           ,CASE WHEN (
                                        ChildResourceId IN ( 737,738,740 )
                                        OR ParentResourceId IN ( 737,738,740 )
                                      ) THEN 1
                                 ELSE 2
                            END AS DisplaySequence
                  FROM      (
                              SELECT    *
                              FROM      #tmpFaculty_StudentPages
                              UNION
                              SELECT DISTINCT
                                        RChild.ResourceId AS ChildResourceId
                                       ,RChild.Resource AS ChildResource
                                       ,RChild.ResourceURL AS ChildResourceURL
                                       ,CASE WHEN ( RChild.ResourceId IN ( 737,738,742,739,740,741 ) ) THEN NULL
                                             ELSE RChild.ResourceId
                                        END AS ParentResourceId
                                       ,NULL AS ParentResource
                                       ,NULL AS MODULE
                              FROM      syResources RChild
                              WHERE     RChild.ResourceId IN ( SELECT DISTINCT
                                                                        ParentResourceId
                                                               FROM     #tmpFaculty_StudentPages )
                            ) t3
                  UNION ALL
                  SELECT    191 AS ModuleResourceId
                           ,394 AS TabId
                           ,ChildResourceId
                           ,ChildResource
                           ,ChildResourceURL
                           ,ParentResourceId
                           ,ParentResource
                           ,CASE WHEN ChildResourceId = 737 THEN 1
                                 ELSE 2
                            END AS FirstSortOrder
                           ,CASE WHEN (
                                        ChildResourceId IN ( 737,738,739 )
                                        OR ParentResourceId IN ( 737,738,739 )
                                      ) THEN 1
                                 ELSE 2
                            END AS DisplaySequence
                  FROM      (
                              SELECT    *
                              FROM      #tmpFA_StudentPages
                              UNION
                              SELECT DISTINCT
                                        RChild.ResourceId AS ChildResourceId
                                       ,RChild.Resource AS ChildResource
                                       ,RChild.ResourceURL AS ChildResourceURL
                                       ,CASE WHEN ( RChild.ResourceId IN ( 737,738,739,740,741,742 ) ) THEN NULL
                                             ELSE RChild.ResourceId
                                        END AS ParentResourceId
                                       ,NULL AS ParentResource
                                       ,NULL AS MODULE
                              FROM      syResources RChild
                              WHERE     RChild.ResourceId IN ( SELECT DISTINCT
                                                                        ParentResourceId
                                                               FROM     #tmpFA_StudentPages )
                            ) t4
                  UNION ALL
                  SELECT    193 AS ModuleResourceId
                           ,394 AS TabId
                           ,ChildResourceId
                           ,ChildResource
                           ,ChildResourceURL
                           ,ParentResourceId
                           ,ParentResource
                           ,CASE WHEN ChildResourceId = 737 THEN 1
                                 ELSE 2
                            END AS FirstSortOrder
                           ,CASE WHEN (
                                        ChildResourceId IN ( 737,738,739 )
                                        OR ParentResourceId IN ( 737,738,739 )
                                      ) THEN 1
                                 ELSE 2
                            END AS DisplaySequence
                  FROM      (
                              SELECT    *
                              FROM      #tmpPlacement_StudentPages
                              UNION
                              SELECT DISTINCT
                                        RChild.ResourceId AS ChildResourceId
                                       ,RChild.Resource AS ChildResource
                                       ,RChild.ResourceURL AS ChildResourceURL
                                       ,CASE WHEN ( RChild.ResourceId IN ( 737,738,739,741,740,742 ) ) THEN NULL
                                             ELSE RChild.ResourceId
                                        END AS ParentResourceId
                                       ,NULL AS ParentResource
                                       ,NULL AS MODULE
                              FROM      syResources RChild
                              WHERE     RChild.ResourceId IN ( SELECT DISTINCT
                                                                        ParentResourceId
                                                               FROM     #tmpPlacement_StudentPages )
                            ) t5
                  UNION ALL
                  SELECT    193 AS ModuleResourceId
                           ,397 AS TabId
                           ,ChildResourceId
                           ,ChildResource
                           ,ChildResourceURL
                           ,ParentResourceId
                           ,ParentResource
                           ,CASE WHEN ChildResourceId = 737 THEN 1
                                 ELSE 2
                            END AS FirstSortOrder
                           ,CASE WHEN (
                                        ChildResourceId IN ( 737 )
                                        OR ParentResourceId IN ( 737 )
                                      ) THEN 1
                                 ELSE 2
                            END AS DisplaySequence
                  FROM      (
                              SELECT DISTINCT
                                        NNChild.ResourceId AS ChildResourceId
                                       ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                             ELSE CASE WHEN NNChild.ResourceId = 397 THEN 'Employer Tabs'
                                                       ELSE RChild.Resource
                                                  END
                                        END AS ChildResource
                                       ,RChild.ResourceURL AS ChildResourceURL
                                       ,CASE WHEN (
                                                    NNParent.ResourceId = 193
                                                    OR NNChild.ResourceId IN ( 737 )
                                                  ) THEN NULL
                                             ELSE NNParent.ResourceId
                                        END AS ParentResourceId
                                       ,RParent.Resource AS ParentResource
                                       ,RParentModule.Resource AS MODULE    
	--NNParent.ParentId    
                              FROM      syResources RChild
                              INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
                              INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                              INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                              LEFT OUTER JOIN (
                                                SELECT  *
                                                FROM    syResources
                                                WHERE   ResourceTypeId = 1
                                              ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                              WHERE     RChild.ResourceTypeId IN ( 2,3,8 )
                                        AND (
                                              RChild.ChildTypeId IS NULL
                                              OR RChild.ChildTypeId = 3
                                            )
                                        AND ( NNParent.ParentId IN ( SELECT HierarchyId
                                                                     FROM   syNavigationNodes
                                                                     WHERE  ResourceId = 397
                                                                            AND ParentId IN ( SELECT    HierarchyId
                                                                                              FROM      syNavigationNodes
                                                                                              WHERE     ResourceId = 193 ) ) )
                                        AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
                              UNION
                              SELECT DISTINCT
                                        RChild.ResourceId AS ChildResourceId
                                       ,RChild.Resource AS ChildResource
                                       ,RChild.ResourceURL AS ChildResourceURL
                                       ,CASE WHEN ( RChild.ResourceId IN ( 737 ) ) THEN NULL
                                             ELSE RChild.ResourceId
                                        END AS ParentResourceId
                                       ,NULL AS ParentResource
                                       ,NULL AS MODULE
                              FROM      syResources RChild
                              WHERE     RChild.ResourceId IN ( 737 )    
	-- The following condition uses Bitwise Operator    
                                        AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
                            ) t5
                  UNION ALL
                  SELECT    192 AS ModuleResourceId
                           ,396 AS TabId
                           ,ChildResourceId
                           ,ChildResource
                           ,ChildResourceURL
                           ,ParentResourceId
                           ,ParentResource
                           ,CASE WHEN ChildResourceId = 737 THEN 1
                                 ELSE 2
                            END AS FirstSortOrder
                           ,CASE WHEN (
                                        ChildResourceId IN ( 737 )
                                        OR ParentResourceId IN ( 737 )
                                      ) THEN 1
                                 ELSE 2
                            END AS DisplaySequence
                  FROM      (
                              SELECT DISTINCT
                                        NNChild.ResourceId AS ChildResourceId
                                       ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                             ELSE CASE WHEN NNChild.ResourceId = 397 THEN 'Employer Tabs'
                                                       ELSE RChild.Resource
                                                  END
                                        END AS ChildResource
                                       ,RChild.ResourceURL AS ChildResourceURL
                                       ,CASE WHEN (
                                                    NNParent.ResourceId = 193
                                                    OR NNChild.ResourceId IN ( 713,735 )
                                                  ) THEN NULL
                                             ELSE NNParent.ResourceId
                                        END AS ParentResourceId
                                       ,RParent.Resource AS ParentResource
                                       ,RParentModule.Resource AS MODULE    
	--NNParent.ParentId    
                              FROM      syResources RChild
                              INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
                              INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                              INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                              LEFT OUTER JOIN (
                                                SELECT  *
                                                FROM    syResources
                                                WHERE   ResourceTypeId = 1
                                              ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                              WHERE     RChild.ResourceTypeId IN ( 2,3,8 )
                                        AND (
                                              RChild.ChildTypeId IS NULL
                                              OR RChild.ChildTypeId = 3
                                            )
                                        AND ( NNParent.ParentId IN ( SELECT HierarchyId
                                                                     FROM   syNavigationNodes
                                                                     WHERE  ResourceId = 396
                                                                            AND ParentId IN ( SELECT    HierarchyId
                                                                                              FROM      syNavigationNodes
                                                                                              WHERE     ResourceId = 192 ) ) )
                                        AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
                              UNION
                              SELECT DISTINCT
                                        RChild.ResourceId AS ChildResourceId
                                       ,RChild.Resource AS ChildResource
                                       ,RChild.ResourceURL AS ChildResourceURL
                                       ,CASE WHEN ( RChild.ResourceId IN ( 737 ) ) THEN NULL
                                             ELSE RChild.ResourceId
                                        END AS ParentResourceId
                                       ,NULL AS ParentResource
                                       ,NULL AS MODULE
                              FROM      syResources RChild
                              WHERE     RChild.ResourceId IN ( 737 )    
	-- The following condition uses Bitwise Operator    
                                        AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
                            ) t5
                ) MainQuery
        WHERE   -- Hide resources based on Configuration Settings    
                (
                  -- The following expression means if showross... is set to false, hide     
	 -- ResourceIds 541,542,532,534,535,538,543,539    
	 -- (Not False) OR (Condition)  
	 -- US4330 removed Resourceid 541   
				  (
                    (
                      NOT LTRIM(RTRIM(@ShowRossOnlyTabs)) = 0
                    )
                    OR ( ChildResourceId NOT IN ( 542,532,
				  --   534, 535,  Commented for DE8041
                                                  538 -- --Commented for DE8204    , 543
					  
					   ) )
                  )
                  AND    
	 -- The following expression means if showross... is set to true, hide     
	 -- ResourceIds 142,375,330,476,508,102,107,237    
	 -- (Not True) OR (Condition)    
                  (
                    (
                      NOT LTRIM(RTRIM(@ShowRossOnlyTabs)) = 1
                    )
                    OR ( ChildResourceId NOT IN ( 142,375,330,476,508,102,107,237,230,286,200,217,290 ) )
                  )
                  AND    
	 -- US4330 added		  
	 -- The following expression means if PostServices is set to false or showross is set to false, hide           
	 -- ResourceId 541 , 508    
	 -- (Not False) OR (Condition)      
                  (
                    (
                      (
                        NOT LTRIM(RTRIM(@PostServicesByStudent)) = 0
                      )
                      OR ( ChildResourceId NOT IN ( 541 ) )
                    )
                    OR (
                         (
                           NOT LTRIM(RTRIM(@ShowRossOnlyTabs)) = 0
                         )
                         OR ( ChildResourceId NOT IN ( 541 ) )
                       )
                  )
                  AND (
                        (
                          (
                            NOT LTRIM(RTRIM(@PostServicesByClass)) = 0
                          )
                          OR ( ChildResourceId NOT IN ( 508 ) )
                        )
                        OR (
                             (
                               NOT LTRIM(RTRIM(@ShowRossOnlyTabs)) = 0
                             )
                             OR ( ChildResourceId NOT IN ( 508 ) )
                           )
                      )
                  AND					  
	 ---- The following expression means if SchedulingMethod=regulartraditional, hide     
	 ---- ResourceIds 91 and 497    
	 ---- (Not True) OR (Condition)    
                  (
                    (
                      NOT LOWER(LTRIM(RTRIM(@SchedulingMethod))) = 'regulartraditional'
                    )
                    OR ( ChildResourceId NOT IN ( 91,497 ) )
                  )
                  AND    
	 -- The following expression means if TrackSAPAttendance=byday, hide     
	 -- ResourceIds 633    
	 -- (Not True) OR (Condition)    
                  (
                    (
                      NOT LOWER(LTRIM(RTRIM(@TrackSAPAttendance))) = 'byday'
                    )
                    OR ( ChildResourceId NOT IN ( 633,327 ) )
                  )
                  AND (
                        (
                          NOT LOWER(LTRIM(RTRIM(@TrackSAPAttendance))) = 'byclass'
                        )
                        OR ( ChildResourceId NOT IN ( 539 ) )
                      )
                  AND  
	 ---- The following expression means if @ShowCollegeOfCourtReporting is set to false, hide     
	 ---- ResourceIds 614,615    
	 ---- (Not False) OR (Condition)    
                  (
                    (
                      NOT LOWER(LTRIM(RTRIM(@ShowCollegeOfCourtReporting))) = 'no'
                    )
                    OR ( ChildResourceId NOT IN ( 614,615 ) )
                  )
                  AND    
	 -- The following expression means if @ShowCollegeOfCourtReporting is set to true, hide     
	 -- ResourceIds 497    
	 -- (Not True) OR (Condition)    
                  (
                    (
                      NOT LOWER(LTRIM(RTRIM(@ShowCollegeOfCourtReporting))) = 'yes'
                    )
                    OR ( ChildResourceId NOT IN ( 497 ) )
                  )
                  AND    
	 -- The following expression means if FAMEESP is set to false, hide     
	 -- ResourceIds 517,523, 525    
	 -- (Not False) OR (Condition)    
                  (
                    (
                      NOT LOWER(LTRIM(RTRIM(@FameESP))) = 'no'
                    )
                    OR ( ChildResourceId NOT IN ( 517,523,525 ) )
                  )
                  AND    
	 ---- The following expression means if EDExpress is set to false, hide     
	 ---- ResourceIds 603,604,606,619    
	 ---- (Not False) OR (Condition)    
                  (
                    (
                      NOT LOWER(LTRIM(RTRIM(@EdExpress))) = 'no'
                    )
                    OR ( ChildResourceId NOT IN ( 603,604,605,606,619 ) )
                  )
                  AND    
	 ---- The following expression means if @GradeBookWeightingLevel is set to courselevel, hide     
	 ---- ResourceIds 107,96,222    
	 ---- (Not False) OR (Condition)    
                  (
                    (
                      NOT LOWER(LTRIM(RTRIM(@GradeBookWeightingLevel))) = 'courselevel'
                    )
                    OR ( ChildResourceId NOT IN ( 107,96,222 ) )
                  )
                  AND (
                        (
                          NOT LOWER(LTRIM(RTRIM(@GradeBookWeightingLevel))) = 'instructorlevel'
                        )
                        OR ( ChildResourceId NOT IN ( 476 ) )
                      )
                  AND (
                        (
                          NOT LOWER(LTRIM(RTRIM(@ShowExternshipTabs))) = 'no'
                        )  
						--OR ( ChildResourceId NOT IN ( 543, 538 ) )  
                        OR ( ChildResourceId NOT IN ( 543 ) )
                      )
                )
        ORDER BY ModuleResourceId
               ,FirstSortOrder
               ,    
 --ParentResourceId,    
                DisplaySequence
               ,ChildResource;   
				  
        DROP TABLE #tmpPlacement_StudentPages;  
        DROP TABLE #tmpFA_StudentPages;  
        DROP TABLE #tmpSA_StudentPages;  
        DROP TABLE #tmpFaculty_StudentPages;  
        DROP TABLE #tmpAcademics_StudentPages;   
    END;  





GO
