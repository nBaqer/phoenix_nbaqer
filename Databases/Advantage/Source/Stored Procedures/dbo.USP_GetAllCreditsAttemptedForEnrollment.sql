SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_GetAllCreditsAttemptedForEnrollment]
    @StuEnrollId VARCHAR(50)
AS
	SELECT ISNULL(SUM(Credits),0) AS CreditsAttempted
	FROM(
    SELECT  B.Credits
    FROM    arClassSections A
            INNER JOIN arReqs B ON B.ReqId = A.ReqId
            INNER JOIN arResults C ON C.TestId = A.ClsSectionId
            INNER JOIN arGradeSystemDetails D ON D.GrdSysDetailId = C.GrdSysDetailId
    WHERE   C.StuEnrollId = @StuEnrollId
            AND D.IsCreditsAttempted = 1
    UNION ALL
    SELECT  RQ.Credits
    FROM    dbo.arTransferGrades TG
            INNER JOIN dbo.arReqs RQ ON RQ.ReqId = TG.ReqId
            INNER JOIN dbo.arGradeSystemDetails GSD ON GSD.GrdSysDetailId = TG.GrdSysDetailId
    WHERE   TG.StuEnrollId = @StuEnrollId
            AND GSD.IsCreditsAttempted = 1)	R 
GO
