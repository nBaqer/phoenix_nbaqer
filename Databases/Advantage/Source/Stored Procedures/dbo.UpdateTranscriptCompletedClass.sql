SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--=================================================================================================
-- UpdateTranscriptCompletedClass
--=================================================================================================
-- Author:		Ginzo, John
-- Create date: 06/16/2015
-- Description:	Change Grades and Terms for transdcript completed classes
-- =============================================
CREATE PROCEDURE [dbo].[UpdateTranscriptCompletedClass]
    @ResultOrTransferId VARCHAR(50)
   ,@TermId VARCHAR(50)
   ,@ReqId VARCHAR(50)
   ,@TestId VARCHAR(50) = NULL
   ,@GrdSysDetailId VARCHAR(50) = NULL
   ,@GradBookWeightingLevel VARCHAR(50)
   ,@user VARCHAR(50)
   ,@StuEnrollId VARCHAR(50)
AS
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;
		
        DECLARE @ReturnVal INT;
        SET @ReturnVal = 0;

        DECLARE @Enrollments TABLE
            (
             StuEnrollID UNIQUEIDENTIFIER
            ,RecordId UNIQUEIDENTIFIER
            );
        INSERT  INTO @Enrollments
                (
                 StuEnrollID
                ,RecordId
                )
        VALUES  (
                 @StuEnrollId
                ,@ResultOrTransferId
                );

        DECLARE @StuEnrollIdInput UNIQUEIDENTIFIER;
        DECLARE @RecordId UNIQUEIDENTIFIER;

        IF @TestId IS NULL
            BEGIN
                                  

				-----------------------------------------------------------
				-- First get the req id of the transferred grade to update
				-----------------------------------------------------------
                DECLARE @Req2id UNIQUEIDENTIFIER;
                SET @Req2id = (
                                SELECT  ReqId
                                FROM    dbo.arTransferGrades
                                WHERE   TransferId = @ResultOrTransferId
                              );	
				
				------------------------------------------------------------------------------------
				--Get the other enrollments with transfer grade for the same course in the same term
				------------------------------------------------------------------------------------				
                INSERT  INTO @Enrollments
                        SELECT DISTINCT
                                r.StuEnrollId
                               ,r.TransferId AS RecordId
                        FROM    dbo.arStuEnrollments se
                        INNER JOIN dbo.arTransferGrades r ON se.StuEnrollId = r.StuEnrollId
                        WHERE   se.StudentId = (
                                                 SELECT StudentId
                                                 FROM   dbo.arStuEnrollments
                                                 WHERE  StuEnrollId = @StuEnrollId
                                               )
                                AND se.StuEnrollId <> @StuEnrollId
                                AND r.ReqId = @Req2id
                                AND r.TermId = @TermId;
				-----------------------------------------------------------
				-- Do the update
				-----------------------------------------------------------	              

                DECLARE UpdateExistingGrade CURSOR
                FOR
                    SELECT  StuEnrollID
                           ,RecordId
                    FROM    @Enrollments;
				

                OPEN UpdateExistingGrade;

                FETCH NEXT FROM UpdateExistingGrade INTO @StuEnrollIdInput,@RecordId;
		 
                BEGIN TRANSACTION;
                BEGIN TRY
                    WHILE @@FETCH_STATUS = 0
                        BEGIN       
							
                            UPDATE  arTransferGrades
                            SET     GrdSysDetailId = @GrdSysDetailId
                                   ,IsCourseCompleted = 1
                                   ,IsGradeOverridden = 1
                                   ,GradeOverriddenBy = @user
                                   ,GradeOverriddenDate = GETDATE()
                                   ,ModUser = @user
                                   ,ModDate = GETDATE()
                                   ,TermId = @TermId
                            WHERE   TransferId = @RecordId;
									
						
                            FETCH NEXT FROM UpdateExistingGrade INTO @StuEnrollIdInput,@RecordId;  
                        END;
                END TRY
                BEGIN CATCH
                    ROLLBACK TRANSACTION;
                    SET @ReturnVal = -1;
                    PRINT ERROR_MESSAGE();
                END CATCH;

                IF @@TranCount > 0
                    COMMIT TRANSACTION;
				

				-----------------------------------------------------------
				-- close the cursor
				-----------------------------------------------------------
                CLOSE UpdateExistingGrade;   
                DEALLOCATE UpdateExistingGrade;
				-----------------------------------------------------------	
                --RETURN @ReturnVal	
            END;
        ELSE
            BEGIN					
											
				---------------------------------------------------------------
				--Get the other enrollments registered for the same class
				----------------------------------------------------------------				
                INSERT  INTO @Enrollments
                        SELECT DISTINCT
                                r.StuEnrollId
                               ,r.ResultId AS RecordId
                        FROM    dbo.arStuEnrollments se
                        INNER JOIN dbo.arResults r ON se.StuEnrollId = r.StuEnrollId
                        WHERE   se.StudentId = (
                                                 SELECT StudentId
                                                 FROM   dbo.arStuEnrollments
                                                 WHERE  StuEnrollId = @StuEnrollId
                                               )
                                AND se.StuEnrollId <> @StuEnrollId
                                AND r.TestId = @TestId;
               			
				-----------------------------------------------------------
				-- Do the update
				-----------------------------------------------------------	              

                DECLARE UpdateExistingGrade CURSOR
                FOR
                    SELECT  StuEnrollID
                           ,RecordId
                    FROM    @Enrollments;
				

                OPEN UpdateExistingGrade;

                FETCH NEXT FROM UpdateExistingGrade INTO @StuEnrollIdInput,@RecordId;
		 
                BEGIN TRANSACTION;
                BEGIN TRY
                    WHILE @@FETCH_STATUS = 0
                        BEGIN       
							
                            UPDATE  arResults
                            SET     GrdSysDetailId = @GrdSysDetailId
                                   ,IsCourseCompleted = 1
                                   ,IsGradeOverridden = 1
                                   ,GradeOverriddenBy = @user
                                   ,GradeOverriddenDate = GETDATE()
                                   ,ModUser = @user
                                   ,ModDate = GETDATE()
                            FROM    arResults r
                            WHERE   ResultId = @RecordId;							 
                              
						
                            FETCH NEXT FROM UpdateExistingGrade INTO @StuEnrollIdInput,@RecordId;  
                        END;
                END TRY
                BEGIN CATCH
                    ROLLBACK TRANSACTION;
                    SET @ReturnVal = -1;
                    PRINT ERROR_MESSAGE();
                END CATCH;

                IF @@TranCount > 0
                    COMMIT TRANSACTION;
				

				-----------------------------------------------------------
				-- close the cursor
				-----------------------------------------------------------
                CLOSE UpdateExistingGrade;   
                DEALLOCATE UpdateExistingGrade;
				-----------------------------------------------------------
			   
                --RETURN @ReturnVal
            END;

    END;
--=================================================================================================
-- END  --  UpdateTranscriptCompletedClass
--=================================================================================================

GO
