SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_RulesByEffectiveDateAndCourse_GetList]
    @EffectiveDate DATETIME
   ,@ReqId UNIQUEIDENTIFIER
AS
    SELECT DISTINCT
            GrdComponentTypeId_ReqId
           ,Descrip
           ,EffectiveDate
           ,NumberofTests
           ,MinimumScore
           ,ROW_NUMBER() OVER ( ORDER BY getDerivedtbl.Descrip ) AS RowNumber
    FROM    (
              SELECT 
                DISTINCT
                        t3.GrdComponentTypeId_ReqId
                       ,t1.Descrip
                       ,t3.EffectiveDate
                       ,t3.NumberofTests
                       ,t3.MinimumScore
              FROM      arGrdComponentTypes t1
              INNER JOIN arBridge_GradeComponentTypes_Courses t2 ON t1.GrdComponentTypeId = t2.GrdComponentTypeId
              INNER JOIN arRules_GradeComponentTypes_Courses t3 ON t2.GrdComponentTypeId_ReqId = t3.GrdComponentTypeId_ReqId
              WHERE     t3.EffectiveDate = @EffectiveDate
                        AND t2.ReqId = @ReqId
              UNION
              SELECT 
                DISTINCT
                        t2.GrdComponentTypeId_ReqId
                       ,t1.Descrip
                       ,NULL AS EffectiveDate
                       ,NULL AS NumberofTests
                       ,NULL AS MinimumScore
              FROM      arGrdComponentTypes t1
              INNER JOIN arBridge_GradeComponentTypes_Courses t2 ON t1.GrdComponentTypeId = t2.GrdComponentTypeId
              WHERE     t2.GrdComponentTypeId_ReqId NOT IN ( SELECT DISTINCT
                                                                    t3.GrdComponentTypeId_ReqId
                                                             FROM   arRules_GradeComponentTypes_Courses t3
                                                             WHERE  t3.EffectiveDate = @EffectiveDate )
                        AND t2.ReqId = @ReqId
            ) getDerivedtbl;



GO
