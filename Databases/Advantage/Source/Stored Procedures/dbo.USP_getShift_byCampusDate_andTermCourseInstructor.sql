SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--=================================================================================================
-- USP_getShift_byCampusDate_andTermCourseInstructor
--=================================================================================================
CREATE PROCEDURE [dbo].[USP_getShift_byCampusDate_andTermCourseInstructor]
    @Campus UNIQUEIDENTIFIER
   ,@WeekEndDate DATE
   ,@TermId VARCHAR(8000) = NULL
   ,@CourseId VARCHAR(8000) = NULL
   ,@InstructorId VARCHAR(8000) = NULL
   ,@ShowInactive BIT
AS --    
    BEGIN    
        DECLARE @ClassesThatFallWithinDateRange TABLE
            (
             CampusId UNIQUEIDENTIFIER
            ,TermId UNIQUEIDENTIFIER
            ,ReqId UNIQUEIDENTIFIER
            ,InstructorId UNIQUEIDENTIFIER
            ,ShiftId UNIQUEIDENTIFIER
            );    
        IF LEN(@TermId) = 0
            BEGIN
             
                SET @TermId = NULL;
            END;
        IF LEN(@CourseId) = 0
            BEGIN
             
                SET @CourseId = NULL;
            END;
        IF LEN(@InstructorId) = 0
            BEGIN
             
                SET @InstructorId = NULL;
            END;
        INSERT  INTO @ClassesThatFallWithinDateRange
                SELECT DISTINCT
                        CampusId UNIQUEIDENTIFIER
                       ,TermId UNIQUEIDENTIFIER
                       ,ReqId UNIQUEIDENTIFIER
                       ,InstructorId UNIQUEIDENTIFIER
                       ,shiftid UNIQUEIDENTIFIER
                FROM    dbo.arClassSections ARC
                WHERE   @WeekEndDate BETWEEN ARC.StartDate AND ARC.EndDate
                        AND ARC.CampusId = @Campus
                        AND (
                              @TermId IS NULL
                              OR ARC.TermId IN ( SELECT strval
                                                 FROM   dbo.SPLIT(@TermId) )
                            )
                        AND (
                              @CourseId IS NULL
                              OR ARC.ReqId IN ( SELECT  strval
                                                FROM    dbo.SPLIT(@CourseId) )
                            )
                        AND (
                              @InstructorId IS NULL
                              OR ARC.InstructorId IN ( SELECT   strval
                                                       FROM     dbo.SPLIT(@InstructorId) )
                            );     
    
        SELECT DISTINCT
                sh.ShiftId
               ,sh.ShiftDescrip
               ,sh.ShiftCode
        FROM    arShifts sh
        INNER JOIN @ClassesThatFallWithinDateRange CDR ON sh.ShiftId = CDR.ShiftId
        WHERE   (
                  @ShowInactive = 0
                  AND sh.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
                )
                OR @ShowInactive = 1
        ORDER BY sh.ShiftDescrip;     
    END;    
--=================================================================================================
-- END  --  USP_getShift_byCampusDate_andTermCourseInstructor
--=================================================================================================
GO
