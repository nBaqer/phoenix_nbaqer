SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_StatusCode_GET_IgnoreSelected]
    @CampusId UNIQUEIDENTIFIER
   ,@ShowActiveOnly BIT = 1
   ,@SelectedStatus VARCHAR(8000)
AS
    BEGIN
        IF ( @ShowActiveOnly = 1 )
            BEGIN
                SELECT  SC.StatusCodeId
                       ,SC.StatusCode
                       ,SC.StatusCodeDescrip
                       ,SC.StatusId
                FROM    syStatusCodes SC
                INNER JOIN syCampGrps CG ON CG.CampGrpId = SC.CampGrpId
                INNER JOIN dbo.syCmpGrpCmps CGC ON CG.CampGrpId = CGC.CampGrpId
                WHERE   CGC.CampusId = @CampusId
                        AND SC.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
                        AND SC.StatusCodeId NOT IN ( SELECT Val
                                                     FROM   MultipleValuesForReportParameters(@SelectedStatus,',',1) )
                ORDER BY SC.StatusCodeDescrip;
            END;
        ELSE
            BEGIN
                SELECT  SC.StatusCodeId
                       ,SC.StatusCode
                       ,SC.StatusCodeDescrip
                       ,SC.StatusId
                FROM    syStatusCodes SC
                INNER JOIN syCampGrps CG ON CG.CampGrpId = SC.CampGrpId
                INNER JOIN dbo.syCmpGrpCmps CGC ON CG.CampGrpId = CGC.CampGrpId
                WHERE   CGC.CampusId = @CampusId
                        AND SC.StatusCodeId NOT IN ( SELECT Val
                                                     FROM   MultipleValuesForReportParameters(@SelectedStatus,',',1) )
                ORDER BY SC.StatusCodeDescrip;
            END;
    END;

GO
