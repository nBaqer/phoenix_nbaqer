SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

--========================================================================================== 
-- USP_Rooms_GetList
--==========================================================================================
CREATE PROCEDURE [dbo].[USP_Rooms_GetList]
    @StuEnrollId UNIQUEIDENTIFIER
   ,@ClsSectionId UNIQUEIDENTIFIER
AS
    BEGIN
        SELECT DISTINCT
                p.PeriodDescrip
               ,AB.BldgDescrip
               ,AR2.Descrip AS Room
        FROM    arResults AS AR
        LEFT JOIN arClsSectMeetings AS ACSM ON ACSM.ClsSectionId = AR.TestId
        INNER JOIN syPeriods p ON ACSM.PeriodId = p.PeriodId
        INNER JOIN arRooms AS AR2 ON AR2.RoomId = ACSM.RoomId
        INNER JOIN arBuildings AS AB ON AB.BldgId = AR2.BldgId
        WHERE   AR.StuEnrollId = @StuEnrollId
                AND AR.TestId = @ClsSectionId;

    END;
--========================================================================================== 
-- END  --  USP_Rooms_GetList
--========================================================================================== 


GO
