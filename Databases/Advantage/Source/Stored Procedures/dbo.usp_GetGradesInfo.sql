SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_GetGradesInfo]
    (
     @grdSysDetailId UNIQUEIDENTIFIER
 

    )
AS
    SET NOCOUNT ON;
    SELECT DISTINCT
            Grade
           ,GradeDescription
           ,Quality
           ,GrdSystemId
           ,GrdSysDetailId
    FROM    arGradeSystemDetails
    WHERE   GrdSysDetailId = @grdSysDetailId; 



GO
