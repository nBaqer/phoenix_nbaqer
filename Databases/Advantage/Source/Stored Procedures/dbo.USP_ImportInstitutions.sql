SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_ImportInstitutions]
AS
    DECLARE @CountryId UNIQUEIDENTIFIER
       ,@CampGrpId UNIQUEIDENTIFIER;
    DECLARE @AddressTypeId UNIQUEIDENTIFIER
       ,@PhoneTypeId UNIQUEIDENTIFIER
       ,@USACountryId UNIQUEIDENTIFIER
       ,@MailingAddressTypeId UNIQUEIDENTIFIER;

    SET @CountryId = (
                       SELECT TOP 1
                                CountryId
                       FROM     dbo.adCountries
                       WHERE    IsDefault = 1
                     ); -- USA
    SET @CampGrpId = (
                       SELECT TOP 1
                                CampGrpId
                       FROM     syCampGrps
                       WHERE    CampGrpDescrip = 'All'
                     );

    IF NOT EXISTS ( SELECT  *
                    FROM    dbo.plAddressTypes
                    WHERE   AddressDescrip = 'Mailing' )
        BEGIN
            INSERT  INTO dbo.plAddressTypes
                    (
                     AddressTypeId
                    ,AddressDescrip
                    ,StatusId
                    ,CampGrpId
                    ,AddressCode
                    ,ModDate
                    ,ModUser
			        )
            VALUES  (
                     NEWID()  -- AddressTypeId - uniqueidentifier
                    ,'Mailing'  -- AddressDescrip - varchar(50)
                    ,'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'  -- StatusId - uniqueidentifier
                    ,@CampGrpId  -- CampGrpId - uniqueidentifier
                    ,'Mailing'  -- AddressCode - varchar(12)
                    ,GETDATE()  -- ModDate - datetime
                    ,'support'  -- ModUser - varchar(50)
			        );
        END;


    SET @AddressTypeId = (
                           SELECT TOP 1
                                    AddressTypeId
                           FROM     dbo.plAddressTypes
                           WHERE    AddressDescrip = 'Work'
                         );
    SET @PhoneTypeId = (
                         SELECT TOP 1
                                PhoneTypeId
                         FROM   dbo.syPhoneType
                         WHERE  PhoneTypeDescrip = 'Work'
                       );
    SET @MailingAddressTypeId = (
                                  SELECT TOP 1
                                            AddressTypeId
                                  FROM      dbo.plAddressTypes
                                  WHERE     AddressDescrip = 'Mailing'
                                );
    SET @USACountryId = (
                          SELECT TOP 1
                                    CountryId
                          FROM      dbo.adCountries
                          WHERE     CountryCode LIKE 'US%'
                        );

    IF (
         --Imported, Private, HighSchool
         SELECT COUNT(*)
         FROM   syInstitutions
         WHERE  ImportTypeId = 2
                AND TypeId = 1
                AND LevelID = 2
       ) = 0
        BEGIN
            INSERT  INTO syInstitutions
                    (
                     HSId
                    ,HSCode
                    ,StatusId
                    ,HSName
                    ,CampGrpId
                    ,ModUser
                    ,ModDate
                    ,ImportTypeId
                    ,TypeId
                    ,LevelID
                    )
                    SELECT  InstitutionId
                           ,[Nces Id]
                           ,'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
                           ,[School Name]
                           ,@CampGrpId
                           ,'support'
                           ,GETDATE()
                           ,2
                           ,1
                           ,2
                    FROM    PrivateSchoolsList
                    WHERE   [High Grade] = '12'
                    ORDER BY [School Name];

		--Work Address
            INSERT  INTO syInstitutionAddresses
                    (
                     InstitutionAddressId
                    ,InstitutionId
                    ,AddressTypeId
                    ,Address1
                    ,Address2
                    ,City
                    ,StateId
                    ,ZipCode
                    ,CountryId
                    ,StatusId
                    ,IsMailingAddress
                    ,ModUser
                    ,ModDate
                    ,State
                    ,IsInternational
                    ,CountyId
                    ,ForeignCountyStr
                    ,AddressApto
                    ,OtherState
                    ,IsDefault
                    )
                    SELECT  NEWID()
                           ,InstitutionId
                           ,@AddressTypeId
                           ,Address
                           ,NULL
                           ,City
                           ,(
                              SELECT TOP 1
                                        StateId
                              FROM      syStates
                              WHERE     StateCode = State
                            )
                           ,Zip
                           ,@USACountryId
                           ,'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
                           ,0
                           ,'support'
                           ,GETDATE()
                           ,NULL
                           ,0
                           ,(
                              SELECT TOP 1
                                        CountyId
                              FROM      dbo.adCounties
                              WHERE     CountyCode = ["Ansi Fips County Number"]
                            ) AS CountyId
                           ,NULL
                           ,NULL
                           ,NULL
                           ,1
                    FROM    PrivateSchoolsList
                    WHERE   [High Grade] = '12'
                    ORDER BY [School Name];

		--Mailing Address
            INSERT  INTO syInstitutionAddresses
                    (
                     InstitutionAddressId
                    ,InstitutionId
                    ,AddressTypeId
                    ,Address1
                    ,Address2
                    ,City
                    ,StateId
                    ,ZipCode
                    ,CountryId
                    ,StatusId
                    ,IsMailingAddress
                    ,ModUser
                    ,ModDate
                    ,State
                    ,IsInternational
                    ,CountyId
                    ,ForeignCountyStr
                    ,AddressApto
                    ,OtherState
                    ,IsDefault
                    )
                    SELECT  NEWID()
                           ,InstitutionId
                           ,@MailingAddressTypeId
                           ,["Mailing Address"]
                           ,NULL
                           ,["Mailing City"]
                           ,(
                              SELECT TOP 1
                                        StateId
                              FROM      syStates
                              WHERE     StateCode = ["Mailing State"]
                            )
                           ,["Mailing Zip"]
                           ,@USACountryId
                           ,'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
                           ,0
                           ,'support'
                           ,GETDATE()
                           ,NULL
                           ,0
                           ,(
                              SELECT TOP 1
                                        CountyId
                              FROM      dbo.adCounties
                              WHERE     CountyCode = ["Ansi Fips County Number"]
                            ) AS CountyId
                           ,NULL
                           ,NULL
                           ,NULL
                           ,0
                    FROM    PrivateSchoolsList
                    WHERE   [High Grade] = '12'
                    ORDER BY [School Name];

            INSERT  INTO syInstitutionPhone
                    (
                     InstitutionPhoneId
                    ,InstitutionId
                    ,PhoneTypeId
                    ,Phone
                    ,ModDate
                    ,ModUser
                    ,IsForeignPhone
                    ,StatusId
                    ,IsDefault
                    )
                    SELECT  NEWID()
                           ,InstitutionId
                           ,@PhoneTypeId
                           ,dbo.fnRemovePhoneFormatting(Phone)
                           ,GETDATE()
                           ,'support'
                           ,0
                           ,'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
                           ,1
                    FROM    PrivateSchoolsList
                    WHERE   [High Grade] = '12'
                    ORDER BY [School Name];

        END;
    IF (
         --Imported, Public, High School
         SELECT COUNT(*)
         FROM   syInstitutions
         WHERE  ImportTypeId = 2
                AND TypeId = 2
                AND LevelID = 2
       ) = 0
        BEGIN
		--Institutions
            INSERT  INTO syInstitutions
                    (
                     HSId
                    ,HSCode
                    ,StatusId
                    ,HSName
                    ,CampGrpId
                    ,ModUser
                    ,ModDate
                    ,ImportTypeId
                    ,TypeId
                    ,LevelID
                    )
                    SELECT  InstitutionId
                           ,[Nces Id]
                           ,'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
                           ,[School Name]
                           ,@CampGrpId
                           ,'support'
                           ,GETDATE()
                           ,2
                           ,2
                           ,2
                    FROM    PublicSchoolsList
                    WHERE   [High Grade] = '12'
                    ORDER BY [School Name];

		--Work Address
            INSERT  INTO syInstitutionAddresses
                    (
                     InstitutionAddressId
                    ,InstitutionId
                    ,AddressTypeId
                    ,Address1
                    ,Address2
                    ,City
                    ,StateId
                    ,ZipCode
                    ,CountryId
                    ,StatusId
                    ,IsMailingAddress
                    ,ModUser
                    ,ModDate
                    ,State
                    ,IsInternational
                    ,CountyId
                    ,ForeignCountyStr
                    ,AddressApto
                    ,OtherState
                    ,IsDefault
                    )
                    SELECT  NEWID()
                           ,InstitutionId
                           ,@AddressTypeId
                           ,Address
                           ,NULL
                           ,City
                           ,(
                              SELECT TOP 1
                                        StateId
                              FROM      syStates
                              WHERE     StateCode = State
                            )
                           ,Zip
                           ,@USACountryId
                           ,'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
                           ,0
                           ,'support'
                           ,GETDATE()
                           ,NULL
                           ,0
                           ,(
                              SELECT TOP 1
                                        CountyId
                              FROM      dbo.adCounties
                              WHERE     CountyCode = ["Ansi Fips County Number"]
                            ) AS CountyId
                           ,NULL
                           ,NULL
                           ,NULL
                           ,1
                    FROM    PublicSchoolsList
                    WHERE   [High Grade] = '12'
                    ORDER BY [School Name];

		--Mailing Address
            INSERT  INTO syInstitutionAddresses
                    (
                     InstitutionAddressId
                    ,InstitutionId
                    ,AddressTypeId
                    ,Address1
                    ,Address2
                    ,City
                    ,StateId
                    ,ZipCode
                    ,CountryId
                    ,StatusId
                    ,IsMailingAddress
                    ,ModUser
                    ,ModDate
                    ,State
                    ,IsInternational
                    ,CountyId
                    ,ForeignCountyStr
                    ,AddressApto
                    ,OtherState
                    ,IsDefault
                    )
                    SELECT  NEWID()
                           ,InstitutionId
                           ,@MailingAddressTypeId
                           ,["Mailing Address"]
                           ,NULL
                           ,["Mailing City"]
                           ,(
                              SELECT TOP 1
                                        StateId
                              FROM      syStates
                              WHERE     StateCode = ["Mailing State"]
                            )
                           ,["Mailing Zip"]
                           ,@USACountryId
                           ,'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
                           ,0
                           ,'support'
                           ,GETDATE()
                           ,NULL
                           ,0
                           ,(
                              SELECT TOP 1
                                        CountyId
                              FROM      dbo.adCounties
                              WHERE     CountyCode = ["Ansi Fips County Number"]
                            ) AS CountyId
                           ,NULL
                           ,NULL
                           ,NULL
                           ,0
                    FROM    PublicSchoolsList
                    WHERE   [High Grade] = '12'
                    ORDER BY [School Name];

		--Institution Phone
            INSERT  INTO syInstitutionPhone
                    (
                     InstitutionPhoneId
                    ,InstitutionId
                    ,PhoneTypeId
                    ,Phone
                    ,ModDate
                    ,ModUser
                    ,IsForeignPhone
                    ,StatusId
                    ,IsDefault
                    )
                    SELECT  NEWID()
                           ,InstitutionId
                           ,@PhoneTypeId
                           ,dbo.fnRemovePhoneFormatting(Phone)
                           ,GETDATE()
                           ,'support'
                           ,0
                           ,'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
                           ,1
                    FROM    PublicSchoolsList
                    WHERE   [High Grade] = '12'
                    ORDER BY [School Name];

        END;
    IF (
         SELECT COUNT(*)
         FROM   syInstitutions
         WHERE  ImportTypeId = 2
                AND TypeId = 1
                AND LevelID = 1
       ) = 0
        BEGIN
            INSERT  INTO syInstitutions
                    (
                     HSId
                    ,HSCode
                    ,StatusId
                    ,HSName
                    ,CampGrpId
                    ,ModUser
                    ,ModDate
                    ,ImportTypeId
                    ,TypeId
                    ,LevelID
                    )
                    SELECT  InstitutionId
                           ,id
                           ,'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
                           ,biz_name
                           ,@CampGrpId
                           ,'support'
                           ,GETDATE()
                           ,2
                           ,1
                           ,1
                    FROM    CollegesList
                    ORDER BY biz_name;

		--Work Address
            INSERT  INTO syInstitutionAddresses
                    (
                     InstitutionAddressId
                    ,InstitutionId
                    ,AddressTypeId
                    ,Address1
                    ,Address2
                    ,City
                    ,StateId
                    ,ZipCode
                    ,CountryId
                    ,StatusId
                    ,IsMailingAddress
                    ,ModUser
                    ,ModDate
                    ,State
                    ,IsInternational
                    ,CountyId
                    ,ForeignCountyStr
                    ,AddressApto
                    ,OtherState
                    ,IsDefault
                    )
                    SELECT  NEWID()
                           ,InstitutionId
                           ,@AddressTypeId
                           ,e_address
                           ,NULL
                           ,e_city
                           ,(
                              SELECT TOP 1
                                        StateId
                              FROM      syStates
                              WHERE     StateCode = e_state
                            )
                           ,e_zip_full
                           ,@USACountryId
                           ,'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
                           ,0
                           ,'support'
                           ,GETDATE()
                           ,NULL
                           ,0
                           ,(
                              SELECT TOP 1
                                        CountyId
                              FROM      dbo.adCounties
                              WHERE     CountyCode = loc_county
                            ) AS CountyId
                           ,NULL
                           ,NULL
                           ,NULL
                           ,1
                    FROM    CollegesList
                    ORDER BY biz_name;

            INSERT  INTO syInstitutionPhone
                    (
                     InstitutionPhoneId
                    ,InstitutionId
                    ,PhoneTypeId
                    ,Phone
                    ,ModDate
                    ,ModUser
                    ,IsForeignPhone
                    ,StatusId
                    ,IsDefault
                    )
                    SELECT  NEWID()
                           ,InstitutionId
                           ,@PhoneTypeId
                           ,dbo.fnRemovePhoneFormatting(biz_phone)
                           ,GETDATE()
                           ,'support'
                           ,0
                           ,'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
                           ,1
                    FROM    CollegesList
                    ORDER BY biz_name;

        END;
GO
