SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

--EXEC USP_ManageSecurity_AR_SubMenus 'BB0E90D6-4D77-4362-95FA-B2D34260A4A1',191
--EXEC USP_ManageSecurity_AR_SubMenus '436FFB3E-A55F-49BB-908D-70B104BC952D', 189
--SELECT * FROM syRoles
CREATE PROCEDURE [dbo].[USP_ManageSecurity_AR_SubMenus]
    @RoleId VARCHAR(50)
   ,@ModuleResourceId INT
AS --DECLARE @RoleId UNIQUEIDENTIFIER
--SET @RoleId='BB0E90D6-4D77-4362-95FA-B2D34260A4A1'
    SELECT  *
           ,CASE WHEN AccessLevel = 15 THEN 1
                 ELSE 0
            END AS FullPermission
           ,CASE WHEN AccessLevel IN ( 14,13,12,11,10,9,8 ) THEN 1
                 ELSE 0
            END AS EditPermission
           ,CASE WHEN AccessLevel IN ( 14,13,12,7,6,5,4 ) THEN 1
                 ELSE 0
            END AS AddPermission
           ,CASE WHEN AccessLevel IN ( 14,11,10,7,6,2 ) THEN 1
                 ELSE 0
            END AS DeletePermission
           ,CASE WHEN AccessLevel IN ( 13,11,9,7,5,3,1 ) THEN 1
                 ELSE 0
            END AS ReadPermission
    FROM    (
              --Admissions
			  SELECT    189 AS ModuleResourceId
                       ,ResourceId AS ChildResourceId
                       ,Resource AS ChildResource
                       ,(
                          SELECT    AccessLevel
                          FROM      syRlsResLvls
                          WHERE     RoleId = @RoleId
                                    AND ResourceId = syResources.ResourceId
                        ) AS AccessLevel
                       ,ResourceURL AS ChildResourceURL
                       ,NULL AS ParentResourceId
                       ,NULL AS ParentResource
                       ,1 AS GroupSortOrder
                       ,NULL AS FirstSortOrder
                       ,NULL AS DisplaySequence
                       ,ResourceTypeID
              FROM      syResources
              WHERE     ResourceId = 189
              UNION
              SELECT    189 AS ModuleResourceId
                       ,NNChild.ResourceId AS ChildResourceId
                       ,CASE WHEN NNChild.ResourceId = 395 THEN 'Lead Tabs'
                             ELSE CASE WHEN NNChild.ResourceId = 398 THEN 'Add/View Leads'
                                       ELSE RChild.Resource
                                  END
                        END AS ChildResource
                       ,(
                          SELECT    AccessLevel
                          FROM      syRlsResLvls
                          WHERE     RoleId = @RoleId
                                    AND ResourceId = NNChild.ResourceId
                        ) AS AccessLevel
                       ,RChild.ResourceURL AS ChildResourceURL
                       ,CASE WHEN NNParent.ResourceId IN ( 687,688,689 ) THEN NULL
                             ELSE NNParent.ResourceId
                        END AS ParentResourceId
                       ,RParent.Resource AS ParentResource
                       ,CASE WHEN (
                                    NNChild.ResourceId = 694
                                    OR NNParent.ResourceId = 694
                                  ) THEN 2
                             ELSE 3
                        END AS GroupSortOrder
                       ,CASE WHEN (
                                    NNChild.ResourceId = 694
                                    OR NNParent.ResourceId = 694
                                  ) THEN 2
                             ELSE 3
                        END AS FirstSortOrder
                       ,CASE WHEN (
                                    NNChild.ResourceId IN ( 398 )
                                    OR NNParent.ResourceId IN ( 398 )
                                  ) THEN 1
                             ELSE 2
                        END AS DisplaySequence
                       ,RChild.ResourceTypeID
	--NNChild.HierarchyIndex AS SecondSortOrder,
	--RParentModule.Resource AS Module
              FROM      syResources RChild
              INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
              INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
              INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
              LEFT OUTER JOIN (
                                SELECT  *
                                FROM    syResources
                                WHERE   ResourceTypeId = 1
                              ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
              WHERE     RChild.ResourceTypeId IN ( 2,3,8 )
                        AND (
                              RChild.ChildTypeId IS NULL
                              OR RChild.ChildTypeId = 3
                            )
                        AND ( RChild.ResourceId NOT IN ( 395 ) )
                        AND ( NNParent.ResourceId NOT IN ( 395 ) )
                        AND (
                              NNParent.ParentId IN ( SELECT HierarchyId
                                                     FROM   syNavigationNodes
                                                     WHERE  ResourceId = 189 )
                              OR NNChild.ParentId IN ( SELECT   HierarchyId
                                                       FROM     syNavigationNodes
                                                       WHERE    ResourceId IN ( 694,398 ) )
                            )
              UNION
              SELECT    26 AS ModuleResourceId
                       ,ResourceId AS ChildResourceId
                       ,Resource AS ChildResource
                       ,(
                          SELECT DISTINCT
                                    AccessLevel
                          FROM      syRlsResLvls
                          WHERE     RoleId = @RoleId
                                    AND ResourceId = syResources.ResourceId
                        ) AS AccessLevel
                       ,ResourceURL AS ChildResourceURL
                       ,NULL AS ParentResourceId
                       ,NULL AS ParentResource
                       ,1 AS GroupSortOrder
                       ,NULL AS FirstSortOrder
                       ,NULL AS DisplaySequence
                       ,ResourceTypeID
              FROM      syResources
              WHERE     ResourceId = 26
              UNION
              SELECT    26 AS ModuleResourceId
                       ,ChildResourceId
                       ,ChildResource
                       ,(
                          SELECT DISTINCT
                                    AccessLevel
                          FROM      syRlsResLvls
                          WHERE     RoleId = @RoleId
                                    AND ResourceId = ChildResourceId
                        ) AS AccessLevel
                       ,ChildResourceURL
                       ,ParentResourceId
                       ,ParentResource
                       ,CASE WHEN (
                                    ChildResourceId = 693
                                    OR ParentResourceId = 693
                                  ) THEN 2
                             ELSE CASE WHEN (
                                              ChildResourceId = 132
                                              OR ParentResourceId = 132
                                            ) THEN 3
                                       ELSE CASE WHEN (
                                                        ChildResourceId = 160
                                                        OR ParentResourceId = 160
                                                      ) THEN 4
                                                 ELSE CASE WHEN (
                                                                  ChildResourceId = 129
                                                                  OR ParentResourceId = 129
                                                                ) THEN 5
                                                           ELSE CASE WHEN (
                                                                            ChildResourceId = 71
                                                                            OR ParentResourceId = 71
                                                                          ) THEN 6
                                                                     ELSE CASE WHEN (
                                                                                      ChildResourceId = 190
                                                                                      OR ParentResourceId = 190
                                                                                    ) THEN 7
                                                                               ELSE 8
                                                                          END
                                                                END
                                                      END
                                            END
                                  END
                        END AS GroupSortOrder
                       ,(
                          SELECT TOP 1
                                    HierarchyIndex
                          FROM      dbo.syNavigationNodes
                          WHERE     ResourceId = ChildResourceId
                        ) AS FirstSortOrder
                       ,CASE WHEN (
                                    ChildResourceId IN ( 190,129,317,71 )
                                    OR ParentResourceId IN ( 190,129,317,71 )
                                  ) THEN 1
                             ELSE 2
                        END AS DisplaySequence
                       ,ResourceTypeID
              FROM      (
                          SELECT DISTINCT
                                    NNChild.ResourceId AS ChildResourceId
                                   ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                         ELSE RChild.Resource
                                    END AS ChildResource
                                   ,RChild.ResourceURL AS ChildResourceURL
                                   ,CASE WHEN NNParent.ResourceId = 687 THEN NULL
                                         ELSE NNParent.ResourceId
                                    END AS ParentResourceId
                                   ,RParent.Resource AS ParentResource
                                   ,RParentModule.Resource AS MODULE
                                   ,RChild.ResourceTypeId
				--NNParent.ParentId
                          FROM      syResources RChild
                          INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
                          INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                          INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                          LEFT OUTER JOIN (
                                            SELECT  *
                                            FROM    syResources
                                            WHERE   ResourceTypeId = 1
                                          ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                          WHERE     RChild.ResourceTypeId IN ( 2,3,8 )
                                    AND (
                                          RChild.ChildTypeId IS NULL
                                          OR RChild.ChildTypeId = 3
                                        )
                                    AND ( RChild.ResourceId <> 394 )
                                    AND ( NNParent.ResourceId NOT IN ( 394 ) )
                                    AND (
                                          NNParent.Parentid IN ( SELECT HierarchyId
                                                                 FROM   syNavigationNodes
                                                                 WHERE  ResourceId = 26 )
                                          OR NNChild.ParentId IN ( SELECT   HierarchyId
                                                                   FROM     syNavigationNodes
                                                                   WHERE    ResourceId IN ( 693,71,129,132,160,190 ) )
                                        )
                        ) t5
--Student Accounts
              UNION
              SELECT    194 AS ModuleResourceId
                       ,ResourceId AS ChildResourceId
                       ,Resource AS ChildResource
                       ,(
                          SELECT    AccessLevel
                          FROM      syRlsResLvls
                          WHERE     RoleId = @RoleId
                                    AND ResourceId = syResources.ResourceId
                        ) AS AccessLevel
                       ,ResourceURL AS ChildResourceURL
                       ,NULL AS ParentResourceId
                       ,NULL AS ParentResource
                       ,1 AS GroupSortOrder
                       ,NULL AS FirstSortOrder
                       ,NULL AS DisplaySequence
                       ,ResourceTypeID
              FROM      syResources
              WHERE     ResourceId = 194
              UNION
              SELECT    194 AS ModuleResourceId
                       ,ChildResourceId
                       ,ChildResource
                       ,(
                          SELECT DISTINCT
                                    AccessLevel
                          FROM      syRlsResLvls
                          WHERE     RoleId = @RoleId
                                    AND ResourceId = ChildResourceId
                        ) AS AccessLevel
                       ,ChildResourceURL
                       ,ParentResourceId
                       ,ParentResource
                       ,CASE WHEN (
                                    ChildResourceId = 695
                                    OR ParentResourceId = 695
                                  ) THEN 2
                             ELSE CASE WHEN (
                                              ChildResourceId = 403
                                              OR ParentResourceId = 403
                                            ) THEN 3
                                       ELSE 4
                                  END
                        END AS GroupSortOrder
                       ,(
                          SELECT TOP 1
                                    HierarchyIndex
                          FROM      dbo.syNavigationNodes
                          WHERE     ResourceId = ChildResourceId
                        ) AS FirstSortOrder
                       ,CASE WHEN (
                                    ChildResourceId IN ( 695 )
                                    OR ParentResourceId IN ( 695 )
                                  ) THEN 1
                             ELSE 2
                        END AS DisplaySequence
                       ,ResourceTypeID
              FROM      (
                          SELECT DISTINCT
                                    NNChild.ResourceId AS ChildResourceId
                                   ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                         ELSE RChild.Resource
                                    END AS ChildResource
                                   ,RChild.ResourceURL AS ChildResourceURL
                                   ,CASE WHEN NNParent.ResourceId IN ( 194 )
                                              OR NNChild.ResourceId IN ( 695,403,698 ) THEN NULL
                                         ELSE NNParent.ResourceId
                                    END AS ParentResourceId
                                   ,RParent.Resource AS ParentResource
                                   ,RParentModule.Resource AS MODULE
                                   ,RChild.ResourceTypeID
				--NNParent.ParentId
                          FROM      syResources RChild
                          INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
                          INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                          INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                          LEFT OUTER JOIN (
                                            SELECT  *
                                            FROM    syResources
                                            WHERE   ResourceTypeId = 1
                                          ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                          WHERE     RChild.ResourceTypeId IN ( 2,3,8 )
                                    AND (
                                          RChild.ChildTypeId IS NULL
                                          OR RChild.ChildTypeId = 3
                                        )
                                    AND (
                                          NNParent.Parentid IN ( SELECT HierarchyId
                                                                 FROM   syNavigationNodes
                                                                 WHERE  ResourceId = 194 )
                                          OR NNChild.ParentId IN ( SELECT   HierarchyId
                                                                   FROM     syNavigationNodes
                                                                   WHERE    ResourceId IN ( 403,695,698 ) )
                                        )
                                    AND ( RChild.ResourceId <> 394 )
                                    AND ( NNParent.ResourceId NOT IN ( 394 ) )
                        ) t1 
--Faculty
              UNION
              SELECT    300 AS ModuleResourceId
                       ,ResourceId AS ChildResourceId
                       ,Resource AS ChildResource
                       ,(
                          SELECT    AccessLevel
                          FROM      syRlsResLvls
                          WHERE     RoleId = @RoleId
                                    AND ResourceId = syResources.ResourceId
                        ) AS AccessLevel
                       ,ResourceURL AS ChildResourceURL
                       ,NULL AS ParentResourceId
                       ,NULL AS ParentResource
                       ,1 AS GroupSortOrder
                       ,NULL AS FirstSortOrder
                       ,NULL AS DisplaySequence
                       ,ResourceTypeID
              FROM      syResources
              WHERE     ResourceId = 300
              UNION
              SELECT    300 AS ModuleResourceId
                       ,ChildResourceId
                       ,ChildResource
                       ,(
                          SELECT DISTINCT
                                    AccessLevel
                          FROM      syRlsResLvls
                          WHERE     RoleId = @RoleId
                                    AND ResourceId = ChildResourceId
                        ) AS AccessLevel
                       ,ChildResourceURL
                       ,ParentResourceId
                       ,ParentResource
                       ,CASE WHEN (
                                    ChildResourceId = 697
                                    OR ParentResourceId = 697
                                  ) THEN 2
                             ELSE 3
                        END AS GroupSortOrder
                       ,(
                          SELECT TOP 1
                                    HierarchyIndex
                          FROM      dbo.syNavigationNodes
                          WHERE     ResourceId = ChildResourceId
                        ) AS FirstSortOrder
                       ,1 AS DisplaySequence
                       ,ResourceTypeId
              FROM      (
                          SELECT DISTINCT
                                    NNChild.ResourceId AS ChildResourceId
                                   ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                         ELSE RChild.Resource
                                    END AS ChildResource
                                   ,RChild.ResourceURL AS ChildResourceURL
                                   ,CASE WHEN NNParent.ResourceId = 300
                                              OR NNChild.ResourceId = 697 THEN NULL
                                         ELSE NNParent.ResourceId
                                    END AS ParentResourceId
                                   ,RParent.Resource AS ParentResource
                                   ,RParentModule.Resource AS MODULE
                                   ,RChild.ResourceTypeId
				--NNParent.ParentId
                          FROM      syResources RChild
                          INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
                          INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                          INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                          LEFT OUTER JOIN (
                                            SELECT  *
                                            FROM    syResources
                                            WHERE   ResourceTypeId = 1
                                          ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                          WHERE     RChild.ResourceTypeId IN ( 2,3,8 )
                                    AND (
                                          RChild.ChildTypeId IS NULL
                                          OR RChild.ChildTypeId = 3
                                        )
                                    AND ( RChild.ResourceId <> 394 )
                                    AND ( NNParent.ResourceId NOT IN ( 394 ) )
                                    AND (
                                          NNParent.Parentid IN ( SELECT HierarchyId
                                                                 FROM   syNavigationNodes
                                                                 WHERE  ResourceId = 300 )
                                          OR NNChild.ParentId IN ( SELECT   HierarchyId
                                                                   FROM     syNavigationNodes
                                                                   WHERE    ResourceId IN ( 697 ) )
                                        )
                        ) t3
              UNION
--Financial Aid
              SELECT    191 AS ModuleResourceId
                       ,ResourceId AS ChildResourceId
                       ,Resource AS ChildResource
                       ,(
                          SELECT    AccessLevel
                          FROM      syRlsResLvls
                          WHERE     RoleId = @RoleId
                                    AND ResourceId = syResources.ResourceId
                        ) AS AccessLevel
                       ,ResourceURL AS ChildResourceURL
                       ,NULL AS ParentResourceId
                       ,NULL AS ParentResource
                       ,1 AS GroupSortOrder
                       ,NULL AS FirstSortOrder
                       ,NULL AS DisplaySequence
                       ,ResourceTypeID
              FROM      syResources
              WHERE     ResourceId = 191
              UNION
              SELECT    191 AS ModuleResourceId
                       ,ChildResourceId
                       ,ChildResource
                       ,(
                          SELECT DISTINCT
                                    AccessLevel
                          FROM      syRlsResLvls
                          WHERE     RoleId = @RoleId
                                    AND ResourceId = ChildResourceId
                        ) AS AccessLevel
                       ,ChildResourceURL
                       ,ParentResourceId
                       ,ParentResource
                       ,CASE WHEN (
                                    ChildResourceId = 700
                                    OR ParentResourceId = 700
                                  ) THEN 2
                             ELSE CASE WHEN (
                                              ChildResourceId = 698
                                              OR ParentResourceId = 698
                                            ) THEN 3
                                       ELSE 4
                                  END
                        END AS GroupSortOrder
                       ,(
                          SELECT TOP 1
                                    HierarchyIndex
                          FROM      dbo.syNavigationNodes
                          WHERE     ResourceId = ChildResourceId
                        ) AS FirstSortOrder
                       ,CASE WHEN (
                                    ChildResourceId IN ( 698 )
                                    OR ParentResourceId IN ( 698 )
                                  ) THEN 1
                             ELSE 2
                        END AS DisplaySequence
                       ,ResourceTypeId
              FROM      (
                          SELECT DISTINCT
                                    NNChild.ResourceId AS ChildResourceId
                                   ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                         ELSE RChild.Resource
                                    END AS ChildResource
                                   ,RChild.ResourceURL AS ChildResourceURL
                                   ,CASE WHEN (
                                                NNParent.ResourceId = 191
                                                OR NNChild.ResourceId = 698
                                                OR NNChild.ResourceId = 699
                                                OR NNChild.ResourceId = 700
                                              ) THEN NULL
                                         ELSE NNParent.ResourceId
                                    END AS ParentResourceId
                                   ,RParent.Resource AS ParentResource
                                   ,RParentModule.Resource AS MODULE
                                   ,RChild.ResourceTypeID
				--NNParent.ParentId
                          FROM      syResources RChild
                          INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
                          INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                          INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                          LEFT OUTER JOIN (
                                            SELECT  *
                                            FROM    syResources
                                            WHERE   ResourceTypeId = 1
                                          ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                          WHERE     RChild.ResourceTypeId IN ( 2,3,8 )
                                    AND (
                                          RChild.ChildTypeId IS NULL
                                          OR RChild.ChildTypeId = 3
                                        )
                                    AND ( RChild.ResourceId <> 394 )
                                    AND ( NNParent.ResourceId NOT IN ( 394 ) )
                                    AND (
                                          NNParent.Parentid IN ( SELECT HierarchyId
                                                                 FROM   syNavigationNodes
                                                                 WHERE  ResourceId = 191 )
                                          OR NNChild.ParentId IN ( SELECT   HierarchyId
                                                                   FROM     syNavigationNodes
                                                                   WHERE    ResourceId IN ( 700,698,699 ) )
                                        )
                        ) t4
              UNION
--Placement
              SELECT    193 AS ModuleResourceId
                       ,ResourceId AS ChildResourceId
                       ,Resource AS ChildResource
                       ,(
                          SELECT    AccessLevel
                          FROM      syRlsResLvls
                          WHERE     RoleId = @RoleId
                                    AND ResourceId = syResources.ResourceId
                        ) AS AccessLevel
                       ,ResourceURL AS ChildResourceURL
                       ,NULL AS ParentResourceId
                       ,NULL AS ParentResource
                       ,1 AS GroupSortOrder
                       ,NULL AS FirstSortOrder
                       ,NULL AS DisplaySequence
                       ,ResourceTypeID
              FROM      syResources
              WHERE     ResourceId = 193
              UNION
              SELECT    193 AS ModuleResourceId
                       ,ChildResourceId
                       ,ChildResource
                       ,(
                          SELECT DISTINCT
                                    AccessLevel
                          FROM      syRlsResLvls
                          WHERE     RoleId = @RoleId
                                    AND ResourceId = ChildResourceId
                        ) AS AccessLevel
                       ,ChildResourceURL
                       ,ParentResourceId
                       ,ParentResource
                       ,CASE WHEN (
                                    ChildResourceId = 696
                                    OR ParentResourceId = 696
                                  ) THEN 2
                             ELSE 3
                        END AS GroupSortOrder
                       ,(
                          SELECT TOP 1
                                    HierarchyIndex
                          FROM      dbo.syNavigationNodes
                          WHERE     ResourceId = ChildResourceId
                        ) AS FirstSortOrder
                       ,CASE WHEN (
                                    ChildResourceId IN ( 696 )
                                    OR ParentResourceId IN ( 696 )
                                  ) THEN 1
                             ELSE 2
                        END AS DisplaySequence
                       ,ResourceTypeID
              FROM      (
                          SELECT DISTINCT
                                    NNChild.ResourceId AS ChildResourceId
                                   ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                         ELSE CASE WHEN NNChild.ResourceId = 397 THEN 'Employer Tabs'
                                                   ELSE RChild.Resource
                                              END
                                    END AS ChildResource
                                   ,RChild.ResourceURL AS ChildResourceURL
                                   ,CASE WHEN (
                                                NNParent.ResourceId = 193
                                                OR NNChild.ResourceId IN ( 696,404 )
                                              ) THEN NULL
                                         ELSE NNParent.ResourceId
                                    END AS ParentResourceId
                                   ,RParent.Resource AS ParentResource
                                   ,RParentModule.Resource AS MODULE
                                   ,RChild.ResourceTypeID
				--NNParent.ParentId
                          FROM      syResources RChild
                          INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
                          INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                          INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                          LEFT OUTER JOIN (
                                            SELECT  *
                                            FROM    syResources
                                            WHERE   ResourceTypeId = 1
                                          ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                          WHERE     RChild.ResourceTypeId IN ( 2,3,8 )
                                    AND (
                                          RChild.ChildTypeId IS NULL
                                          OR RChild.ChildTypeId = 3
                                        )
                                    AND ( RChild.ResourceId <> 394 )
                                    AND ( NNParent.ResourceId NOT IN ( 394,397 ) )
                                    AND (
                                          NNParent.Parentid IN ( SELECT HierarchyId
                                                                 FROM   syNavigationNodes
                                                                 WHERE  ResourceId = 193 )
                                          OR NNChild.ParentId IN ( SELECT   HierarchyId
                                                                   FROM     syNavigationNodes
                                                                   WHERE    ResourceId IN ( 404,696 ) )
                                        )
                        ) t5
            ) t2
    WHERE   t2.ModuleResourceId = @ModuleResourceId
    ORDER BY GroupSortOrder
           ,ParentResourceId;






GO
