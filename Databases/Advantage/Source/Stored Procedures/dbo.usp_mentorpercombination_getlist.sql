SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_mentorpercombination_getlist]
    @reqId UNIQUEIDENTIFIER
   ,
--@effectivedate datetime,
    @stuEnrollId UNIQUEIDENTIFIER
AS
    SELECT DISTINCT
            t3.GrdComponentTypeId
           ,COUNT(t1.istestmentorproctored) AS MentorPerCombination
    FROM    arPostScoreDictationSpeedTest t1
           ,arGrdComponentTypes t2
           ,arBridge_GradeComponentTypes_Courses t3
    WHERE   t1.GrdComponentTypeId = t2.GrdComponentTypeId
            AND t2.GrdComponentTypeId = t3.GrdComponentTypeId
            AND t3.reqId = @reqId
            AND t1.StuEnrollId = @stuEnrollId
            AND t1.istestmentorproctored = 'yes'
    GROUP BY t3.GrdComponentTypeId;



GO
