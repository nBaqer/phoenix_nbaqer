SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_RulesForCourses_Insert]
    @OldEffectiveDate DATETIME
   ,@EffectiveDate DATETIME
   ,@RuleValues NTEXT
AS
    DECLARE @hDoc INT;
        --Prepare input values as an XML document
    EXEC sp_xml_preparedocument @hDoc OUTPUT,@RuleValues;

        -- Delete all records from bridge table based on GrdComponentTypeId
    IF YEAR(@OldEffectiveDate) <> 1900
        BEGIN
            DELETE  FROM arRules_GradeComponentTypes_Courses
            WHERE   EffectiveDate = @OldEffectiveDate
                    AND GrdComponentTypeId_ReqId IN ( SELECT    GrdComponentTypeId_ReqId
                                                      FROM      OPENXML(@hDoc,'/NewDataSet/SetupTestRules',1) 
                            WITH (GrdComponentTypeId VARCHAR(50)) )
                    AND NumberofTests IN ( SELECT   NumberofTests
                                           FROM     OPENXML(@hDoc,'/NewDataSet/SetupTestRules',1) 
                            WITH (NumberofTests INT) )
                    AND MinimumScore IN ( SELECT    MinimumScore
                                          FROM      OPENXML(@hDoc,'/NewDataSet/SetupTestRules',1) 
                            WITH (MinimumScore DECIMAL) );
        END;
        

        -- Insert records into Bridge Table 
    INSERT  INTO arRules_GradeComponentTypes_Courses
            SELECT DISTINCT
                    GrdComponentTypeId_ReqId
                   ,@EffectiveDate
                   ,NumberofTests
                   ,MinimumScore
                   ,ModUser
                   ,GETDATE() --,convert(char(10),ModDate,101) 
            FROM    OPENXML(@hDoc,'/NewDataSet/SetupTestRules',1) 
                    WITH (GrdComponentTypeId_ReqId UNIQUEIDENTIFIER,EffectiveDate DATETIME,NumberofTests INT,MinimumScore DECIMAL,ModUser VARCHAR(50),ModDate DATETIME);
        
    EXEC sp_xml_removedocument @hDoc;



GO
