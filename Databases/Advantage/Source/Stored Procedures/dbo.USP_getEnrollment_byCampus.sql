SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--=================================================================================================
-- USP_getEnrollment_byCampus
--=================================================================================================
CREATE PROCEDURE [dbo].[USP_getEnrollment_byCampus]
    @Campus UNIQUEIDENTIFIER
   ,@ShowInactive BIT
AS --    
    BEGIN  
 
        DECLARE @OriginalSectionList TABLE
            (
             StatusCodeId UNIQUEIDENTIFIER
            ,StatusCodeDescrip VARCHAR(80)
            );        
        
        INSERT  INTO @OriginalSectionList
                SELECT DISTINCT 
				
				--sss.SysStatusId  
    --           ,sss.SysStatusDescrip  
    --           ,sss.StatusId sySysStatus_StatusId  
               --,
                        sc.StatusCodeId  
               --,sc.StatusCode  
                       ,sc.StatusCodeDescrip  
               --,sc.StatusId syStatusCodes_StatusId  
                FROM    sySysStatus sss
                INNER JOIN syStatusCodes sc ON sss.SysStatusId = sc.SysStatusId
                INNER JOIN dbo.syCmpGrpCmps CGC ON CGC.CampGrpId = sc.CampGrpId
                WHERE   CGC.CampusId = @Campus
                        AND StatusLevelId = 2
                        AND sc.SysStatusId NOT IN ( 7,8 )
                        AND (
                              @ShowInactive = 0
                              AND sss.InSchool = 1
                              AND sss.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
                              AND sc.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
                            )
                        OR @ShowInactive = 1
                ORDER BY StatusCodeDescrip;  

        SELECT DISTINCT
                StatusCodeDescrip
               ,StatusCodeIds AS StatusCodeId
        FROM    @OriginalSectionList osl
        CROSS APPLY (
                      SELECT    STUFF( (SELECT  ',' + CONVERT(VARCHAR(100),StatusCodeId)
                                        FROM    @OriginalSectionList sl
                                        WHERE   sl.StatusCodeDescrip = osl.StatusCodeDescrip
                                        ORDER BY StatusCodeDescrip
                                FOR   XML PATH('')
                                         ,TYPE).value('.','varchar(max)'),1,1,'')
                    ) D ( StatusCodeIds )
        ORDER BY StatusCodeDescrip;    


    END; 
--=================================================================================================
-- END  --  USP_getEnrollment_byCampus
--=================================================================================================
GO
