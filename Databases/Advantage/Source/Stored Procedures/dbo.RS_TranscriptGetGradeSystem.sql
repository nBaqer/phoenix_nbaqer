SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RS_TranscriptGetGradeSystem]
    @StudentId UNIQUEIDENTIFIER = 'CBA8BDE5-4C38-4A35-80FC-5E378C583194'
   ,@EnrollmentId UNIQUEIDENTIFIER = '00000000-0000-0000-0000-000000000000'  -- 3AEEFECB-53E8-4EF7-8BE6-03C4144D31A6
AS
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;
        DECLARE @firstTable TABLE
            (
             GrdSysDetailId UNIQUEIDENTIFIER
            ,Grade CHAR(5)
            ,GPA DECIMAL(4,2)
            ,Range VARCHAR(50)
            ,GradeDescription VARCHAR(50)
            ,quality VARCHAR(50)
            );
	
        INSERT  INTO @firstTable
                SELECT DISTINCT
                        gsd.GrdSysDetailId
                       ,gsd.Grade
                       ,gsd.GPA
                       ,(
                          SELECT TOP 1
                                    CAST(MinVal AS VARCHAR) + ' - ' + CAST(MaxVal AS VARCHAR)
                          FROM      arGradeScaleDetails gscd
                                   ,arGradeScales gsc
                          WHERE     gscd.GrdScaleId = gsc.GrdScaleId
                                    AND gscd.GrdSysDetailId = gsd.GrdSysDetailId
                                    AND gsc.GrdSystemId = gs.GrdSystemId
                        ) AS Range
                       ,gsd.GradeDescription
                       ,gsd.quality
                FROM    arStuEnrollments se
                       ,arPrgVersions pv
                       ,arGradeSystems gs
                       ,arGradeSystemDetails gsd
                WHERE   (
                          (
                            @EnrollmentId = '00000000-0000-0000-0000-000000000000'
                            AND se.StuEnrollId IN ( SELECT  aSE.StuEnrollId
                                                    FROM    arStuEnrollments aSE
                                                    WHERE   aSE.StudentId = @StudentId )
                          )
                          OR (
                               @EnrollmentId <> '00000000-0000-0000-0000-000000000000'
                               AND se.StuEnrollId = @EnrollmentId
                             )
                        )
                        AND se.PrgVerId = pv.PrgVerId
                        AND pv.GrdSystemId = gs.GrdSystemId
                        AND gs.GrdSystemId = gsd.GrdSystemId
                ORDER BY gsd.GPA DESC
                       ,gsd.Grade;
	

	
        SELECT  ( ROW_NUMBER() OVER ( ORDER BY f.GPA DESC, f.Grade ) ) AS RowIndex
               ,f.GrdSysDetailId
               ,f.Grade
               ,f.GPA
               ,f.Range
               ,f.GradeDescription
               ,f.quality
        FROM    arGradeSystemDetails d
        INNER JOIN @firstTable f ON d.GrdSysDetailId = f.GrdSysDetailId;
    END;



GO
