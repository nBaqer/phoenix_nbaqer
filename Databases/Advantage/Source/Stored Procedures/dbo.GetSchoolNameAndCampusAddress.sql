SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =========================================================================================================

-- Author:		Jose Alfredo Garcia guirado
-- Create date: 7/14/2012
-- Description:	Get the school name and the name 
-- and address of the  CampusId entered as parameter.
-- =============================================
CREATE PROCEDURE [dbo].[GetSchoolNameAndCampusAddress]
    -- Add the parameters for the stored procedure here
    @CampusId VARCHAR(38) = NULL
   ,@StuEnrollIdList VARCHAR(MAX) = NULL
   ,@StudentGrpId VARCHAR(MAX) = NULL
AS
    BEGIN

        -- SET NOCOUNT ON added to prevent extra result sets from
        -- interfering with SELECT statements.
        SET NOCOUNT ON;
        -- First Get the School Name from parameters
        DECLARE @SchoolName TABLE
            (
                Name VARCHAR(100)
            );

        DECLARE @CampusInfo TABLE
            (
                SchoolName VARCHAR(50)
               ,StuEnrollId UNIQUEIDENTIFIER
               ,CampusId UNIQUEIDENTIFIER
               ,CampusName VARCHAR(50)
               ,CampusCode VARCHAR(10)
               ,Address1 VARCHAR(50)
               ,Address2 VARCHAR(50)
               ,City VARCHAR(80)
               ,State VARCHAR(80)
               ,StateCode VARCHAR(12)
               ,ZIP VARCHAR(5)
               ,Country VARCHAR(50)
               ,Phone1 VARCHAR(30)
               ,Phone2 VARCHAR(30)
               ,Phone3 VARCHAR(30)
               ,Fax VARCHAR(30)
               ,Website VARCHAR(50)
            );

        INSERT INTO @SchoolName (
                                Name
                                )
        VALUES ( dbo.GetAppSettingValueByKeyName('CorporateName', NULL));

        -- If @CampusId is Null and @StuEnrollIdList is not Null we cang get CampusId from the first StuEnrollId in the list
        IF ( @CampusId IS NULL )
            BEGIN
                IF NOT ( @StuEnrollIdList IS NULL )
                    BEGIN
                        SELECT @CampusId = ASE.CampusId
                        FROM   arStuEnrollments AS ASE
                        WHERE  ASE.StuEnrollId = (
                                                 SELECT TOP 1 Val
                                                 FROM   MultipleValuesForReportParameters(@StuEnrollIdList, ',', 1)
                                                 );
                    END;
                ELSE IF NOT ( @StudentGrpId IS NULL )
                         BEGIN
                             SELECT     @CampusId = ASE.CampusId
                             FROM       arStuEnrollments AS ASE
                             INNER JOIN adLeadByLeadGroups LLG ON LLG.StuEnrollId = ASE.StuEnrollId
                             WHERE      LLG.LeadGrpId = (
                                                        SELECT TOP 1 Val
                                                        FROM   MultipleValuesForReportParameters(@StudentGrpId, ',', 1)
                                                        );
                         END;
            END;
        -- Second Get the campusId Name and Address and other grass
        DECLARE @CampusGuid UNIQUEIDENTIFIER = CAST(@CampusId AS UNIQUEIDENTIFIER);
        INSERT INTO @CampusInfo
                    SELECT          TOP 1    c.SchoolName AS SchoolName
                                            ,se.StuEnrollId
                                            ,c.CampusId
                                            ,c.CampDescrip AS CampusName
                                            ,c.CampCode AS CampusCode
                                            ,RTRIM(LTRIM(c.Address1))
                                            ,RTRIM(LTRIM(c.Address2))
                                            ,RTRIM(LTRIM(c.City))
                                            ,sta.StateDescrip AS State
                                            ,sta.StateCode AS StateCode
                                            ,CASE WHEN LEN(c.Zip) > 5
                                                       AND co.CountryDescrip = 'USA' THEN SUBSTRING(c.Zip, 1, 5)
                                                  ELSE c.Zip
                                             END AS ZIP
                                            ,co.CountryDescrip AS Country
                                            ,c.Phone1
                                            ,c.Phone2
                                            ,c.Phone3
                                            ,c.Fax
                                            ,c.Website
                    FROM            dbo.syCampuses c
                    INNER JOIN      dbo.adCountries co ON co.CountryId = c.CountryId
                    INNER JOIN      dbo.syStates sta ON sta.StateId = c.StateId
                    INNER JOIN      dbo.arStuEnrollments se ON se.CampusId = c.CampusId
                    LEFT OUTER JOIN adLeadByLeadGroups LLG ON LLG.StuEnrollId = se.StuEnrollId
                                                              AND (
                                                                  @StuEnrollIdList IS NULL
                                                                  OR se.StuEnrollId IN (
                                                                                       SELECT Val
                                                                                       FROM   MultipleValuesForReportParameters(@StuEnrollIdList, ',', 1)
                                                                                       )
                                                                  )
                                                              AND (
                                                                  @StudentGrpId IS NULL
                                                                  OR LLG.LeadGrpId IN (
                                                                                      SELECT Val
                                                                                      FROM   MultipleValuesForReportParameters(@StudentGrpId, ',', 1)
                                                                                      )
                                                                  )
                    WHERE           c.CampusId = @CampusGuid;

        SELECT SchoolName
              ,StuEnrollId
              ,CampusId
              ,CampusName
              ,CampusCode
              ,Address1
              ,Address2
              ,City
              ,State
              ,StateCode
              ,ZIP
              ,Country
              ,Phone1
              ,Phone2
              ,Phone3
              ,Fax
              ,Website
        FROM   @CampusInfo;

    END;

GO
