SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--=================================================================================================
-- USP_AR_RegSt_GetAvailableStudentsforAllProgsAndClsSection
--=================================================================================================
CREATE PROCEDURE [dbo].[USP_AR_RegSt_GetAvailableStudentsforAllProgsAndClsSection]
    (
     @ClsSectionID UNIQUEIDENTIFIER
    ,@CampusID UNIQUEIDENTIFIER 
	)
AS -----------------------------------------------------------------------------------------------------    
    BEGIN   
	 
--A Variabletable is defined to hold the results from the Storedprocedure USP_GetCoursesFortheCourseGroups    
        CREATE TABLE #VariableReqtable1
            (
             ReqId UNIQUEIDENTIFIER
            ,GrpID UNIQUEIDENTIFIER
            );     
	
        INSERT  INTO #VariableReqtable1
                EXEC USP_AR_RegStd_GetCoursesFortheCourseGroupsforAllProgs;     
				
				
-- DE9790 get list of courses and retake allowed flag
        CREATE TABLE #CourseList
            (
             CourseId UNIQUEIDENTIFIER NOT NULL
                                       PRIMARY KEY
            ,AllowRetake BIT
            );
        INSERT  INTO #CourseList
                (
                 CourseId
                ,AllowRetake
				)
                SELECT  Courses.ReqId
                       ,Courses.AllowCompletedCourseRetake
                FROM    arReqs Courses;

        DECLARE @defaultGradDate DATETIME;
        SET @defaultGradDate = GETDATE();
		
 /*    
In this query we have two tables in the main query ,and one table is formed by the union of 3 queries.    
The first part of the union gets the student and their reqIds(Courses) mapped to the program    
The second part gets the Students and their ReqIds for the Courses and their Course Groups mapped to the program    
The third part fetches the Students and their reqs for the ContinuingEd programs.    
The Objective of the main query is to find the list of students for the selected class sections    
The query checks for the conditions that    
1. Student is not graded in the course    
2. Student has not passed the course    
3. This Course is not a equivalent course which the student has already taken.    
4. Gets the courses starting after the student start datetime    
5. Gets only the status 1 in PostAcademics column and Future Start that start date is before begin the class   
6. gets the students whose shifts match with the courses or if the shift of the student is not mentioned then get all the courses that do not have a shift    
Finaly order by the graduation datetime of the student    
-- DE9790 take AllowRetake flag into consideration  
*/ 
	
        SELECT  R5.StuEnrollId
               ,R5.ShiftId
               ,LastName
               ,R5.StartDate
               ,FirstName
               ,StudentId
               ,R5.StatusCodeId
               ,ExpGradDate
               ,PrgVerId
               ,PrgVerDescrip
               ,R5.ReqId
               ,Descrip
               ,AllowCompletedCourseRetake
        FROM    (
                  SELECT DISTINCT
                            t1.StuEnrollId
                           ,t1.ShiftId
                           ,t5.LastName
                           ,t1.StartDate
                           ,t5.FirstName
                           ,t5.StudentId
                           ,t1.StatusCodeId
                           ,t1.ExpGradDate
                           ,t1.ContractedGradDate
                           ,t1.PrgVerId
                           ,t6.PrgVerDescrip
                           ,t2.ReqId
                           ,t3.Descrip
                           ,(
                              SELECT    AllowRetake
                              FROM      #CourseList
                              WHERE     CourseId = t2.ReqId
                            ) AS AllowCompletedCourseRetake
                  FROM      arStuEnrollments t1
                  INNER JOIN arStudent t5 ON t1.StudentId = t5.StudentId
                  INNER JOIN arPrgVersions t6 ON t1.PrgVerId = t6.PrgVerId
                  INNER JOIN arProgVerDef t4 ON t1.PrgVerId = t4.PrgVerId
                  INNER JOIN arReqs t3 ON t3.ReqId = t4.ReqId
                  INNER JOIN #VariableReqtable1 t2 ON t3.ReqId = t2.GrpID
                  WHERE     -- t6.ProgId = @ProgId and for all progms    
                            t1.CampusId = @CampusID
                            AND t6.IsContinuingEd = 0
                            AND t3.ReqTypeId = 2
                  UNION
                  SELECT DISTINCT
                            t1.StuEnrollId
                           ,t1.ShiftId
                           ,t5.LastName
                           ,t1.StartDate
                           ,t5.FirstName
                           ,t5.StudentId
                           ,t1.StatusCodeId
                           ,t1.ExpGradDate
                           ,t1.ContractedGradDate
                           ,t1.PrgVerId
                           ,t6.PrgVerDescrip
                           ,t3.ReqId
                           ,t3.Descrip
                           ,(
                              SELECT    AllowRetake
                              FROM      #CourseList
                              WHERE     CourseId = t3.ReqId
                            ) AS AllowCompletedCourseRetake
                  FROM      arStuEnrollments t1
                  INNER JOIN arStudent t5 ON t1.StudentId = t5.StudentId
                  INNER JOIN arPrgVersions t6 ON t1.PrgVerId = t6.PrgVerId
                  INNER JOIN arProgVerDef t4 ON t1.PrgVerId = t4.PrgVerId
                  INNER JOIN arReqs t3 ON t3.ReqId = t4.ReqId
                  WHERE     --t6.ProgId = @ProgId AND for all programs    
                            t1.CampusId = @CampusID
                            AND t6.IsContinuingEd = 0
                  UNION
                  SELECT DISTINCT
                            t1.StuEnrollId
                           ,t1.ShiftId
                           ,t5.LastName
                           ,t1.StartDate
                           ,t5.FirstName
                           ,t5.StudentId
                           ,t1.StatusCodeId
                           ,t1.ExpGradDate
                           ,t1.ContractedGradDate
                           ,t1.PrgVerId
                           ,t6.PrgVerDescrip
                           ,t7.ReqId
                           ,t8.Descrip AS Req
                           ,(
                              SELECT    AllowRetake
                              FROM      #CourseList
                              WHERE     CourseId = t7.ReqId
                            ) AS AllowCompletedCourseRetake
                  FROM      arStuEnrollments t1
                  INNER JOIN arStudent t5 ON t1.StudentId = t5.StudentId
                  INNER JOIN arPrgVersions t6 ON t1.PrgVerId = t6.PrgVerId
                           ,arClassSections t7
                  INNER JOIN arReqs t8 ON t7.ReqId = t8.ReqId
                  WHERE     t1.CampusId = @CampusID
                            AND t6.IsContinuingEd = 1
                            AND t7.ClsSectionId = @ClsSectionID    
		--t6.ProgId = @ProgId and for all progms    
                ) R5
        INNER JOIN arClassSections R6 ON R5.ReqId = R6.ReqId
        INNER JOIN syStatusCodes ON R5.StatusCodeId = syStatusCodes.StatusCodeId
        WHERE   R5.StartDate < R6.EndDate
				-- You can not take a class that begin before your enrollment start
                AND CAST(R5.StartDate AS DATE) <= CAST(R6.StartDate AS DATE)
				-- Considerer Future Start and the new student status........
                AND ( syStatusCodes.StatusCodeId IN ( SELECT DISTINCT
                                                                StatusCodeId
                                                      FROM      dbo.syStatusCodes
                                                      JOIN      dbo.sySysStatus ss ON ss.SysStatusId = syStatusCodes.SysStatusId
                                                      WHERE     ss.PostAcademics = 1
                                                                OR ss.SysStatusId = 7 -- future start
					  ) )
					
			  -- AND syStatusCodes.StatusCodeId IN ( 7,9,13,20 )
                AND (
                      ( R5.ShiftId = R6.shiftid )
                      OR (
                           R5.ShiftId IS NULL
                           AND R6.shiftid IS NULL
                         )
                    )
                AND R5.StuEnrollId NOT IN ( SELECT DISTINCT
                                                    StuEnrollId
                                            FROM    arTransferGrades t9
                                                   ,arGradeSystemDetails t10
                                            WHERE   t9.GrdSysDetailId = t10.GrdSysDetailId
                                                    AND t10.IsPass = 1
                                                    AND t9.ReqId IN ( SELECT    ReqId
                                                                      FROM      arClassSections
                                                                      WHERE     ClsSectionId = @ClsSectionID ) )
                AND R5.StuEnrollId NOT IN ( SELECT DISTINCT
                                                    ( t7.StuEnrollId )
                                            FROM    arResults t7
                                                   ,arClassSections t8
                                            WHERE   t8.ReqId IN ( SELECT    ReqId
                                                                  FROM      arClassSections
                                                                  WHERE     ClsSectionId = @ClsSectionID )
                                                    AND t7.TestId = t8.ClsSectionId
                                                    AND t7.GrdSysDetailId IS NULL )
                AND R6.ClsSectionId NOT IN ( SELECT ClsSectionId
                                             FROM   arClassSections
                                             WHERE  ReqId IN ( SELECT   c.EquivReqId
                                                               FROM     arResults a
                                                                       ,arClassSections b
                                                                       ,arCourseEquivalent c
                                                                       ,arGradeSystemDetails d
                                                               WHERE    a.TestId = b.ClsSectionId
                                                                        AND a.StuEnrollId = R5.StuEnrollId
                                                                        AND a.GrdSysDetailId = d.GrdSysDetailId
                                                                        AND d.IsPass = 1
                                                                        AND b.ReqId = c.ReqId
                                                                        AND c.EquivReqId = R5.ReqId ) )
                AND R6.ClsSectionId = @ClsSectionID
                AND R5.StuEnrollId NOT IN ( SELECT  SGS.StuEnrollId
                                            FROM    adStuGrpStudents SGS
                                                   ,adStudentGroups SG
                                            WHERE   SGS.IsDeleted = 0
                                                    AND SGS.StuGrpId = SG.StuGrpId
                                                    AND SG.IsRegHold = 1
                                                    AND SGS.StuEnrollId IS NOT NULL )
        ORDER BY R5.ExpGradDate;     
	
    END;
--=================================================================================================
-- END  --  USP_AR_RegSt_GetAvailableStudentsforAllProgsAndClsSection
--=================================================================================================
GO
