SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_ConsecutiveDaysAbsent_MainReport]
    @TermId VARCHAR(100)
   ,@ReqId VARCHAR(4000)
   ,@ConsDays INT
   ,@CutoffDate DATETIME
AS
    BEGIN
        DECLARE @ClsSectionId VARCHAR(100);

        SELECT  TermId
               ,TermDescrip
               ,ReqId
               ,Course
               ,ClsSectionId
               ,ClsSection
               ,COUNT(*)
        FROM    (
                  SELECT    st.StudentNumber
                           ,st.FirstName + ' ' + ISNULL(st.MiddleName,'') + ' ' + st.LastName AS Student
                           ,se.StartDate
                           ,se.LDA
                           ,sc.StatusCodeDescrip AS status
                           ,tm.termid
                           ,tm.termdescrip
                           ,tm.startdate AS [Term Start Date]
                           ,rq.reqid
                           ,rq.descrip AS Course
                           ,cs.ClsSectionId
                           ,cs.ClsSection
                           ,(
                              SELECT    dbo.ConsecutiveDaysAbsent(se.StuEnrollId,cs.ClsSectionId,@CutoffDate)
                            ) AS 'Days Missed'
                  FROM      arstudent st
                  INNER JOIN dbo.arStuEnrollments se ON st.StudentId = se.StudentId
                  INNER JOIN arprgversions pv ON se.PrgVerId = pv.PrgVerId
                  INNER JOIN dbo.arResults rs ON se.StuEnrollId = rs.StuEnrollId
                  INNER JOIN dbo.arClassSections cs ON rs.TestId = cs.ClsSectionId
                  INNER JOIN dbo.arTerm tm ON cs.TermId = tm.TermId
                  INNER JOIN arreqs rq ON cs.ReqId = rq.ReqId
                  INNER JOIN dbo.syStatusCodes sc ON se.StatusCodeId = sc.StatusCodeId
                  WHERE     tm.TermId = @TermId
                            AND rq.ReqId IN ( SELECT    Val
                                              FROM      MultipleValuesForReportParameters(@ReqId,',',1) )
                ) R
        GROUP BY TermId
               ,TermDescrip
               ,ReqId
               ,Course
               ,ClsSectionId
               ,ClsSection
        ORDER BY R.Course;
    END;



GO
