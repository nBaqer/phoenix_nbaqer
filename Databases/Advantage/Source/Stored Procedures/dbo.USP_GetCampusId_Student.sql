SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_GetCampusId_Student]
    @EntityId UNIQUEIDENTIFIER
AS
    SELECT TOP 1
            CampusId
    FROM    arStuEnrollments
    WHERE   StudentId = @EntityId
    ORDER BY StartDate; 



GO
