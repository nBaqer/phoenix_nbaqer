SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_GetSchedLeadGrpCnt]
    (
     @LeadGrpId VARCHAR(8000)
    )
AS
    SET NOCOUNT ON;
    SELECT  COUNT(*) AS SchedCnt
    FROM    adLeadGroups
    WHERE   LeadGrpId IN ( SELECT   strval
                           FROM     dbo.SPLIT(@LeadGrpId) )
            AND UseForScheduling = 1;



GO
