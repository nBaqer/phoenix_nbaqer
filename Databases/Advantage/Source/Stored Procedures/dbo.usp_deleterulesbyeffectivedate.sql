SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_deleterulesbyeffectivedate]
    @ReqId UNIQUEIDENTIFIER
   ,@EffectiveDate DATETIME
AS
    SET NOCOUNT ON;
    DELETE  FROM arRules_GradeComponentTypes_Courses
    WHERE   GrdComponentTypeId_ReqId IN ( SELECT    GrdComponentTypeId_ReqId
                                          FROM      arBridge_GradeComponentTypes_Courses
                                          WHERE     ReqId = @ReqId )
            AND EffectiveDate = @EffectiveDate;



GO
