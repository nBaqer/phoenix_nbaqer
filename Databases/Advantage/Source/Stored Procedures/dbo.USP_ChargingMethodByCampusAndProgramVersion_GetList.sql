SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_ChargingMethodByCampusAndProgramVersion_GetList]
    @campusid UNIQUEIDENTIFIER
   ,@PrgVerId UNIQUEIDENTIFIER
   ,@StatusId UNIQUEIDENTIFIER
AS
    SELECT DISTINCT
            t5.BillingMethodId AS BillingMethodId
           ,t5.BillingMethodDescrip + ' (' + t5.BillingMethodCode + ')' AS Descrip
    FROM    syCampGrps AS t1
    INNER JOIN syCmpGrpCmps AS t2 ON t1.CampGrpId = t2.CampGrpId
    INNER JOIN syCampuses AS t3 ON t2.CampusId = t3.CampusId
    INNER JOIN saBillingMethods AS t5 ON t1.CampGrpId = t5.CampGrpId
    INNER JOIN syStatuses AS t4 ON t5.StatusId = t4.StatusId
    INNER JOIN arPrgVersions AS t6 ON t6.BillingMethodId = t5.BillingMethodId
    WHERE   t3.CampusId = @campusid
            AND t6.PrgVerId = @PrgVerId
    ORDER BY Descrip;



GO
