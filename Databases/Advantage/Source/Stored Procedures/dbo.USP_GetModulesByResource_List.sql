SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--EXEC USP_GetModulesByResource_List 20
CREATE PROCEDURE [dbo].[USP_GetModulesByResource_List]
    @MaintenanePageId INT
AS
    SELECT DISTINCT
            t1.resourceId
           ,t4.ResourceId AS ModuleId
    FROM    syNavigationNodes t1
    INNER JOIN syNavigationNodes t2 ON t1.ParentId = t2.HierarchyId
    INNER JOIN syNavigationNodes t3 ON t2.ParentId = t3.HierarchyId
    INNER JOIN syNavigationNodes t4 ON t3.ParentId = t4.HierarchyId
    WHERE   t1.ResourceId = @MaintenanePageId;



GO
