SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[USP_AR_GetMinPassingScore]
AS /*----------------------------------------------------------------------------------------------------
	Author          :	Saraswathi Lakshmanan
    
    Create date		:	02/03/2010
    
	Procedure Name	:	USP_AR_GetMinPassingScore

	Objective		:	find minimum passing score 
	
	Parameters		:	Name			Type	Data Type			Required? 	
						=====			====	=========			=========	
						
	
	Output			:	Returns the minimum score				
						
*/-----------------------------------------------------------------------------------------------------

    BEGIN
	
        SELECT  MIN(GSD.MinVal)
        FROM    arGradeScaleDetails GSD
        INNER JOIN arGradeSystemDetails GSY ON GSD.GrdSysDetailId = GSY.GrdSysDetailId
        WHERE   GSY.IsPass = 1; 
    END;



GO
