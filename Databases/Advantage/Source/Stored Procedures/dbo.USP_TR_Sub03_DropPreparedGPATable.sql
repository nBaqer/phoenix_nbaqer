SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =========================================================================================================
-- USP_TR_Sub03_DropPreparedGPATable
-- =========================================================================================================
CREATE PROCEDURE [dbo].[USP_TR_Sub03_DropPreparedGPATable]
(  
    @TableName NVARCHAR(200)
)
AS

	BEGIN
		DECLARE @SQL01 AS NVARCHAR(MAX);
		SET @SQL01 = '';
		SET @SQL01 = @SQL01 + 'DROP TABLE tempdb.dbo.'+  @TableName + ' ' + CHAR(10);
		--PRINT @SQL01;
		EXECUTE (@SQL01);
	END
-- =========================================================================================================
-- END  --  USP_TR_Sub03_DropPreparedGPATable
-- =========================================================================================================
GO
