SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_GetStudentSummaryDetailsHrsMinutesNumeric]
    (
     @stuEnrollId UNIQUEIDENTIFIER
    ,@cutOffDate DATETIME
    )
AS
    SET NOCOUNT ON;

    SELECT DISTINCT
            SchedHours
           ,ActualHours
           ,RecordDate
           ,DATEPART(dw,RecordDate) - 1 AS dw
           ,IsTardy AS Tardy
    FROM    arStudentClockAttendance
    WHERE   StuEnrollId = @stuEnrollId
            AND SchedHours IS NOT NULL
            AND SchedHours <> 0
            AND RecordDate <= @cutOffDate
            AND (
                  ActualHours IS NOT NULL
                  AND ActualHours <> 999.00
                  AND ActualHours <> 9999.00
                )
            AND ( IsTardy = 1 )
    UNION
    SELECT DISTINCT
            Schedule
           ,Actual
           ,MeetDate AS RecordDate
           ,DATEPART(dw,MeetDate) - 1 AS dw
           ,Tardy
    FROM    atConversionAttendance
    WHERE   StuEnrollId = @stuEnrollId
            AND Tardy = 1
            AND Schedule IS NOT NULL
            AND Schedule <> 0
            AND meetDate <= @cutOffDate
            AND (
                  Actual IS NOT NULL
                  AND Actual <> 999.00
                  AND Actual <> 9999.00
                )
    ORDER BY RecordDate;




GO
