SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[USP_AR_InstructionType_GetList] ( @Status NVARCHAR(20) )
AS /*----------------------------------------------------------------------------------------------------
	Author          :	Janet Robinson
    
    Create date		:	08/09/2011
    
	Procedure Name	:	[USP_AR_InstructionType_GetList]

	Objective		:	Get the list of Instruction Types from the arInstructionType table
	
	Parameters		:	Name			Type	Data Type			Required? 	
						=====			====	=========			=========	
						@Status 		In		nvarchar			Required
						
	
	Output			:	Returns the list of Instruction Types for all campgrps
						
*/-----------------------------------------------------------------------------------------------------

    BEGIN

        DECLARE @ShowActiveValue NVARCHAR(20);

        IF LOWER(@Status) = 'all'
            BEGIN
                SELECT  DISTINCT
                        CT.InstructionTypeId
                       ,CT.InstructionTypeDescrip
                FROM    arInstructionType CT
                ORDER BY CT.InstructionTypeDescrip;
		
            END;
        ELSE
            BEGIN
                IF LOWER(@Status) = 'true'
                    BEGIN
                        SET @ShowActiveValue = 'Active';
                    END;
                ELSE
                    BEGIN
                        SET @ShowActiveValue = 'Inactive';
                    END;

                SELECT  DISTINCT
                        CT.InstructionTypeId
                       ,CT.InstructionTypeDescrip
                FROM    arInstructionType CT --INNER JOIN syCampGrps CP ON CP.CampGrpId = CT.CampGrpId
                INNER JOIN syStatuses ST ON CT.StatusId = ST.StatusId
                WHERE   ST.Status = @ShowActiveValue
			  --AND CT.CampGrpId = @CampGrpId
			  	
			--UNION
			
			--SELECT  DISTINCT  
			--	CT.InstructionTypeId,		
			--	CT.InstructionTypeDescrip
				
			--FROM   
			--	arInstructionType CT
			--	INNER JOIN syCampGrps CP ON CP.CampGrpId = CT.CampGrpId
			--	INNER JOIN syStatuses ST ON CT.StatusId = ST.StatusId 
			--WHERE ST.Status = @ShowActiveValue
			--AND CP.CampGrpDescrip = 'All'
                ORDER BY CT.InstructionTypeDescrip;
				
            END;

    END;



GO
