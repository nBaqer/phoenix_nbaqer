SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[Usp_Advantage_MaintenanceNavigation_ByModule_GetList]
    @SchoolEnumerator INT
   ,@CampusId UNIQUEIDENTIFIER
   ,@UserId UNIQUEIDENTIFIER
AS
    BEGIN      
 -- declare local variables      
        DECLARE @ShowRossOnlyTabs BIT
           ,@SchedulingMethod VARCHAR(50)
           ,@TrackSAPAttendance VARCHAR(50);      
        DECLARE @ShowCollegeOfCourtReporting VARCHAR(5)
           ,@FameESP VARCHAR(5)
           ,@EdExpress VARCHAR(5);       
        DECLARE @GradeBookWeightingLevel VARCHAR(20)
           ,@ShowExternshipTabs VARCHAR(5)
           ,@TimeClockClassSection VARCHAR(5)
           ,@IsPowerUser BIT
           ,@SettingId INT;    
		  
        DECLARE @IsUserAPowerUser BIT;  
		  
        SET @IsUserAPowerUser = 0;  
        IF (
             SELECT COUNT(*)
             FROM   syUsers
             WHERE  (
                      LOWER(UserName) = 'support'
                      OR LOWER(UserName) = 'sa'
                      OR LOWER(UserName) = 'super'
                    )
                    AND UserId = @UserId
           ) >= 1
            BEGIN    
                SET @IsUserAPowerUser = 1; -- set PowerUser to True    
            END;     
	 
-- By Default set PowerUser to False    
        SET @IsPowerUser = 0;    
	  
  -- If Logged in User is support    
        IF (
             SELECT COUNT(*)
             FROM   syUsers
             WHERE  ( LOWER(UserName) = 'support' )
                    AND UserId = @UserId
           ) >= 1
            BEGIN    
                SET @IsPowerUser = 1; -- set PowerUser to True    
            END;     
	  
        DECLARE @IsSupportORSuper BIT;  
		  
        SET @IsSupportORSuper = 0;  
        IF (
             SELECT COUNT(*)
             FROM   syUsers
             WHERE  (
                      LOWER(UserName) = 'support'
                      OR LOWER(UserName) = 'super'
                    )
                    AND UserId = @UserId
           ) >= 1
            BEGIN    
                SET @IsSupportORSuper = 1; -- set PowerUser to True    
            END;        
	   
 -- Get Values      
        IF (
             SELECT COUNT(*)
             FROM   syConfigAppSetValues
             WHERE  SettingId = 68
                    AND CampusId = @CampusId
           ) >= 1
            BEGIN    
                SET @ShowRossOnlyTabs = (
                                          SELECT    VALUE
                                          FROM      dbo.syConfigAppSetValues
                                          WHERE     SettingId = 68
                                                    AND CampusId = @CampusId
                                        );      
			
            END;     
        ELSE
            BEGIN    
                SET @ShowRossOnlyTabs = (
                                          SELECT    VALUE
                                          FROM      dbo.syConfigAppSetValues
                                          WHERE     SettingId = 68
                                                    AND CampusId IS NULL
                                        );    
            END;    
	 
	 
		
        IF (
             SELECT COUNT(*)
             FROM   syConfigAppSetValues
             WHERE  SettingId = 65
                    AND CampusId = @CampusId
           ) >= 1
            BEGIN    
                SET @SchedulingMethod = (
                                          SELECT    VALUE
                                          FROM      dbo.syConfigAppSetValues
                                          WHERE     SettingId = 65
                                                    AND CampusId = @CampusId
                                        );      
			
            END;     
        ELSE
            BEGIN    
                SET @SchedulingMethod = (
                                          SELECT    VALUE
                                          FROM      dbo.syConfigAppSetValues
                                          WHERE     SettingId = 65
                                                    AND CampusId IS NULL
                                        );    
            END;                          
									
		
									  
        IF (
             SELECT COUNT(*)
             FROM   syConfigAppSetValues
             WHERE  SettingId = 72
                    AND CampusId = @CampusId
           ) >= 1
            BEGIN    
                SET @TrackSAPAttendance = (
                                            SELECT  VALUE
                                            FROM    dbo.syConfigAppSetValues
                                            WHERE   SettingId = 72
                                                    AND CampusId = @CampusId
                                          );      
			
            END;     
        ELSE
            BEGIN    
                SET @TrackSAPAttendance = (
                                            SELECT  VALUE
                                            FROM    dbo.syConfigAppSetValues
                                            WHERE   SettingId = 72
                                                    AND CampusId IS NULL
                                          );    
            END;     
		   
											   
        IF (
             SELECT COUNT(*)
             FROM   syConfigAppSetValues
             WHERE  SettingId = 118
                    AND CampusId = @CampusId
           ) >= 1
            BEGIN    
                SET @ShowCollegeOfCourtReporting = (
                                                     SELECT VALUE
                                                     FROM   dbo.syConfigAppSetValues
                                                     WHERE  SettingId = 118
                                                            AND CampusId = @CampusId
                                                   );      
			
            END;     
        ELSE
            BEGIN    
                SET @ShowCollegeOfCourtReporting = (
                                                     SELECT VALUE
                                                     FROM   dbo.syConfigAppSetValues
                                                     WHERE  SettingId = 118
                                                            AND CampusId IS NULL
                                                   );    
            END;     
		 
						   
        IF (
             SELECT COUNT(*)
             FROM   syConfigAppSetValues
             WHERE  SettingId = 37
                    AND CampusId = @CampusId
           ) >= 1
            BEGIN    
                SET @FameESP = (
                                 SELECT VALUE
                                 FROM   dbo.syConfigAppSetValues
                                 WHERE  SettingId = 37
                                        AND CampusId = @CampusId
                               );      
			
            END;     
        ELSE
            BEGIN    
                SET @FameESP = (
                                 SELECT VALUE
                                 FROM   dbo.syConfigAppSetValues
                                 WHERE  SettingId = 37
                                        AND CampusId IS NULL
                               );    
            END;     
		 
							 
        IF (
             SELECT COUNT(*)
             FROM   syConfigAppSetValues
             WHERE  SettingId = 91
                    AND CampusId = @CampusId
           ) >= 1
            BEGIN    
                SET @EdExpress = (
                                   SELECT   VALUE
                                   FROM     dbo.syConfigAppSetValues
                                   WHERE    SettingId = 91
                                            AND CampusId = @CampusId
                                 );      
			
            END;     
        ELSE
            BEGIN    
                SET @EdExpress = (
                                   SELECT   VALUE
                                   FROM     dbo.syConfigAppSetValues
                                   WHERE    SettingId = 91
                                            AND CampusId IS NULL
                                 );    
            END;    
			
									   
										   
        IF (
             SELECT COUNT(*)
             FROM   syConfigAppSetValues
             WHERE  SettingId = 43
                    AND CampusId = @CampusId
           ) >= 1
            BEGIN    
                SET @GradeBookWeightingLevel = (
                                                 SELECT VALUE
                                                 FROM   dbo.syConfigAppSetValues
                                                 WHERE  SettingId = 43
                                                        AND CampusId = @CampusId
                                               );      
	 
            END;     
        ELSE
            BEGIN    
                SET @GradeBookWeightingLevel = (
                                                 SELECT VALUE
                                                 FROM   dbo.syConfigAppSetValues
                                                 WHERE  SettingId = 43
                                                        AND CampusId IS NULL
                                               );    
            END;    
		   
									  
									  
        IF (
             SELECT COUNT(*)
             FROM   syConfigAppSetValues
             WHERE  SettingId = 71
                    AND CampusId = @CampusId
           ) >= 1
            BEGIN    
                SET @ShowExternshipTabs = (
                                            SELECT  VALUE
                                            FROM    dbo.syConfigAppSetValues
                                            WHERE   SettingId = 71
                                                    AND CampusId = @CampusId
                                          );      
			
            END;     
        ELSE
            BEGIN    
                SET @ShowExternshipTabs = (
                                            SELECT  VALUE
                                            FROM    dbo.syConfigAppSetValues
                                            WHERE   SettingId = 71
                                                    AND CampusId IS NULL
                                          );    
            END;    
				
				
        SET @SettingId = (
                           SELECT   SettingId
                           FROM     syConfigAppSettings
                           WHERE    KeyName LIKE '%TimeClockClassSection%'
                         );       
        IF (
             SELECT COUNT(*)
             FROM   syConfigAppSetValues
             WHERE  SettingId = @SettingId
                    AND CampusId = @CampusId
           ) >= 1
            BEGIN    
                SET @TimeClockClassSection = (
                                               SELECT   LOWER(VALUE)
                                               FROM     dbo.syConfigAppSetValues
                                               WHERE    SettingId = @SettingId
                                                        AND CampusId = @CampusId
                                             );      
			
            END;     
        ELSE
            BEGIN    
                SET @TimeClockClassSection = (
                                               SELECT   LOWER(VALUE)
                                               FROM     dbo.syConfigAppSetValues
                                               WHERE    SettingId = @SettingId
                                                        AND CampusId IS NULL
                                             );    
            END;    
	   
	  
 -- The first column always refers to the Module Resource Id (189 or 26 or .....)      
	   
        SELECT  *
        FROM    (
                  SELECT DISTINCT
                            189 AS ModuleResourceId
                           ,NNChild.ResourceId AS ChildResourceId
                           ,CASE WHEN NNChild.ResourceId = 395 THEN 'Lead Tabs'
                                 ELSE CASE WHEN NNChild.ResourceId = 398 THEN 'Add/View Leads'
                                           ELSE RChild.Resource
                                      END
                            END AS ChildResource
                           ,RChild.ResourceURL AS ChildResourceURL
                           ,CASE WHEN NNParent.ResourceId IN ( 687,688,689 ) THEN NULL
                                 ELSE NNParent.ResourceId
                            END AS ParentResourceId
                           ,RParent.Resource AS ParentResource
                           ,CASE WHEN NNChild.ResourceId = 398 THEN 1
                                 ELSE CASE WHEN NNChild.ResourceId = 395 THEN 2
                                           ELSE 3
                                      END
                            END AS FirstSortOrder
                           ,CASE WHEN (
                                        NNChild.ResourceId IN ( 400,702 )
                                        OR NNParent.ResourceId IN ( 400,702 )
                                      ) THEN 1
                                 ELSE 2
                            END AS DisplaySequence
                  FROM      syResources RChild
                  INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
                  INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                  INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                  LEFT OUTER JOIN (
                                    SELECT  *
                                    FROM    syResources
                                    WHERE   ResourceTypeId = 1
                                  ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                  WHERE     RChild.ResourceTypeId IN ( 2,4,8 )
                            AND (
                                  RChild.ChildTypeId IS NULL
                                  OR RChild.ChildTypeId = 4
                                )
                            AND ( RChild.ResourceId NOT IN ( 395 ) )
                            AND ( NNParent.ResourceId NOT IN ( 395 ) )
                            AND (
                                  NNParent.ParentId IN ( SELECT HierarchyId
                                                         FROM   syNavigationNodes
                                                         WHERE  ResourceId = 189 )
                                  OR NNChild.ParentId IN ( SELECT   HierarchyId
                                                           FROM     syNavigationNodes
                                                           WHERE    ResourceId IN ( 702,400,469,401,399,694 ) )
                                )
                  UNION ALL
                  SELECT    26 AS ModuleResourceId
                           ,ChildResourceId
                           ,ChildResource
                           ,ChildResourceURL
                           ,ParentResourceId
                           ,ParentResource
                           ,(
                              SELECT TOP 1
                                        HierarchyIndex
                              FROM      dbo.syNavigationNodes
                              WHERE     ResourceId = ChildResourceId
                            ) AS FirstSortOrder
                           ,CASE WHEN (
                                        ChildResourceId IN ( 703,468 )
                                        OR ParentResourceId IN ( 703,468 )
                                      ) THEN 1
                                 ELSE 2
                            END AS DisplaySequence
                  FROM      (
                              SELECT DISTINCT
                                        NNChild.ResourceId AS ChildResourceId
                                       ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                             ELSE RChild.Resource
                                        END AS ChildResource
                                       ,RChild.ResourceURL AS ChildResourceURL
                                       ,CASE WHEN (
                                                    NNParent.ResourceId = 687
                                                    OR NNChild.ResourceId IN ( 468,469,621,471,470,703 )
                                                  ) THEN NULL
                                             ELSE NNParent.ResourceId
                                        END AS ParentResourceId
                                       ,RParent.Resource AS ParentResource
                                       ,RParentModule.Resource AS MODULE      
	--NNParent.ParentId      
                              FROM      syResources RChild
                              INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
                              INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                              INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                              LEFT OUTER JOIN (
                                                SELECT  *
                                                FROM    syResources
                                                WHERE   ResourceTypeId = 1
                                              ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                              WHERE     RChild.ResourceTypeId IN ( 2,4,8 )
                                        AND (
                                              RChild.ChildTypeId IS NULL
                                              OR RChild.ChildTypeId = 4
                                            )
                                        AND ( RChild.ResourceId <> 394 )
                                        AND ( NNParent.ResourceId NOT IN ( 394 ) )
                                        AND (
                                              NNParent.ParentId IN ( SELECT HierarchyId
                                                                     FROM   syNavigationNodes
                                                                     WHERE  ResourceId = 26 )
                                              OR NNChild.ParentId IN ( SELECT   HierarchyId
                                                                       FROM     syNavigationNodes
                                                                       WHERE    ResourceId IN ( 703,468,469,621,471,470 ) )
                                            )
                                        AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
                            ) t1
                  UNION ALL
                  SELECT    194 AS ModuleResourceId
                           ,ChildResourceId
                           ,ChildResource
                           ,ChildResourceURL
                           ,ParentResourceId
                           ,ParentResource
                           ,(
                              SELECT TOP 1
                                        HierarchyIndex
                              FROM      dbo.syNavigationNodes
                              WHERE     ResourceId = ChildResourceId
                            ) AS FirstSortOrder
                           ,CASE WHEN (
                                        ChildResourceId IN ( 705 )
                                        OR ParentResourceId IN ( 705 )
                                      ) THEN 1
                                 ELSE 2
                            END AS DisplaySequence
                  FROM      (
                              SELECT DISTINCT
                                        NNChild.ResourceId AS ChildResourceId
                                       ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                             ELSE RChild.Resource
                                        END AS ChildResource
                                       ,RChild.ResourceURL AS ChildResourceURL
                                       ,CASE WHEN NNParent.ResourceId IN ( 194 )
                                                  OR NNChild.ResourceId IN ( 705 ) THEN NULL
                                             ELSE NNParent.ResourceId
                                        END AS ParentResourceId
                                       ,RParent.Resource AS ParentResource
                                       ,RParentModule.Resource AS MODULE      
	--NNParent.ParentId      
                              FROM      syResources RChild
                              INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
                              INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                              INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                              LEFT OUTER JOIN (
                                                SELECT  *
                                                FROM    syResources
                                                WHERE   ResourceTypeId = 1
                                              ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                              WHERE     RChild.ResourceTypeId IN ( 2,4,8 )
                                        AND (
                                              RChild.ChildTypeId IS NULL
                                              OR RChild.ChildTypeId = 4
                                            )
                                        AND (
                                              NNParent.ParentId IN ( SELECT HierarchyId
                                                                     FROM   syNavigationNodes
                                                                     WHERE  ResourceId = 194 )
                                              OR NNChild.ParentId IN ( SELECT   HierarchyId
                                                                       FROM     syNavigationNodes
                                                                       WHERE    ResourceId IN ( 705 ) )
                                            )
                                        AND ( RChild.ResourceId <> 394 )
                                        AND ( NNParent.ResourceId NOT IN ( 394,277 ) )      
	-- The following condition uses Bitwise Operator      
                                        AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
                            ) t2
                  UNION ALL
                  SELECT    300 AS ModuleResourceId
                           ,ChildResourceId
                           ,ChildResource
                           ,ChildResourceURL
                           ,ParentResourceId
                           ,ParentResource
                           ,(
                              SELECT TOP 1
                                        HierarchyIndex
                              FROM      dbo.syNavigationNodes
                              WHERE     ResourceId = ChildResourceId
                            ) AS FirstSortOrder
                           ,1 AS DisplaySequence
                  FROM      (
                              SELECT DISTINCT
                                        NNChild.ResourceId AS ChildResourceId
                                       ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                             ELSE RChild.Resource
                                        END AS ChildResource
                                       ,RChild.ResourceURL AS ChildResourceURL
                                       ,CASE WHEN NNParent.ResourceId = 300
                                                  OR NNChild.ResourceId = 697 THEN NULL
                                             ELSE NNParent.ResourceId
                                        END AS ParentResourceId
                                       ,RParent.Resource AS ParentResource
                                       ,RParentModule.Resource AS MODULE      
	--NNParent.ParentId      
                              FROM      syResources RChild
                              INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
                              INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                              INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                              LEFT OUTER JOIN (
                                                SELECT  *
                                                FROM    syResources
                                                WHERE   ResourceTypeId = 1
                                              ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                              WHERE     RChild.ResourceTypeId IN ( 2,4,8 )
                                        AND (
                                              RChild.ChildTypeId IS NULL
                                              OR RChild.ChildTypeId = 4
                                            )
                                        AND ( RChild.ResourceId <> 394 )
                                        AND ( NNParent.ResourceId NOT IN ( 394,687 ) )
                                        AND ( NNParent.ParentId IN ( SELECT HierarchyId
                                                                     FROM   syNavigationNodes
                                                                     WHERE  ResourceId = 300 ) )      
	-- The following condition uses Bitwise Operator      
                                        AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
                            ) t3
                  UNION ALL
                  SELECT    191 AS ModuleResourceId
                           ,ChildResourceId
                           ,ChildResource
                           ,ChildResourceURL
                           ,ParentResourceId
                           ,ParentResource
                           ,(
                              SELECT TOP 1
                                        HierarchyIndex
                              FROM      dbo.syNavigationNodes
                              WHERE     ResourceId = ChildResourceId
                            ) AS FirstSortOrder
                           ,CASE WHEN (
                                        ChildResourceId IN ( 706 )
                                        OR ParentResourceId IN ( 706 )
                                      ) THEN 1
                                 ELSE 2
                            END AS DisplaySequence
                  FROM      (
                              SELECT DISTINCT
                                        NNChild.ResourceId AS ChildResourceId
                                       ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                             ELSE RChild.Resource
                                        END AS ChildResource
                                       ,RChild.ResourceURL AS ChildResourceURL
                                       ,CASE WHEN (
                                                    NNParent.ResourceId = 191
                                                    OR NNChild.ResourceId = 706
                                                  ) THEN NULL
                                             ELSE NNParent.ResourceId
                                        END AS ParentResourceId
                                       ,RParent.Resource AS ParentResource
                                       ,RParentModule.Resource AS MODULE      
	--NNParent.ParentId      
                              FROM      syResources RChild
                              INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
                              INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                              INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                              LEFT OUTER JOIN (
                                                SELECT  *
                                                FROM    syResources
                                                WHERE   ResourceTypeId = 1
                                              ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                              WHERE     RChild.ResourceTypeId IN ( 2,4,8 )
                                        AND (
                                              RChild.ChildTypeId IS NULL
                                              OR RChild.ChildTypeId = 4
                                            )
                                        AND ( RChild.ResourceId <> 394 )
                                        AND ( NNParent.ResourceId NOT IN ( 394 ) )
                                        AND (
                                              NNParent.ParentId IN ( SELECT HierarchyId
                                                                     FROM   syNavigationNodes
                                                                     WHERE  ResourceId = 191 )
                                              OR NNChild.ParentId IN ( SELECT   HierarchyId
                                                                       FROM     syNavigationNodes
                                                                       WHERE    ResourceId IN ( 706 ) )
                                            )      
	-- The following condition uses Bitwise Operator      
                                        AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
                            ) t4
                  UNION ALL
                  SELECT    193 AS ModuleResourceId
                           ,ChildResourceId
                           ,ChildResource
                           ,ChildResourceURL
                           ,ParentResourceId
                           ,ParentResource
                           ,(
                              SELECT TOP 1
                                        HierarchyIndex
                              FROM      dbo.syNavigationNodes
                              WHERE     ResourceId = ChildResourceId
                            ) AS FirstSortOrder
                           ,CASE WHEN (
                                        ChildResourceId IN ( 704 )
                                        OR ParentResourceId IN ( 704 )
                                      ) THEN 1
                                 ELSE 2
                            END AS DisplaySequence
                  FROM      (
                              SELECT DISTINCT
                                        NNChild.ResourceId AS ChildResourceId
                                       ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                             ELSE CASE WHEN NNChild.ResourceId = 397 THEN 'Employer Tabs'
                                                       ELSE RChild.Resource
                                                  END
                                        END AS ChildResource
                                       ,RChild.ResourceURL AS ChildResourceURL
                                       ,CASE WHEN (
                                                    NNParent.ResourceId = 193
                                                    OR NNChild.ResourceId IN ( 704,405,406 )
                                                  ) THEN NULL
                                             ELSE NNParent.ResourceId
                                        END AS ParentResourceId
                                       ,RParent.Resource AS ParentResource
                                       ,RParentModule.Resource AS MODULE      
	--NNParent.ParentId      
                              FROM      syResources RChild
                              INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
                              INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                              INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                              LEFT OUTER JOIN (
                                                SELECT  *
                                                FROM    syResources
                                                WHERE   ResourceTypeId = 1
                                              ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                              WHERE     RChild.ResourceTypeId IN ( 2,4,8 )
                                        AND (
                                              RChild.ChildTypeId IS NULL
                                              OR RChild.ChildTypeId = 4
                                            )
                                        AND ( RChild.ResourceId <> 394 )
                                        AND ( NNParent.ResourceId NOT IN ( 394 ) )
                                        AND (
                                              NNParent.ParentId IN ( SELECT HierarchyId
                                                                     FROM   syNavigationNodes
                                                                     WHERE  ResourceId = 193 )
                                              OR NNChild.ParentId IN ( SELECT   HierarchyId
                                                                       FROM     syNavigationNodes
                                                                       WHERE    ResourceId IN ( 694,405,406,704 ) )
                                            )      
	-- The following condition uses Bitwise Operator      
                                        AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
                            ) t5
                  UNION ALL
                  SELECT    195 AS ModuleResourceId
                           ,ChildResourceId
                           ,ChildResource
                           ,ChildResourceURL
                           ,ParentResourceId
                           ,ParentResource
                           ,(
                              SELECT TOP 1
                                        HierarchyIndex
                              FROM      dbo.syNavigationNodes
                              WHERE     ResourceId = ChildResourceId
                            ) AS FirstSortOrder
                           ,CASE WHEN (
                                        ChildResourceId IN ( 707 )
                                        OR ParentResourceId IN ( 707 )
                                      ) THEN 1
                                 ELSE 2
                            END AS DisplaySequence
                  FROM      (
                              SELECT DISTINCT
                                        NNChild.ResourceId AS ChildResourceId
                                       ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                             ELSE RChild.Resource
                                        END AS ChildResource
                                       ,RChild.ResourceURL AS ChildResourceURL
                                       ,CASE WHEN (
                                                    NNParent.ResourceId = 195
                                                    OR NNChild.ResourceId IN ( 707,463,429,408 )
                                                  ) THEN NULL
                                             ELSE NNParent.ResourceId
                                        END AS ParentResourceId
                                       ,RParent.Resource AS ParentResource
                                       ,RParentModule.Resource AS MODULE      
	--NNParent.ParentId      
                              FROM      syResources RChild
                              INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
                              INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                              INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                              LEFT OUTER JOIN (
                                                SELECT  *
                                                FROM    syResources
                                                WHERE   ResourceTypeId = 1
                                              ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                              WHERE     RChild.ResourceTypeId IN ( 2,4,8 )
                                        AND (
                                              RChild.ChildTypeId IS NULL
                                              OR RChild.ChildTypeId = 4
                                            )
                                        AND ( RChild.ResourceId <> 394 )
                                        AND ( NNParent.ResourceId NOT IN ( 394 ) )
                                        AND (
                                              NNParent.ParentId IN ( SELECT HierarchyId
                                                                     FROM   syNavigationNodes
                                                                     WHERE  ResourceId = 195 )
                                              OR NNChild.ParentId IN ( SELECT   HierarchyId
                                                                       FROM     syNavigationNodes
                                                                       WHERE    ResourceId IN ( 707,463,429,408 ) )
                                            )      
	-- The following condition uses Bitwise Operator      
                                        AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
                            ) t6
                  UNION ALL
                  SELECT    192 AS ModuleResourceId
                           ,ChildResourceId
                           ,ChildResource
                           ,ChildResourceURL
                           ,ParentResourceId
                           ,ParentResource
                           ,(
                              SELECT TOP 1
                                        HierarchyIndex
                              FROM      dbo.syNavigationNodes
                              WHERE     ResourceId = ChildResourceId
                            ) AS FirstSortOrder
                           ,CASE WHEN ChildResourceId = 709
                                      OR ParentResourceId = 709 THEN 1
                                 ELSE 2
                            END AS DisplaySequence
                  FROM      (
                              SELECT DISTINCT
                                        NNChild.ResourceId AS ChildResourceId
                                       ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                             ELSE RChild.Resource
                                        END AS ChildResource
                                       ,RChild.ResourceURL AS ChildResourceURL
                                       ,CASE WHEN (
                                                    NNParent.ResourceId = 192
                                                    OR NNChild.ResourceId IN ( 688,709 )
                                                  ) THEN NULL
                                             ELSE NNParent.ResourceId
                                        END AS ParentResourceId
                                       ,RParent.Resource AS ParentResource
                                       ,RParentModule.Resource AS MODULE      
	--NNParent.ParentId      
                              FROM      syResources RChild
                              INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
                              INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                              INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                              LEFT OUTER JOIN (
                                                SELECT  *
                                                FROM    syResources
                                                WHERE   ResourceTypeId = 1
                                              ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                              WHERE     RChild.ResourceTypeId IN ( 2,4,8 )
                                        AND (
                                              RChild.ChildTypeId IS NULL
                                              OR RChild.ChildTypeId = 4
                                            )
                                        AND ( RChild.ResourceId <> 394 )
                                        AND ( NNParent.ResourceId NOT IN ( 394 ) )
                                        AND (
                                              NNParent.ParentId IN ( SELECT HierarchyId
                                                                     FROM   syNavigationNodes
                                                                     WHERE  ResourceId = 192 )
                                              OR NNChild.ParentId IN ( SELECT   HierarchyId
                                                                       FROM     syNavigationNodes
                                                                       WHERE    ResourceId IN ( 709 ) )
                                            )      
	-- The following condition uses Bitwise Operator      
                                        AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
                            ) t7
                ) MainQuery
        WHERE   ChildResourceId NOT IN ( 326,616,797 )
                AND -- Hide resources based on Configuration Settings      
                (
                  -- The following expression means if showross... is set to false, hide       
	 -- ResourceIds 541,542,532,534,535,538,543,539      
	 -- (Not False) OR (Condition)      
				  (
                    ( @ShowRossOnlyTabs <> 0 )
                    OR ( ChildResourceId NOT IN ( 541,542,532,534,535,538,543,539 ) )
                  )
                  AND      
	 -- The following expression means if showross... is set to true, hide       
	 -- ResourceIds 142,375,330,476,508,102,107,237      
	 -- (Not True) OR (Condition)      
                  (
                    ( @ShowRossOnlyTabs <> 1 )
                    OR ( ChildResourceId NOT IN ( 142,375,330,476,508,102,107,237 ) )
                  )
                  AND      
	 ---- The following expression means if SchedulingMethod=regulartraditional, hide       
	 ---- ResourceIds 91 and 497      
	 ---- (Not True) OR (Condition)      
                  (
                    (
                      NOT LOWER(LTRIM(RTRIM(@SchedulingMethod))) = 'regulartraditional'
                    )
                    OR ( ChildResourceId NOT IN ( 91,497 ) )
                  )
                  AND      
	 -- The following expression means if TrackSAPAttendance=byday, hide       
	 -- ResourceIds 633      
	 -- (Not True) OR (Condition)      
                  (
                    (
                      NOT LOWER(LTRIM(RTRIM(@TrackSAPAttendance))) = 'byday'
                    )
                    OR ( ChildResourceId NOT IN ( 633 ) )
                  )
                  AND      
	 ---- The following expression means if @ShowCollegeOfCourtReporting is set to false, hide       
	 ---- ResourceIds 614,615      
	 ---- (Not False) OR (Condition)      
                  (
                    (
                      NOT LOWER(LTRIM(RTRIM(@ShowCollegeOfCourtReporting))) = 'no'
                    )
                    OR ( ChildResourceId NOT IN ( 614,615 ) )
                  )
                  AND      
	 -- The following expression means if @ShowCollegeOfCourtReporting is set to true, hide       
	 -- ResourceIds 497      
	 -- (Not True) OR (Condition)      
                  (
                    (
                      NOT LOWER(LTRIM(RTRIM(@ShowCollegeOfCourtReporting))) = 'yes'
                    )
                    OR ( ChildResourceId NOT IN ( 497 ) )
                  )
                  AND      
	 -- The following expression means if FAMEESP is set to false, hide       
	 -- ResourceIds 517,523, 525      
	 -- (Not False) OR (Condition)      
                  (
                    (
                      NOT LOWER(LTRIM(RTRIM(@FameESP))) = 'no'
                    )
                    OR ( ChildResourceId NOT IN ( 517,523,525 ) )
                  )
                  AND      
	 ---- The following expression means if EDExpress is set to false, hide       
	 ---- ResourceIds 603,604,606,619      
	 ---- (Not False) OR (Condition)      
                  (
                    (
                      NOT LOWER(LTRIM(RTRIM(@EdExpress))) = 'no'
                    )
                    OR ( ChildResourceId NOT IN ( 603,604,605,606,619 ) )
                  )
                  AND    
	 ---- The following expression means if TimeClockClassSection is set to false, hide       
	 ---- ResourceIds 7      
	 ---- (Not False) OR (Condition)      
                  (
                    (
                      NOT LOWER(LTRIM(RTRIM(@TimeClockClassSection))) = 'no'
                    )
                    OR ( ChildResourceId NOT IN ( 7 ) )
                  )
                  AND        
	 ---- The following expression means if @GradeBookWeightingLevel is set to courselevel, hide       
	 ---- ResourceIds 107,96,222      
	 ---- (Not False) OR (Condition)      
                  (
                    (
                      NOT LOWER(LTRIM(RTRIM(@GradeBookWeightingLevel))) = 'courselevel'
                    )
                    OR ( ChildResourceId NOT IN ( 107,96,222 ) )
                  )
                  AND (
                        (
                          NOT LOWER(LTRIM(RTRIM(@GradeBookWeightingLevel))) = 'instructorlevel'
                        )
                        OR ( ChildResourceId NOT IN ( 476 ) )
                      )
                  AND (
                        (
                          NOT LOWER(LTRIM(RTRIM(@ShowExternshipTabs))) = 'no'
                        )
                        OR ( ChildResourceId NOT IN ( 543,538 ) )
                      )
                  AND    
				 --If Time Clock Exception is set to no and user is not support    
				 -- then hide setup punch codes    
				 -- IF A THEN B ==> (NOT A) or (B)    
				 -- A Part: If ConfigSetting TimeClockClassSection is set to NO (AND) User is not a Power User (support)    
				 -- B Part: Hide Resource Setup Punch Codes (769)    
                  (
                    (
                      NOT @TimeClockClassSection = 'no'
                      AND NOT @IsSupportORSuper = 0
                    )
                    OR ( ChildResourceId NOT IN ( 769 ) )
                  )
                  AND (
                        (
                          NOT @IsSupportORSuper = 0
                        )
                        OR ( ChildResourceId NOT IN ( 616 ) )
                      )
                  AND    
	 ---- If user is not a power user (sa or support or super), hide Setup Student page Navigation (326)  
	 ---- (Not False) OR (Condition)      
                  (
                    (
                      NOT @IsUserAPowerUser = 0
                    )
                    OR ( ChildResourceId NOT IN ( 326 ) )
                  )
                )
        ORDER BY ModuleResourceId
               ,      
 --FirstSortOrder,      
 --ParentResourceId,      
                DisplaySequence
               ,ChildResource;      
    END;  



GO
