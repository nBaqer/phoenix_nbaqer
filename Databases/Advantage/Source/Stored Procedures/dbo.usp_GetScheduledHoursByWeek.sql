SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_GetScheduledHoursByWeek]
    (
     @stuEnrollId UNIQUEIDENTIFIER
    )
AS
    SET NOCOUNT ON;

    SELECT  SUM(t4.Total)
    FROM    arStuEnrollments t1
           ,arStudentSchedules t2
           ,arProgSchedules t3
           ,arProgScheduleDetails t4
    WHERE   t1.StuEnrollId = t2.StuEnrollId
            AND t1.PrgVerId = t3.PrgVerId
            AND t2.ScheduleId = t3.ScheduleId
            AND t3.scheduleId = t4.ScheduleId
            AND t2.StuEnrollId = @stuEnrollId
            AND t4.total IS NOT NULL; 



GO
