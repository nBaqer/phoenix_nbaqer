SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Edwin Sosa
-- Create date: 7/9/2018
-- Description:	Used to calculate the trigger date for attendance or credits attempted triggers for Title IV Sap
-- =============================================
CREATE PROCEDURE [dbo].[USP_TitleIV_GetTriggerMetDate]
    -- Add the parameters for the stored procedure here
    @StuEnrollId UNIQUEIDENTIFIER = NULL
   ,@StuEnrollCampusId UNIQUEIDENTIFIER = NULL
   ,@TriggerValue DECIMAL(18, 2)
   ,@TriggerUnitTypeId INTEGER
   ,@IncludeExternship BIT
   ,@IncludeTransferHours BIT
   ,@TriggerMetDate DATETIME OUTPUT
AS
    BEGIN
        -- SET NOCOUNT ON added to prevent extra result sets from
        -- interfering with SELECT statements.
        SET NOCOUNT ON;

        DECLARE @TrackSapAttendance VARCHAR(1000);
        DECLARE @TrigUnitTypeDescrip VARCHAR(250);
        DECLARE @UnitTypeDescrip VARCHAR(250);
        SET @TrigUnitTypeDescrip = (
                                   SELECT LTRIM(RTRIM(TUT.TrigUnitTypDescrip))
                                   FROM   arTrigUnitTyps AS TUT
                                   WHERE  TUT.TrigUnitTypId = @TriggerUnitTypeId
                                   );

        SET @UnitTypeDescrip = (
                               SELECT     LTRIM(RTRIM(AAUT.UnitTypeDescrip))
                               FROM       arAttUnitType AS AAUT
                               INNER JOIN arPrgVersions AS APV ON APV.UnitTypeId = AAUT.UnitTypeId
                               INNER JOIN arStuEnrollments AS ASE ON ASE.PrgVerId = APV.PrgVerId
                               WHERE      ASE.StuEnrollId = @StuEnrollId
                               );
        IF ( @StuEnrollCampusId IS NULL )
            BEGIN
                SET @StuEnrollCampusId = COALESCE((
                                                  SELECT ASE.CampusId
                                                  FROM   arStuEnrollments AS ASE
                                                  WHERE  ASE.StuEnrollId = @StuEnrollId
                                                  )
                                                 ,NULL
                                                 );
            END;

        SET @TrackSapAttendance = dbo.GetAppSettingValueByKeyName('TrackSapAttendance', @StuEnrollCampusId);
        IF ( @TrackSapAttendance IS NOT NULL )
            BEGIN
                SET @TrackSapAttendance = LOWER(LTRIM(RTRIM(@TrackSapAttendance)));
            END;

        DECLARE @hoursBucket AS TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER NOT NULL
               ,MeetDate DATETIME
               ,Actual DECIMAL(18, 2)
               ,Scheduled DECIMAL(18, 2)
            );


        DECLARE @creditsBucket AS TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,DateCompleted DATETIME
               ,CreditsAttempted DECIMAL(18, 2)
            );

        -- Insert statements for procedure here
        --DECLARE @trigger INTEGER = 5;
        --DECLARE @StuEnrollId UNIQUEIDENTIFIER = '433F60AB-1596-4616-8A8E-7DA51E3BDDED';

        --We are calculating a trigger based on actual or scheduled hours
        IF @TrigUnitTypeDescrip IN ( 'Actual Hours', 'Scheduled Hours' )
            BEGIN
                IF @UnitTypeDescrip IN ( 'none', 'present absent' )
                   AND @TrackSapAttendance = 'byclass'
                    BEGIN
                        INSERT INTO @hoursBucket
                                    SELECT nPaByClass.StuEnrollId
                                          ,nPaByClass.MeetDate
                                          ,( CASE WHEN ( nPaByClass.Actual >= 1 ) THEN DATEDIFF(MI, nPaByClass.StartTime, nPaByClass.EndTime)
                                                  ELSE 0
                                             END
                                           ) / 60 AS Actual
                                          ,( CASE WHEN ( nPaByClass.Scheduled >= 1 ) THEN DATEDIFF(MI, nPaByClass.StartTime, nPaByClass.EndTime)
                                                  ELSE 0
                                             END
                                           ) / 60 AS Scheduled
                                    FROM   (
                                           SELECT *
                                                 ,ROW_NUMBER() OVER ( ORDER BY MeetDate ) AS RowNumber
                                           FROM   (
                                                  SELECT     DISTINCT t1.StuEnrollId AS StuEnrollId
                                                                     ,t1.ClsSectionId
                                                                     ,t1.MeetDate AS MeetDate
                                                                     ,DATENAME(dw, t1.MeetDate) AS WeekDay
                                                                     ,t4.StartDate
                                                                     ,t4.EndDate
                                                                     ,t5.PeriodDescrip
                                                                     ,t1.Actual AS Actual
                                                                     ,t1.Excused
                                                                     ,CASE WHEN (
                                                                                t1.Actual = 0
                                                                                AND t1.Excused = 0
                                                                                ) THEN t1.Scheduled
                                                                           ELSE CASE WHEN (
                                                                                          t1.Actual <> 9999.00
                                                                                          AND t1.Actual < t1.Scheduled
                                                                                          --AND t1.Excused <> 1
                                                                                          ) THEN ( t1.Scheduled - t1.Actual )
                                                                                     ELSE 0
                                                                                END
                                                                      END AS Absent
                                                                     ,t1.Scheduled AS Scheduled
                                                                     ,CASE WHEN (
                                                                                t1.Actual > 0
                                                                                AND t1.Actual < t1.Scheduled
                                                                                ) THEN ( t1.Scheduled - t1.Actual )
                                                                           ELSE 0
                                                                      END AS TardyMinutes
                                                                     ,t1.Tardy AS Tardy
                                                                     ,t3.TrackTardies
                                                                     ,t3.TardiesMakingAbsence
                                                                     ,t3.PrgVerId
                                                                     ,t6.TimeIntervalDescrip AS StartTime
                                                                     ,t7.TimeIntervalDescrip AS EndTime
                                                  FROM       atClsSectAttendance t1
                                                  INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                                                  INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                                                  INNER JOIN arAttUnitType AS AAUT1 ON AAUT1.UnitTypeId = t3.UnitTypeId
                                                  INNER JOIN arClsSectMeetings t4 ON t1.ClsSectionId = t4.ClsSectionId
                                                                                     AND (
                                                                                         CONVERT(DATE, t1.MeetDate, 111) >= CONVERT(DATE, t4.StartDate, 111)
                                                                                         AND CONVERT(DATE, t1.MeetDate, 111) <= CONVERT(DATE, t4.EndDate, 111)
                                                                                         )
                                                                                     AND t1.ClsSectMeetingId = t4.ClsSectMeetingId --DE8479 line added
                                                  INNER JOIN syPeriods t5 ON t4.PeriodId = t5.PeriodId
                                                  INNER JOIN cmTimeInterval t6 ON t5.StartTimeId = t6.TimeIntervalId
                                                  INNER JOIN cmTimeInterval t7 ON t5.EndTimeId = t7.TimeIntervalId
                                                  INNER JOIN dbo.arClassSections t9 ON t9.ClsSectionId = t1.ClsSectionId
                                                  INNER JOIN dbo.arReqs t10 ON t10.ReqId = t9.ReqId
                                                  INNER JOIN arAttUnitType AS AAUT2 ON AAUT2.UnitTypeId = t10.UnitTypeId
                                                  WHERE      t2.StuEnrollId = @StuEnrollId
                                                             AND (
                                                                 AAUT1.UnitTypeDescrip IN ( 'None', 'Present Absent' )
                                                                 OR AAUT2.UnitTypeDescrip IN ( 'None', 'Present Absent' )
                                                                 )
                                                             AND t1.Actual <> 9999
                                                  ) dt
                                           ) nPaByClass;


                    END;
                ELSE IF @UnitTypeDescrip IN ( 'minutes', 'clock hours' )
                        AND @TrackSapAttendance = 'byclass'
                         BEGIN

                             INSERT INTO @hoursBucket
                                         SELECT minChByClass.StuEnrollId
                                               ,minChByClass.MeetDate
                                               ,minChByClass.Actual / 60
                                               ,minChByClass.Scheduled / 60
                                         FROM   (
                                                SELECT *
                                                      ,ROW_NUMBER() OVER ( ORDER BY MeetDate ) AS RowNumber
                                                FROM   (
                                                       SELECT     DISTINCT t1.StuEnrollId AS StuEnrollId
                                                                          ,t1.ClsSectionId
                                                                          ,t1.MeetDate AS MeetDate
                                                                          ,DATENAME(dw, t1.MeetDate) AS WeekDay
                                                                          ,t4.StartDate
                                                                          ,t4.EndDate
                                                                          ,t5.PeriodDescrip
                                                                          ,t1.Actual AS Actual
                                                                          ,t1.Excused
                                                                          ,CASE WHEN (
                                                                                     t1.Actual = 0
                                                                                     AND t1.Excused = 0
                                                                                     ) THEN t1.Scheduled
                                                                                ELSE CASE WHEN (
                                                                                               t1.Actual <> 9999.00
                                                                                               AND t1.Actual < t1.Scheduled
                                                                                               ) THEN ( t1.Scheduled - t1.Actual )
                                                                                          ELSE 0
                                                                                     END
                                                                           END AS Absent
                                                                          ,t1.Scheduled AS Scheduled
                                                                          ,CASE WHEN (
                                                                                     t1.Actual > 0
                                                                                     AND t1.Actual < t1.Scheduled
                                                                                     ) THEN ( t1.Scheduled - t1.Actual )
                                                                                ELSE 0
                                                                           END AS TardyMinutes
                                                                          ,t1.Tardy AS Tardy
                                                                          ,t3.TrackTardies
                                                                          ,t3.TardiesMakingAbsence
                                                                          ,t3.PrgVerId
                                                       FROM       atClsSectAttendance t1
                                                       INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                                                       INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                                                       INNER JOIN arAttUnitType AS AAUT1 ON AAUT1.UnitTypeId = t3.UnitTypeId
                                                       INNER JOIN arClsSectMeetings t4 ON t1.ClsSectionId = t4.ClsSectionId
                                                                                          AND (
                                                                                              CONVERT(DATE, t1.MeetDate, 111) >= CONVERT(DATE, t4.StartDate, 111)
                                                                                              AND CONVERT(DATE, t1.MeetDate, 111) <= CONVERT(DATE, t4.EndDate, 111)
                                                                                              )
                                                                                          AND t1.ClsSectMeetingId = t4.ClsSectMeetingId --DE8479 line added
                                                       INNER JOIN syPeriods t5 ON t4.PeriodId = t5.PeriodId
                                                       INNER JOIN cmTimeInterval t6 ON t5.StartTimeId = t6.TimeIntervalId
                                                       INNER JOIN cmTimeInterval t7 ON t5.EndTimeId = t7.TimeIntervalId
                                                       INNER JOIN dbo.arClassSections t9 ON t9.ClsSectionId = t1.ClsSectionId
                                                       INNER JOIN dbo.arReqs t10 ON t10.ReqId = t9.ReqId
                                                       INNER JOIN arAttUnitType AS AAUT2 ON AAUT2.UnitTypeId = t10.UnitTypeId
                                                       WHERE      t2.StuEnrollId = @StuEnrollId
                                                                  AND (
                                                                      AAUT1.UnitTypeDescrip IN ( 'Minutes', 'Clock Hours' )
                                                                      OR AAUT2.UnitTypeDescrip IN ( 'Minutes', 'Clock Hours' )
                                                                      )
                                                                  AND t1.Actual <> 9999
                                                       UNION
                                                       SELECT     DISTINCT t1.StuEnrollId
                                                                          ,NULL AS ClsSectionId
                                                                          ,t1.RecordDate AS MeetDate
                                                                          ,DATENAME(dw, t1.RecordDate) AS WeekDay
                                                                          ,NULL AS StartDate
                                                                          ,NULL AS EndDate
                                                                          ,NULL AS PeriodDescrip
                                                                          ,t1.ActualHours
                                                                          ,NULL AS Excused
                                                                          ,CASE WHEN ( t1.ActualHours = 0 ) THEN t1.SchedHours
                                                                                ELSE 0
                                                                           --ELSE 
                                                                           --Case when (t1.ActualHours <> 9999.00 and t1.ActualHours < t1.SchedHours)
                                                                           --		THEN (t1.SchedHours - t1.ActualHours)
                                                                           --		ELSE 
                                                                           --			0
                                                                           --		End
                                                                           END AS Absent
                                                                          ,t1.SchedHours AS ScheduledMinutes
                                                                          ,CASE WHEN (
                                                                                     t1.ActualHours <> 9999.00
                                                                                     AND t1.ActualHours > 0
                                                                                     AND t1.ActualHours < t1.SchedHours
                                                                                     ) THEN ( t1.SchedHours - t1.ActualHours )
                                                                                ELSE 0
                                                                           END AS TardyMinutes
                                                                          ,NULL AS Tardy
                                                                          ,t3.TrackTardies
                                                                          ,t3.TardiesMakingAbsence
                                                                          ,t3.PrgVerId
                                                       FROM       arStudentClockAttendance t1
                                                       INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                                                       INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                                                       WHERE      t2.StuEnrollId = @StuEnrollId
                                                                  AND t1.Converted = 1
                                                                  AND t1.ActualHours <> 9999
                                                       ) dt
                                                ) minChByClass;
                         END;
                ELSE IF @UnitTypeDescrip IN ( 'minutes' )
                        AND @TrackSapAttendance = 'byday'
                         BEGIN
                             INSERT INTO @hoursBucket
                                         SELECT minByDay.StuEnrollId
                                               ,minByDay.MeetDate
                                               ,minByDay.Actual
                                               ,minByDay.Scheduled
                                         FROM   (
                                                SELECT     t1.StuEnrollId AS StuEnrollId
                                                          ,NULL AS ClsSectionId
                                                          ,t1.RecordDate AS MeetDate
                                                          ,t1.ActualHours AS Actual
                                                          ,t1.SchedHours AS Scheduled
                                                          ,CASE WHEN (
                                                                     (
                                                                     t1.SchedHours >= 1
                                                                     AND t1.SchedHours NOT IN ( 999, 9999 )
                                                                     )
                                                                     AND t1.ActualHours = 0
                                                                     ) THEN t1.SchedHours
                                                                ELSE 0
                                                           END AS Absent
                                                          ,t1.isTardy
                                                          ,(
                                                           SELECT ISNULL(SUM(SchedHours), 0)
                                                           FROM   arStudentClockAttendance
                                                           WHERE  StuEnrollId = t1.StuEnrollId
                                                                  AND RecordDate <= t1.RecordDate
                                                                  AND (
                                                                      t1.SchedHours >= 1
                                                                      AND t1.SchedHours NOT IN ( 999, 9999 )
                                                                      AND t1.ActualHours NOT IN ( 999, 9999 )
                                                                      )
                                                           ) AS ActualRunningScheduledHours
                                                          ,(
                                                           SELECT SUM(ActualHours)
                                                           FROM   arStudentClockAttendance
                                                           WHERE  StuEnrollId = t1.StuEnrollId
                                                                  AND RecordDate <= t1.RecordDate
                                                                  AND (
                                                                      t1.SchedHours >= 1
                                                                      AND t1.SchedHours NOT IN ( 999, 9999 )
                                                                      )
                                                                  AND ActualHours >= 1
                                                                  AND ActualHours NOT IN ( 999, 9999 )
                                                           ) AS ActualRunningPresentHours
                                                          ,(
                                                           SELECT COUNT(ActualHours)
                                                           FROM   arStudentClockAttendance
                                                           WHERE  StuEnrollId = t1.StuEnrollId
                                                                  AND RecordDate <= t1.RecordDate
                                                                  AND (
                                                                      t1.SchedHours >= 1
                                                                      AND t1.SchedHours NOT IN ( 999, 9999 )
                                                                      )
                                                                  AND ActualHours = 0
                                                                  AND ActualHours NOT IN ( 999, 9999 )
                                                           ) AS ActualRunningAbsentHours
                                                          ,(
                                                           SELECT SUM(ActualHours)
                                                           FROM   arStudentClockAttendance
                                                           WHERE  StuEnrollId = t1.StuEnrollId
                                                                  AND RecordDate <= t1.RecordDate
                                                                  AND SchedHours = 0
                                                                  AND ActualHours >= 1
                                                                  AND ActualHours NOT IN ( 999, 9999 )
                                                           ) AS ActualRunningMakeupHours
                                                          ,(
                                                           SELECT SUM(SchedHours - ActualHours)
                                                           FROM   arStudentClockAttendance
                                                           WHERE  StuEnrollId = t1.StuEnrollId
                                                                  AND RecordDate <= t1.RecordDate
                                                                  AND (
                                                                      (
                                                                      t1.SchedHours >= 1
                                                                      AND t1.SchedHours NOT IN ( 999, 9999 )
                                                                      )
                                                                      AND ActualHours >= 1
                                                                      AND ActualHours NOT IN ( 999, 9999 )
                                                                      )
                                                                  AND isTardy = 1
                                                           ) AS ActualRunningTardyHours
                                                          ,t3.TrackTardies
                                                          ,t3.TardiesMakingAbsence
                                                          ,t3.PrgVerId
                                                          ,ROW_NUMBER() OVER ( ORDER BY t1.RecordDate ) AS RowNumber
                                                FROM       arStudentClockAttendance t1
                                                INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                                                INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                                                INNER JOIN arAttUnitType AS AAUT1 ON AAUT1.UnitTypeId = t3.UnitTypeId
                                                --inner join Inserted t4 on t1.StuEnrollId = t4.StuEnrollId and t1.RecordDate=t4.RecordDate
                                                WHERE      AAUT1.UnitTypeDescrip IN ( 'Minutes' )
                                                           AND t2.StuEnrollId = @StuEnrollId
                                                           AND t1.ActualHours <> 9999.00
                                                ) minByDay;
                         END;
                ELSE IF @UnitTypeDescrip IN ( 'present absent', 'clock hours' )
                        AND @TrackSapAttendance = 'byday'
                         BEGIN
                             ---- PRINT 'By Day inside day';
                             INSERT INTO @hoursBucket
                                         SELECT paChByDay.StuEnrollId
                                               ,paChByDay.MeetDate
                                               ,paChByDay.Actual
                                               ,paChByDay.Scheduled
                                         FROM   (
                                                SELECT     t1.StuEnrollId AS StuEnrollId
                                                          ,t1.RecordDate AS MeetDate
                                                          ,t1.ActualHours AS Actual
                                                          ,t1.SchedHours AS Scheduled
                                                          ,CASE WHEN (
                                                                     (
                                                                     t1.SchedHours >= 1
                                                                     AND t1.SchedHours NOT IN ( 999, 9999 )
                                                                     )
                                                                     AND t1.ActualHours = 0
                                                                     ) THEN t1.SchedHours
                                                                ELSE 0
                                                           END AS Absent
                                                          ,t1.isTardy
                                                          ,t3.TrackTardies
                                                          ,t3.TardiesMakingAbsence
                                                FROM       arStudentClockAttendance t1
                                                INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                                                INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                                                INNER JOIN arAttUnitType AS AAUT1 ON AAUT1.UnitTypeId = t3.UnitTypeId
                                                --inner join Inserted t4 on t1.StuEnrollId = t4.StuEnrollId and t1.RecordDate=t4.RecordDate
                                                WHERE -- Unit Types: Present Absent and Clock Hour
                                                           AAUT1.UnitTypeDescrip IN ( 'present absent', 'clock hours' )
                                                           AND ActualHours <> 9999.00
                                                           AND t2.StuEnrollId = @StuEnrollId
                                                ) paChByDay;
                         END;
                IF ( @IncludeTransferHours = 1 )
                    BEGIN
                        INSERT INTO @hoursBucket
                                    SELECT th.StuEnrollId
                                          ,th.MeetDate
                                          ,th.Actual
                                          ,0
                                    FROM   (
                                           SELECT SE.StuEnrollId AS StuEnrollId
                                                 ,SE.StartDate AS MeetDate
                                                 ,SE.TransferHours AS Actual
                                           FROM   dbo.arStuEnrollments SE
                                           WHERE  SE.StuEnrollId = @StuEnrollId
                                           ) th;

                    END;

                IF ( @IncludeExternship = 1 )
                    BEGIN
                        INSERT INTO @hoursBucket
                                    SELECT th.StuEnrollId
                                          ,th.MeetDate
                                          ,th.Actual
                                          ,0
                                    FROM   (
                                           SELECT SE.StuEnrollId AS StuEnrollId
                                                 ,SE.AttendedDate AS MeetDate
                                                 ,SE.HoursAttended AS Actual
                                           FROM   dbo.arExternshipAttendance SE
                                           WHERE  SE.StuEnrollId = @StuEnrollId
                                           ) th;

                    END;

                BEGIN
                    IF @TrigUnitTypeDescrip = 'Actual Hours'
                        BEGIN
                            DECLARE @ActualHoursSum DECIMAL(18, 2);
                            SET @ActualHoursSum = (
                                                  SELECT SUM(t1.Actual)
                                                  FROM   @hoursBucket t1
                                                  WHERE  t1.StuEnrollId = @StuEnrollId
                                                  );
                            --return trigger met date as null if trigger has not been met yet
                            IF ( @ActualHoursSum < @TriggerValue )
                                BEGIN
                                    SET @TriggerMetDate = NULL;
                                END;
                            ELSE
                                SET @TriggerMetDate = (
                                                      SELECT TOP 1 temp.TriggerMet
                                                      FROM   (
                                                             SELECT   MIN(a.MeetDate) AS TriggerMet
                                                                     ,a.StuEnrollId
                                                             FROM     (
                                                                      SELECT     SUM(t1.Actual) AS actualHrs
                                                                                ,t2.MeetDate
                                                                                ,t2.StuEnrollId
                                                                      FROM       @hoursBucket t1
                                                                      INNER JOIN @hoursBucket t2 ON t1.StuEnrollId = t2.StuEnrollId
                                                                                                    AND t1.MeetDate <= t2.MeetDate
                                                                      GROUP BY   t2.MeetDate
                                                                                ,t2.StuEnrollId
                                                                      HAVING     SUM(t1.Actual) >= @TriggerValue
                                                                      ) a
                                                             GROUP BY a.StuEnrollId
                                                             ) temp
                                                      );
                        END;
                    ELSE
                        BEGIN
                            DECLARE @ScheduledHoursSum DECIMAL(18, 2);
                            SET @ScheduledHoursSum = (
                                                     SELECT SUM(Scheduled)
                                                     FROM   @hoursBucket t1
                                                     WHERE  t1.StuEnrollId = @StuEnrollId
                                                     );
                            --return trigger met date as null if trigger has not been met yet
                            IF ( @ScheduledHoursSum < @TriggerValue )
                                BEGIN
                                    SET @TriggerMetDate = NULL;
                                END;
                            ELSE
                                SET @TriggerMetDate = (
                                                      SELECT TOP 1 temp.TriggerMet
                                                      FROM   (
                                                             SELECT   MIN(a.MeetDate) AS TriggerMet
                                                                     ,a.StuEnrollId
                                                             FROM     (
                                                                      SELECT     SUM(t1.Scheduled) AS scheduledHrs
                                                                                ,t2.MeetDate
                                                                                ,t2.StuEnrollId
                                                                      FROM       @hoursBucket t1
                                                                      INNER JOIN @hoursBucket t2 ON t1.StuEnrollId = t2.StuEnrollId
                                                                                                    AND t1.MeetDate <= t2.MeetDate
                                                                      GROUP BY   t2.MeetDate
                                                                                ,t2.StuEnrollId
                                                                      HAVING     SUM(t1.Scheduled) >= @TriggerValue
                                                                      ) a
                                                             GROUP BY a.StuEnrollId
                                                             ) temp
                                                      );
                        END;
                END;

            END;

        ELSE IF @TrigUnitTypeDescrip IN ( 'Credits Attempted' )
                 BEGIN
                     --find offset date for credits attempted here



                     INSERT INTO @creditsBucket
                                 SELECT credsAttempted.StuEnrollId
                                       ,credsAttempted.DateCompleted
                                       ,credsAttempted.CreditsAttempted
                                 FROM   (
                                        SELECT     DISTINCT SE.StuEnrollId
                                                           ,T.TermId
                                                           ,T.TermDescrip
                                                           ,T.StartDate
                                                           ,R.ReqId
                                                           ,'(' + R.Code + ' ) ' + R.Descrip AS CourseCodeDescrip
                                                           ,RES.Score AS FinalScore
                                                           ,RES.GrdSysDetailId AS FinalGrade
                                                           ,RES.DateCompleted AS DateCompleted
                                                           ,GCT.SysComponentTypeId
                                                           ,R.Credits AS CreditsAttempted
                                                           ,CS.ClsSectionId
                                                           ,GSD.Grade
                                                           ,GSD.IsPass
                                                           ,GSD.IsCreditsAttempted
                                                           ,GSD.IsCreditsEarned
                                                           ,SE.PrgVerId
                                                           ,GSD.IsInGPA
                                                           ,R.FinAidCredits AS FinAidCredits
                                                           ,RES.IsCourseCompleted
                                        FROM       arStuEnrollments SE
                                        INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                                        INNER JOIN arResults RES ON RES.StuEnrollId = SE.StuEnrollId
                                        INNER JOIN arClassSections CS ON RES.TestId = CS.ClsSectionId
                                        INNER JOIN arTerm T ON CS.TermId = T.TermId
                                        INNER JOIN arReqs R ON CS.ReqId = R.ReqId
                                        LEFT JOIN  arGrdBkResults GBR ON CS.ClsSectionId = GBR.ClsSectionId
                                        LEFT JOIN  arGrdBkWgtDetails GBWD ON GBR.InstrGrdBkWgtDetailId = GBWD.InstrGrdBkWgtDetailId
                                        LEFT JOIN  arGrdComponentTypes GCT ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
                                        LEFT JOIN  arGradeSystemDetails GSD ON RES.GrdSysDetailId = GSD.GrdSysDetailId
                                        WHERE      SE.StuEnrollId = @StuEnrollId
                                                   AND (
                                                       GSD.GrdSysDetailId IS NOT NULL
                                                       AND GSD.IsCreditsAttempted = 1
                                                       )
                                                   AND RES.DateCompleted IS NOT NULL
                                        UNION
                                        (SELECT     DISTINCT SE.StuEnrollId
                                                            ,T.TermId
                                                            ,T.TermDescrip
                                                            ,T.StartDate
                                                            ,R.ReqId
                                                            ,'(' + R.Code + ' ) ' + R.Descrip AS CourseCodeDescrip
                                                            ,RES.Score AS FinalScore
                                                            ,RES.GrdSysDetailId AS FinalGrade
                                                            ,RES.CompletedDate AS DateCompleted
                                                            ,NULL
                                                            ,R.Credits AS CreditsAttempted
                                                            ,NULL AS ClsSectionId
                                                            ,GSD.Grade
                                                            ,GSD.IsPass
                                                            ,GSD.IsCreditsAttempted
                                                            ,GSD.IsCreditsEarned
                                                            ,SE.PrgVerId
                                                            ,GSD.IsInGPA
                                                            ,R.FinAidCredits AS FinAidCredits
                                                            ,RES.IsTransferred
                                         FROM       arStuEnrollments SE
                                         INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                                         INNER JOIN arTransferGrades RES ON RES.StuEnrollId = SE.StuEnrollId
                                         --INNER JOIN arClassSections CS ON RES.ReqId = CS.ReqId and RES.TermId=CS.TermId 
                                         INNER JOIN arTerm T ON RES.TermId = T.TermId
                                         INNER JOIN arReqs R ON RES.ReqId = R.ReqId
                                         --LEFT JOIN arGrdBkResults GBR ON CS.ClsSectionId=GBR.ClsSectionId
                                         --LEFT JOIN arGrdBkWgtDetails GBWD ON GBR.InstrGrdBkWgtDetailId = GBWD.InstrGrdBkWgtDetailId
                                         --LEFT JOIN arGrdComponentTypes GCT on GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId 
                                         LEFT JOIN  arGradeSystemDetails GSD ON RES.GrdSysDetailId = GSD.GrdSysDetailId
                                         WHERE      SE.StuEnrollId = @StuEnrollId
                                                    AND (
                                                        GSD.GrdSysDetailId IS NOT NULL
                                                        AND GSD.IsCreditsAttempted = 1
                                                        )
                                                    AND RES.CompletedDate IS NOT NULL)
                                        ) credsAttempted;

                     DECLARE @CreditsAttemptedSum DECIMAL(18, 2);
                     SET @CreditsAttemptedSum = (
                                                SELECT SUM(t1.CreditsAttempted)
                                                FROM   @creditsBucket t1
                                                WHERE  t1.StuEnrollId = @StuEnrollId
                                                );
                     --return trigger met date as null if trigger has not been met yet
                     IF ( @CreditsAttemptedSum < @TriggerValue )
                         BEGIN
                             SET @TriggerMetDate = NULL;
                         END;
                     ELSE
                         BEGIN
                             SET @TriggerMetDate = (
                                                   SELECT TOP 1 temp.TriggerMet
                                                   FROM   (
                                                          SELECT   MIN(a.DateCompleted) AS TriggerMet
                                                                  ,a.StuEnrollId
                                                          FROM     (
                                                                   SELECT     SUM(t1.CreditsAttempted) AS creditsAttempted
                                                                             ,t2.DateCompleted
                                                                             ,t2.StuEnrollId
                                                                   FROM       @creditsBucket t1
                                                                   INNER JOIN @creditsBucket t2 ON t1.StuEnrollId = t2.StuEnrollId
                                                                                                   AND t1.DateCompleted <= t2.DateCompleted
                                                                   GROUP BY   t2.DateCompleted
                                                                             ,t2.StuEnrollId
                                                                   HAVING     SUM(t1.CreditsAttempted) >= @TriggerValue
                                                                   ) a
                                                          GROUP BY a.StuEnrollId
                                                          ) temp
                                                   );
                         END;

                 END;

    END;
GO
