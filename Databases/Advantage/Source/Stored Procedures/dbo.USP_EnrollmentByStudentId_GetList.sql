SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_EnrollmentByStudentId_GetList]
    @StudentId UNIQUEIDENTIFIER
AS
    SELECT DISTINCT
            t2.StuEnrollId
           ,t3.PrgVerDescrip + ' (' + t4.StatusCodeDescrip + ')' AS PrgVerDescrip
           ,t2.ModDate
    FROM    arStudent t1
    INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
    INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
    INNER JOIN syStatusCodes t4 ON t2.StatusCodeId = t4.StatusCodeId
    WHERE   t1.StudentId = @StudentId
    ORDER BY t2.ModDate DESC;



GO
