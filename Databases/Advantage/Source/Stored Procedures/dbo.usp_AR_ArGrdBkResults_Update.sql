SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/* 
US4257 Course marked as incomplete when score of one of the component is removed

CREATED: 
7/24/2013 WMP,TT

PURPOSE: 
Update arGrdBkResults


MODIFIED:


*/

CREATE PROCEDURE [dbo].[usp_AR_ArGrdBkResults_Update]
    @ClsSectionId UNIQUEIDENTIFIER
   ,@StuEnrollId UNIQUEIDENTIFIER
   ,@InstrGrdBkWgtDetailId UNIQUEIDENTIFIER
   ,@Score DECIMAL(6,2)
   ,@Comments VARCHAR(50)
   ,@ModUser VARCHAR(50)
   ,@ResNum INT = 0
   ,@IsCompGraded BIT
   ,@DateCompleted DATE
AS
    BEGIN

        DECLARE @IsScoreChanged BIT = 0;
        DECLARE @ExistingScore DECIMAL(6,2);
        DECLARE @ResourceComp INT;
	
        SELECT TOP 1
                @ExistingScore = Score
               ,@ResourceComp = r.ResourceID
        FROM    arGrdBkResults gbr
        JOIN    arGrdBkWgtDetails gbwr ON gbwr.InstrGrdBkWgtDetailId = gbr.InstrGrdBkWgtDetailId
        JOIN    arGrdComponentTypes gct ON gct.GrdComponentTypeId = gbwr.GrdComponentTypeId
        JOIN    syResources r ON r.ResourceID = gct.SysComponentTypeId
        WHERE   ClsSectionId = @ClsSectionId
                AND StuEnrollId = @StuEnrollId
                AND gbr.InstrGrdBkWgtDetailId = @InstrGrdBkWgtDetailId
                AND @ResNum = CASE @ResNum
                                WHEN 0 THEN @ResNum
                                ELSE ResNum
                              END;	


        IF @Score <> @ExistingScore
            SET @IsScoreChanged = 1;			
	    
  
	
        UPDATE  arGrdBkResults
        SET     Score = @Score
               ,Comments = @Comments
               ,ModUser = @ModUser
               ,ModDate = GETDATE()
               ,IsCompGraded = @IsCompGraded
			   ,DateCompleted = @DateCompleted
        WHERE   ClsSectionId = @ClsSectionId
                AND StuEnrollId = @StuEnrollId
                AND InstrGrdBkWgtDetailId = @InstrGrdBkWgtDetailId
                AND @ResNum = CASE @ResNum
                                WHEN 0 THEN @ResNum
                                ELSE ResNum
                              END;								

        IF @IsScoreChanged = 1
            AND @ResourceComp <> 612 --Dictation/Speed Test
            UPDATE  arResults
            SET     IsCourseCompleted = 0
                   ,Score = NULL
                   ,GrdSysDetailId = NULL
            WHERE   TestId = @ClsSectionId
                    AND StuEnrollId = @StuEnrollId;
		

    END; 



GO
