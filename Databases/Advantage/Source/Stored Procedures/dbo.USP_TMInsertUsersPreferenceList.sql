SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[USP_TMInsertUsersPreferenceList]
    (
     @UserID AS VARCHAR(8000)
    ,@OtherUsersId AS VARCHAR(8000)
    ,@DefaultView AS VARCHAR(50) = NULL
    )
AS
    BEGIN
        DECLARE @OtherUserId UNIQUEIDENTIFIER;
        DECLARE @ExistingDefaultView VARCHAR(50);
        SET @ExistingDefaultView = NULL;
    
        IF EXISTS ( SELECT DISTINCT
                            DefaultView
                    FROM    tmUsersTaskDefaultView
                    WHERE   UserID = @UserID )
            BEGIN
                SET @ExistingDefaultView = (
                                             SELECT DISTINCT
                                                    DefaultView
                                             FROM   tmUsersTaskDefaultView
                                             WHERE  UserID = @UserID
                                           );
            END;
    
        IF EXISTS ( SELECT  *
                    FROM    tmUsersTaskDefaultView
                    WHERE   UserID = @UserID )
            BEGIN
                DELETE  FROM tmUsersTaskDefaultView
                WHERE   UserID = @UserID;
            END;
   
        IF @DefaultView = NULL
            BEGIN
                SET @DefaultView = @ExistingDefaultView;
            END;
   
   
   
        DECLARE c1 CURSOR READ_ONLY
        FOR
            (
              SELECT    strval
              FROM      dbo.SPLIT(@OtherUsersId)
            );
  
        OPEN c1; 
  
        FETCH NEXT FROM c1 
        INTO @OtherUserId; 
  
        WHILE @@FETCH_STATUS = 0
            BEGIN 
                INSERT  INTO tmUsersTaskDefaultView
                        (
                         UserTaskDefViewId
                        ,UserID
                        ,OthersID
                        ,DefaultView
                        ,ModDate
                        )
                VALUES  (
                         NEWID()
                        ,@UserID
                        ,@OtherUserId
                        ,@DefaultView
                        ,GETDATE()
                        );
                FETCH NEXT FROM c1 
            INTO @OtherUserId; 
  
            END; 
        CLOSE c1; 
        DEALLOCATE c1; 
         
    END;
GO
