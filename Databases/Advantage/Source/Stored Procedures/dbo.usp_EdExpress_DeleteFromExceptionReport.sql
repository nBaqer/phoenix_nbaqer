SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[usp_EdExpress_DeleteFromExceptionReport]
    @MsgType NVARCHAR(50)
   ,@FileName NVARCHAR(250)
   ,@ExceptionGUI NVARCHAR(50)
AS
    IF @MsgType = 'PELL'
        BEGIN
            DELETE  FROM syEDExpressExceptionReportPell_ACG_SMART_Teach_StudentTable
            WHERE   FileName = @FileName
                    AND ExceptionGUID <> @ExceptionGUI;
            DELETE  FROM syEDExpressExceptionReportPell_ACG_SMART_Teach_DisbursementTable
            WHERE   FileName = @FileName
                    AND ExceptionGUID <> @ExceptionGUI;
        END;
    ELSE
        BEGIN
            DELETE  FROM syEDExpressExceptionReportDirectLoan_StudentTable
            WHERE   FileName = @FileName
                    AND ExceptionGUID <> @ExceptionGUI;
            DELETE  FROM syEDExpressExceptionReportDirectLoan_DisbursementTable
            WHERE   FileName = @FileName
                    AND ExceptionGUID <> @ExceptionGUI;
        END;





GO
