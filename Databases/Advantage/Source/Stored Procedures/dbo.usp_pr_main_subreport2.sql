SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_pr_main_subreport2]
    @StuEnrollId VARCHAR(50)
   ,@StartDate DATETIME = NULL
   ,@StartDateModifier VARCHAR(10) = NULL
   ,@TermId VARCHAR(4000) = NULL
AS
    SELECT  *
           ,ROW_NUMBER() OVER ( ORDER BY PrgVerDescrip, TermStartDate, TermEndDate, TermDescription ) AS rownumber
    FROM    (
              SELECT DISTINCT
                        2 AS Tag
                       ,1 AS Parent
                       ,PV.PrgVerId
                       ,PV.PrgVerDescrip
                       ,NULL AS ProgramCredits
                       ,CS.TermId AS TermId
                       ,T.TermDescrip AS TermDescription
                       ,T.StartDate AS TermStartDate
                       ,T.EndDate AS TermEndDate
                       ,NULL AS CourseId
                       ,NULL AS CourseDescription
                       ,NULL AS CourseCodeDescription
                       ,NULL AS CourseCredits
                       ,(
                          SELECT    SUM(FACreditsEarned)
                          FROM      syCreditSummary
                          WHERE     StuEnrollId = SE.StuEnrollId
                                    AND TermId = T.TermId
                        ) AS CourseFinAidCredits
                       ,NULL AS CoursePassingGrade
                       ,NULL AS CourseScore
                       ,NULL AS GradeBook_ResultId
                       ,NULL AS GradeBookDescription
                       ,NULL AS GradeBookScore
                       ,NULL AS GradeBookPostDate
                       ,NULL AS GradeBookPassingGrade
                       ,NULL AS GradeBookWeight
                       ,NULL AS GradeBookRequired
                       ,NULL AS GradeBookMustPass
                       ,NULL AS GradeBookSysComponentTypeId
                       ,NULL AS GradeBookHoursRequired
                       ,NULL AS GradeBookHoursCompleted
                       ,SE.StuEnrollId
                       ,NULL AS MinResult
                       ,NULL AS GradeComponentDescription
                       , -- Student data  
                        NULL AS CreditsAttempted
                       ,NULL AS CreditsEarned
                       ,NULL AS Completed
                       ,NULL AS CurrentScore
                       ,NULL AS CurrentGrade
                       ,NULL AS FinalScore
                       ,NULL AS FinalGrade
                       ,( CASE WHEN (
                                      SELECT    SUM(Count_WeightedAverage_Credits)
                                      FROM      syCreditSummary
                                      WHERE     StuEnrollId = SE.StuEnrollId
                                                AND TermId = T.TermId
                                    ) > 0 THEN (
                                                 SELECT SUM(Product_WeightedAverage_Credits_GPA) / SUM(Count_WeightedAverage_Credits)
                                                 FROM   syCreditSummary
                                                 WHERE  StuEnrollId = SE.StuEnrollId
                                                        AND TermId = T.TermId
                                               )
                               ELSE 0
                          END ) AS WeightedAverage_GPA
                       ,( CASE WHEN (
                                      SELECT    SUM(Count_SimpleAverage_Credits)
                                      FROM      syCreditSummary
                                      WHERE     StuEnrollId = SE.StuEnrollId
                                                AND TermId = T.TermId
                                    ) > 0 THEN (
                                                 SELECT SUM(Product_SimpleAverage_Credits_GPA) / SUM(Count_SimpleAverage_Credits)
                                                 FROM   syCreditSummary
                                                 WHERE  StuEnrollId = SE.StuEnrollId
                                                        AND TermId = T.TermId
                                               )
                               ELSE 0
                          END ) AS SimpleAverage_GPA
                       ,NULL AS WeightedAverage_CumGPA
                       ,NULL AS SimpleAverage_CumGPA
                       ,C.CampusId
                       ,C.CampDescrip
                       ,
						--ROW_NUMBER() OVER (Order By PV.PrgVerDescrip,T.StartDate,T.EndDate,T.TermDescrip) as rownumber,
                        S.FirstName AS FirstName
                       ,S.LastName AS LastName
                       ,S.MiddleName
                       ,SCS.TermGPA_Simple
                       ,SCS.TermGPA_Weighted
                       ,SCS.Term_CreditsAttempted
                       ,SCS.Term_CreditsEarned
                       ,(
                          SELECT TOP 1
                                    Average
                          FROM      syCreditSummary
                          WHERE     StuEnrollId = SE.StuEnrollId
                                    AND TermId = T.TermId
                        ) AS termAverage
                       ,CASE WHEN P.ACId = 5 THEN 'True'
                             ELSE 'False'
                        END AS ClockHourProgram
              FROM      arClassSections CS
              INNER JOIN arResults GBR ON CS.ClsSectionId = GBR.TestId
              INNER JOIN arStuEnrollments SE ON GBR.StuEnrollId = SE.StuEnrollId
              INNER JOIN (
                           SELECT   StudentId
                                   ,FirstName
                                   ,LastName
                                   ,MiddleName
                           FROM     arStudent
                         ) S ON S.StudentId = SE.StudentId
              INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
              INNER JOIN arPrograms P ON PV.ProgId = P.ProgId
              INNER JOIN arTerm T ON CS.TermId = T.TermId
              INNER JOIN syCampuses C ON SE.CampusId = C.CampusId
              LEFT OUTER JOIN (
                                SELECT  StuEnrollId
                                       ,TermId
                                       ,TermGPA_Simple
                                       ,TermGPA_Weighted
                                       ,SUM(CreditsEarned) AS Term_CreditsEarned
                                       ,SUM(CreditsAttempted) AS Term_CreditsAttempted
                                FROM    syCreditSummary
                                GROUP BY StuEnrollId
                                       ,TermId
                                       ,TermGPA_Simple
                                       ,TermGPA_Weighted
                              ) SCS ON SE.StuEnrollId = SCS.StuEnrollId
                                       AND T.TermId = SCS.TermId
              WHERE     SE.StuEnrollId = @StuEnrollId
                        AND (
                              @StartDate IS NULL
                              OR @StartDateModifier IS NULL
                              OR (
                                   (
                                     ( @StartDateModifier <> '=' )
                                     OR ( T.StartDate = @StartDate )
                                   )
                                   AND (
                                         ( @StartDateModifier <> '>' )
                                         OR ( T.StartDate > @StartDate )
                                       )
                                   AND (
                                         ( @StartDateModifier <> '<' )
                                         OR ( T.StartDate < @StartDate )
                                       )
                                   AND (
                                         ( @StartDateModifier <> '>=' )
                                         OR ( T.StartDate >= @StartDate )
                                       )
                                   AND (
                                         ( @StartDateModifier <> '<=' )
                                         OR ( T.StartDate <= @StartDate )
                                       )
                                 )
                            )
                        AND (
                              @TermId IS NULL
                              OR T.TermId IN ( SELECT   Val
                                               FROM     MultipleValuesForReportParameters(@TermId,',',1) )
                            )
              UNION
              SELECT DISTINCT
                        2
                       ,1
                       ,PV.PrgVerId
                       ,PV.PrgVerDescrip
                       ,NULL
                       ,GBR.TermId
                       ,T.TermDescrip
                       ,T.StartDate
                       ,T.EndDate
                       ,NULL
                       ,NULL
                       ,NULL
                       ,NULL
                       ,(
                          SELECT    SUM(FACreditsEarned)
                          FROM      syCreditSummary
                          WHERE     StuEnrollId = SE.StuEnrollId
                                    AND TermId = T.TermId
                        ) AS CourseFinAidCredits
                       ,NULL
                       ,NULL
                       ,NULL
                       ,NULL
                       ,NULL
                       ,NULL
                       ,NULL
                       ,NULL
                       ,NULL
                       ,NULL
                       ,NULL
                       ,NULL
                       ,NULL
                       ,SE.StuEnrollId
                       ,NULL AS MinResult
                       ,NULL AS GradeComponentDescription -- Student data    
                       ,NULL AS CreditsAttempted
                       ,NULL AS CreditsEarned
                       ,NULL AS Completed
                       ,NULL AS CurrentScore
                       ,NULL AS CurrentGrade
                       ,NULL AS FinalScore
                       ,NULL AS FinalGrade
                       ,( CASE WHEN (
                                      SELECT    SUM(Count_WeightedAverage_Credits)
                                      FROM      syCreditSummary
                                      WHERE     StuEnrollId = SE.StuEnrollId
                                                AND TermId = T.TermId
                                    ) > 0 THEN (
                                                 SELECT SUM(Product_WeightedAverage_Credits_GPA) / SUM(Count_WeightedAverage_Credits)
                                                 FROM   syCreditSummary
                                                 WHERE  StuEnrollId = SE.StuEnrollId
                                                        AND TermId = T.TermId
                                               )
                               ELSE 0
                          END ) AS WeightedAverage_GPA
                       ,( CASE WHEN (
                                      SELECT    SUM(Count_SimpleAverage_Credits)
                                      FROM      syCreditSummary
                                      WHERE     StuEnrollId = SE.StuEnrollId
                                                AND TermId = T.TermId
                                    ) > 0 THEN (
                                                 SELECT SUM(Product_SimpleAverage_Credits_GPA) / SUM(Count_SimpleAverage_Credits)
                                                 FROM   syCreditSummary
                                                 WHERE  StuEnrollId = SE.StuEnrollId
                                                        AND TermId = T.TermId
                                               )
                               ELSE 0
                          END ) AS SimpleAverage_GPA
                       ,NULL
                       ,NULL
                       ,C.CampusId
                       ,C.CampDescrip
                       ,
						--ROW_NUMBER() OVER (Order By PV.PrgVerDescrip,T.StartDate,T.EndDate,T.TermDescrip) as rownumber,
                        S.FirstName AS FirstName
                       ,S.LastName AS LastName
                       ,S.MiddleName
                       ,SCS.TermGPA_Simple
                       ,SCS.TermGPA_Weighted
                       ,SCS.Term_CreditsAttempted
                       ,SCS.Term_CreditsEarned
                       ,(
                          SELECT TOP 1
                                    Average
                          FROM      syCreditSummary
                          WHERE     StuEnrollId = SE.StuEnrollId
                                    AND TermId = T.TermId
                        ) AS termAverage
                       ,CASE WHEN P.ACId = 5 THEN 'True'
                             ELSE 'False'
                        END AS ClockHourProgram
              FROM      arTransferGrades GBR
              INNER JOIN arStuEnrollments SE ON GBR.StuEnrollId = SE.StuEnrollId
              INNER JOIN (
                           SELECT   StudentId
                                   ,FirstName
                                   ,LastName
                                   ,MiddleName
                           FROM     arStudent
                         ) S ON S.StudentId = SE.StudentId
              INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
              INNER JOIN arPrograms P ON PV.ProgId = P.ProgId
              INNER JOIN arTerm T ON GBR.TermId = T.TermId
              INNER JOIN syCampuses C ON SE.CampusId = C.CampusId
              LEFT OUTER JOIN (
                                SELECT  StuEnrollId
                                       ,TermId
                                       ,TermGPA_Simple
                                       ,TermGPA_Weighted
                                       ,SUM(CreditsEarned) AS Term_CreditsEarned
                                       ,SUM(CreditsAttempted) AS Term_CreditsAttempted
                                FROM    syCreditSummary
                                GROUP BY StuEnrollId
                                       ,TermId
                                       ,TermGPA_Simple
                                       ,TermGPA_Weighted
                              ) SCS ON SE.StuEnrollId = SCS.StuEnrollId
                                       AND T.TermId = SCS.TermId
              WHERE     SE.StuEnrollId = @StuEnrollId
                        AND (
                              @StartDate IS NULL
                              OR @StartDateModifier IS NULL
                              OR (
                                   (
                                     ( @StartDateModifier <> '=' )
                                     OR ( T.StartDate = @StartDate )
                                   )
                                   AND (
                                         ( @StartDateModifier <> '>' )
                                         OR ( T.StartDate > @StartDate )
                                       )
                                   AND (
                                         ( @StartDateModifier <> '<' )
                                         OR ( T.StartDate < @StartDate )
                                       )
                                   AND (
                                         ( @StartDateModifier <> '>=' )
                                         OR ( T.StartDate >= @StartDate )
                                       )
                                   AND (
                                         ( @StartDateModifier <> '<=' )
                                         OR ( T.StartDate <= @StartDate )
                                       )
                                 )
                            )
                        AND (
                              @TermId IS NULL
                              OR T.TermId IN ( SELECT   Val
                                               FROM     MultipleValuesForReportParameters(@TermId,',',1) )
                            )
            ) dt
    ORDER BY PrgVerDescrip
           ,TermStartDate
           ,TermEndDate
           ,TermDescription;

GO
