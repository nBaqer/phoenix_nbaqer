SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[USP_ScheduleConflicts_DuringRegistrationMan]
    @CurrentClassSectionId VARCHAR(8000)
AS
    BEGIN
	
        DECLARE @ClassesWithOverlappingDays TABLE
            (
             StartDate DATETIME
            ,EndDate DATETIME
            ,WorkDaysDescrip VARCHAR(50)
            ,StartTime VARCHAR(50)
            ,EndTime VARCHAR(50)
            ,ViewOrder INT
            ,ClsSection VARCHAR(50)
            ,CourseDescrip VARCHAR(50)
            ,ClsSectionId VARCHAR(50)
            ,CourseId VARCHAR(50)
            );
											
        DECLARE @ClassesWithOverlappingTimeInterval TABLE
            (
             StartDate DATETIME
            ,EndDate DATETIME
            ,WorkDaysDescrip VARCHAR(50)
            ,StartTime VARCHAR(50)
            ,EndTime VARCHAR(50)
            ,ViewOrder INT
            ,ClsSection VARCHAR(50)
            ,CourseDescrip VARCHAR(50)
            ,ClsSectionId VARCHAR(50)
            ,CourseId VARCHAR(50)
            );


		
        INSERT  INTO @ClassesWithOverlappingDays
                SELECT DISTINCT
                        A1.StartDate
                       ,A1.EndDate
                       ,A1.WorkDaysDescrip
                       ,A1.StartTime
                       ,A1.EndTime
                       ,A1.ViewOrder
                       ,A1.ClsSection
                       ,A1.Descrip AS CourseDescrip
                       ,A1.ClsSectionId
                       ,A1.ReqId
                FROM    (
                          SELECT DISTINCT
                                    t3.ClsSectMeetingId
                                   ,t3.StartDate
                                   ,t3.EndDate
                                   ,t4.Descrip
                                   ,t2.ClsSection
                                   ,t2.TermId
                                   ,(
                                      SELECT    CONVERT(VARCHAR,TimeIntervalDescrip,108)
                                      FROM      dbo.cmTimeInterval
                                      WHERE     TimeIntervalId = t5.StartTimeId
                                    ) AS StartTime
                                   ,(
                                      SELECT    CONVERT(VARCHAR,TimeIntervalDescrip,108)
                                      FROM      dbo.cmTimeInterval
                                      WHERE     TimeIntervalId = t5.EndTimeId
                                    ) AS EndTime
                                   ,t7.WorkDaysDescrip
                                   ,t7.ViewOrder
                                   ,t2.ClsSectionId
                                   ,t4.ReqId
                          FROM      arClassSections t2
                                   ,arClsSectMeetings t3
                                   ,arReqs t4
                                   ,syPeriods t5
                                   ,dbo.syPeriodsWorkDays t6
                                   ,plWorkDays t7
                          WHERE     t2.ClsSectionId = t3.ClsSectionId
                                    AND t2.ReqId = t4.ReqId
                                    AND t3.PeriodId = t5.PeriodId
                                    AND t5.PeriodId = t6.PeriodId
                                    AND t6.WorkDayId = t7.WorkDaysId
                                    AND ( t2.ClsSectionId IN ( SELECT   Val
                                                               FROM     MultipleValuesForReportParameters(@CurrentClassSectionId,',',1) ) )
                        ) A1
		--WHERE A1.WorkDaysDescrip=A2.WorkDaysDescrip
                ORDER BY A1.ViewOrder
                       ,A1.ClsSection
                       ,A1.StartDate
                       ,A1.EndDate
                       ,A1.StartTime
                       ,A1.EndTime;
	
	
	--SELECT * FROM @ClassesWithOverlappingDays

        INSERT  INTO @ClassesWithOverlappingTimeInterval
                SELECT  A2.*
                FROM    (
                          SELECT DISTINCT
                                    t3.ClsSectMeetingId
                                   ,t3.StartDate
                                   ,t3.EndDate
                                   ,t4.Descrip
                                   ,t2.ClsSection
                                   ,t2.TermId
                                   ,(
                                      SELECT    CONVERT(VARCHAR,TimeIntervalDescrip,108)
                                      FROM      dbo.cmTimeInterval
                                      WHERE     TimeIntervalId = t5.StartTimeId
                                    ) AS StartTime
                                   ,(
                                      SELECT    CONVERT(VARCHAR,TimeIntervalDescrip,108)
                                      FROM      dbo.cmTimeInterval
                                      WHERE     TimeIntervalId = t5.EndTimeId
                                    ) AS EndTime
                                   ,t7.WorkDaysDescrip
                                   ,t7.ViewOrder
                          FROM      arClassSections t2
                                   ,arClsSectMeetings t3
                                   ,arReqs t4
                                   ,syPeriods t5
                                   ,dbo.syPeriodsWorkDays t6
                                   ,plWorkDays t7
                          WHERE     t2.ClsSectionId = t3.ClsSectionId
                                    AND t2.ReqId = t4.ReqId
                                    AND t3.PeriodId = t5.PeriodId
                                    AND t5.PeriodId = t6.PeriodId
                                    AND t6.WorkDayId = t7.WorkDaysId
                                    AND t2.ClsSectionId IN ( SELECT Val
                                                             FROM   MultipleValuesForReportParameters(@CurrentClassSectionId,',',1) )
                        ) A1
                       ,@ClassesWithOverlappingDays A2
                WHERE   --(A1.StartDate>=A2.StartDate OR A1.StartDate <= A2.EndDate) AND
                        (
                          A1.StartDate <= A2.EndDate
                          AND A1.EndDate >= A2.StartDate
                        )
                        AND ( A1.WorkDaysDescrip = A2.WorkDaysDescrip )
                        AND (
                              A1.StartTime < A2.EndTime
                              AND A1.EndTime > A2.StartTime
                            );
		
		--SELECT * FROM @ClassesWithOverlappingTimeInterval
	

        SELECT DISTINCT
                A3.CourseDescrip
        FROM    (
                  SELECT DISTINCT
                            CourseId
                           ,CourseDescrip
                           ,ClsSectionId
                           ,ClsSection
                           ,CONVERT(VARCHAR,StartDate,101) + '- ' + CONVERT(VARCHAR,EndDate,101) AS ClassPeriod
                           ,WorkDaysDescrip + ' (' + StartTime + ' - ' + EndTime + ')' AS MeetingSchedule
                           ,ViewOrder
                           ,StartDate
                           ,EndDate
                  FROM      @ClassesWithOverlappingTimeInterval
                ) A3
               ,(
                  SELECT DISTINCT
                            CourseId
                           ,CourseDescrip
                           ,ClsSectionId
                           ,ClsSection
                           ,CONVERT(VARCHAR,StartDate,101) + '- ' + CONVERT(VARCHAR,EndDate,101) AS ClassPeriod
                           ,WorkDaysDescrip + ' (' + StartTime + ' - ' + EndTime + ')' AS MeetingSchedule
                           ,ViewOrder
                           ,StartDate
                           ,EndDate
                  FROM      @ClassesWithOverlappingTimeInterval
                ) A4
        WHERE   A3.ClassPeriod = A4.ClassPeriod
                AND A3.MeetingSchedule = A4.MeetingSchedule
                AND A3.ViewOrder = A4.ViewOrder
                AND A3.ClsSectionId <> A4.ClsSectionId
        ORDER BY A3.CourseDescrip;
    END;

GO
