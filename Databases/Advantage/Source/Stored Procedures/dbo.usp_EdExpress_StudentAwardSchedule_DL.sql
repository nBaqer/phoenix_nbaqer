SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_EdExpress_StudentAwardSchedule_DL]
    @StudentAwardID VARCHAR(50)
   ,@StuEnrollmentID VARCHAR(50)
   ,@CampusID VARCHAR(50)
   ,@ExpectedDate DATETIME
   ,@Amount DECIMAL(19,4)
   ,@moduser VARCHAR(50)
   ,@moddate DATETIME
   ,@recid VARCHAR(50)
   ,@DisbursementNumber VARCHAR(50)
   ,@SequenceNumber INT
   ,@RelInd VARCHAR(50)
   ,@GrantType VARCHAR(50)
   ,@AcademicYearId VARCHAR(50)
   ,@IsPostPayment BIT
   ,@IsInSchool BIT
   ,@IsPayOutOfSchool BIT
   ,@IsUseExpDate BIT
   ,@SpecifiedDate DATETIME
   ,@DisbNums VARCHAR(1000)
   ,@PaymentType INT
   ,

    --New Parameter--
    @ParentId VARCHAR(50)
   ,@DbIn VARCHAR(50)
   ,@Filter VARCHAR(50)
   ,@SubmitAmt VARCHAR(50)
   ,@ActionStatue VARCHAR(50)
   ,@OriginalSSN VARCHAR(50)
   ,@FileName VARCHAR(250)
   ,@Show INT
    --New Parameter--
    --New Parameter For DL --
   ,@ProcessMsgType VARCHAR(50) = 'SD'
   ,
    --New Parameter For DL --
    ---------------------------------------------------------------------------------------------------------------------------------------------------
    --Params added by Troy to fix DE6163:QA: Scheduled date and amount are overriden when Import
    --the EDExpress file (when user chooses not to override).
    @IsOverrideAdvDateWithEDExpDate BIT
   ,@IsOverrideAdvAmtWithEDExpAmt BIT
    -----------------------------------------------------------------------------------------------------------------------------------------------------
AS
    BEGIN

        DECLARE @intDupCount INT;
        DECLARE @AwardScheduleId VARCHAR(50);
        DECLARE @TransactionId VARCHAR(50);
        DECLARE @Reference VARCHAR(100);
        DECLARE @TransDesc VARCHAR(100);
        DECLARE @GrantTypeDesc VARCHAR(15);
        DECLARE @PmtDisbRelId VARCHAR(50);
        DECLARE @AmountOld DECIMAL(19,4);
        DECLARE @SeqNumOld INT;
        DECLARE @DisbDateOld DATETIME;
        DECLARE @CountPosted INT;
        DECLARE @CheckTransAmount DECIMAL(19,4);


        IF @GrantType = 'P'
            BEGIN
                SET @GrantTypeDesc = 'DL-PLUS';
            END;
        ELSE
            IF @GrantType = 'S'
                BEGIN
                    SET @GrantTypeDesc = 'DL-SUB';
                END;
            ELSE
                IF @GrantType = 'U'
                    BEGIN
                        SET @GrantTypeDesc = 'DL-UNSUB';
                    END;

        SELECT  @intDupCount = COUNT(*)
        FROM    faStudentAwardSchedule
        WHERE   StudentAwardId = @StudentAwardID
                AND DisbursementNumber = @DisbursementNumber; --and SequenceNumber=@SequenceNumber

    -- Delete Disbursment not in current file ---
    --if @DisbNums<>''
    --begin
    --DECLARE @SQL varchar(1000)
    --set @SQL ='Delete From faStudentAwardSchedule WHERE StudentAwardID='''+@StudentAwardID+''' and DisbursementNumber not in (' + @DisbNums + ')'
    --exec(@SQL)
    --end

        IF @intDupCount >= 1 -- Count for Duplicate
            BEGIN --Begin For Duplicate    
		--Get the existing info for the scheduled disbursement from the database
                SELECT  @AwardScheduleId = AwardScheduleId
                       ,@TransactionId = TransactionId
                       ,@AmountOld = Amount
                       ,@SeqNumOld = ISNULL(SequenceNumber,1)
                       ,@DisbDateOld = ExpectedDate
                FROM    faStudentAwardSchedule
                WHERE   StudentAwardId = @StudentAwardID
                        AND DisbursementNumber = @DisbursementNumber; --and SequenceNumber=@SequenceNumber

        --This code was moved here by Troy on 11/1/2011 to perform the necessary updates in case there are any changes.
        --The code was in the nested sections below but was moved here as we need this to be performed whether or not
        --there are any changes in the sequence number.
        --This SP is used to process both scheduled disbursements (SD) and actual disbursements (AD).
        --The updates should be based off the SD.
                IF @ProcessMsgType = 'SD'
                    BEGIN
                        UPDATE  faStudentAwardSchedule
                        SET     SequenceNumber = @SequenceNumber
                        WHERE   AwardScheduleId = @AwardScheduleId;
                        IF @AmountOld <> @Amount
                            AND @IsOverrideAdvAmtWithEDExpAmt = 1
                            BEGIN
                                UPDATE  faStudentAwardSchedule
                                SET     Amount = @Amount
                                WHERE   AwardScheduleId = @AwardScheduleId;

                            END;
                        IF @DisbDateOld <> @ExpectedDate
                            AND @IsOverrideAdvDateWithEDExpDate = 1
                            BEGIN
                                UPDATE  faStudentAwardSchedule
                                SET     ExpectedDate = @ExpectedDate
                                WHERE   AwardScheduleId = @AwardScheduleId;
                            END; 
                    END;         

        --Check for changes in sequence number

                IF @SeqNumOld <> @SequenceNumber
                    BEGIN
                        IF @SequenceNumber > 1
                            BEGIN
				--The following lines were commented out by Troy on 11/1/2011 as we need the update statement to be done
                --whether or not the amount is 0
                --if @Amount=0
                --begin
                ---------Added on 24 March by kamalesh ---
                --    if @IsPostPayment=1
                --    Begin

                --        if (((@IsInSchool=0 and @IsPayOutOfSchool=1) or (@IsInSchool=1)) AND @ProcessMsgType='AD')
                --        Begin
                --            --Delete from saPmtDisbRel Where AwardScheduleId=@AwardScheduleId
                --            --Delete from saPayments Where TransactionId=@TransactionId
                --            --Delete from saTransactions Where TransactionId=@TransactionId
                --            --Delete from faStudentAwardSchedule Where AwardScheduleID=@AwardScheduleId
                --            Update faStudentAwardSchedule Set Amount=@Amount Where AwardScheduleID=@AwardScheduleId
                --        End
                --    End
                ---------------------------------------------

                --end
                                IF @Amount <> 0
                                    BEGIN
                --The following lines were commented out by Troy on 11/1/2011. We need the update to happen whether or not
                --the amount is zero and whether or not the sequence number has changed.

                -------Changed on 24 March by kamalesh (Brought out of nested if conditions) ---
                --Update faStudentAwardSchedule set Amount=@Amount, ExpectedDate=@ExpectedDate, SequenceNumber=@SequenceNumber Where AwardScheduleId=@AwardScheduleId
                -------------------------------------------

                                        IF (
                                             @IsPostPayment = 1
                                             AND @ProcessMsgType = 'AD'
                                           )
                                            BEGIN
                                                IF @IsInSchool = 1
                                                    OR @IsPayOutOfSchool = 1
                                                    BEGIN

                            --if @IsUseExpDate=1
                            --begin
                                                        SET @Reference = 'EdExpress - ' + CONVERT(VARCHAR,@ExpectedDate,101);
                            --end
                            --else
                            --begin
                            -- set @Reference='EdExpress - ' + CONVERT(varchar, @SpecifiedDate,101) + ' '+ @recid
                            --end

                                                        SELECT  @TransactionId = PDR.TransactionId
                                                        FROM    saPmtDisbRel PDR
                                                               ,saTransactions T
                                                        WHERE   PDR.TransactionId = T.TransactionId
                                                                AND T.Voided = 0
                                                                AND PDR.AwardScheduleId = @AwardScheduleId;
                            --Update saTransactions set Voided=1 where TransactionId=@TransactionId
                                                        IF @RelInd = 'A'
                                                            BEGIN --Rel Ind
                                                                SET @TransactionId = NEWID();
                                                                SET @TransDesc = 'EdExpress - ' + @GrantTypeDesc;

                                                                IF @IsUseExpDate = 1
                                                                    BEGIN
                                                                        INSERT  INTO saTransactions
                                                                                (
                                                                                 TransactionId
                                                                                ,StuEnrollId
                                                                                ,CampusId
                                                                                ,TransDate
                                                                                ,TransAmount
                                                                                ,CreateDate
                                                                                ,TransReference
                                                                                ,TransDescrip
                                                                                ,ModUser
                                                                                ,ModDate
                                                                                ,TransTypeId
                                                                                ,IsPosted
                                                                                ,IsAutomatic
                                                                                ,AcademicYearId
                                                                                )
                                                                        VALUES  (
                                                                                 @TransactionId
                                                                                ,@StuEnrollmentID
                                                                                ,@CampusID
                                                                                ,@ExpectedDate
                                                                                ,-1 * @Amount
                                                                                ,GETDATE()
                                                                                ,@recid
                                                                                ,@TransDesc
                                                                                ,@moduser
                                                                                ,@moddate
                                                                                ,2
                                                                                ,1
                                                                                ,0
                                                                                ,@AcademicYearId
                                                                                );
                                                                    END;
                                                                ELSE
                                                                    BEGIN
                                                                        INSERT  INTO saTransactions
                                                                                (
                                                                                 TransactionId
                                                                                ,StuEnrollId
                                                                                ,CampusId
                                                                                ,TransDate
                                                                                ,TransAmount
                                                                                ,CreateDate
                                                                                ,TransReference
                                                                                ,TransDescrip
                                                                                ,ModUser
                                                                                ,ModDate
                                                                                ,TransTypeId
                                                                                ,IsPosted
                                                                                ,IsAutomatic
                                                                                ,AcademicYearId
                                                                                )
                                                                        VALUES  (
                                                                                 @TransactionId
                                                                                ,@StuEnrollmentID
                                                                                ,@CampusID
                                                                                ,@SpecifiedDate
                                                                                ,-1 * @Amount
                                                                                ,GETDATE()
                                                                                ,@Reference
                                                                                ,@TransDesc
                                                                                ,@moduser
                                                                                ,@moddate
                                                                                ,2
                                                                                ,1
                                                                                ,0
                                                                                ,@AcademicYearId
                                                                                );
                                                                    END;

                                                                SET @PmtDisbRelId = NEWID();
                                                                UPDATE  faStudentAwardSchedule
                                                                SET     TransactionId = @TransactionId
                                                                       ,Reference = Reference + ' ' + @recid
                                                                WHERE   AwardScheduleId = @AwardScheduleId;

                                                                INSERT  INTO saPmtDisbRel
                                                                        (
                                                                         PmtDisbRelId
                                                                        ,TransactionId
                                                                        ,Amount
                                                                        ,StuAwardId
                                                                        ,AwardScheduleId
                                                                        ,ModUser
                                                                        ,ModDate
                                                                        )
                                                                VALUES  (
                                                                         @PmtDisbRelId
                                                                        ,@TransactionId
                                                                        ,@Amount
                                                                        ,@StudentAwardID
                                                                        ,@AwardScheduleId
                                                                        ,@moduser
                                                                        ,@moddate
                                                                        );

                                                                INSERT  INTO saPayments
                                                                        (
                                                                         TransactionId
                                                                        ,CheckNumber
                                                                        ,ScheduledPayment
                                                                        ,PaymentTypeId
                                                                        ,IsDeposited
                                                                        ,ModUser
                                                                        ,ModDate
                                                                        )
                                                                VALUES  (
                                                                         @TransactionId
                                                                        ,NULL
                                                                        ,0
                                                                        ,@PaymentType
                                                                        ,0
                                                                        ,@moduser
                                                                        ,@moddate
                                                                        );

                                                            END; --Rel Ind
                                                    END;

                                            END;
                                    END;
                            END;
                    END;
                ELSE
                    BEGIN -- Begin if Sequence number is same ---
			--The following code was commented out by Troy on 11/1/2011. The code should be executed whether or not
            --the sequence number is the same.
            --Update faStudentAwardSchedule set SequenceNumber=@SequenceNumber Where AwardScheduleID=@AwardScheduleId
            --if @AmountOld<>@Amount and @SeqNumOld=@SequenceNumber and @IsOverrideAdvAmtWithEDExpAmt=1 and @ProcessMsgType='SD'
            --begin
            --    Update faStudentAwardSchedule set Amount=@Amount Where AwardScheduleID=@AwardScheduleId
            --end
            --if @DisbDateOld<>@ExpectedDate and @SeqNumOld=@SequenceNumber and @IsOverrideAdvDateWithEDExpDate=1 and @ProcessMsgType='SD'
            --begin
            --    Update faStudentAwardSchedule set ExpectedDate=@ExpectedDate Where AwardScheduleID=@AwardScheduleId
            --end

                        IF @RelInd = 'A'
                            BEGIN
                                SELECT  @CountPosted = COUNT(*)
                                FROM    saPmtDisbRel PDR
                                       ,saTransactions T
                                WHERE   PDR.TransactionId = T.TransactionId
                                        AND T.Voided = 0
                                        AND AwardScheduleId = @AwardScheduleId;

                                IF @CountPosted <= 0
                                    BEGIN --Begin ( i.e., if Sequence number has not changed and records are not posted to ledger)
                    --if @IsUseExpDate=1
                    -- begin
                                        SET @Reference = 'EdExpress - ' + CONVERT(VARCHAR,@ExpectedDate,101);
                        --end
                        --else
                        --begin
                        -- set @Reference='EdExpress - ' + CONVERT(varchar, @SpecifiedDate,101) + ' '+ @recid
                        --end

                                        IF @IsPostPayment = 1
                                            AND @ProcessMsgType = 'AD'
                                            BEGIN
                                                IF @RelInd = 'A'
                                                    BEGIN --Rel Ind
                                                        SET @TransactionId = NEWID();
                                                        SET @TransDesc = 'EdExpress - ' + @GrantTypeDesc;
                                                        IF @IsInSchool = 1 -- If In School and Post Payment is true
                                                            BEGIN
                                                                IF @IsUseExpDate = 1
                                                                    BEGIN
                                                                        INSERT  INTO saTransactions
                                                                                (
                                                                                 TransactionId
                                                                                ,StuEnrollId
                                                                                ,CampusId
                                                                                ,TransDate
                                                                                ,TransAmount
                                                                                ,CreateDate
                                                                                ,TransReference
                                                                                ,TransDescrip
                                                                                ,ModUser
                                                                                ,ModDate
                                                                                ,TransTypeId
                                                                                ,IsPosted
                                                                                ,IsAutomatic
                                                                                ,AcademicYearId
                                                                                )
                                                                        VALUES  (
                                                                                 @TransactionId
                                                                                ,@StuEnrollmentID
                                                                                ,@CampusID
                                                                                ,@ExpectedDate
                                                                                ,-1 * @Amount
                                                                                ,GETDATE()
                                                                                ,@Reference
                                                                                ,@TransDesc
                                                                                ,@moduser
                                                                                ,@moddate
                                                                                ,2
                                                                                ,1
                                                                                ,0
                                                                                ,@AcademicYearId
                                                                                );
                                                                        SET @PmtDisbRelId = NEWID();
                                                                        UPDATE  faStudentAwardSchedule
                                                                        SET     TransactionId = @TransactionId
                                                                               ,Reference = Reference + ' ' + @recid
                                                                        WHERE   AwardScheduleId = @AwardScheduleId;
                                                                        INSERT  INTO saPmtDisbRel
                                                                                (
                                                                                 PmtDisbRelId
                                                                                ,TransactionId
                                                                                ,Amount
                                                                                ,StuAwardId
                                                                                ,AwardScheduleId
                                                                                ,ModUser
                                                                                ,ModDate
                                                                                )
                                                                        VALUES  (
                                                                                 @PmtDisbRelId
                                                                                ,@TransactionId
                                                                                ,@Amount
                                                                                ,@StudentAwardID
                                                                                ,@AwardScheduleId
                                                                                ,@moduser
                                                                                ,@moddate
                                                                                );
                                                                        INSERT  INTO saPayments
                                                                                (
                                                                                 TransactionId
                                                                                ,CheckNumber
                                                                                ,ScheduledPayment
                                                                                ,PaymentTypeId
                                                                                ,IsDeposited
                                                                                ,ModUser
                                                                                ,ModDate
                                                                                )
                                                                        VALUES  (
                                                                                 @TransactionId
                                                                                ,NULL
                                                                                ,0
                                                                                ,@PaymentType
                                                                                ,0
                                                                                ,@moduser
                                                                                ,@moddate
                                                                                );
                                                                    END;
                                                                ELSE
                                                                    BEGIN
                                                                        INSERT  INTO saTransactions
                                                                                (
                                                                                 TransactionId
                                                                                ,StuEnrollId
                                                                                ,CampusId
                                                                                ,TransDate
                                                                                ,TransAmount
                                                                                ,CreateDate
                                                                                ,TransReference
                                                                                ,TransDescrip
                                                                                ,ModUser
                                                                                ,ModDate
                                                                                ,TransTypeId
                                                                                ,IsPosted
                                                                                ,IsAutomatic
                                                                                ,AcademicYearId
                                                                                )
                                                                        VALUES  (
                                                                                 @TransactionId
                                                                                ,@StuEnrollmentID
                                                                                ,@CampusID
                                                                                ,@SpecifiedDate
                                                                                ,-1 * @Amount
                                                                                ,GETDATE()
                                                                                ,@Reference
                                                                                ,@TransDesc
                                                                                ,@moduser
                                                                                ,@moddate
                                                                                ,2
                                                                                ,1
                                                                                ,0
                                                                                ,@AcademicYearId
                                                                                );
                                                                        SET @PmtDisbRelId = NEWID();
                                                                        UPDATE  faStudentAwardSchedule
                                                                        SET     TransactionId = @TransactionId
                                                                               ,Reference = Reference + ' ' + @recid
                                                                        WHERE   AwardScheduleId = @AwardScheduleId;
                                                                        INSERT  INTO saPmtDisbRel
                                                                                (
                                                                                 PmtDisbRelId
                                                                                ,TransactionId
                                                                                ,Amount
                                                                                ,StuAwardId
                                                                                ,AwardScheduleId
                                                                                ,ModUser
                                                                                ,ModDate
                                                                                )
                                                                        VALUES  (
                                                                                 @PmtDisbRelId
                                                                                ,@TransactionId
                                                                                ,@Amount
                                                                                ,@StudentAwardID
                                                                                ,@AwardScheduleId
                                                                                ,@moduser
                                                                                ,@moddate
                                                                                );
                                                                        INSERT  INTO saPayments
                                                                                (
                                                                                 TransactionId
                                                                                ,CheckNumber
                                                                                ,ScheduledPayment
                                                                                ,PaymentTypeId
                                                                                ,IsDeposited
                                                                                ,ModUser
                                                                                ,ModDate
                                                                                )
                                                                        VALUES  (
                                                                                 @TransactionId
                                                                                ,NULL
                                                                                ,0
                                                                                ,@PaymentType
                                                                                ,0
                                                                                ,@moduser
                                                                                ,@moddate
                                                                                );
                                                                    END;

                                                            END; -- IsPost Is True and IsinSchool=true
                                                        ELSE
                                                            IF @IsInSchool = 0 --Is PostIs true and not in school
                                                                BEGIN
                                                                    IF @IsPayOutOfSchool = 1
                                                                        BEGIN
                                                                            IF @IsUseExpDate = 1
                                                                                BEGIN
                                                                                    INSERT  INTO saTransactions
                                                                                            (
                                                                                             TransactionId
                                                                                            ,StuEnrollId
                                                                                            ,CampusId
                                                                                            ,TransDate
                                                                                            ,TransAmount
                                                                                            ,CreateDate
                                                                                            ,TransReference
                                                                                            ,TransDescrip
                                                                                            ,ModUser
                                                                                            ,ModDate
                                                                                            ,TransTypeId
                                                                                            ,IsPosted
                                                                                            ,IsAutomatic
                                                                                            ,AcademicYearId
                                                                                            )
                                                                                    VALUES  (
                                                                                             @TransactionId
                                                                                            ,@StuEnrollmentID
                                                                                            ,@CampusID
                                                                                            ,@ExpectedDate
                                                                                            ,-1 * @Amount
                                                                                            ,GETDATE()
                                                                                            ,@Reference
                                                                                            ,@TransDesc
                                                                                            ,@moduser
                                                                                            ,@moddate
                                                                                            ,2
                                                                                            ,1
                                                                                            ,0
                                                                                            ,@AcademicYearId
                                                                                            );
                                                                                    SET @PmtDisbRelId = NEWID();
                                                                                    UPDATE  faStudentAwardSchedule
                                                                                    SET     TransactionId = @TransactionId
                                                                                           ,Reference = Reference + ' ' + @recid
                                                                                    WHERE   AwardScheduleId = @AwardScheduleId;
                                                                                    INSERT  INTO saPmtDisbRel
                                                                                            (
                                                                                             PmtDisbRelId
                                                                                            ,TransactionId
                                                                                            ,Amount
                                                                                            ,StuAwardId
                                                                                            ,AwardScheduleId
                                                                                            ,ModUser
                                                                                            ,ModDate
                                                                                            )
                                                                                    VALUES  (
                                                                                             @PmtDisbRelId
                                                                                            ,@TransactionId
                                                                                            ,@Amount
                                                                                            ,@StudentAwardID
                                                                                            ,@AwardScheduleId
                                                                                            ,@moduser
                                                                                            ,@moddate
                                                                                            );
                                                                                    INSERT  INTO saPayments
                                                                                            (
                                                                                             TransactionId
                                                                                            ,CheckNumber
                                                                                            ,ScheduledPayment
                                                                                            ,PaymentTypeId
                                                                                            ,IsDeposited
                                                                                            ,ModUser
                                                                                            ,ModDate
                                                                                            )
                                                                                    VALUES  (
                                                                                             @TransactionId
                                                                                            ,NULL
                                                                                            ,0
                                                                                            ,@PaymentType
                                                                                            ,0
                                                                                            ,@moduser
                                                                                            ,@moddate
                                                                                            );
                                                                                END;
                                                                            ELSE
                                                                                BEGIN
                                                                                    INSERT  INTO saTransactions
                                                                                            (
                                                                                             TransactionId
                                                                                            ,StuEnrollId
                                                                                            ,CampusId
                                                                                            ,TransDate
                                                                                            ,TransAmount
                                                                                            ,CreateDate
                                                                                            ,TransReference
                                                                                            ,TransDescrip
                                                                                            ,ModUser
                                                                                            ,ModDate
                                                                                            ,TransTypeId
                                                                                            ,IsPosted
                                                                                            ,IsAutomatic
                                                                                            ,AcademicYearId
                                                                                            )
                                                                                    VALUES  (
                                                                                             @TransactionId
                                                                                            ,@StuEnrollmentID
                                                                                            ,@CampusID
                                                                                            ,@SpecifiedDate
                                                                                            ,-1 * @Amount
                                                                                            ,GETDATE()
                                                                                            ,@Reference
                                                                                            ,@TransDesc
                                                                                            ,@moduser
                                                                                            ,@moddate
                                                                                            ,2
                                                                                            ,1
                                                                                            ,0
                                                                                            ,@AcademicYearId
                                                                                            );
                                                                                    SET @PmtDisbRelId = NEWID();
                                                                                    UPDATE  faStudentAwardSchedule
                                                                                    SET     TransactionId = @TransactionId
                                                                                           ,Reference = Reference + ' ' + @recid
                                                                                    WHERE   AwardScheduleId = @AwardScheduleId;
                                                                                    INSERT  INTO saPmtDisbRel
                                                                                            (
                                                                                             PmtDisbRelId
                                                                                            ,TransactionId
                                                                                            ,Amount
                                                                                            ,StuAwardId
                                                                                            ,AwardScheduleId
                                                                                            ,ModUser
                                                                                            ,ModDate
                                                                                            )
                                                                                    VALUES  (
                                                                                             @PmtDisbRelId
                                                                                            ,@TransactionId
                                                                                            ,@Amount
                                                                                            ,@StudentAwardID
                                                                                            ,@AwardScheduleId
                                                                                            ,@moduser
                                                                                            ,@moddate
                                                                                            );
                                                                                    INSERT  INTO saPayments
                                                                                            (
                                                                                             TransactionId
                                                                                            ,CheckNumber
                                                                                            ,ScheduledPayment
                                                                                            ,PaymentTypeId
                                                                                            ,IsDeposited
                                                                                            ,ModUser
                                                                                            ,ModDate
                                                                                            )
                                                                                    VALUES  (
                                                                                             @TransactionId
                                                                                            ,NULL
                                                                                            ,0
                                                                                            ,@PaymentType
                                                                                            ,0
                                                                                            ,@moduser
                                                                                            ,@moddate
                                                                                            );
                                                                                END;
                                                                        END;

                                                                END; --Is PostIs true and not in school
                                                    END; -- Rel Ind

                                            END;
                                    END; --End Else ( i.e., if Sequence number has not changed and records are not posted to ledger)

                            END;
                    END;
            END; --End For Duplicate
        ELSE -- Else Case for Not Duplicate
            BEGIN -- Begin Else Case for Not Duplicate
                SET @AwardScheduleId = NEWID();
        --if @IsUseExpDate=1
        --begin
                SET @Reference = 'EdExpress - ' + CONVERT(VARCHAR,@ExpectedDate,101);
        --end
        --else
        --begin
        -- set @Reference='EdExpress - ' + CONVERT(varchar, @SpecifiedDate,101) + ' '+ @recid
        --end
                INSERT  INTO faStudentAwardSchedule
                        (
                         AwardScheduleId
                        ,StudentAwardId
                        ,ExpectedDate
                        ,Amount
                        ,Reference
                        ,ModUser
                        ,ModDate
                        ,recid
                        ,DisbursementNumber
                        ,SequenceNumber
                        )
                VALUES  (
                         @AwardScheduleId
                        ,@StudentAwardID
                        ,@ExpectedDate
                        ,@Amount
                        ,@Reference
                        ,@moduser
                        ,@moddate
                        ,@recid
                        ,@DisbursementNumber
                        ,@SequenceNumber
                        );

                IF @IsPostPayment = 1
                    AND @ProcessMsgType = 'AD'
                    BEGIN
                        IF @RelInd = 'A'
                            BEGIN --Rel Ind
                                SET @TransactionId = NEWID();
                                SET @TransDesc = 'EdExpress - ' + @GrantTypeDesc;
                                IF @IsInSchool = 1 -- If In School and Post Payment is true
                                    BEGIN
                                        IF @IsUseExpDate = 1
                                            BEGIN
                                                INSERT  INTO saTransactions
                                                        (
                                                         TransactionId
                                                        ,StuEnrollId
                                                        ,CampusId
                                                        ,TransDate
                                                        ,TransAmount
                                                        ,CreateDate
                                                        ,TransReference
                                                        ,TransDescrip
                                                        ,ModUser
                                                        ,ModDate
                                                        ,TransTypeId
                                                        ,IsPosted
                                                        ,IsAutomatic
                                                        ,AcademicYearId
                                                        )
                                                VALUES  (
                                                         @TransactionId
                                                        ,@StuEnrollmentID
                                                        ,@CampusID
                                                        ,@ExpectedDate
                                                        ,-1 * @Amount
                                                        ,GETDATE()
                                                        ,@Reference
                                                        ,@TransDesc
                                                        ,@moduser
                                                        ,@moddate
                                                        ,2
                                                        ,1
                                                        ,0
                                                        ,@AcademicYearId
                                                        );
                                                SET @PmtDisbRelId = NEWID();
                                                UPDATE  faStudentAwardSchedule
                                                SET     TransactionId = @TransactionId
                                                       ,Reference = Reference + ' ' + @recid
                                                WHERE   AwardScheduleId = @AwardScheduleId;
                                                INSERT  INTO saPmtDisbRel
                                                        (
                                                         PmtDisbRelId
                                                        ,TransactionId
                                                        ,Amount
                                                        ,StuAwardId
                                                        ,AwardScheduleId
                                                        ,ModUser
                                                        ,ModDate
                                                        )
                                                VALUES  (
                                                         @PmtDisbRelId
                                                        ,@TransactionId
                                                        ,@Amount
                                                        ,@StudentAwardID
                                                        ,@AwardScheduleId
                                                        ,@moduser
                                                        ,@moddate
                                                        );
                                                INSERT  INTO saPayments
                                                        (
                                                         TransactionId
                                                        ,CheckNumber
                                                        ,ScheduledPayment
                                                        ,PaymentTypeId
                                                        ,IsDeposited
                                                        ,ModUser
                                                        ,ModDate
                                                        )
                                                VALUES  (
                                                         @TransactionId
                                                        ,NULL
                                                        ,0
                                                        ,@PaymentType
                                                        ,0
                                                        ,@moduser
                                                        ,@moddate
                                                        );
                                            END;
                                        ELSE
                                            BEGIN
                                                INSERT  INTO saTransactions
                                                        (
                                                         TransactionId
                                                        ,StuEnrollId
                                                        ,CampusId
                                                        ,TransDate
                                                        ,TransAmount
                                                        ,CreateDate
                                                        ,TransReference
                                                        ,TransDescrip
                                                        ,ModUser
                                                        ,ModDate
                                                        ,TransTypeId
                                                        ,IsPosted
                                                        ,IsAutomatic
                                                        ,AcademicYearId
                                                        )
                                                VALUES  (
                                                         @TransactionId
                                                        ,@StuEnrollmentID
                                                        ,@CampusID
                                                        ,@SpecifiedDate
                                                        ,-1 * @Amount
                                                        ,GETDATE()
                                                        ,@Reference
                                                        ,@TransDesc
                                                        ,@moduser
                                                        ,@moddate
                                                        ,2
                                                        ,1
                                                        ,0
                                                        ,@AcademicYearId
                                                        );
                                                SET @PmtDisbRelId = NEWID();
                                                UPDATE  faStudentAwardSchedule
                                                SET     TransactionId = @TransactionId
                                                       ,Reference = Reference + ' ' + @recid
                                                WHERE   AwardScheduleId = @AwardScheduleId;
                                                INSERT  INTO saPmtDisbRel
                                                        (
                                                         PmtDisbRelId
                                                        ,TransactionId
                                                        ,Amount
                                                        ,StuAwardId
                                                        ,AwardScheduleId
                                                        ,ModUser
                                                        ,ModDate
                                                        )
                                                VALUES  (
                                                         @PmtDisbRelId
                                                        ,@TransactionId
                                                        ,@Amount
                                                        ,@StudentAwardID
                                                        ,@AwardScheduleId
                                                        ,@moduser
                                                        ,@moddate
                                                        );
                                                INSERT  INTO saPayments
                                                        (
                                                         TransactionId
                                                        ,CheckNumber
                                                        ,ScheduledPayment
                                                        ,PaymentTypeId
                                                        ,IsDeposited
                                                        ,ModUser
                                                        ,ModDate
                                                        )
                                                VALUES  (
                                                         @TransactionId
                                                        ,NULL
                                                        ,0
                                                        ,@PaymentType
                                                        ,0
                                                        ,@moduser
                                                        ,@moddate
                                                        );
                                            END;

                                    END; -- IsPost Is True and IsinSchool=true
                                ELSE
                                    IF @IsInSchool = 0 --Is PostIs true and not in school
                                        BEGIN
                                            IF @IsPayOutOfSchool = 1
                                                BEGIN
                                                    IF @IsUseExpDate = 1
                                                        BEGIN
                                                            INSERT  INTO saTransactions
                                                                    (
                                                                     TransactionId
                                                                    ,StuEnrollId
                                                                    ,CampusId
                                                                    ,TransDate
                                                                    ,TransAmount
                                                                    ,CreateDate
                                                                    ,TransReference
                                                                    ,TransDescrip
                                                                    ,ModUser
                                                                    ,ModDate
                                                                    ,TransTypeId
                                                                    ,IsPosted
                                                                    ,IsAutomatic
                                                                    ,AcademicYearId
                                                                    )
                                                            VALUES  (
                                                                     @TransactionId
                                                                    ,@StuEnrollmentID
                                                                    ,@CampusID
                                                                    ,@ExpectedDate
                                                                    ,-1 * @Amount
                                                                    ,GETDATE()
                                                                    ,@Reference
                                                                    ,@TransDesc
                                                                    ,@moduser
                                                                    ,@moddate
                                                                    ,2
                                                                    ,1
                                                                    ,0
                                                                    ,@AcademicYearId
                                                                    );
                                                            SET @PmtDisbRelId = NEWID();
                                                            UPDATE  faStudentAwardSchedule
                                                            SET     TransactionId = @TransactionId
                                                                   ,Reference = Reference + ' ' + @recid
                                                            WHERE   AwardScheduleId = @AwardScheduleId;
                                                            INSERT  INTO saPmtDisbRel
                                                                    (
                                                                     PmtDisbRelId
                                                                    ,TransactionId
                                                                    ,Amount
                                                                    ,StuAwardId
                                                                    ,AwardScheduleId
                                                                    ,ModUser
                                                                    ,ModDate
                                                                    )
                                                            VALUES  (
                                                                     @PmtDisbRelId
                                                                    ,@TransactionId
                                                                    ,@Amount
                                                                    ,@StudentAwardID
                                                                    ,@AwardScheduleId
                                                                    ,@moduser
                                                                    ,@moddate
                                                                    );
                                                            INSERT  INTO saPayments
                                                                    (
                                                                     TransactionId
                                                                    ,CheckNumber
                                                                    ,ScheduledPayment
                                                                    ,PaymentTypeId
                                                                    ,IsDeposited
                                                                    ,ModUser
                                                                    ,ModDate
                                                                    )
                                                            VALUES  (
                                                                     @TransactionId
                                                                    ,NULL
                                                                    ,0
                                                                    ,@PaymentType
                                                                    ,0
                                                                    ,@moduser
                                                                    ,@moddate
                                                                    );
                                                        END;
                                                    ELSE
                                                        BEGIN
                                                            INSERT  INTO saTransactions
                                                                    (
                                                                     TransactionId
                                                                    ,StuEnrollId
                                                                    ,CampusId
                                                                    ,TransDate
                                                                    ,TransAmount
                                                                    ,CreateDate
                                                                    ,TransReference
                                                                    ,TransDescrip
                                                                    ,ModUser
                                                                    ,ModDate
                                                                    ,TransTypeId
                                                                    ,IsPosted
                                                                    ,IsAutomatic
                                                                    ,AcademicYearId
                                                                    )
                                                            VALUES  (
                                                                     @TransactionId
                                                                    ,@StuEnrollmentID
                                                                    ,@CampusID
                                                                    ,@SpecifiedDate
                                                                    ,-1 * @Amount
                                                                    ,GETDATE()
                                                                    ,@Reference
                                                                    ,@TransDesc
                                                                    ,@moduser
                                                                    ,@moddate
                                                                    ,2
                                                                    ,1
                                                                    ,0
                                                                    ,@AcademicYearId
                                                                    );
                                                            SET @PmtDisbRelId = NEWID();
                                                            UPDATE  faStudentAwardSchedule
                                                            SET     TransactionId = @TransactionId
                                                                   ,Reference = Reference + ' ' + @recid
                                                            WHERE   AwardScheduleId = @AwardScheduleId;
                                                            INSERT  INTO saPmtDisbRel
                                                                    (
                                                                     PmtDisbRelId
                                                                    ,TransactionId
                                                                    ,Amount
                                                                    ,StuAwardId
                                                                    ,AwardScheduleId
                                                                    ,ModUser
                                                                    ,ModDate
                                                                    )
                                                            VALUES  (
                                                                     @PmtDisbRelId
                                                                    ,@TransactionId
                                                                    ,@Amount
                                                                    ,@StudentAwardID
                                                                    ,@AwardScheduleId
                                                                    ,@moduser
                                                                    ,@moddate
                                                                    );
                                                            INSERT  INTO saPayments
                                                                    (
                                                                     TransactionId
                                                                    ,CheckNumber
                                                                    ,ScheduledPayment
                                                                    ,PaymentTypeId
                                                                    ,IsDeposited
                                                                    ,ModUser
                                                                    ,ModDate
                                                                    )
                                                            VALUES  (
                                                                     @TransactionId
                                                                    ,NULL
                                                                    ,0
                                                                    ,@PaymentType
                                                                    ,0
                                                                    ,@moduser
                                                                    ,@moddate
                                                                    );
                                                        END;
                                                END;

                                        END; --Is PostIs true and not in school
                            END; -- Rel Ind
                    END;
            END; -- Begin Else Case for Not Duplicate


        SELECT  @CountPosted = COUNT(*)
               ,@CheckTransAmount = SUM(TR.TransAmount * -1)
        FROM    saTransactions TR
               ,saPmtDisbRel PDR
        WHERE   TR.TransactionId = PDR.TransactionId
                AND TR.Voided = 0
                AND PDR.AwardScheduleId = @AwardScheduleId;
        IF (
             (
               ( @IsPostPayment = 0 )
               OR (
                    @IsPayOutOfSchool = 0
                    AND @IsInSchool = 0
                  )
             )
             AND @ProcessMsgType = 'AD'
           )
            BEGIN
                IF @RelInd = 'A'
                    BEGIN
                        IF ( @CountPosted = 0 )
                            OR ( ISNULL(@CheckTransAmount,0) <> @Amount )
                            BEGIN
                                IF (
                                     @Show > 0
                                     AND @Amount > 0.00
                                   )
                                    BEGIN
                                        EXEC usp_EdExpress_SaveNotPostedRecords_DisbursmentTable @ParentId,@AwardScheduleId,@DbIn,@Filter,@Amount,@ExpectedDate,
                                            @DisbursementNumber,@RelInd,@SequenceNumber,@SubmitAmt,@ActionStatue,@OriginalSSN,@ModDate,@FileName,'PELL';
                                    END;

                            END;
                    END;
            END;


        SELECT  @AwardScheduleId;
    END;

/****** Object: StoredProcedure [dbo].[usp_EdExpress_PostPendingDisbursement] Script Date: 04/01/2010 14:54:22 ******/
    SET ANSI_NULLS ON;







GO
