SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
----------------------------------------------------------------------------------------------------------------
-- Show and Hide Consecutive Absence Report Starts Here Usp_Advantage_ReportsNavigation_ByModule_GetList
---------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[Usp_Advantage_ReportsNavigation_ByModule_GetList]
    @SchoolEnumerator INT
   ,@campusId UNIQUEIDENTIFIER
----------------------------------------------------------------------------------
-- IPEDS Winter Reports to appear in Menu - Sep 14 2012
--DE8360 QA: Weekly Attendance issue on Manage security page.
--Start Janet 08.27.2012
--DE8306 - QA: Consecutive absences reports being shown for byclass type campuses
----------------------------------------------------------------------------------
AS
    BEGIN  
 -- declare local variables  
        DECLARE @ShowRossOnlyTabs BIT
           ,@SchedulingMethod VARCHAR(50)
           ,@TrackSAPAttendance VARCHAR(50);  
        DECLARE @ShowCollegeOfCourtReporting VARCHAR(5)
           ,@FameESP VARCHAR(5)
           ,@EdExpress VARCHAR(5);   
        DECLARE @GradeBookWeightingLevel VARCHAR(20)
           ,@ShowExternshipTabs VARCHAR(5)
           ,@ShowApportioningCreditBalance VARCHAR(5)
           ,@TrackFASAP VARCHAR(5);
   
   
		--Apportioning
        IF (
             SELECT COUNT(*)
             FROM   syConfigAppSetValues
             WHERE  SettingId = 148
                    AND CampusId = @campusId
           ) >= 1
            BEGIN
                SET @ShowApportioningCreditBalance = (
                                                       SELECT   VALUE
                                                       FROM     dbo.syConfigAppSetValues
                                                       WHERE    SettingId = 148
                                                                AND CampusId = @campusId
                                                     );  
								
            END; 
        ELSE
            BEGIN
                SET @ShowApportioningCreditBalance = (
                                                       SELECT   VALUE
                                                       FROM     dbo.syConfigAppSetValues
                                                       WHERE    SettingId = 148
                                                                AND CampusId IS NULL
                                                     );
            END;
   
 -- Get Values  
        IF (
             SELECT COUNT(*)
             FROM   syConfigAppSetValues
             WHERE  SettingId = 68
                    AND CampusId = @campusId
           ) >= 1
            BEGIN
                SET @ShowRossOnlyTabs = (
                                          SELECT    VALUE
                                          FROM      dbo.syConfigAppSetValues
                                          WHERE     SettingId = 68
                                                    AND CampusId = @campusId
                                        );  
								
            END; 
        ELSE
            BEGIN
                SET @ShowRossOnlyTabs = (
                                          SELECT    VALUE
                                          FROM      dbo.syConfigAppSetValues
                                          WHERE     SettingId = 68
                                                    AND CampusId IS NULL
                                        );
            END;
	
	
    
        IF (
             SELECT COUNT(*)
             FROM   syConfigAppSetValues
             WHERE  SettingId = 65
                    AND CampusId = @campusId
           ) >= 1
            BEGIN
                SET @SchedulingMethod = (
                                          SELECT    VALUE
                                          FROM      dbo.syConfigAppSetValues
                                          WHERE     SettingId = 65
                                                    AND CampusId = @campusId
                                        );  
								
            END; 
        ELSE
            BEGIN
                SET @SchedulingMethod = (
                                          SELECT    VALUE
                                          FROM      dbo.syConfigAppSetValues
                                          WHERE     SettingId = 65
                                                    AND CampusId IS NULL
                                        );
            END;                      
                                
    
                                  
        IF (
             SELECT COUNT(*)
             FROM   syConfigAppSetValues
             WHERE  SettingId = 72
                    AND CampusId = @campusId
           ) >= 1
            BEGIN
                SET @TrackSAPAttendance = (
                                            SELECT  VALUE
                                            FROM    dbo.syConfigAppSetValues
                                            WHERE   SettingId = 72
                                                    AND CampusId = @campusId
                                          );  
								
            END; 
        ELSE
            BEGIN
                SET @TrackSAPAttendance = (
                                            SELECT  VALUE
                                            FROM    dbo.syConfigAppSetValues
                                            WHERE   SettingId = 72
                                                    AND CampusId IS NULL
                                          );
            END; 
       
                                           
        IF (
             SELECT COUNT(*)
             FROM   syConfigAppSetValues
             WHERE  SettingId = 118
                    AND CampusId = @campusId
           ) >= 1
            BEGIN
                SET @ShowCollegeOfCourtReporting = (
                                                     SELECT VALUE
                                                     FROM   dbo.syConfigAppSetValues
                                                     WHERE  SettingId = 118
                                                            AND CampusId = @campusId
                                                   );  
								
            END; 
        ELSE
            BEGIN
                SET @ShowCollegeOfCourtReporting = (
                                                     SELECT VALUE
                                                     FROM   dbo.syConfigAppSetValues
                                                     WHERE  SettingId = 118
                                                            AND CampusId IS NULL
                                                   );
            END; 
     
                       
        IF (
             SELECT COUNT(*)
             FROM   syConfigAppSetValues
             WHERE  SettingId = 37
                    AND CampusId = @campusId
           ) >= 1
            BEGIN
                SET @FameESP = (
                                 SELECT VALUE
                                 FROM   dbo.syConfigAppSetValues
                                 WHERE  SettingId = 37
                                        AND CampusId = @campusId
                               );  
								
            END; 
        ELSE
            BEGIN
                SET @FameESP = (
                                 SELECT VALUE
                                 FROM   dbo.syConfigAppSetValues
                                 WHERE  SettingId = 37
                                        AND CampusId IS NULL
                               );
            END; 
     
                         
        IF (
             SELECT COUNT(*)
             FROM   syConfigAppSetValues
             WHERE  SettingId = 91
                    AND CampusId = @campusId
           ) >= 1
            BEGIN
                SET @EdExpress = (
                                   SELECT   VALUE
                                   FROM     dbo.syConfigAppSetValues
                                   WHERE    SettingId = 91
                                            AND CampusId = @campusId
                                 );  
								
            END; 
        ELSE
            BEGIN
                SET @EdExpress = (
                                   SELECT   VALUE
                                   FROM     dbo.syConfigAppSetValues
                                   WHERE    SettingId = 91
                                            AND CampusId IS NULL
                                 );
            END;
        
                                   
                                       
        IF (
             SELECT COUNT(*)
             FROM   syConfigAppSetValues
             WHERE  SettingId = 43
                    AND CampusId = @campusId
           ) >= 1
            BEGIN
                SET @GradeBookWeightingLevel = (
                                                 SELECT VALUE
                                                 FROM   dbo.syConfigAppSetValues
                                                 WHERE  SettingId = 43
                                                        AND CampusId = @campusId
                                               );  
								
            END; 
        ELSE
            BEGIN
                SET @GradeBookWeightingLevel = (
                                                 SELECT VALUE
                                                 FROM   dbo.syConfigAppSetValues
                                                 WHERE  SettingId = 43
                                                        AND CampusId IS NULL
                                               );
            END;
       
                                  
                                  
        IF (
             SELECT COUNT(*)
             FROM   syConfigAppSetValues
             WHERE  SettingId = 71
                    AND CampusId = @campusId
           ) >= 1
            BEGIN
                SET @ShowExternshipTabs = (
                                            SELECT  VALUE
                                            FROM    dbo.syConfigAppSetValues
                                            WHERE   SettingId = 71
                                                    AND CampusId = @campusId
                                          );  
								
            END; 
        ELSE
            BEGIN
                SET @ShowExternshipTabs = (
                                            SELECT  VALUE
                                            FROM    dbo.syConfigAppSetValues
                                            WHERE   SettingId = 71
                                                    AND CampusId IS NULL
                                          );
            END;
            
        DECLARE @SettingIdentifier INT;
        SET @SettingIdentifier = (
                                   SELECT TOP 1
                                            SettingId
                                   FROM     syConfigAppSettings
                                   WHERE    KeyName = 'TrackFASAP'
                                 );
                    
        IF (
             SELECT COUNT(*)
             FROM   syConfigAppSetValues
             WHERE  SettingId = @SettingIdentifier
                    AND CampusId = @campusId
           ) >= 1
            BEGIN
                SET @TrackFASAP = (
                                    SELECT  VALUE
                                    FROM    dbo.syConfigAppSetValues
                                    WHERE   SettingId = @SettingIdentifier
                                            AND CampusId = @campusId
                                  );  
								
            END; 
        ELSE
            BEGIN
                SET @TrackFASAP = (
                                    SELECT  VALUE
                                    FROM    dbo.syConfigAppSetValues
                                    WHERE   SettingId = @SettingIdentifier
                                            AND CampusId IS NULL
                                  );
            END;
        
  
  
 -- The first column always refers to the Module Resource Id (189 or 26 or .....)  
   
        SELECT  *
        FROM    (
                  SELECT    189 AS ModuleResourceId
                           ,NNChild.ResourceId AS ChildResourceId
                           ,CASE WHEN NNChild.ResourceId = 395 THEN 'Lead Tabs'
                                 ELSE CASE WHEN NNChild.ResourceId = 398 THEN 'Add/View Leads'
                                           ELSE RChild.Resource
                                      END
                            END AS ChildResource
                           ,RChild.ResourceURL AS ChildResourceURL
                           ,CASE WHEN (
                                        NNParent.ResourceId IN ( 689 )
                                        OR NNChild.ResourceId IN ( 710,402,617,734,794 )
                                      ) THEN NULL
                                 ELSE NNParent.ResourceId
                            END AS ParentResourceId
                           ,RParent.Resource AS ParentResource
                           ,CASE WHEN NNChild.ResourceId = 398 THEN 1
                                 ELSE CASE WHEN NNChild.ResourceId = 395 THEN 2
                                           ELSE 3
                                      END
                            END AS FirstSortOrder
                           ,CASE WHEN (
                                        NNChild.ResourceId IN ( 710,734 )
                                        OR NNParent.ResourceId IN ( 710,734 )
                                      ) THEN 1
                                 ELSE 2
                            END AS DisplaySequence
                  FROM      syResources RChild
                  INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
                  INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                  INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                  LEFT OUTER JOIN (
                                    SELECT  *
                                    FROM    syResources
                                    WHERE   ResourceTypeId = 1
                                  ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                  WHERE     RChild.ResourceTypeId IN ( 2,5,8 )
                            AND (
                                  RChild.ChildTypeId IS NULL
                                  OR RChild.ChildTypeId = 5
                                )
                            AND ( RChild.ResourceId NOT IN ( 395 ) )
                            AND (
                                  NNParent.ParentId IN ( SELECT HierarchyId
                                                         FROM   syNavigationNodes
                                                         WHERE  ResourceId = 189 )
                                  OR NNChild.ParentId IN ( SELECT   HierarchyId
                                                           FROM     syNavigationNodes
                                                           WHERE    ResourceId IN ( 710,402,617,734,794 ) )
                                )
                  UNION ALL
                  SELECT    26 AS ModuleResourceId
                           ,ChildResourceId
                           ,ChildResource
                           ,ChildResourceURL
                           ,ParentResourceId
                           ,ParentResource
                           ,(
                              SELECT TOP 1
                                        HierarchyIndex
                              FROM      dbo.syNavigationNodes
                              WHERE     ResourceId = ChildResourceId
                            ) AS FirstSortOrder
                           ,CASE WHEN (
                                        ChildResourceId IN ( 690,691,729,730 )
                                        OR ParentResourceId IN ( 690,691,729,730 )
                                      ) THEN 1
                                 ELSE CASE WHEN (
                                                  ChildResourceId IN ( 727,692,736,678 )
                                                  OR ParentResourceId IN ( 727,736,692,678 )
                                                ) THEN 2
                                           ELSE CASE WHEN (
                                                            ChildResourceId IN ( 409 )
                                                            OR ParentResourceId = 409
                                                          ) THEN 3
                                                     ELSE CASE WHEN (
                                                                      ChildResourceId IN ( 719,720,721,725 )
                                                                      OR ParentResourceId IN ( 719,720,721,725 )
                                                                    ) THEN 4
                                                               ELSE CASE WHEN (
                                                                                ChildResourceId IN ( 722,723,724 )
                                                                                OR ParentResourceId IN ( 722,723,724 )
                                                                              ) THEN 5
                                                                         ELSE CASE WHEN (
                                                                                          ChildResourceId IN ( 789 )
                                                                                          OR ParentResourceId IN ( 789 )
                                                                                        ) THEN 6
                                                                                   ELSE 7
                                                                              END
                                                                    END
                                                          END
                                                END
                                      END
                            END AS DisplaySequence
                  FROM      (
                              SELECT DISTINCT
                                        NNChild.ResourceId AS ChildResourceId
                                       ,CASE WHEN NNChild.ResourceId = 409 THEN 'IPEDS - General Reports'
                                             ELSE RChild.Resource
                                        END AS ChildResource
                                       ,RChild.ResourceURL AS ChildResourceURL
                                       ,CASE WHEN (
                                                    NNParent.ResourceId IN ( 689 )
                                                    OR NNChild.ResourceId IN ( 409,472,474,712,719,720,721,722,723,724,725,736,678,789 )
                                                  ) THEN NULL
                                             ELSE NNParent.ResourceId
                                        END AS ParentResourceId
                                       ,RParent.Resource AS ParentResource
                                       ,RParentModule.Resource AS MODULE  
    --NNParent.ParentId  
                              FROM      syResources RChild
                              INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
                              INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                              INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                              LEFT OUTER JOIN (
                                                SELECT  *
                                                FROM    syResources
                                                WHERE   ResourceTypeId = 1
                                              ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                              WHERE     RChild.ResourceTypeId IN ( 2,5,8 )
                                        AND (
                                              RChild.ChildTypeId IS NULL
                                              OR RChild.ChildTypeId = 5
                                            )  
    --AND (NNParent.ResourceId IN (689,409,711,472,474,712))  
                                        AND ( RChild.ResourceId NOT IN ( 394,472,474,711 ) )
                                        AND (
                                              NNParent.Parentid IN ( SELECT HierarchyId
                                                                     FROM   syNavigationNodes
                                                                     WHERE  ResourceId = 26 )
                                              OR NNChild.ParentId IN (
                                              SELECT    HierarchyId
                                              FROM      syNavigationNodes
                                              WHERE     ResourceId IN ( 472,474,409,719,720,721,722,723,724,725,690,691,692,727,729,730,736,678,789 ) )
                                            )
                                        AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
                            ) t1
                  UNION ALL
                  SELECT    194 AS ModuleResourceId
                           ,ChildResourceId
                           ,ChildResource
                           ,ChildResourceURL
                           ,ParentResourceId
                           ,ParentResource
                           ,(
                              SELECT TOP 1
                                        HierarchyIndex
                              FROM      dbo.syNavigationNodes
                              WHERE     ResourceId = ChildResourceId
                            ) AS FirstSortOrder
                           ,CASE WHEN (
                                        ChildResourceId IN ( 731,732 )
                                        OR ParentResourceId IN ( 731,732 )
                                      ) THEN 1
                                 ELSE 2
                            END AS DisplaySequence
                  FROM      (
                              SELECT DISTINCT
                                        NNChild.ResourceId AS ChildResourceId
                                       ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                             ELSE RChild.Resource
                                        END AS ChildResource
                                       ,RChild.ResourceURL AS ChildResourceURL
                                       ,CASE WHEN NNParent.ResourceId IN ( 194 )
                                                  OR NNChild.ResourceId IN ( 731,732,733,711 ) THEN NULL
                                             ELSE NNParent.ResourceId
                                        END AS ParentResourceId
                                       ,RParent.Resource AS ParentResource
                                       ,RParentModule.Resource AS MODULE  
    --NNParent.ParentId  
                              FROM      syResources RChild
                              INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
                              INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                              INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                              LEFT OUTER JOIN (
                                                SELECT  *
                                                FROM    syResources
                                                WHERE   ResourceTypeId = 1
                                              ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                              WHERE     RChild.ResourceTypeId IN ( 2,5,8 )
                                        AND (
                                              RChild.ChildTypeId IS NULL
                                              OR RChild.ChildTypeId = 5
                                            )
                                        AND (
                                              NNParent.Parentid IN ( SELECT HierarchyId
                                                                     FROM   syNavigationNodes
                                                                     WHERE  ResourceId = 194 )
                                              OR NNChild.ParentId IN ( SELECT   HierarchyId
                                                                       FROM     syNavigationNodes
                                                                       WHERE    ResourceId IN ( 731,732,733,711 ) )
                                            )
                                        AND ( RChild.ResourceId <> 394 )   
    -- The following condition uses Bitwise Operator  
                                        AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
                            ) t2
                  UNION ALL
                  SELECT    300 AS ModuleResourceId
                           ,ChildResourceId
                           ,ChildResource
                           ,ChildResourceURL
                           ,ParentResourceId
                           ,ParentResource
                           ,(
                              SELECT TOP 1
                                        HierarchyIndex
                              FROM      dbo.syNavigationNodes
                              WHERE     ResourceId = ChildResourceId
                            ) AS FirstSortOrder
                           ,1 AS DisplaySequence
                  FROM      (
                              SELECT DISTINCT
                                        NNChild.ResourceId AS ChildResourceId
                                       ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                             ELSE RChild.Resource
                                        END AS ChildResource
                                       ,RChild.ResourceURL AS ChildResourceURL
                                       ,CASE WHEN NNParent.ResourceId = 300
                                                  OR NNChild.ResourceId = 715 THEN NULL
                                             --ELSE NNParent.ResourceId
                                             -- Had to hard code the parentresourceid to 715 for DE7532 on 7/25/2012 B. Shanblatt
                                             ELSE 715
                                        END AS ParentResourceId
                                       ,
                                        --RParent.Resource AS ParentResource ,
                                        -- Had to hard code the parentresource to General Reports for DE7532 on 7/25/2012 B. Shanblatt
                                        'General Reports' AS ParentResource
                                       ,RParentModule.Resource AS MODULE  
    --NNParent.ParentId  
                              FROM      syResources RChild
                              INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
                              INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                              INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                              LEFT OUTER JOIN (
                                                SELECT  *
                                                FROM    syResources
                                                WHERE   ResourceTypeId = 1
                                              ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                              WHERE     RChild.ResourceTypeId IN ( 2,5,8 )
                                        AND (
                                              RChild.ChildTypeId IS NULL
                                              OR RChild.ChildTypeId = 5
                                            )										
                                        -- Added resource 325, 329, 366, 492, 554, 588, 633 for DE7532 on 7/25/2012 B. Shanblatt 
                                        AND ( RChild.ResourceId NOT IN ( 394,325,329,366,492,554,633 ) )
                                        AND (
                                              NNParent.Parentid IN ( SELECT HierarchyId
                                                                     FROM   syNavigationNodes
                                                                     WHERE  ResourceId = 300 )
                                              OR NNChild.ParentId IN ( SELECT   HierarchyId
                                                                       FROM     syNavigationNodes
                                        -- Added resource 691 for DE7532 on 7/25/2012 B. Shanblatt 
                                                                       WHERE    ResourceId IN ( 715,691 ) )
                                            )   
    -- The following condition uses Bitwise Operator  
                                        AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
                            ) t3
                  UNION ALL
                  SELECT    191 AS ModuleResourceId
                           ,ChildResourceId
                           ,ChildResource
                           ,ChildResourceURL
                           ,ParentResourceId
                           ,ParentResource
                           ,(
                              SELECT TOP 1
                                        HierarchyIndex
                              FROM      dbo.syNavigationNodes
                              WHERE     ResourceId = ChildResourceId
                            ) AS FirstSortOrder
                           ,CASE WHEN (
                                        ChildResourceId IN ( 716 )
                                        OR ParentResourceId IN ( 716 )
                                      ) THEN 1
                                 ELSE 2
                            END AS DisplaySequence
                  FROM      (
                              SELECT DISTINCT
                                        NNChild.ResourceId AS ChildResourceId
                                       ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                             ELSE RChild.Resource
                                        END AS ChildResource
                                       ,RChild.ResourceURL AS ChildResourceURL
                                       ,CASE WHEN (
                                                    NNParent.ResourceId = 191
                                                    OR NNChild.ResourceId = 716
                                                  ) THEN NULL
                                             ELSE NNParent.ResourceId
                                        END AS ParentResourceId
                                       ,RParent.Resource AS ParentResource
                                       ,RParentModule.Resource AS MODULE  
    --NNParent.ParentId  
                              FROM      syResources RChild
                              INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
                              INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                              INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                              LEFT OUTER JOIN (
                                                SELECT  *
                                                FROM    syResources
                                                WHERE   ResourceTypeId = 1
                                              ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                              WHERE     RChild.ResourceTypeId IN ( 2,5,8 )
                                        AND (
                                              RChild.ChildTypeId IS NULL
                                              OR RChild.ChildTypeId = 5
                                            )
                                        AND ( RChild.ResourceId <> 394 )
                                        AND (
                                              NNParent.Parentid IN ( SELECT HierarchyId
                                                                     FROM   syNavigationNodes
                                                                     WHERE  ResourceId = 191 )
                                              OR NNChild.ParentId IN ( SELECT   HierarchyId
                                                                       FROM     syNavigationNodes
                                                                       WHERE    ResourceId IN ( 716 ) )
                                            )   
    -- The following condition uses Bitwise Operator  
                                        AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
                            ) t4
                  UNION ALL
                  SELECT    193 AS ModuleResourceId
                           ,ChildResourceId
                           ,ChildResource
                           ,ChildResourceURL
                           ,ParentResourceId
                           ,ParentResource
                           ,(
                              SELECT TOP 1
                                        HierarchyIndex
                              FROM      dbo.syNavigationNodes
                              WHERE     ResourceId = ChildResourceId
                            ) AS FirstSortOrder
                           ,CASE WHEN (
                                        ChildResourceId IN ( 713 )
                                        OR ParentResourceId IN ( 713 )
                                      ) THEN 1
                                 ELSE 2
                            END AS DisplaySequence
                  FROM      (
                              SELECT DISTINCT
                                        NNChild.ResourceId AS ChildResourceId
                                       ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                             ELSE CASE WHEN NNChild.ResourceId = 397 THEN 'Employer Tabs'
                                                       ELSE RChild.Resource
                                                  END
                                        END AS ChildResource
                                       ,RChild.ResourceURL AS ChildResourceURL
                                       ,CASE WHEN (
                                                    NNParent.ResourceId = 193
                                                    OR NNChild.ResourceId IN ( 713,735 )
                                                  ) THEN NULL
                                             ELSE NNParent.ResourceId
                                        END AS ParentResourceId
                                       ,RParent.Resource AS ParentResource
                                       ,RParentModule.Resource AS MODULE  
    --NNParent.ParentId  
                              FROM      syResources RChild
                              INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
                              INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                              INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                              LEFT OUTER JOIN (
                                                SELECT  *
                                                FROM    syResources
                                                WHERE   ResourceTypeId = 1
                                              ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                              WHERE     RChild.ResourceTypeId IN ( 2,5,8 )
                                        AND (
                                              RChild.ChildTypeId IS NULL
                                              OR RChild.ChildTypeId = 5
                                            )
                                        AND ( RChild.ResourceId <> 394 )
                                        AND (
                                              NNParent.Parentid IN ( SELECT HierarchyId
                                                                     FROM   syNavigationNodes
                                                                     WHERE  ResourceId = 193 )
                                              OR NNChild.ParentId IN ( SELECT   HierarchyId
                                                                       FROM     syNavigationNodes
                                                                       WHERE    ResourceId IN ( 713,735 ) )
                                            )   
    -- The following condition uses Bitwise Operator  
                                        AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
                            ) t5
                  UNION ALL
                  SELECT    195 AS ModuleResourceId
                           ,ChildResourceId
                           ,ChildResource
                           ,ChildResourceURL
                           ,ParentResourceId
                           ,ParentResource
                           ,(
                              SELECT TOP 1
                                        HierarchyIndex
                              FROM      dbo.syNavigationNodes
                              WHERE     ResourceId = ChildResourceId
                            ) AS FirstSortOrder
                           ,CASE WHEN (
                                        ChildResourceId IN ( 717 )
                                        OR ParentResourceId IN ( 717 )
                                      ) THEN 1
                                 ELSE 2
                            END AS DisplaySequence
                  FROM      (
                              SELECT DISTINCT
                                        NNChild.ResourceId AS ChildResourceId
                                       ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                             ELSE RChild.Resource
                                        END AS ChildResource
                                       ,RChild.ResourceURL AS ChildResourceURL
                                       ,CASE WHEN (
                                                    NNParent.ResourceId = 195
                                                    OR NNChild.ResourceId IN ( 717 )
                                                  ) THEN NULL
                                             ELSE NNParent.ResourceId
                                        END AS ParentResourceId
                                       ,RParent.Resource AS ParentResource
                                       ,RParentModule.Resource AS MODULE  
    --NNParent.ParentId  
                              FROM      syResources RChild
                              INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
                              INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                              INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                              LEFT OUTER JOIN (
                                                SELECT  *
                                                FROM    syResources
                                                WHERE   ResourceTypeId = 1
                                              ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                              WHERE     RChild.ResourceTypeId IN ( 2,5,8 )
                                        AND (
                                              RChild.ChildTypeId IS NULL
                                              OR RChild.ChildTypeId = 5
                                            )
                                        AND ( RChild.ResourceId <> 394 )
                                        AND (
                                              NNParent.Parentid IN ( SELECT HierarchyId
                                                                     FROM   syNavigationNodes
                                                                     WHERE  ResourceId = 195 )
                                              OR NNChild.ParentId IN ( SELECT   HierarchyId
                                                                       FROM     syNavigationNodes
                                                                       WHERE    ResourceId IN ( 717 ) )
                                            )   
    -- The following condition uses Bitwise Operator  
                                        AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
                            ) t6  
 --UNION ALL  
   
 --SELECT 192 AS ModuleResourceId,ChildResourceId,ChildResource,ChildResourceURL,ParentResourceId,ParentResource,  
 --(SELECT TOP 1 HierarchyIndex FROM dbo.syNavigationNodes WHERE ResourceId=ChildResourceId)  
 -- AS FirstSortOrder,  
 -- CASE WHEN ChildResourceId=709 OR ParentResourceId=709 THEN 1 ELSE 2 END AS DisplaySequence  
 -- FROM   
 -- (  
 --  SELECT Distinct  
 --   NNChild.ResourceId AS ChildResourceId,  
 --   CASE WHEN NNChild.ResourceId=394 THEN 'Student Tabs' ELSE RChild.Resource END AS ChildResource,  
 --   RChild.ResourceURL AS ChildResourceURL,  
 --   CASE WHEN (NNParent.ResourceId=192 OR NNChild.ResourceId IN (688,709)) THEN NULL ELSE NNParent.ResourceId END AS ParentResourceId,  
 --   RParent.Resource AS ParentResource,  
 --   RParentModule.Resource AS MODULE  
 --   --NNParent.ParentId  
 --  FROM   
 --   syResources RChild INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId  
 --   INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId  
 --   INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID  
 --   LEFT OUTER JOIN (SELECT * FROM syResources WHERE ResourceTypeId=1) RParentModule ON RParent.ResourceID = RParentModule.ResourceID  
 --  WHERE  
 --   RChild.ResourceTypeId in (2,5,8) AND (RChild.ChildTypeId IS NULL OR RChild.ChildTypeId=5)  
 --   AND   
 --   (  
 --    -- Get resources(both menus and submenus) directly tied to the module  
 --    NNParent.ResourceId IN (688,709)  
 --    --OR   
 --    -- A Student Tab may be part of AR,Financial Aid, Student Accounts or Placement  
 --    -- So the following condition is neccessary to bring in only items tied to Faculty  
 --    --(NNParent.ResourceId IN (394,396) AND NNParent.Parentid IN (SELECT HierarchyId FROM syNavigationNodes WHERE ResourceId=192))  
 --   )  
 --   AND (RChild.ResourceId <> 394)   
 --   AND NNParent.Parentid IN (SELECT HierarchyId FROM syNavigationNodes WHERE ResourceId=192)  
 --   -- The following condition uses Bitwise Operator  
 --   AND (RChild.UsedIn & @SchoolEnumerator > 0)  
 -- ) t7  
                ) MainQuery
        WHERE   -- Hide resources based on Configuration Settings  
                (
                  -- The following expression means if showross... is set to false, hide   
     -- ResourceIds 541,542,532,534,535,538,543,539  
     -- (Not False) OR (Condition)  
                  (
                    ( @ShowRossOnlyTabs <> 0 )
                    OR ( ChildResourceId NOT IN ( 541,542,532,534,535,538,543,539 ) )
                  )
                  AND  
     -- The following expression means if showross... is set to true, hide   
     -- ResourceIds 142,375,330,476,508,102,107,237  
     -- (Not True) OR (Condition)  
                  (
                    ( @ShowRossOnlyTabs <> 1 )
                    OR ( ChildResourceId NOT IN ( 142,375,330,476,508,102,107,237 ) )
                  )
                  AND  
     ---- The following expression means if SchedulingMethod=regulartraditional, hide   
     ---- ResourceIds 91 and 497  
     ---- (Not True) OR (Condition)  
                  (
                    (
                      NOT LOWER(LTRIM(RTRIM(@SchedulingMethod))) = 'regulartraditional'
                    )
                    OR ( ChildResourceId NOT IN ( 91,497 ) )
                  )
                  AND  
     -- The following expression means if TrackSAPAttendance=byday, hide   
     -- ResourceIds 585,586,589,590,677,678,770,670  
     -- (Not True) OR (Condition)       
     -- Removed 667 and 770 and 585 for DE7532 on 7/25/2012 B. Shanblatt 
     --removed-- 586, DE7532 Time CLock Punch report available for ByDay School
                  (
                    (
                      NOT LOWER(LTRIM(RTRIM(@TrackSAPAttendance))) = 'byday'
                    )
                    OR ( ChildResourceId NOT IN ( 800 ) )
                  )
                  AND  
                     -- DE8306 Sep 24 2012
                  (
                    (
                      NOT LOWER(LTRIM(RTRIM(@TrackSAPAttendance))) = 'byclass'
                    )
                    OR ( ChildResourceId NOT IN ( 633 ) )
                  )
                  AND  
     ---- The following expression means if @ShowCollegeOfCourtReporting is set to false, hide   
     ---- ResourceIds 614,615  
     ---- (Not False) OR (Condition)  
                  (
                    (
                      NOT LOWER(LTRIM(RTRIM(@ShowCollegeOfCourtReporting))) = 'no'
                    )
                    OR ( ChildResourceId NOT IN ( 614,615,729 ) )
                  )
                  AND  
     -- The following expression means if @ShowCollegeOfCourtReporting is set to true, hide   
     -- ResourceIds 497  
     -- (Not True) OR (Condition)  
                  (
                    (
                      NOT LOWER(LTRIM(RTRIM(@ShowCollegeOfCourtReporting))) = 'yes'
                    )
                    OR ( ChildResourceId NOT IN ( 497 ) )
                  )
                  AND  
     -- The following expression means if FAMEESP is set to false, hide   
     -- ResourceIds 517,523, 525  
     -- (Not False) OR (Condition)  
                  (
                    (
                      NOT LOWER(LTRIM(RTRIM(@FameESP))) = 'no'
                    )
                    OR ( ChildResourceId NOT IN ( 517,523,525 ) )
                  )
                  AND  
     ---- The following expression means if EDExpress is set to false, hide   
     ---- ResourceIds 603,604,606,619  
     ---- (Not False) OR (Condition)  
                  (
                    (
                      NOT LOWER(LTRIM(RTRIM(@EdExpress))) = 'no'
                    )
                    OR ( ChildResourceId NOT IN ( 603,604,605,619 ) )
                  )
                  AND
      ---- The following expression means if ShowApportioningCreditBalance is set to false, hide   
     ---- ResourceIds 603,604,606,619  
     ---- (Not False) OR (Condition)  
                  (
                    (
                      NOT LOWER(LTRIM(RTRIM(@ShowApportioningCreditBalance))) = 'true'
                    )
                    OR ( ChildResourceId NOT IN ( 798 ) )
                  )
                  AND
     ---- The following expression means if @GradeBookWeightingLevel is set to courselevel, hide   
     ---- ResourceIds 107,96,222  
     ---- (Not False) OR (Condition)  
                  (
                    (
                      NOT LOWER(LTRIM(RTRIM(@GradeBookWeightingLevel))) = 'courselevel'
                    )
                    OR ( ChildResourceId NOT IN ( 107,96,222 ) )
                  )
                  AND (
                        (
                          NOT LOWER(LTRIM(RTRIM(@TrackFASAP))) = 'false'
                        )
                        OR ( ChildResourceId NOT IN ( 805 ) )
                      )
                  AND (
                        (
                          NOT LOWER(LTRIM(RTRIM(@GradeBookWeightingLevel))) = 'instructorlevel'
                        )
                        OR ( ChildResourceId NOT IN ( 476 ) )
                      )
                  AND (
                        (
                          NOT LOWER(LTRIM(RTRIM(@ShowExternshipTabs))) = 'no'
                        )
                        OR ( ChildResourceId NOT IN ( 543,538 ) )
                      )
                )
        ORDER BY ModuleResourceId
               ,  
 --FirstSortOrder,  
 --ParentResourceId,  
                DisplaySequence
               ,ChildResource;  
    END;




GO
