SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[USP_AR_InstructionType_Delete]
    (
     @InstructionTypeId UNIQUEIDENTIFIER
    )
AS /*----------------------------------------------------------------------------------------------------
	Author          :	Janet Robinson
    
    Create date		:	08/25/2011
    
	Procedure Name	:	[USP_AR_InstructionType_Delete]

	Objective		:	deletes record from table arInstructionType
	
	Parameters		:	Name						Type	Data Type				Required? 	
						=====						====	=========				=========	
						@InstructionTypeId			In		UniqueIDENTIFIER		Required
						
	Output			:			
						
*/-----------------------------------------------------------------------------------------------------

    BEGIN

        DELETE  arInstructionType
        WHERE   InstructionTypeId = @InstructionTypeId;
		 
    END;



GO
