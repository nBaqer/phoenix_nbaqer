SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[USP_NACCAS_EnrollmentCount]
    @PrgIdList VARCHAR(MAX) = NULL
   ,@CampusId VARCHAR(50)
   ,@ReportYear INT
AS
    DECLARE @StartMonth INT = 1
           ,@StartDay INT = 1
           ,@EndMonth INT = 12
           ,@EndDay INT = 31
           ,@FutureStart INT = 7
           ,@CurrentlyAttending INT = 9
           ,@LOA INT = 10
           ,@Suspension INT = 11
           ,@Dropped INT = 12
           ,@Graduated INT = 14
           ,@Transfer INT = 19;
    BEGIN


        DECLARE @Enrollments TABLE
            (
                PrgVerID UNIQUEIDENTIFIER
               ,PrgVerDescrip VARCHAR(50)
               ,ProgramHours DECIMAL(9, 2)
               ,ProgramCredits DECIMAL(18, 2)
               ,SSN VARCHAR(50)
               ,Student VARCHAR(50)
               ,LeadId UNIQUEIDENTIFIER
               ,StuEnrollId UNIQUEIDENTIFIER
               ,EnrollmentID NVARCHAR(50)
               ,Status VARCHAR(80)
               ,SysStatusId INT
               ,StartDate DATETIME
               ,LDA DATETIME
               ,StudentEnrolledAsOf BIT
               ,StudentStartedTrainingIn BIT
               ,StudentStartedTrainingBetween BIT
               ,RN INT
               ,cnt INT
            );

        DECLARE @EnrollmentsWithLastStatus TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,SysStatusId INT
               ,StatusCodeDescription VARCHAR(80)
            );
        INSERT INTO @EnrollmentsWithLastStatus
                    SELECT lastStatusOfYear.StuEnrollId
                          ,lastStatusOfYear.SysStatusId
                          ,lastStatusOfYear.StatusCodeDescrip
                    FROM   (
                           SELECT     StuEnrollId
                                     ,syStatusCodes.SysStatusId
                                     ,StatusCodeDescrip
                                     ,DateOfChange
                                     ,ROW_NUMBER() OVER ( PARTITION BY StuEnrollId
                                                          ORDER BY DateOfChange DESC
                                                        ) rn
                           FROM       dbo.syStudentStatusChanges
                           INNER JOIN dbo.syStatusCodes ON syStatusCodes.StatusCodeId = syStudentStatusChanges.NewStatusId
                           WHERE      DateOfChange <= CONVERT(DATETIME, CONVERT(VARCHAR(50), (( @ReportYear ) * 10000 + 8 * 100 + 31 )), 112)
                           ) lastStatusOfYear
                    WHERE  rn = 1;

        INSERT INTO @Enrollments
                    SELECT *
                          ,ROW_NUMBER() OVER ( PARTITION BY enr.LeadId
                                                           ,enr.SysStatusId
                                               ORDER BY enr.StartDate DESC
                                             ) RN
                          ,COUNT(1) OVER ( PARTITION BY enr.LeadId
                                                       ,enr.SysStatusId
                                         ) AS cnt
                    FROM   (
                           SELECT     Prog.ProgId AS PrgVerID
                                     ,Prog.ProgDescrip AS PrgVerDescrip
                                     ,PV.Hours AS ProgramHours
                                     ,PV.Credits AS ProgramCredits
                                     ,LD.SSN
                                     ,LD.LastName + ', ' + LD.FirstName + ' ' + ISNULL(LD.MiddleName, '') AS Student
                                     ,LD.LeadId
                                     ,SE.StuEnrollId
                                     ,LD.StudentNumber AS EnrollmentID
                                     ,[@EnrollmentsWithLastStatus].StatusCodeDescription AS Status
                                     ,[@EnrollmentsWithLastStatus].SysStatusId AS SysStatusId
                                     ,SE.StartDate AS StartDate
                                     ,dbo.GetLDA(SE.StuEnrollId) AS LDA
                                     ,CASE WHEN SE.StartDate <= CONVERT(
                                                                           DATETIME
                                                                          ,CONVERT(VARCHAR(50), (( @ReportYear - 2 ) * 10000 + @EndMonth * 100 + @EndDay ))
                                                                          ,112
                                                                       )
                                                AND 1 IN (
                                                         SELECT     dbo.sySysStatus.InSchool
                                                         FROM       dbo.syStatusCodes
                                                         INNER JOIN dbo.sySysStatus ON sySysStatus.SysStatusId = syStatusCodes.SysStatusId
                                                         WHERE      dbo.syStatusCodes.StatusCodeId = dbo.UDF_GetEnrollmentStatusIdAtGivenDate(
                                                                                                                                                 SE.StuEnrollId
                                                                                                                                                ,CONVERT(
                                                                                                                                                            DATETIME
                                                                                                                                                           ,CONVERT(
                                                                                                                                                                       VARCHAR(50)
                                                                                                                                                                      ,(( @ReportYear
                                                                                                                                                                          - 1
                                                                                                                                                                        )
                                                                                                                                                                        * 10000
                                                                                                                                                                        + @StartMonth
                                                                                                                                                                        * 100
                                                                                                                                                                        + @StartDay
                                                                                                                                                                       )
                                                                                                                                                                   )
                                                                                                                                                           ,112
                                                                                                                                                        )
                                                                                                                                             )
                                                         ) THEN 1
                                           ELSE 0
                                      END AS StudentEnrolledAsOf
                                     ,CASE WHEN DATEPART(YEAR, SE.StartDate) = @ReportYear - 1 THEN 1
                                           ELSE 0
                                      END AS StudentStartedTrainingIn
                                     ,CASE WHEN SE.StartDate >= CONVERT(DATETIME, CONVERT(VARCHAR(50), (( @ReportYear - 1 ) * 10000 + 9 * 100 + 1 )), 112)
                                                AND SE.StartDate <= CONVERT(DATETIME, CONVERT(VARCHAR(50), (( @ReportYear ) * 10000 + 8 * 100 + 31 )), 112) THEN 1
                                           ELSE 0
                                      END AS StudentStartedTrainingBetween
                           FROM       dbo.arStuEnrollments AS SE
                           INNER JOIN dbo.adLeads AS LD ON LD.LeadId = SE.LeadId
                           INNER JOIN dbo.arPrgVersions AS PV ON SE.PrgVerId = PV.PrgVerId
                           INNER JOIN dbo.arPrograms AS Prog ON Prog.ProgId = PV.ProgId
                           INNER JOIN dbo.syApprovedNACCASProgramVersion NaccasApproved ON NaccasApproved.ProgramVersionId = SE.PrgVerId
                           INNER JOIN dbo.syNaccasSettings NaccasSettings ON NaccasSettings.NaccasSettingId = NaccasApproved.NaccasSettingId
                           INNER JOIN @EnrollmentsWithLastStatus ON [@EnrollmentsWithLastStatus].StuEnrollId = SE.StuEnrollId
                           WHERE      SE.CampusId = @CampusId
                                      AND NaccasSettings.CampusId = @CampusId
                                      --that have a contracted graduation date between January 1 of reporting year minus 1 year and December 31st of reporting year minus 1 year
                                      -- Not a third party contract
                                      AND NOT SE.ThirdPartyContract = 1
                                      AND NaccasApproved.IsApproved = 1
                           ) enr
                    WHERE  enr.StudentEnrolledAsOf = 1
                           OR enr.StudentStartedTrainingIn = 1
                           OR enr.StudentStartedTrainingBetween = 1; --Base Case 

        WITH A
        AS ( SELECT *
             FROM   @Enrollments
             --Is currently attending the school or on LOA or Suspended or is enrolled but have not yet started
             -- Will handle multi NACCAS approved programs by selecting rn = 1
             WHERE  [@Enrollments].SysStatusId IN ( @FutureStart, @CurrentlyAttending, @Suspension, @LOA )
                    AND RN = 1 )
            --Was enrolled in two different NACCAS approved programs in the same school and graduated/licensed/placed from both the programs in the same report period
            ,B
        AS ( SELECT PrgVerID
                   ,PrgVerDescrip
                   ,ProgramHours
                   ,ProgramCredits
                   ,SSN
                   ,Student
                   ,LeadId
                   ,StuEnrollId
                   ,EnrollmentID
                   ,Status
                   ,SysStatusId
                   ,StartDate
                   ,LDA
                   ,StudentEnrolledAsOf
                   ,StudentStartedTrainingIn
                   ,StudentStartedTrainingBetween
                   ,RN
                   ,cnt
             FROM   (
                    SELECT *
                          ,ROW_NUMBER() OVER ( PARTITION BY [@Enrollments].LeadId
                                                           ,[@Enrollments].PrgVerID
                                               ORDER BY [@Enrollments].StartDate DESC
                                             ) AS RN1
                    FROM   @Enrollments
                    WHERE  [@Enrollments].SysStatusId IN ( @Graduated )
                    ) tbl
             WHERE  tbl.SysStatusId IN ( @Graduated )
                    AND tbl.RN1 = 1 )
            ,C
        AS (
           --Has dropped from a program whose length is less than one academic year of 900 hours after being enrolled for more than 15 calendar days
           --Has dropped from a program whose length is equal to or more than one academic year of 900 hours after being enrolled for more than 30 calendar DAYS
           SELECT *
           FROM   @Enrollments
           WHERE  [@Enrollments].SysStatusId IN ( @Dropped )
                  AND (
                      (
                      [@Enrollments].ProgramHours < 900
                      AND DATEDIFF(DAY, [@Enrollments].StartDate, [@Enrollments].LDA) > 15
                      )
                      OR (
                         [@Enrollments].ProgramHours >= 900
                         AND DATEDIFF(DAY, [@Enrollments].StartDate, [@Enrollments].LDA) > 30
                         )
                      ))
            --Was transferred from the school for which the report is generated to another school and both the schools are a branch of the same parent.
            -- Such a student would be included in the report generated for the original school as did not complete
            ,D
        AS ( SELECT     se.PrgVerID
                       ,se.PrgVerDescrip
                       ,se.ProgramHours
                       ,se.ProgramCredits
                       ,se.SSN
                       ,se.Student
                       ,se.LeadId
                       ,se.StuEnrollId
                       ,se.EnrollmentID
                       ,se.Status
                       ,se.SysStatusId
                       ,se.StartDate
                       ,se.LDA
                       ,se.StudentEnrolledAsOf
                       ,se.StudentStartedTrainingIn
                       ,se.StudentStartedTrainingBetween
                       ,se.RN
                       ,se.cnt
             FROM       @Enrollments se
             INNER JOIN dbo.arTrackTransfer tt ON se.StuEnrollId = tt.StuEnrollId
             INNER JOIN dbo.syCampuses srcCmp ON srcCmp.CampusId = tt.SourceCampusId
             INNER JOIN dbo.syCampuses targetCmp ON targetCmp.CampusId = tt.TargetCampusId
             WHERE      se.SysStatusId IN ( @Transfer )
                        AND (
                            srcCmp.IsBranch = 1
                            AND targetCmp.IsBranch = 1
                            )
                        AND ( srcCmp.ParentCampusId = targetCmp.ParentCampusId ))
        --Below the 'Students enrolled as of 12/31/<Reporting Year - 2 Years> & who remained enrolled as of January 1, <Re Year - 1 Year>' column the total number of cells with a value "X" is displayed
        --Below the '<Reporting Year - 1 Year> Year Starts Students who started training in <Reporting Year - 1 Year>' column the total number of cells with a value "X" is displayed
        --Below the 'Students who started training between 9-01-<Reporting Year - 1 Year> & 8-31-<Reporting Year>' column the total number of cells with a value "X" is displayed
        SELECT   result.PrgVerID
                ,result.PrgVerDescrip
                ,result.ProgramHours
                ,result.ProgramCredits
                ,result.SSN
                ,result.Student
                ,result.LeadId
                ,result.StuEnrollId
                ,result.EnrollmentID
                ,result.Status
                ,result.SysStatusId
                ,result.StartDate
                ,result.LDA
                ,CASE WHEN result.StudentEnrolledAsOf = 1 THEN 'true'
                      ELSE 'false'
                 END AS StudentEnrolledAsOf
                ,CASE WHEN result.StudentStartedTrainingIn = 1
                           AND result.StudentStartedTrainingBetween = 0 THEN 'true'
                      ELSE 'false'
                 END AS StudentStartedTrainingIn
                ,CASE WHEN result.StudentStartedTrainingBetween = 1 THEN 'true'
                      ELSE 'false'
                 END AS StudentStartedTrainingBetween
                ,result.RN
                ,result.cnt
        FROM     (
                 SELECT *
                 FROM   A
                 UNION
                 SELECT *
                 FROM   B
                 UNION
                 SELECT *
                 FROM   C
                 UNION
                 SELECT *
                 FROM   D
                 ) AS result
        WHERE    (
                 @PrgIdList IS NULL
                 OR result.PrgVerID IN (
                                       SELECT Val
                                       FROM   MultipleValuesForReportParameters(@PrgIdList, ',', 1)
                                       )
                 )
        ORDER BY result.PrgVerDescrip
                ,result.Student
                ,result.SSN
                ,result.EnrollmentID;
    END;






GO
