SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_CurrentAndPastTermsForAnyUserbyRoles_GetList]
    @TermStartDate DATETIME
   ,@SupervisorId UNIQUEIDENTIFIER
   ,@InstructorId UNIQUEIDENTIFIER
   ,@CampusId UNIQUEIDENTIFIER
   ,@isAcademicAdvisor BIT
   ,@userName VARCHAR(50)
AS
    DECLARE @DictationTestComponent TABLE
        (
         GrdComponentTypeId UNIQUEIDENTIFIER
        );
    INSERT  INTO @DictationTestComponent
            SELECT  GrdComponentTypeId
            FROM    arGrdComponentTypes
            WHERE   SysComponentTypeId = 612;
    IF @userName <> 'sa'
        AND @isAcademicAdvisor = 0
        BEGIN
            SELECT DISTINCT
                    T.TermId
                   ,T.TermDescrip
                   ,T.StartDate
            FROM    arTerm T
                   ,syStatuses ST
                   ,arClassSectionTerms CST
                   ,arClassSections CS
                   ,arBridge_GradeComponentTypes_Courses BGC
                   ,@DictationTestComponent DTC
            WHERE   T.StartDate <= @TermStartDate
                    AND T.StatusId = ST.StatusId
                    AND ST.Status = 'Active'
                    AND T.TermId = CST.TermId
                    AND CST.ClsSectionId = CS.ClsSectionId
                    AND T.TermId = CS.TermId
                    AND CS.ReqId = BGC.ReqId
                    AND BGC.GrdComponentTypeId = DTC.GrdComponentTypeId
                    AND (
                          CS.InstructorId IN ( SELECT   InstructorId
                                               FROM     arInstructorsSupervisors
                                               WHERE    SupervisorId = @SupervisorId )
                          OR CS.InstructorId = @InstructorId
                        )
                    AND (
                          T.CampGrpId IN ( SELECT   CampGrpId
                                           FROM     syCmpGrpCmps
                                           WHERE    CampusId = @CampusId
                                                    AND CampGrpId <> (
                                                                       SELECT   CampGrpId
                                                                       FROM     syCampGrps
                                                                       WHERE    CampGrpDescrip = 'All'
                                                                     ) )
                          OR CampGrpId = (
                                           SELECT   CampGrpId
                                           FROM     syCampGrps
                                           WHERE    CampGrpDescrip = 'All'
                                         )
                        )
            ORDER BY T.StartDate
                   ,T.TermDescrip;
        END;
    ELSE
        BEGIN
            SELECT DISTINCT
                    T.TermId
                   ,T.TermDescrip
                   ,T.StartDate
            FROM    arTerm T
                   ,syStatuses ST
                   ,arClassSectionTerms CST
                   ,arClassSections CS
                   ,arBridge_GradeComponentTypes_Courses BGC
                   ,@DictationTestComponent DTC
            WHERE   T.StartDate <= @TermStartDate
                    AND T.StatusId = ST.StatusId
                    AND ST.Status = 'Active'
                    AND T.TermId = CST.TermId
                    AND CST.ClsSectionId = CS.ClsSectionId
                    AND CS.ReqId = BGC.ReqId
                    AND BGC.GrdComponentTypeId = DTC.GrdComponentTypeId
                    AND (
                          T.CampGrpId IN ( SELECT   CampGrpId
                                           FROM     syCmpGrpCmps
                                           WHERE    CampusId = @CampusId
                                                    AND CampGrpId <> (
                                                                       SELECT   CampGrpId
                                                                       FROM     syCampGrps
                                                                       WHERE    CampGrpDescrip = 'All'
                                                                     ) )
                          OR CampGrpId = (
                                           SELECT   CampGrpId
                                           FROM     syCampGrps
                                           WHERE    CampGrpDescrip = 'All'
                                         )
                        )
            ORDER BY T.StartDate
                   ,T.TermDescrip;
        END;
        



GO
