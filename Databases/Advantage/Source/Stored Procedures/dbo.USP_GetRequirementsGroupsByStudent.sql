SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

 
CREATE PROCEDURE [dbo].[USP_GetRequirementsGroupsByStudent]
    (
     @StudentID UNIQUEIDENTIFIER
    ,@CampusID UNIQUEIDENTIFIER
    ,@ProgID UNIQUEIDENTIFIER = NULL
    ,@ShiftID UNIQUEIDENTIFIER = NULL
    ,@StatusCodeID UNIQUEIDENTIFIER = NULL 
    )
AS
    BEGIN

        DECLARE @tab1 TABLE
            (
             ReqGrpId UNIQUEIDENTIFIER
            ,Descrip VARCHAR(100)
            );
        INSERT  INTO @tab1
                EXEC USP_GetRequirementsGroupsByStudent_Enroll @StudentID,@CampusID,@ProgID,@ShiftID,@StatusCodeID;
        DECLARE @tab2 TABLE
            (
             ReqGrpId UNIQUEIDENTIFIER
            ,Descrip VARCHAR(100)
            );
        INSERT  INTO @tab2
                EXEC USP_GetRequirementsGroupsByStudent_Grad @StudentID,@CampusID,@ProgID,@ShiftID,@StatusCodeID;
        DECLARE @tab3 TABLE
            (
             ReqGrpId UNIQUEIDENTIFIER
            ,Descrip VARCHAR(100)
            );
        INSERT  INTO @tab3
                EXEC USP_GetRequirementsGroupsByStudent_FinAid @StudentID,@CampusID,@ProgID,@ShiftID,@StatusCodeID;

        SELECT  *
        FROM    @tab1
        UNION
        SELECT  *
        FROM    @tab2
        UNION
        SELECT  *
        FROM    @tab3;


    END;







GO
