SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_GetStatesByCampus_List]
    @CampusId UNIQUEIDENTIFIER
AS
    SELECT DISTINCT
            S.StateId
           ,S.StateDescrip
    FROM    syCampuses C
    INNER JOIN syStates S ON C.StateId = S.StateId
    WHERE   C.CampusId = @CampusId;



GO
