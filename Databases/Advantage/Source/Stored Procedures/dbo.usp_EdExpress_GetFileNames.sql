SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[usp_EdExpress_GetFileNames]
AS
    BEGIN
        SELECT DISTINCT
                Filename
        FROM    syEDExpNotPostPell_ACG_SMART_Teach_StudentTable
        ORDER BY FileName;
    END;




GO
