SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[USP_SA_GetStudentTerms_WithoutTerms]
    (
     @StuenrollId UNIQUEIDENTIFIER
    ,@StudentId UNIQUEIDENTIFIER
    ,@CampusId UNIQUEIDENTIFIER
    ,@StartDate DATETIME
    ,@IsSuper BIT
    )
AS /*----------------------------------------------------------------------------------------------------
	Author          :	Bruce Shanblatt
    
    Create date		:	01/31/2011
    
	Procedure Name	:	USP_SA_GetStudentTerms_WithoutTerms

	Objective		:	Get the students terms for payments
	
	Parameters		:	Name			Type	Data Type			Required? 	
						=====			====	=========			=========	
						@stuEnrollId	In		uniqueidentifier	Required
						@studentId      In      uniqueidentifier    Required
						@CampusId       In      uniqueidentifier    Required
						@StartDate      In      date                Required
						@IsSuper        In      bit                 Required	                    
	                     
	Output			:	returns student terms for payments			
						
*/-----------------------------------------------------------------------------------------------------
    BEGIN

--DECLARE @isSuper BIT

        DECLARE @MyEnrollments TABLE
            (
             StuEnrollId UNIQUEIDENTIFIER PRIMARY KEY
            ,PrgVerId UNIQUEIDENTIFIER
            ,CampusId UNIQUEIDENTIFIER
            ,ExpStartDate SMALLDATETIME
            ,StartDate SMALLDATETIME
            ,SysStatusId INT
            ,IsCurrentEnrollment BIT -- 0 = noncurrent enrollment 1 = current enrollment
            );
        INSERT  INTO @MyEnrollments
                SELECT  SE.StuEnrollId
                       ,SE.PrgVerId
                       ,SE.CampusId
                       ,SE.StartDate
                       ,SE.ExpStartDate
                       ,SS.SysStatusId
                       ,IsCurrentEnrollment = CASE WHEN SS.SysStatusId IN ( 7,9,10,11,20,21,22 ) THEN 1
                                                   ELSE 0
                                              END
                FROM    arStuEnrollments AS SE
                INNER JOIN dbo.syStatusCodes AS SC ON SE.StatusCodeId = SC.StatusCodeId
                INNER JOIN dbo.sySysStatus AS SS ON SC.SysStatusId = SS.SysStatusId
                WHERE   SE.StuEnrollId = @StuenrollId;
	
	
	

--DECLARE @MyEnrollments TABLE
--(
-- StuEnrollId UNIQUEIDENTIFIER PRIMARY KEY,  
-- PrgVerId UNIQUEIDENTIFIER,  
-- CampusId UNIQUEIDENTIFIER,  
-- ExpStartDate SMALLDATETIME,  
-- StartDate SMALLDATETIME  
--)

--IF (@isSuper = 0)
--BEGIN
--	-- If user is non-sa	
--	INSERT INTO @MyEnrollments
--	SELECT StuEnrollId , PrgVerId ,CampusId,StartDate ,ExpStartDate 
--	FROM arStuEnrollments AS SE
--	INNER JOIN dbo.syStatusCodes AS SC ON SE.StatusCodeId = SC.StatusCodeId
--	INNER JOIN dbo.sySysStatus AS SS ON SC.SysStatusId = SS.SysStatusId
--	WHERE SS.SysStatusId in(7,9,10,11,20,21,22) 
--	AND SE.StuEnrollId = @stuEnrollId
--END
--ELSE
--BEGIN
--	-- If user is sa or System Administrator	
--	INSERT INTO @MyEnrollments
--	SELECT StuEnrollId , PrgVerId ,CampusId,StartDate ,ExpStartDate 
--	FROM arStuEnrollments AS SE
--	INNER JOIN dbo.syStatusCodes AS SC ON SE.StatusCodeId = SC.StatusCodeId
--	INNER JOIN dbo.sySysStatus AS SS ON SC.SysStatusId = SS.SysStatusId
--	WHERE SE.StuEnrollId = @stuEnrollId
--END

-- Get all terms that are for every program (NULL ProgId in ArTerm)
-- where the student enrollment's start date is between the term's
-- start and end dates
        SELECT  T.TermId
               ,T.TermDescrip
               ,T.StartDate
        FROM    dbo.arTerm AS T
        WHERE   T.CampGrpId IN ( SELECT PV.CampGrpId
                                 FROM   dbo.arPrgVersions AS PV
                                 INNER JOIN @MyEnrollments AS SE ON PV.PrgVerId = SE.PrgVerId
                                 INNER JOIN dbo.syCampGrps AS CG ON PV.CampGrpId = CG.CampGrpId
                                 WHERE  PV.CampGrpId IN ( SELECT    CampGrpId
                                                          FROM      dbo.syCmpGrpCmps
                                                          WHERE     CampusId = @CampusId )
                                        OR CG.IsAllCampusGrp = 1 )
                AND @StartDate BETWEEN T.StartDate AND T.EndDate
                AND T.ProgId IS NULL
        UNION  
  
-- Get all terms that are continuing education that have a term start date
-- greater than or equal to the student enrollment's start date
        SELECT  T.TermId
               ,T.TermDescrip
               ,T.StartDate
        FROM    dbo.arTerm AS T
        INNER JOIN dbo.arPrograms AS P ON T.ProgId = P.ProgId
        INNER JOIN dbo.arPrgVersions AS PV ON P.ProgId = PV.ProgId
        INNER JOIN @MyEnrollments AS SE ON PV.PrgVerId = SE.PrgVerId
        WHERE   PV.IsContinuingEd = 1
                AND T.StartDate >= @StartDate
                AND SE.CampusId = @CampusId
        UNION

-- Get all terms available at that student enrollment's campus location
-- where the term start date is less than or equal to the enrollment's
-- expected start date and the
        SELECT DISTINCT
                T.TermId
               ,T.TermDescrip
               ,T.StartDate
        FROM    dbo.arTerm AS T
        LEFT OUTER JOIN dbo.arPrograms P ON T.ProgId = P.ProgId
        LEFT OUTER  JOIN dbo.arPrgVersions PV ON P.ProgId = PV.ProgId
        LEFT OUTER JOIN @MyEnrollments AS SE ON PV.PrgVerId = SE.PrgVerId
        INNER JOIN dbo.syCmpGrpCmps CGC ON T.CampGrpId = CGC.CampGrpId
        LEFT OUTER JOIN dbo.syCampuses AS C ON CGC.CampusId = C.CampusId
        WHERE   (
                  SE.StuEnrollId = @StuenrollId
                  AND (
                        C.CampusId = @CampusId
                        OR C.CampusId IS NULL
                      )
                  AND T.StartDate <= SE.ExpStartDate
                  AND SE.ExpStartDate <= T.EndDate
                )
        UNION

-- Get the terms that are assigned to all programs
-- and have a start date => the student enrollment expected start date
        SELECT  T.TermId
               ,T.TermDescrip
               ,T.StartDate
        FROM    dbo.arTerm AS T
        INNER JOIN dbo.syCmpGrpCmps CGC ON T.CampGrpId = CGC.CampGrpId
        WHERE   T.ProgId IS NULL
                AND CGC.CampusId = @CampusId
                AND T.StartDate >= (
                                     SELECT ExpStartDate
                                     FROM   @MyEnrollments
                                   );
	
    END;



GO
