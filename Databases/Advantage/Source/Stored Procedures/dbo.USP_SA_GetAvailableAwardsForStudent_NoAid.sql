SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[USP_SA_GetAvailableAwardsForStudent_NoAid]
    (
     @StuEnrollID UNIQUEIDENTIFIER
    --New Variable Added On June 09, 2010 For Mantis Id 18950--
    ,@CutoffDate DATETIME
    --New Variable Added On June 09, 2010 For Mantis Id 18950--
    )
AS /*----------------------------------------------------------------------------------------------------
    Author : Saraswathi Lakshmanan
    
    Create date : 04/12/2010
    
    Procedure Name : [USP_SA_GetAvailableAwardsForStudent]

    Objective : Get the awards for the student
    
    Parameters : Name Type Data Type Required? 
                        ===== ==== ========= ========= 
                        @StuEnrollID In Uniqueidentifier Required
    
    Output : returns all the available awards for a student
                        
*/-----------------------------------------------------------------------------------------------------

    BEGIN
        SELECT DISTINCT
                T.AwardTypeId AS FundSourceId
               ,F.FundSourceDescrip
        FROM    (
                  SELECT    SA.AwardTypeId
                           ,SA.AwardStartDate
                           ,SAS.AwardScheduleId
                           ,Amount - COALESCE((
                                                SELECT  SUM(Amount)
                                                FROM    saPmtDisbRel
                                                WHERE   AwardScheduleId = SAS.AwardScheduleId
                                              ),0) + COALESCE((
                                                                SELECT  SUM(TransAmount)
                                                                FROM    saTransactions TR
                                                                       ,saRefunds R
                                                                WHERE   R.AwardScheduleId = SAS.AwardScheduleId
                                                                        AND TR.TransactionId = R.TransactionId
                                                              ),0) AS Balance
                  FROM      faStudentAwards SA
                           ,faStudentAwardSchedule SAS
                  WHERE     SA.StuEnrollId = @StuEnrollID
                            AND SA.StudentAwardId = SAS.StudentAwardId
                ) T
               ,saFundSources F
        WHERE   T.AwardTypeId = F.FundSourceId
                AND Balance > 0 
    --New Variable Added On June 09, 2010 For Mantis Id 18950--
                AND F.AdvFundSourceId = 1
    --New Variable Added On June 09, 2010 For Mantis Id 18950-- 
        ORDER BY FundSourceDescrip; 
    END;




GO
