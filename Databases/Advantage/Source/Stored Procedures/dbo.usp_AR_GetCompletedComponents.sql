SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/* 
US4110 Transfer Partially Completed Components 

CREATED: 
6/27/2013 WMP

PURPOSE: 
Select completed components for a course to be displayed in transfer components grid
	A record in arGrdBkResults table (regardless of score) means a grade has been entered (and component is completed)

MODIFIED:
7/10/2013 WMP	DE9829	Remove score > 0 from where
7/10/2013 WMP	DE9832	Show lab work (500), lab hours (503) and externship hours (544) as cumulative amounts
7/18/2013 WMP			Update sort order
7/22/2013 WMP	DE9875	Exclude externship hours (544) since they are only empty links
9/3/2013  WMP	US4366	renamed to remove ByCourse (formerly usp_AR_GetCompletedComponentsByCourse)

*/

CREATE PROCEDURE [dbo].[usp_AR_GetCompletedComponents] @ResultId UNIQUEIDENTIFIER
AS
BEGIN


    CREATE TABLE #ComponentScoreList
    --create list of results to include cumulative and individual scores/hours
    (
        InstrGrdBkWgtId UNIQUEIDENTIFIER,
        GrdComponentTypeId UNIQUEIDENTIFIER,
        ClsSectionId UNIQUEIDENTIFIER,
        StuEnrollId UNIQUEIDENTIFIER,
        Score DECIMAL(10, 2)
    );
    INSERT INTO #ComponentScoreList
    (
        InstrGrdBkWgtId,
        GrdComponentTypeId,
        ClsSectionId,
        StuEnrollId,
        Score
    )
    SELECT gbwd.InstrGrdBkWgtId,
           gbwd.GrdComponentTypeId,
           gbr.ClsSectionId,
           StuEnrollId,
           gbr.Score AS Score
    FROM arGrdBkResults gbr
        INNER JOIN arGrdBkWgtDetails gbwd
            ON gbwd.InstrGrdBkWgtDetailId = gbr.InstrGrdBkWgtDetailId
        INNER JOIN arGrdComponentTypes gct
            ON gct.GrdComponentTypeId = gbwd.GrdComponentTypeId
    WHERE gct.SysComponentTypeId NOT IN ( 500, 503, 544 ); --individual scores

    INSERT INTO #ComponentScoreList
    (
        InstrGrdBkWgtId,
        GrdComponentTypeId,
        ClsSectionId,
        StuEnrollId,
        Score
    )
    SELECT gbwd.InstrGrdBkWgtId,
           gbwd.GrdComponentTypeId,
           gbr.ClsSectionId,
           StuEnrollId,
           SUM(ISNULL(gbr.Score, 0)) AS Score
    FROM arGrdBkResults gbr
        INNER JOIN arGrdBkWgtDetails gbwd
            ON gbwd.InstrGrdBkWgtDetailId = gbr.InstrGrdBkWgtDetailId
        INNER JOIN arGrdComponentTypes gct
            ON gct.GrdComponentTypeId = gbwd.GrdComponentTypeId
    WHERE gct.SysComponentTypeId IN ( 500, 503 ) --cumulative scores
    GROUP BY gbwd.InstrGrdBkWgtId,
             gbwd.GrdComponentTypeId,
             gbr.ClsSectionId,
             StuEnrollId;



    SELECT csl.InstrGrdBkWgtId,
           gct.Descrip AS Description,
           r.Resource AS ComponentType,
           csl.Score,
           rs.ResultId,
           rq.ReqId
    FROM #ComponentScoreList csl
        INNER JOIN arGrdComponentTypes gct
            ON gct.GrdComponentTypeId = csl.GrdComponentTypeId
        INNER JOIN syResources r
            ON r.ResourceID = gct.SysComponentTypeId
        INNER JOIN arClassSections cs
            ON cs.ClsSectionId = csl.ClsSectionId
        INNER JOIN arReqs rq
            ON rq.ReqId = cs.ReqId
        INNER JOIN arResults rs
            ON rs.TestId = cs.ClsSectionId
               AND rs.StuEnrollId = csl.StuEnrollId
    WHERE rs.ResultId = @ResultId
    ORDER BY r.Resource,
             gct.Descrip;

    DROP TABLE #ComponentScoreList;


END;




GO
