SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[USP_TimeInterval_Insert]
    (
     @TimeIntervalId UNIQUEIDENTIFIER
    ,@TimeIntervalDescrip DATETIME = NULL
    ,@StatusId UNIQUEIDENTIFIER
    ,@UserName VARCHAR(50)
    ,@ModDate DATETIME
    )
AS /*----------------------------------------------------------------------------------------------------
	Author          :	Janet Robinson
    
    Create date		:	08/28/2011
    
	Procedure Name	:	[USP_TimeInterval_Insert]

	Objective		:	Inserts into table cmTimeInterval
	
	Parameters		:	Name						Type	Data Type				Required? 	
						=====						====	=========				=========	
						@TimeIntervalId				In		UniqueIDENTIFIER		Required
						@TimeIntervalDescrip		In		datetime				No												
						@StatusId					In		UniqueIDENTIFIER		Required						
						@UserName					In		varchar					No
						@ModDate                    In		datetime				Required
	Output			:			
						
*/-----------------------------------------------------------------------------------------------------

    BEGIN

        IF NOT EXISTS ( SELECT  *
                        FROM    dbo.cmTimeInterval
                        WHERE   TimeIntervalDescrip = @TimeIntervalDescrip
                                AND StatusId = @StatusId
                                AND CampGrpId = (
                                                  SELECT    CampGrpId
                                                  FROM      syCampGrps
                                                  WHERE     RTRIM(CampGrpCode) = 'All'
                                                ) )
            BEGIN
                INSERT  cmTimeInterval
                        (
                         TimeIntervalId
                        ,TimeIntervalDescrip
                        ,ModUser
                        ,ModDate
                        ,StatusId
                        ,CampGrpId
                        )
                VALUES  (
                         @TimeIntervalId
                        ,@TimeIntervalDescrip
                        ,@UserName
                        ,@ModDate
                        ,@StatusId
                        ,(
                           SELECT   CampGrpId
                           FROM     syCampGrps
                           WHERE    RTRIM(CampGrpCode) = 'All'
                         )
                        );
            END;
	
		 
    END;




GO
