SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[USP_AR_GetSystemTransCodesCharges_GetList]
    (
     @ShowActive NVARCHAR(50)
    )
AS /*----------------------------------------------------------------------------------------------------
	Author          :	Saraswathi Lakshmanan
    
    Create date		:	05/05/2010
    
	Procedure Name	:	[USP_AR_GetSystemTransCodesCharges_GetList]

	Objective		:	Get the system TransactionCodes of type charges
	
	Parameters		:	Name			Type	Data Type			Required? 	
						=====			====	=========			=========	
						@ShowActive		In		nvarchar			Required
	
	Output			:	Returns the TransCodes other than  whose systransCodeID is 11,12,13,14,15,16	
						
*/-----------------------------------------------------------------------------------------------------

    BEGIN

        DECLARE @ShowActiveValue NVARCHAR(20);
        IF LOWER(@ShowActive) = 'true'
            BEGIN
                SET @ShowActiveValue = 'Active';
            END;
        ELSE
            BEGIN
                SET @ShowActiveValue = 'InActive';
            END;

        SELECT  TC.SysTransCodeId
               ,TC.Description
        FROM    saSysTransCodes TC
               ,syStatuses ST
        WHERE   TC.StatusId = ST.StatusId
                AND SysTransCodeId NOT IN ( 11,12,13,14,15,16 )
                AND ST.Status = @ShowActiveValue
        ORDER BY TC.Description;

    END;




GO
