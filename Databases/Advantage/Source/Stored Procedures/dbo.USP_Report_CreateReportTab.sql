SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_Report_CreateReportTab]
    @ReportId AS UNIQUEIDENTIFIER
   ,@FilterSetId AS INT
   ,@OptionsSetId AS INT
   ,@SortSetId AS INT
   ,@SetName AS VARCHAR(MAX)
   ,@ReportName AS VARCHAR(MAX)
-- WITH ENCRYPTION, RECOMPILE, EXECUTE AS CALLER|SELF|OWNER| 'user_name'
AS
    IF @FilterSetId > 0
        BEGIN
            IF NOT EXISTS (
                          SELECT TOP 1 ReportId
                          FROM   dbo.syReportTabs
                          WHERE  ReportId = @ReportId
                                 AND PageViewId = 'RadRptPage_Filtering'
                          )
                BEGIN
                    INSERT INTO dbo.syReportTabs (
                                                 ReportId
                                                ,PageViewId
                                                ,UserControlName
                                                ,SetId
                                                ,ControllerClass
                                                ,DisplayName
                                                ,ParameterSetLookUp
                                                ,TabName
                                                 )
                    VALUES ( @ReportId                             -- ReportId - uniqueidentifier
                            ,N'RadRptPage_Filtering'               -- PageViewId - nvarchar(200)
                            ,N'ParamSetPanelBarFilterControl'      -- UserControlName - nvarchar(200)
                            ,@FilterSetId                          -- SetId - bigint
                            ,N'ParamSetPanelBarFilterControl.ascx' -- ControllerClass - nvarchar(200)
                            ,'Filter for ' + @ReportName           -- DisplayName - nvarchar(200)
                            ,@SetName                              -- ParameterSetLookUp - nvarchar(200)
                            ,N'Filtering'                          -- TabName - nvarchar(200)
                        );
                END;
        END;

    IF @OptionsSetId > 0
        BEGIN
            IF NOT EXISTS (
                          SELECT TOP 1 ReportId
                          FROM   dbo.syReportTabs
                          WHERE  ReportId = @ReportId
                                 AND PageViewId = 'RadRptPage_Options'
                          )
                BEGIN
                    INSERT INTO dbo.syReportTabs (
                                                 ReportId
                                                ,PageViewId
                                                ,UserControlName
                                                ,SetId
                                                ,ControllerClass
                                                ,DisplayName
                                                ,ParameterSetLookUp
                                                ,TabName
                                                 )
                    VALUES ( @ReportId                        -- ReportId - uniqueidentifier
                            ,N'RadRptPage_Options'            -- PageViewId - nvarchar(200)
                            ,N'ParamPanelBarSetCustomOptions' -- UserControlName - nvarchar(200)
                            ,@OptionsSetId                    -- SetId - bigint
                            ,N'ParamSetPanelBarControl.ascx'  -- ControllerClass - nvarchar(200)
                            ,N'Options for ' + @ReportName    -- DisplayName - nvarchar(200)
                            ,@SetName                         -- ParameterSetLookUp - nvarchar(200)
                            ,N'Options'                       -- TabName - nvarchar(200)
                        );
                END;
        END;


    IF @SortSetId > 0
        BEGIN
            IF NOT EXISTS (
                          SELECT TOP 1 ReportId
                          FROM   dbo.syReportTabs
                          WHERE  ReportId = @ReportId
                                 AND PageViewId = 'RadRptPage_Sorting'
                          )
                BEGIN
                    INSERT INTO dbo.syReportTabs (
                                                 ReportId
                                                ,PageViewId
                                                ,UserControlName
                                                ,SetId
                                                ,ControllerClass
                                                ,DisplayName
                                                ,ParameterSetLookUp
                                                ,TabName
                                                 )
                    VALUES ( @ReportId                       -- ReportId - uniqueidentifier
                            ,N'RadRptPage_Sorting'           -- PageViewId - nvarchar(200)
                            ,N'ParamPanelBarSetSorting'      -- UserControlName - nvarchar(200)
                            ,@SortSetId                      -- SetId - bigint
                            ,N'ParamSetPanelBarControl.ascx' -- ControllerClass - nvarchar(200)
                            ,N'Sort for ' + @ReportName      -- DisplayName - nvarchar(200)
                            ,@SetName                        -- ParameterSetLookUp - nvarchar(200)
                            ,N'Sorting'                      -- TabName - nvarchar(200)
                        );
                END;
        END;

GO
