SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_GetAllActiveTerms] @campusId VARCHAR(50)
AS
    SET NOCOUNT ON;
    SELECT  T.TermId
           ,T.StatusId
           ,T.TermCode
           ,T.TermDescrip
           ,T.StartDate
           ,T.EndDate
           ,T.ShiftId
           ,(
              SELECT    COUNT(*)
              FROM      saTransactions T1
                       ,arStuEnrollments SE
                       ,saProgramVersionFees PVF
              WHERE     T1.StuEnrollId = SE.StuEnrollId
                        AND T1.TransCodeId = PVF.TransCodeId
                        AND PVF.PrgVerId = SE.PrgVerId
                        AND T1.TermId = T.TermId
                        AND Voided = 0
                        AND IsAutomatic = 1
            ) + (
                  SELECT    COUNT(*)
                  FROM      saTransactions T1
                           ,arStuEnrollments SE
                           ,saCourseFees CF
                  WHERE     T1.StuEnrollId = SE.StuEnrollId
                            AND T1.TransCodeId = CF.TransCodeId
                            AND T1.TermId = T.TermId
                            AND Voided = 0
                            AND IsAutomatic = 1
                ) + (
                      SELECT    COUNT(*)
                      FROM      saTransactions T1
                               ,arStuEnrollments SE
                               ,saPeriodicFees PF
                      WHERE     T1.StuEnrollId = SE.StuEnrollId
                                AND T1.TransCodeId = PF.TransCodeId
                                AND PF.TermId = T1.TermId
                                AND T1.TermId = T.TermId
                                AND Voided = 0
                                AND IsAutomatic = 1
                    ) AS NumberOfTransactionsPosted
    FROM    arTerm T
           ,syStatuses ST
    WHERE   T.StatusId = ST.StatusId
            AND ST.Status = 'Active'
            AND (
                  T.CampGrpId IN ( SELECT   CampGrpId
                                   FROM     syCmpGrpCmps
                                   WHERE    CampusId = @campusId
                                            AND CampGrpId <> (
                                                               SELECT   CampGrpId
                                                               FROM     syCampGrps
                                                               WHERE    CampGrpDescrip = 'ALL'
                                                             ) )
                  OR CampGrpId = (
                                   SELECT   CampGrpId
                                   FROM     syCampGrps
                                   WHERE    CampGrpDescrip = 'ALL'
                                 )
                )
    ORDER BY T.StartDate
           ,T.TermDescrip;





GO
