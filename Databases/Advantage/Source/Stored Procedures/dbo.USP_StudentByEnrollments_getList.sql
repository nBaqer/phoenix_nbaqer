SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_StudentByEnrollments_getList]
    @StuEnrollId VARCHAR(8000)
AS
    SELECT DISTINCT
            S.FirstName + ' ' + S.LastName AS StudentName
           ,SE.StartDate
           ,SE.ExpGradDate
    FROM    arStudent S
    INNER JOIN arStuEnrollments SE ON S.StudentId = SE.StudentId
    WHERE   ( SE.StuEnrollId IN ( SELECT    Val
                                  FROM      MultipleValuesForReportParameters(@StuEnrollId,',',1) ) );
GO
