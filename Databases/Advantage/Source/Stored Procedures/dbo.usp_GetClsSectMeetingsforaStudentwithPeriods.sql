SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[usp_GetClsSectMeetingsforaStudentwithPeriods]
    (
     @StuEnrollID UNIQUEIDENTIFIER 
       
    )
AS
    BEGIN 

        SELECT  CSM.ClsSectMeetingId
               ,R.Descrip + '/' + RM.Descrip + ' - ' + PR.PeriodDescrip + ' - ' + ACT.InstructionTypeDescrip + '( ' + CONVERT(NVARCHAR(30),CSM.StartDate,107)
                + '-' + CONVERT(NVARCHAR(30),CSM.EndDate,107) + ')' AS Descrip
               ,CSM.StartDate
               ,CSM.EndDate
        FROM    dbo.arClsSectMeetings CSM
               ,arRooms RM
               ,SyPeriods PR
               ,dbo.arInstructionType ACT
               ,dbo.arClassSections CS
               ,dbo.arReqs R
               ,arresults AR
        WHERE   CSM.RoomId = RM.RoomId
                AND CSM.PeriodId = PR.PeriodId
                AND CSM.InstructionTypeID = ACT.InstructionTypeID
                AND CS.ClsSectionId = CSM.ClsSectionId
                AND CS.ClsSectionId = AR.TestId
                AND StuEnrollId = @StuEnrollID
                AND R.ReqId = CS.ReqId
                AND R.UnitTypeId <> '2600592A-9739-4A13-BDCE-7A25FE4A7478';
 
    END;







GO
