SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[USP_SA_Get1098TDataForTaxYear]
    @TaxYear AS CHAR(4)
   ,@CampusId AS UNIQUEIDENTIFIER

	-------------------------------------------------------------------------------------------------
	--Testing Purposes Only
	--------------------------------------------------------------------------------------------------
	-- Version Date 1.27.2017 : DE13327
	--DECLARE @TaxYear AS CHAR(4)
	--declare @CampusId AS UNIQUEIDENTIFIER
  
	--SET @TaxYear='2016' 
	--SET @CampusId='3F5E839A-589A-4B2A-B258-35A1A8B3B819' 
	-----------------------------------------------------------------------------------------------
AS
    BEGIN
        DECLARE @strFirstQuarter AS VARCHAR(10) = '03/31/' + CONVERT(VARCHAR(10),CONVERT(INT,@TaxYear) + 1);
        DECLARE @strFirstCalendarDay AS VARCHAR(10) = '01/01/' + @TaxYear;
        DECLARE @intPrevYear AS INTEGER = CONVERT(INT,@TaxYear) - 1;
        DECLARE @intCurrentTaxYear AS INTEGER = CONVERT(INT,@TaxYear);        
        DECLARE @EndOfYear AS VARCHAR(10) = '12/31/' + @TaxYear;
        DECLARE @StartOfYear AS VARCHAR(10) = '1/1/' + @TaxYear;
	
        SELECT DISTINCT
                StuEnrollId
               ,t1.EnrollmentId
               ,CONVERT(CHAR(10),StartDate,101) AS StartDate
               ,(
                  SELECT    CONVERT(CHAR(10),MAX(LDA),101)
                  FROM      arStuEnrollments
                  WHERE     StuEnrollId = t1.StuEnrollId
                            AND LDA IS NOT NULL
                ) AS LDA
               ,(
                  SELECT    CASE WHEN COUNT(*) > 0 THEN 1
                                 ELSE 0
                            END
                  FROM      arStuEnrollments se
                           ,syStatusCodes sc
                  WHERE     se.StatusCodeId = sc.StatusCodeId
                            AND sc.SysStatusId = 8 -- no start status
                            AND se.StuEnrollId = t1.StuEnrollId
                ) AS IsStudentNoStart
               ,(
                  SELECT    CONVERT(CHAR(10),MAX(TransDate),101)
                  FROM      saTransactions
                  WHERE     StuEnrollId = t1.StuEnrollId
                            AND TransDate IS NOT NULL
                            AND Voided = 0
                            AND TransTypeId = 2
                ) AS TransDate
               ,t2.FirstName
               ,t2.LastName
               ,t2.SSN
               ,( CASE WHEN EXISTS ( SELECT *
                                     FROM   dbo.arStudAddresses sa1
                                     WHERE  sa1.StudentId = t2.StudentId
                                            AND sa1.default1 = 1 ) THEN (
                                                                          SELECT    RTRIM(sa2.Address1 + ' ' + ISNULL(sa2.Address2,''))
                                                                          FROM      dbo.arStudAddresses sa2
                                                                          WHERE     sa2.StudentId = t2.StudentId
                                                                                    AND sa2.default1 = 1
                                                                        )
                       ELSE (
                              SELECT TOP 1
                                        RTRIM(sa2.Address1 + ' ' + ISNULL(sa2.Address2,''))
                              FROM      dbo.arStudAddresses sa2
                              WHERE     sa2.StudentId = t2.StudentId
                              ORDER BY  sa2.ModDate DESC
                            )
                  END ) AS Address1
               ,( CASE WHEN EXISTS ( SELECT *
                                     FROM   dbo.arStudAddresses sa1
                                     WHERE  sa1.StudentId = t2.StudentId
                                            AND sa1.default1 = 1 ) THEN (
                                                                          SELECT    RTRIM(ISNULL(sa2.City,''))
                                                                          FROM      dbo.arStudAddresses sa2
                                                                          WHERE     sa2.StudentId = t2.StudentId
                                                                                    AND sa2.default1 = 1
                                                                        )
                       ELSE (
                              SELECT TOP 1
                                        RTRIM(ISNULL(sa2.City,''))
                              FROM      dbo.arStudAddresses sa2
                              WHERE     sa2.StudentId = t2.StudentId
                              ORDER BY  sa2.ModDate DESC
                            )
                  END ) AS City
               ,( CASE WHEN EXISTS ( SELECT *
                                     FROM   dbo.arStudAddresses sa1
                                     WHERE  sa1.StudentId = t2.StudentId
                                            AND sa1.default1 = 1 ) THEN (
                                                                          SELECT    RTRIM(ISNULL(ss.StateCode,''))
                                                                          FROM      dbo.arStudAddresses sa2
                                                                                   ,syStates ss
                                                                          WHERE     sa2.StudentId = t2.StudentId
                                                                                    AND sa2.StateId = ss.StateId
                                                                                    AND sa2.default1 = 1
                                                                        )
                       ELSE (
                              SELECT TOP 1
                                        RTRIM(ISNULL(ss.StateCode,''))
                              FROM      dbo.arStudAddresses sa2
                                       ,syStates ss
                              WHERE     sa2.StudentId = t2.StudentId
                                        AND sa2.StateId = ss.StateId
                              ORDER BY  sa2.ModDate DESC
                            )
                  END ) AS State
               ,( CASE WHEN EXISTS ( SELECT *
                                     FROM   dbo.arStudAddresses sa1
                                     WHERE  sa1.StudentId = t2.StudentId
                                            AND sa1.default1 = 1 ) THEN (
                                                                          SELECT    RTRIM(ISNULL(sa2.Zip,''))
                                                                          FROM      dbo.arStudAddresses sa2
                                                                          WHERE     sa2.StudentId = t2.StudentId
                                                                                    AND sa2.default1 = 1
                                                                        )
                       ELSE (
                              SELECT TOP 1
                                        RTRIM(ISNULL(sa2.Zip,''))
                              FROM      dbo.arStudAddresses sa2
                              WHERE     sa2.StudentId = t2.StudentId
                              ORDER BY  sa2.ModDate DESC
                            )
                  END ) AS Zip
               ,(
                  SELECT    ISNULL(SUM(TransAmount),0)
                  FROM      saTransactions SQ1
                           ,dbo.saTransCodes tc
                  WHERE     SQ1.TransCodeId = tc.TransCodeId
                            AND SQ1.StuEnrollId = t1.StuEnrollId
                            AND (
                                  SQ1.TransTypeId = 0
                                  OR (
                                       SQ1.TransTypeId = 1
                                       AND tc.SysTransCodeId <> 16
                                     )
                                )
                            AND SQ1.Voided = 0
                            AND tc.Is1098T = 1
                ) AS TotalCost
               ,(
                  SELECT    ISNULL(SUM(TransAmount * -1),0)
                  FROM      saTransactions SQ1
                  WHERE     SQ1.StuEnrollId = t1.StuEnrollId
                            AND SQ1.TransTypeId = 2
                            AND SQ1.Voided = 0
                            AND YEAR(TransDate) = CONVERT(INT,@TaxYear)
                ) AS TotalPaid
               ,(
                  SELECT    ISNULL(SUM(TransAmount * -1),0)
                  FROM      saTransactions SQ1
                  WHERE     SQ1.StuEnrollId = t1.StuEnrollId
                            AND SQ1.TransTypeId = 2
                            AND SQ1.Voided = 0
                            AND YEAR(TransDate) < CONVERT(INT,@TaxYear)
                ) AS PriorYearPayments
               ,(
                  SELECT    ISNULL(SUM(TransAmount),0)
                  FROM      saTransactions SQ1
                           ,saRefunds SR
                  WHERE     SQ1.StuEnrollId = t1.StuEnrollId
                            AND SQ1.TransactionId = SR.TransactionId
                            AND SQ1.Voided = 0
                            AND YEAR(TransDate) < CONVERT(INT,@TaxYear)
                ) AS PriorYearRefunds
               ,(
                  SELECT    ISNULL(SUM(TransAmount * -1),0)
                  FROM      saTransactions SQ1
                  WHERE     SQ1.StuEnrollId = t1.StuEnrollId
                            AND SQ1.TransTypeId = 2
                            AND SQ1.Voided = 0
                            AND YEAR(TransDate) = CONVERT(INT,@TaxYear)
                ) AS GrossPayments
               ,(
                  SELECT    ISNULL(SUM(TransAmount),0)
                  FROM      saTransactions SQ1
                           ,saRefunds SQ2
                  WHERE     SQ1.TransactionId = SQ2.TransactionId
                            AND SQ1.Voided = 0
                            AND SQ1.StuEnrollId = t1.StuEnrollId
                            AND YEAR(TransDate) = CONVERT(INT,@TaxYear)
                ) AS GrossRefunds
               ,(
                  SELECT    ISNULL(SUM(TransAmount),0)
                  FROM      saTransactions A1
                           ,saFundSources A5
                           ,saRefunds A6
                  WHERE     A1.TransactionId = A6.TransactionId
                            AND A5.FundSourceId = A6.FundSourceId
                            AND A1.Voided = 0
                            AND A1.StuEnrollId = t1.StuEnrollId
                            AND YEAR(TransDate) = CONVERT(INT,@TaxYear)
                            AND A5.AwardTypeId = 1
                ) AS GrantRefunds
               ,(
                  SELECT    ISNULL(SUM(TransAmount),0)
                  FROM      saTransactions A1
                           ,saFundSources A5
                           ,saRefunds A6
                  WHERE     A1.TransactionId = A6.TransactionId
                            AND A5.FundSourceId = A6.FundSourceId
                            AND A1.Voided = 0
                            AND A1.StuEnrollId = t1.StuEnrollId
                            AND YEAR(TransDate) = CONVERT(INT,@TaxYear)
                            AND A6.RefundTypeId = 1
                            AND A5.AwardTypeId = 2
                ) AS LoanRefunds
               ,(
                  SELECT    ISNULL(SUM(TransAmount),0)
                  FROM      saTransactions SQ1
                           ,saRefunds SQ2
                  WHERE     SQ1.TransactionId = SQ2.TransactionId
                            AND SQ1.Voided = 0
                            AND SQ2.RefundTypeId = 0
                            AND SQ1.StuEnrollId = t1.StuEnrollId
                            AND YEAR(TransDate) = CONVERT(INT,@TaxYear)
                ) AS StudentRefunds
               ,(
                  SELECT    ISNULL(SUM(TransAmount * -1),0)
                  FROM      (
                              SELECT    A1.TransDate
                                       ,A1.TransAmount
                                       ,A1.TransDescrip
                                       ,A1.FundSourceId
                              FROM      saTransactions A1
                                       ,saFundSources A5
                              WHERE     A1.FundSourceId = A5.FundSourceId
                                        AND A1.Voided = 0
                                        AND A1.StuEnrollId = t1.StuEnrollId
                                        AND YEAR(TransDate) = CONVERT(INT,@TaxYear)
                                        AND A5.AwardTypeId = 1
                            ) R
                ) AS GrantPayments
               ,(
                  SELECT    ISNULL(SUM(TransAmount * -1),0)
                  FROM      (
                              SELECT    A1.TransDate
                                       ,A1.TransAmount
                                       ,A1.TransDescrip
                                       ,A1.FundSourceId
                              FROM      saTransactions A1
                                       ,saFundSources A5
                              WHERE     A1.FundSourceId = A5.FundSourceId
                                        AND A1.Voided = 0
                                        AND A1.StuEnrollId = t1.StuEnrollId
                                        AND YEAR(TransDate) = CONVERT(INT,@TaxYear)
                                        AND A5.AwardTypeId = 2
                            ) R
                ) AS LoanPayments
               ,(
                  SELECT    ISNULL(SUM(TransAmount * -1),0)
                  FROM      (
                              SELECT    A1.TransDate
                                       ,A1.TransAmount
                                       ,A1.TransDescrip
                                       ,A1.FundSourceId
                              FROM      saTransactions A1
                                       ,saFundSources A5
                              WHERE     A1.FundSourceId = A5.FundSourceId
                                        AND A1.Voided = 0
                                        AND A1.StuEnrollId = t1.StuEnrollId
                                        AND YEAR(TransDate) = CONVERT(INT,@TaxYear)
                                        AND A5.AwardTypeId IN ( 3,4,5 )
                            ) R
                ) AS Scholarships
               ,(
                  SELECT DISTINCT
                            IsContinuingEd
                  FROM      arPrgVersions
                  WHERE     PrgVerId = t1.PrgVerId
                ) AS IsContinuingEd
               ,(
                  SELECT    ISNULL(SUM(TransAmount * -1),0)
                  FROM      (
                              SELECT    A1.TransDate
                                       ,A1.TransAmount
                                       ,A1.TransDescrip
                                       ,A1.FundSourceId
                              FROM      saTransactions A1
                                       ,saFundSources A5
                              WHERE     A1.FundSourceId = A5.FundSourceId
                                        AND A1.Voided = 0
                                        AND A1.StuEnrollId = t1.StuEnrollId
                                        AND YEAR(TransDate) = CONVERT(INT,@TaxYear)
                                        AND A5.AwardTypeId = 6
                            ) R
                ) AS StudentPayments
               ,(
                  SELECT    COUNT(*)
                  FROM      saTransactions SQ1
                  WHERE     SQ1.StuEnrollId = t1.StuEnrollId
                            AND SQ1.Voided = 0
                            AND TransTypeId = 0
                ) AS CostForEnrollmentExist
               ,(
                  SELECT    ISNULL(SUM(TransAmount),0)
                  FROM      saTransactions SQ1
                           ,dbo.saTransCodes tc
                  WHERE     SQ1.TransCodeId = tc.TransCodeId
                            AND SQ1.StuEnrollId = t1.StuEnrollId
                            AND (
                                  SQ1.TransTypeId = 0
                                  OR (
                                       SQ1.TransTypeId = 1
                                       AND tc.SysTransCodeId <> 16
                                     )
                                )
                            AND SQ1.Voided = 0
                            AND tc.Is1098T = 1
                            AND YEAR(TransDate) = CONVERT(INT,@TaxYear)
                ) AS CurrentYrInstitutionalCharges
               ,(
                  SELECT    ISNULL(SUM(TransAmount),0)
                  FROM      saTransactions SQ1
                           ,dbo.saTransCodes tc
                  WHERE     SQ1.TransCodeId = tc.TransCodeId
                            AND SQ1.StuEnrollId = t1.StuEnrollId
                            AND (
                                  SQ1.TransTypeId = 0
                                  OR (
                                       SQ1.TransTypeId = 1
                                       AND tc.SysTransCodeId <> 16
                                     )
                                )
                            AND SQ1.Voided = 0
                            AND tc.Is1098T = 1
                            AND YEAR(TransDate) < CONVERT(INT,@TaxYear)
                ) AS PriorYrInstitutionalCharges
        FROM    arStuEnrollments t1
        INNER JOIN arStudent t2 ON t1.StudentId = t2.StudentId
        INNER JOIN syStatusCodes t5 ON t1.StatusCodeId = t5.StatusCodeId
        INNER JOIN arPrgVersions t7 ON t1.PrgVerId = t7.PrgVerId
        INNER JOIN arPrograms t6 ON t6.ProgId = t7.ProgId
        WHERE   t1.CampusId = @CampusId
                AND t6.Is1098T = '1'
                AND (
                      --This part mostly handles the attendance taking schools. The student must have started
            --before the end of the current year and must have attendance for the current year.
                      (
                        t1.StartDate <= @EndOfYear
                        AND t1.LDA >= @StartOfYear
                      )
                      OR
			--Student enrolled in the current year but starts in the first quarter of next year
                      (
                        t1.StartDate > @EndOfYear
                        AND t1.StartDate <= @strFirstQuarter
                        AND YEAR(t1.EnrollDate) = CONVERT(INT,@TaxYear)
                      )
                      OR
			--This part mostly handles schools that do not not take attendance
			--Students who are still in school - currently attending, LOA, Suspended, Academic Probation, Suspension
			--Note that we include future starts.                                
                      (
                        t1.StartDate <= @EndOfYear
                        AND t5.SysStatusId IN ( 7,9,10,11,20,21,22 )
                      )
                      OR
			--Include no starts who started during the year. They will be added to the exclusions table.
                      (
                        t1.StartDate BETWEEN @StartOfYear AND @EndOfYear
                        AND t5.SysStatusId = 8
                      )
                      OR
			--Students who graduated during the current year and the school
                      (
                        t1.StartDate <= @EndOfYear
                        AND t5.SysStatusId = 14
                        AND t1.ExpGradDate > @StartOfYear
                      )
                    )
            --The student must have a record in the student addresses table
                AND EXISTS ( SELECT *
                             FROM   dbo.arStudAddresses sa
                             WHERE  sa.StudentId = t2.StudentId )
			--Only return students with a SSN
                AND (
                      t2.SSN IS NOT NULL
                      AND t2.SSN <> ''
                      AND t2.SSN <> '000000000'
                    )
        ORDER BY LastName;

    END;
GO
