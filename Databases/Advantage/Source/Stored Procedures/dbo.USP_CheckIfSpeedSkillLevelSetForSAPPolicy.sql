SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_CheckIfSpeedSkillLevelSetForSAPPolicy]
    @SAPDetailId UNIQUEIDENTIFIER
AS
    SELECT  COUNT(*) AS rowcounter
    FROM    arSAP_ShortHandSkillRequirement
    WHERE   SAPDetailId = @SAPDetailId;



GO
