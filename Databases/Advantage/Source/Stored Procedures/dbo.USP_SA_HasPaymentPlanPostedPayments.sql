SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_SA_HasPaymentPlanPostedPayments]
    (
     @PaymentPlanId UNIQUEIDENTIFIER
    )
AS
    DECLARE @PostedPayments BIGINT;
    SET @PostedPayments = (
                            SELECT  COUNT(*)
                            FROM    dbo.saPmtDisbRel postedPayment
                            INNER JOIN dbo.faStuPaymentPlanSchedule scheduledPayment ON scheduledPayment.PayPlanScheduleId = postedPayment.PayPlanScheduleId
                            WHERE   scheduledPayment.PaymentPlanId = @PaymentPlanId
                          );
    IF ( @PostedPayments > 0 )
        SELECT  1 AS HasPostedPayment;
    ELSE
        SELECT  0 AS HasPostedPayment;


GO
