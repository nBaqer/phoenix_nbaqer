SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_GetCourseDescriptionByClass]
    @ClsSectionId UNIQUEIDENTIFIER
AS
    SELECT DISTINCT
            t1.ReqId
           ,t1.Code
           ,t1.Descrip
    FROM    arReqs t1
           ,arClassSections t2
    WHERE   t1.ReqId = t2.ReqId
            AND t2.ClsSectionId = @ClsSectionId;



GO
