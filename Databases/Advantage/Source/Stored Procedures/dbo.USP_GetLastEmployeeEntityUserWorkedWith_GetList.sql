SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_GetLastEmployeeEntityUserWorkedWith_GetList]
    @UserId UNIQUEIDENTIFIER
   ,@MRUTypeId INT
AS
    SELECT TOP 1
            ChildId
    FROM    syMRUs
    WHERE   UserId = @UserId
            AND MRUTypeId = @MRUTypeId
    ORDER BY ModDate DESC;



GO
