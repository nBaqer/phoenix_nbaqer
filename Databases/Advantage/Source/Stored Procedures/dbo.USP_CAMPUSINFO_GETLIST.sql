SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_CAMPUSINFO_GETLIST]
    @CampusId UNIQUEIDENTIFIER
AS
    BEGIN
        DECLARE @SchoolName VARCHAR(100);
        SET @SchoolName = dbo.GetAppSettingValueByKeyName('CorporateName',NULL);
        SELECT TOP 1
                @SchoolName AS SchoolName
               ,c.CampusId
               ,c.CampDescrip AS CampusName
               ,c.CampCode AS CampusCode
               ,c.Address1
               ,c.Address2
               ,c.City
               ,sta.StateDescrip AS State
               ,sta.StateCode AS StateCode
               ,c.Zip AS ZIP
               ,co.CountryDescrip AS Country
               ,c.Phone1
               ,c.Phone2
               ,c.Phone3
               ,c.Fax
               ,c.Website
        FROM    dbo.syCampuses c
        JOIN    dbo.adCountries co ON co.CountryId = c.CountryId
        JOIN    dbo.syStates sta ON sta.StateId = c.StateId
        WHERE   c.CampusId = @CampusId;
    END;
GO
