SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_GetAllCourses]
    @showActiveOnly BIT
   ,@campusId VARCHAR(50)
AS
    SET NOCOUNT ON;
                 
    IF @showActiveOnly = 1
        BEGIN
            EXEC usp_GetAllActiveCourses @campusId;
        END; 
    ELSE
        BEGIN
            EXEC usp_GetAllActiveAndInActiveCourses @campusId,NULL;
        END; 
                 



GO
