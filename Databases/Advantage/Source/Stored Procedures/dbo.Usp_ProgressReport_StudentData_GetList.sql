SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

--=================================================================================================
-- USP_ProgressReport_StudentData_GetList
--=================================================================================================
--exec USP_ProgressReport_StudentData_GetList '503C8F6D-4B28-4ECC-AEA1-4DDD5D68917F',null,0,0,null,'byclass','Hours',null
CREATE PROCEDURE [dbo].[Usp_ProgressReport_StudentData_GetList]
    @StuEnrollId VARCHAR(50)
   ,@TermStartDate DATETIME = NULL
   ,@ShowTotalCostAndCurrentBalance BIT = 0
   , -- by default hide cost and current balance
    @ShowWeeklySchedule BIT = 0
   ,@TermStartDateModifier VARCHAR(10) = NULL
   ,@TrackAttendanceBy VARCHAR(10) = NULL
   ,@displayhours VARCHAR(10) = NULL
   ,@termid VARCHAR(4000) = NULL
AS
    BEGIN
        IF ( @displayhours IS NOT NULL )
            BEGIN
                SET @displayhours = LOWER(@displayhours);
            END;
   
        IF ( @termid IS NOT NULL )
            BEGIN
                SET @TermStartDate = (
                                       SELECT TOP 1
                                                EndDate
                                       FROM     arTerm
                                       WHERE    TermId = @termid
                                     );
                IF @TermStartDate IS NULL
                    BEGIN
                        SET @TermStartDate = (
                                               SELECT TOP 1
                                                        StartDate
                                               FROM     arTerm
                                               WHERE    TermId = @termid
                                             );
                    END;
                SET @TermStartDateModifier = '<=';
            END;

	-- modified by Janet on 11/4/2011 get campusid from arStuEnrollments
        DECLARE @StuEnrollCampusId UNIQUEIDENTIFIER;
        SET @StuEnrollCampusId = COALESCE((
                                            SELECT  CampusId
                                            FROM    arStuEnrollments
                                            WHERE   StuEnrollId = @StuEnrollId
                                          ),NULL);
        SET @TrackAttendanceBy = (
                                   SELECT   dbo.GetAppSettingValue(72,@StuEnrollCampusId)
                                 );

        IF ( @TrackAttendanceBy IS NOT NULL )
            BEGIN
                SET @TrackAttendanceBy = LOWER(@TrackAttendanceBy);
            END;
        DECLARE @ReturnValue VARCHAR(100);
        SELECT  @ReturnValue = COALESCE(@ReturnValue,'') + Descrip + ','
        FROM    adLeadByLeadGroups t1
        INNER JOIN adLeadGroups t2 ON t1.LeadGrpId = t2.LeadGrpId
        WHERE   t1.StuEnrollId = @StuEnrollId; 
        SET @ReturnValue = SUBSTRING(@ReturnValue,1,LEN(@ReturnValue) - 1);

-- For weighted average
        DECLARE @wCourseCredits DECIMAL(18,2)
           ,@wWeighted_GPA_Credits DECIMAL(18,2)
           ,@sCourseCredits DECIMAL(18,2)
           ,@sSimple_GPA_Credits DECIMAL(18,2);
        DECLARE @ComputedWeightedGPA DECIMAL(18,2)
           ,@ComputedSimpleGPA DECIMAL(18,2)
           ,@compareUnitTypeId UNIQUEIDENTIFIER;

/******************* GPA Calculations - Simple and Weighted Avg *********************/
        DECLARE @cumCourseCredits DECIMAL(18,2)
           ,@cumWeighted_GPA_Credits DECIMAL(18,2)
           ,@cumWeightedGPA DECIMAL(18,2);
        SET @cumWeightedGPA = 0;
        SET @cumCourseCredits = (
                                  SELECT    SUM(coursecredits)
                                  FROM      syCreditSummary
                                  WHERE     StuEnrollId = @StuEnrollId
                                            AND FinalGPA IS NOT NULL
								--and
								--(@TermId is null or TermId in (Select Val from [MultipleValuesForReportParameters](@TermId,',',1)))
                                            AND (
                                                  @TermStartDate IS NULL
                                                  OR @TermStartDateModifier IS NULL
                                                  OR (
                                                       (
                                                         ( @TermStartDateModifier <> '=' )
                                                         OR ( TermStartDate = @TermStartDate )
                                                       )
                                                       AND (
                                                             ( @TermStartDateModifier <> '>' )
                                                             OR ( TermStartDate > @TermStartDate )
                                                           )
                                                       AND (
                                                             ( @TermStartDateModifier <> '<' )
                                                             OR ( TermStartDate < @TermStartDate )
                                                           )
                                                       AND (
                                                             ( @TermStartDateModifier <> '>=' )
                                                             OR ( TermStartDate >= @TermStartDate )
                                                           )
                                                       AND (
                                                             ( @TermStartDateModifier <> '<=' )
                                                             OR ( TermStartDate <= @TermStartDate )
                                                           )
                                                     )
                                                )
                                );
        SET @cumWeighted_GPA_Credits = (
                                         SELECT SUM(coursecredits * FinalGPA)
                                         FROM   syCreditSummary
                                         WHERE  StuEnrollId = @StuEnrollId
                                                AND FinalGPA IS NOT NULL
										--and
										--(@TermId is null or TermId in (Select Val from [MultipleValuesForReportParameters](@TermId,',',1)))
                                                AND (
                                                      @TermStartDate IS NULL
                                                      OR @TermStartDateModifier IS NULL
                                                      OR (
                                                           (
                                                             ( @TermStartDateModifier <> '=' )
                                                             OR ( TermStartDate = @TermStartDate )
                                                           )
                                                           AND (
                                                                 ( @TermStartDateModifier <> '>' )
                                                                 OR ( TermStartDate > @TermStartDate )
                                                               )
                                                           AND (
                                                                 ( @TermStartDateModifier <> '<' )
                                                                 OR ( TermStartDate < @TermStartDate )
                                                               )
                                                           AND (
                                                                 ( @TermStartDateModifier <> '>=' )
                                                                 OR ( TermStartDate >= @TermStartDate )
                                                               )
                                                           AND (
                                                                 ( @TermStartDateModifier <> '<=' )
                                                                 OR ( TermStartDate <= @TermStartDate )
                                                               )
                                                         )
                                                    )
                                       );
					
        IF @cumCourseCredits >= 1
            BEGIN
                SET @cumWeightedGPA = @cumWeighted_GPA_Credits / @cumCourseCredits;
            END; 
			
--CumulativeSimpleGPA
        DECLARE @cumSimpleCourseCredits DECIMAL(18,2)
           ,@cumSimple_GPA_Credits DECIMAL(18,2)
           ,@cumSimpleGPA DECIMAL(18,2);
        SET @cumSimpleGPA = 0;
        SET @cumSimpleCourseCredits = (
                                        SELECT  COUNT(coursecredits)
                                        FROM    syCreditSummary
                                        WHERE   StuEnrollId = @StuEnrollId
                                                AND FinalGPA IS NOT NULL
										--and
										--(@TermId is null or TermId in (Select Val from [MultipleValuesForReportParameters](@TermId,',',1)))
                                                AND (
                                                      @TermStartDate IS NULL
                                                      OR @TermStartDateModifier IS NULL
                                                      OR (
                                                           (
                                                             ( @TermStartDateModifier <> '=' )
                                                             OR ( TermStartDate = @TermStartDate )
                                                           )
                                                           AND (
                                                                 ( @TermStartDateModifier <> '>' )
                                                                 OR ( TermStartDate > @TermStartDate )
                                                               )
                                                           AND (
                                                                 ( @TermStartDateModifier <> '<' )
                                                                 OR ( TermStartDate < @TermStartDate )
                                                               )
                                                           AND (
                                                                 ( @TermStartDateModifier <> '>=' )
                                                                 OR ( TermStartDate >= @TermStartDate )
                                                               )
                                                           AND (
                                                                 ( @TermStartDateModifier <> '<=' )
                                                                 OR ( TermStartDate <= @TermStartDate )
                                                               )
                                                         )
                                                    )
                                      );
        SET @cumSimple_GPA_Credits = (
                                       SELECT   SUM(FinalGPA)
                                       FROM     syCreditSummary
                                       WHERE    StuEnrollId = @StuEnrollId
                                                AND FinalGPA IS NOT NULL
										--and
										--(@TermId is null or TermId in (Select Val from [MultipleValuesForReportParameters](@TermId,',',1)))
                                                AND (
                                                      @TermStartDate IS NULL
                                                      OR @TermStartDateModifier IS NULL
                                                      OR (
                                                           (
                                                             ( @TermStartDateModifier <> '=' )
                                                             OR ( TermStartDate = @TermStartDate )
                                                           )
                                                           AND (
                                                                 ( @TermStartDateModifier <> '>' )
                                                                 OR ( TermStartDate > @TermStartDate )
                                                               )
                                                           AND (
                                                                 ( @TermStartDateModifier <> '<' )
                                                                 OR ( TermStartDate < @TermStartDate )
                                                               )
                                                           AND (
                                                                 ( @TermStartDateModifier <> '>=' )
                                                                 OR ( TermStartDate >= @TermStartDate )
                                                               )
                                                           AND (
                                                                 ( @TermStartDateModifier <> '<=' )
                                                                 OR ( TermStartDate <= @TermStartDate )
                                                               )
                                                         )
                                                    )
                                     );
			
        IF @cumSimpleCourseCredits >= 1
            BEGIN
                SET @cumSimpleGPA = @cumSimple_GPA_Credits / @cumSimpleCourseCredits;
            END; 
        DECLARE @SUMCreditsEarned DECIMAL(18,2)
           ,@SUMFACreditsEarned DECIMAL(18,2);
        SET @SUMCreditsEarned = 0;
        SET @SUMFACreditsEarned = 0;
        SET @SUMCreditsEarned = (
                                  SELECT    SUM(CreditsEarned)
                                  FROM      syCreditSummary
                                  WHERE     StuEnrollId = @StuEnrollId  
										--and
										--(@TermId is null or TermId in (Select Val from [MultipleValuesForReportParameters](@TermId,',',1)))
                                            AND (
                                                  @TermStartDate IS NULL
                                                  OR @TermStartDateModifier IS NULL
                                                  OR (
                                                       (
                                                         ( @TermStartDateModifier <> '=' )
                                                         OR ( TermStartDate = @TermStartDate )
                                                       )
                                                       AND (
                                                             ( @TermStartDateModifier <> '>' )
                                                             OR ( TermStartDate > @TermStartDate )
                                                           )
                                                       AND (
                                                             ( @TermStartDateModifier <> '<' )
                                                             OR ( TermStartDate < @TermStartDate )
                                                           )
                                                       AND (
                                                             ( @TermStartDateModifier <> '>=' )
                                                             OR ( TermStartDate >= @TermStartDate )
                                                           )
                                                       AND (
                                                             ( @TermStartDateModifier <> '<=' )
                                                             OR ( TermStartDate <= @TermStartDate )
                                                           )
                                                     )
                                                )
                                ); 

        SET @SUMFACreditsEarned = (
                                    SELECT  SUM(FACreditsEarned)
                                    FROM    syCreditSummary
                                    WHERE   StuEnrollId = @StuEnrollId  
										--and
										--(@TermId is null or TermId in (Select Val from [MultipleValuesForReportParameters](@TermId,',',1)))
                                            AND (
                                                  @TermStartDate IS NULL
                                                  OR @TermStartDateModifier IS NULL
                                                  OR (
                                                       (
                                                         ( @TermStartDateModifier <> '=' )
                                                         OR ( TermStartDate = @TermStartDate )
                                                       )
                                                       AND (
                                                             ( @TermStartDateModifier <> '>' )
                                                             OR ( TermStartDate > @TermStartDate )
                                                           )
                                                       AND (
                                                             ( @TermStartDateModifier <> '<' )
                                                             OR ( TermStartDate < @TermStartDate )
                                                           )
                                                       AND (
                                                             ( @TermStartDateModifier <> '>=' )
                                                             OR ( TermStartDate >= @TermStartDate )
                                                           )
                                                       AND (
                                                             ( @TermStartDateModifier <> '<=' )
                                                             OR ( TermStartDate <= @TermStartDate )
                                                           )
                                                     )
                                                )
                                  ); 


/******************* GPA Calculations - Simple and Weighted Avg - Ends Here *********************/

/************************** Attendance Calculations ********************************/
        DECLARE @Scheduledhours DECIMAL(18,2)
           ,@ActualPresentDays_ConvertTo_Hours DECIMAL(18,2);
        DECLARE @ActualAbsentDays_ConvertTo_Hours DECIMAL(18,2)
           ,@ActualTardyDays_ConvertTo_Hours DECIMAL(18,2);
        SET @Scheduledhours = 0;
        SET @Scheduledhours = (
                                SELECT  SUM(ScheduledHours)
                                FROM    syStudentAttendanceSummary
                                WHERE   StuEnrollId = @StuEnrollId 
								--and
								--(@TermId is null or TermId in (Select Val from [MultipleValuesForReportParameters](@TermId,',',1)))
                                        AND (
                                              @TermStartDate IS NULL
                                              OR @TermStartDateModifier IS NULL
                                              OR (
                                                   (
                                                     ( @TermStartDateModifier <> '=' )
                                                     OR ( TermStartDate = @TermStartDate )
                                                   )
                                                   AND (
                                                         ( @TermStartDateModifier <> '>' )
                                                         OR ( TermStartDate > @TermStartDate )
                                                       )
                                                   AND (
                                                         ( @TermStartDateModifier <> '<' )
                                                         OR ( TermStartDate < @TermStartDate )
                                                       )
                                                   AND (
                                                         ( @TermStartDateModifier <> '>=' )
                                                         OR ( TermStartDate >= @TermStartDate )
                                                       )
                                                   AND (
                                                         ( @TermStartDateModifier <> '<=' )
                                                         OR ( TermStartDate <= @TermStartDate )
                                                       )
                                                 )
                                            )
                              );

        SET @ActualPresentDays_ConvertTo_Hours = (
                                                   SELECT   SUM(ActualPresentDays_ConvertTo_Hours)
                                                   FROM     syStudentAttendanceSummary
                                                   WHERE    StuEnrollId = @StuEnrollId 
								--and
								--(@TermId is null or TermId in (Select Val from [MultipleValuesForReportParameters](@TermId,',',1)))
                                                            AND (
                                                                  @TermStartDate IS NULL
                                                                  OR @TermStartDateModifier IS NULL
                                                                  OR (
                                                                       (
                                                                         ( @TermStartDateModifier <> '=' )
                                                                         OR ( TermStartDate = @TermStartDate )
                                                                       )
                                                                       AND (
                                                                             ( @TermStartDateModifier <> '>' )
                                                                             OR ( TermStartDate > @TermStartDate )
                                                                           )
                                                                       AND (
                                                                             ( @TermStartDateModifier <> '<' )
                                                                             OR ( TermStartDate < @TermStartDate )
                                                                           )
                                                                       AND (
                                                                             ( @TermStartDateModifier <> '>=' )
                                                                             OR ( TermStartDate >= @TermStartDate )
                                                                           )
                                                                       AND (
                                                                             ( @TermStartDateModifier <> '<=' )
                                                                             OR ( TermStartDate <= @TermStartDate )
                                                                           )
                                                                     )
                                                                )
                                                 );
						
        SET @ActualAbsentDays_ConvertTo_Hours = (
                                                  SELECT    SUM(ActualAbsentDays_ConvertTo_Hours)
                                                  FROM      syStudentAttendanceSummary
                                                  WHERE     StuEnrollId = @StuEnrollId 
								--and
								--(@TermId is null or TermId in (Select Val from [MultipleValuesForReportParameters](@TermId,',',1)))
                                                            AND (
                                                                  @TermStartDate IS NULL
                                                                  OR @TermStartDateModifier IS NULL
                                                                  OR (
                                                                       (
                                                                         ( @TermStartDateModifier <> '=' )
                                                                         OR ( TermStartDate = @TermStartDate )
                                                                       )
                                                                       AND (
                                                                             ( @TermStartDateModifier <> '>' )
                                                                             OR ( TermStartDate > @TermStartDate )
                                                                           )
                                                                       AND (
                                                                             ( @TermStartDateModifier <> '<' )
                                                                             OR ( TermStartDate < @TermStartDate )
                                                                           )
                                                                       AND (
                                                                             ( @TermStartDateModifier <> '>=' )
                                                                             OR ( TermStartDate >= @TermStartDate )
                                                                           )
                                                                       AND (
                                                                             ( @TermStartDateModifier <> '<=' )
                                                                             OR ( TermStartDate <= @TermStartDate )
                                                                           )
                                                                     )
                                                                )
                                                );


        SET @ActualTardyDays_ConvertTo_Hours = (
                                                 SELECT SUM(ActualTardyDays_ConvertTo_Hours)
                                                 FROM   syStudentAttendanceSummary
                                                 WHERE  StuEnrollId = @StuEnrollId 
											--	and
											--(@TermId is null or TermId in (Select Val from [MultipleValuesForReportParameters](@TermId,',',1)))
                                                        AND (
                                                              @TermStartDate IS NULL
                                                              OR @TermStartDateModifier IS NULL
                                                              OR (
                                                                   (
                                                                     ( @TermStartDateModifier <> '=' )
                                                                     OR ( TermStartDate = @TermStartDate )
                                                                   )
                                                                   AND (
                                                                         ( @TermStartDateModifier <> '>' )
                                                                         OR ( TermStartDate > @TermStartDate )
                                                                       )
                                                                   AND (
                                                                         ( @TermStartDateModifier <> '<' )
                                                                         OR ( TermStartDate < @TermStartDate )
                                                                       )
                                                                   AND (
                                                                         ( @TermStartDateModifier <> '>=' )
                                                                         OR ( TermStartDate >= @TermStartDate )
                                                                       )
                                                                   AND (
                                                                         ( @TermStartDateModifier <> '<=' )
                                                                         OR ( TermStartDate <= @TermStartDate )
                                                                       )
                                                                 )
                                                            )
                                               );


--Average calculation
        DECLARE @termAverageSum DECIMAL(18,2)
           ,@CumAverage DECIMAL(18,2)
           ,@cumAverageSum DECIMAL(18,2)
           ,@cumAveragecount INT;
        DECLARE @TermAverageCount INT
           ,@TermAverage DECIMAL(18,2);		


        DECLARE @TardiesMakingAbsence INT
           ,@UnitTypeId UNIQUEIDENTIFIER
           ,@OriginalTardiesMakingAbsence INT;
        SET @TardiesMakingAbsence = (
                                      SELECT TOP 1
                                                t1.TardiesMakingAbsence
                                      FROM      arPrgVersions t1
                                               ,arStuEnrollments t2
                                      WHERE     t1.PrgVerId = t2.PrgVerId
                                                AND t2.StuEnrollId = @StuEnrollId
                                    );
							
        SET @UnitTypeId = (
                            SELECT TOP 1
                                    t1.UnitTypeId
                            FROM    arPrgVersions t1
                                   ,arStuEnrollments t2
                            WHERE   t1.PrgVerId = t2.PrgVerId
                                    AND t2.StuEnrollId = @StuEnrollId
                          );

        SET @OriginalTardiesMakingAbsence = (
                                              SELECT TOP 1
                                                        tardiesmakingabsence
                                              FROM      syStudentAttendanceSummary
                                              WHERE     StuEnrollId = @StuEnrollId
                                            );
        DECLARE @termstartdate1 DATETIME;

        DECLARE @MeetDate DATETIME
           ,@WeekDay VARCHAR(15)
           ,@StartDate DATETIME
           ,@EndDate DATETIME;
        DECLARE @PeriodDescrip VARCHAR(50)
           ,@Actual DECIMAL(18,2)
           ,@Excused DECIMAL(18,2)
           ,@ClsSectionId UNIQUEIDENTIFIER;
        DECLARE @Absent DECIMAL(18,2)
           ,@SchedHours DECIMAL(18,2)
           ,@TardyMinutes DECIMAL(18,2);
        DECLARE @tardy DECIMAL(18,2)
           ,@tracktardies INT
           ,@rownumber INT
           ,@IsTardy BIT
           ,@ActualRunningScheduledHours DECIMAL(18,2);
        DECLARE @ActualRunningPresentHours DECIMAL(18,2)
           ,@ActualRunningAbsentHours DECIMAL(18,2)
           ,@ActualRunningTardyHours DECIMAL(18,2)
           ,@ActualRunningMakeupHours DECIMAL(18,2);
        DECLARE @PrevStuEnrollId UNIQUEIDENTIFIER
           ,@intTardyBreakPoint INT
           ,@AdjustedRunningPresentHours DECIMAL(18,2)
           ,@AdjustedRunningAbsentHours DECIMAL(18,2)
           ,@ActualRunningScheduledDays DECIMAL(18,2);
		--declare @Scheduledhours decimal(18,2),@ActualPresentDays_ConvertTo_Hours decimal(18,2),@ActualAbsentDays_ConvertTo_Hours decimal(18,2),@AttendanceTrack varchar(50)
        DECLARE @TermDescrip VARCHAR(50)
           ,@PrgVerId UNIQUEIDENTIFIER;
        DECLARE @TardyHit VARCHAR(10)
           ,@ScheduledMinutes DECIMAL(18,2);
        DECLARE @Scheduledhours_noperiods DECIMAL(18,2)
           ,@ActualPresentDays_ConvertTo_Hours_NoPeriods DECIMAL(18,2)
           ,@ActualAbsentDays_ConvertTo_Hours_NoPeriods DECIMAL(18,2);
        DECLARE @ActualTardyDays_ConvertTo_Hours_NoPeriods DECIMAL(18,2);
		
--Select * from arAttUnitType
		-- By Class and PA, NOne
        IF (
             @UnitTypeId = '2600592A-9739-4A13-BDCE-7A25FE4A7478'
             OR @UnitTypeId = 'EF5535C2-142C-4223-AE3C-25A50A153CC6'
           )
            AND LOWER(RTRIM(LTRIM(@TrackAttendanceBy))) = 'byclass'
            BEGIN
			--Print 'Step1!!!!'
                DELETE  FROM syStudentAttendanceSummary
                WHERE   StuEnrollId = @StuEnrollId;
                DECLARE @boolReset BIT
                   ,@MakeupHours DECIMAL(18,2)
                   ,@AdjustedPresentDaysComputed DECIMAL(18,2);
                DECLARE GetAttendance_Cursor CURSOR FORWARD_ONLY FAST_FORWARD
                FOR
                    SELECT  *
                           ,ROW_NUMBER() OVER ( ORDER BY MeetDate ) AS RowNumber
                    FROM    (
                              SELECT DISTINCT
                                        t1.StuEnrollId
                                       ,t1.ClsSectionId
                                       ,t1.MeetDate
                                       ,DATENAME(dw,t1.MeetDate) AS WeekDay
                                       ,t4.StartDate
                                       ,t4.EndDate
                                       ,t5.PeriodDescrip
                                       ,t1.Actual
                                       ,t1.Excused
                                       ,CASE WHEN (
                                                    t1.Actual = 0
                                                    AND t1.Excused = 0
                                                  ) THEN t1.Scheduled
                                             ELSE CASE WHEN (
                                                              t1.Actual <> 9999.00
                                                              AND t1.Actual < t1.Scheduled
                                                              AND t1.Excused <> 1
                                                            ) THEN ( t1.Scheduled - t1.Actual )
                                                       ELSE 0
                                                  END
                                        END AS Absent
                                       ,t1.Scheduled AS ScheduledMinutes
                                       ,CASE WHEN (
                                                    t1.Actual > 0
                                                    AND t1.Actual < t1.Scheduled
                                                  ) THEN ( t1.Scheduled - t1.Actual )
                                             ELSE 0
                                        END AS TardyMinutes
                                       ,t1.Tardy AS Tardy
                                       ,t3.TrackTardies
                                       ,t3.TardiesMakingAbsence
                                       ,t3.PrgVerId
                              FROM      atClsSectAttendance t1
                              INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                              INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                              INNER JOIN arClsSectMeetings t4 ON t1.ClsSectionId = t4.ClsSectionId
                                                                 AND (
                                                                       CONVERT(DATE,t1.MeetDate,111) >= CONVERT(DATE,t4.StartDate,111)
                                                                       AND CONVERT(DATE,t1.MeetDate,111) <= CONVERT(DATE,t4.EndDate,111)
                                                                     )
                                                                 AND t1.ClsSectMeetingId = t4.ClsSectMeetingId --DE8479 line added
                              INNER JOIN syPeriods t5 ON t4.PeriodId = t5.PeriodId
                              INNER JOIN cmTimeInterval t6 ON t5.StartTimeId = t6.TimeIntervalId
                              INNER JOIN cmTimeInterval t7 ON t5.EndTimeId = t7.TimeIntervalId
                              INNER JOIN dbo.arClassSections t9 ON t9.ClsSectionId = t1.ClsSectionId
                              INNER JOIN dbo.arReqs t10 ON t10.ReqId = t9.ReqId
                              WHERE     t2.StuEnrollId = @StuEnrollId
                                        AND (
                                              t10.UnitTypeId IN ( '2600592A-9739-4A13-BDCE-7A25FE4A7478','EF5535C2-142C-4223-AE3C-25A50A153CC6' )
                                              OR t3.UnitTypeId IN ( '2600592A-9739-4A13-BDCE-7A25FE4A7478','EF5535C2-142C-4223-AE3C-25A50A153CC6' )
                                            ) -- Minutes
                                        AND t1.Actual <> 9999
                            ) dt
                    ORDER BY StuEnrollId
                           ,MeetDate;
                OPEN GetAttendance_Cursor;
                FETCH NEXT FROM GetAttendance_Cursor INTO @StuEnrollId,@ClsSectionId,@MeetDate,@WeekDay,@StartDate,@EndDate,@PeriodDescrip,@Actual,@Excused,
                    @Absent,@ScheduledMinutes,@TardyMinutes,@tardy,@tracktardies,@TardiesMakingAbsence,@PrgVerId,@rownumber;
                SET @ActualRunningPresentHours = 0;
                SET @ActualRunningPresentHours = 0;
                SET @ActualRunningAbsentHours = 0;
                SET @ActualRunningTardyHours = 0;
                SET @ActualRunningMakeupHours = 0;
                SET @intTardyBreakPoint = 0;
                SET @AdjustedRunningPresentHours = 0;
                SET @AdjustedRunningAbsentHours = 0;
                SET @ActualRunningScheduledDays = 0;
                SET @boolReset = 0;
                SET @MakeupHours = 0;
                WHILE @@FETCH_STATUS = 0
                    BEGIN

                        IF @PrevStuEnrollId <> @StuEnrollId
                            BEGIN
                                SET @ActualRunningPresentHours = 0;
                                SET @ActualRunningAbsentHours = 0;
                                SET @intTardyBreakPoint = 0;
                                SET @ActualRunningTardyHours = 0;
                                SET @AdjustedRunningPresentHours = 0;
                                SET @AdjustedRunningAbsentHours = 0;
                                SET @ActualRunningScheduledDays = 0;
                                SET @MakeupHours = 0;
                                SET @boolReset = 1;
                            END;

	                  
					-- Scheduled and Actual: Calculate Students Actual and Adjusted Running Present Hours/Min/Day
                        IF @Actual <> 9999.00
                            BEGIN
                                SET @ActualRunningPresentHours = @ActualRunningPresentHours + @Actual + @Excused;
                                SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours + @Actual + @Excused;
		                    
							-- If there are make up hrs deduct that otherwise it will be added again in progress report
                                IF (
                                     @Actual > 0
                                     AND @Actual > @ScheduledMinutes
                                     AND @Actual <> 9999.00
                                   )
                                    BEGIN
                                        SET @ActualRunningPresentHours = @ActualRunningPresentHours - ( @Actual - @ScheduledMinutes );
                                        SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - ( @Actual - @ScheduledMinutes );
                                    END;
		                      
                                SET @ActualRunningScheduledDays = @ActualRunningScheduledDays + @ScheduledMinutes;                      
                            END;
		           
					-- Absent: Calculate Students Actual and Adjusted Running Absent Hours/Min/Day
                        SET @ActualRunningAbsentHours = @ActualRunningAbsentHours + @Absent;
                        SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Absent;	
		      
					-- Tardy: Calculate Students Actual Running Tardy Hours/Min/Day			
                        IF (
                             @Actual > 0
                             AND @Actual < @ScheduledMinutes
                             AND @tardy = 1
                           )
                            BEGIN
                                SET @ActualRunningTardyHours = @ActualRunningTardyHours + ( @ScheduledMinutes - @Actual );
                            END;
							
					-- Track how many days student has been tardy only when 
					-- program version requires to track tardy
                        IF (
                             @tracktardies = 1
                             AND @tardy = 1
                           )
                            BEGIN
                                SET @intTardyBreakPoint = @intTardyBreakPoint + 1;
                            END;	    
           
            
					-- Tardy: If student is tardy multiple times and when the number of days student is tardy matches
					-- the tardy rule set (for ex: 2 Tardies make 1 Absence). This rule indicates that
					-- when student is tardy the second time, that second occurance will be considered as
					-- absence
					-- Variable @intTardyBreakpoint tracks how many times the student was tardy
					-- Variable @tardiesMakingAbsence tracks the tardy rule
                        IF (
                             @tracktardies = 1
                             AND @intTardyBreakPoint = @TardiesMakingAbsence
                           )
                            BEGIN
                                SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - @Actual - @Excused;
                                SET @AdjustedRunningAbsentHours = ( @AdjustedRunningAbsentHours - @Absent ) + @ScheduledMinutes; --@TardyMinutes
                                SET @intTardyBreakPoint = 0;
                            END;
		           
				   -- Makeup: Calculate Makeup Hours when Student Attended more than scheduled hours          	       
                        IF (
                             @Actual > 0
                             AND @Actual > @ScheduledMinutes
                             AND @Actual <> 9999.00
                           )
                            BEGIN
                                SET @MakeupHours = @MakeupHours + ( @Actual - @ScheduledMinutes );
                            END;
			  
                        IF ( @tracktardies = 1 )
                            BEGIN
                                SET @AdjustedPresentDaysComputed = @AdjustedRunningPresentHours; --+IsNULL(@ActualRunningTardyHours,0)
                            END;
                        ELSE
                            BEGIN
                                SET @AdjustedPresentDaysComputed = @AdjustedRunningPresentHours;
                            END;
				
				--Print @MeetDate
				--Print @ActualRunningAbsentHours
				
                        DELETE  FROM syStudentAttendanceSummary
                        WHERE   StuEnrollId = @StuEnrollId
                                AND ClsSectionId = @ClsSectionId
                                AND StudentAttendedDate = @MeetDate;
                        INSERT  INTO syStudentAttendanceSummary
                                (
                                 StuEnrollId
                                ,ClsSectionId
                                ,StudentAttendedDate
                                ,ScheduledDays
                                ,ActualDays
                                ,ActualRunningScheduledDays
                                ,ActualRunningPresentDays
                                ,ActualRunningAbsentDays
                                ,ActualRunningMakeupDays
                                ,ActualRunningTardyDays
                                ,AdjustedPresentDays
                                ,AdjustedAbsentDays
                                ,AttendanceTrackType
                                ,ModUser
                                ,ModDate
                                ,tardiesmakingabsence
                                )
                        VALUES  (
                                 @StuEnrollId
                                ,@ClsSectionId
                                ,@MeetDate
                                ,@ScheduledMinutes
                                ,@Actual
                                ,@ActualRunningScheduledDays
                                ,@ActualRunningPresentHours
                                ,@ActualRunningAbsentHours
                                ,ISNULL(@MakeupHours,0)
                                ,@ActualRunningTardyHours
                                ,@AdjustedPresentDaysComputed
                                ,@AdjustedRunningAbsentHours
                                ,'Post Attendance by Class Min'
                                ,'sa'
                                ,GETDATE()
                                ,@TardiesMakingAbsence
                                );

				--update syStudentAttendanceSummary set TardiesMakingAbsence=@TardiesMakingAbsence where StuEnrollId=@StuEnrollId
                        SET @PrevStuEnrollId = @StuEnrollId; 

                        FETCH NEXT FROM GetAttendance_Cursor INTO @StuEnrollId,@ClsSectionId,@MeetDate,@WeekDay,@StartDate,@EndDate,@PeriodDescrip,@Actual,
                            @Excused,@Absent,@ScheduledMinutes,@TardyMinutes,@tardy,@tracktardies,@TardiesMakingAbsence,@PrgVerId,@rownumber;
                    END;
                CLOSE GetAttendance_Cursor;
                DEALLOCATE GetAttendance_Cursor;
            END;			
		
		-- By Class and Attendance Unit Type - Minutes and Clock Hour
		
        IF (
             (
               @UnitTypeId = 'A1389C74-0BB9-4BBF-A47F-68428BE7FA4D'
               OR @UnitTypeId = 'B937C92E-FD7A-455E-A731-527A9918C734'
             )
             AND LOWER(@TrackAttendanceBy) = 'byclass'
           )
            BEGIN
                DELETE  FROM syStudentAttendanceSummary
                WHERE   StuEnrollId = @StuEnrollId;
				
                DECLARE GetAttendance_Cursor CURSOR
                FOR
                    SELECT  *
                           ,ROW_NUMBER() OVER ( ORDER BY MeetDate ) AS RowNumber
                    FROM    (
                              SELECT DISTINCT
                                        t1.StuEnrollId
                                       ,t1.ClsSectionId
                                       ,t1.MeetDate
                                       ,DATENAME(dw,t1.MeetDate) AS WeekDay
                                       ,t4.StartDate
                                       ,t4.EndDate
                                       ,t5.PeriodDescrip
                                       ,t1.Actual
                                       ,t1.Excused
                                       ,CASE WHEN (
                                                    t1.Actual = 0
                                                    AND t1.Excused = 0
                                                  ) THEN t1.Scheduled
                                             ELSE CASE WHEN (
                                                              t1.Actual <> 9999.00
                                                              AND t1.Actual < t1.Scheduled
                                                            ) THEN ( t1.Scheduled - t1.Actual )
                                                       ELSE 0
                                                  END
                                        END AS Absent
                                       ,t1.Scheduled AS ScheduledMinutes
                                       ,CASE WHEN (
                                                    t1.Actual > 0
                                                    AND t1.Actual < t1.Scheduled
                                                  ) THEN ( t1.Scheduled - t1.Actual )
                                             ELSE 0
                                        END AS TardyMinutes
                                       ,t1.Tardy AS Tardy
                                       ,t3.TrackTardies
                                       ,t3.TardiesMakingAbsence
                                       ,t3.PrgVerId
                              FROM      atClsSectAttendance t1
                              INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                              INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                              INNER JOIN arClsSectMeetings t4 ON t1.ClsSectionId = t4.ClsSectionId
                                                                 AND (
                                                                       CONVERT(DATE,t1.MeetDate,111) >= CONVERT(DATE,t4.StartDate,111)
                                                                       AND CONVERT(DATE,t1.MeetDate,111) <= CONVERT(DATE,t4.EndDate,111)
                                                                     )
                                                                 AND t1.ClsSectMeetingId = t4.ClsSectMeetingId --DE8479 line added
                              INNER JOIN syPeriods t5 ON t4.PeriodId = t5.PeriodId
                              INNER JOIN cmTimeInterval t6 ON t5.StartTimeId = t6.TimeIntervalId
                              INNER JOIN cmTimeInterval t7 ON t5.EndTimeId = t7.TimeIntervalId
                              INNER JOIN dbo.arClassSections t9 ON t9.ClsSectionId = t1.ClsSectionId
                              INNER JOIN dbo.arReqs t10 ON t10.ReqId = t9.ReqId
                              WHERE     t2.StuEnrollId = @StuEnrollId
                                        AND (
                                              t10.UnitTypeId IN ( 'A1389C74-0BB9-4BBF-A47F-68428BE7FA4D','B937C92E-FD7A-455E-A731-527A9918C734' )
                                              OR t3.UnitTypeId IN ( 'A1389C74-0BB9-4BBF-A47F-68428BE7FA4D','B937C92E-FD7A-455E-A731-527A9918C734' )
                                            ) -- Minutes
                                        AND t1.Actual <> 9999
                              UNION
                              SELECT DISTINCT
                                        t1.StuEnrollId
                                       ,NULL AS ClsSectionId
                                       ,t1.RecordDate AS MeetDate
                                       ,DATENAME(dw,t1.RecordDate) AS WeekDay
                                       ,NULL AS StartDate
                                       ,NULL AS EndDate
                                       ,NULL AS PeriodDescrip
                                       ,t1.ActualHours
                                       ,NULL AS Excused
                                       ,CASE WHEN ( t1.ActualHours = 0 ) THEN t1.SchedHours
                                             ELSE 0
								 --ELSE 
									--Case when (t1.ActualHours <> 9999.00 and t1.ActualHours < t1.SchedHours)
									--		THEN (t1.SchedHours - t1.ActualHours)
									--		ELSE 
									--			0
									--		End
                                        END AS Absent
                                       ,t1.SchedHours AS ScheduledMinutes
                                       ,CASE WHEN (
                                                    t1.ActualHours <> 9999.00
                                                    AND t1.ActualHours > 0
                                                    AND t1.ActualHours < t1.SchedHours
                                                  ) THEN ( t1.SchedHours - t1.ActualHours )
                                             ELSE 0
                                        END AS TardyMinutes
                                       ,NULL AS Tardy
                                       ,t3.TrackTardies
                                       ,t3.TardiesMakingAbsence
                                       ,t3.PrgVerId
                              FROM      arStudentClockAttendance t1
                              INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                              INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                              WHERE     t2.StuEnrollId = @StuEnrollId
                                        AND t1.Converted = 1
                                        AND t1.ActualHours <> 9999
                            ) dt
                    ORDER BY StuEnrollId
                           ,MeetDate;
                OPEN GetAttendance_Cursor;
                FETCH NEXT FROM GetAttendance_Cursor INTO @StuEnrollId,@ClsSectionId,@MeetDate,@WeekDay,@StartDate,@EndDate,@PeriodDescrip,@Actual,@Excused,
                    @Absent,@ScheduledMinutes,@TardyMinutes,@tardy,@tracktardies,@TardiesMakingAbsence,@PrgVerId,@rownumber;
                SET @ActualRunningPresentHours = 0;
                SET @ActualRunningPresentHours = 0;
                SET @ActualRunningAbsentHours = 0;
                SET @ActualRunningTardyHours = 0;
                SET @ActualRunningMakeupHours = 0;
                SET @intTardyBreakPoint = 0;
                SET @AdjustedRunningPresentHours = 0;
                SET @AdjustedRunningAbsentHours = 0;
                SET @ActualRunningScheduledDays = 0;
                SET @boolReset = 0;
                SET @MakeupHours = 0;
                WHILE @@FETCH_STATUS = 0
                    BEGIN

                        IF @PrevStuEnrollId <> @StuEnrollId
                            BEGIN
                                SET @ActualRunningPresentHours = 0;
                                SET @ActualRunningAbsentHours = 0;
                                SET @intTardyBreakPoint = 0;
                                SET @ActualRunningTardyHours = 0;
                                SET @AdjustedRunningPresentHours = 0;
                                SET @AdjustedRunningAbsentHours = 0;
                                SET @ActualRunningScheduledDays = 0;
                                SET @MakeupHours = 0;
                                SET @boolReset = 1;
                            END;

	                  
            -- Scheduled and Actual: Calculate Students Actual and Adjusted Running Present Hours/Min/Day
                        IF @Actual <> 9999.00
                            BEGIN
                                SET @ActualRunningPresentHours = @ActualRunningPresentHours + @Actual;
                                SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours + @Actual; 
                    -- If there are make up hrs deduct that otherwise it will be added again in progress report
                                IF (
                                     @Actual > 0
                                     AND @Actual > @ScheduledMinutes
                                     AND @Actual <> 9999.00
                                   )
                                    BEGIN
                                        SET @ActualRunningPresentHours = @ActualRunningPresentHours - ( @Actual - @ScheduledMinutes );
                                        SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - ( @Actual - @ScheduledMinutes );
                                    END; 
                                SET @ActualRunningScheduledDays = @ActualRunningScheduledDays + @ScheduledMinutes;                      
                            END;
           
			-- Absent: Calculate Students Actual and Adjusted Running Absent Hours/Min/Day
                        SET @ActualRunningAbsentHours = @ActualRunningAbsentHours + @Absent;
                        SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Absent;	
      
			-- Tardy: Calculate Students Actual Running Tardy Hours/Min/Day			
                        IF (
                             @Actual > 0
                             AND @Actual < @ScheduledMinutes
                             AND @tardy = 1
                           )
                            BEGIN
                                SET @ActualRunningTardyHours = @ActualRunningTardyHours + ( @ScheduledMinutes - @Actual );
                            END;
					
			-- Track how many days student has been tardy only when 
			-- program version requires to track tardy
                        IF (
                             @tracktardies = 1
                             AND @tardy = 1
                           )
                            BEGIN
                                SET @intTardyBreakPoint = @intTardyBreakPoint + 1;
                            END;	    
           
            
            -- Tardy: If student is tardy multiple times and when the number of days student is tardy matches
            -- the tardy rule set (for ex: 2 Tardies make 1 Absence). This rule indicates that
            -- when student is tardy the second time, that second occurance will be considered as
            -- absence
            -- Variable @intTardyBreakpoint tracks how many times the student was tardy
            -- Variable @tardiesMakingAbsence tracks the tardy rule
                        IF (
                             @tracktardies = 1
                             AND @intTardyBreakPoint = @TardiesMakingAbsence
                           )
                            BEGIN
                                SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - @Actual;
                                SET @AdjustedRunningAbsentHours = ( @AdjustedRunningAbsentHours - @Absent ) + @ScheduledMinutes; --@TardyMinutes
                                SET @intTardyBreakPoint = 0;
                            END;
           
           -- Makeup: Calculate Makeup Hours when Student Attended more than scheduled hours          	       
                        IF (
                             @Actual > 0
                             AND @Actual > @ScheduledMinutes
                             AND @Actual <> 9999.00
                           )
                            BEGIN
                                SET @MakeupHours = @MakeupHours + ( @Actual - @ScheduledMinutes );
                            END;
	  
	 
                        IF ( @tracktardies = 1 )
                            BEGIN
                                SET @AdjustedPresentDaysComputed = @AdjustedRunningPresentHours; --+IsNULL(@ActualRunningTardyHours,0)
                            END;
                        ELSE
                            BEGIN
                                SET @AdjustedPresentDaysComputed = @AdjustedRunningPresentHours;
                            END;
		
		
		
		
                        DELETE  FROM syStudentAttendanceSummary
                        WHERE   StuEnrollId = @StuEnrollId
                                AND ClsSectionId = @ClsSectionId
                                AND StudentAttendedDate = @MeetDate;
                        INSERT  INTO syStudentAttendanceSummary
                                (
                                 StuEnrollId
                                ,ClsSectionId
                                ,StudentAttendedDate
                                ,ScheduledDays
                                ,ActualDays
                                ,ActualRunningScheduledDays
                                ,ActualRunningPresentDays
                                ,ActualRunningAbsentDays
                                ,ActualRunningMakeupDays
                                ,ActualRunningTardyDays
                                ,AdjustedPresentDays
                                ,AdjustedAbsentDays
                                ,AttendanceTrackType
                                ,ModUser
                                ,ModDate
                                )
                        VALUES  (
                                 @StuEnrollId
                                ,@ClsSectionId
                                ,@MeetDate
                                ,@ScheduledMinutes
                                ,@Actual
                                ,@ActualRunningScheduledDays
                                ,@ActualRunningPresentHours
                                ,@ActualRunningAbsentHours
                                ,ISNULL(@MakeupHours,0)
                                ,@ActualRunningTardyHours
                                ,@AdjustedPresentDaysComputed
                                ,@AdjustedRunningAbsentHours
                                ,'Post Attendance by Class Min'
                                ,'sa'
                                ,GETDATE()
                                );

                        UPDATE  syStudentAttendanceSummary
                        SET     tardiesmakingabsence = @TardiesMakingAbsence
                        WHERE   StuEnrollId = @StuEnrollId;
                        SET @PrevStuEnrollId = @StuEnrollId; 

                        FETCH NEXT FROM GetAttendance_Cursor INTO @StuEnrollId,@ClsSectionId,@MeetDate,@WeekDay,@StartDate,@EndDate,@PeriodDescrip,@Actual,
                            @Excused,@Absent,@ScheduledMinutes,@TardyMinutes,@tardy,@tracktardies,@TardiesMakingAbsence,@PrgVerId,@rownumber;
                    END;
                CLOSE GetAttendance_Cursor;
                DEALLOCATE GetAttendance_Cursor;
            END;
--Select * from arAttUnitType

			
-- By Minutes/Day
-- remove clock hour from here
        IF ( @UnitTypeId = 'A1389C74-0BB9-4BBF-A47F-68428BE7FA4D' )
            AND LOWER(@TrackAttendanceBy) = 'byday'
            PRINT GETDATE();	
        PRINT @UnitTypeId;
        PRINT @TrackAttendanceBy;
	
        BEGIN
			--Delete from syStudentAttendanceSummary where StuEnrollId=@StuEnrollId

            DECLARE GetAttendance_Cursor CURSOR FORWARD_ONLY FAST_FORWARD
            FOR
                SELECT  t1.StuEnrollId
                       ,NULL AS ClsSectionId
                       ,t1.RecordDate AS MeetDate
                       ,t1.ActualHours
                       ,t1.SchedHours AS ScheduledMinutes
                       ,CASE WHEN (
                                    (
                                      t1.SchedHours >= 1
                                      AND t1.SchedHours NOT IN ( 999,9999 )
                                    )
                                    AND t1.ActualHours = 0
                                  ) THEN t1.SchedHours
                             ELSE 0
                        END AS Absent
                       ,t1.isTardy
                       ,(
                          SELECT    ISNULL(SUM(SchedHours),0)
                          FROM      arStudentClockAttendance
                          WHERE     StuEnrollId = t1.StuEnrollId
                                    AND RecordDate <= t1.RecordDate
                                    AND (
                                          t1.SchedHours >= 1
                                          AND t1.SchedHours NOT IN ( 999,9999 )
                                          AND t1.ActualHours NOT IN ( 999,9999 )
                                        )
                        ) AS ActualRunningScheduledHours
                       ,(
                          SELECT    SUM(ActualHours)
                          FROM      arStudentClockAttendance
                          WHERE     StuEnrollId = t1.StuEnrollId
                                    AND RecordDate <= t1.RecordDate
                                    AND (
                                          t1.SchedHours >= 1
                                          AND t1.SchedHours NOT IN ( 999,9999 )
                                        )
                                    AND ActualHours >= 1
                                    AND ActualHours NOT IN ( 999,9999 )
                        ) AS ActualRunningPresentHours
                       ,(
                          SELECT    COUNT(ActualHours)
                          FROM      arStudentClockAttendance
                          WHERE     StuEnrollId = t1.StuEnrollId
                                    AND RecordDate <= t1.RecordDate
                                    AND (
                                          t1.SchedHours >= 1
                                          AND t1.SchedHours NOT IN ( 999,9999 )
                                        )
                                    AND ActualHours = 0
                                    AND ActualHours NOT IN ( 999,9999 )
                        ) AS ActualRunningAbsentHours
                       ,(
                          SELECT    SUM(ActualHours)
                          FROM      arStudentClockAttendance
                          WHERE     StuEnrollId = t1.StuEnrollId
                                    AND RecordDate <= t1.RecordDate
                                    AND SchedHours = 0
                                    AND ActualHours >= 1
                                    AND ActualHours NOT IN ( 999,9999 )
                        ) AS ActualRunningMakeupHours
                       ,(
                          SELECT    SUM(SchedHours - ActualHours)
                          FROM      arStudentClockAttendance
                          WHERE     StuEnrollId = t1.StuEnrollId
                                    AND RecordDate <= t1.RecordDate
                                    AND (
                                          (
                                            t1.SchedHours >= 1
                                            AND t1.SchedHours NOT IN ( 999,9999 )
                                          )
                                          AND ActualHours >= 1
                                          AND ActualHours NOT IN ( 999,9999 )
                                        )
                                    AND isTardy = 1
                        ) AS ActualRunningTardyHours
                       ,t3.TrackTardies
                       ,t3.TardiesMakingAbsence
                       ,t3.PrgVerId
                       ,ROW_NUMBER() OVER ( ORDER BY t1.RecordDate ) AS RowNumber
                FROM    arStudentClockAttendance t1
                INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId 
			--inner join Inserted t4 on t1.StuEnrollId = t4.StuEnrollId and t1.RecordDate=t4.RecordDate
                WHERE   t3.UnitTypeId IN ( 'A1389C74-0BB9-4BBF-A47F-68428BE7FA4D' )
                        AND t2.StuEnrollId = @StuEnrollId
                        AND t1.ActualHours <> 9999.00
                ORDER BY t1.StuEnrollId
                       ,MeetDate;
            OPEN GetAttendance_Cursor;
--Declare @ActualHours decimal(18,2),@SchedHours decimal(18,2)
            FETCH NEXT FROM GetAttendance_Cursor INTO @StuEnrollId,@ClsSectionId,@MeetDate,@Actual,@ScheduledMinutes,@Absent,@IsTardy,
                @ActualRunningScheduledHours,@ActualRunningPresentHours,@ActualRunningAbsentHours,@ActualRunningMakeupHours,@ActualRunningTardyHours,
                @tracktardies,@TardiesMakingAbsence,@PrgVerId,@rownumber;

            SET @ActualRunningPresentHours = 0;
            SET @ActualRunningPresentHours = 0;
            SET @ActualRunningAbsentHours = 0;
            SET @ActualRunningTardyHours = 0;
            SET @ActualRunningMakeupHours = 0;
            SET @intTardyBreakPoint = 0;
            SET @AdjustedRunningPresentHours = 0;
            SET @AdjustedRunningAbsentHours = 0;
            SET @ActualRunningScheduledDays = 0;
            WHILE @@FETCH_STATUS = 0
                BEGIN

                    IF @PrevStuEnrollId <> @StuEnrollId
                        BEGIN
                            SET @ActualRunningPresentHours = 0;
                            SET @ActualRunningAbsentHours = 0;
                            SET @intTardyBreakPoint = 0;
                            SET @ActualRunningTardyHours = 0;
                            SET @AdjustedRunningPresentHours = 0;
                            SET @AdjustedRunningAbsentHours = 0;
                            SET @ActualRunningScheduledDays = 0;
                        END;

                    IF (
                         @ScheduledMinutes >= 1
                         AND (
                               @Actual <> 9999
                               AND @Actual <> 999
                             )
                         AND (
                               @ScheduledMinutes <> 9999
                               AND @Actual <> 999
                             )
                       )
                        BEGIN
                            SET @ActualRunningScheduledDays = ISNULL(@ActualRunningScheduledDays,0) + ISNULL(@ScheduledMinutes,0);
                        END;
                    ELSE
                        BEGIN
                            SET @ActualRunningScheduledDays = ISNULL(@ActualRunningScheduledDays,0); 
                        END;
	   
                    IF (
                         @Actual <> 9999
                         AND @Actual <> 999
                       )
                        BEGIN
                            SET @ActualRunningPresentHours = @ActualRunningPresentHours + @Actual;
                        END;
                    SET @ActualRunningAbsentHours = @ActualRunningAbsentHours + @Absent;

                    IF (
                         @Actual > 0
                         AND @Actual < @ScheduledMinutes
                       )
                        BEGIN
                            SET @ActualRunningAbsentHours = @ActualRunningAbsentHours + ( @ScheduledMinutes - @Actual );
                        END;
			-- Make up hours
		--sched=5, Actual =7, makeup= 2,ab = 0
		--sched=0, Actual =7, makeup= 7,ab = 0
                    IF (
                         @Actual > 0
                         AND @ScheduledMinutes > 0
                         AND @Actual > @ScheduledMinutes
                         AND (
                               @Actual <> 9999
                               AND @Actual <> 999
                             )
                       )
                        BEGIN
                            SET @ActualRunningMakeupHours = @ActualRunningMakeupHours + ( @Actual - @ScheduledMinutes );
                        END;

		
                    IF (
                         @Actual <> 9999
                         AND @Actual <> 999
                       )
                        BEGIN
                            SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours + @Actual;
                        END;	
                    IF (
                         @Actual = 0
                         AND @ScheduledMinutes >= 1
                         AND @ScheduledMinutes NOT IN ( 999,9999 )
                       )
                        BEGIN
                            SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Absent;
                        END;
		-- Absent hours
		--1. sched = 5, Actual = 2 then Ab = (5-3) = 2
                    IF (
                         @Absent = 0
                         AND @ActualRunningAbsentHours > 0
                         AND ( @Actual < @ScheduledMinutes )
                       )
                        BEGIN
                            SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + ( @ScheduledMinutes - @Actual );	
                        END;
                    IF (
                         @Actual > 0
                         AND @Actual < @ScheduledMinutes
                         AND (
                               @Actual <> 9999.00
                               AND @Actual <> 999.00
                             )
                       )
                        BEGIN
                            SET @ActualRunningTardyHours = @ActualRunningTardyHours + ( @ScheduledMinutes - @Actual );
                        END;
	
                    IF @tracktardies = 1
                        AND (
                              @TardyMinutes > 0
                              OR @IsTardy = 1
                            )
                        BEGIN
                            SET @intTardyBreakPoint = @intTardyBreakPoint + 1;
                        END;	    
                    IF (
                         @tracktardies = 1
                         AND @intTardyBreakPoint = @TardiesMakingAbsence
                       )
                        BEGIN
                            SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - @Actual;
                            SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Actual; --@TardyMinutes
                            SET @intTardyBreakPoint = 0;
                        END;
                    DELETE  FROM syStudentAttendanceSummary
                    WHERE   StuEnrollId = @StuEnrollId
                            AND StudentAttendedDate = @MeetDate;
                    INSERT  INTO syStudentAttendanceSummary
                            (
                             StuEnrollId
                            ,ClsSectionId
                            ,StudentAttendedDate
                            ,ScheduledDays
                            ,ActualDays
                            ,ActualRunningScheduledDays
                            ,ActualRunningPresentDays
                            ,ActualRunningAbsentDays
                            ,ActualRunningMakeupDays
                            ,ActualRunningTardyDays
                            ,AdjustedPresentDays
                            ,AdjustedAbsentDays
                            ,AttendanceTrackType
                            ,ModUser
                            ,ModDate
                            )
                    VALUES  (
                             @StuEnrollId
                            ,@ClsSectionId
                            ,@MeetDate
                            ,@ScheduledMinutes
                            ,@Actual
                            ,@ActualRunningScheduledDays
                            ,@ActualRunningPresentHours
                            ,@ActualRunningAbsentHours
                            ,@ActualRunningMakeupHours
                            ,@ActualRunningTardyHours
                            ,@AdjustedRunningPresentHours
                            ,@AdjustedRunningAbsentHours
                            ,'Post Attendance by Class'
                            ,'sa'
                            ,GETDATE()
                            );
				
                    UPDATE  syStudentAttendanceSummary
                    SET     tardiesmakingabsence = @TardiesMakingAbsence
                    WHERE   StuEnrollId = @StuEnrollId;
			--end
                    SET @PrevStuEnrollId = @StuEnrollId; 
                    FETCH NEXT FROM GetAttendance_Cursor INTO @StuEnrollId,@ClsSectionId,@MeetDate,@Actual,@ScheduledMinutes,@Absent,@IsTardy,
                        @ActualRunningScheduledHours,@ActualRunningPresentHours,@ActualRunningAbsentHours,@ActualRunningMakeupHours,@ActualRunningTardyHours,
                        @tracktardies,@TardiesMakingAbsence,@PrgVerId,@rownumber;
                END;
            CLOSE GetAttendance_Cursor;
            DEALLOCATE GetAttendance_Cursor;
        END;

        PRINT 'Does it get to Clock Hour/PA';
        PRINT @UnitTypeId;
        PRINT @TrackAttendanceBy;
	-- By Day and PA, Clock Hour
        IF (
             LTRIM(RTRIM(@UnitTypeId)) = 'EF5535C2-142C-4223-AE3C-25A50A153CC6'
             OR LTRIM(RTRIM(@UnitTypeId)) = 'B937C92E-FD7A-455E-A731-527A9918C734'
           )
            AND LOWER(@TrackAttendanceBy) = 'byday'
            BEGIN
		
                PRINT 'By Day inside day';
			/***************************************************************************************
		Calculation for Attendance by Day and Present Absent : 
		
			Total Days Attended = Adjusted Present Days + Makeup days
			Absent Days = Days Absent + Tardy Days

	*******************************************************************************************/
                DECLARE GetAttendance_Cursor CURSOR
                FOR
                    SELECT  t1.StuEnrollId
                           ,t1.RecordDate
                           ,t1.ActualHours
                           ,t1.SchedHours
                           ,CASE WHEN (
                                        (
                                          t1.SchedHours >= 1
                                          AND t1.SchedHours NOT IN ( 999,9999 )
                                        )
                                        AND t1.ActualHours = 0
                                      ) THEN t1.SchedHours
                                 ELSE 0
                            END AS Absent
                           ,t1.isTardy
                           ,t3.TrackTardies
                           ,t3.TardiesMakingAbsence
                    FROM    arStudentClockAttendance t1
                    INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                    INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId 
			--inner join Inserted t4 on t1.StuEnrollId = t4.StuEnrollId and t1.RecordDate=t4.RecordDate
                    WHERE   -- Unit Types: Present Absent and Clock Hour
                            t3.UnitTypeId IN ( 'ef5535c2-142c-4223-ae3c-25a50a153cc6','B937C92E-FD7A-455E-A731-527A9918C734' )
                            AND ActualHours <> 9999.00
                            AND t2.StuEnrollId = @StuEnrollId
                    ORDER BY t1.StuEnrollId
                           ,t1.RecordDate;
                OPEN GetAttendance_Cursor;
--Declare @ActualHours decimal(18,2),@SchedHours decimal(18,2)
                FETCH NEXT FROM GetAttendance_Cursor INTO @StuEnrollId,@MeetDate,@Actual,@ScheduledMinutes,@Absent,@IsTardy,@tracktardies,@TardiesMakingAbsence;

                SET @ActualRunningPresentHours = 0;
                SET @ActualRunningPresentHours = 0;
                SET @ActualRunningAbsentHours = 0;
                SET @ActualRunningTardyHours = 0;
                SET @ActualRunningMakeupHours = 0;
                SET @intTardyBreakPoint = 0;
                SET @AdjustedRunningPresentHours = 0;
                SET @AdjustedRunningAbsentHours = 0;
                SET @ActualRunningScheduledDays = 0;
                SET @MakeupHours = 0;
                WHILE @@FETCH_STATUS = 0
                    BEGIN

                        IF @PrevStuEnrollId <> @StuEnrollId
                            BEGIN
                                SET @ActualRunningPresentHours = 0;
                                SET @ActualRunningAbsentHours = 0;
                                SET @intTardyBreakPoint = 0;
                                SET @ActualRunningTardyHours = 0;
                                SET @AdjustedRunningPresentHours = 0;
                                SET @AdjustedRunningAbsentHours = 0;
                                SET @ActualRunningScheduledDays = 0;
                                SET @MakeupHours = 0;
                            END;
	   
                        IF (
                             @Actual <> 9999
                             AND @Actual <> 999
                           )
                            BEGIN
                                SET @ActualRunningScheduledDays = ISNULL(@ActualRunningScheduledDays,0) + @ScheduledMinutes;
                                SET @ActualRunningPresentHours = ISNULL(@ActualRunningPresentHours,0) + @Actual;
                                SET @AdjustedRunningPresentHours = ISNULL(@AdjustedRunningPresentHours,0) + @Actual;
                            END;
                        SET @ActualRunningAbsentHours = ISNULL(@ActualRunningAbsentHours,0) + @Absent;
                        IF (
                             @Actual = 0
                             AND @ScheduledMinutes >= 1
                             AND @ScheduledMinutes NOT IN ( 999,9999 )
                           )
                            BEGIN
                                SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Absent;
                            END;
		
		-- NWH 
		-- If Scheduled = 3.0 hrs and Actual = 1.0 Then 2 hrs is considered absent
                        IF (
                             @ScheduledMinutes >= 1
                             AND @ScheduledMinutes NOT IN ( 999,9999 )
                             AND @Actual > 0
                             AND ( @Actual < @ScheduledMinutes )
                           )
                            BEGIN
                                SET @ActualRunningAbsentHours = ISNULL(@ActualRunningAbsentHours,0) + ( @ScheduledMinutes - @Actual );
                                SET @AdjustedRunningAbsentHours = ISNULL(@AdjustedRunningAbsentHours,0) + ( @ScheduledMinutes - @Actual );
                            END; 
		
                        IF (
                             @tracktardies = 1
                             AND @IsTardy = 1
                           )
                            BEGIN
                                SET @ActualRunningTardyHours = @ActualRunningTardyHours + 1;
                            END;
		 --commented by balaji on 10/22/2012 as report (rdl) doesn't add days attended and make up days
		 ---- If there are make up hrs deduct that otherwise it will be added again in progress report
                        IF (
                             @Actual > 0
                             AND @Actual > @ScheduledMinutes
                             AND @Actual <> 9999.00
                           )
                            BEGIN
                                SET @ActualRunningPresentHours = @ActualRunningPresentHours - ( @Actual - @ScheduledMinutes );
                                SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - ( @Actual - @ScheduledMinutes );
                            END;
			
                        IF @tracktardies = 1
                            AND (
                                  @TardyMinutes > 0
                                  OR @IsTardy = 1
                                )
                            BEGIN
                                SET @intTardyBreakPoint = @intTardyBreakPoint + 1;
                            END;	    
		
		
		
		-- Makeup: Calculate Makeup Hours when Student Attended more than scheduled hours          	       
                        IF (
                             @Actual > 0
                             AND @Actual > @ScheduledMinutes
                             AND @Actual <> 9999.00
                           )
                            BEGIN
                                SET @MakeupHours = @MakeupHours + ( @Actual - @ScheduledMinutes );
                            END;
		
                        IF (
                             @tracktardies = 1
                             AND @intTardyBreakPoint = @TardiesMakingAbsence
                           )
                            BEGIN
                                SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - @Actual;
                                SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @ScheduledMinutes; --@TardyMinutes
                                SET @intTardyBreakPoint = 0;
                            END;
                
                        DELETE  FROM syStudentAttendanceSummary
                        WHERE   StuEnrollId = @StuEnrollId
                                AND StudentAttendedDate = @MeetDate;
                        INSERT  INTO syStudentAttendanceSummary
                                (
                                 StuEnrollId
                                ,ClsSectionId
                                ,StudentAttendedDate
                                ,ScheduledDays
                                ,ActualDays
                                ,ActualRunningScheduledDays
                                ,ActualRunningPresentDays
                                ,ActualRunningAbsentDays
                                ,ActualRunningMakeupDays
                                ,ActualRunningTardyDays
                                ,AdjustedPresentDays
                                ,AdjustedAbsentDays
                                ,AttendanceTrackType
                                ,ModUser
                                ,ModDate
                                ,tardiesmakingabsence
                                )
                        VALUES  (
                                 @StuEnrollId
                                ,@ClsSectionId
                                ,@MeetDate
                                ,ISNULL(@ScheduledMinutes,0)
                                ,@Actual
                                ,ISNULL(@ActualRunningScheduledDays,0)
                                ,ISNULL(@ActualRunningPresentHours,0)
                                ,ISNULL(@ActualRunningAbsentHours,0)
                                ,ISNULL(@MakeupHours,0)
                                ,ISNULL(@ActualRunningTardyHours,0)
                                ,ISNULL(@AdjustedRunningPresentHours,0)
                                ,ISNULL(@AdjustedRunningAbsentHours,0)
                                ,'Post Attendance by Class'
                                ,'sa'
                                ,GETDATE()
                                ,@TardiesMakingAbsence
                                );

		--update syStudentAttendanceSummary set TardiesMakingAbsence=@TardiesMakingAbsence where StuEnrollId=@StuEnrollId
		--end
                        SET @PrevStuEnrollId = @StuEnrollId; 
                        FETCH NEXT FROM GetAttendance_Cursor INTO @StuEnrollId,@MeetDate,@Actual,@ScheduledMinutes,@Absent,@IsTardy,@tracktardies,
                            @TardiesMakingAbsence;
                    END;
                CLOSE GetAttendance_Cursor;
                DEALLOCATE GetAttendance_Cursor;
            END;		
--end

-- Based on grade rounding round the final score and current score
--Declare @PrevTermId uniqueidentifier,@PrevReqId uniqueidentifier,@RowCount int,@FinalScore decimal(18,4),@CurrentScore decimal(18,4)
--declare @ReqId uniqueidentifier,@CourseCodeDescrip varchar(100),@FinalGrade varchar(10),@sysComponentTypeId int
--declare @CreditsAttempted decimal(18,4),@Grade varchar(10),@IsPass bit,@IsCreditsAttempted bit,@IsCreditsEarned bit
--declare @IsInGPA bit,@FinAidCredits decimal(18,4),@CurrentGrade varchar(10),@CourseCredits decimal(18,4)
        DECLARE @GPA DECIMAL(18,4);

        DECLARE @PrevReqId UNIQUEIDENTIFIER
           ,@PrevTermId UNIQUEIDENTIFIER
           ,@CreditsAttempted DECIMAL(18,2)
           ,@CreditsEarned DECIMAL(18,2);
        DECLARE @reqid UNIQUEIDENTIFIER
           ,@CourseCodeDescrip VARCHAR(50)
           ,@FinalGrade UNIQUEIDENTIFIER
           ,@FinalScore DECIMAL(18,2)
           ,@Grade VARCHAR(50);
        DECLARE @IsPass BIT
           ,@IsCreditsAttempted BIT
           ,@IsCreditsEarned BIT
           ,@CurrentScore DECIMAL(18,2)
           ,@CurrentGrade VARCHAR(10)
           ,@FinalGPA DECIMAL(18,2);
        DECLARE @sysComponentTypeId INT
           ,@RowCount INT;
        DECLARE @IsInGPA BIT;
        DECLARE @CourseCredits DECIMAL(18,2);
        DECLARE @FinAidCreditsEarned DECIMAL(18,2)
           ,@FinAidCredits DECIMAL(18,2);




        DECLARE GetCreditsSummary_Cursor CURSOR
        FOR
            SELECT	DISTINCT
                    SE.StuEnrollId
                   ,T.TermId
                   ,T.TermDescrip
                   ,T.StartDate
                   ,R.ReqId
                   ,'(' + R.Code + ' ) ' + R.Descrip AS CourseCodeDescrip
                   ,RES.Score AS FinalScore
                   ,RES.GrdSysDetailId AS FinalGrade
                   ,GCT.SysComponentTypeId
                   ,R.Credits AS CreditsAttempted
                   ,CS.ClsSectionId
                   ,GSD.Grade
                   ,GSD.IsPass
                   ,GSD.IsCreditsAttempted
                   ,GSD.IsCreditsEarned
                   ,SE.PrgVerId
                   ,GSD.IsInGPA
                   ,R.FinAidCredits AS FinAidCredits
            FROM    arStuEnrollments SE
            INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
            INNER JOIN arResults RES ON RES.StuEnrollId = SE.StuEnrollId
            INNER JOIN arClassSections CS ON RES.TestId = CS.ClsSectionId
            INNER JOIN arTerm T ON CS.TermId = T.TermId
            INNER JOIN arReqs R ON CS.ReqId = R.ReqId
            LEFT JOIN arGrdBkResults GBR ON CS.ClsSectionId = GBR.ClsSectionId
            LEFT JOIN arGrdBkWgtDetails GBWD ON GBR.InstrGrdBkWgtDetailId = GBWD.InstrGrdBkWgtDetailId
            LEFT JOIN arGrdComponentTypes GCT ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
            LEFT JOIN arGradeSystemDetails GSD ON RES.GrdSysDetailId = GSD.GrdSysDetailId
            WHERE   SE.StuEnrollId = @StuEnrollId
            ORDER BY T.StartDate
                   ,T.TermDescrip
                   ,R.ReqId; 

        DECLARE @varGradeRounding VARCHAR(3);
        DECLARE @roundfinalscore DECIMAL(18,4);
        SET @varGradeRounding = (
                                  SELECT    Value
                                  FROM      syConfigAppSetValues
                                  WHERE     SettingId = 45
                                );

        OPEN GetCreditsSummary_Cursor;
        SET @PrevStuEnrollId = NULL;
        SET @PrevTermId = NULL;
        SET @PrevReqId = NULL;
        SET @RowCount = 0;
        FETCH NEXT FROM GetCreditsSummary_Cursor INTO @StuEnrollId,@termid,@TermDescrip,@termstartdate1,@reqid,@CourseCodeDescrip,@FinalScore,@FinalGrade,
            @sysComponentTypeId,@CreditsAttempted,@ClsSectionId,@Grade,@IsPass,@IsCreditsAttempted,@IsCreditsEarned,@PrgVerId,@IsInGPA,@FinAidCredits;
        WHILE @@FETCH_STATUS = 0
            BEGIN
			
			
                SET @CurrentScore = (
                                      SELECT    CASE WHEN SUM(GradeBookWeight) > 0 THEN ( SUM(ActualWeight) / SUM(GradeBookWeight) ) * 100
                                                     ELSE NULL
                                                END AS CurrentScore
                                      FROM      (
                                                  SELECT    InstrGrdBkWgtDetailId
                                                           ,Code
                                                           ,Descrip
                                                           ,Weight AS GradeBookWeight
                                                           ,CASE WHEN S.NumberOfComponents > 0 THEN SUM(( Weight * ( Score / NumberOfComponents ) ) / 100)
                                                                 ELSE 0
                                                            END AS ActualWeight
                                                  FROM      (
                                                              SELECT    C.InstrGrdBkWgtDetailId
                                                                       ,D.Code
                                                                       ,D.Descrip
                                                                       ,ISNULL(C.Weight,0) AS Weight
                                                                       ,C.Number AS MinNumber
                                                                       ,C.GrdPolicyId
                                                                       ,C.Parameter AS Param
                                                                       ,X.GrdScaleId
                                                                       ,SUM(GR.Score) AS Score
                                                                       ,COUNT(D.Descrip) AS NumberOfComponents
                                                              FROM      (
                                                                          SELECT DISTINCT TOP 1
                                                                                    A.InstrGrdBkWgtId
                                                                                   ,A.EffectiveDate
                                                                                   ,B.GrdScaleId
                                                                          FROM      arGrdBkWeights A
                                                                                   ,arClassSections B
                                                                          WHERE     A.ReqId = B.ReqId
                                                                                    AND A.EffectiveDate <= B.StartDate
                                                                                    AND B.ClsSectionId = @ClsSectionId
                                                                          ORDER BY  A.EffectiveDate DESC
                                                                        ) X
                                                                       ,arGrdBkWgtDetails C
                                                                       ,arGrdComponentTypes D
                                                                       ,arGrdBkResults GR
                                                              WHERE     X.InstrGrdBkWgtId = C.InstrGrdBkWgtId
                                                                        AND C.GrdComponentTypeId = D.GrdComponentTypeId
                                                                        AND C.InstrGrdBkWgtDetailId = GR.InstrGrdBkWgtDetailId
                                                                        AND D.SysComponentTypeId NOT IN ( 500,503 )
                                                                        AND GR.StuEnrollId = @StuEnrollId
                                                                        AND GR.ClsSectionId = @ClsSectionId
                                                                        AND GR.Score IS NOT NULL
                                                              GROUP BY  C.InstrGrdBkWgtDetailId
                                                                       ,D.Code
                                                                       ,D.Descrip
                                                                       ,C.Weight
                                                                       ,C.Number
                                                                       ,C.GrdPolicyId
                                                                       ,C.Parameter
                                                                       ,X.GrdScaleId
                                                            ) S
                                                  GROUP BY  InstrGrdBkWgtDetailId
                                                           ,Code
                                                           ,Descrip
                                                           ,Weight
                                                           ,NumberOfComponents
                                                ) FinalTblToComputeCurrentScore
                                    );
                IF ( @CurrentScore IS NULL )
                    BEGIN
				-- instructor grade books
                        SET @CurrentScore = (
                                              SELECT    CASE WHEN SUM(GradeBookWeight) > 0 THEN ( SUM(ActualWeight) / SUM(GradeBookWeight) ) * 100
                                                             ELSE NULL
                                                        END AS CurrentScore
                                              FROM      (
                                                          SELECT    InstrGrdBkWgtDetailId
                                                                   ,Code
                                                                   ,Descrip
                                                                   ,Weight AS GradeBookWeight
                                                                   ,CASE WHEN S.NumberOfComponents > 0
                                                                         THEN SUM(( Weight * ( Score / NumberOfComponents ) ) / 100)
                                                                         ELSE 0
                                                                    END AS ActualWeight
                                                          FROM      (
                                                                      SELECT    C.InstrGrdBkWgtDetailId
                                                                               ,D.Code
                                                                               ,D.Descrip
                                                                               ,ISNULL(C.Weight,0) AS Weight
                                                                               ,C.Number AS MinNumber
                                                                               ,C.GrdPolicyId
                                                                               ,C.Parameter AS Param
                                                                               ,X.GrdScaleId
                                                                               ,SUM(GR.Score) AS Score
                                                                               ,COUNT(D.Descrip) AS NumberOfComponents
                                                                      FROM      (
                                                                                  --SELECT Distinct Top 1 A.InstrGrdBkWgtId,A.EffectiveDate,B.GrdScaleId        
															--FROM          arGrdBkWeights A,arClassSections B        
															--WHERE         A.ReqId=B.ReqId AND A.EffectiveDate<=B.StartDate AND B.ClsSectionId=@ClsSectionId
															--ORDER BY      A.EffectiveDate DESC
                                                              SELECT DISTINCT TOP 1
                                                                        t1.InstrGrdBkWgtId
                                                                       ,t1.GrdScaleId
                                                              FROM      arClassSections t1
                                                                       ,arGrdBkWeights t2
                                                              WHERE     t1.InstrGrdBkWgtId = t2.InstrGrdBkWgtId
                                                                        AND t1.ClsSectionId = @ClsSectionId
                                                                                ) X
                                                                               ,arGrdBkWgtDetails C
                                                                               ,arGrdComponentTypes D
                                                                               ,arGrdBkResults GR
                                                                      WHERE     X.InstrGrdBkWgtId = C.InstrGrdBkWgtId
                                                                                AND C.GrdComponentTypeId = D.GrdComponentTypeId
                                                                                AND C.InstrGrdBkWgtDetailId = GR.InstrGrdBkWgtDetailId
                                                                                AND
													-- D.SysComponentTypeID not in (500,503) and 
                                                                                GR.StuEnrollId = @StuEnrollId
                                                                                AND GR.ClsSectionId = @ClsSectionId
                                                                                AND GR.Score IS NOT NULL
                                                                      GROUP BY  C.InstrGrdBkWgtDetailId
                                                                               ,D.Code
                                                                               ,D.Descrip
                                                                               ,C.Weight
                                                                               ,C.Number
                                                                               ,C.GrdPolicyId
                                                                               ,C.Parameter
                                                                               ,X.GrdScaleId
                                                                    ) S
                                                          GROUP BY  InstrGrdBkWgtDetailId
                                                                   ,Code
                                                                   ,Descrip
                                                                   ,Weight
                                                                   ,NumberOfComponents
                                                        ) FinalTblToComputeCurrentScore
                                            );	
			
                    END;
			
		
			-- If rounding is set to yes, then round the scores to next available score or ignore rounding
                IF ( LOWER(@varGradeRounding) = 'yes' )
                    BEGIN
                        IF @FinalScore IS NOT NULL
                            BEGIN
                                SET @FinalScore = ROUND(@FinalScore,0);
                                SET @CurrentScore = @FinalScore;
                            END;
                        IF @FinalScore IS NULL
                            AND @CurrentScore IS NOT NULL
                            BEGIN
                                SET @CurrentScore = ROUND(@CurrentScore,0);
                            END;
                    END;
                ELSE
                    BEGIN
                        IF @FinalScore IS NOT NULL
                            BEGIN
                                SET @CurrentScore = @FinalScore;
                            END;	
                        IF @FinalScore IS NULL
                            AND @CurrentScore IS NOT NULL
                            BEGIN
                                SET @CurrentScore = ROUND(@CurrentScore,0);
                            END;
                    END;
				
                UPDATE  syCreditSummary
                SET     FinalScore = @FinalScore
                       ,CurrentScore = @CurrentScore
                WHERE   StuEnrollId = @StuEnrollId
                        AND TermId = @termid
                        AND ReqId = @reqid;

			--Average calculation
			
			-- Term Average
                SET @TermAverageCount = (
                                          SELECT    COUNT(*)
                                          FROM      syCreditSummary
                                          WHERE     StuEnrollId = @StuEnrollId
                                                    AND TermId = @termid
                                                    AND FinalScore IS NOT NULL
                                        );
                SET @termAverageSum = (
                                        SELECT  SUM(FinalScore)
                                        FROM    syCreditSummary
                                        WHERE   StuEnrollId = @StuEnrollId
                                                AND TermId = @termid
                                                AND FinalScore IS NOT NULL
                                      );
                SET @TermAverage = @termAverageSum / @TermAverageCount; 
			
			-- Cumulative Average
                SET @cumAveragecount = (
                                         SELECT COUNT(*)
                                         FROM   syCreditSummary
                                         WHERE  StuEnrollId = @StuEnrollId
                                                AND FinalScore IS NOT NULL
                                       );
                SET @cumAverageSum = (
                                       SELECT   SUM(FinalScore)
                                       FROM     syCreditSummary
                                       WHERE    StuEnrollId = @StuEnrollId
                                                AND FinalScore IS NOT NULL
                                     );
                SET @CumAverage = @cumAverageSum / @cumAveragecount; 
			
                UPDATE  syCreditSummary
                SET     Average = @TermAverage
                WHERE   StuEnrollId = @StuEnrollId
                        AND TermId = @termid; 
			
			--Update Cumulative GPA
                UPDATE  syCreditSummary
                SET     CumAverage = @CumAverage
                WHERE   StuEnrollId = @StuEnrollId;
			

                FETCH NEXT FROM GetCreditsSummary_Cursor INTO @StuEnrollId,@termid,@TermDescrip,@termstartdate1,@reqid,@CourseCodeDescrip,@FinalScore,
                    @FinalGrade,@sysComponentTypeId,@CreditsAttempted,@ClsSectionId,@Grade,@IsPass,@IsCreditsAttempted,@IsCreditsEarned,@PrgVerId,@IsInGPA,
                    @FinAidCredits;
            END;
        CLOSE GetCreditsSummary_Cursor;
        DEALLOCATE GetCreditsSummary_Cursor;


        DECLARE GetCreditsSummary_Cursor CURSOR
        FOR
            SELECT	DISTINCT
                    SE.StuEnrollId
                   ,T.TermId
                   ,T.TermDescrip
                   ,T.StartDate
                   ,R.ReqId
                   ,'(' + R.Code + ' ) ' + R.Descrip AS CourseCodeDescrip
                   ,RES.Score AS FinalScore
                   ,RES.GrdSysDetailId AS FinalGrade
                   ,GCT.SysComponentTypeId
                   ,R.Credits AS CreditsAttempted
                   ,CS.ClsSectionId
                   ,GSD.Grade
                   ,GSD.IsPass
                   ,GSD.IsCreditsAttempted
                   ,GSD.IsCreditsEarned
                   ,SE.PrgVerId
                   ,GSD.IsInGPA
                   ,R.FinAidCredits AS FinAidCredits
            FROM    arStuEnrollments SE
            INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
            INNER JOIN arTransferGrades RES ON RES.StuEnrollId = SE.StuEnrollId
            INNER JOIN arClassSections CS ON RES.ReqId = CS.ReqId
                                             AND RES.TermId = CS.TermId
            INNER JOIN arTerm T ON CS.TermId = T.TermId
            INNER JOIN arReqs R ON CS.ReqId = R.ReqId
            LEFT JOIN arGrdBkResults GBR ON CS.ClsSectionId = GBR.ClsSectionId
            LEFT JOIN arGrdBkWgtDetails GBWD ON GBR.InstrGrdBkWgtDetailId = GBWD.InstrGrdBkWgtDetailId
            LEFT JOIN arGrdComponentTypes GCT ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
            LEFT JOIN arGradeSystemDetails GSD ON RES.GrdSysDetailId = GSD.GrdSysDetailId
            WHERE   SE.StuEnrollId = @StuEnrollId
            ORDER BY T.StartDate
                   ,T.TermDescrip
                   ,R.ReqId;


        SET @varGradeRounding = (
                                  SELECT    Value
                                  FROM      syConfigAppSetValues
                                  WHERE     SettingId = 45
                                );

        OPEN GetCreditsSummary_Cursor;
        SET @PrevStuEnrollId = NULL;
        SET @PrevTermId = NULL;
        SET @PrevReqId = NULL;
        SET @RowCount = 0;
        FETCH NEXT FROM GetCreditsSummary_Cursor INTO @StuEnrollId,@termid,@TermDescrip,@termstartdate1,@reqid,@CourseCodeDescrip,@FinalScore,@FinalGrade,
            @sysComponentTypeId,@CreditsAttempted,@ClsSectionId,@Grade,@IsPass,@IsCreditsAttempted,@IsCreditsEarned,@PrgVerId,@IsInGPA,@FinAidCredits;
        WHILE @@FETCH_STATUS = 0
            BEGIN
			
                SET @CurrentScore = (
                                      SELECT    CASE WHEN SUM(GradeBookWeight) > 0 THEN ( SUM(ActualWeight) / SUM(GradeBookWeight) ) * 100
                                                     ELSE NULL
                                                END AS CurrentScore
                                      FROM      (
                                                  SELECT    InstrGrdBkWgtDetailId
                                                           ,Code
                                                           ,Descrip
                                                           ,Weight AS GradeBookWeight
                                                           ,CASE WHEN S.NumberOfComponents > 0 THEN SUM(( Weight * ( Score / NumberOfComponents ) ) / 100)
                                                                 ELSE 0
                                                            END AS ActualWeight
                                                  FROM      (
                                                              SELECT    C.InstrGrdBkWgtDetailId
                                                                       ,D.Code
                                                                       ,D.Descrip
                                                                       ,ISNULL(C.Weight,0) AS Weight
                                                                       ,C.Number AS MinNumber
                                                                       ,C.GrdPolicyId
                                                                       ,C.Parameter AS Param
                                                                       ,X.GrdScaleId
                                                                       ,SUM(GR.Score) AS Score
                                                                       ,COUNT(D.Descrip) AS NumberOfComponents
                                                              FROM      (
                                                                          SELECT DISTINCT TOP 1
                                                                                    A.InstrGrdBkWgtId
                                                                                   ,A.EffectiveDate
                                                                                   ,B.GrdScaleId
                                                                          FROM      arGrdBkWeights A
                                                                                   ,arClassSections B
                                                                          WHERE     A.ReqId = B.ReqId
                                                                                    AND A.EffectiveDate <= B.StartDate
                                                                                    AND B.ClsSectionId = @ClsSectionId
                                                                          ORDER BY  A.EffectiveDate DESC
                                                                        ) X
                                                                       ,arGrdBkWgtDetails C
                                                                       ,arGrdComponentTypes D
                                                                       ,arGrdBkResults GR
                                                              WHERE     X.InstrGrdBkWgtId = C.InstrGrdBkWgtId
                                                                        AND C.GrdComponentTypeId = D.GrdComponentTypeId
                                                                        AND C.InstrGrdBkWgtDetailId = GR.InstrGrdBkWgtDetailId
                                                                        AND D.SysComponentTypeId NOT IN ( 500,503 )
                                                                        AND GR.StuEnrollId = @StuEnrollId
                                                                        AND GR.ClsSectionId = @ClsSectionId
                                                                        AND GR.Score IS NOT NULL
                                                              GROUP BY  C.InstrGrdBkWgtDetailId
                                                                       ,D.Code
                                                                       ,D.Descrip
                                                                       ,C.Weight
                                                                       ,C.Number
                                                                       ,C.GrdPolicyId
                                                                       ,C.Parameter
                                                                       ,X.GrdScaleId
                                                            ) S
                                                  GROUP BY  InstrGrdBkWgtDetailId
                                                           ,Code
                                                           ,Descrip
                                                           ,Weight
                                                           ,NumberOfComponents
                                                ) FinalTblToComputeCurrentScore
                                    );
                IF ( @CurrentScore IS NULL )
                    BEGIN
				-- instructor grade books
                        SET @CurrentScore = (
                                              SELECT    CASE WHEN SUM(GradeBookWeight) > 0 THEN ( SUM(ActualWeight) / SUM(GradeBookWeight) ) * 100
                                                             ELSE NULL
                                                        END AS CurrentScore
                                              FROM      (
                                                          SELECT    InstrGrdBkWgtDetailId
                                                                   ,Code
                                                                   ,Descrip
                                                                   ,Weight AS GradeBookWeight
                                                                   ,CASE WHEN S.NumberOfComponents > 0
                                                                         THEN SUM(( Weight * ( Score / NumberOfComponents ) ) / 100)
                                                                         ELSE 0
                                                                    END AS ActualWeight
                                                          FROM      (
                                                                      SELECT    C.InstrGrdBkWgtDetailId
                                                                               ,D.Code
                                                                               ,D.Descrip
                                                                               ,ISNULL(C.Weight,0) AS Weight
                                                                               ,C.Number AS MinNumber
                                                                               ,C.GrdPolicyId
                                                                               ,C.Parameter AS Param
                                                                               ,X.GrdScaleId
                                                                               ,SUM(GR.Score) AS Score
                                                                               ,COUNT(D.Descrip) AS NumberOfComponents
                                                                      FROM      (
                                                                                  --SELECT Distinct Top 1 A.InstrGrdBkWgtId,A.EffectiveDate,B.GrdScaleId        
															--FROM          arGrdBkWeights A,arClassSections B        
															--WHERE         A.ReqId=B.ReqId AND A.EffectiveDate<=B.StartDate AND B.ClsSectionId=@ClsSectionId
															--ORDER BY      A.EffectiveDate DESC
                                                              SELECT DISTINCT TOP 1
                                                                        t1.InstrGrdBkWgtId
                                                                       ,t1.GrdScaleId
                                                              FROM      arClassSections t1
                                                                       ,arGrdBkWeights t2
                                                              WHERE     t1.InstrGrdBkWgtId = t2.InstrGrdBkWgtId
                                                                        AND t1.ClsSectionId = @ClsSectionId
                                                                                ) X
                                                                               ,arGrdBkWgtDetails C
                                                                               ,arGrdComponentTypes D
                                                                               ,arGrdBkResults GR
                                                                      WHERE     X.InstrGrdBkWgtId = C.InstrGrdBkWgtId
                                                                                AND C.GrdComponentTypeId = D.GrdComponentTypeId
                                                                                AND C.InstrGrdBkWgtDetailId = GR.InstrGrdBkWgtDetailId
                                                                                AND
													-- D.SysComponentTypeID not in (500,503) and 
                                                                                GR.StuEnrollId = @StuEnrollId
                                                                                AND GR.ClsSectionId = @ClsSectionId
                                                                                AND GR.Score IS NOT NULL
                                                                      GROUP BY  C.InstrGrdBkWgtDetailId
                                                                               ,D.Code
                                                                               ,D.Descrip
                                                                               ,C.Weight
                                                                               ,C.Number
                                                                               ,C.GrdPolicyId
                                                                               ,C.Parameter
                                                                               ,X.GrdScaleId
                                                                    ) S
                                                          GROUP BY  InstrGrdBkWgtDetailId
                                                                   ,Code
                                                                   ,Descrip
                                                                   ,Weight
                                                                   ,NumberOfComponents
                                                        ) FinalTblToComputeCurrentScore
                                            );	
			
                    END;
			
			-- If rounding is set to yes, then round the scores to next available score or ignore rounding
                IF ( LOWER(@varGradeRounding) = 'yes' )
                    BEGIN
                        IF @FinalScore IS NOT NULL
                            BEGIN
                                SET @FinalScore = ROUND(@FinalScore,0);
                                SET @CurrentScore = @FinalScore;
                            END;
                        IF @FinalScore IS NULL
                            AND @CurrentScore IS NOT NULL
                            BEGIN
                                SET @CurrentScore = ROUND(@CurrentScore,0);
                            END;
                    END;
                ELSE
                    BEGIN
                        IF @FinalScore IS NOT NULL
                            BEGIN
                                SET @CurrentScore = @FinalScore;
                            END;	
                        IF @FinalScore IS NULL
                            AND @CurrentScore IS NOT NULL
                            BEGIN
                                SET @CurrentScore = ROUND(@CurrentScore,0);
                            END;
                    END;
				
                UPDATE  syCreditSummary
                SET     FinalScore = @FinalScore
                       ,CurrentScore = @CurrentScore
                WHERE   StuEnrollId = @StuEnrollId
                        AND TermId = @termid
                        AND ReqId = @reqid;

			--Average calculation
--			declare @CumAverage decimal(18,2),@cumAverageSum decimal(18,2),@cumAveragecount int
			
			-- Term Average
                SET @TermAverageCount = (
                                          SELECT    COUNT(*)
                                          FROM      syCreditSummary
                                          WHERE     StuEnrollId = @StuEnrollId
                                                    AND TermId = @termid
                                                    AND FinalScore IS NOT NULL
                                        );
                SET @termAverageSum = (
                                        SELECT  SUM(FinalScore)
                                        FROM    syCreditSummary
                                        WHERE   StuEnrollId = @StuEnrollId
                                                AND TermId = @termid
                                                AND FinalScore IS NOT NULL
                                      );
                SET @TermAverage = @termAverageSum / @TermAverageCount; 
			
			-- Cumulative Average
                SET @cumAveragecount = (
                                         SELECT COUNT(*)
                                         FROM   syCreditSummary
                                         WHERE  StuEnrollId = @StuEnrollId
                                                AND FinalScore IS NOT NULL
                                       );
                SET @cumAverageSum = (
                                       SELECT   SUM(FinalScore)
                                       FROM     syCreditSummary
                                       WHERE    StuEnrollId = @StuEnrollId
                                                AND FinalScore IS NOT NULL
                                     );
                SET @CumAverage = @cumAverageSum / @cumAveragecount; 
			
                UPDATE  syCreditSummary
                SET     Average = @TermAverage
                WHERE   StuEnrollId = @StuEnrollId
                        AND TermId = @termid; 
			
			--Update Cumulative GPA
                UPDATE  syCreditSummary
                SET     CumAverage = @CumAverage
                WHERE   StuEnrollId = @StuEnrollId;
			

                FETCH NEXT FROM GetCreditsSummary_Cursor INTO @StuEnrollId,@termid,@TermDescrip,@termstartdate1,@reqid,@CourseCodeDescrip,@FinalScore,
                    @FinalGrade,@sysComponentTypeId,@CreditsAttempted,@ClsSectionId,@Grade,@IsPass,@IsCreditsAttempted,@IsCreditsEarned,@PrgVerId,@IsInGPA,
                    @FinAidCredits;
            END;
        CLOSE GetCreditsSummary_Cursor;
        DEALLOCATE GetCreditsSummary_Cursor;

        DECLARE @GradeSystemDetailId UNIQUEIDENTIFIER
           ,@IsGPA BIT;
        DECLARE GetCreditsSummary_Cursor CURSOR
        FOR
            SELECT DISTINCT
                    SE.StuEnrollId
                   ,T.TermId
                   ,T.TermDescrip
                   ,T.StartDate
                   ,R.ReqId
                   ,'(' + R.Code + ' ) ' + R.Descrip AS CourseCodeDescrip
                   ,NULL AS FinalScore
                   ,RES.GrdSysDetailId AS GradeSystemDetailId
                   ,GSD.Grade AS FinalGrade
                   ,GSD.Grade AS CurrentGrade
                   ,R.Credits
                   ,NULL AS ClsSectionId
                   ,GSD.IsPass
                   ,GSD.IsCreditsAttempted
                   ,GSD.IsCreditsEarned
                   ,GSD.GPA
                   ,GSD.IsInGPA
                   ,SE.PrgVerId
                   ,GSD.IsInGPA
                   ,R.FinAidCredits AS FinAidCredits
            FROM    arStuEnrollments SE
            INNER JOIN arTransferGrades RES ON SE.StuEnrollId = RES.StuEnrollId
            INNER JOIN syCreditSummary CS ON CS.StuEnrollId = RES.StuEnrollId
            INNER JOIN arGradeSystemDetails GSD ON RES.GrdSysDetailId = GSD.GrdSysDetailId
            INNER JOIN arReqs R ON RES.ReqId = R.ReqId
            INNER JOIN arTerm T ON RES.TermId = T.TermId
            WHERE   RES.ReqId NOT IN ( SELECT DISTINCT
                                                ReqId
                                       FROM     syCreditSummary
                                       WHERE    StuEnrollId = SE.StuEnrollId
                                                AND TermId = T.TermId )
                    AND SE.StuEnrollId = @StuEnrollId;

        SET @varGradeRounding = (
                                  SELECT    Value
                                  FROM      syConfigAppSetValues
                                  WHERE     SettingId = 45
                                );

        OPEN GetCreditsSummary_Cursor;
        SET @PrevStuEnrollId = NULL;
        SET @PrevTermId = NULL;
        SET @PrevReqId = NULL;
        SET @RowCount = 0;
        FETCH NEXT FROM GetCreditsSummary_Cursor INTO @StuEnrollId,@termid,@TermDescrip,@termstartdate1,@reqid,@CourseCodeDescrip,@FinalScore,
            @GradeSystemDetailId,@FinalGrade,@CurrentGrade,@CourseCredits,@ClsSectionId,@IsPass,@IsCreditsAttempted,@IsCreditsEarned,@GPA,@IsGPA,@PrgVerId,
            @IsInGPA,@FinAidCredits; 
        WHILE @@FETCH_STATUS = 0
            BEGIN
			
			-- If rounding is set to yes, then round the scores to next available score or ignore rounding
                IF ( LOWER(@varGradeRounding) = 'yes' )
                    BEGIN
                        IF @FinalScore IS NOT NULL
                            BEGIN
                                SET @FinalScore = ROUND(@FinalScore,0);
                                SET @CurrentScore = @FinalScore;
                            END;
                        IF @FinalScore IS NULL
                            AND @CurrentScore IS NOT NULL
                            BEGIN
                                SET @CurrentScore = ROUND(@CurrentScore,0);
                            END;
                    END;
                ELSE
                    BEGIN
                        IF @FinalScore IS NOT NULL
                            BEGIN
                                SET @CurrentScore = @FinalScore;
                            END;	
                        IF @FinalScore IS NULL
                            AND @CurrentScore IS NOT NULL
                            BEGIN
                                SET @CurrentScore = ROUND(@CurrentScore,0);
                            END;
                    END;
				
                UPDATE  syCreditSummary
                SET     FinalScore = @FinalScore
                       ,CurrentScore = @CurrentScore
                WHERE   StuEnrollId = @StuEnrollId
                        AND TermId = @termid
                        AND ReqId = @reqid;

			--Average calculation
		
			-- Term Average
                SET @TermAverageCount = (
                                          SELECT    COUNT(*)
                                          FROM      syCreditSummary
                                          WHERE     StuEnrollId = @StuEnrollId
                                                    AND TermId = @termid
                                                    AND FinalScore IS NOT NULL
                                        );
                SET @termAverageSum = (
                                        SELECT  SUM(FinalScore)
                                        FROM    syCreditSummary
                                        WHERE   StuEnrollId = @StuEnrollId
                                                AND TermId = @termid
                                                AND FinalScore IS NOT NULL
                                      );
                SET @TermAverage = @termAverageSum / @TermAverageCount; 
			
			-- Cumulative Average
                SET @cumAveragecount = (
                                         SELECT COUNT(*)
                                         FROM   syCreditSummary
                                         WHERE  StuEnrollId = @StuEnrollId
                                                AND FinalScore IS NOT NULL
                                       );
                SET @cumAverageSum = (
                                       SELECT   SUM(FinalScore)
                                       FROM     syCreditSummary
                                       WHERE    StuEnrollId = @StuEnrollId
                                                AND FinalScore IS NOT NULL
                                     );
                SET @CumAverage = @cumAverageSum / @cumAveragecount; 
			
                UPDATE  syCreditSummary
                SET     Average = @TermAverage
                WHERE   StuEnrollId = @StuEnrollId
                        AND TermId = @termid; 
			
			--Update Cumulative GPA
                UPDATE  syCreditSummary
                SET     CumAverage = @CumAverage
                WHERE   StuEnrollId = @StuEnrollId;
			

                FETCH NEXT FROM GetCreditsSummary_Cursor INTO @StuEnrollId,@termid,@TermDescrip,@termstartdate1,@reqid,@CourseCodeDescrip,@FinalScore,
                    @GradeSystemDetailId,@FinalGrade,@CurrentGrade,@CourseCredits,@ClsSectionId,@IsPass,@IsCreditsAttempted,@IsCreditsEarned,@GPA,@IsGPA,
                    @PrgVerId,@IsInGPA,@FinAidCredits; 
            END;
        CLOSE GetCreditsSummary_Cursor;
        DEALLOCATE GetCreditsSummary_Cursor;

        DECLARE GetCreditsSummary_Cursor CURSOR
        FOR
            SELECT DISTINCT
                    SE.StuEnrollId
                   ,T.TermId
                   ,T.TermDescrip
                   ,T.StartDate
                   ,R.ReqId
                   ,'(' + R.Code + ' ) ' + R.Descrip AS CourseCodeDescrip
                   ,NULL AS FinalScore
                   ,RES.GrdSysDetailId AS GradeSystemDetailId
                   ,GSD.Grade AS FinalGrade
                   ,GSD.Grade AS CurrentGrade
                   ,R.Credits
                   ,NULL AS ClsSectionId
                   ,GSD.IsPass
                   ,GSD.IsCreditsAttempted
                   ,GSD.IsCreditsEarned
                   ,GSD.GPA
                   ,GSD.IsInGPA
                   ,SE.PrgVerId
                   ,GSD.IsInGPA
                   ,R.FinAidCredits AS FinAidCredits
            FROM    arStuEnrollments SE
            INNER JOIN arTransferGrades RES ON SE.StuEnrollId = RES.StuEnrollId
            INNER JOIN arGradeSystemDetails GSD ON RES.GrdSysDetailId = GSD.GrdSysDetailId
            INNER JOIN arReqs R ON RES.ReqId = R.ReqId
            INNER JOIN arTerm T ON RES.TermId = T.TermId
            WHERE   SE.StuEnrollId NOT IN ( SELECT DISTINCT
                                                    StuEnrollId
                                            FROM    syCreditSummary )
                    AND SE.StuEnrollId = @StuEnrollId;

        SET @varGradeRounding = (
                                  SELECT    Value
                                  FROM      syConfigAppSetValues
                                  WHERE     SettingId = 45
                                );

        OPEN GetCreditsSummary_Cursor;
        SET @PrevStuEnrollId = NULL;
        SET @PrevTermId = NULL;
        SET @PrevReqId = NULL;
        SET @RowCount = 0;
        FETCH NEXT FROM GetCreditsSummary_Cursor INTO @StuEnrollId,@termid,@TermDescrip,@termstartdate1,@reqid,@CourseCodeDescrip,@FinalScore,
            @GradeSystemDetailId,@FinalGrade,@CurrentGrade,@CourseCredits,@ClsSectionId,@IsPass,@IsCreditsAttempted,@IsCreditsEarned,@GPA,@IsGPA,@PrgVerId,
            @IsInGPA,@FinAidCredits; 
        WHILE @@FETCH_STATUS = 0
            BEGIN
			
			-- If rounding is set to yes, then round the scores to next available score or ignore rounding
                IF ( LOWER(@varGradeRounding) = 'yes' )
                    BEGIN
                        IF @FinalScore IS NOT NULL
                            BEGIN
                                SET @FinalScore = ROUND(@FinalScore,0);
                                SET @CurrentScore = @FinalScore;
                            END;
                        IF @FinalScore IS NULL
                            AND @CurrentScore IS NOT NULL
                            BEGIN
                                SET @CurrentScore = ROUND(@CurrentScore,0);
                            END;
                    END;
                ELSE
                    BEGIN
                        IF @FinalScore IS NOT NULL
                            BEGIN
                                SET @CurrentScore = @FinalScore;
                            END;	
                        IF @FinalScore IS NULL
                            AND @CurrentScore IS NOT NULL
                            BEGIN
                                SET @CurrentScore = ROUND(@CurrentScore,0);
                            END;
                    END;
				
                UPDATE  syCreditSummary
                SET     FinalScore = @FinalScore
                       ,CurrentScore = @CurrentScore
                WHERE   StuEnrollId = @StuEnrollId
                        AND TermId = @termid
                        AND ReqId = @reqid;

			--Average calculation
                SET @TermAverageCount = (
                                          SELECT    COUNT(*)
                                          FROM      syCreditSummary
                                          WHERE     StuEnrollId = @StuEnrollId
                                                    AND TermId = @termid
                                                    AND FinalScore IS NOT NULL
                                        );
                SET @termAverageSum = (
                                        SELECT  SUM(FinalScore)
                                        FROM    syCreditSummary
                                        WHERE   StuEnrollId = @StuEnrollId
                                                AND TermId = @termid
                                                AND FinalScore IS NOT NULL
                                      );
                SET @TermAverage = @termAverageSum / @TermAverageCount; 
			
			-- Cumulative Average
                SET @cumAveragecount = (
                                         SELECT COUNT(*)
                                         FROM   syCreditSummary
                                         WHERE  StuEnrollId = @StuEnrollId
                                                AND FinalScore IS NOT NULL
                                       );
                SET @cumAverageSum = (
                                       SELECT   SUM(FinalScore)
                                       FROM     syCreditSummary
                                       WHERE    StuEnrollId = @StuEnrollId
                                                AND FinalScore IS NOT NULL
                                     );
                SET @CumAverage = @cumAverageSum / @cumAveragecount; 
			
                UPDATE  syCreditSummary
                SET     Average = @TermAverage
                WHERE   StuEnrollId = @StuEnrollId
                        AND TermId = @termid; 
			
			--Update Cumulative GPA
                UPDATE  syCreditSummary
                SET     CumAverage = @CumAverage
                WHERE   StuEnrollId = @StuEnrollId;
			

                FETCH NEXT FROM GetCreditsSummary_Cursor INTO @StuEnrollId,@termid,@TermDescrip,@termstartdate1,@reqid,@CourseCodeDescrip,@FinalScore,
                    @GradeSystemDetailId,@FinalGrade,@CurrentGrade,@CourseCredits,@ClsSectionId,@IsPass,@IsCreditsAttempted,@IsCreditsEarned,@GPA,@IsGPA,
                    @PrgVerId,@IsInGPA,@FinAidCredits;  
            END;
        CLOSE GetCreditsSummary_Cursor;
        DEALLOCATE GetCreditsSummary_Cursor;

-- Report Query Starts Here
--Declare @CurrentBal_Compute Decimal(18,2)
--set @CurrentBal_Compute = (select SUM(IsNULL(TransAmount,0)) as CurrentBalance from saTransactions where StuEnrollId=@StuEnrollId 
--							AND Voided = 0 AND 
--							(@TermStartDate is null or @TermStartDateModifier is null or 
--								(
--									((@TermStartDateModifier <> '=') OR (TransDate= ltrim(rtrim(@TermStartDate)))) And ((@TermStartDateModifier <> '>') OR (TransDate > ltrim(rtrim(@TermStartDate)))) 
--									And ((@TermStartDateModifier <> '<') OR (TransDate < ltrim(rtrim(@TermStartDate))))	
--									And ((@TermStartDateModifier <> '>=') OR (TransDate >= ltrim(rtrim(@TermStartDate)))) 
--									And ((@TermStartDateModifier <> '<=') OR (TransDate <= ltrim(rtrim(@TermStartDate))))
--								)
--							)
--						) 

--Print '@CurrentBal_Compute'
--Print @CurrentBal_Compute

        SET @TermAverageCount = (
                                  SELECT    COUNT(*)
                                  FROM      syCreditSummary
                                  WHERE     StuEnrollId = @StuEnrollId
                                            AND FinalScore IS NOT NULL 
										--and Completed=1
										--and
										--(@TermId is null or TermId in (Select Val from [MultipleValuesForReportParameters](@TermId,',',1)))
                                            AND (
                                                  @TermStartDate IS NULL
                                                  OR @TermStartDateModifier IS NULL
                                                  OR (
                                                       (
                                                         ( @TermStartDateModifier <> '=' )
                                                         OR ( TermStartDate = @TermStartDate )
                                                       )
                                                       AND (
                                                             ( @TermStartDateModifier <> '>' )
                                                             OR ( TermStartDate > @TermStartDate )
                                                           )
                                                       AND (
                                                             ( @TermStartDateModifier <> '<' )
                                                             OR ( TermStartDate < @TermStartDate )
                                                           )
                                                       AND (
                                                             ( @TermStartDateModifier <> '>=' )
                                                             OR ( TermStartDate >= @TermStartDate )
                                                           )
                                                       AND (
                                                             ( @TermStartDateModifier <> '<=' )
                                                             OR ( TermStartDate <= @TermStartDate )
                                                           )
                                                     )
                                                )
                                );
        SET @termAverageSum = (
                                SELECT  SUM(FinalScore)
                                FROM    syCreditSummary
                                WHERE   StuEnrollId = @StuEnrollId
                                        AND FinalScore IS NOT NULL 
										--and Completed=1
										--and
										--(@TermId is null or TermId in (Select Val from [MultipleValuesForReportParameters](@TermId,',',1)))
                                        AND (
                                              @TermStartDate IS NULL
                                              OR @TermStartDateModifier IS NULL
                                              OR (
                                                   (
                                                     ( @TermStartDateModifier <> '=' )
                                                     OR ( TermStartDate = @TermStartDate )
                                                   )
                                                   AND (
                                                         ( @TermStartDateModifier <> '>' )
                                                         OR ( TermStartDate > @TermStartDate )
                                                       )
                                                   AND (
                                                         ( @TermStartDateModifier <> '<' )
                                                         OR ( TermStartDate < @TermStartDate )
                                                       )
                                                   AND (
                                                         ( @TermStartDateModifier <> '>=' )
                                                         OR ( TermStartDate >= @TermStartDate )
                                                       )
                                                   AND (
                                                         ( @TermStartDateModifier <> '<=' )
                                                         OR ( TermStartDate <= @TermStartDate )
                                                       )
                                                 )
                                            )
                              );
        IF @TermAverageCount >= 1
            BEGIN
                SET @TermAverage = @termAverageSum / @TermAverageCount; 
            END;
			
-- Cumulative Average
        SET @cumAveragecount = (
                                 SELECT COUNT(*)
                                 FROM   syCreditSummary
                                 WHERE  StuEnrollId = @StuEnrollId
                                        AND FinalScore IS NOT NULL 
										--and Completed=1
										--and
										--(@TermId is null or TermId in (Select Val from [MultipleValuesForReportParameters](@TermId,',',1)))
                                        AND (
                                              @TermStartDate IS NULL
                                              OR @TermStartDateModifier IS NULL
                                              OR 
											--(
											--	((@TermStartDateModifier <> '=') OR (TermStartDate= @TermStartDate)) And ((@TermStartDateModifier <> '>') OR (TermStartDate > @TermStartDate)) 
											--	And ((@TermStartDateModifier <> '<') OR (TermStartDate < @TermStartDate))	
											--	And ((@TermStartDateModifier <> '>=') OR (TermStartDate >= @TermStartDate)) 
											--	And ((@TermStartDateModifier <> '<=') OR (TermStartDate <= @TermStartDate))
											--)
                                              (
                                                (
                                                  ( @TermStartDateModifier <> '=' )
                                                  OR ( TermStartDate = LTRIM(RTRIM(@TermStartDate)) )
                                                  OR ( DATEDIFF(DAY,@TermStartDate,TermStartDate) ) = 0
                                                )
                                                AND (
                                                      ( @TermStartDateModifier <> '>' )
                                                      OR ( TermStartDate > LTRIM(RTRIM(@TermStartDate)) )
                                                      OR ( DATEDIFF(DAY,@TermStartDate,TermStartDate) ) > 0
                                                    )
                                                AND (
                                                      ( @TermStartDateModifier <> '<' )
                                                      OR ( TermStartDate < LTRIM(RTRIM(@TermStartDate)) )
                                                      OR ( DATEDIFF(DAY,@TermStartDate,TermStartDate) ) < 0
                                                    )
                                                AND (
                                                      ( @TermStartDateModifier <> '>=' )
                                                      OR ( TermStartDate >= LTRIM(RTRIM(@TermStartDate)) )
                                                      OR ( DATEDIFF(DAY,@TermStartDate,TermStartDate) ) >= 0
                                                    )
                                                AND (
                                                      ( @TermStartDateModifier <> '<=' )
                                                      OR ( TermStartDate <= LTRIM(RTRIM(@TermStartDate)) )
                                                      OR ( DATEDIFF(DAY,@TermStartDate,TermStartDate) ) <= 0
                                                    )
                                              )
                                            )
                               );
--Print @cumAveragecount

        SET @cumAverageSum = (
                               SELECT   SUM(FinalScore)
                               FROM     syCreditSummary
                               WHERE    StuEnrollId = @StuEnrollId
                                        AND FinalScore IS NOT NULL 
										--and Completed=1
										--and
										--(@TermId is null or TermId in (Select Val from [MultipleValuesForReportParameters](@TermId,',',1)))
                                        AND (
                                              @TermStartDate IS NULL
                                              OR @TermStartDateModifier IS NULL
                                              OR 
											--(
											--	((@TermStartDateModifier <> '=') OR (TermStartDate= @TermStartDate)) And ((@TermStartDateModifier <> '>') OR (TermStartDate > @TermStartDate)) 
											--	And ((@TermStartDateModifier <> '<') OR (TermStartDate < @TermStartDate))	
											--	And ((@TermStartDateModifier <> '>=') OR (TermStartDate >= @TermStartDate)) 
											--	And ((@TermStartDateModifier <> '<=') OR (TermStartDate <= @TermStartDate))
											--)
                                              (
                                                (
                                                  ( @TermStartDateModifier <> '=' )
                                                  OR ( TermStartDate = LTRIM(RTRIM(@TermStartDate)) )
                                                  OR ( DATEDIFF(DAY,@TermStartDate,TermStartDate) ) = 0
                                                )
                                                AND (
                                                      ( @TermStartDateModifier <> '>' )
                                                      OR ( TermStartDate > LTRIM(RTRIM(@TermStartDate)) )
                                                      OR ( DATEDIFF(DAY,@TermStartDate,TermStartDate) ) > 0
                                                    )
                                                AND (
                                                      ( @TermStartDateModifier <> '<' )
                                                      OR ( TermStartDate < LTRIM(RTRIM(@TermStartDate)) )
                                                      OR ( DATEDIFF(DAY,@TermStartDate,TermStartDate) ) < 0
                                                    )
                                                AND (
                                                      ( @TermStartDateModifier <> '>=' )
                                                      OR ( TermStartDate >= LTRIM(RTRIM(@TermStartDate)) )
                                                      OR ( DATEDIFF(DAY,@TermStartDate,TermStartDate) ) >= 0
                                                    )
                                                AND (
                                                      ( @TermStartDateModifier <> '<=' )
                                                      OR ( TermStartDate <= LTRIM(RTRIM(@TermStartDate)) )
                                                      OR ( DATEDIFF(DAY,@TermStartDate,TermStartDate) ) <= 0
                                                    )
                                              )
                                            )
                             );
--Print @CumAverageSum
								
        IF @cumAveragecount >= 1
            BEGIN
                SET @CumAverage = @cumAverageSum / @cumAveragecount;
            END;
--Print @CumAverage

        PRINT @UnitTypeId;

        PRINT @TrackAttendanceBy;
        PRINT @displayhours;


-- Main query starts here 
        SELECT DISTINCT TOP 1
                S.SSN
               ,S.FirstName
               ,S.LastName
               ,SC.StatusCodeDescrip
               ,SE.StartDate AS StudentStartDate
               ,SE.ExpGradDate AS StudentExpectedGraduationDate
               ,PV.UnitTypeId
               ,CASE WHEN @TermStartDate IS NULL THEN (
                                                        SELECT  MAX(LDA)
                                                        FROM    (
                                                                  SELECT    MAX(AttendedDate) AS LDA
                                                                  FROM      arExternshipAttendance
                                                                  WHERE     StuEnrollId = @StuEnrollId
                                                                  UNION ALL
                                                                  SELECT    MAX(MeetDate) AS LDA
                                                                  FROM      atClsSectAttendance
                                                                  WHERE     StuEnrollId = @StuEnrollId
                                                                            AND Actual > 0
                                                                  UNION ALL
                                                                  SELECT    MAX(AttendanceDate) AS LDA
                                                                  FROM      atAttendance
                                                                  WHERE     EnrollId = @StuEnrollId
                                                                            AND Actual > 0
                                                                  UNION ALL
                                                                  SELECT    MAX(RecordDate) AS LDA
                                                                  FROM      arStudentClockAttendance
                                                                  WHERE     StuEnrollId = @StuEnrollId
                                                                            AND (
                                                                                  ActualHours > 0
                                                                                  AND ActualHours <> 99.00
                                                                                  AND ActualHours <> 999.00
                                                                                  AND ActualHours <> 9999.00
                                                                                )
                                                                  UNION ALL
                                                                  SELECT    MAX(MeetDate) AS LDA
                                                                  FROM      atConversionAttendance
                                                                  WHERE     StuEnrollId = @StuEnrollId
                                                                            AND (
                                                                                  Actual > 0
                                                                                  AND Actual <> 99.00
                                                                                  AND Actual <> 999.00
                                                                                  AND Actual <> 9999.00
                                                                                )
                                                                  UNION ALL
                                                                  SELECT TOP 1
                                                                            LDA
                                                                  FROM      arStuEnrollments
                                                                  WHERE     StuEnrollId = @StuEnrollId
                                                                ) TR
                                                      )
                     ELSE 
		-- Get the LDA Based on Term Start Date
                          (
                            SELECT  MAX(LDA)
                            FROM    (
                                      SELECT TOP 1
                                                LDA
                                      FROM      arStuEnrollments
                                      WHERE     StuEnrollId = @StuEnrollId
                                      UNION ALL
                                      SELECT    MAX(AttendedDate) AS LDA
                                      FROM      arExternshipAttendance
                                      WHERE     StuEnrollId = @StuEnrollId
                                                AND
						--(@TermId is null or TermId in (Select Val from [MultipleValuesForReportParameters](@TermId,',',1))) and
                                                (
                                                  @TermStartDate IS NULL
                                                  OR @TermStartDateModifier IS NULL
                                                  OR (
                                                       (
                                                         ( @TermStartDateModifier <> '=' )
                                                         OR ( AttendedDate = @TermStartDate )
                                                       )
                                                       AND (
                                                             ( @TermStartDateModifier <> '>' )
                                                             OR ( AttendedDate > @TermStartDate )
                                                           )
                                                       AND (
                                                             ( @TermStartDateModifier <> '<' )
                                                             OR ( AttendedDate < @TermStartDate )
                                                           )
                                                       AND (
                                                             ( @TermStartDateModifier <> '>=' )
                                                             OR ( AttendedDate >= @TermStartDate )
                                                           )
                                                       AND (
                                                             ( @TermStartDateModifier <> '<=' )
                                                             OR ( AttendedDate <= @TermStartDate )
                                                           )
                                                     )
                                                )
                                      UNION ALL
                                      SELECT    MAX(MeetDate) AS LDA
                                      FROM      atClsSectAttendance t1
                                               ,arClassSections t2
                                      WHERE     t1.ClsSectionId = t2.ClsSectionId
                                                AND StuEnrollId = @StuEnrollId
                                                AND Actual > 0
                                                AND
						-- (@TermId is null or t2.TermId in (Select Val from [MultipleValuesForReportParameters](@TermId,',',1))) and
                                                (
                                                  @TermStartDate IS NULL
                                                  OR @TermStartDateModifier IS NULL
                                                  OR (
                                                       (
                                                         ( @TermStartDateModifier <> '=' )
                                                         OR ( MeetDate = @TermStartDate )
                                                       )
                                                       AND (
                                                             ( @TermStartDateModifier <> '>' )
                                                             OR ( MeetDate > @TermStartDate )
                                                           )
                                                       AND (
                                                             ( @TermStartDateModifier <> '<' )
                                                             OR ( MeetDate < @TermStartDate )
                                                           )
                                                       AND (
                                                             ( @TermStartDateModifier <> '>=' )
                                                             OR ( MeetDate >= @TermStartDate )
                                                           )
                                                       AND (
                                                             ( @TermStartDateModifier <> '<=' )
                                                             OR ( MeetDate <= @TermStartDate )
                                                           )
                                                     )
                                                )
                                      UNION ALL
                                      SELECT    MAX(RecordDate) AS LDA
                                      FROM      arStudentClockAttendance
                                      WHERE     StuEnrollId = @StuEnrollId
                                                AND (
                                                      ActualHours > 0
                                                      AND ActualHours <> 99.00
                                                      AND ActualHours <> 999.00
                                                      AND ActualHours <> 9999.00
                                                    )
                                                AND
						--(@TermId is null or TermId in (Select Val from [MultipleValuesForReportParameters](@TermId,',',1))) and
                                                (
                                                  @TermStartDate IS NULL
                                                  OR @TermStartDateModifier IS NULL
                                                  OR (
                                                       (
                                                         ( @TermStartDateModifier <> '=' )
                                                         OR ( RecordDate = @TermStartDate )
                                                       )
                                                       AND (
                                                             ( @TermStartDateModifier <> '>' )
                                                             OR ( RecordDate > @TermStartDate )
                                                           )
                                                       AND (
                                                             ( @TermStartDateModifier <> '<' )
                                                             OR ( RecordDate < @TermStartDate )
                                                           )
                                                       AND (
                                                             ( @TermStartDateModifier <> '>=' )
                                                             OR ( RecordDate >= @TermStartDate )
                                                           )
                                                       AND (
                                                             ( @TermStartDateModifier <> '<=' )
                                                             OR ( RecordDate <= @TermStartDate )
                                                           )
                                                     )
                                                )
                                      UNION ALL
                                      SELECT    MAX(MeetDate) AS LDA
                                      FROM      atConversionAttendance
                                      WHERE     StuEnrollId = @StuEnrollId
                                                AND (
                                                      Actual > 0
                                                      AND Actual <> 99.00
                                                      AND Actual <> 999.00
                                                      AND Actual <> 9999.00
                                                    )
                                                AND
					--(@TermId is null or TermId in (Select Val from [MultipleValuesForReportParameters](@TermId,',',1))) and
                                                (
                                                  @TermStartDate IS NULL
                                                  OR @TermStartDateModifier IS NULL
                                                  OR (
                                                       (
                                                         ( @TermStartDateModifier <> '=' )
                                                         OR ( MeetDate = @TermStartDate )
                                                       )
                                                       AND (
                                                             ( @TermStartDateModifier <> '>' )
                                                             OR ( MeetDate > @TermStartDate )
                                                           )
                                                       AND (
                                                             ( @TermStartDateModifier <> '<' )
                                                             OR ( MeetDate < @TermStartDate )
                                                           )
                                                       AND (
                                                             ( @TermStartDateModifier <> '>=' )
                                                             OR ( MeetDate >= @TermStartDate )
                                                           )
                                                       AND (
                                                             ( @TermStartDateModifier <> '<=' )
                                                             OR ( MeetDate <= @TermStartDate )
                                                           )
                                                     )
                                                )
                                    ) TR
                          )
                END AS StudentLastDateAttended
               ,
 --SE.LDA AS StudentLastDateAttended,
                (
                  SELECT    SUM(PSD.total)
                  FROM      arStudentSchedules SS
                           ,arProgScheduleDetails PSD
                  WHERE     StuEnrollId = @StuEnrollId
                            AND SS.ScheduleId = PSD.ScheduleId
                ) AS WeeklySchedule
               , 
	--End 
                CASE WHEN (
                            LOWER(RTRIM(LTRIM(@TrackAttendanceBy))) = 'byclass'
                            AND LOWER(RTRIM(LTRIM(@displayhours))) = 'hours'
                            AND LTRIM(RTRIM(PV.UnitTypeId)) = 'EF5535C2-142C-4223-AE3C-25A50A153CC6'
                          ) -- PA
                          THEN @ActualPresentDays_ConvertTo_Hours
                     ELSE CASE WHEN (
                                      LOWER(RTRIM(LTRIM(@TrackAttendanceBy))) = 'byclass'
                                      AND LOWER(RTRIM(LTRIM(@displayhours))) = 'hours'
                                      AND LTRIM(RTRIM(PV.UnitTypeId)) = 'A1389C74-0BB9-4BBF-A47F-68428BE7FA4D'
                                    ) -- Minutes
                                    THEN CASE WHEN @TermStartDate IS NULL THEN SAS_NoTerm.AttendedDays / 60
                                              ELSE SAS_ForTerm.AttendedDays / 60
                                         END
                               WHEN (
                                      LOWER(RTRIM(LTRIM(@TrackAttendanceBy))) = 'byclass'
                                      AND LOWER(RTRIM(LTRIM(@displayhours))) = 'hours'
                                      AND LTRIM(RTRIM(PV.UnitTypeId)) = 'B937C92E-FD7A-455E-A731-527A9918C734'
                                    ) -- Clock Hour
                                    THEN CASE WHEN @TermStartDate IS NULL THEN ( SAS_NoTerm.AttendedDays / 60 )
                                              ELSE ( SAS_ForTerm.AttendedDays / 60 )
                                         END
                               WHEN (
                                      LOWER(RTRIM(LTRIM(@TrackAttendanceBy))) = 'byclass'
                                      AND LOWER(RTRIM(LTRIM(@displayhours))) = 'presentabsent'
                                      AND LTRIM(RTRIM(PV.UnitTypeId)) = 'A1389C74-0BB9-4BBF-A47F-68428BE7FA4D'
                                    ) -- Minutes
                                    THEN CASE WHEN @TermStartDate IS NULL THEN ( ( SAS_NoTerm.AttendedDays / 60 ) / 24 )
                                              ELSE ( ( SAS_ForTerm.AttendedDays / 60 ) / 24 )
                                         END
                               ELSE CASE WHEN @TermStartDate IS NULL THEN SAS_NoTerm.AttendedDays
                                         ELSE SAS_ForTerm.AttendedDays
                                    END
                          END
                END AS TotalDaysAttended
               ,CASE WHEN (
                            LOWER(RTRIM(LTRIM(@TrackAttendanceBy))) = 'byclass'
                            AND LOWER(RTRIM(LTRIM(@displayhours))) = 'hours'
                            AND LTRIM(RTRIM(PV.UnitTypeId)) = 'EF5535C2-142C-4223-AE3C-25A50A153CC6'
                          ) THEN @Scheduledhours
                     ELSE CASE WHEN (
                                      LOWER(RTRIM(LTRIM(@TrackAttendanceBy))) = 'byclass'
                                      AND LOWER(RTRIM(LTRIM(@displayhours))) = 'hours'
                                      AND LTRIM(RTRIM(PV.UnitTypeId)) = 'A1389C74-0BB9-4BBF-A47F-68428BE7FA4D'
                                    ) -- Minutes
                                    THEN CASE WHEN @TermStartDate IS NULL THEN SAS_NoTerm.ScheduledDays / 60
                                              ELSE SAS_ForTerm.ScheduledDays / 60
                                         END
                               WHEN (
                                      LOWER(RTRIM(LTRIM(@TrackAttendanceBy))) = 'byclass'
                                      AND LOWER(RTRIM(LTRIM(@displayhours))) = 'hours'
                                      AND LTRIM(RTRIM(PV.UnitTypeId)) = 'B937C92E-FD7A-455E-A731-527A9918C734'
                                    ) -- Clock Hour
                                    THEN CASE WHEN @TermStartDate IS NULL THEN SAS_NoTerm.ScheduledDays / 60
                                              ELSE SAS_ForTerm.ScheduledDays / 60
                                         END
                               WHEN (
                                      LOWER(RTRIM(LTRIM(@TrackAttendanceBy))) = 'byclass'
                                      AND LOWER(RTRIM(LTRIM(@displayhours))) = 'presentabsent'
                                      AND LTRIM(RTRIM(PV.UnitTypeId)) = 'A1389C74-0BB9-4BBF-A47F-68428BE7FA4D'
                                    ) -- Minutes
                                    THEN CASE WHEN @TermStartDate IS NULL THEN ( ( SAS_NoTerm.ScheduledDays / 60 ) / 24 )
                                              ELSE ( ( SAS_ForTerm.ScheduledDays / 60 ) / 24 )
                                         END
                               ELSE CASE WHEN @TermStartDate IS NULL THEN ( SAS_NoTerm.ScheduledDays )
                                         ELSE ( SAS_ForTerm.ScheduledDays )
                                    END
                          END
                END AS ScheduledDays
               ,CASE WHEN (
                            LOWER(RTRIM(LTRIM(@TrackAttendanceBy))) = 'byclass'
                            AND LOWER(RTRIM(LTRIM(@displayhours))) = 'hours'
                            AND LTRIM(RTRIM(PV.UnitTypeId)) = 'EF5535C2-142C-4223-AE3C-25A50A153CC6'
                          ) THEN @ActualAbsentDays_ConvertTo_Hours
                     ELSE CASE WHEN (
                                      LOWER(RTRIM(LTRIM(@TrackAttendanceBy))) = 'byclass'
                                      AND LOWER(RTRIM(LTRIM(@displayhours))) = 'hours'
                                      AND LTRIM(RTRIM(PV.UnitTypeId)) = 'A1389C74-0BB9-4BBF-A47F-68428BE7FA4D'
                                    ) -- Minutes
                                    THEN CASE WHEN @TermStartDate IS NULL THEN SAS_NoTerm.AbsentDays / 60
                                              ELSE SAS_ForTerm.AbsentDays / 60
                                         END
                               WHEN (
                                      LOWER(RTRIM(LTRIM(@TrackAttendanceBy))) = 'byclass'
                                      AND LOWER(RTRIM(LTRIM(@displayhours))) = 'hours'
                                      AND LTRIM(RTRIM(PV.UnitTypeId)) = 'B937C92E-FD7A-455E-A731-527A9918C734'
                                    ) -- Clock Hour
                                    THEN CASE WHEN @TermStartDate IS NULL THEN SAS_NoTerm.AbsentDays / 60
                                              ELSE SAS_ForTerm.AbsentDays / 60
                                         END
                               WHEN (
                                      LOWER(RTRIM(LTRIM(@TrackAttendanceBy))) = 'byclass'
                                      AND LOWER(RTRIM(LTRIM(@displayhours))) = 'presentabsent'
                                      AND LTRIM(RTRIM(PV.UnitTypeId)) = 'A1389C74-0BB9-4BBF-A47F-68428BE7FA4D'
                                    ) -- Minutes
                                    THEN CASE WHEN @TermStartDate IS NULL THEN ( ( SAS_NoTerm.AbsentDays / 60 ) / 24 )
                                              ELSE ( ( SAS_ForTerm.AbsentDays / 60 ) / 24 )
                                         END
                               ELSE CASE WHEN @TermStartDate IS NULL THEN ( SAS_NoTerm.AbsentDays )
                                         ELSE ( SAS_ForTerm.AbsentDays )
                                    END
                          END
                END AS DaysAbsent
               ,CASE WHEN (
                            LOWER(RTRIM(LTRIM(@TrackAttendanceBy))) = 'byclass'
                            AND LOWER(RTRIM(LTRIM(@displayhours))) = 'hours'
                            AND LTRIM(RTRIM(PV.UnitTypeId)) = 'EF5535C2-142C-4223-AE3C-25A50A153CC6'
                          ) THEN SAS_ForTerm.MakeupDays
                     ELSE CASE WHEN (
                                      LOWER(RTRIM(LTRIM(@TrackAttendanceBy))) = 'byclass'
                                      AND LOWER(RTRIM(LTRIM(@displayhours))) = 'hours'
                                      AND LTRIM(RTRIM(PV.UnitTypeId)) = 'A1389C74-0BB9-4BBF-A47F-68428BE7FA4D'
                                    ) -- Minutes
                                    THEN CASE WHEN @TermStartDate IS NULL THEN SAS_NoTerm.MakeupDays / 60
                                              ELSE SAS_ForTerm.MakeupDays / 60
                                         END
                               WHEN (
                                      LOWER(RTRIM(LTRIM(@TrackAttendanceBy))) = 'byclass'
                                      AND LOWER(RTRIM(LTRIM(@displayhours))) = 'hours'
                                      AND LTRIM(RTRIM(PV.UnitTypeId)) = 'B937C92E-FD7A-455E-A731-527A9918C734'
                                    ) -- Clock Hour
                                    THEN CASE WHEN @TermStartDate IS NULL THEN SAS_NoTerm.MakeupDays / 60
                                              ELSE SAS_ForTerm.MakeupDays / 60
                                         END
                               WHEN (
                                      LOWER(RTRIM(LTRIM(@TrackAttendanceBy))) = 'byclass'
                                      AND LOWER(RTRIM(LTRIM(@displayhours))) = 'presentabsent'
                                      AND LTRIM(RTRIM(PV.UnitTypeId)) = 'A1389C74-0BB9-4BBF-A47F-68428BE7FA4D'
                                    ) -- Minutes
                                    THEN CASE WHEN @TermStartDate IS NULL THEN ( ( SAS_NoTerm.MakeupDays / 60 ) / 24 )
                                              ELSE ( ( SAS_ForTerm.MakeupDays / 60 ) / 24 )
                                         END
                               ELSE CASE WHEN @TermStartDate IS NULL THEN ( SAS_NoTerm.MakeupDays )
                                         ELSE ( SAS_ForTerm.MakeupDays )
                                    END
                          END
                END AS MakeupDays
               ,SE.StuEnrollId
               ,@SUMCreditsEarned AS CreditsEarned
               ,@SUMFACreditsEarned AS FACreditsEarned
               ,SE.DateDetermined AS DateDetermined
               ,SYS.InSchool
               ,P.ProgDescrip
               ,(
                  SELECT    SUM(ISNULL(TransAmount,0))
                  FROM      saTransactions t1
                           ,saTransCodes t2
                  WHERE     t1.TransCodeId = t2.TransCodeId
                            AND t1.StuEnrollId = @StuEnrollId
                            AND t2.IsInstCharge = 1
                            AND t1.TransTypeId IN ( 0,1 )
                            AND t1.Voided = 0
                            AND
	--(@TermId is null or TermId in (Select Val from [MultipleValuesForReportParameters](@TermId,',',1))) and
                            (
                              @TermStartDate IS NULL
                              OR @TermStartDateModifier IS NULL
                              OR 
				--(
				--	((@TermStartDateModifier <> '=') OR (Convert(char(10),TransDate,101)= ltrim(rtrim(@TermStartDate))) OR (TransDate= ltrim(rtrim(@TermStartDate)))) 
				--	And ((@TermStartDateModifier <> '>') OR (Convert(char(10),TransDate,101) > ltrim(rtrim(@TermStartDate))) OR (TransDate > ltrim(rtrim(@TermStartDate)))) 
				--	And ((@TermStartDateModifier <> '<') OR (Convert(char(10),TransDate,101) < ltrim(rtrim(@TermStartDate))) OR (TransDate < ltrim(rtrim(@TermStartDate))))	
				--	And ((@TermStartDateModifier <> '>=') OR (Convert(char(10),TransDate,101) >= ltrim(rtrim(@TermStartDate))) OR (TransDate >= ltrim(rtrim(@TermStartDate)))) 
				--	And ((@TermStartDateModifier <> '<=') OR (Convert(char(10),TransDate,101) <= ltrim(rtrim(@TermStartDate))) OR (TransDate <= ltrim(rtrim(@TermStartDate))))
				--)
                              (
                                (
                                  ( @TermStartDateModifier <> '=' )
                                  OR ( TransDate = LTRIM(RTRIM(@TermStartDate)) )
                                  OR ( DATEDIFF(DAY,@TermStartDate,TransDate) ) = 0
                                )
                                AND (
                                      ( @TermStartDateModifier <> '>' )
                                      OR ( TransDate > LTRIM(RTRIM(@TermStartDate)) )
                                      OR ( DATEDIFF(DAY,@TermStartDate,TransDate) ) > 0
                                    )
                                AND (
                                      ( @TermStartDateModifier <> '<' )
                                      OR ( TransDate < LTRIM(RTRIM(@TermStartDate)) )
                                      OR ( DATEDIFF(DAY,@TermStartDate,TransDate) ) < 0
                                    )
                                AND (
                                      ( @TermStartDateModifier <> '>=' )
                                      OR ( TransDate >= LTRIM(RTRIM(@TermStartDate)) )
                                      OR ( DATEDIFF(DAY,@TermStartDate,TransDate) ) >= 0
                                    )
                                AND (
                                      ( @TermStartDateModifier <> '<=' )
                                      OR ( TransDate <= LTRIM(RTRIM(@TermStartDate)) )
                                      OR ( DATEDIFF(DAY,@TermStartDate,TransDate) ) <= 0
                                    )
                              )
                            )
                ) AS TotalCost
               ,(
                  SELECT    SUM(ISNULL(TransAmount,0)) AS CurrentBalance
                  FROM      saTransactions
                  WHERE     StuEnrollId = @StuEnrollId
                            AND Voided = 0
                            AND 
	--(@TermId is null or TermId in (Select Val from [MultipleValuesForReportParameters](@TermId,',',1))) and
                            (
                              @TermStartDate IS NULL
                              OR @TermStartDateModifier IS NULL
                              OR 
			--(
			--	((@TermStartDateModifier <> '=') OR (Convert(char(10),TransDate,101)= ltrim(rtrim(@TermStartDate)))) 
			--	And ((@TermStartDateModifier <> '>') OR (Convert(char(10),TransDate,101) > ltrim(rtrim(@TermStartDate)))) 
			--	And ((@TermStartDateModifier <> '<') OR (Convert(char(10),TransDate,101) < ltrim(rtrim(@TermStartDate))))	
			--	And ((@TermStartDateModifier <> '>=') OR (Convert(char(10),TransDate,101) >= ltrim(rtrim(@TermStartDate)))) 
			--	And ((@TermStartDateModifier <> '<=') OR (Convert(char(10),TransDate,101) <= ltrim(rtrim(@TermStartDate))))
			--)
                              (
                                (
                                  ( @TermStartDateModifier <> '=' )
                                  OR ( TransDate = LTRIM(RTRIM(@TermStartDate)) )
                                  OR ( DATEDIFF(DAY,@TermStartDate,TransDate) ) = 0
                                )
                                AND (
                                      ( @TermStartDateModifier <> '>' )
                                      OR ( TransDate > LTRIM(RTRIM(@TermStartDate)) )
                                      OR ( DATEDIFF(DAY,@TermStartDate,TransDate) ) > 0
                                    )
                                AND (
                                      ( @TermStartDateModifier <> '<' )
                                      OR ( TransDate < LTRIM(RTRIM(@TermStartDate)) )
                                      OR ( DATEDIFF(DAY,@TermStartDate,TransDate) ) < 0
                                    )
                                AND (
                                      ( @TermStartDateModifier <> '>=' )
                                      OR ( TransDate >= LTRIM(RTRIM(@TermStartDate)) )
                                      OR ( DATEDIFF(DAY,@TermStartDate,TransDate) ) >= 0
                                    )
                                AND (
                                      ( @TermStartDateModifier <> '<=' )
                                      OR ( TransDate <= LTRIM(RTRIM(@TermStartDate)) )
                                      OR ( DATEDIFF(DAY,@TermStartDate,TransDate) ) <= 0
                                    )
                              )
                            )
                ) AS CurrentBalance
               ,@CumAverage AS OverallAverage
               ,@cumWeightedGPA AS WeightedAverage_CumGPA
               ,@cumSimpleGPA AS SimpleAverage_CumGPA
               ,@ReturnValue AS StudentGroups
               ,CASE WHEN P.ACId = 5 THEN 'True'
                     ELSE 'False'
                END AS ClockHourProgram
               ,CASE WHEN LTRIM(RTRIM(PV.UnitTypeId)) = 'EF5535C2-142C-4223-AE3C-25A50A153CC6'
                          AND LOWER(RTRIM(LTRIM(@displayhours))) = 'hours' THEN 'Hours'
                     WHEN LTRIM(RTRIM(PV.UnitTypeId)) = 'EF5535C2-142C-4223-AE3C-25A50A153CC6'
                          AND LOWER(RTRIM(LTRIM(@displayhours))) <> 'hours' THEN 'Days'
                     ELSE 'Hours'
                END AS UnitTypeDescrip
        FROM    arStudent S
        INNER JOIN arStuEnrollments SE ON S.StudentId = SE.StudentId
        INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
        INNER JOIN arPrograms P ON PV.ProgId = P.ProgId
        INNER JOIN syStatusCodes SC ON SE.StatusCodeId = SC.StatusCodeId
        INNER JOIN sySysStatus SYS ON SC.SysStatusId = SYS.SysStatusId
        LEFT JOIN (
                    SELECT  StuEnrollId
                           ,MAX(ActualRunningScheduledDays) AS ScheduledDays
                           ,MAX(AdjustedPresentDays) AS AttendedDays
                           ,MAX(AdjustedAbsentDays) AS AbsentDays
                           ,MAX(ActualRunningMakeupDays) AS MakeupDays
                           ,MAX(StudentAttendedDate) AS LDA
                    FROM    syStudentAttendanceSummary
                    GROUP BY StuEnrollId
                  ) SAS_NoTerm ON SE.StuEnrollId = SAS_NoTerm.StuEnrollId
        LEFT JOIN (
                    SELECT  StuEnrollId
                           ,MAX(ActualRunningScheduledDays) AS ScheduledDays
                           ,MAX(AdjustedPresentDays) AS AttendedDays
                           ,MAX(AdjustedAbsentDays) AS AbsentDays
                           ,MAX(ActualRunningMakeupDays) AS MakeupDays
                           ,MAX(StudentAttendedDate) AS LDA
                    FROM    syStudentAttendanceSummary
                    WHERE   (
                              @TermStartDate IS NULL
                              OR @TermStartDateModifier IS NULL
                              OR (
                                   (
                                     ( @TermStartDateModifier <> '=' )
                                     OR ( StudentAttendedDate = LTRIM(RTRIM(@TermStartDate)) )
                                     OR ( DATEDIFF(DAY,@TermStartDate,StudentAttendedDate) ) = 0
                                   )
                                   AND (
                                         ( @TermStartDateModifier <> '>' )
                                         OR ( StudentAttendedDate > LTRIM(RTRIM(@TermStartDate)) )
                                         OR ( DATEDIFF(DAY,@TermStartDate,StudentAttendedDate) ) > 0
                                       )
                                   AND (
                                         ( @TermStartDateModifier <> '<' )
                                         OR ( StudentAttendedDate < LTRIM(RTRIM(@TermStartDate)) )
                                         OR ( DATEDIFF(DAY,@TermStartDate,StudentAttendedDate) ) < 0
                                       )
                                   AND (
                                         ( @TermStartDateModifier <> '>=' )
                                         OR ( StudentAttendedDate >= LTRIM(RTRIM(@TermStartDate)) )
                                         OR ( DATEDIFF(DAY,@TermStartDate,StudentAttendedDate) ) >= 0
                                       )
                                   AND (
                                         ( @TermStartDateModifier <> '<=' )
                                         OR ( StudentAttendedDate <= LTRIM(RTRIM(@TermStartDate)) )
                                         OR ( DATEDIFF(DAY,@TermStartDate,StudentAttendedDate) ) <= 0
                                       )
                                 )
                            )
                    GROUP BY StuEnrollId
                  ) SAS_ForTerm ON SE.StuEnrollId = SAS_ForTerm.StuEnrollId
        WHERE   ( SE.StuEnrollId = @StuEnrollId ); 
    END;
--=================================================================================================
-- END  --  USP_ProgressReport_StudentData_GetList
--=================================================================================================
GO
