SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[USP_TimeInterval_Update]
    (
     @TimeIntervalId UNIQUEIDENTIFIER
    ,@TimeIntervalDescrip DATETIME = NULL
    ,@StatusId UNIQUEIDENTIFIER
    ,@UserName VARCHAR(50)
    ,@ModDate DATETIME
    )
AS /*----------------------------------------------------------------------------------------------------
	Author          :	Janet Robinson
    
    Create date		:	08/28/2011
    
	Procedure Name	:	[USP_TimeInterval_Update]

	Objective		:	Updates table cmTimeInterval
	
	Parameters		:	Name						Type	Data Type				Required? 	
						=====						====	=========				=========	
						@TimeIntervalId				In		UniqueIDENTIFIER		Required
						@TimeIntervalDescrip		In		datetime				No												
						@StatusId					In		UniqueIDENTIFIER		Required						
						@UserName					In		varchar					No
						@ModDate                    In		datetime				Required
	Output			:			
						
*/-----------------------------------------------------------------------------------------------------

    BEGIN

        UPDATE  cmTimeInterval
        SET     TimeIntervalId = @TimeIntervalId
               ,TimeIntervalDescrip = @TimeIntervalDescrip
               ,StatusId = @StatusId
               ,CampGrpId = (
                              SELECT    CampGrpId
                              FROM      syCampGrps
                              WHERE     RTRIM(CampGrpCode) = 'All'
                            )
               ,ModUser = @UserName
               ,ModDate = @ModDate
        WHERE   TimeIntervalId = @TimeIntervalId;
		 
    END;


--END OF SCRIPT********************************************************

---------------------------------------------------------------
---------------------------------------------------------------
    IF NOT EXISTS ( SELECT  COLUMN_NAME = LEFT(COLUMN_NAME,20)
                    FROM    INFORMATION_SCHEMA.COLUMNS
                    WHERE   TABLE_NAME = 'arClsSectMeetings'
                            AND LOWER(COLUMN_NAME) = 'InstructionTypeID' )
        BEGIN
            ALTER TABLE dbo.arClsSectMeetings ADD InstructionTypeID UNIQUEIDENTIFIER;
        END;



GO
