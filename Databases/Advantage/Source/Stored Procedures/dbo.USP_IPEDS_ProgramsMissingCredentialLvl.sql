SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[USP_IPEDS_ProgramsMissingCredentialLvl]
    @ProgId AS VARCHAR(8000) = NULL
   ,@CampusId VARCHAR(50)
AS
    BEGIN	
        SELECT  ProgDescrip
        FROM    arPrograms
        WHERE   (
                  (
                    StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
                    AND @ProgId IS NULL
                  )
                  OR ProgId IN ( SELECT Val
                                 FROM   MultipleValuesForReportParameters(@ProgId,',',1) )
                )
                AND ( CampGrpId IN ( SELECT CampGrpId
                                     FROM   syCmpGrpCmps
                                     WHERE  CampusId = @CampusId ) )
                AND (
                      CredentialLvlId IS NULL
                      OR LEN(LTRIM(RTRIM(CredentialLvlId))) = 0
                      OR NOT CredentialLvlId IN ( SELECT    CredentialId
                                                  FROM      arProgCredential
                                                  WHERE     IPEDSValue IS NOT NULL -- and statusid='F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
							   )
                    );
    END;
GO
