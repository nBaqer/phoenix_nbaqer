SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[USP_AR_RegStd_CheckStdScheduleConflict]
    (
     @StuEnrollid UNIQUEIDENTIFIER
    ,@ClsSectID UNIQUEIDENTIFIER
    ,@ErrorString VARCHAR(4000) OUTPUT
    )
AS /*----------------------------------------------------------------------------------------------------
	Author          :	Saraswathi Lakshmanan
    
    Create date		:	02/02/2010
    
	Procedure Name	:	USP_AR_RegStd_CheckStdScheduleConflict

	Objective		:	Check if student schedules conflict
	
	Parameters		:	Name			Type	Data Type			
						=====			====	=========			
						@StuEnrollid	In		UniqueIdentifier
						@ClsSectID		In		UniqueIdentifier
						@ErrorString	Out		Varchar
	
	Output			:	Returns the error message
	
	Dependent Procedures: 		USP_AR_RegStd_GetClsSectMeetings @ClsSectID			
						
*/-----------------------------------------------------------------------------------------------------
/*
Procedure created by Saraswathi Lakshmanan on Feb 1st 2010
--To fix issue 17548: ENH: All: Register Students Page is Taking Too Long to Populate Available Students 
*/

    BEGIN

        DECLARE @StartDate DATETIME
           ,@EndDate DATETIME;
--Get the Class Section Start and End Dates
        SET @StartDate = (
                           SELECT   StartDate
                           FROM     arClassSections
                           WHERE    ClsSectionId = @ClsSectID
                         );
        SET @EndDate = (
                         SELECT EndDate
                         FROM   arClassSections
                         WHERE  ClsSectionId = @ClsSectID
                       );

---get all the distinct Class Sections the student is registered for
        DECLARE @VariableStdClsSecttable TABLE
            (
             TestId UNIQUEIDENTIFIER
            ,ClsSection UNIQUEIDENTIFIER
            ,StartDate DATETIME
            ,EndDate DATETIME
            ); 
        INSERT  INTO @VariableStdClsSecttable
                SELECT  a.TestID
                       ,b.ClsSection
                       ,b.StartDate
                       ,b.EndDate
                FROM    arResults a
                INNER JOIN arClassSections b ON a.TestId = b.ClsSectionID
                WHERE   a.StuEnrollId = @StuEnrollid
                        AND ClsSectionId = @ClsSectID
                        AND NOT EXISTS ( SELECT OverridenConflictId
                                         FROM   arOverridenConflicts
                                         WHERE  leftClsSectionId = @ClsSectID
                                                OR rightClsSectionId = @ClsSectID );
	--
        DECLARE @TestIdforCur UNIQUEIDENTIFIER
           ,@ClsSectionforCur UNIQUEIDENTIFIER
           ,@StartDateforCur DATETIME
           ,@EndDAteforCur DATETIME;	
        DECLARE CurStdClsSects CURSOR READ_ONLY
        FOR
            (
              SELECT    TestId
                       ,ClsSection
                       ,StartDate
                       ,EndDate
              FROM      @VariableStdClsSecttable
            ); 

        OPEN CurStdClsSects;

        FETCH NEXT FROM CurStdClsSects
        INTO @TestIdforCur,@ClsSectionforCur,@StartDateforCur,@EndDAteforCur;

        WHILE @@FETCH_STATUS = 0
            BEGIN

            --A Variabletable is defined to hold the results from the Storedprocedure USP_GetCoursesFortheCourseGroups
                DECLARE @VariableClsSectMeettable1 TABLE
                    (
                     ClsSectMeetingId UNIQUEIDENTIFIER
                    ,WorkDaysId UNIQUEIDENTIFIER
                    ,StartTime DATETIME
                    ,EndTime DATETIME
                    ,Vieworder INTEGER
                    ); 

			--Get ClsSection Meetings for this ClsSection
                INSERT  INTO @VariableClsSectMeettable1
                        EXEC USP_AR_RegStd_GetClsSectMeetings @TestIdforCur;
            
             --A Variabletable is defined to hold the results from the Storedprocedure USP_GetCoursesFortheCourseGroups
                DECLARE @VariableClsSectMeettable2 TABLE
                    (
                     ClsSectMeetingId UNIQUEIDENTIFIER
                    ,WorkDaysId UNIQUEIDENTIFIER
                    ,StartTime DATETIME
                    ,EndTime DATETIME
                    ,Vieworder INTEGER
                    ); 
			
			--Get ClsSection Meetings for the ClsSection that Stud is registering for.
                INSERT  INTO @VariableClsSectMeettable2
                        EXEC USP_AR_RegStd_GetClsSectMeetings @ClsSectID;
            
            ---Check if start and end date range of class sections clash. If they is no conflict, then no need to go ahead
            --else check for other conflicts such as room, instructor, start & end times. 
                IF NOT (
                         (
                           @StartDateforCur = @StartDate
                           AND @EndDAteforCur = @EndDate
                         )
                         OR (
                              @StartDateforCur >= @StartDate
                              AND @EndDAteforCur >= @EndDate
                              AND @StartDateforCur <= @EndDate
                            )
                         OR (
                              @StartDateforCur <= @StartDate
                              AND @EndDAteforCur >= @StartDate
                              AND @EndDAteforCur <= @EndDate
                            )
                         OR (
                              @StartDateforCur >= @StartDate
                              AND @EndDAteforCur <= @EndDate
                            )
                         OR (
                              @StartDateforCur < @StartDate
                              AND @EndDAteforCur > @EndDate
                            )
                       )
                    BEGIN
                        RETURN '';
                    END;
			
			--Check for any Conflicts in the Cls Section Meetings
                DECLARE @CurClsSectMeetingId1 UNIQUEIDENTIFIER
                   ,@CurWorkDaysId1 UNIQUEIDENTIFIER
                   ,@CurStartTime1 DATETIME
                   ,@EndTime1 DATETIME;	

                DECLARE CurClsSectMeeting1 CURSOR READ_ONLY
                FOR
                    (
                      SELECT    ClsSectMeetingId
                               ,WorkDaysId
                               ,StartTime
                               ,EndTime
                      FROM      @VariableClsSectMeettable1
                    ); 

                OPEN CurClsSectMeeting1;

                FETCH NEXT FROM CurClsSectMeeting1
					INTO @CurClsSectMeetingId1,@CurWorkDaysId1,@CurStartTime1,@EndTime1;

                WHILE @@FETCH_STATUS = 0
                    BEGIN	
					
                        DECLARE @CurClsSectMeetingId2 UNIQUEIDENTIFIER
                           ,@CurWorkDaysId2 UNIQUEIDENTIFIER
                           ,@CurStartTime2 DATETIME
                           ,@EndTime2 DATETIME;	
                        DECLARE CurClsSectMeeting2 CURSOR READ_ONLY
                        FOR
                            (
                              SELECT    ClsSectMeetingId
                                       ,WorkDaysId
                                       ,StartTime
                                       ,EndTime
                              FROM      @VariableClsSectMeettable2
                            ); 

                        OPEN CurClsSectMeeting2;

                        FETCH NEXT FROM CurClsSectMeeting2
							INTO @CurClsSectMeetingId2,@CurWorkDaysId2,@CurStartTime2,@EndTime2;

                        WHILE @@FETCH_STATUS = 0
                            BEGIN	
                                IF NOT (
                                         ( @CurWorkDaysId1 = @CurWorkDaysId2 )
                                         OR ( @CurClsSectMeetingId1 = @CurClsSectMeetingId2 )
                                       )
                                    BEGIN
                                        RETURN '';
                                    END;
                                ELSE
                                    BEGIN
                                        IF NOT (
                                                 ( @EndTime2 <= @CurStartTime1 )
                                                 OR ( @CurStartTime2 >= @EndTime2 )
                                               )
                                            BEGIN
                                                RETURN 'Conflicting with class section ' + CONVERT(VARCHAR,@TestIdforCur) + ' that Student was previously registered for. ' + 
										CONVERT(VARCHAR,@ClsSectionforCur) + ' meets from ' + CONVERT(VARCHAR,@CurStartTime2) + ' to ' + CONVERT(VARCHAR,@EndTime2); --Error Message
                                            END;
                                    END;
                                FETCH NEXT FROM CurClsSectMeeting2
								INTO @CurClsSectMeetingId2,@CurWorkDaysId2,@CurStartTime2,@EndTime2;
                            END;
                        CLOSE CurClsSectMeeting2;
                        DEALLOCATE CurClsSectMeeting2;	
					--Write the code here
					
                        FETCH NEXT FROM CurClsSectMeeting1
					INTO @CurClsSectMeetingId1,@CurWorkDaysId1,@CurStartTime1,@EndTime1;

                    END;
                CLOSE CurClsSectMeeting1;
                DEALLOCATE CurClsSectMeeting1;	
	
                FETCH NEXT FROM CurStdClsSects
			INTO @TestIdforCur,@ClsSectionforCur,@StartDateforCur,@EndDAteforCur;
            END;
        CLOSE CurStdClsSects;
        DEALLOCATE CurStdClsSects;	
	
	
    END;



GO
