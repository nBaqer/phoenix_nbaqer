SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- Add into Program Version Definition all courses that do not appear in theirs definitions but show as taken by students. 
CREATE PROCEDURE [dbo].[InsertPrgVersionDef]
AS
    DECLARE @prgVerId AS UNIQUEIDENTIFIER;
    DECLARE @reqId AS UNIQUEIDENTIFIER;
    DECLARE @reqSeq AS TINYINT;
    DECLARE @incrReqSeq AS TINYINT;
    DECLARE getCoursesOutOfPrgVer_Cursor CURSOR
    FOR
        SELECT 
		DISTINCT
                E.PrgVerId
               ,C.ReqId
        FROM    arStuEnrollments E
               ,arStudent S
               ,arResults R
               ,arClassSections C
               ,arPrgVersions P
        WHERE   E.StudentId = S.StudentId
                AND E.StuEnrollId = R.StuEnrollId
                AND R.TestId = C.ClsSectionId
                AND E.PrgVerId = P.PrgVerId
                AND P.IsContinuingEd = 0
                AND NOT EXISTS ( SELECT 
						DISTINCT        t100.StuEnrollId
                                       ,t500.LastName
                                       ,t500.FirstName
                                       ,t100.ExpGradDate
                                       ,t100.PrgVerId
                                       ,t600.PrgVerDescrip
                                       ,t400.ReqId AS ReqId
                                       ,t3.Descrip AS Req
                                       ,t3.Code AS Code
                                       ,t3.Credits
                                       ,t3.Hours
                                 FROM   arStuEnrollments t100
                                       ,arReqs t3
                                       ,arProgVerDef t400
                                       ,arStudent t500
                                       ,arPrgVersions t600
                                 WHERE  t100.StudentId = t500.StudentId
                                        AND t100.PrgVerId = t600.PrgVerId
                                        AND t100.PrgVerId = t400.PrgVerId
                                        AND t3.ReqId = t400.ReqId
                                        AND t3.ReqTypeId = 1
                                        AND t100.StuEnrollId = E.StuEnrollId
                                        AND t3.ReqId = C.ReqId
                                 UNION
                                 SELECT 
						DISTINCT        t100.StuEnrollId
                                       ,t500.LastName
                                       ,t500.FirstName
                                       ,t100.ExpGradDate
                                       ,t100.PrgVerId
                                       ,t600.PrgVerDescrip
                                       ,t700.ReqId AS ReqId
                                       ,t3.Descrip AS Req
                                       ,t3.Code AS Code
                                       ,t3.Credits
                                       ,t3.Hours
                                 FROM   arStuEnrollments t100
                                       ,arReqs t3
                                       ,arProgVerDef t400
                                       ,arStudent t500
                                       ,arPrgVersions t600
                                       ,arReqGrpDef t700
                                 WHERE  t100.StudentId = t500.StudentId
                                        AND t100.PrgVerId = t600.PrgVerId
                                        AND t100.PrgVerId = t400.PrgVerId
                                        AND t400.ReqId = t700.GrpId
                                        AND t3.ReqId = t700.ReqId
                                        AND t3.ReqTypeId = 1
                                        AND t100.StuEnrollId = E.StuEnrollId
                                        AND t3.ReqId = C.ReqId
                                 UNION
                                 SELECT 
						DISTINCT        t100.StuEnrollId
                                       ,t500.LastName
                                       ,t500.FirstName
                                       ,t100.ExpGradDate
                                       ,t100.PrgVerId
                                       ,t600.PrgVerDescrip
                                       ,t800.ReqId AS ReqId
                                       ,t3.Descrip AS Req
                                       ,t3.Code AS Code
                                       ,t3.Credits
                                       ,t3.Hours
                                 FROM   arStuEnrollments t100
                                       ,arReqs t3
                                       ,arProgVerDef t400
                                       ,arStudent t500
                                       ,arPrgVersions t600
                                       ,arReqGrpDef t700
                                       ,arReqGrpDef t800
                                 WHERE  t100.StudentId = t500.StudentId
                                        AND t100.PrgVerId = t600.PrgVerId
                                        AND t100.PrgVerId = t400.PrgVerId
                                        AND t400.ReqId = t700.GrpId
                                        AND t700.ReqId = t800.GrpId
                                        AND t3.ReqId = t800.ReqId
                                        AND t3.ReqTypeId = 1
                                        AND t100.StuEnrollId = E.StuEnrollId
                                        AND t3.ReqId = C.ReqId
                                 UNION
                                 SELECT DISTINCT
                                        t100.StuEnrollId
                                       ,t500.LastName
                                       ,t500.FirstName
                                       ,t100.ExpGradDate
                                       ,t100.PrgVerId
                                       ,t600.PrgVerDescrip
                                       ,t900.ReqId AS ReqId
                                       ,t3.Descrip AS Req
                                       ,t3.Code AS Code
                                       ,t3.Credits
                                       ,t3.Hours
                                 FROM   arStuEnrollments t100
                                       ,arReqs t3
                                       ,arProgVerDef t400
                                       ,arStudent t500
                                       ,arPrgVersions t600
                                       ,arReqGrpDef t700
                                       ,arReqGrpDef t800
                                       ,arReqGrpDef t900
                                 WHERE  t100.StudentId = t500.StudentId
                                        AND t100.PrgVerId = t600.PrgVerId
                                        AND t100.PrgVerId = t400.PrgVerId
                                        AND t400.ReqId = t700.GrpId
                                        AND t700.ReqId = t800.GrpId
                                        AND t800.ReqId = t900.GrpId
                                        AND t3.ReqId = t900.ReqId
                                        AND t3.ReqTypeId = 1
                                        AND t100.StuEnrollId = E.StuEnrollId
                                        AND t3.ReqId = C.ReqId
                                 UNION
                                 SELECT DISTINCT
                                        t100.StuEnrollId
                                       ,t500.LastName
                                       ,t500.FirstName
                                       ,t100.ExpGradDate
                                       ,t100.PrgVerId
                                       ,t600.PrgVerDescrip
                                       ,t1000.ReqId AS ReqId
                                       ,t3.Descrip AS Req
                                       ,t3.Code AS Code
                                       ,t3.Credits
                                       ,t3.Hours
                                 FROM   arStuEnrollments t100
                                       ,arReqs t3
                                       ,arProgVerDef t400
                                       ,arStudent t500
                                       ,arPrgVersions t600
                                       ,arReqGrpDef t700
                                       ,arReqGrpDef t800
                                       ,arReqGrpDef t900
                                       ,arReqGrpDef t1000
                                 WHERE  t100.StudentId = t500.StudentId
                                        AND t100.PrgVerId = t600.PrgVerId
                                        AND t100.PrgVerId = t400.PrgVerId
                                        AND t400.ReqId = t700.GrpId
                                        AND t700.ReqId = t800.GrpId
                                        AND t800.ReqId = t900.GrpId
                                        AND t900.ReqId = t1000.GrpId
                                        AND t3.ReqId = t1000.ReqId
                                        AND t3.ReqTypeId = 1
                                        AND t100.StuEnrollId = E.StuEnrollId
                                        AND t3.ReqId = C.ReqId
                                 UNION
                                 SELECT DISTINCT
                                        t100.StuEnrollId
                                       ,t500.LastName
                                       ,t500.FirstName
                                       ,t100.ExpGradDate
                                       ,t100.PrgVerId
                                       ,t600.PrgVerDescrip
                                       ,t1100.ReqId AS ReqId
                                       ,t3.Descrip AS Req
                                       ,t3.Code AS Code
                                       ,t3.Credits
                                       ,t3.Hours
                                 FROM   arStuEnrollments t100
                                       ,arReqs t3
                                       ,arProgVerDef t400
                                       ,arStudent t500
                                       ,arPrgVersions t600
                                       ,arReqGrpDef t700
                                       ,arReqGrpDef t800
                                       ,arReqGrpDef t900
                                       ,arReqGrpDef t1000
                                       ,arReqGrpDef t1100
                                 WHERE  t100.StudentId = t500.StudentId
                                        AND t100.PrgVerId = t600.PrgVerId
                                        AND t100.PrgVerId = t400.PrgVerId
                                        AND t400.ReqId = t700.GrpId
                                        AND t700.ReqId = t800.GrpId
                                        AND t800.ReqId = t900.GrpId
                                        AND t900.ReqId = t1000.GrpId
                                        AND t1000.ReqId = t1100.GrpId
                                        AND t3.ReqId = t1100.ReqId
                                        AND t3.ReqTypeId = 1
                                        AND t100.StuEnrollId = E.StuEnrollId
                                        AND t3.ReqId = C.ReqId );

    OPEN getCoursesOutOfPrgVer_Cursor;

    FETCH NEXT FROM getCoursesOutOfPrgVer_Cursor
INTO @prgVerId,@reqId;

    WHILE @@FETCH_STATUS = 0
        BEGIN
            SET @reqSeq = (
                            SELECT  MAX(ReqSeq)
                            FROM    arProgVerDef
                            WHERE   PrgVerId = @prgVerId
                          );

            IF @reqSeq IS NULL
                BEGIN  
                    SET @reqSeq = 0;  
                END;  

            SET @incrReqSeq = @reqSeq + 1;

            INSERT  INTO arProgVerDef
                    (
                     PrgVerId
                    ,ReqId
                    ,ReqSeq
                    ,IsRequired
                    ,TrkForCompletion
                    ,ModDate
                    ,ModUser
                    )
            VALUES  (
                     @prgVerId
                    ,@reqId
                    ,@incrReqSeq
                    ,0
                    ,1
                    ,GETDATE()
                    ,'sa'
                    );


            FETCH NEXT FROM getCoursesOutOfPrgVer_Cursor
INTO @prgVerId,@reqId;

        END;

    CLOSE getCoursesOutOfPrgVer_Cursor;

    DEALLOCATE getCoursesOutOfPrgVer_Cursor;




GO
