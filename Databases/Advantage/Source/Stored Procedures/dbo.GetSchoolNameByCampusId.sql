SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		JAGG
-- Create date: 03/10/2015
-- Description:Get the SchoolName by CampusId
-- =============================================

CREATE PROCEDURE [dbo].[GetSchoolNameByCampusId] 
	-- Add the parameters for the stored procedure here
    @CampusId VARCHAR(100)
AS
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;
		-- Add the T-SQL statements to compute the return value here
        SELECT  cam.SchoolName
               ,cam.CampDescrip
        FROM    syCampuses cam
        WHERE   CampusId = @CampusId;

    END;




GO
