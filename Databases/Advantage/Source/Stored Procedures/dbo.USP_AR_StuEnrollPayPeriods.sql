SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


-- Batch submitted through debugger: SQLQuery2.sql|7|0|C:\Documents and Settings\dreissigmayer\Local Settings\Temp\~vs3BF.sql

-- Procedure To Generate stuenrollment Pay Periods
CREATE PROCEDURE [dbo].[USP_AR_StuEnrollPayPeriods]
--@studentid uniqueidentifier,
    @StudentEnrollmentId UNIQUEIDENTIFIER
AS --This Cursor will determine the Payment Periods
--based on a StudentId and StudentEnrollment.
--Will cut off periods BEFORE or AT the
--the payment period credit limit.
    DECLARE @StudentID AS UNIQUEIDENTIFIER;
    DECLARE @AcaYrLen AS DECIMAL(9,2);

    DECLARE @ProgramID AS UNIQUEIDENTIFIER;
    DECLARE @ProgramVersionID AS UNIQUEIDENTIFIER;
    DECLARE @AcaCalendar AS VARCHAR(50);
    DECLARE @ReqID AS UNIQUEIDENTIFIER;
    DECLARE @CourseCredits AS DECIMAL(18,2);
    DECLARE @FinAidCred AS DECIMAL(18,2);
    DECLARE @ReqHours AS DECIMAL(18,2);
    DECLARE @PrgVerHours AS DECIMAL(18,2);
    DECLARE @TotalProgCred AS DECIMAL(18,2);
    DECLARE @PayPerPerAcYr AS INT;
    DECLARE @ClassStartDate AS DATETIME;
    DECLARE @ClassEndDate AS DATETIME;
    DECLARE @TermStartDate AS DATETIME;
    DECLARE @TermEndDate AS DATETIME;
    DECLARE @PrevTermEndDate AS DATETIME;
    DECLARE @TableRowCount AS INT;

    DECLARE @AddCreditsCum AS DECIMAL(18,2);
    DECLARE @CumCreditsPrevious AS DECIMAL(18,2);
    DECLARE @CreditsPerPeriod AS DECIMAL(18,2);
    DECLARE @PeriodNumber AS INT;
    DECLARE @Counter AS INT;	
    DECLARE @PeriodStartDate AS DATETIME;
    DECLARE @PeriodEndDate AS DATETIME;
    DECLARE @PreviousEndDate AS DATETIME;
    DECLARE @TotalRowCount AS INT;

    DECLARE @ClkHrsPerPeriod AS DECIMAL(18,2);
    DECLARE @AddClkHrsNew AS DECIMAL(18,2);
    DECLARE @AddClkHrsPrevious AS DECIMAL(18,2);

    DECLARE @hdrrec_uniqueidentifier AS UNIQUEIDENTIFIER;

    DECLARE @PrevTermStartDate AS DATETIME;

    DECLARE @PrgWeeks AS DECIMAL(18,2);
    DECLARE @StuEnrollStartDate AS DATETIME;
    DECLARE @CreditsEarned AS DECIMAL(18,2);
    DECLARE @HoursEarned AS DECIMAL(18,2);
    DECLARE @IsPass AS BIT;
    DECLARE @WeeksPerPP AS DECIMAL(18,2);
    DECLARE @PrgWeeksPerPeriod AS DECIMAL(18,2);

    DECLARE @1stPPEndDateMinimum AS DATETIME;
    DECLARE @IsExternship AS BIT;

    DECLARE @LOA_StartDate AS DATETIME;
    DECLARE @LOA_Days AS INT;

    DECLARE @Comments AS VARCHAR(200);

    DECLARE @LOA_RowCount AS INT;
    DECLARE @LOA_LoopCount AS INT;
    DECLARE @LOA_DayCum AS INT;

    DECLARE @Is_LOA AS BIT;
    DECLARE @LoopCount AS INT;
    DECLARE @Range AS VARCHAR(10);
    DECLARE @OutsideLoopCount INT;
    DECLARE @CumHours AS DECIMAL(18,2);
    DECLARE @ProgCredPerPP AS DECIMAL(18,2);
    DECLARE @AcademicYear_INT INT;
    DECLARE @AcademicYear_INT_Count INT;

    DECLARE @Total_FACredits AS DECIMAL(18,2);


    IF OBJECT_ID('tempdb..#LOA') IS NOT NULL
        BEGIN
            DROP TABLE #LOA;
        END;
	
    CREATE TABLE #LOA
        (
         LOA_Row INT
        ,LOA_StartDate DATETIME
        ,LOA_Days INT
        );

    INSERT  INTO #LOA
            SELECT  ROW_NUMBER() OVER ( ORDER BY StartDate ) AS LOA_Row
                   ,StartDate
                   ,DATEDIFF(DAY,StartDate,EndDate)
            FROM    arStudentLOAs
            WHERE   StuEnrollId = @StudentEnrollmentId;
	
    SET @LOA_RowCount = (
                          SELECT    COUNT(*)
                          FROM      #LOA
                        );
    SET @LOA_LoopCount = 0;
    SET @LOA_DayCum = 0;
    SET @PrevTermStartDate = '1900-01-01';

    SET @AcademicYear_INT = 0;
    SET @AcademicYear_INT_Count = 0;

--
--set @StudentEnrollmentId = '5B27496C-BF7F-4A81-882D-00025893B2BA'
--set @StudentID = 'EA8551FA-E445-4140-ADAD-CBF006CA61FF' 



--IF OBJECT_ID('tempdb..#PayPeriods') IS NOT NULL
--	BEGIN
--		DROP TABLE #PayPeriods
--	END
	
--Get studentid
    SET @StudentID = (
                       SELECT TOP 1
                                studentid
                       FROM     dbo.arStuEnrollments
                       WHERE    StuEnrollId = @StudentEnrollmentId
                     );
				  
--Create Temp Table
    DECLARE @PayPeriods TABLE
--CREATE TABLE #PayPeriods
        (
         PaymentPeriod INT
        ,PaymentPeriodRange VARCHAR(12)
        ,AcaCalendar VARCHAR(50)
        ,StudentID UNIQUEIDENTIFIER
        ,StudentEnrollmentId UNIQUEIDENTIFIER
        ,ProgramID UNIQUEIDENTIFIER
        ,ProgramVersionID UNIQUEIDENTIFIER
        ,PeriodStartDate DATETIME
        ,PeriodEndDate DATETIME
        ,AcaYrLen INT
        ,ProgLength DECIMAL(18,2)
        ,Hours DECIMAL(18,2)
        ,TotalProgCred DECIMAL(18,2)
        ,PayPeriodsPerAcYr INT
        ,Comments VARCHAR(200)
        ,AcaYear_INT INT
        );

    DECLARE PaymentPeriods CURSOR
    FOR
        SELECT  ASE.studentID AS StudentID
               ,ASE.stuenrollid AS StudentEnrollmentId
               ,AP.ProgId AS ProgramID
               ,APV.PrgVerId AS ProgramVersionID
               ,SAC.ACDescrip AS AcaCalendar
               ,AR.ReqId AS ReqID
               ,APV.AcademicYearLength AS AcaYearLen
               ,ASE.StartDate AS EnrollmentStartDate
               ,AR.Credits AS CourseCredits
               ,AR.IsExternship AS IsExternship
               ,CASE AR.IsExternship
                  WHEN 1 THEN 1
                  ELSE
			 --This will check arResults (IsPass)	
                       COALESCE(COALESCE((
                                           SELECT   IsPass
                                           FROM     arGradeSystemDetails A
                                           LEFT JOIN arGradeScaleDetails B ON A.GrdSysDetailId = B.GrdSysDetailId
                                           WHERE    RES.Score BETWEEN B.MinVal AND B.MaxVal
                                                    AND ( APV.GrdSystemId = A.GrdSystemId )
                                                    AND ( RES.GrdSysDetailId = B.GrdSysDetailId )
                                         ),0),
			--This will check arTransferGrades (IsPass)	 
                                COALESCE((
                                           SELECT   IsPass
                                           FROM     arGradeSystemDetails A
                                           LEFT JOIN arGradeScaleDetails B ON A.GrdSysDetailId = B.GrdSysDetailId
                                           WHERE    TG.Score BETWEEN B.MinVal AND B.MaxVal
                                                    AND ( APV.GrdSystemId = A.GrdSystemId )
                                                    AND ( TG.GrdSysDetailId = B.GrdSysDetailId )
                                                    AND TG.IsTransferred = 0
                                         ),0))
                END AS IsPass
               , 
			 --This will check arResults (CreditsEarned)	 
                CASE COALESCE(COALESCE((
                                         SELECT IsPass
                                         FROM   arGradeSystemDetails A
                                         LEFT JOIN arGradeScaleDetails B ON A.GrdSysDetailId = B.GrdSysDetailId
                                         WHERE  RES.Score BETWEEN B.MinVal AND B.MaxVal
                                                AND ( APV.GrdSystemId = A.GrdSystemId )
                                                AND ( RES.GrdSysDetailId = B.GrdSysDetailId )
                                       ),0),
				  --This will check arTransferGrades (CreditsEarned)	
                              COALESCE((
                                         SELECT IsPass
                                         FROM   arGradeSystemDetails A
                                         LEFT JOIN arGradeScaleDetails B ON A.GrdSysDetailId = B.GrdSysDetailId
                                         WHERE  TG.Score BETWEEN B.MinVal AND B.MaxVal
                                                AND ( APV.GrdSystemId = A.GrdSystemId )
                                                AND ( TG.GrdSysDetailId = B.GrdSysDetailId )
                                                AND TG.IsTransferred = 0
                                       ),0))
				         --Check the Req  
                  WHEN 0 THEN COALESCE(COALESCE((
                                                  SELECT    Credits
                                                  FROM      dbo.arReqs
                                                  WHERE     ReqId = AR.ReqId
                                                            AND IsExternship = 1
                                                ),0),
						--Check the Req from arTransferGrades
                                       COALESCE((
                                                  SELECT    Credits
                                                  FROM      dbo.arReqs
                                                  WHERE     ReqId = TG.ReqId
                                                            AND AR.IsExternship = 1
                                                            AND TG.IsTransferred = 0
                                                ),0))
                  ELSE AR.Credits
                END AS CreditsEarned
               ,AR.FinAidCredits AS FinAidCred
               ,CASE COALESCE(COALESCE((
                                         SELECT IsPass
                                         FROM   arGradeSystemDetails A
                                         LEFT JOIN arGradeScaleDetails B ON A.GrdSysDetailId = B.GrdSysDetailId
                                         WHERE  RES.Score BETWEEN B.MinVal AND B.MaxVal
                                                AND ( APV.GrdSystemId = A.GrdSystemId )
                                                AND ( RES.GrdSysDetailId = B.GrdSysDetailId )
                                       ),0),COALESCE((
                                                       SELECT   IsPass
                                                       FROM     arGradeSystemDetails A
                                                       LEFT JOIN arGradeScaleDetails B ON A.GrdSysDetailId = B.GrdSysDetailId
                                                       WHERE    TG.Score BETWEEN B.MinVal AND B.MaxVal
                                                                AND ( APV.GrdSystemId = A.GrdSystemId )
                                                                AND ( TG.GrdSysDetailId = B.GrdSysDetailId )
                                                     ),0))
						 --Check the Req
                  WHEN 0 THEN COALESCE(COALESCE((
                                                  SELECT    Hours
                                                  FROM      dbo.arReqs
                                                  WHERE     ReqId = AR.ReqId
                                                            AND IsExternship = 1
                                                ),0),
			 			 --Check the Req from arTransferGrades
                                       COALESCE((
                                                  SELECT    Credits
                                                  FROM      dbo.arReqs
                                                  WHERE     ReqId = TG.ReqId
                                                            AND AR.IsExternship = 1
                                                            AND TG.IsTransferred = 0
                                                ),0))
                  ELSE AR.Hours
                END AS HoursEarned
               ,AR.Hours AS ReqHours
               ,APV.Hours AS PrgVerHours
               ,APV.Weeks AS PrgWeeks
               ,APV.Credits AS TotalProgCred
               ,APV.PayPeriodPerAcYear AS PayPerPerAcYr
               ,ACS.startdate AS ClassStartDate
               ,ACS.enddate AS ClassEndDate
               ,AT.startdate AS TermStartDate
               ,AT.enddate AS TermEndDate
        FROM    arClassSections ACS
        INNER JOIN arTerm AT ON ACS.TermId = AT.TermId
        INNER JOIN arReqs AR ON ACS.Reqid = AR.Reqid
        INNER JOIN arStuEnrollments ASE ON ASE.studentID = @StudentID
                                           AND ASE.StuEnrollId = @StudentEnrollmentId
        LEFT JOIN arTransferGrades TG ON ASE.StuEnrollId = TG.StuEnrollId
                                         AND AR.ReqId = TG.ReqId
        INNER JOIN arPrgVersions APV ON ASE.prgverid = APV.prgverid
        INNER JOIN arPrograms AP ON APV.ProgId = AP.ProgId
        INNER JOIN syAcademicCalendars SAC ON AP.ACId = SAC.ACId
        INNER JOIN arResults RES ON ACS.ClsSectionId = RES.TestId
        WHERE   RES.StuEnrollId = @StudentEnrollmentId
        ORDER BY ACS.startdate;
	   
    OPEN PaymentPeriods;

    FETCH NEXT FROM PaymentPeriods
INTO @StudentID,@StudentEnrollmentId,@ProgramID,@ProgramVersionID,@AcaCalendar,@ReqID,@AcaYrLen,@StuEnrollStartDate,@CourseCredits,@IsExternship,@IsPass,
        @CreditsEarned,@FinAidCred,@HoursEarned,@ReqHours,@PrgVerHours,@PrgWeeks,@TotalProgCred,@PayPerPerAcYr,@ClassStartDate,@ClassEndDate,@TermStartDate,
        @TermEndDate;
--, @LOA_StartDate, @LOA_Days


--IF Academic Year Length does not exist (resulting from bad data)
--than handle here. Bruce Shanblatt
    IF @AcaYrLen = 0
        BEGIN
            CLOSE PaymentPeriods;
            DEALLOCATE PaymentPeriods;
            RETURN;
        END;


--Initialize out needed vars here
    SET @PeriodNumber = 1;
    SET @Counter = 1;

    SET @AddCreditsCum = 0.00;
    SET @AddClkHrsNew = 0.00;

    SET @TableRowCount = (
                           SELECT   COUNT(*)
                           FROM     arResults
                           WHERE    StuEnrollId = @StudentEnrollmentId
                         );
    SET @TotalRowCount = 0;
    SET @OutsideLoopCount = 0;

--Set the Total FA Credits
    SET @Total_FACredits = (
                             SELECT SUM(FinAidCredits) AS FinAidCred
                             FROM   dbo.arReqs
                             WHERE  ReqId IN ( SELECT   ReqId
                                               FROM     dbo.arProgVerDef
                                               WHERE    PrgVerId = @ProgramVersionID )
                           );
		
--If Total FA Credits Exist, then use them, else, just use Total Credits
    IF @Total_FACredits <> 0
        BEGIN
            SET @TotalProgCred = @Total_FACredits;
        END;

    WHILE @@FETCH_STATUS = 0
        BEGIN


            SET @OutsideLoopCount = @OutsideLoopCount + 1;
--Here, We'll set the length. This needs to be commented out at production time. Right now, this 
--data does not exist in the database, so we're setting it here.

--set @AcaYrLen = 36

--Here, we are determining the credit threshold per payment period
            SET @CreditsPerPeriod = @AcaYrLen / @PayPerPerAcYr;
            SET @ClkHrsPerPeriod = @PrgVerHours / @PayPerPerAcYr;
--Need to divide the Program Weeks by the PP per Academic Year so we know
--how many weeks to make each payment period
            SET @PrgWeeksPerPeriod = @PrgWeeks / @PayPerPerAcYr;

--Here, we determine the Program credits per PP. For use then the Total Program Credits 
--are less than the Academic Year Length Credits
            SET @ProgCredPerPP = @TotalProgCred / @PayPerPerAcYr;

            SET @TotalRowCount = @TotalRowCount + 1;

	--Set all the preliminary data here at first pass.
            IF @Counter = 1
                BEGIN			
			--Get the start date for the Payment Period
                    SET @PeriodStartDate = @StuEnrollStartDate;
			--Determines the weeks in a Payment Period
                    SET @WeeksPerPP = @PrgWeeks / @PayPerPerAcYr;
			--This will set the 1st PP end date based on the number of weeks required (1/2 of the total program
			--weeks). This is the MINIMUM of weeks allowed in a payment period. The PP may extend beyond this.
                    SET @1stPPEndDateMinimum = (
                                                 SELECT DATEADD(DAY,( 7 * @WeeksPerPP ),@PeriodStartDate)
                                               );
                END;

--If FA Credits exist, use them as Credits Earned or else, use Credits Earned
            IF @FinAidCred <> 0
                BEGIN
                    SET @CreditsEarned = @FinAidCred;
                END;

--Set this before adding the next course credit
            SET @CumCreditsPrevious = @AddCreditsCum;
            SET @AddClkHrsPrevious = @AddClkHrsNew;

--We are accumulating the credits. Add the next course credit
            SET @AddCreditsCum = @AddCreditsCum + @CreditsEarned;
            SET @AddClkHrsNew = @AddClkHrsNew + @HoursEarned;



--Clear the comments
            SET @Comments = '';


--Now we need to branch off the logic depending on which Academic Calendar the school is set. (@AcaCalendar)

--*****NEW CODE 11_17_10
            IF @AcaCalendar = 'Non-Term' 
	--We're only going through this once, no matter how many records are in the Master Cursor
                IF @OutsideLoopCount = 1
		--BEGIN #1
                    BEGIN
			--BEGIN #2
                        BEGIN
				--Use this 'IF' when the actual Program Version Credits is less than the Academic Year Length Credits
				--Ex: 760 < 900
                            IF @TotalProgCred < @AcaYrLen 
					--BEGIN #3
                                BEGIN
						--Initialize the variables
                                    SET @LoopCount = 0;
                                    SET @PeriodStartDate = @StuEnrollStartDate;	
						
						--Loop until we equal the payment periods per academic year
                                    WHILE @LoopCount < @PayPerPerAcYr
							--BEGIN #4
                                        BEGIN
                                            SET @LoopCount = @LoopCount + 1;
                                            SET @PeriodNumber = @LoopCount;
								
                                            IF @LoopCount <> 1
                                                BEGIN
										--This is our second time around here
                                                    SET @PeriodStartDate = (
                                                                             SELECT DATEADD(DAY,1,@PreviousEndDate)
                                                                           );
                                                END;
								--Here we set the PP end date	
                                            SET @PeriodEndDate = (
                                                                   SELECT   DATEADD(DAY,( @PrgWeeksPerPeriod * 7 ),@PeriodStartDate)
                                                                 );	
								
                                            IF @LoopCount = 1
                                                BEGIN
                                                    SET @Range = '1-' + CONVERT(VARCHAR(10),CONVERT(INT,@ProgCredPerPP));
                                                END;
                                            ELSE
                                                BEGIN
                                                    SET @Range = CONVERT(VARCHAR(10),( CONVERT(INT,@ProgCredPerPP) ) + 1) + '-'
                                                        + CONVERT(VARCHAR(10),CONVERT(INT,@TotalProgCred));
                                                END;
									
                                            SET @PreviousEndDate = @PeriodEndDate;
									
								--Reset the loop count
                                            SET @LOA_LoopCount = 0;
								
								--We're doing LOAs here. There may be more than one LOA per student, so we'll loop through LOAs with this segment.	
                                            SET @LOA_DayCum = 0;
                                            WHILE ( @LOA_RowCount <> 0 )
                                                AND ( @LOA_LoopCount <> @LOA_RowCount )
                                                BEGIN
                                                    SET @LOA_LoopCount = @LOA_LoopCount + 1; 
                                                    SET @LOA_StartDate = (
                                                                           SELECT   LOA_StartDate
                                                                           FROM     #LOA
                                                                           WHERE    LOA_Row = @LOA_LoopCount
                                                                         );
                                                    SET @LOA_Days = (
                                                                      SELECT    LOA_Days
                                                                      FROM      #LOA
                                                                      WHERE     LOA_Row = @LOA_LoopCount
                                                                    );
								
									--If Student was on LOA, add that to the end of the PP where the LOA exists. 
                                                    IF @LOA_StartDate BETWEEN @PeriodStartDate AND (
                                                                                                     SELECT DATEADD(DAY,( 7 * @WeeksPerPP ),@PeriodStartDate)
                                                                                                   )
                                                        BEGIN
                                                            IF @LOA_Days < 180
                                                                BEGIN
                                                                    IF @LOA_LoopCount = 1
                                                                        BEGIN
                                                                            SET @PreviousEndDate = (
                                                                                                     SELECT DATEADD(DAY,( 7 * @WeeksPerPP ),@PeriodStartDate)
                                                                                                   );
                                                                            SET @PreviousEndDate = (
                                                                                                     SELECT DATEADD(DAY,@LOA_Days,@PreviousEndDate)
                                                                                                   );
                                                                            SET @Is_LOA = 1;
                                                                        END;
                                                                    ELSE
                                                                        BEGIN
                                                                            SET @PreviousEndDate = (
                                                                                                     SELECT DATEADD(DAY,@LOA_Days,@PreviousEndDate)
                                                                                                   );
                                                                            SET @Is_LOA = 1;
                                                                        END;
                                                                    SET @Comments = 'LOA of ' + CONVERT(VARCHAR(5),@LOA_Days) + ' day(s).';
                                                                END;
                                                            ELSE
                                                                BEGIN
                                                                    SET @Comments = 'Students LOA is longer than 180 Days';
                                                                    SET @PreviousEndDate = '1900-01-31';
                                                                    SET @Is_LOA = 1;
                                                                END;
                                                        END;
                                                    ELSE
                                                        BEGIN
                                                            SET @PreviousEndDate = (
                                                                                     SELECT DATEADD(DAY,( 7 * @WeeksPerPP ),@PeriodStartDate)
                                                                                   );
                                                            SET @Is_LOA = 0;
                                                        END;	
                                                END;	
								--No LOA
                                            IF @Is_LOA = 0
                                                BEGIN
                                                    SET @PreviousEndDate = (
                                                                             SELECT DATEADD(DAY,( 7 * @WeeksPerPP ),@PeriodStartDate)
                                                                           );
                                                END;
								
								
								--This is Aca Year INT info
                                            SET @AcademicYear_INT_Count = @AcademicYear_INT_Count + 1;
								
                                            IF @AcademicYear_INT_Count <= @PayPerPerAcYr
                                                BEGIN
                                                    SET @AcademicYear_INT = @AcademicYear_INT + 1;
                                                    IF @AcademicYear_INT_Count = @PayPerPerAcYr
                                                        BEGIN
                                                            SET @AcademicYear_INT = @AcademicYear_INT - 1;
                                                            SET @AcademicYear_INT_Count = 0;
                                                        END;
                                                END;
								
								
                                            INSERT  INTO @PayPeriods
                                                    (
                                                     PaymentPeriod
                                                    ,PaymentPeriodRange
                                                    ,StudentID
                                                    ,StudentEnrollmentId
                                                    ,AcaYrLen
                                                    ,ProgramID
                                                    ,ProgramVersionID
                                                    ,AcaCalendar
                                                    ,ProgLength
                                                    ,Hours
                                                    ,TotalProgCred
                                                    ,PayPeriodsPerAcYr
                                                    ,PeriodStartDate
                                                    ,PeriodEndDate
                                                    ,Comments
                                                    ,AcaYear_INT
                                                    )
                                            VALUES  (
                                                     @PeriodNumber
                                                    ,@Range
                                                    ,@StudentID
                                                    ,@StudentEnrollmentId
                                                    ,@AcaYrLen
                                                    ,@ProgramID
                                                    ,@ProgramVersionID
                                                    ,@AcaCalendar
                                                    ,@PrgVerHours
                                                    ,@AddClkHrsPrevious
                                                    ,@TotalProgCred
                                                    ,@PayPerPerAcYr
                                                    ,@PeriodStartDate
                                                    ,@PreviousEndDate
                                                    ,@Comments
                                                    ,@AcademicYear_INT
                                                    );
								
                                            SET @Comments = '';
							--END #4							
                                        END;
					--END #3		
                                END; 
				--This will be for instances where the Program Credits are = or > than the 
				--Academic Year Length Credits
				--Ex: Program Credits 44.50 and Academic Year Length Credits 36	
                            IF @TotalProgCred >= @AcaYrLen 
					--BEGIN #5
                                BEGIN
					 	--Initialize the variables
                                    SET @LoopCount = 0;
                                    SET @PeriodStartDate = @StuEnrollStartDate;	
                                    SET @CumHours = 0;
						
                                    WHILE @CumHours <= @TotalProgCred
							--BEGIN #6
                                        BEGIN
                                            SET @LoopCount = @LoopCount + 1;
                                            SET @PeriodNumber = @LoopCount;
							
                                            IF @LoopCount <> 1
                                                BEGIN
										--This is our second time around here
                                                    SET @PeriodStartDate = (
                                                                             SELECT DATEADD(DAY,1,@PreviousEndDate)
                                                                           );
                                                END;
								--Here we set the PP end date	
                                            SET @PeriodEndDate = (
                                                                   SELECT   DATEADD(DAY,( @PrgWeeksPerPeriod * 7 ),@PeriodStartDate)
                                                                 );	
								
                                            IF @LoopCount = 1
                                                BEGIN
                                                    SET @Range = '1-' + CONVERT(VARCHAR(10),CONVERT(INT,@CreditsPerPeriod));
                                                END;
                                            ELSE
                                                BEGIN
                                                    IF ( @CumHours + @CreditsPerPeriod ) > @TotalProgCred
                                                        BEGIN
                                                            SET @Range = CONVERT(VARCHAR(10),( CONVERT(INT,@CumHours) ) + 1) + '-'
                                                                + CONVERT(VARCHAR(10),@TotalProgCred);
                                                        END;
                                                    ELSE
                                                        BEGIN
                                                            SET @Range = CONVERT(VARCHAR(10),( CONVERT(INT,@CumHours) ) + 1) + '-'
                                                                + CONVERT(VARCHAR(10),CONVERT(INT,@CumHours) + CONVERT(INT,@CreditsPerPeriod));
                                                        END;
										
                                                END;
									
                                            SET @PreviousEndDate = @PeriodEndDate;
								
                                            SET @CumHours = @CumHours + @CreditsPerPeriod;
								
								
								--Reset the loop count
                                            SET @LOA_LoopCount = 0;
								
								--We're doing LOAs here. There may be more than one LOA per student, so we'll loop through LOAs with this segment.	
                                            SET @LOA_DayCum = 0;
                                            WHILE ( @LOA_RowCount <> 0 )
                                                AND ( @LOA_LoopCount <> @LOA_RowCount )
                                                BEGIN
                                                    SET @LOA_LoopCount = @LOA_LoopCount + 1; 
                                                    SET @LOA_StartDate = (
                                                                           SELECT   LOA_StartDate
                                                                           FROM     #LOA
                                                                           WHERE    LOA_Row = @LOA_LoopCount
                                                                         );
                                                    SET @LOA_Days = (
                                                                      SELECT    LOA_Days
                                                                      FROM      #LOA
                                                                      WHERE     LOA_Row = @LOA_LoopCount
                                                                    );
								
									--If Student was on LOA, add that to the end of the PP where the LOA exists. 
                                                    IF @LOA_StartDate BETWEEN @PeriodStartDate AND (
                                                                                                     SELECT DATEADD(DAY,( 7 * @WeeksPerPP ),@PeriodStartDate)
                                                                                                   )
                                                        BEGIN
                                                            IF @LOA_Days < 180
                                                                BEGIN
                                                                    IF @LOA_LoopCount = 1
                                                                        BEGIN
                                                                            SET @PreviousEndDate = (
                                                                                                     SELECT DATEADD(DAY,( 7 * @WeeksPerPP ),@PeriodStartDate)
                                                                                                   );
                                                                            SET @PreviousEndDate = (
                                                                                                     SELECT DATEADD(DAY,@LOA_Days,@PreviousEndDate)
                                                                                                   );
                                                                            SET @Is_LOA = 1;
                                                                        END;
                                                                    ELSE
                                                                        BEGIN
                                                                            SET @PreviousEndDate = (
                                                                                                     SELECT DATEADD(DAY,@LOA_Days,@PreviousEndDate)
                                                                                                   );
                                                                            SET @Is_LOA = 1;
                                                                        END;
                                                                    SET @Comments = 'LOA of ' + CONVERT(VARCHAR(5),@LOA_Days) + ' day(s).';
                                                                END;
                                                            ELSE
                                                                BEGIN
                                                                    SET @Comments = 'Students LOA is longer than 180 Days';
                                                                    SET @PreviousEndDate = '1900-01-31';
                                                                    SET @Is_LOA = 1;
                                                                END;
                                                        END;
                                                    ELSE
                                                        BEGIN
                                                            SET @PreviousEndDate = (
                                                                                     SELECT DATEADD(DAY,( 7 * @WeeksPerPP ),@PeriodStartDate)
                                                                                   );
                                                            SET @Is_LOA = 0;
                                                        END;	
                                                END;	
								--No LOA
                                            IF @Is_LOA = 0
                                                BEGIN
                                                    SET @PreviousEndDate = (
                                                                             SELECT DATEADD(DAY,( 7 * @WeeksPerPP ),@PeriodStartDate)
                                                                           );
                                                END;
								
								--This is Aca Year INT info
                                            SET @AcademicYear_INT_Count = @AcademicYear_INT_Count + 1;
								
                                            IF @AcademicYear_INT_Count <= @PayPerPerAcYr
                                                BEGIN
                                                    SET @AcademicYear_INT = @AcademicYear_INT + 1;
                                                    IF @AcademicYear_INT_Count = @PayPerPerAcYr
                                                        BEGIN
                                                            SET @AcademicYear_INT = @AcademicYear_INT - 1;
                                                            SET @AcademicYear_INT_Count = 0;
                                                        END;
                                                END;
								
								
                                            INSERT  INTO @PayPeriods
                                                    (
                                                     PaymentPeriod
                                                    ,PaymentPeriodRange
                                                    ,StudentID
                                                    ,StudentEnrollmentId
                                                    ,AcaYrLen
                                                    ,ProgramID
                                                    ,ProgramVersionID
                                                    ,AcaCalendar
                                                    ,ProgLength
                                                    ,Hours
                                                    ,TotalProgCred
                                                    ,PayPeriodsPerAcYr
                                                    ,PeriodStartDate
                                                    ,PeriodEndDate
                                                    ,Comments
                                                    ,AcaYear_INT
                                                    )
                                            VALUES  (
                                                     @PeriodNumber
                                                    ,@Range
                                                    ,@StudentID
                                                    ,@StudentEnrollmentId
                                                    ,@AcaYrLen
                                                    ,@ProgramID
                                                    ,@ProgramVersionID
                                                    ,@AcaCalendar
                                                    ,@PrgVerHours
                                                    ,@AddClkHrsPrevious
                                                    ,@TotalProgCred
                                                    ,@PayPerPerAcYr
                                                    ,@PeriodStartDate
                                                    ,@PreviousEndDate
                                                    ,@Comments
                                                    ,@AcademicYear_INT
                                                    );
								
                                            SET @Comments = '';
							--END #6
                                        END;
					--END #5
                                END;


					
			--END #2		
                        END;	
		--END #1	
                    END;		
--*****

--***OLD CODE
--IF @AcaCalendar = 'Non-Term'  --Non-Term Credit Schools
--BEGIN
--	--Cycle to the next period; reset the counter
--	IF @AddCreditsCum > @CreditsPerPeriod
--		BEGIN					
--			IF @PeriodNumber = 1
--				BEGIN
--					SET @PreviousEndDate = @1stPPEndDateMinimum
--				END	
--			ELSE
--				BEGIN
--					--Begin the next payment period the following day
--					SET @PeriodStartDate = (SELECT DATEADD(DAY, 1,@PreviousEndDate))	
--				END

--			--Reset the loop count
--			SET @LOA_LoopCount = 0
			
--			--We're doing LOAs here. There may be more than one LOA per student, so we'll loop through LOAs with this segment.	
--			SET @LOA_DayCum = 0
--			WHILE (@LOA_RowCount <> 0) AND (@LOA_LoopCount < @LOA_RowCount)
--			BEGIN
--				SET @LOA_LoopCount = @LOA_LoopCount + 1 
--				SET @LOA_StartDate = (SELECT LOA_StartDate FROM #LOA WHERE LOA_Row = @LOA_LoopCount)
--				SET @LOA_Days = (SELECT LOA_Days FROM #LOA WHERE LOA_Row = @LOA_LoopCount)
--				SET @LOA_DayCum = @LOA_DayCum + @LOA_Days

--				--If Student was on LOA, add that to the end of the PP where the LOA exists. 
--				IF @LOA_StartDate BETWEEN @PeriodStartDate AND (SELECT DATEADD(DAY, (7*@WeeksPerPP),@PeriodStartDate))
--					BEGIN
--						IF @LOA_Days < 180
--								BEGIN
--									IF @LOA_LoopCount = 1
--										BEGIN
--											SET @PreviousEndDate = (SELECT DATEADD(DAY, (7*@WeeksPerPP),@PeriodStartDate))
--											SET @PreviousEndDate = (SELECT DATEADD(DAY, @LOA_Days,@PreviousEndDate))
--											SET @Is_LOA = 1
--										END
--									ELSE
--										BEGIN
--											SET @PreviousEndDate = (SELECT DATEADD(DAY, @LOA_Days,@PreviousEndDate))
--											SET @Is_LOA = 1
--										END
--									SET @Comments = 'LOA of ' + CONVERT(VARCHAR(5), @LOA_DayCum) + ' day(s).'
--								END
--							ELSE
--								BEGIN
--									SET @Comments = 'Students LOA is longer than 180 Days'
--									SET @PreviousEndDate = '1900-01-31'
--									SET @Is_LOA = 1
--								END
--					END
--				ELSE
--					BEGIN
--						SET @PreviousEndDate = (SELECT DATEADD(DAY, (7*@WeeksPerPP),@PeriodStartDate))
--						SET @Is_LOA = 0
--					END	
--			END
--			--No LOA
--			IF 	@Is_LOA = 0
--			BEGIN
--				SET @PreviousEndDate = (SELECT DATEADD(DAY, (7*@WeeksPerPP),@PeriodStartDate))
--			END

--			INSERT INTO @PayPeriods(PaymentPeriod, StudentID, StudentEnrollmentId, AcaYrLen, ProgramID, ProgramVersionID, AcaCalendar,
--			ProgLength, Hours, TotalProgCred, PayPeriodsPerAcYr, PeriodStartDate, PeriodEndDate, Comments) VALUES
--			(@PeriodNumber, @StudentID, @StudentEnrollmentId, @AcaYrLen, @ProgramID, @ProgramVersionID, @AcaCalendar, 
--			 @TotalProgCred, @CumCreditsPrevious, NULL, @PayPerPerAcYr, @PeriodStartDate, @PreviousEndDate, @Comments)
			
--			SET @Is_LOA = 0

--			SET @PeriodNumber = @PeriodNumber + 1
--			SET @Counter = 0
--			--Since we're already at our limit in this IF statement (in terms of credits) for the payment period, we will put the current 
--			--credit for this class section into the next payment period by placing it into the variable. Then, it will
--			--be rolled into the next payment period. Cannot exceed the allowable credits per payment period. (as defined in: @CreditsPerPeriod)
--			SET @AddCreditsCum = @CreditsEarned
--		END
--END
--****

--Clear the comments
            SET @Comments = '';


--******NEW CODE 11_16_10
            IF @AcaCalendar = 'Clock Hour'
	--We're only going through this once, no matter how many records are in the Master Cursor
                IF @OutsideLoopCount = 1
		--BEGIN #1
                    BEGIN
			--BEGIN #2
                        BEGIN
				--Use this 'IF' when the actual Program Version Hours is less than the Academic Year Length
				--Ex: 760 < 900
                            IF @PrgVerHours < @AcaYrLen 
					--BEGIN #3
                                BEGIN
						--Initialize the variables
                                    SET @LoopCount = 0;
                                    SET @PeriodStartDate = @StuEnrollStartDate;	
						
						--Loop until we equal the payment periods per academic year
                                    WHILE @LoopCount < @PayPerPerAcYr
							--BEGIN #4
                                        BEGIN
                                            SET @LoopCount = @LoopCount + 1;
                                            SET @PeriodNumber = @LoopCount;
								
                                            IF @LoopCount <> 1
                                                BEGIN
										--This is our second time around here
                                                    SET @PeriodStartDate = (
                                                                             SELECT DATEADD(DAY,1,@PreviousEndDate)
                                                                           );
                                                END;
								--Here we set the PP end date	
                                            SET @PeriodEndDate = (
                                                                   SELECT   DATEADD(DAY,( @PrgWeeksPerPeriod * 7 ),@PeriodStartDate)
                                                                 );	
								
                                            IF @LoopCount = 1
                                                BEGIN
                                                    SET @Range = '1-' + CONVERT(VARCHAR(10),CONVERT(INT,@ClkHrsPerPeriod));
                                                END;
                                            ELSE
                                                BEGIN
                                                    SET @Range = CONVERT(VARCHAR(10),( CONVERT(INT,@ClkHrsPerPeriod) ) + 1) + '-'
                                                        + CONVERT(VARCHAR(10),CONVERT(INT,@PrgVerHours));
                                                END;
									
                                            SET @PreviousEndDate = @PeriodEndDate;
									
								--Reset the loop count
                                            SET @LOA_LoopCount = 0;
								
								--We're doing LOAs here. There may be more than one LOA per student, so we'll loop through LOAs with this segment.	
                                            SET @LOA_DayCum = 0;
                                            WHILE ( @LOA_RowCount <> 0 )
                                                AND ( @LOA_LoopCount <> @LOA_RowCount )
                                                BEGIN
                                                    SET @LOA_LoopCount = @LOA_LoopCount + 1; 
                                                    SET @LOA_StartDate = (
                                                                           SELECT   LOA_StartDate
                                                                           FROM     #LOA
                                                                           WHERE    LOA_Row = @LOA_LoopCount
                                                                         );
                                                    SET @LOA_Days = (
                                                                      SELECT    LOA_Days
                                                                      FROM      #LOA
                                                                      WHERE     LOA_Row = @LOA_LoopCount
                                                                    );
								
									--If Student was on LOA, add that to the end of the PP where the LOA exists. 
                                                    IF @LOA_StartDate BETWEEN @PeriodStartDate AND (
                                                                                                     SELECT DATEADD(DAY,( 7 * @WeeksPerPP ),@PeriodStartDate)
                                                                                                   )
                                                        BEGIN
                                                            IF @LOA_Days < 180
                                                                BEGIN
                                                                    IF @LOA_LoopCount = 1
                                                                        BEGIN
                                                                            SET @PreviousEndDate = (
                                                                                                     SELECT DATEADD(DAY,( 7 * @WeeksPerPP ),@PeriodStartDate)
                                                                                                   );
                                                                            SET @PreviousEndDate = (
                                                                                                     SELECT DATEADD(DAY,@LOA_Days,@PreviousEndDate)
                                                                                                   );
                                                                            SET @Is_LOA = 1;
                                                                        END;
                                                                    ELSE
                                                                        BEGIN
                                                                            SET @PreviousEndDate = (
                                                                                                     SELECT DATEADD(DAY,@LOA_Days,@PreviousEndDate)
                                                                                                   );
                                                                            SET @Is_LOA = 1;
                                                                        END;
                                                                    SET @Comments = 'LOA of ' + CONVERT(VARCHAR(5),@LOA_Days) + ' day(s).';
                                                                END;
                                                            ELSE
                                                                BEGIN
                                                                    SET @Comments = 'Students LOA is longer than 180 Days';
                                                                    SET @PreviousEndDate = '1900-01-31';
                                                                    SET @Is_LOA = 1;
                                                                END;
                                                        END;
                                                    ELSE
                                                        BEGIN
                                                            SET @PreviousEndDate = (
                                                                                     SELECT DATEADD(DAY,( 7 * @WeeksPerPP ),@PeriodStartDate)
                                                                                   );
                                                            SET @Is_LOA = 0;
                                                        END;	
                                                END;	
								--No LOA
                                            IF @Is_LOA = 0
                                                BEGIN
                                                    SET @PreviousEndDate = (
                                                                             SELECT DATEADD(DAY,( 7 * @WeeksPerPP ),@PeriodStartDate)
                                                                           );
                                                END;
								
								
								--This is Aca Year INT info
                                            SET @AcademicYear_INT_Count = @AcademicYear_INT_Count + 1;
								
                                            IF @AcademicYear_INT_Count <= @PayPerPerAcYr
                                                BEGIN
                                                    SET @AcademicYear_INT = @AcademicYear_INT + 1;
                                                    IF @AcademicYear_INT_Count = @PayPerPerAcYr
                                                        BEGIN
                                                            SET @AcademicYear_INT = @AcademicYear_INT - 1;
                                                            SET @AcademicYear_INT_Count = 0;
                                                        END;
                                                END;
								
                                            INSERT  INTO @PayPeriods
                                                    (
                                                     PaymentPeriod
                                                    ,PaymentPeriodRange
                                                    ,StudentID
                                                    ,StudentEnrollmentId
                                                    ,AcaYrLen
                                                    ,ProgramID
                                                    ,ProgramVersionID
                                                    ,AcaCalendar
                                                    ,ProgLength
                                                    ,Hours
                                                    ,TotalProgCred
                                                    ,PayPeriodsPerAcYr
                                                    ,PeriodStartDate
                                                    ,PeriodEndDate
                                                    ,Comments
                                                    ,AcaYear_INT
                                                    )
                                            VALUES  (
                                                     @PeriodNumber
                                                    ,@Range
                                                    ,@StudentID
                                                    ,@StudentEnrollmentId
                                                    ,@AcaYrLen
                                                    ,@ProgramID
                                                    ,@ProgramVersionID
                                                    ,@AcaCalendar
                                                    ,@PrgVerHours
                                                    ,@AddClkHrsPrevious
                                                    ,@TotalProgCred
                                                    ,@PayPerPerAcYr
                                                    ,@PeriodStartDate
                                                    ,@PreviousEndDate
                                                    ,@Comments
                                                    ,@AcademicYear_INT
                                                    );
								
                                            SET @Comments = '';
							--END #4							
                                        END;
					--END #3		
                                END; 
				--This will be for instances where the Program Hours is = or > than the 
				--Academic Year Length
				--Ex: Program Hours 1200 and Academic Year Length 900	
                            IF @PrgVerHours >= @AcaYrLen 
					--BEGIN #5
                                BEGIN
					 	--Initialize the variables
                                    SET @LoopCount = 0;
                                    SET @PeriodStartDate = @StuEnrollStartDate;	
                                    SET @CumHours = 0;
						
                                    WHILE @CumHours <= @PrgVerHours
							--BEGIN #6
                                        BEGIN
                                            SET @LoopCount = @LoopCount + 1;
                                            SET @PeriodNumber = @LoopCount;
							
                                            IF @LoopCount <> 1
                                                BEGIN
										--This is our second time around here
                                                    SET @PeriodStartDate = (
                                                                             SELECT DATEADD(DAY,1,@PreviousEndDate)
                                                                           );
                                                END;
								--Here we set the PP end date	
                                            SET @PeriodEndDate = (
                                                                   SELECT   DATEADD(DAY,( @PrgWeeksPerPeriod * 7 ),@PeriodStartDate)
                                                                 );	
								
                                            IF @LoopCount = 1
                                                BEGIN
                                                    SET @Range = '1-' + CONVERT(VARCHAR(10),CONVERT(INT,@CreditsPerPeriod));
                                                END;
                                            ELSE
                                                BEGIN
                                                    IF ( @CumHours + @CreditsPerPeriod ) > @PrgVerHours
                                                        BEGIN
                                                            SET @Range = CONVERT(VARCHAR(10),( CONVERT(INT,@CumHours) ) + 1) + '-'
                                                                + CONVERT(VARCHAR(10),CONVERT(INT,@PrgVerHours));
                                                        END;
                                                    ELSE
                                                        BEGIN
                                                            SET @Range = CONVERT(VARCHAR(10),( CONVERT(INT,@CumHours) ) + 1) + '-'
                                                                + CONVERT(VARCHAR(10),CONVERT(INT,@CumHours) + CONVERT(INT,@CreditsPerPeriod));
                                                        END;
										
                                                END;
									
                                            SET @PreviousEndDate = @PeriodEndDate;
								
                                            SET @CumHours = @CumHours + @CreditsPerPeriod;
								
								
								--Reset the loop count
                                            SET @LOA_LoopCount = 0;
								
								--We're doing LOAs here. There may be more than one LOA per student, so we'll loop through LOAs with this segment.	
                                            SET @LOA_DayCum = 0;
                                            WHILE ( @LOA_RowCount <> 0 )
                                                AND ( @LOA_LoopCount <> @LOA_RowCount )
                                                BEGIN
                                                    SET @LOA_LoopCount = @LOA_LoopCount + 1; 
                                                    SET @LOA_StartDate = (
                                                                           SELECT   LOA_StartDate
                                                                           FROM     #LOA
                                                                           WHERE    LOA_Row = @LOA_LoopCount
                                                                         );
                                                    SET @LOA_Days = (
                                                                      SELECT    LOA_Days
                                                                      FROM      #LOA
                                                                      WHERE     LOA_Row = @LOA_LoopCount
                                                                    );
								
									--If Student was on LOA, add that to the end of the PP where the LOA exists. 
                                                    IF @LOA_StartDate BETWEEN @PeriodStartDate AND (
                                                                                                     SELECT DATEADD(DAY,( 7 * @WeeksPerPP ),@PeriodStartDate)
                                                                                                   )
                                                        BEGIN
                                                            IF @LOA_Days < 180
                                                                BEGIN
                                                                    IF @LOA_LoopCount = 1
                                                                        BEGIN
                                                                            SET @PreviousEndDate = (
                                                                                                     SELECT DATEADD(DAY,( 7 * @WeeksPerPP ),@PeriodStartDate)
                                                                                                   );
                                                                            SET @PreviousEndDate = (
                                                                                                     SELECT DATEADD(DAY,@LOA_Days,@PreviousEndDate)
                                                                                                   );
                                                                            SET @Is_LOA = 1;
                                                                        END;
                                                                    ELSE
                                                                        BEGIN
                                                                            SET @PreviousEndDate = (
                                                                                                     SELECT DATEADD(DAY,@LOA_Days,@PreviousEndDate)
                                                                                                   );
                                                                            SET @Is_LOA = 1;
                                                                        END;
                                                                    SET @Comments = 'LOA of ' + CONVERT(VARCHAR(5),@LOA_Days) + ' day(s).';
                                                                END;
                                                            ELSE
                                                                BEGIN
                                                                    SET @Comments = 'Students LOA is longer than 180 Days';
                                                                    SET @PreviousEndDate = '1900-01-31';
                                                                    SET @Is_LOA = 1;
                                                                END;
                                                        END;
                                                    ELSE
                                                        BEGIN
                                                            SET @PreviousEndDate = (
                                                                                     SELECT DATEADD(DAY,( 7 * @WeeksPerPP ),@PeriodStartDate)
                                                                                   );
                                                            SET @Is_LOA = 0;
                                                        END;	
                                                END;	
								--No LOA
                                            IF @Is_LOA = 0
                                                BEGIN
                                                    SET @PreviousEndDate = (
                                                                             SELECT DATEADD(DAY,( 7 * @WeeksPerPP ),@PeriodStartDate)
                                                                           );
                                                END;
								
								
								--This is Aca Year INT info
                                            SET @AcademicYear_INT_Count = @AcademicYear_INT_Count + 1;
								
                                            IF @AcademicYear_INT_Count <= @PayPerPerAcYr
                                                BEGIN
                                                    SET @AcademicYear_INT = @AcademicYear_INT + 1;
                                                    IF @AcademicYear_INT_Count = @PayPerPerAcYr
                                                        BEGIN
                                                            SET @AcademicYear_INT = @AcademicYear_INT - 1;
                                                            SET @AcademicYear_INT_Count = 0;
                                                        END;
                                                END;
								
								
                                            INSERT  INTO @PayPeriods
                                                    (
                                                     PaymentPeriod
                                                    ,PaymentPeriodRange
                                                    ,StudentID
                                                    ,StudentEnrollmentId
                                                    ,AcaYrLen
                                                    ,ProgramID
                                                    ,ProgramVersionID
                                                    ,AcaCalendar
                                                    ,ProgLength
                                                    ,Hours
                                                    ,TotalProgCred
                                                    ,PayPeriodsPerAcYr
                                                    ,PeriodStartDate
                                                    ,PeriodEndDate
                                                    ,Comments
                                                    ,AcaYear_INT
                                                    )
                                            VALUES  (
                                                     @PeriodNumber
                                                    ,@Range
                                                    ,@StudentID
                                                    ,@StudentEnrollmentId
                                                    ,@AcaYrLen
                                                    ,@ProgramID
                                                    ,@ProgramVersionID
                                                    ,@AcaCalendar
                                                    ,@PrgVerHours
                                                    ,@AddClkHrsPrevious
                                                    ,@TotalProgCred
                                                    ,@PayPerPerAcYr
                                                    ,@PeriodStartDate
                                                    ,@PreviousEndDate
                                                    ,@Comments
                                                    ,@AcademicYear_INT
                                                    );
								
                                            SET @Comments = '';
							--END #6
                                        END;
					--END #5
                                END;
			--END #2
                        END;
		--END #1	
                    END;	

--***OLD CODE
--IF @AcaCalendar = 'Clock Hour'
--BEGIN
--	--Cycle to the next period; reset the counter
--	IF @AddClkHrsNew >= @ClkHrsPerPeriod
--		BEGIN
--			IF @PeriodNumber = 1
--				BEGIN
--					SET @PreviousEndDate = @1stPPEndDateMinimum
--				END	
--			ELSE
--				BEGIN
--					--Begin the next payment period the following day
--					SET @PeriodStartDate = (SELECT DATEADD(DAY, 1,@PreviousEndDate))	
--				END
				
--			--If the cumed clock hrs = the clock hrs per pay period, we'll use the cumed clock hrs as the Hours (earned).
--			IF @AddClkHrsNew = @ClkHrsPerPeriod
--				BEGIN
--					SET @AddClkHrsPrevious = @AddClkHrsNew
--					--Clear the cum
--					SET @AddClkHrsNew = 0
--				END

--			--Reset the loop count
--			SET @LOA_LoopCount = 0
			
--			--We're doing LOAs here. There may be more than one LOA per student, so we'll loop through LOAs with this segment.	
--			SET @LOA_DayCum = 0
--			WHILE (@LOA_RowCount <> 0) AND (@LOA_LoopCount <> @LOA_RowCount)
--			BEGIN
--			SET @LOA_LoopCount = @LOA_LoopCount + 1 
--			SET @LOA_StartDate = (SELECT LOA_StartDate FROM #LOA WHERE LOA_Row = @LOA_LoopCount)
--			SET @LOA_Days = (SELECT LOA_Days FROM #LOA WHERE LOA_Row = @LOA_LoopCount)
			
--				--If Student was on LOA, add that to the end of the PP where the LOA exists. 
--				IF @LOA_StartDate BETWEEN @PeriodStartDate AND (SELECT DATEADD(DAY, (7*@WeeksPerPP),@PeriodStartDate))
--					BEGIN
--						IF @LOA_Days < 180
--								BEGIN
--									IF @LOA_LoopCount = 1
--										BEGIN
--											SET @PreviousEndDate = (SELECT DATEADD(DAY, (7*@WeeksPerPP),@PeriodStartDate))
--											SET @PreviousEndDate = (SELECT DATEADD(DAY, @LOA_Days,@PreviousEndDate))
--											SET @Is_LOA = 1
--										END
--									ELSE
--										BEGIN
--											SET @PreviousEndDate = (SELECT DATEADD(DAY, @LOA_Days,@PreviousEndDate))
--											SET @Is_LOA = 1
--										END
--									SET @Comments = 'LOA of ' + CONVERT(VARCHAR(5), @LOA_Days) + ' day(s).'
--								END
--							ELSE
--								BEGIN
--									SET @Comments = 'Students LOA is longer than 180 Days'
--									SET @PreviousEndDate = '1900-01-31'
--									SET @Is_LOA = 1
--								END
--					END
--				ELSE
--					BEGIN
--						SET @PreviousEndDate = (SELECT DATEADD(DAY, (7*@WeeksPerPP),@PeriodStartDate))
--						SET @Is_LOA = 0
--					END	
--			END	
--			--No LOA
--			IF 	@Is_LOA = 0
--			BEGIN
--				SET @PreviousEndDate = (SELECT DATEADD(DAY, (7*@WeeksPerPP),@PeriodStartDate))
--			END

--			INSERT INTO @PayPeriods(PaymentPeriod, StudentID, StudentEnrollmentId, AcaYrLen, ProgramID, ProgramVersionID, AcaCalendar,
--			ProgLength, Hours, TotalProgCred, PayPeriodsPerAcYr, PeriodStartDate, PeriodEndDate, Comments) VALUES
--			(@PeriodNumber, @StudentID, @StudentEnrollmentId, @AcaYrLen, @ProgramID, @ProgramVersionID, @AcaCalendar, 
--			 @PrgVerHours, @AddClkHrsPrevious, @TotalProgCred, @PayPerPerAcYr, @PeriodStartDate, @PreviousEndDate, @Comments)
			
--			SET @Is_LOA = 0

--			SET @PeriodNumber = @PeriodNumber + 1
--			SET @Counter = 0
--			--Since we're already at our limit in this IF statement (in terms of credits) for the payment period, we will put the current 
--			--credit for this class section into the next payment period by placing it into the variable. Then, it will
--			--be rolled into the next payment period. Cannot exceed the allowable credits per payment period. (as defined in: @CreditsPerPeriod)
--			IF @AddClkHrsNew <> 0
--				--We need to keep the cum cleared if cumed clock hrs = the clock hrs per pay period.
--				--If not, then we need to preserve the hrs for previous. We use that for the next payment period.
--				BEGIN
--					SET @AddClkHrsNew = @ReqHours
--				END
			
--		END

--END
--***

--Clear the comments
            SET @Comments = '';

--***OLD CODE
--IF @AcaCalendar = 'Non-Term' --Non-Term Credit Schools
--BEGIN
--	--Get the remaining credits that don't quite fill a full payment period
--	IF @TotalRowCount = @TableRowCount AND @AddCreditsCum < @CreditsPerPeriod
--		BEGIN
--			IF @PeriodNumber > 1
--				BEGIN
--					--Begin the next payment period the following day
--					SET @PeriodStartDate = (SELECT DATEADD(DAY, 1,@PreviousEndDate))
--					SET @PreviousEndDate = (SELECT DATEADD(DAY, (7*@WeeksPerPP),@PeriodStartDate))
--				END

--			--Reset the loop count
--			SET @LOA_LoopCount = 0
				
--			--We're doing LOAs here. There may be more than one LOA per student, so we'll loop through LOAs with this segment.	
--			SET @LOA_DayCum = 0
--			WHILE @LOA_RowCount <> 0 AND @LOA_LoopCount <> @LOA_RowCount
--			BEGIN
--			SET @LOA_LoopCount = @LOA_LoopCount + 1 
--			SET @LOA_StartDate = (SELECT LOA_StartDate FROM #LOA WHERE LOA_Row = @LOA_LoopCount)
--			SET @LOA_Days = (SELECT LOA_Days FROM #LOA WHERE LOA_Row = @LOA_LoopCount)
--			SET @LOA_DayCum = @LOA_DayCum + @LOA_Days
			
--				--If Student was on LOA, add that to the end of the PP where the LOA exists. 
--				IF @LOA_StartDate BETWEEN @PeriodStartDate AND (SELECT DATEADD(DAY, (7*@WeeksPerPP),@PeriodStartDate))
--					BEGIN
--						IF @LOA_Days < 180
--								BEGIN
--									IF @LOA_LoopCount = 1
--										BEGIN
--											SET @PreviousEndDate = (SELECT DATEADD(DAY, (7*@WeeksPerPP),@PeriodStartDate))
--											SET @PreviousEndDate = (SELECT DATEADD(DAY, @LOA_Days,@PreviousEndDate))
--											SET @Is_LOA = 1
--										END
--									ELSE
--										BEGIN
--											SET @PreviousEndDate = (SELECT DATEADD(DAY, @LOA_Days,@PreviousEndDate))
--											SET @Is_LOA = 1
--										END
--									SET @Comments = 'LOA of ' + CONVERT(VARCHAR(5), @LOA_DayCum) + ' day(s).'
--								END
--							ELSE
--								BEGIN
--									SET @Comments = 'Students LOA is longer than 180 Days'
--									SET @PreviousEndDate = '1900-01-31'
--									SET @Is_LOA = 1
--								END
--					END
--				ELSE
--					BEGIN
--						SET @PreviousEndDate = (SELECT DATEADD(DAY, (7*@WeeksPerPP),@PeriodStartDate))
--						SET @Is_LOA = 0
--					END
--			END	
--			--No LOA
--			IF 	@Is_LOA = 0
--			BEGIN
--				SET @PreviousEndDate = (SELECT DATEADD(DAY, (7*@WeeksPerPP),@PeriodStartDate))
--			END		

--			INSERT INTO @PayPeriods(PaymentPeriod, StudentID, StudentEnrollmentId, AcaYrLen, ProgramID, ProgramVersionID, AcaCalendar,
--			ProgLength, Hours, TotalProgCred, PayPeriodsPerAcYr, PeriodStartDate, PeriodEndDate, Comments) VALUES
--			(@PeriodNumber, @StudentID, @StudentEnrollmentId, @AcaYrLen, @ProgramID, @ProgramVersionID, @AcaCalendar, 
--			 @TotalProgCred, @AddCreditsCum, NULL, @PayPerPerAcYr, @PeriodStartDate, @PreviousEndDate, @Comments)
			
--			SET @Is_LOA = 0
--		END
--END
--****


--Clear the comments
            SET @Comments = '';


--***OLD CODE
--IF @AcaCalendar = 'Clock Hour'
--BEGIN
--	--Get the remaining credits that don't quite fill a full payment period
--	IF @TotalRowCount = @TableRowCount AND (@AddClkHrsNew < @ClkHrsPerPeriod) AND (@AddClkHrsNew <> 0)
--		BEGIN

--			IF @PeriodNumber > 1
--				BEGIN
--					--Begin the next payment period the following day
--					SET @PeriodStartDate = (SELECT DATEADD(DAY, 1,@PreviousEndDate))
--					SET @PreviousEndDate = (SELECT DATEADD(DAY, (7*@WeeksPerPP),@PeriodStartDate))
--				END

--			--Reset the loop count
--			SET @LOA_LoopCount = 0

--			--We're doing LOAs here. There may be more than one LOA per student, so we'll loop through LOAs with this segment.	
--			SET @LOA_DayCum = 0
--			WHILE @LOA_RowCount <> 0 AND @LOA_LoopCount <> @LOA_RowCount
--			BEGIN
--			SET @LOA_LoopCount = @LOA_LoopCount + 1 
--			SET @LOA_StartDate = (SELECT LOA_StartDate FROM #LOA WHERE LOA_Row = @LOA_LoopCount)
--			SET @LOA_Days = (SELECT LOA_Days FROM #LOA WHERE LOA_Row = @LOA_LoopCount)
--				--If Student was on LOA, add that to the end of the PP where the LOA exists. 
--				IF @LOA_StartDate BETWEEN @PeriodStartDate AND (SELECT DATEADD(DAY, (7*@WeeksPerPP),@PeriodStartDate))
--					BEGIN
--						IF @LOA_Days < 180
--								BEGIN
--									IF @LOA_LoopCount = 1
--										BEGIN
--											SET @PreviousEndDate = (SELECT DATEADD(DAY, (7*@WeeksPerPP),@PeriodStartDate))
--											SET @PreviousEndDate = (SELECT DATEADD(DAY, @LOA_Days,@PreviousEndDate))
--											SET @Is_LOA = 1
--										END
--									ELSE
--										BEGIN
--											SET @PreviousEndDate = (SELECT DATEADD(DAY, @LOA_Days,@PreviousEndDate))
--											SET @Is_LOA = 1
--										END
--									SET @Comments = 'LOA of ' + CONVERT(VARCHAR(5), @LOA_DayCum) + ' day(s).'
--								END
--							ELSE
--								BEGIN
--									SET @Comments = 'Students LOA is longer than 180 Days'
--									SET @PreviousEndDate = '1900-01-31'
--									SET @Is_LOA = 1
--								END
--					END
--				ELSE
--					BEGIN
--						SET @PreviousEndDate = (SELECT DATEADD(DAY, (7*@WeeksPerPP),@PeriodStartDate))
--						SET @Is_LOA = 0
--					END	
--			END	
--			--No LOA
--			IF 	@Is_LOA = 0
--			BEGIN
--				SET @PreviousEndDate = (SELECT DATEADD(DAY, (7*@WeeksPerPP),@PeriodStartDate))
--			END		

--			INSERT INTO @PayPeriods(PaymentPeriod, StudentID, StudentEnrollmentId, AcaYrLen, ProgramID, ProgramVersionID, AcaCalendar,
--			ProgLength, Hours, TotalProgCred, PayPeriodsPerAcYr, PeriodStartDate, PeriodEndDate, Comments) VALUES
--			(@PeriodNumber, @StudentID, @StudentEnrollmentId, @AcaYrLen, @ProgramID, @ProgramVersionID, @AcaCalendar, 
--			 @PrgVerHours, @AddClkHrsNew, @TotalProgCred, @PayPerPerAcYr, @PeriodStartDate, @PreviousEndDate, @Comments)
			
--			SET @Is_LOA = 0
--		END
--END
--*****

--Clear the comments
            SET @Comments = '';


            IF (
                 @AcaCalendar = 'Quarter'
                 OR @AcaCalendar = 'Semester'
                 OR @AcaCalendar = 'Trimester'
               )
	--These are considered Term Structures
                BEGIN
                    IF @TermStartDate <> @PrevTermStartDate
                        BEGIN
		
                            SET @PrevTermStartDate = @TermStartDate;
			--If LOA exists, then we need to adjust the new start and end dates 
			--based on the previous LOA days
			--IF @Is_LOA = 1 
			--	BEGIN
			--		SET @TermStartDate = (SELECT DATEADD(DAY, @LOA_DayCum, @TermStartDate)) 
			--		SET @TermEndDate = (SELECT DATEADD(DAY, @LOA_DayCum, @TermEndDate)) 
			--	END
			
			--Reset the loop count
                            SET @LOA_LoopCount = 0;

			--We're doing LOAs here. There may be more than one LOA per student, so we'll loop through LOAs with this segment.	
                            SET @LOA_DayCum = 0;
                            SET @Comments = '';
                            WHILE @LOA_RowCount <> 0
                                AND @LOA_LoopCount <> @LOA_RowCount
                                BEGIN
                                    SET @LOA_LoopCount = @LOA_LoopCount + 1; 
                                    SET @LOA_StartDate = (
                                                           SELECT   LOA_StartDate
                                                           FROM     #LOA
                                                           WHERE    LOA_Row = @LOA_LoopCount
                                                         );
                                    SET @LOA_Days = (
                                                      SELECT    LOA_Days
                                                      FROM      #LOA
                                                      WHERE     LOA_Row = @LOA_LoopCount
                                                    );
					--If Student was on LOA, add that to the end of the PP where the LOA exists. 
                                    IF @LOA_StartDate BETWEEN @TermStartDate AND @TermEndDate
                                        BEGIN
                                            SET @Is_LOA = 1;
                                            IF @LOA_Days < 180
                                                BEGIN	
                                                    SET @LOA_DayCum = @LOA_DayCum + @LOA_Days;
                                                    SET @Comments = 'LOA of ' + CONVERT(VARCHAR(5),@LOA_DayCum) + ' day(s).';
                                                END;
                                            ELSE
                                                BEGIN
                                                    SET @Comments = 'Students LOA is longer than 180 Days';
                                                    SET @TermEndDate = '1900-01-31';
                                                END;		
                                        END;
                                    ELSE
                                        BEGIN
                                            SET @PrevTermEndDate = @TermEndDate;
                                            SET @Is_LOA = 0;
                                        END;	
                                END;	
				
				--IF @Is_LOA = 1 
				--We need to adjust the term end date here
					--BEGIN
					--	SET @TermEndDate = (SELECT DATEADD(DAY, @LOA_DayCum, @TermEndDate)) 
					--END
				
			--This is Aca Year INT info
                            SET @AcademicYear_INT_Count = @AcademicYear_INT_Count + 1;
			
                            IF @AcademicYear_INT_Count <= @PayPerPerAcYr
                                BEGIN
                                    SET @AcademicYear_INT = @AcademicYear_INT + 1;
                                    IF @AcademicYear_INT_Count = @PayPerPerAcYr
                                        BEGIN
                                            SET @AcademicYear_INT = @AcademicYear_INT - 1;
                                            SET @AcademicYear_INT_Count = 0;
                                        END;
                                END;
						
                            INSERT  INTO @PayPeriods
                                    (
                                     PaymentPeriod
                                    ,StudentID
                                    ,StudentEnrollmentId
                                    ,AcaYrLen
                                    ,ProgramID
                                    ,ProgramVersionID
                                    ,AcaCalendar
                                    ,ProgLength
                                    ,Hours
                                    ,TotalProgCred
                                    ,PayPeriodsPerAcYr
                                    ,PeriodStartDate
                                    ,PeriodEndDate
                                    ,Comments
                                    ,AcaYear_INT
                                    )
                            VALUES  (
                                     @PeriodNumber
                                    ,@StudentID
                                    ,@StudentEnrollmentId
                                    ,@AcaYrLen
                                    ,@ProgramID
                                    ,@ProgramVersionID
                                    ,@AcaCalendar
                                    ,NULL
                                    ,NULL
                                    ,@TotalProgCred
                                    ,@PayPerPerAcYr
                                    ,@TermStartDate
                                    ,@TermEndDate
                                    ,@Comments
                                    ,@AcademicYear_INT
                                    );
			 
                            SET @PeriodNumber = @PeriodNumber + 1;
                        END;	

				

                    SET @PrevTermEndDate = @TermEndDate;
                END;

--Clear the comments
            SET @Comments = '';


            IF @AcaCalendar = 'Non-Standard Term'
	--This one may be tricky. So far we know that, according to the official documentation: If the program uses
	--Nonstandatd terms, the payment period is the term.
                BEGIN
                    IF @TermStartDate <> @PrevTermStartDate
                        BEGIN
				
                            SET @PrevTermStartDate = @TermStartDate;
					--If LOA exists, then we need to adjust the new start and end dates 
					--based on the previous LOA days
					--IF @Is_LOA = 1 
					--	BEGIN
					--		SET @TermStartDate = (SELECT DATEADD(DAY, @LOA_DayCum, @TermStartDate)) 
					--		SET @TermEndDate = (SELECT DATEADD(DAY, @LOA_DayCum, @TermEndDate)) 
					--	END
					
					--Reset the loop count
                            SET @LOA_LoopCount = 0;

					--We're doing LOAs here. There may be more than one LOA per student, so we'll loop through LOAs with this segment.	
                            SET @LOA_DayCum = 0;
                            SET @Comments = '';
                            WHILE @LOA_RowCount <> 0
                                AND @LOA_LoopCount <> @LOA_RowCount
                                BEGIN
                                    SET @LOA_LoopCount = @LOA_LoopCount + 1; 
                                    SET @LOA_StartDate = (
                                                           SELECT   LOA_StartDate
                                                           FROM     #LOA
                                                           WHERE    LOA_Row = @LOA_LoopCount
                                                         );
                                    SET @LOA_Days = (
                                                      SELECT    LOA_Days
                                                      FROM      #LOA
                                                      WHERE     LOA_Row = @LOA_LoopCount
                                                    );
							--If Student was on LOA, add that to the end of the PP where the LOA exists. 
                                    IF @LOA_StartDate BETWEEN @TermStartDate AND @TermEndDate
                                        BEGIN
                                            SET @Is_LOA = 1;
                                            IF @LOA_Days < 180
                                                BEGIN	
                                                    SET @LOA_DayCum = @LOA_DayCum + @LOA_Days;
                                                    SET @Comments = 'LOA of ' + CONVERT(VARCHAR(5),@LOA_DayCum) + ' day(s).';
                                                END;
                                            ELSE
                                                BEGIN
                                                    SET @Comments = 'Students LOA is longer than 180 Days';
                                                    SET @TermEndDate = '1900-01-31';
                                                END;		
                                        END;
                                    ELSE
                                        BEGIN
                                            SET @PrevTermEndDate = @TermEndDate;
                                            SET @Is_LOA = 0;
                                        END;	
                                END;	
						
						--IF @Is_LOA = 1 
						--We need to adjust the term end date here
							--BEGIN
							--	SET @TermEndDate = (SELECT DATEADD(DAY, @LOA_DayCum, @TermEndDate)) 
							--END


				--This is Aca Year INT info
                            SET @AcademicYear_INT_Count = @AcademicYear_INT_Count + 1;
				
                            IF @AcademicYear_INT_Count <= @PayPerPerAcYr
                                BEGIN
                                    SET @AcademicYear_INT = @AcademicYear_INT + 1;
                                    IF @AcademicYear_INT_Count = @PayPerPerAcYr
                                        BEGIN
                                            SET @AcademicYear_INT = @AcademicYear_INT - 1;
                                            SET @AcademicYear_INT_Count = 0;
                                        END;
                                END;
								
                            INSERT  INTO @PayPeriods
                                    (
                                     PaymentPeriod
                                    ,StudentID
                                    ,StudentEnrollmentId
                                    ,AcaYrLen
                                    ,ProgramID
                                    ,ProgramVersionID
                                    ,AcaCalendar
                                    ,ProgLength
                                    ,Hours
                                    ,TotalProgCred
                                    ,PayPeriodsPerAcYr
                                    ,PeriodStartDate
                                    ,PeriodEndDate
                                    ,Comments
                                    ,AcaYear_INT
                                    )
                            VALUES  (
                                     @PeriodNumber
                                    ,@StudentID
                                    ,@StudentEnrollmentId
                                    ,@AcaYrLen
                                    ,@ProgramID
                                    ,@ProgramVersionID
                                    ,@AcaCalendar
                                    ,NULL
                                    ,NULL
                                    ,@TotalProgCred
                                    ,@PayPerPerAcYr
                                    ,@TermStartDate
                                    ,@TermEndDate
                                    ,@Comments
                                    ,@AcademicYear_INT
                                    );
					 
                            SET @PeriodNumber = @PeriodNumber + 1;
                        END;	

						

                    SET @PrevTermEndDate = @TermEndDate;
                END;
	
	--Clear the comments
            SET @Comments = '';


            SET @Counter = @Counter + 1;
--SET @PreviousEndDate = @ClassEndDate

            FETCH NEXT FROM PaymentPeriods
INTO @StudentID,@StudentEnrollmentId,@ProgramID,@ProgramVersionID,@AcaCalendar,@ReqID,@AcaYrLen,@StuEnrollStartDate,@CourseCredits,@IsExternship,@IsPass,
                @CreditsEarned,@FinAidCred,@HoursEarned,@ReqHours,@PrgVerHours,@PrgWeeks,@TotalProgCred,@PayPerPerAcYr,@ClassStartDate,@ClassEndDate,
                @TermStartDate,@TermEndDate;
--, @LOA_StartDate, @LOA_Days


        END;

    CLOSE PaymentPeriods;
    DEALLOCATE PaymentPeriods;

--insert the pay period records if the student has earned some credits

--first we delete the existing records
    DELETE  FROM dbo.arStuPayPeriods
    WHERE   StudentEnrollmentID = @StudentEnrollmentId;

    BEGIN

        INSERT  INTO arStuPayPeriods
                (
                 StudentEnrollmentID
                ,PaymentPeriod
                ,PaymentPeriodRange
                ,AcademicCalendarType
                ,PeriodStartDate
                ,PeriodEndDate
                ,AcaYrLen
                ,ProgLength
                ,PeriodLength
                ,PayPeriodsPerAcYr
                ,Comments
                ,AcademicYear
                )
                SELECT  StudentEnrollmentId
                       ,PaymentPeriod
                       ,PaymentPeriodRange
                       ,AcaCalendar
                       ,PeriodStartDate
                       ,NULLIF(PeriodEndDate,'1900-01-31')
                       ,AcaYrLen
                       ,CASE COALESCE(ProgLength,0)
                          WHEN 0 THEN COALESCE(TotalProgCred,0)
                          ELSE 0
                        END
                       ,COALESCE(Hours,0)
                       ,PayPeriodsPerAcYr
                       ,NULLIF(Comments,'')
                       ,AcaYear_INT
                FROM    @PayPeriods t;

    END;

    SELECT  *
    FROM    @PayPeriods;








GO
