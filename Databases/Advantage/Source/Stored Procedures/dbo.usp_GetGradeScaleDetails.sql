SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_GetGradeScaleDetails]
    (
     @termId UNIQUEIDENTIFIER
    ,@clsSectionId UNIQUEIDENTIFIER
 

    )
AS
    SET NOCOUNT ON;
    SELECT  A.GrdScaleDetailId
           ,A.GrdScaleId
           ,A.MinVal
           ,A.MaxVal
           ,A.GrdSysDetailId
           ,(
              SELECT DISTINCT
                        IsPass
              FROM      arGradeSystemDetails
              WHERE     GrdSysDetailId = A.GrdSysDetailId
            ) AS Pass
           ,(
              SELECT DISTINCT
                        GrdSystemId
              FROM      arGradeSystemDetails
              WHERE     GrdSysDetailId = A.GrdSysDetailId
            ) AS GrdSystemId
    FROM    arGradeScaleDetails A
    WHERE   A.GrdScaleId IN ( SELECT    GrdScaleId
                              FROM      arClassSections
                              WHERE     TermId = @termId
                                        AND ClsSectionId = @clsSectionId )
    ORDER BY MinVal; 



GO
