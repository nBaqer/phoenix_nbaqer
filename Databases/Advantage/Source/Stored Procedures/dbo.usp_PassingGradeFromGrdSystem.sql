SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_PassingGradeFromGrdSystem]
    (
     @GrdSysDetailId UNIQUEIDENTIFIER
    ,@grade VARCHAR(100)
    )
AS
    SET NOCOUNT ON;
    SELECT  COUNT(Grade)
    FROM    arGradeSystemDetails
    WHERE   Vieworder <= (
                           (SELECT  b.ViewOrder
                            FROM    arGradeSystemDetails b
                            WHERE   b.GrdSystemId IN ( SELECT DISTINCT
                                                                a.GrdSystemId
                                                       FROM     arGradeSystemDetails a
                                                       WHERE    a.GrdSysDetailId = @GrdSysDetailId )
                                    AND b.GrdSysDetailId = @GrdSysDetailId)
                         )
            AND GrdSystemId IN ( SELECT DISTINCT
                                        a.GrdSystemId
                                 FROM   arGradeSystemDetails a
                                 WHERE  a.GrdSysDetailId = @GrdSysDetailId )
            AND Grade = @grade;



GO
