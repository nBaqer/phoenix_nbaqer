SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
------------------------------------------------------------------------------------------------------------------------------
---DE7659 QA:  Issue on the Manage Security page with the listing of pages. Geetha M 06/04/2012 End
------------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------------
---DE7652 Balaji Start - Changes made based on Peer Code Review Findings
------------------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[USP_UpdateMRUList]
    @EntityId UNIQUEIDENTIFIER
   ,@UserId UNIQUEIDENTIFIER
   ,@CampusId UNIQUEIDENTIFIER
   ,@ModUser VARCHAR(50)
AS
    UPDATE  syMRUS
    SET     ModUser = @ModUser
           ,ModDate = GETDATE()
--,CampusId=@CampusId 
    WHERE   ChildId = @EntityId
            AND UserId = @UserId
            AND CampusId = @CampusId; 



GO
