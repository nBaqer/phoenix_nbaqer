SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Edwin Sosa
-- Create date: 12/11/2018
-- Description:	Stored procedure for calculating scheduled and completed hours for an enrollment
-- =============================================
CREATE PROCEDURE [dbo].[USP_SchedAndCompletedHours_ByDay]
    -- Add the parameters for the stored procedure here
    @stuEnrollId UNIQUEIDENTIFIER = NULL
   ,@attUnitType VARCHAR(50)
   ,@startDate DATETIME = NULL
   ,@cutOffDate DATETIME = NULL
AS
    BEGIN
        -- SET NOCOUNT ON added to prevent extra result sets from
        -- interfering with SELECT statements.
        SET NOCOUNT ON;

        DECLARE @AttendanceWindow TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,RecordDate DATETIME
               ,SchedHours DECIMAL(18, 2)
               ,ActualHours DECIMAL(18, 2)
               ,IsTardy BIT
            );

        DECLARE @AttendanceResults TABLE
            (
                TotalHoursAbsent DECIMAL(18, 2)
               ,TotalPresentHours DECIMAL(18, 2)
               ,SchedHours DECIMAL(18, 2)
               ,TotalMakeupHours DECIMAL(18, 2)
               ,ScheduleHoursByWeek DECIMAL(18, 2)
               ,LastDateAttended DATETIME
               ,TardyCount INT
            );
        INSERT INTO @AttendanceWindow (
                                      StuEnrollId
                                     ,RecordDate
                                     ,SchedHours
                                     ,ActualHours
                                     ,IsTardy
                                      )
                    SELECT attWindow.StuEnrollId
                          ,attWindow.RecordDate
                          ,attWindow.SchedHours
                          ,attWindow.ActualHours
                          ,attWindow.isTardy
                    FROM   (
                           SELECT SCA.StuEnrollId
                                 ,SCA.RecordDate
                                 ,SCA.SchedHours
                                 ,SCA.ActualHours
                                 ,SCA.isTardy
                           FROM   dbo.arStudentClockAttendance SCA
                           WHERE  (
                                  @stuEnrollId IS NULL
                                  OR SCA.StuEnrollId = @stuEnrollId
                                  )
                                  AND (
                                      @startDate IS NULL
                                      OR SCA.RecordDate >= @startDate
                                      )
                                  AND (
                                      @cutOffDate IS NULL
                                      OR SCA.RecordDate < @cutOffDate
                                      )
                           UNION
                           SELECT ACA.StuEnrollId
                                 ,ACA.MeetDate
                                 ,ACA.Schedule
                                 ,ACA.Actual
                                 ,ACA.Tardy
                           FROM   dbo.atConversionAttendance ACA
                           WHERE  (
                                  @stuEnrollId IS NULL
                                  OR ACA.StuEnrollId = @stuEnrollId
                                  )
                                  AND (
                                      @startDate IS NULL
                                      OR ACA.MeetDate >= @startDate
                                      )
                                  AND (
                                      @cutOffDate IS NULL
                                      OR ACA.MeetDate < @cutOffDate
                                      )
                           ) attWindow;
        -- Insert statements for procedure here
        --if unit type is Present Absent
        IF LOWER(@attUnitType) = 'pa'
            BEGIN

                ;WITH ScheduledHours
                 AS ((SELECT SUM(SchedHours) AS SchedHours
                      FROM   @AttendanceWindow
                      WHERE  SchedHours >= 1.00
                             AND ActualHours IS NOT NULL
                             AND ActualHours <> 999.00
                             AND ActualHours <> 9999.00))
                     ,PresentHours
                 AS ( SELECT SUM(aw.ActualHours) AS TotalPresentHours
                      FROM   @AttendanceWindow aw
                      WHERE  (
                             aw.ActualHours >= 1.00
                             AND aw.ActualHours <> 999.00
                             AND aw.ActualHours <> 9999.00
                             ))
                     ,AbsentHours
                 AS ((SELECT SUM(SchedHours - ActualHours) TotalHoursAbsent
                      FROM   @AttendanceWindow aw
                      WHERE  (
                             ActualHours <> 999.00
                             AND ActualHours <> 9999.00
                             AND ( aw.ActualHours < aw.SchedHours )
                             )))
                     ,MakeupHours
                 AS ( SELECT SUM(ActualHours - SchedHours) AS TotalMakeUpHours
                      FROM   @AttendanceWindow aw
                      WHERE  (
                             (
                             ActualHours >= 0.00
                             AND ActualHours <> 999.00
                             AND ActualHours <> 9999.00
                             )
                             AND ( ActualHours > SchedHours )
                             ))
                     ,ScheduleHoursByWeek
                 AS ((SELECT SUM(t4.total) ScheduleHoursByWeek
                      FROM   arStuEnrollments t1
                            ,arStudentSchedules t2
                            ,arProgSchedules t3
                            ,arProgScheduleDetails t4
                      WHERE  t1.StuEnrollId = t2.StuEnrollId
                             AND t1.PrgVerId = t3.PrgVerId
                             AND t2.ScheduleId = t3.ScheduleId
                             AND t3.ScheduleId = t4.ScheduleId
                             AND t2.StuEnrollId IN (
                                                   SELECT TOP 1 aw.StuEnrollId
                                                   FROM   @AttendanceWindow aw
                                                   )
                             AND t4.total IS NOT NULL))
                     ,LastDateAttended
                 AS ((SELECT MAX(RecordDate) AS LastDateAttended
                      FROM   @AttendanceWindow aw
                      WHERE  (
                             ActualHours >= 1.00
                             AND ActualHours <> 999.00
                             AND ActualHours <> 9999.00
                             )))
                     ,TardyCount
                 AS ( SELECT COUNT(*) AS TardyCount
                      FROM   @AttendanceWindow aw
                      WHERE  IsTardy = 1 )
                INSERT INTO @AttendanceResults
                            SELECT AbsentHours.TotalHoursAbsent
                                  ,PresentHours.TotalPresentHours
                                  ,ScheduledHours.SchedHours
                                  ,MakeupHours.TotalMakeUpHours
                                  ,ScheduleHoursByWeek.ScheduleHoursByWeek
                                  ,LastDateAttended.LastDateAttended
                                  ,TardyCount.TardyCount
                            FROM   ScheduledHours
                                  ,AbsentHours
                                  ,MakeupHours
                                  ,PresentHours
                                  ,ScheduleHoursByWeek
                                  ,LastDateAttended
                                  ,TardyCount;
            END;


        ELSE IF LOWER(@attUnitType) = 'minutes'
                 BEGIN


                     ;WITH ScheduledHours
                      AS ((SELECT SUM(SchedHours) AS SchedHours
                           FROM   @AttendanceWindow
                           WHERE  SchedHours >= 0.00
                                  AND ActualHours IS NOT NULL
                                  AND ActualHours <> 999.00
                                  AND ActualHours <> 9999.00))
                          ,PresentHours
                      AS ( SELECT SUM(aw.ActualHours) AS TotalPresentHours
                           FROM   @AttendanceWindow aw
                           WHERE  (
                                  aw.ActualHours >= 0.00
                                  AND aw.ActualHours <> 999.00
                                  AND aw.ActualHours <> 9999.00
                                  ))
                          ,AbsentHours
                      AS ((SELECT SUM(SchedHours - ActualHours) TotalHoursAbsent
                           FROM   @AttendanceWindow aw
                           WHERE  (
                                  ActualHours <> 999.00
                                  AND SchedHours >= 0.00
                                  AND ActualHours <> 9999.00
                                  AND ( aw.ActualHours < aw.SchedHours )
                                  )))
                          ,MakeupHours
                      AS ( SELECT SUM(ActualHours - SchedHours) AS TotalMakeUpHours
                           FROM   @AttendanceWindow aw
                           WHERE  (
                                  (
                                  ActualHours >= 0.00
                                  AND ActualHours <> 999.00
                                  AND ActualHours <> 9999.00
                                  )
                                  AND ( ActualHours > SchedHours )
                                  ))
                          ,ScheduleHoursByWeek
                      AS ((SELECT SUM(t4.total) ScheduleHoursByWeek
                           FROM   arStuEnrollments t1
                                 ,arStudentSchedules t2
                                 ,arProgSchedules t3
                                 ,arProgScheduleDetails t4
                           WHERE  t1.StuEnrollId = t2.StuEnrollId
                                  AND t1.PrgVerId = t3.PrgVerId
                                  AND t2.ScheduleId = t3.ScheduleId
                                  AND t3.ScheduleId = t4.ScheduleId
                                  AND t2.StuEnrollId IN (
                                                        SELECT TOP 1 aw.StuEnrollId
                                                        FROM   @AttendanceWindow aw
                                                        )
                                  AND t4.total IS NOT NULL))
                          ,LastDateAttended
                      AS ((SELECT MAX(RecordDate) AS LastDateAttended
                           FROM   @AttendanceWindow aw
                           WHERE  (
                                  ActualHours >= 1.00
                                  AND ActualHours <> 999.00
                                  AND ActualHours <> 9999.00
                                  )))
                          ,TardyCount
                      AS ( SELECT COUNT(*) AS TardyCount
                           FROM   @AttendanceWindow aw
                           WHERE  IsTardy = 1 )
                     INSERT INTO @AttendanceResults
                                 SELECT AbsentHours.TotalHoursAbsent
                                       ,PresentHours.TotalPresentHours
                                       ,ScheduledHours.SchedHours
                                       ,MakeupHours.TotalMakeUpHours
                                       ,ScheduleHoursByWeek.ScheduleHoursByWeek
                                       ,LastDateAttended.LastDateAttended
                                       ,TardyCount.TardyCount
                                 FROM   ScheduledHours
                                       ,AbsentHours
                                       ,MakeupHours
                                       ,PresentHours
                                       ,ScheduleHoursByWeek
                                       ,LastDateAttended
                                       ,TardyCount;
                 END;


        ELSE IF LOWER(@attUnitType) = 'hrs'
                 BEGIN

                     ;WITH ScheduledHours
                      AS ((SELECT SUM(SchedHours) AS SchedHours
                           FROM   @AttendanceWindow
                           WHERE  ActualHours IS NOT NULL
                                  AND ActualHours <> 999.00
                                  AND ActualHours <> 9999.00))
                          ,PresentHours
                      AS ( SELECT SUM(aw.ActualHours) AS TotalPresentHours
                           FROM   @AttendanceWindow aw
                           WHERE  (
                                  aw.ActualHours <> 999.00
                                  AND aw.ActualHours <> 9999.00
                                  ))
                          ,AbsentHours
                      AS ((SELECT SUM(SchedHours - ActualHours) TotalHoursAbsent
                           FROM   @AttendanceWindow aw
                           WHERE  (
                                  ActualHours <> 999.00
                                  AND ActualHours <> 9999.00
                                  AND ( aw.ActualHours < aw.SchedHours )
                                  )))
                          ,MakeupHours
                      AS ( SELECT SUM(ActualHours - SchedHours) AS TotalMakeUpHours
                           FROM   @AttendanceWindow aw
                           WHERE  (
                                  (
                                  ActualHours <> 999.00
                                  AND ActualHours <> 9999.00
                                  )
                                  AND ( ActualHours > SchedHours )
                                  ))
                          ,ScheduleHoursByWeek
                      AS ((SELECT SUM(t4.total) ScheduleHoursByWeek
                           FROM   arStuEnrollments t1
                                 ,arStudentSchedules t2
                                 ,arProgSchedules t3
                                 ,arProgScheduleDetails t4
                           WHERE  t1.StuEnrollId = t2.StuEnrollId
                                  AND t1.PrgVerId = t3.PrgVerId
                                  AND t2.ScheduleId = t3.ScheduleId
                                  AND t3.ScheduleId = t4.ScheduleId
                                  AND t2.StuEnrollId IN (
                                                        SELECT TOP 1 aw.StuEnrollId
                                                        FROM   @AttendanceWindow aw
                                                        )
                                  AND t4.total IS NOT NULL))
                          ,LastDateAttended
                      AS ((SELECT MAX(RecordDate) AS LastDateAttended
                           FROM   @AttendanceWindow aw
                           WHERE  (
                                  ActualHours <> 999.00
                                  AND ActualHours <> 9999.00
                                  )))
                          ,TardyCount
                      AS ( SELECT COUNT(*) AS TardyCount
                           FROM   @AttendanceWindow aw
                           WHERE  IsTardy = 1 )
                     INSERT INTO @AttendanceResults
                                 SELECT AbsentHours.TotalHoursAbsent
                                       ,PresentHours.TotalPresentHours
                                       ,ScheduledHours.SchedHours
                                       ,MakeupHours.TotalMakeUpHours
                                       ,ScheduleHoursByWeek.ScheduleHoursByWeek
                                       ,LastDateAttended.LastDateAttended
                                       ,TardyCount.TardyCount
                                 FROM   ScheduledHours
                                       ,AbsentHours
                                       ,MakeupHours
                                       ,PresentHours
                                       ,ScheduleHoursByWeek
                                       ,LastDateAttended
                                       ,TardyCount;
                 END;

        ELSE
                 BEGIN

                     ;WITH ScheduledHours
                      AS ((SELECT SUM(SchedHours) AS SchedHours
                           FROM   @AttendanceWindow
                           WHERE  SchedHours >= 1.00
                                  AND ActualHours IS NOT NULL
                                  AND ActualHours <> 999.00
                                  AND ActualHours <> 9999.00))
                          ,PresentHours
                      AS ( SELECT SUM(aw.ActualHours) AS TotalPresentHours
                           FROM   @AttendanceWindow aw
                           WHERE  (
                                  aw.ActualHours >= 1.00
                                  AND aw.ActualHours <> 999.00
                                  AND aw.ActualHours <> 9999.00
                                  ))
                          ,AbsentHours
                      AS ((SELECT SUM(SchedHours - ActualHours) TotalHoursAbsent
                           FROM   @AttendanceWindow aw
                           WHERE  (
                                  ActualHours <> 999.00
                                  AND ActualHours <> 9999.00
                                  AND SchedHours >= 1.00
                                  AND ( aw.ActualHours < aw.SchedHours )
                                  )))
                          ,MakeupHours
                      AS ( SELECT SUM(ActualHours - SchedHours) AS TotalMakeUpHours
                           FROM   @AttendanceWindow aw
                           WHERE  (
                                  (
                                  ActualHours >= 1.00
                                  AND ActualHours <> 999.00
                                  AND SchedHours IS NULL
                                  OR SchedHours = 0.00
                                     AND ActualHours <> 9999.00
                                  )
                                  AND ( ActualHours > SchedHours )
                                  ))
                          ,ScheduleHoursByWeek
                      AS ((SELECT SUM(t4.total) ScheduleHoursByWeek
                           FROM   arStuEnrollments t1
                                 ,arStudentSchedules t2
                                 ,arProgSchedules t3
                                 ,arProgScheduleDetails t4
                           WHERE  t1.StuEnrollId = t2.StuEnrollId
                                  AND t1.PrgVerId = t3.PrgVerId
                                  AND t2.ScheduleId = t3.ScheduleId
                                  AND t3.ScheduleId = t4.ScheduleId
                                  AND t2.StuEnrollId IN (
                                                        SELECT TOP 1 aw.StuEnrollId
                                                        FROM   @AttendanceWindow aw
                                                        )
                                  AND t4.total IS NOT NULL))
                          ,LastDateAttended
                      AS ((SELECT MAX(RecordDate) AS LastDateAttended
                           FROM   @AttendanceWindow aw
                           WHERE  (
                                  ActualHours >= 1.00
                                  AND ActualHours <> 999.00
                                  AND ActualHours <> 9999.00
                                  )))
                          ,TardyCount
                      AS ( SELECT COUNT(*) AS TardyCount
                           FROM   @AttendanceWindow aw
                           WHERE  IsTardy = 1 )
                     INSERT INTO @AttendanceResults
                                 SELECT AbsentHours.TotalHoursAbsent
                                       ,PresentHours.TotalPresentHours
                                       ,ScheduledHours.SchedHours
                                       ,MakeupHours.TotalMakeUpHours
                                       ,ScheduleHoursByWeek.ScheduleHoursByWeek
                                       ,LastDateAttended.LastDateAttended
                                       ,TardyCount.TardyCount
                                 FROM   ScheduledHours
                                       ,AbsentHours
                                       ,MakeupHours
                                       ,PresentHours
                                       ,ScheduleHoursByWeek
                                       ,LastDateAttended
                                       ,TardyCount;
                 END;

        SELECT ISNULL(TotalHoursAbsent, 0) AS TotalHoursAbsent
              ,ISNULL(TotalPresentHours, 0) AS TotalPresentHours
              ,ISNULL(SchedHours, 0) AS SchedHours
              ,ISNULL(TotalMakeupHours, 0) AS TotalMakeUpHours
              ,ScheduleHoursByWeek
              ,LastDateAttended
              ,TardyCount
        FROM   @AttendanceResults;
    END;
GO
