SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[USP_AR_GetSystemTransCodesRefunds_GetList]
    (
        @ShowActive NVARCHAR(50)
       ,@CampusId NVARCHAR(50) = NULL
    )
AS /*----------------------------------------------------------------------------------------------------
    Author : Saraswathi Lakshmanan
    
    Create date : 05/05/2010
    
    Procedure Name : [USP_AR_GetSystemTransCodesRefunds_GetList]

    Objective : Get the system TransactionCodes of type charges
    
    Parameters : Name Type Data Type Required? 
                        ===== ==== ========= ========= 
                        @ShowActive In nvarchar Required
    
    Output : Returns the TransCodes of type whose systransCodeID 16 
                        
*/
    -----------------------------------------------------------------------------------------------------

    BEGIN

        DECLARE @ShowActiveValue NVARCHAR(20);
        IF LOWER(@ShowActive) = 'true'
            BEGIN
                SET @ShowActiveValue = 'Active';
            END;
        ELSE
            BEGIN
                SET @ShowActiveValue = 'InActive';
            END;


        SELECT   TC.TransCodeId
                ,TC.TransCodeCode
                ,TC.TransCodeDescrip
                ,TC.StatusId
                ,TC.CampGrpId
                ,ST.StatusId
                ,ST.Status
        FROM     saTransCodes TC
                ,syStatuses ST
        WHERE    TC.StatusId = ST.StatusId
                 AND SysTransCodeId = 16
                 AND ST.Status = @ShowActiveValue
                 AND (
                     @CampusId IS NULL
                     OR CampGrpId IN (
                                     SELECT CampGrpId
                                     FROM   dbo.syCmpGrpCmps
                                     WHERE  CampusId = @CampusId
                                     )
                     )
        ORDER BY TC.TransCodeDescrip;

    END;








GO
