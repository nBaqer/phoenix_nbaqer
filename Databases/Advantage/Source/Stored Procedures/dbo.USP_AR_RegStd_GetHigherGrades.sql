SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[USP_AR_RegStd_GetHigherGrades]
    (
     @GrdSysDetailID UNIQUEIDENTIFIER
    )
AS /*----------------------------------------------------------------------------------------------------
	Author          :	Saraswathi Lakshmanan
    
    Create date		:	02/04/2010
    
	Procedure Name	:	USP_AR_RegStd_GetHigherGrades

	Objective		:	Gets the higher grades
	
	Parameters		:	Name			Type	Data Type			Required? 	
						=====			====	=========			=========	
						@GrdSysDetailID	In		UniqueIdentifier
						
	Output			:	Returns the higher grade
						
*/-----------------------------------------------------------------------------------------------------
/*
Procedure created by Saraswathi Lakshmanan on Jan 29 2010
--To fix issue 17548: ENH: All: Register Students Page is Taking Too Long to Populate Available Students 
*/

    BEGIN
        SELECT  Grade
        FROM    arGradeSystemDetails
        WHERE   Vieworder <= (
                               (SELECT  b.ViewOrder
                                FROM    arGradeSystemDetails b
                                WHERE   b.GrdSystemId IN ( SELECT DISTINCT
                                                                    a.GrdSystemId
                                                           FROM     arGradeSystemDetails a
                                                           WHERE    a.GrdSysDetailId = @GrdSysDetailID )
                                        AND b.GrdSysDetailId = @GrdSysDetailID)
                             )
                AND GrdSystemId IN ( SELECT DISTINCT
                                            a.GrdSystemId
                                     FROM   arGradeSystemDetails a
                                     WHERE  a.GrdSysDetailId = @GrdSysDetailID ); 
    END;




GO
