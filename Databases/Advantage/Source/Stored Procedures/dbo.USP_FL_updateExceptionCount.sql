SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[USP_FL_updateExceptionCount]
    @FileName VARCHAR(100)
   ,@Success INT
   ,@Failed INT
AS
    UPDATE  syFameESPExceptionReport
    SET     Success = @Success
           ,Failed = @Failed
    WHERE   FileName = @FileName;



GO
