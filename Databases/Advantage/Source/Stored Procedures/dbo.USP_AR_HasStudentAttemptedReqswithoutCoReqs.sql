SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[USP_AR_HasStudentAttemptedReqswithoutCoReqs]
    (
     @StuEnrollId UNIQUEIDENTIFIER
    ,@ClsSectionID UNIQUEIDENTIFIER
    )
AS /*----------------------------------------------------------------------------------------------------
	Author          :	Saraswathi Lakshmanan
    
    Create date		:	02/03/2010
    
	Procedure Name	:	USP_AR_HasStudentAttemptedReqswithoutCoReqs

	Objective		:	find if the Student has attempted the Reqs and the prereqs
	
	Parameters		:	Name			Type	Data Type	Required? 	
						=====			====	=========	=========	
						@StuEnrollId	In		Varchar		Required	
						@ClsSectionID	In		varChar		Required
	
	Output			:	Returns the rowcount					
						
*/-----------------------------------------------------------------------------------------------------

    BEGIN
	
--SELECT 
--	Sum(Num) 
--from 
--	(Select Coalesce(
--		(Select 
--			count(*) as Count 
--		from 
--			arResults a inner Join  arClassSections b on a.TestId=b.ClsSectionId
--		where 
--			a.StuEnrollId in (Select StuEnrollId from arStuEnrollments where StudentId in 
--					(Select StudentId from arStuEnrollments where StuEnrollId = @StuEnrollId)) 
--			and b.ClsSectionId = @ClsSectionID)
--	,0 )as Num 
--	union all 
--	Select Coalesce(
--		(Select 
--			count(*) as Count 
--		from 
--			arTransferGrades t9 inner Join arClassSections C on t9.ReqID=C.ReqID
--		where 
--			t9.StuEnrollId = @StuEnrollId and 
--			C.ClsSectionID=@ClsSectionID)
--	,0)as Num 
--)R1 

        SELECT  SUM(Num)
        FROM    (
                  SELECT    COALESCE((
                                       SELECT   COUNT(*) AS Count
                                       FROM     arResults a
                                               ,arClassSections b
                                       WHERE    a.StuEnrollId IN ( SELECT   StuEnrollId
                                                                   FROM     arStuEnrollments
                                                                   WHERE    StudentId IN ( SELECT   StudentId
                                                                                           FROM     arStuEnrollments
                                                                                           WHERE    StuEnrollId = @StuEnrollId ) )
                                                AND a.TestId = b.ClsSectionId
                                                AND b.ReqId = (
                                                                SELECT  ReqId
                                                                FROM    arClassSections
                                                                WHERE   ClsSectionId = @ClsSectionID
                                                              )
                                     ),0) AS Num
                  UNION ALL
                  SELECT    COALESCE((
                                       SELECT   COUNT(*) AS Count
                                       FROM     arTransferGrades t9
                                       WHERE    t9.StuEnrollId = @StuEnrollId
                                                AND t9.ReqId IN ( SELECT    ReqId
                                                                  FROM      arClassSections
                                                                  WHERE     ClsSectionId = @ClsSectionID )
                                     ),0) AS Num
                ) R1;


    END;



GO
