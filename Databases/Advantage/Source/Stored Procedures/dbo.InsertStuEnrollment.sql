SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[InsertStuEnrollment]
AS
    DECLARE @strControlName VARCHAR(200);
    DECLARE @StuEnrollId UNIQUEIDENTIFIER
       ,@LeadId UNIQUEIDENTIFIER
       ,@StudentId UNIQUEIDENTIFIER;
    DECLARE @FldName VARCHAR(100);
    DECLARE @intRecExists INT;
    DECLARE @LeadGrpId UNIQUEIDENTIFIER;
    SELECT  @LeadGrpId = (
                           SELECT TOP 1
                                    LeadGrpId
                           FROM     adLeadGroups
                           WHERE    Descrip LIKE 'US%'
                                    OR Descrip LIKE 'Local%'
                         );
    DECLARE GetDDL_Cursor CURSOR
    FOR
        SELECT 
		DISTINCT
                a2.StuEnrollId
               ,a2.LeadId
               ,a2.StudentId
        FROM    plStudentDocs a1
               ,arStuEnrollments a2
        WHERE   a1.StudentId = a2.StudentId
                AND a2.StuEnrollId NOT IN ( SELECT 
						DISTINCT                    StuEnrollId
                                            FROM    adLeadByLeadGroups
                                            WHERE   StuEnrollId IS NOT NULL );

--select * from adLeadByLeadGroups1 
    OPEN GetDDL_Cursor;

    FETCH NEXT FROM GetDDL_Cursor
INTO @StuEnrollId,@LeadId,@StudentId;

    WHILE @@FETCH_STATUS = 0
        BEGIN
            SET @intRecExists = (
                                  SELECT    COUNT(*)
                                  FROM      adLeadByLeadGroups
                                  WHERE     LeadId = @LeadId
                                            AND StudentId = @StudentId
                                            AND StudentId IS NOT NULL
                                );
            IF @intRecExists = 1
                BEGIN
                    UPDATE  adLeadByLeadGroups
                    SET     StuEnrollId = @StuEnrollId
                    WHERE   LeadId = @LeadId
                            AND StudentId = @StudentId;
                END; 
            ELSE
                BEGIN
                    INSERT  INTO adLeadByLeadGroups
                            (
                             LeadGrpLeadId
                            ,LeadGrpId
                            ,StuEnrollId
                            )
                    VALUES  (
                             NEWID()
                            ,@LeadGrpId
                            ,@StuEnrollId
                            );
                END; 	  
      	
            FETCH NEXT FROM GetDDL_Cursor INTO @StuEnrollId,@LeadId,@StudentId;
        END;
    CLOSE GetDDL_Cursor;
    DEALLOCATE GetDDL_Cursor;



GO
