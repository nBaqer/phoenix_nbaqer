SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_DBA_extended_properties_SET]
    @TableName sysname
   ,@MS_DescriptionValue SQL_VARIANT
AS
    BEGIN
		-- SP to add or update an extended property on a table
        DECLARE @MS_Description NVARCHAR(MAX); 
        SET @MS_Description = NULL;

        SET @MS_Description = (
                                SELECT  CAST(value AS NVARCHAR(200)) AS MS_Description
                                FROM    sys.extended_properties AS ep
                                WHERE   ep.major_id = OBJECT_ID(@TableName)
                                        AND ep.name = N'MS_Description'
                                        AND ep.minor_id = 0
                              ); 

        IF @MS_Description IS NULL
            BEGIN
                EXEC sys.sp_addextendedproperty @name = N'MS_Description',@value = @MS_DescriptionValue,@level0type = N'SCHEMA',@level0name = N'dbo',
                    @level1type = N'TABLE',@level1name = @TableName;
            END;
        ELSE
            BEGIN
                EXEC sys.sp_updateextendedproperty @name = N'MS_Description',@value = @MS_DescriptionValue,@level0type = N'SCHEMA',@level0name = N'dbo',
                    @level1type = N'TABLE',@level1name = @TableName;
            END;
    END;



GO
