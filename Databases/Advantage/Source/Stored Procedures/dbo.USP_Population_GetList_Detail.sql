SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--=================================================================================================
-- USP_Population_GetList_Detail
--=================================================================================================
---------------------------------------------------------------------------------------------------------------------------
--This section is for testing purposes only.
----------------------------------------------------------------------------------------------------------------------------
--declare @CampGrpId as varchar(8000)
--declare @PrgVerId as varchar(8000)
--declare @StartDate as datetime
--declare @EndDate as datetime
--declare @IncludeLOA as bit
--
--set @CampGrpId='0F811F4D-FA5E-45A7-B9C4-3B52A8FE1238' 
--set @PrgVerId = null
--set @StartDate='3/1/2010'
--set @EndDate='4/1/2011'
--set @IncludeLOA=1
----Truncate Table #TotalByProgramVersion 
---------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[USP_Population_GetList_Detail]
    @CampGrpId VARCHAR(8000) = '0F811F4D-FA5E-45A7-B9C4-3B52A8FE1238'
   ,@PrgVerId VARCHAR(8000) = NULL
   ,@StartDate DATETIME = '7/1/2010'
   ,@EndDate DATETIME = '6/30/2011'
   ,@IncludeLOA BIT = 1
AS
    BEGIN 
        CREATE TABLE #TotalByProgramVersion
            (
             StudentName VARCHAR(100)
            ,PrgVerId UNIQUEIDENTIFIER
            ,PrgVerDescrip VARCHAR(100)
            ,PrgVerCode VARCHAR(50)
            ,GenderDescrip VARCHAR(50)
            ,EthCodeDescrip VARCHAR(100)
            ,StuEnrollId UNIQUEIDENTIFIER
            ,BeginningEnrollment INT
            ,NewStarts INT
            ,ReEntry INT
            ,ReturnFromLOA INT
            ,LOA INT
            ,Incomplete INT
            ,Droped INT
            ,Graduated INT
            ,Complete INT
            ,TransferIn INT
            ,TransferOut INT
            ,EndingEnrollment INT
            ,CampDescrip VARCHAR(100)
            ,CampGrpDescrip VARCHAR(50)
            );
 
        IF @IncludeLOA = 1
            BEGIN

                INSERT  INTO #TotalByProgramVersion
                        SELECT  S.FirstName + ' ' + S.LastName + ' ' + ( ISNULL(S.MiddleName,'') ) AS StudentName
                               ,SE.PrgVerId
                               ,PV.PrgVerDescrip
                               ,PV.PrgVerCode
                               ,G.GenderDescrip
                               ,EC.EthCodeDescrip
                               ,SE.StuEnrollId
                               ,
---- Beginning enrollment ----
---- They must have started before the reporting period and expected to grad on/after the end of the reporting period.
---- However, they must also be in one of the in-school status at the start of the reporitng period. 
---- To determine if they were in an in-school status we need to use the syStudentStatusChanges table to examine the 
---- last status up to just before the report period. If the status is an in-school status return 1 else return 0
                                (
                                  SELECT    COUNT(DISTINCT StuEnrollId)
                                  FROM      syStudentStatusChanges SSC
                                           ,syStatusCodes SC
                                           ,sySysStatus SS
                                  WHERE     SSC.StuEnrollId = SE.StuEnrollId
          --The following line was commented out as there are students made currently attending before the
          --start date. Example: Akintunde Azeez in the Coyne DB has a start date of 7/12/2010 but he was made
          --currently attending on 6/8/2010.
          --and SE.StartDate < @StartDate
                                            AND (
                                                  (
                                                    SSC.NewStatusId = SC.StatusCodeId
                                                    AND SC.SysStatusId = SS.SysStatusId
                                                    AND SS.InSchool = 1
                                                    AND SS.SysStatusId <> 7	--This is to exclude Future Starts
                                                    AND SSC.DateOfChange = (
                                                                             SELECT MAX(DateOfChange)
                                                                             FROM   syStudentStatusChanges SSC2
                                                                             WHERE  SSC2.StuEnrollId = SE.StuEnrollId
                                                                                    AND DateOfChange < @StartDate
                                                                           )
                                                  )
                                                  OR
                                  
                                  
                --This section is for the scenario where the student was in a currently attending status but the first 
                --status change was recorded after the start of the reporting period. Example student is Candice Dashmon
                --in Coyne when report is run for 7/1/2010 to 6/30/2011. The first status change recorded from currently
                --attending to something else.
                                                  (
                                                    SSC.OrigStatusId = SC.StatusCodeId
                                                    AND SC.SysStatusId = SS.SysStatusId
                                                    AND SS.SysStatusId = 9
                                                    AND SSC.DateOfChange = (
                                                                             SELECT MIN(DateOfChange)
                                                                             FROM   syStudentStatusChanges SSC2
                                                                             WHERE  SSC2.StuEnrollId = SE.StuEnrollId
                                                                                    AND DateOfChange >= @StartDate
                                                                           )
                                                  )
                                                  OR
                                   
                --This section is for the scenario where student was in a drop status but the first status change was 
                --recorded after the start of the reporting period. Example Student is Portia Harper in Coyne. The first
                --status change recorded is from drop out to currently attending. This change was recorded on 2010-10-19 18:24:31.000.
                --The problem is that the Reenrollment date was on 10/18/2010. If the report is run for 10/19/2010 the student should
                --show up in the BE since she reenrolled on 10/18/2010. 
                                                  (
                                                    SSC.OrigStatusId = SC.StatusCodeId
                                                    AND SC.SysStatusId = SS.SysStatusId
                                                    AND SS.SysStatusId IN ( 12,19 )
                                                    AND SSC.DateOfChange = (
                                                                             SELECT MIN(DateOfChange)
                                                                             FROM   syStudentStatusChanges SSC2
                                                                             WHERE  SSC2.StuEnrollId = SE.StuEnrollId
                                                                                    AND DateOfChange >= @StartDate
                                                                           )
                                                    AND SE.ReEnrollmentDate < @StartDate
                                                  )
                                                )
        --We also have to take into account the DateDetermined when student drops because the ModDate might be later.
        --For example, it was determined that the student dropped on 6/15/2010 so that is entered as the
        --DateDetermined field. However, this was posted in Advantage on 7/5/2010 so the ModDate would be 7/5/2010.
        --If the report was run for an end date of 6/30/2010 the student should be shown as a drop in that case and
        --so should not appear in the BE for a report for 7/1/2010 to 6/30/2011.
                                            AND (
                                                  SE.DateDetermined IS NULL
                                                  OR (
                                                       SE.DateDetermined < @StartDate
                                                       AND SE.ReEnrollmentDate < @StartDate
                                                     )
                                                  OR ( SE.DateDetermined > @StartDate )
                                                )
                                ) AS BeginningEnrollment
                               , 

---- New Starts 
--We need to get students who were were expected to start during the report period.
--We are filtering out the no-starts in the where clause so we don't need to worry about them here.
--(select COUNT(distinct StuEnrollId)
-- from arStuEnrollments SE1
-- where SE1.StuEnrollId=SE.StuEnrollId
-- and SE1.StartDate between @StartDate and @EndDate 
 
--) as NewStarts , 
--Logic was changed because there are students with start dates that are long before the date they were made currently attending
--in the status change table. Example: Christian Ray in the Coyne DB. For the Electrical Construction & Planning (N) he has 
--start date of 3/29/2010. However, he was not made currently attending until 8/9/2010. When the report was run for 
--7/1/2010 to 6/30/2011 the student did not show up in either the BE field or the New Start field.
--The new logic is that the student must be placed in currently attedning or future start status during the report period.
                                (
                                  SELECT    COUNT(DISTINCT StuEnrollId)
                                  FROM      syStudentStatusChanges ssc
                                           ,syStatusCodes sc
                                  WHERE     ssc.StuEnrollId = SE.StuEnrollId
                                            AND ssc.NewStatusId = sc.StatusCodeId
                                            AND sc.SysStatusId = 9
                                            AND ssc.DateOfChange BETWEEN @StartDate AND @EndDate
 --The change to future start/currently attending has to be the first for the enrollment. This is important
 --because a student could have been in the BE and then dropped and made currently attending during the reporting period.
 --Example student is Brittany Splett A in the Coyne DB for the MA 650 (Course) (Nights) program.
 --She was made currently attending on 3/5/2010. When the report was run for 3/10/2010 to 3/9/2011 she appeared in both the
 --BE and New Starts. She was appearing in the New Starts because she was dropped a couple of times and made currently 
 --attending during the reporting period. She was being picked up as New Start because she was made currently attending.
                                            AND ssc.DateOfChange = (
                                                                     SELECT MIN(ssc2.DateOfChange)
                                                                     FROM   syStudentStatusChanges ssc2
                                                                           ,syStatusCodes sc2
                                                                     WHERE  ssc2.StuEnrollId = SE.StuEnrollId
                                                                            AND ssc2.NewStatusId = sc2.StatusCodeId
                                                                            AND sc2.SysStatusId = 9	
    --and ssc2.DateOfChange between @StartDate and @EndDate
                                                                   )
                                ) AS NewStarts
                               ,

---- RE-Entry ---
---- The student needs to have been in a drop status at the start of the report period and then
---- reenrolled during the report period.
                                (
                                  SELECT    COUNT(DISTINCT StuEnrollId)
                                  FROM      syStudentStatusChanges SSC
                                           ,syStatusCodes SC
                                           ,sySysStatus SS
                                  WHERE     SSC.StuEnrollId = SE.StuEnrollId
                                            AND (
                                                  (
                                                    SSC.NewStatusId = SC.StatusCodeId
                                                    AND SC.SysStatusId = SS.SysStatusId
                                                    AND SS.SysStatusId IN ( 12,19 )
                                                    AND SSC.DateOfChange = (
                                                                             SELECT MAX(DateOfChange)
                                                                             FROM   syStudentStatusChanges SSC2
                                                                             WHERE  SSC2.StuEnrollId = SE.StuEnrollId
                                                                                    AND DateOfChange < @StartDate
                                                                           )
                                                    AND SE.ReEnrollmentDate BETWEEN @StartDate AND @EndDate
                                                  )
                                                  OR
            --This code is to catch the scenario where the student was in a drop status before the start of the period
            --and the first entry recorded in the status changes table is when the student is changed from drop back to
            --currently attending. Example student is Portia Harper in Coyne when report is run for 7/1/2010 to 6/30/2011. 
                                                  (
                                                    SSC.OrigStatusId = SC.StatusCodeId
                                                    AND SC.SysStatusId = SS.SysStatusId
                                                    AND SS.SysStatusId IN ( 12,19 )
                                                    AND SSC.DateOfChange = (
                                                                             SELECT MIN(DateOfChange)
                                                                             FROM   syStudentStatusChanges SSC2
                                                                             WHERE  SSC2.StuEnrollId = SE.StuEnrollId
                                                                                    AND SSC.DateOfChange > @StartDate
                                                                           )
                                                    AND SE.ReEnrollmentDate BETWEEN @StartDate AND @EndDate
                                                  )
                                                )
                                ) AS ReEntry
                               , 

---- Return From LOA ---
                                0 AS ReturnFromLOA
                               ,
--0 as ReturnFromLOA,

----- LOA --------
                                0 AS LOA
                               ,

----- Incomplete --------
                                0 AS Incomplete
                               ,

---- Dropped ----
---- The student must be in a drop status at the end of the reporing period. This is important as we don't want to count
---- a drop where the student then reenrolled after that in the same reporting period. 
---- As with the BE we have to take account of the DateDetermined field. This is to prevent the situation where say the ModDate
---- on the Drop record is 7/5/2010 but the DateDetermined is 6/15/2010.
                                (
                                  SELECT    COUNT(DISTINCT StuEnrollId)
                                  FROM      syStudentStatusChanges SSC
                                           ,syStatusCodes SC
                                           ,sySysStatus SS
                                  WHERE     SSC.StuEnrollId = SE.StuEnrollId
                                            AND (
                                                  --This part is for where the student is in a drop status at the end of the reporting period striclty based off
                     --the records in the syStudentStatusChanges table.
                                                  (
                                                    SSC.NewStatusId = SC.StatusCodeId
                                                    AND SC.SysStatusId = SS.SysStatusId
                                                    AND SS.SysStatusId = 12
                                                    AND SSC.DateOfChange = (
                                                                             SELECT MAX(DateOfChange)
                                                                             FROM   syStudentStatusChanges SSC2
                                                                             WHERE  SSC2.StuEnrollId = SE.StuEnrollId
                                                                                    AND DateOfChange BETWEEN @StartDate AND @EndDate
                                                                           )
                                                  )
                                                  OR
                                   
                    --This part is for where the student is changed to drop status shortly after the end of the reporting period
                    --but the DateDetermined is entered for a date during the reporting period. For example, if the reporting
                    --period is 7/1/2009 to 6/30/2010 and the student is dropped on 7/15/2010 but the DateDetermined is entered as
                    --6/15/2010. In this case the ModDate in the student status changes table will be 7/15/2010 so we would not have
                    --picked up the student with the first query. The logic here is to check if the first status change after the
                    --reporting period is to drop and the date determined is between the report period. 
                                                  (
                                                    SSC.NewStatusId = SC.StatusCodeId
                                                    AND SC.SysStatusId = SS.SysStatusId
                                                    AND SS.SysStatusId = 12
                                                    AND SSC.DateOfChange = (
                                                                             SELECT MIN(DateOfChange)
                                                                             FROM   syStudentStatusChanges SSC2
                                                                             WHERE  SSC2.StuEnrollId = SE.StuEnrollId
                                                                                    AND DateOfChange > @EndDate
                                                                           )
                                                    AND SE.DateDetermined BETWEEN @StartDate AND @EndDate
                                                  )
                                                )
                                ) AS Droped
                               ,
    

---- Graduated ---
---- For this we simply want students who are in a graduated status and who graduated during the report period.
                                CASE WHEN ( (
                                              SELECT    COUNT(*)
                                              FROM      arStuEnrollments SEI
                                                       ,syStatusCodes SCI
                                              WHERE     SE.StuEnrollId = SEI.StuEnrollId
                                                        AND SEI.StatusCodeId = SCI.StatusCodeId
                                                        AND SCI.SysStatusId = 14
                                                        AND SEI.ExpGradDate BETWEEN @StartDate AND @EndDate
                                            ) >= 1 ) THEN 1
                                     ELSE 0
                                END AS Graduated
                               ,

---- Complete ---
                                0 AS Complete
                               ,

---- Transfer In ---
                                (
                                  SELECT    COUNT(DISTINCT SEI.StuEnrollId)
                                  FROM      arStuEnrollments SEI
                                           ,arTrackTransfer TR
                                           ,syStatusCodes SCI
                                  WHERE     SE.StudentId = SEI.StudentId
                                            AND TR.StuEnrollId = SEI.StuEnrollId 
 --And (SE.PrgVerId=TR.TargetPrgVerId or SE.CampusId=TR.TargetCampusId)
                                            AND ( SE.PrgVerId = TR.TargetPrgVerId )
                                            AND SE.StatusCodeId = SCI.StatusCodeId
                                            AND SCI.SysStatusId NOT IN ( 19 ) 
 --The Transfer has to be between the report period
                                            AND TR.ModDate BETWEEN @StartDate AND @EndDate
                                ) AS TransferIn
                               ,

---- Transfer out ---
--The student must be in Transfer Out status at the end of the reporting period. We don't want to get students who Transfer Out
--and then reenrolled.
                                (
                                  SELECT    COUNT(DISTINCT StuEnrollId)
                                  FROM      syStudentStatusChanges SSC
                                           ,syStatusCodes SC
                                           ,sySysStatus SS
                                  WHERE     SSC.StuEnrollId = SE.StuEnrollId
                                            AND SSC.NewStatusId = SC.StatusCodeId
                                            AND SC.SysStatusId = SS.SysStatusId
                                            AND SS.SysStatusId = 19
                                            AND SSC.DateOfChange = (
                                                                     SELECT MAX(DateOfChange)
                                                                     FROM   syStudentStatusChanges SSC2
                                                                     WHERE  SSC2.StuEnrollId = SE.StuEnrollId
                                                                            AND DateOfChange BETWEEN @StartDate AND @EndDate
                                                                   ) 
                           
        --We need to filter out any students who had transferred out and then re-enrolled before the end of the reporting period.
        --The reenrollment might actually be recorded at a later date than the reenrollment date stored on the enrollment. 
        --Example Student: Alexis Hayden. Cynthia reenrolled the student but set the reenrollment date for an earlier date than the
        --current date. The status changes table uses the ModDate, the current date, but the enrollment record is storing the reenrollment
        --date. The result was that the student still appeared under Transfer Out even though she had reenrolled before the end of the
        --reporitng period.
                                            AND NOT EXISTS ( SELECT StuEnrollId
                                                             FROM   arStuEnrollments SE2
                                                             WHERE  SE2.StuEnrollId = SE.StuEnrollId
            --We want reenrollments that are after the transfer out date
            --but up to the end of the reporting period.
                                                                    AND SE2.ReEnrollmentDate IS NOT NULL
                                                                    AND SE2.ReEnrollmentDate > SSC.DateOfChange
                                                                    AND SE2.ReEnrollmentDate <= @EndDate )
                                ) AS TransferOut
                               ,
 
---- Ending Enrollment ---
--The student must be in an in-school status at the end of the reporting period.
--For some statuses we have a start and end date: Academic Probation, LOA, Susupended, Suspension
--For some statuses we have to use the ModDate: Currently Attending, Externship, Future Start
--We also have to handle the scenario where the student is in a drop/transfer out status at the end of the reporting period
--based on the status changes table but reenrolled before the end of the reporting period. Remember that the status change table
--will store the actual date the student was reenrolled but the reenrollment date entered on the enrollment could be before the 
--end of the reporting period.
                                (
                                  SELECT    COUNT(DISTINCT StuEnrollId)
                                  FROM      syStudentStatusChanges SSC
                                           ,syStatusCodes SC
                                           ,sySysStatus SS
                                  WHERE     SSC.StuEnrollId = SE.StuEnrollId
                                            AND SSC.NewStatusId = SC.StatusCodeId
                                            AND SC.SysStatusId = SS.SysStatusId
                                            AND (
                                                  --This section handles the scenario where the student is in a currently attending status at the end of the
                  --reporing period based on the status changes table.
                                                  (
                                                    SS.InSchool = 1
                                                    AND SSC.DateOfChange = (
                                                                             SELECT MAX(DateOfChange)
                                                                             FROM   syStudentStatusChanges SSC2
                                                                             WHERE  SSC2.StuEnrollId = SE.StuEnrollId
                                                                                    AND DateOfChange <= @EndDate
                                                                           ) 
                                   
                 --There are records where the student was changed from future start to currently attending and then transferred out.
                 --The problem is that the records in the student status changes table have the same ModDate for these changes. 
                 --Example student is Stacey Moore in the Coyne db when the report is run for 7/1/2010 to 6/30/2011. 
                 --She was made currently attending on 2010-05-24 11:51:00.000 and was then transferred to another school with the same
                 --2010-05-24 11:51:00.000. This student should not show up in the BE for 7/1/2010. However, she was showing up as 
                 --the currenlty attending status had the max date.
                                                    AND NOT EXISTS ( SELECT NewStatusId
                                                                     FROM   syStudentStatusChanges SSC5
                                                                           ,syStatusCodes SC5
                                                                           ,sySysStatus SS5
                                                                     WHERE  SSC5.StuEnrollId = SE.StuEnrollId
                                                                            AND SSC5.NewStatusId = SC5.StatusCodeId
                                                                            AND SC5.SysStatusId = SS5.SysStatusId
                                                                            AND SS5.InSchool = 0
                                                                            AND SSC5.DateOfChange = (
                                                                                                      SELECT    MAX(DateOfChange)
                                                                                                      FROM      syStudentStatusChanges SSC6
                                                                                                      WHERE     SSC6.StuEnrollId = SE.StuEnrollId
                                                                                                                AND DateOfChange < @EndDate
                                                                                                    ) )
                                   
                                   
                --We want to filter out any drop students where the DateDetermined is before the ModDate on the status change.
                 --For eg. the student was changed to a drop on 7/5/2010 with a date determined of 6/15/2010.
                                                    AND NOT EXISTS ( SELECT StuEnrollId
                                                                     FROM   arStuEnrollments SE2
                                                                     WHERE  SE2.StuEnrollId = SE.StuEnrollId
                                                                            AND SE2.DateDetermined BETWEEN @StartDate AND @EndDate
                                                                            AND (
                                                                                  SE2.ReEnrollmentDate IS NULL
                                                                                  OR SE2.ReEnrollmentDate > @EndDate
                                                                                ) ) 
                        
                --We want to filter out any students who might have grad but the status change was not recorded until after the reporting period.
                --Example student is Ivy Smith in the Coyne DB when the report is run for 2/2/2010 to 10/25/2010. The student graduated on 10/20/2010
                --but the status change was not recorded until 10/28/2010. The student was still appearing in the Ending Enrollment but she should not
                --since she graduated on 10/20/2010.
                                                    AND NOT EXISTS ( SELECT StuEnrollId
                                                                     FROM   arStuEnrollments SE2
                                                                           ,syStatusCodes SC2
                                                                     WHERE  SE2.StuEnrollId = SE.StuEnrollId
                                                                            AND SE2.StatusCodeId = SC2.StatusCodeId
                                                                            AND SC2.SysStatusId = 14
                                                                            AND SE2.ExpGradDate BETWEEN @StartDate AND @EndDate )
                                                  )
                                                  OR
                                    
                --This section handles the scenario where the student is in a drop/transfer out status at the end of the reporting period
                --based on the status changes table but actually reenrolled before the end of the reporitng period.
                                                  (
                                                    SS.SysStatusId IN ( 12,19 )
                                                    AND SSC.DateOfChange = (
                                                                             SELECT MAX(DateOfChange)
                                                                             FROM   syStudentStatusChanges SSC2
                                                                             WHERE  SSC2.StuEnrollId = SE.StuEnrollId
                                                                                    AND DateOfChange <= @EndDate
                                                                           )
                                                    AND SE.ReEnrollmentDate > SSC.DateOfChange
                                                    AND SE.ReEnrollmentDate <= @EndDate
                                                  )
                                                )
                                ) AS EndingEnrollment
                               ,(
                                  SELECT    CampDescrip
                                  FROM      syCampuses cm
                                  WHERE     cm.CampusId = SE.CampusId
                                ) AS CampDescrip
                               ,(
                                  SELECT    CG.CampGrpDescrip
                                  FROM      syCmpGrpCmps CGC
                                           ,syCampGrps CG
                                  WHERE     CGC.CampGrpId = CG.CampGrpId
                                            AND CGC.CampusId = SE.CampusId
                                            AND CGC.CampGrpId IN ( SELECT   strval
                                                                   FROM     dbo.SPLIT(@CampGrpId) )
                                ) AS CampGrpDescrip
                        FROM    arStudent S
                               ,arStuEnrollments SE
                               ,syStatusCodes SC
                               ,arPrgVersions PV
                               ,adGenders G
                               ,adEthCodes EC
                        WHERE   S.StudentId = SE.StudentId
                                AND SE.StatusCodeId = SC.StatusCodeId
                                AND SE.PrgVerId = PV.PrgVerId
                                AND S.Gender = G.GenderId
                                AND S.Race = EC.EthCodeId
                                AND (
                                      SE.StartDate <= @EndDate
                                      AND SE.ExpGradDate >= @StartDate
                                    )
--And SC.SysStatusId in (9,10,12,14,19,22,7)
--We need to filter out any student with a No-Start or Future Start status at the end of the report period.
--We have to do this as a student might first be changed from future to currently attending and then back to
--future start or even a no-start. In those scenarios the student should not be included in the new starts.
                                AND NOT EXISTS ( SELECT StuEnrollId
                                                 FROM   syStudentStatusChanges SSC
                                                       ,syStatusCodes SC
                                                       ,sySysStatus SS
                                                 WHERE  SSC.StuEnrollId = SE.StuEnrollId
                                                        AND SSC.NewStatusId = SC.StatusCodeId
                                                        AND SC.SysStatusId = SS.SysStatusId
                                                        AND SS.SysStatusId IN ( 7,8 )
                                                        AND SSC.DateOfChange = (
                                                                                 SELECT MAX(DateOfChange)
                                                                                 FROM   syStudentStatusChanges SSC2
                                                                                 WHERE  SSC2.StuEnrollId = SE.StuEnrollId
                                                                                        AND DateOfChange <= @EndDate
                                                                               ) )
--We don't want to get students who dropped before the start of the report period and never reenrolled
--or reenrolled after the end of the report period but whose expected grad date cuts over the report period.
                                AND NOT EXISTS ( SELECT StuEnrollId
                                                 FROM   arStuEnrollments se3
                                                 WHERE  se3.StuEnrollId = SE.StuEnrollId
                                                        AND (
                                                              (
                                                                se3.DateDetermined < @StartDate
                                                                AND se3.ReEnrollmentDate IS NULL
                                                              )
                                                              OR (
                                                                   se3.DateDetermined < @StartDate
                                                                   AND se3.ReEnrollmentDate > @EndDate
                                                                 )
--                                    or
--            (se3.LDA < @StartDate and se3.DateDetermined is null)
                                                              OR (
                                                                   se3.DateDetermined < @StartDate
                                                                   AND se3.ReEnrollmentDate < se3.DateDetermined
                                                                 )
                                                            ) )

--We don't want to get students who transferred out before the start of the report period and never reenrolled
                                AND NOT EXISTS ( SELECT StuEnrollId
                                                 FROM   syStudentStatusChanges SSC
                                                       ,syStatusCodes SC
                                                       ,sySysStatus SS
                                                 WHERE  SSC.StuEnrollId = SE.StuEnrollId
                                                        AND SSC.NewStatusId = SC.StatusCodeId
                                                        AND SC.SysStatusId = SS.SysStatusId
                                                        AND SS.SysStatusId = 19
                                                        AND SSC.DateOfChange = (
                                                                                 SELECT MAX(DateOfChange)
                                                                                 FROM   syStudentStatusChanges SSC2
                                                                                 WHERE  SSC2.StuEnrollId = SE.StuEnrollId
                                                                                        AND DateOfChange <= @StartDate
                                                                               )
                                                        AND (
                                                              SE.ReEnrollmentDate IS NULL
                                                              OR SE.ReEnrollmentDate > @EndDate
                                                            ) )


--Since we are relying on the syStudentStatusChanges to track certain changes we need to make sure that the enrollment has
--records in that table.
                                AND EXISTS ( SELECT StuEnrollId
                                             FROM   syStudentStatusChanges ssc
                                             WHERE  ssc.StuEnrollId = SE.StuEnrollId )
                                AND (
                                      @PrgVerId IS NULL
                                      OR SE.PrgVerId IN ( SELECT    strval
                                                          FROM      SPLIT(@PrgVerId) )
                                    )
                                AND SE.CampusId IN ( SELECT DISTINCT
                                                            CGC.CampusId
                                                     FROM   syCmpGrpCmps CGC
                                                     WHERE  CGC.CampGrpId IN ( SELECT   strval
                                                                               FROM     dbo.SPLIT(@CampGrpId) ) )
                        ORDER BY LastName;
            END;


        ELSE
            BEGIN
                INSERT  INTO #TotalByProgramVersion
                        SELECT  S.FirstName + ' ' + S.LastName + ' ' + ( ISNULL(S.MiddleName,'') ) AS StudentName
                               ,SE.PrgVerId
                               ,PV.PrgVerDescrip
                               ,PV.PrgVerCode
                               ,G.GenderDescrip
                               ,EC.EthCodeDescrip
                               ,SE.StuEnrollId
                               ,
---- Beginning enrollment ----
---- Same logic as above except that we also need to filter out any student who was on LOA at the start of the period.
                                (
                                  SELECT    COUNT(DISTINCT StuEnrollId)
                                  FROM      syStudentStatusChanges SSC
                                           ,syStatusCodes SC
                                           ,sySysStatus SS
                                  WHERE     SSC.StuEnrollId = SE.StuEnrollId
          --The following line was commented out as there are students made currently attending before the
          --start date. Example: Akintunde Azeez in the Coyne DB has a start date of 7/12/2010 but he was made
          --currently attending on 6/8/2010.
          --and SE.StartDate < @StartDate
                                            AND (
                                                  (
                                                    SSC.NewStatusId = SC.StatusCodeId
                                                    AND SC.SysStatusId = SS.SysStatusId
                                                    AND SS.InSchool = 1
                                                    AND SS.SysStatusId <> 7	--This is to exclude Future Starts
                                                    AND SSC.DateOfChange = (
                                                                             SELECT MAX(DateOfChange)
                                                                             FROM   syStudentStatusChanges SSC2
                                                                             WHERE  SSC2.StuEnrollId = SE.StuEnrollId
                                                                                    AND DateOfChange < @StartDate
                                                                           )
                                                  )
                                                  OR
                --This section is for the scenario where the student was in a currently attending status but the first 
                --status change was recorded after the start of the reporting period. Example student is Candice Dashmon
                --in Coyne when report is run for 7/1/2010 to 6/30/2011
                                                  (
                                                    SSC.OrigStatusId = SC.StatusCodeId
                                                    AND SC.SysStatusId = SS.SysStatusId
                                                    AND SS.SysStatusId = 9
                                                    AND SSC.DateOfChange = (
                                                                             SELECT MIN(DateOfChange)
                                                                             FROM   syStudentStatusChanges SSC2
                                                                             WHERE  SSC2.StuEnrollId = SE.StuEnrollId
                                                                                    AND DateOfChange > @StartDate
                                                                           )
                                                  )
                                                  OR
                                   
                --This section is for the scenario where student was in a drop status but the first status change was 
                --recorded after the start of the reporting period. Example Student is Portia Harper in Coyne. The first
                --status change recorded is from drop out to currently attending. This change was recorded on 2010-10-19 18:24:31.000.
                --The problem is that the Reenrollment date was on 10/18/2010. If the report is run for 10/19/2010 the student should
                --show up in the BE since she reenrolled on 10/18/2010. 
                                                  (
                                                    SSC.OrigStatusId = SC.StatusCodeId
                                                    AND SC.SysStatusId = SS.SysStatusId
                                                    AND SS.SysStatusId IN ( 12,19 )
                                                    AND SSC.DateOfChange = (
                                                                             SELECT MIN(DateOfChange)
                                                                             FROM   syStudentStatusChanges SSC2
                                                                             WHERE  SSC2.StuEnrollId = SE.StuEnrollId
                                                                                    AND DateOfChange >= @StartDate
                                                                           )
                                                    AND SE.ReEnrollmentDate < @StartDate
                                                  )
                                                )
                                            AND (
                                                  SE.DateDetermined IS NULL
                                                  OR (
                                                       SE.DateDetermined < @StartDate
                                                       AND SE.ReEnrollmentDate < @StartDate
                                                     )
                                                  OR ( SE.DateDetermined > @StartDate )
                                                )
            
        --We need to filter out any students who were on LOA at the start of the period
                                            AND NOT EXISTS ( SELECT StuEnrollId
                                                             FROM   arStudentLOAs l
                                                             WHERE  l.StuEnrollId = SE.StuEnrollId
                                                                    AND l.StartDate <= @StartDate
                                                                    AND l.EndDate >= @StartDate
                                                                    AND (
                                                                          l.LOAReturnDate IS NULL
                                                                          OR l.LOAReturnDate > @StartDate
                                                                        ) )
                                ) AS BeginningEnrollment
                               , 

---- New Starts 
--We need to get students who were were expected to start during the report period.
--We are filtering out the no-starts in the where clause so we don't need to worry about them here.
--(select COUNT(distinct StuEnrollId)
-- from arStuEnrollments SE1
-- where SE1.StuEnrollId=SE.StuEnrollId
-- and SE1.StartDate between @StartDate and @EndDate 
 
--) as NewStarts , 
--Logic was changed because there are students with start dates that are long before the date they were made currently attending
--in the status change table. Example: Christian Ray in the Coyne DB. For the Electrical Construction & Planning (N) he has 
--start date of 3/29/2010. However, he was not made currently attending until 8/9/2010. When the report was run for 
--7/1/2010 to 6/30/2011 the student did not show up in either the BE field or the New Start field.
--The new logic is that the student must be placed in currently attedning or future start status during the report period.
                                (
                                  SELECT    COUNT(DISTINCT StuEnrollId)
                                  FROM      syStudentStatusChanges ssc
                                           ,syStatusCodes sc
                                  WHERE     ssc.StuEnrollId = SE.StuEnrollId
                                            AND ssc.NewStatusId = sc.StatusCodeId
                                            AND sc.SysStatusId = 9
                                            AND ssc.DateOfChange BETWEEN @StartDate AND @EndDate
   --The change to future start/currently attending has to be the first for the enrollment. This is important
 --because a student could have been in the BE and then dropped and made currently attending during the reporting period.
 --Example student is Brittany Splett A in the Coyne DB for the MA 650 (Course) (Nights) program.
 --She was made currently attending on 3/5/2010. When the report was run for 3/10/2010 to 3/9/2011 she appeared in both the
 --BE and New Starts. She was appearing in the New Starts because she was dropped a couple of times and made currently 
 --attending during the reporting period. She was being picked up as New Start because she was made currently attending.
                                            AND ssc.DateOfChange = (
                                                                     SELECT MIN(ssc2.DateOfChange)
                                                                     FROM   syStudentStatusChanges ssc2
                                                                           ,syStatusCodes sc2
                                                                     WHERE  ssc2.StuEnrollId = SE.StuEnrollId
                                                                            AND ssc2.NewStatusId = sc2.StatusCodeId
                                                                            AND sc2.SysStatusId = 9	
    --and ssc2.ModDate between @StartDate and @EndDate
                                                                   )
                                ) AS NewStarts
                               ,

---- RE-Entry ---
---- RE-Entry ---
---- The student needs to have been in a drop status at the start of the report period and then
---- reenrolled during the report period.
                                (
                                  SELECT    COUNT(DISTINCT StuEnrollId)
                                  FROM      syStudentStatusChanges SSC
                                           ,syStatusCodes SC
                                           ,sySysStatus SS
                                  WHERE     SSC.StuEnrollId = SE.StuEnrollId
                                            AND (
                                                  (
                                                    SSC.NewStatusId = SC.StatusCodeId
                                                    AND SC.SysStatusId = SS.SysStatusId
                                                    AND SS.SysStatusId IN ( 12,19 )
                                                    AND SSC.DateOfChange = (
                                                                             SELECT MAX(DateOfChange)
                                                                             FROM   syStudentStatusChanges SSC2
                                                                             WHERE  SSC2.StuEnrollId = SE.StuEnrollId
                                                                                    AND DateOfChange < @StartDate
                                                                           )
                                                    AND SE.ReEnrollmentDate BETWEEN @StartDate AND @EndDate
                                                  )
                                                  OR
            --This code is to catch the scenario where the student was in a drop status before the start of the period
            --and the first entry recorded in the status changes table is when the student is changed from drop back to
            --currently attending. Example student is Portia Harper in Coyne when report is run for 7/1/2010 to 6/30/2011. 
                                                  (
                                                    SSC.OrigStatusId = SC.StatusCodeId
                                                    AND SC.SysStatusId = SS.SysStatusId
                                                    AND SS.SysStatusId = 12
                                                    AND SSC.DateOfChange = (
                                                                             SELECT MIN(DateOfChange)
                                                                             FROM   syStudentStatusChanges SSC2
                                                                             WHERE  SSC2.StuEnrollId = SE.StuEnrollId
                                                                                    AND SSC.DateOfChange > @StartDate
                                                                           )
                                                    AND SE.ReEnrollmentDate BETWEEN @StartDate AND @EndDate
                                                  )
                                                )
                                ) AS ReEntry
                               , 

---- Return From LOA ---
--Return from LOA means the student was on LOA at the start of the reporting period and returned during the period.
                                (
                                  SELECT    COUNT(DISTINCT StuEnrollId)
                                  FROM      arStudentLOAs l
                                  WHERE     l.StuEnrollId = SE.StuEnrollId
                                            AND l.LOAReturnDate BETWEEN @StartDate AND @EndDate
                                            AND @StartDate BETWEEN l.StartDate AND l.EndDate
                                ) AS ReturnFromLOA
                               ,
--0 as ReturnFromLOA,

----- LOA --------
-----The student must be on LOA at the end of the reporting period.
                                (
                                  SELECT    COUNT(DISTINCT StuEnrollId)
                                  FROM      arStudentLOAs l
                                  WHERE     l.StuEnrollId = SE.StuEnrollId
                                            AND l.StartDate <= @EndDate
                                            AND l.EndDate >= @EndDate
                                            AND (
                                                  l.LOAReturnDate > @EndDate
                                                  OR l.LOAReturnDate IS NULL
                                                )
                                ) AS LOA
                               ,

----- Incomplete --------
                                0 AS Incomplete
                               ,

---- Dropped ----
---- The student must be in a drop status at the end of the reporing period. This is important as we don't want to count
---- a drop where the student then reenrolled after that in the same reporting period. 
---- As with the BE we have to take account of the DateDetermined field. This is to prevent the situation where say the ModDate
---- on the Drop record is 7/5/2010 but the DateDetermined is 6/15/2010.
                                (
                                  SELECT    COUNT(DISTINCT StuEnrollId)
                                  FROM      syStudentStatusChanges SSC
                                           ,syStatusCodes SC
                                           ,sySysStatus SS
                                  WHERE     SSC.StuEnrollId = SE.StuEnrollId
                                            AND (
                                                  --This part is for where the student is in a drop status at the end of the reporting period striclty based off
                     --the records in the syStudentStatusChanges table.
                                                  (
                                                    SSC.NewStatusId = SC.StatusCodeId
                                                    AND SC.SysStatusId = SS.SysStatusId
                                                    AND SS.SysStatusId = 12
                                                    AND SSC.DateOfChange = (
                                                                             SELECT MAX(DateOfChange)
                                                                             FROM   syStudentStatusChanges SSC2
                                                                             WHERE  SSC2.StuEnrollId = SE.StuEnrollId
                                                                                    AND DateOfChange BETWEEN @StartDate AND @EndDate
                                                                           )
                                                  )
                                                  OR
                                   
                    --This part is for where the student is changed to drop status shortly after the end of the reporting period
                    --but the DateDetermined is entered for a date during the reporting period. For example, if the reporting
                    --period is 7/1/2009 to 6/30/2010 and the student is dropped on 7/15/2010 but the DateDetermined is entered as
                    --6/15/2010. In this case the ModDate in the student status changes table will be 7/15/2010 so we would not have
                    --picked up the student with the first query. The logic here is to check if the first status change after the
                    --reporting period is to drop and the date determined is between the report period. 
                                                  (
                                                    SSC.NewStatusId = SC.StatusCodeId
                                                    AND SC.SysStatusId = SS.SysStatusId
                                                    AND SS.SysStatusId = 12
                                                    AND SSC.DateOfChange = (
                                                                             SELECT MIN(DateOfChange)
                                                                             FROM   syStudentStatusChanges SSC2
                                                                             WHERE  SSC2.StuEnrollId = SE.StuEnrollId
                                                                                    AND DateOfChange > @EndDate
                                                                           )
                                                    AND SE.DateDetermined BETWEEN @StartDate AND @EndDate
                                                  )
                                                )
                                ) AS Droped
                               ,

---- Graduated ---
---- For this we simply want students who are in a graduated status and who graduated during the report period.
                                CASE WHEN ( (
                                              SELECT    COUNT(*)
                                              FROM      arStuEnrollments SEI
                                                       ,syStatusCodes SCI
                                              WHERE     SE.StuEnrollId = SEI.StuEnrollId
                                                        AND SEI.StatusCodeId = SCI.StatusCodeId
                                                        AND SCI.SysStatusId = 14
                                                        AND SEI.ExpGradDate BETWEEN @StartDate AND @EndDate
                                            ) >= 1 ) THEN 1
                                     ELSE 0
                                END AS Graduated
                               ,

---- Complete ---
                                0 AS Complete
                               ,

---- Transfer In ---
                                (
                                  SELECT    COUNT(DISTINCT SEI.StuEnrollId)
                                  FROM      arStuEnrollments SEI
                                           ,arTrackTransfer TR
                                           ,syStatusCodes SCI
                                  WHERE     SE.StudentId = SEI.StudentId
                                            AND TR.StuEnrollId = SEI.StuEnrollId 
 --And (SE.PrgVerId=TR.TargetPrgVerId or SE.CampusId=TR.TargetCampusId)
                                            AND ( SE.PrgVerId = TR.TargetPrgVerId )
                                            AND SE.StatusCodeId = SCI.StatusCodeId
                                            AND SCI.SysStatusId NOT IN ( 19 ) 
 --The Transfer has to be between the report period
                                            AND TR.ModDate BETWEEN @StartDate AND @EndDate
                                ) AS TransferIn
                               ,

---- Transfer out ---
--The student must be in Transfer Out status at the end of the reporting period. We don't want to get students who Transfer Out
--and then reenrolled.
                                (
                                  SELECT    COUNT(DISTINCT StuEnrollId)
                                  FROM      syStudentStatusChanges SSC
                                           ,syStatusCodes SC
                                           ,sySysStatus SS
                                  WHERE     SSC.StuEnrollId = SE.StuEnrollId
                                            AND SSC.NewStatusId = SC.StatusCodeId
                                            AND SC.SysStatusId = SS.SysStatusId
                                            AND SS.SysStatusId = 19
                                            AND SSC.DateOfChange = (
                                                                     SELECT MAX(DateOfChange)
                                                                     FROM   syStudentStatusChanges SSC2
                                                                     WHERE  SSC2.StuEnrollId = SE.StuEnrollId
                                                                            AND DateOfChange BETWEEN @StartDate AND @EndDate
                                                                   ) 
                            
        --We need to filter out any students who had transferred out and then re-enrolled before the end of the reporting period.
        --The reenrollment might actually be recorded at a later date than the reenrollment date stored on the enrollment. 
        --Example Student: Alexis Hayden. Cynthia reenrolled the student but set the reenrollment date for an earlier date than the
        --current date. The status changes table uses the ModDate, the current date, but the enrollment record is storing the reenrollment
        --date. The result was that the student still appeared under Transfer Out even though she had reenrolled before the end of the
        --reporitng period.
                                            AND NOT EXISTS ( SELECT StuEnrollId
                                                             FROM   arStuEnrollments SE2
                                                             WHERE  SE2.StuEnrollId = SE.StuEnrollId
            --We want reenrollments that are after the transfer out date
            --but up to the end of the reporting period.
                                                                    AND SE2.ReEnrollmentDate > SSC.DateOfChange
                                                                    AND SE2.ReEnrollmentDate <= @EndDate )
                                ) AS TransferOut
                               ,
 
---- Ending Enrollment ---
--The student must be in an in-school status at the end of the reporting period.
--For some statuses we have a start and end date: Academic Probation, LOA, Susupended, Suspension
--For drops we have to use the date determined.
--For some statuses we have to use the ModDate: Currently Attending, Externship, Future Start
--We also have to handle the scenario where the student is in a drop/transfer out status at the end of the reporting period
--based on the status changes table but reenrolled before the end of the reporting period. Remember that the status change table
--will store the actual date the student was reenrolled but the reenrollment date entered on the enrollment could be before the 
--end of the reporting period.
                                (
                                  SELECT    COUNT(DISTINCT StuEnrollId)
                                  FROM      syStudentStatusChanges SSC
                                           ,syStatusCodes SC
                                           ,sySysStatus SS
                                  WHERE     SSC.StuEnrollId = SE.StuEnrollId
                                            AND SSC.NewStatusId = SC.StatusCodeId
                                            AND SC.SysStatusId = SS.SysStatusId
                                            AND (
                                                  --This section handles the scenario where the student is in a currently attending status at the end of the
                  --reporing period based on the status changes table.
                                                  (
                                                    SS.InSchool = 1
                                                    AND SSC.DateOfChange = (
                                                                             SELECT MAX(DateOfChange)
                                                                             FROM   syStudentStatusChanges SSC2
                                                                             WHERE  SSC2.StuEnrollId = SE.StuEnrollId
                                                                                    AND DateOfChange <= @EndDate
                                                                           ) 
                                   
                 --There are records where the student was changed from future start to currently attending and then transferred out.
                 --The problem is that the records in the student status changes table have the same ModDate for these changes. 
                 --Example student is Stacey Moore in the Coyne db when the report is run for 7/1/2010 to 6/30/2011. 
                 --She was made currently attending on 2010-05-24 11:51:00.000 and was then transferred to another school with the same
                 --2010-05-24 11:51:00.000. This student should not show up in the BE for 7/1/2010. However, she was showing up as 
                 --the currenlty attending status had the max date.
                                                    AND NOT EXISTS ( SELECT NewStatusId
                                                                     FROM   syStudentStatusChanges SSC5
                                                                           ,syStatusCodes SC5
                                                                           ,sySysStatus SS5
                                                                     WHERE  SSC5.StuEnrollId = SE.StuEnrollId
                                                                            AND SSC5.NewStatusId = SC5.StatusCodeId
                                                                            AND SC5.SysStatusId = SS5.SysStatusId
                                                                            AND SS5.InSchool = 0
                                                                            AND SSC5.DateOfChange = (
                                                                                                      SELECT    MAX(DateOfChange)
                                                                                                      FROM      syStudentStatusChanges SSC6
                                                                                                      WHERE     SSC6.StuEnrollId = SE.StuEnrollId
                                                                                                                AND DateOfChange < @EndDate
                                                                                                    ) )
                                   
                                   
                --We want to filter out any drop students where the DateDetermined is before the ModDate on the status change.
                 --For eg. the student was changed to a drop on 7/5/2010 with a date determined of 6/15/2010.
                                                    AND NOT EXISTS ( SELECT StuEnrollId
                                                                     FROM   arStuEnrollments SE2
                                                                     WHERE  SE2.StuEnrollId = SE.StuEnrollId
                                                                            AND SE2.DateDetermined BETWEEN @StartDate AND @EndDate
                                                                            AND (
                                                                                  SE2.ReEnrollmentDate IS NULL
                                                                                  OR SE2.ReEnrollmentDate > @EndDate
                                                                                ) ) 
                        
                --We want to filter out any students who might have grad but the status change was not recorded until after the reporting period.
                --Example student is Ivy Smith in the Coyne DB when the report is run for 2/2/2010 to 10/25/2010. The student graduated on 10/20/2010
                --but the status change was not recorded until 10/28/2010. The student was still appearing in the Ending Enrollment but she should not
                --since she graduated on 10/20/2010.
                                                    AND NOT EXISTS ( SELECT StuEnrollId
                                                                     FROM   arStuEnrollments SE2
                                                                           ,syStatusCodes SC2
                                                                     WHERE  SE2.StuEnrollId = SE.StuEnrollId
                                                                            AND SE2.StatusCodeId = SC2.StatusCodeId
                                                                            AND SC2.SysStatusId = 14
                                                                            AND SE2.ExpGradDate BETWEEN @StartDate AND @EndDate )
                                                  )
                                                  OR
                                    
                --This section handles the scenario where the student is in a drop/transfer out status at the end of the reporting period
                --based on the status changes table but actually reenrolled before the end of the reporitng period.
                                                  (
                                                    SS.SysStatusId IN ( 12,19 )
                                                    AND SSC.DateOfChange = (
                                                                             SELECT MAX(DateOfChange)
                                                                             FROM   syStudentStatusChanges SSC2
                                                                             WHERE  SSC2.StuEnrollId = SE.StuEnrollId
                                                                                    AND DateOfChange <= @EndDate
                                                                           )
                                                    AND SE.ReEnrollmentDate > SSC.DateOfChange
                                                    AND SE.ReEnrollmentDate <= @EndDate
                                                  )
                                                ) 
         
         --We want to filter out any students who are on LOA at the end of the reporting period but the LOA start date
         --is before the ModDate on the status change. 
         --For eg. the student was changed to LOA on 7/5/2010 with a LOA start date of 6/28/2010.
                                            AND NOT EXISTS ( SELECT StuEnrollId
                                                             FROM   arStudentLOAs l
                                                             WHERE  l.StuEnrollId = SE.StuEnrollId
                                                                    AND l.StartDate <= @EndDate
                                                                    --AND l.EndDate >= @EndDate
                                                                    AND (
                                                                          l.LOAReturnDate > @EndDate
                                                                          OR l.LOAReturnDate IS NULL
                                                                        ) )
                                ) AS EndingEnrollment
                               ,(
                                  SELECT    CampDescrip
                                  FROM      syCampuses cm
                                  WHERE     cm.CampusId = SE.CampusId
                                ) AS CampDescrip
                               ,(
                                  SELECT    CG.CampGrpDescrip
                                  FROM      syCmpGrpCmps CGC
                                           ,syCampGrps CG
                                  WHERE     CGC.CampGrpId = CG.CampGrpId
                                            AND CGC.CampusId = SE.CampusId
                                            AND CGC.CampGrpId IN ( SELECT   strval
                                                                   FROM     dbo.SPLIT(@CampGrpId) )
                                ) AS CampGrpDescrip
                        FROM    arStudent S
                               ,arStuEnrollments SE
                               ,syStatusCodes SC
                               ,arPrgVersions PV
                               ,adGenders G
                               ,adEthCodes EC
                        WHERE   S.StudentId = SE.StudentId
                                AND SE.StatusCodeId = SC.StatusCodeId
                                AND SE.PrgVerId = PV.PrgVerId
                                AND S.Gender = G.GenderId
                                AND S.Race = EC.EthCodeId
                                AND (
                                      SE.StartDate <= @EndDate
                                      AND SE.ExpGradDate >= @StartDate
                                    )
--And SC.SysStatusId in (9,10,12,14,19,22,7)
--We need to filter out any student with a No-Start or Future Start status at the end of the report period.
--We have to do this as a student might first be changed from future to currently attending and then back to
--future start or even a no-start. In those scenarios the student should not be included in the new starts.
                                AND NOT EXISTS ( SELECT StuEnrollId
                                                 FROM   syStudentStatusChanges SSC
                                                       ,syStatusCodes SC
                                                       ,sySysStatus SS
                                                 WHERE  SSC.StuEnrollId = SE.StuEnrollId
                                                        AND SSC.NewStatusId = SC.StatusCodeId
                                                        AND SC.SysStatusId = SS.SysStatusId
                                                        AND SS.SysStatusId IN ( 7,8 )
                                                        AND SSC.DateOfChange = (
                                                                                 SELECT MAX(DateOfChange)
                                                                                 FROM   syStudentStatusChanges SSC2
                                                                                 WHERE  SSC2.StuEnrollId = SE.StuEnrollId
                                                                                        AND DateOfChange <= @EndDate
                                                                               ) )
--We don't want to get students who dropped before the start of the report period and never reenrolled
--or reenrolled after the end of the report period but whose expected grad date cuts over the report period.
                                AND NOT EXISTS ( SELECT StuEnrollId
                                                 FROM   arStuEnrollments se3
                                                 WHERE  se3.StuEnrollId = SE.StuEnrollId
                                                        AND (
                                                              (
                                                                se3.DateDetermined < @StartDate
                                                                AND se3.ReEnrollmentDate IS NULL
                                                              )
                                                              OR (
                                                                   se3.DateDetermined < @StartDate
                                                                   AND se3.ReEnrollmentDate > @EndDate
                                                                 )
--                                    or
--            (se3.LDA < @StartDate and se3.DateDetermined is null)
                                                              OR (
                                                                   se3.DateDetermined < @StartDate
                                                                   AND se3.ReEnrollmentDate < se3.DateDetermined
                                                                 )
                                                            ) )

--We don't want to get students who transferred out before the start of the report period and never reenrolled
                                AND NOT EXISTS ( SELECT StuEnrollId
                                                 FROM   syStudentStatusChanges SSC
                                                       ,syStatusCodes SC
                                                       ,sySysStatus SS
                                                 WHERE  SSC.StuEnrollId = SE.StuEnrollId
                                                        AND SSC.NewStatusId = SC.StatusCodeId
                                                        AND SC.SysStatusId = SS.SysStatusId
                                                        AND SS.SysStatusId = 19
                                                        AND SSC.DateOfChange = (
                                                                                 SELECT MAX(DateOfChange)
                                                                                 FROM   syStudentStatusChanges SSC2
                                                                                 WHERE  SSC2.StuEnrollId = SE.StuEnrollId
                                                                                        AND DateOfChange <= @StartDate
                                                                               )
                                                        AND (
                                                              SE.ReEnrollmentDate IS NULL
                                                              OR SE.ReEnrollmentDate > @EndDate
                                                            ) )


--Since we are relying on the syStudentStatusChanges to track certain changes we need to make sure that the enrollment has
--records in that table.
                                AND EXISTS ( SELECT StuEnrollId
                                             FROM   syStudentStatusChanges ssc
                                             WHERE  ssc.StuEnrollId = SE.StuEnrollId )
                                AND (
                                      @PrgVerId IS NULL
                                      OR SE.PrgVerId IN ( SELECT    strval
                                                          FROM      SPLIT(@PrgVerId) )
                                    )
                                AND SE.CampusId IN ( SELECT DISTINCT
                                                            CGC.CampusId
                                                     FROM   syCmpGrpCmps CGC
                                                     WHERE  CGC.CampGrpId IN ( SELECT   strval
                                                                               FROM     dbo.SPLIT(@CampGrpId) ) )
                        ORDER BY LastName;
            END;

        IF @IncludeLOA = 1
            BEGIN
                UPDATE  #TotalByProgramVersion
                SET     BeginningEnrollment = 0
                       ,NewStarts = 0
                WHERE   (
                          ReEntry > 0
                          OR ReturnFromLOA > 0
                          OR TransferIn > 0
                        );
            END;
        ELSE
            BEGIN
                UPDATE  #TotalByProgramVersion
                SET     BeginningEnrollment = 0
                       ,NewStarts = 0
                WHERE   (
                          ReEntry > 0
                          OR TransferIn > 0
                        );
            END;

        SELECT  CampGrpDescrip
               ,CampDescrip
               ,StudentName
               ,PrgVerDescrip
               ,PrgVerCode
               ,BeginningEnrollment
               ,NewStarts
               ,ReEntry
               ,ReturnFromLOA
               ,LOA
               ,Incomplete
               ,Droped
               ,Graduated
               ,Complete
               ,TransferIn
               ,TransferOut
               ,EndingEnrollment
               ,(
                  SELECT    SUM(BeginningEnrollment)
                  FROM      #TotalByProgramVersion CBPV
                  WHERE     CBPV.PrgVerId = R.PrgVerId
                ) AS BeginningEnrollmentPV
               ,(
                  SELECT    SUM(NewStarts)
                  FROM      #TotalByProgramVersion CBPV
                  WHERE     CBPV.PrgVerId = R.PrgVerId
                ) AS NewStartsPV
               ,(
                  SELECT    SUM(ReEntry)
                  FROM      #TotalByProgramVersion CBPV
                  WHERE     CBPV.PrgVerId = R.PrgVerId
                ) AS ReEntryPV
               ,(
                  SELECT    SUM(ReturnFromLOA)
                  FROM      #TotalByProgramVersion CBPV
                  WHERE     CBPV.PrgVerId = R.PrgVerId
                ) AS ReturnFromLOAPV
               ,(
                  SELECT    SUM(LOA)
                  FROM      #TotalByProgramVersion CBPV
                  WHERE     CBPV.PrgVerId = R.PrgVerId
                ) AS LOAPV
               ,(
                  SELECT    SUM(Incomplete)
                  FROM      #TotalByProgramVersion CBPV
                  WHERE     CBPV.PrgVerId = R.PrgVerId
                ) AS IncompletePV
               ,(
                  SELECT    SUM(Droped)
                  FROM      #TotalByProgramVersion CBPV
                  WHERE     CBPV.PrgVerId = R.PrgVerId
                ) AS DropedPV
               ,(
                  SELECT    SUM(Graduated)
                  FROM      #TotalByProgramVersion CBPV
                  WHERE     CBPV.PrgVerId = R.PrgVerId
                ) AS GraduatedPV
               ,(
                  SELECT    SUM(Complete)
                  FROM      #TotalByProgramVersion CBPV
                  WHERE     CBPV.PrgVerId = R.PrgVerId
                ) AS CompletePV
               ,(
                  SELECT    SUM(TransferIn)
                  FROM      #TotalByProgramVersion CBPV
                  WHERE     CBPV.PrgVerId = R.PrgVerId
                ) AS TransferInPV
               ,(
                  SELECT    SUM(TransferOut)
                  FROM      #TotalByProgramVersion CBPV
                  WHERE     CBPV.PrgVerId = R.PrgVerId
                ) AS TransferOutPV
               ,(
                  SELECT    SUM(EndingEnrollment)
                  FROM      #TotalByProgramVersion CBPV
                  WHERE     CBPV.PrgVerId = R.PrgVerId
                ) AS EndingEnrollmentPV

--into tempWithLOA
        FROM    #TotalByProgramVersion R
        ORDER BY CampGrpDescrip
               ,CampDescrip
               ,PrgVerDescrip
               ,StudentName; 


    END;
--=================================================================================================
-- END  --  USP_Population_GetList_Detail
--=================================================================================================	
GO
