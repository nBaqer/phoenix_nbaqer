SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =========================================================================================================
-- USP_TR_Sub06_Courses
-- =========================================================================================================
CREATE PROCEDURE [dbo].[USP_TR_Sub06_Courses]
    @CoursesTakenTableName NVARCHAR(200)
   ,@GPASummaryTableName NVARCHAR(200)
   ,@StuEnrollIdList NVARCHAR(MAX)
   ,@TermId UNIQUEIDENTIFIER = NULL
   ,@ShowMultipleEnrollments BIT = 0
AS --
    BEGIN
        -- 0) Declare varialbles and Set initial values
        BEGIN

            IF EXISTS (
                      SELECT 1
                      FROM   tempdb.sys.objects AS O
                      WHERE  O.type IN ( 'U' )
                             AND O.object_id = OBJECT_ID(N'tempdb..#CoursesTaken')
                      )
                BEGIN
                    DROP TABLE #CoursesTaken;
                END;
            CREATE TABLE #CoursesTaken
                (
                    CoursesTakenId INTEGER NOT NULL PRIMARY KEY
                   ,StudentId UNIQUEIDENTIFIER
                   ,StuEnrollId UNIQUEIDENTIFIER
                   ,TermId UNIQUEIDENTIFIER
                   ,TermDescrip VARCHAR(100)
                   ,TermStartDate DATETIME
                   ,ReqId UNIQUEIDENTIFIER
                   ,ReqCode VARCHAR(100)
                   ,ReqDescription VARCHAR(100)
                   ,ReqCodeDescrip VARCHAR(100)
                   ,ReqCredits DECIMAL(18, 2)
                   ,MinVal DECIMAL(18, 2)
                   ,ClsSectionId VARCHAR(50)
                   ,CourseStarDate DATETIME
                   ,CourseEndDate DATETIME
                   ,SCS_CreditsAttempted DECIMAL(18, 2)
                   ,SCS_CreditsEarned DECIMAL(18, 2)
                   ,SCS_CurrentScore DECIMAL(18, 2)
                   ,SCS_CurrentGrade VARCHAR(10)
                   ,SCS_FinalScore DECIMAL(18, 2)
                   ,SCS_FinalGrade VARCHAR(10)
                   ,SCS_Completed BIT
                   ,SCS_FinalGPA DECIMAL(18, 2)
                   ,SCS_Product_WeightedAverage_Credits_GPA DECIMAL(18, 2)
                   ,SCS_Count_WeightedAverage_Credits DECIMAL(18, 2)
                   ,SCS_Product_SimpleAverage_Credits_GPA DECIMAL(18, 2)
                   ,SCS_Count_SimpleAverage_Credits DECIMAL(18, 2)
                   ,SCS_ModUser VARCHAR(50)
                   ,SCS_ModDate DATETIME
                   ,SCS_TermGPA_Simple DECIMAL(18, 2)
                   ,SCS_TermGPA_Weighted DECIMAL(18, 2)
                   ,SCS_coursecredits DECIMAL(18, 2)
                   ,SCS_CumulativeGPA DECIMAL(18, 2)
                   ,SCS_CumulativeGPA_Simple DECIMAL(18, 2)
                   ,SCS_FACreditsEarned DECIMAL(18, 2)
                   ,SCS_Average DECIMAL(18, 2)
                   ,SCS_CumAverage DECIMAL(18, 2)
                   ,ScheduleDays DECIMAL(18, 2)
                   ,ActualDay DECIMAL(18, 2)
                   ,FinalGPA_Calculations DECIMAL(18, 2)
                   ,FinalGPA_TermCalculations DECIMAL(18, 2)
                   ,CreditsEarned_Calculations DECIMAL(18, 2)
                   ,ActualDay_Calculations DECIMAL(18, 2)
                   ,GrdBkWgtDetailsCount INTEGER
                   ,CountMultipleEnrollment INTEGER
                   ,RowNumberMultipleEnrollment INTEGER
                   ,CountRepeated INTEGER
                   ,RowNumberRetaked INTEGER
                   ,SameTermCountRetaken INTEGER
                   ,RowNumberSameTermRetaked INTEGER
                   ,RowNumberMultEnrollNoRetaken INTEGER
                   ,RowNumberOverAllByClassSection INTEGER
                   ,IsCreditsEarned BIT
                );

            IF EXISTS (
                      SELECT 1
                      FROM   tempdb.sys.objects AS O
                      WHERE  O.type IN ( 'U' )
                             AND O.object_id = OBJECT_ID(N'tempdb..#GPASummary')
                      )
                BEGIN
                    DROP TABLE #GPASummary;
                END;
            CREATE TABLE #GPASummary
                (
                    GPASummaryId INTEGER NOT NULL PRIMARY KEY
                   ,StudentId UNIQUEIDENTIFIER
                   ,LastName VARCHAR(50)
                   ,FirstName VARCHAR(50)
                   ,MiddleName VARCHAR(50)
                   ,SSN VARCHAR(11)
                   ,StudentNumber VARCHAR(50)
                   ,StuEnrollId UNIQUEIDENTIFIER
                   ,EnrollmentID VARCHAR(50)
                   ,PrgVerId UNIQUEIDENTIFIER
                   ,PrgVerDescrip VARCHAR(50)
                   ,PrgVersionTrackCredits BIT
                   ,GrdSystemId UNIQUEIDENTIFIER
                   ,AcademicType VARCHAR(50)
                   ,ClockHourProgram VARCHAR(10)
                   ,IsMakingSAP BIT
                   ,TermId UNIQUEIDENTIFIER
                   ,TermDescrip VARCHAR(100)
                   ,TermStartDate DATETIME
                   ,TermEndDate DATETIME
                   ,DescripXTranscript VARCHAR(250)
                   ,TermAverage DECIMAL(18, 2)
                   ,EnroTermSimple_CourseCredits DECIMAL(18, 2)
                   ,EnroTermSimple_GPACredits DECIMAL(18, 2)
                   ,EnroTermSimple_GPA DECIMAL(18, 2)
                   ,EnroTermWeight_CoursesCredits DECIMAL(18, 2)
                   ,EnroTermWeight_GPACredits DECIMAL(18, 2)
                   ,EnroTermWeight_GPA DECIMAL(18, 2)
                   ,StudTermSimple_CourseCredits DECIMAL(18, 2)
                   ,StudTermSimple_GPACredits DECIMAL(18, 2)
                   ,StudTermSimple_GPA DECIMAL(18, 2)
                   ,StudTermWeight_CoursesCredits DECIMAL(18, 2)
                   ,StudTermWeight_GPACredits DECIMAL(18, 2)
                   ,StudTermWeight_GPA DECIMAL(18, 2)
                   ,EnroSimple_CourseCredits DECIMAL(18, 2)
                   ,EnroSimple_GPACredits DECIMAL(18, 2)
                   ,EnroSimple_GPA DECIMAL(18, 2)
                   ,EnroWeight_CoursesCredits DECIMAL(18, 2)
                   ,EnroWeight_GPACredits DECIMAL(18, 2)
                   ,EnroWeight_GPA DECIMAL(18, 2)
                   ,StudSimple_CourseCredits DECIMAL(18, 2)
                   ,StudSimple_GPACredits DECIMAL(18, 2)
                   ,StudSimple_GPA DECIMAL(18, 2)
                   ,StudWeight_CoursesCredits DECIMAL(18, 2)
                   ,StudWeight_GPACredits DECIMAL(18, 2)
                   ,StudWeight_GPA DECIMAL(18, 2)
                   ,GradesFormat VARCHAR(50)
                   ,GPAMethod VARCHAR(50)
                   ,GradeBookAt VARCHAR(50)
                   ,RetakenPolicy VARCHAR(50)
                );
        END;
        -- END --  0) Declare varialbles and Set initial values

        --  1)Fill temp tables
        BEGIN
            INSERT INTO #CoursesTaken
            EXEC dbo.USP_TR_Sub03_GetPreparedGPATable @TableName = @CoursesTakenTableName;


            INSERT INTO #GPASummary
            EXEC dbo.USP_TR_Sub03_GetPreparedGPATable @TableName = @GPASummaryTableName;

        END;
        -- END  --  1)Fill temp tables

        --  3) select info for courses
        BEGIN
            SELECT   DT.PrgVerId
                    ,DT.TermId
                    ,DT.TermDescription AS TermDescription
                    ,DT.TermStartDate
                    ,DT.AcademicType
                    ,DT.ReqId
                    ,DT.Course
                    ,DT.CourseStartDate
                    ,DT.ReqCode
                    ,DT.ReqDescription
                    ,DT.ReqCodeDescrip
                    ,DT.ReqCredits
                    ,DT.MinVal
                    ,DT.SCS_ReqDescrip
                    ,DT.SCS_CreditsAttempted
                    ,DT.SCS_CreditsEarned
                    ,DT.SCS_CurrentScore
                    ,DT.SCS_CurrentGrade
                    ,DT.FinalScore
                    ,DT.FinalGrade
                    ,DT.FinalGPA
                    ,DT.ScheduleDays
                    ,DT.ActualDay
                    ,DT.GradesFormat
            INTO     #FinalResult
            FROM     (
                     SELECT     DISTINCT GS.PrgVerId AS PrgVerId
                                        ,GS.TermId AS TermId
                                        ,GS.TermDescrip AS TermDescription
                                        ,GS.TermStartDate AS TermStartDate
                                        ,GS.AcademicType AS AcademicType
                                        ,CT.ReqId AS ReqId
                                        ,CT.ReqCode AS Course
                                        ,CT.CourseStarDate AS CourseStartDate
                                        ,CT.ReqCode AS ReqCode
                                        ,CT.ReqDescription AS ReqDescription
                                        ,CT.ReqCodeDescrip AS ReqCodeDescrip
                                        ,CT.ReqCredits AS ReqCredits
                                        ,CT.MinVal AS MinVal
                                        ,CT.ReqDescription AS SCS_ReqDescrip
                                        ,CT.SCS_CreditsAttempted AS SCS_CreditsAttempted
                                        ,CT.SCS_CreditsEarned AS SCS_CreditsEarned
                                        ,CT.SCS_CurrentScore AS SCS_CurrentScore
                                        ,CT.SCS_CurrentGrade AS SCS_CurrentGrade
                                        ,CT.SCS_FinalScore AS FinalScore
                                        ,CT.SCS_FinalGrade AS FinalGrade
                                        ,CT.SCS_FinalGPA AS FinalGPA
                                        ,CT.ScheduleDays AS ScheduleDays
                                        ,CT.ActualDay AS ActualDay
                                        ,GS.GradesFormat AS GradesFormat
                                        ,CASE WHEN @TermId IS NOT NULL THEN D.Seq
                                              ELSE 0
                                         END AS Seq
                     FROM       #GPASummary AS GS
                     INNER JOIN #CoursesTaken AS CT ON CT.StuEnrollId = GS.StuEnrollId
                                                       AND CT.TermId = GS.TermId
                     INNER JOIN (
                                SELECT T.Val
                                FROM   dbo.MultipleValuesForReportParameters(@StuEnrollIdList, ',', 1) AS T
                                ) AS List ON List.Val = GS.StuEnrollId
                     LEFT JOIN  dbo.arClassSections CS ON CS.ReqId = CT.ReqId
                     LEFT JOIN  dbo.arGrdBkWgtDetails D ON D.InstrGrdBkWgtId = CS.InstrGrdBkWgtId
                     WHERE      (
                                @TermId IS NULL
                                OR GS.TermId = @TermId
                                )
                                AND (
                                    (
                                    @ShowMultipleEnrollments = 1
                                    AND CT.RowNumberMultEnrollNoRetaken = 1
                                    )
                                    OR @ShowMultipleEnrollments = 0
                                    )
                     ) AS DT
            ORDER BY CASE WHEN @TermId IS NOT NULL THEN ( RANK() OVER ( ORDER BY DT.TermStartDate
                                                                                ,DT.TermDescription
                                                                                ,DT.Seq
                                                                                ,DT.ReqDescription
                                                                      )
                                                        )
                          ELSE ( RANK() OVER ( ORDER BY DT.CourseStartDate
                                                       ,DT.ReqDescription
                                             )
                               )
                     END;


            SELECT DISTINCT *
            FROM   #FinalResult;

            DROP TABLE #FinalResult;
        END;
        -- END  --  3) select info for courses

        -- 4) drop temp tables
        BEGIN
            --Drop temp tables
            IF EXISTS (
                      SELECT 1
                      FROM   tempdb.sys.objects AS O
                      WHERE  O.type IN ( 'U' )
                             AND O.object_id = OBJECT_ID(N'tempdb..#CoursesTaken')
                      )
                BEGIN
                    DROP TABLE #CoursesTaken;
                END;
            IF EXISTS (
                      SELECT 1
                      FROM   tempdb.sys.objects AS O
                      WHERE  O.type IN ( 'U' )
                             AND O.object_id = OBJECT_ID(N'tempdb..#GPASummary')
                      )
                BEGIN
                    DROP TABLE #GPASummary;
                END;
        END;
    -- END  --  4) drop temp tables
    END;
-- =========================================================================================================
-- END  --  USP_TR_Sub06_Courses
-- =========================================================================================================


GO
