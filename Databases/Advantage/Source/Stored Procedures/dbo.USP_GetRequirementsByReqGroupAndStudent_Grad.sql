SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[USP_GetRequirementsByReqGroupAndStudent_Grad]
    (
     @ReqGrpId UNIQUEIDENTIFIER
    ,@StudentID UNIQUEIDENTIFIER
    ,@CampusID UNIQUEIDENTIFIER 
    
    )
AS /*----------------------------------------------------------------------------------------------------
	Author          :	Saraswathi Lakshmanan
    
    Create date		:	03/03/2011
    
	Procedure Name	:	[USP_GetRequirementsByReqGroupAndStudent_Grad]

	Objective		:	Get the list of Students who have overrides that are not approved
		
	Parameters		:	Name			Type	Data Type			Required? 	
						=====			====	=========			=========	
					
					
	
	Output			:	Returns the requested details	
						
*/-----------------------------------------------------------------------------------------------------
    BEGIN


  
        DECLARE @StudentStartDate AS DATETIME;
        SET @StudentStartDate = (
                                  SELECT    MIN(StartDate)
                                  FROM      arStuEnrollments
                                  WHERE     Studentid = @StudentID
                                  GROUP BY  studentID
                                );
 
        DECLARE @ActiveStatusID AS UNIQUEIDENTIFIER;
        SET @ActiveStatusID = (
                                SELECT  StatusID
                                FROM    syStatuses
                                WHERE   Status = 'Active'
                              ); 
  
        SELECT DISTINCT
                t1.adReqId
               ,Descrip
               ,adReqTypeId
               ,t6.ReqGrpId AS ReqGrpId
        FROM    (
                  SELECT DISTINCT
                            adReqId
                           ,Descrip
                           ,adReqTypeId
                           ,CampGrpId
                  FROM      adReqs
                  WHERE     adReqTypeId IN ( 1,3 )
                            AND ReqforGraduation = 1
                            AND StatusId = @ActiveStatusID
                ) t1
               ,(
                  SELECT DISTINCT
                            StartDate
                           ,EndDate
                           ,MinScore
                           ,adReqId
                           ,adReqEffectiveDateId
                  FROM      adReqsEffectiveDates
                  WHERE     MandatoryRequirement <> 1
                            AND @StudentStartDate >= StartDate
                            AND (
                                  @StudentStartDate <= EndDate
                                  OR EndDate IS NULL
                                )
                ) t2
               ,(
                  SELECT DISTINCT
                            LeadGrpId
                           ,adReqEffectiveDateId
                           ,IsRequired
                  FROM      adReqLeadGroups
                  WHERE     LeadGrpId IN ( SELECT DISTINCT
                                                    LeadGrpId
                                           FROM     adLeadByLeadGroups t1
                                                   ,arStuEnrollments t2
                                                   ,arStudent t3
                                           WHERE    t1.StuEnrollId = t2.StuEnrollId
                                                    AND t2.StudentId = t3.StudentId
                                                    AND t3.StudentId = @StudentID )
                ) t3
               ,(
                  SELECT DISTINCT
                            ReqGrpId
                  FROM      adPrgVerTestDetails t1
                           ,arStuEnrollments t2
                           ,arStudent t3
                  WHERE     t1.PrgVerId = t2.PrgVerId
                            AND t2.StudentId = t3.StudentId
                            AND t3.StudentId = @StudentID
                            AND ReqGrpId = @ReqGrpId
                            AND ReqGrpId IS NOT NULL
                ) t5
               ,(
                  SELECT DISTINCT
                            adReqId
                           ,ReqGrpId
                           ,LeadGrpId
                  FROM      adReqGrpDef
                  WHERE     ReqGrpId = @ReqGrpId
                ) t6
               ,(
                  SELECT DISTINCT
                            LeadGrpId
                  FROM      adLeadByLeadGroups t1
                           ,arStuEnrollments t2
                           ,arStudent t3
                  WHERE     t1.StuEnrollId = t2.StuEnrollId
                            AND t2.StudentId = t3.StudentId
                            AND t3.StudentId = @StudentID
                ) t7
        WHERE   t1.adReqId = t2.adReqId
                AND t2.adReqEffectiveDateId = t3.adReqEffectiveDateId
                AND t5.ReqGrpId = t6.ReqGrpId
                AND t3.LeadGrpId = t7.LeadGrpId
                AND t6.LeadGrpId = t7.LeadGrpId
                AND t1.adReqId = t6.adReqId
                AND t1.CampGrpId IN ( SELECT DISTINCT
                                                B2.CampGrpId
                                      FROM      syCmpGrpCmps B1
                                               ,syCampGrps B2
                                      WHERE     B1.CampGrpId = B2.CampGrpId
                                                AND B1.CampusId = @CampusID
                                                AND B2.StatusId = @ActiveStatusID );
    END;




GO
