SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/* 
PURPOSE: 
Get component summaries for a selected student enrollment, class section and component type

CREATED: 


MODIFIED:
10/25/2013 WP	US4547 Refactor Post Services by Student process

ResourceId
Homework = 499
LabWork = 500
Exam = 501
Final = 502
LabHours = 503
Externship = 544

*/

CREATE PROCEDURE [dbo].[usp_GetClassroomWorkServices]
    (
        @StuEnrollId UNIQUEIDENTIFIER
       ,@ClsSectionId UNIQUEIDENTIFIER
       ,@GradeBookComponentType INT
    )
AS
    SET NOCOUNT ON;


    DECLARE @ReqId AS UNIQUEIDENTIFIER;
    SELECT @ReqId = ReqId
    FROM   arClassSections
    WHERE  ClsSectionId = @ClsSectionId;

    SELECT *
    INTO   #PostedServices
    FROM   arGrdBkResults
    WHERE  StuEnrollId = @StuEnrollId
	AND PostDate IS NOT NULL AND Score IS NOT null
           AND ClsSectionId = @ClsSectionId
           AND InstrGrdBkWgtDetailId IN (
                                        SELECT InstrGrdBkWgtDetailId
                                        FROM   arGrdBkWgtDetails posted_gbwd
                                        JOIN   arGrdComponentTypes posted_gct ON posted_gct.GrdComponentTypeId = posted_gbwd.GrdComponentTypeId
                                        WHERE  posted_gct.SysComponentTypeId = @GradeBookComponentType
                                        );

    --SELECT * FROM #PostedServices

    DECLARE @ClinicScoresEnabled BIT = (
                                       SELECT CASE WHEN ISNULL(
                                                                  dbo.GetAppSettingValueByKeyName(
                                                                                                     'ClinicServicesScoresEnabled'
                                                                                                    ,(
                                                                                                    SELECT CampusId
                                                                                                    FROM   dbo.arStuEnrollments
                                                                                                    WHERE  StuEnrollId = @StuEnrollId
                                                                                                    )
                                                                                                 )
                                                                 ,''
                                                              ) = 'No' THEN 0
                                                   ELSE 1
                                              END
                                       );

    SELECT    gct.Descrip
             ,ISNULL(gbwd.Number, 0) AS Required
             ,CASE WHEN @GradeBookComponentType = 503 THEN ISNULL((
                                                                  SELECT SUM(Score)
                                                                  FROM   #PostedServices
                                                                  WHERE  InstrGrdBkWgtDetailId = gbwd.InstrGrdBkWgtDetailId
                                                                  )
                                                                 ,0
                                                                 )
                   ELSE ISNULL((
                               SELECT COUNT(*)
                               FROM   #PostedServices
                               WHERE  InstrGrdBkWgtDetailId = gbwd.InstrGrdBkWgtDetailId
                               )
                              ,0
                              )
              END AS Completed
             ,CASE WHEN @GradeBookComponentType = 503 THEN CASE WHEN ( ISNULL(gbwd.Number, 0) - ISNULL(( SUM(ps.Score)), 0)) <= 0 THEN 0
                                                                ELSE ( ISNULL(gbwd.Number, 0) - ISNULL(( SUM(ps.Score)), 0))
                                                           END
                   ELSE CASE WHEN ( ISNULL(gbwd.Number, 0) - ISNULL((
                                                                    SELECT COUNT(*)
                                                                    FROM   #PostedServices
                                                                    WHERE  InstrGrdBkWgtDetailId = gbwd.InstrGrdBkWgtDetailId
                                                                    )
                                                                   ,0
                                                                   )
                                  ) <= 0 THEN 0
                             ELSE ( ISNULL(gbwd.Number, 0) - ISNULL((
                                                                    SELECT COUNT(*)
                                                                    FROM   #PostedServices
                                                                    WHERE  InstrGrdBkWgtDetailId = gbwd.InstrGrdBkWgtDetailId
                                                                    )
                                                                   ,0
                                                                   )
                                  )
                        END
              END AS Remaining
             ,CASE WHEN @GradeBookComponentType = 503 THEN ISNULL((
                                                                  SELECT SUM(Score)
                                                                  FROM   #PostedServices
                                                                  WHERE  InstrGrdBkWgtDetailId = gbwd.InstrGrdBkWgtDetailId
                                                                         AND PostDate IS NOT NULL
                                                                         AND Score IS NOT NULL
                                                                  )
                                                                 ,0
                                                                 )
                   WHEN @ClinicScoresEnabled = 1 THEN AVG(ps.Score)
                   ELSE ( ISNULL((
                                 SELECT COUNT(*)
                                 FROM   #PostedServices
                                 WHERE  InstrGrdBkWgtDetailId = gbwd.InstrGrdBkWgtDetailId
                                        AND PostDate IS NOT NULL
                                        AND Score IS NOT NULL
                                 )
                                ,0
                                )
                        )
              END AS Average
             ,se.EnrollDate
             ,gbwd.InstrGrdBkWgtDetailId
             ,@ClsSectionId AS ClsSectionId
             ,gbwd.Seq
    FROM      arGrdBkWeights gbw
    LEFT JOIN arGrdBkWgtDetails gbwd ON gbwd.InstrGrdBkWgtId = gbw.InstrGrdBkWgtId
    LEFT JOIN arGrdComponentTypes gct ON gct.GrdComponentTypeId = gbwd.GrdComponentTypeId
    LEFT JOIN arClassSections cs ON cs.ReqId = gbw.ReqId
    LEFT JOIN arResults ar ON ar.TestId = cs.ClsSectionId
    LEFT JOIN arStuEnrollments se ON ar.StuEnrollId = se.StuEnrollId
    LEFT JOIN #PostedServices ps ON ps.InstrGrdBkWgtDetailId = gbwd.InstrGrdBkWgtDetailId
    WHERE     gct.SysComponentTypeId = @GradeBookComponentType
              AND gbw.ReqId = @ReqId
              AND ar.TestId = @ClsSectionId
              AND ar.StuEnrollId = @StuEnrollId
    GROUP BY  gct.Descrip
             ,gbwd.Number
             ,se.EnrollDate
             ,gbwd.InstrGrdBkWgtDetailId
             ,gbwd.Seq
    ORDER BY  gbwd.Seq
             ,gct.Descrip;



    DROP TABLE #PostedServices;









GO
