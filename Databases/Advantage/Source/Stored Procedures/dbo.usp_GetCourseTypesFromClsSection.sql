SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_GetCourseTypesFromClsSection]
    (
     @ClsSectionID UNIQUEIDENTIFIER 
       
    )
AS
    BEGIN 

        SELECT  dbo.arClsSectMeetings.ClsSectionId
               ,dbo.arClsSectMeetings.ClsSectMeetingId
               ,dbo.arClsSectMeetings.InstructionTypeID
               ,dbo.arInstructionType.InstructionTypeDescrip
        FROM    dbo.arClsSectMeetings
               ,dbo.arInstructionType
        WHERE   dbo.arClsSectMeetings.InstructionTypeID = dbo.arInstructionType.InstructionTypeID
                AND dbo.arClsSectMeetings.ClsSectionId = @ClsSectionID; 
 
    END;



GO
