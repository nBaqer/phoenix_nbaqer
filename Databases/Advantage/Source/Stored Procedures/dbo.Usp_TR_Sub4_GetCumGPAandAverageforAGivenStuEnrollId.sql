SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--=================================================================================================
-- Usp_TR_Sub4_GetCumGPAandAverageforAGivenStuEnrollId
--=================================================================================================
CREATE PROCEDURE [dbo].[Usp_TR_Sub4_GetCumGPAandAverageforAGivenStuEnrollId]
    @StuEnrollId VARCHAR(MAX) = NULL
   ,@CumAverage DECIMAL(18,2) OUTPUT
   ,@cumWeightedGPA DECIMAL(18,2) OUTPUT
   ,@cumSimpleGPA DECIMAL(18,2) OUTPUT
AS
    BEGIN
        DECLARE @TrackSapAttendance VARCHAR(1000);  
        DECLARE @displayHours AS VARCHAR(1000);   
        DECLARE @GradeCourseRepetitionsMethod VARCHAR(1000);
        DECLARE @GradesFormat AS VARCHAR(1000);
        DECLARE @StuEnrollCampusId UNIQUEIDENTIFIER;
		
        SET @StuEnrollCampusId = COALESCE((
                                            SELECT  ASE.CampusId
                                            FROM    arStuEnrollments AS ASE
                                            WHERE   ASE.StuEnrollId = @StuEnrollId
                                          ),NULL);
        SET @TrackSapAttendance = dbo.GetAppSettingValueByKeyName('TrackSapAttendance',@StuEnrollCampusId);
        IF ( @TrackSapAttendance IS NOT NULL )
            BEGIN
                SET @TrackSapAttendance = LOWER(LTRIM(RTRIM(@TrackSapAttendance)));
            END;

        SET @GradeCourseRepetitionsMethod = dbo.GetAppSettingValueByKeyName('GradeCourseRepetitionsMethod',@StuEnrollCampusId); 
        SET @GradesFormat = dbo.GetAppSettingValueByKeyName('GradesFormat',@StuEnrollCampusId);
        IF ( @GradesFormat IS NOT NULL )
            BEGIN
                SET @GradesFormat = LOWER(LTRIM(RTRIM(@GradesFormat)));
            END;



        DECLARE @CoursesNotRepeated TABLE
            (
             StuEnrollId UNIQUEIDENTIFIER
            ,TermId UNIQUEIDENTIFIER
            ,TermDescrip VARCHAR(100)
            ,ReqId UNIQUEIDENTIFIER
            ,ReqDescrip VARCHAR(100)
            ,ClsSectionId VARCHAR(50)
            ,CreditsEarned DECIMAL(18,2)
            ,CreditsAttempted DECIMAL(18,2)
            ,CurrentScore DECIMAL(18,2)
            ,CurrentGrade VARCHAR(10)
            ,FinalScore DECIMAL(18,2)
            ,FinalGrade VARCHAR(10)
            ,Completed BIT
            ,FinalGPA DECIMAL(18,2)
            ,Product_WeightedAverage_Credits_GPA DECIMAL(18,2)
            ,Count_WeightedAverage_Credits DECIMAL(18,2)
            ,Product_SimpleAverage_Credits_GPA DECIMAL(18,2)
            ,Count_SimpleAverage_Credits DECIMAL(18,2)
            ,ModUser VARCHAR(50)
            ,ModDate DATETIME
            ,TermGPA_Simple DECIMAL(18,2)
            ,TermGPA_Weighted DECIMAL(18,2)
            ,coursecredits DECIMAL(18,2)
            ,CumulativeGPA DECIMAL(18,2)
            ,CumulativeGPA_Simple DECIMAL(18,2)
            ,FACreditsEarned DECIMAL(18,2)
            ,Average DECIMAL(18,2)
            ,CumAverage DECIMAL(18,2)
            ,TermStartDate DATETIME
            ,rownumber INT
            );

        INSERT  INTO @CoursesNotRepeated
                SELECT  StuEnrollId
                       ,TermId
                       ,TermDescrip
                       ,ReqId
                       ,ReqDescrip
                       ,ClsSectionId
                       ,CreditsEarned
                       ,CreditsAttempted
                       ,CurrentScore
                       ,CurrentGrade
                       ,FinalScore
                       ,FinalGrade
                       ,Completed
                       ,FinalGPA
                       ,Product_WeightedAverage_Credits_GPA
                       ,Count_WeightedAverage_Credits
                       ,Product_SimpleAverage_Credits_GPA
                       ,Count_SimpleAverage_Credits
                       ,ModUser
                       ,ModDate
                       ,TermGPA_Simple
                       ,TermGPA_Weighted
                       ,coursecredits
                       ,CumulativeGPA
                       ,CumulativeGPA_Simple
                       ,FACreditsEarned
                       ,Average
                       ,CumAverage
                       ,TermStartDate
                       ,NULL AS rownumber
                FROM    syCreditSummary
                WHERE   StuEnrollId = @StuEnrollId
                        AND ReqId IN ( SELECT   ReqId
                                       FROM     (
                                                  SELECT    ReqId
                                                           ,ReqDescrip
                                                           ,COUNT(*) AS counter
                                                  FROM      syCreditSummary
                                                  WHERE     StuEnrollId = @StuEnrollId
                                                  GROUP BY  ReqId
                                                           ,ReqDescrip
                                                  HAVING    COUNT(*) = 1
                                                ) dt );

	

        IF LOWER(@GradeCourseRepetitionsMethod) = 'latest'
            BEGIN
                INSERT  INTO @CoursesNotRepeated
                        SELECT  dt2.StuEnrollId
                               ,dt2.TermId
                               ,dt2.TermDescrip
                               ,dt2.ReqId
                               ,dt2.ReqDescrip
                               ,dt2.ClsSectionId
                               ,dt2.CreditsEarned
                               ,dt2.CreditsAttempted
                               ,dt2.CurrentScore
                               ,dt2.CurrentGrade
                               ,dt2.FinalScore
                               ,dt2.FinalGrade
                               ,dt2.Completed
                               ,dt2.FinalGPA
                               ,dt2.Product_WeightedAverage_Credits_GPA
                               ,dt2.Count_WeightedAverage_Credits
                               ,dt2.Product_SimpleAverage_Credits_GPA
                               ,dt2.Count_SimpleAverage_Credits
                               ,dt2.ModUser
                               ,dt2.ModDate
                               ,dt2.TermGPA_Simple
                               ,dt2.TermGPA_Weighted
                               ,dt2.coursecredits
                               ,dt2.CumulativeGPA
                               ,dt2.CumulativeGPA_Simple
                               ,dt2.FACreditsEarned
                               ,dt2.Average
                               ,dt2.CumAverage
                               ,dt2.TermStartDate
                               ,dt2.RowNumber
                        FROM    (
                                  SELECT    StuEnrollId
                                           ,TermId
                                           ,TermDescrip
                                           ,ReqId
                                           ,ReqDescrip
                                           ,ClsSectionId
                                           ,CreditsEarned
                                           ,CreditsAttempted
                                           ,CurrentScore
                                           ,CurrentGrade
                                           ,FinalScore
                                           ,FinalGrade
                                           ,Completed
                                           ,FinalGPA
                                           ,Product_WeightedAverage_Credits_GPA
                                           ,Count_WeightedAverage_Credits
                                           ,Product_SimpleAverage_Credits_GPA
                                           ,Count_SimpleAverage_Credits
                                           ,ModUser
                                           ,ModDate
                                           ,TermGPA_Simple
                                           ,TermGPA_Weighted
                                           ,coursecredits
                                           ,CumulativeGPA
                                           ,CumulativeGPA_Simple
                                           ,FACreditsEarned
                                           ,Average
                                           ,CumAverage
                                           ,TermStartDate
                                           ,ROW_NUMBER() OVER ( PARTITION BY ReqId ORDER BY TermStartDate DESC ) AS RowNumber
                                  FROM      syCreditSummary
                                  WHERE     StuEnrollId = @StuEnrollId
                                            AND (
                                                  FinalScore IS NOT NULL
                                                  OR FinalGrade IS NOT NULL
                                                )
                                            AND ReqId IN ( SELECT   ReqId
                                                           FROM     (
                                                                      SELECT    ReqId
                                                                               ,ReqDescrip
                                                                               ,COUNT(*) AS Counter
                                                                      FROM      syCreditSummary
                                                                      WHERE     StuEnrollId = @StuEnrollId
                                                                      GROUP BY  ReqId
                                                                               ,ReqDescrip
                                                                      HAVING    COUNT(*) > 1
                                                                    ) dt )
                                ) dt2
                        WHERE   RowNumber = 1
                        ORDER BY TermStartDate DESC;
            END;

        IF LOWER(@GradeCourseRepetitionsMethod) = 'best'
            BEGIN
                INSERT  INTO @CoursesNotRepeated
                        SELECT  dt2.StuEnrollId
                               ,dt2.TermId
                               ,dt2.TermDescrip
                               ,dt2.ReqId
                               ,dt2.ReqDescrip
                               ,dt2.ClsSectionId
                               ,dt2.CreditsEarned
                               ,dt2.CreditsAttempted
                               ,dt2.CurrentScore
                               ,dt2.CurrentGrade
                               ,dt2.FinalScore
                               ,dt2.FinalGrade
                               ,dt2.Completed
                               ,dt2.FinalGPA
                               ,dt2.Product_WeightedAverage_Credits_GPA
                               ,dt2.Count_WeightedAverage_Credits
                               ,dt2.Product_SimpleAverage_Credits_GPA
                               ,dt2.Count_SimpleAverage_Credits
                               ,dt2.ModUser
                               ,dt2.ModDate
                               ,dt2.TermGPA_Simple
                               ,dt2.TermGPA_Weighted
                               ,dt2.coursecredits
                               ,dt2.CumulativeGPA
                               ,dt2.CumulativeGPA_Simple
                               ,dt2.FACreditsEarned
                               ,dt2.Average
                               ,dt2.CumAverage
                               ,dt2.TermStartDate
                               ,dt2.RowNumber
                        FROM    (
                                  SELECT    StuEnrollId
                                           ,TermId
                                           ,TermDescrip
                                           ,ReqId
                                           ,ReqDescrip
                                           ,ClsSectionId
                                           ,CreditsEarned
                                           ,CreditsAttempted
                                           ,CurrentScore
                                           ,CurrentGrade
                                           ,FinalScore
                                           ,FinalGrade
                                           ,Completed
                                           ,FinalGPA
                                           ,Product_WeightedAverage_Credits_GPA
                                           ,Count_WeightedAverage_Credits
                                           ,Product_SimpleAverage_Credits_GPA
                                           ,Count_SimpleAverage_Credits
                                           ,ModUser
                                           ,ModDate
                                           ,TermGPA_Simple
                                           ,TermGPA_Weighted
                                           ,coursecredits
                                           ,CumulativeGPA
                                           ,CumulativeGPA_Simple
                                           ,FACreditsEarned
                                           ,Average
                                           ,CumAverage
                                           ,TermStartDate
                                           ,ROW_NUMBER() OVER ( PARTITION BY ReqId ORDER BY Completed DESC, CreditsEarned DESC, FinalGPA DESC ) AS RowNumber
                                  FROM      syCreditSummary
                                  WHERE     StuEnrollId = @StuEnrollId
                                            AND (
                                                  FinalScore IS NOT NULL
                                                  OR FinalGrade IS NOT NULL
                                                )
                                            AND ReqId IN ( SELECT   ReqId
                                                           FROM     (
                                                                      SELECT    ReqId
                                                                               ,ReqDescrip
                                                                               ,COUNT(*) AS Counter
                                                                      FROM      syCreditSummary
                                                                      WHERE     StuEnrollId = @StuEnrollId
                                                                      GROUP BY  ReqId
                                                                               ,ReqDescrip
                                                                      HAVING    COUNT(*) > 1
                                                                    ) dt )
                                ) dt2
                        WHERE   RowNumber = 1; 
            END;

        IF LOWER(@GradeCourseRepetitionsMethod) = 'average'
            BEGIN
                INSERT  INTO @CoursesNotRepeated
                        SELECT  dt2.StuEnrollId
                               ,dt2.TermId
                               ,dt2.TermDescrip
                               ,dt2.ReqId
                               ,dt2.ReqDescrip
                               ,dt2.ClsSectionId
                               ,dt2.CreditsEarned
                               ,dt2.CreditsAttempted
                               ,dt2.CurrentScore
                               ,dt2.CurrentGrade
                               ,dt2.FinalScore
                               ,dt2.FinalGrade
                               ,dt2.Completed
                               ,dt2.FinalGPA
                               ,dt2.Product_WeightedAverage_Credits_GPA
                               ,dt2.Count_WeightedAverage_Credits
                               ,dt2.Product_SimpleAverage_Credits_GPA
                               ,dt2.Count_SimpleAverage_Credits
                               ,dt2.ModUser
                               ,dt2.ModDate
                               ,dt2.TermGPA_Simple
                               ,dt2.TermGPA_Weighted
                               ,dt2.coursecredits
                               ,dt2.CumulativeGPA
                               ,dt2.CumulativeGPA_Simple
                               ,dt2.FACreditsEarned
                               ,dt2.Average
                               ,dt2.CumAverage
                               ,dt2.TermStartDate
                               ,dt2.RowNumber
                        FROM    (
                                  SELECT    StuEnrollId
                                           ,TermId
                                           ,TermDescrip
                                           ,ReqId
                                           ,ReqDescrip
                                           ,ClsSectionId
                                           ,CreditsEarned
                                           ,CreditsAttempted
                                           ,CurrentScore
                                           ,CurrentGrade
                                           ,FinalScore
                                           ,FinalGrade
                                           ,Completed
                                           ,FinalGPA
                                           ,Product_WeightedAverage_Credits_GPA
                                           ,Count_WeightedAverage_Credits
                                           ,Product_SimpleAverage_Credits_GPA
                                           ,Count_SimpleAverage_Credits
                                           ,ModUser
                                           ,ModDate
                                           ,TermGPA_Simple
                                           ,TermGPA_Weighted
                                           ,coursecredits
                                           ,CumulativeGPA
                                           ,CumulativeGPA_Simple
                                           ,FACreditsEarned
                                           ,Average
                                           ,CumAverage
                                           ,TermStartDate
                                           ,ROW_NUMBER() OVER ( PARTITION BY ReqId ORDER BY FinalGPA DESC ) AS RowNumber
                                  FROM      syCreditSummary
                                  WHERE     StuEnrollId = @StuEnrollId
                                            AND (
                                                  FinalScore IS NOT NULL
                                                  OR FinalGrade IS NOT NULL
                                                )
                                            AND ReqId IN ( SELECT   ReqId
                                                           FROM     (
                                                                      SELECT    ReqId
                                                                               ,ReqDescrip
                                                                               ,COUNT(*) AS Counter
                                                                      FROM      syCreditSummary
                                                                      WHERE     StuEnrollId = @StuEnrollId
                                                                      GROUP BY  ReqId
                                                                               ,ReqDescrip
                                                                      HAVING    COUNT(*) > 1
                                                                    ) dt )
                                ) dt2; 
            END;

   
        DECLARE @cumSimpleCourseCredits DECIMAL(18,2);
        DECLARE @cumSimple_GPA_Credits DECIMAL(18,2);
	 -- (OUTPUT parameter)  DECLARE @cumSimpleGPA DECIMAL(18, 2)
        
	

        DECLARE @cumCourseCredits DECIMAL(18,2)
           ,@cumWeighted_GPA_Credits DECIMAL(18,2);
        DECLARE @cumCourseCredits_repeated DECIMAL(18,2)
           ,@cumWeighted_GPA_Credits_repeated DECIMAL(18,2);

        PRINT @GradeCourseRepetitionsMethod;

        IF LOWER(@GradeCourseRepetitionsMethod) = 'average'
            BEGIN
                SET @cumWeightedGPA = 0;
                SET @cumSimpleGPA = 0;
					
                SET @cumSimpleCourseCredits = (
                                                SELECT  COUNT(*)
                                                FROM    @CoursesNotRepeated
                                                WHERE   StuEnrollId = @StuEnrollId
                                                        AND FinalGPA IS NOT NULL
                                                        AND (
                                                              rownumber = 0
                                                              OR rownumber IS NULL
                                                            )
                                              );

                DECLARE @cumSimpleCourseCredits_repeated DECIMAL(18,2);
                SET @cumSimpleCourseCredits_repeated = (
                                                         SELECT SUM(CourseCreditCount)
                                                         FROM   (
                                                                  SELECT    ReqId
                                                                           ,COUNT(coursecredits) AS CourseCreditCount
                                                                  FROM      @CoursesNotRepeated
                                                                  WHERE     StuEnrollId IN ( @StuEnrollId )
                                                                            AND FinalGPA IS NOT NULL
                                                                            AND rownumber = 1
                                                                  GROUP BY  ReqId
                                                                ) dt
                                                       );
                SET @cumSimpleCourseCredits = @cumSimpleCourseCredits + ISNULL(@cumSimpleCourseCredits_repeated,0);
		
                DECLARE @cumSimple_GPA_Credits_repeated DECIMAL(18,2);

                SET @cumSimple_GPA_Credits = (
                                               SELECT   SUM(FinalGPA)
                                               FROM     @CoursesNotRepeated
                                               WHERE    StuEnrollId = @StuEnrollId
                                                        AND FinalGPA IS NOT NULL
                                                        AND (
                                                              rownumber = 0
                                                              OR rownumber IS NULL
                                                            )
                                             );

                SET @cumSimple_GPA_Credits_repeated = (
                                                        SELECT  SUM(AverageGPA)
                                                        FROM    (
                                                                  SELECT    ReqId
                                                                           ,SUM(FinalGPA) / MAX(rownumber) AS AverageGPA
                                                                  FROM      @CoursesNotRepeated
                                                                  WHERE     StuEnrollId IN ( @StuEnrollId )
                                                                            AND FinalGPA IS NOT NULL
                                                                            AND rownumber >= 1
                                                                  GROUP BY  ReqId
                                                                ) dt
                                                      );

                SET @cumSimple_GPA_Credits = @cumSimple_GPA_Credits + ISNULL(@cumSimple_GPA_Credits_repeated,0.00);

                IF @cumSimpleCourseCredits >= 1
                    BEGIN
                        SET @cumSimpleGPA = @cumSimple_GPA_Credits / @cumSimpleCourseCredits;
                    END; 

                SET @cumCourseCredits = (
                                          SELECT    SUM(coursecredits)
                                          FROM      @CoursesNotRepeated
                                          WHERE     StuEnrollId IN ( @StuEnrollId )
                                                    AND FinalGPA IS NOT NULL
                                                    AND (
                                                          rownumber = 0
                                                          OR rownumber IS NULL
                                                        )
                                        );

                SET @cumCourseCredits_repeated = (
                                                   SELECT   SUM(CourseCreditCount)
                                                   FROM     (
                                                              SELECT    ReqId
                                                                       ,MAX(coursecredits) AS CourseCreditCount
                                                              FROM      @CoursesNotRepeated
                                                              WHERE     StuEnrollId IN ( @StuEnrollId )
                                                                        AND FinalGPA IS NOT NULL
                                                                        AND rownumber >= 1
                                                              GROUP BY  ReqId
                                                            ) dt
                                                 );
                SET @cumCourseCredits = @cumCourseCredits + ISNULL(@cumCourseCredits_repeated,0.00);

                PRINT @cumCourseCredits;

                SET @cumWeighted_GPA_Credits = (
                                                 SELECT SUM(coursecredits * FinalGPA)
                                                 FROM   @CoursesNotRepeated
                                                 WHERE  StuEnrollId IN ( @StuEnrollId )
                                                        AND FinalGPA IS NOT NULL
                                                        AND (
                                                              rownumber = 0
                                                              OR rownumber IS NULL
                                                            )
                                               );
                SET @cumWeighted_GPA_Credits_repeated = (
                                                          SELECT    SUM(CourseCreditCount)
                                                          FROM      (
                                                                      SELECT    ReqId
                                                                               ,SUM(coursecredits * FinalGPA) / MAX(rownumber) AS CourseCreditCount
                                                                      FROM      @CoursesNotRepeated
                                                                      WHERE     StuEnrollId IN ( @StuEnrollId )
                                                                                AND FinalGPA IS NOT NULL
                                                                                AND rownumber >= 1
                                                                      GROUP BY  ReqId
                                                                    ) dt
                                                        );
			
				
			
                SET @cumWeighted_GPA_Credits = @cumWeighted_GPA_Credits + ISNULL(@cumWeighted_GPA_Credits_repeated,0);
                PRINT @cumWeighted_GPA_Credits;

                IF @cumCourseCredits >= 1
                    BEGIN
                        SET @cumWeightedGPA = @cumWeighted_GPA_Credits / @cumCourseCredits;
                    END; 
            END;
        ELSE
            BEGIN 
                SET @cumSimpleGPA = 0;

                SET @cumSimpleCourseCredits = (
                                                SELECT  COUNT(*)
                                                FROM    @CoursesNotRepeated
                                                WHERE   StuEnrollId = @StuEnrollId
                                                        AND FinalGPA IS NOT NULL
                                              );
                SET @cumSimple_GPA_Credits = (
                                               SELECT   SUM(FinalGPA)
                                               FROM     @CoursesNotRepeated
                                               WHERE    StuEnrollId = @StuEnrollId
                                                        AND FinalGPA IS NOT NULL
                                             );		
                IF @cumSimpleCourseCredits >= 1
                    BEGIN
                        SET @cumSimpleGPA = @cumSimple_GPA_Credits / @cumSimpleCourseCredits;
                    END; 
                SET @cumWeightedGPA = 0;
                SET @cumCourseCredits = (
                                          SELECT    SUM(coursecredits)
                                          FROM      @CoursesNotRepeated
                                          WHERE     StuEnrollId = @StuEnrollId
                                                    AND FinalGPA IS NOT NULL
                                        );
                SET @cumWeighted_GPA_Credits = (
                                                 SELECT SUM(coursecredits * FinalGPA)
                                                 FROM   @CoursesNotRepeated
                                                 WHERE  StuEnrollId = @StuEnrollId
                                                        AND FinalGPA IS NOT NULL
                                               );

                IF @cumCourseCredits >= 1
                    BEGIN
                        SET @cumWeightedGPA = @cumWeighted_GPA_Credits / @cumCourseCredits;
                    END; 

            END;




					
        IF @cumCourseCredits >= 1
            BEGIN
                SET @cumWeightedGPA = @cumWeighted_GPA_Credits / @cumCourseCredits;
            END; 


        DECLARE @TermAverageCount INT;
        DECLARE @TermAverage DECIMAL(18,2); 
		-- (output parameter)  DECLARE @CumAverage DECIMAL(18, 2)
        IF @GradesFormat <> 'letter'
            BEGIN

                DECLARE @termAverageSum DECIMAL(18,2)
                   ,@cumAverageSum DECIMAL(18,2)
                   ,@cumAveragecount INT;
                DECLARE @TardiesMakingAbsence INT
                   ,@UnitTypeDescrip VARCHAR(20)
                   ,@OriginalTardiesMakingAbsence INT;
                SET @TardiesMakingAbsence = (
                                              SELECT TOP 1
                                                        t1.TardiesMakingAbsence
                                              FROM      arPrgVersions t1
                                                       ,arStuEnrollments t2
                                              WHERE     t1.PrgVerId = t2.PrgVerId
                                                        AND t2.StuEnrollId = @StuEnrollId
                                            );
							
                SET @UnitTypeDescrip = (
                                         SELECT LTRIM(RTRIM(AAUT.UnitTypeDescrip))
                                         FROM   arAttUnitType AS AAUT
                                         INNER JOIN arPrgVersions AS APV ON APV.UnitTypeId = AAUT.UnitTypeId
                                         INNER JOIN arStuEnrollments AS ASE ON ASE.PrgVerId = APV.PrgVerId
                                         WHERE  ASE.StuEnrollId = @StuEnrollId
                                       );

                SET @OriginalTardiesMakingAbsence = (
                                                      SELECT TOP 1
                                                                tardiesmakingabsence
                                                      FROM      syStudentAttendanceSummary
                                                      WHERE     StuEnrollId = @StuEnrollId
                                                    );
                DECLARE @termstartdate1 DATETIME;

-- Declare Variables
                BEGIN -- Declare Variables
                    DECLARE @MeetDate DATETIME
                       ,@WeekDay VARCHAR(15)
                       ,@StartDate DATETIME
                       ,@EndDate DATETIME;
                    DECLARE @PeriodDescrip VARCHAR(50)
                       ,@Actual DECIMAL(18,2)
                       ,@Excused DECIMAL(18,2)
                       ,@ClsSectionId UNIQUEIDENTIFIER;
                    DECLARE @Absent DECIMAL(18,2)
                       ,@SchedHours DECIMAL(18,2)
                       ,@TardyMinutes DECIMAL(18,2);
                    DECLARE @tardy DECIMAL(18,2)
                       ,@tracktardies INT
                       ,@rownumber INT
                       ,@IsTardy BIT
                       ,@ActualRunningScheduledHours DECIMAL(18,2);
                    DECLARE @ActualRunningPresentHours DECIMAL(18,2)
                       ,@ActualRunningAbsentHours DECIMAL(18,2)
                       ,@ActualRunningTardyHours DECIMAL(18,2)
                       ,@ActualRunningMakeupHours DECIMAL(18,2);
                    DECLARE @PrevStuEnrollId UNIQUEIDENTIFIER
                       ,@intTardyBreakPoint INT
                       ,@AdjustedRunningPresentHours DECIMAL(18,2)
                       ,@AdjustedRunningAbsentHours DECIMAL(18,2)
                       ,@ActualRunningScheduledDays DECIMAL(18,2);
		--declare @Scheduledhours decimal(18,2),@ActualPresentDays_ConvertTo_Hours decimal(18,2),@ActualAbsentDays_ConvertTo_Hours decimal(18,2),@AttendanceTrack varchar(50)
                    DECLARE @TermId VARCHAR(50)
                       ,@TermDescrip VARCHAR(50)
                       ,@PrgVerId UNIQUEIDENTIFIER;
                    DECLARE @TardyHit VARCHAR(10)
                       ,@ScheduledMinutes DECIMAL(18,2);
                    DECLARE @Scheduledhours_noperiods DECIMAL(18,2)
                       ,@ActualPresentDays_ConvertTo_Hours_NoPeriods DECIMAL(18,2)
                       ,@ActualAbsentDays_ConvertTo_Hours_NoPeriods DECIMAL(18,2);
                    DECLARE @ActualTardyDays_ConvertTo_Hours_NoPeriods DECIMAL(18,2);
                    DECLARE @boolReset BIT
                       ,@MakeupHours DECIMAL(18,2)
                       ,@AdjustedPresentDaysComputed DECIMAL(18,2);		
                END;

-- Step 3  --  InsertAttendance_Class_PresentAbsent   -- UnitTypeDescrip = ('None', 'Present Absent') 
--         --  TrackSapAttendance = 'byclass'
                BEGIN  -- Step 3  --  InsertAttendance_Class_PresentAbsent   -- UnitTypeDescrip = ('None', 'Present Absent') 
--         --  TrackSapAttendance = 'byclass'
                    IF @UnitTypeDescrip IN ( 'none','present absent' )
                        AND @TrackSapAttendance = 'byclass'
                        BEGIN
			---- PRINT 'Step1!!!!'
                            DELETE  FROM syStudentAttendanceSummary
                            WHERE   StuEnrollId = @StuEnrollId;

                            DECLARE GetAttendance_Cursor CURSOR FORWARD_ONLY FAST_FORWARD
                            FOR
                                SELECT  *
                                       ,ROW_NUMBER() OVER ( ORDER BY MeetDate ) AS RowNumber
                                FROM    (
                                          SELECT DISTINCT
                                                    t1.StuEnrollId
                                                   ,t1.ClsSectionId
                                                   ,t1.MeetDate
                                                   ,DATENAME(dw,t1.MeetDate) AS WeekDay
                                                   ,t4.StartDate
                                                   ,t4.EndDate
                                                   ,t5.PeriodDescrip
                                                   ,t1.Actual
                                                   ,t1.Excused
                                                   ,CASE WHEN (
                                                                t1.Actual = 0
                                                                AND t1.Excused = 0
                                                              ) THEN t1.Scheduled
                                                         ELSE CASE WHEN (
                                                                          t1.Actual <> 9999.00
                                                                          AND t1.Actual < t1.Scheduled
														  --AND t1.Excused <> 1
                                                                        ) THEN ( t1.Scheduled - t1.Actual )
                                                                   ELSE 0
                                                              END
                                                    END AS Absent
                                                   ,t1.Scheduled AS ScheduledMinutes
                                                   ,CASE WHEN (
                                                                t1.Actual > 0
                                                                AND t1.Actual < t1.Scheduled
                                                              ) THEN ( t1.Scheduled - t1.Actual )
                                                         ELSE 0
                                                    END AS TardyMinutes
                                                   ,t1.Tardy AS Tardy
                                                   ,t3.TrackTardies
                                                   ,t3.TardiesMakingAbsence
                                                   ,t3.PrgVerId
                                          FROM      atClsSectAttendance t1
                                          INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                                          INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                                          INNER JOIN arAttUnitType AS AAUT1 ON AAUT1.UnitTypeId = t3.UnitTypeId
                                          INNER JOIN arClsSectMeetings t4 ON t1.ClsSectionId = t4.ClsSectionId
                                                                             AND (
                                                                                   CONVERT(DATE,t1.MeetDate,111) >= CONVERT(DATE,t4.StartDate,111)
                                                                                   AND CONVERT(DATE,t1.MeetDate,111) <= CONVERT(DATE,t4.EndDate,111)
                                                                                 )
                                                                             AND t1.ClsSectMeetingId = t4.ClsSectMeetingId --DE8479 line added
                                          INNER JOIN syPeriods t5 ON t4.PeriodId = t5.PeriodId
                                          INNER JOIN cmTimeInterval t6 ON t5.StartTimeId = t6.TimeIntervalId
                                          INNER JOIN cmTimeInterval t7 ON t5.EndTimeId = t7.TimeIntervalId
                                          INNER JOIN dbo.arClassSections t9 ON t9.ClsSectionId = t1.ClsSectionId
                                          INNER JOIN dbo.arReqs t10 ON t10.ReqId = t9.ReqId
                                          INNER JOIN arAttUnitType AS AAUT2 ON AAUT2.UnitTypeId = t10.UnitTypeId
                                          WHERE     t2.StuEnrollId = @StuEnrollId
                                                    AND (
                                                          AAUT1.UnitTypeDescrip IN ( 'None','Present Absent' )
                                                          OR AAUT2.UnitTypeDescrip IN ( 'None','Present Absent' )
                                                        )
                                                    AND t1.Actual <> 9999
                                        ) dt
                                ORDER BY StuEnrollId
                                       ,MeetDate;
                            OPEN GetAttendance_Cursor;
                            FETCH NEXT FROM GetAttendance_Cursor INTO @StuEnrollId,@ClsSectionId,@MeetDate,@WeekDay,@StartDate,@EndDate,@PeriodDescrip,@Actual,
                                @Excused,@Absent,@ScheduledMinutes,@TardyMinutes,@tardy,@tracktardies,@TardiesMakingAbsence,@PrgVerId,@rownumber;
                            SET @ActualRunningPresentHours = 0;
                            SET @ActualRunningPresentHours = 0;
                            SET @ActualRunningAbsentHours = 0;
                            SET @ActualRunningTardyHours = 0;
                            SET @ActualRunningMakeupHours = 0;
                            SET @intTardyBreakPoint = 0;
                            SET @AdjustedRunningPresentHours = 0;
                            SET @AdjustedRunningAbsentHours = 0;
                            SET @ActualRunningScheduledDays = 0;
                            SET @boolReset = 0;
                            SET @MakeupHours = 0;
                            WHILE @@FETCH_STATUS = 0
                                BEGIN

                                    IF @PrevStuEnrollId <> @StuEnrollId
                                        BEGIN
                                            SET @ActualRunningPresentHours = 0;
                                            SET @ActualRunningAbsentHours = 0;
                                            SET @intTardyBreakPoint = 0;
                                            SET @ActualRunningTardyHours = 0;
                                            SET @AdjustedRunningPresentHours = 0;
                                            SET @AdjustedRunningAbsentHours = 0;
                                            SET @ActualRunningScheduledDays = 0;
                                            SET @MakeupHours = 0;
                                            SET @boolReset = 1;
                                        END;

					  
					  -- Scheduled and Actual: Calculate Students Actual and Adjusted Running Present Hours/Min/Day
                                    IF @Actual <> 9999.00
                                        BEGIN
					-- Commented by Balaji on 2.6.2015 as Excused Absence is still considered an absence
					-- @Excused is not added to Present Hours
                                            SET @ActualRunningPresentHours = @ActualRunningPresentHours + @Actual;  -- + @Excused
                                            SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours + @Actual; -- + @Excused
					
					-- If there are make up hrs deduct that otherwise it will be added again in progress report
                                            IF (
                                                 @Actual > 0
                                                 AND @Actual > @ScheduledMinutes
                                                 AND @Actual <> 9999.00
                                               )
                                                BEGIN
                                                    SET @ActualRunningPresentHours = @ActualRunningPresentHours - ( @Actual - @ScheduledMinutes );
                                                    SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - ( @Actual - @ScheduledMinutes );
                                                END;
					  
                                            SET @ActualRunningScheduledDays = @ActualRunningScheduledDays + @ScheduledMinutes;                      
                                        END;
				   
					-- Absent: Calculate Students Actual and Adjusted Running Absent Hours/Min/Day
                                    SET @ActualRunningAbsentHours = @ActualRunningAbsentHours + @Absent;
                                    SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Absent;	
			  
					-- Tardy: Calculate Students Actual Running Tardy Hours/Min/Day			
                                    IF (
                                         @Actual > 0
                                         AND @Actual < @ScheduledMinutes
                                         AND @tardy = 1
                                       )
                                        BEGIN
                                            SET @ActualRunningTardyHours = @ActualRunningTardyHours + ( @ScheduledMinutes - @Actual );
                                        END;
                                    ELSE
                                        IF (
                                             @Actual = 1
                                             AND @Actual = @ScheduledMinutes
                                             AND @tardy = 1
                                           )
                                            BEGIN
                                                SET @ActualRunningTardyHours += 1;
                                            END;
							
					-- Track how many days student has been tardy only when 
					-- program version requires to track tardy
                                    IF (
                                         @tracktardies = 1
                                         AND @tardy = 1
                                       )
                                        BEGIN
                                            SET @intTardyBreakPoint = @intTardyBreakPoint + 1;
                                        END;	    
		   
			
					-- Tardy: If student is tardy multiple times and when the number of days student is tardy matches
					-- the tardy rule set (for ex: 2 Tardies make 1 Absence). This rule indicates that
					-- when student is tardy the second time, that second occurance will be considered as
					-- absence
					-- Variable @intTardyBreakpoint tracks how many times the student was tardy
					-- Variable @tardiesMakingAbsence tracks the tardy rule
                                    IF (
                                         @tracktardies = 1
                                         AND @intTardyBreakPoint = @TardiesMakingAbsence
                                       )
                                        BEGIN
                                            SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - @Actual - @Excused;
                                            SET @AdjustedRunningAbsentHours = ( @AdjustedRunningAbsentHours - @Absent ) + @ScheduledMinutes; --@TardyMinutes
                                            SET @intTardyBreakPoint = 0;
                                        END;
				   
				   -- Makeup: Calculate Makeup Hours when Student Attended more than scheduled hours          	       
                                    IF (
                                         @Actual > 0
                                         AND @Actual > @ScheduledMinutes
                                         AND @Actual <> 9999.00
                                       )
                                        BEGIN
                                            SET @MakeupHours = @MakeupHours + ( @Actual - @ScheduledMinutes );
                                        END;
			  
                                    IF ( @tracktardies = 1 )
                                        BEGIN
                                            SET @AdjustedPresentDaysComputed = @AdjustedRunningPresentHours; --+IsNULL(@ActualRunningTardyHours,0)
                                        END;
                                    ELSE
                                        BEGIN
                                            SET @AdjustedPresentDaysComputed = @AdjustedRunningPresentHours;
                                        END;
				
				---- PRINT @MeetDate
				---- PRINT @ActualRunningAbsentHours
				
                                    DELETE  FROM syStudentAttendanceSummary
                                    WHERE   StuEnrollId = @StuEnrollId
                                            AND ClsSectionId = @ClsSectionId
                                            AND StudentAttendedDate = @MeetDate;
                                    INSERT  INTO syStudentAttendanceSummary
                                            (
                                             StuEnrollId
                                            ,ClsSectionId
                                            ,StudentAttendedDate
                                            ,ScheduledDays
                                            ,ActualDays
                                            ,ActualRunningScheduledDays
                                            ,ActualRunningPresentDays
                                            ,ActualRunningAbsentDays
                                            ,ActualRunningMakeupDays
                                            ,ActualRunningTardyDays
                                            ,AdjustedPresentDays
                                            ,AdjustedAbsentDays
                                            ,AttendanceTrackType
                                            ,ModUser
                                            ,ModDate
                                            ,tardiesmakingabsence
										
                                            )
                                    VALUES  (
                                             @StuEnrollId
                                            ,@ClsSectionId
                                            ,@MeetDate
                                            ,@ScheduledMinutes
                                            ,@Actual
                                            ,@ActualRunningScheduledDays
                                            ,@ActualRunningPresentHours
                                            ,@ActualRunningAbsentHours
                                            ,ISNULL(@MakeupHours,0)
                                            ,@ActualRunningTardyHours
                                            ,@AdjustedPresentDaysComputed
                                            ,@AdjustedRunningAbsentHours
                                            ,'Post Attendance by Class Min'
                                            ,'sa'
                                            ,GETDATE()
                                            ,@TardiesMakingAbsence
										
                                            );

				--update syStudentAttendanceSummary set TardiesMakingAbsence=@TardiesMakingAbsence where StuEnrollId=@StuEnrollId
                                    SET @PrevStuEnrollId = @StuEnrollId; 

                                    FETCH NEXT FROM GetAttendance_Cursor INTO @StuEnrollId,@ClsSectionId,@MeetDate,@WeekDay,@StartDate,@EndDate,@PeriodDescrip,
                                        @Actual,@Excused,@Absent,@ScheduledMinutes,@TardyMinutes,@tardy,@tracktardies,@TardiesMakingAbsence,@PrgVerId,@rownumber;
                                END;
                            CLOSE GetAttendance_Cursor;
                            DEALLOCATE GetAttendance_Cursor;
			
                            DECLARE @MyTardyTable TABLE
                                (
                                 ClsSectionId UNIQUEIDENTIFIER
                                ,TardiesMakingAbsence INT
                                ,AbsentHours DECIMAL(18,2)
                                ); 

                            INSERT  INTO @MyTardyTable
                                    SELECT  ClsSectionId
                                           ,PV.TardiesMakingAbsence
                                           ,CASE WHEN ( COUNT(*) >= PV.TardiesMakingAbsence ) THEN ( COUNT(*) / PV.TardiesMakingAbsence )
                                                 ELSE 0
                                            END AS AbsentHours 
--Count(*) as NumberofTimesTardy 
                                    FROM    syStudentAttendanceSummary SAS
                                    INNER JOIN arStuEnrollments SE ON SAS.StuEnrollId = SE.StuEnrollId
                                    INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                                    WHERE   SE.StuEnrollId = @StuEnrollId
                                            AND SAS.IsTardy = 1
                                            AND PV.TrackTardies = 1
                                    GROUP BY ClsSectionId
                                           ,PV.TardiesMakingAbsence; 

--Drop table @MyTardyTable
                            DECLARE @TotalTardyAbsentDays DECIMAL(18,2);
                            SET @TotalTardyAbsentDays = (
                                                          SELECT    ISNULL(SUM(AbsentHours),0)
                                                          FROM      @MyTardyTable
                                                        );

--Print @TotalTardyAbsentDays

                            UPDATE  syStudentAttendanceSummary
                            SET     AdjustedPresentDays = AdjustedPresentDays - @TotalTardyAbsentDays
                                   ,AdjustedAbsentDays = AdjustedAbsentDays + @TotalTardyAbsentDays
                            WHERE   StuEnrollId = @StuEnrollId
                                    AND StudentAttendedDate = (
                                                                SELECT TOP 1
                                                                        StudentAttendedDate
                                                                FROM    syStudentAttendanceSummary
                                                                WHERE   StuEnrollId = @StuEnrollId
                                                                ORDER BY StudentAttendedDate DESC
                                                              );
			
                        END;	
                END; -- Step 3 
-- END -- Step 3 

-- Step 4  --  InsertAttendance_Class_Minutes
--         --  TrackSapAttendance = 'byclass'
                BEGIN  -- Step 4  --  InsertAttendance_Class_Minutes
		-- By Class and Attendance Unit Type - Minutes and Clock Hour
		
                    IF @UnitTypeDescrip IN ( 'minutes','clock hours' )
                        AND @TrackSapAttendance = 'byclass'
                        BEGIN
                            DELETE  FROM syStudentAttendanceSummary
                            WHERE   StuEnrollId = @StuEnrollId;
				
                            DECLARE GetAttendance_Cursor CURSOR
                            FOR
                                SELECT  *
                                       ,ROW_NUMBER() OVER ( ORDER BY MeetDate ) AS RowNumber
                                FROM    (
                                          SELECT DISTINCT
                                                    t1.StuEnrollId
                                                   ,t1.ClsSectionId
                                                   ,t1.MeetDate
                                                   ,DATENAME(dw,t1.MeetDate) AS WeekDay
                                                   ,t4.StartDate
                                                   ,t4.EndDate
                                                   ,t5.PeriodDescrip
                                                   ,t1.Actual
                                                   ,t1.Excused
                                                   ,CASE WHEN (
                                                                t1.Actual = 0
                                                                AND t1.Excused = 0
                                                              ) THEN t1.Scheduled
                                                         ELSE CASE WHEN (
                                                                          t1.Actual <> 9999.00
                                                                          AND t1.Actual < t1.Scheduled
                                                                        ) THEN ( t1.Scheduled - t1.Actual )
                                                                   ELSE 0
                                                              END
                                                    END AS Absent
                                                   ,t1.Scheduled AS ScheduledMinutes
                                                   ,CASE WHEN (
                                                                t1.Actual > 0
                                                                AND t1.Actual < t1.Scheduled
                                                              ) THEN ( t1.Scheduled - t1.Actual )
                                                         ELSE 0
                                                    END AS TardyMinutes
                                                   ,t1.Tardy AS Tardy
                                                   ,t3.TrackTardies
                                                   ,t3.TardiesMakingAbsence
                                                   ,t3.PrgVerId
                                          FROM      atClsSectAttendance t1
                                          INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                                          INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                                          INNER JOIN arAttUnitType AS AAUT1 ON AAUT1.UnitTypeId = t3.UnitTypeId
                                          INNER JOIN arClsSectMeetings t4 ON t1.ClsSectionId = t4.ClsSectionId
                                                                             AND (
                                                                                   CONVERT(DATE,t1.MeetDate,111) >= CONVERT(DATE,t4.StartDate,111)
                                                                                   AND CONVERT(DATE,t1.MeetDate,111) <= CONVERT(DATE,t4.EndDate,111)
                                                                                 )
                                                                             AND t1.ClsSectMeetingId = t4.ClsSectMeetingId --DE8479 line added
                                          INNER JOIN syPeriods t5 ON t4.PeriodId = t5.PeriodId
                                          INNER JOIN cmTimeInterval t6 ON t5.StartTimeId = t6.TimeIntervalId
                                          INNER JOIN cmTimeInterval t7 ON t5.EndTimeId = t7.TimeIntervalId
                                          INNER JOIN dbo.arClassSections t9 ON t9.ClsSectionId = t1.ClsSectionId
                                          INNER JOIN dbo.arReqs t10 ON t10.ReqId = t9.ReqId
                                          INNER JOIN arAttUnitType AS AAUT2 ON AAUT2.UnitTypeId = t10.UnitTypeId
                                          WHERE     t2.StuEnrollId = @StuEnrollId
                                                    AND (
                                                          AAUT1.UnitTypeDescrip IN ( 'Minutes','Clock Hours' )
                                                          OR AAUT2.UnitTypeDescrip IN ( 'Minutes','Clock Hours' )
                                                        )
                                                    AND t1.Actual <> 9999
                                          UNION
                                          SELECT DISTINCT
                                                    t1.StuEnrollId
                                                   ,NULL AS ClsSectionId
                                                   ,t1.RecordDate AS MeetDate
                                                   ,DATENAME(dw,t1.RecordDate) AS WeekDay
                                                   ,NULL AS StartDate
                                                   ,NULL AS EndDate
                                                   ,NULL AS PeriodDescrip
                                                   ,t1.ActualHours
                                                   ,NULL AS Excused
                                                   ,CASE WHEN ( t1.ActualHours = 0 ) THEN t1.SchedHours
                                                         ELSE 0
								 --ELSE 
									--Case when (t1.ActualHours <> 9999.00 and t1.ActualHours < t1.SchedHours)
									--		THEN (t1.SchedHours - t1.ActualHours)
									--		ELSE 
									--			0
									--		End
                                                    END AS Absent
                                                   ,t1.SchedHours AS ScheduledMinutes
                                                   ,CASE WHEN (
                                                                t1.ActualHours <> 9999.00
                                                                AND t1.ActualHours > 0
                                                                AND t1.ActualHours < t1.SchedHours
                                                              ) THEN ( t1.SchedHours - t1.ActualHours )
                                                         ELSE 0
                                                    END AS TardyMinutes
                                                   ,NULL AS Tardy
                                                   ,t3.TrackTardies
                                                   ,t3.TardiesMakingAbsence
                                                   ,t3.PrgVerId
                                          FROM      arStudentClockAttendance t1
                                          INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                                          INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                                          WHERE     t2.StuEnrollId = @StuEnrollId
                                                    AND t1.Converted = 1
                                                    AND t1.ActualHours <> 9999
                                        ) dt
                                ORDER BY StuEnrollId
                                       ,MeetDate;
                            OPEN GetAttendance_Cursor;
                            FETCH NEXT FROM GetAttendance_Cursor INTO @StuEnrollId,@ClsSectionId,@MeetDate,@WeekDay,@StartDate,@EndDate,@PeriodDescrip,@Actual,
                                @Excused,@Absent,@ScheduledMinutes,@TardyMinutes,@tardy,@tracktardies,@TardiesMakingAbsence,@PrgVerId,@rownumber;
                            SET @ActualRunningPresentHours = 0;
                            SET @ActualRunningPresentHours = 0;
                            SET @ActualRunningAbsentHours = 0;
                            SET @ActualRunningTardyHours = 0;
                            SET @ActualRunningMakeupHours = 0;
                            SET @intTardyBreakPoint = 0;
                            SET @AdjustedRunningPresentHours = 0;
                            SET @AdjustedRunningAbsentHours = 0;
                            SET @ActualRunningScheduledDays = 0;
                            SET @boolReset = 0;
                            SET @MakeupHours = 0;
                            WHILE @@FETCH_STATUS = 0
                                BEGIN

                                    IF @PrevStuEnrollId <> @StuEnrollId
                                        BEGIN
                                            SET @ActualRunningPresentHours = 0;
                                            SET @ActualRunningAbsentHours = 0;
                                            SET @intTardyBreakPoint = 0;
                                            SET @ActualRunningTardyHours = 0;
                                            SET @AdjustedRunningPresentHours = 0;
                                            SET @AdjustedRunningAbsentHours = 0;
                                            SET @ActualRunningScheduledDays = 0;
                                            SET @MakeupHours = 0;
                                            SET @boolReset = 1;
                                        END;

					  
			-- Scheduled and Actual: Calculate Students Actual and Adjusted Running Present Hours/Min/Day
                                    IF @Actual <> 9999.00
                                        BEGIN
                                            SET @ActualRunningPresentHours = @ActualRunningPresentHours + @Actual;
                                            SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours + @Actual; 
					-- If there are make up hrs deduct that otherwise it will be added again in progress report
                                            IF (
                                                 @Actual > 0
                                                 AND @Actual > @ScheduledMinutes
                                                 AND @Actual <> 9999.00
                                               )
                                                BEGIN
                                                    SET @ActualRunningPresentHours = @ActualRunningPresentHours - ( @Actual - @ScheduledMinutes );
                                                    SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - ( @Actual - @ScheduledMinutes );
                                                END; 
                                            SET @ActualRunningScheduledDays = @ActualRunningScheduledDays + @ScheduledMinutes;                      
                                        END;
		   
			-- Absent: Calculate Students Actual and Adjusted Running Absent Hours/Min/Day
                                    SET @ActualRunningAbsentHours = @ActualRunningAbsentHours + @Absent;
                                    SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Absent;	
	  
			-- Tardy: Calculate Students Actual Running Tardy Hours/Min/Day			
                                    IF (
                                         @Actual > 0
                                         AND @Actual < @ScheduledMinutes
                                         AND @tardy = 1
                                       )
                                        BEGIN
                                            SET @ActualRunningTardyHours = @ActualRunningTardyHours + ( @ScheduledMinutes - @Actual );
                                        END;
					
			-- Track how many days student has been tardy only when 
			-- program version requires to track tardy
                                    IF (
                                         @tracktardies = 1
                                         AND @tardy = 1
                                       )
                                        BEGIN
                                            SET @intTardyBreakPoint = @intTardyBreakPoint + 1;
                                        END;	    
		   
			
			-- Tardy: If student is tardy multiple times and when the number of days student is tardy matches
			-- the tardy rule set (for ex: 2 Tardies make 1 Absence). This rule indicates that
			-- when student is tardy the second time, that second occurance will be considered as
			-- absence
			-- Variable @intTardyBreakpoint tracks how many times the student was tardy
			-- Variable @tardiesMakingAbsence tracks the tardy rule
                                    IF (
                                         @tracktardies = 1
                                         AND @intTardyBreakPoint = @TardiesMakingAbsence
                                       )
                                        BEGIN
                                            SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - @Actual;
                                            SET @AdjustedRunningAbsentHours = ( @AdjustedRunningAbsentHours - @Absent ) + @ScheduledMinutes; --@TardyMinutes
                                            SET @intTardyBreakPoint = 0;
                                        END;
		   
		   -- Makeup: Calculate Makeup Hours when Student Attended more than scheduled hours          	       
                                    IF (
                                         @Actual > 0
                                         AND @Actual > @ScheduledMinutes
                                         AND @Actual <> 9999.00
                                       )
                                        BEGIN
                                            SET @MakeupHours = @MakeupHours + ( @Actual - @ScheduledMinutes );
                                        END;
	  
	 
                                    IF ( @tracktardies = 1 )
                                        BEGIN
                                            SET @AdjustedPresentDaysComputed = @AdjustedRunningPresentHours; --+IsNULL(@ActualRunningTardyHours,0)
                                        END;
                                    ELSE
                                        BEGIN
                                            SET @AdjustedPresentDaysComputed = @AdjustedRunningPresentHours;
                                        END;
		
		
		
		
                                    DELETE  FROM syStudentAttendanceSummary
                                    WHERE   StuEnrollId = @StuEnrollId
                                            AND ClsSectionId = @ClsSectionId
                                            AND StudentAttendedDate = @MeetDate;
                                    INSERT  INTO syStudentAttendanceSummary
                                            (
                                             StuEnrollId
                                            ,ClsSectionId
                                            ,StudentAttendedDate
                                            ,ScheduledDays
                                            ,ActualDays
                                            ,ActualRunningScheduledDays
                                            ,ActualRunningPresentDays
                                            ,ActualRunningAbsentDays
                                            ,ActualRunningMakeupDays
                                            ,ActualRunningTardyDays
                                            ,AdjustedPresentDays
                                            ,AdjustedAbsentDays
                                            ,AttendanceTrackType
                                            ,ModUser
                                            ,ModDate
										
                                            )
                                    VALUES  (
                                             @StuEnrollId
                                            ,@ClsSectionId
                                            ,@MeetDate
                                            ,@ScheduledMinutes
                                            ,@Actual
                                            ,@ActualRunningScheduledDays
                                            ,@ActualRunningPresentHours
                                            ,@ActualRunningAbsentHours
                                            ,ISNULL(@MakeupHours,0)
                                            ,@ActualRunningTardyHours
                                            ,@AdjustedPresentDaysComputed
                                            ,@AdjustedRunningAbsentHours
                                            ,'Post Attendance by Class Min'
                                            ,'sa'
                                            ,GETDATE()
                                            );

                                    UPDATE  syStudentAttendanceSummary
                                    SET     tardiesmakingabsence = @TardiesMakingAbsence
                                    WHERE   StuEnrollId = @StuEnrollId;
                                    SET @PrevStuEnrollId = @StuEnrollId; 

                                    FETCH NEXT FROM GetAttendance_Cursor INTO @StuEnrollId,@ClsSectionId,@MeetDate,@WeekDay,@StartDate,@EndDate,@PeriodDescrip,
                                        @Actual,@Excused,@Absent,@ScheduledMinutes,@TardyMinutes,@tardy,@tracktardies,@TardiesMakingAbsence,@PrgVerId,@rownumber;
                                END;
                            CLOSE GetAttendance_Cursor;
                            DEALLOCATE GetAttendance_Cursor;
                        END;
                END; --  Step 3  --   InsertAttendance_Class_Minutes
--END --  Step 3  --   InsertAttendance_Class_Minutes


--Select * from arAttUnitType

			
-- By Minutes/Day
-- remove clock hour from here
                IF @UnitTypeDescrip IN ( 'minutes' )
                    AND @TrackSapAttendance = 'byday'
	-- -- PRINT GETDATE();	
	---- PRINT @UnitTypeId;
	---- PRINT @TrackSapAttendance;
                    BEGIN
			--Delete from syStudentAttendanceSummary where StuEnrollId=@StuEnrollId

                        DECLARE GetAttendance_Cursor CURSOR FORWARD_ONLY FAST_FORWARD
                        FOR
                            SELECT  t1.StuEnrollId
                                   ,NULL AS ClsSectionId
                                   ,t1.RecordDate AS MeetDate
                                   ,t1.ActualHours
                                   ,t1.SchedHours AS ScheduledMinutes
                                   ,CASE WHEN (
                                                (
                                                  t1.SchedHours >= 1
                                                  AND t1.SchedHours NOT IN ( 999,9999 )
                                                )
                                                AND t1.ActualHours = 0
                                              ) THEN t1.SchedHours
                                         ELSE 0
                                    END AS Absent
                                   ,t1.isTardy
                                   ,(
                                      SELECT    ISNULL(SUM(SchedHours),0)
                                      FROM      arStudentClockAttendance
                                      WHERE     StuEnrollId = t1.StuEnrollId
                                                AND RecordDate <= t1.RecordDate
                                                AND (
                                                      t1.SchedHours >= 1
                                                      AND t1.SchedHours NOT IN ( 999,9999 )
                                                      AND t1.ActualHours NOT IN ( 999,9999 )
                                                    )
                                    ) AS ActualRunningScheduledHours
                                   ,(
                                      SELECT    SUM(ActualHours)
                                      FROM      arStudentClockAttendance
                                      WHERE     StuEnrollId = t1.StuEnrollId
                                                AND RecordDate <= t1.RecordDate
                                                AND (
                                                      t1.SchedHours >= 1
                                                      AND t1.SchedHours NOT IN ( 999,9999 )
                                                    )
                                                AND ActualHours >= 1
                                                AND ActualHours NOT IN ( 999,9999 )
                                    ) AS ActualRunningPresentHours
                                   ,(
                                      SELECT    COUNT(ActualHours)
                                      FROM      arStudentClockAttendance
                                      WHERE     StuEnrollId = t1.StuEnrollId
                                                AND RecordDate <= t1.RecordDate
                                                AND (
                                                      t1.SchedHours >= 1
                                                      AND t1.SchedHours NOT IN ( 999,9999 )
                                                    )
                                                AND ActualHours = 0
                                                AND ActualHours NOT IN ( 999,9999 )
                                    ) AS ActualRunningAbsentHours
                                   ,(
                                      SELECT    SUM(ActualHours)
                                      FROM      arStudentClockAttendance
                                      WHERE     StuEnrollId = t1.StuEnrollId
                                                AND RecordDate <= t1.RecordDate
                                                AND SchedHours = 0
                                                AND ActualHours >= 1
                                                AND ActualHours NOT IN ( 999,9999 )
                                    ) AS ActualRunningMakeupHours
                                   ,(
                                      SELECT    SUM(SchedHours - ActualHours)
                                      FROM      arStudentClockAttendance
                                      WHERE     StuEnrollId = t1.StuEnrollId
                                                AND RecordDate <= t1.RecordDate
                                                AND (
                                                      (
                                                        t1.SchedHours >= 1
                                                        AND t1.SchedHours NOT IN ( 999,9999 )
                                                      )
                                                      AND ActualHours >= 1
                                                      AND ActualHours NOT IN ( 999,9999 )
                                                    )
                                                AND isTardy = 1
                                    ) AS ActualRunningTardyHours
                                   ,t3.TrackTardies
                                   ,t3.TardiesMakingAbsence
                                   ,t3.PrgVerId
                                   ,ROW_NUMBER() OVER ( ORDER BY t1.RecordDate ) AS RowNumber
                            FROM    arStudentClockAttendance t1
                            INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                            INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                            INNER JOIN arAttUnitType AS AAUT1 ON AAUT1.UnitTypeId = t3.UnitTypeId
			--inner join Inserted t4 on t1.StuEnrollId = t4.StuEnrollId and t1.RecordDate=t4.RecordDate
                            WHERE   AAUT1.UnitTypeDescrip IN ( 'Minutes' )
                                    AND t2.StuEnrollId = @StuEnrollId
                                    AND t1.ActualHours <> 9999.00
                            ORDER BY t1.StuEnrollId
                                   ,MeetDate;
                        OPEN GetAttendance_Cursor;
--Declare @ActualHours decimal(18,2),@SchedHours decimal(18,2)
                        FETCH NEXT FROM GetAttendance_Cursor INTO @StuEnrollId,@ClsSectionId,@MeetDate,@Actual,@ScheduledMinutes,@Absent,@IsTardy,
                            @ActualRunningScheduledHours,@ActualRunningPresentHours,@ActualRunningAbsentHours,@ActualRunningMakeupHours,@ActualRunningTardyHours,
                            @tracktardies,@TardiesMakingAbsence,@PrgVerId,@rownumber;

                        SET @ActualRunningPresentHours = 0;
                        SET @ActualRunningPresentHours = 0;
                        SET @ActualRunningAbsentHours = 0;
                        SET @ActualRunningTardyHours = 0;
                        SET @ActualRunningMakeupHours = 0;
                        SET @intTardyBreakPoint = 0;
                        SET @AdjustedRunningPresentHours = 0;
                        SET @AdjustedRunningAbsentHours = 0;
                        SET @ActualRunningScheduledDays = 0;
                        WHILE @@FETCH_STATUS = 0
                            BEGIN

                                IF @PrevStuEnrollId <> @StuEnrollId
                                    BEGIN
                                        SET @ActualRunningPresentHours = 0;
                                        SET @ActualRunningAbsentHours = 0;
                                        SET @intTardyBreakPoint = 0;
                                        SET @ActualRunningTardyHours = 0;
                                        SET @AdjustedRunningPresentHours = 0;
                                        SET @AdjustedRunningAbsentHours = 0;
                                        SET @ActualRunningScheduledDays = 0;
                                    END;

                                IF (
                                     @ScheduledMinutes >= 1
                                     AND (
                                           @Actual <> 9999
                                           AND @Actual <> 999
                                         )
                                     AND (
                                           @ScheduledMinutes <> 9999
                                           AND @Actual <> 999
                                         )
                                   )
                                    BEGIN
                                        SET @ActualRunningScheduledDays = ISNULL(@ActualRunningScheduledDays,0) + ISNULL(@ScheduledMinutes,0);
                                    END;
                                ELSE
                                    BEGIN
                                        SET @ActualRunningScheduledDays = ISNULL(@ActualRunningScheduledDays,0); 
                                    END;
	   
                                IF (
                                     @Actual <> 9999
                                     AND @Actual <> 999
                                   )
                                    BEGIN
                                        SET @ActualRunningPresentHours = @ActualRunningPresentHours + @Actual;
                                    END;
                                SET @ActualRunningAbsentHours = @ActualRunningAbsentHours + @Absent;

                                IF (
                                     @Actual > 0
                                     AND @Actual < @ScheduledMinutes
                                   )
                                    BEGIN
                                        SET @ActualRunningAbsentHours = @ActualRunningAbsentHours + ( @ScheduledMinutes - @Actual );
                                    END;
			-- Make up hours
		--sched=5, Actual =7, makeup= 2,ab = 0
		--sched=0, Actual =7, makeup= 7,ab = 0
                                IF (
                                     @Actual > 0
                                     AND @ScheduledMinutes > 0
                                     AND @Actual > @ScheduledMinutes
                                     AND (
                                           @Actual <> 9999
                                           AND @Actual <> 999
                                         )
                                   )
                                    BEGIN
                                        SET @ActualRunningMakeupHours = @ActualRunningMakeupHours + ( @Actual - @ScheduledMinutes );
                                    END;

		
                                IF (
                                     @Actual <> 9999
                                     AND @Actual <> 999
                                   )
                                    BEGIN
                                        SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours + @Actual;
                                    END;	
                                IF (
                                     @Actual = 0
                                     AND @ScheduledMinutes >= 1
                                     AND @ScheduledMinutes NOT IN ( 999,9999 )
                                   )
                                    BEGIN
                                        SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Absent;
                                    END;
		-- Absent hours
		--1. sched = 5, Actual = 2 then Ab = (5-3) = 2
                                IF (
                                     @Absent = 0
                                     AND @ActualRunningAbsentHours > 0
                                     AND ( @Actual < @ScheduledMinutes )
                                   )
                                    BEGIN
                                        SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + ( @ScheduledMinutes - @Actual );	
                                    END;
                                IF (
                                     @Actual > 0
                                     AND @Actual < @ScheduledMinutes
                                     AND (
                                           @Actual <> 9999.00
                                           AND @Actual <> 999.00
                                         )
                                   )
                                    BEGIN
                                        SET @ActualRunningTardyHours = @ActualRunningTardyHours + ( @ScheduledMinutes - @Actual );
                                    END;
	
                                IF @tracktardies = 1
                                    AND (
                                          @TardyMinutes > 0
                                          OR @IsTardy = 1
                                        )
                                    BEGIN
                                        SET @intTardyBreakPoint = @intTardyBreakPoint + 1;
                                    END;	    
                                IF (
                                     @tracktardies = 1
                                     AND @intTardyBreakPoint = @TardiesMakingAbsence
                                   )
                                    BEGIN
                                        SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - @Actual;
                                        SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Actual; --@TardyMinutes
                                        SET @intTardyBreakPoint = 0;
                                    END;
                                DELETE  FROM syStudentAttendanceSummary
                                WHERE   StuEnrollId = @StuEnrollId
                                        AND StudentAttendedDate = @MeetDate;
                                INSERT  INTO syStudentAttendanceSummary
                                        (
                                         StuEnrollId
                                        ,ClsSectionId
                                        ,StudentAttendedDate
                                        ,ScheduledDays
                                        ,ActualDays
                                        ,ActualRunningScheduledDays
                                        ,ActualRunningPresentDays
                                        ,ActualRunningAbsentDays
                                        ,ActualRunningMakeupDays
                                        ,ActualRunningTardyDays
                                        ,AdjustedPresentDays
                                        ,AdjustedAbsentDays
                                        ,AttendanceTrackType
                                        ,ModUser
                                        ,ModDate
										)
                                VALUES  (
                                         @StuEnrollId
                                        ,@ClsSectionId
                                        ,@MeetDate
                                        ,@ScheduledMinutes
                                        ,@Actual
                                        ,@ActualRunningScheduledDays
                                        ,@ActualRunningPresentHours
                                        ,@ActualRunningAbsentHours
                                        ,@ActualRunningMakeupHours
                                        ,@ActualRunningTardyHours
                                        ,@AdjustedRunningPresentHours
                                        ,@AdjustedRunningAbsentHours
                                        ,'Post Attendance by Class'
                                        ,'sa'
                                        ,GETDATE()
                                        );
				
                                UPDATE  syStudentAttendanceSummary
                                SET     tardiesmakingabsence = @TardiesMakingAbsence
                                WHERE   StuEnrollId = @StuEnrollId;
			--end
                                SET @PrevStuEnrollId = @StuEnrollId; 
                                FETCH NEXT FROM GetAttendance_Cursor INTO @StuEnrollId,@ClsSectionId,@MeetDate,@Actual,@ScheduledMinutes,@Absent,@IsTardy,
                                    @ActualRunningScheduledHours,@ActualRunningPresentHours,@ActualRunningAbsentHours,@ActualRunningMakeupHours,
                                    @ActualRunningTardyHours,@tracktardies,@TardiesMakingAbsence,@PrgVerId,@rownumber;
                            END;
                        CLOSE GetAttendance_Cursor;
                        DEALLOCATE GetAttendance_Cursor;
                    END;

	---- PRINT 'Does it get to Clock Hour/PA';
	---- PRINT @UnitTypeId;
	---- PRINT @TrackSapAttendance;
	-- By Day and PA, Clock Hour
	-- -- Step 2  --  InsertAttendance_Day_PresentAbsent  -- UnitTypeDescrip = ('Present Absent', 'Clock Hours')  and TrackSapAttendance = 'byday'
                BEGIN -- Step 2  --  InsertAttendance_Day_PresentAbsent  -- UnitTypeDescrip = ('Present Absent', 'Clock Hours') and TrackSapAttendance = 'byday'
                    IF @UnitTypeDescrip IN ( 'present absent','clock hours' )
                        AND @TrackSapAttendance = 'byday'
                        BEGIN
			---- PRINT 'By Day inside day';
                            DECLARE GetAttendance_Cursor CURSOR
                            FOR
                                SELECT  t1.StuEnrollId
                                       ,t1.RecordDate
                                       ,t1.ActualHours
                                       ,t1.SchedHours
                                       ,CASE WHEN (
                                                    (
                                                      t1.SchedHours >= 1
                                                      AND t1.SchedHours NOT IN ( 999,9999 )
                                                    )
                                                    AND t1.ActualHours = 0
                                                  ) THEN t1.SchedHours
                                             ELSE 0
                                        END AS Absent
                                       ,t1.isTardy
                                       ,t3.TrackTardies
                                       ,t3.TardiesMakingAbsence
                                FROM    arStudentClockAttendance t1
                                INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                                INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                                INNER JOIN arAttUnitType AS AAUT1 ON AAUT1.UnitTypeId = t3.UnitTypeId
			--inner join Inserted t4 on t1.StuEnrollId = t4.StuEnrollId and t1.RecordDate=t4.RecordDate
                                WHERE   -- Unit Types: Present Absent and Clock Hour
                                        AAUT1.UnitTypeDescrip IN ( 'present absent','clock hours' )
                                        AND ActualHours <> 9999.00
                                        AND t2.StuEnrollId = @StuEnrollId
                                ORDER BY t1.StuEnrollId
                                       ,t1.RecordDate;
                            OPEN GetAttendance_Cursor;
--Declare @ActualHours decimal(18,2),@SchedHours decimal(18,2)
                            FETCH NEXT FROM GetAttendance_Cursor INTO @StuEnrollId,@MeetDate,@Actual,@ScheduledMinutes,@Absent,@IsTardy,@tracktardies,
                                @TardiesMakingAbsence;

                            SET @ActualRunningPresentHours = 0;
                            SET @ActualRunningPresentHours = 0;
                            SET @ActualRunningAbsentHours = 0;
                            SET @ActualRunningTardyHours = 0;
                            SET @ActualRunningMakeupHours = 0;
                            SET @intTardyBreakPoint = 0;
                            SET @AdjustedRunningPresentHours = 0;
                            SET @AdjustedRunningAbsentHours = 0;
                            SET @ActualRunningScheduledDays = 0;
                            SET @MakeupHours = 0;
                            WHILE @@FETCH_STATUS = 0
                                BEGIN

                                    IF @PrevStuEnrollId <> @StuEnrollId
                                        BEGIN
                                            SET @ActualRunningPresentHours = 0;
                                            SET @ActualRunningAbsentHours = 0;
                                            SET @intTardyBreakPoint = 0;
                                            SET @ActualRunningTardyHours = 0;
                                            SET @AdjustedRunningPresentHours = 0;
                                            SET @AdjustedRunningAbsentHours = 0;
                                            SET @ActualRunningScheduledDays = 0;
                                            SET @MakeupHours = 0;
                                        END;
	   
                                    IF (
                                         @Actual <> 9999
                                         AND @Actual <> 999
                                       )
                                        BEGIN
                                            SET @ActualRunningScheduledDays = ISNULL(@ActualRunningScheduledDays,0) + @ScheduledMinutes;
                                            SET @ActualRunningPresentHours = ISNULL(@ActualRunningPresentHours,0) + @Actual;
                                            SET @AdjustedRunningPresentHours = ISNULL(@AdjustedRunningPresentHours,0) + @Actual;
                                        END;
                                    SET @ActualRunningAbsentHours = ISNULL(@ActualRunningAbsentHours,0) + @Absent;
                                    IF (
                                         @Actual = 0
                                         AND @ScheduledMinutes >= 1
                                         AND @ScheduledMinutes NOT IN ( 999,9999 )
                                       )
                                        BEGIN
                                            SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Absent;
                                        END;
		
		-- NWH 
		-- If Scheduled = 3.0 hrs and Actual = 1.0 Then 2 hrs is considered absent
                                    IF (
                                         @ScheduledMinutes >= 1
                                         AND @ScheduledMinutes NOT IN ( 999,9999 )
                                         AND @Actual > 0
                                         AND ( @Actual < @ScheduledMinutes )
                                       )
                                        BEGIN
                                            SET @ActualRunningAbsentHours = ISNULL(@ActualRunningAbsentHours,0) + ( @ScheduledMinutes - @Actual );
                                            SET @AdjustedRunningAbsentHours = ISNULL(@AdjustedRunningAbsentHours,0) + ( @ScheduledMinutes - @Actual );
                                        END; 
		
                                    IF (
                                         @tracktardies = 1
                                         AND @IsTardy = 1
                                       )
                                        BEGIN
                                            SET @ActualRunningTardyHours = @ActualRunningTardyHours + 1;
                                        END;
		 --commented by balaji on 10/22/2012 as report (rdl) doesn't add days attended and make up days
		 ---- If there are make up hrs deduct that otherwise it will be added again in progress report
                                    IF (
                                         @Actual > 0
                                         AND @Actual > @ScheduledMinutes
                                         AND @Actual <> 9999.00
                                       )
                                        BEGIN
                                            SET @ActualRunningPresentHours = @ActualRunningPresentHours - ( @Actual - @ScheduledMinutes );
                                            SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - ( @Actual - @ScheduledMinutes );
                                        END;
			
                                    IF @tracktardies = 1
                                        AND (
                                              @TardyMinutes > 0
                                              OR @IsTardy = 1
                                            )
                                        BEGIN
                                            SET @intTardyBreakPoint = @intTardyBreakPoint + 1;
                                        END;	    
		
		
		
		-- Makeup: Calculate Makeup Hours when Student Attended more than scheduled hours          	       
                                    IF (
                                         @Actual > 0
                                         AND @Actual > @ScheduledMinutes
                                         AND @Actual <> 9999.00
                                       )
                                        BEGIN
                                            SET @MakeupHours = @MakeupHours + ( @Actual - @ScheduledMinutes );
                                        END;
		
                                    IF (
                                         @tracktardies = 1
                                         AND @intTardyBreakPoint = @TardiesMakingAbsence
                                       )
                                        BEGIN
                                            SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - @Actual;
                                            SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @ScheduledMinutes; --@TardyMinutes
                                            SET @intTardyBreakPoint = 0;
                                        END;
				
                                    DELETE  FROM syStudentAttendanceSummary
                                    WHERE   StuEnrollId = @StuEnrollId
                                            AND StudentAttendedDate = @MeetDate;
                                    INSERT  INTO syStudentAttendanceSummary
                                            (
                                             StuEnrollId
                                            ,ClsSectionId
                                            ,StudentAttendedDate
                                            ,ScheduledDays
                                            ,ActualDays
                                            ,ActualRunningScheduledDays
                                            ,ActualRunningPresentDays
                                            ,ActualRunningAbsentDays
                                            ,ActualRunningMakeupDays
                                            ,ActualRunningTardyDays
                                            ,AdjustedPresentDays
                                            ,AdjustedAbsentDays
                                            ,AttendanceTrackType
                                            ,ModUser
                                            ,ModDate
                                            ,tardiesmakingabsence
										
                                            )
                                    VALUES  (
                                             @StuEnrollId
                                            ,@ClsSectionId
                                            ,@MeetDate
                                            ,ISNULL(@ScheduledMinutes,0)
                                            ,@Actual
                                            ,ISNULL(@ActualRunningScheduledDays,0)
                                            ,ISNULL(@ActualRunningPresentHours,0)
                                            ,ISNULL(@ActualRunningAbsentHours,0)
                                            ,ISNULL(@MakeupHours,0)
                                            ,ISNULL(@ActualRunningTardyHours,0)
                                            ,ISNULL(@AdjustedRunningPresentHours,0)
                                            ,ISNULL(@AdjustedRunningAbsentHours,0)
                                            ,'Post Attendance by Class'
                                            ,'sa'
                                            ,GETDATE()
                                            ,@TardiesMakingAbsence
										
                                            );

		--update syStudentAttendanceSummary set TardiesMakingAbsence=@TardiesMakingAbsence where StuEnrollId=@StuEnrollId
		--end
                                    SET @PrevStuEnrollId = @StuEnrollId; 
                                    FETCH NEXT FROM GetAttendance_Cursor INTO @StuEnrollId,@MeetDate,@Actual,@ScheduledMinutes,@Absent,@IsTardy,@tracktardies,
                                        @TardiesMakingAbsence;
                                END;
                            CLOSE GetAttendance_Cursor;
                            DEALLOCATE GetAttendance_Cursor;
                        END;
                END;
							
--end

-- Based on grade rounding round the final score and current score
--Declare @PrevTermId uniqueidentifier,@PrevReqId uniqueidentifier,@RowCount int,@FinalScore decimal(18,4),@CurrentScore decimal(18,4)
--declare @ReqId uniqueidentifier,@CourseCodeDescrip varchar(100),@FinalGrade varchar(10),@sysComponentTypeId int
--declare @CreditsAttempted decimal(18,4),@Grade varchar(10),@IsPass bit,@IsCreditsAttempted bit,@IsCreditsEarned bit
--declare @IsInGPA bit,@FinAidCredits decimal(18,4),@CurrentGrade varchar(10),@CourseCredits decimal(18,4)
                DECLARE @GPA DECIMAL(18,4);

                DECLARE @PrevReqId UNIQUEIDENTIFIER
                   ,@PrevTermId UNIQUEIDENTIFIER
                   ,@CreditsAttempted DECIMAL(18,2)
                   ,@CreditsEarned DECIMAL(18,2);
                DECLARE @reqid UNIQUEIDENTIFIER
                   ,@CourseCodeDescrip VARCHAR(50)
                   ,@FinalGrade VARCHAR(50)
                   ,@FinalScore DECIMAL(18,2)
                   ,@Grade VARCHAR(50);
                DECLARE @IsPass BIT
                   ,@IsCreditsAttempted BIT
                   ,@IsCreditsEarned BIT
                   ,@CurrentScore DECIMAL(18,2)
                   ,@CurrentGrade VARCHAR(10)
                   ,@FinalGPA DECIMAL(18,2);
                DECLARE @sysComponentTypeId INT
                   ,@RowCount INT;
                DECLARE @IsInGPA BIT;
                DECLARE @CourseCredits DECIMAL(18,2);
                DECLARE @FinAidCreditsEarned DECIMAL(18,2)
                   ,@FinAidCredits DECIMAL(18,2);

                DECLARE GetCreditsSummary_Cursor CURSOR
                FOR
                    SELECT	DISTINCT
                            SE.StuEnrollId
                           ,T.TermId
                           ,T.TermDescrip
                           ,T.StartDate
                           ,R.ReqId
                           ,'(' + R.Code + ' ) ' + R.Descrip AS CourseCodeDescrip
                           ,RES.Score AS FinalScore
                           ,RES.GrdSysDetailId AS FinalGrade
                           ,GCT.SysComponentTypeId
                           ,R.Credits AS CreditsAttempted
                           ,CS.ClsSectionId
                           ,GSD.Grade
                           ,GSD.IsPass
                           ,GSD.IsCreditsAttempted
                           ,GSD.IsCreditsEarned
                           ,SE.PrgVerId
                           ,GSD.IsInGPA
                           ,R.FinAidCredits AS FinAidCredits
                    FROM    arStuEnrollments SE
                    INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                    INNER JOIN arResults RES ON RES.StuEnrollId = SE.StuEnrollId
                    INNER JOIN arClassSections CS ON RES.TestId = CS.ClsSectionId
                    INNER JOIN arTerm T ON CS.TermId = T.TermId
                    INNER JOIN arReqs R ON CS.ReqId = R.ReqId
                    LEFT JOIN arGrdBkResults GBR ON CS.ClsSectionId = GBR.ClsSectionId
                    LEFT JOIN arGrdBkWgtDetails GBWD ON GBR.InstrGrdBkWgtDetailId = GBWD.InstrGrdBkWgtDetailId
                    LEFT JOIN arGrdComponentTypes GCT ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
                    LEFT JOIN arGradeSystemDetails GSD ON RES.GrdSysDetailId = GSD.GrdSysDetailId
                    WHERE   SE.StuEnrollId = @StuEnrollId
                    ORDER BY T.StartDate
                           ,T.TermDescrip
                           ,R.ReqId; 

                DECLARE @varGradeRounding VARCHAR(3);
                DECLARE @roundfinalscore DECIMAL(18,4);
                SET @varGradeRounding = (
                                          SELECT    Value
                                          FROM      syConfigAppSetValues
                                          WHERE     SettingId = 45
                                        );

                OPEN GetCreditsSummary_Cursor;
                SET @PrevStuEnrollId = NULL;
                SET @PrevTermId = NULL;
                SET @PrevReqId = NULL;
                SET @RowCount = 0;
                FETCH NEXT FROM GetCreditsSummary_Cursor INTO @StuEnrollId,@TermId,@TermDescrip,@termstartdate1,@reqid,@CourseCodeDescrip,@FinalScore,
                    @FinalGrade,@sysComponentTypeId,@CreditsAttempted,@ClsSectionId,@Grade,@IsPass,@IsCreditsAttempted,@IsCreditsEarned,@PrgVerId,@IsInGPA,
                    @FinAidCredits;
                WHILE @@FETCH_STATUS = 0
                    BEGIN
			
			
                        SET @CurrentScore = (
                                              SELECT    CASE WHEN SUM(GradeBookWeight) > 0 THEN ( SUM(ActualWeight) / SUM(GradeBookWeight) ) * 100
                                                             ELSE NULL
                                                        END AS CurrentScore
                                              FROM      (
                                                          SELECT    InstrGrdBkWgtDetailId
                                                                   ,Code
                                                                   ,Descrip
                                                                   ,Weight AS GradeBookWeight
                                                                   ,CASE WHEN S.NumberOfComponents > 0
                                                                         THEN SUM(( Weight * ( Score / NumberOfComponents ) ) / 100)
                                                                         ELSE 0
                                                                    END AS ActualWeight
                                                          FROM      (
                                                                      SELECT    C.InstrGrdBkWgtDetailId
                                                                               ,D.Code
                                                                               ,D.Descrip
                                                                               ,ISNULL(C.Weight,0) AS Weight
                                                                               ,C.Number AS MinNumber
                                                                               ,C.GrdPolicyId
                                                                               ,C.Parameter AS Param
                                                                               ,X.GrdScaleId
                                                                               ,SUM(GR.Score) AS Score
                                                                               ,COUNT(D.Descrip) AS NumberOfComponents
                                                                      FROM      (
                                                                                  SELECT DISTINCT TOP 1
                                                                                            A.InstrGrdBkWgtId
                                                                                           ,A.EffectiveDate
                                                                                           ,B.GrdScaleId
                                                                                  FROM      arGrdBkWeights A
                                                                                           ,arClassSections B
                                                                                  WHERE     A.ReqId = B.ReqId
                                                                                            AND A.EffectiveDate <= B.StartDate
                                                                                            AND B.ClsSectionId = @ClsSectionId
                                                                                  ORDER BY  A.EffectiveDate DESC
                                                                                ) X
                                                                               ,arGrdBkWgtDetails C
                                                                               ,arGrdComponentTypes D
                                                                               ,arGrdBkResults GR
                                                                      WHERE     X.InstrGrdBkWgtId = C.InstrGrdBkWgtId
                                                                                AND C.GrdComponentTypeId = D.GrdComponentTypeId
                                                                                AND C.InstrGrdBkWgtDetailId = GR.InstrGrdBkWgtDetailId
                                                                                AND D.SysComponentTypeId NOT IN ( 500,503 )
                                                                                AND GR.StuEnrollId = @StuEnrollId
                                                                                AND GR.ClsSectionId = @ClsSectionId
                                                                                AND GR.Score IS NOT NULL
                                                                      GROUP BY  C.InstrGrdBkWgtDetailId
                                                                               ,D.Code
                                                                               ,D.Descrip
                                                                               ,C.Weight
                                                                               ,C.Number
                                                                               ,C.GrdPolicyId
                                                                               ,C.Parameter
                                                                               ,X.GrdScaleId
                                                                    ) S
                                                          GROUP BY  InstrGrdBkWgtDetailId
                                                                   ,Code
                                                                   ,Descrip
                                                                   ,Weight
                                                                   ,NumberOfComponents
                                                        ) FinalTblToComputeCurrentScore
                                            );
                        IF ( @CurrentScore IS NULL )
                            BEGIN
				-- instructor grade books
                                SET @CurrentScore = (
                                                      SELECT    CASE WHEN SUM(GradeBookWeight) > 0 THEN ( SUM(ActualWeight) / SUM(GradeBookWeight) ) * 100
                                                                     ELSE NULL
                                                                END AS CurrentScore
                                                      FROM      (
                                                                  SELECT    InstrGrdBkWgtDetailId
                                                                           ,Code
                                                                           ,Descrip
                                                                           ,Weight AS GradeBookWeight
                                                                           ,CASE WHEN S.NumberOfComponents > 0
                                                                                 THEN SUM(( Weight * ( Score / NumberOfComponents ) ) / 100)
                                                                                 ELSE 0
                                                                            END AS ActualWeight
                                                                  FROM      (
                                                                              SELECT    C.InstrGrdBkWgtDetailId
                                                                                       ,D.Code
                                                                                       ,D.Descrip
                                                                                       ,ISNULL(C.Weight,0) AS Weight
                                                                                       ,C.Number AS MinNumber
                                                                                       ,C.GrdPolicyId
                                                                                       ,C.Parameter AS Param
                                                                                       ,X.GrdScaleId
                                                                                       ,SUM(GR.Score) AS Score
                                                                                       ,COUNT(D.Descrip) AS NumberOfComponents
                                                                              FROM      (
                                                                                          --SELECT Distinct Top 1 A.InstrGrdBkWgtId,A.EffectiveDate,B.GrdScaleId        
															--FROM          arGrdBkWeights A,arClassSections B        
															--WHERE         A.ReqId=B.ReqId AND A.EffectiveDate<=B.StartDate AND B.ClsSectionId=@ClsSectionId
															--ORDER BY      A.EffectiveDate DESC
                                                                                          SELECT DISTINCT TOP 1
                                                                                                    t1.InstrGrdBkWgtId
                                                                                                   ,t1.GrdScaleId
                                                                                          FROM      arClassSections t1
                                                                                                   ,arGrdBkWeights t2
                                                                                          WHERE     t1.InstrGrdBkWgtId = t2.InstrGrdBkWgtId
                                                                                                    AND t1.ClsSectionId = @ClsSectionId
                                                                                        ) X
                                                                                       ,arGrdBkWgtDetails C
                                                                                       ,arGrdComponentTypes D
                                                                                       ,arGrdBkResults GR
                                                                              WHERE     X.InstrGrdBkWgtId = C.InstrGrdBkWgtId
                                                                                        AND C.GrdComponentTypeId = D.GrdComponentTypeId
                                                                                        AND C.InstrGrdBkWgtDetailId = GR.InstrGrdBkWgtDetailId
                                                                                        AND
													-- D.SysComponentTypeID not in (500,503) and 
                                                                                        GR.StuEnrollId = @StuEnrollId
                                                                                        AND GR.ClsSectionId = @ClsSectionId
                                                                                        AND GR.Score IS NOT NULL
                                                                              GROUP BY  C.InstrGrdBkWgtDetailId
                                                                                       ,D.Code
                                                                                       ,D.Descrip
                                                                                       ,C.Weight
                                                                                       ,C.Number
                                                                                       ,C.GrdPolicyId
                                                                                       ,C.Parameter
                                                                                       ,X.GrdScaleId
                                                                            ) S
                                                                  GROUP BY  InstrGrdBkWgtDetailId
                                                                           ,Code
                                                                           ,Descrip
                                                                           ,Weight
                                                                           ,NumberOfComponents
                                                                ) FinalTblToComputeCurrentScore
                                                    );	
			
                            END;
			
		
			-- If rounding is set to yes, then round the scores to next available score or ignore rounding
                        IF ( LOWER(@varGradeRounding) = 'yes' )
                            BEGIN
                                IF @FinalScore IS NOT NULL
                                    BEGIN
                                        SET @FinalScore = ROUND(@FinalScore,0);
                                        SET @CurrentScore = @FinalScore;
                                    END;
                                IF @FinalScore IS NULL
                                    AND @CurrentScore IS NOT NULL
                                    BEGIN
                                        SET @CurrentScore = ROUND(@CurrentScore,0);
                                    END;
                            END;
                        ELSE
                            BEGIN
                                IF @FinalScore IS NOT NULL
                                    BEGIN
                                        SET @CurrentScore = @FinalScore;
                                    END;	
                                IF @FinalScore IS NULL
                                    AND @CurrentScore IS NOT NULL
                                    BEGIN
                                        SET @CurrentScore = ROUND(@CurrentScore,0);
                                    END;
                            END;
				
                        UPDATE  syCreditSummary
                        SET     FinalScore = @FinalScore
                               ,CurrentScore = @CurrentScore
                        WHERE   StuEnrollId = @StuEnrollId
                                AND TermId = @TermId
                                AND ReqId = @reqid;

			--Average calculation
			
			-- Term Average
                        SET @TermAverageCount = (
                                                  SELECT    COUNT(*)
                                                  FROM      syCreditSummary
                                                  WHERE     StuEnrollId = @StuEnrollId
                                                            AND TermId = @TermId
                                                            AND FinalScore IS NOT NULL
                                                );
                        SET @termAverageSum = (
                                                SELECT  SUM(FinalScore)
                                                FROM    syCreditSummary
                                                WHERE   StuEnrollId = @StuEnrollId
                                                        AND TermId = @TermId
                                                        AND FinalScore IS NOT NULL
                                              );
                        SET @TermAverage = @termAverageSum / @TermAverageCount; 
			
			-- Cumulative Average
                        SET @cumAveragecount = (
                                                 SELECT COUNT(*)
                                                 FROM   syCreditSummary
                                                 WHERE  StuEnrollId = @StuEnrollId
                                                        AND FinalScore IS NOT NULL
                                               );
                        SET @cumAverageSum = (
                                               SELECT   SUM(FinalScore)
                                               FROM     syCreditSummary
                                               WHERE    StuEnrollId = @StuEnrollId
                                                        AND FinalScore IS NOT NULL
                                             );
                        SET @CumAverage = @cumAverageSum / @cumAveragecount; 
			
                        UPDATE  syCreditSummary
                        SET     Average = @TermAverage
                        WHERE   StuEnrollId = @StuEnrollId
                                AND TermId = @TermId; 
			
			--Update Cumulative GPA
                        UPDATE  syCreditSummary
                        SET     CumAverage = @CumAverage
                        WHERE   StuEnrollId = @StuEnrollId;
			

                        FETCH NEXT FROM GetCreditsSummary_Cursor INTO @StuEnrollId,@TermId,@TermDescrip,@termstartdate1,@reqid,@CourseCodeDescrip,@FinalScore,
                            @FinalGrade,@sysComponentTypeId,@CreditsAttempted,@ClsSectionId,@Grade,@IsPass,@IsCreditsAttempted,@IsCreditsEarned,@PrgVerId,
                            @IsInGPA,@FinAidCredits;
                    END;
                CLOSE GetCreditsSummary_Cursor;
                DEALLOCATE GetCreditsSummary_Cursor;


                DECLARE GetCreditsSummary_Cursor CURSOR
                FOR
                    SELECT	DISTINCT
                            SE.StuEnrollId
                           ,T.TermId
                           ,T.TermDescrip
                           ,T.StartDate
                           ,R.ReqId
                           ,'(' + R.Code + ' ) ' + R.Descrip AS CourseCodeDescrip
                           ,RES.Score AS FinalScore
                           ,RES.GrdSysDetailId AS FinalGrade
                           ,GCT.SysComponentTypeId
                           ,R.Credits AS CreditsAttempted
                           ,CS.ClsSectionId
                           ,GSD.Grade
                           ,GSD.IsPass
                           ,GSD.IsCreditsAttempted
                           ,GSD.IsCreditsEarned
                           ,SE.PrgVerId
                           ,GSD.IsInGPA
                           ,R.FinAidCredits AS FinAidCredits
                    FROM    arStuEnrollments SE
                    INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                    INNER JOIN arTransferGrades RES ON RES.StuEnrollId = SE.StuEnrollId
                    INNER JOIN arClassSections CS ON RES.ReqId = CS.ReqId
                                                     AND RES.TermId = CS.TermId
                    INNER JOIN arTerm T ON CS.TermId = T.TermId
                    INNER JOIN arReqs R ON CS.ReqId = R.ReqId
                    LEFT JOIN arGrdBkResults GBR ON CS.ClsSectionId = GBR.ClsSectionId
                    LEFT JOIN arGrdBkWgtDetails GBWD ON GBR.InstrGrdBkWgtDetailId = GBWD.InstrGrdBkWgtDetailId
                    LEFT JOIN arGrdComponentTypes GCT ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
                    LEFT JOIN arGradeSystemDetails GSD ON RES.GrdSysDetailId = GSD.GrdSysDetailId
                    WHERE   SE.StuEnrollId = @StuEnrollId
                    ORDER BY T.StartDate
                           ,T.TermDescrip
                           ,R.ReqId;


                SET @varGradeRounding = (
                                          SELECT    Value
                                          FROM      syConfigAppSetValues
                                          WHERE     SettingId = 45
                                        );

                OPEN GetCreditsSummary_Cursor;
                SET @PrevStuEnrollId = NULL;
                SET @PrevTermId = NULL;
                SET @PrevReqId = NULL;
                SET @RowCount = 0;
                FETCH NEXT FROM GetCreditsSummary_Cursor INTO @StuEnrollId,@TermId,@TermDescrip,@termstartdate1,@reqid,@CourseCodeDescrip,@FinalScore,
                    @FinalGrade,@sysComponentTypeId,@CreditsAttempted,@ClsSectionId,@Grade,@IsPass,@IsCreditsAttempted,@IsCreditsEarned,@PrgVerId,@IsInGPA,
                    @FinAidCredits;
                WHILE @@FETCH_STATUS = 0
                    BEGIN
			
                        SET @CurrentScore = (
                                              SELECT    CASE WHEN SUM(GradeBookWeight) > 0 THEN ( SUM(ActualWeight) / SUM(GradeBookWeight) ) * 100
                                                             ELSE NULL
                                                        END AS CurrentScore
                                              FROM      (
                                                          SELECT    InstrGrdBkWgtDetailId
                                                                   ,Code
                                                                   ,Descrip
                                                                   ,Weight AS GradeBookWeight
                                                                   ,CASE WHEN S.NumberOfComponents > 0
                                                                         THEN SUM(( Weight * ( Score / NumberOfComponents ) ) / 100)
                                                                         ELSE 0
                                                                    END AS ActualWeight
                                                          FROM      (
                                                                      SELECT    C.InstrGrdBkWgtDetailId
                                                                               ,D.Code
                                                                               ,D.Descrip
                                                                               ,ISNULL(C.Weight,0) AS Weight
                                                                               ,C.Number AS MinNumber
                                                                               ,C.GrdPolicyId
                                                                               ,C.Parameter AS Param
                                                                               ,X.GrdScaleId
                                                                               ,SUM(GR.Score) AS Score
                                                                               ,COUNT(D.Descrip) AS NumberOfComponents
                                                                      FROM      (
                                                                                  SELECT DISTINCT TOP 1
                                                                                            A.InstrGrdBkWgtId
                                                                                           ,A.EffectiveDate
                                                                                           ,B.GrdScaleId
                                                                                  FROM      arGrdBkWeights A
                                                                                           ,arClassSections B
                                                                                  WHERE     A.ReqId = B.ReqId
                                                                                            AND A.EffectiveDate <= B.StartDate
                                                                                            AND B.ClsSectionId = @ClsSectionId
                                                                                  ORDER BY  A.EffectiveDate DESC
                                                                                ) X
                                                                               ,arGrdBkWgtDetails C
                                                                               ,arGrdComponentTypes D
                                                                               ,arGrdBkResults GR
                                                                      WHERE     X.InstrGrdBkWgtId = C.InstrGrdBkWgtId
                                                                                AND C.GrdComponentTypeId = D.GrdComponentTypeId
                                                                                AND C.InstrGrdBkWgtDetailId = GR.InstrGrdBkWgtDetailId
                                                                                AND D.SysComponentTypeId NOT IN ( 500,503 )
                                                                                AND GR.StuEnrollId = @StuEnrollId
                                                                                AND GR.ClsSectionId = @ClsSectionId
                                                                                AND GR.Score IS NOT NULL
                                                                      GROUP BY  C.InstrGrdBkWgtDetailId
                                                                               ,D.Code
                                                                               ,D.Descrip
                                                                               ,C.Weight
                                                                               ,C.Number
                                                                               ,C.GrdPolicyId
                                                                               ,C.Parameter
                                                                               ,X.GrdScaleId
                                                                    ) S
                                                          GROUP BY  InstrGrdBkWgtDetailId
                                                                   ,Code
                                                                   ,Descrip
                                                                   ,Weight
                                                                   ,NumberOfComponents
                                                        ) FinalTblToComputeCurrentScore
                                            );
                        IF ( @CurrentScore IS NULL )
                            BEGIN
				-- instructor grade books
                                SET @CurrentScore = (
                                                      SELECT    CASE WHEN SUM(GradeBookWeight) > 0 THEN ( SUM(ActualWeight) / SUM(GradeBookWeight) ) * 100
                                                                     ELSE NULL
                                                                END AS CurrentScore
                                                      FROM      (
                                                                  SELECT    InstrGrdBkWgtDetailId
                                                                           ,Code
                                                                           ,Descrip
                                                                           ,Weight AS GradeBookWeight
                                                                           ,CASE WHEN S.NumberOfComponents > 0
                                                                                 THEN SUM(( Weight * ( Score / NumberOfComponents ) ) / 100)
                                                                                 ELSE 0
                                                                            END AS ActualWeight
                                                                  FROM      (
                                                                              SELECT    C.InstrGrdBkWgtDetailId
                                                                                       ,D.Code
                                                                                       ,D.Descrip
                                                                                       ,ISNULL(C.Weight,0) AS Weight
                                                                                       ,C.Number AS MinNumber
                                                                                       ,C.GrdPolicyId
                                                                                       ,C.Parameter AS Param
                                                                                       ,X.GrdScaleId
                                                                                       ,SUM(GR.Score) AS Score
                                                                                       ,COUNT(D.Descrip) AS NumberOfComponents
                                                                              FROM      (
                                                                                          --SELECT Distinct Top 1 A.InstrGrdBkWgtId,A.EffectiveDate,B.GrdScaleId        
															--FROM          arGrdBkWeights A,arClassSections B        
															--WHERE         A.ReqId=B.ReqId AND A.EffectiveDate<=B.StartDate AND B.ClsSectionId=@ClsSectionId
															--ORDER BY      A.EffectiveDate DESC
                                                                                          SELECT DISTINCT TOP 1
                                                                                                    t1.InstrGrdBkWgtId
                                                                                                   ,t1.GrdScaleId
                                                                                          FROM      arClassSections t1
                                                                                                   ,arGrdBkWeights t2
                                                                                          WHERE     t1.InstrGrdBkWgtId = t2.InstrGrdBkWgtId
                                                                                                    AND t1.ClsSectionId = @ClsSectionId
                                                                                        ) X
                                                                                       ,arGrdBkWgtDetails C
                                                                                       ,arGrdComponentTypes D
                                                                                       ,arGrdBkResults GR
                                                                              WHERE     X.InstrGrdBkWgtId = C.InstrGrdBkWgtId
                                                                                        AND C.GrdComponentTypeId = D.GrdComponentTypeId
                                                                                        AND C.InstrGrdBkWgtDetailId = GR.InstrGrdBkWgtDetailId
                                                                                        AND
													-- D.SysComponentTypeID not in (500,503) and 
                                                                                        GR.StuEnrollId = @StuEnrollId
                                                                                        AND GR.ClsSectionId = @ClsSectionId
                                                                                        AND GR.Score IS NOT NULL
                                                                              GROUP BY  C.InstrGrdBkWgtDetailId
                                                                                       ,D.Code
                                                                                       ,D.Descrip
                                                                                       ,C.Weight
                                                                                       ,C.Number
                                                                                       ,C.GrdPolicyId
                                                                                       ,C.Parameter
                                                                                       ,X.GrdScaleId
                                                                            ) S
                                                                  GROUP BY  InstrGrdBkWgtDetailId
                                                                           ,Code
                                                                           ,Descrip
                                                                           ,Weight
                                                                           ,NumberOfComponents
                                                                ) FinalTblToComputeCurrentScore
                                                    );	
			
                            END;
			
			-- If rounding is set to yes, then round the scores to next available score or ignore rounding
                        IF ( LOWER(@varGradeRounding) = 'yes' )
                            BEGIN
                                IF @FinalScore IS NOT NULL
                                    BEGIN
                                        SET @FinalScore = ROUND(@FinalScore,0);
                                        SET @CurrentScore = @FinalScore;
                                    END;
                                IF @FinalScore IS NULL
                                    AND @CurrentScore IS NOT NULL
                                    BEGIN
                                        SET @CurrentScore = ROUND(@CurrentScore,0);
                                    END;
                            END;
                        ELSE
                            BEGIN
                                IF @FinalScore IS NOT NULL
                                    BEGIN
                                        SET @CurrentScore = @FinalScore;
                                    END;	
                                IF @FinalScore IS NULL
                                    AND @CurrentScore IS NOT NULL
                                    BEGIN
                                        SET @CurrentScore = ROUND(@CurrentScore,0);
                                    END;
                            END;
				
                        UPDATE  syCreditSummary
                        SET     FinalScore = @FinalScore
                               ,CurrentScore = @CurrentScore
                        WHERE   StuEnrollId = @StuEnrollId
                                AND TermId = @TermId
                                AND ReqId = @reqid;

			--Average calculation
--			declare @CumAverage decimal(18,2),@cumAverageSum decimal(18,2),@cumAveragecount int
			
			-- Term Average
                        SET @TermAverageCount = (
                                                  SELECT    COUNT(*)
                                                  FROM      syCreditSummary
                                                  WHERE     StuEnrollId = @StuEnrollId
                                                            AND TermId = @TermId
                                                            AND FinalScore IS NOT NULL
                                                );
                        SET @termAverageSum = (
                                                SELECT  SUM(FinalScore)
                                                FROM    syCreditSummary
                                                WHERE   StuEnrollId = @StuEnrollId
                                                        AND TermId = @TermId
                                                        AND FinalScore IS NOT NULL
                                              );
                        SET @TermAverage = @termAverageSum / @TermAverageCount; 
			
			-- Cumulative Average
                        SET @cumAveragecount = (
                                                 SELECT COUNT(*)
                                                 FROM   syCreditSummary
                                                 WHERE  StuEnrollId = @StuEnrollId
                                                        AND FinalScore IS NOT NULL
                                               );
                        SET @cumAverageSum = (
                                               SELECT   SUM(FinalScore)
                                               FROM     syCreditSummary
                                               WHERE    StuEnrollId = @StuEnrollId
                                                        AND FinalScore IS NOT NULL
                                             );
                        SET @CumAverage = @cumAverageSum / @cumAveragecount; 
			
                        UPDATE  syCreditSummary
                        SET     Average = @TermAverage
                        WHERE   StuEnrollId = @StuEnrollId
                                AND TermId = @TermId; 
			
			--Update Cumulative GPA
                        UPDATE  syCreditSummary
                        SET     CumAverage = @CumAverage
                        WHERE   StuEnrollId = @StuEnrollId;
			

                        FETCH NEXT FROM GetCreditsSummary_Cursor INTO @StuEnrollId,@TermId,@TermDescrip,@termstartdate1,@reqid,@CourseCodeDescrip,@FinalScore,
                            @FinalGrade,@sysComponentTypeId,@CreditsAttempted,@ClsSectionId,@Grade,@IsPass,@IsCreditsAttempted,@IsCreditsEarned,@PrgVerId,
                            @IsInGPA,@FinAidCredits;
                    END;
                CLOSE GetCreditsSummary_Cursor;
                DEALLOCATE GetCreditsSummary_Cursor;

                DECLARE @GradeSystemDetailId UNIQUEIDENTIFIER
                   ,@IsGPA BIT;
                DECLARE GetCreditsSummary_Cursor CURSOR
                FOR
                    SELECT DISTINCT
                            SE.StuEnrollId
                           ,T.TermId
                           ,T.TermDescrip
                           ,T.StartDate
                           ,R.ReqId
                           ,'(' + R.Code + ' ) ' + R.Descrip AS CourseCodeDescrip
                           ,NULL AS FinalScore
                           ,RES.GrdSysDetailId AS GradeSystemDetailId
                           ,GSD.Grade AS FinalGrade
                           ,GSD.Grade AS CurrentGrade
                           ,R.Credits
                           ,NULL AS ClsSectionId
                           ,GSD.IsPass
                           ,GSD.IsCreditsAttempted
                           ,GSD.IsCreditsEarned
                           ,GSD.GPA
                           ,GSD.IsInGPA
                           ,SE.PrgVerId
                           ,GSD.IsInGPA
                           ,R.FinAidCredits AS FinAidCredits
                    FROM    arStuEnrollments SE
                    INNER JOIN arTransferGrades RES ON SE.StuEnrollId = RES.StuEnrollId
                    INNER JOIN syCreditSummary CS ON CS.StuEnrollId = RES.StuEnrollId
                    INNER JOIN arGradeSystemDetails GSD ON RES.GrdSysDetailId = GSD.GrdSysDetailId
                    INNER JOIN arReqs R ON RES.ReqId = R.ReqId
                    INNER JOIN arTerm T ON RES.TermId = T.TermId
                    WHERE   RES.ReqId NOT IN ( SELECT DISTINCT
                                                        ReqId
                                               FROM     syCreditSummary
                                               WHERE    StuEnrollId = SE.StuEnrollId
                                                        AND TermId = T.TermId )
                            AND SE.StuEnrollId = @StuEnrollId;

                SET @varGradeRounding = (
                                          SELECT    Value
                                          FROM      syConfigAppSetValues
                                          WHERE     SettingId = 45
                                        );

                OPEN GetCreditsSummary_Cursor;
                SET @PrevStuEnrollId = NULL;
                SET @PrevTermId = NULL;
                SET @PrevReqId = NULL;
                SET @RowCount = 0;
                FETCH NEXT FROM GetCreditsSummary_Cursor INTO @StuEnrollId,@TermId,@TermDescrip,@termstartdate1,@reqid,@CourseCodeDescrip,@FinalScore,
                    @GradeSystemDetailId,@FinalGrade,@CurrentGrade,@CourseCredits,@ClsSectionId,@IsPass,@IsCreditsAttempted,@IsCreditsEarned,@GPA,@IsGPA,
                    @PrgVerId,@IsInGPA,@FinAidCredits; 
                WHILE @@FETCH_STATUS = 0
                    BEGIN
			
			-- If rounding is set to yes, then round the scores to next available score or ignore rounding
                        IF ( LOWER(@varGradeRounding) = 'yes' )
                            BEGIN
                                IF @FinalScore IS NOT NULL
                                    BEGIN
                                        SET @FinalScore = ROUND(@FinalScore,0);
                                        SET @CurrentScore = @FinalScore;
                                    END;
                                IF @FinalScore IS NULL
                                    AND @CurrentScore IS NOT NULL
                                    BEGIN
                                        SET @CurrentScore = ROUND(@CurrentScore,0);
                                    END;
                            END;
                        ELSE
                            BEGIN
                                IF @FinalScore IS NOT NULL
                                    BEGIN
                                        SET @CurrentScore = @FinalScore;
                                    END;	
                                IF @FinalScore IS NULL
                                    AND @CurrentScore IS NOT NULL
                                    BEGIN
                                        SET @CurrentScore = ROUND(@CurrentScore,0);
                                    END;
                            END;
				
                        UPDATE  syCreditSummary
                        SET     FinalScore = @FinalScore
                               ,CurrentScore = @CurrentScore
                        WHERE   StuEnrollId = @StuEnrollId
                                AND TermId = @TermId
                                AND ReqId = @reqid;

			--Average calculation
		
			-- Term Average
                        SET @TermAverageCount = (
                                                  SELECT    COUNT(*)
                                                  FROM      syCreditSummary
                                                  WHERE     StuEnrollId = @StuEnrollId
                                                            AND TermId = @TermId
                                                            AND FinalScore IS NOT NULL
                                                );
                        SET @termAverageSum = (
                                                SELECT  SUM(FinalScore)
                                                FROM    syCreditSummary
                                                WHERE   StuEnrollId = @StuEnrollId
                                                        AND TermId = @TermId
                                                        AND FinalScore IS NOT NULL
                                              );
                        SET @TermAverage = @termAverageSum / @TermAverageCount; 
			
			-- Cumulative Average
                        SET @cumAveragecount = (
                                                 SELECT COUNT(*)
                                                 FROM   syCreditSummary
                                                 WHERE  StuEnrollId = @StuEnrollId
											--AND FinalScore IS NOT NULL
                                               );
                        SET @cumAverageSum = (
                                               SELECT   SUM(FinalScore)
                                               FROM     syCreditSummary
                                               WHERE    StuEnrollId = @StuEnrollId
                                                        AND FinalScore IS NOT NULL
                                             );
                        SET @CumAverage = @cumAverageSum / @cumAveragecount; 
			
                        UPDATE  syCreditSummary
                        SET     Average = @TermAverage
                        WHERE   StuEnrollId = @StuEnrollId
                                AND TermId = @TermId; 
			
			--Update Cumulative GPA
                        UPDATE  syCreditSummary
                        SET     CumAverage = @CumAverage
                        WHERE   StuEnrollId = @StuEnrollId;
			

                        FETCH NEXT FROM GetCreditsSummary_Cursor INTO @StuEnrollId,@TermId,@TermDescrip,@termstartdate1,@reqid,@CourseCodeDescrip,@FinalScore,
                            @GradeSystemDetailId,@FinalGrade,@CurrentGrade,@CourseCredits,@ClsSectionId,@IsPass,@IsCreditsAttempted,@IsCreditsEarned,@GPA,@IsGPA,
                            @PrgVerId,@IsInGPA,@FinAidCredits; 
                    END;
                CLOSE GetCreditsSummary_Cursor;
                DEALLOCATE GetCreditsSummary_Cursor;

                DECLARE GetCreditsSummary_Cursor CURSOR
                FOR
                    SELECT DISTINCT
                            SE.StuEnrollId
                           ,T.TermId
                           ,T.TermDescrip
                           ,T.StartDate
                           ,R.ReqId
                           ,'(' + R.Code + ' ) ' + R.Descrip AS CourseCodeDescrip
                           ,NULL AS FinalScore
                           ,RES.GrdSysDetailId AS GradeSystemDetailId
                           ,GSD.Grade AS FinalGrade
                           ,GSD.Grade AS CurrentGrade
                           ,R.Credits
                           ,NULL AS ClsSectionId
                           ,GSD.IsPass
                           ,GSD.IsCreditsAttempted
                           ,GSD.IsCreditsEarned
                           ,GSD.GPA
                           ,GSD.IsInGPA
                           ,SE.PrgVerId
                           ,GSD.IsInGPA
                           ,R.FinAidCredits AS FinAidCredits
                    FROM    arStuEnrollments SE
                    INNER JOIN arTransferGrades RES ON SE.StuEnrollId = RES.StuEnrollId
                    INNER JOIN arGradeSystemDetails GSD ON RES.GrdSysDetailId = GSD.GrdSysDetailId
                    INNER JOIN arReqs R ON RES.ReqId = R.ReqId
                    INNER JOIN arTerm T ON RES.TermId = T.TermId
                    WHERE   SE.StuEnrollId NOT IN ( SELECT DISTINCT
                                                            StuEnrollId
                                                    FROM    syCreditSummary )
                            AND SE.StuEnrollId = @StuEnrollId;

                SET @varGradeRounding = (
                                          SELECT    Value
                                          FROM      syConfigAppSetValues
                                          WHERE     SettingId = 45
                                        );

                OPEN GetCreditsSummary_Cursor;
                SET @PrevStuEnrollId = NULL;
                SET @PrevTermId = NULL;
                SET @PrevReqId = NULL;
                SET @RowCount = 0;
                FETCH NEXT FROM GetCreditsSummary_Cursor INTO @StuEnrollId,@TermId,@TermDescrip,@termstartdate1,@reqid,@CourseCodeDescrip,@FinalScore,
                    @GradeSystemDetailId,@FinalGrade,@CurrentGrade,@CourseCredits,@ClsSectionId,@IsPass,@IsCreditsAttempted,@IsCreditsEarned,@GPA,@IsGPA,
                    @PrgVerId,@IsInGPA,@FinAidCredits; 
                WHILE @@FETCH_STATUS = 0
                    BEGIN
			
			-- If rounding is set to yes, then round the scores to next available score or ignore rounding
                        IF ( LOWER(@varGradeRounding) = 'yes' )
                            BEGIN
                                IF @FinalScore IS NOT NULL
                                    BEGIN
                                        SET @FinalScore = ROUND(@FinalScore,0);
                                        SET @CurrentScore = @FinalScore;
                                    END;
                                IF @FinalScore IS NULL
                                    AND @CurrentScore IS NOT NULL
                                    BEGIN
                                        SET @CurrentScore = ROUND(@CurrentScore,0);
                                    END;
                            END;
                        ELSE
                            BEGIN
                                IF @FinalScore IS NOT NULL
                                    BEGIN
                                        SET @CurrentScore = @FinalScore;
                                    END;	
                                IF @FinalScore IS NULL
                                    AND @CurrentScore IS NOT NULL
                                    BEGIN
                                        SET @CurrentScore = ROUND(@CurrentScore,0);
                                    END;
                            END;
				
                        UPDATE  syCreditSummary
                        SET     FinalScore = @FinalScore
                               ,CurrentScore = @CurrentScore
                        WHERE   StuEnrollId = @StuEnrollId
                                AND TermId = @TermId
                                AND ReqId = @reqid;

			--Average calculation
                        SET @TermAverageCount = (
                                                  SELECT    COUNT(*)
                                                  FROM      syCreditSummary
                                                  WHERE     StuEnrollId = @StuEnrollId
                                                            AND TermId = @TermId
                                                            AND FinalScore IS NOT NULL
                                                );
                        SET @termAverageSum = (
                                                SELECT  SUM(FinalScore)
                                                FROM    syCreditSummary
                                                WHERE   StuEnrollId = @StuEnrollId
                                                        AND TermId = @TermId
                                                        AND FinalScore IS NOT NULL
                                              );
                        SET @TermAverage = @termAverageSum / @TermAverageCount; 
			
			-- Cumulative Average
                        SET @cumAveragecount = (
                                                 SELECT COUNT(*)
                                                 FROM   syCreditSummary
                                                 WHERE  StuEnrollId = @StuEnrollId
                                                        AND FinalScore IS NOT NULL
                                               );
                        SET @cumAverageSum = (
                                               SELECT   SUM(FinalScore)
                                               FROM     syCreditSummary
                                               WHERE    StuEnrollId = @StuEnrollId
                                                        AND FinalScore IS NOT NULL
                                             );
                        SET @CumAverage = @cumAverageSum / @cumAveragecount; 
			
                        UPDATE  syCreditSummary
                        SET     Average = @TermAverage
                        WHERE   StuEnrollId = @StuEnrollId
                                AND TermId = @TermId; 
			
			--Update Cumulative GPA
                        UPDATE  syCreditSummary
                        SET     CumAverage = @CumAverage
                        WHERE   StuEnrollId = @StuEnrollId;
			

                        FETCH NEXT FROM GetCreditsSummary_Cursor INTO @StuEnrollId,@TermId,@TermDescrip,@termstartdate1,@reqid,@CourseCodeDescrip,@FinalScore,
                            @GradeSystemDetailId,@FinalGrade,@CurrentGrade,@CourseCredits,@ClsSectionId,@IsPass,@IsCreditsAttempted,@IsCreditsEarned,@GPA,@IsGPA,
                            @PrgVerId,@IsInGPA,@FinAidCredits;  
                    END;
                CLOSE GetCreditsSummary_Cursor;
                DEALLOCATE GetCreditsSummary_Cursor;

---- PRINT '@CurrentBal_Compute', 
---- PRINT @CurrentBal_Compute
		
            END;

        SET @TermAverageCount = (
                                  SELECT    COUNT(*)
                                  FROM      syCreditSummary
                                  WHERE     StuEnrollId = @StuEnrollId
                                            AND FinalScore IS NOT NULL
                                );
        SET @termAverageSum = (
                                SELECT  SUM(FinalScore)
                                FROM    syCreditSummary
                                WHERE   StuEnrollId = @StuEnrollId
                                        AND FinalScore IS NOT NULL
                              );
        IF @TermAverageCount >= 1
            BEGIN
                SET @TermAverage = @termAverageSum / @TermAverageCount; 
            END;
			
		-- Cumulative Average
        SET @cumAveragecount = (
                                 SELECT COUNT(*)
                                 FROM   syCreditSummary
                                 WHERE  StuEnrollId = @StuEnrollId
                                        AND FinalScore IS NOT NULL
                               );
        SET @cumAverageSum = (
                               SELECT   SUM(FinalScore)
                               FROM     syCreditSummary
                               WHERE    StuEnrollId = @StuEnrollId
                                        AND FinalScore IS NOT NULL
                             );
								
        IF @cumAveragecount >= 1
            BEGIN
                SET @CumAverage = @cumAverageSum / @cumAveragecount;
            END;    
			
        --If Clock Hour / Numeric - use New Average Calculation
        IF (
           SELECT TOP 1 P.ACId
           FROM   dbo.arStuEnrollments E
           JOIN   dbo.arPrgVersions PV ON PV.PrgVerId = E.PrgVerId
           JOIN   dbo.arPrograms P ON P.ProgId = PV.ProgId
           WHERE  E.StuEnrollId = @StuEnrollId
           ) = 5
           AND @GradesFormat = 'numeric'
            BEGIN
                EXEC dbo.USP_GPACalculator @EnrollmentId = @StuEnrollId
                                          ,@StudentGPA = @CumAverage OUTPUT;
            END;
	---- PRINT @cumAveragecount 
	---- PRINT @CumAverageSum
	---- PRINT @CumAverage
	---- PRINT @UnitTypeId;
	---- PRINT @TrackSapAttendance;
	---- PRINT @displayHours;
    END;
--=================================================================================================
-- END  --  Usp_TR_Sub4_GetCumGPAandAverageforAGivenStuEnrollId
--=================================================================================================	

GO
