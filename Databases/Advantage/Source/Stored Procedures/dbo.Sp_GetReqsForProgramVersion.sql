SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



  
CREATE PROCEDURE [dbo].[Sp_GetReqsForProgramVersion]
    (
     @CampusId UNIQUEIDENTIFIER
    ,@PrgVerId UNIQUEIDENTIFIER
    ,@IncludeCourseEquivalents BIT = 0 
	)
AS
    IF @PrgVerId = (
                     SELECT CAST(CAST(0 AS BINARY) AS UNIQUEIDENTIFIER)
                   )
        BEGIN
            SELECT  *
            FROM    arReqs
            WHERE   arReqs.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
                    AND arReqs.ReqTypeId = 1
                    AND arReqs.CampGrpId IN ( SELECT    CampGrpId
                                              FROM      syCmpGrpCmps
                                              WHERE     CampusId = @CampusId ); 
        END;
    IF @IncludeCourseEquivalents = 0
        BEGIN
            SELECT  arReqs.*
            FROM    arReqs
                   ,arProgVerDef
            WHERE   arReqs.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
                    AND arReqs.ReqTypeId = 1
                    AND arReqs.ReqId = arProgVerDef.ReqId
                    AND arProgVerDef.PrgVerId = @PrgVerId
                    AND arReqs.CampGrpId IN ( SELECT    CampGrpId
                                              FROM      syCmpGrpCmps
                                              WHERE     CampusId = @CampusId )
            UNION
            SELECT  T5.*
            FROM    arReqs t1
                   ,arProgVerDef t2
                   ,arReqGrpDef t4
                   ,arReqs T5
            WHERE   t1.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
                    AND T5.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
                    AND t1.ReqId = t4.GrpId
                    AND t1.ReqTypeId = 2
                    AND t1.ReqId = t2.ReqId
                    AND PrgVerId = @PrgVerId
                    AND t1.CampGrpId IN ( SELECT    CampGrpId
                                          FROM      syCmpGrpCmps
                                          WHERE     CampusId = @CampusId )
                    AND t4.ReqId = T5.ReqId; 
        END;      
		  
    ELSE
        BEGIN
            SELECT  arReqs.*
            FROM    arReqs
                   ,arProgVerDef
            WHERE   arReqs.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
                    AND arReqs.ReqTypeId = 1
                    AND arReqs.ReqId = arProgVerDef.ReqId
                    AND arProgVerDef.PrgVerId = @PrgVerId
                    AND arReqs.CampGrpId IN ( SELECT    CampGrpId
                                              FROM      syCmpGrpCmps
                                              WHERE     CampusId = @CampusId )
            UNION
            SELECT  T5.*
            FROM    arReqs t1
                   ,arProgVerDef t2
                   ,arReqGrpDef t4
                   ,arReqs T5
            WHERE   t1.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
                    AND T5.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
                    AND t1.ReqId = t4.GrpId
                    AND t1.ReqTypeId = 2
                    AND t1.ReqId = t2.ReqId
                    AND PrgVerId = @PrgVerId
                    AND t1.CampGrpId IN ( SELECT    CampGrpId
                                          FROM      syCmpGrpCmps
                                          WHERE     CampusId = @CampusId )
                    AND t4.ReqId = T5.ReqId
            UNION
			--Take course equivalents into account
			--Scenario where the ReqId in arCourseEquivalent is the course (ReqId) in the program version definition          
            SELECT  rq.*
            FROM    arReqs
                   ,arProgVerDef
                   ,dbo.arCourseEquivalent ce
                   ,arReqs rq
            WHERE   arReqs.ReqTypeId = 1
                    AND arReqs.ReqId = arProgVerDef.ReqId
                    AND arProgVerDef.PrgVerId = @PrgVerId
                    AND arReqs.CampGrpId IN ( SELECT    CampGrpId
                                              FROM      syCmpGrpCmps
                                              WHERE     CampusId = @CampusId )
                    AND dbo.arProgVerDef.ReqId = ce.ReqId
                    AND ce.EquivReqId = rq.ReqId
            UNION
			--Take course equivalents into account
			--Scenario where the EquivReqId in arCourseEquivalent is the course (ReqId) in the program version definition           
            SELECT  rq.*
            FROM    arReqs
                   ,arProgVerDef
                   ,dbo.arCourseEquivalent ce
                   ,arReqs rq
            WHERE   arReqs.ReqTypeId = 1
                    AND arReqs.ReqId = arProgVerDef.ReqId
                    AND arProgVerDef.PrgVerId = @PrgVerId
                    AND arReqs.CampGrpId IN ( SELECT    CampGrpId
                                              FROM      syCmpGrpCmps
                                              WHERE     CampusId = @CampusId )
                    AND dbo.arProgVerDef.ReqId = ce.EquivReqId
                    AND ce.ReqId = rq.ReqId
			--Take course equivalents into account
			--Course Groups: Scenario where the ReqId in arCourseEquivalent is the course (ReqId) in the course group definition in arReqGrpDef
            UNION
            SELECT  rq2.*
            FROM    arProgVerDef pvd
                   ,arReqs rq
                   ,arReqGrpDef rgd
                   ,arCourseEquivalent ce
                   ,arReqs rq2
            WHERE   pvd.ReqId = rq.ReqId
                    AND rq.ReqTypeId = 2
                    AND pvd.ReqId = rgd.GrpId
                    AND rgd.ReqId = ce.ReqId
                    AND pvd.PrgVerId = @PrgVerId
                    AND ce.EquivReqId = rq2.ReqId
                    AND rq.CampGrpId IN ( SELECT    CampGrpId
                                          FROM      syCmpGrpCmps
                                          WHERE     CampusId = @CampusId )
            UNION
			--Take course equivalents into account
			--Course Groups: Scenario where the EquivReqId in arCourseEquivalent is the course (ReqId) in the course group definition in arReqGrpDef          
            SELECT  rq2.*
            FROM    arProgVerDef pvd
                   ,arReqs rq
                   ,arReqGrpDef rgd
                   ,arCourseEquivalent ce
                   ,arReqs rq2
            WHERE   pvd.ReqId = rq.ReqId
                    AND rq.ReqTypeId = 2
                    AND pvd.ReqId = rgd.GrpId
                    AND rgd.ReqId = ce.EquivReqId
                    AND pvd.PrgVerId = @PrgVerId
                    AND ce.ReqId = rq2.ReqId
                    AND rq.CampGrpId IN ( SELECT    CampGrpId
                                          FROM      syCmpGrpCmps
                                          WHERE     CampusId = @CampusId ); 

        END;



GO
