SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[USP_SA_GetChargePeriodSequences_GetList]
    (
     @StuEnrollID UNIQUEIDENTIFIER
    ,@DefPeriod INT
    )
AS /*----------------------------------------------------------------------------------------------------
	Author          :	Saraswathi Lakshmanan
    
    Create date		:	28/05/2010
    
	Procedure Name	:	[USP_SA_GetChargePeriodSequences_GetList]

	Objective		:	get the academic year details or Payment periods
	
	Parameters		:	Name			Type	Data Type			Required? 	
						=====			====	=========			=========	
						@StuEnrollId	IN		Uniqueuidentifier	Yes
						@DefPeriod		IN		INt					Yes
	
	Output			:	Returns the requested details	
						
*/-----------------------------------------------------------------------------------------------------
    BEGIN

        SELECT  AR.*
        FROM    arStuEnrollments SE
               ,arPrgChargePeriodSeq AR
        WHERE   SE.StuEnrollId = @StuEnrollID
                AND AR.FeeLevelID = @DefPeriod
                AND AR.PrgVerId = SE.PrgVerId
        ORDER BY AR.Sequence;
    END;





GO
