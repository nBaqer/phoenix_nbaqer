SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE PROCEDURE [dbo].[USP_SA_GetAvailableAwardsForStudent_NoAidNew]
    (
     --@StuEnrollID uniqueIdentifier,
    --New Variable Added On June 09, 2010 For Mantis Id 18950--
     @CutoffDate DATETIME
    ,
    --New Variable Added On June 09, 2010 For Mantis Id 18950--
     @CampusId UNIQUEIDENTIFIER
    )
AS /*----------------------------------------------------------------------------------------------------
    Author : Saraswathi Lakshmanan
    
    Create date : 04/12/2010
    
    Procedure Name : [USP_SA_GetAvailableAwardsForStudent]

    Objective : Get the awards for the student
    
    Parameters : Name Type Data Type Required? 
                        ===== ==== ========= ========= 
                        @StuEnrollID In Uniqueidentifier Required
    
    Output : returns all the available awards for a student
                        
*/-----------------------------------------------------------------------------------------------------

    BEGIN
    --SELECT DISTINCT 
    -- T.AwardTypeId As FundSourceId, 
    -- F.FundSourceDescrip 
    --FROM 
    -- (select SA.AwardTypeId,SAS.AwardScheduleId, Amount - Coalesce((Select Sum(Amount) from saPmtDisbrel where AwardScheduleId=SAS.AwardScheduleId),0) 
    -- + Coalesce((Select Sum(TransAmount) from satransactions TR,saRefunds R where R.AwardScheduleId=SAS.AwardScheduleId and TR.transactionid=R.Transactionid),0) as Balance 
    -- from 
    -- faStudentAwards SA, faStudentAwardSchedule SAS 
    -- where 
    -- SA.StuEnrollId = @StuEnrollId and SA.StudentAwardId=SAS.StudentAwardId 
    -- ) T, saFundSources F 
    --WHERE 
    -- T.AwardTypeId=F.FundSourceId AND Balance > 0 
        SELECT  CCT.FundSourceId
               ,CCT.StatusId
               ,CCT.FundSourceCode
               ,CCT.FundSourceDescrip
               ,ST.StatusId
               ,ST.Status
               ,IsUsedInFA = ( CASE WHEN (
                                           SELECT   COUNT(*)
                                           FROM     faStudentAwards
                                           WHERE    AwardTypeId = CCT.FundSourceId
                                         ) > 0 THEN 'True'
                                    ELSE 'False'
                               END )
        FROM    saFundSources CCT
               ,syStatuses ST
        WHERE   CCT.StatusId = ST.StatusId
                AND ST.Status = 'Active'
                AND CCT.AdvFundSourceId = 1
                AND CCT.CampGrpId IN ( SELECT   CampGrpId
                                       FROM     syCmpGrpCmps
                                       WHERE    CampusId = @CampusId )
        ORDER BY FundSourceDescrip; 
    END;






GO
