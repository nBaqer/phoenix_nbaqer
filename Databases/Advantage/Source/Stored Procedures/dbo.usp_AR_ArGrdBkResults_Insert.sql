SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/* 
US4257 Course marked as incomplete when score of one of the component is removed

CREATED: 
7/24/2013 TT

PURPOSE: 
Insert arGrdBkResults


MODIFIED:


*/
CREATE   PROCEDURE [dbo].[usp_AR_ArGrdBkResults_Insert]
    @GrdBkResultId UNIQUEIDENTIFIER
   ,@ClsSectionId UNIQUEIDENTIFIER
   ,@StuEnrollId UNIQUEIDENTIFIER
   ,@InstrGrdBkWgtDetailId UNIQUEIDENTIFIER
   ,@Score DECIMAL(6, 2)
   ,@Comments VARCHAR(50)
   ,@ResNum INT = 0
   ,@ModUser VARCHAR(50)
   ,@IsCompGraded BIT
   ,@DateCompleted DATE = NULL
   ,@PostDate DATE = NULL
   ,@Adjustment INT = NULL
AS
    BEGIN

        DECLARE @NewResNum INT = 0;
        SET @NewResNum = ( ISNULL((
                                  SELECT MAX(ResNum)
                                  FROM   arGrdBkResults
                                  WHERE  StuEnrollId = @StuEnrollId
                                         AND ClsSectionId = @ClsSectionId
                                         AND InstrGrdBkWgtDetailId = @InstrGrdBkWgtDetailId
                                  )
                                 ,0
                                 )
                         ) + 1;



        SET NOCOUNT ON;


        --new code to insert arGrdBkResults for clinic services by bulk
        IF ( @Adjustment > 1 )
            BEGIN

                DECLARE @ToInsert TABLE
                    (
                        [GrdBkResultId] UNIQUEIDENTIFIER
                       ,[ClsSectionId] UNIQUEIDENTIFIER
                       ,[InstrGrdBkWgtDetailId] UNIQUEIDENTIFIER
                       ,[Score] DECIMAL(6, 2)
                       ,[Comments] VARCHAR(50)
                       ,[StuEnrollId] UNIQUEIDENTIFIER
                       ,[ModUser] VARCHAR(50)
                       ,[ModDate] DATETIME
                       ,[ResNum] INT
                       ,[PostDate] SMALLDATETIME
                       ,[IsCompGraded] BIT
                       ,[isCourseCredited] BIT
                       ,[DateCompleted] DATE
                    );

                --DECLARE @Numbers TABLE
                --    (
                --        num INT IDENTITY(1, 1)
                --    );


                INSERT INTO @ToInsert (
                                      GrdBkResultId
                                     ,ClsSectionId
                                     ,StuEnrollId
                                     ,InstrGrdBkWgtDetailId
                                     ,Score
                                     ,Comments
                                     ,ResNum
                                     ,ModUser
                                     ,ModDate
                                     ,PostDate
                                     ,IsCompGraded
                                     ,DateCompleted
                                      )
                VALUES ( @GrdBkResultId
                                       -- GrdBkResultId - uniqueidentifier
                        ,@ClsSectionId
                                       -- ClsSectionId - uniqueidentifier
                        ,@StuEnrollId
                                       -- StuEnrollId - uniqueidentifier
                        ,@InstrGrdBkWgtDetailId
                                       -- InstrGrdBkWgtDetailId- uniqueidentifier
                        ,@Score
                                       -- Score  - decimal
                        ,@Comments
                                       -- Comments - varchar
                        ,@Adjustment
                                       -- ResNum - int
                        ,@ModUser
                                       -- ModUser - varchar
                        ,GETDATE()
                                       -- ModDate - datetime
                        ,CASE WHEN ( @PostDate IS NULL ) THEN GETDATE()
                              ELSE @PostDate
                         END
                                       -- PostDate - datetime
                        ,@IsCompGraded --IsCompGraded - bit
                                       -- DateCompleted - date
                        ,@PostDate );


                SELECT     TOP 1000 IDENTITY(INT, 1, 1) AS num
                INTO       #numbers
                FROM       sys.objects s1 --use sys.columns if you don't get enough rows returned to generate all the numbers you need
                CROSS JOIN sys.objects s2;


                INSERT INTO dbo.arGrdBkResults
                            SELECT   NEWID() AS GrdBkResultId
                                    ,yt.ClsSectionId
                                    ,yt.InstrGrdBkWgtDetailId
                                    ,yt.Score
                                    ,yt.Comments
                                    ,yt.StuEnrollId
                                    ,yt.ModUser
                                    ,yt.ModDate
                                    ,n.num + @NewResNum - 1 AS ResNum
                                    ,yt.PostDate
                                    ,yt.IsCompGraded
                                    ,yt.isCourseCredited
                                    ,yt.DateCompleted
                            FROM     @ToInsert yt
                            JOIN     #numbers n ON n.num <= yt.ResNum
                            ORDER BY yt.ResNum
                                    ,n.num;

                DROP TABLE #numbers;


            END;
        ELSE
            BEGIN
                INSERT INTO arGrdBkResults (
                                           GrdBkResultId
                                          ,ClsSectionId
                                          ,StuEnrollId
                                          ,InstrGrdBkWgtDetailId
                                          ,Score
                                          ,Comments
                                          ,ResNum
                                          ,ModUser
                                          ,ModDate
                                          ,PostDate
                                          ,IsCompGraded
                                          ,DateCompleted
                                           )
                VALUES ( NEWID()
                                       -- GrdBkResultId - uniqueidentifier
                        ,@ClsSectionId
                                       -- ClsSectionId - uniqueidentifier
                        ,@StuEnrollId
                                       -- StuEnrollId - uniqueidentifier
                        ,@InstrGrdBkWgtDetailId
                                       -- InstrGrdBkWgtDetailId- uniqueidentifier
                        ,@Score
                                       -- Score  - decimal
                        ,@Comments
                                       -- Comments - varchar
                        ,@NewResNum
                                       -- ResNum - int
                        ,@ModUser
                                       -- ModUser - varchar
                        ,GETDATE()
                                       -- ModDate - datetime
                        ,CASE WHEN ( @PostDate IS NULL ) THEN GETDATE()
                              ELSE @PostDate
                         END
                                       -- PostDate - datetime
                        ,@IsCompGraded --IsCompGraded - bit
                                       -- DateCompleted - date
                        ,@PostDate );
            END;

        UPDATE arResults
        SET    IsCourseCompleted = 0
              ,Score = NULL
              ,GrdSysDetailId = NULL
        WHERE  TestId = @ClsSectionId
               AND StuEnrollId = @StuEnrollId;

    END;
    SET ANSI_NULLS ON;




GO
