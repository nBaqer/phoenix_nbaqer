SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jose Alfredo
-- Create date: 4/15/2014
-- Description:	This get the internal setting 
--              for WAPI services. it use only 
--              for WAPI
-- =============================================
CREATE PROCEDURE [dbo].[WapiGetSettingsService]
AS
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;

	-- Insert statements for procedure here
        SELECT  ws.Code
               ,wec.Code
               ,Url
        FROM    dbo.syWapiAllowedServices ws
        JOIN    dbo.syWapiBridgeExternalCompanyAllowedServices wbridge ON wbridge.IdAllowedServices = ws.Id
        JOIN    dbo.syWapiExternalCompanies wec ON wec.Id = wbridge.IdExternalCompanies
        WHERE   ws.IsActive = 1;
    END;




GO
