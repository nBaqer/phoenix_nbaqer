SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

--=================================================================================================
-- USP_TC_Step03_TransferCreditsSyCreditSummary
--=================================================================================================
CREATE PROCEDURE [dbo].[USP_TC_Step03_TransferCreditsSyCreditSummary]
AS -- 
    -- Step 03_Transfer credits to syCreditSummary
    BEGIN -- Step 03_Transfer credits to syCreditSummary
        DECLARE @PrgVerId UNIQUEIDENTIFIER
               ,@rownumber INT
               ,@StuEnrollId UNIQUEIDENTIFIER;
        DECLARE @PrevStuEnrollId UNIQUEIDENTIFIER
               ,@PrevReqId UNIQUEIDENTIFIER
               ,@PrevTermId UNIQUEIDENTIFIER
               ,@CreditsAttempted DECIMAL(18, 2)
               ,@CreditsEarned DECIMAL(18, 2)
               ,@TermId UNIQUEIDENTIFIER
               ,@TermDescrip VARCHAR(50);
        DECLARE @reqid UNIQUEIDENTIFIER
               ,@CourseCodeDescrip VARCHAR(50)
               ,@FinalGrade VARCHAR(50)
               ,@FinalScore DECIMAL(18, 2)
               ,@ClsSectionId UNIQUEIDENTIFIER
               ,@Grade VARCHAR(50)
               ,@IsGradeBookNotSatisified BIT
               ,@TermStartDate DATETIME;
        DECLARE @IsPass BIT
               ,@IsCreditsAttempted BIT
               ,@IsCreditsEarned BIT
               ,@Completed BIT
               ,@CurrentScore DECIMAL(18, 2)
               ,@CurrentGrade VARCHAR(10)
               ,@FinalGradeDesc VARCHAR(50)
               ,@FinalGPA DECIMAL(18, 2)
               ,@GrdBkResultId UNIQUEIDENTIFIER;
        DECLARE @Product_WeightedAverage_Credits_GPA DECIMAL(18, 2)
               ,@Count_WeightedAverage_Credits DECIMAL(18, 2)
               ,@Product_SimpleAverage_Credits_GPA DECIMAL(18, 2)
               ,@Count_SimpleAverage_Credits DECIMAL(18, 2);
        DECLARE @CreditsPerService DECIMAL(18, 2)
               ,@NumberOfServicesAttempted INT
               ,@boolCourseHasLabWorkOrLabHours INT
               ,@sysComponentTypeId INT
               ,@RowCount INT;
        DECLARE @decGPALoop DECIMAL(18, 2)
               ,@intCourseCount INT
               ,@decWeightedGPALoop DECIMAL(18, 2)
               ,@IsInGPA BIT
               ,@isGradeEligibleForCreditsEarned BIT
               ,@isGradeEligibleForCreditsAttempted BIT;
        DECLARE @ComputedSimpleGPA DECIMAL(18, 2)
               ,@ComputedWeightedGPA DECIMAL(18, 2)
               ,@CourseCredits DECIMAL(18, 2)
               ,@GradeSystemDetailId UNIQUEIDENTIFIER
               ,@GPA DECIMAL(18, 2)
               ,@IsGPA BIT;
        DECLARE @FinAidCreditsEarned DECIMAL(18, 2)
               ,@FinAidCredits DECIMAL(18, 2)
               ,@TermAverage DECIMAL(18, 2)
               ,@TermAverageCount INT;
        DECLARE @IsWeighted INT;
        SET @decGPALoop = 0;
        SET @intCourseCount = 0;
        SET @decWeightedGPALoop = 0;
        SET @ComputedSimpleGPA = 0;
        SET @ComputedWeightedGPA = 0;
        SET @CourseCredits = 0;
        DECLARE GetCreditsSummary_Cursor CURSOR FOR
            SELECT DISTINCT SE.StuEnrollId
                  ,T.TermId
                  ,T.TermDescrip
                  ,T.StartDate
                  ,R.ReqId
                  ,'(' + R.Code + ' ) ' + R.Descrip AS CourseCodeDescrip
                  ,NULL AS FinalScore
                  ,RES.GrdSysDetailId AS GradeSystemDetailId
                  ,GSD.Grade AS FinalGrade
                  ,GSD.Grade AS CurrentGrade
                  ,R.Credits
                  ,NULL AS ClsSectionId
                  ,GSD.IsPass
                  ,GSD.IsCreditsAttempted
                  ,GSD.IsCreditsEarned
                  ,GSD.GPA
                  ,GSD.IsInGPA
                  ,SE.PrgVerId
                  ,GSD.IsInGPA
                  ,R.FinAidCredits AS FinAidCredits
            FROM   arStuEnrollments SE
            INNER JOIN arTransferGrades RES ON SE.StuEnrollId = RES.StuEnrollId
            INNER JOIN arGradeSystemDetails GSD ON RES.GrdSysDetailId = GSD.GrdSysDetailId
            INNER JOIN arReqs R ON RES.ReqId = R.ReqId
            INNER JOIN arTerm T ON RES.TermId = T.TermId
            WHERE  SE.StuEnrollId NOT IN (
                                         SELECT DISTINCT StuEnrollId
                                         FROM   syCreditSummary
                                         );
        --and SE.StuEnrollId = '9CEB67F9-37FD-4943-A1B4-E8FB0F92C0F6' 


        OPEN GetCreditsSummary_Cursor;
        SET @PrevStuEnrollId = NULL;
        SET @PrevTermId = NULL;
        SET @PrevReqId = NULL;
        SET @RowCount = 0;
        FETCH NEXT FROM GetCreditsSummary_Cursor
        INTO @StuEnrollId
            ,@TermId
            ,@TermDescrip
            ,@TermStartDate
            ,@reqid
            ,@CourseCodeDescrip
            ,@FinalScore
            ,@GradeSystemDetailId
            ,@FinalGrade
            ,@CurrentGrade
            ,@CourseCredits
            ,@ClsSectionId
            ,@IsPass
            ,@IsCreditsAttempted
            ,@IsCreditsEarned
            ,@GPA
            ,@IsGPA
            ,@PrgVerId
            ,@IsInGPA
            ,@FinAidCredits;
        WHILE @@FETCH_STATUS = 0
            BEGIN
                DECLARE @DefaultCampusId UNIQUEIDENTIFIER;
                SET @DefaultCampusId = NULL;
                SET @DefaultCampusId = (
                                       SELECT TOP 1 CampusId
                                       FROM   arStuEnrollments
                                       WHERE  StuEnrollId = @StuEnrollId
                                       );

                IF (@GradeSystemDetailId IS NOT NULL)
                    BEGIN
                        SET @Completed = 1;
                        IF (@IsCreditsAttempted = 1)
                            BEGIN
                                SET @CreditsAttempted = @CourseCredits;
                            END;
                        IF (@IsPass = 1)
                            BEGIN
                                IF (@IsCreditsEarned = 1)
                                    BEGIN
                                        SET @CreditsEarned = @CourseCredits;
                                        SET @FinAidCreditsEarned = @FinAidCredits;
                                    END;
                            END;
                    END;

                SET @FinalGPA = (
                                SELECT GPA
                                FROM   arGradeSystemDetails
                                WHERE  GrdSysDetailId = @GradeSystemDetailId
                                );

                -- Get the final Gpa only when IsCreditsAttempted is set to 1 and IsInGPA is set to 1
                IF @IsInGPA = 1
                    BEGIN
                        IF (@IsCreditsAttempted = 0)
                            BEGIN
                                SET @FinalGPA = NULL;
                            END;
                    END;
                ELSE
                    BEGIN
                        SET @FinalGPA = NULL;
                    END;

                IF @FinalScore IS NOT NULL
                   AND @CurrentScore IS NULL
                    BEGIN
                        SET @CurrentScore = @FinalScore;
                    END;

                -- Rally case DE 738 KeyBoarding Courses
                SET @IsWeighted = (
                                  SELECT COUNT(*) AS WeightsCount
                                  FROM   (
                                         SELECT T.TermId
                                               ,T.TermDescrip
                                               ,'(' + R.Code + ' ) ' + R.Descrip AS CourseCodeDescrip
                                               ,R.ReqId
                                               ,RTRIM(GCT.Descrip) AS GradeBookDescription
                                               ,(CASE WHEN GCT.SysComponentTypeId IN ( 500, 503, 504, 544 ) THEN GBWD.Number
                                                      ELSE (
                                                           SELECT MIN(MinVal)
                                                           FROM   arGradeScaleDetails GSD
                                                                 ,arGradeSystemDetails GSS
                                                           WHERE  GSD.GrdSysDetailId = GSS.GrdSysDetailId
                                                                  AND GSS.IsPass = 1
                                                           )
                                                 END
                                                ) AS MinimumScore
                                               ,GBR.Score AS Score
                                               ,GBWD.Weight AS Weight
                                               ,RES.Score AS FinalScore
                                               ,RES.GrdSysDetailId AS FinalGrade
                                               ,GBWD.Required
                                               ,GBWD.MustPass
                                               ,GBWD.GrdPolicyId
                                               ,(CASE GCT.SysComponentTypeId
                                                      WHEN 544 THEN (
                                                                    SELECT SUM(HoursAttended)
                                                                    FROM   arExternshipAttendance
                                                                    WHERE  StuEnrollId = SE.StuEnrollId
                                                                    )
                                                      ELSE GBR.Score
                                                 END
                                                ) AS GradeBookResult
                                               ,GCT.SysComponentTypeId
                                               ,SE.StuEnrollId
                                               ,GBR.GrdBkResultId
                                               ,R.Credits AS CreditsAttempted
                                               ,CS.ClsSectionId
                                               ,GSD.Grade
                                               ,GSD.IsPass
                                               ,GSD.IsCreditsAttempted
                                               ,GSD.IsCreditsEarned
                                         FROM   arStuEnrollments SE
                                         INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                                         INNER JOIN arTransferGrades RES ON RES.StuEnrollId = SE.StuEnrollId
                                         INNER JOIN arClassSections CS ON RES.ReqId = CS.ReqId
                                                                          AND RES.TermId = CS.TermId
                                         INNER JOIN arTerm T ON CS.TermId = T.TermId
                                         INNER JOIN arReqs R ON CS.ReqId = R.ReqId
                                         LEFT JOIN arGrdBkResults GBR ON CS.ClsSectionId = GBR.ClsSectionId
                                         LEFT JOIN arGrdBkWgtDetails GBWD ON GBR.InstrGrdBkWgtDetailId = GBWD.InstrGrdBkWgtDetailId
                                         LEFT JOIN arGrdComponentTypes GCT ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
                                         LEFT JOIN arGradeSystemDetails GSD ON RES.GrdSysDetailId = GSD.GrdSysDetailId
                                         WHERE  SE.StuEnrollId = @StuEnrollId
                                                AND T.TermId = @TermId
                                                AND R.ReqId = @reqid
                                         ) D
                                  WHERE  Weight >= 1
                                  );

                -- Rally case DE 738 KeyBoarding Courses
                /************************************************* Changes for Build 2816 *********************/
                -- Rally case DE 738 KeyBoarding Courses
                DECLARE @GradesFormat VARCHAR(50);
                --set @GradesFormat = (select Value from syConfigAppSetValues where SettingId=47) -- 47 refers to grades format
                SET @GradesFormat = (
                                    SELECT TOP 1 Value
                                    FROM   syConfigAppSetValues
                                    WHERE  SettingId = 47
                                           AND (
                                               CampusId = @DefaultCampusId
                                               OR CampusId IS NULL
                                               )
                                    ); -- 47 refers to grades format
                -- This condition is met only for numeric grade schools
                IF (
                   @IsGradeBookNotSatisified = 0
                   AND @IsWeighted = 0
                   AND @FinalScore IS NULL
                   AND @FinalGradeDesc IS NULL
                   AND LOWER(LTRIM(RTRIM(@GradesFormat))) <> 'letter'
                   )
                    BEGIN
                        SET @Completed = 1;
                        SET @CreditsAttempted = (
                                                SELECT Credits
                                                FROM   arReqs
                                                WHERE  ReqId = @reqid
                                                );
                        SET @FinAidCredits = (
                                             SELECT FinAidCredits
                                             FROM   arReqs
                                             WHERE  ReqId = @reqid
                                             );
                        SET @CreditsAttempted = @CreditsAttempted;
                        SET @CreditsEarned = @CreditsAttempted;
                        SET @FinAidCreditsEarned = @FinAidCredits;
                    END;

                -- DE748 Name: ROSS: Completed field should also check for the Must Pass property of the work unit. 
                IF LOWER(LTRIM(RTRIM(@GradesFormat))) <> 'letter'
                   AND @IsGradeBookNotSatisified >= 1
                    BEGIN
                        SET @Completed = 0;
                        SET @CreditsEarned = 0;
                        SET @FinAidCreditsEarned = 0;
                    END;

                --DE738 Name: ROSS: Progress Report not taking care of courses that are not weighted. 

                IF (
                   LOWER(LTRIM(RTRIM(@GradesFormat))) <> 'letter'
                   AND @Completed = 1
                   AND @FinalScore IS NULL
                   AND @FinalGradeDesc IS NULL
                   )
                    BEGIN
                        SET @CreditsAttempted = @CreditsAttempted;
                        SET @CreditsEarned = @CreditsAttempted;
                        SET @FinAidCreditsEarned = @FinAidCredits;
                    END;

                -- In Ross Example : Externship, the student may not have completed the course but once he attempts a work unit
                -- we need to take the credits as attempted
                IF (
                   LOWER(LTRIM(RTRIM(@GradesFormat))) <> 'letter'
                   AND @Completed = 0
                   AND @FinalScore IS NULL
                   AND @FinalGradeDesc IS NULL
                   )
                    BEGIN
                        DECLARE @rowcount4 INT;
                        SET @rowcount4 = (
                                         SELECT COUNT(*)
                                         FROM   arGrdBkResults
                                         WHERE  StuEnrollId = @StuEnrollId
                                                AND ClsSectionId = @ClsSectionId
                                                AND Score IS NOT NULL
                                         );
                        IF @rowcount4 >= 1
                            BEGIN
                                SET @CreditsAttempted = (
                                                        SELECT Credits
                                                        FROM   arReqs
                                                        WHERE  ReqId = @reqid
                                                        );
                                SET @CreditsEarned = 0;
                                SET @FinAidCreditsEarned = 0;
                            END;
                        ELSE
                            BEGIN
                                SET @rowcount4 = (
                                                 SELECT COUNT(*)
                                                 FROM   arGrdBkConversionResults
                                                 WHERE  StuEnrollId = @StuEnrollId
                                                        AND ReqId = @reqid
                                                        AND TermId = @TermId
                                                        AND Score IS NOT NULL
                                                 );
                                IF @rowcount4 >= 1
                                    BEGIN
                                        SET @CreditsAttempted = (
                                                                SELECT Credits
                                                                FROM   arReqs
                                                                WHERE  ReqId = @reqid
                                                                );
                                        SET @CreditsEarned = 0;
                                        SET @FinAidCreditsEarned = 0;
                                    END;
                            END;

                        --For Externship Attendance						
                        IF @sysComponentTypeId = 544
                            BEGIN
                                SET @rowcount4 = (
                                                 SELECT COUNT(*)
                                                 FROM   arExternshipAttendance
                                                 WHERE  StuEnrollId = @StuEnrollId
                                                        AND HoursAttended >= 1
                                                 );
                                IF @rowcount4 >= 1
                                    BEGIN
                                        SET @CreditsAttempted = (
                                                                SELECT Credits
                                                                FROM   arReqs
                                                                WHERE  ReqId = @reqid
                                                                );
                                        SET @CreditsEarned = 0;
                                        SET @FinAidCreditsEarned = 0;
                                    END;

                            END;

                    END;
                /************************************************* Changes for Build 2816 *********************/

                IF @FinalGrade IS NOT NULL
                    BEGIN
                        SET @CurrentGrade = @FinalGrade;
                    END;

                /************************* Case added for brownson starts here ******************/
                -- For Letter Grade Schools, if any one of work unit is attempted and if no final grade is posted then 
                -- set credit attempted
                DECLARE @IsScorePostedForAnyWorkUnit INT; -- If value>=1 then score was posted
                SET @IsScorePostedForAnyWorkUnit = (
                                                   SELECT COUNT(*)
                                                   FROM   arGrdBkResults
                                                   WHERE  StuEnrollId = @StuEnrollId
                                                          AND ClsSectionId = @ClsSectionId
                                                          AND Score IS NOT NULL
                                                   );
                IF (
                   LOWER(LTRIM(RTRIM(@GradesFormat))) = 'letter'
                   AND @FinalGradeDesc IS NULL
                   AND @IsScorePostedForAnyWorkUnit >= 1
                   )
                    BEGIN
                        SET @Completed = 0;
                        SET @CreditsAttempted = (
                                                SELECT Credits
                                                FROM   arReqs
                                                WHERE  ReqId = @reqid
                                                );
                        SET @CreditsEarned = 0;
                        SET @FinAidCreditsEarned = 0;
                    END;
                /************************* Case added for brownson ends here ******************/

                -- DE 996 Transfer Grades has completed set to no even when the credits were earned.
                -- If Credits was earned set completed to yes
                IF @IsCreditsEarned = 1
                    BEGIN
                        SET @Completed = 1;
                    END;


                DELETE FROM syCreditSummary
                WHERE StuEnrollId = @StuEnrollId
                      AND TermId = @TermId
                      AND ReqId = @reqid;

                INSERT INTO syCreditSummary
                VALUES (@StuEnrollId
                       ,@TermId
                       ,@TermDescrip
                       ,@reqid
                       ,@CourseCodeDescrip
                       ,@ClsSectionId
                       ,@CreditsEarned
                       ,@CreditsAttempted
                       ,@CurrentScore
                       ,@CurrentGrade
                       ,@FinalScore
                       ,@FinalGrade
                       ,@Completed
                       ,@FinalGPA
                       ,@Product_WeightedAverage_Credits_GPA
                       ,@Count_WeightedAverage_Credits
                       ,@Product_SimpleAverage_Credits_GPA
                       ,@Count_SimpleAverage_Credits
                       ,'sa'
                       ,GETDATE()
                       ,@ComputedSimpleGPA
                       ,@ComputedWeightedGPA
                       ,@CourseCredits
                       ,NULL
                       ,NULL
                       ,@FinAidCreditsEarned
                       ,NULL
                       ,NULL
                       ,@TermStartDate
                       );

                DECLARE @wCourseCredits DECIMAL(18, 2)
                       ,@wWeighted_GPA_Credits DECIMAL(18, 2)
                       ,@sCourseCredits DECIMAL(18, 2)
                       ,@sSimple_GPA_Credits DECIMAL(18, 2);
                -- For weighted average
                SET @ComputedWeightedGPA = 0;
                SET @ComputedSimpleGPA = 0;
                SET @wCourseCredits = (
                                      SELECT SUM(coursecredits)
                                      FROM   syCreditSummary
                                      WHERE  StuEnrollId = @StuEnrollId
                                             AND TermId = @TermId
                                             AND FinalGPA IS NOT NULL
                                      );
                SET @wWeighted_GPA_Credits = (
                                             SELECT SUM(coursecredits * FinalGPA)
                                             FROM   syCreditSummary
                                             WHERE  StuEnrollId = @StuEnrollId
                                                    AND TermId = @TermId
                                                    AND FinalGPA IS NOT NULL
                                             );

                IF @wCourseCredits >= 1
                    BEGIN
                        SET @ComputedWeightedGPA = @wWeighted_GPA_Credits / @wCourseCredits;
                    END;



                --For Simple Average
                SET @sCourseCredits = (
                                      SELECT COUNT(*)
                                      FROM   syCreditSummary
                                      WHERE  StuEnrollId = @StuEnrollId
                                             AND TermId = @TermId
                                             AND FinalGPA IS NOT NULL
                                      );
                SET @sSimple_GPA_Credits = (
                                           SELECT SUM(FinalGPA)
                                           FROM   syCreditSummary
                                           WHERE  StuEnrollId = @StuEnrollId
                                                  AND TermId = @TermId
                                                  AND FinalGPA IS NOT NULL
                                           );
                IF @sCourseCredits >= 1
                    BEGIN
                        SET @ComputedSimpleGPA = @sSimple_GPA_Credits / @sCourseCredits;
                    END;


                --CumulativeGPA
                DECLARE @cumCourseCredits DECIMAL(18, 2)
                       ,@cumWeighted_GPA_Credits DECIMAL(18, 2)
                       ,@cumWeightedGPA DECIMAL(18, 2);
                SET @cumWeightedGPA = 0;
                SET @cumCourseCredits = (
                                        SELECT SUM(coursecredits)
                                        FROM   syCreditSummary
                                        WHERE  StuEnrollId = @StuEnrollId
                                               AND FinalGPA IS NOT NULL
                                        );
                SET @cumWeighted_GPA_Credits = (
                                               SELECT SUM(coursecredits * FinalGPA)
                                               FROM   syCreditSummary
                                               WHERE  StuEnrollId = @StuEnrollId
                                                      AND FinalGPA IS NOT NULL
                                               );

                IF @cumCourseCredits >= 1
                    BEGIN
                        SET @cumWeightedGPA = @cumWeighted_GPA_Credits / @cumCourseCredits;
                    END;

                --CumulativeSimpleGPA
                DECLARE @cumSimpleCourseCredits DECIMAL(18, 2)
                       ,@cumSimple_GPA_Credits DECIMAL(18, 2)
                       ,@cumSimpleGPA DECIMAL(18, 2);
                SET @cumSimpleGPA = 0;
                SET @cumSimpleCourseCredits = (
                                              SELECT COUNT(coursecredits)
                                              FROM   syCreditSummary
                                              WHERE  StuEnrollId = @StuEnrollId
                                                     AND FinalGPA IS NOT NULL
                                              );
                SET @cumSimple_GPA_Credits = (
                                             SELECT SUM(FinalGPA)
                                             FROM   syCreditSummary
                                             WHERE  StuEnrollId = @StuEnrollId
                                                    AND FinalGPA IS NOT NULL
                                             );

                IF @cumSimpleCourseCredits >= 1
                    BEGIN
                        SET @cumSimpleGPA = @cumSimple_GPA_Credits / @cumSimpleCourseCredits;
                    END;

                --Average calculation
                DECLARE @termAverageSum DECIMAL(18, 2)
                       ,@CumAverage DECIMAL(18, 2)
                       ,@cumAverageSum DECIMAL(18, 2)
                       ,@cumAveragecount INT;

                -- Term Average
                SET @TermAverageCount = (
                                        SELECT COUNT(*)
                                        FROM   syCreditSummary
                                        WHERE  StuEnrollId = @StuEnrollId
                                               --and Completed=1 
                                               AND TermId = @TermId
                                               AND FinalScore IS NOT NULL
                                        );
                SET @termAverageSum = (
                                      SELECT SUM(FinalScore)
                                      FROM   syCreditSummary
                                      WHERE  StuEnrollId = @StuEnrollId
                                             --and Completed=1 
                                             AND TermId = @TermId
                                             AND FinalScore IS NOT NULL
                                      );
                SET @TermAverage = @termAverageSum / @TermAverageCount;

                -- Cumulative Average
                SET @cumAveragecount = (
                                       SELECT COUNT(*)
                                       FROM   syCreditSummary
                                       WHERE  StuEnrollId = @StuEnrollId
                                              --and Completed=1 
                                              AND FinalScore IS NOT NULL
                                       );
                SET @cumAverageSum = (
                                     SELECT SUM(FinalScore)
                                     FROM   syCreditSummary
                                     WHERE  StuEnrollId = @StuEnrollId
                                            AND
                                         --Completed=1 and 
                                         FinalScore IS NOT NULL
                                     );
                SET @CumAverage = @cumAverageSum / @cumAveragecount;



                UPDATE syCreditSummary
                SET    TermGPA_Simple = @ComputedSimpleGPA
                      ,TermGPA_Weighted = @ComputedWeightedGPA
                      ,Average = @TermAverage
                WHERE  StuEnrollId = @StuEnrollId
                       AND TermId = @TermId;

                --Update Cumulative GPA
                UPDATE syCreditSummary
                SET    CumulativeGPA = @cumWeightedGPA
                      ,CumulativeGPA_Simple = @cumSimpleGPA
                      ,CumAverage = @CumAverage
                WHERE  StuEnrollId = @StuEnrollId;


                SET @PrevStuEnrollId = @StuEnrollId;
                SET @PrevTermId = @TermId;
                SET @PrevReqId = @reqid;

                FETCH NEXT FROM GetCreditsSummary_Cursor
                INTO @StuEnrollId
                    ,@TermId
                    ,@TermDescrip
                    ,@TermStartDate
                    ,@reqid
                    ,@CourseCodeDescrip
                    ,@FinalScore
                    ,@GradeSystemDetailId
                    ,@FinalGrade
                    ,@CurrentGrade
                    ,@CourseCredits
                    ,@ClsSectionId
                    ,@IsPass
                    ,@IsCreditsAttempted
                    ,@IsCreditsEarned
                    ,@GPA
                    ,@IsGPA
                    ,@PrgVerId
                    ,@IsInGPA
                    ,@FinAidCredits;
            END;
        CLOSE GetCreditsSummary_Cursor;
        DEALLOCATE GetCreditsSummary_Cursor;
    END;
--=================================================================================================
-- END  --  USP_TC_Step03_TransferCreditsSyCreditSummary
--=================================================================================================
GO
