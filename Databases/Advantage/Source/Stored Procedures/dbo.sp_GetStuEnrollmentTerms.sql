SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[sp_GetStuEnrollmentTerms]
    @StuEnrollId VARCHAR(100)
AS
    BEGIN  
        SET FMTONLY OFF;  
      
        IF OBJECT_ID('tempdb..#tmpStuEnrollmentTerms') IS NOT NULL
            DROP TABLE #tmpStuEnrollmentTerms;  
      
      
        CREATE TABLE #tmpStuEnrollmentTerms
            (
             TermId VARCHAR(100)
            ,TermDescrip VARCHAR(100)
            ,StartDate DATETIME
            );  
      
        INSERT  INTO #tmpStuEnrollmentTerms
                SELECT DISTINCT
                        C.TermId
                       ,C.TermDescrip
                       ,C.StartDate
                FROM    arResults A
                INNER JOIN arClassSections B ON A.TestId = B.ClsSectionId
                INNER JOIN arTerm AS C ON B.TermId = C.TermId
                INNER JOIN syStatuses AS S ON C.StatusId = S.StatusId
                WHERE   A.StuEnrollId = @StuEnrollId
                        AND C.StartDate <= GETDATE()
                        AND S.Status = 'Active';  
      
        IF (
             SELECT COUNT(*)
             FROM   #tmpStuEnrollmentTerms
           ) > 1
            BEGIN  
                INSERT  INTO #tmpStuEnrollmentTerms
                VALUES  ( '00000000-0000-0000-0000-000000000000','All',GETDATE() );  
            END;  
      
        SELECT  TermId
               ,TermDescrip
        FROM    #tmpStuEnrollmentTerms
        ORDER BY StartDate DESC;  
      
    END;  
      
GO
