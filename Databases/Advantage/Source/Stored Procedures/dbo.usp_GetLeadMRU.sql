SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_GetLeadMRU]
    @UserId UNIQUEIDENTIFIER
   ,@CampusId UNIQUEIDENTIFIER
AS
    SET NOCOUNT ON;
-- For new users pull 20 students from the user's default campus
-- For existing users pull 20 students from the syMRU table, students can be from multiple campuses
    DECLARE @CountMRU INT;
    DECLARE @DefaultCampusId UNIQUEIDENTIFIER;
-- used to return MRU results
    DECLARE @MRUList TABLE
        (
         MRUId UNIQUEIDENTIFIER NOT NULL
        ,Counter INT NOT NULL
        ,ChildId UNIQUEIDENTIFIER NOT NULL
        ,MRUTypeId TINYINT NOT NULL
        ,UserId UNIQUEIDENTIFIER NOT NULL
        ,CampusId UNIQUEIDENTIFIER NULL
        ,SortOrder INT NULL
        ,IsSticky BIT NULL
        ,LeadId UNIQUEIDENTIFIER NOT NULL
        ,FullName VARCHAR(200) NOT NULL
        ,CampusDescrip VARCHAR(100) NOT NULL
        ,ModDate DATETIME
        );
-- used to create default MRU list for the user if one doesn't exist
    DECLARE @DefaultMRU TABLE
        (
         ChildId UNIQUEIDENTIFIER NOT NULL
        ,MRUTypeId TINYINT NOT NULL
        ,UserId UNIQUEIDENTIFIER NOT NULL
        ,CampusId UNIQUEIDENTIFIER NULL
        ,SortOrder INT NOT NULL
                       IDENTITY
        ,IsSticky BIT NULL
        ,ModDate DATETIME
        );
    SELECT  @CountMRU = COUNT(*)
    FROM    dbo.syMRUS
    WHERE   UserId = @UserId
            AND MRUTypeId = 4; -- Leads
    IF ( @CountMRU > 0 )
        BEGIN
		/**** User is not a first time user - Starts Here ********/
            INSERT  INTO @MRUList
                    (
                     MRUId
                    ,Counter
                    ,ChildId
                    ,MRUTypeId
                    ,UserId
                    ,CampusId
                    ,SortOrder
                    ,IsSticky
                    ,LeadId
                    ,FullName
                    ,CampusDescrip
                    ,ModDate
					)
                    SELECT DISTINCT TOP 20
                            M.MRUId
                           ,M.Counter
                           ,M.ChildId
                           ,M.MRUTypeId
                           ,M.UserId
                           ,M.CampusId
                           ,M.SortOrder
                           ,M.IsSticky
                           ,L.LeadId
                           ,CASE WHEN L.MiddleName IS NULL
                                      OR L.MiddleName = '' THEN L.FirstName + ' ' + L.LastName
                                 ELSE L.FirstName + ' ' + L.MiddleName + ' ' + L.LastName
                            END AS FullName
                           ,C.CampDescrip
                           ,M.ModDate
                    FROM    dbo.syMRUS AS M
                    INNER JOIN dbo.syMRUTypes AS T ON M.MRUTypeId = T.MRUTypeId
                    INNER  JOIN dbo.adLeads AS L ON M.ChildId = L.LeadId
                    INNER  JOIN dbo.syCampuses AS C ON M.CampusId = C.CampusId
                    WHERE   M.MRUTypeId = 4
                            AND UserId = @UserId
                            AND
		-- MRU List will show leads from the campuses user has access to
                            L.CampusId IN ( SELECT DISTINCT
                                                    C.CampusId
                                            FROM    syUsers U
                                            INNER JOIN syUsersRolesCampGrps URC ON U.UserId = URC.UserId
                                            INNER JOIN syCmpGrpCmps CGC ON URC.CampGrpId = CGC.CampGrpId
                                            INNER JOIN syCampuses C ON C.CampusId = CGC.CampusId
                                            WHERE   U.UserId = @UserId
                                                    AND C.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
                                            UNION
                                            SELECT DISTINCT
                                                    U.CampusId
                                            FROM    syUsers U
                                            WHERE   U.UserId = @UserId --Order by C.CampDescrip
							)
                    ORDER BY M.ModDate DESC; 
					
            DECLARE @MruListCount INT; 
            SET @MruListCount = 0;
            SET @MruListCount = (
                                  SELECT    COUNT(*) AS MruCountForFirstTimeUser
                                  FROM      @MRUList
                                );
			
            SET @DefaultCampusId = (
                                     SELECT DISTINCT
                                            U.CampusId
                                     FROM   syUsers U
                                     WHERE  U.UserId = @UserId
                                   );

            IF @DefaultCampusId IS NULL
                BEGIN
                    SET @DefaultCampusId = (
                                             SELECT DISTINCT TOP 1
                                                    SC.CampusId
                                             FROM   dbo.syCmpGrpCmps SC
                                                   ,dbo.syUsersRolesCampGrps URC
                                                   ,dbo.syCampuses S
                                             WHERE  URC.UserId = @UserId
                                                    AND SC.CampGrpId = URC.CampGrpId
                                                    AND S.CampusId = SC.CampusId
                                                    AND S.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
						--Order by S.CampDescrip
                                           );

                END;
			
			-- The MRU list is currently empty
			-- Get the most recently added 20 leads from default campus
            IF @MruListCount = 0
                BEGIN
                    INSERT  INTO @MRUList
                            (
                             MRUId
                            ,Counter
                            ,ChildId
                            ,MRUTypeId
                            ,UserId
                            ,CampusId
                            ,SortOrder
                            ,IsSticky
                            ,LeadId
                            ,FullName
                            ,CampusDescrip
                            ,ModDate
					        )
                            SELECT DISTINCT TOP 20
                                    NEWID() AS MRUId
                                   ,101 AS Counter
                                   ,  -- not being used so hardcoded a value
                                    L.LeadId AS ChildId
                                   ,4 AS MRUTypeId
                                   ,@UserId AS UserId
                                   ,C.CampusId
                                   ,0 AS SortOrder
                                   ,0 AS IsSticky
                                   ,L.LeadId AS LeadId
                                   ,CASE WHEN L.MiddleName IS NULL
                                              OR L.MiddleName = '' THEN L.FirstName + ' ' + L.LastName
                                         ELSE L.FirstName + ' ' + L.MiddleName + ' ' + L.LastName
                                    END AS FullName
                                   ,C.CampDescrip
                                   ,L.ModDate
                            FROM    dbo.adLeads AS L
                            INNER JOIN dbo.syCampuses AS C ON L.CampusId = C.CampusId
                            WHERE   -- MRU List will show leads from the campuses user has access to
                                    C.CampusId = @DefaultCampusId
                            ORDER BY L.ModDate DESC;
                END;
				
            SET @MruListCount = (
                                  SELECT    COUNT(*) AS MruCountForFirstTimeUser
                                  FROM      @MRUList
                                );
			
				-- The MRU list from default campus is currently empty
				-- Generate the list again from other campus
            IF @MruListCount = 0
                BEGIN
                    INSERT  INTO @MRUList
                            (
                             MRUId
                            ,Counter
                            ,ChildId
                            ,MRUTypeId
                            ,UserId
                            ,CampusId
                            ,SortOrder
                            ,IsSticky
                            ,LeadId
                            ,FullName
                            ,CampusDescrip
                            ,ModDate
							)
                            SELECT DISTINCT TOP 20
                                    NEWID() AS MRUId
                                   ,101 AS Counter
                                   ,  -- not being used so hardcoded a value
                                    L.LeadId AS ChildId
                                   ,4 AS MRUTypeId
                                   ,@UserId AS UserId
                                   ,C.CampusId
                                   ,0 AS SortOrder
                                   ,0 AS IsSticky
                                   ,L.LeadId AS LeadId
                                   ,CASE WHEN L.MiddleName IS NULL
                                              OR L.MiddleName = '' THEN L.FirstName + ' ' + L.LastName
                                         ELSE L.FirstName + ' ' + L.MiddleName + ' ' + L.LastName
                                    END AS FullName
                                   ,C.CampDescrip
                                   ,L.ModDate
                            FROM    dbo.adLeads AS L
                            INNER JOIN dbo.syCampuses AS C ON L.CampusId = C.CampusId
                            WHERE   -- MRU List will show leads from the campuses user has access to
                                    C.CampusId IN ( SELECT DISTINCT
                                                            C.CampusId
                                                    FROM    syUsers U -- Commented by Balaji on 5/4/2012
											-- Reason: For first time user we will pick
											-- leads only from default campusid
                                                    INNER JOIN syUsersRolesCampGrps URC ON U.UserId = URC.UserId
                                                    INNER JOIN syCmpGrpCmps CGC ON URC.CampGrpId = CGC.CampGrpId
                                                    INNER JOIN syCampuses C ON C.CampusId = CGC.CampusId
                                                    WHERE   U.UserId = @UserId
                                                            AND C.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' --Order by C.CampDescrip
									)
                            ORDER BY L.ModDate DESC;
                END;
					
					-- Always keep the count to 20.
					-- If existing users had more than 20 records delete it
            DELETE  FROM syMRUS
            WHERE   UserId = @UserId
                    AND MRUTypeId = 4
                    AND MRUId NOT IN ( SELECT DISTINCT
                                                MRUId
                                       FROM     @MRUList );
			/**** User is not a first time user - Ends Here ********/
        END;
    ELSE
        BEGIN
			
            SET @DefaultCampusId = (
                                     SELECT DISTINCT
                                            U.CampusId
                                     FROM   syUsers U
                                     WHERE  U.UserId = @UserId
                                   );

            IF @DefaultCampusId IS NULL
                BEGIN
                    SET @DefaultCampusId = (
                                             SELECT DISTINCT TOP 1
                                                    SC.CampusId
                                             FROM   dbo.syCmpGrpCmps SC
                                                   ,dbo.syUsersRolesCampGrps URC
                                                   ,dbo.syCampuses S
                                             WHERE  URC.UserId = @UserId
                                                    AND SC.CampGrpId = URC.CampGrpId
                                                    AND S.CampusId = SC.CampusId
                                                    AND S.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
						--Order by S.CampDescrip
                                           );

                END;
		
			/**** User is a first time user - Starts Here ********/ 
			 -- no rows so we must fill the MRUs with a default MRU list
			 --Print 'Else'
            INSERT  INTO @DefaultMRU
                    (
                     ChildId
                    ,MRUTypeId
                    ,UserId
                    ,CampusId
                    ,IsSticky
                    ,ModDate 
					)
                    SELECT DISTINCT TOP 20
                            L.LeadId AS ChildId
                           ,4 AS MRUTypeId
                           ,@UserId AS UserId
                           ,L.CampusId
                           ,0 AS IsSticky
                           ,L.ModDate
                    FROM    dbo.adLeads AS L
                    WHERE   -- MRU List will show leads from the campuses user has access to
                            L.CampusId = @DefaultCampusId
                    ORDER BY L.ModDate DESC;
					
			 --Select * from @DefaultMRU
					
            DECLARE @MruCountForFirstTimeUser INT; 
            SET @MruCountForFirstTimeUser = 0;
            SET @MruCountForFirstTimeUser = (
                                              SELECT    COUNT(*) AS MruCountForFirstTimeUser
                                              FROM      @DefaultMRU
                                            );
			
			-- There are no leads from default campus, so get leads from other campus
			-- user has access to
            IF @MruCountForFirstTimeUser = 0
                BEGIN
                    INSERT  INTO @DefaultMRU
                            (
                             ChildId
                            ,MRUTypeId
                            ,UserId
                            ,CampusId
                            ,IsSticky
                            ,ModDate 
						    )
                            SELECT DISTINCT TOP 20
                                    L.LeadId AS ChildId
                                   ,4 AS MRUTypeId
                                   ,@UserId AS UserId
                                   ,L.CampusId
                                   ,0 AS IsSticky
                                   ,L.ModDate
                            FROM    dbo.adLeads AS L
                            WHERE   -- MRU List will show leads from the campuses user has access to
                                    L.CampusId IN ( SELECT DISTINCT
                                                            C.CampusId
                                                    FROM    syUsers U
                                                    INNER JOIN syUsersRolesCampGrps URC ON U.UserId = URC.UserId
                                                    INNER JOIN syCmpGrpCmps CGC ON URC.CampGrpId = CGC.CampGrpId
                                                    INNER JOIN syCampuses C ON C.CampusId = CGC.CampusId
                                                    WHERE   U.UserId = @UserId
                                                            AND C.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' --Order by C.CampDescrip
								)
                            ORDER BY L.ModDate DESC;
                END; 
            PRINT @MruCountForFirstTimeUser;
			--    Select * from @DefaultMRU     
					



-- Fill the MRU results into the @MRUList table variable
            INSERT  INTO @MRUList
                    (
                     MRUId
                    ,Counter
                    ,ChildId
                    ,MRUTypeId
                    ,UserId
                    ,CampusId
                    ,SortOrder
                    ,IsSticky
                    ,LeadId
                    ,FullName
                    ,CampusDescrip
                    ,ModDate
					)
                    SELECT DISTINCT
                            NEWID() AS MRUId
                           ,101 AS Counter
                           ,ChildId
                           ,MRUTypeId
                           ,UserId
                           ,CampusId
                           ,SortOrder
                           ,IsSticky
                           ,ChildId AS LeadId
                           ,(
                              SELECT DISTINCT
                                        FirstName + ' ' + ISNULL(MiddleName,'') + ' ' + LastName
                              FROM      adLeads L
                              WHERE     LeadId = DM.ChildId
                            ) AS FullName
                           ,(
                              SELECT DISTINCT
                                        CampDescrip
                              FROM      syCampuses
                              WHERE     CampusId = DM.CampusId
                            ) AS CampusDescrip
                           ,ModDate
                    FROM    @DefaultMRU DM
                    ORDER BY ModDate DESC;
			   -- Select * from @MRUList
			   
			   -- insert records into the MRU table
            INSERT  INTO dbo.syMRUS
                    (
                     ChildId
                    ,MRUTypeId
                    ,UserId
                    ,CampusId
                    ,SortOrder
                    ,IsSticky
                    ,ModDate  
					)
                    SELECT  ChildId
                           ,MRUTypeId
                           ,UserId
                           ,CampusId
                           ,SortOrder
                           ,IsSticky
                           ,ModDate
                    FROM    @DefaultMRU;
		   /**** User is a first time user - Ends Here ********/ 
        END;


-- return the @MRUList table variable
    SELECT  MRUId
           ,Counter
           ,ChildId
           ,MRUTypeId
           ,UserId
           ,CampusId
           ,SortOrder
           ,IsSticky
           ,LeadId
           ,FullName
           ,CampusDescrip
           ,ModDate
           ,'Program: ' + (
                            SELECT TOP 1
                                    t1.PrgVerDescrip
                            FROM    arPrgVersions t1
                            INNER JOIN adLeads t2 ON t1.PrgVerId = t2.PrgVerId
                            WHERE   t2.LeadId = MRUList.LeadId
                                    AND t2.CampusId = MRUList.CampusId
                          ) AS ProgramVersionDesc
           ,(
              SELECT TOP 1
                        t1.StatusCodeDescrip
              FROM      syStatusCodes t1
              INNER JOIN adLeads t2 ON t1.StatusCodeId = t2.LeadStatus
              WHERE     t2.LeadId = MRUList.LeadId
                        AND t2.CampusId = MRUList.CampusId
            ) AS StatusCodeDescrip
    FROM    @MRUList MRUList
    ORDER BY ModDate DESC;
GO
