SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[USP_AR_DoesStudentHaveOverride]
    (
     @StuEnrollId UNIQUEIDENTIFIER
    ,@preReqID UNIQUEIDENTIFIER
    )
AS /*----------------------------------------------------------------------------------------------------
	Author          :	Saraswathi Lakshmanan
    
    Create date		:	02/03/2010
    
	Procedure Name	:	USP_AR_DoesStudentHaveOverride

	Objective		:	find if the Student has OverRidden the Prereqs
	
	Parameters		:	Name			Type	Data Type	Required? 	
						=====			====	=========	=========	
						@StuEnrollId	In		Varchar		Required	
						@preReqID		In		varChar		Required
	
	Output			:	Returns the rowcount					
						
*/-----------------------------------------------------------------------------------------------------

    BEGIN
	
        SELECT  COUNT(*) AS Count
        FROM    arOverridenPreReqs a
        INNER JOIN arClassSections C ON a.ReqId = C.ReqId
        WHERE   a.StuEnrollId = @StuEnrollId
                AND ClsSectionId = @preReqID;
    END;



GO
