SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[Usp_TermswithCoursesmappedtoDictationTestComponent_GetList]
AS
    DECLARE @DictationTestComponent TABLE
        (
         GrdComponentTypeId UNIQUEIDENTIFIER
        );
    INSERT  INTO @DictationTestComponent
            SELECT  GrdComponentTypeId
            FROM    arGrdComponentTypes
            WHERE   SysComponentTypeId = 612;

    SELECT 
            DISTINCT
            t4.TermId
           ,t4.TermDescrip
           ,t4.StartDate
           ,t4.EndDate
    FROM    @DictationTestComponent t1
    INNER JOIN arBridge_GradeComponentTypes_Courses t2 ON t1.GrdComponentTypeId = t2.GrdComponentTypeId
    INNER JOIN arClassSections t3 ON t2.ReqId = t3.ReqId
    INNER JOIN arTerm t4 ON t3.TermId = t4.TermId
    ORDER BY t4.StartDate
           ,t4.EndDate
           ,t4.TermDescrip;



GO
