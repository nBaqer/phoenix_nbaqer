SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

 
CREATE PROCEDURE [dbo].[USP_GetRequirementsByReqGroupAndStudent]
    (
     @ReqGrpId UNIQUEIDENTIFIER
    ,@StudentID UNIQUEIDENTIFIER
    ,@CampusID UNIQUEIDENTIFIER 
    )
AS
    BEGIN

        DECLARE @tab1 TABLE
            (
             adReqId UNIQUEIDENTIFIER
            ,Descrip VARCHAR(100)
            ,adReqTypeId UNIQUEIDENTIFIER
            ,ReqGrpId UNIQUEIDENTIFIER
            );
        INSERT  INTO @tab1
                EXEC USP_GetRequirementsByReqGroupAndStudent_Enroll @ReqGrpId,@StudentID,@CampusID;
        DECLARE @tab2 TABLE
            (
             adReqId UNIQUEIDENTIFIER
            ,Descrip VARCHAR(100)
            ,adReqTypeId UNIQUEIDENTIFIER
            ,ReqGrpId UNIQUEIDENTIFIER
            );
        INSERT  INTO @tab2
                EXEC USP_GetRequirementsByReqGroupAndStudent_Grad @ReqGrpId,@StudentID,@CampusID;
        DECLARE @tab3 TABLE
            (
             adReqId UNIQUEIDENTIFIER
            ,Descrip VARCHAR(100)
            ,adReqTypeId UNIQUEIDENTIFIER
            ,ReqGrpId UNIQUEIDENTIFIER
            );
        INSERT  INTO @tab3
                EXEC USP_GetRequirementsByReqGroupAndStudent_FinAid @ReqGrpId,@StudentID,@CampusID;

        SELECT  *
        FROM    @tab1
        UNION
        SELECT  *
        FROM    @tab2
        UNION
        SELECT  *
        FROM    @tab3;


    END;








GO
