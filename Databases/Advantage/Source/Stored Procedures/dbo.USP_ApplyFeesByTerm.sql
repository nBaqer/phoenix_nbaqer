SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

----For testing purposes only
--declare @TermId uniqueidentifier	
--declare @User varchar(1000)
--declare @CampusID uniqueidentifier
--declare @PostFeesDate datetime

--set @TermId='92180db0-4057-4dfd-9f67-8fca584b210e' 
--set @CampusID='3f5e839a-589a-4b2a-b258-35a1a8b3b819'


CREATE PROCEDURE [dbo].[USP_ApplyFeesByTerm]
    (
     @TermId UNIQUEIDENTIFIER
    ,@User VARCHAR(100)
    ,@CampusID UNIQUEIDENTIFIER
    ,@PostFeesDate DATETIME
    ,@Result INTEGER OUTPUT
    )
AS
    SET NOCOUNT ON;
    BEGIN 
    
        DECLARE @StuEnrollID UNIQUEIDENTIFIER;
        DECLARE @PeriodicfeeID AS UNIQUEIDENTIFIER;
        DECLARE @TransCodeID AS UNIQUEIDENTIFIER;
        DECLARE @TransCodeDescrip AS VARCHAR(50);
        DECLARE @StudentName AS VARCHAR(50);
        DECLARE @TermDescrip AS VARCHAR(50);
        DECLARE @TransDescrip AS VARCHAR(50);
        DECLARE @TransAmount DECIMAL(18,2);
        SET @TransAmount = 0;
          
        BEGIN TRANSACTION;
       
        DECLARE c1 CURSOR READ_ONLY
        FOR
            (
              SELECT    SE.StuEnrollId
                       ,PF.PeriodicFeeId
                       ,PF.TransCodeId
                       ,( (
                            SELECT  TransCodeDescrip
                            FROM    saTransCodes
                            WHERE   TransCodeId = PF.TransCodeId
                          ) + CASE PF.ApplyTo
                                WHEN 0 THEN '- All Programs'
                                WHEN 1 THEN '-' + PT.Description
                                WHEN 2 THEN '-' + PV.PrgVerDescrip
                                ELSE ''
                              END ) TransCodeDescrip
                       ,(
                          SELECT    LastName + ' ' + FirstName
                          FROM      arStudent
                          WHERE     StudentId = (
                                                  SELECT    StudentId
                                                  FROM      arStuEnrollments
                                                  WHERE     StuEnrollId = SE.StuEnrollId
                                                )
                        ) StudentName
                       ,(
                          SELECT    TermDescrip
                          FROM      arTerm
                          WHERE     TermId = PF.TermId
                        ) TransDescrip
                       ,T.TermDescrip
                       ,( CASE LEN(PF.RateScheduleId) 
			--This is to handle a term that has a rate schedule assigned to it in the saPeriodicFees table.
                            WHEN 36
                            THEN (
                                   SELECT   CASE RSD.FlatAmount
                                              WHEN 0.00 --The rate schedule has 0 for the flat amount so it is using some kind of unit such as credit hour
                                                   THEN CASE RSD.UnitId
                                                          WHEN 0 THEN Rate * (
                                                                               SELECT   COALESCE(SUM(RQ.Credits),0)
                                                                               FROM     arResults R
                                                                                       ,arClassSections CS
                                                                                       ,arClassSectionTerms CST
                                                                                       ,arReqs RQ
                                                                               WHERE    R.TestId = CS.ClsSectionId
                                                                                        AND CS.ReqId = RQ.ReqId
                                                                                        AND R.StuEnrollId = SE.StuEnrollId
                                                                                        AND CST.ClsSectionId = CS.ClsSectionId
                                                                                        AND CST.TermId = @TermId
                                                                             )
                                                          WHEN 1 THEN Rate * (
                                                                               SELECT   COALESCE(SUM(RQ.Hours),0)
                                                                               FROM     arResults R
                                                                                       ,arClassSections CS
                                                                                       ,arClassSectionTerms CST
                                                                                       ,arReqs RQ
                                                                               WHERE    R.TestId = CS.ClsSectionId
                                                                                        AND CS.ReqId = RQ.ReqId
                                                                                        AND R.StuEnrollId = SE.StuEnrollId
                                                                                        AND CST.ClsSectionId = CS.ClsSectionId
                                                                                        AND CST.TermId = @TermId
                                                                             )
                                                        END
                                              ELSE --The rate schedule is using a flat amount
                                                   FlatAmount
                                            END
                                   FROM     saRateSchedules RS
                                           ,saRateScheduleDetails RSD
                                   WHERE    RS.RateScheduleId = RSD.RateScheduleId
                                            AND RS.RateScheduleId = PF.RateScheduleId
                                            AND (
                                                  RSD.TuitionCategoryId = SE.TuitionCategoryId
                                                  OR (
                                                       RSD.TuitionCategoryId IS NULL
                                                       AND SE.TuitionCategoryId IS NULL
                                                     )
                                                )
                                            AND (
                                                  (
                                                    RSD.FlatAmount = 0.00
                                                    AND RSD.UnitId = 0
						 --AND RSD.MaxUnits=(SELECT min(MaxUnits)
							--				FROM saRateScheduleDetails
							--				WHERE MaxUnits>=( SELECT COALESCE(SUM(RQ.Credits),0)
							--								  FROM arResults R, arClassSections CS, arClassSectionTerms CST, arReqs RQ 
							--								  WHERE R.TestId = CS.ClsSectionId
							--								  AND CS.ReqId=RQ.ReqId
							--								  AND R.StuEnrollId=SE.StuEnrollId
							--								  AND CST.ClsSectionId=CS.ClsSectionId
							--								  AND CST.TermId= @TermId
							--								 )
							--				AND RateScheduleId=RSD.RateScheduleId
							--				AND (	RSD.TuitionCategoryId=SE.TuitionCategoryId
							--										OR 
							--						(RSD.TuitionCategoryId IS NULL AND SE.TuitionCategoryId IS NULL)
							--					)
							--				)
                                                    AND RSD.MinUnits <= (
                                                                          SELECT    COALESCE(SUM(RQ.Credits),0)
                                                                          FROM      arResults R
                                                                                   ,arClassSections CS
                                                                                   ,arClassSectionTerms CST
                                                                                   ,arReqs RQ
                                                                          WHERE     R.TestId = CS.ClsSectionId
                                                                                    AND CS.ReqId = RQ.ReqId
                                                                                    AND R.StuEnrollId = SE.StuEnrollId
                                                                                    AND CST.ClsSectionId = CS.ClsSectionId
                                                                                    AND CST.TermId = @TermId
                                                                        )
                                                    AND RSD.MaxUnits >= (
                                                                          SELECT    COALESCE(SUM(RQ.Credits),0)
                                                                          FROM      arResults R
                                                                                   ,arClassSections CS
                                                                                   ,arClassSectionTerms CST
                                                                                   ,arReqs RQ
                                                                          WHERE     R.TestId = CS.ClsSectionId
                                                                                    AND CS.ReqId = RQ.ReqId
                                                                                    AND R.StuEnrollId = SE.StuEnrollId
                                                                                    AND CST.ClsSectionId = CS.ClsSectionId
                                                                                    AND CST.TermId = @TermId
                                                                        )
                                                  )
                                                  OR (
                                                       RSD.FlatAmount = 0.00
                                                       AND RSD.UnitId = 1
						 --AND RSD.MaxUnits=(SELECT min(MaxUnits)
							--				FROM saRateScheduleDetails 
							--				WHERE MaxUnits>=(SELECT COALESCE(SUM(RQ.Hours),0)
							--								 FROM arResults R, arClassSections CS, arClassSectionTerms CST, arReqs RQ
							--								 WHERE R.TestId = CS.ClsSectionId
							--								 AND CS.ReqId=RQ.ReqId
							--								 AND R.StuEnrollId=SE.StuEnrollId
							--								 AND CST.ClsSectionId=CS.ClsSectionId
							--								 AND CST.TermId= @TermID
							--								) 
							--				AND RateScheduleId=RSD.RateScheduleId
							--				AND (	RSD.TuitionCategoryId=SE.TuitionCategoryId
							--									 OR
							--					   (RSD.TuitionCategoryId IS NULL AND SE.TuitionCategoryId IS NULL)
							--					)
							--			 )
                                                       AND RSD.MinUnits <= (
                                                                             SELECT COALESCE(SUM(RQ.Hours),0)
                                                                             FROM   arResults R
                                                                                   ,arClassSections CS
                                                                                   ,arClassSectionTerms CST
                                                                                   ,arReqs RQ
                                                                             WHERE  R.TestId = CS.ClsSectionId
                                                                                    AND CS.ReqId = RQ.ReqId
                                                                                    AND R.StuEnrollId = SE.StuEnrollId
                                                                                    AND CST.ClsSectionId = CS.ClsSectionId
                                                                                    AND CST.TermId = @TermId
                                                                           )
                                                       AND RSD.MaxUnits >= (
                                                                             SELECT COALESCE(SUM(RQ.Hours),0)
                                                                             FROM   arResults R
                                                                                   ,arClassSections CS
                                                                                   ,arClassSectionTerms CST
                                                                                   ,arReqs RQ
                                                                             WHERE  R.TestId = CS.ClsSectionId
                                                                                    AND CS.ReqId = RQ.ReqId
                                                                                    AND R.StuEnrollId = SE.StuEnrollId
                                                                                    AND CST.ClsSectionId = CS.ClsSectionId
                                                                                    AND CST.TermId = @TermId
                                                                           )
                                                     )
                                                  OR (
                                                       RSD.FlatAmount > 0.00
						 --AND RSD.MaxUnits=(SELECT min(MaxUnits)
							--				FROM saRateScheduleDetails
							--				WHERE MaxUnits>=(SELECT COALESCE(SUM(RQ.Credits),0)
							--								 FROM arResults R, arClassSections CS, arClassSectionTerms CST, arReqs RQ
							--								 WHERE R.TestId = CS.ClsSectionId
							--								 AND CS.ReqId=RQ.ReqId
							--								 AND R.StuEnrollId=SE.StuEnrollId
							--								 AND CST.ClsSectionId=CS.ClsSectionId
							--								 AND CST.TermId= @TermId 
							--								 ) 
							--				AND RateScheduleId=RSD.RateScheduleId
							--				AND (	RSD.TuitionCategoryId=SE.TuitionCategoryId
							--										 OR 
							--						(RSD.TuitionCategoryId IS NULL AND SE.TuitionCategoryId IS NULL)
							--					)
							--				)
                                                       AND RSD.MinUnits <= (
                                                                             SELECT COALESCE(SUM(RQ.Credits),0)
                                                                             FROM   arResults R
                                                                                   ,arClassSections CS
                                                                                   ,arClassSectionTerms CST
                                                                                   ,arReqs RQ
                                                                             WHERE  R.TestId = CS.ClsSectionId
                                                                                    AND CS.ReqId = RQ.ReqId
                                                                                    AND R.StuEnrollId = SE.StuEnrollId
                                                                                    AND CST.ClsSectionId = CS.ClsSectionId
                                                                                    AND CST.TermId = @TermId
                                                                           )
                                                       AND RSD.MaxUnits >= (
                                                                             SELECT COALESCE(SUM(RQ.Credits),0)
                                                                             FROM   arResults R
                                                                                   ,arClassSections CS
                                                                                   ,arClassSectionTerms CST
                                                                                   ,arReqs RQ
                                                                             WHERE  R.TestId = CS.ClsSectionId
                                                                                    AND CS.ReqId = RQ.ReqId
                                                                                    AND R.StuEnrollId = SE.StuEnrollId
                                                                                    AND CST.ClsSectionId = CS.ClsSectionId
                                                                                    AND CST.TermId = @TermId
                                                                           )
                                                     )
                                                )
                                 ) --End handling of scenario where the term is assigned a rate schedule in the saPeriodicFees table
                            ELSE ( CASE PF.UnitId	--This is to handle scenario where the term is not using a rate schedule
                                     WHEN 0 THEN PF.Amount * (
                                                               SELECT   SUM(RQ.Credits)
                                                               FROM     arResults R
                                                                       ,arClassSections CS
                                                                       ,arClassSectionTerms CST
                                                                       ,arReqs RQ
                                                               WHERE    R.TestId = CS.ClsSectionId
                                                                        AND CS.ReqId = RQ.ReqId
                                                                        AND R.StuEnrollId = SE.StuEnrollId
                                                                        AND CST.ClsSectionId = CS.ClsSectionId
                                                                        AND CST.TermId = @TermId
                                                                        AND (
                                                                              (
                                                                                SE.TuitionCategoryId IS NULL
                                                                                AND PF.TuitionCategoryId IS NULL
                                                                              )
                                                                              OR ( SE.TuitionCategoryId = PF.TuitionCategoryId )
                                                                            )
                                                             )
                                     WHEN 1 THEN PF.Amount * (
                                                               SELECT   SUM(RQ.Hours)
                                                               FROM     arResults R
                                                                       ,arClassSections CS
                                                                       ,arClassSectionTerms CST
                                                                       ,arReqs RQ
                                                               WHERE    R.TestId = CS.ClsSectionId
                                                                        AND CS.ReqId = RQ.ReqId
                                                                        AND R.StuEnrollId = SE.StuEnrollId
                                                                        AND CST.ClsSectionId = CS.ClsSectionId
                                                                        AND CST.TermId = @TermId
                                                                        AND (
                                                                              (
                                                                                SE.TuitionCategoryId IS NULL
                                                                                AND PF.TuitionCategoryId IS NULL
                                                                              )
                                                                              OR ( SE.TuitionCategoryId = PF.TuitionCategoryId )
                                                                            )
                                                             )
                                     WHEN 2
                                     THEN PF.Amount
                                          * (
                                              SELECT    ( CASE WHEN EXISTS ( SELECT *
                                                                             FROM   arResults
                                                                             WHERE  StuEnrollId = SE.StuEnrollId
                                                                                    AND TestId IN (
                                                                                    SELECT  ClsSectionId
                                                                                    FROM    arClassSections
                                                                                    WHERE   TermId = @TermId
                                                                                            AND (
                                                                                                  (
                                                                                                    SE.TuitionCategoryId IS NULL
                                                                                                    AND PF.TuitionCategoryId IS NULL
                                                                                                  )
                                                                                                  OR ( SE.TuitionCategoryId = PF.TuitionCategoryId )
                                                                                                ) ) ) THEN 1
                                                               ELSE 0
                                                          END )
                                            )
                                   END )
                          END ) TransAmount
              FROM      saPeriodicFees PF
                       ,arTerm T
                       ,arStuEnrollments SE
                       ,syStatusCodes SC
                       ,arPrgVersions PV
                       ,arProgTypes PT
              WHERE     PF.TermId = T.TermId
                        AND PF.TermId = @TermId
                        AND SE.CampusId = @CampusID
                        AND SE.StatusCodeId = SC.StatusCodeId
                        AND SC.SysStatusId IN ( 7,9,13,20 )
                        AND SE.PrgVerId = PV.PrgVerId
                        AND PV.ProgTypId = PT.ProgTypId
                        AND (
                              ( PF.ApplyTo = 0 )
                              OR (
                                   PF.ApplyTo = 1
                                   AND PV.ProgTypId = PF.ProgTypId
                                 )
                              OR (
                                   PF.ApplyTo = 2
                                   AND PV.PrgVerId = PF.PrgVerId
                                 )
                            )
                        AND (
                              ( PF.TermStartDate IS NULL )
                              OR ( PF.TermStartDate = SE.ExpStartDate )
                            )  
				
		--We only want to get students registered for classes in the term that is passed in.
		--Otherwise it is going to end up posting zeroes for all the other students
                        AND EXISTS ( SELECT rs.StuEnrollId
                                     FROM   arResults rs
                                           ,arClassSections cs
                                     WHERE  rs.TestId = cs.ClsSectionId
                                            AND cs.TermId = @TermId
                                            AND rs.StuEnrollId = SE.StuEnrollId )  
       
        --AND SE.StuEnrollId='AB2AE9C4-9C04-4153-AF3E-F3E179E6B916'
        --ORDER BY StudentName
            ); 
  
        OPEN c1; 
  
        FETCH NEXT FROM c1 
        INTO @StuEnrollID,@PeriodicfeeID,@TransCodeID,@TransCodeDescrip,@StudentName,@TransDescrip,@TermDescrip,@TransAmount;
  
        WHILE @@FETCH_STATUS = 0
            BEGIN 
                IF ISNULL(@TransAmount,0) <> 0
                    BEGIN
                        INSERT  INTO dbo.saTransactions
                                (
                                 TransactionId
                                ,StuEnrollId
                                ,TermId
                                ,CampusId
                                ,TransDate
                                ,TransCodeId
                                ,TransReference
                                ,AcademicYearId
                                ,TransDescrip
                                ,TransAmount
                                ,TransTypeId
                                ,IsPosted
                                ,CreateDate
                                ,BatchPaymentId
                                ,ViewOrder
                                ,IsAutomatic
                                ,ModUser
                                ,ModDate
                                ,Voided
                                ,FeeLevelId
                                ,FeeId
                                ,PaymentCodeId
                                ,FundSourceId 
                                )
                        VALUES  (
                                 NEWID()
                                , -- TransactionId - uniqueidentifier
                                 @StuEnrollID
                                , -- StuEnrollId - uniqueidentifier
                                 @TermId
                                , -- TermId - uniqueidentifier
                                 @CampusID
                                , -- CampusId - uniqueidentifier
                                 @PostFeesDate
                                , -- TransDate - datetime
                                 @TransCodeID
                                , -- TransCodeId - uniqueidentifier
                                 @TransDescrip
                                , -- TransReference - varchar(50)
                                 NULL
                                , -- AcademicYearId - uniqueidentifier
                                 @TransCodeDescrip
                                , -- TransDescrip - varchar(50)
                                 ISNULL(@TransAmount,0)
                                , -- TransAmount - decimal
                                 0
                                , -- TransTypeId - int
                                 1
                                , -- IsPosted - bit
                                 GETDATE()
                                , -- CreateDate - datetime
                                 NULL
                                , -- BatchPaymentId - uniqueidentifier
                                 0
                                , -- ViewOrder - int
                                 1
                                , -- IsAutomatic - bit
                                 @User
                                , -- ModUser - varchar(50)
                                 GETDATE()
                                ,-- ModDate - datetime
                                 0
                                , -- Voided - bit
                                 1
                                , -- FeeLevelId - tinyint
                                 @PeriodicfeeID
                                , -- FeeId - uniqueidentifier
                                 NULL
                                , -- PaymentCodeId - uniqueidentifier
                                 NULL  -- FundSourceId - uniqueidentifier
                         
                                );
  
                        IF @@ERROR <> 0
                            BEGIN
						-- Rollback the transaction
                                ROLLBACK; 
                                SET @Result = 0;
                                RETURN 0;--"Error while inserting"
                            END;
					
                    END;
			
                

                FETCH NEXT FROM c1 
            INTO @StuEnrollID,@PeriodicfeeID,@TransCodeID,@TransCodeDescrip,@StudentName,@TransDescrip,@TermDescrip,@TransAmount;
  
            END; 
        CLOSE c1; 
        DEALLOCATE c1; 
 
        IF @@ERROR = 0
            BEGIN
                COMMIT TRANSACTION;
                SET @Result = 1;
                RETURN 1;
            END;
        ELSE
            BEGIN
					-- Rollback the transaction
                ROLLBACK TRANSACTION;	
                SET @Result = 0;
                RETURN 0;--"Error while inserting"	
            END;
	  
    END;






GO
