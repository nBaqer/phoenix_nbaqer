SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_GetAttempted]
    (
     @stuEnrollId UNIQUEIDENTIFIER
    ,@instrGrdBkWgtDetailId UNIQUEIDENTIFIER
    )
AS
    SET NOCOUNT ON;

    SELECT  CAST(ISNULL(number,0) AS INT) AS num
           ,ISNULL((
                     SELECT COUNT(InstrGrdBkWgtDetailId)
                     FROM   arGrdBkResults
                     WHERE  StuEnrollId = @stuEnrollId
                            AND InstrGrdBkWgtDetailId = @instrGrdBkWgtDetailId
                     GROUP BY InstrGrdBkWgtDetailId
                   ),0) AS attempted
    FROM    arGrdBkWgtDetails
    WHERE   InstrGrdBkWgtDetailId = @instrGrdBkWgtDetailId;



GO
