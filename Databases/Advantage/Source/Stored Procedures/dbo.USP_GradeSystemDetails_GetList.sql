SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_GradeSystemDetails_GetList]
    @StuEnrollId UNIQUEIDENTIFIER
AS
    SELECT DISTINCT
            t5.GrdSysDetailId
           ,t5.Grade
           ,t5.IsPass
    FROM    arStuEnrollments t2
    INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
    INNER JOIN arGradeSystems t4 ON t3.GrdSystemId = t4.GrdSystemId
    INNER JOIN arGradeSystemDetails t5 ON t4.GrdSystemId = t5.GrdSystemId
    WHERE   t2.StuEnrollId = @StuEnrollId
            AND t5.IsPass = 1
    ORDER BY t5.Grade; 



GO
