SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

 
 
CREATE PROCEDURE [dbo].[usp_GetModulesCoursesResultsForMultipleStudents_GroupByClass]
    (
     @campGrpId VARCHAR(8000)
    ,@prgVerId VARCHAR(8000)
    ,@statusCodeId VARCHAR(8000) = NULL
    ,@termId UNIQUEIDENTIFIER = NULL

    )
AS
    SET NOCOUNT ON;
    SELECT  P.StuEnrollId
           ,P.TermId
           ,P.TermDescrip
           ,P.Score AS score
           ,P.StartDate
           ,P.EndDate
           ,P.Code
           ,P.Descrip
           ,P.ReqId
           ,P.Credits
           ,P.FinAidCredits
           ,P.ClsSectionid
           ,P.Grade
           ,P.IsPass
           ,P.IsCreditsAttempted
           ,P.IsCreditsEarned
           ,P.ExpGradDate
           ,P.labCount
    FROM    (
              SELECT    ar.StuEnrollId
                       ,cs.TermId
                       ,tm2.TermDescrip
                       ,ar.Score AS score
                       ,tm2.StartDate
                       ,tm2.EndDate
                       ,rq2.Code
                       ,rq2.Descrip
                       ,cs.ReqId
                       ,rq2.Credits
                       ,rq2.FinAidCredits
                       ,cs.ClsSectionid
                       ,(
                          SELECT    Grade
                          FROM      arGradesystemdetails
                          WHERE     GrdSysdetailid = ar.GrdSysdetailid
                        ) AS Grade
                       ,(
                          SELECT    IsPass
                          FROM      arGradesystemdetails
                          WHERE     GrdSysdetailid = ar.GrdSysdetailid
                        ) AS IsPass
                       ,(
                          SELECT    IsCreditsAttempted
                          FROM      arGradesystemdetails
                          WHERE     GrdSysdetailid = ar.GrdSysdetailid
                        ) AS IsCreditsAttempted
                       ,(
                          SELECT    IsCreditsEarned
                          FROM      arGradesystemdetails
                          WHERE     GrdSysdetailid = ar.GrdSysdetailid
                        ) AS IsCreditsEarned
                       ,(
                          SELECT    ExpGradDate
                          FROM      arStuEnrollments
                          WHERE     stuEnrollid = ar.StuEnrollId
                        ) AS ExpGradDate
                       ,(
                          SELECT DISTINCT
                                    COUNT(GC.Descrip)
                          FROM      arGrdBkWeights GBW
                                   ,arGrdComponentTypes GC
                                   ,arGrdBkWgtDetails GD
                          WHERE     GBW.InstrGrdBkWgtId = GD.InstrGrdBkWgtId
                                    AND GC.GrdComponentTypeId = GD.GrdComponentTypeId
                                    AND GBW.ReqId = cs.ReqId
                                    AND GC.SysComponentTypeID IS NOT NULL
                                    AND GC.SysComponentTypeID IN ( SELECT DISTINCT
                                                                            ResourceId
                                                                   FROM     syResources
                                                                   WHERE    Resource IN ( 'Lab Work','Lab Hours' ) )
                        ) AS labCount
                       ,cs.StartDate AS clsStartDate
                       ,ROW_NUMBER() OVER ( PARTITION BY ar.StuEnrollId ORDER BY cs.StartDate DESC ) AS 'RowNumber'
              FROM      arResults ar
                       ,arClassSections cs
                       ,arTerm tm2
                       ,arReqs rq2
              WHERE     ar.TestId = cs.ClsSectionId
                        AND cs.TermId = tm2.TermId
                        AND cs.ReqId = rq2.ReqId
                        AND tm2.Termid = @termId
            ) P
    JOIN    arStuEnrollments ON P.StuEnrollId = arStuEnrollments.StuEnrollId
    JOIN    arStudent A ON A.StudentId = arStuEnrollments.StudentId
    JOIN    syCampuses C ON arStuEnrollments.CampusId = C.CampusId
    JOIN    syCmpGrpCmps ON syCmpGrpCmps.CampusId = arStuEnrollments.CampusId
    JOIN    syCampGrps ON syCampGrps.CampGrpId = syCmpGrpCmps.CampGrpId
                          AND syCmpGrpCmps.campgrpid IN ( SELECT    strval
                                                          FROM      dbo.SPLIT(@campGrpId) )
                          AND arStuEnrollments.prgverid IN ( SELECT strval
                                                             FROM   dbo.SPLIT(@prgVerId) )
                          AND (
                                @statusCodeId IS NULL
                                OR arStuEnrollments.statuscodeid IN ( SELECT    strval
                                                                      FROM      dbo.SPLIT(@statusCodeId) )
                              )
        --and P.StuEnrollid in ('AC2041F0-54BF-42EA-8CB2-00B03600C2AD','D1C95C6C-A725-4E34-A614-01FF763D2C10')
                          AND P.RowNumber = 1
                          AND P.StuEnrollId NOT IN ( SELECT SGS.StuEnrollId
                                                     FROM   adStuGrpStudents SGS
                                                           ,adStudentGroups SG
                                                     WHERE  SGS.StuGrpId = SG.StuGrpId
                                                            AND SG.IsTransHold = 1
                                                            AND SGS.IsDeleted = 0
                                                            AND SGS.StuEnrollId IS NOT NULL )
    ORDER BY P.stuEnrollId
           ,P.StartDate
           ,Grade DESC
           ,Score DESC; 





GO
