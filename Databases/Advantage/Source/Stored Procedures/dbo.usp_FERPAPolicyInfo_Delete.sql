SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_FERPAPolicyInfo_Delete]
    (
     @FERPAEntityID UNIQUEIDENTIFIER
    ,@studentID UNIQUEIDENTIFIER
    )
AS
    SET NOCOUNT ON;
    DELETE  FROM arFERPAPolicy
    WHERE   FERPAEntityID = @FERPAEntityID
            AND studentId = @studentID;



GO
