SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--=================================================================================================
-- USP_AT_Step03_InsertAttendance_Class_PresentAbsent
--=================================================================================================
CREATE PROCEDURE [dbo].[USP_AT_Step03_InsertAttendance_Class_PresentAbsent]
    (
        @StuEnrollIdList AS VARCHAR(MAX) = NULL
    )
AS --
    -- Step 3  --  InsertAttendance_Class_PresentAbsent   -- UnitTypeDescrip = ('None', 'Present Absent') 
    --         --  TrackSapAttendance = 'byclass'
    BEGIN
        DECLARE @StuEnrollId UNIQUEIDENTIFIER
               ,@MeetDate DATETIME
               ,@WeekDay VARCHAR(15)
               ,@StartDate DATETIME
               ,@EndDate DATETIME
               ,@PeriodDescrip VARCHAR(50)
               ,@Actual DECIMAL(18, 2)
               ,@Excused DECIMAL(18, 2)
               ,@ClsSectionId UNIQUEIDENTIFIER
               ,@Absent DECIMAL(18, 2)
               ,@ScheduledMinutes DECIMAL(18, 2)
               ,@TardyMinutes DECIMAL(18, 2)
               ,@tardy DECIMAL(18, 2)
               ,@tracktardies INT
               ,@tardiesMakingAbsence INT
               ,@PrgVerId UNIQUEIDENTIFIER
               ,@rownumber INT
               ,@ActualRunningPresentHours DECIMAL(18, 2)
               ,@ActualRunningAbsentHours DECIMAL(18, 2)
               ,@ActualRunningTardyHours DECIMAL(18, 2)
               ,@ActualRunningMakeupHours DECIMAL(18, 2)
               ,@PrevStuEnrollId UNIQUEIDENTIFIER
               ,@intTardyBreakPoint INT
               ,@AdjustedRunningPresentHours DECIMAL(18, 2)
               ,@AdjustedRunningAbsentHours DECIMAL(18, 2)
               ,@ActualRunningScheduledDays DECIMAL(18, 2)
               ,@AdjustedPresentDaysComputed DECIMAL(18, 2)
               ,@MakeupHours DECIMAL(18, 2)
               ,@boolReset BIT;



        DECLARE @attendanceSummary TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,ClsSectionId UNIQUEIDENTIFIER
               ,MeetDate DATETIME
               ,WeekDay NVARCHAR(25)
               ,StartDate DATETIME
               ,EndDate DATETIME
               ,PeriodDescrip VARCHAR(50)
               ,Actual DECIMAL(18, 2)
               ,Excused BIT
               ,Absent DECIMAL(18, 2)
               ,ScheduledMinutes DECIMAL(18, 2)
               ,TardyMinutes DECIMAL(18, 2)
               ,Tardy BIT
               ,TrackTardies BIT
               ,TardiesMakingAbsence INT
               ,PrgVerId UNIQUEIDENTIFIER
               ,RowNumber INT
            );

        DECLARE @MyEnrollments TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
            );
        INSERT INTO @MyEnrollments
                    SELECT enrollments.StuEnrollId
                    FROM   dbo.arStuEnrollments enrollments
                    WHERE  (
                           @StuEnrollIdList IS NULL
                           OR ( enrollments.StuEnrollId IN (
                                                           SELECT zz.Val
                                                           FROM   MultipleValuesForReportParameters(@StuEnrollIdList, ',', 1) zz
                                                           )
                              )
                           );
        INSERT INTO @attendanceSummary (
                                       StuEnrollId
                                      ,ClsSectionId
                                      ,MeetDate
                                      ,WeekDay
                                      ,StartDate
                                      ,EndDate
                                      ,PeriodDescrip
                                      ,Actual
                                      ,Excused
                                      ,Absent
                                      ,ScheduledMinutes
                                      ,TardyMinutes
                                      ,Tardy
                                      ,TrackTardies
                                      ,TardiesMakingAbsence
                                      ,PrgVerId
                                      ,RowNumber
                                       )
                    SELECT *
                    FROM   (
                           SELECT *
                                 ,ROW_NUMBER() OVER ( ORDER BY MeetDate ) AS RowNumber
                           FROM   (
                                  SELECT     DISTINCT t1.StuEnrollId
                                                     ,t1.ClsSectionId
                                                     ,t1.MeetDate
                                                     ,DATENAME(dw, t1.MeetDate) AS WeekDay
                                                     ,t4.StartDate
                                                     ,t4.EndDate
                                                     ,t5.PeriodDescrip
                                                     ,t1.Actual
                                                     ,t1.Excused
                                                     ,CASE WHEN (
                                                                t1.Actual = 0
                                                                AND t1.Excused = 0
                                                                ) THEN t1.Scheduled
                                                           ELSE
                                                               CASE WHEN (
                                                                         t1.Actual <> 9999.00
                                                                         AND t1.Actual < t1.Scheduled
                                                                         ) --Commented by Balaji on 2.6.2015 as Excused absence is still considered Absent, but considered present only while calculating payment period
                                                     --Case when (t1.Actual <> 9999.00 and t1.Actual < t1.Scheduled and t1.Excused <> 1) 
                                                     THEN ( t1.Scheduled - t1.Actual )
                                                                    ELSE 0
                                                               END
                                                      END AS Absent
                                                     ,t1.Scheduled AS ScheduledMinutes
                                                     ,CASE WHEN (
                                                                t1.Actual > 0
                                                                AND t1.Actual < t1.Scheduled
                                                                ) THEN ( t1.Scheduled - t1.Actual )
                                                           ELSE 0
                                                      END AS TardyMinutes
                                                     ,t1.Tardy AS Tardy
                                                     ,t3.TrackTardies
                                                     ,t3.TardiesMakingAbsence
                                                     ,t3.PrgVerId
                                  FROM       atClsSectAttendance t1
                                  INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
								  INNER JOIN @MyEnrollments ON [@MyEnrollments].StuEnrollId = t1.StuEnrollId
                                  INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                                  INNER JOIN arAttUnitType AS AAUT1 ON AAUT1.UnitTypeId = t3.UnitTypeId
                                  INNER JOIN arClsSectMeetings t4 ON t1.ClsSectionId = t4.ClsSectionId
                                                                     AND (
                                                                         CONVERT(DATE, t1.MeetDate, 111) >= CONVERT(DATE, t4.StartDate, 111)
                                                                         AND CONVERT(DATE, t1.MeetDate, 111) <= CONVERT(DATE, t4.EndDate, 111)
                                                                         )
                                                                     AND t1.ClsSectMeetingId = t4.ClsSectMeetingId --DE8479 line added
                                  INNER JOIN syPeriods t5 ON t4.PeriodId = t5.PeriodId
                                  INNER JOIN cmTimeInterval t6 ON t5.StartTimeId = t6.TimeIntervalId
                                  INNER JOIN cmTimeInterval t7 ON t5.EndTimeId = t7.TimeIntervalId
                                  INNER JOIN dbo.arClassSections t9 ON t9.ClsSectionId = t1.ClsSectionId
                                  INNER JOIN dbo.arReqs t10 ON t10.ReqId = t9.ReqId
                                  INNER JOIN arAttUnitType AS AAUT2 ON AAUT2.UnitTypeId = t10.UnitTypeId
                                  WHERE      (
                                             --  t10.UnitTypeId IN ( '2600592A-9739-4A13-BDCE-7A25FE4A7478','EF5535C2-142C-4223-AE3C-25A50A153CC6' )  -- AAUT1.UnitTypeDescrip IN ( 'None','Present Absent' )
                                             --OR t3.UnitTypeId IN ( '2600592A-9739-4A13-BDCE-7A25FE4A7478','EF5535C2-142C-4223-AE3C-25A50A153CC6' )  -- AAUT2.UnitTypeDescrip IN ( 'None','Present Absent' )
                                             AAUT1.UnitTypeDescrip IN ( 'None', 'Present Absent' )
                                             OR AAUT2.UnitTypeDescrip IN ( 'None', 'Present Absent' )
                                             ) -- Minutes
                                             AND t1.Actual <> 9999
                                  ) dt
                           ) paAttendance;
        --exec Usp_ProgressReport_StudentData_GetList 'BDB21FA4-CBED-4780-AD7C-A55C50725503',null,0,0,null,'byclass',null,null

        DELETE FROM dbo.syStudentAttendanceSummary
        WHERE StuEnrollId IN (
                             SELECT DISTINCT StuEnrollId
                             FROM   @attendanceSummary
                             );
        DECLARE GetAttendance_Cursor CURSOR FORWARD_ONLY FAST_FORWARD FOR
            SELECT   *
            FROM     @attendanceSummary
            ORDER BY StuEnrollId
                    ,MeetDate;
        OPEN GetAttendance_Cursor;
        FETCH NEXT FROM GetAttendance_Cursor
        INTO @StuEnrollId
            ,@ClsSectionId
            ,@MeetDate
            ,@WeekDay
            ,@StartDate
            ,@EndDate
            ,@PeriodDescrip
            ,@Actual
            ,@Excused
            ,@Absent
            ,@ScheduledMinutes
            ,@TardyMinutes
            ,@tardy
            ,@tracktardies
            ,@tardiesMakingAbsence
            ,@PrgVerId
            ,@rownumber;
        SET @ActualRunningPresentHours = 0;
        SET @ActualRunningPresentHours = 0;
        SET @ActualRunningAbsentHours = 0;
        SET @ActualRunningTardyHours = 0;
        SET @ActualRunningMakeupHours = 0;
        SET @intTardyBreakPoint = 0;
        SET @AdjustedRunningPresentHours = 0;
        SET @AdjustedRunningAbsentHours = 0;
        SET @ActualRunningScheduledDays = 0;
        SET @boolReset = 0;
        SET @MakeupHours = 0;
        WHILE @@FETCH_STATUS = 0
            BEGIN

                IF @PrevStuEnrollId <> @StuEnrollId
                    BEGIN
                        SET @ActualRunningPresentHours = 0;
                        SET @ActualRunningAbsentHours = 0;
                        SET @intTardyBreakPoint = 0;
                        SET @ActualRunningTardyHours = 0;
                        SET @AdjustedRunningPresentHours = 0;
                        SET @AdjustedRunningAbsentHours = 0;
                        SET @ActualRunningScheduledDays = 0;
                        SET @MakeupHours = 0;
                        SET @boolReset = 1;
                    END;


                -- Scheduled and Actual: Calculate Students Actual and Adjusted Running Present Hours/Min/Day
                IF @Actual <> 9999.00
                    BEGIN
                        -- Commented by Balaji on 2.6.2015 as Excused Absence is still considered an absence
                        -- @Excused is not added to Present Hours
                        SET @ActualRunningPresentHours = @ActualRunningPresentHours + @Actual; -- + @Excused
                        SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours + @Actual; -- + @Excused

                        -- If there are make up hrs deduct that otherwise it will be added again in progress report
                        IF (
                           @Actual > 0
                           AND @Actual > @ScheduledMinutes
                           AND @Actual <> 9999.00
                           )
                            BEGIN
                                SET @ActualRunningPresentHours = @ActualRunningPresentHours - ( @Actual - @ScheduledMinutes );
                                SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - ( @Actual - @ScheduledMinutes );
                            END;

                        SET @ActualRunningScheduledDays = @ActualRunningScheduledDays + @ScheduledMinutes;
                    END;

                -- Absent: Calculate Students Actual and Adjusted Running Absent Hours/Min/Day
                SET @ActualRunningAbsentHours = @ActualRunningAbsentHours + @Absent;
                SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Absent;

                -- Tardy: Calculate Students Actual Running Tardy Hours/Min/Day			
                IF (
                   @Actual > 0
                   AND @Actual < @ScheduledMinutes
                   AND @tardy = 1
                   )
                    BEGIN
                        SET @ActualRunningTardyHours = @ActualRunningTardyHours + ( @ScheduledMinutes - @Actual );
                    END;
                ELSE IF (
                        @Actual = 1
                        AND @Actual = @ScheduledMinutes
                        AND @tardy = 1
                        )
                         BEGIN
                             SET @ActualRunningTardyHours += 1;
                         END;

                -- Track how many days student has been tardy only when 
                -- program version requires to track tardy
                IF (
                   @tracktardies = 1
                   AND @tardy = 1
                   )
                    BEGIN
                        SET @intTardyBreakPoint = @intTardyBreakPoint + 1;
                    END;


                -- Tardy: If student is tardy multiple times and when the number of days student is tardy matches
                -- the tardy rule set (for ex: 2 Tardies make 1 Absence). This rule indicates that
                -- when student is tardy the second time, that second occurance will be considered as
                -- absence
                -- Variable @intTardyBreakpoint tracks how many times the student was tardy
                -- Variable @tardiesMakingAbsence tracks the tardy rule
                DECLARE @CountTardyForClass INT;
                SET @CountTardyForClass = (
                                          SELECT COUNT(*)
                                          FROM   atClsSectAttendance
                                          WHERE  StuEnrollId = @StuEnrollId
                                                 AND ClsSectionId = @ClsSectionId
                                                 AND Tardy = 1
                                          );
                --IF (@tracktardies = 1 AND @intTardyBreakPoint = @tardiesMakingAbsence) 
                /*
				IF (@tracktardies = 1 AND @intTardyBreakPoint = @tardiesMakingAbsence) 
                BEGIN
				Commented by Balaji on 2.9.2015 as this tardy calculation doesn't take class section in to account
                    SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - @Actual - @Excused
                    SET @AdjustedRunningAbsentHours = (@AdjustedRunningAbsentHours - @Absent) + @ScheduledMinutes --@TardyMinutes
                    SET @intTardyBreakPoint = 0
				
                END
			*/

                -- Makeup: Calculate Makeup Hours when Student Attended more than scheduled hours          	       
                IF (
                   @Actual > 0
                   AND @Actual > @ScheduledMinutes
                   AND @Actual <> 9999.00
                   )
                    BEGIN
                        SET @MakeupHours = @MakeupHours + ( @Actual - @ScheduledMinutes );
                    END;

                IF ( @tracktardies = 1 )
                    BEGIN
                        SET @AdjustedPresentDaysComputed = @AdjustedRunningPresentHours; --+IsNULL(@ActualRunningTardyHours,0)
                    END;
                ELSE
                    BEGIN
                        SET @AdjustedPresentDaysComputed = @AdjustedRunningPresentHours;
                    END;

                DELETE FROM syStudentAttendanceSummary
                WHERE StuEnrollId = @StuEnrollId
                      AND ClsSectionId = @ClsSectionId
                      AND StudentAttendedDate = @MeetDate;
                INSERT INTO syStudentAttendanceSummary (
                                                       StuEnrollId
                                                      ,ClsSectionId
                                                      ,StudentAttendedDate
                                                      ,ScheduledDays
                                                      ,ActualDays
                                                      ,ActualRunningScheduledDays
                                                      ,ActualRunningPresentDays
                                                      ,ActualRunningAbsentDays
                                                      ,ActualRunningMakeupDays
                                                      ,ActualRunningTardyDays
                                                      ,AdjustedPresentDays
                                                      ,AdjustedAbsentDays
                                                      ,AttendanceTrackType
                                                      ,ModUser
                                                      ,ModDate
                                                      ,tardiesmakingabsence
                                                      ,IsTardy
                                                       )
                VALUES ( @StuEnrollId, @ClsSectionId, @MeetDate, @ScheduledMinutes, @Actual, @ActualRunningScheduledDays, @ActualRunningPresentHours
                        ,@ActualRunningAbsentHours, ISNULL(@MakeupHours, 0), @ActualRunningTardyHours, @AdjustedPresentDaysComputed
                        ,@AdjustedRunningAbsentHours, 'Post Attendance by Class Min', 'sa', GETDATE(), @tardiesMakingAbsence, @tardy );

                UPDATE syStudentAttendanceSummary
                SET    tardiesmakingabsence = @tardiesMakingAbsence
                WHERE  StuEnrollId = @StuEnrollId;
                SET @PrevStuEnrollId = @StuEnrollId;

                FETCH NEXT FROM GetAttendance_Cursor
                INTO @StuEnrollId
                    ,@ClsSectionId
                    ,@MeetDate
                    ,@WeekDay
                    ,@StartDate
                    ,@EndDate
                    ,@PeriodDescrip
                    ,@Actual
                    ,@Excused
                    ,@Absent
                    ,@ScheduledMinutes
                    ,@TardyMinutes
                    ,@tardy
                    ,@tracktardies
                    ,@tardiesMakingAbsence
                    ,@PrgVerId
                    ,@rownumber;
            END;
        CLOSE GetAttendance_Cursor;
        DEALLOCATE GetAttendance_Cursor;

        DECLARE @MyTardyTable TABLE
            (
                ClsSectionId UNIQUEIDENTIFIER
               ,TardiesMakingAbsence INT
               ,AbsentHours DECIMAL(18, 2)
            );

        INSERT INTO @MyTardyTable
                    SELECT     ClsSectionId
                              ,PV.TardiesMakingAbsence
                              ,CASE WHEN ( COUNT(*) >= PV.TardiesMakingAbsence ) THEN ( COUNT(*) / PV.TardiesMakingAbsence )
                                    ELSE 0
                               END AS AbsentHours
                    --Count(*) as NumberofTimesTardy 
                    FROM       syStudentAttendanceSummary SAS
                    INNER JOIN arStuEnrollments SE ON SAS.StuEnrollId = SE.StuEnrollId
                    INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                    WHERE      SE.StuEnrollId = @StuEnrollId
                               AND SAS.IsTardy = 1
                               AND PV.TrackTardies = 1
                    GROUP BY   ClsSectionId
                              ,PV.TardiesMakingAbsence;

        --Drop table @MyTardyTable
        DECLARE @TotalTardyAbsentDays DECIMAL(18, 2);
        SET @TotalTardyAbsentDays = (
                                    SELECT ISNULL(SUM(AbsentHours), 0)
                                    FROM   @MyTardyTable
                                    );

        --Print @TotalTardyAbsentDays

        UPDATE syStudentAttendanceSummary
        SET    AdjustedPresentDays = AdjustedPresentDays - @TotalTardyAbsentDays
              ,AdjustedAbsentDays = AdjustedAbsentDays + @TotalTardyAbsentDays
        WHERE  StuEnrollId = @StuEnrollId
               AND StudentAttendedDate = (
                                         SELECT   TOP 1 StudentAttendedDate
                                         FROM     syStudentAttendanceSummary
                                         WHERE    StuEnrollId = @StuEnrollId
                                         ORDER BY StudentAttendedDate DESC
                                         );



    END;
--=================================================================================================
-- END  --  USP_AT_Step03_InsertAttendance_Class_PresentAbsent
--=================================================================================================
GO
