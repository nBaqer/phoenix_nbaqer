SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--sp_helptext USP_FallPartC3_GetList_ApplicantsNotAdmitted_Detail
CREATE PROCEDURE [dbo].[USP_FallPartC3_GetList_ApplicantsNotAdmitted_Detail]
    @CampusId VARCHAR(50)
   ,@ProgId VARCHAR(4000) = NULL
   ,@StartDate DATETIME = NULL
   ,@EndDate DATETIME = NULL
   ,@DateRangeText VARCHAR(100) = NULL
   ,@OrderBy VARCHAR(100)
   ,@AreasOfInterest VARCHAR(4000) = NULL
AS
    BEGIN
        SELECT  dbo.UDF_FormatSSN(t1.SSN) AS SSN
               ,0 AS StudentNumber
               ,t1.LastName + ', ' + t1.FirstName + ' ' + ISNULL(t1.MiddleName,'') AS StudentName
               ,t3.IPEDSValue
               ,t1.CreatedDate
               ,t3.IPEDSSequence AS GenderSequence
               ,
			
	-- Men --
                CASE WHEN (
                            t3.IPEDSValue = 30
                            AND t11.IPEDSValue = 11
                          ) THEN 'X'
                     ELSE ''
                END AS ApplicantMen
               ,'' AS AdmissionMen
               ,CASE WHEN (
                            t3.IPEDSValue = 30
                            AND t11.IPEDSValue = 11
                          ) THEN 1
                     ELSE 0
                END AS ApplicantMenCount
               ,0 AS AdmissionMenCount
               ,'' AS FullTimeMen
               ,'' AS PartTimeMen
               ,0 AS FullTimeCountMen
               ,0 AS PartTimeCountMen
               ,
				-- Men --
				-- Women --
                CASE WHEN (
                            t3.IPEDSValue = 31
                            AND t11.IPEDSValue = 11
                          ) THEN 'X'
                     ELSE ''
                END AS ApplicantWomen
               ,'' AS AdmissionWomen
               ,CASE WHEN (
                            t3.IPEDSValue = 31
                            AND t11.IPEDSValue = 11
                          ) THEN 1
                     ELSE 0
                END AS ApplicantWomenCount
               ,0 AS AdmissionWomenCount
               ,'' AS FullTimeWomen
               ,'' AS PartTimeWomen
               ,0 AS FullTimeCountWomen
               ,0 AS PartTimeCountWomen
				-- Women --
        FROM    adLeads t1
        LEFT JOIN adGenders t3 ON t1.Gender = t3.GenderId
				-- For Degree Seeking --
        LEFT JOIN adDegCertSeeking t11 ON t1.DegCertSeekingId = t11.degcertseekingid
				-- For Degree Seeking -- 
        INNER JOIN syStatusCodes t5 ON t1.LeadStatus = t5.StatusCodeId
        INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId 
				--and t6.SysStatusId not in (7,8)
        INNER JOIN arPrgVersions t7 ON t1.PrgVerId = t7.PrgVerId
        INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
        INNER JOIN arPrgGrp t12 ON t12.PrgGrpId = t7.PrgGrpId -- Areas of Interest
        INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
        WHERE   t1.CampusId = @CampusId
                AND (
                      @ProgId IS NULL
                      OR t8.ProgId IN ( SELECT  Val
                                        FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                    )
                AND t9.IPEDSValue = 58
                AND -- UnderGraduate
                (
                  t3.IPEDSValue = 30
                  OR t3.IPEDSValue = 31
                )
                AND -- Men or Women
				--(t11.IPEDSValue=11) and -- First-Time Degree Certificate Seeking
                (
                  @AreasOfInterest IS NULL
                  OR t12.PrgGrpId IN ( SELECT   Val
                                       FROM     MultipleValuesForReportParameters(@AreasOfInterest,',',1) )
                )
                AND t1.LeadId NOT IN ( SELECT DISTINCT
                                                LeadID
                                       FROM     arStuEnrollments
                                       WHERE    LeadId IS NOT NULL )
        ORDER BY t1.LastName;
				--t3.[IPEDSSequence],t1.LastName
				--Case when @OrderBy='SSN' Then t1.SSN end,
				--Case when @OrderBy='LastName' Then t1.LastName end
    END;



GO
