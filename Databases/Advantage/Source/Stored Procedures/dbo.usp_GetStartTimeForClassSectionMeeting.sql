SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
  
CREATE PROCEDURE [dbo].[usp_GetStartTimeForClassSectionMeeting]
    (
     @ClsSectMeetingID VARCHAR(50)
    ,@selectWeek INT 
         
    )
AS
    BEGIN   
		
    
        IF @selectWeek = 0
            BEGIN
                SELECT  CONVERT(VARCHAR,CT.TimeIntervalDescrip,108)
                FROM    dbo.arClsSectMeetings CSM
                       ,SyPeriods PR
                       ,dbo.cmTimeInterval CT
                WHERE   CSM.PeriodId = PR.PeriodId
                        AND CSM.ClsSectMeetingId = @ClsSectMeetingID
                        AND PR.StartTimeId = CT.TimeIntervalId;        
    
            END;
        ELSE
            BEGIN
                DECLARE @Altcnt INT;
                SET @Altcnt = (
                                SELECT  COUNT(CONVERT(VARCHAR,CT.TimeIntervalDescrip,108))
                                FROM    dbo.arClsSectMeetings CSM
                                       ,SyPeriods PR
                                       ,dbo.cmTimeInterval CT
                                WHERE   CSM.AltPeriodId = PR.PeriodId
                                        AND CSM.ClsSectMeetingId = @ClsSectMeetingID--'0209136e-6bb8-43ff-abfc-aea7c1cd4d60'  
                                        AND PR.StartTimeId = CT.TimeIntervalId
                              );
						
                IF @Altcnt > 0
                    BEGIN
                        SELECT  CONVERT(VARCHAR,CT.TimeIntervalDescrip,108)
                        FROM    dbo.arClsSectMeetings CSM
                               ,SyPeriods PR
                               ,dbo.cmTimeInterval CT
                        WHERE   CSM.AltPeriodId = PR.PeriodId
                                AND CSM.ClsSectMeetingId = @ClsSectMeetingID--'0209136e-6bb8-43ff-abfc-aea7c1cd4d60'  
                                AND PR.StartTimeId = CT.TimeIntervalId; 
					
                    END;
                ELSE
                    BEGIN
                        SELECT  CONVERT(VARCHAR,CT.TimeIntervalDescrip,108)
                        FROM    dbo.arClsSectMeetings CSM
                               ,SyPeriods PR
                               ,dbo.cmTimeInterval CT
                        WHERE   CSM.PeriodId = PR.PeriodId
                                AND CSM.ClsSectMeetingId = @ClsSectMeetingID
                                AND PR.StartTimeId = CT.TimeIntervalId;        
                    END;
            END;
    
    END;



GO
