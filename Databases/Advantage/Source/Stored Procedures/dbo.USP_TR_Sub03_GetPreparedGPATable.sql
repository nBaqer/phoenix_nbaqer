SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =========================================================================================================
-- USP_TR_Sub03_GetPreparedGPATable
-- =========================================================================================================
CREATE PROCEDURE [dbo].[USP_TR_Sub03_GetPreparedGPATable]
(  
    @TableName NVARCHAR(200)
)
AS

	BEGIN
		DECLARE @SQL01 AS NVARCHAR(MAX);
		SET @SQL01 = '';
		SET @SQL01 = @SQL01 + 'SELECT *     ' + CHAR(10);
		SET @SQL01 = @SQL01 + 'FROM  tempdb.dbo.'+  @TableName + ' ' + CHAR(10);
		--PRINT @SQL01;
		EXECUTE (@SQL01);
	END
-- =========================================================================================================
-- END  --  USP_TR_Sub03_GetPreparedGPATable
-- =========================================================================================================
GO
