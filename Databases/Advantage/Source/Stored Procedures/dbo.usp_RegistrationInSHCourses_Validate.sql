SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_RegistrationInSHCourses_Validate]
    @stuenrollid UNIQUEIDENTIFIER
   ,@coursecode VARCHAR(50)
   ,@SHRepeatLimit INT
   ,@SHSemesterLimit INT
   ,@ReqId UNIQUEIDENTIFIER
   ,@ValidForregistration VARCHAR(50) OUTPUT
AS
    DECLARE @RegCount INT
       ,@RowCounter INT
       ,@CountCourseRegistered INT
       ,@CourseRegisteredInSemester INT;
    SET @RowCounter = 0;
	-- First check if the registeration for SH Level Courses has exceeded the limit set by school.
	-- In CCR, only 11 semesters are set for SH courses
    SET @CourseRegisteredInSemester = (
                                        SELECT  COUNT(t1.ModUser)
                                        FROM    arResults t1
                                        INNER JOIN arClassSections t2 ON t1.TestId = t2.ClsSectionId
                                        INNER JOIN arReqs t3 ON t2.ReqId = t3.ReqId
                                        WHERE   t1.StuEnrollId = @stuenrollid
                                                AND
                            --t3.Code like @coursecode + '%'
                                                t3.ReqId IN ( SELECT DISTINCT
                                                                        ReqId
                                                              FROM      arBridge_GradeComponentTypes_Courses )
                                      );
    IF @CourseRegisteredInSemester >= @SHSemesterLimit
        BEGIN
            SET @ValidForregistration = 'False'; -- If a sum of SH course registeration equals 11, then regiteration is closed for this student as far as SH Level course is concerned
        END;
    ELSE
        BEGIN
			-- If a sum of SH course registeration is less than @SHSemesterLimit (example: 11 semester)
			-- check if student was registered in a course more than @SHRepeatLimit (in CCR, its 4 times)
            SET @RowCounter = (
                                SELECT TOP 1
                                        TotalCount
                                FROM    (
                                          SELECT    t3.ReqId
                                                   ,COUNT(t3.ReqTypeId) AS TotalCount
                                          FROM      arResults t1
                                          INNER JOIN arClassSections t2 ON t1.TestId = t2.ClsSectionId
                                          INNER JOIN arReqs t3 ON t2.ReqId = t3.ReqId
                                          WHERE     t1.StuEnrollId = @stuenrollid
                                                    AND
														  --t3.Code like @coursecode + '%'
                                                    t3.ReqId IN ( SELECT DISTINCT
                                                                            ReqId
                                                                  FROM      arBridge_GradeComponentTypes_Courses )
                                          GROUP BY  t3.ReqId
                                          HAVING    COUNT(t3.ReqTypeId) >= @SHRepeatLimit
                                        ) GetRegCourseCount
                              );
            SET @RowCounter = ISNULL(@RowCounter,0);
	   				-- If student was registered more than 4 times in a course, check if the student was already registered in the selected course
            IF @RowCounter >= @SHRepeatLimit
                BEGIN
                    SET @CountCourseRegistered = (
                                                   SELECT   COUNT(t1.ModUser)
                                                   FROM     arResults t1
                                                   INNER JOIN arClassSections t2 ON t1.TestId = t2.ClsSectionId
                                                   INNER JOIN arReqs t3 ON t2.ReqId = t3.ReqId
                                                   WHERE    t1.StuEnrollId = @stuenrollid
                                                            AND t3.ReqId = @ReqId
                                                            AND
														--t3.Code like @coursecode + '%' 
                                                            t3.ReqId IN ( SELECT DISTINCT
                                                                                    ReqId
                                                                          FROM      arBridge_GradeComponentTypes_Courses )
                                                 );

					-- If Student was already registered in the course, then don't let the student to repeat the 
					-- course, as allowing the student to repeat will violate the total number of semesters : 11
                    IF @CountCourseRegistered >= 1
                        BEGIN
                            SET @ValidForregistration = 'False'; -- Student has already been registered once, not possible to repeat the course 
                        END;
                    ELSE
                        BEGIN
                            SET @ValidForregistration = 'True'; -- Student can be registered
                        END;
                END;
            IF @RowCounter < @SHRepeatLimit
                BEGIN
                    SET @ValidForregistration = 'True'; -- Student can be registered
                END;
        END;



GO
