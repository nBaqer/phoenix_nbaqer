SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--=================================================================================================
-- USP_getOnTimeGraduationDetails
--=================================================================================================
CREATE PROCEDURE [dbo].[USP_getOnTimeGraduationDetails]
    (
     @StartDate AS DATETIME
    ,@EndDate AS DATETIME
    ,@RevGradDateType AS VARCHAR(50)
    ,@CampusId AS VARCHAR(50) = NULL
    ,@ProgramIdList AS VARCHAR(MAX) = NULL
    )
AS
    BEGIN
        IF ( @RevGradDateType = 'fromprogram' )
            BEGIN
                EXEC dbo.USP_getOnTimeGraduationDetails_ProgramWeeks @StartDate,@EndDate,@CampusId,@ProgramIdList;
            END;
        ELSE
            BEGIN
                EXEC dbo.USP_getOnTimeGraduationDetails_RevisedGradDate @StartDate,@EndDate,@CampusId,@ProgramIdList;
            END;
    
    END;
--=================================================================================================
-- END  --  USP_getOnTimeGraduationDetails
--=================================================================================================    

GO
