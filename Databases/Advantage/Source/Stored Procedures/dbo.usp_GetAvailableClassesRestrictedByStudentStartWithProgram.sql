SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[usp_GetAvailableClassesRestrictedByStudentStartWithProgram]
    @campusId UNIQUEIDENTIFIER
   ,@progId UNIQUEIDENTIFIER
AS
    SET NOCOUNT ON;
    SELECT  R.ClsSectionId
           ,R.ClsSection
           ,R.ReqId
           ,R.ShiftId
           ,R.Code
           ,R.Descrip
           ,R.StartDate
           ,R.EndDate
           ,R.MaxStud - R.Registered AS Available
           ,R.TermId
           ,R.StudentStartDate
           ,R.LeadGrpId
    FROM    (
              SELECT    cs.ClsSectionId
                       ,cs.ClsSection
                       ,cs.MaxStud
                       ,cs.ReqId
                       ,cs.ShiftId
                       ,rq.Code
                       ,rq.Descrip
                       ,cs.StartDate
                       ,cs.EndDate
                       ,cs.StudentStartDate
                       ,tm.TermId
                       ,(
                          SELECT    COUNT(*)
                          FROM      arResults ar
                          WHERE     ar.TestId = cs.ClsSectionId
                        ) AS Registered
                       ,cs.LeadGrpId
              FROM      arClassSections cs
                       ,arClassSectionTerms ct
                       ,arTerm tm
                       ,arReqs rq
              WHERE     cs.ClsSectionId = ct.ClsSectionId
                        AND ct.TermId = tm.TermId
                        AND cs.ReqId = rq.ReqId
                        AND cs.EndDate > GETDATE()
                        AND cs.CampusId = @campusId
                        AND (
                              cs.StudentStartDate IS NOT NULL
                              OR cs.LeadGrpId IS NOT NULL
                              OR cs.CohortStartDate IS NOT NULL
                            )
                        AND (
                              tm.ProgId = @progId
                              OR tm.ProgId IS NULL
                            )
            ) R
    WHERE   R.MaxStud - R.Registered > 0
    ORDER BY ( CASE WHEN R.leadgrpID IS NULL THEN 1
                    ELSE 0
               END )
           ,( CASE WHEN R.StudentStartDate IS NULL THEN 1
                   ELSE 0
              END )
           ,R.leadgrpID
           ,R.StudentStartDate ASC
           ,R.EndDate DESC; 




GO
