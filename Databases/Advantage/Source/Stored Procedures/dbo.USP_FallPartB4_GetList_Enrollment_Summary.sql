SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[USP_FallPartB4_GetList_Enrollment_Summary]
    @CampusId VARCHAR(50) ,
    @ProgId VARCHAR(4000) = NULL ,
    @StartDate DATETIME = NULL ,
    @EndDate DATETIME = NULL ,
    @DateRangeText VARCHAR(100) = NULL ,
    @OrderBy VARCHAR(100)
AS
    DECLARE @ReturnValue VARCHAR(100);
    DECLARE @ContinuingStartDate DATETIME;
    DECLARE @Value VARCHAR(50);
    SET @Value = 'letter';
    SET @ContinuingStartDate = @StartDate;
-- For Academic Year Reporter, the End Date should be 10/15/Cohort Year
-- and Start Date should be blank
    IF DAY(@EndDate) = 15
        AND MONTH(@EndDate) = 10
        BEGIN
            SET @EndDate = @StartDate; -- it will always be 10/15/cohort year example: for cohort year 2009, it is 10/15/2009
            SET @StartDate = @StartDate;
        END;

    IF @ProgId IS NOT NULL
        BEGIN
            SELECT  @ReturnValue = COALESCE(@ReturnValue, '') + ProgDescrip
                    + ','
            FROM    arPrograms t1
            WHERE   t1.ProgId IN (
                    SELECT  Val
                    FROM    MultipleValuesForReportParameters(@ProgId, ',', 1) );
            SET @ReturnValue = SUBSTRING(@ReturnValue, 1,
                                         LEN(@ReturnValue) - 1);
        END;
    ELSE
        BEGIN
            SELECT  @ReturnValue = COALESCE(@ReturnValue, '') + ProgDescrip
                    + ','
            FROM    arPrograms t1
            WHERE   t1.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
                    AND t1.CampGrpId IN ( SELECT    CampGrpId
                                          FROM      syCmpGrpCmps
                                          WHERE     CampusId = @CampusId );
            SET @ReturnValue = SUBSTRING(@ReturnValue, 1,
                                         LEN(@ReturnValue) - 1);
        END;

--Declare @FirstTime_MenCount int
--set @FirstTime_MenCount=0
--Select 
----		RowNumber,SSN,StudentNumber,StudentName,Gender,Race,FirstTime,TransferIn,Continuing,
----		NonDegreeSeeking,AllOther,FirstTimeCount,TransferInCount,NonDegreeSeekingCount,
----		CountinuingCount,AllOtherCount,GenderSequence,RaceSequence,ProgDescrip,
--		SUM(FirstTime_MenCount) as FirstTime_MenCount
--From
----(		
--Create Table #FallPartB4FTUG
--			(RowNumber uniqueidentifier,SSN varchar(50),StudentNumber varchar(50),StudentName varchar(100),
--			Gender varchar(50),Race varchar(50),
--			FirstTime varchar(10),TransferIn varchar(10),Continuing varchar(10),NonDegreeSeeking varchar(10),AllOther varchar(10),
--			FirstTimeCount int,TransferInCount int,CountinuingCount int,
--			NonDegreeSeekingCount int,AllOtherCount int,GenderSequence int,RaceSequence int,
--			ProgDescrip varchar(100)) 
--
--Insert into #FallPartB4FTUG

    CREATE TABLE #EnrollmentDetail
        (
          RowNumber UNIQUEIDENTIFIER ,
          StudentNumber VARCHAR(50) ,
          StudentName VARCHAR(100) ,
          Gender VARCHAR(50) ,
          Race VARCHAR(50) ,
          UnderGraduate INT ,
          Graduate INT ,
          GenderSequence INT ,
          RaceSequence INT ,
          StudentId UNIQUEIDENTIFIER
        );

    INSERT  INTO #EnrollmentDetail
            SELECT  NEWID() AS RowNumber ,
                    StudentNumber ,
                    StudentName ,
                    Gender AS Gender ,
                    Race AS Race ,
                    ISNULL(UnderGraduateCount, 0) AS UnderGraduate ,
                    ISNULL(GraduateCount, 0) AS Graduate ,
                    GenderSequence AS GenderSequence ,
                    dt.RaceSequence AS RaceSequence ,
                    StudentId
            FROM    (
                      -- Check if there are any Foreign Students who are Full-Time Undergraduates
			-- If there are no foreign students in that category, insert a blank record
			-- with race 'Nonresident Alien'
                      SELECT TOP 1
                                NULL AS SSN ,
                                NULL AS StudentNumber ,
                                NULL AS StudentName ,
                                'Men' AS Gender ,
                                'Nonresident Alien' AS Race ,
                                NULL AS UnderGraduate ,
                                NULL AS Graduate ,
                                NULL AS UnderGraduateCount ,
                                NULL AS GraduateCount ,
                                1 AS GenderSequence ,
                                1 AS RaceSequence ,
                                NULL AS StudentId
                      FROM      arStudent
                      WHERE     ( SELECT    COUNT(DISTINCT ( t1.FirstName ))
                                  FROM      adGenders t3
                                            LEFT JOIN arStudent t1 ON t3.GenderId = t1.Gender
                                            INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                                            INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                                            INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                                              AND t6.SysStatusId NOT IN (
                                                              8 )
                                            INNER JOIN adCitizenships t11 ON t1.Citizen = t11.CitizenshipId
                                            INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                                            INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                                            INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                                            LEFT JOIN arAttendTypes t10 ON t2.AttendTypeId = t10.AttendTypeId
                                  WHERE     LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                                            AND t11.IPEDSValue = 65
                                            AND -- non citizen
                                            ( t9.IPEDSValue = 58
                                              OR t9.IPEDSValue = 59
                                            )
                                            AND -- UnderGraduate or Graduate
                                            ( t10.IPEDSValue = 61
                                              OR t10.IPEDSValue = 62
                                            )
                                            AND -- FullTime or Part Time
                                            t3.IPEDSValue = 30
                                            AND --Men
						--and (@ProgId is null or t8.ProgId in (Select Val from [MultipleValuesForReportParameters](@ProgId,',',1))) 
						--and (@StartDate is null or t2.StartDate>=@StartDate) and (@EndDate is null or t2.StartDate<=@EndDate)
						--and 
                                            t2.StartDate <= @EndDate
                                            AND StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
						(
                                            SELECT  t1.StuEnrollId
                                            FROM    arStuEnrollments t1 ,
                                                    syStatusCodes t2
                                            WHERE   t1.StatusCodeId = t2.StatusCodeId
                                                    AND StartDate <= @EndDate
                                                    AND -- Student started before the end date range
                                                    LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                                    AND ( @ProgId IS NULL
                                                          OR t8.ProgId IN (
                                                          SELECT
                                                              Val
                                                          FROM
                                                              MultipleValuesForReportParameters(@ProgId,
                                                              ',', 1) )
                                                        )
                                                    AND t2.SysStatusId IN ( 12,
                                                              14, 19, 8 ) -- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                                    AND ( t1.DateDetermined < @StartDate
                                                          OR ExpGradDate < @StartDate
                                                          OR LDA < @StartDate
                                                        ) )
						-- If Student is enrolled in only program version and if that program version 
						-- happens to be a continuing ed program exclude the student
                                            AND t2.StudentId NOT IN (
                                            SELECT DISTINCT
                                                    StudentId
                                            FROM    ( SELECT  StudentId ,
                                                              COUNT(*) AS RowCounter
                                                      FROM    arStuEnrollments se1
                                                      WHERE   PrgVerId IN (
                                                              SELECT
                                                              PrgVerId
                                                              FROM
                                                              arPrgVersions
                                                              WHERE
                                                              IsContinuingEd = 1 )
                                                              AND ( SELECT
                                                              COUNT(*)
                                                              FROM
                                                              dbo.arStuEnrollments se2
                                                              WHERE
                                                              se2.StudentId = se1.StudentId
                                                              ) = 1
                                                      GROUP BY StudentId
                                                      HAVING  COUNT(*) > 0
                                                    ) dtStudent_ContinuingEd )
                                ) = 0
                      UNION
			-- Check if there are any Foreign Students who are Full-Time Undergraduates
			-- If there are no foreign students in that category, insert a blank record
			-- with race 'Nonresident Alien'
                      SELECT DISTINCT
                                t1.SSN ,
                                t1.StudentNumber ,
                                t1.LastName + ', ' + t1.FirstName + ' '
                                + ISNULL(t1.MiddleName, '') AS StudentName ,
                                ( SELECT DISTINCT
                                            AgencyDescrip
                                  FROM      syRptAgencyFldValues
                                  WHERE     RptAgencyFldValId = t3.IPEDSValue
                                ) AS Gender ,
                                'Nonresident Alien' AS Race ,
                                CASE WHEN ( t9.IPEDSValue = 58 ) THEN 'X'
                                     ELSE ''
                                END AS UnderGraduate ,
                                CASE WHEN ( t9.IPEDSValue = 59
                                            OR t9.IPEDSValue = 60
                                          ) THEN 'X'
                                     ELSE ''
                                END AS Graduate ,
                                CASE WHEN ( t9.IPEDSValue = 58 ) THEN 1
                                     ELSE 0
                                END AS UnderGraduateCount ,
                                CASE WHEN ( t9.IPEDSValue = 59
                                            OR t9.IPEDSValue = 60
                                          ) THEN 1
                                     ELSE 0
                                END AS GraduateCount ,
                                t3.IPEDSSequence AS GenderSequence ,
                                1 AS RaceSequence ,
                                t1.StudentId
--select * from adDegCertSeeking
/*****
Select SQ1.* from arStuEnrollments SQ1,adDegCertSeeking SQ2,syStatusCodes SQ3,sySysStatus SQ4
					where 
						SQ1.StuEnrollId = '0D1079B5-378E-437F-8F2D-331A9D9FA9B6' and 
						SQ1.degcertseekingid = SQ2.DegCertSeekingId and
						-- The StartDate condition modified to < @StartDate due to Rally case DE868
						-- originally it was SQ1.StartDate<=@StartDate
						(SQ2.IPEDSValue = 12 or (SQ2.IPEDSValue=11 and SQ1.StartDate<'08/01/2009')) 
						 and SQ1.StatusCodeId = SQ3.StatusCodeId and SQ3.SysStatusId = SQ4.SysStatusId  -- and SQ4.InSchool=1
						
*****************/
                      FROM      adGenders t3
                                LEFT JOIN arStudent t1 ON t3.GenderId = t1.Gender
                                LEFT JOIN adEthCodes t4 ON t4.EthCodeId = t1.Race
                                INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                                INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                                INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                                             AND t6.SysStatusId NOT IN (
                                                             8 )
                                INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                                INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                                INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                                LEFT JOIN arAttendTypes t10 ON t2.AttendTypeId = t10.AttendTypeId
                                INNER JOIN adCitizenships t11 ON t1.Citizen = t11.CitizenshipId 
				--left JOIN adDegCertSeeking t16 on t2.degcertseekingid = t16.degcertseekingid
                      WHERE     LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                                AND ( @ProgId IS NULL
                                      OR t8.ProgId IN (
                                      SELECT    Val
                                      FROM      MultipleValuesForReportParameters(@ProgId,
                                                              ',', 1) )
                                    )
                                AND ( t9.IPEDSValue = 58
                                      OR t9.IPEDSValue = 59
                                      OR t9.IPEDSValue = 60
                                    )
                                AND -- UnderGraduate or Graduate
                                ( t10.IPEDSValue = 61
                                  OR t10.IPEDSValue = 62
                                )
                                AND -- Full Time or Part Time
                                t3.IPEDSValue = 30
                                AND -- Men
				--t1.Race is not null	 
                                t11.IPEDSValue = 65 -- Non-Citizen
				--and (@StartDate is null or t2.StartDate>=@StartDate) and (@EndDate is null or t2.StartDate<=@EndDate)
                                AND t2.StartDate <= @EndDate
                                AND StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
						(
                                SELECT  t1.StuEnrollId
                                FROM    arStuEnrollments t1 ,
                                        syStatusCodes t2
                                WHERE   t1.StatusCodeId = t2.StatusCodeId
                                        AND StartDate <= @EndDate
                                        AND -- Student started before the end date range
                                        LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                        AND ( @ProgId IS NULL
                                              OR t8.ProgId IN (
                                              SELECT    Val
                                              FROM      MultipleValuesForReportParameters(@ProgId,
                                                              ',', 1) )
                                            )
                                        AND t2.SysStatusId IN ( 12, 14, 19, 8 ) -- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                        AND ( t1.DateDetermined < @StartDate
                                              OR ExpGradDate < @StartDate
                                              OR LDA < @StartDate
                                            ) )
						-- If Student is enrolled in only program version and if that program version 
						-- happens to be a continuing ed program exclude the student
                                AND t2.StudentId NOT IN (
                                SELECT DISTINCT
                                        StudentId
                                FROM    ( SELECT    StudentId ,
                                                    COUNT(*) AS RowCounter
                                          FROM      arStuEnrollments se1
                                          WHERE     PrgVerId IN (
                                                    SELECT  PrgVerId
                                                    FROM    arPrgVersions
                                                    WHERE   IsContinuingEd = 1 )
                                                    AND ( SELECT
                                                              COUNT(*)
                                                          FROM
                                                              dbo.arStuEnrollments se2
                                                          WHERE
                                                              se2.StudentId = se1.StudentId
                                                        ) = 1
                                          GROUP BY  StudentId
                                          HAVING    COUNT(*) > 0
                                        ) dtStudent_ContinuingEd )
                      UNION				
		-- Check if there are any US Students who are Full-Time Undergraduates
		-- If there are no US Citizens in that category, insert a blank record
		-- for corresponding races
		-- Ignore NonResident Alien
                      SELECT DISTINCT
                                NULL AS SSN ,
                                NULL AS StudentNumber ,
                                NULL AS StudentName ,
                                'Men' AS Gender ,
                                ( SELECT DISTINCT
                                            AgencyDescrip
                                  FROM      syRptAgencyFldValues
                                  WHERE     RptAgencyFldValId = IPEDSValue
                                ) AS Race ,
                                NULL AS UnderGraduate ,
                                NULL AS Graduate ,
                                NULL AS UnderGraduateCount ,
                                NULL AS GraduateCount ,
                                1 AS GenderSequence ,
                                IPEDSSequence AS RaceSequence ,
                                NULL AS studentid
                      FROM      adEthCodes
                      WHERE     EthCodeDescrip <> 'Nonresident Alien'
                                AND EthCodeId NOT IN (
                                SELECT DISTINCT
                                        t1.Race
                                FROM    arStudent t1
                                        INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                                        INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                                        INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                                              AND t6.SysStatusId NOT IN (
                                                              8 )
                                        INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                                        INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                                        INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                                        LEFT JOIN arAttendTypes t10 ON t2.AttendTypeId = t10.AttendTypeId
                                        INNER JOIN adGenders t11 ON t1.Gender = t11.GenderId
                                        INNER JOIN adCitizenships t12 ON t1.Citizen = t12.CitizenshipId
                                WHERE   LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                                        AND ( @ProgId IS NULL
                                              OR t8.ProgId IN (
                                              SELECT    Val
                                              FROM      MultipleValuesForReportParameters(@ProgId,
                                                              ',', 1) )
                                            )
                                        AND ( t9.IPEDSValue = 58
                                              OR t9.IPEDSValue = 59
                                            )
                                        AND -- UnderGraduate or Graduate
                                        ( t10.IPEDSValue = 61
                                          OR t10.IPEDSValue = 62
                                        )
                                        AND -- Full Time or Part Time
                                        t11.IPEDSValue = 30
                                        AND t1.Race IS NOT NULL
                                        AND t12.IPEDSValue <> 65 -- Ignore Non US Citizens (Foreign Students)
								--and (@StartDate is null or t2.StartDate>=@StartDate) and (@EndDate is null or t2.StartDate<=@EndDate)
                                        AND t2.StartDate <= @EndDate
                                        AND StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
								-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
								(
                                        SELECT  t1.StuEnrollId
                                        FROM    arStuEnrollments t1 ,
                                                syStatusCodes t2
                                        WHERE   t1.StatusCodeId = t2.StatusCodeId
                                                AND StartDate <= @EndDate
                                                AND -- Student started before the end date range
                                                LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                                AND ( @ProgId IS NULL
                                                      OR t8.ProgId IN (
                                                      SELECT  Val
                                                      FROM    MultipleValuesForReportParameters(@ProgId,
                                                              ',', 1) )
                                                    )
                                                AND t2.SysStatusId IN ( 12, 14,
                                                              19, 8 ) -- Dropped out/Transferred/Graduated/No Start
									-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                                AND ( t1.DateDetermined < @StartDate
                                                      OR ExpGradDate < @StartDate
                                                      OR LDA < @StartDate
                                                    ) )
								-- If Student is enrolled in only program version and if that program version 
							-- happens to be a continuing ed program exclude the student
                                        AND t2.StudentId NOT IN (
                                        SELECT DISTINCT
                                                studentid
                                        FROM    ( SELECT    StudentId ,
                                                            COUNT(*) AS RowCounter
                                                  FROM      arStuEnrollments se1
                                                  WHERE     PrgVerId IN (
                                                            SELECT
                                                              PrgVerId
                                                            FROM
                                                              arPrgVersions
                                                            WHERE
                                                              IsContinuingEd = 1 )
                                                            AND ( SELECT
                                                              COUNT(*)
                                                              FROM
                                                              dbo.arStuEnrollments se2
                                                              WHERE
                                                              se2.StudentId = se1.StudentId
                                                              ) = 1
                                                  GROUP BY  StudentId
                                                  HAVING    COUNT(*) > 0
                                                ) dtStudent_ContinuingEd ) )
                      UNION
                      SELECT DISTINCT
                                t1.SSN ,
                                t1.StudentNumber ,
                                t1.LastName + ', ' + t1.FirstName + ' '
                                + ISNULL(t1.MiddleName, '') AS StudentName ,
                                ( SELECT DISTINCT
                                            AgencyDescrip
                                  FROM      syRptAgencyFldValues
                                  WHERE     RptAgencyFldValId = t3.IPEDSValue
                                ) AS Gender ,
                                CASE WHEN t4.IPEDSValue IS NULL
                                     THEN 'Race/ethnicity unknown'
                                     ELSE ( SELECT DISTINCT
                                                    AgencyDescrip
                                            FROM    syRptAgencyFldValues
                                            WHERE   RptAgencyFldValId = t4.IPEDSValue
                                          )
                                END AS Race ,
                                CASE WHEN ( t9.IPEDSValue = 58 ) THEN 'X'
                                     ELSE ''
                                END AS UnderGraduate ,
                                CASE WHEN ( t9.IPEDSValue = 59
                                            OR t9.IPEDSValue = 60
                                          ) THEN 'X'
                                     ELSE ''
                                END AS Graduate ,
                                CASE WHEN ( t9.IPEDSValue = 58 ) THEN 1
                                     ELSE 0
                                END AS UnderGraduateCount ,
                                CASE WHEN ( t9.IPEDSValue = 59
                                            OR t9.IPEDSValue = 60
                                          ) THEN 1
                                     ELSE 0
                                END AS GraduateCount ,
                                t3.IPEDSSequence AS GenderSequence ,
                                CASE WHEN t4.IPEDSValue IS NULL THEN 9
                                     ELSE t4.IPEDSSequence
                                END AS RaceSequence ,
                                t1.StudentId
                      FROM      adGenders t3
                                LEFT JOIN arStudent t1 ON t3.GenderId = t1.Gender
                                LEFT JOIN adEthCodes t4 ON t4.EthCodeId = t1.Race
                                INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                                INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                                INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                                             AND t6.SysStatusId NOT IN (
                                                             8 )
                                INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                                INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                                INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                                LEFT JOIN arAttendTypes t10 ON t2.AttendTypeId = t10.AttendTypeId
                                INNER JOIN adCitizenships t12 ON t1.Citizen = t12.CitizenshipId 
				--left JOIN adDegCertSeeking t16 on t2.degcertseekingid = t16.degcertseekingid 
                      WHERE     t2.CampusId = LTRIM(RTRIM(@CampusId))
                                AND ( @ProgId IS NULL
                                      OR t8.ProgId IN (
                                      SELECT    Val
                                      FROM      MultipleValuesForReportParameters(@ProgId,
                                                              ',', 1) )
                                    )
                                AND ( t9.IPEDSValue = 58
                                      OR t9.IPEDSValue = 59
                                      OR t9.IPEDSValue = 60
                                    )
                                AND -- UnderGraduate or Graduate
                                ( t10.IPEDSValue = 61
                                  OR t10.IPEDSValue = 62
                                )
                                AND -- Full Time or Part Time
                                t3.IPEDSValue = 30
                                AND t1.Race IS NOT NULL
                                AND ( t12.IPEDSValue <> 65
                                      OR t12.IPEDSValue IS NULL
                                    ) -- Ignore Non US Citizens (Foreign Students)
				--and (@StartDate is null or t2.StartDate>=@StartDate) and (@EndDate is null or t2.StartDate<=@EndDate) 
                                AND t2.StartDate <= @EndDate
                                AND StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
						(
                                SELECT  t1.StuEnrollId
                                FROM    arStuEnrollments t1 ,
                                        syStatusCodes t2
                                WHERE   t1.StatusCodeId = t2.StatusCodeId
                                        AND StartDate <= @EndDate
                                        AND -- Student started before the end date range
                                        LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                        AND ( @ProgId IS NULL
                                              OR t8.ProgId IN (
                                              SELECT    Val
                                              FROM      MultipleValuesForReportParameters(@ProgId,
                                                              ',', 1) )
                                            )
                                        AND t2.SysStatusId IN ( 12, 14, 19, 8 ) -- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                        AND ( t1.DateDetermined < @StartDate
                                              OR ExpGradDate < @StartDate
                                              OR LDA < @StartDate
                                            ) )
						-- If Student is enrolled in only program version and if that program version 
						-- happens to be a continuing ed program exclude the student              
                                AND t2.StudentId NOT IN (
                                SELECT DISTINCT
                                        StudentId
                                FROM    ( SELECT    StudentId ,
                                                    COUNT(*) AS RowCounter
                                          FROM      arStuEnrollments se1
                                          WHERE     PrgVerId IN (
                                                    SELECT  PrgVerId
                                                    FROM    arPrgVersions
                                                    WHERE   IsContinuingEd = 1 )
                                                    AND ( SELECT
                                                              COUNT(*)
                                                          FROM
                                                              dbo.arStuEnrollments se2
                                                          WHERE
                                                              se2.StudentId = se1.StudentId
                                                        ) = 1
                                          GROUP BY  StudentId
                                          HAVING    COUNT(*) > 0
                                        ) dtStudent_ContinuingEd )
                      UNION
		-- Check if there are any Foreign Male Students who are Full-Time Undergraduates
		-- If there are no foreign students in that category, insert a blank record
		-- with race 'Nonresident Alien'
		-- This race does not exist in adEthCodes anymore 
		-- Needs to be deleted from adEthCodes
                      SELECT TOP 1
                                NULL AS SSN ,
                                NULL AS StudentNumber ,
                                NULL AS StudentName ,
                                'Women' AS Gender ,
                                'Nonresident Alien' AS Race ,
                                NULL AS UnderGraduate ,
                                NULL AS Graduate ,
                                NULL AS UnderGraduateCount ,
                                NULL AS GraduateCount ,
                                2 AS GenderSequence ,
                                1 AS RaceSequence ,
                                NULL AS studentid
                      FROM      arStudent
                      WHERE     ( SELECT    COUNT(DISTINCT ( t1.FirstName ))
                                  FROM      adGenders t3
                                            LEFT JOIN arStudent t1 ON t3.GenderId = t1.Gender
                                            INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                                            INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                                            INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                                              AND t6.SysStatusId NOT IN (
                                                              8 )
                                            INNER JOIN adCitizenships t11 ON t1.Citizen = t11.CitizenshipId
                                            INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                                            INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                                            INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                                            LEFT JOIN arAttendTypes t10 ON t2.AttendTypeId = t10.AttendTypeId
                                  WHERE     LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                                            AND t11.IPEDSValue = 65
                                            AND -- non citizen
                                            ( t9.IPEDSValue = 58
                                              OR t9.IPEDSValue = 59
                                            )
                                            AND -- UnderGraduate or Graduate
                                            t3.IPEDSValue = 31
                                            AND --Women
                                            t10.IPEDSValue = 61  -- Full Time
                                            AND ( @ProgId IS NULL
                                                  OR t8.ProgId IN (
                                                  SELECT    Val
                                                  FROM      MultipleValuesForReportParameters(@ProgId,
                                                              ',', 1) )
                                                ) 
						--and (@StartDate is null or t2.StartDate>=@StartDate) and (@EndDate is null or t2.StartDate<=@EndDate) 
                                            AND t2.StartDate <= @EndDate
                                            AND StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
						(
                                            SELECT  t1.StuEnrollId
                                            FROM    arStuEnrollments t1 ,
                                                    syStatusCodes t2
                                            WHERE   t1.StatusCodeId = t2.StatusCodeId
                                                    AND StartDate <= @EndDate
                                                    AND -- Student started before the end date range
                                                    LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                                    AND ( @ProgId IS NULL
                                                          OR t8.ProgId IN (
                                                          SELECT
                                                              Val
                                                          FROM
                                                              MultipleValuesForReportParameters(@ProgId,
                                                              ',', 1) )
                                                        )
                                                    AND t2.SysStatusId IN ( 12,
                                                              14, 19, 8 ) -- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                                    AND ( t1.DateDetermined < @StartDate
                                                          OR ExpGradDate < @StartDate
                                                          OR LDA < @StartDate
                                                        ) )
						-- If Student is enrolled in only program version and if that program version 
						-- happens to be a continuing ed program exclude the student
                                            AND t2.StudentId NOT IN (
                                            SELECT DISTINCT
                                                    studentid
                                            FROM    ( SELECT  StudentId ,
                                                              COUNT(*) AS RowCounter
                                                      FROM    arStuEnrollments se1
                                                      WHERE   PrgVerId IN (
                                                              SELECT
                                                              PrgVerId
                                                              FROM
                                                              arPrgVersions
                                                              WHERE
                                                              IsContinuingEd = 1 )
                                                              AND ( SELECT
                                                              COUNT(*)
                                                              FROM
                                                              dbo.arStuEnrollments se2
                                                              WHERE
                                                              se2.StudentId = se1.StudentId
                                                              ) = 1
                                                      GROUP BY StudentId
                                                      HAVING  COUNT(*) > 0
                                                    ) dtStudent_ContinuingEd )
                                ) = 0
                      UNION
			-- Check if there are any Foreign Female Students who are Full-Time Undergraduates
			-- If there are no foreign students in that category, insert a blank record
			-- with race 'Nonresident Alien'
                      SELECT DISTINCT
                                t1.SSN ,
                                t1.StudentNumber ,
                                t1.LastName + ', ' + t1.FirstName + ' '
                                + ISNULL(t1.MiddleName, '') AS StudentName ,
                                ( SELECT DISTINCT
                                            AgencyDescrip
                                  FROM      syRptAgencyFldValues
                                  WHERE     RptAgencyFldValId = t3.IPEDSValue
                                ) AS Gender ,
                                'Nonresident Alien' AS Race ,
                                CASE WHEN ( t9.IPEDSValue = 58 ) THEN 'X'
                                     ELSE ''
                                END AS UnderGraduate ,
                                CASE WHEN ( t9.IPEDSValue = 59
                                            OR t9.IPEDSValue = 60
                                          ) THEN 'X'
                                     ELSE ''
                                END AS Graduate ,
                                CASE WHEN ( t9.IPEDSValue = 58 ) THEN 1
                                     ELSE 0
                                END AS UnderGraduateCount ,
                                CASE WHEN ( t9.IPEDSValue = 59
                                            OR t9.IPEDSValue = 60
                                          ) THEN 1
                                     ELSE 0
                                END AS GraduateCount ,
                                2 AS GenderSequence ,
                                1 AS RaceSequence ,
                                t1.StudentId
                      FROM      adGenders t3
                                LEFT JOIN arStudent t1 ON t3.GenderId = t1.Gender
                                LEFT JOIN adEthCodes t4 ON t4.EthCodeId = t1.Race
                                INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                                INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                                INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                                             AND t6.SysStatusId NOT IN (
                                                             8 )
                                INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                                INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                                INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                                LEFT JOIN arAttendTypes t10 ON t2.AttendTypeId = t10.AttendTypeId
                                INNER JOIN adCitizenships t11 ON t1.Citizen = t11.CitizenshipId
                      WHERE     LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                                AND ( @ProgId IS NULL
                                      OR t8.ProgId IN (
                                      SELECT    Val
                                      FROM      MultipleValuesForReportParameters(@ProgId,
                                                              ',', 1) )
                                    )
                                AND ( t9.IPEDSValue = 58
                                      OR t9.IPEDSValue = 59
                                    )
                                AND -- UnderGraduate or Graduate
                                ( t10.IPEDSValue = 61
                                  OR t10.IPEDSValue = 62
                                )
                                AND -- Full Time or Part Time
                                t3.IPEDSValue = 31
                                AND -- Women
				--t1.Race is not null	 
                                t11.IPEDSValue = 65 -- Non-Citizen				
				--and (@StartDate is null or t2.StartDate>=@StartDate) and (@EndDate is null or t2.StartDate<=@EndDate) 
                                AND t2.StartDate <= @EndDate
                                AND StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
						(
                                SELECT  t1.StuEnrollId
                                FROM    arStuEnrollments t1 ,
                                        syStatusCodes t2
                                WHERE   t1.StatusCodeId = t2.StatusCodeId
                                        AND StartDate <= @EndDate
                                        AND -- Student started before the end date range
                                        LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                        AND ( @ProgId IS NULL
                                              OR t8.ProgId IN (
                                              SELECT    Val
                                              FROM      MultipleValuesForReportParameters(@ProgId,
                                                              ',', 1) )
                                            )
                                        AND t2.SysStatusId IN ( 12, 14, 19, 8 ) -- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                        AND ( t1.DateDetermined < @StartDate
                                              OR ExpGradDate < @StartDate
                                              OR LDA < @StartDate
                                            ) )
							-- If Student is enrolled in only program version and if that program version 
						-- happens to be a continuing ed program exclude the student                                
                                AND t2.StudentId NOT IN (
                                SELECT DISTINCT
                                        StudentId
                                FROM    ( SELECT    StudentId ,
                                                    COUNT(*) AS RowCounter
                                          FROM      arStuEnrollments se1
                                          WHERE     PrgVerId IN (
                                                    SELECT  PrgVerId
                                                    FROM    arPrgVersions
                                                    WHERE   IsContinuingEd = 1 )
                                                    AND ( SELECT
                                                              COUNT(*)
                                                          FROM
                                                              dbo.arStuEnrollments se2
                                                          WHERE
                                                              se2.StudentId = se1.StudentId
                                                        ) = 1
                                          GROUP BY  StudentId
                                          HAVING    COUNT(*) > 0
                                        ) dtStudent_ContinuingEd )
                      UNION
                      SELECT DISTINCT
                                NULL AS SSN ,
                                NULL AS StudentNumber ,
                                NULL AS StudentName ,
                                'Women' AS Gender ,
                                ( SELECT DISTINCT
                                            AgencyDescrip
                                  FROM      syRptAgencyFldValues
                                  WHERE     RptAgencyFldValId = IPEDSValue
                                ) AS Race ,
                                NULL AS UnderGraduate ,
                                NULL AS Graduate ,
                                NULL AS UnderGraduateCount ,
                                NULL AS GraduateCount ,
                                2 AS GenderSequence ,
                                IPEDSSequence AS RaceSequence ,
                                NULL AS studentid
                      FROM      adEthCodes
                      WHERE     EthCodeDescrip <> 'Nonresident Alien'
                                AND EthCodeId NOT IN (
                                SELECT DISTINCT
                                        t1.Race
                                FROM    arStudent t1
                                        INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                                        INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                                        INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                                              AND t6.SysStatusId NOT IN (
                                                              8 )
                                        INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                                        INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                                        INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                                        LEFT JOIN arAttendTypes t10 ON t2.AttendTypeId = t10.AttendTypeId
                                        INNER JOIN adGenders t11 ON t1.Gender = t11.GenderId
                                        INNER JOIN adCitizenships t12 ON t1.Citizen = t12.CitizenshipId
                                WHERE   LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                                        AND ( @ProgId IS NULL
                                              OR t8.ProgId IN (
                                              SELECT    Val
                                              FROM      MultipleValuesForReportParameters(@ProgId,
                                                              ',', 1) )
                                            )
                                        AND ( t9.IPEDSValue = 58
                                              OR t9.IPEDSValue = 59
                                            )
                                        AND -- UnderGraduate or Graduate
                                        ( t10.IPEDSValue = 61
                                          OR t10.IPEDSValue = 62
                                        )
                                        AND -- Full Time or Part Time
                                        t11.IPEDSValue = 31
                                        AND t1.Race IS NOT NULL
                                        AND t12.IPEDSValue <> 65 -- Ignore Non US Citizens (Foreign Students)
								--and (@StartDate is null or t2.StartDate>=@StartDate) and (@EndDate is null or t2.StartDate<=@EndDate) 
                                        AND t2.StartDate <= @EndDate
                                        AND StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
								-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
								(
                                        SELECT  t1.StuEnrollId
                                        FROM    arStuEnrollments t1 ,
                                                syStatusCodes t2
                                        WHERE   t1.StatusCodeId = t2.StatusCodeId
                                                AND StartDate <= @EndDate
                                                AND -- Student started before the end date range
                                                LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                                AND ( @ProgId IS NULL
                                                      OR t8.ProgId IN (
                                                      SELECT  Val
                                                      FROM    MultipleValuesForReportParameters(@ProgId,
                                                              ',', 1) )
                                                    )
                                                AND t2.SysStatusId IN ( 12, 14,
                                                              19, 8 ) -- Dropped out/Transferred/Graduated/No Start
									-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                                AND ( t1.DateDetermined < @StartDate
                                                      OR ExpGradDate < @StartDate
                                                      OR LDA < @StartDate
                                                    ) )
									-- If Student is enrolled in only program version and if that program version 
						-- happens to be a continuing ed program exclude the student
                                        AND t2.StudentId NOT IN (
                                        SELECT DISTINCT
                                                studentid
                                        FROM    ( SELECT    StudentId ,
                                                            COUNT(*) AS RowCounter
                                                  FROM      arStuEnrollments se1
                                                  WHERE     PrgVerId IN (
                                                            SELECT
                                                              PrgVerId
                                                            FROM
                                                              arPrgVersions
                                                            WHERE
                                                              IsContinuingEd = 1 )
                                                            AND ( SELECT
                                                              COUNT(*)
                                                              FROM
                                                              dbo.arStuEnrollments se2
                                                              WHERE
                                                              se2.StudentId = se1.StudentId
                                                              ) = 1
                                                  GROUP BY  StudentId
                                                  HAVING    COUNT(*) > 0
                                                ) dtStudent_ContinuingEd ) )
                      UNION
                      SELECT DISTINCT
                                t1.SSN ,
                                t1.StudentNumber ,
                                t1.LastName + ', ' + t1.FirstName + ' '
                                + ISNULL(t1.MiddleName, '') AS StudentName ,
                                ( SELECT DISTINCT
                                            AgencyDescrip
                                  FROM      syRptAgencyFldValues
                                  WHERE     RptAgencyFldValId = t3.IPEDSValue
                                ) AS Gender ,
                                CASE WHEN t4.IPEDSValue IS NULL
                                     THEN 'Race/ethnicity unknown'
                                     ELSE ( SELECT DISTINCT
                                                    AgencyDescrip
                                            FROM    syRptAgencyFldValues
                                            WHERE   RptAgencyFldValId = t4.IPEDSValue
                                          )
                                END AS Race ,
                                CASE WHEN ( t9.IPEDSValue = 58 ) THEN 'X'
                                     ELSE ''
                                END AS UnderGraduate ,
                                CASE WHEN ( t9.IPEDSValue = 59
                                            OR t9.IPEDSValue = 60
                                          ) THEN 'X'
                                     ELSE ''
                                END AS Graduate ,
                                CASE WHEN ( t9.IPEDSValue = 58 ) THEN 1
                                     ELSE 0
                                END AS UnderGraduateCount ,
                                CASE WHEN ( t9.IPEDSValue = 59
                                            OR t9.IPEDSValue = 60
                                          ) THEN 1
                                     ELSE 0
                                END AS GraduateCount ,
                                2 AS GenderSequence ,
                                CASE WHEN t4.IPEDSValue IS NULL THEN 9
                                     ELSE t4.IPEDSSequence
                                END AS RaceSequence ,
                                t1.StudentId
                      FROM      adGenders t3
                                LEFT JOIN arStudent t1 ON t3.GenderId = t1.Gender
                                LEFT JOIN adEthCodes t4 ON t4.EthCodeId = t1.Race
                                INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                                INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                                INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                                             AND t6.SysStatusId NOT IN (
                                                             8 )
                                INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                                INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                                INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                                LEFT JOIN arAttendTypes t10 ON t2.AttendTypeId = t10.AttendTypeId
                                INNER JOIN adCitizenships t12 ON t1.Citizen = t12.CitizenshipId 
				--left JOIN adDegCertSeeking t16 on t2.degcertseekingid = t16.degcertseekingid 
                      WHERE     t2.CampusId = LTRIM(RTRIM(@CampusId))
                                AND ( @ProgId IS NULL
                                      OR t8.ProgId IN (
                                      SELECT    Val
                                      FROM      MultipleValuesForReportParameters(@ProgId,
                                                              ',', 1) )
                                    )
                                AND ( t9.IPEDSValue = 58
                                      OR t9.IPEDSValue = 59
                                      OR t9.IPEDSValue = 60
                                    )
                                AND -- UnderGraduate or Graduate or FirstProfessional
                                ( t10.IPEDSValue = 61
                                  OR t10.IPEDSValue = 62
                                )
                                AND -- Full Time or Part Time
                                t3.IPEDSValue = 31
                                AND t1.Race IS NOT NULL
                                AND ( t12.IPEDSValue <> 65
                                      OR t12.IPEDSValue IS NULL
                                    ) -- Ignore Non US Citizens (Foreign Students)
				--and (@StartDate is null or t2.StartDate>=@StartDate) and (@EndDate is null or t2.StartDate<=@EndDate) 
                                AND t2.StartDate <= @EndDate
                                AND StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
						(
                                SELECT  t1.StuEnrollId
                                FROM    arStuEnrollments t1 ,
                                        syStatusCodes t2
                                WHERE   t1.StatusCodeId = t2.StatusCodeId
                                        AND StartDate <= @EndDate
                                        AND -- Student started before the end date range
                                        LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                        AND ( @ProgId IS NULL
                                              OR t8.ProgId IN (
                                              SELECT    Val
                                              FROM      MultipleValuesForReportParameters(@ProgId,
                                                              ',', 1) )
                                            )
                                        AND t2.SysStatusId IN ( 12, 14, 19, 8 ) -- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                        AND ( t1.DateDetermined < @StartDate
                                              OR ExpGradDate < @StartDate
                                              OR LDA < @StartDate
                                            ) )
							-- If Student is enrolled in only program version and if that program version 
						-- happens to be a continuing ed program exclude the student
                                AND t2.StudentId NOT IN (
                                SELECT DISTINCT
                                        StudentId
                                FROM    ( SELECT    StudentId ,
                                                    COUNT(*) AS RowCounter
                                          FROM      arStuEnrollments se1
                                          WHERE     PrgVerId IN (
                                                    SELECT  PrgVerId
                                                    FROM    arPrgVersions
                                                    WHERE   IsContinuingEd = 1 )
                                                    AND ( SELECT
                                                              COUNT(*)
                                                          FROM
                                                              dbo.arStuEnrollments se2
                                                          WHERE
                                                              se2.StudentId = se1.StudentId
                                                        ) = 1
                                          GROUP BY  StudentId
                                          HAVING    COUNT(*) > 0
                                        ) dtStudent_ContinuingEd )
                    ) dt; 



--Select * from #EnrollmentDetail WHERE Gender='Men' and Race='Black or African American' ORDER BY StudentName
--SELECT  *
--INTO    #TempOutput
--FROM #EnrollmentDetail
		
    SELECT  *
    INTO    #TempOutput
    FROM    ( SELECT    StudentName ,
                        Gender ,
                        Race ,
                        UnderGraduate ,
                        Graduate ,
                        GenderSequence ,
                        RaceSequence ,
                        StudentId
              FROM      (
                          -- Bring in students with one enrollment
                          SELECT    StudentName ,
                                    Gender ,
                                    Race ,
                                    UnderGraduate ,
                                    Graduate ,
                                    GenderSequence ,
                                    RaceSequence ,
                                    StudentId
                          FROM      #EnrollmentDetail
                          WHERE     StudentId IN ( SELECT   StudentId
                                                   FROM     #EnrollmentDetail
                                                   GROUP BY StudentId
                                                   HAVING   COUNT(*) = 1 )
                          UNION
			 --The Inner query brings in all students with multiple enrollments         
                          SELECT    R.StudentName ,
                                    R.Gender ,
                                    R.Race ,
                                    ( SELECT TOP 1
                                                UnderGraduate
                                      FROM      #EnrollmentDetail
                                      WHERE     StudentId = R.StudentId
                                    ) AS UnderGraduate ,
                                    ( SELECT TOP 1
                                                Graduate
                                      FROM      #EnrollmentDetail
                                      WHERE     StudentId = R.StudentId
                                    ) AS Graduate ,
                                    R.GenderSequence ,
                                    R.RaceSequence ,
                                    R.StudentId
                          FROM      ( SELECT    StudentName ,
                                                Gender ,
                                                Race ,
                                                GenderSequence ,
                                                RaceSequence ,
                                                StudentId ,
                                                COUNT(*) AS NumEnrollments
                                      FROM      #EnrollmentDetail
                                      GROUP BY  StudentName ,
                                                Gender ,
                                                Race ,
                                                GenderSequence ,
                                                RaceSequence ,
                                                StudentId
                                      HAVING    COUNT(*) > 1
                                    ) R
				--Graduate>=1
                          UNION
			 --The Inner query brings in all students with multiple enrollments
                          SELECT    StudentName ,
                                    Gender ,
                                    Race ,
                                    UnderGraduate ,
                                    Graduate ,
                                    GenderSequence ,
                                    RaceSequence ,
                                    StudentId
                          FROM      #EnrollmentDetail
                          WHERE     StudentId IS NULL
				
		--Union
		--	Select Top 1 
		--		Gender,Race,
		--		UnderGraduate,
		--		Graduate,
		--		GenderSequence,RaceSequence,studentid
		--	from 
		--		#EnrollmentDetail
		--	where
		--		StudentId not in 
		--				(
		--					Select StudentId
		--					from 
		--						#EnrollmentDetail
		--					where
		--						Graduate >=1
		--					Group by 
		--							StudentId having count(*) > 1
		--				)
                        ) dtFinalQuery
            ) dtOutput;

--Select * from  #TempOutput WHERE Race='Hispanic/Latino' 



    SELECT  Gender ,
            Race ,
            SUM(ISNULL(UnderGraduate, 0)) AS UnderGraduate ,
            SUM(ISNULL(Graduate, 0)) AS Graduate ,
            GenderSequence ,
            RaceSequence
    FROM    #TempOutput
    GROUP BY Gender ,
            Race ,
            GenderSequence ,
            RaceSequence
    ORDER BY GenderSequence ,
            RaceSequence;

    DROP TABLE #EnrollmentDetail;
    DROP TABLE #TempOutput;


GO
