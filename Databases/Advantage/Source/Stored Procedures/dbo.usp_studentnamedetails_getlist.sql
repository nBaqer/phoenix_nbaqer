SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_studentnamedetails_getlist]
    @StuEnrollId VARCHAR(50)
AS
    SELECT DISTINCT
            S.SSN
           ,S.FirstName
           ,S.LastName
           ,S.MiddleName
           ,SC.StatusCodeDescrip
           ,SE.ExpGradDate AS StudentExpectedGraduationDate
           ,SE.StuEnrollId
           ,SE.DateDetermined AS DateDetermined
           ,SYS.InSchool
           ,P.ProgDescrip
    FROM    arStudent S
    INNER JOIN arStuEnrollments SE ON S.StudentId = SE.StudentId
    INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
    INNER JOIN arPrograms P ON PV.ProgId = P.ProgId
    INNER JOIN syStatusCodes SC ON SE.StatusCodeId = SC.StatusCodeId
    INNER JOIN sySysStatus SYS ON SC.SysStatusId = SYS.SysStatusId
    WHERE   ( SE.StuEnrollId = @StuEnrollId );



GO
