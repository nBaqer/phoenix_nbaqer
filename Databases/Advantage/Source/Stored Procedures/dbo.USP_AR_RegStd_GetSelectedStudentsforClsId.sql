SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[USP_AR_RegStd_GetSelectedStudentsforClsId]
    (
     @TestId UNIQUEIDENTIFIER
    )
AS /*----------------------------------------------------------------------------------------------------
	Author          :	Saraswathi Lakshmanan
    
    Create date		:	01/28/2010
    
	Procedure Name	:	USP_AR_RegStd_GetSelectedStudentsforClsId

	Objective		:	get the list of Selected students for the given ClsSection
	
	Parameters		:	Name			Type	Data Type			
						=====			====	=========			
						@TestID		In		UniqueIdentifier
	
	Output			:	Returns the list of Students
	
	Dependent Procedures: 		-		
						
*/-----------------------------------------------------------------------------------------------------
/*
Modified that get also StudentId (5/2015)
--To fix issue 17548: ENH: All: Register Students Page is Taking Too Long to Populate Available Students 
This query fetches the list of students selected for the given class section

*/

    BEGIN

        SELECT DISTINCT
                t1.StuEnrollId
               ,t1.GrdSysDetailId
               ,t1.ModUser
               ,t5.LastName
               ,t5.FirstName
               ,t5.StudentId
               ,t6.ExpGradDate
               ,t7.ReqId
        FROM    arResults t1
        INNER JOIN arStuEnrollments t6 ON t1.StuEnrollId = t6.StuEnrollId
        INNER JOIN arStudent t5 ON t6.StudentId = t5.StudentId
        INNER JOIN arClassSections t7 ON t1.TestId = t7.ClsSectionId
        LEFT OUTER JOIN (
                          SELECT    ResultId
                          FROM      arResults R
                          INNER JOIN arGradeSystemDetails GSD ON R.GrdSysDetailId = GSD.GrdSysDetailId
                          WHERE     GSD.IsDrop = 1
                                    AND TestId = @TestId
                        ) Table2 ON t1.ResultId = Table2.ResultId
        WHERE   t1.TestId = @TestId
                AND t1.DroppedInAddDrop = 0; 
    ------'Added on 3/31/2005 by Troy. This is to accommodate dropping courses during the add/drop period.
    ------'During this period no grade is recorded when a course is dropped. Ideally the record should
    ------'be deleted from the arResults table. However, if we actually deleted the record it would no
    ------'longer appear in the instructor's gradebook. It is good to preserve the scores that were already
    ------'recorded in the grade book just in case the student decide to take up the course again.
    ------'The arResults table was modified to include a DroppedInAddDrop field. This defaults to false
    ------'but is set to true when a course is dropped during the add/drop period.
 
    END;




GO
