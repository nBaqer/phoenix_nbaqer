SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_GetRemainingCoursesForContinuingEdProgVersion]
    @stuenrollid UNIQUEIDENTIFIER
AS
    SELECT  *
    FROM    (
              SELECT DISTINCT
                        Enrollments.StuEnrollId
                       ,Student.LastName
                       ,Student.FirstName
                       ,Enrollments.ExpGradDate
                       ,Enrollments.PrgVerId
                       ,ProgramVersions.PrgVerDescrip
                       ,Requirements.ReqId AS ReqId
                       ,Requirements.Descrip AS Req
                       ,Requirements.Code AS Code
                       ,Requirements.Credits
                       ,Requirements.Hours
                       ,'False' AS IsRequired
                       ,Enrollments.StartDate
                       ,ClassSections.StartDate AS ClassStartDate
                       ,ClassSections.EndDate AS ClassEndDate
                       ,Enrollments.ExpStartDate
                       ,(
                          SELECT    CampDescrip
                          FROM      syCampuses
                          WHERE     CampusId = Enrollments.CampusId
                        ) AS CampDescrip
                       ,Requirements.AllowCompletedCourseRetake
                       ,ClassSections.ClsSectionId
              FROM      arStuEnrollments Enrollments
                       ,arStudent Student
                       ,arPrgVersions ProgramVersions
                       ,arReqs Requirements
                       ,syStatuses Statuses
                       ,arClassSections ClassSections
              WHERE     Enrollments.StudentId = Student.StudentId
                        AND Enrollments.PrgVerId = ProgramVersions.PrgVerId
                        AND Requirements.ReqId = ClassSections.ReqId
                        AND Enrollments.StuEnrollId = @stuenrollid
                        AND Requirements.StatusId = Statuses.StatusId
                        AND Statuses.Status = 'Active'
            ) Table1
    WHERE   NOT EXISTS ( SELECT *
                         FROM   (
                                  SELECT DISTINCT
                                            ClassSections.TermId
                                           ,Terms.TermDescrip
                                           ,ClassSections.ReqId
                                           ,Requirements.Code
                                           ,Requirements.Descrip
                                           ,Requirements.Credits
                                           ,Requirements.Hours
                                           ,Terms.StartDate
                                           ,Terms.EndDate
                                           ,Results.GrdSysDetailId
                                           ,(
                                              SELECT    Grade
                                              FROM      arGradeSystemDetails
                                              WHERE     GrdSysDetailId = Results.GrdSysDetailId
                                            ) AS Grade
                                           ,(
                                              SELECT    IsPass
                                              FROM      arGradeSystemDetails
                                              WHERE     GrdSysDetailId = Results.GrdSysDetailId
                                            ) AS IsPass
                                           ,(
                                              SELECT    GPA
                                              FROM      arGradeSystemDetails
                                              WHERE     GrdSysDetailId = Results.GrdSysDetailId
                                            ) AS GPA
                                           ,(
                                              SELECT    IsCreditsAttempted
                                              FROM      arGradeSystemDetails
                                              WHERE     GrdSysDetailId = Results.GrdSysDetailId
                                            ) AS IsCreditsAttempted
                                           ,(
                                              SELECT    IsCreditsEarned
                                              FROM      arGradeSystemDetails
                                              WHERE     GrdSysDetailId = Results.GrdSysDetailId
                                            ) AS IsCreditsEarned
                                           ,(
                                              SELECT    IsInGPA
                                              FROM      arGradeSystemDetails
                                              WHERE     GrdSysDetailId = Results.GrdSysDetailId
                                            ) AS IsInGPA
                                           ,Results.StuEnrollId
                                           ,Requirements.AllowCompletedCourseRetake
                                  FROM      arResults Results
                                           ,arReqs Requirements
                                           ,arTerm Terms
                                           ,arClassSections ClassSections
                                  WHERE     Results.TestId = ClassSections.ClsSectionId
                                            AND ClassSections.TermId = Terms.TermId
                                            AND ClassSections.ReqId = Requirements.ReqId
                                            AND Results.GrdSysDetailId IS NOT NULL
                                            AND Results.StuEnrollId = @stuenrollid
                                  UNION
                                  SELECT DISTINCT
                                            TransferGrades.TermId
                                           ,Terms.TermDescrip
                                           ,TransferGrades.ReqId
                                           ,Requirements.Code
                                           ,Requirements.Descrip
                                           ,Requirements.Credits
                                           ,Requirements.Hours
                                           ,Terms.StartDate
                                           ,Terms.EndDate
                                           ,TransferGrades.GrdSysDetailId
                                           ,(
                                              SELECT    Grade
                                              FROM      arGradeSystemDetails
                                              WHERE     GrdSysDetailId = TransferGrades.GrdSysDetailId
                                            ) AS Grade
                                           ,(
                                              SELECT    IsPass
                                              FROM      arGradeSystemDetails
                                              WHERE     GrdSysDetailId = TransferGrades.GrdSysDetailId
                                            ) AS IsPass
                                           ,(
                                              SELECT    GPA
                                              FROM      arGradeSystemDetails
                                              WHERE     GrdSysDetailId = TransferGrades.GrdSysDetailId
                                            ) AS GPA
                                           ,(
                                              SELECT    IsCreditsAttempted
                                              FROM      arGradeSystemDetails
                                              WHERE     GrdSysDetailId = TransferGrades.GrdSysDetailId
                                            ) AS IsCreditsAttempted
                                           ,(
                                              SELECT    IsCreditsEarned
                                              FROM      arGradeSystemDetails
                                              WHERE     GrdSysDetailId = TransferGrades.GrdSysDetailId
                                            ) AS IsCreditsEarned
                                           ,(
                                              SELECT    IsInGPA
                                              FROM      arGradeSystemDetails
                                              WHERE     GrdSysDetailId = TransferGrades.GrdSysDetailId
                                            ) AS IsInGPA
                                           ,TransferGrades.StuEnrollId
                                           ,Requirements.AllowCompletedCourseRetake
                                  FROM      arTransferGrades TransferGrades
                                           ,arReqs Requirements
                                           ,arTerm Terms
                                  WHERE     TransferGrades.TermId = Terms.TermId
                                            AND TransferGrades.ReqId = Requirements.ReqId
                                            AND TransferGrades.GrdSysDetailId IS NOT NULL
                                            AND TransferGrades.StuEnrollId = @stuenrollid
                                ) Table2
                         WHERE  Table1.StuEnrollId = Table2.StuEnrollId
                                AND Table1.ReqId = Table2.ReqId
                                AND Table2.IsPass = 1 )
            AND NOT EXISTS ( SELECT *
                             FROM   arResults Results
                                   ,arClassSections ClassSections
                                   ,arTerm Terms
                                   ,syStatuses Statuses
                                   ,arReqs Requirements
                             WHERE  Results.StuEnrollId = @stuenrollid
                                    AND Results.GrdSysDetailId IS NULL
                                    AND Results.TestId = ClassSections.ClsSectionId
                                    AND ClassSections.TermId = Terms.TermId
                                    AND Terms.StatusId = Statuses.StatusId
                                    AND ClassSections.ReqId = Requirements.ReqId
                                    AND Results.StuEnrollId = Table1.StuEnrollId
                                    AND Table1.ReqId = ClassSections.ReqId )
            AND Table1.StartDate <= Table1.ClassStartDate -- Ignore courses that begin before the begin of enrollment
            AND Table1.ClassEndDate >= GETDATE() -- Take only courses that had not ended
    ORDER BY CampDescrip
           ,Table1.Code
           ,Table1.Req
           ,Table1.PrgVerDescrip; 
  

			----/**********************************************************************************
			----'   Build query that:
   ----         '       1. retrieves all courses regardless of the program version they belong to.
   ----         '       2. excludes courses already taken with a passing grade.
   ----         '       3. excludes courses already scheduled for.
   ----         **********************************************************************************/
            
   
            
   



GO
