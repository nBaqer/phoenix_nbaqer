SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 

CREATE PROCEDURE [dbo].[USP_GetStudentWithMissingRequirementsAndRequirementGroup_FinAidforaStudent]
    (
     @StudentID UNIQUEIDENTIFIER
    ,@CampusID UNIQUEIDENTIFIER
    ,@ProgID VARCHAR(100) = NULL
    ,@ShiftID VARCHAR(100) = NULL
    ,@StatusCodeID VARCHAR(100) = NULL 

    )
AS
    BEGIN

        DECLARE @ActiveStatusID AS UNIQUEIDENTIFIER;

        SET @ActiveStatusID = (
                                SELECT  StatusId
                                FROM    syStatuses
                                WHERE   Status = 'Active'
                              ); 

        SELECT DISTINCT
                Descrip
               ,StudentId
        FROM    (
                  SELECT DISTINCT
                            adReqId
                           ,Descrip
                           ,adReqTypeId
                           ,CampGrpId
                  FROM      adReqs
                  WHERE     adReqTypeId IN ( 1,3 )
                            AND ReqforFinancialAid = 1
                            AND StatusId = @ActiveStatusID
                ) t1
               ,(
                  SELECT DISTINCT
                            StartDate
                           ,EndDate
                           ,MinScore
                           ,adReqId
                  FROM      adReqsEffectiveDates
                  WHERE     MandatoryRequirement = 1
                ) t2
               ,(
                  SELECT DISTINCT
                            DocumentId
                           ,DocStatusId
                           ,PL.StudentId
                           ,(
                              SELECT    MIN(SE.StartDate)
                              FROM      arStuEnrollments SE
                              WHERE     StudentId = PL.StudentId
                              GROUP BY  StudentId
                            ) AS StudentStartDate
                  FROM      plStudentDocs PL
                  WHERE     StudentId = @StudentID
                            AND DocStatusId IN ( SELECT DISTINCT
                                                        DocStatusId
                                                 FROM   syDocStatuses A2
                                                       ,sySysDocStatuses A3
                                                       ,syCmpGrpCmps A4
                                                 WHERE  A2.SysDocStatusId = A3.SysDocStatusId
                                                        AND A2.CampGrpId = A4.CampGrpId
                                                        AND A4.CampusId = @CampusID
                                                        AND A3.SysDocStatusId = 2
                                                        AND A2.StatusId = @ActiveStatusID )
                ) t3
        WHERE   t1.adReqId = t2.adReqId
                AND t1.adReqId = t3.DocumentId
                AND GETDATE() >= StartDate
                AND (
                      GETDATE() <= EndDate
                      OR EndDate IS NULL
                    )
                AND t1.CampGrpId IN ( SELECT DISTINCT
                                                B2.CampGrpId
                                      FROM      syCmpGrpCmps B1
                                               ,syCampGrps B2
                                      WHERE     B1.CampGrpId = B2.CampGrpId
                                                AND B1.CampusId = @CampusID
                                                AND B2.StatusId = @ActiveStatusID )
        UNION
        SELECT DISTINCT
                Descrip
               ,StudentId
        FROM    (
                  SELECT DISTINCT
                            adReqId
                           ,Descrip
                           ,adReqTypeId
                           ,CampGrpId
                  FROM      adReqs
                  WHERE     adReqTypeId IN ( 1,3 )
                            AND ReqforFinancialAid = 1
                            AND StatusId = @ActiveStatusID
                ) t1
               ,(
                  SELECT DISTINCT
                            StartDate
                           ,EndDate
                           ,MinScore
                           ,adReqId
                  FROM      adReqsEffectiveDates
                  WHERE     MandatoryRequirement = 1
                ) t2
               ,(
                  SELECT    EntrTestId
                           ,ET.StudentId
                           ,(
                              SELECT    MIN(StartDate)
                              FROM      arStuEnrollments SE
                              WHERE     StudentId = ET.StudentId
                              GROUP BY  StudentId
                            ) AS StudentStartDate
                  FROM      adLeadEntranceTest ET
                  WHERE     StudentId = @StudentID
                            AND Pass = 0
                            AND ET.StudentId IS NOT NULL
                ) t4
        WHERE   t1.adReqId = t2.adReqId
                AND t1.adReqId = t4.EntrTestId
                AND GETDATE() >= StartDate
                AND (
                      GETDATE() <= EndDate
                      OR EndDate IS NULL
                    )
                AND t1.CampGrpId IN ( SELECT DISTINCT
                                                B2.CampGrpId
                                      FROM      syCmpGrpCmps B1
                                               ,syCampGrps B2
                                      WHERE     B1.CampGrpId = B2.CampGrpId
                                                AND B1.CampusId = @CampusID
                                                AND B2.StatusId = @ActiveStatusID )
        UNION
        SELECT DISTINCT
                Descrip
               ,StudentId
        FROM    (
                  SELECT DISTINCT
                            adReqId
                           ,Descrip
                           ,adReqTypeId
                           ,CampGrpId
                  FROM      adReqs
                  WHERE     adReqTypeId IN ( 1,3 )
                            AND ReqforFinancialAid = 1
                            AND StatusId = @ActiveStatusID
                ) t1
               ,(
                  SELECT DISTINCT
                            StartDate
                           ,EndDate
                           ,MinScore
                           ,adReqId
                  FROM      adReqsEffectiveDates
                  WHERE     MandatoryRequirement = 1
                ) t2
               ,(
                  SELECT DISTINCT
                            EntrTestId
                           ,ETO.StudentId
                           ,(
                              SELECT    MIN(StartDate)
                              FROM      arStuEnrollments
                              WHERE     StudentId = ETO.StudentId
                              GROUP BY  StudentId
                            ) AS StudentStartDate
                  FROM      adEntrTestOverRide ETO
                  WHERE     StudentId = @StudentID
                            AND OverRide = 1
                            AND ETO.StudentId IS NOT NULL
                ) t4
        WHERE   t1.adReqId = t2.adReqId
                AND t1.adReqId = t4.EntrTestId
                AND GETDATE() >= StartDate
                AND (
                      GETDATE() <= EndDate
                      OR EndDate IS NULL
                    )
                AND t1.CampGrpId IN ( SELECT DISTINCT
                                                B2.CampGrpId
                                      FROM      syCmpGrpCmps B1
                                               ,syCampGrps B2
                                      WHERE     B1.CampGrpId = B2.CampGrpId
                                                AND B1.CampusId = @CampusID
                                                AND B2.StatusId = @ActiveStatusID )
        UNION
        SELECT DISTINCT
                Descrip
               ,StudentId
        FROM    (
                  SELECT DISTINCT
                            adReqId
                           ,Descrip
                           ,adReqTypeId
                           ,CampGrpId
                  FROM      adReqs
                  WHERE     adReqTypeId IN ( 1,3 )
                            AND ReqforFinancialAid = 1
                            AND StatusId = @ActiveStatusID
                ) t1
               ,(
                  SELECT DISTINCT
                            StartDate
                           ,EndDate
                           ,MinScore
                           ,adReqId
                  FROM      adReqsEffectiveDates
                ) t2
               ,(
                  SELECT DISTINCT
                            adReqId
                  FROM      adPrgVerTestDetails t1
                           ,arStuEnrollments t2
                           ,arStudent t3
                  WHERE     t1.PrgVerId = t2.PrgVerId
                            AND t2.StudentId = t3.StudentId
                            AND t3.StudentId = @StudentID
                            AND ReqGrpId IS NULL
                            AND t2.PrgVerId IN ( SELECT DISTINCT
                                                        PrgVerId
                                                 FROM   arPrgVersions
                                                 WHERE  ProgId = @ProgID
                                                        OR @ProgID IS NULL )
                            AND (
                                  t2.ShiftId = @ShiftID
                                  OR @ShiftID IS NULL
                                )
                            AND (
                                  t2.StatusCodeId = @StatusCodeID
                                  OR @StatusCodeID IS NULL
                                )
                            AND t1.adReqId NOT IN ( SELECT DISTINCT
                                                            s1.adReqId
                                                    FROM    adReqGrpDef s1
                                                           ,adPrgVerTestDetails s2
                                                           ,arStuEnrollments s3
                                                           ,arStudent s4
                                                    WHERE   s1.ReqGrpId = s2.ReqGrpId
                                                            AND s2.PrgVerId = s3.PrgVerId
                                                            AND s3.StudentId = s4.StudentId
                                                            AND s3.StudentId = @StudentID
                                                            AND s2.adReqId IS NULL
                                                            AND s3.PrgVerId IN ( SELECT DISTINCT
                                                                                        PrgVerId
                                                                                 FROM   arPrgVersions
                                                                                 WHERE  (
                                                                                          ProgId = @ProgID
                                                                                          OR @ProgID IS NULL
                                                                                        ) )
                                                            AND (
                                                                  s3.ShiftId = @ShiftID
                                                                  OR @ShiftID IS NULL
                                                                )
                                                            AND (
                                                                  s3.StatusCodeId = @StatusCodeID
                                                                  OR @StatusCodeID IS NULL
                                                                ) )
                ) t5
               ,(
                  SELECT DISTINCT
                            DocumentId
                           ,DocStatusId
                           ,StudentId
                           ,(
                              SELECT    MIN(StartDate)
                              FROM      arStuEnrollments
                              WHERE     StudentId = SD.StudentId
                              GROUP BY  StudentId
                            ) AS StudentStartDate
                  FROM      plStudentDocs SD
                  WHERE     StudentId = @StudentID
                            AND DocStatusId IN ( SELECT DISTINCT
                                                        DocStatusId
                                                 FROM   syDocStatuses A2
                                                       ,sySysDocStatuses A3
                                                       ,syCmpGrpCmps A4
                                                 WHERE  A2.SysDocStatusId = A3.SysDocStatusId
                                                        AND A2.CampGrpId = A4.CampGrpId
                                                        AND A4.CampusId = @CampusID
                                                        AND A3.SysDocStatusId = 2
                                                        AND A2.StatusId = @ActiveStatusID )
                ) t6
        WHERE   t1.adReqId = t2.adReqId
                AND t1.adReqId = t5.adReqId
                AND t1.adReqId = t6.DocumentId
                AND GETDATE() >= StartDate
                AND (
                      GETDATE() <= EndDate
                      OR EndDate IS NULL
                    )
                AND t1.CampGrpId IN ( SELECT DISTINCT
                                                B2.CampGrpId
                                      FROM      syCmpGrpCmps B1
                                               ,syCampGrps B2
                                      WHERE     B1.CampGrpId = B2.CampGrpId
                                                AND B1.CampusId = @CampusID
                                                AND B2.StatusId = @ActiveStatusID )
        UNION
        SELECT DISTINCT
                Descrip
               ,StudentId
        FROM    (
                  SELECT DISTINCT
                            adReqId
                           ,Descrip
                           ,adReqTypeId
                           ,CampGrpId
                  FROM      adReqs
                  WHERE     adReqTypeId IN ( 1,3 )
                            AND ReqforFinancialAid = 1
                            AND StatusId = @ActiveStatusID
                ) t1
               ,(
                  SELECT DISTINCT
                            StartDate
                           ,EndDate
                           ,MinScore
                           ,adReqId
                  FROM      adReqsEffectiveDates
                ) t2
               ,(
                  SELECT DISTINCT
                            adReqId
                  FROM      adPrgVerTestDetails t1
                           ,arStuEnrollments t2
                           ,arStudent t3
                  WHERE     t1.PrgVerId = t2.PrgVerId
                            AND t2.StudentId = t3.StudentId
                            AND t3.StudentId = @StudentID
                            AND ReqGrpId IS NULL
                            AND t2.PrgVerId IN ( SELECT DISTINCT
                                                        PrgVerId
                                                 FROM   arPrgVersions
                                                 WHERE  (
                                                          ProgId = @ProgID
                                                          OR @ProgID IS NULL
                                                        ) )
                            AND (
                                  t2.ShiftId = @ShiftID
                                  OR @ShiftID IS NULL
                                )
                            AND (
                                  t2.StatusCodeId = @StatusCodeID
                                  OR @StatusCodeID IS NULL
                                )
                            AND t1.adReqId NOT IN ( SELECT DISTINCT
                                                            s1.adReqId
                                                    FROM    adReqGrpDef s1
                                                           ,adPrgVerTestDetails s2
                                                           ,arStuEnrollments s3
                                                           ,arStudent s4
                                                    WHERE   s1.ReqGrpId = s2.ReqGrpId
                                                            AND s2.PrgVerId = s3.PrgVerId
                                                            AND s3.StudentId = s4.StudentId
                                                            AND s3.StudentId = @StudentID
                                                            AND s2.adReqId IS NULL
                                                            AND s3.PrgVerId IN ( SELECT DISTINCT
                                                                                        PrgVerId
                                                                                 FROM   arPrgVersions
                                                                                 WHERE  ProgId = @ProgID
                                                                                        OR @ProgID IS NULL )
                                                            AND (
                                                                  s3.ShiftId = @ShiftID
                                                                  OR @ShiftID IS NULL
                                                                )
                                                            AND (
                                                                  s3.StatusCodeId = @StatusCodeID
                                                                  OR @StatusCodeID IS NULL
                                                                ) )
                ) t5
               ,(
                  SELECT DISTINCT
                            EntrTestId
                           ,StudentId
                           ,(
                              SELECT    MIN(StartDate)
                              FROM      dbo.arStuEnrollments
                              WHERE     StudentId = LT.StudentId
                              GROUP BY  StudentId
                            ) AS StudentStartDate
                  FROM      adLeadEntranceTest LT
                  WHERE     StudentId = @StudentID
                            AND Pass = 0
                            AND StudentId IS NOT NULL
                ) t7
        WHERE   t1.adReqId = t2.adReqId
                AND t1.adReqId = t5.adReqId
                AND t1.adReqId = t7.EntrTestId
                AND GETDATE() >= StartDate
                AND (
                      GETDATE() <= EndDate
                      OR EndDate IS NULL
                    )
                AND t1.CampGrpId IN ( SELECT DISTINCT
                                                B2.CampGrpId
                                      FROM      syCmpGrpCmps B1
                                               ,syCampGrps B2
                                      WHERE     B1.CampGrpId = B2.CampGrpId
                                                AND B1.CampusId = @CampusID
                                                AND B2.StatusId = @ActiveStatusID )
        UNION
        SELECT DISTINCT
                Descrip
               ,StudentId
        FROM    (
                  SELECT DISTINCT
                            adReqId
                           ,Descrip
                           ,adReqTypeId
                           ,CampGrpId
                  FROM      adReqs
                  WHERE     adReqTypeId IN ( 1,3 )
                            AND ReqforFinancialAid = 1
                            AND StatusId = @ActiveStatusID
                ) t1
               ,(
                  SELECT DISTINCT
                            StartDate
                           ,EndDate
                           ,MinScore
                           ,adReqId
                  FROM      adReqsEffectiveDates
                ) t2
               ,(
                  SELECT DISTINCT
                            adReqId
                  FROM      adPrgVerTestDetails t1
                           ,arStuEnrollments t2
                           ,arStudent t3
                  WHERE     t1.PrgVerId = t2.PrgVerId
                            AND t2.StudentId = t3.StudentId
                            AND t3.StudentId = @StudentID
                            AND ReqGrpId IS NULL
                            AND ( t2.PrgVerId IN ( SELECT DISTINCT
                                                            PrgVerId
                                                   FROM     arPrgVersions
                                                   WHERE    ProgId = @ProgID
                                                            OR @ProgID IS NULL ) )
                            AND (
                                  t2.ShiftId = @ShiftID
                                  OR @ShiftID IS NULL
                                )
                            AND t2.StatusCodeId = @StatusCodeID
                            OR @StatusCodeID IS NULL
                            AND t1.adReqId NOT IN ( SELECT DISTINCT
                                                            s1.adReqId
                                                    FROM    adReqGrpDef s1
                                                           ,adPrgVerTestDetails s2
                                                           ,arStuEnrollments s3
                                                           ,arStudent s4
                                                    WHERE   s1.ReqGrpId = s2.ReqGrpId
                                                            AND s2.PrgVerId = s3.PrgVerId
                                                            AND s3.StudentId = s4.StudentId
                                                            AND s3.StudentId = @StudentID
                                                            AND s2.adReqId IS NULL
                                                            AND s3.PrgVerId IN ( SELECT DISTINCT
                                                                                        PrgVerId
                                                                                 FROM   arPrgVersions
                                                                                 WHERE  ProgId = @ProgID
                                                                                        AND @ProgID IS NULL )
                                                            AND (
                                                                  s3.ShiftId = @ShiftID
                                                                  OR @ShiftID IS NULL
                                                                )
                                                            AND (
                                                                  s3.StatusCodeId = @StatusCodeID
                                                                  OR @StatusCodeID IS NULL
                                                                ) )
                ) t5
               ,(
                  SELECT DISTINCT
                            EntrTestId
                           ,StudentId
                           ,(
                              SELECT    MIN(StartDate)
                              FROM      arStuEnrollments
                              WHERE     StudentId = ETO.StudentId
                              GROUP BY  StudentId
                            ) AS StudentStartDate
                  FROM      adEntrTestOverRide ETO
                  WHERE     OverRide = 1
                            AND StudentId = @StudentID
                            AND StudentId IS NOT NULL
                ) t7
        WHERE   t1.adReqId = t2.adReqId
                AND t1.adReqId = t5.adReqId
                AND t1.adReqId = t7.EntrTestId
                AND GETDATE() >= StartDate
                AND (
                      GETDATE() <= EndDate
                      OR EndDate IS NULL
                    )
                AND t1.CampGrpId IN ( SELECT DISTINCT
                                                B2.CampGrpId
                                      FROM      syCmpGrpCmps B1
                                               ,syCampGrps B2
                                      WHERE     B1.CampGrpId = B2.CampGrpId
                                                AND B1.CampusId = @CampusID
                                                AND B2.StatusId = @ActiveStatusID )
        UNION
        SELECT DISTINCT
                Descrip
               ,StudentId
        FROM    (
                  SELECT DISTINCT
                            adReqId
                           ,Descrip
                           ,adReqTypeId
                           ,CampGrpId
                  FROM      adReqs
                  WHERE     adReqTypeId IN ( 1,3 )
                            AND ReqforFinancialAid = 1
                            AND StatusId = @ActiveStatusID
                ) t1
               ,(
                  SELECT DISTINCT
                            StartDate
                           ,EndDate
                           ,MinScore
                           ,adReqId
                           ,adReqEffectiveDateId
                  FROM      adReqsEffectiveDates
                ) t2
               ,adReqLeadGroups t3
               ,(
                  SELECT DISTINCT
                            DocumentId
                           ,DocStatusId
                           ,StudentId
                           ,(
                              SELECT    MIN(StartDate)
                              FROM      arStuEnrollments
                              WHERE     StudentId = SD.StudentId
                              GROUP BY  StudentId
                            ) AS StudentStartDate
                  FROM      plStudentDocs SD
                  WHERE     StudentId = @StudentID
                            AND DocStatusId IN ( SELECT DISTINCT
                                                        DocStatusId
                                                 FROM   syDocStatuses A2
                                                       ,sySysDocStatuses A3
                                                       ,syCmpGrpCmps A4
                                                 WHERE  A2.SysDocStatusId = A3.SysDocStatusId
                                                        AND A2.CampGrpId = A4.CampGrpId
                                                        AND A4.CampusId = @CampusID
                                                        AND A3.SysDocStatusId = 2
                                                        AND A2.StatusId = @ActiveStatusID )
                ) t4
        WHERE   t1.adReqId = t2.adReqId
                AND t2.adReqEffectiveDateId = t3.adReqEffectiveDateId
                AND t1.adReqId NOT IN ( SELECT DISTINCT
                                                s1.adReqId
                                        FROM    adReqGrpDef s1
                                               ,adPrgVerTestDetails s2
                                               ,arStuEnrollments s3
                                               ,arStudent s4
                                        WHERE   s1.ReqGrpId = s2.ReqGrpId
                                                AND s2.PrgVerId = s3.PrgVerId
                                                AND s3.StudentId = s4.StudentId
                                                AND s3.StudentId = @StudentID
                                                AND s2.adReqId IS NULL
                                                AND s3.PrgVerId IN ( SELECT DISTINCT
                                                                            PrgVerId
                                                                     FROM   arPrgVersions
                                                                     WHERE  ProgId = @ProgID
                                                                            OR @ProgID IS NULL )
                                                AND (
                                                      s3.ShiftId = @ShiftID
                                                      OR @ShiftID IS NULL
                                                    )
                                                AND (
                                                      s3.StatusCodeId = @StatusCodeID
                                                      OR @StatusCodeID IS NULL
                                                    )
                                                AND t3.LeadGrpId IN ( SELECT DISTINCT
                                                                                LeadGrpId
                                                                      FROM      adLeadByLeadGroups t1
                                                                               ,arStuEnrollments t2
                                                                               ,arStudent t3
                                                                      WHERE     t1.StuEnrollId = t2.StuEnrollId
                                                                                AND t2.StudentId = t3.StudentId
                                                                                AND t3.StudentId = @StudentID
                                                                                AND t2.PrgVerId IN ( SELECT DISTINCT
                                                                                                            PrgVerId
                                                                                                     FROM   arPrgVersions
                                                                                                     WHERE  ProgId = @ProgID
                                                                                                            OR @ProgID IS NULL )
                                                                                AND (
                                                                                      t2.ShiftId = @ShiftID
                                                                                      OR @ShiftID IS NULL
                                                                                    )
                                                                                AND t2.StatusCodeId = @StatusCodeID
                                                                                OR @StatusCodeID IS NULL ) )
                AND t1.adReqId NOT IN ( SELECT DISTINCT
                                                adReqId
                                        FROM    adPrgVerTestDetails t1
                                               ,arStuEnrollments t2
                                               ,arStudent t3
                                        WHERE   t1.PrgVerId = t2.PrgVerId
                                                AND t2.StudentId = t3.StudentId
                                                AND t3.StudentId = @StudentID
                                                AND ReqGrpId IS NOT NULL
                                                AND t2.PrgVerId IN ( SELECT DISTINCT
                                                                            PrgVerId
                                                                     FROM   arPrgVersions
                                                                     WHERE  ProgId = @ProgID
                                                                            OR @ProgID IS NULL )
                                                AND (
                                                      t2.ShiftId = @ShiftID
                                                      OR @ShiftID IS NULL
                                                    )
                                                AND t2.StatusCodeId = @StatusCodeID
                                                OR @StatusCodeID IS NULL )
                AND GETDATE() >= StartDate
                AND (
                      GETDATE() <= EndDate
                      OR EndDate IS NULL
                    )
                AND t1.CampGrpId IN ( SELECT DISTINCT
                                                B2.CampGrpId
                                      FROM      syCmpGrpCmps B1
                                               ,syCampGrps B2
                                      WHERE     B1.CampGrpId = B2.CampGrpId
                                                AND B1.CampusId = @CampusID
                                                AND B2.StatusId = @ActiveStatusID )
                AND t3.IsRequired = 1
                AND t1.adReqId = t4.DocumentId
        UNION
        SELECT DISTINCT
                Descrip
               ,StudentId
        FROM    (
                  SELECT DISTINCT
                            adReqId
                           ,Descrip
                           ,adReqTypeId
                           ,CampGrpId
                  FROM      adReqs
                  WHERE     adReqTypeId IN ( 1,3 )
                            AND ReqforFinancialAid = 1
                            AND StatusId = @ActiveStatusID
                ) t1
               ,(
                  SELECT DISTINCT
                            StartDate
                           ,EndDate
                           ,MinScore
                           ,adReqId
                           ,adReqEffectiveDateId
                  FROM      adReqsEffectiveDates
                ) t2
               ,adReqLeadGroups t3
               ,(
                  SELECT    EntrTestId
                           ,StudentId
                           ,(
                              SELECT    MIN(StartDate)
                              FROM      arStuEnrollments
                              WHERE     StudentId = ET.StudentId
                              GROUP BY  StudentId
                            ) AS StudentStartDate
                  FROM      adLeadEntranceTest ET
                  WHERE     Pass = 0
                            AND StudentId = @StudentID
                            AND StudentId IS NOT NULL
                ) t5
        WHERE   t1.adReqId = t2.adReqId
                AND t2.adReqEffectiveDateId = t3.adReqEffectiveDateId
                AND t1.adReqId NOT IN ( SELECT DISTINCT
                                                s1.adReqId
                                        FROM    adReqGrpDef s1
                                               ,adPrgVerTestDetails s2
                                               ,arStuEnrollments s3
                                               ,arStudent s4
                                        WHERE   s1.ReqGrpId = s2.ReqGrpId
                                                AND s2.PrgVerId = s3.PrgVerId
                                                AND s3.StudentId = s4.StudentId
                                                AND s3.StudentId = @StudentID
                                                AND s2.adReqId IS NULL
                                                AND s3.PrgVerId IN ( SELECT DISTINCT
                                                                            PrgVerId
                                                                     FROM   arPrgVersions
                                                                     WHERE  ProgId = @ProgID
                                                                            OR @ProgID IS NULL )
                                                AND (
                                                      s3.ShiftId = @ShiftID
                                                      OR @ShiftID IS NULL
                                                    )
                                                AND (
                                                      s3.StatusCodeId = @StatusCodeID
                                                      OR @StatusCodeID IS NULL
                                                    ) )
                AND t3.LeadGrpId IN ( SELECT DISTINCT
                                                LeadGrpId
                                      FROM      adLeadByLeadGroups t1
                                               ,arStuEnrollments t2
                                               ,arStudent t3
                                      WHERE     t1.StuEnrollId = t2.StuEnrollId
                                                AND t2.StudentId = t3.StudentId
                                                AND t3.StudentId = @StudentID )
                AND t1.adReqId NOT IN ( SELECT DISTINCT
                                                adReqId
                                        FROM    adPrgVerTestDetails t1
                                               ,arStuEnrollments t2
                                               ,arStudent t3
                                        WHERE   t1.PrgVerId = t2.PrgVerId
                                                AND t2.StudentId = t3.StudentId
                                                AND t3.StudentId = @StudentID
                                                AND ReqGrpId IS NULL
                                                AND t2.PrgVerId IN ( SELECT DISTINCT
                                                                            PrgVerId
                                                                     FROM   arPrgVersions
                                                                     WHERE  ProgId = @ProgID
                                                                            OR @ProgID IS NULL )
                                                AND (
                                                      t2.ShiftId = @ShiftID
                                                      OR @ShiftID IS NULL
                                                    )
                                                AND (
                                                      t2.StatusCodeId = @StatusCodeID
                                                      OR @StatusCodeID IS NULL
                                                    ) )
                AND GETDATE() >= StartDate
                AND (
                      GETDATE() <= EndDate
                      OR EndDate IS NULL
                    )
                AND t1.CampGrpId IN ( SELECT DISTINCT
                                                B2.CampGrpId
                                      FROM      syCmpGrpCmps B1
                                               ,syCampGrps B2
                                      WHERE     B1.CampGrpId = B2.CampGrpId
                                                AND B1.CampusId = @CampusID
                                                AND B2.StatusId = @ActiveStatusID )
                AND t3.IsRequired = 1
                AND t1.adReqId = t5.EntrTestId
        UNION
        SELECT DISTINCT
                Descrip
               ,StudentId
        FROM    (
                  SELECT DISTINCT
                            adReqId
                           ,Descrip
                           ,adReqTypeId
                           ,CampGrpId
                  FROM      adReqs
                  WHERE     adReqTypeId IN ( 1,3 )
                            AND ReqforFinancialAid = 1
                            AND StatusId = @ActiveStatusID
                ) t1
               ,(
                  SELECT DISTINCT
                            StartDate
                           ,EndDate
                           ,MinScore
                           ,adReqId
                           ,adReqEffectiveDateId
                  FROM      adReqsEffectiveDates
                ) t2
               ,adReqLeadGroups t3
               ,(
                  SELECT DISTINCT
                            EntrTestId
                           ,StudentId
                           ,(
                              SELECT    MIN(StartDate)
                              FROM      arStuEnrollments
                              WHERE     StudentId = ETO.StudentId
                              GROUP BY  StudentId
                            ) AS StudentStartDate
                  FROM      adEntrTestOverRide ETO
                  WHERE     OverRide = 1
                            AND StudentId = @StudentID
                            AND StudentId IS NOT NULL
                ) t5
        WHERE   t1.adReqId = t2.adReqId
                AND t2.adReqEffectiveDateId = t3.adReqEffectiveDateId
                AND t1.adReqId NOT IN ( SELECT DISTINCT
                                                s1.adReqId
                                        FROM    adReqGrpDef s1
                                               ,adPrgVerTestDetails s2
                                               ,arStuEnrollments s3
                                               ,arStudent s4
                                        WHERE   s1.ReqGrpId = s2.ReqGrpId
                                                AND s2.PrgVerId = s3.PrgVerId
                                                AND s3.StudentId = s4.StudentId
                                                AND s3.StudentId = @StudentID
                                                AND s2.adReqId IS NULL
                                                AND s3.PrgVerId IN ( SELECT DISTINCT
                                                                            PrgVerId
                                                                     FROM   arPrgVersions
                                                                     WHERE  ProgId = @ProgID
                                                                            OR @ProgID IS NULL )
                                                AND (
                                                      s3.ShiftId = @ShiftID
                                                      OR @ShiftID IS NULL
                                                    )
                                                AND (
                                                      s3.StatusCodeId = @StatusCodeID
                                                      OR @StatusCodeID IS NULL
                                                    ) )
                AND t3.LeadGrpId IN ( SELECT DISTINCT
                                                LeadGrpId
                                      FROM      adLeadByLeadGroups t1
                                               ,arStuEnrollments t2
                                               ,arStudent t3
                                      WHERE     t1.StuEnrollId = t2.StuEnrollId
                                                AND t2.StudentId = t3.StudentId
                                                AND t3.StudentId = @StudentID )
                AND t1.adReqId NOT IN ( SELECT DISTINCT
                                                adReqId
                                        FROM    adPrgVerTestDetails t1
                                               ,arStuEnrollments t2
                                               ,arStudent t3
                                        WHERE   t1.PrgVerId = t2.PrgVerId
                                                AND t2.StudentId = t3.StudentId
                                                AND t3.StudentId = @StudentID
                                                AND ReqGrpId IS NULL
                                                AND t2.PrgVerId IN ( SELECT DISTINCT
                                                                            PrgVerId
                                                                     FROM   arPrgVersions
                                                                     WHERE  ProgId = @ProgID
                                                                            OR @ProgID IS NULL )
                                                AND (
                                                      t2.ShiftId = @ShiftID
                                                      OR @ShiftID IS NULL
                                                    )
                                                AND (
                                                      t2.StatusCodeId = @StatusCodeID
                                                      OR @StatusCodeID IS NULL
                                                    ) )
                AND GETDATE() >= StartDate
                AND (
                      GETDATE() <= EndDate
                      OR EndDate IS NULL
                    )
                AND t1.CampGrpId IN ( SELECT DISTINCT
                                                B2.CampGrpId
                                      FROM      syCmpGrpCmps B1
                                               ,syCampGrps B2
                                      WHERE     B1.CampGrpId = B2.CampGrpId
                                                AND B1.CampusId = @CampusID
                                                AND B2.StatusId = @ActiveStatusID )
                AND t3.IsRequired = 1
                AND t1.adReqId = t5.EntrTestId;
    END;




GO
