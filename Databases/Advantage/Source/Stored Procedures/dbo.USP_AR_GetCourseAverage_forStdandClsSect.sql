SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--DE9762 fixed
CREATE PROCEDURE [dbo].[USP_AR_GetCourseAverage_forStdandClsSect]
    (
     @StuEnrollID UNIQUEIDENTIFIER
    ,@clsSectId UNIQUEIDENTIFIER
    ,@Score DECIMAL OUTPUT
    )
AS /*----------------------------------------------------------------------------------------------------
    Author : Saraswathi Lakshmanan
    
    Create date : 02/03/2010
    
    Procedure Name : USP_AR_GetCourseAverage_forStdandClsSect

    Objective : get the average score
    
    Parameters : Name Type Data Type Required? 
                        ===== ==== ========= ========= 
                        @StuEnrollId In Uniqueidentifier
                        @ClsSectID In UniqueIdentifier 
                    
    Output : returns the score
    
    Calling procedure : DoesStdHavePassingGrdForNumeric 
                        
*/-----------------------------------------------------------------------------------------------------

/*
Procedure created by Saraswathi Lakshmanan on Jan 29 2010
--To fix issue 17548: ENH: All: Register Students Page is Taking Too Long to Populate Available Students 
*/

    BEGIN
        SET @Score = (
                       SELECT TOP 1
                                Score AS Score
                       FROM     (
                                  SELECT    ISNULL((
                                                     SELECT ISNULL(Score,0)
                                                     FROM   arResults
                                                     WHERE  stuEnrollId = @StuEnrollID
                                                            AND Testid = @clsSectId
                                                   ),0) AS Score
                                  UNION
                                  SELECT    ISNULL((
                                                     SELECT ISNULL(a.score,0)
                                                     FROM   arTransferGrades a
                                                     INNER JOIN arClassSections b ON a.TermId = b.TermId
                                                                                     AND a.Reqid = b.Reqid
                                                     WHERE  a.stuEnrollId = @StuEnrollID
                                                            AND b.ClsSectionId = @clsSectId
                                                   ),0) AS Score
                                ) tblScore
                       ORDER BY Score DESC
                     );
    --return @Score 
    END;



GO
