SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_pr_main_subreport1]
    @StuEnrollId VARCHAR(MAX) = NULL
   ,@CampGrpId VARCHAR(4000) = NULL
   ,@PrgVerId VARCHAR(4000) = NULL
   ,@StatusCodeId VARCHAR(4000) = NULL
   ,@TermId VARCHAR(4000) = NULL
   ,@StudentGrpId VARCHAR(4000) = NULL
   ,@GradesFormat VARCHAR(50)
   ,@GPAMethod VARCHAR(50)
   ,@ShowWorkUnitGrouping BIT = 0
   ,@SysComponentTypeId VARCHAR(100) = NULL
   ,@OrderBy VARCHAR(100) = NULL
   ,@ShowFinanceCalculations BIT = 0
   ,@ShowWeeklySchedule BIT = 0
   ,@GradeRounding BIT = 0
   ,@ShowStudentSignatureLine BIT = 0
   ,@ShowSchoolSignatureLine BIT = 0
   ,@ShowPageNumber BIT = 1
   ,@ShowHeading BIT = 1
   ,@StartDateModifier VARCHAR(10) = NULL
   ,@StartDate VARCHAR(50) = NULL
   ,@ExpectedGradDateModifier VARCHAR(10) = NULL
   ,@ExpectedGradDate VARCHAR(50) = NULL
   ,@UserId VARCHAR(50) = NULL
   ,@CampusId VARCHAR(4000) = NULL
AS -- If User selects no filters in the progress report, then go in to the if condition and there is no need to store the student enrollments in a table variable
-- If User selects filters in the progress report, then go in to the else condition and there is a need to store the student enrollments in a table variable
    IF (
         @CampGrpId IS NULL
         AND @PrgVerId IS NULL
         AND @StatusCodeId IS NULL
         AND @StuEnrollId IS NULL
         AND @StartDate IS NULL
         AND @StudentGrpId IS NULL
         AND @TermId IS NULL
         AND @CampusId IS NULL
       )
        BEGIN
		--Print 'if'
            SELECT  StuEnrollId
                   ,PrgVerDescrip
                   ,CampDescrip
                   ,
		--Case When SSN is not NULL Then dbo.UDF_FormatSSN(SSN) Else NULL End as SSN,
                    dbo.UDF_FormatSSN(SSN) AS SSN
                   ,FirstName
                   ,LastName
                   ,StudentNumber
                   ,MiddleName
                   ,ProgDescrip
                   ,StatusCodeDescrip
                   ,StudentExpectedGraduationDate
                   ,DateDetermined
                   ,InSchool
                   ,Address1
                   ,Address2
                   ,City
                   ,StateDescrip
                   ,CASE WHEN Phone1 IS NOT NULL THEN 'Phone: ' + dbo.UDF_FormatPhone(Phone1)
                         ELSE NULL
                    END AS Phone1
                   ,Phone2
                   ,Phone3
                   ,CASE WHEN Fax IS NOT NULL THEN 'Fax: ' + dbo.UDF_FormatPhone(Fax)
                         ELSE NULL
                    END AS Fax
                   ,Website
                   ,dbo.UDF_FormatZip(Zip) AS Zip
                   ,CampusId
                   ,ClockHourProgram
                   ,SysStatusId
                   ,CASE WHEN SysStatusId = 11 THEN CONVERT(CHAR(10),SuspensionStartDate,101) + ' to ' + CONVERT(CHAR(10),SuspensionEndDate,101)
                         WHEN SysStatusId = 20 THEN CONVERT(CHAR(10),BeginProbationDate,101)
                         WHEN SysStatusId = 10 THEN CONVERT(CHAR(10),BeginLeaveDate,101) + ' to ' + CONVERT(CHAR(10),EndLeaveDate,101)
                         ELSE NULL
                    END AS SuspensionRange
                   ,EnrollmentId
            FROM    (
                      SELECT DISTINCT
                                SE.StuEnrollId
                               ,PV.PrgVerDescrip
                               ,C.CampDescrip
                               ,S.SSN
                               ,S.FirstName
                               ,S.LastName
                               ,S.MiddleName
                               ,S.StudentNumber
                               ,SC.StatusCodeDescrip
                               ,SE.ExpGradDate AS StudentExpectedGraduationDate
                               ,SE.DateDetermined AS DateDetermined
                               ,SS.InSchool
                               ,P.ProgDescrip
                               ,Address1
                               ,Address2
                               ,City
                               ,SY.StateDescrip
                               ,Phone1
                               ,Phone2
                               ,Phone3
                               ,Fax
                               ,Website
                               ,Zip
                               ,C.CampusId
                               ,CASE WHEN P.ACId = 5 THEN 'True'
                                     ELSE 'False'
                                END AS ClockHourProgram
                               ,SS.SysStatusId
                               ,SE.EnrollmentId
                               ,CASE WHEN SS.SysStatusId = 11 THEN (
                                                                     SELECT MAX(StartDate)
                                                                     FROM   arStdSuspensions
                                                                     WHERE  StuEnrollId = @StuEnrollId
                                                                   )
                                     ELSE NULL
                                END AS SuspensionStartDate
                               , --Suspended
                                CASE WHEN SS.SysStatusId = 11 THEN (
                                                                     SELECT MAX(EndDate)
                                                                     FROM   arStdSuspensions
                                                                     WHERE  StuEnrollId = @StuEnrollId
                                                                   )
                                     ELSE NULL
                                END AS SuspensionEndDate
                               , -- Suspended
                                CASE WHEN SS.SysStatusId = 20 THEN (
                                                                     SELECT MAX(StartDate)
                                                                     FROM   arStuProbWarnings SP
                                                                     WHERE  SP.StuEnrollId = @StuEnrollId
                                                                            AND SP.ProbWarningTypeId = 1
                                                                   )
                                     ELSE NULL
                                END AS BeginProbationDate
                               , -- Probation
                                CASE WHEN SS.SysStatusId = 10 THEN (
                                                                     SELECT MAX(StartDate)
                                                                     FROM   arStudentLOAs
                                                                     WHERE  StuEnrollId = @StuEnrollId
                                                                   )
                                     ELSE NULL
                                END AS BeginLeaveDate
                               , -- LOA
                                CASE WHEN SS.SysStatusId = 10 THEN (
                                                                     SELECT MAX(EndDate)
                                                                     FROM   arStudentLOAs
                                                                     WHERE  StuEnrollId = @StuEnrollId
                                                                   )
                                     ELSE NULL
                                END AS EndLeaveDate  --LOA
                      FROM      arStuEnrollments SE
                      INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                      INNER JOIN (
                                   SELECT   StudentId
                                           ,FirstName
                                           ,LastName
                                           ,MiddleName
                                           ,SSN
                                           ,StudentNumber
                                   FROM     arStudent
                                 ) S ON S.StudentId = SE.StudentId
                      INNER JOIN syCampuses C ON C.CampusId = SE.CampusId
                      INNER JOIN syStatusCodes SC ON SE.StatusCodeId = SC.StatusCodeId
                      INNER JOIN sySysStatus SS ON SS.SysStatusId = SC.SysStatusId
                      INNER JOIN arPrograms P ON P.ProgId = PV.ProgId
                      LEFT OUTER JOIN syStates SY ON C.StateId = SY.StateId
                    ) dt
            ORDER BY CampDescrip
                   ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'LastName Asc,FirstName Asc,MiddleName Asc'
                         THEN ( RANK() OVER ( ORDER BY LastName, FirstName, MiddleName ) )
                    END
                   ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'LastName Asc,FirstName Asc,MiddleName Desc'
                         THEN ( RANK() OVER ( ORDER BY LastName, FirstName, MiddleName DESC ) )
                    END
                   ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'LastName Asc,FirstName Desc,MiddleName Asc'
                         THEN ( RANK() OVER ( ORDER BY LastName, FirstName DESC, MiddleName ) )
                    END
                   ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'LastName Asc,FirstName Desc,MiddleName Desc'
                         THEN ( RANK() OVER ( ORDER BY LastName, FirstName DESC, MiddleName DESC ) )
                    END
                   ,
			 --LD
                    CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'LastName Desc,FirstName Asc,MiddleName Asc'
                         THEN ( RANK() OVER ( ORDER BY LastName DESC, FirstName, MiddleName ) )
                    END
                   ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'LastName Desc,FirstName Asc,MiddleName Desc'
                         THEN ( RANK() OVER ( ORDER BY LastName DESC, FirstName, MiddleName DESC ) )
                    END
                   ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'LastName Desc,FirstName Desc,MiddleName Asc'
                         THEN ( RANK() OVER ( ORDER BY LastName DESC, FirstName DESC, MiddleName ) )
                    END
                   ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'LastName Desc,FirstName Desc,MiddleName Desc'
                         THEN ( RANK() OVER ( ORDER BY LastName DESC, FirstName DESC, MiddleName DESC ) )
                    END
                   ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'LastName Asc,MiddleName Asc,FirstName Asc'
                         THEN ( RANK() OVER ( ORDER BY LastName ASC, MiddleName ASC, FirstName ASC ) )
                    END
                   ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'LastName Asc,MiddleName Asc,FirstName Desc'
                         THEN ( RANK() OVER ( ORDER BY LastName ASC, MiddleName ASC, FirstName DESC ) )
                    END
                   ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'LastName Asc,MiddleName Desc,FirstName Asc'
                         THEN ( RANK() OVER ( ORDER BY LastName ASC, MiddleName DESC, FirstName ASC ) )
                    END
                   ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'LastName Asc,MiddleName Desc,FirstName Desc'
                         THEN ( RANK() OVER ( ORDER BY LastName ASC, MiddleName DESC, FirstName DESC ) )
                    END
                   ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'LastName Desc,MiddleName Asc,FirstName Asc'
                         THEN ( RANK() OVER ( ORDER BY LastName DESC, MiddleName ASC, FirstName ASC ) )
                    END
                   ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'LastName Desc,MiddleName Asc,FirstName Desc'
                         THEN ( RANK() OVER ( ORDER BY LastName DESC, MiddleName ASC, FirstName DESC ) )
                    END
                   ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'LastName Desc,MiddleName Desc,FirstName Asc'
                         THEN ( RANK() OVER ( ORDER BY LastName DESC, MiddleName DESC, FirstName ASC ) )
                    END
                   ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'LastName Desc,MiddleName Desc,FirstName Desc'
                         THEN ( RANK() OVER ( ORDER BY LastName DESC, MiddleName DESC, FirstName DESC ) )
                    END
                   ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'FirstName Asc,LastName Asc,MiddleName Asc'
                         THEN ( RANK() OVER ( ORDER BY FirstName ASC, LastName ASC, MiddleName ASC ) )
                    END
                   ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'FirstName Asc,LastName Asc,MiddleName Desc'
                         THEN ( RANK() OVER ( ORDER BY FirstName ASC, LastName ASC, MiddleName DESC ) )
                    END
                   ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'FirstName Asc,LastName Desc,MiddleName Asc'
                         THEN ( RANK() OVER ( ORDER BY FirstName ASC, LastName DESC, MiddleName ASC ) )
                    END
                   ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'FirstName Asc,LastName Desc,MiddleName Desc'
                         THEN ( RANK() OVER ( ORDER BY FirstName ASC, LastName DESC, MiddleName DESC ) )
                    END
                   ,
			 --LD
                    CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'FirstName Desc,LastName Asc,MiddleName Asc'
                         THEN ( RANK() OVER ( ORDER BY FirstName DESC, LastName ASC, MiddleName ASC ) )
                    END
                   ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'FirstName Desc,LastName Asc,MiddleName Desc'
                         THEN ( RANK() OVER ( ORDER BY FirstName DESC, LastName ASC, MiddleName DESC ) )
                    END
                   ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'FirstName Desc,LastName Desc,MiddleName Asc'
                         THEN ( RANK() OVER ( ORDER BY FirstName DESC, LastName DESC, MiddleName ASC ) )
                    END
                   ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'FirstName Desc,LastName Desc,MiddleName Desc'
                         THEN ( RANK() OVER ( ORDER BY FirstName DESC, LastName DESC, MiddleName DESC ) )
                    END
                   ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'FirstName Asc,MiddleName Asc,LastName Asc'
                         THEN ( RANK() OVER ( ORDER BY FirstName ASC, MiddleName ASC, LastName ASC ) )
                    END
                   ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'FirstName Asc,MiddleName Asc,LastName Desc'
                         THEN ( RANK() OVER ( ORDER BY FirstName ASC, MiddleName ASC, LastName DESC ) )
                    END
                   ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'FirstName Asc,MiddleName Desc,LastName Asc'
                         THEN ( RANK() OVER ( ORDER BY FirstName ASC, MiddleName DESC, LastName ASC ) )
                    END
                   ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'FirstName Asc,MiddleName Desc,LastName Desc'
                         THEN ( RANK() OVER ( ORDER BY FirstName ASC, MiddleName DESC, LastName DESC ) )
                    END
                   ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'FirstName Desc,MiddleName Asc,LastName Asc'
                         THEN ( RANK() OVER ( ORDER BY FirstName DESC, MiddleName ASC, LastName ASC ) )
                    END
                   ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'FirstName Desc,MiddleName Asc,LastName Desc'
                         THEN ( RANK() OVER ( ORDER BY FirstName DESC, MiddleName ASC, LastName DESC ) )
                    END
                   ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'FirstName Desc,MiddleName Desc,LastName Asc'
                         THEN ( RANK() OVER ( ORDER BY FirstName DESC, MiddleName DESC, LastName ASC ) )
                    END
                   ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'FirstName Desc,MiddleName Desc,LastName Desc'
                         THEN ( RANK() OVER ( ORDER BY FirstName DESC, MiddleName DESC, LastName DESC ) )
                    END
                   ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'MiddleName Asc,LastName Asc,FirstName Asc'
                         THEN ( RANK() OVER ( ORDER BY MiddleName ASC, LastName ASC, FirstName ASC ) )
                    END
                   ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'MiddleName Asc,LastName Asc,FirstName Desc'
                         THEN ( RANK() OVER ( ORDER BY MiddleName ASC, LastName ASC, FirstName DESC ) )
                    END
                   ,
			 
			 -- Start here
                    CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'MiddleName Asc,LastName Desc,FirstName Asc'
                         THEN ( RANK() OVER ( ORDER BY MiddleName ASC, LastName DESC, FirstName ASC ) )
                    END
                   ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'MiddleName Asc,LastName Desc,FirstName Desc'
                         THEN ( RANK() OVER ( ORDER BY MiddleName ASC, LastName DESC, FirstName DESC ) )
                    END
                   ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'MiddleName Desc,LastName Asc,FirstName Asc'
                         THEN ( RANK() OVER ( ORDER BY MiddleName DESC, LastName ASC, FirstName ASC ) )
                    END
                   ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'MiddleName Desc,LastName Asc,FirstName Desc'
                         THEN ( RANK() OVER ( ORDER BY MiddleName DESC, LastName ASC, FirstName DESC ) )
                    END
                   ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'MiddleName Desc,LastName Desc,FirstName Asc'
                         THEN ( RANK() OVER ( ORDER BY MiddleName DESC, LastName DESC, FirstName ASC ) )
                    END
                   ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'MiddleName Desc,LastName Desc,FirstName Desc'
                         THEN ( RANK() OVER ( ORDER BY MiddleName DESC, LastName DESC, FirstName DESC ) )
                    END
                   ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'MiddleName Asc,FirstName Asc,LastName Asc'
                         THEN ( RANK() OVER ( ORDER BY MiddleName ASC, FirstName ASC, LastName ASC ) )
                    END
                   ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'MiddleName Asc,FirstName Asc,LastName Desc'
                         THEN ( RANK() OVER ( ORDER BY MiddleName ASC, FirstName ASC, LastName DESC ) )
                    END
                   ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'MiddleName Asc,FirstName Desc,LastName Asc'
                         THEN ( RANK() OVER ( ORDER BY MiddleName ASC, FirstName DESC, LastName ASC ) )
                    END
                   ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'MiddleName Asc,FirstName Desc,LastName Desc'
                         THEN ( RANK() OVER ( ORDER BY MiddleName ASC, FirstName DESC, LastName DESC ) )
                    END
                   ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'MiddleName Desc,FirstName Asc,LastName Asc'
                         THEN ( RANK() OVER ( ORDER BY MiddleName DESC, FirstName ASC, LastName ASC ) )
                    END
                   ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'MiddleName Desc,FirstName Asc,LastName Desc'
                         THEN ( RANK() OVER ( ORDER BY MiddleName DESC, FirstName ASC, LastName DESC ) )
                    END
                   ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'MiddleName Desc,FirstName Desc,LastName Asc'
                         THEN ( RANK() OVER ( ORDER BY MiddleName DESC, FirstName DESC, LastName ASC ) )
                    END
                   ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'MiddleName Desc,FirstName Desc,LastName Desc'
                         THEN ( RANK() OVER ( ORDER BY MiddleName DESC, FirstName DESC, LastName DESC ) )
                    END;
        END;
	------------- Ends Here --------------------------------
    ELSE
        BEGIN
            SELECT  StuEnrollId
                   ,PrgVerDescrip
                   ,CampDescrip
                   ,
		--Case When SSN is not NULL Then dbo.UDF_FormatSSN(SSN) Else NULL End as SSN,
                    dbo.UDF_FormatSSN(SSN) AS SSN
                   ,FirstName
                   ,LastName
                   ,StudentNumber
                   ,MiddleName
                   ,ProgDescrip
                   ,StatusCodeDescrip
                   ,StudentExpectedGraduationDate
                   ,DateDetermined
                   ,InSchool
                   ,Address1
                   ,Address2
                   ,City
                   ,StateDescrip
                   ,CASE WHEN Phone1 IS NOT NULL THEN 'Phone: ' + dbo.UDF_FormatPhone(Phone1)
                         ELSE NULL
                    END AS Phone1
                   ,Phone2
                   ,Phone3
                   ,CASE WHEN Fax IS NOT NULL THEN 'Fax: ' + dbo.UDF_FormatPhone(Fax)
                         ELSE NULL
                    END AS Fax
                   ,Website
                   ,dbo.UDF_FormatZip(Zip) AS Zip
                   ,CampusId
                   ,ClockHourProgram
                   ,SysStatusId
                   ,CASE WHEN SysStatusId = 11 THEN CONVERT(CHAR(10),SuspensionStartDate,101) + ' to ' + CONVERT(CHAR(10),SuspensionEndDate,101)
                         WHEN SysStatusId = 20 THEN CONVERT(CHAR(10),BeginProbationDate,101)
                         WHEN SysStatusId = 10 THEN CONVERT(CHAR(10),BeginLeaveDate,101) + ' to ' + CONVERT(CHAR(10),EndLeaveDate,101)
                         ELSE NULL
                    END AS SuspensionRange
                   ,EnrollmentId
            FROM    (
                      SELECT DISTINCT
                                SE.StuEnrollId
                               ,PV.PrgVerDescrip
                               ,C.CampDescrip
                               ,C.CampusId
                               ,S.SSN
                               ,S.FirstName
                               ,S.LastName
                               ,S.MiddleName
                               ,S.StudentNumber
                               ,SC.StatusCodeDescrip
                               ,SE.ExpGradDate AS StudentExpectedGraduationDate
                               ,SE.DateDetermined AS DateDetermined
                               ,SS.InSchool
                               ,P.ProgDescrip
                               ,Address1
                               ,Address2
                               ,City
                               ,SY.StateDescrip
                               ,Phone1
                               ,Phone2
                               ,Phone3
                               ,Fax
                               ,Website
                               ,Zip
                               ,CASE WHEN P.ACId = 5 THEN 'True'
                                     ELSE 'False'
                                END AS ClockHourProgram
                               ,SS.SysStatusId
                               ,CASE WHEN SS.SysStatusId = 11 THEN (
                                                                     SELECT MAX(StartDate)
                                                                     FROM   arStdSuspensions
                                                                     WHERE  StuEnrollId = @StuEnrollId
                                                                   )
                                     ELSE NULL
                                END AS SuspensionStartDate
                               , --Suspended
                                CASE WHEN SS.SysStatusId = 11 THEN (
                                                                     SELECT MAX(EndDate)
                                                                     FROM   arStdSuspensions
                                                                     WHERE  StuEnrollId = @StuEnrollId
                                                                   )
                                     ELSE NULL
                                END AS SuspensionEndDate
                               , -- Suspended
                                CASE WHEN SS.SysStatusId = 20 THEN (
                                                                     SELECT MAX(StartDate)
                                                                     FROM   arStuProbWarnings SP
                                                                     WHERE  SP.StuEnrollId = @StuEnrollId
                                                                            AND SP.ProbWarningTypeId = 1
                                                                   )
                                     ELSE NULL
                                END AS BeginProbationDate
                               , -- Probation
                                CASE WHEN SS.SysStatusId = 10 THEN (
                                                                     SELECT MAX(StartDate)
                                                                     FROM   arStudentLOAs
                                                                     WHERE  StuEnrollId = @StuEnrollId
                                                                   )
                                     ELSE NULL
                                END AS BeginLeaveDate
                               , -- LOA
                                CASE WHEN SS.SysStatusId = 10 THEN (
                                                                     SELECT MAX(EndDate)
                                                                     FROM   arStudentLOAs
                                                                     WHERE  StuEnrollId = @StuEnrollId
                                                                   )
                                     ELSE NULL
                                END AS EndLeaveDate
                               ,SE.EnrollmentId   --LOA
                      FROM      arStuEnrollments SE
                      INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                      INNER JOIN (
                                   SELECT   StudentId
                                           ,FirstName
                                           ,LastName
                                           ,MiddleName
                                           ,SSN
                                           ,StudentNumber
                                   FROM     arStudent
                                 ) S ON S.StudentId = SE.StudentId
                      INNER JOIN syCmpGrpCmps CGC ON SE.CampusId = CGC.CampusId
                      INNER JOIN syStatusCodes SC ON SE.StatusCodeId = SC.StatusCodeId
                      INNER JOIN sySysStatus SS ON SS.SysStatusId = SC.SysStatusId
                      INNER JOIN arResults R ON SE.StuEnrollId = R.StuEnrollId
                      INNER JOIN arClassSections CS ON R.TestId = CS.ClsSectionId
                      INNER JOIN arTerm T ON CS.TermId = T.TermId
                      INNER JOIN syCampuses C ON C.CampusId = SE.CampusId
                      INNER JOIN arPrograms P ON P.ProgId = PV.ProgId
                      LEFT OUTER JOIN adLeadByLeadGroups LLG ON SE.StuEnrollId = LLG.StuEnrollId
                      LEFT OUTER JOIN syStates SY ON C.StateId = SY.StateId
                      WHERE     (
                                  @CampGrpId IS NULL
                                  OR CGC.CampGrpId IN ( SELECT  Val
                                                        FROM    MultipleValuesForReportParameters(@CampGrpId,',',1) )
                                )
                                AND (
                                      @StatusCodeId IS NULL
                                      OR SC.StatusCodeId IN ( SELECT    Val
                                                              FROM      MultipleValuesForReportParameters(@StatusCodeId,',',1) )
                                    )
                                AND (
                                      @PrgVerId IS NULL
                                      OR SE.PrgVerId IN ( SELECT    Val
                                                          FROM      MultipleValuesForReportParameters(@PrgVerId,',',1) )
                                    )
                                AND
					-- if A Then B is equivalent to (Not A) or B
                                (
                                  @StartDate IS NULL
                                  OR @StartDateModifier IS NULL
                                  OR (
                                       (
                                         ( @StartDateModifier <> '=' )
                                         OR ( T.StartDate = @StartDate )
                                       )
                                       AND (
                                             ( @StartDateModifier <> '>' )
                                             OR ( T.StartDate > @StartDate )
                                           )
                                       AND (
                                             ( @StartDateModifier <> '<' )
                                             OR ( T.StartDate < @StartDate )
                                           )
                                       AND (
                                             ( @StartDateModifier <> '>=' )
                                             OR ( T.StartDate >= @StartDate )
                                           )
                                       AND (
                                             ( @StartDateModifier <> '<=' )
                                             OR ( T.StartDate <= @StartDate )
                                           )
                                     )
                                )
                                AND (
                                      @ExpectedGradDate IS NULL
                                      OR @ExpectedGradDateModifier IS NULL
                                      OR (
                                           (
                                             ( @ExpectedGradDateModifier <> '=' )
                                             OR ( SE.ExpGradDate = @ExpectedGradDate )
                                           )
                                           AND (
                                                 ( @ExpectedGradDateModifier <> '>' )
                                                 OR ( SE.ExpGradDate > @ExpectedGradDate )
                                               )
                                           AND (
                                                 ( @ExpectedGradDateModifier <> '<' )
                                                 OR ( SE.ExpGradDate < @ExpectedGradDate )
                                               )
                                           AND (
                                                 ( @ExpectedGradDateModifier <> '>=' )
                                                 OR ( SE.ExpGradDate >= @ExpectedGradDate )
                                               )
                                           AND (
                                                 ( @ExpectedGradDateModifier <> '<=' )
                                                 OR ( SE.ExpGradDate <= @ExpectedGradDate )
                                               )
                                         )
                                    )
                                AND (
                                      @StuEnrollId IS NULL
                                      OR SE.StuEnrollId IN ( SELECT Val
                                                             FROM   MultipleValuesForReportParameters(@StuEnrollId,',',1) )
                                    )
                                AND (
                                      @TermId IS NULL
                                      OR T.TermId IN ( SELECT   Val
                                                       FROM     MultipleValuesForReportParameters(@TermId,',',1) )
                                    )
                                AND (
                                      @StudentGrpId IS NULL
                                      OR LLG.LeadGrpId IN ( SELECT  Val
                                                            FROM    MultipleValuesForReportParameters(@StudentGrpId,',',1) )
                                    )
                                AND (
                                      @CampusId IS NULL
                                      OR SE.CampusId IN ( SELECT    Val
                                                          FROM      MultipleValuesForReportParameters(@CampusId,',',1) )
                                    )
                      UNION
                      SELECT DISTINCT
                                SE.StuEnrollId
                               ,PV.PrgVerDescrip
                               ,C.CampDescrip
                               ,C.CampusId
                               ,S.SSN
                               ,S.FirstName
                               ,S.LastName
                               ,S.MiddleName
                               ,S.StudentNumber
                               ,SC.StatusCodeDescrip
                               ,SE.ExpGradDate AS StudentExpectedGraduationDate
                               ,SE.DateDetermined AS DateDetermined
                               ,SS.InSchool
                               ,P.ProgDescrip
                               ,Address1
                               ,Address2
                               ,City
                               ,SY.StateDescrip
                               ,Phone1
                               ,Phone2
                               ,Phone3
                               ,Fax
                               ,Website
                               ,Zip
                               ,CASE WHEN P.ACId = 5 THEN 'True'
                                     ELSE 'False'
                                END AS ClockHourProgram
                               ,SS.SysStatusId
                               ,CASE WHEN SS.SysStatusId = 11 THEN (
                                                                     SELECT MAX(StartDate)
                                                                     FROM   arStdSuspensions
                                                                     WHERE  StuEnrollId = @StuEnrollId
                                                                   )
                                     ELSE NULL
                                END AS SuspensionStartDate
                               , --Suspended
                                CASE WHEN SS.SysStatusId = 11 THEN (
                                                                     SELECT MAX(EndDate)
                                                                     FROM   arStdSuspensions
                                                                     WHERE  StuEnrollId = @StuEnrollId
                                                                   )
                                     ELSE NULL
                                END AS SuspensionEndDate
                               , -- Suspended
                                CASE WHEN SS.SysStatusId = 20 THEN (
                                                                     SELECT MAX(StartDate)
                                                                     FROM   arStuProbWarnings SP
                                                                     WHERE  SP.StuEnrollId = @StuEnrollId
                                                                            AND SP.ProbWarningTypeId = 1
                                                                   )
                                     ELSE NULL
                                END AS BeginProbationDate
                               , -- Probation
                                CASE WHEN SS.SysStatusId = 10 THEN (
                                                                     SELECT MAX(StartDate)
                                                                     FROM   arStudentLOAs
                                                                     WHERE  StuEnrollId = @StuEnrollId
                                                                   )
                                     ELSE NULL
                                END AS BeginLeaveDate
                               , -- LOA
                                CASE WHEN SS.SysStatusId = 10 THEN (
                                                                     SELECT MAX(EndDate)
                                                                     FROM   arStudentLOAs
                                                                     WHERE  StuEnrollId = @StuEnrollId
                                                                   )
                                     ELSE NULL
                                END AS EndLeaveDate
                               ,SE.EnrollmentId  --LOA
                      FROM      arStuEnrollments SE
                      INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                      INNER JOIN (
                                   SELECT   StudentId
                                           ,FirstName
                                           ,LastName
                                           ,MiddleName
                                           ,SSN
                                           ,StudentNumber
                                   FROM     arStudent
                                 ) S ON S.StudentId = SE.StudentId
                      INNER JOIN syCmpGrpCmps CGC ON SE.CampusId = CGC.CampusId
                      INNER JOIN syStatusCodes SC ON SE.StatusCodeId = SC.StatusCodeId
                      INNER JOIN sySysStatus SS ON SS.SysStatusId = SC.SysStatusId
                      INNER JOIN arTransferGrades R ON SE.StuEnrollId = R.StuEnrollId
                      INNER JOIN arTerm T ON R.TermId = T.TermId
                      INNER JOIN arReqs RQ ON RQ.ReqId = R.ReqId
                      INNER JOIN syCampuses C ON C.CampusId = SE.CampusId
                      INNER JOIN arPrograms P ON P.ProgId = PV.ProgId
                      LEFT OUTER JOIN adLeadByLeadGroups LLG ON SE.StuEnrollId = LLG.StuEnrollId
                      LEFT OUTER JOIN syStates SY ON C.StateId = SY.StateId
                      WHERE     (
                                  @CampGrpId IS NULL
                                  OR CGC.CampGrpId IN ( SELECT  Val
                                                        FROM    MultipleValuesForReportParameters(@CampGrpId,',',1) )
                                )
                                AND (
                                      @StatusCodeId IS NULL
                                      OR SC.StatusCodeId IN ( SELECT    Val
                                                              FROM      MultipleValuesForReportParameters(@StatusCodeId,',',1) )
                                    )
                                AND (
                                      @PrgVerId IS NULL
                                      OR SE.PrgVerId IN ( SELECT    Val
                                                          FROM      MultipleValuesForReportParameters(@PrgVerId,',',1) )
                                    )
                                AND
					-- if A Then B is equivalent to (Not A) or B
                                (
                                  @StartDate IS NULL
                                  OR @StartDateModifier IS NULL
                                  OR (
                                       (
                                         ( @StartDateModifier <> '=' )
                                         OR ( T.StartDate = @StartDate )
                                       )
                                       AND (
                                             ( @StartDateModifier <> '>' )
                                             OR ( T.StartDate > @StartDate )
                                           )
                                       AND (
                                             ( @StartDateModifier <> '<' )
                                             OR ( T.StartDate < @StartDate )
                                           )
                                       AND (
                                             ( @StartDateModifier <> '>=' )
                                             OR ( T.StartDate >= @StartDate )
                                           )
                                       AND (
                                             ( @StartDateModifier <> '<=' )
                                             OR ( T.StartDate <= @StartDate )
                                           )
                                     )
                                )
                                AND (
                                      @ExpectedGradDate IS NULL
                                      OR @ExpectedGradDateModifier IS NULL
                                      OR (
                                           (
                                             ( @ExpectedGradDateModifier <> '=' )
                                             OR ( SE.ExpGradDate = @ExpectedGradDate )
                                           )
                                           AND (
                                                 ( @ExpectedGradDateModifier <> '>' )
                                                 OR ( SE.ExpGradDate > @ExpectedGradDate )
                                               )
                                           AND (
                                                 ( @ExpectedGradDateModifier <> '<' )
                                                 OR ( SE.ExpGradDate < @ExpectedGradDate )
                                               )
                                           AND (
                                                 ( @ExpectedGradDateModifier <> '>=' )
                                                 OR ( SE.ExpGradDate >= @ExpectedGradDate )
                                               )
                                           AND (
                                                 ( @ExpectedGradDateModifier <> '<=' )
                                                 OR ( SE.ExpGradDate <= @ExpectedGradDate )
                                               )
                                         )
                                    )
                                AND (
                                      @StuEnrollId IS NULL
                                      OR SE.StuEnrollId IN ( SELECT Val
                                                             FROM   MultipleValuesForReportParameters(@StuEnrollId,',',1) )
                                    )
                                AND (
                                      @TermId IS NULL
                                      OR T.TermId IN ( SELECT   Val
                                                       FROM     MultipleValuesForReportParameters(@TermId,',',1) )
                                    )
                                AND (
                                      @StudentGrpId IS NULL
                                      OR LLG.LeadGrpId IN ( SELECT  Val
                                                            FROM    MultipleValuesForReportParameters(@StudentGrpId,',',1) )
                                    )
                                AND (
                                      @CampusId IS NULL
                                      OR SE.CampusId IN ( SELECT    Val
                                                          FROM      MultipleValuesForReportParameters(@CampusId,',',1) )
                                    )
                    ) dt
            ORDER BY dt.CampDescrip
                   ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'LastName Asc,FirstName Asc,MiddleName Asc'
                         THEN ( RANK() OVER ( ORDER BY LastName, FirstName, MiddleName ) )
                    END
                   ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'LastName Asc,FirstName Asc,MiddleName Desc'
                         THEN ( RANK() OVER ( ORDER BY LastName, FirstName, MiddleName DESC ) )
                    END
                   ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'LastName Asc,FirstName Desc,MiddleName Asc'
                         THEN ( RANK() OVER ( ORDER BY LastName, FirstName DESC, MiddleName ) )
                    END
                   ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'LastName Asc,FirstName Desc,MiddleName Desc'
                         THEN ( RANK() OVER ( ORDER BY LastName, FirstName DESC, MiddleName DESC ) )
                    END
                   ,
			 --LD
                    CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'LastName Desc,FirstName Asc,MiddleName Asc'
                         THEN ( RANK() OVER ( ORDER BY LastName DESC, FirstName, MiddleName ) )
                    END
                   ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'LastName Desc,FirstName Asc,MiddleName Desc'
                         THEN ( RANK() OVER ( ORDER BY LastName DESC, FirstName, MiddleName DESC ) )
                    END
                   ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'LastName Desc,FirstName Desc,MiddleName Asc'
                         THEN ( RANK() OVER ( ORDER BY LastName DESC, FirstName DESC, MiddleName ) )
                    END
                   ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'LastName Desc,FirstName Desc,MiddleName Desc'
                         THEN ( RANK() OVER ( ORDER BY LastName DESC, FirstName DESC, MiddleName DESC ) )
                    END
                   ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'LastName Asc,MiddleName Asc,FirstName Asc'
                         THEN ( RANK() OVER ( ORDER BY LastName ASC, MiddleName ASC, FirstName ASC ) )
                    END
                   ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'LastName Asc,MiddleName Asc,FirstName Desc'
                         THEN ( RANK() OVER ( ORDER BY LastName ASC, MiddleName ASC, FirstName DESC ) )
                    END
                   ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'LastName Asc,MiddleName Desc,FirstName Asc'
                         THEN ( RANK() OVER ( ORDER BY LastName ASC, MiddleName DESC, FirstName ASC ) )
                    END
                   ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'LastName Asc,MiddleName Desc,FirstName Desc'
                         THEN ( RANK() OVER ( ORDER BY LastName ASC, MiddleName DESC, FirstName DESC ) )
                    END
                   ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'LastName Desc,MiddleName Asc,FirstName Asc'
                         THEN ( RANK() OVER ( ORDER BY LastName DESC, MiddleName ASC, FirstName ASC ) )
                    END
                   ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'LastName Desc,MiddleName Asc,FirstName Desc'
                         THEN ( RANK() OVER ( ORDER BY LastName DESC, MiddleName ASC, FirstName DESC ) )
                    END
                   ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'LastName Desc,MiddleName Desc,FirstName Asc'
                         THEN ( RANK() OVER ( ORDER BY LastName DESC, MiddleName DESC, FirstName ASC ) )
                    END
                   ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'LastName Desc,MiddleName Desc,FirstName Desc'
                         THEN ( RANK() OVER ( ORDER BY LastName DESC, MiddleName DESC, FirstName DESC ) )
                    END
                   ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'FirstName Asc,LastName Asc,MiddleName Asc'
                         THEN ( RANK() OVER ( ORDER BY FirstName ASC, LastName ASC, MiddleName ASC ) )
                    END
                   ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'FirstName Asc,LastName Asc,MiddleName Desc'
                         THEN ( RANK() OVER ( ORDER BY FirstName ASC, LastName ASC, MiddleName DESC ) )
                    END
                   ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'FirstName Asc,LastName Desc,MiddleName Asc'
                         THEN ( RANK() OVER ( ORDER BY FirstName ASC, LastName DESC, MiddleName ASC ) )
                    END
                   ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'FirstName Asc,LastName Desc,MiddleName Desc'
                         THEN ( RANK() OVER ( ORDER BY FirstName ASC, LastName DESC, MiddleName DESC ) )
                    END
                   ,
			 --LD
                    CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'FirstName Desc,LastName Asc,MiddleName Asc'
                         THEN ( RANK() OVER ( ORDER BY FirstName DESC, LastName ASC, MiddleName ASC ) )
                    END
                   ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'FirstName Desc,LastName Asc,MiddleName Desc'
                         THEN ( RANK() OVER ( ORDER BY FirstName DESC, LastName ASC, MiddleName DESC ) )
                    END
                   ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'FirstName Desc,LastName Desc,MiddleName Asc'
                         THEN ( RANK() OVER ( ORDER BY FirstName DESC, LastName DESC, MiddleName ASC ) )
                    END
                   ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'FirstName Desc,LastName Desc,MiddleName Desc'
                         THEN ( RANK() OVER ( ORDER BY FirstName DESC, LastName DESC, MiddleName DESC ) )
                    END
                   ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'FirstName Asc,MiddleName Asc,LastName Asc'
                         THEN ( RANK() OVER ( ORDER BY FirstName ASC, MiddleName ASC, LastName ASC ) )
                    END
                   ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'FirstName Asc,MiddleName Asc,LastName Desc'
                         THEN ( RANK() OVER ( ORDER BY FirstName ASC, MiddleName ASC, LastName DESC ) )
                    END
                   ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'FirstName Asc,MiddleName Desc,LastName Asc'
                         THEN ( RANK() OVER ( ORDER BY FirstName ASC, MiddleName DESC, LastName ASC ) )
                    END
                   ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'FirstName Asc,MiddleName Desc,LastName Desc'
                         THEN ( RANK() OVER ( ORDER BY FirstName ASC, MiddleName DESC, LastName DESC ) )
                    END
                   ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'FirstName Desc,MiddleName Asc,LastName Asc'
                         THEN ( RANK() OVER ( ORDER BY FirstName DESC, MiddleName ASC, LastName ASC ) )
                    END
                   ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'FirstName Desc,MiddleName Asc,LastName Desc'
                         THEN ( RANK() OVER ( ORDER BY FirstName DESC, MiddleName ASC, LastName DESC ) )
                    END
                   ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'FirstName Desc,MiddleName Desc,LastName Asc'
                         THEN ( RANK() OVER ( ORDER BY FirstName DESC, MiddleName DESC, LastName ASC ) )
                    END
                   ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'FirstName Desc,MiddleName Desc,LastName Desc'
                         THEN ( RANK() OVER ( ORDER BY FirstName DESC, MiddleName DESC, LastName DESC ) )
                    END
                   ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'MiddleName Asc,LastName Asc,FirstName Asc'
                         THEN ( RANK() OVER ( ORDER BY MiddleName ASC, LastName ASC, FirstName ASC ) )
                    END
                   ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'MiddleName Asc,LastName Asc,FirstName Desc'
                         THEN ( RANK() OVER ( ORDER BY MiddleName ASC, LastName ASC, FirstName DESC ) )
                    END
                   ,
			 
			 -- Start here
                    CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'MiddleName Asc,LastName Desc,FirstName Asc'
                         THEN ( RANK() OVER ( ORDER BY MiddleName ASC, LastName DESC, FirstName ASC ) )
                    END
                   ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'MiddleName Asc,LastName Desc,FirstName Desc'
                         THEN ( RANK() OVER ( ORDER BY MiddleName ASC, LastName DESC, FirstName DESC ) )
                    END
                   ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'MiddleName Desc,LastName Asc,FirstName Asc'
                         THEN ( RANK() OVER ( ORDER BY MiddleName DESC, LastName ASC, FirstName ASC ) )
                    END
                   ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'MiddleName Desc,LastName Asc,FirstName Desc'
                         THEN ( RANK() OVER ( ORDER BY MiddleName DESC, LastName ASC, FirstName DESC ) )
                    END
                   ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'MiddleName Desc,LastName Desc,FirstName Asc'
                         THEN ( RANK() OVER ( ORDER BY MiddleName DESC, LastName DESC, FirstName ASC ) )
                    END
                   ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'MiddleName Desc,LastName Desc,FirstName Desc'
                         THEN ( RANK() OVER ( ORDER BY MiddleName DESC, LastName DESC, FirstName DESC ) )
                    END
                   ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'MiddleName Asc,FirstName Asc,LastName Asc'
                         THEN ( RANK() OVER ( ORDER BY MiddleName ASC, FirstName ASC, LastName ASC ) )
                    END
                   ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'MiddleName Asc,FirstName Asc,LastName Desc'
                         THEN ( RANK() OVER ( ORDER BY MiddleName ASC, FirstName ASC, LastName DESC ) )
                    END
                   ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'MiddleName Asc,FirstName Desc,LastName Asc'
                         THEN ( RANK() OVER ( ORDER BY MiddleName ASC, FirstName DESC, LastName ASC ) )
                    END
                   ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'MiddleName Asc,FirstName Desc,LastName Desc'
                         THEN ( RANK() OVER ( ORDER BY MiddleName ASC, FirstName DESC, LastName DESC ) )
                    END
                   ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'MiddleName Desc,FirstName Asc,LastName Asc'
                         THEN ( RANK() OVER ( ORDER BY MiddleName DESC, FirstName ASC, LastName ASC ) )
                    END
                   ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'MiddleName Desc,FirstName Asc,LastName Desc'
                         THEN ( RANK() OVER ( ORDER BY MiddleName DESC, FirstName ASC, LastName DESC ) )
                    END
                   ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'MiddleName Desc,FirstName Desc,LastName Asc'
                         THEN ( RANK() OVER ( ORDER BY MiddleName DESC, FirstName DESC, LastName ASC ) )
                    END
                   ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'MiddleName Desc,FirstName Desc,LastName Desc'
                         THEN ( RANK() OVER ( ORDER BY MiddleName DESC, FirstName DESC, LastName DESC ) )
                    END;
        END;


GO
