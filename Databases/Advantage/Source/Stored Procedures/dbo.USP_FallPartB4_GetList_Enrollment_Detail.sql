SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[USP_FallPartB4_GetList_Enrollment_Detail]
    @CampusId VARCHAR(50)
   ,@ProgId VARCHAR(4000) = NULL
   ,@StartDate DATETIME = NULL
   ,@EndDate DATETIME = NULL
   ,@DateRangeText VARCHAR(100) = NULL
   ,@OrderBy VARCHAR(100)
AS
    DECLARE @ReturnValue VARCHAR(100);
    DECLARE @ContinuingStartDate DATETIME;
--declare @Value varchar(50)
-- commented the @value setting - not used anywhere Oct 10 2012
--set @Value=(SELECT dbo.GetAppSettingValue(47, @CampusId))
    SET @ContinuingStartDate = @StartDate;
-- For Academic Year Reporter, the End Date should be 10/15/Cohort Year
-- and Start Date should be blank
    IF DAY(@EndDate) = 15
        AND MONTH(@EndDate) = 10
        BEGIN
            SET @EndDate = @StartDate; -- it will always be 10/15/cohort year example: for cohort year 2009, it is 10/15/2009
            SET @StartDate = @StartDate;
        END;

    IF @ProgId IS NOT NULL
        BEGIN
            SELECT  @ReturnValue = COALESCE(@ReturnValue,'') + ProgDescrip + ','
            FROM    arPrograms t1
            WHERE   t1.ProgId IN ( SELECT   Val
                                   FROM     MultipleValuesForReportParameters(@ProgId,',',1) );
            SET @ReturnValue = SUBSTRING(@ReturnValue,1,LEN(@ReturnValue) - 1);
        END;
    ELSE
        BEGIN
            SELECT  @ReturnValue = COALESCE(@ReturnValue,'') + ProgDescrip + ','
            FROM    arPrograms t1
            WHERE   t1.CampGrpId IN ( SELECT    t1.CampGrpId
                                      FROM      syCmpGrpCmps
                                      WHERE     CampusId = @CampusId );
            SET @ReturnValue = SUBSTRING(@ReturnValue,1,LEN(@ReturnValue) - 1);
        END;

    CREATE TABLE #EnrollmentDetail
        (
         RowNumber UNIQUEIDENTIFIER
        ,SSN VARCHAR(50)
        ,StudentNumber VARCHAR(50)
        ,StudentName VARCHAR(100)
        ,Gender VARCHAR(50)
        ,Race VARCHAR(50)
        ,UnderGraduate VARCHAR(1)
        ,Graduate VARCHAR(1)
        ,UnderGraduateCount INT
        ,GraduateCount INT
        ,GenderSequence INT
        ,RaceSequence INT
        ,StudentId UNIQUEIDENTIFIER
        );

    INSERT  INTO #EnrollmentDetail
            SELECT  NEWID() AS RowNumber
                   ,dbo.UDF_FormatSSN(SSN) AS SSN
                   ,StudentNumber
                   ,StudentName
                   ,Gender
                   ,Race
                   ,UnderGraduate
                   ,Graduate
                   ,UnderGraduateCount
                   ,GraduateCount
                   ,GenderSequence
                   ,RaceSequence
                   ,StudentId
            FROM    (
                      -- Check if there are any Foreign Male Students who are Full-Time Undergraduates
		-- If there are no foreign students in that category, insert a blank record
		-- with race 'Nonresident Alien'
		-- This race does not exist in adEthCodes anymore 
		-- Needs to be deleted from adEthCodes
                      SELECT TOP 1
                                NULL AS SSN
                               ,NULL AS StudentNumber
                               ,NULL AS StudentName
                               ,'Men' AS Gender
                               ,'Nonresident Alien' AS Race
                               ,NULL AS UnderGraduate
                               ,NULL AS Graduate
                               ,NULL AS UnderGraduateCount
                               ,NULL AS GraduateCount
                               ,1 AS GenderSequence
                               ,1 AS RaceSequence
                               ,StudentId
                      FROM      arStudent
                      WHERE     (
                                  SELECT    COUNT(t1.FirstName)
                                  FROM      adGenders t3
                                  LEFT JOIN arStudent t1 ON t3.GenderId = t1.Gender
                                  INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                                  INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                                  INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                                               AND t6.SysStatusId NOT IN ( 8 )
                                  INNER JOIN adCitizenships t11 ON t1.Citizen = t11.CitizenshipId
                                  INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                                  INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                                  INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                                  LEFT JOIN arAttendTypes t10 ON t2.AttendTypeId = t10.AttendTypeId
                                  WHERE     LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                                            AND t11.IPEDSValue = 65
                                            AND -- non citizen
                                            (
                                              t9.IPEDSValue = 58
                                              OR t9.IPEDSValue = 59
                                            )
                                            AND -- UnderGraduate or Graduate
                                            (
                                              t10.IPEDSValue = 61
                                              OR t10.IPEDSValue = 62
                                            )
                                            AND -- FullTime or Part Time
                                            t3.IPEDSValue = 30
                                            AND --Men
                                            (
                                              @ProgId IS NULL
                                              OR t8.ProgId IN ( SELECT  Val
                                                                FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                            ) 
						--and (@StartDate is null or t2.StartDate>=@StartDate) and (@EndDate is null or t2.StartDate<=@EndDate)
                                            AND t2.StartDate <= @EndDate
                                            AND StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
						( SELECT    t1.StuEnrollId
                          FROM      arStuEnrollments t1
                                   ,syStatusCodes t2
                          WHERE     t1.StatusCodeId = t2.StatusCodeId
                                    AND StartDate <= @EndDate
                                    AND -- Student started before the end date range
                                    LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                    AND (
                                          @ProgId IS NULL
                                          OR t8.ProgId IN ( SELECT  Val
                                                            FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                        )
                                    AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                    AND (
                                          t1.DateDetermined < @StartDate
                                          OR ExpGradDate < @StartDate
                                          OR LDA < @StartDate
                                        ) )
						-- DE8492
						-- If Student is enrolled in only program version and if that program version 
						-- happens to be a continuing ed program exclude the student
                                            AND t2.StudentId NOT IN ( SELECT DISTINCT
                                                                                StudentId
                                                                      FROM      (
                                                                                  SELECT    StudentId
                                                                                           ,COUNT(*) AS RowCounter
                                                                                  FROM      arStuEnrollments se1
                                                                                  WHERE     PrgVerId IN ( SELECT    PrgVerId
                                                                                                          FROM      arPrgVersions
                                                                                                          WHERE     IsContinuingEd = 1 )
                                                                                            AND (
                                                                                                  SELECT    COUNT(*)
                                                                                                  FROM      dbo.arStuEnrollments se2
                                                                                                  WHERE     se2.StudentId = se1.StudentId
                                                                                                ) = 1
                                                                                  GROUP BY  StudentId
                                                                                  HAVING    COUNT(*) > 0
                                                                                ) dtStudent_ContinuingEd )
                                ) = 0
                      UNION
			-- Check if there are any Male Foreign Students who are Full-Time Undergraduates
			-- If there are no foreign students in that category, insert a blank record
			-- with race 'Nonresident Alien'
                      SELECT    t1.SSN
                               ,t1.StudentNumber
                               ,t1.LastName + ', ' + t1.FirstName + ' ' + ISNULL(t1.MiddleName,'') AS StudentName
                               ,(
                                  SELECT DISTINCT
                                            AgencyDescrip
                                  FROM      syRptAgencyFldValues
                                  WHERE     RptAgencyFldValId = t3.IPEDSValue
                                ) AS Gender
                               ,'Nonresident Alien' AS Race
                               ,CASE WHEN ( t9.IPEDSValue = 58 ) THEN 'X'
                                     ELSE ''
                                END AS UnderGraduate
                               ,CASE WHEN (
                                            t9.IPEDSValue = 59
                                            OR t9.IPEDSValue = 60
                                          ) THEN 'X'
                                     ELSE ''
                                END AS Graduate
                               ,CASE WHEN ( t9.IPEDSValue = 58 ) THEN 1
                                     ELSE 0
                                END AS UnderGraduateCount
                               ,CASE WHEN (
                                            t9.IPEDSValue = 59
                                            OR t9.IPEDSValue = 60
                                          ) THEN 1
                                     ELSE 0
                                END AS GraduateCount
                               ,t3.IPEDSSequence AS GenderSequence
                               ,1 AS RaceSequence
                               ,t1.StudentId
                      FROM      adGenders t3
                      LEFT JOIN arStudent t1 ON t3.GenderId = t1.Gender
                      LEFT JOIN adEthCodes t4 ON t4.EthCodeId = t1.Race
                      INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                      INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                      INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                                   AND t6.SysStatusId NOT IN ( 8 )
                      INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                      INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                      INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                      LEFT JOIN arAttendTypes t10 ON t2.AttendTypeId = t10.AttendTypeId
                      INNER JOIN adCitizenships t11 ON t1.Citizen = t11.CitizenshipId
                      WHERE     LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                                AND (
                                      @ProgId IS NULL
                                      OR t8.ProgId IN ( SELECT  Val
                                                        FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                    )
                                AND (
                                      t9.IPEDSValue = 58
                                      OR t9.IPEDSValue = 59
                                      OR t9.IPEDSValue = 60
                                    )
                                AND -- graduate or first professional or UnderGraduate
                                (
                                  t10.IPEDSValue = 61
                                  OR t10.IPEDSValue = 62
                                )
                                AND -- Full Time or Part Time
                                t3.IPEDSValue = 30
                                AND -- Men
				--t1.Race is not null	 
                                t11.IPEDSValue = 65 -- Non-Citizen
				--and (@StartDate is null or t2.StartDate>=@StartDate) and (@EndDate is null or t2.StartDate<=@EndDate)
                                AND t2.StartDate <= @EndDate
                                AND StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
						( SELECT    t1.StuEnrollId
                          FROM      arStuEnrollments t1
                                   ,syStatusCodes t2
                          WHERE     t1.StatusCodeId = t2.StatusCodeId
                                    AND StartDate <= @EndDate
                                    AND -- Student started before the end date range
                                    LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                    AND (
                                          @ProgId IS NULL
                                          OR t8.ProgId IN ( SELECT  Val
                                                            FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                        )
                                    AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                    AND (
                                          t1.DateDetermined < @StartDate
                                          OR ExpGradDate < @StartDate
                                          OR LDA < @StartDate
                                        ) )
				-- DE8492
				-- If Student is enrolled in only program version and if that program version 
				-- happens to be a continuing ed program exclude the student
                                AND t2.StudentId NOT IN ( SELECT DISTINCT
                                                                    StudentId
                                                          FROM      (
                                                                      SELECT    StudentId
                                                                               ,COUNT(*) AS RowCounter
                                                                      FROM      arStuEnrollments se1
                                                                      WHERE     PrgVerId IN ( SELECT    PrgVerId
                                                                                              FROM      arPrgVersions
                                                                                              WHERE     IsContinuingEd = 1 )
                                                                                AND (
                                                                                      SELECT    COUNT(*)
                                                                                      FROM      dbo.arStuEnrollments se2
                                                                                      WHERE     se2.StudentId = se1.StudentId
                                                                                    ) = 1
                                                                      GROUP BY  StudentId
                                                                      HAVING    COUNT(*) > 0
                                                                    ) dtStudent_ContinuingEd )
                      UNION				
		-- Check if there are any US Students who are Full-Time Undergraduates
		-- If there are no US Citizens in that category, insert a blank record
		-- for corresponding races
		-- Ignore NonResident Alien
                      SELECT    NULL AS SSN
                               ,NULL AS StudentNumber
                               ,NULL AS StudentName
                               ,'Men' AS Gender
                               ,(
                                  SELECT DISTINCT
                                            AgencyDescrip
                                  FROM      syRptAgencyFldValues
                                  WHERE     RptAgencyFldValId = IPEDSValue
                                ) AS Race
                               ,NULL AS UnderGraduate
                               ,NULL AS Graduate
                               ,NULL AS UnderGraduateCount
                               ,NULL AS GraduateCount
                               ,1 AS GenderSequence
                               ,IPEDSSequence AS RaceSequence
                               ,NULL AS StudentId
                      FROM      adEthCodes
                      WHERE     EthCodeDescrip <> 'Nonresident Alien'
                                AND EthCodeId NOT IN ( SELECT DISTINCT
                                                                t1.Race
                                                       FROM     arStudent t1
                                                       INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                                                       INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                                                       INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                                                                    AND t6.SysStatusId NOT IN ( 8 )
                                                       INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                                                       INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                                                       INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                                                       LEFT JOIN arAttendTypes t10 ON t2.AttendTypeId = t10.AttendTypeId
                                                       INNER JOIN adGenders t11 ON t1.Gender = t11.GenderId
                                                       INNER JOIN adCitizenships t12 ON t1.Citizen = t12.CitizenshipId
                                                       WHERE    LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                                                                AND (
                                                                      @ProgId IS NULL
                                                                      OR t8.ProgId IN ( SELECT  Val
                                                                                        FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                                                    )
                                                                AND (
                                                                      t9.IPEDSValue = 58
                                                                      OR t9.IPEDSValue = 59
                                                                      OR t9.IPEDSValue = 60
                                                                    )
                                                                AND -- UnderGraduate or Graduate
                                                                (
                                                                  t10.IPEDSValue = 61
                                                                  OR t10.IPEDSValue = 62
                                                                )
                                                                AND -- Full Time or Part Time
                                                                t11.IPEDSValue = 30
                                                                AND t1.Race IS NOT NULL
                                                                AND t12.IPEDSValue <> 65 -- Ignore Non US Citizens (Foreign Students)
								--and (@StartDate is null or t2.StartDate>=@StartDate) and (@EndDate is null or t2.StartDate<=@EndDate)
                                                                AND t2.StartDate <= @EndDate
                                                                AND StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
								-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
								( SELECT    t1.StuEnrollId
                                  FROM      arStuEnrollments t1
                                           ,syStatusCodes t2
                                  WHERE     t1.StatusCodeId = t2.StatusCodeId
                                            AND StartDate <= @EndDate
                                            AND -- Student started before the end date range
                                            LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                            AND (
                                                  @ProgId IS NULL
                                                  OR t8.ProgId IN ( SELECT  Val
                                                                    FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                                )
                                            AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
									-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                            AND (
                                                  t1.DateDetermined < @StartDate
                                                  OR ExpGradDate < @StartDate
                                                  OR LDA < @StartDate
                                                ) )
								-- DE8492
								-- If Student is enrolled in only program version and if that program version 
								-- happens to be a continuing ed program exclude the student
                                                                AND t2.StudentId NOT IN ( SELECT DISTINCT
                                                                                                    StudentId
                                                                                          FROM      (
                                                                                                      SELECT    StudentId
                                                                                                               ,COUNT(*) AS RowCounter
                                                                                                      FROM      arStuEnrollments se1
                                                                                                      WHERE     PrgVerId IN ( SELECT    PrgVerId
                                                                                                                              FROM      arPrgVersions
                                                                                                                              WHERE     IsContinuingEd = 1 )
                                                                                                                AND (
                                                                                                                      SELECT    COUNT(*)
                                                                                                                      FROM      dbo.arStuEnrollments se2
                                                                                                                      WHERE     se2.StudentId = se1.StudentId
                                                                                                                    ) = 1
                                                                                                      GROUP BY  StudentId
                                                                                                      HAVING    COUNT(*) > 0
                                                                                                    ) dtStudent_ContinuingEd ) )
                      UNION
                      SELECT    t1.SSN
                               ,t1.StudentNumber
                               ,t1.LastName + ', ' + t1.FirstName + ' ' + ISNULL(t1.MiddleName,'') AS StudentName
                               ,(
                                  SELECT DISTINCT
                                            AgencyDescrip
                                  FROM      syRptAgencyFldValues
                                  WHERE     RptAgencyFldValId = t3.IPEDSValue
                                ) AS Gender
                               ,CASE WHEN t4.IPEDSValue IS NULL THEN 'Race/ethnicity unknown'
                                     ELSE (
                                            SELECT DISTINCT
                                                    AgencyDescrip
                                            FROM    syRptAgencyFldValues
                                            WHERE   RptAgencyFldValId = t4.IPEDSValue
                                          )
                                END AS Race
                               ,CASE WHEN ( t9.IPEDSValue = 58 ) THEN 'X'
                                     ELSE ''
                                END AS UnderGraduate
                               ,CASE WHEN (
                                            t9.IPEDSValue = 59
                                            OR t9.IPEDSValue = 60
                                          ) THEN 'X'
                                     ELSE ''
                                END AS Graduate
                               ,CASE WHEN ( t9.IPEDSValue = 58 ) THEN 1
                                     ELSE 0
                                END AS UnderGraduateCount
                               ,CASE WHEN (
                                            t9.IPEDSValue = 59
                                            OR t9.IPEDSValue = 60
                                          ) THEN 1
                                     ELSE 0
                                END AS GraduateCount
                               ,t3.IPEDSSequence AS GenderSequence
                               ,CASE WHEN t4.IPEDSValue IS NULL THEN 9
                                     ELSE t4.IPEDSSequence
                                END AS RaceSequence
                               ,t1.StudentId
                      FROM      adGenders t3
                      LEFT JOIN arStudent t1 ON t3.GenderId = t1.Gender
                      LEFT JOIN adEthCodes t4 ON t4.EthCodeId = t1.Race
                      INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                      INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                      INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                                   AND t6.SysStatusId NOT IN ( 8 )
                      INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                      INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                      INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                      LEFT JOIN arAttendTypes t10 ON t2.AttendTypeId = t10.AttendTypeId
                      INNER JOIN adCitizenships t12 ON t1.Citizen = t12.CitizenshipId
                      WHERE     t2.CampusId = LTRIM(RTRIM(@CampusId))
                                AND (
                                      @ProgId IS NULL
                                      OR t8.ProgId IN ( SELECT  Val
                                                        FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                    )
                                AND (
                                      t9.IPEDSValue = 58
                                      OR t9.IPEDSValue = 59
                                      OR t9.IPEDSValue = 60
                                    )
                                AND -- graduate or first professional or UnderGraduate
                                (
                                  t10.IPEDSValue = 61
                                  OR t10.IPEDSValue = 62
                                )
                                AND -- Full Time or Part Time
                                t3.IPEDSValue = 30
                                AND t1.Race IS NOT NULL
                                AND (
                                      t12.IPEDSValue <> 65
                                      OR t12.IPEDSValue IS NULL
                                    ) -- Ignore Non US Citizens (Foreign Students)
				--and (@StartDate is null or t2.StartDate>=@StartDate) and (@EndDate is null or t2.StartDate<=@EndDate) 
                                AND t2.StartDate <= @EndDate
                                AND StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
						( SELECT    t1.StuEnrollId
                          FROM      arStuEnrollments t1
                                   ,syStatusCodes t2
                          WHERE     t1.StatusCodeId = t2.StatusCodeId
                                    AND StartDate <= @EndDate
                                    AND -- Student started before the end date range
                                    LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                    AND (
                                          @ProgId IS NULL
                                          OR t8.ProgId IN ( SELECT  Val
                                                            FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                        )
                                    AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                    AND (
                                          t1.DateDetermined < @StartDate
                                          OR ExpGradDate < @StartDate
                                          OR LDA < @StartDate
                                        ) )
				-- DE8492
				-- If Student is enrolled in only program version and if that program version 
				-- happens to be a continuing ed program exclude the student
                                AND t2.StudentId NOT IN ( SELECT DISTINCT
                                                                    StudentId
                                                          FROM      (
                                                                      SELECT    StudentId
                                                                               ,COUNT(*) AS RowCounter
                                                                      FROM      arStuEnrollments se1
                                                                      WHERE     PrgVerId IN ( SELECT    PrgVerId
                                                                                              FROM      arPrgVersions
                                                                                              WHERE     IsContinuingEd = 1 )
                                                                                AND (
                                                                                      SELECT    COUNT(*)
                                                                                      FROM      dbo.arStuEnrollments se2
                                                                                      WHERE     se2.StudentId = se1.StudentId
                                                                                    ) = 1
                                                                      GROUP BY  StudentId
                                                                      HAVING    COUNT(*) > 0
                                                                    ) dtStudent_ContinuingEd )
                      UNION
		-- Check if there are any Foreign Male Students who are Full-Time Undergraduates
		-- If there are no foreign students in that category, insert a blank record
		-- with race 'Nonresident Alien'
		-- This race does not exist in adEthCodes anymore 
		-- Needs to be deleted from adEthCodes
                      SELECT TOP 1
                                NULL AS SSN
                               ,NULL AS StudentNumber
                               ,NULL AS StudentName
                               ,'Women' AS Gender
                               ,'Nonresident Alien' AS Race
                               ,NULL AS UnderGraduate
                               ,NULL AS Graduate
                               ,NULL AS UnderGraduateCount
                               ,NULL AS GraduateCount
                               ,2 AS GenderSequence
                               ,1 AS RaceSequence
                               ,NULL AS StudentId
                      FROM      arStudent
                      WHERE     (
                                  SELECT    COUNT(t1.FirstName)
                                  FROM      adGenders t3
                                  LEFT JOIN arStudent t1 ON t3.GenderId = t1.Gender
                                  INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                                  INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                                  INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                                               AND t6.SysStatusId NOT IN ( 8 )
                                  INNER JOIN adCitizenships t11 ON t1.Citizen = t11.CitizenshipId
                                  INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                                  INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                                  INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                                  LEFT JOIN arAttendTypes t10 ON t2.AttendTypeId = t10.AttendTypeId
                                  WHERE     LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                                            AND t11.IPEDSValue = 65
                                            AND -- non citizen
                                            (
                                              t9.IPEDSValue = 58
                                              OR t9.IPEDSValue = 59
                                            )
                                            AND -- UnderGraduate or Graduate
                                            (
                                              t10.IPEDSValue = 61
                                              OR t10.IPEDSValue = 62
                                            )
                                            AND -- FullTime or Part Time
                                            t3.IPEDSValue = 31
                                            AND --Women
                                            (
                                              @ProgId IS NULL
                                              OR t8.ProgId IN ( SELECT  Val
                                                                FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                            ) 
						--and (@StartDate is null or t2.StartDate>=@StartDate) and (@EndDate is null or t2.StartDate<=@EndDate) 
                                            AND t2.StartDate <= @EndDate
                                            AND StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
						( SELECT    t1.StuEnrollId
                          FROM      arStuEnrollments t1
                                   ,syStatusCodes t2
                          WHERE     t1.StatusCodeId = t2.StatusCodeId
                                    AND StartDate <= @EndDate
                                    AND -- Student started before the end date range
                                    LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                    AND (
                                          @ProgId IS NULL
                                          OR t8.ProgId IN ( SELECT  Val
                                                            FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                        )
                                    AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                    AND (
                                          t1.DateDetermined < @StartDate
                                          OR ExpGradDate < @StartDate
                                          OR LDA < @StartDate
                                        ) )
					-- DE8492
					-- If Student is enrolled in only program version and if that program version 
					-- happens to be a continuing ed program exclude the student
                                            AND t2.StudentId NOT IN ( SELECT DISTINCT
                                                                                StudentId
                                                                      FROM      (
                                                                                  SELECT    StudentId
                                                                                           ,COUNT(*) AS RowCounter
                                                                                  FROM      arStuEnrollments se1
                                                                                  WHERE     PrgVerId IN ( SELECT    PrgVerId
                                                                                                          FROM      arPrgVersions
                                                                                                          WHERE     IsContinuingEd = 1 )
                                                                                            AND (
                                                                                                  SELECT    COUNT(*)
                                                                                                  FROM      dbo.arStuEnrollments se2
                                                                                                  WHERE     se2.StudentId = se1.StudentId
                                                                                                ) = 1
                                                                                  GROUP BY  StudentId
                                                                                  HAVING    COUNT(*) > 0
                                                                                ) dtStudent_ContinuingEd )
                                ) = 0
                      UNION
			-- Check if there are any Foreign Female Students who are Full-Time Undergraduates
			-- If there are no foreign students in that category, insert a blank record
			-- with race 'Nonresident Alien'
                      SELECT    t1.SSN
                               ,t1.StudentNumber
                               ,t1.LastName + ', ' + t1.FirstName + ' ' + ISNULL(t1.MiddleName,'') AS StudentName
                               ,(
                                  SELECT DISTINCT
                                            AgencyDescrip
                                  FROM      syRptAgencyFldValues
                                  WHERE     RptAgencyFldValId = t3.IPEDSValue
                                ) AS Gender
                               ,'Nonresident Alien' AS Race
                               ,CASE WHEN ( t9.IPEDSValue = 58 ) THEN 'X'
                                     ELSE ''
                                END AS UnderGraduate
                               ,CASE WHEN (
                                            t9.IPEDSValue = 59
                                            OR t9.IPEDSValue = 60
                                          ) THEN 'X'
                                     ELSE ''
                                END AS Graduate
                               ,CASE WHEN ( t9.IPEDSValue = 58 ) THEN 1
                                     ELSE 0
                                END AS UnderGraduateCount
                               ,CASE WHEN (
                                            t9.IPEDSValue = 59
                                            OR t9.IPEDSValue = 60
                                          ) THEN 1
                                     ELSE 0
                                END AS GraduateCount
                               ,t3.IPEDSSequence AS GenderSequence
                               ,1 AS RaceSequence
                               ,t1.StudentId
                      FROM      adGenders t3
                      LEFT JOIN arStudent t1 ON t3.GenderId = t1.Gender
                      LEFT JOIN adEthCodes t4 ON t4.EthCodeId = t1.Race
                      INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                      INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                      INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                                   AND t6.SysStatusId NOT IN ( 8 )
                      INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                      INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                      INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                      LEFT JOIN arAttendTypes t10 ON t2.AttendTypeId = t10.AttendTypeId
                      INNER JOIN adCitizenships t11 ON t1.Citizen = t11.CitizenshipId
                      WHERE     LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                                AND (
                                      @ProgId IS NULL
                                      OR t8.ProgId IN ( SELECT  Val
                                                        FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                    )
                                AND (
                                      t9.IPEDSValue = 58
                                      OR t9.IPEDSValue = 59
                                      OR t9.IPEDSValue = 60
                                    )
                                AND -- graduate or first professional or UnderGraduate
                                (
                                  t10.IPEDSValue = 61
                                  OR t10.IPEDSValue = 62
                                )
                                AND -- Full Time or Part Time
                                t3.IPEDSValue = 31
                                AND -- Women
				--t1.Race is not null	 
                                t11.IPEDSValue = 65 -- Non-Citizen				
				--and (@StartDate is null or t2.StartDate>=@StartDate) and (@EndDate is null or t2.StartDate<=@EndDate) 
                                AND t2.StartDate <= @EndDate
                                AND StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
						( SELECT    t1.StuEnrollId
                          FROM      arStuEnrollments t1
                                   ,syStatusCodes t2
                          WHERE     t1.StatusCodeId = t2.StatusCodeId
                                    AND StartDate <= @EndDate
                                    AND -- Student started before the end date range
                                    LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                    AND (
                                          @ProgId IS NULL
                                          OR t8.ProgId IN ( SELECT  Val
                                                            FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                        )
                                    AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                    AND (
                                          t1.DateDetermined < @StartDate
                                          OR ExpGradDate < @StartDate
                                          OR LDA < @StartDate
                                        ) )
				-- DE8492
				-- If Student is enrolled in only program version and if that program version 
				-- happens to be a continuing ed program exclude the student
                                AND t2.StudentId NOT IN ( SELECT DISTINCT
                                                                    StudentId
                                                          FROM      (
                                                                      SELECT    StudentId
                                                                               ,COUNT(*) AS RowCounter
                                                                      FROM      arStuEnrollments se1
                                                                      WHERE     PrgVerId IN ( SELECT    PrgVerId
                                                                                              FROM      arPrgVersions
                                                                                              WHERE     IsContinuingEd = 1 )
                                                                                AND (
                                                                                      SELECT    COUNT(*)
                                                                                      FROM      dbo.arStuEnrollments se2
                                                                                      WHERE     se2.StudentId = se1.StudentId
                                                                                    ) = 1
                                                                      GROUP BY  StudentId
                                                                      HAVING    COUNT(*) > 0
                                                                    ) dtStudent_ContinuingEd )
                      UNION
		-- Check if there are any US Students who are Full-Time Undergraduates
		-- If there are no US Citizens in that category, insert a blank record
		-- for corresponding races
		-- Ignore NonResident Alien
                      SELECT    NULL AS SSN
                               ,NULL AS StudentNumber
                               ,NULL AS StudentName
                               ,'Women' AS Gender
                               ,(
                                  SELECT DISTINCT
                                            AgencyDescrip
                                  FROM      syRptAgencyFldValues
                                  WHERE     RptAgencyFldValId = IPEDSValue
                                ) AS Race
                               ,NULL AS UnderGraduate
                               ,NULL AS Graduate
                               ,NULL AS UnderGraduateCount
                               ,NULL AS GraduateCount
                               ,2 AS GenderSequence
                               ,IPEDSSequence AS RaceSequence
                               ,NULL AS StudentId
                      FROM      adEthCodes
                      WHERE     EthCodeDescrip <> 'Nonresident Alien'
                                AND EthCodeId NOT IN ( SELECT DISTINCT
                                                                t1.Race
                                                       FROM     arStudent t1
                                                       INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                                                       INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                                                       INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                                                                    AND t6.SysStatusId NOT IN ( 8 )
                                                       INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                                                       INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                                                       INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                                                       LEFT JOIN arAttendTypes t10 ON t2.AttendTypeId = t10.AttendTypeId
                                                       INNER JOIN adGenders t11 ON t1.Gender = t11.GenderId
                                                       INNER JOIN adCitizenships t12 ON t1.Citizen = t12.CitizenshipId
                                                       WHERE    LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                                                                AND (
                                                                      @ProgId IS NULL
                                                                      OR t8.ProgId IN ( SELECT  Val
                                                                                        FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                                                    )
                                                                AND (
                                                                      t9.IPEDSValue = 58
                                                                      OR t9.IPEDSValue = 59
                                                                      OR t9.IPEDSValue = 60
                                                                    )
                                                                AND -- UnderGraduate or Graduate
                                                                (
                                                                  t10.IPEDSValue = 61
                                                                  OR t10.IPEDSValue = 62
                                                                )
                                                                AND -- Full Time or Part Time
                                                                t11.IPEDSValue = 31
                                                                AND t1.Race IS NOT NULL
                                                                AND t12.IPEDSValue <> 65 -- Ignore Non US Citizens (Foreign Students)
								--and (@StartDate is null or t2.StartDate>=@StartDate) and (@EndDate is null or t2.StartDate<=@EndDate) 
                                                                AND t2.StartDate <= @EndDate
                                                                AND StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
								-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
						   ( SELECT t1.StuEnrollId
                             FROM   arStuEnrollments t1
                                   ,syStatusCodes t2
                             WHERE  t1.StatusCodeId = t2.StatusCodeId
                                    AND StartDate <= @EndDate
                                    AND -- Student started before the end date range
                                    LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                    AND (
                                          @ProgId IS NULL
                                          OR t8.ProgId IN ( SELECT  Val
                                                            FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                        )
                                    AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
									-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                    AND (
                                          t1.DateDetermined < @StartDate
                                          OR ExpGradDate < @StartDate
                                          OR LDA < @StartDate
                                        ) )
								-- DE8492
								-- If Student is enrolled in only program version and if that program version 
								-- happens to be a continuing ed program exclude the student
                                                                AND t2.StudentId NOT IN ( SELECT DISTINCT
                                                                                                    StudentId
                                                                                          FROM      (
                                                                                                      SELECT    StudentId
                                                                                                               ,COUNT(*) AS RowCounter
                                                                                                      FROM      arStuEnrollments se1
                                                                                                      WHERE     PrgVerId IN ( SELECT    PrgVerId
                                                                                                                              FROM      arPrgVersions
                                                                                                                              WHERE     IsContinuingEd = 1 )
                                                                                                                AND (
                                                                                                                      SELECT    COUNT(*)
                                                                                                                      FROM      dbo.arStuEnrollments se2
                                                                                                                      WHERE     se2.StudentId = se1.StudentId
                                                                                                                    ) = 1
                                                                                                      GROUP BY  StudentId
                                                                                                      HAVING    COUNT(*) > 0
                                                                                                    ) dtStudent_ContinuingEd ) )
                      UNION
                      SELECT    t1.SSN
                               ,t1.StudentNumber
                               ,t1.LastName + ', ' + t1.FirstName + ' ' + ISNULL(t1.MiddleName,'') AS StudentName
                               ,(
                                  SELECT DISTINCT
                                            AgencyDescrip
                                  FROM      syRptAgencyFldValues
                                  WHERE     RptAgencyFldValId = t3.IPEDSValue
                                ) AS Gender
                               ,CASE WHEN t4.IPEDSValue IS NULL THEN 'Race/ethnicity unknown'
                                     ELSE (
                                            SELECT DISTINCT
                                                    AgencyDescrip
                                            FROM    syRptAgencyFldValues
                                            WHERE   RptAgencyFldValId = t4.IPEDSValue
                                          )
                                END AS Race
                               ,CASE WHEN ( t9.IPEDSValue = 58 ) THEN 'X'
                                     ELSE ''
                                END AS UnderGraduate
                               ,CASE WHEN (
                                            t9.IPEDSValue = 59
                                            OR t9.IPEDSValue = 60
                                          ) THEN 'X'
                                     ELSE ''
                                END AS Graduate
                               ,CASE WHEN ( t9.IPEDSValue = 58 ) THEN 1
                                     ELSE 0
                                END AS UnderGraduateCount
                               ,CASE WHEN (
                                            t9.IPEDSValue = 59
                                            OR t9.IPEDSValue = 60
                                          ) THEN 1
                                     ELSE 0
                                END AS GraduateCount
                               ,t3.IPEDSSequence AS GenderSequence
                               ,CASE WHEN t4.IPEDSValue IS NULL THEN 9
                                     ELSE t4.IPEDSSequence
                                END AS RaceSequence
                               ,t1.StudentId
                      FROM      adGenders t3
                      LEFT JOIN arStudent t1 ON t3.GenderId = t1.Gender
                      LEFT JOIN adEthCodes t4 ON t4.EthCodeId = t1.Race
                      INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                      INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                      INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                                   AND t6.SysStatusId NOT IN ( 8 )
                      INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                      INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                      INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                      LEFT JOIN arAttendTypes t10 ON t2.AttendTypeId = t10.AttendTypeId
                      INNER JOIN adCitizenships t12 ON t1.Citizen = t12.CitizenshipId
                      WHERE     t2.CampusId = LTRIM(RTRIM(@CampusId))
                                AND (
                                      @ProgId IS NULL
                                      OR t8.ProgId IN ( SELECT  Val
                                                        FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                    )
                                AND (
                                      t9.IPEDSValue = 58
                                      OR t9.IPEDSValue = 59
                                      OR t9.IPEDSValue = 60
                                    )
                                AND -- UnderGraduate or Graduate or first professional
                                (
                                  t10.IPEDSValue = 61
                                  OR t10.IPEDSValue = 62
                                )
                                AND -- Full Time or Part Time
                                t3.IPEDSValue = 31
                                AND t1.Race IS NOT NULL
                                AND (
                                      t12.IPEDSValue <> 65
                                      OR t12.IPEDSValue IS NULL
                                    ) -- Ignore Non US Citizens (Foreign Students)
				--and (@StartDate is null or t2.StartDate>=@StartDate) and (@EndDate is null or t2.StartDate<=@EndDate) 
                                AND t2.StartDate <= @EndDate
                                AND StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
						( SELECT    t1.StuEnrollId
                          FROM      arStuEnrollments t1
                                   ,syStatusCodes t2
                          WHERE     t1.StatusCodeId = t2.StatusCodeId
                                    AND StartDate <= @EndDate
                                    AND -- Student started before the end date range
                                    LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                    AND (
                                          @ProgId IS NULL
                                          OR t8.ProgId IN ( SELECT  Val
                                                            FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                        )
                                    AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
									-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                    AND (
                                          t1.DateDetermined < @StartDate
                                          OR ExpGradDate < @StartDate
                                          OR LDA < @StartDate
                                        ) )
				-- DE8492
				-- If Student is enrolled in only program version and if that program version 
				-- happens to be a continuing ed program exclude the student
                                AND t2.StudentId NOT IN ( SELECT DISTINCT
                                                                    StudentId
                                                          FROM      (
                                                                      SELECT    StudentId
                                                                               ,COUNT(*) AS RowCounter
                                                                      FROM      arStuEnrollments se1
                                                                      WHERE     PrgVerId IN ( SELECT    PrgVerId
                                                                                              FROM      arPrgVersions
                                                                                              WHERE     IsContinuingEd = 1 )
                                                                                AND (
                                                                                      SELECT    COUNT(*)
                                                                                      FROM      dbo.arStuEnrollments se2
                                                                                      WHERE     se2.StudentId = se1.StudentId
                                                                                    ) = 1
                                                                      GROUP BY  StudentId
                                                                      HAVING    COUNT(*) > 0
                                                                    ) dtStudent_ContinuingEd )
                    ) dt;
--Order by 
--		GenderSequence, RaceSequence,
--		Case when @OrderBy='SSN' Then dt.SSN end,
--		Case when @OrderBy='LastName' Then dt.StudentName end,
--		Case When @OrderBy='StudentNumber' Then Convert(int,dt.StudentNumber) end


--select * from #EnrollmentDetail order by Gender,Race,GenderSequence,RaceSequence


    SELECT  dtOutput.SSN
           ,dtOutput.StudentNumber
           ,dtOutput.StudentName
           ,dtOutput.Gender
           ,dtOutput.Race
           ,dtOutput.UnderGraduate
           ,dtOutput.Graduate
           ,dtOutput.UnderGraduateCount
           ,dtOutput.GraduateCount
           ,dtOutput.GenderSequence
           ,dtOutput.RaceSequence
           ,dtOutput.MenGraduateCount
           ,dtOutput.WomenGraduateCount
           ,dtOutput.MenUnderGraduateCount
           ,dtOutput.WomenUnderGraduateCount
           ,dtOutput.StudentId
    INTO    #TempOutput
    FROM    (
              SELECT    SSN
                       ,StudentNumber
                       ,StudentName
                       ,Gender
                       ,Race
                       ,UnderGraduate
                       ,Graduate
                       ,UnderGraduateCount
                       ,GraduateCount
                       ,GenderSequence
                       ,RaceSequence
                       ,0 AS MenGraduateCount
                       ,0 AS WomenGraduateCount
                       ,0 AS MenUnderGraduateCount
                       ,0 AS WomenUnderGraduateCount
                       ,StudentId
              FROM      (
                          -- Bring in students with one enrollment
                          SELECT    SSN
                                   ,StudentNumber
                                   ,StudentName
                                   ,Gender
                                   ,Race
                                   ,UnderGraduate
                                   ,Graduate
                                   ,UnderGraduateCount
                                   ,GraduateCount
                                   ,GenderSequence
                                   ,RaceSequence
                                   ,0 AS MenGraduateCount
                                   ,0 AS WomenGraduateCount
                                   ,0 AS MenUnderGraduateCount
                                   ,0 AS WomenUnderGraduateCount
                                   ,StudentId
                          FROM      #EnrollmentDetail
                          WHERE     StudentId IN ( SELECT   StudentId
                                                   FROM     #EnrollmentDetail
                                                   GROUP BY StudentId
                                                   HAVING   COUNT(*) = 1 )
                          UNION
 --The Inner query brings in all students with multiple enrollments
                          SELECT    SSN
                                   ,StudentNumber
                                   ,StudentName
                                   ,Gender
                                   ,Race
                                   ,UnderGraduate
                                   ,Graduate
                                   ,UnderGraduateCount
                                   ,GraduateCount
                                   ,GenderSequence
                                   ,RaceSequence
                                   ,0 AS MenGraduateCount
                                   ,0 AS WomenGraduateCount
                                   ,0 AS MenUnderGraduateCount
                                   ,0 AS WomenUnderGraduateCount
                                   ,StudentId
                          FROM      #EnrollmentDetail
                          WHERE     StudentId IN ( SELECT   StudentId
                                                   FROM     #EnrollmentDetail
                                                   GROUP BY StudentId
                                                   HAVING   COUNT(*) > 1 )
                                    AND GraduateCount >= 1
                          UNION
                          SELECT TOP 1
                                    SSN
                                   ,StudentNumber
                                   ,StudentName
                                   ,Gender
                                   ,Race
                                   ,UnderGraduate
                                   ,Graduate
                                   ,UnderGraduateCount
                                   ,GraduateCount
                                   ,GenderSequence
                                   ,RaceSequence
                                   ,0 AS MenGraduateCount
                                   ,0 AS WomenGraduateCount
                                   ,0 AS MenUnderGraduateCount
                                   ,0 AS WomenUnderGraduateCount
                                   ,StudentId
                          FROM      #EnrollmentDetail
                          WHERE     StudentId NOT IN ( SELECT   StudentId
                                                       FROM     #EnrollmentDetail
                                                       WHERE    GraduateCount >= 1
                                                       GROUP BY StudentId
                                                       HAVING   COUNT(*) > 1 )
                        ) dtFinalQuery
            ) dtOutput;

    DECLARE @MenGraduateCount INT
       ,@MenUnderGraduateCount INT
       ,@WomenGraduateCount INT
       ,@WomenUnderGraduateCount INT;
    SET @MenGraduateCount = 0;
    SET @MenUnderGraduateCount = 0;
    SET @WomenGraduateCount = 0;
    SET @WomenUnderGraduateCount = 0;

    SET @MenGraduateCount = (
                              SELECT    SUM(ISNULL(GraduateCount,0))
                              FROM      #TempOutput
                              WHERE     LOWER(Gender) = 'men'
                            );
    SET @WomenGraduateCount = (
                                SELECT  SUM(ISNULL(GraduateCount,0))
                                FROM    #TempOutput
                                WHERE   LOWER(Gender) = 'women'
                              );
    SET @MenUnderGraduateCount = (
                                   SELECT   SUM(ISNULL(UnderGraduateCount,0))
                                   FROM     #TempOutput
                                   WHERE    LOWER(Gender) = 'men'
                                 );
    SET @WomenUnderGraduateCount = (
                                     SELECT SUM(ISNULL(UnderGraduateCount,0))
                                     FROM   #TempOutput
                                     WHERE  LOWER(Gender) = 'women'
                                   );

	UPDATE #TempOutput
	SET MenGraduateCount = @MenGraduateCount,
		WomenGraduateCount = @WomenGraduateCount,
		MenUnderGraduateCount = @MenUnderGraduateCount,
		WomenUnderGraduateCount = @WomenUnderGraduateCount

--select Distinct Race from #TempOutput where Gender='Women'
--Select EthCodeDescrip from adEthCodes
-- The dtFinalQuery is missing some races and the following section will bring the data
    SELECT  SSN
           ,StudentNumber
           ,StudentName
           ,Gender
           ,Race
           ,UnderGraduate
           ,Graduate
           ,UnderGraduateCount
           ,GraduateCount
           ,GenderSequence
           ,RaceSequence
           ,MenGraduateCount
           ,WomenGraduateCount
           ,MenUnderGraduateCount
           ,WomenUnderGraduateCount
           ,StudentId
           ,EnrollmentId
    FROM    (
              SELECT    SSN
                       ,StudentNumber
                       ,StudentName
                       ,Gender
                       ,Race
                       ,UnderGraduate
                       ,Graduate
                       ,UnderGraduateCount
                       ,GraduateCount
                       ,GenderSequence
                       ,RaceSequence
                       ,MenGraduateCount
                       ,WomenGraduateCount
                       ,MenUnderGraduateCount
                       ,WomenUnderGraduateCount
                       ,StudentId
                       ,(
                          SELECT TOP 1
                                    A5.EnrollmentId
                          FROM      arStuEnrollments A5
                          WHERE     A5.StudentId = dt6.StudentId
                          ORDER BY  A5.StartDate
                                   ,A5.EnrollDate ASC
                        ) AS EnrollmentId
              FROM      (
                          SELECT    SSN
                                   ,StudentNumber
                                   ,StudentName
                                   ,Gender
                                   ,Race
                                   ,UnderGraduate
                                   ,Graduate
                                   ,UnderGraduateCount
                                   ,GraduateCount
                                   ,GenderSequence
                                   ,RaceSequence
                                   ,MenGraduateCount
                                   ,WomenGraduateCount
                                   ,MenUnderGraduateCount
                                   ,WomenUnderGraduateCount
                                   ,CASE WHEN StudentName IS NULL THEN NULL
                                         ELSE StudentId
                                    END AS StudentId
                          FROM      #TempOutput
                          UNION
                          SELECT    NULL AS SSN
                                   ,NULL AS StudentNumber
                                   ,NULL AS StudentName
                                   ,'Men' AS Gender
                                   ,(
                                      SELECT DISTINCT
                                                AgencyDescrip
                                      FROM      syRptAgencyFldValues
                                      WHERE     RptAgencyFldValId = IPEDSValue
                                    ) AS Race
                                   ,NULL AS UnderGraduate
                                   ,NULL AS Graduate
                                   ,NULL AS UnderGraduateCount
                                   ,NULL AS GraduateCount
                                   ,1 AS GenderSequence
                                   ,IPEDSSequence AS RaceSequence
                                   ,@MenGraduateCount AS MenGraduateCount
                                   ,@WomenGraduateCount AS WomenGraduateCount
                                   ,@MenUnderGraduateCount AS MenUnderGraduateCount
                                   ,@WomenUnderGraduateCount AS WomenUnderGraduateCount
                                   ,NULL AS StudentId
                          FROM      adEthCodes A1
                                   ,(
                                      SELECT    RptAgencyFldValId
                                               ,RptAgencyFldId
                                               ,AgencyDescrip
                                               ,AgencyValue
                                      FROM      syRptAgencyFldValues
                                      WHERE     RptAgencyFldId = 15
                                    ) A2
                          WHERE     A1.IPEDSValue = A2.RptAgencyFldValId
                                    AND A2.AgencyDescrip NOT IN ( SELECT DISTINCT
                                                                            Race
                                                                  FROM      #TempOutput
                                                                  WHERE     Gender = 'Men' )
                          UNION
                          SELECT    NULL AS SSN
                                   ,NULL AS StudentNumber
                                   ,NULL AS StudentName
                                   ,'Men' AS Gender
                                   ,'Nonresident Alien' AS Race
                                   ,NULL AS UnderGraduate
                                   ,NULL AS Graduate
                                   ,NULL AS UnderGraduateCount
                                   ,NULL AS GraduateCount
                                   ,1 AS GenderSequence
                                   ,1 AS RaceSequence
                                   ,@MenGraduateCount AS MenGraduateCount
                                   ,@WomenGraduateCount AS WomenGraduateCount
                                   ,@MenUnderGraduateCount AS MenUnderGraduateCount
                                   ,@WomenUnderGraduateCount AS WomenUnderGraduateCount
                                   ,NULL AS StudentId
                          FROM      #TempOutput
                          WHERE     Race <> 'Nonresident Alien'
                                    AND Gender = 'Men'
                          UNION
                          SELECT    NULL AS SSN
                                   ,NULL AS StudentNumber
                                   ,NULL AS StudentName
                                   ,'Women' AS Gender
                                   ,(
                                      SELECT DISTINCT
                                                AgencyDescrip
                                      FROM      syRptAgencyFldValues
                                      WHERE     RptAgencyFldValId = IPEDSValue
                                    ) AS Race
                                   ,NULL AS UnderGraduate
                                   ,NULL AS Graduate
                                   ,NULL AS UnderGraduateCount
                                   ,NULL AS GraduateCount
                                   ,2 AS GenderSequence
                                   ,IPEDSSequence AS RaceSequence
                                   ,@MenGraduateCount AS MenGraduateCount
                                   ,@WomenGraduateCount AS WomenGraduateCount
                                   ,@MenUnderGraduateCount AS MenUnderGraduateCount
                                   ,@WomenUnderGraduateCount AS WomenUnderGraduateCount
                                   ,NULL AS StudentId
                          FROM      adEthCodes A1
                                   ,(
                                      SELECT    RptAgencyFldValId
                                               ,RptAgencyFldId
                                               ,AgencyDescrip
                                               ,AgencyValue
                                      FROM      syRptAgencyFldValues
                                      WHERE     RptAgencyFldId = 15
                                    ) A2
                          WHERE     A1.IPEDSValue = A2.RptAgencyFldValId
                                    AND A2.AgencyDescrip NOT IN ( SELECT DISTINCT
                                                                            Race
                                                                  FROM      #TempOutput
                                                                  WHERE     Gender = 'Women' )
                          UNION
                          SELECT    NULL AS SSN
                                   ,NULL AS StudentNumber
                                   ,NULL AS StudentName
                                   ,'Women' AS Gender
                                   ,'Nonresident Alien' AS Race
                                   ,NULL AS UnderGraduate
                                   ,NULL AS Graduate
                                   ,NULL AS UnderGraduateCount
                                   ,NULL AS GraduateCount
                                   ,2 AS GenderSequence
                                   ,1 AS RaceSequence
                                   ,@MenGraduateCount AS MenGraduateCount
                                   ,@WomenGraduateCount AS WomenGraduateCount
                                   ,@MenUnderGraduateCount AS MenUnderGraduateCount
                                   ,@WomenUnderGraduateCount AS WomenUnderGraduateCount
                                   ,NULL AS StudentId
                          FROM      #TempOutput
                          WHERE     Race <> 'Nonresident Alien'
                                    AND Gender = 'Women'
                        ) dt6
            ) dt7
--where Gender='Women'
ORDER BY    GenderSequence
           ,RaceSequence
           ,CASE WHEN @OrderBy = 'SSN' THEN SSN
            END
           ,CASE WHEN @OrderBy = 'LastName' THEN StudentName
            END
           ,CASE WHEN @OrderBy = 'student number' THEN StudentNumber
            END
           ,CASE WHEN @OrderBy = 'EnrollmentId' THEN EnrollmentId
            END;


--select * from #EnrollmentDetail order by StudentId
    DROP TABLE #TempOutput;
    DROP TABLE  #EnrollmentDetail;
--------------------------------------------------------------------------------------------
-- Fall Enrollment report updated for MTI while testing in Sanbox - GM  - Start
-- Comment an unused variable which was causing report not to load
--------------------------------------------------------------------------------------------



GO
