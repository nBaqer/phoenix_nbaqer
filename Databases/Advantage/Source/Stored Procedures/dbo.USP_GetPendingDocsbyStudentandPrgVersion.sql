SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[USP_GetPendingDocsbyStudentandPrgVersion]
    (
     @StudentId UNIQUEIDENTIFIER
    ,@CampusID UNIQUEIDENTIFIER    
    )
AS
    BEGIN    
     
    
     
        DECLARE @StudentStartDate AS DATETIME;    
        DECLARE @LeadID AS UNIQUEIDENTIFIER;    
      
        SET @LeadID = (
                        SELECT TOP 1
                                LeadID
                        FROM    arStuEnrollments
                        WHERE   StudentID = @StudentId
                      );    
       
        SET @StudentStartDate = (
                                  SELECT    MIN(StartDate)
                                  FROM      arStuEnrollments
                                  WHERE     StudentID = @StudentId
                                );    
    
                               
        DECLARE @ActiveStatusID AS UNIQUEIDENTIFIER;    
        SET @ActiveStatusID = (
                                SELECT  StatusID
                                FROM    syStatuses
                                WHERE   Status = 'Active'
                              );     
     
    
    
    
    
    
    
        (
          SELECT  DISTINCT
                    adReqId AS DocumentId
                   ,Descrip AS DocumentDescrip
                   ,ReqGrpId
                   ,ReqGrpDescrip
                   ,NumRequired
                   ,StartDate
                   ,EndDate
                   ,Required
                   ,DocSubmittedCount
                   ,ReqforEnrollment
                   ,ReqforFinancialAid
                   ,ReqforGraduation
                   ,CASE WHEN DocStatusDescrip = 'Approved' THEN 'Approved'
                         ELSE 'Not Approved'
                    END AS DocStatusDescrip
          FROM      (
                      SELECT DISTINCT
                                adReqId
                               ,Descrip
                               ,ReqGrpId
                               ,ReqGrpDescrip
                               ,NumRequired
                               ,StartDate
                               ,EndDate
                               ,Required
                               ,DocSubmittedCount
                               ,DocStatusDescrip
                               ,ReqforEnrollment
                               ,ReqforFinancialAid
                               ,ReqforGraduation
                               ,CampGrpId
                               ,COUNT(*) OVER ( PARTITION BY adReqId ) AS DupCount
                      FROM      (
                                  SELECT DISTINCT
                                            t1.adReqId
                                           ,t1.Descrip
                                           ,'00000000-0000-0000-0000-000000000000' AS ReqGrpId
                                           ,'' AS ReqGrpDescrip
                                           ,0 AS NumRequired
                                           ,@StudentStartDate AS CurrentDate
                                           ,t2.StartDate
                                           ,t2.EndDate
                                           ,DocSubmittedCount = (
                                                                  SELECT    COUNT(*)
                                                                  FROM      plStudentDocs
                                                                  WHERE     DocumentId = t1.adReqId
                                                                            AND StudentId = @StudentId
                                                                )
                                           ,t2.Minscore
                                           ,1 AS Required
                                           ,DocStatusDescrip = (
                                                                 SELECT TOP 1
                                                                        s1.DocStatusDescrip
                                                                 FROM   sySysDocStatuses s1
                                                                       ,syDocStatuses s2
                                                                       ,plStudentDocs s3
                                                                 WHERE  s1.SysDocStatusId = s2.sysDocStatusId
                                                                        AND s2.DocStatusId = s3.DocStatusId
                                                                        AND s3.StudentId = @StudentId
                                                                        AND s3.DocumentId = t1.adReqId
                                                               )
                                           ,ReqforEnrollment
                                           ,ReqforFinancialAid
                                           ,ReqforGraduation
                                           ,t1.CampGrpId
                                  FROM      adReqs t1
                                           ,adReqsEffectiveDates t2
                                  WHERE     t1.adReqId = t2.adReqId
                                            AND t2.MandatoryRequirement = 1
                                            AND t1.adReqTypeId IN ( 3 )
                                            AND t1.StatusId = @ActiveStatusID
                                  UNION
                                  SELECT DISTINCT
                                            t3.adReqId
                                           ,t4.Descrip
                                           ,t1.ReqGrpId
                                           ,t1.Descrip AS ReqGrpDescrip
                                           ,NumRequired = (
                                                            SELECT  COUNT(*)
                                                            FROM    adReqGrpDef
                                                            WHERE   ReqGrpId = t1.ReqGrpId
                                                          )
                                           ,@StudentStartDate AS CurrentDate
                                           ,t2.StartDate
                                           ,t2.EndDate
                                           ,DocSubmittedCount = (
                                                                  SELECT    COUNT(*)
                                                                  FROM      plStudentDocs
                                                                  WHERE     DocumentId = t3.adReqId
                                                                            AND StudentId = @StudentId
                                                                )
                                           ,t2.Minscore
                                           ,1 AS Required
                                           ,DocStatusDescrip = (
                                                                 SELECT TOP 1
                                                                        s1.DocStatusDescrip
                                                                 FROM   sySysDocStatuses s1
                                                                       ,syDocStatuses s2
                                                                       ,plStudentDocs s3
                                                                 WHERE  s1.SysDocStatusId = s2.sysDocStatusId
                                                                        AND s2.DocStatusId = s3.DocStatusId
                                                                        AND s3.StudentId = @StudentId
                                                                        AND s3.DocumentId = t3.adReqId
                                                               )
                                           ,ReqforEnrollment
                                           ,ReqforFinancialAid
                                           ,ReqforGraduation
                                           ,t4.CampGrpId
                                  FROM      adReqGroups t1
                                           ,adReqGrpDef t3
                                           ,adReqs t4
                                           ,adReqsEffectiveDates t2
                                  WHERE     t1.IsMandatoryReqGrp = 1
                                            AND t1.ReqGrpId = t3.ReqGrpId
                                            AND t3.adReqId = t2.adReqId
                                            AND t3.adReqId = t4.adReqId
                                            AND t4.adReqTypeId IN ( 3 )
                                            AND t4.StatusId = @ActiveStatusID
                                ) R1
                      WHERE     R1.CurrentDate >= R1.StartDate
                                AND (
                                      R1.CurrentDate <= R1.EndDate
                                      OR R1.EndDate IS NULL
                                    )
                      UNION
                      SELECT DISTINCT
                                adReqId
                               ,Descrip
                               ,reqGrpId
                               ,ReqGrpDescrip
                               ,NumRequired
                               ,StartDate
                               ,EndDate
                               ,Required
                               ,DocSubmittedCount
                               ,DocStatusDescrip
                               ,ReqforEnrollment
                               ,ReqforFinancialAid
                               ,ReqforGraduation
                               ,CampGrpId
                               ,COUNT(*) OVER ( PARTITION BY adReqId ) AS DupCount
                      FROM      (
                                  SELECT DISTINCT
                                            t1.adReqId
                                           ,t1.Descrip
                                           ,'00000000-0000-0000-0000-000000000000' AS reqGrpId
                                           ,'' AS ReqGrpDescrip
                                           ,0 AS NumRequired
                                           ,@StudentStartDate AS CurrentDate
                                           ,t2.StartDate
                                           ,t2.EndDate
                                           ,DocSubmittedCount = (
                                                                  SELECT    COUNT(*)
                                                                  FROM      plStudentDocs
                                                                  WHERE     DocumentId = t1.adReqId
                                                                            AND StudentId = @StudentId
                                                                )
                                           ,t2.Minscore
                                           ,1 AS Required
                                           ,DocStatusDescrip = (
                                                                 SELECT TOP 1
                                                                        s1.DocStatusDescrip
                                                                 FROM   sySysDocStatuses s1
                                                                       ,syDocStatuses s2
                                                                       ,plStudentDocs s3
                                                                 WHERE  s1.SysDocStatusId = s2.sysDocStatusId
                                                                        AND s2.DocStatusId = s3.DocStatusId
                                                                        AND s3.StudentId = @StudentId
                                                                        AND s3.DocumentId = t1.adReqId
                                                               )
                                           ,ReqforEnrollment
                                           ,ReqforFinancialAid
                                           ,ReqforGraduation
                                           ,t1.CampGrpId
                                  FROM      adReqs t1
                                           ,adReqsEffectiveDates t2
                                           ,adReqLeadGroups t3
                                           ,adLeads t4
                                           ,adPrgVerTestDetails t5
                                  WHERE     t1.adReqId = t2.adReqId
                                            AND t2.MandatoryRequirement <> 1
                                            AND t1.StatusId = @ActiveStatusID
                                            AND t1.adreqTypeId IN ( 3 )
                                            AND t2.adReqEffectiveDateId = t3.adReqEffectiveDateId
                                            AND t4.PrgVerId = t5.PrgVerId
                                            AND t1.adReqId = t5.adReqId
                                            AND t5.PrgVerId IN ( SELECT DISTINCT
                                                                        PrgVerId
                                                                 FROM   arStuEnrollments
                                                                 WHERE  StudentId = @StudentId )    
                                            --AND t3.LeadGrpId IN (    
                                            --SELECT DISTINCT    
                                            --        LeadGrpId    
                                            --FROM    adLeadByLeadGroups    
                                            --WHERE   StuEnrollId IN (    
                                            --        SELECT  StuEnrollId    
                                            --        FROM    arStuEnrollments    
                                            --        WHERE   StudentId = @StudentId ) )    
                                            AND t5.adReqId IS NOT NULL
                                ) R1
                      WHERE     R1.CurrentDate >= R1.StartDate
                                AND (
                                      R1.CurrentDate <= R1.EndDate
                                      OR R1.EndDate IS NULL
                                    )
                      UNION
                      SELECT DISTINCT
                                adReqId
                               ,Descrip
                               ,ReqGrpId
                               ,ReqGrpDescrip
                               ,NumRequired
                               ,StartDate
                               ,EndDate
                               ,Required
                               ,DocSubmittedCount
                               ,DocStatusDescrip
                               ,ReqforEnrollment
                               ,ReqforFinancialAid
                               ,ReqforGraduation
                               ,CampGrpId
                               ,COUNT(*) OVER ( PARTITION BY adReqId ) AS DupCount
                      FROM      (
                                  SELECT DISTINCT
                                            t13.adReqId
                                           ,t1.Descrip
                                           ,t11.ReqGrpId
                                           ,t11.Descrip AS ReqGrpDescrip
                                           ,Z.NumReqs AS NumRequired
                                           ,@StudentStartDate AS CurrentDate
                                           ,t2.StartDate
                                           ,t2.EndDate
                                           ,DocSubmittedCount = (
                                                                  SELECT    COUNT(*)
                                                                  FROM      plStudentDocs
                                                                  WHERE     DocumentId = t13.adReqId
                                                                            AND StudentId = @StudentId
                                                                )
                                           ,t2.Minscore
                                           ,ISNULL(t13.IsRequired,0) AS Required
                                           ,DocStatusDescrip = (
                                                                 SELECT TOP 1
                                                                        s1.DocStatusDescrip
                                                                 FROM   sySysDocStatuses s1
                                                                       ,syDocStatuses s2
                                                                       ,plStudentDocs s3
                                                                 WHERE  s1.SysDocStatusId = s2.sysDocStatusId
                                                                        AND s2.DocStatusId = s3.DocStatusId
                                                                        AND s3.StudentId = @StudentId
                                                                        AND s3.DocumentId = t13.adReqId
                                                               )
                                           ,ReqforEnrollment
                                           ,ReqforFinancialAid
                                           ,ReqforGraduation
                                           ,t1.CampGrpId
                                  FROM      adReqs t1
                                           ,adReqsEffectiveDates t2
                                           ,adReqLeadGroups t3
                                           ,adLeads t4
                                           ,adPrgVerTestDetails t5
                                           ,adReqGroups t11
                                           ,adReqGrpDef t13
                                           ,(
                                              SELECT    LeadGrpId
                                                       ,ReqGrpId
                                                       ,NumReqs
                                              FROM      adLeadGrpReqGroups
                                              WHERE     LeadGrpId IN ( SELECT DISTINCT
                                                                                LeadGrpId
                                                                       FROM     adLeadByLeadGroups
                                                                       WHERE    StuEnrollId IN ( SELECT StuEnrollId
                                                                                                 FROM   arStuEnrollments
                                                                                                 WHERE  StudentId = @StudentId ) )
                                            ) Z
                                  WHERE     t11.ReqGrpId = t5.ReqGrpId
                                            AND t1.StatusId = @ActiveStatusID
                                            AND t5.adReqId IS NULL
                                            AND t5.PrgVerId IN ( SELECT DISTINCT
                                                                        PrgVerId
                                                                 FROM   arStuEnrollments
                                                                 WHERE  StudentId = @StudentId )
                                            AND t4.PrgVerId = t5.PrgVerId
                                            AND t11.ReqGrpId = t13.ReqGrpId
                                            AND t13.adReqId = t2.adReqId
                                            AND t2.MandatoryRequirement <> 1
                                            AND t13.adReqId = t1.adReqId
                                            AND t1.adreqTypeId IN ( 3 )
                                            AND t2.adReqEffectiveDateId = t3.adReqEffectiveDateId
                                            AND t3.LeadGrpId IN ( SELECT DISTINCT
                                                                            LeadGrpId
                                                                  FROM      adLeadByLeadGroups
                                                                  WHERE     StuEnrollId IN ( SELECT StuEnrollId
                                                                                             FROM   arStuEnrollments
                                                                                             WHERE  StudentId = @StudentId ) )
                                            AND Z.LeadGrpId = t3.LeadGrpId
                                            AND Z.ReqGrpId = t11.ReqGrpId
                                            AND t13.LeadGrpId = Z.LeadGrpId
                                ) R1
                      WHERE     R1.CurrentDate >= R1.StartDate
                                AND (
                                      R1.CurrentDate <= R1.EndDate
                                      OR R1.EndDate IS NULL
                                    )
                      UNION
                      SELECT  DISTINCT
                                adReqId
                               ,Descrip
                               ,ReqGrpId
                               ,ReqGrpDescrip
                               ,NumRequired
                               ,StartDate
                               ,EndDate
                               ,Required
                               ,DocSubmittedCount
                               ,DocStatusDescrip
                               ,ReqforEnrollment
                               ,ReqforFinancialAid
                               ,ReqforGraduation
                               ,CampGrpId
                               ,COUNT(*) OVER ( PARTITION BY adReqId ) AS DupCount
                      FROM      (
                                  SELECT  DISTINCT
                                            t1.adReqId
                                           ,t1.Descrip
                                           ,'00000000-0000-0000-0000-000000000000' AS ReqGrpId
                                           ,'' AS ReqGrpDescrip
                                           ,0 AS NumRequired
                                           ,@StudentStartDate AS CurrentDate
                                           ,t2.StartDate
                                           ,t2.EndDate
                                           ,(
                                              SELECT    COUNT(*)
                                              FROM      plStudentDocs
                                              WHERE     DocumentId = t1.adReqId
                                                        AND StudentId = @StudentId
                                            ) AS DocSubmittedCount
                                           ,t2.Minscore
                                           ,ISNULL(t3.IsRequired,0) AS Required
                                           ,(
                                              SELECT TOP 1
                                                        s1.DocStatusDescrip
                                              FROM      sySysDocStatuses s1
                                                       ,syDocStatuses s2
                                                       ,plStudentDocs s3
                                              WHERE     s1.SysDocStatusId = s2.sysDocStatusId
                                                        AND s2.DocStatusId = s3.DocStatusId
                                                        AND s3.StudentId = @StudentId
                                                        AND s3.DocumentId = t1.adReqId
                                            ) AS DocStatusDescrip
                                           ,ReqforEnrollment
                                           ,ReqforFinancialAid
                                           ,ReqforGraduation
                                           ,t1.CampGrpId
                                  FROM      adReqs t1
                                           ,adReqsEffectiveDates t2
                                           ,adReqLeadGroups t3
                                  WHERE     t1.adReqId = t2.adReqId
                                            AND t2.adReqEffectiveDateId = t3.adReqEffectiveDateId
                                            AND t1.StatusId = @ActiveStatusID
                                            AND t1.adReqId NOT IN ( SELECT DISTINCT
                                                                            s1.adReqId
                                                                    FROM    adReqGrpDef s1
                                                                           ,adPrgVerTestDetails s2
                                                                    WHERE   s1.ReqGrpId = s2.ReqGrpId
                                                                            AND s2.PrgVerId IN ( SELECT DISTINCT
                                                                                                        PrgVerId
                                                                                                 FROM   arStuEnrollments
                                                                                                 WHERE  StudentId = @StudentId )
                                                                            AND s2.adReqId IS NULL )
                                            AND t3.LeadGrpId IN ( SELECT    LeadGrpId
                                                                  FROM      adLeadByLeadGroups
                                                                  WHERE     StuEnrollId IN ( SELECT DISTINCT
                                                                                                    StuEnrollId
                                                                                             FROM   arStuEnrollments
                                                                                             WHERE  StudentId = @StudentId ) )
                                            AND t1.adReqTypeId IN ( 3 )
                                            AND t2.MandatoryRequirement <> 1
                                            AND t1.adReqId NOT IN ( (
                                                                      SELECT DISTINCT
                                                                                adReqId
                                                                      FROM      adPrgVerTestDetails
                                                                      WHERE     PrgVerId IN ( SELECT DISTINCT
                                                                                                        PrgVerId
                                                                                              FROM      arStuEnrollments
                                                                                              WHERE     StudentId = @StudentId )
                                                                                AND adreqID IS NOT NULL
                                                                    )
                                                           )
                                ) R1
                      WHERE     R1.CurrentDate >= R1.StartDate
                                AND (
                                      R1.CurrentDate <= R1.EndDate
                                      OR R1.EndDate IS NULL
                                    )
                    ) R2
          WHERE     (
                      (
                        DupCount > 1
                        AND Required = 1
                      )
                      OR (
                           DupCount = 1
                           AND Required IN ( 0,1 )
                         )
                    )
                    AND R2.CampGrpId IN ( SELECT DISTINCT
                                                    t2.CampGrpId
                                          FROM      syCmpGrpCmps t1
                                                   ,syCampGrps t2
                                          WHERE     t1.CampGrpId = t2.CampGrpId
                                                    AND t1.CampusId = @CampusID
                                                    AND t2.StatusId = @ActiveStatusID )
                    AND ( ReqForGraduation = 1 )
        )
        UNION
        (
          SELECT  DISTINCT
                    adReqId AS DocumentId
                   ,Descrip AS DocumentDescrip
                   ,ReqGrpId
                   ,ReqGrpDescrip
                   ,NumRequired
                   ,StartDate
                   ,EndDate
                   ,Required
                   ,DocSubmittedCount
                   ,ReqforEnrollment
                   ,ReqforFinancialAid
                   ,ReqforGraduation
                   ,CASE WHEN DocStatusDescrip = 'Approved' THEN 'Approved'
                         ELSE 'Not Approved'
                    END AS DocStatusDescrip
          FROM      (
                      SELECT DISTINCT
                                adReqId
                               ,Descrip
                               ,ReqGrpId
                               ,ReqGrpDescrip
                               ,NumRequired
                               ,StartDate
                               ,EndDate
                               ,Required
                               ,DocSubmittedCount
                               ,DocStatusDescrip
                               ,ReqforEnrollment
                               ,ReqforFinancialAid
                               ,ReqforGraduation
                               ,CampGrpId
                               ,COUNT(*) OVER ( PARTITION BY adReqId ) AS DupCount
                      FROM      (
                                  SELECT DISTINCT
                                            t1.adReqId
                                           ,t1.Descrip
                                           ,'00000000-0000-0000-0000-000000000000' AS ReqGrpId
                                           ,'' AS ReqGrpDescrip
                                           ,0 AS NumRequired
                                           ,GETDATE() AS CurrentDate
                                           ,t2.StartDate
                                           ,t2.EndDate
                                           ,DocSubmittedCount = (
                                                                  SELECT    COUNT(*)
                                                                  FROM      plStudentDocs
                                                                  WHERE     DocumentId = t1.adReqId
                                                                            AND StudentId = @StudentId
                                                                )
                                           ,t2.Minscore
                                           ,1 AS Required
                                           ,DocStatusDescrip = (
                                                                 SELECT TOP 1
                                                                        s1.DocStatusDescrip
                                                                 FROM   sySysDocStatuses s1
                                                                       ,syDocStatuses s2
                                                                       ,plStudentDocs s3
                                                                 WHERE  s1.SysDocStatusId = s2.sysDocStatusId
                                                                        AND s2.DocStatusId = s3.DocStatusId
                                                                        AND s3.StudentId = @StudentId
                                                                        AND s3.DocumentId = t1.adReqId
                                                               )
                                           ,ReqforEnrollment
                                           ,ReqforFinancialAid
                                           ,ReqforGraduation
                                           ,t1.CampGrpId
                                  FROM      adReqs t1
                                           ,adReqsEffectiveDates t2
                                  WHERE     t1.adReqId = t2.adReqId
                                            AND t2.MandatoryRequirement = 1
                                            AND t1.adReqTypeId IN ( 3 )
                                            AND t1.StatusId = @ActiveStatusID
                                  UNION
                                  SELECT DISTINCT
                                            t3.adReqId
                                           ,t4.Descrip
                                           ,t1.ReqGrpId
                                           ,t1.Descrip AS ReqGrpDescrip
                                           ,NumRequired = (
                                                            SELECT  COUNT(*)
                                                            FROM    adReqGrpDef
                                                            WHERE   ReqGrpId = t1.ReqGrpId
                                                          )
                                           ,GETDATE() AS CurrentDate
                                           ,t2.StartDate
                                           ,t2.EndDate
                                           ,DocSubmittedCount = (
                                                                  SELECT    COUNT(*)
                                                                  FROM      plStudentDocs
                                                                  WHERE     DocumentId = t3.adReqId
                                                                            AND StudentId = @StudentId
                                                                )
                                           ,t2.Minscore
                                           ,1 AS Required
                                           ,DocStatusDescrip = (
                                                                 SELECT TOP 1
                                                                        s1.DocStatusDescrip
                                                                 FROM   sySysDocStatuses s1
                                                                       ,syDocStatuses s2
                                                                       ,plStudentDocs s3
                                                                 WHERE  s1.SysDocStatusId = s2.sysDocStatusId
                                                                        AND s2.DocStatusId = s3.DocStatusId
                                                                        AND s3.StudentId = @StudentId
                                                                        AND s3.DocumentId = t3.adReqId
                                                               )
                                           ,ReqforEnrollment
                                           ,ReqforFinancialAid
                                           ,ReqforGraduation
                                           ,t4.CampGrpId
                                  FROM      adReqGroups t1
                                           ,adReqGrpDef t3
                                           ,adReqs t4
                                           ,adReqsEffectiveDates t2
                                  WHERE     t1.IsMandatoryReqGrp = 1
                                            AND t1.ReqGrpId = t3.ReqGrpId
                                            AND t3.adReqId = t2.adReqId
                                            AND t3.adReqId = t4.adReqId
                                            AND t4.adReqTypeId IN ( 3 )
                                            AND t4.StatusId = @ActiveStatusID
                                ) R1
                      WHERE     R1.CurrentDate >= R1.StartDate
                                AND (
                                      R1.CurrentDate <= R1.EndDate
                                      OR R1.EndDate IS NULL
                                    )
                      UNION
                      SELECT DISTINCT
                                adReqId
                               ,Descrip
                               ,reqGrpId
                               ,ReqGrpDescrip
                               ,NumRequired
                               ,StartDate
                               ,EndDate
                               ,Required
                               ,DocSubmittedCount
                               ,DocStatusDescrip
                               ,ReqforEnrollment
                               ,ReqforFinancialAid
                               ,ReqforGraduation
                               ,CampGrpId
                               ,COUNT(*) OVER ( PARTITION BY adReqId ) AS DupCount
                      FROM      (
                                  SELECT DISTINCT
                                            t1.adReqId
                                           ,t1.Descrip
                                           ,'00000000-0000-0000-0000-000000000000' AS reqGrpId
                                           ,'' AS ReqGrpDescrip
                                           ,0 AS NumRequired
                                           ,GETDATE() AS CurrentDate
                                           ,t2.StartDate
                                           ,t2.EndDate
                                           ,DocSubmittedCount = (
                                                                  SELECT    COUNT(*)
                                                                  FROM      plStudentDocs
                                                                  WHERE     DocumentId = t1.adReqId
                                                                            AND StudentId = @StudentId
                                                                )
                                           ,t2.Minscore
                                           ,1 AS Required
                                           ,DocStatusDescrip = (
                                                                 SELECT TOP 1
                                                                        s1.DocStatusDescrip
                                                                 FROM   sySysDocStatuses s1
                                                                       ,syDocStatuses s2
                                                                       ,plStudentDocs s3
                                                                 WHERE  s1.SysDocStatusId = s2.sysDocStatusId
                                                                        AND s2.DocStatusId = s3.DocStatusId
                                                                        AND s3.StudentId = @StudentId
                                                                        AND s3.DocumentId = t1.adReqId
                                                               )
                                           ,ReqforEnrollment
                                           ,ReqforFinancialAid
                                           ,ReqforGraduation
                                           ,t1.CampGrpId
                                  FROM      adReqs t1
                                           ,adReqsEffectiveDates t2
                                           ,adReqLeadGroups t3
                                           ,adLeads t4
                                           ,adPrgVerTestDetails t5
                                  WHERE     t1.adReqId = t2.adReqId
                                            AND t2.MandatoryRequirement <> 1
                                            AND t1.StatusId = @ActiveStatusID
                                            AND t1.adreqTypeId IN ( 3 )
                                            AND t2.adReqEffectiveDateId = t3.adReqEffectiveDateId
                                            AND t4.PrgVerId = t5.PrgVerId
                                            AND t1.adReqId = t5.adReqId
                                            AND t5.PrgVerId IN ( SELECT DISTINCT
                                                                        PrgVerId
                                                                 FROM   arStuEnrollments
                                                                 WHERE  StudentId = @StudentId )    
                                            --AND t3.LeadGrpId IN (    
                                            --SELECT DISTINCT    
                                            --        LeadGrpId    
                                            --FROM    adLeadByLeadGroups    
                                            --WHERE   StuEnrollId IN (    
                                            --        SELECT  StuEnrollId    
                                            --        FROM    arStuEnrollments    
                                            --        WHERE   StudentId = @StudentId ) )    
                                            AND t5.adReqId IS NOT NULL
                                ) R1
                      WHERE     R1.CurrentDate >= R1.StartDate
                                AND (
                                      R1.CurrentDate <= R1.EndDate
                                      OR R1.EndDate IS NULL
                                    )
                      UNION
                      SELECT DISTINCT
                                adReqId
                               ,Descrip
                               ,ReqGrpId
                               ,ReqGrpDescrip
                               ,NumRequired
                               ,StartDate
                               ,EndDate
                               ,Required
                               ,DocSubmittedCount
                               ,DocStatusDescrip
                               ,ReqforEnrollment
                               ,ReqforFinancialAid
                               ,ReqforGraduation
                               ,CampGrpId
                               ,COUNT(*) OVER ( PARTITION BY adReqId ) AS DupCount
                      FROM      (
                                  SELECT DISTINCT
                                            t13.adReqId
                                           ,t1.Descrip
                                           ,t11.ReqGrpId
                                           ,t11.Descrip AS ReqGrpDescrip
                                           ,Z.NumReqs AS NumRequired
                                           ,GETDATE() AS CurrentDate
                                           ,t2.StartDate
                                           ,t2.EndDate
                                           ,DocSubmittedCount = (
                                                                  SELECT    COUNT(*)
                                                                  FROM      plStudentDocs
                                                                  WHERE     DocumentId = t13.adReqId
                                                                            AND StudentId = @StudentId
                                                                )
                                           ,t2.Minscore
                                           ,ISNULL(t13.IsRequired,0) AS Required
                                           ,DocStatusDescrip = (
                                                                 SELECT TOP 1
                                                                        s1.DocStatusDescrip
                                                                 FROM   sySysDocStatuses s1
                                                                       ,syDocStatuses s2
                                                                       ,plStudentDocs s3
                                                                 WHERE  s1.SysDocStatusId = s2.sysDocStatusId
                                                                        AND s2.DocStatusId = s3.DocStatusId
                                                                        AND s3.StudentId = @StudentId
                                                                        AND s3.DocumentId = t13.adReqId
                                                               )
                                           ,ReqforEnrollment
                                           ,ReqforFinancialAid
                                           ,ReqforGraduation
                                           ,t1.CampGrpId
                                  FROM      adReqs t1
                                           ,adReqsEffectiveDates t2
                                           ,adReqLeadGroups t3
                                           ,adLeads t4
                                           ,adPrgVerTestDetails t5
                                           ,adReqGroups t11
                                           ,adReqGrpDef t13
                                           ,(
                                              SELECT    LeadGrpId
                                                       ,ReqGrpId
                                                       ,NumReqs
                                              FROM      adLeadGrpReqGroups
                                              WHERE     LeadGrpId IN ( SELECT DISTINCT
                                                                                LeadGrpId
                                                                       FROM     adLeadByLeadGroups
                                                                       WHERE    StuEnrollId IN ( SELECT StuEnrollId
                                                                                                 FROM   arStuEnrollments
                                                                                                 WHERE  StudentId = @StudentId ) )
                                            ) Z
                                  WHERE     t11.ReqGrpId = t5.ReqGrpId
                                            AND t1.StatusId = @ActiveStatusID
                                            AND t5.adReqId IS NULL
                                            AND t5.PrgVerId IN ( SELECT DISTINCT
                                                                        PrgVerId
                                                                 FROM   arStuEnrollments
                                                                 WHERE  StudentId = @StudentId )
                                            AND t4.PrgVerId = t5.PrgVerId
                                            AND t11.ReqGrpId = t13.ReqGrpId
                                            AND t13.adReqId = t2.adReqId
                                            AND t2.MandatoryRequirement <> 1
                                            AND t13.adReqId = t1.adReqId
                                            AND t1.adreqTypeId IN ( 3 )
                                            AND t2.adReqEffectiveDateId = t3.adReqEffectiveDateId
                                            AND t3.LeadGrpId IN ( SELECT DISTINCT
                                                                            LeadGrpId
                                                                  FROM      adLeadByLeadGroups
                                                                  WHERE     StuEnrollId IN ( SELECT StuEnrollId
                                                                                             FROM   arStuEnrollments
                                                                                             WHERE  StudentId = @StudentId ) )
                                            AND Z.LeadGrpId = t3.LeadGrpId
                                            AND Z.ReqGrpId = t11.ReqGrpId
                                            AND t13.LeadGrpId = Z.LeadGrpId
                                ) R1
                      WHERE     R1.CurrentDate >= R1.StartDate
                                AND (
                                      R1.CurrentDate <= R1.EndDate
                                      OR R1.EndDate IS NULL
                                    )
                      UNION
                      SELECT  DISTINCT
                                adReqId
                               ,Descrip
                               ,ReqGrpId
                               ,ReqGrpDescrip
                               ,NumRequired
                               ,StartDate
                               ,EndDate
                               ,Required
                               ,DocSubmittedCount
                               ,DocStatusDescrip
                               ,ReqforEnrollment
                               ,ReqforFinancialAid
                               ,ReqforGraduation
                               ,CampGrpId
                               ,COUNT(*) OVER ( PARTITION BY adReqId ) AS DupCount
                      FROM      (
                                  SELECT  DISTINCT
                                            t1.adReqId
                                           ,t1.Descrip
                                           ,'00000000-0000-0000-0000-000000000000' AS ReqGrpId
                                           ,'' AS ReqGrpDescrip
                                           ,0 AS NumRequired
                                           ,GETDATE() AS CurrentDate
                                           ,t2.StartDate
                                           ,t2.EndDate
                                           ,(
                                              SELECT    COUNT(*)
                                              FROM      plStudentDocs
                                              WHERE     DocumentId = t1.adReqId
                                                        AND StudentId = @StudentId
                                            ) AS DocSubmittedCount
                                           ,t2.Minscore
                                           ,ISNULL(t3.IsRequired,0) AS Required
                                           ,(
                                              SELECT TOP 1
                                                        s1.DocStatusDescrip
                                              FROM      sySysDocStatuses s1
                                                       ,syDocStatuses s2
                                                       ,plStudentDocs s3
                                              WHERE     s1.SysDocStatusId = s2.sysDocStatusId
                                                        AND s2.DocStatusId = s3.DocStatusId
                                                        AND s3.StudentId = @StudentId
                                                        AND s3.DocumentId = t1.adReqId
                                            ) AS DocStatusDescrip
                                           ,ReqforEnrollment
                                           ,ReqforFinancialAid
                                           ,ReqforGraduation
                                           ,t1.CampGrpId
                                  FROM      adReqs t1
                                           ,adReqsEffectiveDates t2
                                           ,adReqLeadGroups t3
                                  WHERE     t1.adReqId = t2.adReqId
                                            AND t2.adReqEffectiveDateId = t3.adReqEffectiveDateId
                                            AND t1.StatusId = @ActiveStatusID
                                            AND t1.adReqId NOT IN ( SELECT DISTINCT
                                                                            s1.adReqId
                                                                    FROM    adReqGrpDef s1
                                                                           ,adPrgVerTestDetails s2
                                                                    WHERE   s1.ReqGrpId = s2.ReqGrpId
                                                                            AND s2.PrgVerId IN ( SELECT DISTINCT
                                                                                                        PrgVerId
                                                                                                 FROM   arStuEnrollments
                                                                                                 WHERE  StudentId = @StudentId )
                                                                            AND s2.adReqId IS NULL )
                                            AND t3.LeadGrpId IN ( SELECT    LeadGrpId
                                                                  FROM      adLeadByLeadGroups
                                                                  WHERE     StuEnrollId IN ( SELECT DISTINCT
                                                                                                    StuEnrollId
                                                                                             FROM   arStuEnrollments
                                                                                             WHERE  StudentId = @StudentId ) )
                                            AND t1.adReqTypeId IN ( 3 )
                                            AND t2.MandatoryRequirement <> 1
                                            AND t1.adReqId NOT IN ( (
                                                                      SELECT DISTINCT
                                                                                adReqId
                                                                      FROM      adPrgVerTestDetails
                                                                      WHERE     PrgVerId IN ( SELECT DISTINCT
                                                                                                        PrgVerId
                                                                                              FROM      arStuEnrollments
                                                                                              WHERE     StudentId = @StudentId )
                                                                                AND adreqID IS NOT NULL
                                                                    )
                                                           )
                                ) R1
                      WHERE     R1.CurrentDate >= R1.StartDate
                                AND (
                                      R1.CurrentDate <= R1.EndDate
                                      OR R1.EndDate IS NULL
                                    )
                    ) R2
          WHERE     (
                      (
                        DupCount > 1
                        AND Required = 1
                      )
                      OR (
                           DupCount = 1
                           AND Required IN ( 0,1 )
                         )
                    )
                    AND R2.CampGrpId IN ( SELECT DISTINCT
                                                    t2.CampGrpId
                                          FROM      syCmpGrpCmps t1
                                                   ,syCampGrps t2
                                          WHERE     t1.CampGrpId = t2.CampGrpId
                                                    AND t1.CampusId = @CampusID
                                                    AND t2.StatusId = @ActiveStatusID )
                    AND ReqforFinancialAid = 1
        )
        UNION
        (
          SELECT  DISTINCT
                    adReqId AS DocumentId
                   ,Descrip AS DocumentDescrip
                   ,ReqGrpId
                   ,ReqGrpDescrip
                   ,NumRequired
                   ,StartDate
                   ,EndDate
                   ,Required
                   ,DocSubmittedCount
                   ,ReqforEnrollment
                   ,ReqforFinancialAid
                   ,ReqforGraduation
                   ,CASE WHEN DocStatusDescrip = 'Approved' THEN 'Approved'
                         ELSE 'Not Approved'
                    END AS DocStatusDescrip
          FROM      (
                      SELECT DISTINCT
                                adReqId
                               ,Descrip
                               ,ReqGrpId
                               ,ReqGrpDescrip
                               ,NumRequired
                               ,StartDate
                               ,EndDate
                               ,Required
                               ,DocSubmittedCount
                               ,DocStatusDescrip
                               ,ReqforEnrollment
                               ,ReqforFinancialAid
                               ,ReqforGraduation
                               ,CampGrpId
                               ,COUNT(*) OVER ( PARTITION BY adReqId ) AS DupCount
                      FROM      (
                                  SELECT DISTINCT
                                            t1.adReqId
                                           ,t1.Descrip
                                           ,'00000000-0000-0000-0000-000000000000' AS ReqGrpId
                                           ,'' AS ReqGrpDescrip
                                           ,0 AS NumRequired
                                           ,@StudentStartDate AS CurrentDate
                                           ,t2.StartDate
                                           ,t2.EndDate
                                           ,DocSubmittedCount = (
                                                                  SELECT    COUNT(*)
                                                                  FROM      dbo.adLeadDocsReceived
                                                                  WHERE     DocumentId = t1.adReqId
                                                                            AND LeadID = @LeadID
                                                                )
                                           ,t2.Minscore
                                           ,1 AS Required
                                           ,DocStatusDescrip = ( ISNULL((
                                                                          SELECT TOP 1
                                                                                    s1.DocStatusDescrip
                                                                          FROM      sySysDocStatuses s1
                                                                                   ,syDocStatuses s2
                                                                                   ,plStudentDocs s3
                                                                          WHERE     s1.SysDocStatusId = s2.sysDocStatusId
                                                                                    AND s2.DocStatusId = s3.DocStatusId
                                                                                    AND s3.StudentId = @StudentId
                                                                                    AND s3.DocumentId = t1.adReqId
                                                                        ),(
                                                                            SELECT DISTINCT
                                                                                    s1.DocStatusDescrip
                                                                            FROM    sySysDocStatuses s1
                                                                                   ,syDocStatuses s2
                                                                                   ,dbo.adLeadDocsReceived s3
                                                                            WHERE   s1.SysDocStatusId = s2.sysDocStatusId
                                                                                    AND s2.DocStatusId = s3.DocStatusId
                                                                                    AND s3.LeadID = @LeadID
                                                                                    AND s3.DocumentId = t1.adReqId
                                                                          )) )
                                           ,ReqforEnrollment
                                           ,ReqforFinancialAid
                                           ,ReqforGraduation
                                           ,t1.CampGrpId
                                  FROM      adReqs t1
                                           ,adReqsEffectiveDates t2
                                  WHERE     t1.adReqId = t2.adReqId
                                            AND t2.MandatoryRequirement = 1
                                            AND t1.adReqTypeId IN ( 3 )
                                            AND t1.StatusId = @ActiveStatusID
                                  UNION
                                  SELECT DISTINCT
                                            t3.adReqId
                                           ,t4.Descrip
                                           ,t1.ReqGrpId
                                           ,t1.Descrip AS ReqGrpDescrip
                                           ,NumRequired = (
                                                            SELECT  COUNT(*)
                                                            FROM    adReqGrpDef
                                                            WHERE   ReqGrpId = t1.ReqGrpId
                                                          )
                                           ,@StudentStartDate AS CurrentDate
                                           ,t2.StartDate
                                           ,t2.EndDate
                                           ,DocSubmittedCount = (
                                                                  SELECT    COUNT(*)
                                                                  FROM      dbo.adLeadDocsReceived
                                                                  WHERE     DocumentId = t3.adReqId
                                                                            AND LeadID = @LeadID
                                                                )
                                           ,t2.Minscore
                                           ,1 AS Required
                                           ,DocStatusDescrip = ( ISNULL((
                                                                          SELECT TOP 1
                                                                                    s1.DocStatusDescrip
                                                                          FROM      sySysDocStatuses s1
                                                                                   ,syDocStatuses s2
                                                                                   ,plStudentDocs s3
                                                                          WHERE     s1.SysDocStatusId = s2.sysDocStatusId
                                                                                    AND s2.DocStatusId = s3.DocStatusId
                                                                                    AND s3.StudentId = @StudentId
                                                                                    AND s3.DocumentId = t4.adReqId
                                                                        ),(
                                                                            SELECT TOP 1
                                                                                    s1.DocStatusDescrip
                                                                            FROM    sySysDocStatuses s1
                                                                                   ,syDocStatuses s2
                                                                                   ,dbo.adLeadDocsReceived s3
                                                                            WHERE   s1.SysDocStatusId = s2.sysDocStatusId
                                                                                    AND s2.DocStatusId = s3.DocStatusId
                                                                                    AND s3.LeadID = @LeadID
                                                                                    AND s3.DocumentId = t4.adReqId
                                                                          )) )
                                           ,ReqforEnrollment
                                           ,ReqforFinancialAid
                                           ,ReqforGraduation
                                           ,t4.CampGrpId
                                  FROM      adReqGroups t1
                                           ,adReqGrpDef t3
                                           ,adReqs t4
                                           ,adReqsEffectiveDates t2
                                  WHERE     t1.IsMandatoryReqGrp = 1
                                            AND t1.ReqGrpId = t3.ReqGrpId
                                            AND t3.adReqId = t2.adReqId
                                            AND t3.adReqId = t4.adReqId
                                            AND t4.adReqTypeId IN ( 3 )
                                            AND t4.StatusId = @ActiveStatusID
                                ) R1
                      WHERE     R1.CurrentDate >= R1.StartDate
                                AND (
                                      R1.CurrentDate <= R1.EndDate
                                      OR R1.EndDate IS NULL
                                    )
                      UNION
                      SELECT DISTINCT
                                adReqId
                               ,Descrip
                               ,reqGrpId
                               ,ReqGrpDescrip
                               ,NumRequired
                               ,StartDate
                               ,EndDate
                               ,Required
                               ,DocSubmittedCount
                               ,DocStatusDescrip
                               ,ReqforEnrollment
                               ,ReqforFinancialAid
                               ,ReqforGraduation
                               ,CampGrpId
                               ,COUNT(*) OVER ( PARTITION BY adReqId ) AS DupCount
                      FROM      (
                                  SELECT DISTINCT
                                            t1.adReqId
                                           ,t1.Descrip
                                           ,'00000000-0000-0000-0000-000000000000' AS reqGrpId
                                           ,'' AS ReqGrpDescrip
                                           ,0 AS NumRequired
                                           ,@StudentStartDate AS CurrentDate
                                           ,t2.StartDate
                                           ,t2.EndDate
                                           ,DocSubmittedCount = (
                                                                  SELECT    COUNT(*)
                                                                  FROM      dbo.adLeadDocsReceived
                                                                  WHERE     DocumentId = t1.adReqId
                                                                            AND LeadID = @LeadID
                                                                )
                                           ,t2.Minscore
                                           ,1 AS Required
                                           ,DocStatusDescrip = ( ISNULL((
                                                                          SELECT TOP 1
                                                                                    s1.DocStatusDescrip
                                                                          FROM      sySysDocStatuses s1
                                                                                   ,syDocStatuses s2
                                                                                   ,plStudentDocs s3
                                                                          WHERE     s1.SysDocStatusId = s2.sysDocStatusId
                                                                                    AND s2.DocStatusId = s3.DocStatusId
                                                                                    AND s3.StudentId = @StudentId
                                                                                    AND s3.DocumentId = t1.adReqId
                                                                        ),(
                                                                            SELECT TOP 1
                                                                                    s1.DocStatusDescrip
                                                                            FROM    sySysDocStatuses s1
                                                                                   ,syDocStatuses s2
                                                                                   ,dbo.adLeadDocsReceived s3
                                                                            WHERE   s1.SysDocStatusId = s2.sysDocStatusId
                                                                                    AND s2.DocStatusId = s3.DocStatusId
                                                                                    AND s3.LeadID = @LeadID
                                                                                    AND s3.DocumentId = t1.adReqId
                                                                          )) )
                                           ,ReqforEnrollment
                                           ,ReqforFinancialAid
                                           ,ReqforGraduation
                                           ,t1.CampGrpId
                                  FROM      adReqs t1
                                           ,adReqsEffectiveDates t2
                                           ,adReqLeadGroups t3
                                           ,adLeads t4
                                           ,adPrgVerTestDetails t5
                                  WHERE     t1.adReqId = t2.adReqId
                                            AND t2.MandatoryRequirement <> 1
                                            AND t1.StatusId = @ActiveStatusID
                                            AND t1.adreqTypeId IN ( 3 )
                                            AND t2.adReqEffectiveDateId = t3.adReqEffectiveDateId
                                            AND t4.PrgVerId = t5.PrgVerId
                                            AND t1.adReqId = t5.adReqId
                                            AND t5.PrgVerId IN ( SELECT DISTINCT
                                                                        PrgVerId
                                                                 FROM   arStuEnrollments
                                                                 WHERE  StudentId = @StudentId )    
                                            --AND t3.LeadGrpId IN (    
                                            --SELECT DISTINCT    
                                            --        LeadGrpId    
                                            --FROM    adLeadByLeadGroups    
                                            --WHERE   StuEnrollId IN (    
                                            --        SELECT  StuEnrollId    
                                            --        FROM    arStuEnrollments    
                                            --        WHERE   StudentId = @StudentId ) )    
                                            AND t5.adReqId IS NOT NULL
                                ) R1
                      WHERE     R1.CurrentDate >= R1.StartDate
                                AND (
                                      R1.CurrentDate <= R1.EndDate
                                      OR R1.EndDate IS NULL
                                    )
                      UNION
                      SELECT DISTINCT
                                adReqId
                               ,Descrip
                               ,ReqGrpId
                               ,ReqGrpDescrip
                               ,NumRequired
                               ,StartDate
                               ,EndDate
                               ,Required
                               ,DocSubmittedCount
                               ,DocStatusDescrip
                               ,ReqforEnrollment
                               ,ReqforFinancialAid
                               ,ReqforGraduation
                               ,CampGrpId
                               ,COUNT(*) OVER ( PARTITION BY adReqId ) AS DupCount
                      FROM      (
                                  SELECT DISTINCT
                                            t13.adReqId
                                           ,t1.Descrip
                                           ,t11.ReqGrpId
                                           ,t11.Descrip AS ReqGrpDescrip
                                           ,Z.NumReqs AS NumRequired
                                           ,@StudentStartDate AS CurrentDate
                                           ,t2.StartDate
                                           ,t2.EndDate
                                           ,DocSubmittedCount = (
                                                                  SELECT    COUNT(*)
                                                                  FROM      dbo.adLeadDocsReceived
                                                                  WHERE     DocumentId = t13.adReqId
                                                                            AND leadID = @LeadID
                                                                )
                                           ,t2.Minscore
                                           ,ISNULL(t13.IsRequired,0) AS Required
                                           ,DocStatusDescrip = ( ISNULL((
                                                                          SELECT TOP 1
                                                                                    s1.DocStatusDescrip
                                                                          FROM      sySysDocStatuses s1
                                                                                   ,syDocStatuses s2
                                                                                   ,plStudentDocs s3
                                                                          WHERE     s1.SysDocStatusId = s2.sysDocStatusId
                                                                                    AND s2.DocStatusId = s3.DocStatusId
                                                                                    AND s3.StudentId = @StudentId
                                                                                    AND s3.DocumentId = t1.adReqId
                                                                        ),(
                                                                            SELECT TOP 1
                                                                                    s1.DocStatusDescrip
                                                                            FROM    sySysDocStatuses s1
                                                                                   ,syDocStatuses s2
                                                                                   ,dbo.adLeadDocsReceived s3
                                                                            WHERE   s1.SysDocStatusId = s2.sysDocStatusId
                                                                                    AND s2.DocStatusId = s3.DocStatusId
                                                                                    AND s3.LeadID = @LeadID
                                                                                    AND s3.DocumentId = t1.adReqId
                                                                          )) )
                                           ,ReqforEnrollment
                                           ,ReqforFinancialAid
                                           ,ReqforGraduation
                                           ,t1.CampGrpId
                                  FROM      adReqs t1
                                           ,adReqsEffectiveDates t2
                                           ,adReqLeadGroups t3
                                           ,adLeads t4
                                           ,adPrgVerTestDetails t5
                                           ,adReqGroups t11
                                           ,adReqGrpDef t13
                                           ,(
                                              SELECT    LeadGrpId
                                                       ,ReqGrpId
                                                       ,NumReqs
                                              FROM      adLeadGrpReqGroups
                                              WHERE     LeadGrpId IN ( SELECT DISTINCT
                                                                                LeadGrpId
                                                                       FROM     adLeadByLeadGroups
                                                                       WHERE    StuEnrollId IN ( SELECT StuEnrollId
                                                                                                 FROM   arStuEnrollments
                                                                                                 WHERE  StudentId = @StudentId ) )
                                            ) Z
                                  WHERE     t11.ReqGrpId = t5.ReqGrpId
                                            AND t1.StatusId = @ActiveStatusID
                                            AND t5.adReqId IS NULL
                                            AND t5.PrgVerId IN ( SELECT DISTINCT
                                                                        PrgVerId
                                                                 FROM   arStuEnrollments
                                                                 WHERE  StudentId = @StudentId )
                                            AND t4.PrgVerId = t5.PrgVerId
                                            AND t11.ReqGrpId = t13.ReqGrpId
                                            AND t13.adReqId = t2.adReqId
                                            AND t2.MandatoryRequirement <> 1
                                            AND t13.adReqId = t1.adReqId
                                            AND t1.adreqTypeId IN ( 3 )
                                            AND t2.adReqEffectiveDateId = t3.adReqEffectiveDateId
                                            AND t3.LeadGrpId IN ( SELECT DISTINCT
                                                                            LeadGrpId
                                                                  FROM      adLeadByLeadGroups
                                                                  WHERE     StuEnrollId IN ( SELECT StuEnrollId
                                                                                             FROM   arStuEnrollments
                                                                                             WHERE  StudentId = @StudentId ) )
                                            AND Z.LeadGrpId = t3.LeadGrpId
                                            AND Z.ReqGrpId = t11.ReqGrpId
                                            AND t13.LeadGrpId = Z.LeadGrpId
                                ) R1
                      WHERE     R1.CurrentDate >= R1.StartDate
                                AND (
                                      R1.CurrentDate <= R1.EndDate
                                      OR R1.EndDate IS NULL
                                    )
                      UNION
                      SELECT  DISTINCT
                                adReqId
                               ,Descrip
                               ,ReqGrpId
                               ,ReqGrpDescrip
                               ,NumRequired
                               ,StartDate
                               ,EndDate
                               ,Required
                               ,DocSubmittedCount
                               ,DocStatusDescrip
                               ,ReqforEnrollment
                               ,ReqforFinancialAid
                               ,ReqforGraduation
                               ,CampGrpId
                               ,COUNT(*) OVER ( PARTITION BY adReqId ) AS DupCount
                      FROM      (
                                  SELECT  DISTINCT
                                            t1.adReqId
                                           ,t1.Descrip
                                           ,'00000000-0000-0000-0000-000000000000' AS ReqGrpId
                                           ,'' AS ReqGrpDescrip
                                           ,0 AS NumRequired
                                           ,@StudentStartDate AS CurrentDate
                                           ,t2.StartDate
                                           ,t2.EndDate
                                           ,(
                                              SELECT    COUNT(*)
                                              FROM      dbo.adLeadDocsReceived
                                              WHERE     DocumentId = t1.adReqId
                                                        AND LeadID = @LeadID
                                            ) AS DocSubmittedCount
                                           ,t2.Minscore
                                           ,ISNULL(t3.IsRequired,0) AS Required
                                           ,( ISNULL((
                                                       SELECT TOP 1
                                                                s1.DocStatusDescrip
                                                       FROM     sySysDocStatuses s1
                                                               ,syDocStatuses s2
                                                               ,plStudentDocs s3
                                                       WHERE    s1.SysDocStatusId = s2.sysDocStatusId
                                                                AND s2.DocStatusId = s3.DocStatusId
                                                                AND s3.StudentId = @StudentId
                                                                AND s3.DocumentId = t1.adReqId
                                                     ),(
                                                         SELECT TOP 1
                                                                s1.DocStatusDescrip
                                                         FROM   sySysDocStatuses s1
                                                               ,syDocStatuses s2
                                                               ,dbo.adLeadDocsReceived s3
                                                         WHERE  s1.SysDocStatusId = s2.sysDocStatusId
                                                                AND s2.DocStatusId = s3.DocStatusId
                                                                AND s3.LeadID = @LeadID
                                                                AND s3.DocumentId = t1.adReqId
                                                       )) ) AS DocStatusDescrip
                                           ,ReqforEnrollment
                                           ,ReqforFinancialAid
                                           ,ReqforGraduation
                                           ,t1.CampGrpId
                                  FROM      adReqs t1
                                           ,adReqsEffectiveDates t2
                                           ,adReqLeadGroups t3
                                  WHERE     t1.adReqId = t2.adReqId
                                            AND t2.adReqEffectiveDateId = t3.adReqEffectiveDateId
                                            AND t1.StatusId = @ActiveStatusID
                                            AND t1.adReqId NOT IN ( SELECT DISTINCT
                                                                            s1.adReqId
                                                                    FROM    adReqGrpDef s1
                                                                           ,adPrgVerTestDetails s2
                                                                    WHERE   s1.ReqGrpId = s2.ReqGrpId
                                                                            AND s2.PrgVerId IN ( SELECT DISTINCT
                                                                                                        PrgVerId
                                                                                                 FROM   arStuEnrollments
                                                                                                 WHERE  StudentId = @StudentId )
                                                                            AND s2.adReqId IS NULL )
                                            AND t3.LeadGrpId IN ( SELECT    LeadGrpId
                                                                  FROM      adLeadByLeadGroups
                                                                  WHERE     StuEnrollId IN ( SELECT DISTINCT
                                                                                                    StuEnrollId
                                                                                             FROM   arStuEnrollments
                                                                                             WHERE  StudentId = @StudentId ) )
                                            AND t1.adReqTypeId IN ( 3 )
                                            AND t2.MandatoryRequirement <> 1
                                            AND t1.adReqId NOT IN ( (
                                                                      SELECT DISTINCT
                                                                                adReqId
                                                                      FROM      adPrgVerTestDetails
                                                                      WHERE     PrgVerId IN ( SELECT DISTINCT
                                                                                                        PrgVerId
                                                                                              FROM      arStuEnrollments
                                                                                              WHERE     StudentId = @StudentId )
                                                                                AND adreqID IS NOT NULL
                                                                    )
                                                           )
                                ) R1
                      WHERE     R1.CurrentDate >= R1.StartDate
                                AND (
                                      R1.CurrentDate <= R1.EndDate
                                      OR R1.EndDate IS NULL
                                    )
                    ) R2
          WHERE     (
                      (
                        DupCount > 1
                        AND Required = 1
                      )
                      OR (
                           DupCount = 1
                           AND Required IN ( 0,1 )
                         )
                    )
                    AND R2.CampGrpId IN ( SELECT DISTINCT
                                                    t2.CampGrpId
                                          FROM      syCmpGrpCmps t1
                                                   ,syCampGrps t2
                                          WHERE     t1.CampGrpId = t2.CampGrpId
                                                    AND t1.CampusId = @CampusID
                                                    AND t2.StatusId = @ActiveStatusID )
                    AND ( ReqforEnrollment = 1 )
        );      
       
       
       
    END;    
       
       




GO
