--=================================================================================================
-- USP_STUDENTGPA_GETDETAILS
--=================================================================================================
CREATE PROCEDURE dbo.USP_STUDENTGPA_GETDETAILS
    @CampusId UNIQUEIDENTIFIER
   ,@PrgVerId VARCHAR(8000) = NULL
   ,@pTermId VARCHAR(8000) = NULL
   ,@EnrollmentStatusId VARCHAR(8000) = NULL
   ,@StudentGrpId VARCHAR(8000) = NULL
   ,@CumulativeGPAGT DECIMAL(18, 2) = NULL
   ,@CumulativeGPALT DECIMAL(18, 2) = NULL
   ,@termGPAGT DECIMAL(18, 2) = NULL
   ,@termGPALT DECIMAL(18, 2) = NULL
   ,@termCreditsRangeGT DECIMAL(18, 2) = NULL
   ,@termCreditsRangeLT DECIMAL(18, 2) = NULL

/*
-- Note:
-- Review Average Process. already was updated in other SP 
-- Pending changes

*/
AS
    BEGIN

        DECLARE @CumulativeGPA_Range DECIMAL(18, 2);
        DECLARE @GradeCourseRepetitionsMethod VARCHAR(50);
        DECLARE @cumSimpleCourses INTEGER;
        DECLARE @cumSimpleCourseCredits DECIMAL(18, 2);
        DECLARE @cumSimple_GPA_Credits DECIMAL(18, 2);
        DECLARE @cumSimpleGPA DECIMAL(18, 2);
        DECLARE @cumSimpleCourses_OverAll INTEGER;
        DECLARE @cumSimple_GPA_Credits_OverAll DECIMAL(18, 2);
        DECLARE @cumSimpleGPA_OverAll DECIMAL(18, 2);
        DECLARE @times INT;
        DECLARE @counter INT;
        DECLARE @StudentId VARCHAR(50);
        DECLARE @StuEnrollId VARCHAR(50);
        DECLARE @cumCourseCredits DECIMAL(18, 2);
        DECLARE @cumWeighted_GPA_Credits DECIMAL(18, 2);
        DECLARE @cumWeightedGPA DECIMAL(18, 2);
        DECLARE @EquivCourses_SA_CC INT;
        DECLARE @EquivCourse_SA_GPA DECIMAL(18, 2);
        DECLARE @EquivCourses_SA_CC_OverAll INT;
        DECLARE @EquivCourse_SA_GPA_OverAll DECIMAL(18, 2);
        DECLARE @EquivCourse_WGPA_CC1 INT;
        DECLARE @EquivCourse_WGPA_GPA1 DECIMAL(18, 2);
        DECLARE @GPAMethod AS VARCHAR(1000);
        DECLARE @GradesFormat AS VARCHAR(1000);

        SET @GPAMethod = dbo.GetAppSettingValueByKeyName('GPAMethod', @CampusId);
        IF ( @GPAMethod IS NOT NULL )
            BEGIN
                SET @GPAMethod = LOWER(LTRIM(RTRIM(@GPAMethod)));
            END;
        SET @GradesFormat = dbo.GetAppSettingValueByKeyName('GradesFormat', @CampusId);
        IF ( @GradesFormat IS NOT NULL )
            BEGIN
                SET @GradesFormat = LOWER(LTRIM(RTRIM(@GradesFormat)));
            END;

        IF EXISTS (
                  SELECT 1
                  FROM   tempdb.dbo.sysobjects AS O
                  WHERE  O.xtype IN ( 'U' )
                         AND O.id = OBJECT_ID(N'tempdb..#PrgVersionsList')
                  )
            BEGIN
                DROP TABLE #PrgVersionsList;
            END;
        CREATE TABLE #PrgVersionsList
            (
                PrgVerId UNIQUEIDENTIFIER NOT NULL
               ,CONSTRAINT PK_#PrgVersionsList
                    PRIMARY KEY CLUSTERED ( PrgVerId ASC ) ON [PRIMARY]
            );
        IF EXISTS (
                  SELECT 1
                  FROM   tempdb.dbo.sysobjects AS O
                  WHERE  O.xtype IN ( 'U' )
                         AND O.id = OBJECT_ID(N'tempdb..#EnrollmentStatusList')
                  )
            BEGIN
                DROP TABLE #EnrollmentStatusList;
            END;
        CREATE TABLE #EnrollmentStatusList
            (
                StatusCodeId UNIQUEIDENTIFIER NOT NULL
               ,CONSTRAINT PK_#EnrollmentStatusList
                    PRIMARY KEY CLUSTERED ( StatusCodeId ASC ) ON [PRIMARY]
            );
        IF EXISTS (
                  SELECT 1
                  FROM   tempdb.dbo.sysobjects AS O
                  WHERE  O.xtype IN ( 'U' )
                         AND O.id = OBJECT_ID(N'tempdb..#LeadGroupsList')
                  )
            BEGIN
                DROP TABLE #LeadGroupsList;
            END;
        CREATE TABLE #LeadGroupsList
            (
                LeadGrpId UNIQUEIDENTIFIER NOT NULL
               ,CONSTRAINT PK_#LeadGroupsList
                    PRIMARY KEY CLUSTERED ( LeadGrpId ASC ) ON [PRIMARY]
            );
        IF EXISTS (
                  SELECT 1
                  FROM   tempdb.dbo.sysobjects AS O
                  WHERE  O.xtype IN ( 'U' )
                         AND O.id = OBJECT_ID(N'tempdb..#TermIdList')
                  )
            BEGIN
                DROP TABLE #TermIdList;
            END;
        CREATE TABLE #TermIdList
            (
                TermId UNIQUEIDENTIFIER NOT NULL
               ,CONSTRAINT PK_#TermIdList
                    PRIMARY KEY CLUSTERED ( TermId ASC ) ON [PRIMARY]
            );
        IF EXISTS (
                  SELECT 1
                  FROM   tempdb.dbo.sysobjects AS O
                  WHERE  O.xtype IN ( 'U' )
                         AND O.id = OBJECT_ID(N'tempdb..#CoursesNotRepeated')
                  )
            BEGIN
                DROP TABLE #CoursesNotRepeated;
            END;
        CREATE TABLE #CoursesNotRepeated
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,TermId UNIQUEIDENTIFIER
               ,TermDescrip VARCHAR(100)
               ,ReqId UNIQUEIDENTIFIER
               ,ReqDescrip VARCHAR(100)
               ,ClsSectionId VARCHAR(50)
               ,CreditsEarned DECIMAL(18, 2)
               ,CreditsAttempted DECIMAL(18, 2)
               ,CurrentScore DECIMAL(18, 2)
               ,CurrentGrade VARCHAR(10)
               ,FinalScore DECIMAL(18, 2)
               ,FinalGrade VARCHAR(10)
               ,Completed BIT
               ,FinalGPA DECIMAL(18, 2)
               ,Product_WeightedAverage_Credits_GPA DECIMAL(18, 2)
               ,Count_WeightedAverage_Credits DECIMAL(18, 2)
               ,Product_SimpleAverage_Credits_GPA DECIMAL(18, 2)
               ,Count_SimpleAverage_Credits DECIMAL(18, 2)
               ,ModUser VARCHAR(50)
               ,ModDate DATETIME
               ,TermGPA_Simple DECIMAL(18, 2)
               ,TermGPA_Weighted DECIMAL(18, 2)
               ,coursecredits DECIMAL(18, 2)
               ,CumulativeGPA DECIMAL(18, 2)
               ,CumulativeGPA_Simple DECIMAL(18, 2)
               ,FACreditsEarned DECIMAL(18, 2)
               ,Average DECIMAL(18, 2)
               ,CumAverage DECIMAL(18, 2)
               ,TermStartDate DATETIME
               ,rownumber INT NULL
               ,StudentId UNIQUEIDENTIFIER
            );
        IF EXISTS (
                  SELECT 1
                  FROM   tempdb.dbo.sysobjects AS O
                  WHERE  O.xtype IN ( 'U' )
                         AND O.id = OBJECT_ID(N'tempdb..#tmpCoursesNotRepeatedOnlyIds')
                  )
            BEGIN
                -- ALTER TABLE tempdb..#tmpCoursesNotRepeatedOnlyIds 
                --     DROP CONSTRAINT PK_#tmpCoursesNotRepeatedOnlyIds 
                DROP TABLE #tmpCoursesNotRepeatedOnlyIds;
            END;
        CREATE TABLE #tmpCoursesNotRepeatedOnlyIds
            (
                StuEnrollId UNIQUEIDENTIFIER NOT NULL
               ,TermId UNIQUEIDENTIFIER NOT NULL
               ,CONSTRAINT PK_#tmpCoursesNotRepeatedOnlyIds
                    PRIMARY KEY CLUSTERED
                        (
                        StuEnrollId ASC
                       ,TermId ASC
                        ) ON [PRIMARY]
            );
        IF EXISTS (
                  SELECT 1
                  FROM   tempdb.dbo.sysobjects AS O
                  WHERE  O.xtype IN ( 'U' )
                         AND O.id = OBJECT_ID(N'tempdb..#getStudentTerms')
                  )
            BEGIN
                DROP TABLE #getStudentTerms;
            END;
        CREATE TABLE #getStudentTerms
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,TermId UNIQUEIDENTIFIER
               ,TermDescrip VARCHAR(50)
               ,TermCredits DECIMAL(18, 2) NULL
               ,TermSimpleCourses INTEGER NULL
               ,TermSimple_GPA_Credits DECIMAL(18, 2) NULL
               ,TermEquivCourse_SA_CC INTEGER NULL
               ,TermEquivCourse_SA_GPA DECIMAL(18, 2) NULL
               ,TermSimpleGPA DECIMAL(18, 2) NULL
               ,TermCourseCredits DECIMAL(18, 2) NULL
               ,TermWeighted_GPA_Credits DECIMAL(18, 2) NULL
               ,TermEquivCourse_WGPA_CC1 DECIMAL(18, 2) NULL
               ,TermEquivCourse_WGPA_GPA1 DECIMAL(18, 2) NULL
               ,TermWeightedGPA DECIMAL(18, 2) NULL
               ,TermAverageCount INTEGER NULL
               ,TermAverageSum DECIMAL(18, 2) NULL
               ,TermAverage DECIMAL(18, 2) NULL
               ,TermStartDate DATETIME
            );
        IF EXISTS (
                  SELECT 1
                  FROM   tempdb.dbo.sysobjects AS O
                  WHERE  O.xtype IN ( 'U' )
                         AND O.id = OBJECT_ID(N'tempdb..#getStudentEnrollments')
                  )
            BEGIN
                DROP TABLE #getStudentEnrollments;
            END;
        CREATE TABLE #getStudentEnrollments
            (
                LastName VARCHAR(50)
               ,FirstName VARCHAR(50)
               ,StudentId UNIQUEIDENTIFIER
               ,StudentNumber VARCHAR(50)
               ,StuEnrollId UNIQUEIDENTIFIER
               ,PrgVerId UNIQUEIDENTIFIER
               ,ProgramVersion VARCHAR(50)
               ,PrgVersionTrackCredits BIT
               ,PrgVers_EquivCourses_SA_CC INTEGER           --@EquivCourses_SA_CC
               ,PrgVers_SimpleCourseCredits INTEGER          --@cumSimpleCourseCredits
               ,PrgVers_EquivCourse_SA_GPA DECIMAL(18, 2)    --@EquivCourse_SA_GPA
               ,PrgVers_Simple_GPA_Credits DECIMAL(18, 2)    --@cumSimple_GPA_Credits
               ,PrgVers_SimpleGPA DECIMAL(18, 2)             --@cumSimpleGPA
               ,PrgVers_EquivCourse_WGPA_CC1 DECIMAL(18, 2)  --@EquivCourse_WGPA_CC1
               ,PrgVers_CourseCredits DECIMAL(18, 2)         --@cumCourseCredits
               ,PrgVers_EquivCourse_WGPA_GPA1 DECIMAL(18, 2) --@EquivCourse_WGPA_GPA1
               ,PrgVers_Weighted_GPA_Credits DECIMAL(18, 2)  --@cumWeighted_GPA_Credits
               ,PrgVers_WeightedGPA DECIMAL(18, 2)           --@cumWeightedGPA
               ,PrgVers_AverageCount INTEGER
               ,PrgVers_AverageSum DECIMAL(18, 2)
               ,PrgVers_Average DECIMAL(18, 2)
               ,OverAll_EquivCourses_SA_CC INTEGER           --@EquivCourses_SA_CC_OverAll
               ,OverAll_SimpleCourses INTEGER                --@cumSimpleCourses_OverAll
               ,OverAll_EquivCourse_SA_GPA DECIMAL(18, 2)    --@EquivCourse_SA_GPA_OverAll
               ,OverAll_Simple_GPA_Credits DECIMAL(18, 2)    --@cumSimple_GPA_Credits_OverAll
               ,OverAll_SimpleGPA DECIMAL(18, 2)             --@cumSimpleGPA_OverAll
               ,OverAll_EquivCourse_WGPA_CC1 DECIMAL(18, 2)  --@EquivCourse_WGPA_CC1_OverAll
               ,OverAll_CourseCredits DECIMAL(18, 2)         --@cumCourseCredits_OverAll
               ,OverAll_EquivCourse_WGPA_GPA1 DECIMAL(18, 2) --@EquivCourse_WGPA_GPA1_OverAll
               ,OverAll_Weighted_GPA_Credits DECIMAL(18, 2)  --@cumWeighted_GPA_Credits_OverAll
               ,OverAll_WeightedGPA DECIMAL(18, 2)           --@cumWeightedGPA_OverAll
               ,OverAll_AverageCount INTEGER
               ,OverAll_AverageSum DECIMAL(18, 2)
               ,OverAll_Average DECIMAL(18, 2)
               ,SSN VARCHAR(11)
               ,EnrollmentID VARCHAR(50)
               ,MiddleName VARCHAR(50)
               ,TermId UNIQUEIDENTIFIER
               ,TermDescription VARCHAR(50)
               ,TermStartDate DATETIME
               ,rowNumber INT
            );
        IF EXISTS (
                  SELECT 1
                  FROM   tempdb.dbo.sysobjects AS O
                  WHERE  O.xtype IN ( 'U' )
                         AND O.id = OBJECT_ID(N'tempdb..#tmpStudentEnrollmentsOnlyIds')
                  )
            BEGIN
                DROP TABLE #tmpStudentEnrollmentsOnlyIds;
            END;
        CREATE TABLE #tmpStudentEnrollmentsOnlyIds
            (
                StudentId UNIQUEIDENTIFIER NOT NULL
               ,StuEnrollId UNIQUEIDENTIFIER NOT NULL
               ,CONSTRAINT PK_#tmpStudentEnrollmentsOnlyIds
                    PRIMARY KEY CLUSTERED ( StuEnrollId ASC ) ON [PRIMARY]
            );
        IF EXISTS (
                  SELECT 1
                  FROM   tempdb.dbo.sysobjects AS O
                  WHERE  O.xtype IN ( 'U' )
                         AND O.id = OBJECT_ID(N'tempdb..#tmpEquivalentCoursesTakenByStudentInTerm')
                  )
            BEGIN
                DROP TABLE #tmpEquivalentCoursesTakenByStudentInTerm;
            END;
        CREATE TABLE #tmpEquivalentCoursesTakenByStudentInTerm
            (
                EquivReqId UNIQUEIDENTIFIER NOT NULL
               ,StuEnrollId UNIQUEIDENTIFIER NOT NULL
            );
        IF EXISTS (
                  SELECT 1
                  FROM   tempdb.dbo.sysobjects AS O
                  WHERE  O.xtype IN ( 'U' )
                         AND O.id = OBJECT_ID(N'tempdb..#tmpStudentsWhoHasGradedEquivalentCourseInTerm')
                  )
            BEGIN
                DROP TABLE #tmpStudentsWhoHasGradedEquivalentCourseInTerm;
            END;
        CREATE TABLE #tmpStudentsWhoHasGradedEquivalentCourseInTerm
            (
                EquivReqId UNIQUEIDENTIFIER NOT NULL
               ,StuEnrollId UNIQUEIDENTIFIER NOT NULL
               ,TermId UNIQUEIDENTIFIER NOT NULL
            );
        IF EXISTS (
                  SELECT 1
                  FROM   tempdb.dbo.sysobjects AS O
                  WHERE  O.xtype IN ( 'U' )
                         AND O.id = OBJECT_ID(N'tempdb..#tmpEquivalentCoursesTakenByStudent')
                  )
            BEGIN
                DROP TABLE #tmpEquivalentCoursesTakenByStudent;
            END;
        CREATE TABLE #tmpEquivalentCoursesTakenByStudent
            (
                EquivReqId UNIQUEIDENTIFIER NOT NULL
               ,StuEnrollId UNIQUEIDENTIFIER NOT NULL
            );
        IF EXISTS (
                  SELECT 1
                  FROM   tempdb.dbo.sysobjects AS O
                  WHERE  O.xtype IN ( 'U' )
                         AND O.id = OBJECT_ID(N'tempdb..#tmpStudentsWhoHasGradedEquivalentCourse')
                  )
            BEGIN
                DROP TABLE #tmpStudentsWhoHasGradedEquivalentCourse;
            END;
        CREATE TABLE #tmpStudentsWhoHasGradedEquivalentCourse
            (
                EquivReqId UNIQUEIDENTIFIER NOT NULL
               ,StuEnrollId UNIQUEIDENTIFIER NOT NULL
            );
        IF EXISTS (
                  SELECT 1
                  FROM   tempdb.dbo.sysobjects AS O
                  WHERE  O.xtype IN ( 'U' )
                         AND O.id = OBJECT_ID(N'tempdb..#tmpCreditSummary')
                  )
            BEGIN
                DROP TABLE #tmpCreditSummary;
            END;
        CREATE TABLE #tmpCreditSummary
            (
                ReqId UNIQUEIDENTIFIER
               ,StuEnrollId UNIQUEIDENTIFIER
               ,Counted INTEGER
            );

        IF @PrgVerId IS NULL
           OR @PrgVerId = ''
            BEGIN
                INSERT INTO #PrgVersionsList
                            SELECT APV.PrgVerId
                            FROM   arPrgVersions AS APV;
            END;
        ELSE
            BEGIN
                INSERT INTO #PrgVersionsList
                            SELECT Val
                            FROM   MultipleValuesForReportParameters(@PrgVerId, ',', 1);
            END;
        IF @EnrollmentStatusId IS NULL
           OR @EnrollmentStatusId = ''
            BEGIN
                INSERT INTO #EnrollmentStatusList
                            SELECT SSC.StatusCodeId
                            FROM   syStatusCodes AS SSC;
            END;
        ELSE
            BEGIN
                INSERT INTO #EnrollmentStatusList
                            SELECT Val
                            FROM   MultipleValuesForReportParameters(@EnrollmentStatusId, ',', 1);
            END;
        IF @StudentGrpId IS NULL
           OR @StudentGrpId = ''
            BEGIN
                INSERT INTO #LeadGroupsList
                            SELECT ALG.LeadGrpId
                            FROM   adLeadGroups AS ALG;
            END;
        ELSE
            BEGIN
                INSERT INTO #LeadGroupsList
                            SELECT Val
                            FROM   MultipleValuesForReportParameters(@StudentGrpId, ',', 1);
            END;
        -- When term is null --> means Only Show information of Lastest term for the student
        IF (
           @pTermId IS NULL
           OR @pTermId = ''
           )
            BEGIN
                INSERT INTO #TermIdList
                            SELECT AT.TermId
                            FROM   arTerm AS AT;
            END;
        ELSE
            BEGIN
                INSERT INTO #TermIdList
                            SELECT Val
                            FROM   MultipleValuesForReportParameters(@pTermId, ',', 1);
            END;

        SET @GradeCourseRepetitionsMethod = (
                                            SELECT TOP 1 Value
                                            FROM   dbo.syConfigAppSetValues t1
                                            INNER JOIN dbo.syConfigAppSettings t2 ON t1.SettingId = t2.SettingId
                                                                                     AND t2.KeyName = 'GradeCourseRepetitionsMethod'
                                            );
        SET @GradeCourseRepetitionsMethod = LOWER(LTRIM(RTRIM(@GradeCourseRepetitionsMethod)));

        INSERT INTO #getStudentTerms
                    SELECT R.StuEnrollId
                          ,T.TermId
                          ,T.TermDescrip
                          ,NULL
                          ,NULL
                          ,NULL
                          ,NULL
                          ,NULL
                          ,NULL
                          ,NULL
                          ,NULL
                          ,NULL
                          ,NULL
                          ,NULL
                          ,NULL
                          ,NULL
                          ,NULL
                          ,T.StartDate
                    FROM   arResults R
                    INNER JOIN arClassSections CS ON R.TestId = CS.ClsSectionId
                    INNER JOIN arTerm T ON T.TermId = CS.TermId
                    INNER JOIN arStuEnrollments SE ON SE.StuEnrollId = R.StuEnrollId
                    INNER JOIN #PrgVersionsList AS PVL ON PVL.PrgVerId = SE.PrgVerId
                    INNER JOIN dbo.adLeadByLeadGroups LLG ON LLG.StuEnrollId = SE.StuEnrollId
                    INNER JOIN #LeadGroupsList AS LGL ON LGL.LeadGrpId = LLG.LeadGrpId
                    INNER JOIN #EnrollmentStatusList AS ESL ON ESL.StatusCodeId = SE.StatusCodeId
                    WHERE  SE.CampusId = @CampusId
                    UNION
                    SELECT TG.StuEnrollId
                          ,T.TermId
                          ,T.TermDescrip
                          ,NULL
                          ,NULL
                          ,NULL
                          ,NULL
                          ,NULL
                          ,NULL
                          ,NULL
                          ,NULL
                          ,NULL
                          ,NULL
                          ,NULL
                          ,NULL
                          ,NULL
                          ,NULL
                          ,T.StartDate
                    FROM   arTransferGrades TG
                    INNER JOIN arTerm T ON T.TermId = TG.TermId
                    INNER JOIN arStuEnrollments SE ON SE.StuEnrollId = TG.StuEnrollId
                    INNER JOIN dbo.adLeadByLeadGroups LLG ON LLG.StuEnrollId = SE.StuEnrollId
                    INNER JOIN #PrgVersionsList AS PVL2 ON PVL2.PrgVerId = SE.PrgVerId
                    INNER JOIN #EnrollmentStatusList AS ESL ON ESL.StatusCodeId = SE.StatusCodeId
                    INNER JOIN #LeadGroupsList AS LGL ON LGL.LeadGrpId = LLG.LeadGrpId
                    WHERE  SE.CampusId = @CampusId;

        INSERT INTO #getStudentEnrollments
                    SELECT S.LastName
                          ,S.FirstName
                          ,S.StudentId
                          ,S.StudentNumber
                          ,SE.StuEnrollId
                          ,APV.PrgVerId
                          ,APV.PrgVerDescrip
                          ,CASE WHEN ( APV.Credits > 0.0 ) THEN 1
                                ELSE 0
                           END AS PrgVersionTrackCredits
                          ,NULL AS PrgVers_EquivCourses_SA_CC
                          ,NULL AS PrgVers_SimpleCourseCredits
                          ,NULL AS PrgVers_EquivCourse_SA_GPA
                          ,NULL AS PrgVers_Simple_GPA_Credits
                          ,NULL AS PrgVers_SimpleGPA
                          ,NULL AS PrgVers_EquivCourse_WGPA_CC1
                          ,NULL AS PrgVers_CourseCredits
                          ,NULL AS PrgVers_EquivCourse_WGPA_GPA1
                          ,NULL AS PrgVers_Weighted_GPA_Credits
                          ,NULL AS PrgVers_WeightedGPA
                          ,NULL AS PrgVers_AverageCount
                          ,NULL AS PrgVers_AverageSum
                          ,NULL AS PrgVers_Average
                          ,NULL AS OverAll_EquivCourses_SA_CC
                          ,NULL AS OverAll_SimpleCourses
                          ,NULL AS OverAll_EquivCourse_SA_GPA
                          ,NULL AS OverAll_Simple_GPA_Credits
                          ,NULL AS OverAll_SimpleGPA
                          ,NULL AS OverAll_EquivCourse_WGPA_CC1
                          ,NULL AS OverAll_CourseCredits
                          ,NULL AS OverAll_EquivCourse_WGPA_GPA1
                          ,NULL AS OverAll_Weighted_GPA_Credits
                          ,NULL AS OverAll_WeightedGPA
                          ,NULL AS OverAll_AverageCount
                          ,NULL AS OverAll_AverageSum
                          ,NULL AS OverAll_Average
                          ,S.SSN
                          ,SE.EnrollmentId
                          ,S.MiddleName
                          ,GST.TermId
                          ,GST.TermDescrip
                          ,GST.TermStartDate
                          ,ROW_NUMBER() OVER ( PARTITION BY SE.StuEnrollId
                                               ORDER BY GST.TermStartDate DESC
                                             ) AS RowNumber
                    FROM   arStudent S
                    INNER JOIN arStuEnrollments SE ON S.StudentId = SE.StudentId
                    INNER JOIN #EnrollmentStatusList AS ESL ON ESL.StatusCodeId = SE.StatusCodeId
                    INNER JOIN dbo.arPrgVersions APV ON SE.PrgVerId = APV.PrgVerId
                    INNER JOIN #getStudentTerms GST ON GST.StuEnrollId = SE.StuEnrollId
                    INNER JOIN #PrgVersionsList AS PVL ON PVL.PrgVerId = SE.PrgVerId
                    LEFT OUTER JOIN adLeadByLeadGroups LLG ON SE.StuEnrollId = LLG.StuEnrollId
                    LEFT OUTER JOIN #LeadGroupsList AS LGL ON LGL.LeadGrpId = LLG.LeadGrpId
                    WHERE  SE.CampusId = @CampusId;

        INSERT INTO #tmpStudentEnrollmentsOnlyIds
                    SELECT DISTINCT GSE.StudentId
                          ,GSE.StuEnrollId
                    FROM   #getStudentEnrollments AS GSE;

        INSERT INTO #tmpCreditSummary
                    SELECT   SCS.ReqId
                            ,SCS.StuEnrollId
                            ,COUNT(*) AS Counted
                    FROM     syCreditSummary AS SCS
                    INNER JOIN #tmpStudentEnrollmentsOnlyIds AS GSE ON GSE.StuEnrollId = SCS.StuEnrollId
                    GROUP BY SCS.StuEnrollId
                            ,SCS.ReqId
                    ORDER BY SCS.StuEnrollId
                            ,SCS.ReqId;



        INSERT INTO #CoursesNotRepeated
                    SELECT SCS.StuEnrollId
                          ,SCS.TermId
                          ,SCS.TermDescrip
                          ,SCS.ReqId
                          ,SCS.ReqDescrip
                          ,SCS.ClsSectionId
                          ,SCS.CreditsEarned
                          ,SCS.CreditsAttempted
                          ,SCS.CurrentScore
                          ,SCS.CurrentGrade
                          ,SCS.FinalScore
                          ,SCS.FinalGrade
                          ,SCS.Completed
                          ,SCS.FinalGPA
                          ,SCS.Product_WeightedAverage_Credits_GPA
                          ,SCS.Count_WeightedAverage_Credits
                          ,SCS.Product_SimpleAverage_Credits_GPA
                          ,SCS.Count_SimpleAverage_Credits
                          ,SCS.ModUser
                          ,SCS.ModDate
                          ,SCS.TermGPA_Simple
                          ,SCS.TermGPA_Weighted
                          ,SCS.coursecredits
                          ,SCS.CumulativeGPA
                          ,SCS.CumulativeGPA_Simple
                          ,SCS.FACreditsEarned
                          ,SCS.Average
                          ,SCS.CumAverage
                          ,SCS.TermStartDate
                          ,0 AS rownumber
                          ,GSE.StudentId
                    FROM   syCreditSummary AS SCS
                    INNER JOIN #tmpCreditSummary AS TCS ON TCS.ReqId = SCS.ReqId
                                                           AND TCS.StuEnrollId = SCS.StuEnrollId
                    INNER JOIN #tmpStudentEnrollmentsOnlyIds AS GSE ON GSE.StuEnrollId = TCS.StuEnrollId
                                                                       AND TCS.Counted = 1
                                                                       AND (
                                                                           FinalScore IS NOT NULL
                                                                           OR FinalGrade IS NOT NULL
                                                                           );

        IF @GradeCourseRepetitionsMethod = 'latest'
            BEGIN
                INSERT INTO #CoursesNotRepeated
                            SELECT dt2.StuEnrollId
                                  ,dt2.TermId
                                  ,dt2.TermDescrip
                                  ,dt2.ReqId
                                  ,dt2.ReqDescrip
                                  ,dt2.ClsSectionId
                                  ,dt2.CreditsEarned
                                  ,dt2.CreditsAttempted
                                  ,dt2.CurrentScore
                                  ,dt2.CurrentGrade
                                  ,dt2.FinalScore
                                  ,dt2.FinalGrade
                                  ,dt2.Completed
                                  ,dt2.FinalGPA
                                  ,dt2.Product_WeightedAverage_Credits_GPA
                                  ,dt2.Count_WeightedAverage_Credits
                                  ,dt2.Product_SimpleAverage_Credits_GPA
                                  ,dt2.Count_SimpleAverage_Credits
                                  ,dt2.ModUser
                                  ,dt2.ModDate
                                  ,dt2.TermGPA_Simple
                                  ,dt2.TermGPA_Weighted
                                  ,dt2.coursecredits
                                  ,dt2.CumulativeGPA
                                  ,dt2.CumulativeGPA_Simple
                                  ,dt2.FACreditsEarned
                                  ,dt2.Average
                                  ,dt2.CumAverage
                                  ,dt2.TermStartDate
                                  ,dt2.RowNumber
                                  ,dt2.StudentId
                            FROM   (
                                   SELECT SCS.StuEnrollId
                                         ,SCS.TermId
                                         ,SCS.TermDescrip
                                         ,SCS.ReqId
                                         ,SCS.ReqDescrip
                                         ,SCS.ClsSectionId
                                         ,SCS.CreditsEarned
                                         ,SCS.CreditsAttempted
                                         ,SCS.CurrentScore
                                         ,SCS.CurrentGrade
                                         ,SCS.FinalScore
                                         ,SCS.FinalGrade
                                         ,SCS.Completed
                                         ,SCS.FinalGPA
                                         ,SCS.Product_WeightedAverage_Credits_GPA
                                         ,SCS.Count_WeightedAverage_Credits
                                         ,SCS.Product_SimpleAverage_Credits_GPA
                                         ,SCS.Count_SimpleAverage_Credits
                                         ,SCS.ModUser
                                         ,SCS.ModDate
                                         ,SCS.TermGPA_Simple
                                         ,SCS.TermGPA_Weighted
                                         ,SCS.coursecredits
                                         ,SCS.CumulativeGPA
                                         ,SCS.CumulativeGPA_Simple
                                         ,SCS.FACreditsEarned
                                         ,SCS.Average
                                         ,SCS.CumAverage
                                         ,SCS.TermStartDate
                                         ,ROW_NUMBER() OVER ( PARTITION BY SCS.StuEnrollId
                                                                          ,SCS.ReqId
                                                              ORDER BY SCS.TermStartDate DESC
                                                            ) AS RowNumber
                                         ,GSE.StudentId
                                   FROM   syCreditSummary AS SCS
                                   INNER JOIN #tmpCreditSummary AS TCS ON TCS.ReqId = SCS.ReqId
                                                                          AND TCS.StuEnrollId = SCS.StuEnrollId
                                   INNER JOIN #tmpStudentEnrollmentsOnlyIds AS GSE ON GSE.StuEnrollId = SCS.StuEnrollId
                                   WHERE  (
                                          SCS.FinalScore IS NOT NULL
                                          OR SCS.FinalGrade IS NOT NULL
                                          )
                                          AND TCS.Counted > 1
                                   ) AS dt2
                            WHERE  dt2.RowNumber = 1;
            END;

        IF @GradeCourseRepetitionsMethod = 'best'
            BEGIN
                INSERT INTO #CoursesNotRepeated
                            SELECT dt2.StuEnrollId
                                  ,dt2.TermId
                                  ,dt2.TermDescrip
                                  ,dt2.ReqId
                                  ,dt2.ReqDescrip
                                  ,dt2.ClsSectionId
                                  ,dt2.CreditsEarned
                                  ,dt2.CreditsAttempted
                                  ,dt2.CurrentScore
                                  ,dt2.CurrentGrade
                                  ,dt2.FinalScore
                                  ,dt2.FinalGrade
                                  ,dt2.Completed
                                  ,dt2.FinalGPA
                                  ,dt2.Product_WeightedAverage_Credits_GPA
                                  ,dt2.Count_WeightedAverage_Credits
                                  ,dt2.Product_SimpleAverage_Credits_GPA
                                  ,dt2.Count_SimpleAverage_Credits
                                  ,dt2.ModUser
                                  ,dt2.ModDate
                                  ,dt2.TermGPA_Simple
                                  ,dt2.TermGPA_Weighted
                                  ,dt2.coursecredits
                                  ,dt2.CumulativeGPA
                                  ,dt2.CumulativeGPA_Simple
                                  ,dt2.FACreditsEarned
                                  ,dt2.Average
                                  ,dt2.CumAverage
                                  ,dt2.TermStartDate
                                  ,dt2.RowNumber
                                  ,dt2.StudentId
                            FROM   (
                                   SELECT SCS.StuEnrollId
                                         ,SCS.TermId
                                         ,SCS.TermDescrip
                                         ,SCS.ReqId
                                         ,SCS.ReqDescrip
                                         ,SCS.ClsSectionId
                                         ,SCS.CreditsEarned
                                         ,SCS.CreditsAttempted
                                         ,SCS.CurrentScore
                                         ,SCS.CurrentGrade
                                         ,SCS.FinalScore
                                         ,SCS.FinalGrade
                                         ,SCS.Completed
                                         ,SCS.FinalGPA
                                         ,SCS.Product_WeightedAverage_Credits_GPA
                                         ,SCS.Count_WeightedAverage_Credits
                                         ,SCS.Product_SimpleAverage_Credits_GPA
                                         ,SCS.Count_SimpleAverage_Credits
                                         ,SCS.ModUser
                                         ,SCS.ModDate
                                         ,SCS.TermGPA_Simple
                                         ,SCS.TermGPA_Weighted
                                         ,SCS.coursecredits
                                         ,SCS.CumulativeGPA
                                         ,SCS.CumulativeGPA_Simple
                                         ,SCS.FACreditsEarned
                                         ,SCS.Average
                                         ,SCS.CumAverage
                                         ,SCS.TermStartDate
                                         --,ROW_NUMBER() OVER ( PARTITION BY SCS.StuEnrollId, ReqId ORDER BY FinalGPA DESC ) AS RowNumber
                                         ,ROW_NUMBER() OVER ( PARTITION BY SCS.StuEnrollId
                                                                          ,SCS.ReqId
                                                              ORDER BY Completed DESC
                                                                      ,CreditsEarned DESC
                                                                      ,FinalGPA DESC
                                                            ) AS RowNumber
                                         ,GSE.StudentId AS StudentId
                                   FROM   syCreditSummary AS SCS
                                   INNER JOIN #tmpCreditSummary AS TCS ON TCS.ReqId = SCS.ReqId
                                                                          AND TCS.StuEnrollId = SCS.StuEnrollId
                                   INNER JOIN #tmpStudentEnrollmentsOnlyIds AS GSE ON GSE.StuEnrollId = SCS.StuEnrollId
                                   WHERE  (
                                          SCS.FinalScore IS NOT NULL
                                          OR SCS.FinalGrade IS NOT NULL
                                          )
                                          AND TCS.Counted > 1
                                   ) dt2
                            WHERE  RowNumber = 1;
            END;

        /*
-- Note:
-- Review Average Process. already was updated in other SP 
-- Pending changes
*/

        IF @GradeCourseRepetitionsMethod = 'average'
            BEGIN
                INSERT INTO #CoursesNotRepeated
                            SELECT dt2.StuEnrollId
                                  ,dt2.TermId
                                  ,dt2.TermDescrip
                                  ,dt2.ReqId
                                  ,dt2.ReqDescrip
                                  ,dt2.ClsSectionId
                                  ,dt2.CreditsEarned
                                  ,dt2.CreditsAttempted
                                  ,dt2.CurrentScore
                                  ,dt2.CurrentGrade
                                  ,dt2.FinalScore
                                  ,dt2.FinalGrade
                                  ,dt2.Completed
                                  ,dt2.FinalGPA
                                  ,dt2.Product_WeightedAverage_Credits_GPA
                                  ,dt2.Count_WeightedAverage_Credits
                                  ,dt2.Product_SimpleAverage_Credits_GPA
                                  ,dt2.Count_SimpleAverage_Credits
                                  ,dt2.ModUser
                                  ,dt2.ModDate
                                  ,dt2.TermGPA_Simple
                                  ,dt2.TermGPA_Weighted
                                  ,dt2.coursecredits
                                  ,dt2.CumulativeGPA
                                  ,dt2.CumulativeGPA_Simple
                                  ,dt2.FACreditsEarned
                                  ,dt2.Average
                                  ,dt2.CumAverage
                                  ,dt2.TermStartDate
                                  ,dt2.RowNumber
                                  ,dt2.StudentId
                            FROM   (
                                   SELECT SCS.StuEnrollId
                                         ,SCS.TermId
                                         ,SCS.TermDescrip
                                         ,SCS.ReqId
                                         ,SCS.ReqDescrip
                                         ,SCS.ClsSectionId
                                         ,SCS.CreditsEarned
                                         ,SCS.CreditsAttempted
                                         ,SCS.CurrentScore
                                         ,SCS.CurrentGrade
                                         ,SCS.FinalScore
                                         ,SCS.FinalGrade
                                         ,SCS.Completed
                                         ,SCS.FinalGPA
                                         ,SCS.Product_WeightedAverage_Credits_GPA
                                         ,SCS.Count_WeightedAverage_Credits
                                         ,SCS.Product_SimpleAverage_Credits_GPA
                                         ,SCS.Count_SimpleAverage_Credits
                                         ,SCS.ModUser
                                         ,SCS.ModDate
                                         ,SCS.TermGPA_Simple
                                         ,SCS.TermGPA_Weighted
                                         ,SCS.coursecredits
                                         ,SCS.CumulativeGPA
                                         ,SCS.CumulativeGPA_Simple
                                         ,SCS.FACreditsEarned
                                         ,SCS.Average
                                         ,SCS.CumAverage
                                         ,SCS.TermStartDate
                                         ,ROW_NUMBER() OVER ( PARTITION BY SCS.StuEnrollId
                                                                          ,SCS.ReqId
                                                              ORDER BY SCS.FinalGPA DESC
                                                            ) AS RowNumber
                                         ,GSE.StudentId AS StudentId
                                   FROM   syCreditSummary AS SCS
                                   INNER JOIN #tmpCreditSummary AS TCS ON TCS.ReqId = SCS.ReqId
                                                                          AND TCS.StuEnrollId = SCS.StuEnrollId
                                   INNER JOIN #tmpStudentEnrollmentsOnlyIds AS GSE ON GSE.StuEnrollId = SCS.StuEnrollId
                                   WHERE  (
                                          SCS.FinalScore IS NOT NULL
                                          OR SCS.FinalGrade IS NOT NULL
                                          )
                                          AND TCS.Counted > 1
                                   ) dt2;
            END;


        --- ******************* Equivalent Courses in Term Temp Table Starts Here *************************	
        -- Get Equivalent courses for all courses that student is currently registered in
        INSERT INTO #tmpEquivalentCoursesTakenByStudentInTerm
                    SELECT dt.EquivReqId
                          ,dt.StuEnrollId
                    FROM -- Get the list of Equivalent courses taken by student enrollment
                           (
                           SELECT DISTINCT CE.EquivReqId
                                 ,R.StuEnrollId
                                 ,CS.TermId
                           FROM   arResults R
                           INNER JOIN dbo.arClassSections CS ON CS.ClsSectionId = R.TestId
                           INNER JOIN syCreditSummary CSS ON CSS.StuEnrollId = R.StuEnrollId
                                                             AND CSS.ReqId = CS.ReqId
                           INNER JOIN arReqs R1 ON R1.ReqId = CS.ReqId
                           INNER JOIN arCourseEquivalent CE ON R1.ReqId = CE.ReqId
                           INNER JOIN #tmpStudentEnrollmentsOnlyIds AS GSE ON GSE.StuEnrollId = R.StuEnrollId
                           WHERE  R.GrdSysDetailId IS NOT NULL
                                  AND (
                                      (
                                      CSS.FinalGPA IS NOT NULL
                                      AND @GradesFormat = 'letter'
                                      )
                                      OR (
                                         CSS.FinalScore IS NOT NULL
                                         AND @GradesFormat = 'numeric'
                                         )
                                      )
                           UNION
                           SELECT DISTINCT CE.EquivReqId
                                 ,TR.StuEnrollId
                                 ,TR.TermId
                           FROM   dbo.arTransferGrades TR
                           INNER JOIN syCreditSummary CSS ON CSS.StuEnrollId = TR.StuEnrollId
                                                             AND CSS.ReqId = TR.ReqId
                           INNER JOIN arReqs R1 ON R1.ReqId = TR.ReqId
                           INNER JOIN arCourseEquivalent CE ON R1.ReqId = CE.ReqId
                           INNER JOIN #tmpStudentEnrollmentsOnlyIds AS GSE ON GSE.StuEnrollId = TR.StuEnrollId
                           WHERE  TR.GrdSysDetailId IS NOT NULL
                                  AND (
                                      (
                                      CSS.FinalGPA IS NOT NULL
                                      AND @GradesFormat = 'letter'
                                      )
                                      OR (
                                         CSS.FinalScore IS NOT NULL
                                         AND @GradesFormat = 'numeric'
                                         )
                                      )
                           ) dt;

        -- Check if student has got a grade on any of the Equivalent courses
        INSERT INTO #tmpStudentsWhoHasGradedEquivalentCourseInTerm
                    SELECT DISTINCT ECS.EquivReqId
                          ,R.StuEnrollId
                          ,CS.TermId
                    FROM   arResults R
                    INNER JOIN dbo.arClassSections CS ON CS.ClsSectionId = R.TestId
                    INNER JOIN syCreditSummary CSS ON CSS.StuEnrollId = R.StuEnrollId
                                                      AND CSS.ReqId = CS.ReqId
                    INNER JOIN #tmpEquivalentCoursesTakenByStudentInTerm AS ECS ON ECS.StuEnrollId = R.StuEnrollId
                                                                                   AND ECS.EquivReqId = CS.ReqId
                    WHERE  R.GrdSysDetailId IS NOT NULL
                           AND (
                               (
                               CSS.FinalGPA IS NOT NULL
                               AND @GradesFormat = 'letter'
                               )
                               OR (
                                  CSS.FinalScore IS NOT NULL
                                  AND @GradesFormat = 'numeric'
                                  )
                               )
                    UNION
                    SELECT DISTINCT ECS.EquivReqId
                          ,TR.StuEnrollId
                          ,TR.TermId
                    FROM   dbo.arTransferGrades TR
                    INNER JOIN dbo.arClassSections CS ON CS.ClsSectionId = TR.ReqId
                    INNER JOIN syCreditSummary CSS ON CSS.StuEnrollId = TR.StuEnrollId
                                                      AND CSS.ReqId = CS.ReqId
                    INNER JOIN #tmpEquivalentCoursesTakenByStudentInTerm ECS ON TR.StuEnrollId = ECS.StuEnrollId
                                                                                AND ECS.EquivReqId = CS.ReqId
                    WHERE  TR.GrdSysDetailId IS NOT NULL
                           AND (
                               (
                               CSS.FinalGPA IS NOT NULL
                               AND @GradesFormat = 'letter'
                               )
                               OR (
                                  CSS.FinalScore IS NOT NULL
                                  AND @GradesFormat = 'numeric'
                                  )
                               );


        --- ******************* Equivalent Courses Temp Table Ends Here *************************

        INSERT INTO #tmpCoursesNotRepeatedOnlyIds (
                                                  StuEnrollId
                                                 ,TermId
                                                  )
                    SELECT DISTINCT CNR.StuEnrollId
                          ,CNR.TermId
                    FROM   #CoursesNotRepeated AS CNR;

        IF @GradesFormat = 'letter'
            BEGIN
                UPDATE GST
                SET    GST.TermSimpleCourses = T.TermSimpleCourses
                      ,GST.TermSimple_GPA_Credits = T.TermSimple_GPA_Credits
                      ,GST.TermCourseCredits = T.TermCourseCredits
                      ,GST.TermWeighted_GPA_Credits = T.TermWeighted_GPA_Credits
                FROM   #getStudentTerms AS GST
                INNER JOIN (
                           SELECT   CNR.StuEnrollId
                                   ,CNR.TermId
                                   ,COUNT(ISNULL(CNR.coursecredits, 0)) AS TermSimpleCourses
                                   ,SUM(ISNULL(CNR.FinalGPA, 0.0)) AS TermSimple_GPA_Credits
                                   ,SUM(ISNULL(CNR.coursecredits, 0.0)) AS TermCourseCredits
                                   ,SUM(ISNULL(CNR.coursecredits * CNR.FinalGPA, 0.0)) AS TermWeighted_GPA_Credits
                           FROM     #CoursesNotRepeated AS CNR
                           WHERE    CNR.FinalGPA IS NOT NULL
                           GROUP BY CNR.StuEnrollId
                                   ,CNR.TermId
                           ) AS T ON T.StuEnrollId = GST.StuEnrollId
                                     AND T.TermId = GST.TermId;
                UPDATE GST
                SET    GST.TermCredits = T.TermCredits
                FROM   #getStudentTerms AS GST
                INNER JOIN (
                           SELECT   CNR.StuEnrollId
                                   ,CNR.TermId
                                   ,SUM(ISNULL(CNR.CreditsEarned, 0)) AS TermCredits
                           FROM     #CoursesNotRepeated AS CNR
                           --WHERE   CNR.FinalGPA IS NOT NULL    --AND FinalGPA IS NOT NULL 'Commented by Balaji on 2/24 as this variable just reports credits earned
                           GROUP BY CNR.StuEnrollId
                                   ,CNR.TermId
                           ) AS T ON T.StuEnrollId = GST.StuEnrollId
                                     AND T.TermId = GST.TermId;
                UPDATE GST
                SET    GST.TermEquivCourse_SA_CC = T.TermEquivCourse_SA_CC
                FROM   #getStudentTerms AS GST
                INNER JOIN (
                           SELECT   CE.StuEnrollId
                                   ,CE.TermId
                                   ,COUNT(ISNULL(AR.Credits, 0)) AS TermEquivCourse_SA_CC
                           FROM     arReqs AS AR
                           INNER JOIN #tmpStudentsWhoHasGradedEquivalentCourseInTerm AS CE ON CE.EquivReqId = AR.ReqId
                           INNER JOIN #tmpCoursesNotRepeatedOnlyIds AS TCN ON TCN.StuEnrollId = CE.StuEnrollId
                                                                              AND TCN.TermId = CE.TermId
                           GROUP BY CE.StuEnrollId
                                   ,CE.TermId
                           ) AS T ON T.StuEnrollId = GST.StuEnrollId
                                     AND T.TermId = GST.TermId;
                UPDATE GST
                SET    GST.TermEquivCourse_SA_GPA = T.TermEquivCourse_SA_GPA
                FROM   #getStudentTerms AS GST
                INNER JOIN (
                           SELECT   CE.StuEnrollId
                                   ,CE.TermId
                                   ,SUM(ISNULL(AGSD.GPA, 0.00)) AS TermEquivCourse_SA_GPA
                           FROM     #tmpStudentsWhoHasGradedEquivalentCourseInTerm AS CE
                           INNER JOIN arResults AS AR ON AR.StuEnrollId = CE.StuEnrollId
                           INNER JOIN arGradeSystemDetails AS AGSD ON AGSD.GrdSysDetailId = AR.GrdSysDetailId
                           INNER JOIN syCreditSummary AS SCS ON SCS.StuEnrollId = AR.StuEnrollId
                                                                AND SCS.ReqId = CE.EquivReqId
                           INNER JOIN #tmpCoursesNotRepeatedOnlyIds AS TCN ON TCN.StuEnrollId = CE.StuEnrollId
                                                                              AND TCN.TermId = CE.TermId
                           GROUP BY CE.StuEnrollId
                                   ,CE.TermId
                           ) AS T ON T.StuEnrollId = GST.StuEnrollId
                                     AND T.TermId = GST.TermId;
                UPDATE GST
                SET    GST.TermSimpleGPA = CASE WHEN ( ISNULL(GST.TermSimpleCourses, 0) + ISNULL(GST.TermEquivCourse_SA_CC, 0) > 0 ) THEN
                    ( ISNULL(GST.TermSimple_GPA_Credits, 0.0) + ISNULL(GST.TermEquivCourse_SA_GPA, 0.0))
                    / ( ISNULL(GST.TermSimpleCourses, 0) + ISNULL(GST.TermEquivCourse_SA_CC, 0))
                                                ELSE 0.0
                                           END
                      ,GST.TermWeightedGPA = CASE WHEN ( ISNULL(GST.TermCourseCredits, 0.0) + ISNULL(GST.TermEquivCourse_WGPA_CC1, 0.0) > 0.00 ) THEN
                      ( ISNULL(GST.TermWeighted_GPA_Credits, 0.0) + ISNULL(GST.TermEquivCourse_SA_GPA, 0.0))
                      / ( ISNULL(GST.TermCourseCredits, 0.0) + ISNULL(GST.TermEquivCourse_WGPA_CC1, 0.0))
                                                  ELSE 0.0
                                             END
                      ,GST.TermAverage = CASE WHEN ( ISNULL(GST.TermAverageCount, 0) > 0 ) THEN
                      ( ISNULL(GST.TermAverageSum, 0.0) / ( ISNULL(GST.TermAverageCount, 0)))
                                              ELSE 0.0
                                         END
                FROM   #getStudentTerms AS GST;
            END;
        ELSE
            BEGIN
                UPDATE GST
                SET    GST.TermAverageCount = T.TermAverageCount
                      ,GST.TermAverageSum = T.TermAverageSum
                FROM   #getStudentTerms AS GST
                INNER JOIN (
                           SELECT   CNR.StuEnrollId
                                   ,CNR.TermId
                                   ,COUNT(ISNULL(CNR.coursecredits, 0)) AS TermAverageCount
                                   ,SUM(ISNULL(CNR.FinalScore, 0.0)) AS TermAverageSum
                           FROM     #CoursesNotRepeated AS CNR
                           WHERE    CNR.FinalScore IS NOT NULL
                           GROUP BY CNR.StuEnrollId
                                   ,CNR.TermId
                           ) AS T ON T.StuEnrollId = GST.StuEnrollId
                                     AND T.TermId = GST.TermId;
                UPDATE GST
                SET    GST.TermCredits = T.TermCredits
                FROM   #getStudentTerms AS GST
                INNER JOIN (
                           SELECT   CNR.StuEnrollId
                                   ,CNR.TermId
                                   ,SUM(ISNULL(CNR.CreditsEarned, 0)) AS TermCredits
                           FROM     #CoursesNotRepeated AS CNR
                           --WHERE   CNR.FinalGPA IS NOT NULL    --AND FinalGPA IS NOT NULL 'Commented by Balaji on 2/24 as this variable just reports credits earned
                           GROUP BY CNR.StuEnrollId
                                   ,CNR.TermId
                           ) AS T ON T.StuEnrollId = GST.StuEnrollId
                                     AND T.TermId = GST.TermId;
                UPDATE GST
                SET    GST.TermAverage = CASE WHEN ( ISNULL(GST.TermAverageCount, 0) > 0 ) THEN
                    ( ISNULL(GST.TermAverageSum, 0.0) / ( ISNULL(GST.TermAverageCount, 0)))
                                              ELSE 0.0
                                         END
                FROM   #getStudentTerms AS GST;
            END;

        INSERT INTO #tmpEquivalentCoursesTakenByStudent
                    SELECT dt.EquivReqId
                          ,dt.StuEnrollId
                    FROM -- Get the list of Equivalent courses taken by student enrollment
                           (
                           SELECT DISTINCT CE.EquivReqId
                                 ,R.StuEnrollId
                           FROM   arResults R
                           INNER JOIN dbo.arClassSections CS ON CS.ClsSectionId = R.TestId
                           INNER JOIN syCreditSummary CSS ON CSS.StuEnrollId = R.StuEnrollId
                                                             AND CSS.ReqId = CS.ReqId
                           INNER JOIN arReqs R1 ON R1.ReqId = CS.ReqId
                           INNER JOIN arCourseEquivalent CE ON R1.ReqId = CE.ReqId
                           INNER JOIN #tmpStudentEnrollmentsOnlyIds AS GSE ON GSE.StuEnrollId = R.StuEnrollId
                           WHERE  R.GrdSysDetailId IS NOT NULL
                                  AND (
                                      (
                                      CSS.FinalGPA IS NOT NULL
                                      AND @GradesFormat = 'letter'
                                      )
                                      OR (
                                         CSS.FinalScore IS NOT NULL
                                         AND @GradesFormat = 'numeric'
                                         )
                                      )
                           UNION
                           SELECT DISTINCT CE.EquivReqId
                                 ,TR.StuEnrollId
                           FROM   dbo.arTransferGrades TR
                           INNER JOIN syCreditSummary CSS ON CSS.StuEnrollId = TR.StuEnrollId
                                                             AND CSS.ReqId = TR.ReqId
                           INNER JOIN arReqs R1 ON R1.ReqId = TR.ReqId
                           INNER JOIN arCourseEquivalent CE ON R1.ReqId = CE.ReqId
                           INNER JOIN #tmpStudentEnrollmentsOnlyIds AS GSE ON GSE.StuEnrollId = TR.StuEnrollId
                           WHERE  TR.GrdSysDetailId IS NOT NULL
                                  AND (
                                      (
                                      CSS.FinalGPA IS NOT NULL
                                      AND @GradesFormat = 'letter'
                                      )
                                      OR (
                                         CSS.FinalScore IS NOT NULL
                                         AND @GradesFormat = 'numeric'
                                         )
                                      )
                           ) dt;
        -- Check if student has got a grade on any of the Equivalent courses
        INSERT INTO #tmpStudentsWhoHasGradedEquivalentCourse
                    SELECT DISTINCT ECS.EquivReqId
                          ,R.StuEnrollId
                    FROM   arResults R
                    INNER JOIN dbo.arClassSections CS ON CS.ClsSectionId = R.TestId
                    INNER JOIN syCreditSummary CSS ON CSS.StuEnrollId = R.StuEnrollId
                                                      AND CSS.ReqId = CS.ReqId
                    INNER JOIN #tmpEquivalentCoursesTakenByStudent ECS ON R.StuEnrollId = ECS.StuEnrollId
                                                                          AND ECS.EquivReqId = CS.ReqId
                    WHERE  R.GrdSysDetailId IS NOT NULL
                           AND (
                               (
                               CSS.FinalGPA IS NOT NULL
                               AND @GradesFormat = 'letter'
                               )
                               OR (
                                  CSS.FinalScore IS NOT NULL
                                  AND @GradesFormat = 'numeric'
                                  )
                               )
                    UNION
                    SELECT DISTINCT ECS.EquivReqId
                          ,TR.StuEnrollId
                    FROM   dbo.arTransferGrades TR
                    INNER JOIN dbo.arClassSections CS ON CS.ClsSectionId = TR.ReqId
                    INNER JOIN syCreditSummary CSS ON CSS.StuEnrollId = TR.StuEnrollId
                                                      AND CSS.ReqId = CS.ReqId
                    INNER JOIN #tmpEquivalentCoursesTakenByStudent ECS ON TR.StuEnrollId = ECS.StuEnrollId
                                                                          AND ECS.EquivReqId = CS.ReqId
                    WHERE  TR.GrdSysDetailId IS NOT NULL
                           AND (
                               (
                               CSS.FinalGPA IS NOT NULL
                               AND @GradesFormat = 'letter'
                               )
                               OR (
                                  CSS.FinalScore IS NOT NULL
                                  AND @GradesFormat = 'numeric'
                                  )
                               );


        IF @GradesFormat = 'letter'
            BEGIN
                UPDATE GST
                SET    GST.PrgVers_SimpleCourseCredits = T.PrgVers_SimpleCourseCredits
                      ,GST.PrgVers_Simple_GPA_Credits = T.PrgVers_Simple_GPA_Credits
                      ,GST.PrgVers_CourseCredits = T.PrgVers_CourseCredits
                      ,GST.PrgVers_Weighted_GPA_Credits = T.PrgVers_Weighted_GPA_Credits
                FROM   #getStudentEnrollments AS GST
                INNER JOIN (
                           SELECT   CNR.StuEnrollId
                                   ,COUNT(ISNULL(CNR.coursecredits, 0)) AS PrgVers_SimpleCourseCredits
                                   ,SUM(ISNULL(CNR.FinalGPA, 0.0)) AS PrgVers_Simple_GPA_Credits
                                   ,SUM(ISNULL(CNR.coursecredits, 0.0)) AS PrgVers_CourseCredits
                                   ,SUM(ISNULL(CNR.coursecredits, 0.0) * ISNULL(CNR.FinalGPA, 0.0)) AS PrgVers_Weighted_GPA_Credits
                           FROM     #CoursesNotRepeated AS CNR
                           WHERE    CNR.FinalGPA IS NOT NULL
                           GROUP BY CNR.StuEnrollId
                           ) AS T ON T.StuEnrollId = GST.StuEnrollId;
                UPDATE GST
                SET    GST.OverAll_SimpleCourses = T.OverAll_SimpleCourses
                      ,GST.OverAll_Simple_GPA_Credits = T.OverAll_Simple_GPA_Credits
                      ,GST.OverAll_Weighted_GPA_Credits = T.OverAll_Weighted_GPA_Credits
                      ,GST.OverAll_CourseCredits = T.OverAll_CourseCredits
                FROM   #getStudentEnrollments AS GST
                INNER JOIN (
                           SELECT TSE.StuEnrollId
                                 ,ST.OverAll_SimpleCourses
                                 ,ST.OverAll_Simple_GPA_Credits
                                 ,ST.OverAll_Weighted_GPA_Credits
                                 ,ST.OverAll_CourseCredits
                           FROM   #tmpStudentEnrollmentsOnlyIds AS TSE
                           LEFT JOIN (
                                     SELECT   CNR.StudentId
                                             ,COUNT(ISNULL(CNR.coursecredits, 0)) AS OverAll_SimpleCourses
                                             ,SUM(ISNULL(CNR.FinalGPA, 0.0)) AS OverAll_Simple_GPA_Credits
                                             ,SUM(ISNULL(CNR.coursecredits, 0.0)) AS OverAll_CourseCredits
                                             ,SUM(ISNULL(CNR.coursecredits, 0.0) * ISNULL(CNR.FinalGPA, 0.0)) AS OverAll_Weighted_GPA_Credits
                                     FROM     #CoursesNotRepeated AS CNR
                                     WHERE    CNR.FinalGPA IS NOT NULL
                                     GROUP BY CNR.StudentId
                                     ) AS ST ON ST.StudentId = TSE.StudentId
                           ) AS T ON T.StuEnrollId = GST.StuEnrollId;
                UPDATE GST
                SET    GST.PrgVers_EquivCourses_SA_CC = T.PrgVers_EquivCourses_SA_CC
                      ,GST.PrgVers_EquivCourse_WGPA_CC1 = T.PrgVers_EquivCourse_WGPA_CC1
                FROM   #getStudentEnrollments AS GST
                INNER JOIN (
                           SELECT   CE.StuEnrollId
                                   ,COUNT(ISNULL(AR.Credits, 0)) AS PrgVers_EquivCourses_SA_CC
                                   ,SUM(ISNULL(AR.Credits, 0.0)) AS PrgVers_EquivCourse_WGPA_CC1
                           FROM     arReqs AS AR
                           INNER JOIN #tmpStudentsWhoHasGradedEquivalentCourse AS CE ON CE.EquivReqId = AR.ReqId
                           INNER JOIN #tmpStudentEnrollmentsOnlyIds AS TSE ON TSE.StuEnrollId = CE.StuEnrollId
                           GROUP BY CE.StuEnrollId
                           ) AS T ON T.StuEnrollId = GST.StuEnrollId;
                UPDATE GST
                SET    GST.OverAll_EquivCourses_SA_CC = T.OverAll_EquivCourses_SA_CC
                      ,GST.OverAll_EquivCourse_WGPA_CC1 = T.OverAll_EquivCourse_WGPA_CC1
                FROM   #getStudentEnrollments AS GST
                INNER JOIN (
                           SELECT TSE.StuEnrollId
                                 ,ST.OverAll_EquivCourses_SA_CC
                                 ,ST.OverAll_EquivCourse_WGPA_CC1
                           FROM   #tmpStudentEnrollmentsOnlyIds AS TSE
                           LEFT JOIN (
                                     SELECT   TSE1.StudentId
                                             ,COUNT(ISNULL(AR.Credits, 0)) AS OverAll_EquivCourses_SA_CC
                                             ,SUM(ISNULL(Credits, 0)) AS OverAll_EquivCourse_WGPA_CC1
                                     FROM     arReqs AS AR
                                     INNER JOIN #tmpStudentsWhoHasGradedEquivalentCourse AS CE ON CE.EquivReqId = AR.ReqId
                                     INNER JOIN #tmpStudentEnrollmentsOnlyIds AS TSE1 ON TSE1.StuEnrollId = CE.StuEnrollId
                                     GROUP BY TSE1.StudentId
                                     ) AS ST ON ST.StudentId = TSE.StudentId
                           ) AS T ON T.StuEnrollId = GST.StuEnrollId;
                UPDATE GST
                SET    GST.PrgVers_EquivCourse_SA_GPA = T.PrgVers_EquivCourse_SA_GPA
                FROM   #getStudentEnrollments AS GST
                INNER JOIN (
                           SELECT   CE.StuEnrollId
                                   ,SUM(ISNULL(AGSD.GPA, 0.00)) AS PrgVers_EquivCourse_SA_GPA
                           FROM     arGradeSystemDetails AS AGSD
                           INNER JOIN arResults AS AR ON AR.GrdSysDetailId = AGSD.GrdSysDetailId
                           INNER JOIN #tmpStudentsWhoHasGradedEquivalentCourse AS CE ON CE.StuEnrollId = AR.StuEnrollId
                           INNER JOIN syCreditSummary AS SCS ON SCS.StuEnrollId = AR.StuEnrollId
                                                                AND SCS.ReqId = CE.EquivReqId
                           INNER JOIN #tmpStudentEnrollmentsOnlyIds AS TSE ON TSE.StuEnrollId = CE.StuEnrollId
                           GROUP BY CE.StuEnrollId
                           ) AS T ON T.StuEnrollId = GST.StuEnrollId;
                UPDATE GST
                SET    GST.OverAll_EquivCourse_SA_GPA = T.OverAll_EquivCourse_SA_GPA
                FROM   #getStudentEnrollments AS GST
                INNER JOIN (
                           SELECT TSE.StuEnrollId
                                 ,ST.OverAll_EquivCourse_SA_GPA
                           FROM   #tmpStudentEnrollmentsOnlyIds AS TSE
                           LEFT JOIN (
                                     SELECT   TSE1.StudentId
                                             ,SUM(ISNULL(AGSD.GPA, 0.00)) AS OverAll_EquivCourse_SA_GPA
                                     FROM     arGradeSystemDetails AS AGSD
                                     INNER JOIN arResults AS AR ON AR.GrdSysDetailId = AGSD.GrdSysDetailId
                                     INNER JOIN #tmpStudentsWhoHasGradedEquivalentCourse AS CE ON CE.StuEnrollId = AR.StuEnrollId
                                     INNER JOIN syCreditSummary AS SCS ON SCS.StuEnrollId = AR.StuEnrollId
                                                                          AND SCS.ReqId = CE.EquivReqId
                                     INNER JOIN #tmpStudentEnrollmentsOnlyIds AS TSE1 ON TSE1.StuEnrollId = CE.StuEnrollId
                                     GROUP BY TSE1.StudentId
                                     ) AS ST ON ST.StudentId = TSE.StudentId
                           ) AS T ON T.StuEnrollId = GST.StuEnrollId;
                UPDATE GST
                SET    GST.PrgVers_EquivCourse_WGPA_GPA1 = T.PrgVers_EquivCourse_WGPA_GPA1
                FROM   #getStudentEnrollments AS GST
                INNER JOIN (
                           SELECT   CE.StuEnrollId
                                   ,SUM(ISNULL(AGSD.GPA, 0.0) * ISNULL(AR2.Credits, 0.0)) AS PrgVers_EquivCourse_WGPA_GPA1
                           FROM     #tmpStudentsWhoHasGradedEquivalentCourse AS CE
                           INNER JOIN arResults AS AR ON AR.StuEnrollId = CE.StuEnrollId
                           INNER JOIN arGradeSystemDetails AS AGSD ON AGSD.GrdSysDetailId = AR.GrdSysDetailId
                           INNER JOIN syCreditSummary AS SCS ON SCS.StuEnrollId = AR.StuEnrollId
                                                                AND SCS.ReqId = CE.EquivReqId
                           INNER JOIN arReqs AS AR2 ON AR2.ReqId = CE.EquivReqId
                           INNER JOIN #tmpStudentEnrollmentsOnlyIds AS TSE ON TSE.StuEnrollId = CE.StuEnrollId
                           GROUP BY CE.StuEnrollId
                           ) AS T ON T.StuEnrollId = GST.StuEnrollId;
                UPDATE GST
                SET    GST.OverAll_EquivCourse_WGPA_GPA1 = T.OverAll_EquivCourse_WGPA_GPA1
                FROM   #getStudentEnrollments AS GST
                INNER JOIN (
                           SELECT TSE.StuEnrollId
                                 ,ST.OverAll_EquivCourse_WGPA_GPA1
                           FROM   #tmpStudentEnrollmentsOnlyIds AS TSE
                           LEFT JOIN (
                                     SELECT   TSE1.StudentId
                                             ,SUM(ISNULL(AGSD.GPA, 0.0) * ISNULL(AR2.Credits, 0.0)) AS OverAll_EquivCourse_WGPA_GPA1
                                     FROM     arGradeSystemDetails AS AGSD
                                     INNER JOIN arResults AS AR ON AR.GrdSysDetailId = AGSD.GrdSysDetailId
                                     INNER JOIN #tmpStudentsWhoHasGradedEquivalentCourse AS CE ON CE.StuEnrollId = AR.StuEnrollId
                                     INNER JOIN syCreditSummary AS SCS ON SCS.StuEnrollId = AR.StuEnrollId
                                                                          AND SCS.ReqId = CE.EquivReqId
                                     INNER JOIN arReqs AR2 ON AR2.ReqId = CE.EquivReqId
                                     INNER JOIN #tmpStudentEnrollmentsOnlyIds AS TSE1 ON TSE1.StuEnrollId = CE.StuEnrollId
                                     GROUP BY TSE1.StudentId
                                     ) AS ST ON ST.StudentId = TSE.StudentId
                           ) AS T ON T.StuEnrollId = GST.StuEnrollId;
                UPDATE GST
                SET    GST.PrgVers_SimpleGPA = CASE WHEN ( ISNULL(GST.PrgVers_SimpleCourseCredits, 0) + ISNULL(GST.PrgVers_EquivCourses_SA_CC, 0) > 0 ) THEN
                    ( ISNULL(GST.PrgVers_Simple_GPA_Credits, 0.0) + ISNULL(GST.PrgVers_EquivCourse_SA_GPA, 0.0))
                    / ( ISNULL(GST.PrgVers_SimpleCourseCredits, 0) + ISNULL(GST.PrgVers_EquivCourses_SA_CC, 0))
                                                    ELSE 0.0
                                               END
                      ,GST.PrgVers_WeightedGPA = CASE WHEN ( ISNULL(GST.PrgVers_CourseCredits, 0) + ISNULL(GST.PrgVers_EquivCourse_WGPA_CC1, 0) > 0.00 ) THEN
                      ( ISNULL(GST.PrgVers_Weighted_GPA_Credits, 0.0) + ISNULL(GST.PrgVers_EquivCourse_WGPA_GPA1, 0.0))
                      / ( ISNULL(GST.PrgVers_CourseCredits, 0) + ISNULL(GST.PrgVers_EquivCourse_WGPA_CC1, 0))
                                                      ELSE 0.0
                                                 END
                      ,GST.OverAll_SimpleGPA = CASE WHEN ( ISNULL(GST.OverAll_SimpleCourses, 0) + ISNULL(GST.OverAll_EquivCourse_SA_GPA, 0) > 0 ) THEN
                      ( ISNULL(GST.OverAll_Simple_GPA_Credits, 0.0) + ISNULL(GST.OverAll_EquivCourse_SA_GPA, 0.0))
                      / ( ISNULL(GST.OverAll_SimpleCourses, 0) + ISNULL(GST.OverAll_EquivCourse_SA_GPA, 0))
                                                    ELSE 0.0
                                               END
                      ,GST.OverAll_WeightedGPA = CASE WHEN ( ISNULL(GST.OverAll_CourseCredits, 0) + ISNULL(GST.OverAll_EquivCourse_WGPA_CC1, 0) > 0.00 ) THEN
                      ( ISNULL(GST.OverAll_Weighted_GPA_Credits, 0.0) + ISNULL(GST.OverAll_EquivCourse_WGPA_GPA1, 0.0))
                      / ( ISNULL(GST.OverAll_CourseCredits, 0) + ISNULL(GST.OverAll_EquivCourse_WGPA_CC1, 0))
                                                      ELSE 0.0
                                                 END
                FROM   #getStudentEnrollments AS GST;
            END;
        ELSE
            BEGIN
                UPDATE GST
                SET    GST.PrgVers_AverageCount = T.PrgVers_AverageCount
                      ,GST.PrgVers_AverageSum = T.PrgVers_AverageSum
                FROM   #getStudentEnrollments AS GST
                INNER JOIN (
                           SELECT   CNR.StuEnrollId
                                   ,COUNT(ISNULL(CNR.coursecredits, 0)) AS PrgVers_AverageCount
                                   ,SUM(ISNULL(CNR.FinalScore, 0.0)) AS PrgVers_AverageSum
                           FROM     #CoursesNotRepeated AS CNR
                           WHERE    CNR.FinalScore IS NOT NULL
                           GROUP BY CNR.StuEnrollId
                           ) AS T ON T.StuEnrollId = GST.StuEnrollId;
                UPDATE GST
                SET    GST.OverAll_AverageCount = T.OverAll_AverageCount
                      ,GST.OverAll_AverageSum = T.OverAll_AverageSum
                FROM   #getStudentEnrollments AS GST
                INNER JOIN (
                           SELECT TSE.StuEnrollId
                                 ,ST.OverAll_AverageCount
                                 ,ST.OverAll_AverageSum
                           FROM   #tmpStudentEnrollmentsOnlyIds AS TSE
                           LEFT JOIN (
                                     SELECT   CNR.StudentId
                                             ,COUNT(ISNULL(CNR.coursecredits, 0)) AS OverAll_AverageCount
                                             ,SUM(ISNULL(CNR.FinalScore, 0.0)) AS OverAll_AverageSum
                                     FROM     #CoursesNotRepeated AS CNR
                                     WHERE    CNR.FinalScore IS NOT NULL
                                     GROUP BY CNR.StudentId
                                     ) AS ST ON ST.StudentId = TSE.StudentId
                           ) AS T ON T.StuEnrollId = GST.StuEnrollId;
                UPDATE GST
                SET    GST.PrgVers_Average = CASE WHEN ( ISNULL(GST.PrgVers_AverageCount, 0) > 0 ) THEN
                    ( ISNULL(GST.PrgVers_AverageSum, 0.0) / ( ISNULL(GST.PrgVers_AverageCount, 0)))
                                                  ELSE 0.0
                                             END
                      ,GST.OverAll_Average = CASE WHEN ( ISNULL(GST.OverAll_AverageCount, 0) > 0 ) THEN
                      ( ISNULL(GST.OverAll_AverageSum, 0.0) / ( ISNULL(GST.OverAll_AverageCount, 0)))
                                                  ELSE 0.0
                                             END
                FROM   #getStudentEnrollments AS GST;
            END;


        SELECT   DT.LastName
                ,DT.FirstName
                ,DT.StudentId
                ,DT.StudentNumber
                ,DT.StuEnrollId
                ,DT.PrgVerId
                ,DT.ProgramVersion
                ,DT.PrgVersionTrackCredits
                ,DT.PrgVers_SimpleGPA
                ,DT.PrgVers_WeightedGPA
                ,DT.PrgVers_Average
                ,DT.OverAll_SimpleGPA
                ,DT.OverAll_WeightedGPA
                ,DT.OverAll_Average
                ,DT.SSN
                ,DT.EnrollmentID
                ,DT.MiddleName
                ,DT.TermId
                ,DT.TermDescription
                ,DT.TermStartDate
                ,DT.rowNumber
                ,DT.TermDescrip
                ,DT.TermCredits
                ,DT.TermSimpleGPA
                ,DT.TermWeightedGPA
                ,DT.TermAverage
                ,DT.ProgramCredits
                ,@GPAMethod AS GPAMethod
                ,@GradesFormat AS GradesFormat
        FROM     (
                 SELECT DISTINCT GSE.LastName
                       ,GSE.FirstName
                       ,GSE.StudentId
                       ,GSE.StudentNumber
                       ,GSE.StuEnrollId
                       ,GSE.PrgVerId
                       ,GSE.ProgramVersion
                       ,GSE.PrgVersionTrackCredits
                       ,GSE.PrgVers_SimpleGPA
                       ,GSE.PrgVers_WeightedGPA
                       ,GSE.PrgVers_Average
                       ,GSE.OverAll_SimpleGPA
                       ,GSE.OverAll_WeightedGPA
                       ,GSE.OverAll_Average
                       ,GSE.SSN
                       ,GSE.EnrollmentID
                       ,GSE.MiddleName
                       ,GSE.TermId
                       ,GSE.TermDescription
                       ,GSE.TermStartDate
                       ,GSE.rowNumber
                       ,GST.TermDescrip
                       ,GST.TermCredits
                       ,GST.TermSimpleGPA
                       ,GST.TermWeightedGPA
                       ,GST.TermAverage
                       ,(
                        SELECT SUM(ISNULL(TermCredits, 0))
                        FROM   #getStudentTerms
                        WHERE  StuEnrollId = GSE.StuEnrollId
                        ) AS ProgramCredits
                       ,ROW_NUMBER() OVER ( PARTITION BY GSE.StuEnrollId
                                                        ,GSE.PrgVerId
                                            ORDER BY GSE.TermStartDate DESC
                                          ) AS RowNumber2
                 FROM   #getStudentEnrollments GSE
                 INNER JOIN #getStudentTerms GST ON GSE.StuEnrollId = GST.StuEnrollId
                                                    AND GSE.TermId = GST.TermId
                 INNER JOIN #TermIdList AS TIL ON TIL.TermId = GST.TermId
                 WHERE  CASE WHEN @GradesFormat = 'letter' THEN CASE WHEN (
                                                                          (
                                                                          ( @GPAMethod = 'WeightedAVG' )
                                                                          AND (
                                                                              @CumulativeGPAGT IS NULL
                                                                              OR PrgVers_WeightedGPA >= @CumulativeGPAGT
                                                                              )
                                                                          AND (
                                                                              @CumulativeGPALT IS NULL
                                                                              OR PrgVers_WeightedGPA <= @CumulativeGPALT
                                                                              )
                                                                          AND (
                                                                              @termGPAGT IS NULL
                                                                              OR GST.TermWeightedGPA >= @termGPAGT
                                                                              )
                                                                          AND (
                                                                              @termGPALT IS NULL
                                                                              OR GST.TermWeightedGPA <= @termGPALT
                                                                              )
                                                                          )
                                                                          OR (
                                                                             ( @GPAMethod != 'WeightedAVG' )
                                                                             AND (
                                                                                 @CumulativeGPAGT IS NULL
                                                                                 OR PrgVers_SimpleGPA >= @CumulativeGPAGT
                                                                                 )
                                                                             AND (
                                                                                 @CumulativeGPALT IS NULL
                                                                                 OR PrgVers_SimpleGPA <= @CumulativeGPALT
                                                                                 )
                                                                             AND (
                                                                                 @termGPAGT IS NULL
                                                                                 OR GST.TermSimpleGPA >= @termGPAGT
                                                                                 )
                                                                             AND (
                                                                                 @termGPALT IS NULL
                                                                                 OR GST.TermSimpleGPA <= @termGPALT
                                                                                 )
                                                                             )
                                                                          )
                                                                          AND (
                                                                              @termCreditsRangeGT IS NULL
                                                                              OR GST.TermCredits >= @termCreditsRangeGT
                                                                              )
                                                                          AND (
                                                                              @termCreditsRangeLT IS NULL
                                                                              OR GST.TermCredits <= @termCreditsRangeLT
                                                                              ) THEN 1
                                                                     ELSE 0
                                                                END
                             WHEN @GradesFormat = 'numeric' THEN CASE WHEN (
                                                                           @CumulativeGPAGT IS NULL
                                                                           OR GSE.PrgVers_Average >= @CumulativeGPAGT
                                                                           )
                                                                           AND (
                                                                               @CumulativeGPALT IS NULL
                                                                               OR GSE.PrgVers_Average <= @CumulativeGPALT
                                                                               )
                                                                           AND (
                                                                               @termGPAGT IS NULL
                                                                               OR GST.TermAverage >= @termGPAGT
                                                                               )
                                                                           AND (
                                                                               @termGPALT IS NULL
                                                                               OR GST.TermAverage <= @termGPALT
                                                                               )
                                                                           AND (
                                                                               @termCreditsRangeGT IS NULL
                                                                               OR GST.TermCredits >= @termCreditsRangeGT
                                                                               )
                                                                           AND (
                                                                               @termCreditsRangeLT IS NULL
                                                                               OR GST.TermCredits <= @termCreditsRangeLT
                                                                               ) THEN 1
                                                                      ELSE 0
                                                                 END
                        END = 1
                 ) AS DT
        WHERE    (
                 (
                 (
                 @pTermId IS NULL
                 OR @pTermId = ''
                 )
                 AND DT.RowNumber2 = 1
                 )
                 OR (
                    (
                    @pTermId IS NOT NULL
                    AND @pTermId <> ''
                    )
                    AND DT.RowNumber2 = 1
                    )
                 )
        ORDER BY CASE WHEN (
                           @pTermId IS NULL
                           OR @pTermId = ''
                           ) THEN ( RANK() OVER ( ORDER BY DT.LastName ASC
                                                          ,DT.FirstName ASC
                                                          ,DT.TermStartDate DESC
                                                )
                                  )
                      ELSE ( RANK() OVER ( ORDER BY DT.LastName ASC
                                                   ,DT.FirstName ASC
                                                   ,DT.TermStartDate ASC
                                         )
                           )
                 END;

        IF EXISTS (
                  SELECT 1
                  FROM   tempdb.dbo.sysobjects AS O
                  WHERE  O.xtype IN ( 'U' )
                         AND O.id = OBJECT_ID(N'tempdb..#PrgVersionsList')
                  )
            BEGIN
                DROP TABLE #PrgVersionsList;
            END;
        IF EXISTS (
                  SELECT 1
                  FROM   tempdb.dbo.sysobjects AS O
                  WHERE  O.xtype IN ( 'U' )
                         AND O.id = OBJECT_ID(N'tempdb..#EnrollmentStatusList')
                  )
            BEGIN
                DROP TABLE #EnrollmentStatusList;
            END;
        IF EXISTS (
                  SELECT 1
                  FROM   tempdb.dbo.sysobjects AS O
                  WHERE  O.xtype IN ( 'U' )
                         AND O.id = OBJECT_ID(N'tempdb..#LeadGroupsList')
                  )
            BEGIN
                DROP TABLE #LeadGroupsList;
            END;
        IF EXISTS (
                  SELECT 1
                  FROM   tempdb.dbo.sysobjects AS O
                  WHERE  O.xtype IN ( 'U' )
                         AND O.id = OBJECT_ID(N'tempdb..#TermIdList')
                  )
            BEGIN
                DROP TABLE #TermIdList;
            END;
        IF EXISTS (
                  SELECT 1
                  FROM   tempdb.dbo.sysobjects AS O
                  WHERE  O.xtype IN ( 'U' )
                         AND O.id = OBJECT_ID(N'tempdb..#CoursesNotRepeated')
                  )
            BEGIN
                DROP TABLE #CoursesNotRepeated;
            END;
        IF EXISTS (
                  SELECT 1
                  FROM   tempdb.dbo.sysobjects AS O
                  WHERE  O.xtype IN ( 'U' )
                         AND O.id = OBJECT_ID(N'tempdb..#tmpCoursesNotRepeatedOnlyIds')
                  )
            BEGIN
                -- ALTER TABLE tempdb..#tmpCoursesNotRepeatedOnlyIds 
                --     DROP CONSTRAINT PK_#tmpCoursesNotRepeatedOnlyIds 
                DROP TABLE #tmpCoursesNotRepeatedOnlyIds;
            END;
        IF EXISTS (
                  SELECT 1
                  FROM   tempdb.dbo.sysobjects AS O
                  WHERE  O.xtype IN ( 'U' )
                         AND O.id = OBJECT_ID(N'tempdb..#getStudentTerms')
                  )
            BEGIN
                DROP TABLE #getStudentTerms;
            END;
        IF EXISTS (
                  SELECT 1
                  FROM   tempdb.dbo.sysobjects AS O
                  WHERE  O.xtype IN ( 'U' )
                         AND O.id = OBJECT_ID(N'tempdb..#getStudentEnrollments')
                  )
            BEGIN
                DROP TABLE #getStudentEnrollments;
            END;
        IF EXISTS (
                  SELECT 1
                  FROM   tempdb.dbo.sysobjects AS O
                  WHERE  O.xtype IN ( 'U' )
                         AND O.id = OBJECT_ID(N'tempdb..#tmpStudentEnrollmentsOnlyIds')
                  )
            BEGIN
                DROP TABLE #tmpStudentEnrollmentsOnlyIds;
            END;
        IF EXISTS (
                  SELECT 1
                  FROM   tempdb.dbo.sysobjects AS O
                  WHERE  O.xtype IN ( 'U' )
                         AND O.id = OBJECT_ID(N'tempdb..#tmpEquivalentCoursesTakenByStudentInTerm')
                  )
            BEGIN
                DROP TABLE #tmpEquivalentCoursesTakenByStudentInTerm;
            END;
        IF EXISTS (
                  SELECT 1
                  FROM   tempdb.dbo.sysobjects AS O
                  WHERE  O.xtype IN ( 'U' )
                         AND O.id = OBJECT_ID(N'tempdb..#tmpStudentsWhoHasGradedEquivalentCourseInTerm')
                  )
            BEGIN
                DROP TABLE #tmpStudentsWhoHasGradedEquivalentCourseInTerm;
            END;
        IF EXISTS (
                  SELECT 1
                  FROM   tempdb.dbo.sysobjects AS O
                  WHERE  O.xtype IN ( 'U' )
                         AND O.id = OBJECT_ID(N'tempdb..#tmpEquivalentCoursesTakenByStudent')
                  )
            BEGIN
                DROP TABLE #tmpEquivalentCoursesTakenByStudent;
            END;
        IF EXISTS (
                  SELECT 1
                  FROM   tempdb.dbo.sysobjects AS O
                  WHERE  O.xtype IN ( 'U' )
                         AND O.id = OBJECT_ID(N'tempdb..#tmpStudentsWhoHasGradedEquivalentCourse')
                  )
            BEGIN
                DROP TABLE #tmpStudentsWhoHasGradedEquivalentCourse;
            END;
        IF EXISTS (
                  SELECT 1
                  FROM   tempdb.dbo.sysobjects AS O
                  WHERE  O.xtype IN ( 'U' )
                         AND O.id = OBJECT_ID(N'tempdb..#tmpCreditSummary')
                  )
            BEGIN
                DROP TABLE #tmpCreditSummary;
            END;

    END;
--=================================================================================================
-- END  --  USP_STUDENTGPA_GETDETAILS
--=================================================================================================