SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_CheckIfClassWasAlreadyCompleted]
    @StuEnrollId UNIQUEIDENTIFIER
   ,@ClsSectionId UNIQUEIDENTIFIER
AS
    SELECT  COUNT(*) AS gradedcount
    FROM    arResults
    WHERE   StuEnrollId = @StuEnrollId
            AND TestId = @ClsSectionId
            AND (
                  Score IS NOT NULL
                  OR Grdsysdetailid IS NOT NULL
                );



GO
