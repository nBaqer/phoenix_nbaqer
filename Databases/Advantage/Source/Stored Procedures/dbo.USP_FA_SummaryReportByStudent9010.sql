SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[USP_FA_SummaryReportByStudent9010]
    @StartDate DATETIME
   ,@EndDate DATETIME
   ,@campGrpId AS VARCHAR(8000)
   ,@strStudentId AS VARCHAR(50)
   ,@PrgVerId AS VARCHAR(8000) = NULL
AS /*----------------------------------------------------------------------------------------------------
--    Author : Kamalesh Ahuja
    
--    Create date : 06/21/2010
--    modified: 2/1/2012  To accomodate US2735 Adjust additional unsub
--    Commented out 21,22  - additional unsub
    
--    Procedure Name : USP_FA_SummaryReportByStudent9010

--    Objective : Get Revenue Ratio 90/10 ratio) report
    
--    Parameters : Name Type Data Type Required?
--                        ===== ==== ========= =========
                        
    
--    Output : Returns the Revenue Ratio (90/10 ratio) report dataset
                        
--*/-----------------------------------------------------------------------------------------------------

--SELECT * FROM dbo.syCampGrps


--DECLARE @StartDate DATETIME = '01/01/2011'
--DECLARE @EndDate DATETIME = '12/12/2012'
--DECLARE @campGrpId AS VARCHAR(8000) = '6C352064-D135-4ACC-972A-035170D51B1B'
--DECLARE @strStudentId AS VARCHAR(50) = 'StudentId'
--DECLARE @PrgVerId AS VARCHAR(8000) = NULL



    BEGIN

        SELECT  *
        INTO    #TempTrans
        FROM    satransactions
        WHERE   TransDate >= @StartDate
                AND TransDate <= @EndDate
                AND Voided = 0
                AND saTransactions.StuEnrollId IN ( SELECT  StuEnrollId
                                                    FROM    arStuEnrollments
                                                    WHERE   arStuEnrollments.CampusId IN ( (
                                                                                             SELECT DISTINCT
                                                                                                    t1.campusid
                                                                                             FROM   sycmpgrpcmps t1
                                                                                             WHERE  t1.campgrpid IN ( SELECT    strval
                                                                                                                      FROM      dbo.SPLIT(@campGrpId) )
                                                                                           )
                                                                                      ) );

--SELECT * FROM #TempTrans

        SELECT DISTINCT
                StuEnrollId
        INTO    #TempStuEnrollment
        FROM    saTransactions T
        WHERE   TransDate < @StartDate
                AND Voided = 0
                AND T.StuEnrollId IN ( SELECT   StuEnrollId
                                       FROM     arStuEnrollments
                                       WHERE    arStuEnrollments.CampusId IN ( (
                                                                                 SELECT DISTINCT
                                                                                        t1.campusid
                                                                                 FROM   sycmpgrpcmps t1
                                                                                 WHERE  t1.campgrpid IN ( SELECT    strval
                                                                                                          FROM      dbo.SPLIT(@campGrpId) )
                                                                               )
                                                                          ) )
                AND Voided = 0
        GROUP BY StuEnrollId
        HAVING  SUM(TransAmount) < 0;

 --SELECT * FROM  #TempStuEnrollment

        SELECT  *
        INTO    #TempTransBalance
        FROM    saTransactions T
        WHERE   TransDate < @StartDate
                AND T.Voided = 0
                AND T.StuEnrollId IN ( SELECT   StuEnrollId
                                       FROM     #TempStuEnrollment );

    --SELECT  *
    --FROM    #TempTransBalance
        CREATE NONCLUSTERED INDEX INDX_Trans_TransId_SR ON #TempTrans (TransactionId);
        CREATE NONCLUSTERED INDEX INDX_Trans_StuEnrollment_SR ON #TempTrans (StuEnrollId);
        CREATE NONCLUSTERED INDEX INDX_Trans_TransAmount_SR ON #TempTrans (TransAmount);

        CREATE NONCLUSTERED INDEX INDX_Trans_TransId_SR ON #TempTransBalance (TransactionId);
        CREATE NONCLUSTERED INDEX INDX_Trans_StuEnrollment_SR ON #TempTransBalance (StuEnrollId);
        CREATE NONCLUSTERED INDEX INDX_Trans_TransAmount_SR ON #TempTransBalance (TransAmount);

        SELECT  ProgramVersion
               ,StuEnrollId
               ,LastName
               ,FirstName
               ,MiddleName
               ,StudentIdentifier
               ,CampGrpId
               ,CampGrpDescrip
               ,CampusId
               ,CampDescrip
               ,TitleIV
               ,SUM(Received) AS Received
               ,SUM(Refunded) AS Refunded
               ,SUM(RefundToStudent) AS RefundToStudent
               ,CASE WHEN ( TitleIV = 1 ) THEN ( SUM(Received) - SUM(Refunded) )
                     ELSE CASE WHEN ( SUM(Received) - SUM(RefundToStudent) ) < 0.00 THEN ( SUM(Received) - SUM(RefundToStudent) )
                               ELSE 0.00
                          END
                END AS Numerator
               ,( SUM(Received) - SUM(Refunded) - SUM(RefundToStudent) ) AS Denominator
--0.00 as Numerator,0.00 as Denominator
        INTO    #tempRecords
        FROM    (
                  ------ Title IV funds Query -------------------------------
                  SELECT    tt.*
                  FROM      (
                              -------- Get All Refunds To Student Title IV and Non Title IV--------
                              SELECT    NEWID() AS AGID
                                       ,'00000000-0000-0000-0000-000000000000' AS AwardTypeId
                                       ,'00000000-0000-0000-0000-000000000000' AS StudentAwardId
                                       ,PV.PrgVerCode AS ProgramVersion
                                       ,0 AS TitleIV
                                       ,t1.StuEnrollId
                                       ,S.LastName
                                       ,S.FirstName
                                       ,S.MiddleName
                                       ,CASE @strStudentId
                                          WHEN 'StudentId' THEN S.StudentNumber
                                          WHEN 'EnrollmentId' THEN SE.EnrollmentId
                                          ELSE S.SSN
                                        END AS StudentIdentifier
                                       ,CGC.CampGrpId
                                       ,syCampGrps.CampGrpDescrip
                                       ,SE.CampusId
                                       ,C.CampDescrip
                                       ,0.00 AS Received
                                       ,0.00 AS InstCharges
                                       ,0 AS PmtPlanCount
                                       ,0.00 AS Refunded
                                       ,t1.TransAmount AS RefundToStudent
                              FROM      #TempTrans t1
                                       ,arStuEnrollments SE
                                       ,arStudent S
                                       ,syCmpGrpCmps CGC
                                       ,syCampGrps
                                       ,syCampuses C
                                       ,arPrgVersions PV
                                       ,saRefunds R
                              WHERE     t1.voided = 0
                                        AND t1.TransactionId = R.TransactionId
                                        AND t1.StuEnrollId = SE.StuEnrollId
                                        AND SE.StudentId = S.StudentId
                                        AND CGC.CampusId = SE.CampusId
                                        AND syCampGrps.CampGrpId = CGC.CampGrpId
                                        AND C.CampusId = SE.CampusId
                                        AND t1.Voided = 0
                                        AND PV.PrgVerId = SE.PrgVerId
                                        AND (
                                              SELECT    COUNT(TransCodeId)
                                              FROM      saTransCodes TC
                                              WHERE     TC.TransCodeId = t1.TransCodeId
                                                        AND TC.IsInstCharge = 1
                                            ) > 0
                                        AND syCampGrps.campgrpid IN ( SELECT DISTINCT
                                                                                t1.campgrpid
                                                                      FROM      sycmpgrpcmps t1
                                                                      WHERE     t1.campgrpid IN ( SELECT    strval
                                                                                                  FROM      dbo.SPLIT(@campGrpId) ) )
                                        AND t1.TransDate >= @StartDate
                                        AND t1.TransDate <= @EndDate
                                        AND (
                                              @PrgVerId IS NULL
                                              OR SE.PrgVerId IN ( SELECT    strval
                                                                  FROM      dbo.SPLIT(@PrgVerId) )
                                            )
                                        AND R.RefundTypeId = 0
-------------Refunds To Student------------------
                              UNION
 
 -------- Get Title IV Institutional Refunds --------
                              SELECT    NEWID() AS AGID
                                       ,'00000000-0000-0000-0000-000000000000' AS AwardTypeId
                                       ,'00000000-0000-0000-0000-000000000000' AS StudentAwardId
                                       ,PV.PrgVerCode AS ProgramVersion
                                       ,FS.TitleIV
                                       ,t1.StuEnrollId
                                       ,S.LastName
                                       ,S.FirstName
                                       ,S.MiddleName
                                       ,CASE @strStudentId
                                          WHEN 'StudentId' THEN S.StudentNumber
                                          WHEN 'EnrollmentId' THEN SE.EnrollmentId
                                          ELSE S.SSN
                                        END AS StudentIdentifier
                                       ,CGC.CampGrpId
                                       ,syCampGrps.CampGrpDescrip
                                       ,SE.CampusId
                                       ,C.CampDescrip
                                       ,0.00 AS Received
                                       ,0.00 AS InstCharges
                                       ,0 AS PmtPlanCount
                                       ,t1.TransAmount AS Refunded
                                       ,0.00 AS RefundToStudent
                              FROM      #TempTrans t1
                                       ,saFundSources FS
                                       ,arStuEnrollments SE
                                       ,arStudent S
                                       ,syCmpGrpCmps CGC
                                       ,syCampGrps
                                       ,syCampuses C
                                       ,arPrgVersions PV
                                       ,saRefunds R
                              WHERE     t1.voided = 0
                                        AND t1.TransactionId = R.TransactionId
                                        AND R.FundSourceId = FS.FundSourceId
                                        AND FS.TitleIV = 1
                                        AND t1.StuEnrollId = SE.StuEnrollId
                                        AND SE.StudentId = S.StudentId
                                        AND CGC.CampusId = SE.CampusId
                                        AND syCampGrps.CampGrpId = CGC.CampGrpId
                                        AND C.CampusId = SE.CampusId
                                        AND t1.Voided = 0
                                        AND PV.PrgVerId = SE.PrgVerId
                                        AND (
                                              (
                                                SELECT  COUNT(TransCodeId)
                                                FROM    saTransCodes TC
                                                WHERE   TC.TransCodeId = t1.TransCodeId
                                                        AND TC.IsInstCharge = 1
                                              ) > 0
                                              OR (
                                                   SELECT   COUNT(T2.TransCodeId)
                                                   FROM     saTransCodes TC
                                                           ,saTransactions T2
                                                           ,saAppliedPayments AP1
                                                   WHERE    AP1.TransactionId = t1.TransactionId
                                                            AND AP1.ApplyToTransId = T2.TransactionId
                                                            AND TC.TransCodeId = T2.TransCodeId
                                                            AND TC.IsInstCharge = 1
                                                 ) > 0
                                            )
                                        AND syCampGrps.campgrpid IN ( SELECT DISTINCT
                                                                                t1.campgrpid
                                                                      FROM      sycmpgrpcmps t1
                                                                      WHERE     t1.campgrpid IN ( SELECT    strval
                                                                                                  FROM      dbo.SPLIT(@campGrpId) ) )
                                        AND t1.TransDate >= @StartDate
                                        AND t1.TransDate <= @EndDate
                                        AND (
                                              @PrgVerId IS NULL
                                              OR SE.PrgVerId IN ( SELECT    strval
                                                                  FROM      dbo.SPLIT(@PrgVerId) )
                                            )
                                        AND R.RefundTypeId <> 0
  
  --------Title IV Institutional Refunds --------
                              UNION
 
 ----------- Get Title IV Payments ------------------
                              SELECT    NEWID() AS AGID
                                       ,AwardTypeId
                                       ,StudentAwardId
                                       ,PrgVerCode AS ProgramVersion
                                       ,TitleIV
                                       ,StuEnrollId
                                       ,LastName
                                       ,FirstName
                                       ,MiddleName
                                       ,StudentIdentifier
                                       ,CampGrpId
                                       ,CampGrpDescrip
                                       ,CampusId
                                       ,CampDescrip
                                       ,SUM(Received) AS Received
                                       ,InstCharges
                                       ,PmtPlanCount
                                       ,0.00 AS Refund
                                       ,0.00 AS RefundToStudent
                              FROM      (
                                          SELECT    '00000000-0000-0000-0000-000000000000' AS AwardTypeId
                                                   ,'00000000-0000-0000-0000-000000000000' AS StudentAwardId
                                                   ,PV.PrgVerCode
                                                   ,FS.TitleIV
                                                   ,t1.StuEnrollId
                                                   ,S.LastName
                                                   ,S.FirstName
                                                   ,S.MiddleName
                                                   ,CASE @strStudentId
                                                      WHEN 'StudentId' THEN S.StudentNumber
                                                      WHEN 'EnrollmentId' THEN SE.EnrollmentId
                                                      ELSE S.SSN
                                                    END AS StudentIdentifier
                                                   ,CGC.CampGrpId
                                                   ,syCampGrps.CampGrpDescrip
                                                   ,SE.CampusId
                                                   ,C.CampDescrip
                                                   ,-t1.TransAmount AS Received
                                                   ,0.00 AS InstCharges
                                                   ,0 AS PmtPlanCount
                                                   ,0.00 AS Refunded
                                          FROM      #TempTrans t1
                                                   ,saFundSources FS
                                                   ,arStuEnrollments SE
                                                   ,arStudent S
                                                   ,syCmpGrpCmps CGC
                                                   ,syCampGrps
                                                   ,syCampuses C
                                                   ,arPrgVersions PV
                                          WHERE     t1.voided = 0
                                                    AND t1.FundSourceId = FS.FundSourceId
                                                    AND FS.TitleIV = 1
                                                    AND t1.StuEnrollId = SE.StuEnrollId
                                                    AND SE.StudentId = S.StudentId
                                                    AND CGC.CampusId = SE.CampusId
                                                    AND syCampGrps.CampGrpId = CGC.CampGrpId
                                                    AND C.CampusId = SE.CampusId
                                                    AND t1.Voided = 0
                                                    AND PV.PrgVerId = SE.PrgVerId
                                                    AND (
                                                          (
                                                            SELECT  COUNT(TransCodeId)
                                                            FROM    saTransCodes TC
                                                            WHERE   TC.TransCodeId = t1.TransCodeId
                                                                    AND TC.IsInstCharge = 1
                                                          ) > 0
                                                          OR (
                                                               SELECT   COUNT(T2.TransCodeId)
                                                               FROM     saTransCodes TC
                                                                       ,saTransactions T2
                                                                       ,saAppliedPayments AP1
                                                               WHERE    AP1.TransactionId = t1.TransactionId
                                                                        AND AP1.ApplyToTransId = T2.TransactionId
                                                                        AND TC.TransCodeId = T2.TransCodeId
                                                                        AND TC.IsInstCharge = 1
                                                             ) > 0
                                                        )
                                                    AND syCampGrps.campgrpid IN ( SELECT DISTINCT
                                                                                            t1.campgrpid
                                                                                  FROM      sycmpgrpcmps t1
                                                                                  WHERE     t1.campgrpid IN ( SELECT    strval
                                                                                                              FROM      dbo.SPLIT(@campGrpId) ) )
                                                    AND t1.TransDate >= @StartDate
                                                    AND t1.TransDate <= @EndDate
                                                    AND (
                                                          @PrgVerId IS NULL
                                                          OR SE.PrgVerId IN ( SELECT    strval
                                                                              FROM      dbo.SPLIT(@PrgVerId) )
                                                        )
                                        ) t
                              GROUP BY  AwardTypeId
                                       ,StudentAwardId
                                       ,PrgVerCode
                                       ,TitleIV
                                       ,StuEnrollId
                                       ,LastName
                                       ,FirstName
                                       ,MiddleName
                                       ,StudentIdentifier
                                       ,CampGrpId
                                       ,CampGrpDescrip
                                       ,CampusId
                                       ,CampDescrip
                                       ,InstCharges
                                       ,PmtPlanCount
                            ) tt




------------------------------------------------------------------------------

----------- Non Title VI funds query--------------------------
                  UNION
                  SELECT    tt.*
                  FROM      (
                              SELECT    NEWID() AS AGID
                                       ,'00000000-0000-0000-0000-000000000000' AS AwardTypeId
                                       ,'00000000-0000-0000-0000-000000000000' AS StudentAwardId
                                       ,PV.PrgVerCode AS ProgramVersion
                                       ,0 AS TitleIV
                                       ,t1.StuEnrollId
                                       ,S.LastName
                                       ,S.FirstName
                                       ,S.MiddleName
                                       ,CASE @strStudentId
                                          WHEN 'StudentId' THEN S.StudentNumber
                                          WHEN 'EnrollmentId' THEN SE.EnrollmentId
                                          ELSE S.SSN
                                        END AS StudentIdentifier
                                       ,CGC.CampGrpId
                                       ,syCampGrps.CampGrpDescrip
                                       ,SE.CampusId
                                       ,C.CampDescrip
                                       ,0.00 AS Received
                                       ,0.00 AS InstCharges
                                       ,0 AS PmtPlanCount
                                       ,t1.TransAmount AS Refunded
                                       ,0.00 AS RefundToStudent
                              FROM      #TempTrans t1
                                       ,saFundSources FS
                                       ,arStuEnrollments SE
                                       ,arStudent S
                                       ,syCmpGrpCmps CGC
                                       ,syCampGrps
                                       ,syCampuses C
                                       ,arPrgVersions PV
                                       ,saRefunds R
                              WHERE     t1.voided = 0
                                        AND t1.TransactionId = R.TransactionId
                                        AND R.FundSourceId = FS.FundSourceId
                                        AND FS.TitleIV = 0
                                        AND t1.StuEnrollId = SE.StuEnrollId
                                        AND SE.StudentId = S.StudentId
                                        AND CGC.CampusId = SE.CampusId
                                        AND syCampGrps.CampGrpId = CGC.CampGrpId
                                        AND C.CampusId = SE.CampusId
                                        AND t1.Voided = 0
                                        AND PV.PrgVerId = SE.PrgVerId
                                        AND (
                                              (
                                                SELECT  COUNT(TransCodeId)
                                                FROM    saTransCodes TC
                                                WHERE   TC.TransCodeId = t1.TransCodeId
                                                        AND TC.IsInstCharge = 1
                                              ) > 0
                                              OR (
                                                   SELECT   COUNT(T2.TransCodeId)
                                                   FROM     saTransCodes TC
                                                           ,saTransactions T2
                                                           ,saAppliedPayments AP1
                                                   WHERE    AP1.TransactionId = t1.TransactionId
                                                            AND AP1.ApplyToTransId = T2.TransactionId
                                                            AND TC.TransCodeId = T2.TransCodeId
                                                            AND TC.IsInstCharge = 1
                                                 ) > 0
                                            )
                                        AND syCampGrps.campgrpid IN ( SELECT DISTINCT
                                                                                t1.campgrpid
                                                                      FROM      sycmpgrpcmps t1
                                                                      WHERE     t1.campgrpid IN ( SELECT    strval
                                                                                                  FROM      dbo.SPLIT(@campGrpId) ) )
                                        AND t1.TransDate >= @StartDate
                                        AND t1.TransDate <= @EndDate
                                        AND (
                                              @PrgVerId IS NULL
                                              OR SE.PrgVerId IN ( SELECT    strval
                                                                  FROM      dbo.SPLIT(@PrgVerId) )
                                            )
                                        AND R.RefundTypeId <> 0
                              UNION
                              SELECT    NEWID() AS AGID
                                       ,AwardTypeId
                                       ,StudentAwardId
                                       ,PrgVerCode AS ProgramVersion
                                       ,0 AS TitleIV
                                       ,StuEnrollId
                                       ,LastName
                                       ,FirstName
                                       ,MiddleName
                                       ,StudentIdentifier
                                       ,CampGrpId
                                       ,CampGrpDescrip
                                       ,CampusId
                                       ,CampDescrip
                                       ,SUM(Received) AS Received
                                       ,InstCharges
                                       ,PmtPlanCount
                                       ,0.00 AS Refunded
                                       ,0.00 AS RefundToStudent
                              FROM      (
                                          SELECT    NULL AS AwardTypeId
                                                   ,NULL AS StudentAwardId
                                                   ,PV.PrgVerCode
                                                   ,FS.TitleIV
                                                   ,t1.StuEnrollId
                                                   ,S.LastName
                                                   ,S.FirstName
                                                   ,S.MiddleName
                                                   ,CASE @strStudentId
                                                      WHEN 'StudentId' THEN S.StudentNumber
                                                      WHEN 'EnrollmentId' THEN SE.EnrollmentId
                                                      ELSE S.SSN
                                                    END AS StudentIdentifier
                                                   ,CGC.CampGrpId
                                                   ,syCampGrps.CampGrpDescrip
                                                   ,SE.CampusId
                                                   ,C.CampDescrip
                                                   ,-t1.TransAmount AS Received
                                                   ,0.00 AS InstCharges
                                                   ,0 AS PmtPlanCount
                                          FROM      #TempTrans t1
                                                   ,saFundSources FS
                                                   ,arStuEnrollments SE
                                                   ,arStudent S
                                                   ,syCmpGrpCmps CGC
                                                   ,syCampGrps
                                                   ,syCampuses C
                                                   ,arPrgVersions PV
                                          WHERE     t1.voided = 0
                                                    AND t1.FundSourceId = FS.FundSourceId
                                                    AND FS.TitleIV = 0
                                                    AND t1.StuEnrollId = SE.StuEnrollId
                                                    AND SE.StudentId = S.StudentId
                                                    AND CGC.CampusId = SE.CampusId
                                                    AND syCampGrps.CampGrpId = CGC.CampGrpId
                                                    AND C.CampusId = SE.CampusId
                                                    AND t1.Voided = 0
                                                    AND PV.PrgVerId = SE.PrgVerId
                                                    AND syCampGrps.campgrpid IN ( SELECT DISTINCT
                                                                                            t1.campgrpid
                                                                                  FROM      sycmpgrpcmps t1
                                                                                  WHERE     t1.campgrpid IN ( SELECT    strval
                                                                                                              FROM      dbo.SPLIT(@campGrpId) ) )
                                                    AND t1.TransDate >= @StartDate
                                                    AND t1.TransDate <= @EndDate
                                                    AND (
                                                          (
                                                            SELECT  COUNT(TransCodeId)
                                                            FROM    saTransCodes TC
                                                            WHERE   TC.TransCodeId = t1.TransCodeId
                                                                    AND TC.IsInstCharge = 1
                                                          ) > 0
                                                          OR (
                                                               SELECT   COUNT(T2.TransCodeId)
                                                               FROM     saTransCodes TC
                                                                       ,saTransactions T2
                                                                       ,saAppliedPayments AP1
                                                               WHERE    AP1.TransactionId = t1.TransactionId
                                                                        AND AP1.ApplyToTransId = T2.TransactionId
                                                                        AND TC.TransCodeId = T2.TransCodeId
                                                                        AND TC.IsInstCharge = 1
                                                             ) > 0
                                                        )
                                                    AND (
                                                          @PrgVerId IS NULL
                                                          OR SE.PrgVerId IN ( SELECT    strval
                                                                              FROM      dbo.SPLIT(@PrgVerId) )
                                                        )
                                        ) t
                              GROUP BY  AwardTypeId
                                       ,StudentAwardId
                                       ,PrgVerCode
                                       ,TitleIV
                                       ,StuEnrollId
                                       ,LastName
                                       ,FirstName
                                       ,MiddleName
                                       ,StudentIdentifier
                                       ,CampGrpId
                                       ,CampGrpDescrip
                                       ,CampusId
                                       ,CampDescrip
                                       ,InstCharges
                                       ,PmtPlanCount
                            ) tt
                ) ttt

--where (Denominator <>0.00 or Numerator <>0.00)
        GROUP BY ProgramVersion
               ,StuEnrollId
               ,LastName
               ,FirstName
               ,MiddleName
               ,StudentIdentifier
               ,CampGrpId
               ,CampGrpDescrip
               ,CampusId
               ,CampDescrip
               ,TitleIV
        ORDER BY LastName
               ,FirstName;


--SELECT * from #tempRecords
        SELECT  ProgramVersion
               ,StuEnrollId
               ,LastName
               ,FirstName
               ,MiddleName
               ,StudentIdentifier
               ,CampGrpId
               ,CampGrpDescrip
               ,CampusId
               ,CampDescrip
               ,SUM(Received) AS Received
               ,SUM(Refunded) AS Refunded
               ,SUM(RefundToStudent) AS RefundToStudent
               ,SUM(Numerator) AS Numerator
               ,SUM(Denominator) AS Denominator
        INTO    #tempRecordsnew
        FROM    #TempRecords
        GROUP BY ProgramVersion
               ,StuEnrollId
               ,LastName
               ,FirstName
               ,MiddleName
               ,StudentIdentifier
               ,CampGrpId
               ,CampGrpDescrip
               ,CampusId
               ,CampDescrip;


--SELECT * FROM  #tempRecordsnew
--select * from #tempRecords
--select * from #tempRecordsnew


-- New Code Added By Vijay Ramteke On Septembaer 06, 2010 --
        SELECT  ISNULL((
                         SELECT SUM(TransAmount)
                         FROM   #TempTransBalance TC
                         WHERE  TC.TransTypeId = 0
                                AND TC.StuEnrollId = TM.StuEnrollId
                       ),0) AS Charges
               ,ISNULL((
                         SELECT SUM(TransAmount)
                         FROM   #TempTransBalance TB
                         WHERE  TB.StuEnrollId = TM.StuEnrollId
                       ),0) AS Balance
               ,ISNULL((
                         SELECT SUM(Amount)
                         FROM   (
                                  --Title IV --
                                  SELECT    -1 * ( -1 * T1.TransAmount ) AS Amount
                                           ,T1.TransactionId
                                  FROM      #TempTransBalance T1
                                           ,saFundSources FS
                                           ,arStuEnrollments SE
                                  WHERE     T1.voided = 0
                                            AND T1.FundSourceId = FS.FundSourceId
                                            AND FS.TitleIV = 1
                                            AND T1.StuEnrollId = SE.StuEnrollId
                                            AND T1.StuEnrollId = TM.StuEnrollId
                                            AND T1.TransTypeId IN ( 1,2 )
                                  UNION
                                  SELECT    -1 * COALESCE(-1 * T1.TransAmount,0.00) AS Amount
                                           ,T1.TransactionId
                                  FROM      #TempTransBalance T1
                                           ,saRefunds R
                                           ,faStudentAwardSchedule SAS
                                           ,faStudentAwards SA
                                           ,saFundSources FS
                                           ,arStuEnrollments SE
                                  WHERE     T1.voided = 0
                                            AND SA.StuEnrollId = SE.StuEnrollId
                                            AND SAS.StudentAwardId = SA.StudentAwardId
                                            AND SA.AwardTypeId = FS.FundSourceId
                                            AND R.StudentAwardId = SA.StudentAwardId
                                            AND T1.TransactionId = R.TransactionId
                                            AND R.RefundTypeId <> 0
                                            AND FS.TitleIV = 1
                                            AND T1.StuEnrollId = SE.StuEnrollId
                                            AND SA.StuEnrollId = T1.StuEnrollId
                                            AND T1.TransTypeId IN ( 1,2 )
                                            AND T1.StuEnrollId = TM.StuEnrollId                                  
        
        ---Title IV---
                                ) tt
                       ),0) AS Payments
               ,ISNULL((
                         SELECT SUM(Amount)
                         FROM   (
                                  SELECT    T1.TransAmount AS Amount
                                           ,T1.TransactionId
                                  FROM      #TempTransBalance T1
                                           ,saRefunds R
                                           ,arStuEnrollments SE
                                  WHERE     T1.voided = 0
                                            AND T1.TransactionId = R.TransactionId
                                            AND T1.StuEnrollId = SE.StuEnrollId
                                            AND R.RefundTypeId = 0
                                            AND T1.TransTypeId IN ( 1,2 )
                                            AND T1.StuEnrollId = TM.StuEnrollId
                                  UNION
                                  SELECT    -1 * ( -1 * T1.TransAmount ) AS Amount
                                           ,T1.TransactionId
                                  FROM      #TempTransBalance T1
                                           ,saFundSources FS
                                           ,arStuEnrollments SE
                                  WHERE     T1.voided = 0
                                            AND T1.FundSourceId = FS.FundSourceId
                                            AND T1.StuEnrollId = SE.StuEnrollId
                                            AND FS.AdvFundSourceId IN ( 1,23 )
                                            AND T1.TransTypeId IN ( 1,2 )
                                            AND T1.StuEnrollId = TM.StuEnrollId
                                ) tt
                       ),0) AS CashAgenctFund
               ,TM.StuEnrollId
        INTO    #tempStuBalance
        FROM    #TempStuEnrollment TM;
 
 --SELECT * FROM #tempStuBalance
 
 
-- New Code Added By Vijay Ramteke On Septembaer 06, 2010 --

        SELECT  TR.ProgramVersion
               ,TR.StuEnrollId
               ,TR.LastName
               ,TR.FirstName
               ,TR.MiddleName
               ,TR.StudentIdentifier
               ,TR.CampGrpId
               ,TR.CampGrpDescrip
               ,TR.CampusId
               ,TR.CampDescrip
               ,TR.Received
               ,TR.Refunded
               ,TR.Numerator
               ,TR.Denominator
               ,ISNULL(TSB.Charges,0) AS Charges
               ,ISNULL(TSB.Balance,0) AS Balance
               ,ISNULL(TSB.Payments,0) AS Payments
               ,ISNULL(TSB.CashAgenctFund,0) AS CashAgencyFund
               ,RefundToStudent
        FROM    #tempRecordsnew TR
        LEFT OUTER JOIN #tempStuBalance TSB ON TR.StuEnrollId = TSB.StuEnrollId
        ORDER BY LastName
               ,FirstName;


 --   SELECT * FROM   #tempRecordsnew
    --    SELECT * FROM  #TempRecords
        -- SELECT * FROM  #TempTrans
        --SELECT * FROM  #TempTransBalance
        -- SELECT * FROM  #tempStuBalance
        -- SELECT * FROM  #TempStuEnrollment



        DROP TABLE #tempRecordsnew;
        DROP TABLE #TempRecords;
        DROP TABLE #TempTrans;
        DROP TABLE #TempTransBalance;
        DROP TABLE #tempStuBalance;
        DROP TABLE #TempStuEnrollment;
    END;

  
                  







GO
