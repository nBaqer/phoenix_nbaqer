SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_EdExpress_Check_StudentAward]
    @StuEnrollId VARCHAR(50)
   ,@FA_Id VARCHAR(50)
AS
    BEGIN

        SELECT  StudentAwardId
               ,GrossAmount
               ,Disbursements
               ,FA_Id
        FROM    faStudentAwards
        WHERE   StuEnrollId = @StuEnrollId
                AND FA_Id = @FA_Id;
    
    END;




GO
