SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[USP_CheckTransDuplicate]
    (
     @StuEnrollId UNIQUEIDENTIFIER
    ,@TermId UNIQUEIDENTIFIER
    ,@CampusId UNIQUEIDENTIFIER
    ,@TransDate DATETIME
    ,@TransCodeId UNIQUEIDENTIFIER
    ,@TransReference VARCHAR(50)
    ,@AcademicYearId UNIQUEIDENTIFIER
    ,@TransDescrip VARCHAR(50)
    ,@TransAmount DECIMAL(19,4)
    ,@TransTypeId INT
    ,@IsPosted BIT
    ,@IsVoided BIT
    ,@FeeLevelId TINYINT
    ,@FeeId UNIQUEIDENTIFIER
    ,@PaymentCodeId UNIQUEIDENTIFIER
    ,@FundSourceId UNIQUEIDENTIFIER 
    )
AS
    BEGIN


        SELECT  COUNT(*)
        FROM    dbo.saTransactions
        WHERE   StuEnrollId = @StuEnrollId
                AND (
                      TermId = @TermId
                      OR @TermId IS NULL
                    )
                AND (
                      CampusId = @CampusId
                      OR @CampusId IS NULL
                    )
                AND TransDate = @TransDate
                AND (
                      TransCodeId = @TransCodeId
                      OR @TransCodeId IS NULL
                    )
                AND TransReference = @TransReference
                AND (
                      AcademicYearId = @AcademicYearId
                      OR @AcademicYearId IS NULL
                    )
                AND TransDescrip = @TransDescrip
                AND TransAmount = @TransAmount
                AND TransTypeId = @TransTypeId
                AND IsPosted = @IsPosted
                AND Voided = @IsVoided
                AND (
                      FeeLevelId = @FeeLevelId
                      OR @FeeLevelId IS NULL
                    )
                AND (
                      FeeId = @FeeId
                      OR @FeeId IS NULL
                    )
                AND (
                      PaymentCodeId = @PaymentCodeId
                      OR @PaymentCodeId IS NULL
                    )
                AND (
                      FundSourceId = @FundSourceId
                      OR @FundSourceId IS NULL
                    );
                
    END;




GO
