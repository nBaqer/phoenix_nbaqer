SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/* 
CREATED: 


PURPOSE: 


MODIFIED:
11.8.2013	WP	Add union with arStudentClockAttendance

*/      
CREATE PROCEDURE [dbo].[usp_GetAttendanceMinutes]
    (
     @stuEnrollId UNIQUEIDENTIFIER
    ,@cutOffDate DATETIME      
    )
AS
    SET NOCOUNT ON;      
  
    SELECT DISTINCT
            CSA.ClsSectionId
           ,CSA.MeetDate
           ,CSA.Actual
           ,CSA.Tardy
           ,CSA.Excused
           ,(
              SELECT    t3.UnitTypeDescrip
              FROM      arClassSections t1
                       ,arReqs t2
                       ,arAttUnitType t3
              WHERE     t1.ClsSectionId = CSA.ClsSectionId
                        AND t1.ReqId = t2.ReqId
                        AND t2.UnitTypeId = t3.UnitTypeId
            ) AS AttendanceType
           ,WorkDaysDescrip
           ,CS.TermID
           ,CSA.ClsSectMeetingId
           ,CSA.Scheduled
           ,CSM.InstructionTypeID
    FROM    arClassSections CS
           ,atClsSectAttendance CSA
           ,arClsSectMeetings CSM
           ,plWorkDays WD
    WHERE   CS.ClsSectionId = CSA.ClsSectionId
            AND CSA.ClsSectionId = CSM.ClsSectionId
            AND CSA.ClsSectMeetingId = CSM.ClsSectMeetingId
            AND WD.ViewOrder = DATEPART(dw,MeetDate) - 1
            AND StuEnrollId = @stuEnrollId
            AND CSA.MeetDate <= @cutOffDate
            AND CSA.Actual <> 999
            AND CSA.Actual <> 9999
            AND CSA.Actual <> 99
    UNION
    SELECT DISTINCT
            NULL AS ClsSectionId
           ,sca.RecordDate AS MeetDate
           ,sca.ActualHours AS Actual
           ,isTardy AS Tardy
           ,0 AS Excused
           ,NULL AS AttendanceType
           ,NULL AS WorkDaysDescrip
           ,NULL AS TermID
           ,NULL AS ClsSectMeetingId
           ,sca.SchedHours AS Scheduled
           ,NULL AS InstructionTypeID
    FROM    arStudentClockAttendance sca
    JOIN    arStuEnrollments se ON se.StuEnrollId = sca.StuEnrollId
    WHERE   sca.Converted = 1
            AND sca.ActualHours <> 9999
            AND sca.ActualHours <> 999
            AND sca.ActualHours <> 99
            AND sca.StuEnrollId = @stuEnrollId
            AND sca.RecordDate <= @cutOffDate
    ORDER BY CSA.MeetDate;




GO
