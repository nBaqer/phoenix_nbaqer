SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--------------------------------------------------------------------------------------------------------------------------------------------------



CREATE PROCEDURE [dbo].[usp_GetAvailableStudents_WithNoLeadGroups]
    @clsSectionId UNIQUEIDENTIFIER
   ,@campusId UNIQUEIDENTIFIER    
--,@leadGrpId uniqueidentifier    
AS
    SET NOCOUNT ON;    
    
    SELECT DISTINCT
            se.StuEnrollId
           ,se.ShiftId
           ,pg.ProgDescrip
           ,st.LastName
           ,st.FirstName
           ,pv.PrgVerId
           ,pg.ProgId
           ,se.ExpGradDate
           ,lg.LeadGrpId
           ,se.ExpStartDate
    FROM    arStuEnrollments se
           ,arPrgVersions pv
           ,arPrograms pg
           ,arStudent st
           ,syStatusCodes sc
           ,adLeadByLeadGroups lg
    WHERE   se.stuEnrollid = lg.stuenrollid
            AND se.PrgVerId = pv.PrgVerId
            AND se.StudentId = st.StudentId
            AND pv.ProgId = pg.ProgId
            AND se.StatusCodeId = sc.StatusCodeId
            AND sc.SysStatusId IN ( 7,9,13,20 )
            AND (
                  pv.ProgId = (
                                SELECT  tm.ProgId
                                FROM    arClassSections cs
                                       ,arClassSectionTerms ct
                                       ,arTerm tm
                                       ,arProgVerDef pvd
                                WHERE   cs.ClsSectionId = ct.ClsSectionId
                                        AND ct.TermId = tm.TermId
                                        AND pv.PrgVerId = pvd.PrgVerId
                                        AND pvd.Reqid = cs.Reqid
                                        AND cs.ClsSectionId = @clsSectionId
                                UNION
                                SELECT  tm.ProgId
                                FROM    arClassSections cs
                                       ,arClassSectionTerms ct
                                       ,arTerm tm
                                       ,arProgVerDef pvd
                                       ,arReqs R
                                       ,arreqGrpDef GD
                                WHERE   cs.ClsSectionId = ct.ClsSectionId
                                        AND ct.TermId = tm.TermId
                                        AND pv.PrgVerId = pvd.PrgVerId
                                        AND pvd.Reqid = R.Reqid
                                        AND R.ReqId = GD.GrpId
                                        AND GD.ReqId = cs.ReqId
                                        AND cs.ClsSectionId = @clsSectionId
                              )
                  OR (
                       SELECT   ISNULL(CAST(tm.ProgId AS CHAR(50)),'')
                       FROM     arClassSections cs
                               ,arClassSectionTerms ct
                               ,arTerm tm
                               ,arProgVerDef pvd
                       WHERE    cs.ClsSectionId = ct.ClsSectionId
                                AND ct.TermId = tm.TermId
                                AND pv.PrgVerId = pvd.PrgVerId
                                AND pvd.Reqid = cs.Reqid
                                AND cs.ClsSectionId = @clsSectionId
                     ) = ''
                )
            AND (
                  se.ExpStartDate = (
                                      SELECT    StudentStartDate
                                      FROM      arClassSections
                                      WHERE     ClsSectionId = @clsSectionId
                                    )
                  OR se.stuEnrollid IN ( SELECT stuEnrollid
                                         FROM   adLeadByLeadGroups a
                                               ,arClassSections b
                                         WHERE  a.LeadGrpId = b.LeadGrpId
                                                AND b.ClsSectionId = @clsSectionId )
                  OR se.CohortStartDate = (
                                            SELECT  CohortStartDate
                                            FROM    arClassSections
                                            WHERE   ClsSectionId = @clsSectionId
                                          )
                )
            AND se.stuenrollid NOT IN ( SELECT  stuenrollid
                                        FROM    arResults
                                               ,arGradesystemdetails
                                        WHERE   testid IN ( SELECT  ClsSectionId
                                                            FROM    arclassSections
                                                            WHERE   reqid IN ( SELECT   equivreqid
                                                                               FROM     arCourseequivalent
                                                                               WHERE    reqid = (
                                                                                                  SELECT    reqid
                                                                                                  FROM      arClassSections
                                                                                                  WHERE     ClsSectionid = @clsSectionId
                                                                                                ) ) )
                                                AND arResults.GrdSysDetailid = arGradesystemdetails.GrdSysDetailid
                                                AND arGradesystemdetails.IsPass = 1 )
            AND se.StuEnrollId NOT IN ( SELECT  StuEnrollid
                                        FROM    arresults
                                        WHERE   TestId = @clsSectionId )    
 --New Code Added By Vijay Ramteke On June 20, 2010    
 --and se.StuEnrollId not in (select SE.StuEnrollId from adStuGrpStudents SGS,arStuEnrollments SE,adStudentGroups SG where SGS.StudentId=SE.StudentId and SGS.StuGrpId=SG.StuGrpId and SG.IsRegHold=1)     
            AND se.StuEnrollId NOT IN ( SELECT  SGS.StuEnrollId
                                        FROM    adStuGrpStudents SGS
                                               ,adStudentGroups SG
                                        WHERE   SGS.StuGrpId = SG.StuGrpId
                                                AND SG.IsRegHold = 1
                                                AND SGS.IsDeleted = 0
                                                AND SGS.StuEnrollId IS NOT NULL )
            AND se.StuEnrollId NOT IN ( SELECT DISTINCT
                                                StuEnrollId
                                        FROM    arTransferGrades t9
                                               ,arGradeSystemDetails t10
                                        WHERE   t9.GrdSysDetailID = t10.GrdSysDetailID
                                                AND t10.IsPass = 1
                                                AND t9.ReqId IN ( SELECT    ReqId
                                                                  FROM      arClassSections
                                                                  WHERE     ClsSectionId = @clsSectionId ) )
            AND se.campusid = @campusId    
 --New Code Added By Vijay Ramteke On June 20, 2010    
    ORDER BY se.ExpGradDate
           ,st.LastName
           ,st.FirstName;    
  



GO
