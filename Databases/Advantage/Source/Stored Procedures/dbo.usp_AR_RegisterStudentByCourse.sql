SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_AR_RegisterStudentByCourse]
    @ClsSectionId UNIQUEIDENTIFIER
   ,@StuEnrollId UNIQUEIDENTIFIER
   ,@User NVARCHAR(50)
AS
    BEGIN

        INSERT  INTO arResults
                (
                 ResultId
                ,TestId
                ,StuEnrollId
                ,ModUser
                ,ModDate
                )
        VALUES  (
                 NEWID()
                ,@ClsSectionId
                ,@StuEnrollId
                ,@User
                ,GETDATE()
                );

    END;



GO
