SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[usp_AR_GetAllStudentByStartDateAndProgramCountByStatus]
    @StartDate DATETIME
   ,@CampGrpId AS VARCHAR(8000)
   ,@ProgVerId AS VARCHAR(8000) = NULL
AS /*----------------------------------------------------------------------------------------------------
    Author : Vijay Ramteke
    
    Create date : 09/13/2010
    
    Procedure Name : usp_AR_GetAllStudentByStartDateAndProgramCountByStatus

    Objective : Get All The Student Count Status For Start Date And Program Version
    
    Parameters : Name Type Data Type Required? 
    
    Output : Returns All The Student Count Status For Start Date And Program Version for Report Dataset
                        
*/-----------------------------------------------------------------------------------------------------

    BEGIN

        DECLARE @IsCampusgrpAll INT;
        SELECT  @IsCampusgrpAll = COUNT(*)
        FROM    syCampGrps
        WHERE   CampGrpId IN ( SELECT   strval
                               FROM     dbo.SPLIT(@CampGrpId) )
                AND CampGrpDescrip = 'All';

        CREATE TABLE #tempStatusCodes
            (
             EnrollmentStatusCount INT
            ,PrgVerId UNIQUEIDENTIFIER
            ,CampusId UNIQUEIDENTIFIER
            ,CampGrpId UNIQUEIDENTIFIER
            ,StatusCodeId UNIQUEIDENTIFIER
            );

        IF @IsCampusgrpAll > 0
            BEGIN
                INSERT  INTO #tempStatusCodes
                        SELECT  COUNT(SC.StatusCodeId) AS EnrollmentStatusCount
                               ,SE.PrgVerId
                               ,SE.CampusId
                               ,CG.CampGrpId
                               ,SE.StatusCodeId
                        FROM    arStuEnrollments SE
                               ,syStatusCodes SC
                               ,syCampuses C
                               ,syCmpGrpCmps CGC
                               ,syCampGrps CG
                               ,sySysStatus SS
                        WHERE   SE.StatusCodeId = SC.StatusCodeId
                                AND SC.SysStatusId = SS.SysStatusId
                                AND SS.StatusLevelId = 2
                                AND C.CampusId = SE.CampusId
                                AND CGC.CampGrpId = CG.CampGrpId
                                AND CGC.CampusId = C.CampusId
                                AND CG.CampGrpDescrip = 'All'
                                AND CAST(SE.StartDate AS DATE) = CAST(@StartDate AS DATE)
                                AND (
                                      SE.PrgVerId IN ( SELECT   strval
                                                       FROM     dbo.SPLIT(@ProgVerId) )
                                      OR @ProgVerId IS NULL
                                    )
                                --AND SE.CampusId IN ( SELECT DISTINCT
                                --                            t1.CampusId
                                --                     FROM   syCmpGrpCmps t1
                                --                     WHERE  t1.CampGrpId IN ( SELECT    strval
                                --                                              FROM      dbo.SPLIT(@CampGrpId) ) )
                                --AND CG.CampGrpId IN ( SELECT DISTINCT
                                --                                t1.CampGrpId
                                --                      FROM      syCmpGrpCmps t1
                                --                      WHERE     t1.CampGrpId IN ( SELECT    strval
                                --                                                  FROM      dbo.SPLIT(@CampGrpId) ) )
                        GROUP BY CG.CampGrpId
                               ,SE.CampusId
                               ,SE.PrgVerId
                               ,SE.StatusCodeId;
            END;
        ELSE
            BEGIN
                INSERT  INTO #tempStatusCodes
                        SELECT  COUNT(SC.StatusCodeId) AS EnrollmentStatusCount
                               ,SE.PrgVerId
                               ,SE.CampusId
                               ,CG.CampGrpId
                               ,SE.StatusCodeId
                        FROM    arStuEnrollments SE
                               ,syStatusCodes SC
                               ,syCampuses C
                               ,syCmpGrpCmps CGC
                               ,syCampGrps CG
                               ,sySysStatus SS
                        WHERE   SE.StatusCodeId = SC.StatusCodeId
                                AND SC.SysStatusId = SS.SysStatusId
                                AND SS.StatusLevelId = 2
                                AND C.CampusId = SE.CampusId
                                AND CGC.CampGrpId = CG.CampGrpId
                                AND CGC.CampusId = C.CampusId
                                AND CG.CampGrpDescrip <> 'All'
                                AND CAST(SE.StartDate AS DATE) = CAST(@StartDate AS DATE)
                                AND (
                                      SE.PrgVerId IN ( SELECT   strval
                                                       FROM     dbo.SPLIT(@ProgVerId) )
                                      OR @ProgVerId IS NULL
                                    )
                                AND SE.CampusId IN ( SELECT DISTINCT
                                                            t1.CampusId
                                                     FROM   syCmpGrpCmps t1
                                                     WHERE  t1.CampGrpId IN ( SELECT    strval
                                                                              FROM      dbo.SPLIT(@CampGrpId) ) )
                                AND CG.CampGrpId IN ( SELECT DISTINCT
                                                                t1.CampGrpId
                                                      FROM      syCmpGrpCmps t1
                                                      WHERE     t1.CampGrpId IN ( SELECT    strval
                                                                                  FROM      dbo.SPLIT(@CampGrpId) ) )
                        GROUP BY CG.CampGrpId
                               ,SE.CampusId
                               ,SE.PrgVerId
                               ,SE.StatusCodeId;
            END;


--Select COUNT(TSC.EnrollmentStatusCount) as EnrollmentStatusCount, 
--SC.StatusCodeDescrip, TSC.PrgVerId, TSC.CampusId, TSC.CampGrpId 
--from syStatusCodes SC Left Outer Join #tempStatusCodes TSC On SC.StatusCodeId=TSC.StatusCodeId
--Group By TSC.CampGrpId, TSC.CampusId, TSC.PrgVerId, SC.StatusCodeDescrip

----------

        SELECT  SC.StatusCodeId
               ,TSC.PrgVerId
               ,TSC.CampusId
               ,TSC.CampGrpId
               ,SC.SysStatusId
        INTO    #tempAllStatusCodes
        FROM    syStatusCodes SC
        LEFT OUTER JOIN #tempStatusCodes TSC ON SC.StatusCodeId = TSC.StatusCodeId
        INNER JOIN sySysStatus SS ON SS.SysStatusId = SC.SysStatusId
        WHERE   SS.StatusLevelId = 2;

        SELECT DISTINCT
                PrgVerId
               ,CampGrpId
               ,CampusId
        INTO    #tempDistPCCG
        FROM    #tempAllStatusCodes
        WHERE   (
                  PrgVerId IS NOT NULL
                  AND CampGrpId IS NOT NULL
                  AND CampusId IS NOT NULL
                );

        SELECT DISTINCT
                StatusCodeId
               ,SysStatusId
        INTO    #tempDistPCCGNULL
        FROM    #tempAllStatusCodes
        WHERE   (
                  PrgVerId IS NULL
                  AND CampGrpId IS NULL
                  AND CampusId IS NULL
                );

--Select * into #tempPCCGSC from #tempDistPCCGNULL, #tempDistPCCG
        SELECT  SC.StatusCodeId
               ,SC.SysStatusId
               ,T3.*
        INTO    #tempPCCGSC
        FROM    syStatusCodes SC
               ,#tempDistPCCG T3;

        SELECT  EnrollmentStatusCount
               ,PrgVerId
               ,CampusId
               ,CampGrpId
               ,(
                  SELECT    StatusCodeDescrip
                  FROM      syStatusCodes SC
                  WHERE     SC.StatusCodeId = TSC.StatusCodeId
                ) AS StatusCodeDescrip
               ,TSC.StatusCodeId
        FROM    #tempStatusCodes TSC
        UNION
        SELECT  0 AS EnrollmentStatusCount
               ,PrgVerId
               ,CampusId
               ,CampGrpId
               ,(
                  SELECT    StatusCodeDescrip
                  FROM      syStatusCodes SC
                           ,sySysStatus SS
                  WHERE     SC.StatusCodeId = T1.StatusCodeId
                            AND SC.SysStatusId = SS.SysStatusId
                            AND SS.StatusLevelId = 2
                ) AS StatusCodeDescrip
               ,T1.StatusCodeId
        FROM    #tempPCCGSC T1
               ,sySysStatus SS
        WHERE   T1.SysStatusId = SS.SysStatusId
                AND SS.StatusLevelId = 2
                AND T1.StatusCodeId NOT IN ( SELECT DISTINCT
                                                    StatusCodeId
                                             FROM   #tempStatusCodes
                                             WHERE  PrgVerId = T1.PrgVerId
                                                    AND CampusId = T1.CampusId
                                                    AND CampGrpId = T1.CampGrpId )
        ORDER BY StatusCodeDescrip;
-------------

        DROP TABLE #tempStatusCodes;
        DROP TABLE #tempAllStatusCodes;
        DROP TABLE #tempDistPCCG;
        DROP TABLE #tempDistPCCGNULL;
        DROP TABLE #tempPCCGSC;

    END;








GO
