SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_ComputeWithCreditsPerService]
    (
     @stuEnrollId UNIQUEIDENTIFIER
    ,@reqId UNIQUEIDENTIFIER
    )
AS
    SET NOCOUNT ON;
    SELECT  SUM(GBR.Score) AS LabsCompletedByStudent
           ,(
              SELECT DISTINCT
                        isClinicsSatisfied
              FROM      arResults
              WHERE     StuEnrollId = @stuEnrollId
                        AND TestId IN ( SELECT  ClsSectionId
                                        FROM    arClassSections
                                        WHERE   ReqId = @reqId )
                                /* Code added by kamalesh ahuja on March 08 2010*/
                        AND isClinicsSatisfied IS NOT NULL
                                /**/
            ) AS isClinicsSatisfied
           ,GBR.InstrGrdBkWgtDetailId
           ,GBWD.Number AS RequiredNumberofLabs
           ,GBWD.CreditsPerService
    FROM    (
              SELECT    *
              FROM      arGrdBkResults
              WHERE     Score IS NOT NULL
                        AND ClsSectionId IN ( SELECT DISTINCT
                                                        ClsSectionId
                                              FROM      arClassSections
                                              WHERE     ReqId = @reqId )
            ) GBR
           ,arGrdBkWeights GBW
           ,arGrdBkWgtDetails GBWD
           ,arGrdComponentTypes GDT
    WHERE   GBR.InstrGrdBkWgtDetailId = GBWD.InstrGrdBkWgtDetailId
            AND GBW.InstrGrdBkWgtId = GBWD.InstrGrdBkWgtId
            AND GBWD.GrdComponentTypeId = GDT.GrdComponentTypeId
            AND GBR.Score IS NOT NULL
            AND StuEnrollId = @stuEnrollId
    GROUP BY GBR.InstrGrdBkWgtDetailId
           ,GBWD.Number
           ,GBWD.CreditsPerService; 




GO
