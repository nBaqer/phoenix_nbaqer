SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[USP_IsStudentProgramClockHourType]
    (
     @StuEnrollId UNIQUEIDENTIFIER
    )
AS /*----------------------------------------------------------------------------------------------------
	Author          :	Saraswathi Lakshmanan
    
    Create date		:	06/01/2010
    
	Procedure Name	:	[USP_IsStudentProgramClockHourType]

	Objective		:	Find if the student has a clock hour program type
	
	Parameters		:	Name			Type	Data Type			Required? 	
						=====			====	=========			=========	
						@StuEnrollId	In		UniqueIdentifier	Required
	
	Output			: Returns if the student has a clock hour program type 
						
*/-----------------------------------------------------------------------------------------------------

    BEGIN


        SELECT  arPrograms.ACId
        FROM    arprgversions
               ,arprograms
               ,syAcademicCalendars
               ,arStuEnrollments
        WHERE   arPrgVersions.ProgId = arPrograms.ProgId
                AND syAcademicCalendars.ACId = arPrograms.ACId
                AND arStuEnrollments.PrgVerId = arPrgVersions.PrgVerId
                AND ACDescrip LIKE 'Clock%'
                AND StuEnrollId = @StuEnrollId;
    END;




GO
