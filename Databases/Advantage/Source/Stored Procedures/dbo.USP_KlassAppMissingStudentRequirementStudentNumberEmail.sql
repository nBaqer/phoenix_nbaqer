SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		JAGG
-- Create date: 6/29/2017
-- Description:	
-- =============================================
CREATE PROCEDURE [dbo].[USP_KlassAppMissingStudentRequirementStudentNumberEmail]
AS
    BEGIN
        -- SET NOCOUNT ON added to prevent extra result sets from
        -- interfering with SELECT statements.
        SET NOCOUNT ON;

        -- Insert statements for procedure here
        SELECT   DISTINCT l.FirstName
                ,LastName
                ,l.StudentNumber
                ,em.EMail
        FROM     dbo.adLeads l
        LEFT JOIN AdLeadEmail em ON em.LeadId = l.LeadId
        WHERE    l.StudentId <> '00000000-0000-0000-0000-000000000000'
                 AND (
                     l.StudentNumber IS NULL
                     OR l.StudentNumber = ''
                     OR em.EMail IS NULL
                     OR em.EMail = ''
                     )
        ORDER BY LastName
                ,FirstName;
    END;
--=================================================================================================
-- END  --  USP_KlassAppMissingStudentRequirementStudentNumberEmail
--=================================================================================================
GO
