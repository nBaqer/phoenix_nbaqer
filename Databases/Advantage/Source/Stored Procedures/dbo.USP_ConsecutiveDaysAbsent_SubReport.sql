SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[USP_ConsecutiveDaysAbsent_SubReport]
    @TermId VARCHAR(100)
   ,@ReqId VARCHAR(4000)
   ,@ConsDays INT
   ,@CutoffDate DATETIME
AS
    BEGIN
        DECLARE @ClsSectionId VARCHAR(100);
        SELECT  *
        FROM    (
                  SELECT    st.StudentNumber
                           ,st.FirstName + ' ' + ISNULL(st.MiddleName,'') + ' ' + st.LastName AS Student
                           ,se.StartDate
                           ,( CASE WHEN se.LDA IS NULL THEN CONVERT(DATE,(
                                                                           SELECT TOP 1
                                                                                    StudentAttendedDate
                                                                           FROM     syStudentAttendanceSummary
                                                                           WHERE    StuEnrollId = se.StuEnrollId
                                                                                    AND ActualDays <> 0
                                                                                    AND ActualDays <> 9999
                                                                                    AND ActualDays <> 99999
                                                                           ORDER BY StudentAttendedDate DESC
                                                                         ),101)
                                   ELSE se.LDA
                              END ) AS LDA
                           ,sc.StatusCodeDescrip AS status
                           ,tm.TermDescrip
                           ,tm.StartDate AS [Term Start Date]
                           ,rq.ReqId
                           ,rq.Descrip AS Course
                           ,(
                              SELECT    dbo.ConsecutiveDaysAbsent(se.StuEnrollId,cs.ClsSectionId,@CutoffDate)
                            ) AS 'Days Missed'
                  FROM      arStudent st
                  INNER JOIN dbo.arStuEnrollments se ON st.StudentId = se.StudentId
                  INNER JOIN arPrgVersions pv ON se.PrgVerId = pv.PrgVerId
                  INNER JOIN dbo.arResults rs ON se.StuEnrollId = rs.StuEnrollId
                  INNER JOIN dbo.arClassSections cs ON rs.TestId = cs.ClsSectionId
                  INNER JOIN dbo.arTerm tm ON cs.TermId = tm.TermId
                  INNER JOIN arReqs rq ON cs.ReqId = rq.ReqId
                  INNER JOIN dbo.syStatusCodes sc ON se.StatusCodeId = sc.StatusCodeId
                  WHERE     tm.TermId = @TermId
                            AND rq.ReqId = @ReqId
                ) R
        WHERE   ( R.[Days Missed] >= @ConsDays )
        ORDER BY R.Course;
    END;




GO
