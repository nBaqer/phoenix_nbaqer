SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_OutcomeMeasures_Detail]
    @CampusId VARCHAR(50)
   ,@ProgId VARCHAR(4000) = NULL
   ,@StartDate DATETIME = NULL
   ,@EndDate DATETIME = NULL
   ,@DateRangeText VARCHAR(100) = NULL
   ,@OrderBy VARCHAR(100)
   ,@FullOrPartTime INT
   ,@FirstTimeOrNot INT
AS
    DECLARE @ReturnValue VARCHAR(100);
    DECLARE @ContinuingStartDate DATETIME;

    SET @ContinuingStartDate = @StartDate;
	-- For Academic Year Reporter, the End Date should be 10/15/Cohort Year
	-- and Start Date should be blank
    IF DAY(@EndDate) = 15
        AND MONTH(@EndDate) = 10
        BEGIN
            SET @EndDate = @StartDate; -- it will always be 10/15/cohort year example: for cohort year 2009, it is 10/15/2009
            SET @StartDate = @StartDate;
        END;

    IF @ProgId IS NOT NULL
        BEGIN
            SELECT  @ReturnValue = COALESCE(@ReturnValue,'') + ProgDescrip + ','
            FROM    arPrograms t1
            WHERE   t1.ProgId IN ( SELECT   Val
                                   FROM     MultipleValuesForReportParameters(@ProgId,',',1) );
            SET @ReturnValue = SUBSTRING(@ReturnValue,1,LEN(@ReturnValue) - 1);
        END;
    ELSE
        BEGIN
            SELECT  @ReturnValue = COALESCE(@ReturnValue,'') + ProgDescrip + ','
            FROM    arPrograms t1
            WHERE   t1.CampGrpId IN ( SELECT    t1.CampGrpId
                                      FROM      syCmpGrpCmps
                                      WHERE     CampusId = @CampusId );
            SET @ReturnValue = SUBSTRING(@ReturnValue,1,LEN(@ReturnValue) - 1);
        END;
	
	
	-----------------------------------------------------------------
	---John ginzo - start - get list of enrollments
	-----------------------------------------------------------------
    DECLARE @enrollments TABLE
        (
         StuEnrollId UNIQUEIDENTIFIER
        );
    DECLARE @InSchoolEnrolmentOutput TABLE
        (
         StuEnrollId UNIQUEIDENTIFIER
        ,DateOfChange DATETIME
        ,WithinPeriod BIT
        ,BeforePeriod BIT
        );
    DECLARE @StuEnrollId UNIQUEIDENTIFIER;

    INSERT  INTO @enrollments
            SELECT  StuEnrollId
            FROM    dbo.arStuEnrollments e
            INNER JOIN dbo.syStatusCodes st ON e.StatusCodeId = st.StatusCodeId
            INNER JOIN dbo.sySysStatus ss ON ss.SysStatusId = ss.SysStatusId
            WHERE   ss.InSchool = 0;
			

	--SELECT * FROM @enrollments

    DECLARE @enrollmentsbeforestart TABLE
        (
         StuEnrollId UNIQUEIDENTIFIER
        );

    INSERT  INTO @InSchoolEnrolmentOutput
            SELECT DISTINCT
                    ( e.StuEnrollId )
                   ,e2.inPeriodDateOfChange
                   ,1
                   ,0
            FROM    @enrollments e
            INNER JOIN (
                         SELECT StuEnrollId
                               ,MIN(DateOfChange) AS inPeriodDateOfChange
                         FROM   dbo.syStudentStatusChanges e
                         INNER JOIN dbo.syStatusCodes st ON e.NewStatusId = st.StatusCodeId
                         INNER JOIN dbo.sySysStatus ss ON st.SysStatusId = ss.SysStatusId
                         WHERE  ss.InSchool = 1
                                AND DateOfChange BETWEEN @StartDate AND @EndDate
                         GROUP BY StuEnrollId
                       ) e2 ON e.StuEnrollId = e2.StuEnrollId;        


	--SELECT * FROM @InSchoolEnrolmentOutput


    DECLARE ENROLLMENT_CHANGES CURSOR
    FOR
        SELECT  StuEnrollId
        FROM    @enrollments;

    OPEN ENROLLMENT_CHANGES;

    FETCH NEXT FROM ENROLLMENT_CHANGES INTO @StuEnrollId;

    WHILE @@FETCH_STATUS = 0
        BEGIN	
	   
            DECLARE @inSchoolDateofChange DATETIME;
            SET @inSchoolDateofChange = (
                                          SELECT    MAX(DateOfChange)
                                          FROM      dbo.syStudentStatusChanges e
                                          INNER JOIN dbo.syStatusCodes st ON e.NewStatusId = st.StatusCodeId
                                          INNER JOIN dbo.sySysStatus ss ON st.SysStatusId = ss.SysStatusId
                                          WHERE     ss.InSchool = 1
                                                    AND DateOfChange < @StartDate
                                                    AND StuEnrollId = @StuEnrollId
                                        );

            IF @inSchoolDateofChange IS NOT NULL
                BEGIN		
                    DECLARE @OutOFSchoolChange DATETIME;

                    IF EXISTS ( SELECT  DateOfChange
                                FROM    dbo.syStudentStatusChanges e
                                INNER JOIN dbo.syStatusCodes st ON e.NewStatusId = st.StatusCodeId
                                INNER JOIN dbo.sySysStatus ss ON st.SysStatusId = ss.SysStatusId
                                WHERE   ss.InSchool = 0
                                        AND DateOfChange >= @inSchoolDateofChange
                                        AND DateOfChange > @StartDate
                                        AND e.StuEnrollId = @StuEnrollId )
                        AND NOT EXISTS ( SELECT *
                                         FROM   @InSchoolEnrolmentOutput
                                         WHERE  StuEnrollId = @StuEnrollId )
                        BEGIN
                            INSERT  INTO @InSchoolEnrolmentOutput
                                    (
                                     StuEnrollId
                                    ,DateOfChange
                                    ,WithinPeriod
                                    ,BeforePeriod
                                    )
                            VALUES  (
                                     @StuEnrollId
                                    ,@inSchoolDateofChange
                                    ,0
                                    ,1
                                    );
                        END;
                END;

            FETCH NEXT FROM ENROLLMENT_CHANGES INTO @StuEnrollId;

        END;


    CLOSE ENROLLMENT_CHANGES;
    DEALLOCATE ENROLLMENT_CHANGES;

	-----------------------------------------------------------------
	--John ginzo - end 
	-----------------------------------------------------------------

    DECLARE @StatusAsOf2013 DATETIME
       ,@StatusAsOf2015 DATETIME;
    SET @StatusAsOf2013 = '08/31/' + CONVERT(CHAR(4),YEAR(GETDATE()) - 2); 
    SET @StatusAsOf2015 = '08/31/' + CONVERT(CHAR(4),YEAR(GETDATE()));


    DECLARE @ExclusionsAsOf2013 DATETIME
       ,@ExclusionsAsOf2015 DATETIME;
    SET @ExclusionsAsOf2013 = '09/01/' + CONVERT(CHAR(4),YEAR(GETDATE()) - 3); --2017 is Current Year and 3 Yr Prev is 2014
    SET @ExclusionsAsOf2015 = '08/31/' + CONVERT(CHAR(4),YEAR(GETDATE()) - 1); --2017 is Current Year and 1 Yr Prev is 2016


    CREATE TABLE #GraduatedStudentsAsOfAug312013
        (
         StudentId UNIQUEIDENTIFIER
        );

    CREATE TABLE #GraduatedStudentsAsOfAug312015
        (
         StudentId UNIQUEIDENTIFIER
        );

    CREATE TABLE #OutofSchoolStudentsAsOfAug312013
        (
         StuEnrollId UNIQUEIDENTIFIER
        );

    CREATE TABLE #OutofSchoolStudentsAsOfAug312015
        (
         StuEnrollId UNIQUEIDENTIFIER
        );

    INSERT  INTO #OutofSchoolStudentsAsOfAug312013
            SELECT  DISTINCT
                    e.StuEnrollId
            FROM    dbo.syStudentStatusChanges e
            INNER JOIN dbo.syStatusCodes st ON e.NewStatusId = st.StatusCodeId
            INNER JOIN dbo.sySysStatus ss ON st.SysStatusId = ss.SysStatusId
            WHERE   ss.InSchool = 0
                    AND DateOfChange < @StatusAsOf2013
                    AND e.DateOfChange = (
                                           SELECT   MAX(DateOfChange)
                                           FROM     dbo.syStudentStatusChanges
                                           WHERE    StuEnrollId = e.StuEnrollId
                                         );
			
                                        
    INSERT  INTO #OutofSchoolStudentsAsOfAug312015
            SELECT  DISTINCT
                    e.StuEnrollId
            FROM    dbo.syStudentStatusChanges e
            INNER JOIN dbo.syStatusCodes st ON e.NewStatusId = st.StatusCodeId
            INNER JOIN dbo.sySysStatus ss ON st.SysStatusId = ss.SysStatusId
            WHERE   ss.InSchool = 0
                    AND DateOfChange < @StatusAsOf2015
                    AND e.DateOfChange = (
                                           SELECT   MAX(DateOfChange)
                                           FROM     dbo.syStudentStatusChanges
                                           WHERE    StuEnrollId = e.StuEnrollId
                                         );
										--ORDER BY DateOfChange Desc

    INSERT  INTO #GraduatedStudentsAsOfAug312013
            SELECT  DISTINCT
                    S.StudentId
            FROM    dbo.syStudentStatusChanges e
            INNER JOIN dbo.syStatusCodes st ON e.NewStatusId = st.StatusCodeId
            INNER JOIN dbo.sySysStatus ss ON st.SysStatusId = ss.SysStatusId
            INNER JOIN arStuEnrollments SE ON SE.StuEnrollId = e.StuEnrollId
            INNER JOIN arStudent S ON S.StudentId = SE.StudentId
            WHERE   ss.InSchool = 0
                    AND DateOfChange < @StatusAsOf2013
                    AND e.DateOfChange = (
                                           SELECT   MAX(DateOfChange)
                                           FROM     dbo.syStudentStatusChanges
                                           WHERE    StuEnrollId = e.StuEnrollId
                                         )
                    AND ss.SysStatusId = 14; 

    INSERT  INTO #GraduatedStudentsAsOfAug312015
            SELECT  DISTINCT
                    S.StudentId
            FROM    dbo.syStudentStatusChanges e
            INNER JOIN dbo.syStatusCodes st ON e.NewStatusId = st.StatusCodeId
            INNER JOIN dbo.sySysStatus ss ON st.SysStatusId = ss.SysStatusId
            INNER JOIN arStuEnrollments SE ON SE.StuEnrollId = e.StuEnrollId
            INNER JOIN arStudent S ON S.StudentId = SE.StudentId
            WHERE   ss.InSchool = 0
                    AND DateOfChange < @StatusAsOf2015
                    AND e.DateOfChange = (
                                           SELECT   MAX(DateOfChange)
                                           FROM     dbo.syStudentStatusChanges
                                           WHERE    StuEnrollId = e.StuEnrollId
                                         )
                    AND ss.SysStatusId = 14;

    CREATE TABLE #EnrollmentDetail
        (
         RowNumber UNIQUEIDENTIFIER
        ,SSN VARCHAR(50)
        ,StudentNumber VARCHAR(50)
        ,StudentName VARCHAR(100)
        ,StudentId UNIQUEIDENTIFIER
        ,EnrollmentID VARCHAR(50)
        ,Exclusions2013 INT
        ,Exclusions2015 INT
        ,RecAdAward2013 INT
        ,RecAdAward2015 INT
        ,RevisedCohort2015 INT
        ,RevisedCohort2013 INT
        );

    CREATE TABLE #EnrollmentDetailOutput
        (
         RowNumber UNIQUEIDENTIFIER
        ,SSN VARCHAR(50)
        ,StudentNumber VARCHAR(50)
        ,StudentName VARCHAR(100)
        ,StudentId UNIQUEIDENTIFIER
        ,EnrollmentID VARCHAR(50)
        ,Exclusions2013 INT
        ,Exclusions2015 INT
        ,RecAdAward2013 INT
        ,RecAdAward2015 INT
        ,RevisedCohort2015 INT
        ,RevisedCohort2013 INT
        ,Exclusions2013_Total INT
        ,Exclusions2015_Total INT
        ,RecAdAward2013_Total INT
        ,RecAdAward2015_Total INT
        ,RevisedCohort2015_Total INT
        ,RevisedCohort2013_Total INT
        ,StillEnrolled_Total INT
        );
   

    INSERT  INTO #EnrollmentDetail
            SELECT  NEWID() AS RowNumber
                   ,dbo.UDF_FormatSSN(SSN) AS SSN
                   ,StudentNumber
                   ,StudentName
                   ,StudentId
                   ,EnrollmentId
                   ,( CASE WHEN (
                                  SELECT    COUNT(*)
                                  FROM      arStuEnrollments SQ1
                                           ,syStatusCodes SQ2
                                           ,dbo.sySysStatus SQ3
                                           ,arDropReasons SQ44
                                  WHERE     SQ1.StatusCodeId = SQ2.StatusCodeId
                                            AND SQ2.SysStatusId = SQ3.SysStatusId
                                            AND SQ1.DropReasonId = SQ44.DropReasonId
                                            AND SQ1.StuEnrollId = R1.StuEnrollId
                                            AND SQ3.SysStatusId IN ( 12 ) -- Dropped 
                                            AND SQ1.DateDetermined <= @StatusAsOf2013
                                            AND SQ44.IPEDSValue IN ( 15,16,17,18,19 )
                                ) >= 1 THEN 1
                           ELSE 0
                      END ) AS ExclusionsAsOf2013
                   ,( CASE WHEN (
                                  SELECT    COUNT(*)
                                  FROM      arStuEnrollments SQ1
                                           ,syStatusCodes SQ2
                                           ,dbo.sySysStatus SQ3
                                           ,arDropReasons SQ44
                                  WHERE     SQ1.StatusCodeId = SQ2.StatusCodeId
                                            AND SQ2.SysStatusId = SQ3.SysStatusId
                                            AND SQ1.DropReasonId = SQ44.DropReasonId
                                            AND SQ1.StuEnrollId = R1.StuEnrollId
                                            AND SQ3.SysStatusId IN ( 12 ) -- Dropped  
                                            --AND SQ1.DateDetermined <= @StatusAsOf2015
                                            AND (
                                                  SQ1.DateDetermined >= @ExclusionsAsOf2013
                                                  AND SQ1.DateDetermined <= @ExclusionsAsOf2015
                                                )
                                            AND SQ44.IPEDSValue IN ( 15,16,17,18,19 )
                                ) >= 1 THEN 1
                           ELSE 0
                      END ) AS ExclusionsAsOf2015
                   ,(
                      SELECT    COUNT(*)
                      FROM      #GraduatedStudentsAsOfAug312013 G2013
                      WHERE     G2013.StudentId = R1.StudentId
                    ) AS RecAdAward2013
                   ,(
                      SELECT    COUNT(*)
                      FROM      #GraduatedStudentsAsOfAug312015 G2015
                      WHERE     G2015.StudentId = R1.StudentId
                    ) AS RecAdAward2015
                   ,(
                      SELECT    COUNT(*)
                      FROM      arStuEnrollments
                      WHERE     StuEnrollId = R1.StuEnrollId
                                AND StuEnrollId NOT IN ( SELECT t1.StuEnrollId
                                                         FROM   arStuEnrollments t1
                                                               ,syStatusCodes t2
                                                         WHERE  t1.StatusCodeId = t2.StatusCodeId
                                                                AND StartDate <= @StatusAsOf2015
                                                                AND -- Student started before the end date range
                                                                LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                                                AND t1.StuEnrollId IN ( SELECT  StuEnrollId
                                                                                        FROM    #OutofSchoolStudentsAsOfAug312015 ) )
                    ) AS RevisedCohort2015
                   ,1 AS RevisedCohort2013
            FROM    (
                      -- Check if there are any Foreign Students who are Full-Time Undergraduates
			-- If there are no foreign students in that category, insert a blank record
			-- with race 'Nonresident Alien'
                      SELECT    t1.SSN
                               ,t1.StudentNumber
                               ,t1.LastName + ', ' + t1.FirstName + ' ' + ISNULL(t1.MiddleName,'') AS StudentName
                               ,t1.StudentId
                               ,t2.EnrollmentId
                               ,t2.StuEnrollId
                      FROM      arStudent t1
                      INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                      INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                      INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                                   AND t6.SysStatusId NOT IN ( 8 )
                      INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                      INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                      INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                      INNER JOIN arAttendTypes t10 ON t2.AttendTypeId = t10.AttendTypeId
                      INNER JOIN adDegCertSeeking t16 ON t2.DegCertSeekingId = t16.DegCertSeekingId
                      WHERE     LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                                AND (
                                      @ProgId IS NULL
                                      OR t8.ProgId IN ( SELECT  Val
                                                        FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                    )
                                AND ( t9.IPEDSValue = 58 ) -- Undergraduate
                                AND ( t10.IPEDSValue = @FullOrPartTime ) -- Full Time Or Part Time
                                AND t2.IsFirstTimeInSchool = @FirstTimeOrNot -- First Time or Non-First Time in School
								-- IF A Then B Else C is equivalent to ((Not A) OR B) AND (A OR C)
								--AND ((@CohortType<>'Full') OR (t2.StartDate>=@StartDate AND t2.StartDate<=@EndDate)) AND (@CohortType='Full' OR t2.StartDate <= @EndDate)
                                AND ( t2.StartDate <= @EndDate )
								-- Exclude students who are Dropped out/Transferred/Graduated/No Start 
								-- Who were Dropped or Graduated or LDA before report StartDate
                                AND StuEnrollId NOT IN ( SELECT t1.StuEnrollId
                                                         FROM   arStuEnrollments t1
                                                               ,syStatusCodes t2
                                                         WHERE  t1.StatusCodeId = t2.StatusCodeId
                                                                AND StartDate <= @EndDate
                                                                AND -- Student started before the end date range
                                                                LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                                                AND (
                                                                      @ProgId IS NULL
                                                                      OR t8.ProgId IN ( SELECT  Val
                                                                                        FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                                                    )
                                                                AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                                                AND (
                                                                      t1.DateDetermined < @StartDate
                                                                      OR ExpGradDate < @StartDate
                                                                      OR LDA < @StartDate
                                                                    )
                                                                AND t1.StuEnrollId NOT IN ( SELECT  StuEnrollId
                                                                                            FROM    @InSchoolEnrolmentOutput ) )
					-- DE8492
					-- If Student is enrolled in only program version and if that program version 
					-- happens to be a continuing ed program exclude the student
                                AND t2.StudentId NOT IN ( SELECT DISTINCT
                                                                    StudentId
                                                          FROM      (
                                                                      SELECT    StudentId
                                                                               ,COUNT(*) AS RowCounter
                                                                      FROM      arStuEnrollments
                                                                      WHERE     PrgVerId IN ( SELECT    PrgVerId
                                                                                              FROM      arPrgVersions
                                                                                              WHERE     IsContinuingEd = 1 )
                                                                      GROUP BY  StudentId
                                                                      HAVING    COUNT(*) = 1
                                                                    ) dtStudent_ContinuingEd )
                    ) R1;

    DECLARE @Studentidentifiervalue VARCHAR(50);
    SET @Studentidentifiervalue = (
                                    SELECT TOP 1
                                            Value
                                    FROM    dbo.syConfigAppSetValues
                                    WHERE   SettingId IN ( SELECT   SettingId
                                                           FROM     dbo.syConfigAppSettings
                                                           WHERE    KeyName = 'StudentIdentifier' )
                                  );
	
    IF LOWER(@Studentidentifiervalue) = 'ssn'
        OR LOWER(@Studentidentifiervalue) = 'studentnumber'
        BEGIN

            INSERT  INTO #EnrollmentDetailOutput
                    SELECT  NULL AS RowNumber
                           ,SSN
                           ,StudentNumber
                           ,StudentName
                           ,StudentId
                           ,NULL AS EnrollmetID
                           ,Exclusions2013
                           ,Exclusions2015
                           ,RecAdAward2013
                           ,RecAdAward2015
                           ,RevisedCohort2015
                           ,RevisedCohort2013
                           ,(
                              SELECT    COUNT(Exclusions2013)
                              FROM      #EnrollmentDetail SQ
                              WHERE     Exclusions2013 >= 1
                            ) AS Exclusions2013_Total
                           ,(
                              SELECT    COUNT(SQ.Exclusions2015)
                              FROM      #EnrollmentDetail SQ
                              WHERE     Exclusions2015 >= 1
                            ) AS Exclusions2015_Total
                           ,(
                              SELECT    COUNT(SQ.RecAdAward2013)
                              FROM      #EnrollmentDetail SQ
                              WHERE     RecAdAward2013 >= 1
                            ) AS RecAdAward2013_Total
                           ,(
                              SELECT    COUNT(SQ.RecAdAward2015)
                              FROM      #EnrollmentDetail SQ
                              WHERE     SQ.RecAdAward2015 >= 1
                            ) AS RecAdAward2015_Total
                           ,(
                              SELECT    COUNT(SQ.RevisedCohort2015)
                              FROM      #EnrollmentDetail SQ
                              WHERE     SQ.RevisedCohort2015 >= 1
                            ) AS RevisedCohort2015_Total
                           ,(
                              SELECT    COUNT(SQ.RevisedCohort2013)
                              FROM      #EnrollmentDetail SQ
                              WHERE     SQ.RevisedCohort2013 >= 1
                            ) AS RevisedCohort2013_Total
                           ,(
                              SELECT    COUNT(*)
                              FROM      #EnrollmentDetail SQ
                              WHERE     RecAdAward2015 = 0
                                        AND RevisedCohort2015 >= 1
                            ) AS StillEnrolled_Total
                    FROM    #EnrollmentDetail
                    ORDER BY CASE WHEN @OrderBy = 'SSN' THEN SSN
                             END
                           ,CASE WHEN @OrderBy = 'LastName' THEN StudentName
                            END;

            IF LOWER(@OrderBy) = 'ssn'
                BEGIN
                    SELECT DISTINCT
                            *
                    FROM    #EnrollmentDetailOutput
                    ORDER BY SSN; 
                END;
            ELSE
                BEGIN
                    SELECT DISTINCT
                            *
                    FROM    #EnrollmentDetailOutput
                    ORDER BY StudentName; 
                END;
        END;
    ELSE
        BEGIN
            SELECT  RowNumber
                   ,SSN
                   ,StudentNumber
                   ,StudentName
                   ,StudentId
                   ,EnrollmentID
                   ,Exclusions2013
                   ,Exclusions2015
                   ,RecAdAward2013
                   ,RecAdAward2015
                   ,RevisedCohort2015
                   ,RevisedCohort2013
                   ,(
                      SELECT    COUNT(Exclusions2013)
                      FROM      #EnrollmentDetail SQ
                      WHERE     Exclusions2013 >= 1
                    ) AS Exclusions2013_Total
                   ,(
                      SELECT    COUNT(SQ.Exclusions2015)
                      FROM      #EnrollmentDetail SQ
                      WHERE     Exclusions2015 >= 1
                    ) AS Exclusions2015_Total
                   ,(
                      SELECT    COUNT(SQ.RecAdAward2013)
                      FROM      #EnrollmentDetail SQ
                      WHERE     RecAdAward2013 >= 1
                    ) AS RecAdAward2013_Total
                   ,(
                      SELECT    COUNT(SQ.RecAdAward2015)
                      FROM      #EnrollmentDetail SQ
                      WHERE     SQ.RecAdAward2015 >= 1
                    ) AS RecAdAward2015_Total
                   ,(
                      SELECT    COUNT(SQ.RevisedCohort2015)
                      FROM      #EnrollmentDetail SQ
                      WHERE     SQ.RevisedCohort2015 >= 1
                    ) AS RevisedCohort2015_Total
                   ,(
                      SELECT    COUNT(SQ.RevisedCohort2013)
                      FROM      #EnrollmentDetail SQ
                      WHERE     SQ.RevisedCohort2013 >= 1
                    ) AS RevisedCohort2013_Total
                   ,(
                      SELECT    COUNT(*)
                      FROM      #EnrollmentDetail SQ
                      WHERE     RecAdAward2015 = 0
                                AND RevisedCohort2015 >= 1
                    ) AS StillEnrolled_Total
            FROM    #EnrollmentDetail
            ORDER BY CASE WHEN @OrderBy = 'SSN' THEN SSN
                     END
                   ,CASE WHEN @OrderBy = 'LastName' THEN StudentName
                    END;
		
        END;
GO
