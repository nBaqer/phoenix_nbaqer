SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_FERPAEntityInfo_Insert]
    (
     @FERPAEntityID UNIQUEIDENTIFIER
    ,@FERPAEntityCode VARCHAR(12)
    ,@StatusId UNIQUEIDENTIFIER
    ,@FERPAEntityDescrip VARCHAR(50)
    ,@CampGrpId UNIQUEIDENTIFIER
    ,@ModUser VARCHAR(50)
    ,@moddate DATETIME
    )
AS
    SET NOCOUNT ON;
    INSERT  INTO arFERPAEntity
    VALUES  ( @FERPAEntityID,@FERPAEntityCode,@StatusId,@FERPAEntityDescrip,@CampGrpId,@ModUser,@moddate );




GO
