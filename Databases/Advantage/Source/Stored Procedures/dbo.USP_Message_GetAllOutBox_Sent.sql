
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


-- =============================================

-- Author:		<Sweta>

-- Create date: <7/15/2016>

-- Description:	<Retrives a MessageInfo given a messageid>

-- =============================================

CREATE PROCEDURE [dbo].[USP_Message_GetAllOutBox_Sent]
    @RecipientType VARCHAR(15) = NULL
   ,@RecipientId UNIQUEIDENTIFIER = NULL
   ,@DeliveryType VARCHAR(10) = NULL
   ,@GroupId UNIQUEIDENTIFIER = NULL
   ,@TemplateId UNIQUEIDENTIFIER = NULL
   ,@filterShowOnlyErrors BIT
   ,@MessageType VARCHAR(10) = NULL
   ,@MessageID UNIQUEIDENTIFIER = NULL
AS
    BEGIN

        DECLARE @Email VARCHAR(100);

        DECLARE @leadId UNIQUEIDENTIFIER;

-- SET NOCOUNT ON added to prevent extra result sets from

	-- interfering with SELECT statements.

        SET NOCOUNT ON;



        SELECT  M.MessageId
               ,M.TemplateId
               ,M.FromId
               ,(
                  SELECT    Email
                  FROM      syUsers U
                  WHERE     U.UserId = M.FromId
                ) AS MailFrom
               ,ISNULL(MT.Descrip,'{Undefined}') AS TemplateDescrip
               ,(
                  SELECT    Descrip
                  FROM      msgGroups
                  WHERE     GroupId = MT.GroupId
                ) AS GroupDescrip
               ,M.DeliveryType
               ,M.RecipientId
               ,M.RecipientType AS EntityId
               ,(
                  SELECT    Resource
                  FROM      syResources R
                  WHERE     R.ResourceID = CONVERT(INT,M.RecipientType)
                ) AS RecipientType
               ,CASE M.RecipientType
                  WHEN '394' THEN (
                                    SELECT  FirstName + ' ' + LastName
                                    FROM    arStudent S
                                    WHERE   S.StudentId = M.RecipientId
                                  )
                  WHEN '395' THEN (
                                    SELECT  FirstName + ' ' + LastName
                                    FROM    adLeads
                                    WHERE   LeadId = M.RecipientId
                                  )
                  WHEN '434' THEN (
                                    SELECT  LenderDescrip
                                    FROM    faLenders
                                    WHERE   LenderId = M.RecipientId
                                  )
                  WHEN '396' THEN (
                                    SELECT  FirstName + ' ' + LastName
                                    FROM    hrEmployees E
                                    WHERE   E.EmpId = M.RecipientId
                                  )
                  WHEN '397' THEN (
                                    SELECT  EmployerDescrip
                                    FROM    plEmployers E
                                    WHERE   E.EmployerId = M.RecipientId
                                  )
                END AS RecipientName
               ,CASE M.RecipientType
                  WHEN '394' THEN (
                                    SELECT  HomeEmail
                                    FROM    arStudent S
                                    WHERE   S.StudentId = M.RecipientId
                                  )
                  WHEN '395' THEN (
                                    SELECT  CASE WHEN (
                                                        ALE.IsPreferred = 1
                                                        AND ISNULL(ALE.EMail,'') <> ''
                                                      ) THEN ALE.EMail
                                                 ELSE ISNULL(ALE2.EMail,'')
                                            END AS EMail
                                    FROM    adLeads AS AL
                                    LEFT JOIN AdLeadEmail AS ALE ON ALE.LeadId = AL.LeadId
                                                                    AND ALE.IsPreferred = 1
                                    LEFT JOIN AdLeadEmail AS ALE2 ON ALE2.LeadId = AL.LeadId
                                                                     AND ALE.IsPreferred = 0
                                    WHERE   AL.LeadId IN ( SELECT   RecipientId
                                                           FROM     dbo.msgMessages )
                                            AND AL.LeadId IN ( SELECT   MM.RecipientId
                                                               FROM     msgMessages AS MM )
                                            AND AL.LeadId = M.RecipientId
                                  )
                  WHEN '434' THEN (
                                    SELECT  Email
                                    FROM    faLenders
                                    WHERE   LenderId = M.RecipientId
                                  )
                  WHEN '396' THEN (
                                    SELECT TOP 1
                                            WorkEmail
                                    FROM    hrEmpContactInfo E
                                    WHERE   E.EmpId = M.RecipientId
                                  )
                  WHEN '397' THEN (
                                    SELECT  Email
                                    FROM    plEmployers E
                                    WHERE   E.EmployerId = M.RecipientId
                                  )
                END AS MailTo
               ,M.ReId
               ,M.ReType
               ,CASE M.ReType
                  WHEN '394' THEN (
                                    SELECT  FirstName + ' ' + LastName
                                    FROM    arStudent S
                                    WHERE   S.StudentId = M.ReId
                                  )
                  WHEN '395' THEN (
                                    SELECT  FirstName + ' ' + LastName
                                    FROM    adLeads
                                    WHERE   LeadId = M.ReId
                                  )
                  WHEN '434' THEN (
                                    SELECT  LenderDescrip
                                    FROM    faLenders
                                    WHERE   LenderId = M.ReId
                                  )
                  WHEN '396' THEN (
                                    SELECT  FirstName + ' ' + LastName
                                    FROM    hrEmployees E
                                    WHERE   E.EmpId = M.ReId
                                  )
                  WHEN '397' THEN (
                                    SELECT  EmployerDescrip
                                    FROM    plEmployers E
                                    WHERE   E.EmployerId = M.ReId
                                  )
                END AS ReName
               ,M.CreatedDate
               ,M.DeliveryDate
               ,M.LastDeliveryAttempt
               ,M.LastDeliveryMsg
               ,M.DateDelivered
               ,M.ModDate
               ,M.ModUser
        FROM    msgMessages M
        INNER JOIN msgTemplates MT ON M.TemplateId = MT.TemplateId
        WHERE   (
                  (
                    @MessageType = 'outBox'
                    AND (
                          (
                            M.DateDelivered IS NULL
                            AND @MessageID IS  NULL
                          )
                          OR (
                               M.DateDelivered IS NOT NULL
                               AND M.MessageId = @MessageID
                             )
                        )
                  )
                  OR (
                       @MessageType = 'sent'
                       AND M.DateDelivered IS NOT NULL
                     )
                )



       -- GetAll_OutBox and not null for GetAll_Sent
                AND (
                      @RecipientType IS NULL
                      OR M.RecipientType = @RecipientType
                    )
                AND (
                      @RecipientId IS NULL
                      OR M.RecipientId = @RecipientId
                    )
                AND (
                      @DeliveryType IS NULL
                      OR M.DeliveryType = @DeliveryType
                    )
                AND (
                      @GroupId IS NULL
                      OR MT.GroupId = @GroupId
                    )
                AND (
                      @TemplateId IS NULL
                      OR M.TemplateId = @TemplateId
                    )
                AND (
                      ( @filterShowOnlyErrors = 0
                      OR (
                           @filterShowOnlyErrors = 1
                           AND LastDeliveryMsg IS NOT NULL
                         )
                    )
                    )
        ORDER BY M.CreatedDate DESC;

    END;
GO
