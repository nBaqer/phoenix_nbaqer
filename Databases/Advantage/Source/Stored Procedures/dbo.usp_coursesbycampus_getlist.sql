SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_coursesbycampus_getlist]
    @campusid UNIQUEIDENTIFIER
AS
    SELECT  t5.ReqId
           ,t5.Code
           ,t5.Descrip
    FROM    (
              SELECT DISTINCT
                        t0.ReqId
                       ,t0.CourseID
                       ,t0.Code
                       ,t0.Descrip
                       ,t0.StatusId
                       ,t0.ReqTypeId
                       ,t0.Credits
                       ,t0.Hours
                       ,t0.Cnt
                       ,t0.GrdLvlId
                       ,t0.UnitTypeId
                       ,t0.CampGrpId
                       ,t0.CourseCatalog
                       ,t0.CourseComments
                       ,t0.ModDate
                       ,t0.ModUser
                       ,t0.SU
                       ,t0.PF
                       ,t0.CourseCategoryId
                       ,t0.TrackTardies
                       ,t0.TardiesMakingAbsence
                       ,t0.DeptId
                       ,t0.IsComCourse
                       ,t0.IsOnLine
                       ,t0.FinAidCredits
                       ,t0.CompletedDate
                       ,t0.IsExternship
                       ,t0.UseTimeClock
                       ,t0.IsAttendanceOnly
                       ,t0.AllowCompletedCourseRetake
                       ,t0.MinDate
              FROM      arReqs AS t0
              INNER JOIN syCampGrps AS t1 ON t0.CampGrpId = t1.CampGrpId
              INNER JOIN syCmpGrpCmps AS t2 ON t1.CampGrpId = t2.CampGrpId
              INNER JOIN syCampuses AS t3 ON t2.CampusId = t3.CampusId
              INNER JOIN syStatuses AS t4 ON t0.StatusId = t4.StatusId
              WHERE     t3.CampusId = @campusid
            ) AS t5
    ORDER BY t5.Descrip;



GO
