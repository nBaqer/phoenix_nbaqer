SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[Usp_Advantage_CommonTaskNavigation_GetList]
    @SchoolEnumerator INT
AS
    BEGIN
	-- declare local variables
        DECLARE @ShowRossOnlyTabs BIT
           ,@SchedulingMethod VARCHAR(50)
           ,@TrackSAPAttendance VARCHAR(50);
        DECLARE @ShowCollegeOfCourtReporting VARCHAR(5)
           ,@FameESP VARCHAR(5)
           ,@EdExpress VARCHAR(5); 
        DECLARE @GradeBookWeightingLevel VARCHAR(20)
           ,@ShowExternshipTabs VARCHAR(5);
	
	-- Get Values
        SET @ShowRossOnlyTabs = (
                                  SELECT    VALUE
                                  FROM      dbo.syConfigAppSetValues
                                  WHERE     SettingId = 68
                                );
        SET @SchedulingMethod = (
                                  SELECT    VALUE
                                  FROM      dbo.syConfigAppSetValues
                                  WHERE     SettingId = 65
                                );
        SET @TrackSAPAttendance = (
                                    SELECT  VALUE
                                    FROM    dbo.syConfigAppSetValues
                                    WHERE   SettingId = 72
                                  );
        SET @ShowCollegeOfCourtReporting = (
                                             SELECT VALUE
                                             FROM   dbo.syConfigAppSetValues
                                             WHERE  SettingId = 118
                                           );
        SET @FameESP = (
                         SELECT VALUE
                         FROM   dbo.syConfigAppSetValues
                         WHERE  SettingId = 37
                       );
        SET @EdExpress = (
                           SELECT   VALUE
                           FROM     dbo.syConfigAppSetValues
                           WHERE    SettingId = 91
                         );
        SET @GradeBookWeightingLevel = (
                                         SELECT VALUE
                                         FROM   dbo.syConfigAppSetValues
                                         WHERE  SettingId = 43
                                       );
        SET @ShowExternshipTabs = (
                                    SELECT  VALUE
                                    FROM    dbo.syConfigAppSetValues
                                    WHERE   SettingId = 71
                                  );

	-- The first column always refers to the Module Resource Id (189 or 26 or .....)
	/******************** Admissions *****************************/
        SELECT  *
        FROM    (
                  SELECT    189 AS ModuleResourceId
                           ,NNChild.ResourceId AS ChildResourceId
                           ,CASE WHEN NNChild.ResourceId = 395 THEN 'Lead Tabs'
                                 ELSE CASE WHEN NNChild.ResourceId = 398 THEN 'Add/View Leads'
                                           ELSE RChild.Resource
                                      END
                            END AS ChildResource
                           ,RChild.ResourceURL AS ChildResourceURL
                           ,CASE WHEN NNParent.ResourceId = 189 THEN NULL
                                 ELSE NNParent.ResourceId
                            END AS ParentResourceId
                           ,RParent.Resource AS ParentResource
                           ,CASE WHEN NNChild.ResourceId = 398 THEN 1
                                 ELSE CASE WHEN NNChild.ResourceId = 395 THEN 2
                                           ELSE 3
                                      END
                            END AS FirstSortOrder
	--NNChild.HierarchyIndex AS SecondSortOrder,
	--RParentModule.Resource AS Module
                  FROM      syResources RChild
                  INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
                  INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                  INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                  LEFT OUTER JOIN (
                                    SELECT  *
                                    FROM    syResources
                                    WHERE   ResourceTypeId = 1
                                  ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                  WHERE     RChild.ResourceTypeId IN ( 2,3,8 )
                            AND (
                                  RChild.ChildTypeId IS NULL
                                  OR RChild.ChildTypeId = 3
                                )
                            AND NNParent.ResourceId IN ( 189,398,395 )
                  UNION ALL
	
	/********************Academic Records *****************************/
                  SELECT    26 AS ModuleResourceId
                           ,ChildResourceId
                           ,ChildResource
                           ,ChildResourceURL
                           ,ParentResourceId
                           ,ParentResource
                           ,(
                              SELECT TOP 1
                                        HierarchyIndex
                              FROM      dbo.syNavigationNodes
                              WHERE     ResourceId = ChildResourceId
                            ) AS FirstSortOrder
                  FROM      (
                              SELECT DISTINCT
                                        NNChild.ResourceId AS ChildResourceId
                                       ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                             ELSE RChild.Resource
                                        END AS ChildResource
                                       ,RChild.ResourceURL AS ChildResourceURL
                                       ,CASE WHEN NNParent.ResourceId = 26 THEN NULL
                                             ELSE NNParent.ResourceId
                                        END AS ParentResourceId
                                       ,RParent.Resource AS ParentResource
                                       ,RParentModule.Resource AS MODULE
				--NNParent.ParentId
                              FROM      syResources RChild
                              INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
                              INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                              INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                              LEFT OUTER JOIN (
                                                SELECT  *
                                                FROM    syResources
                                                WHERE   ResourceTypeId = 1
                                              ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                              WHERE     RChild.ResourceTypeId IN ( 2,3,8 )
                                        AND (
                                              RChild.ChildTypeId IS NULL
                                              OR RChild.ChildTypeId = 3
                                            )
                                        AND (
                                              NNParent.ResourceId IN ( 26,190,129,71,160,132 )
                                              OR 
					-- A Student Tab may be part of AR,Financial Aid, Student Accounts or Placement
					-- So the following condition is neccessary to bring in only items tied to AR
                                              (
                                                NNParent.ResourceId IN ( 394 )
                                                AND NNParent.Parentid IN ( SELECT   HierarchyId
                                                                           FROM     syNavigationNodes
                                                                           WHERE    ResourceId = 26 )
                                              )
                                            )
                                        AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
                            ) t1
                  UNION ALL
	/********************** Student Accounts ************************************/
                  SELECT    194 AS ModuleResourceId
                           ,ChildResourceId
                           ,ChildResource
                           ,ChildResourceURL
                           ,ParentResourceId
                           ,ParentResource
                           ,(
                              SELECT TOP 1
                                        HierarchyIndex
                              FROM      dbo.syNavigationNodes
                              WHERE     ResourceId = ChildResourceId
                            ) AS FirstSortOrder
                  FROM      (
                              SELECT DISTINCT
                                        NNChild.ResourceId AS ChildResourceId
                                       ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                             ELSE RChild.Resource
                                        END AS ChildResource
                                       ,RChild.ResourceURL AS ChildResourceURL
                                       ,CASE WHEN NNParent.ResourceId = 194 THEN NULL
                                             ELSE NNParent.ResourceId
                                        END AS ParentResourceId
                                       ,RParent.Resource AS ParentResource
                                       ,RParentModule.Resource AS MODULE
				--NNParent.ParentId
                              FROM      syResources RChild
                              INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
                              INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                              INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                              LEFT OUTER JOIN (
                                                SELECT  *
                                                FROM    syResources
                                                WHERE   ResourceTypeId = 1
                                              ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                              WHERE     RChild.ResourceTypeId IN ( 2,3,8 )
                                        AND (
                                              RChild.ChildTypeId IS NULL
                                              OR RChild.ChildTypeId = 3
                                            )
                                        AND (
                                              NNParent.ResourceId IN ( 194,403 )
                                              OR 
					-- A Student Tab may be part of AR,Financial Aid, Student Accounts or Placement
					-- So the following condition is neccessary to bring in only items tied to AR
                                              (
                                                NNParent.ResourceId IN ( 394 )
                                                AND NNParent.Parentid IN ( SELECT   HierarchyId
                                                                           FROM     syNavigationNodes
                                                                           WHERE    ResourceId = 194 )
                                              )
                                            )
				-- The following condition uses Bitwise Operator
                                        AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
                            ) t2
                  UNION ALL
	/********************** FACULTY ************************************/
                  SELECT    300 AS ModuleResourceId
                           ,ChildResourceId
                           ,ChildResource
                           ,ChildResourceURL
                           ,ParentResourceId
                           ,ParentResource
                           ,(
                              SELECT TOP 1
                                        HierarchyIndex
                              FROM      dbo.syNavigationNodes
                              WHERE     ResourceId = ChildResourceId
                            ) AS FirstSortOrder
                  FROM      (
                              SELECT DISTINCT
                                        NNChild.ResourceId AS ChildResourceId
                                       ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                             ELSE RChild.Resource
                                        END AS ChildResource
                                       ,RChild.ResourceURL AS ChildResourceURL
                                       ,CASE WHEN NNParent.ResourceId = 300 THEN NULL
                                             ELSE NNParent.ResourceId
                                        END AS ParentResourceId
                                       ,RParent.Resource AS ParentResource
                                       ,RParentModule.Resource AS MODULE
				--NNParent.ParentId
                              FROM      syResources RChild
                              INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
                              INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                              INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                              LEFT OUTER JOIN (
                                                SELECT  *
                                                FROM    syResources
                                                WHERE   ResourceTypeId = 1
                                              ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                              WHERE     RChild.ResourceTypeId IN ( 2,3,8 )
                                        AND (
                                              RChild.ChildTypeId IS NULL
                                              OR RChild.ChildTypeId = 3
                                            )
                                        AND (
                                              NNParent.ResourceId IN ( 300 )
                                              OR 
					-- A Student Tab may be part of AR,Financial Aid, Student Accounts or Placement
					-- So the following condition is neccessary to bring in only items tied to Faculty
                                              (
                                                NNParent.ResourceId IN ( 394 )
                                                AND NNParent.Parentid IN ( SELECT   HierarchyId
                                                                           FROM     syNavigationNodes
                                                                           WHERE    ResourceId = 300 )
                                              )
                                            )
				-- The following condition uses Bitwise Operator
                                        AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
                            ) t3
                  UNION ALL
	/***************************** Financial Aid ******************************/
                  SELECT    191 AS ModuleResourceId
                           ,ChildResourceId
                           ,ChildResource
                           ,ChildResourceURL
                           ,ParentResourceId
                           ,ParentResource
                           ,(
                              SELECT TOP 1
                                        HierarchyIndex
                              FROM      dbo.syNavigationNodes
                              WHERE     ResourceId = ChildResourceId
                            ) AS FirstSortOrder
                  FROM      (
                              SELECT DISTINCT
                                        NNChild.ResourceId AS ChildResourceId
                                       ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                             ELSE RChild.Resource
                                        END AS ChildResource
                                       ,RChild.ResourceURL AS ChildResourceURL
                                       ,CASE WHEN NNParent.ResourceId = 191 THEN NULL
                                             ELSE NNParent.ResourceId
                                        END AS ParentResourceId
                                       ,RParent.Resource AS ParentResource
                                       ,RParentModule.Resource AS MODULE
				--NNParent.ParentId
                              FROM      syResources RChild
                              INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
                              INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                              INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                              LEFT OUTER JOIN (
                                                SELECT  *
                                                FROM    syResources
                                                WHERE   ResourceTypeId = 1
                                              ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                              WHERE     RChild.ResourceTypeId IN ( 2,3,8 )
                                        AND (
                                              RChild.ChildTypeId IS NULL
                                              OR RChild.ChildTypeId = 3
                                            )
                                        AND (
                                              -- Get resources directly tied to the module
                                              NNParent.ResourceId IN ( 191 )
                                              OR 
					-- A Student Tab may be part of AR,Financial Aid, Student Accounts or Placement
					-- So the following condition is neccessary to bring in only items tied to Faculty
                                              (
                                                NNParent.ResourceId IN ( 394 )
                                                AND NNParent.Parentid IN ( SELECT   HierarchyId
                                                                           FROM     syNavigationNodes
                                                                           WHERE    ResourceId = 191 )
                                              )
                                            )
				-- The following condition uses Bitwise Operator
                                        AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
                            ) t4
                  UNION ALL
	/***************************** Placement ******************************/
                  SELECT    193 AS ModuleResourceId
                           ,ChildResourceId
                           ,ChildResource
                           ,ChildResourceURL
                           ,ParentResourceId
                           ,ParentResource
                           ,(
                              SELECT TOP 1
                                        HierarchyIndex
                              FROM      dbo.syNavigationNodes
                              WHERE     ResourceId = ChildResourceId
                            ) AS FirstSortOrder
                  FROM      (
                              SELECT DISTINCT
                                        NNChild.ResourceId AS ChildResourceId
                                       ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                             ELSE CASE WHEN NNChild.ResourceId = 397 THEN 'Employer Tabs'
                                                       ELSE RChild.Resource
                                                  END
                                        END AS ChildResource
                                       ,RChild.ResourceURL AS ChildResourceURL
                                       ,CASE WHEN NNParent.ResourceId = 193 THEN NULL
                                             ELSE NNParent.ResourceId
                                        END AS ParentResourceId
                                       ,RParent.Resource AS ParentResource
                                       ,RParentModule.Resource AS MODULE
				--NNParent.ParentId
                              FROM      syResources RChild
                              INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
                              INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                              INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                              LEFT OUTER JOIN (
                                                SELECT  *
                                                FROM    syResources
                                                WHERE   ResourceTypeId = 1
                                              ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                              WHERE     RChild.ResourceTypeId IN ( 2,3,8 )
                                        AND (
                                              RChild.ChildTypeId IS NULL
                                              OR RChild.ChildTypeId = 3
                                            )
                                        AND (
                                              -- Get resources directly tied to the module
                                              NNParent.ResourceId IN ( 193,404 )
                                              OR 
					-- A Student Tab may be part of AR,Financial Aid, Student Accounts or Placement
					-- So the following condition is neccessary to bring in only items tied to Faculty
					-- 394 (Student Tabs) and 397 (Employers Tabs)
                                              (
                                                NNParent.ResourceId IN ( 394,397 )
                                                AND NNParent.Parentid IN ( SELECT   HierarchyId
                                                                           FROM     syNavigationNodes
                                                                           WHERE    ResourceId = 193 )
                                              )
                                            )
				-- The following condition uses Bitwise Operator
                                        AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
                            ) t5
                  UNION ALL
	/***************************** System ******************************/
                  SELECT    195 AS ModuleResourceId
                           ,ChildResourceId
                           ,ChildResource
                           ,ChildResourceURL
                           ,ParentResourceId
                           ,ParentResource
                           ,(
                              SELECT TOP 1
                                        HierarchyIndex
                              FROM      dbo.syNavigationNodes
                              WHERE     ResourceId = ChildResourceId
                            ) AS FirstSortOrder
                  FROM      (
                              SELECT DISTINCT
                                        NNChild.ResourceId AS ChildResourceId
                                       ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                             ELSE RChild.Resource
                                        END AS ChildResource
                                       ,RChild.ResourceURL AS ChildResourceURL
                                       ,CASE WHEN NNParent.ResourceId = 195 THEN NULL
                                             ELSE NNParent.ResourceId
                                        END AS ParentResourceId
                                       ,RParent.Resource AS ParentResource
                                       ,RParentModule.Resource AS MODULE
				--NNParent.ParentId
                              FROM      syResources RChild
                              INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
                              INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                              INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                              LEFT OUTER JOIN (
                                                SELECT  *
                                                FROM    syResources
                                                WHERE   ResourceTypeId = 1
                                              ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                              WHERE     RChild.ResourceTypeId IN ( 2,3,8 )
                                        AND (
                                              RChild.ChildTypeId IS NULL
                                              OR RChild.ChildTypeId = 3
                                            )
                                        AND (
                                              -- Get resources(both menus and submenus) directly tied to the module
                                              NNParent.ResourceId IN ( 195,243,407 )
                                              OR 
					-- A Student Tab may be part of AR,Financial Aid, Student Accounts or Placement
					-- So the following condition is neccessary to bring in only items tied to Faculty
                                              (
                                                NNParent.ResourceId IN ( 394 )
                                                AND NNParent.Parentid IN ( SELECT   HierarchyId
                                                                           FROM     syNavigationNodes
                                                                           WHERE    ResourceId = 195 )
                                              )
                                            )
				-- The following condition uses Bitwise Operator
                                        AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
                            ) t6
                  UNION ALL
	/***************************** Human Resources ******************************/
                  SELECT    192 AS ModuleResourceId
                           ,ChildResourceId
                           ,ChildResource
                           ,ChildResourceURL
                           ,ParentResourceId
                           ,ParentResource
                           ,(
                              SELECT TOP 1
                                        HierarchyIndex
                              FROM      dbo.syNavigationNodes
                              WHERE     ResourceId = ChildResourceId
                            ) AS FirstSortOrder
                  FROM      (
                              SELECT DISTINCT
                                        NNChild.ResourceId AS ChildResourceId
                                       ,CASE WHEN NNChild.ResourceId = 394 THEN 'Student Tabs'
                                             ELSE RChild.Resource
                                        END AS ChildResource
                                       ,RChild.ResourceURL AS ChildResourceURL
                                       ,CASE WHEN NNParent.ResourceId = 192 THEN NULL
                                             ELSE NNParent.ResourceId
                                        END AS ParentResourceId
                                       ,RParent.Resource AS ParentResource
                                       ,RParentModule.Resource AS MODULE
				--NNParent.ParentId
                              FROM      syResources RChild
                              INNER JOIN syNavigationNodes NNChild ON RChild.ResourceId = NNChild.ResourceId
                              INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId
                              INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID
                              LEFT OUTER JOIN (
                                                SELECT  *
                                                FROM    syResources
                                                WHERE   ResourceTypeId = 1
                                              ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID
                              WHERE     RChild.ResourceTypeId IN ( 2,3,8 )
                                        AND (
                                              RChild.ChildTypeId IS NULL
                                              OR RChild.ChildTypeId = 3
                                            )
                                        AND (
                                              -- Get resources(both menus and submenus) directly tied to the module
                                              NNParent.ResourceId IN ( 192 )
                                              OR 
					-- A Student Tab may be part of AR,Financial Aid, Student Accounts or Placement
					-- So the following condition is neccessary to bring in only items tied to Faculty
                                              (
                                                NNParent.ResourceId IN ( 394,396 )
                                                AND NNParent.Parentid IN ( SELECT   HierarchyId
                                                                           FROM     syNavigationNodes
                                                                           WHERE    ResourceId = 192 )
                                              )
                                            )
				-- The following condition uses Bitwise Operator
                                        AND ( RChild.UsedIn & @SchoolEnumerator > 0 )
                            ) t7
                ) MainQuery
        WHERE   -- Hide resources based on Configuration Settings
                (
                  -- The following expression means if showross... is set to false, hide 
					-- ResourceIds 541,542,532,534,535,538,543,539
					-- (Not False) OR (Condition)
				  (
                    ( @ShowRossOnlyTabs <> 0 )
                    OR ( ChildResourceId NOT IN ( 541,542,532,534,535,538,543,539 ) )
                  )
                  AND
					-- The following expression means if showross... is set to true, hide 
					-- ResourceIds 142,375,330,476,508,102,107,237
					-- (Not True) OR (Condition)
                  (
                    ( @ShowRossOnlyTabs <> 1 )
                    OR ( ChildResourceId NOT IN ( 142,375,330,476,508,102,107,237 ) )
                  )
                  AND
					---- The following expression means if SchedulingMethod=regulartraditional, hide 
					---- ResourceIds 91 and 497
					---- (Not True) OR (Condition)
                  (
                    (
                      NOT LOWER(LTRIM(RTRIM(@SchedulingMethod))) = 'regulartraditional'
                    )
                    OR ( ChildResourceId NOT IN ( 91,497 ) )
                  )
                  AND
					-- The following expression means if TrackSAPAttendance=byday, hide 
					-- ResourceIds 633
					-- (Not True) OR (Condition)
                  (
                    (
                      NOT LOWER(LTRIM(RTRIM(@TrackSAPAttendance))) = 'byday'
                    )
                    OR ( ChildResourceId NOT IN ( 633 ) )
                  )
                  AND
					---- The following expression means if @ShowCollegeOfCourtReporting is set to false, hide 
					---- ResourceIds 614,615
					---- (Not False) OR (Condition)
                  (
                    (
                      NOT LOWER(LTRIM(RTRIM(@ShowCollegeOfCourtReporting))) = 'no'
                    )
                    OR ( ChildResourceId NOT IN ( 614,615 ) )
                  )
                  AND
					-- The following expression means if @ShowCollegeOfCourtReporting is set to true, hide 
					-- ResourceIds 497
					-- (Not True) OR (Condition)
                  (
                    (
                      NOT LOWER(LTRIM(RTRIM(@ShowCollegeOfCourtReporting))) = 'yes'
                    )
                    OR ( ChildResourceId NOT IN ( 497 ) )
                  )
                  AND
					-- The following expression means if FAMEESP is set to false, hide 
					-- ResourceIds 517,523, 525
					-- (Not False) OR (Condition)
                  (
                    (
                      NOT LOWER(LTRIM(RTRIM(@FameESP))) = 'no'
                    )
                    OR ( ChildResourceId NOT IN ( 517,523,525 ) )
                  )
                  AND
					---- The following expression means if EDExpress is set to false, hide 
					---- ResourceIds 603,604,606,619
					---- (Not False) OR (Condition)
                  (
                    (
                      NOT LOWER(LTRIM(RTRIM(@EdExpress))) = 'no'
                    )
                    OR ( ChildResourceId NOT IN ( 603,604,605,606,619 ) )
                  )
                  AND
					---- The following expression means if @GradeBookWeightingLevel is set to courselevel, hide 
					---- ResourceIds 107,96,222
					---- (Not False) OR (Condition)
                  (
                    (
                      NOT LOWER(LTRIM(RTRIM(@GradeBookWeightingLevel))) = 'courselevel'
                    )
                    OR ( ChildResourceId NOT IN ( 107,96,222 ) )
                  )
                  AND (
                        (
                          NOT LOWER(LTRIM(RTRIM(@GradeBookWeightingLevel))) = 'instructorlevel'
                        )
                        OR ( ChildResourceId NOT IN ( 476 ) )
                      )
                  AND (
                        (
                          NOT LOWER(LTRIM(RTRIM(@ShowExternshipTabs))) = 'no'
                        )
                        OR ( ChildResourceId NOT IN ( 543,538 ) )
                      )
                )
        ORDER BY ModuleResourceId
               ,FirstSortOrder
               ,ParentResourceId;
    END; 		



GO
