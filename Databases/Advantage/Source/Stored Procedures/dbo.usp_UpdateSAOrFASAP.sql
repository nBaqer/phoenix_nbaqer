SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_UpdateSAOrFASAP]
    (
     @FASAPid UNIQUEIDENTIFIER
    ,@PrgVerId UNIQUEIDENTIFIER
    ,@ModUser VARCHAR(50)
    )
AS
    SET NOCOUNT ON;
	
    BEGIN
     
        UPDATE  arPrgVersions
        SET     FASAPId = @FASAPid
               ,ModUser = @ModUser
               ,ModDate = GETDATE()
        WHERE   PrgVerId = @PrgVerId;
        RETURN 1;

    END;



GO
