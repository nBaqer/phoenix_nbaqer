SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Ginzo, John
-- Create date: 05/12/2014
-- Description:	GET back roles for a user
-- =============================================
CREATE PROCEDURE [dbo].[GetRolesByUser]
    @UserId UNIQUEIDENTIFIER
AS
    BEGIN
	
        SET NOCOUNT ON;

        SELECT  u.UserId
               ,u.UserName
               ,ur.RoleId
               ,r.Role
        FROM    syUsers u
        INNER JOIN syUsersRolesCampGrps ur ON u.UserId = ur.UserId
        INNER JOIN syRoles r ON ur.RoleId = r.RoleId
        WHERE   u.UserId = @UserId;

    END;




GO
