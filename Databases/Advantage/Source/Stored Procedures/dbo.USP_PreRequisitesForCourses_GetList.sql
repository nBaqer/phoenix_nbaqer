SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_PreRequisitesForCourses_GetList]
    @EffectiveDate DATETIME
   ,@ReqId UNIQUEIDENTIFIER
AS
    SELECT  *
    FROM    arPrerequisites_GradeComponentTypes_Courses
    WHERE   EffectiveDate = @EffectiveDate
            AND ReqId = @ReqId;




GO
