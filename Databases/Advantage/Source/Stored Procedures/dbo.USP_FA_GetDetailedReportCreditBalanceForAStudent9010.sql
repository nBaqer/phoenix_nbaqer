SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[USP_FA_GetDetailedReportCreditBalanceForAStudent9010]
    @EndDate DATETIME
   ,@StudentId AS VARCHAR(50)
   ,@StuEnrollId AS VARCHAR(50)
   ,@StudentIdentifier AS VARCHAR(50)
AS /*----------------------------------------------------------------------------------------------------
	Author : Vijay Ramteke
	
	Create date : 07/22/2010
	
	Procedure Name : USP_FA_GetDetailedReportCreditBalanceForAStudent9010

	Objective : Get Revenue Ratio 90/10 ratio report for A Student
	
	Parameters : Name Type Data Type Required? 
	
	Output : Returns the Revenue Ratio (90/10 ratio) report dataset for A Student
						
*/-----------------------------------------------------------------------------------------------------

    BEGIN

        SELECT  *
        INTO    #TempTrans
        FROM    satransactions
        WHERE   TransDate < @EndDate
                AND saTransactions.StuEnrollId IN ( SELECT  StuEnrollId
                                                    FROM    arStuEnrollments
                                                    WHERE   arStuEnrollments.StudentId = @StudentId
                                                            AND (
                                                                  @StuEnrollId IS NULL
                                                                  OR arStuEnrollments.StuEnrollId = @StuEnrollId
                                                                ) )
                AND TransTypeId IN ( 0,1,2 )
                AND Voided = 0;

        CREATE NONCLUSTERED INDEX INDX_Trans_TransId_DR ON #TempTrans (TransactionId);
        CREATE NONCLUSTERED INDEX INDX_Trans_StuEnrollment_DR ON #TempTrans (StuEnrollId);
        CREATE NONCLUSTERED INDEX INDX_Trans_TransAmount_DR ON #TempTrans (TransAmount);

        DECLARE @Balance DECIMAL(19,4);
        DECLARE @Paymnets DECIMAL(19,4);
        DECLARE @CashAgencyFund DECIMAL(19,4);
        DECLARE @Charges DECIMAL(19,4);
        DECLARE @Numerator DECIMAL(19,4);
        DECLARE @Denominator DECIMAL(19,4);
        DECLARE @BalAgencyFund DECIMAL(19,4);

        SET @Charges = ISNULL((
                                SELECT  SUM(TransAmount)
                                FROM    #TempTrans
                                WHERE   TransTypeId = 0
                              ),0);
        SET @Balance = ISNULL((
                                SELECT  SUM(TransAmount)
                                FROM    #TempTrans
                              ),0);
   
        SET @Paymnets = ISNULL((
                                 SELECT SUM(Amount)
                                 FROM   (
                                          --Title IV --
                                          SELECT    S.LastName
                                                   ,S.FirstName
                                                   ,S.MiddleName
                                                   ,CASE @StudentIdentifier
                                                      WHEN 'StudentId' THEN S.StudentNumber
                                                      WHEN 'EnrollmentId' THEN SE.EnrollmentId
                                                      ELSE S.SSN
                                                    END AS StudentIdentifier
                                                   ,SE.CampusId
                                                   ,C.CampDescrip
                                                   ,( -T1.TransAmount ) AS Numerator
                                                   ,( -T1.TransAmount ) AS Denominator
                                                   ,T1.TransDate
                                                   ,T1.TransDescrip
                                                   ,(
                                                      SELECT    TransCodeDescrip
                                                      FROM      saTransCodes TCI
                                                      WHERE     T1.TransCodeId = TCI.TransCodeId
                                                    ) AS TransCode
                                                   ,(
                                                      SELECT    Description
                                                      FROM      saTransTypes TT
                                                      WHERE     T1.TransTypeId = TT.TransTypeId
                                                    ) AS TransType
                                                   ,-1 * ( -1 * T1.TransAmount ) AS Amount
                                                   ,(
                                                      SELECT    LTRIM(RTRIM(PrgVerDescrip)) + ' (' + LTRIM(RTRIM(PrgVerCode)) + ')'
                                                      FROM      arPrgVersions PV
                                                      WHERE     PV.PrgVerId = SE.PrgVerId
                                                    ) AS PrgVerDescrip
                                                   ,T1.TransactionId
                                          FROM      #TempTrans T1
                                                   ,saFundSources FS
                                                   ,arStuEnrollments SE
                                                   ,arStudent S
                                                   ,syCampuses C
                                          WHERE     T1.voided = 0
                                                    AND T1.FundSourceId = FS.FundSourceId
                                                    AND FS.TitleIV = 1
                                                    AND T1.StuEnrollId = SE.StuEnrollId
                                                    AND SE.StudentId = S.StudentId
                                                    AND C.CampusId = SE.CampusId
                                                    AND T1.Voided = 0
                                                    AND T1.TransTypeId IN ( 1,2 )
                                          UNION
                                          SELECT    S.LastName
                                                   ,S.FirstName
                                                   ,S.MiddleName
                                                   ,CASE @StudentIdentifier
                                                      WHEN 'StudentId' THEN S.StudentNumber
                                                      WHEN 'EnrollmentId' THEN SE.EnrollmentId
                                                      ELSE S.SSN
                                                    END AS StudentIdentifier
                                                   ,SE.CampusId
                                                   ,C.CampDescrip
                                                   ,COALESCE(-1 * T1.TransAmount,0.00) AS Numerator
                                                   ,COALESCE(-1 * T1.TransAmount,0.00) AS Denominator
                                                   ,T1.TransDate
                                                   ,T1.TransDescrip
                                                   ,(
                                                      SELECT    TransCodeDescrip
                                                      FROM      saTransCodes TCI
                                                      WHERE     T1.TransCodeId = TCI.TransCodeId
                                                    ) AS TransCode
                                                   ,(
                                                      SELECT    Description
                                                      FROM      saTransTypes TT
                                                      WHERE     T1.TransTypeId = TT.TransTypeId
                                                    ) AS TransType
                                                   ,-1 * COALESCE(-1 * T1.TransAmount,0.00) AS Amount
                                                   ,(
                                                      SELECT    LTRIM(RTRIM(PrgVerDescrip)) + ' (' + LTRIM(RTRIM(PrgVerCode)) + ')'
                                                      FROM      arPrgVersions PV
                                                      WHERE     PV.PrgVerId = SE.PrgVerId
                                                    ) AS PrgVerDescrip
                                                   ,T1.TransactionId
                                          FROM      #TempTrans T1
                                                   ,saRefunds R
                                                   ,saFundSources FS
                                                   ,arStuEnrollments SE
                                                   ,arStudent S
                                                   ,syCampuses C
                                          WHERE     T1.voided = 0
                                                    AND T1.FundSourceId = FS.FundSourceId
                                                    AND T1.TransactionId = R.TransactionId
                                                    AND R.RefundTypeId = 1
                                                    AND FS.TitleIV = 1
                                                    AND T1.StuEnrollId = SE.StuEnrollId
                                                    AND SE.StudentId = S.StudentId
                                                    AND C.CampusId = SE.CampusId
                                                    AND T1.Voided = 0
                                                    AND T1.TransTypeId IN ( 1,2 )
		
		---Title IV---
                                        ) ttTIV
                               ),0);
		
        SET @CashAgencyFund = ISNULL((
                                       SELECT   SUM(Amount)
                                       FROM     (
                                                  SELECT    S.LastName
                                                           ,S.FirstName
                                                           ,S.MiddleName
                                                           ,CASE @StudentIdentifier
                                                              WHEN 'StudentId' THEN S.StudentNumber
                                                              WHEN 'EnrollmentId' THEN SE.EnrollmentId
                                                              ELSE S.SSN
                                                            END AS StudentIdentifier
                                                           ,SE.CampusId
                                                           ,C.CampDescrip
                                                           ,0.00 AS Numerator
                                                           ,-1 * T1.TransAmount AS Denominator
                                                           ,T1.TransDate
                                                           ,T1.TransDescrip
                                                           ,(
                                                              SELECT    TransCodeDescrip
                                                              FROM      saTransCodes TCI
                                                              WHERE     T1.TransCodeId = TCI.TransCodeId
                                                            ) AS TransCode
                                                           ,(
                                                              SELECT    Description
                                                              FROM      saTransTypes TT
                                                              WHERE     T1.TransTypeId = TT.TransTypeId
                                                            ) AS TransType
                                                           ,T1.TransAmount AS Amount
                                                           ,(
                                                              SELECT    LTRIM(RTRIM(PrgVerDescrip)) + ' (' + LTRIM(RTRIM(PrgVerCode)) + ')'
                                                              FROM      arPrgVersions PV
                                                              WHERE     PV.PrgVerId = SE.PrgVerId
                                                            ) AS PrgVerDescrip
                                                           ,T1.TransactionId
                                                  FROM      #TempTrans T1
                                                           ,saRefunds R
                                                           ,arStuEnrollments SE
                                                           ,arStudent S
                                                           ,syCampuses C
                                                  WHERE     T1.voided = 0
                                                            AND T1.TransactionId = R.TransactionId
                                                            AND T1.StuEnrollId = SE.StuEnrollId
                                                            AND R.RefundTypeId = 0
                                                            AND SE.StudentId = S.StudentId
                                                            AND C.CampusId = SE.CampusId
                                                            AND T1.TransTypeId IN ( 1,2 )
                                                  UNION
                                                  SELECT    S.LastName
                                                           ,S.FirstName
                                                           ,S.MiddleName
                                                           ,CASE @StudentIdentifier
                                                              WHEN 'StudentId' THEN S.StudentNumber
                                                              WHEN 'EnrollmentId' THEN SE.EnrollmentId
                                                              ELSE S.SSN
                                                            END AS StudentIdentifier
                                                           , 
	  --  syCampGrps.CampGrpId,syCampGrps.CampGrpDescrip,
                                                            SE.CampusId
                                                           ,C.CampDescrip
                                                           ,( -T1.TransAmount ) AS Numerator
                                                           ,( -T1.TransAmount ) AS Denominator
                                                           ,T1.TransDate
                                                           ,T1.TransDescrip
                                                           ,(
                                                              SELECT    TransCodeDescrip
                                                              FROM      saTransCodes TCI
                                                              WHERE     T1.TransCodeId = TCI.TransCodeId
                                                            ) AS TransCode
                                                           ,(
                                                              SELECT    Description
                                                              FROM      saTransTypes TT
                                                              WHERE     T1.TransTypeId = TT.TransTypeId
                                                            ) AS TransType
                                                           ,-1 * ( -1 * T1.TransAmount ) AS Amount
                                                           ,(
                                                              SELECT    LTRIM(RTRIM(PrgVerDescrip)) + ' (' + LTRIM(RTRIM(PrgVerCode)) + ')'
                                                              FROM      arPrgVersions PV
                                                              WHERE     PV.PrgVerId = SE.PrgVerId
                                                            ) AS PrgVerDescrip
                                                           ,T1.TransactionId
                                                  FROM      #TempTrans T1
                                                           ,saFundSources FS
                                                           ,arStuEnrollments SE
                                                           ,arStudent S
                                                           ,syCampuses C
                                                  WHERE     T1.voided = 0
                                                            AND T1.FundSourceId = FS.FundSourceId
                                                            AND T1.StuEnrollId = SE.StuEnrollId
                                                            AND SE.StudentId = S.StudentId
                                                            AND C.CampusId = SE.CampusId
                                                            AND T1.Voided = 0
                                                            AND FS.AdvFundSourceId IN ( 1,23 )
                                                            AND T1.TransTypeId IN ( 1,2 )
                                                ) tt
                                     ),0);


        IF ( @Balance ) >= 0
            BEGIN
                SET @Numerator = 0;
                SET @Denominator = 0;
                SET @Balance = 0;
            END;
        ELSE
            BEGIN
                IF ( @Charges + @Paymnets ) > 0
                    BEGIN
                        SET @Numerator = 0;
                        SET @Denominator = 0;
                        IF ( @Charges + @Paymnets > 0 )
                            BEGIN
                                SET @BalAgencyFund = @Balance;
                            END;
                        ELSE
                            BEGIN
                                SET @BalAgencyFund = @Charges + @Paymnets - @Balance;
                            END;
                        IF (
                             -1 * @BalAgencyFund > 0
                             AND -1 * @BalAgencyFund < -1 * @CashAgencyFund
                           )
                            BEGIN
                                SET @Denominator = @Denominator + @BalAgencyFund;
                            END;
                        ELSE
                            BEGIN
                                SET @Denominator = @Denominator + @CashAgencyFund;
                            END;
                    END;
                ELSE
                    BEGIN
                        IF ( -1 * ( @Charges + @Paymnets ) < -1 * @Balance )
                            BEGIN
                                SET @Numerator = @Charges + @Paymnets;
                                SET @Denominator = @Charges + @Paymnets;
                                IF ( @Charges + @Paymnets > 0 )
                                    BEGIN
                                        SET @BalAgencyFund = @Balance;
                                    END;
                                ELSE
                                    BEGIN
                                        SET @BalAgencyFund = @Charges + @Paymnets - @Balance;
                                    END;
                                IF (
                                     -1 * @BalAgencyFund > 0
                                     AND -1 * @BalAgencyFund < -1 * @CashAgencyFund
                                   )
                                    BEGIN
                                        SET @Denominator = @Denominator + @BalAgencyFund;
                                    END;
                                ELSE
                                    BEGIN
                                        SET @Denominator = @Denominator + @CashAgencyFund;
                                    END;
                            END;
                        ELSE
                            BEGIN
                                SET @Numerator = @Balance;
                                SET @Denominator = @Balance;
                            END;
                    END;
            END;

        SELECT  @Balance AS Balance
               ,@Charges AS Charges
               ,@Paymnets AS Payments
               ,@CashAgencyFund AS CashAgencyFund
               ,@Numerator AS Numerator
               ,@Denominator AS Denominator;

        DROP TABLE #TempTrans;


    END;






GO
