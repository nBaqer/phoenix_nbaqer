SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

  
CREATE PROCEDURE [dbo].[usp_GetAttendancePercentageDT_TR]
    (
     @stuEnrollId UNIQUEIDENTIFIER
    ,@cutOffDate DATETIME    
    )
AS
    SET NOCOUNT ON;    
    
    
    SET DATEFIRST 7;     
    SET DATEFIRST 7;     
    SELECT DISTINCT
            CSA.ClsSectionId
           ,TI1.TimeIntervalDescrip AS StartTime
           ,(
              SELECT    TimeIntervalDescrip
              FROM      cmTimeInterval
              WHERE     TimeIntervalId = CSM.EndIntervalId
            ) AS EndTime
           ,CSA.MeetDate
           ,CSA.Actual
           ,CSA.Tardy
           ,CSA.Excused
           ,(
              SELECT    t3.UnitTypeDescrip
              FROM      arClassSections t1
                       ,arReqs t2
                       ,arAttUnitType t3
              WHERE     t1.ClsSectionId = CSA.ClsSectionId
                        AND t1.ReqId = t2.ReqId
                        AND t2.UnitTypeId = t3.UnitTypeId
            ) AS AttendanceType
           ,CSA.ClsSectMeetingId
           ,CSA.Scheduled
           ,CSM.InstructionTypeID
    FROM    atClsSectAttendance CSA
           ,arClsSectMeetings CSM
           ,syPeriods P
           ,syPeriodsWorkDays PWD
           ,plWorkdays WD
           ,cmTimeInterval TI1
           ,cmTimeInterval TI2
    WHERE   CSM.PeriodId = P.PeriodId
            AND P.PeriodId = PWD.PeriodId
            AND PWD.WorkDayId = WD.WorkDaysId
            AND P.StartTimeId = TI1.TimeIntervalId
            AND P.EndTimeId = TI2.TimeIntervalId
            AND CSA.ClsSectionId = CSM.ClsSectionId
            AND CSA.ClsSectMeetingId = CSM.ClsSectMeetingId
            AND CSA.Actual <> 999
            AND CSA.Actual <> 9999
            AND CSA.Actual <> 99
            AND StuEnrollId = @stuEnrollId
            AND CSA.MeetDate <= @cutOffDate
            AND (
                  SELECT    DATEPART(hh,TimeIntervalDescrip)
                  FROM      syPeriods
                           ,cmTimeInterval
                  WHERE     syPeriods.startTimeId = cmTimeInterval.TimeIntervalId
                            AND Periodid = CSM.Periodid
                ) = DATEPART(hh,MeetDate)
            AND (
                  SELECT    DATEPART(Mi,TimeIntervalDescrip)
                  FROM      syPeriods
                           ,cmTimeInterval
                  WHERE     syPeriods.startTimeId = cmTimeInterval.TimeIntervalId
                            AND Periodid = CSM.Periodid
                ) = DATEPART(mi,meetdate)
    ORDER BY CSA.ClsSectionId
           ,CSA.MeetDate;    



GO
