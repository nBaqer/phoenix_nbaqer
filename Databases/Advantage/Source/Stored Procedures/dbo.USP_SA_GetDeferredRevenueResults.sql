

SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO

--=================================================================================================
-- USP_SA_GetDeferredRevenueResults
--=================================================================================================
/**
Author: Troy
Date: 6/8/2012
Comments: The original code was written by Anatoly when we were not using sprocs. I took the code he had in a class and moved all the logic here.
		  The UI page first allows the user to specify the report date they want to run. When they click build list, if there are no exceptions
		  then a summary by transcodes is displayed. If there are exceptions, they are displayed in a grid and the user cannot post until they have
		  cleared all the exceptions. If there are no exceptions, the user can click the Post Deferred Revenue button to do the actual posting.
		  
		  The original code returned a DataSet with several tables. However, the two tables are used in the UI are the TransCodes and the 
		  StudentExceptions. Since we can only return one result set here, the GetDeferredRevenueResults functionn in TuitionEarningsDB.vb
		  specifies the DataTable name as DeferredRevenueResultsTable. That table could be the contents of the TransCodes or the StudentExceptions
		  table. These two temp tables have a field called TableName that is set to TransCodes and StudentExceptions respectively. The UI code
		  can therefore check the TableName field of the first record to determine if the result set is for the TransCodes or the 
		  StudentExceptions.
		  
		  The mode parameter allows us to specify what function(s) we want the SP to perform. 
		  Mode=1: When the user clicks the Build List button we just want to get back a summary for each transcode. 
		  Mode=2: When the user clicks on a transcode after building the list, we want to show the details for that transcode.
		  Mode=3: When the user confirm that they want to post the deferred revenue the actual posting should be done.

**/
----EXECUTE USP_SA_GetDeferredRevenueResults '2/29/2012','436BF45E-5420-4C1F-8687-CD32A60D64BE','ssn',1,'sa'
CREATE PROCEDURE dbo.USP_SA_GetDeferredRevenueResults
    @ReportDate DATE = NULL
   ,@CampusId VARCHAR(50) = NULL
   ,@StudentIdentifierType VARCHAR(50) = NULL
   ,@Mode INTEGER = 1
   , --BuildList=1, BindTransCodeDetails=2 and PostDeferredRevenue=3
    @User VARCHAR(50) = NULL
   ,@CutoffTransDate DATE = NULL
AS
    BEGIN
--Testing Purposes----------------------------------------------------------------------------------------------------------
--DECLARE @ReportDate DATETIME
--DECLARE @CampusId VARCHAR(50)
--DECLARE @CutoffTransDate DATETIME
--DECLARE @StudentIdentifierType VARCHAR(50)
--DECLARE @PostDeferredRevenue BIT
--DECLARE @User VARCHAR(50)
--DECLARE @Mode INTEGER
-----------------------------------------------------------------------------------------------------------------------------

        DECLARE @TransactionId UNIQUEIDENTIFIER;
        DECLARE @StudentName VARCHAR(200);
        DECLARE @StudentIdentifier VARCHAR(50);
        DECLARE @StuEnrollId UNIQUEIDENTIFIER;
        DECLARE @TransCodeId UNIQUEIDENTIFIER;
        DECLARE @TransAmount DECIMAL(18,2);
        DECLARE @LOAStartDate DATETIME;
        DECLARE @LOAEndDate DATETIME;
        DECLARE @Earned DECIMAL(18,2);
        DECLARE @FeeLevelId INT;
        DECLARE @TermStart DATETIME;
        DECLARE @TermEnd DATETIME;
        DECLARE @percentToEarnIdx VARCHAR(50);
        DECLARE @RemainingMethod INT;
        DECLARE @DeferredRevenue DECIMAL(18,2);


----------------Testing Purposes----------------------------------------------------------------------------------------------
--SET @ReportDate='3/31/2012'
--SET @CampusId='436BF45E-5420-4C1F-8687-CD32A60D64BE'
--SET @StudentIdentifier='ssn'
--SET @PostDeferredRevenue=1
--SET @User = 'sa'
--SET @Mode=2
-------------------------------------------------------------------------------------------------------------------------------

-----------------------------------------------------------------------------------------------------------------------------
--This section creates the supporting temp tables
-----------------------------------------------------------------------------------------------------------------------------
--Transactions Table
        CREATE TABLE #Transactions
            (
             TransactionId UNIQUEIDENTIFIER NOT NULL
                                            PRIMARY KEY
            ,StudentName VARCHAR(200) NOT NULL
            ,StudentIdentifier VARCHAR(50) NULL
            ,StuEnrollId UNIQUEIDENTIFIER NOT NULL
            ,TransCodeId UNIQUEIDENTIFIER NULL
            ,TransAmount DECIMAL(18,2) NOT NULL
            ,LOAStartDate DATETIME NULL
            ,LOAEndDate DATETIME NULL
            ,Earned DECIMAL(18,2) NULL
            ,FeeLevelId INT NULL
            ,TermStart DATETIME NULL
            ,TermEnd DATETIME NULL
            ,percentToEarnIdx INT NULL
            ,RemainingMethod INT NULL
            ,DeferredRevenue DECIMAL(18,2) NULL
            ); 


--TransCodes Table
        CREATE TABLE #TransCodes
            (
             TransCodeId UNIQUEIDENTIFIER NOT NULL
                                          PRIMARY KEY
            ,TransCodeDescrip VARCHAR(100) NOT NULL
            ,TotalDeferredRevenue DECIMAL(18,2) NULL
            ,AccruedAmount DECIMAL(18,2) NULL
            ,DeferredRevenueAmount DECIMAL(18,2) NULL
            ,TotalAmount DECIMAL(18,2) NULL
            ,TableName VARCHAR(50) NOT NULL
            );


--Enrollments Table
        CREATE TABLE #StuEnrollments
            (
             StuEnrollId UNIQUEIDENTIFIER NOT NULL
                                          PRIMARY KEY
            ,StudentId UNIQUEIDENTIFIER NOT NULL
            ,PrgVerId UNIQUEIDENTIFIER NOT NULL
            ,StartDate DATETIME NOT NULL
            ,ExpGradDate DATETIME NULL
            ,DropReasonId UNIQUEIDENTIFIER NULL
            ,DateDetermined DATETIME NULL
            ,SysSTatusID INT NULL
            ,LDA DATETIME NULL
            );


--Program Versions Table
        CREATE TABLE #PrgVersions
            (
             PrgVerId UNIQUEIDENTIFIER NOT NULL
                                       PRIMARY KEY
            ,TuitionEarningId UNIQUEIDENTIFIER NOT NULL
            ,Weeks SMALLINT NOT NULL
            ,Hours DECIMAL(9,2) NOT NULL
            ,Credits DECIMAL(9,2) NOT NULL
            );


--Tuition Earnings Table
        CREATE TABLE #TuitionEarnings
            (
             TuitionEarningId UNIQUEIDENTIFIER NOT NULL
                                               PRIMARY KEY
            ,FullMonthBeforeDay TINYINT NULL
            ,HalfMonthBeforeDay TINYINT NULL
            ,RevenueBasisIdx TINYINT NOT NULL
            ,PercentageRangeBasisIdx TINYINT NOT NULL
            ,PercentToEarnIdx TINYINT NOT NULL
            ,TakeHolidays BIT NULL
            ,RemainingMethod BIT NULL
            );


        CREATE TABLE #TuitionEarningsPercentageRanges
            (
             TuitionEarningsPercentageRangeId UNIQUEIDENTIFIER NOT NULL
                                                               PRIMARY KEY
            ,TuitionEarningId UNIQUEIDENTIFIER NOT NULL
            ,UpTo DECIMAL(5,2) NOT NULL
            ,EarnPercent DECIMAL(5,2) NOT NULL
            );


        CREATE TABLE #Results
            (
             ResultId UNIQUEIDENTIFIER NOT NULL
                                       PRIMARY KEY
            ,ClsSectionId UNIQUEIDENTIFIER NOT NULL
            ,StuEnrollId UNIQUEIDENTIFIER NOT NULL
            ,Credits DECIMAL(9,2) NULL
            ,IsPass BIT NULL
            ,IsCreditsEarned BIT NULL
            ,IsCreditsAttempted BIT NULL
            );


        CREATE TABLE #Reqs
            (
             PrgVerId UNIQUEIDENTIFIER NOT NULL
            ,TuitionEarningId UNIQUEIDENTIFIER NOT NULL
            ,Descrip VARCHAR(150) NOT NULL
            ,IsRequired BIT NOT NULL
            );


--Exceptions Table
        CREATE TABLE #StudentExceptions
            (
             StudentName VARCHAR(200) NOT NULL
            ,StuEnrollId VARCHAR(50) NOT NULL
            ,FeeLevelId INT NULL
            ,TermStart DATETIME NULL
            ,TermEnd DATETIME NULL
            ,TransCode VARCHAR(100) NULL
            ,TransAmount VARCHAR(50) NULL
            ,Exception VARCHAR(250) NULL
            ,TableName VARCHAR(50) NOT NULL
            ); 

--Table to be used for inserting records into saDeferredRevenues
        CREATE TABLE #DeferredRevenues
            (
             DefRevenueId UNIQUEIDENTIFIER NOT NULL
                                           PRIMARY KEY
            ,DefRevenueDate DATETIME NOT NULL
            ,TransactionId UNIQUEIDENTIFIER NOT NULL
            ,Type BIT NOT NULL
            ,Source TINYINT NOT NULL
            ,Amount MONEY NOT NULL
            ,IsPosted BIT NOT NULL
            ,ModUser VARCHAR(50) NOT NULL
            ,ModDate DATETIME NOT NULL
            );


--Table to be used for displaying the details of a transcode selected on the left hand side



----------------------------------------------------------------------------------------------------------------------------------------------
--Populate the Temp Tables
-----------------------------------------------------------------------------------------------------------------------------------------------
--Transactions Table
        INSERT  INTO #Transactions
                SELECT  T.TransactionId
                       ,(
                          SELECT    FirstName + ' ' + LastName
                          FROM      arStudent S
                          WHERE     StudentId = (
                                                  SELECT    StudentId
                                                  FROM      arStuEnrollments
                                                  WHERE     StuEnrollId = (
                                                                            SELECT  StuEnrollId
                                                                            FROM    saTransactions
                                                                            WHERE   TransactionId = T.TransactionId
                                                                                    AND saTransactions.Voided = 0
                                                                          )
                                                )
                        ) AS StudentName
                       ,( CASE WHEN UPPER(@StudentIdentifier) = 'SSN'
                               THEN (
                                      SELECT    COALESCE(CASE LEN(S.SSN)
                                                           WHEN 9 THEN '***-**-' + SUBSTRING(SSN,6,4)
                                                           ELSE ' '
                                                         END,' ')
                                      FROM      arStudent S
                                      WHERE     StudentId = (
                                                              SELECT    StudentId
                                                              FROM      arStuEnrollments
                                                              WHERE     StuEnrollId = (
                                                                                        SELECT  StuEnrollId
                                                                                        FROM    saTransactions
                                                                                        WHERE   TransactionId = T.TransactionId
                                                                                                AND saTransactions.Voided = 0
                                                                                      )
                                                            )
                                    )
                               WHEN LOWER(@StudentIdentifier) = 'studentid'
                               THEN (
                                      SELECT    COALESCE(StudentNumber,' ')
                                      FROM      arStudent S
                                      WHERE     StudentId = (
                                                              SELECT    StudentId
                                                              FROM      arStuEnrollments
                                                              WHERE     StuEnrollId = (
                                                                                        SELECT  StuEnrollId
                                                                                        FROM    saTransactions
                                                                                        WHERE   TransactionId = T.TransactionId
                                                                                                AND saTransactions.Voided = 0
                                                                                      )
                                                            )
                                    )
                               ELSE (
                                      SELECT    ''
                                      FROM      arStudent S
                                      WHERE     StudentId = (
                                                              SELECT    StudentId
                                                              FROM      arStuEnrollments
                                                              WHERE     StuEnrollId = (
                                                                                        SELECT  StuEnrollId
                                                                                        FROM    saTransactions
                                                                                        WHERE   TransactionId = T.TransactionId
                                                                                                AND saTransactions.Voided = 0
                                                                                      )
                                                            )
                                    )
                          END ) AS StudentIdentifier
                       ,T.StuEnrollId
                       ,T.TransCodeId
                       ,T.TransAmount
                       ,(
                          SELECT TOP 1
                                    StartDate
                          FROM      arStudentLOAs
                          WHERE     StuEnrollId = T.StuEnrollId
                          ORDER BY  StartDate DESC
                        ) AS LOAStartDate
                       ,(
                          SELECT TOP 1
                                    EndDate
                          FROM      arStudentLOAs
                          WHERE     StuEnrollId = T.StuEnrollId
                          ORDER BY  EndDate DESC
                        ) AS LOAEndDate
                       ,COALESCE((
                                   SELECT   SUM(Amount)
                                   FROM     saDeferredRevenues
                                   WHERE    TransactionId = T.TransactionId
                                 ),0) Earned
                       ,T.FeeLevelId
                       ,(
                          SELECT    StartDate
                          FROM      arTerm tm
                          WHERE     tm.TermId = T.TermId
                        ) AS TermStart
                       ,(
                          SELECT    EndDate
                          FROM      arTerm tm
                          WHERE     tm.TermId = T.TermId
                        ) AS TermEnd
                       ,te.PercentToEarnIdx AS percentToEarnIdx
                       ,te.RemainingMethod AS RemainingMethod
                       ,0.00 AS DeferredRevenue
                FROM    saTransactions T
                       ,saTransCodes TC
                       ,arStuEnrollments SE
                       ,saTransTypes S
                       ,dbo.arPrgVersions pv
                       ,dbo.saTuitionEarnings te
                WHERE   ( T.TransCodeId = TC.TransCodeId )
                        AND TC.DefEarnings = 1
                        AND SE.StuEnrollId = T.StuEnrollId
                        AND T.Voided = 0
                        AND T.TransTypeId = S.TransTypeId
                        AND T.TransTypeId <> 2
                        AND SE.StartDate <= @ReportDate
                        AND T.TransDate <= @ReportDate
                        AND T.CampusId = ISNULL(@CampusId,T.CampusId)
                        AND T.TransDate >= ISNULL(@CutoffTransDate,T.TransDate)
                        AND SE.PrgVerId = pv.PrgVerId
                        AND pv.TuitionEarningId = te.TuitionEarningId;
        --                AND SE.StuEnrollId = '40375EF8-2CE3-464E-923C-2E2F8E38AD86'; --Scott Graham
		--AND	   T.TransactionId='C2CB1BDB-A6E3-4DC6-92D5-7935250CCD74' --Dawn Higgins Tuition Charged on 2012-03-05 00:00:00.000
		--AND		SE.StuEnrollId='B527B234-4961-46CA-A108-E5D6960F7E6D' --Ammanda Medina (Expelled on 8/2/2011 LDA 7/19/2011)
		--AND		T.TransactionId='8AAD7B80-C082-41E8-8F3D-9CF058929D67' --Ammanda Medina Tuition charged on 5/2/2011
		--And T.StuEnrollId='67887763-86CC-4829-90BA-B01C056191A0' --Aaron Defauw Tuition charged on 8/29/2011 $12,154.00
		--And T.TransactionId='4805ED14-748D-4660-B60B-D16228BFF180'  --Aaron Defauw Tuition charged on 8/29/2011 $12,154.00
		--And T.StuEnrollId='04F8E9F3-BA4C-4A3F-9C35-2365FD8BC5C7' --Aaron Rodgers Tuition charged on 3/5/2012 $12074.0000
		--And T.TransactionId='BCDA9AB4-C465-4B99-BDCC-9DE2F3ABFF7B'	--Aaron Rodgers Tuition charged on 3/5/2012 $12074.0000
		--AND T.StuEnrollId='B7FF0506-98E2-463E-97B9-1019AEC2F2A6'	--Aaron Renter Tuition charged on 1/3/2012 $12,830.00
		--AND T.TransactionId='30E7EE3D-ACC2-47F4-BEEC-002EC4D09B20'	--Aaron Renter Tuition charged on 1/3/2012 $12,830.00
		--AND T.StuEnrollId='CFE46667-C6EC-4EBB-AFB3-EA354881371F'	--Bobbie Vance Tuition charged on 1/3/2012 $13,400.00
		--AND T.TransactionId='78C35984-9EF3-4D3C-B5BC-001A862AA2C6'	--Bobbie Vance Tuition charged on 1/3/2012 $13,400.00
		--AND T.StuEnrollId='2923660B-5C3E-4962-B30B-920F05472D19'	--Anastasia Anderson Started 3/5/2012 Tuition Charged on 3/5/2012 $13,400
		--AND T.TransactionId='04AD729A-F8C3-4821-A6F0-6EC6A7DD5ACA'	--Anastasia Anderson Started 3/5/2012 Tuition Charged on 3/5/2012 $13,400

--TransCodes Table
        INSERT  INTO #TransCodes
                (
                 TransCodeId
                ,TransCodeDescrip
                ,TableName
                ,TotalDeferredRevenue
                ,AccruedAmount
                ,DeferredRevenueAmount
                ,TotalAmount
                )
                SELECT  TC.TransCodeId
                       ,TC.TransCodeDescrip
                       ,'TransCodes' AS TableName
                       ,0.00 AS TotalDeferredRevenue
                       ,0.00 AS AccruedAmount
                       ,0.00 AS DeferredRevenueAmount
                       ,SUM(T.TransAmount) TotalAmount
                FROM    saTransCodes TC
                       ,saTransactions T
                       ,saTransTypes S
                WHERE   ( T.TransCodeId = TC.TransCodeId )
                        AND ( TC.DefEarnings = 1 )
                        AND T.CampusId = ISNULL(@CampusId,T.CampusId)
                        AND T.Voided = 0
                        AND T.TransTypeId = S.TransTypeId
                        AND T.TransTypeId <> 2
                GROUP BY TC.TransCodeId
                       ,TC.TransCodeDescrip;
	
	--SELECT ((12074*90)/100)*100/736
--Enrollments Table
        INSERT  INTO #StuEnrollments
                SELECT DISTINCT
                        SE.StuEnrollId
                       ,SE.StudentId
                       ,SE.PrgVerId
                       ,SE.StartDate
                       ,SE.ExpGradDate
                       ,SE.DropReasonId
                       ,SE.DateDetermined
                       ,(
                          SELECT    SysStatusId
                          FROM      syStatusCodes
                          WHERE     StatusCodeId IN ( SELECT    StatusCodeId
                                                      FROM      arStuEnrollments
                                                      WHERE     StuEnrollId = SE.StuEnrollId )
                        ) SysSTatusID
                       ,(
                          SELECT    MAX(LDA)
                          FROM      (
                                      SELECT    MAX(AttendedDate) AS LDA
                                      FROM      arExternshipAttendance
                                      WHERE     StuEnrollId = SE.StuEnrollId
                                      UNION ALL
                                      SELECT    MAX(MeetDate) AS LDA
                                      FROM      atClsSectAttendance
                                      WHERE     StuEnrollId = SE.StuEnrollId
                                                AND Actual >= 1
                                      UNION ALL
                                      SELECT    MAX(AttendanceDate) AS LDA
                                      FROM      atAttendance
                                      WHERE     EnrollId = SE.StuEnrollId
                                                AND Actual >= 1
                                      UNION ALL
                                      SELECT    MAX(RecordDate) AS LDA
                                      FROM      arStudentClockAttendance
                                      WHERE     StuEnrollId = SE.StuEnrollId
                                                AND (
                                                      ActualHours >= 1.00
                                                      AND ActualHours <> 99.00
                                                      AND ActualHours <> 999.00
                                                      AND ActualHours <> 9999.00
                                                    )
                                      UNION ALL
                                      SELECT    MAX(MeetDate) AS LDA
                                      FROM      atConversionAttendance
                                      WHERE     StuEnrollId = SE.StuEnrollId
                                                AND (
                                                      Actual >= 1.00
                                                      AND Actual <> 99.00
                                                      AND Actual <> 999.00
                                                      AND Actual <> 9999.00
                                                    )
                                      UNION ALL
                                      SELECT    LDA
                                      FROM      arStuEnrollments
                                      WHERE     StuEnrollId = SE.StuEnrollId
                                    ) T1
                        ) AS LDA
                FROM    arStuEnrollments SE
                       ,saTransactions T
                       ,saTransCodes TC
                WHERE   ( SE.StuEnrollId = T.StuEnrollId )
                        AND ( T.TransCodeId = TC.TransCodeId )
                        AND TC.DefEarnings = 1
                        AND T.Voided = 0
                        AND T.CampusId = ISNULL(@CampusId,T.CampusId); 



--Program Versions Table
        INSERT  INTO #PrgVersions
                SELECT DISTINCT
                        PV.PrgVerId
                       ,PV.TuitionEarningId
                       ,PV.Weeks
                       ,PV.Hours
                       ,PV.Credits
                FROM    arPrgVersions PV
                       ,arStuEnrollments SE
                       ,saTransactions T
                       ,saTransCodes TC
                WHERE   ( PV.PrgVerId = SE.PrgVerId )
                        AND ( SE.StuEnrollId = T.StuEnrollId )
                        AND ( T.TransCodeId = TC.TransCodeId )
                        AND ( TC.DefEarnings = 1 )
                        AND T.Voided = 0
                        AND T.CampusId = ISNULL(@CampusId,T.CampusId); 


--Tuition Earnings Table
        INSERT  INTO #TuitionEarnings
                SELECT DISTINCT
                        TE.TuitionEarningId
                       ,TE.FullMonthBeforeDay
                       ,TE.HalfMonthBeforeDay
                       ,TE.RevenueBasisIdx
                       ,TE.PercentageRangeBasisIdx
                       ,TE.PercentToEarnIdx
                       ,TE.TakeHolidays
                       ,TE.RemainingMethod
                FROM    saTuitionEarnings TE
                       ,arPrgVersions PV
                       ,arStuEnrollments SE
                       ,saTransactions T
                       ,saTransCodes TC
                WHERE   ( TE.TuitionEarningId = PV.TuitionEarningId )
                        AND ( PV.PrgVerId = SE.PrgVerId )
                        AND ( SE.StuEnrollId = T.StuEnrollId )
                        AND ( T.TransCodeId = TC.TransCodeId )
                        AND ( TC.DefEarnings = 1 )
                        AND T.Voided = 0
                        AND T.CampusId = ISNULL(@CampusId,T.CampusId);


--TuitionEarningsPercentageRanges Table
        INSERT  INTO #TuitionEarningsPercentageRanges
                SELECT DISTINCT
                        TEPR.TuitionEarningsPercentageRangeId
                       ,TE.TuitionEarningId
                       ,TEPR.UpTo
                       ,TEPR.EarnPercent
                FROM    saTuitionEarningsPercentageRanges TEPR
                       ,saTuitionEarnings TE
                       ,arPrgVersions PV
                       ,arStuEnrollments SE
                       ,saTransactions T
                       ,saTransCodes TC
                WHERE   ( TEPR.TuitionEarningId = TE.TuitionEarningId )
                        AND ( TE.TuitionEarningId = PV.TuitionEarningId )
                        AND ( PV.PrgVerId = SE.PrgVerId )
                        AND ( SE.StuEnrollId = T.StuEnrollId )
                        AND ( T.TransCodeId = TC.TransCodeId )
                        AND ( TC.DefEarnings = 1 )
                        AND T.CampusId = ISNULL(@CampusId,T.CampusId)
                ORDER BY TEPR.UpTo; 


--Results Table
        INSERT  INTO #Results
                SELECT DISTINCT
                        R.ResultId
                       ,CS.ClsSectionId
                       ,R.StuEnrollId
                       ,RQ.Credits
                       ,(
                          SELECT    IsPass
                          FROM      arGradeSystemDetails
                          WHERE     GrdSysDetailId = R.GrdSysDetailId
                        ) AS IsPass
                       ,(
                          SELECT    IsCreditsEarned
                          FROM      arGradeSystemDetails
                          WHERE     GrdSysDetailId = R.GrdSysDetailId
                        ) AS IsCreditsEarned
                       ,(
                          SELECT    IsCreditsAttempted
                          FROM      arGradeSystemDetails
                          WHERE     GrdSysDetailId = R.GrdSysDetailId
                        ) AS IsCreditsAttempted
                FROM    saTransactions T
                       ,saTransCodes TC
                       ,arResults R
                       ,arClassSections CS
                       ,arReqs RQ
                WHERE   T.TransCodeId = TC.TransCodeId
                        AND TC.DefEarnings = 1
                        AND T.Voided = 0
                        AND T.StuEnrollId = R.StuEnrollId
                        AND R.TestId = CS.ClsSectionId
                        AND CS.ReqId = RQ.ReqId
                        AND T.CampusId = ISNULL(@CampusId,T.CampusId);


--Reqs Table
        INSERT  INTO #Reqs
                SELECT DISTINCT
                        PV.PrgVerId
                       ,PV.TuitionEarningId
                       ,R.Descrip
                       ,IsRequired
                FROM    arPrgVersions PV
                       ,arStuEnrollments SE
                       ,saTransactions T
                       ,saTransCodes TC
                       ,arProgVerDef PVD
                       ,arReqs R
                WHERE   PV.PrgVerId = SE.PrgVerId
                        AND SE.PrgVerId = PVD.PrgVerId
                        AND SE.StuEnrollId = T.StuEnrollId
                        AND T.TransCodeId = TC.TransCodeId
                        AND TC.DefEarnings = 1
                        AND T.Voided = 0
                        AND PVD.ReqId = R.ReqId
                        AND T.CampusId = ISNULL(@CampusId,T.CampusId); 





-------------------------------------------------------------------------------------------------------------------------------------------------------------------
--This section is where the calculations are performed.
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
        DECLARE transCursor CURSOR FAST_FORWARD FORWARD_ONLY
        FOR
            SELECT  *
            FROM    #Transactions;
	

        OPEN transCursor;
        FETCH NEXT FROM transCursor
INTO @TransactionId,@StudentName,@StudentIdentifier,@StuEnrollId,@TransCodeId,@TransAmount,@LOAStartDate,@LOAEndDate,@Earned,@FeeLevelId,@TermStart,@TermEnd,
            @percentToEarnIdx,@RemainingMethod,@DeferredRevenue;


        WHILE @@FETCH_STATUS = 0
            BEGIN
	
		--Get the status of the student, if the student is of nostart status and it was changed from currently attending
		--Then get the date they modified the status
		--Post deferred revenue for the student for the remaining amount in the next month and then donot post deferred revenue at all, once they have earned all the revenue
		--Modified on july 27 2009
                DECLARE @nostartdate DATETIME;
                DECLARE @ExceptionNo INTEGER;
                DECLARE @TransCodeDescrip VARCHAR(100);
                DECLARE @SpecialCAseUnearnedRevExistsandRemainingDurationNotExists AS BIT;
                DECLARE @ExitFor AS BIT;
                DECLARE @halfMonthBeforeDay AS INTEGER;
                DECLARE @fullMonthBeforeDay AS INTEGER;
		
                SET @SpecialCAseUnearnedRevExistsandRemainingDurationNotExists = 0;
                SET @ExitFor = 0;
		
                SET @nostartdate = '1/1/1800';
		
                IF (
                     SELECT SysSTatusID
                     FROM   #StuEnrollments d
                     WHERE  d.StuEnrollId = @StuEnrollId
                            AND d.SysSTatusID = 8
                   ) IS NOT NULL
                    BEGIN
                        SET @nostartdate = (
                                             SELECT TOP 1
                                                    ModDate
                                             FROM   syStudentStatusChanges
                                             WHERE  StuEnrollId = @StuEnrollId
                                                    AND NewStatusId IN ( SELECT StatusCodeId
                                                                         FROM   syStatusCodes
                                                                         WHERE  SysStatusId = 8 )
                                                    AND OrigStatusId IN ( SELECT    StatusCodeId
                                                                          FROM      syStatusCodes
                                                                          WHERE     SysStatusId = 9 )
                                           ); 
			
                    END;
		
		--This is the calculated duration in months of the term
                DECLARE @termStart1 DATE;
                DECLARE @termEnd1 DATE;
		
                SET @termStart1 = '1/1/1800';
                SET @termEnd1 = '1/1/1800';
		
		--Calculate last date of attendance
                DECLARE @lda DATE;
		
                IF (
                     SELECT DropReasonId
                     FROM   #StuEnrollments
                     WHERE  StuEnrollId = @StuEnrollId
                   ) IS NULL
                    BEGIN
                        SET @lda = '1/1/5000'; --Use 1/1/5000 as a max date				
                    END;	
				
                ELSE
                    BEGIN
                        IF (
                             SELECT DateDetermined
                             FROM   #StuEnrollments
                             WHERE  StuEnrollId = @StuEnrollId
                           ) IS NULL
                            BEGIN
                                IF (
                                     SELECT LDA
                                     FROM   #StuEnrollments
                                     WHERE  StuEnrollId = @StuEnrollId
                                   ) IS NOT NULL
                                    BEGIN
                                        SET @lda = (
                                                     SELECT LDA
                                                     FROM   #StuEnrollments
                                                     WHERE  StuEnrollId = @StuEnrollId
                                                   );
                                    END;
                                ELSE
                                    BEGIN
                                        SET @lda = '1/1/1800';
                                    END;
				
                            END;
                        ELSE
                            BEGIN
                                SET @lda = (
                                             SELECT DateDetermined
                                             FROM   #StuEnrollments
                                             WHERE  StuEnrollId = @StuEnrollId
                                           );
                            END;
				
                    END;
		
		
                IF @lda = '1/1/1800'
                    BEGIN
				--Exception date determined is null for this student
				--Add a record to the exceptions table
                        SET @ExceptionNo = 3;
								
                        SET @TransCodeDescrip = (
                                                  SELECT    TransCodeDescrip
                                                  FROM      #TransCodes tc
                                                  WHERE     tc.TransCodeId = @TransCodeId
                                                );
				
                        INSERT  INTO #StudentExceptions
                        VALUES  ( @StudentName,@StuEnrollId,@FeeLevelId,@TermStart,@TermEnd,@TransCodeDescrip,@TransAmount,
                                  'The student has a drop reason and no date determined.','StudentExceptions' );
			
                    END;
		
                ELSE
                    BEGIN			
		
                        IF @lda > @ReportDate
                            BEGIN
                                SET @lda = '1/1/5000';
                            END; 
				 
                        DECLARE @FeeLevelId1 INT;
                        SET @FeeLevelId1 = 0;
				
                        IF @FeeLevelId IS NOT NULL
                            BEGIN
                                SET @FeeLevelId1 = @FeeLevelId;
                            END;
				 
                        DECLARE @strTransactionID VARCHAR(50);
                        SET @strTransactionID = RTRIM(CONVERT(VARCHAR(50),@TransactionId));
				
                        DECLARE @defrevPostedDate DATE;
				
                        SET @defrevPostedDate = (
                                                  SELECT    MAX(DefRevenueDate)
                                                  FROM      saDeferredRevenues
                                                  WHERE     TransactionId = @TransactionId
                                                );
				
				
				--Modified by Saraswathi On June 03 2009
				--1- Term and 3- Course 2- Program level
				-- Course added to function as term.
                        IF (
                             @FeeLevelId1 = 1
                             OR @FeeLevelId1 = 3
                           )
                            BEGIN
					
                                SET @termStart1 = @TermStart;
                                SET @termEnd1 = (
                                                  SELECT    dbo.MinDate(@TermEnd,@lda)
                                                );
											
                                IF @nostartdate <> '1/1/1800'
                                    BEGIN					
                                        SET @termEnd1 = (
                                                          SELECT    dbo.MinDate(@termEnd1,@nostartdate)
                                                        );				
                                    END;
						
                            END;
					
				--Case 2 is program version
				--Modified by Saraswathi On June 3 2009
                        IF @FeeLevelId1 = 2
                            BEGIN
                                SET @termStart1 = (
                                                    SELECT  StartDate
                                                    FROM    #StuEnrollments
                                                    WHERE   StuEnrollId = @StuEnrollId
                                                  );
					
                                IF (
                                     SELECT ExpGradDate
                                     FROM   #StuEnrollments
                                     WHERE  StuEnrollId = @StuEnrollId
                                   ) IS NULL
                                    BEGIN
                                        DECLARE @programDurationInWeeks INT;
                                        SET @programDurationInWeeks = (
                                                                        SELECT  Weeks
                                                                        FROM    #StuEnrollments se
                                                                               ,#PrgVersions pv
                                                                        WHERE   se.PrgVerId = pv.PrgVerId
                                                                                AND se.StuEnrollId = @StuEnrollId
                                                                      );
							
                                        SET @termEnd1 = (
                                                          SELECT    dbo.MinDate(DATEADD(DAY,@programDurationInWeeks * 7,@termStart1),@lda)
                                                        ); 
							
                                    END;
                                ELSE
                                    BEGIN
                                        DECLARE @ExpGradDate DATE;
                                        SET @ExpGradDate = (
                                                             SELECT ExpGradDate
                                                             FROM   #StuEnrollments
                                                             WHERE  StuEnrollId = @StuEnrollId
                                                           );
                                        SET @termEnd1 = (
                                                          SELECT    dbo.MinDate(@ExpGradDate,@lda)
                                                        );
							
							
                                    END;	
							
                                IF @nostartdate <> '1/1/1800'
                                    BEGIN
                                        SET @termEnd1 = (
                                                          SELECT    dbo.MinDate(@termEnd1,@nostartdate)
                                                        );
                                    END;
					
					
                            END;			
				
					
				
                        IF ( @FeeLevelId1 = 0 ) --No FeeLevelId is on the Transaction Record so add record to the exceptions table	
                            BEGIN
                                SET @ExceptionNo = 1;
										
                                SET @TransCodeDescrip = (
                                                          SELECT    TransCodeDescrip
                                                          FROM      #TransCodes tc
                                                          WHERE     tc.TransCodeId = @TransCodeId
                                                        );
						
                                INSERT  INTO #StudentExceptions
                                VALUES  ( @StudentName,@StuEnrollId,@FeeLevelId,@TermStart,@TermEnd,@TransCodeDescrip,@TransAmount,
                                          'There is no Fee Level Id defined for the student','StudentExceptions' );
					
                            END;
			
                        ELSE
                            BEGIN
                                DECLARE @termDuration DECIMAL;
                                DECLARE @NewtermDuration DECIMAL;
                                DECLARE @RemainingMethod1 BIT;
						
                                SET @termDuration = 0;
                                SET @NewtermDuration = 0;
						
                                SET @RemainingMethod1 = (
                                                          SELECT    RemainingMethod
                                                          FROM      #StuEnrollments se
                                                                   ,#PrgVersions pv
                                                                   ,#TuitionEarnings te
                                                          WHERE     se.PrgVerId = pv.PrgVerId
                                                                    AND pv.TuitionEarningId = te.TuitionEarningId
                                                                    AND se.StuEnrollId = @StuEnrollId
                                                        );	
												
							
                            --IF @termStart1 = @termEnd1
                            --    BEGIN
                            --        SET @ExceptionNo = 2;
						
                            --        SET @TransCodeDescrip = (
                            --                                  SELECT    TransCodeDescrip
                            --                                  FROM      #TransCodes tc
                            --                                  WHERE     tc.TransCodeId = @TransCodeId
                            --                                );
						
                            --        INSERT  INTO #StudentExceptions
                            --        VALUES  ( @StudentName,@StuEnrollId,@FeeLevelId,@TermStart,@TermEnd,@TransCodeDescrip,@TransAmount,
                            --                  'Term Start Date and Term End Date/Date Determined/LDA are the same','StudentExceptions' );
						
					
                            --    END;	
							
                            --ELSE
                                IF @termStart1 <> @termEnd1
                                    BEGIN
								--Added by Saraswathi lakshmanan On July 1 2009
								--check whether to apply a straight percentage or a conversion table
                                        DECLARE @DeferredRevenuePostingBasedonHolidays VARCHAR(5);
								
                                        IF (
                                             SELECT TakeHolidays
                                             FROM   #StuEnrollments se
                                                   ,#PrgVersions pv
                                                   ,#TuitionEarnings te
                                             WHERE  se.StuEnrollId = @StuEnrollId
                                                    AND se.PrgVerId = pv.PrgVerId
                                                    AND pv.TuitionEarningId = te.TuitionEarningId
                                           ) IS NOT NULL
                                            BEGIN
                                                IF (
                                                     SELECT TakeHolidays
                                                     FROM   #StuEnrollments se
                                                           ,#PrgVersions pv
                                                           ,#TuitionEarnings te
                                                     WHERE  se.StuEnrollId = @StuEnrollId
                                                            AND se.PrgVerId = pv.PrgVerId
                                                            AND pv.TuitionEarningId = te.TuitionEarningId
                                                   ) = 1
                                                    BEGIN
                                                        SET @DeferredRevenuePostingBasedonHolidays = 'yes';																				
                                                    END;
                                                ELSE
                                                    BEGIN
                                                        SET @DeferredRevenuePostingBasedonHolidays = 'no';												
                                                    END;
									
									
                                            END;
                                        ELSE
                                            BEGIN
                                                SET @DeferredRevenuePostingBasedonHolidays = 'no';
									
                                            END;
									
									
                                        DECLARE @newStartDate AS DATE;
                                        SET @newStartDate = (
                                                              SELECT    dbo.MaxDate(@termStart1,DATEADD(DAY,1,@defrevPostedDate))
                                                            );
								
                                        DECLARE @startDate DATE; 
                                        SET @startDate = @termStart1;
								
								--Code modified by Saraswathi Lakshmanan on Sept 10 2010
								--INclude LOA to elapsed time and term duration only if LOA is between the elapsed time period.
                                        DECLARE @Trad_numberofHOlidaysbetweentheGivenPeriod AS INTEGER;
                                        DECLARE @Rem_numberofHOlidaysbetweentheGivenPeriod AS INTEGER;
                                        DECLARE @Trad_numberofLOAdaysbetweentheGivenPeriod AS INTEGER;
                                        DECLARE @Rem_numberofLOAdaysbetweentheGivenPeriod AS INTEGER; 								
                                        DECLARE @NumberOfHolidaysBetweenTerm AS INTEGER;
								
                                        SET @Trad_numberofHOlidaysbetweentheGivenPeriod = (
                                                                                            SELECT  dbo.NumberofHolidaysBetweentheGivenPeriod(@ReportDate,
                                                                                                                                              @startDate,
                                                                                                                                              @CampusId)
                                                                                          );					
                                        SET @Rem_numberofHOlidaysbetweentheGivenPeriod = (
                                                                                           SELECT   dbo.NumberofHolidaysBetweentheGivenPeriod(@ReportDate,
                                                                                                                                              @newStartDate,
                                                                                                                                              @CampusId)
                                                                                         );
                                        SET @Trad_numberofLOAdaysbetweentheGivenPeriod = (
                                                                                           SELECT   dbo.NumberofLOADaysBetweentheGivenPeriod(@ReportDate,
                                                                                                                                             @startDate,
                                                                                                                                             @StuEnrollId)
                                                                                         );
                                        SET @Rem_numberofLOAdaysbetweentheGivenPeriod = (
                                                                                          SELECT    dbo.NumberofLOADaysBetweentheGivenPeriod(@ReportDate,
                                                                                                                                             @newStartDate,
                                                                                                                                             @StuEnrollId)
                                                                                        );
                                        SET @NumberOfHolidaysBetweenTerm = (
                                                                             SELECT dbo.NumberofHolidaysBetweentheGivenPeriod(@TermEnd,@startDate,@CampusId)
                                                                           );
																								
                                        IF @termStart1 <> '1/1/1800'
                                            AND @termEnd1 <> '1/1/1800'
                                            BEGIN										
                                                IF @percentToEarnIdx = 1
                                                    BEGIN												
												--If it is a equal percentage every month  Then there is no holidays into considereation
												--termDuration = (termEnd.Subtract(termStart).TotalDays) / 30
                                                        SET @termDuration = ( DATEDIFF(MONTH,@startDate,@termEnd1) + 1 ) * 100;
                                                        SET @NewtermDuration = ( DATEDIFF(MONTH,@newStartDate,@termEnd1) + 1 ) * 100;
                                                        SET @DeferredRevenuePostingBasedonHolidays = 'no';
											
                                                    END;
                                                ELSE
                                                    BEGIN												
                                                        IF ( DATEDIFF(DAYOFYEAR,@startDate,@termEnd1) + 0 - @Trad_numberofLOAdaysbetweentheGivenPeriod ) <= 0
                                                            BEGIN														
                                                                SET @termDuration = 0;
                                                            END;
                                                        ELSE
                                                            BEGIN														
                                                                SET @termDuration = ( ( DATEDIFF(DAYOFYEAR,@startDate,@termEnd1) + 0
                                                                                        - @Trad_numberofLOAdaysbetweentheGivenPeriod ) * 100 ) / 30;
														
                                                            END;
													
                                                        IF ( DATEDIFF(DAYOFYEAR,@newStartDate,@termEnd1) + 0 - @Rem_numberofLOAdaysbetweentheGivenPeriod ) <= 0
                                                            BEGIN
                                                                SET @NewtermDuration = 0;
                                                            END;
                                                        ELSE
                                                            BEGIN
                                                                SET @NewtermDuration = ( ( DATEDIFF(DAYOFYEAR,@newStartDate,@termEnd1) + 0
                                                                                           - @Rem_numberofLOAdaysbetweentheGivenPeriod ) * 100 ) / 30;
													
                                                            END;
												
                                                        IF LOWER(@DeferredRevenuePostingBasedonHolidays) = 'yes'
                                                            BEGIN
                                                                IF ( DATEDIFF(DAYOFYEAR,@startDate,@termEnd1) + 0 - @Trad_numberofLOAdaysbetweentheGivenPeriod
                                                                     - @NumberOfHolidaysBetweenTerm ) <= 0
                                                                    BEGIN
                                                                        SET @termDuration = 0;
                                                                    END;
                                                                ELSE
                                                                    BEGIN
                                                                        SET @termDuration = ( ( DATEDIFF(DAYOFYEAR,@startDate,@termEnd1) + 0
                                                                                                - @Trad_numberofLOAdaysbetweentheGivenPeriod
                                                                                                - @NumberOfHolidaysBetweenTerm ) * 100 ) / 30;
                                                                    END;
															
                                                                IF ( DATEDIFF(DAYOFYEAR,@newStartDate,@termEnd1) + 0 - @Rem_numberofLOAdaysbetweentheGivenPeriod
                                                                     - @Rem_numberofHOlidaysbetweentheGivenPeriod ) <= 0
                                                                    BEGIN
                                                                        SET @NewtermDuration = 0;
                                                                    END;
                                                                ELSE
                                                                    BEGIN
                                                                        SET @NewtermDuration = ( ( DATEDIFF(DAYOFYEAR,@newStartDate,@termEnd1) + 0
                                                                                                   - @Rem_numberofLOAdaysbetweentheGivenPeriod
                                                                                                   - @Rem_numberofHOlidaysbetweentheGivenPeriod ) * 100 ) / 30;
                                                                    END;
															
													
                                                            END;
													
													
												
                                                    END;
											
											
									
									
                                            END;
									
                                        DECLARE @EarnedRevenue AS DECIMAL;
                                        DECLARE @UnearnedRevenue AS DECIMAL;
								
                                        SET @EarnedRevenue = ISNULL((
                                                                      SELECT    SUM(Amount)
                                                                      FROM      saDeferredRevenues
                                                                      WHERE     TransactionId = @TransactionId
                                                                    ),0);
                                        SET @UnearnedRevenue = @TransAmount - @EarnedRevenue;								
								
								--ignore all calculations if the termDuration is zero or negative
								--If percentToEarnIdx = 0 And Not (termDuration > 0) Then Exit For
								--Modified by Saraswathi on July 1 2009								
                                        IF ( @RemainingMethod = 0 )
                                            BEGIN
                                                IF (
                                                     @percentToEarnIdx = 0
                                                     OR @percentToEarnIdx = 1
                                                   )
                                                    AND (
                                                          @termDuration <= 0
                                                          AND @UnearnedRevenue > 0
                                                        )
                                                    BEGIN
                                                        SET @SpecialCAseUnearnedRevExistsandRemainingDurationNotExists = 1;
												
                                                    END;
                                                ELSE
                                                    BEGIN
                                                        IF (
                                                             @percentToEarnIdx = 0
                                                             OR @percentToEarnIdx = 1
                                                           )
                                                            AND NOT ( @termDuration > 0 )
                                                            AND ( @UnearnedRevenue < 0 )
                                                            BEGIN
                                                                SET @ExitFor = 1;	
                                                            END;
																				
                                                    END;									
                                            END;
								
                                        IF @RemainingMethod = 1
                                            BEGIN
                                                IF (
                                                     @percentToEarnIdx = 0
                                                     OR @percentToEarnIdx = 1
                                                   )
                                                    AND (
                                                          @NewtermDuration <= 0
                                                          AND @UnearnedRevenue > 0
                                                        )
                                                    BEGIN
                                                        SET @SpecialCAseUnearnedRevExistsandRemainingDurationNotExists = 1;									
                                                    END;
                                                ELSE
                                                    BEGIN
                                                        IF (
                                                             @percentToEarnIdx = 0
                                                             OR @percentToEarnIdx = 1
                                                           )
                                                            AND NOT ( @NewtermDuration > 0 )
                                                            AND ( @UnearnedRevenue < 0 )
                                                            BEGIN
                                                                SET @ExitFor = 1;
                                                            END;										
												
                                                    END;
									
                                            END;
								
								--The rest of the code should only execute if @EXITFOR is still 0
                                        IF @ExitFor = 0
                                            BEGIN
										--this is the elapsed time in months from the beginning of the period to the report date
                                                DECLARE @elapsedTime AS DECIMAL = 0.00;
                                                DECLARE @newElapsedTime AS DECIMAL = 0.00;
										
                                                IF LOWER(@DeferredRevenuePostingBasedonHolidays) = 'yes'
                                                    BEGIN
                                                        IF ( DATEDIFF(DAYOFYEAR,@startDate,@ReportDate) + 0 - @Trad_numberofLOAdaysbetweentheGivenPeriod
                                                             - @Trad_numberofHOlidaysbetweentheGivenPeriod ) <= 0
                                                            BEGIN
                                                                SET @elapsedTime = 0;
                                                            END;
                                                        ELSE
                                                            BEGIN
                                                                SET @elapsedTime = ( ( DATEDIFF(DAYOFYEAR,@startDate,@ReportDate) + 0
                                                                                       - @Trad_numberofLOAdaysbetweentheGivenPeriod
                                                                                       - @Trad_numberofHOlidaysbetweentheGivenPeriod ) * 100 ) / 30;
                                                            END;
													
                                                        IF @RemainingMethod = 1
                                                            BEGIN
                                                                IF ( DATEDIFF(DAYOFYEAR,@newStartDate,@ReportDate) + 0
                                                                     - @Rem_numberofLOAdaysbetweentheGivenPeriod - @Rem_numberofHOlidaysbetweentheGivenPeriod ) <= 0
                                                                    BEGIN
                                                                        SET @newElapsedTime = 0;
                                                                    END;
                                                                ELSE
                                                                    BEGIN
                                                                        SET @newElapsedTime = ( ( DATEDIFF(DAYOFYEAR,@newStartDate,@ReportDate) + 0
                                                                                                  - @Rem_numberofLOAdaysbetweentheGivenPeriod
                                                                                                  - @Rem_numberofHOlidaysbetweentheGivenPeriod ) * 100 ) / 30;
                                                                    END;
                                                            END;
													
													
                                                    END;
                                                ELSE
                                                    BEGIN												
												
                                                        IF ( DATEDIFF(DAYOFYEAR,@startDate,@ReportDate) + 1 - @Trad_numberofLOAdaysbetweentheGivenPeriod ) <= 0
                                                            BEGIN														
                                                                SET @elapsedTime = 0;
                                                            END;
                                                        ELSE
                                                            BEGIN														
														--The original code from the VB class used (DateDiff(DayOfYear, @startDate, @reportDate) + 1 - @Trad_numberofLOAdaysbetweentheGivenPeriod)/100
														--However, in SQL Server the value returned is 0 rather than 0.90. For SQL Server we have to first mulitply by 100 and then remember
														--later on to divide by 100 since here we are now going to get 90.
                                                                SET @elapsedTime = ( ( DATEDIFF(DAYOFYEAR,@startDate,@ReportDate) + 1
                                                                                       - @Trad_numberofLOAdaysbetweentheGivenPeriod ) * 100 ) / 30;
														
                                                            END;
													
                                                        IF @RemainingMethod = 1
                                                            BEGIN
                                                                IF ( DATEDIFF(DAYOFYEAR,@newStartDate,@ReportDate) + 1
                                                                     - @Rem_numberofLOAdaysbetweentheGivenPeriod ) <= 0
                                                                    BEGIN
                                                                        SET @newElapsedTime = 0;
                                                                    END;
                                                                ELSE
                                                                    BEGIN
                                                                        SET @newElapsedTime = ( ( DATEDIFF(DAYOFYEAR,@newStartDate,@ReportDate) + 1
                                                                                                  - @Rem_numberofLOAdaysbetweentheGivenPeriod ) * 100 ) / 30;
                                                                    END;
														 
                                                            END;
													
											
                                                    END;
										
										 --elapsed time can not be negative
                                                IF @elapsedTime < 0
                                                    BEGIN
                                                        SET @elapsedTime = 0.0;
                                                    END;
											
                                                IF @newElapsedTime < 0
                                                    BEGIN
                                                        SET @newElapsedTime = 0.0;
                                                    END;	
										
										--if the student started before this day of the month .. round elapsed time to a half month
                                                DECLARE @roundedElapsedTime AS DECIMAL;
                                                DECLARE @NewroundedElapsedTime AS DECIMAL; 
										
                                                SET @roundedElapsedTime = @elapsedTime;
                                                SET @NewroundedElapsedTime = @newElapsedTime;
										
										--round elapsed time only if the elapsed time is > 0
                                                IF @elapsedTime > 0
                                                    BEGIN
                                                        IF @percentToEarnIdx = 1
                                                            BEGIN
                                                                SET @roundedElapsedTime = ( DATEDIFF(MONTH,@startDate,@ReportDate) + 1 ) * 100;
                                                            END;
                                                        ELSE
                                                            BEGIN
                                                                IF DATEPART(MONTH,@ReportDate) = DATEPART(MONTH,@startDate)
                                                                    AND DATEPART(YEAR,@ReportDate) = DATEPART(YEAR,@startDate)
                                                                    BEGIN
																
                                                                        SET @halfMonthBeforeDay = (
                                                                                                    SELECT  te.HalfMonthBeforeDay
                                                                                                    FROM    #StuEnrollments se
                                                                                                           ,#PrgVersions pv
                                                                                                           ,#TuitionEarnings te
                                                                                                    WHERE   se.StuEnrollId = @StuEnrollId
                                                                                                            AND se.PrgVerId = pv.PrgVerId
                                                                                                            AND pv.TuitionEarningId = te.TuitionEarningId
                                                                                                  );
																						   
                                                                        IF DATEPART(DAY,@startDate) <= @halfMonthBeforeDay
                                                                            AND @halfMonthBeforeDay > 0
                                                                            BEGIN
                                                                                SET @roundedElapsedTime = 0.5 * 100;
																	
                                                                            END;
																
																
                                                                        SET @fullMonthBeforeDay = (
                                                                                                    SELECT  te.FullMonthBeforeDay
                                                                                                    FROM    #StuEnrollments se
                                                                                                           ,#PrgVersions pv
                                                                                                           ,#TuitionEarnings te
                                                                                                    WHERE   se.StuEnrollId = @StuEnrollId
                                                                                                            AND se.PrgVerId = pv.PrgVerId
                                                                                                            AND pv.TuitionEarningId = te.TuitionEarningId
                                                                                                  );
																
																
                                                                        IF DATEPART(DAY,@startDate) <= @fullMonthBeforeDay
                                                                            AND @fullMonthBeforeDay > 0
                                                                            BEGIN
                                                                                SET @roundedElapsedTime = 1 * 100;
                                                                            END;	
																						   
																																
																					   
                                                                    END;
                                                            END;
													
											
                                                    END;
											
                                                IF @newElapsedTime > 0
                                                    BEGIN
                                                        IF @percentToEarnIdx = 1
                                                            BEGIN
                                                                SET @NewroundedElapsedTime = ( DATEDIFF(MONTH,@newStartDate,@ReportDate) + 1 ) * 100;
                                                            END;
                                                        ELSE
                                                            BEGIN
                                                                IF DATEPART(MONTH,@ReportDate) = DATEPART(MONTH,@startDate)
                                                                    AND DATEPART(YEAR,@ReportDate) = DATEPART(YEAR,@startDate)
                                                                    BEGIN
                                                                        SET @halfMonthBeforeDay = (
                                                                                                    SELECT  te.HalfMonthBeforeDay
                                                                                                    FROM    #StuEnrollments se
                                                                                                           ,#PrgVersions pv
                                                                                                           ,#TuitionEarnings te
                                                                                                    WHERE   se.StuEnrollId = @StuEnrollId
                                                                                                            AND se.PrgVerId = pv.PrgVerId
                                                                                                            AND pv.TuitionEarningId = te.TuitionEarningId
                                                                                                  );
                                                                        IF DATEPART(DAY,@startDate) <= @halfMonthBeforeDay
                                                                            AND @halfMonthBeforeDay > 0
                                                                            BEGIN
                                                                                SET @NewroundedElapsedTime = 0.5 * 100;
                                                                            END;
																	
                                                                        SET @fullMonthBeforeDay = (
                                                                                                    SELECT  te.FullMonthBeforeDay
                                                                                                    FROM    #StuEnrollments se
                                                                                                           ,#PrgVersions pv
                                                                                                           ,#TuitionEarnings te
                                                                                                    WHERE   se.StuEnrollId = @StuEnrollId
                                                                                                            AND se.PrgVerId = pv.PrgVerId
                                                                                                            AND pv.TuitionEarningId = te.TuitionEarningId
                                                                                                  );
																						   
                                                                        IF DATEPART(DAY,@startDate) <= @fullMonthBeforeDay
                                                                            AND @fullMonthBeforeDay > 0
                                                                            BEGIN
                                                                                SET @NewroundedElapsedTime = 1 * 100;
                                                                            END;						   
																
                                                                    END;
													
													
                                                            END;
											
                                                    END;											
											
											--check that rounded elapsed time does not exceed term duration
                                                IF @roundedElapsedTime > @termDuration
                                                    BEGIN
                                                        SET @roundedElapsedTime = @termDuration;
                                                    END; 
												
                                                IF @NewroundedElapsedTime > @NewtermDuration
                                                    BEGIN
                                                        SET @NewroundedElapsedTime = @NewtermDuration;
                                                    END; 
												
                                                IF (
                                                     @percentToEarnIdx = 0
                                                     OR @percentToEarnIdx = 1
                                                   )
                                                    BEGIN											
                                                        IF @RemainingMethod = 1
                                                            BEGIN
                                                                IF @NewtermDuration <= 0
                                                                    AND @SpecialCAseUnearnedRevExistsandRemainingDurationNotExists = 1
                                                                    BEGIN
                                                                        UPDATE  #Transactions
                                                                        SET     DeferredRevenue = @UnearnedRevenue
                                                                        WHERE   TransactionId = @TransactionId
                                                                                AND StuEnrollId = @StuEnrollId;														
                                                                    END;
                                                                ELSE
                                                                    BEGIN
                                                                        IF @NewtermDuration <> 0
                                                                            BEGIN																
																			
																			 --apply straight line percentage
                                                                                UPDATE  #Transactions
                                                                                SET     DeferredRevenue = ( @UnearnedRevenue * @NewroundedElapsedTime )
                                                                                        / ( @NewtermDuration )
                                                                                WHERE   TransactionId = @TransactionId
                                                                                        AND StuEnrollId = @StuEnrollId;	
                                                                            END;
                                                                        ELSE
                                                                            BEGIN
																			--Exception
																			--CAse 1: feelevelid is not set,
																			--Case 2:term duration =0 ,  term Start and term End is the same or term start and LDA or Datedetermined is the same..
                                                                                UPDATE  #Transactions
                                                                                SET     DeferredRevenue = 0
                                                                                       ,Earned = 0
                                                                                WHERE   TransactionId = @TransactionId
                                                                                        AND StuEnrollId = @StuEnrollId;
                                                                            END;
																	 
																
                                                                    END;
															
																
                                                            END;
													
                                                        ELSE
                                                            BEGIN
                                                                IF @termDuration <= 0
                                                                    AND @SpecialCAseUnearnedRevExistsandRemainingDurationNotExists = 1
                                                                    BEGIN
                                                                        UPDATE  #Transactions
                                                                        SET     DeferredRevenue = @TransAmount
                                                                        WHERE   TransactionId = @TransactionId
                                                                                AND StuEnrollId = @StuEnrollId;	
                                                                    END;
                                                                ELSE
                                                                    BEGIN
                                                                        IF @termDuration <> 0
                                                                            BEGIN																			
																			-- apply straight line percentage
                                                                                UPDATE  #Transactions
                                                                                SET     DeferredRevenue = ( @TransAmount * @roundedElapsedTime )
                                                                                        / ( @termDuration )
                                                                                WHERE   TransactionId = @TransactionId
                                                                                        AND StuEnrollId = @StuEnrollId;
                                                                            END;
                                                                        ELSE
                                                                            BEGIN
																			--Exception
																			--CAse 1: feelevelid is not set,
																			--Case 2:term duration =0 ,  term Start and term End is the same or term start and LDA or Datedetermined is the same..
                                                                                UPDATE  #Transactions
                                                                                SET     DeferredRevenue = 0
                                                                                       ,Earned = 0
                                                                                WHERE   TransactionId = @TransactionId
                                                                                        AND StuEnrollId = @StuEnrollId;
                                                                            END;
																
                                                                    END;
														
                                                            END;
													
													
                                                    END;
												
                                                IF ( @percentToEarnIdx = 2 )
                                                    BEGIN											
                                                        DECLARE @totalCredits AS INTEGER;
                                                        DECLARE @PercentageRangeBasisIdx AS INTEGER;
                                                        DECLARE @hoursScheduled AS DECIMAL = 0.0;
                                                        DECLARE @hoursCompleted AS DECIMAL = 0.0;
                                                        DECLARE @totalProgramHours AS DECIMAL = 0.00;
                                                        DECLARE @ClsSectionId VARCHAR(50);
													 
                                                        SET @PercentageRangeBasisIdx = (
                                                                                         SELECT PercentageRangeBasisIdx
                                                                                         FROM   #StuEnrollments se
                                                                                               ,#PrgVersions pv
                                                                                               ,#TuitionEarnings te
                                                                                         WHERE  se.StuEnrollId = @StuEnrollId
                                                                                                AND se.PrgVerId = pv.PrgVerId
                                                                                                AND pv.TuitionEarningId = te.TuitionEarningId
                                                                                       );
																				   
                                                        DECLARE @percentArgument AS INTEGER;
													
                                                        IF @PercentageRangeBasisIdx = 0
                                                            BEGIN													
															-- Program Length
															--Modified by Saraswathi lakshmanan
															--When the term duration is zero the percent argument is considered to be zero . Since it is giving divide by zero error
                                                                IF @termDuration > 0
                                                                    BEGIN
                                                                        SET @percentArgument = @roundedElapsedTime * 100 / @termDuration;
                                                                    END;
                                                                ELSE
                                                                    BEGIN
                                                                        SET @percentArgument = 0.0;														
                                                                    END;														 
														
                                                            END;
														
                                                        IF @PercentageRangeBasisIdx = 1
                                                            BEGIN													
															--Credits Earned
															-- add up Credits Earned
                                                                DECLARE @creditsEarned AS INTEGER = 0;
															
                                                                SET @creditsEarned = (
                                                                                       SELECT   SUM(Credits)
                                                                                       FROM     #Results
                                                                                       WHERE    StuEnrollId = @StuEnrollId
                                                                                                AND IsCreditsEarned IS NOT NULL
                                                                                                AND IsCreditsEarned = 1
                                                                                     );
																				  
															-- this is the total number of credits for the program
                                                                SET @totalCredits = (
                                                                                      SELECT    pv.Credits
                                                                                      FROM      #StuEnrollments se
                                                                                               ,#PrgVersions pv
                                                                                      WHERE     se.StuEnrollId = @StuEnrollId
                                                                                                AND se.PrgVerId = pv.PrgVerId
                                                                                    );
																				  
															-- Credits Earned
                                                                IF @totalCredits > 0
                                                                    BEGIN
                                                                        SET @percentArgument = @creditsEarned * 100.0 / @totalCredits; 
                                                                    END;
                                                                ELSE
                                                                    BEGIN
                                                                        SET @percentArgument = 0.0;
                                                                    END;
														
                                                            END;
														
                                                        IF @PercentageRangeBasisIdx = 2
                                                            BEGIN
															--Credits Attempted
															--add up Credits Attempted
                                                                DECLARE @creditsAttempted AS INTEGER = 0;
															 
                                                                SET @creditsAttempted = (
                                                                                          SELECT    SUM(Credits)
                                                                                          FROM      #Results
                                                                                          WHERE     StuEnrollId = @StuEnrollId
                                                                                                    AND IsCreditsAttempted IS NOT NULL
                                                                                                    AND IsCreditsAttempted = 1
                                                                                        );
																					  
															--this is the total number of credits for the program
                                                                SET @totalCredits = (
                                                                                      SELECT    pv.Credits
                                                                                      FROM      #StuEnrollments se
                                                                                               ,#PrgVersions pv
                                                                                      WHERE     se.StuEnrollId = @StuEnrollId
                                                                                                AND se.PrgVerId = pv.PrgVerId
                                                                                    );
																				  
															-- Credits Attempted
                                                                IF @totalCredits > 0
                                                                    BEGIN
                                                                        SET @percentArgument = @creditsAttempted * 100.0 / @totalCredits;														
                                                                    END; 
                                                                ELSE
                                                                    BEGIN
                                                                        SET @percentArgument = 0.0;
                                                                    END;								 
														
														
                                                            END;
														
														
                                                        IF @PercentageRangeBasisIdx = 3
                                                            BEGIN													
															-- Courses completed
															--get the courses for the program version
                                                                DECLARE @numberOfCourses AS INTEGER;
															 
                                                                SET @numberOfCourses = (
                                                                                         SELECT COUNT(*)
                                                                                         FROM   #StuEnrollments se
                                                                                               ,#PrgVersions pv
                                                                                               ,#Reqs rq
                                                                                         WHERE  se.StuEnrollId = @StuEnrollId
                                                                                                AND se.PrgVerId = pv.PrgVerId
                                                                                                AND pv.PrgVerId = rq.PrgVerId
                                                                                       );
																					 
															--calculate number of courses taken
                                                                DECLARE @numberOfCoursesTaken AS INTEGER = 0;
															
                                                                SET @numberOfCoursesTaken = (
                                                                                              SELECT    COUNT(*)
                                                                                              FROM      #StuEnrollments se
                                                                                                       ,#Results rs
                                                                                              WHERE     se.StuEnrollId = @StuEnrollId
                                                                                                        AND se.StuEnrollId = rs.StuEnrollId
                                                                                                        AND rs.IsPass IS NOT NULL
                                                                                                        AND rs.IsPass = 1
                                                                                            );
																						 
															--'Percentage of courses completed
                                                                IF @numberOfCourses > 0
                                                                    BEGIN
                                                                        SET @percentArgument = @numberOfCoursesTaken * 100.0 / @numberOfCourses; 
                                                                    END;
                                                                ELSE
                                                                    BEGIN
                                                                        SET @percentArgument = 0.0;
                                                                    END;
															
														
                                                            END;
														
														
                                                        IF @PercentageRangeBasisIdx = 4
                                                            BEGIN
															--get number of scheduled hours												
																
															--If attendance is being tracked by class then we need to get the total scheduled hours so far for those classes
															--If attendance is by day there is no need to use the cursor as we can go straight to the arStudentClockAttendance table.
                                                                IF LOWER((
                                                                           SELECT   dbo.AppSettings('TrackSapAttendance',@CampusId)
                                                                         )) = 'byclass'
                                                                    BEGIN
                                                                        DECLARE ResultCursor CURSOR FORWARD_ONLY FAST_FORWARD
                                                                        FOR
                                                                            SELECT  ClsSectionId
                                                                            FROM    #Results rs
                                                                            WHERE   rs.StuEnrollId = @StuEnrollId;
																		
                                                                        OPEN ResultCursor;
                                                                        FETCH NEXT FROM ResultCursor
																	INTO @ClsSectionId;
																	
                                                                        WHILE @@FETCH_STATUS = 0
                                                                            BEGIN
                                                                                SET @hoursScheduled = @hoursScheduled
                                                                                    + (
                                                                                        SELECT  SUM(Scheduled)
                                                                                        FROM    dbo.atClsSectAttendance
                                                                                        WHERE   ClsSectionId = @ClsSectionId
                                                                                                AND StuEnrollId = @StuEnrollId
                                                                                                AND MeetDate <= @ReportDate
                                                                                      );													 
																																						
                                                                                FETCH NEXT FROM ResultCursor
																			INTO @ClsSectionId;
                                                                            END;
																		
                                                                        CLOSE ResultCursor;
                                                                        DEALLOCATE ResultCursor;
																
																
                                                                    END;
                                                                ELSE
                                                                    BEGIN
                                                                        SET @hoursScheduled = @hoursScheduled + (
                                                                                                                  SELECT    SUM(SchedHours)
                                                                                                                  FROM      dbo.arStudentClockAttendance
                                                                                                                  WHERE     StuEnrollId = @StuEnrollId
                                                                                                                            AND RecordDate <= @ReportDate
                                                                                                                );
                                                                    END;
																
															
															
															--get the total duration of the program in hours																										
                                                                SET @totalProgramHours = (
                                                                                           SELECT   Hours
                                                                                           FROM     #StuEnrollments se
                                                                                                   ,#PrgVersions pv
                                                                                           WHERE    se.StuEnrollId = @StuEnrollId
                                                                                                    AND se.PrgVerId = pv.PrgVerId
                                                                                         );
															
															--Percentage of scheduled hours
                                                                IF @totalProgramHours > 0
                                                                    BEGIN
                                                                        SET @percentArgument = @hoursScheduled * 100.0 / @totalProgramHours; 
                                                                    END; 
                                                                ELSE
                                                                    BEGIN
                                                                        SET @percentArgument = 0.0;
                                                                    END;
															
														
                                                            END;
														
														
                                                        IF @PercentageRangeBasisIdx = 5
                                                            BEGIN
															--get number of completed hours
                                                                IF LOWER((
                                                                           SELECT   dbo.AppSettings('TrackSapAttendance',@CampusId)
                                                                         )) = 'byclass'
                                                                    BEGIN
                                                                        DECLARE ResultCursor CURSOR FAST_FORWARD FORWARD_ONLY
                                                                        FOR
                                                                            SELECT  ClsSectionId
                                                                            FROM    #Results rs
                                                                            WHERE   rs.StuEnrollId = @StuEnrollId;
																		
                                                                        OPEN ResultCursor;
                                                                        FETCH NEXT FROM ResultCursor
																	INTO @ClsSectionId;
																	
                                                                        WHILE @@FETCH_STATUS = 0
                                                                            BEGIN
                                                                                SET @hoursScheduled = @hoursScheduled
                                                                                    + (
                                                                                        SELECT  SUM(Scheduled)
                                                                                        FROM    dbo.atClsSectAttendance
                                                                                        WHERE   ClsSectionId = @ClsSectionId
                                                                                                AND StuEnrollId = @StuEnrollId
                                                                                                AND MeetDate <= @ReportDate
                                                                                      );
																													 
                                                                                SET @hoursCompleted = @hoursCompleted
                                                                                    + (
                                                                                        SELECT  SUM(Actual)
                                                                                        FROM    dbo.atClsSectAttendance
                                                                                        WHERE   ClsSectionId = @ClsSectionId
                                                                                                AND StuEnrollId = @StuEnrollId
                                                                                                AND MeetDate <= @ReportDate
                                                                                      );
																																 
																																						
                                                                                FETCH NEXT FROM ResultCursor
																			INTO @ClsSectionId;
                                                                            END;
																		
                                                                        CLOSE ResultCursor;
                                                                        DEALLOCATE ResultCursor;
																
																
                                                                    END;
                                                                ELSE
                                                                    BEGIN
                                                                        SET @hoursScheduled = @hoursScheduled + (
                                                                                                                  SELECT    SUM(SchedHours)
                                                                                                                  FROM      dbo.arStudentClockAttendance
                                                                                                                  WHERE     StuEnrollId = @StuEnrollId
                                                                                                                            AND RecordDate <= @ReportDate
                                                                                                                );
																											 
                                                                        SET @hoursCompleted = @hoursCompleted + (
                                                                                                                  SELECT    SUM(ActualHours)
                                                                                                                  FROM      dbo.arStudentClockAttendance
                                                                                                                  WHERE     StuEnrollId = @StuEnrollId
                                                                                                                            AND RecordDate <= @ReportDate
                                                                                                                );
																											 
                                                                    END;
																
															--get the total duration of the program in hours																										
                                                                SET @totalProgramHours = (
                                                                                           SELECT   Hours
                                                                                           FROM     #StuEnrollments se
                                                                                                   ,#PrgVersions pv
                                                                                           WHERE    se.StuEnrollId = @StuEnrollId
                                                                                                    AND se.PrgVerId = pv.PrgVerId
                                                                                         );
																					  
															--Percentage of scheduled hours
                                                                IF @totalProgramHours > 0
                                                                    BEGIN
                                                                        SET @percentArgument = @hoursCompleted * 100.0 / @totalProgramHours; 
                                                                    END; 
                                                                ELSE
                                                                    BEGIN
                                                                        SET @percentArgument = 0.0;	
                                                                    END;
																
														
                                                            END;	
													
													--percent argument should never be grater that 100
                                                        IF @percentArgument > 100.0
                                                            BEGIN
                                                                SET @percentArgument = 100.0;
                                                            END;
														
														
                                                        DECLARE @percentResult AS INTEGER = 0; 
													
                                                        IF @percentArgument > 0
                                                            BEGIN
                                                                SET @percentResult = (
                                                                                       SELECT TOP 1
                                                                                                tepr.EarnPercent
                                                                                       FROM     #StuEnrollments se
                                                                                               ,#PrgVersions pv
                                                                                               ,#TuitionEarnings te
                                                                                               ,#TuitionEarningsPercentageRanges tepr
                                                                                       WHERE    se.StuEnrollId = @StuEnrollId
                                                                                                AND se.PrgVerId = pv.PrgVerId
                                                                                                AND pv.TuitionEarningId = te.TuitionEarningId
                                                                                                AND te.TuitionEarningId = tepr.TuitionEarningId
                                                                                                AND tepr.UpTo >= @percentArgument
                                                                                     );														
                                                            END;
																		 
													--apply percentage
                                                        UPDATE  #Transactions
                                                        SET     DeferredRevenue = ( TransAmount * @percentResult ) / 100
                                                        WHERE   TransactionId = @TransactionId
                                                                AND StuEnrollId = @StuEnrollId;																		  
												
                                                    END;	
												
											--add this value to the totalDeferredRevenue in the transCodes table
											--add Earned Amount from each transaction to AccruedAmount in the transCodes table
                                                IF (
                                                     SELECT TotalDeferredRevenue
                                                     FROM   #TransCodes
                                                     WHERE  TransCodeId = @TransCodeId
                                                   ) IS NOT NULL
                                                    BEGIN
                                                        UPDATE  #TransCodes
                                                        SET     TotalDeferredRevenue = TotalDeferredRevenue + (
                                                                                                                SELECT  DeferredRevenue
                                                                                                                FROM    #Transactions
                                                                                                                WHERE   TransactionId = @TransactionId
                                                                                                              )
                                                        WHERE   TransCodeId = @TransCodeId;										
                                                    END;
                                                ELSE
                                                    BEGIN
                                                        UPDATE  #TransCodes
                                                        SET     TotalDeferredRevenue = (
                                                                                         SELECT DeferredRevenue
                                                                                         FROM   #Transactions
                                                                                         WHERE  TransactionId = @TransactionId
                                                                                       )
                                                        WHERE   TransCodeId = @TransCodeId;									
                                                    END;
																				
																							  
                                                IF (
                                                     SELECT AccruedAmount
                                                     FROM   #TransCodes
                                                     WHERE  TransCodeId = @TransCodeId
                                                   ) IS NOT NULL
                                                    BEGIN
                                                        UPDATE  #TransCodes
                                                        SET     AccruedAmount = AccruedAmount + (
                                                                                                  SELECT    Earned
                                                                                                  FROM      #Transactions
                                                                                                  WHERE     TransactionId = @TransactionId
                                                                                                )
                                                        WHERE   TransCodeId = @TransCodeId;
                                                    END;	
                                                ELSE
                                                    BEGIN
                                                        UPDATE  #TransCodes
                                                        SET     AccruedAmount = (
                                                                                  SELECT    Earned
                                                                                  FROM      #Transactions
                                                                                  WHERE     TransactionId = @TransactionId
                                                                                )
                                                        WHERE   TransCodeId = @TransCodeId;
                                                    END;								
											
											
											
											
                                                IF @RemainingMethod = 1
                                                    BEGIN
                                                        UPDATE  #TransCodes
                                                        SET     DeferredRevenueAmount = TotalDeferredRevenue
                                                        WHERE   TransCodeId = @TransCodeId;										
                                                    END;	
                                                ELSE
                                                    BEGIN
                                                        UPDATE  #TransCodes
                                                        SET     DeferredRevenueAmount = TotalDeferredRevenue - AccruedAmount
                                                        WHERE   TransCodeId = @TransCodeId;
                                                    END;	
											
											--At this point we should populate the #DeferredRevenues table if the @PostDeferredRevenue was passed in as 1
                                                IF @Mode = 3
                                                    BEGIN
                                                        DECLARE @Amount AS DECIMAL(18,2);
													
                                                        IF @RemainingMethod = 1
                                                            BEGIN
                                                                SET @Amount = (
                                                                                SELECT  DeferredRevenue
                                                                                FROM    #Transactions
                                                                                WHERE   TransactionId = @TransactionId
                                                                              );												
                                                            END;
                                                        ELSE
                                                            BEGIN
															
                                                                IF (
                                                                     SELECT DeferredRevenue
                                                                     FROM   #Transactions
                                                                     WHERE  TransactionId = @TransactionId
                                                                   ) <> 0.0
                                                                    BEGIN
                                                                        SET @Amount = (
                                                                                        SELECT  DeferredRevenue
                                                                                        FROM    #Transactions
                                                                                        WHERE   TransactionId = @TransactionId
                                                                                      ) - (
                                                                                            SELECT  Earned
                                                                                            FROM    #Transactions
                                                                                            WHERE   TransactionId = @TransactionId
                                                                                          );
                                                                    END;
                                                                ELSE
                                                                    BEGIN
                                                                        SET @Amount = 0;
                                                                    END;
																
														
                                                            END;
													
													--Add row to the table only if amount is <> 0
                                                        IF @Amount <> 0
                                                            BEGIN
                                                                INSERT  INTO #DeferredRevenues
                                                                        (
                                                                         DefRevenueId
                                                                        ,DefRevenueDate
                                                                        ,TransactionId
                                                                        ,Type
                                                                        ,Source
                                                                        ,Amount
                                                                        ,IsPosted
                                                                        ,ModUser
                                                                        ,ModDate
                                                                        )
                                                                        SELECT  NEWID()
                                                                               , -- DefRevenueId - uniqueidentifier
                                                                                @ReportDate
                                                                               , -- DefRevenueDate - datetime
                                                                                @TransactionId
                                                                               , -- TransactionId - uniqueidentifier
                                                                                0
                                                                               , -- Type - bit
                                                                                1
                                                                               , -- Source - tinyint
                                                                                @Amount
                                                                               , -- Amount - money
                                                                                1
                                                                               , -- IsPosted - bit
                                                                                @User
                                                                               , -- ModUser - varchar(50)
                                                                                CONVERT(SMALLDATETIME,GETDATE());  -- ModDate - datetime													 
														
                                                            END;											
                                                    END;															 
                                            END;							
                                    END;						
                            END;
                    END;
                FETCH NEXT FROM transCursor
				INTO @TransactionId,@StudentName,@StudentIdentifier,@StuEnrollId,@TransCodeId,@TransAmount,@LOAStartDate,@LOAEndDate,@Earned,@FeeLevelId,
                    @TermStart,@TermEnd,@percentToEarnIdx,@RemainingMethod,@DeferredRevenue;

            END;

        CLOSE transCursor;
        DEALLOCATE transCursor;


        ALTER TABLE dbo.saDeferredRevenues DISABLE TRIGGER ALL;
        ALTER TABLE dbo.saDeferredRevenues NOCHECK CONSTRAINT ALL;  

        IF @Mode = 3
            BEGIN
                BEGIN TRAN;	
                INSERT  INTO dbo.saDeferredRevenues
                        SELECT  *
                        FROM    #DeferredRevenues WITH ( NOLOCK );	 
                COMMIT TRAN;
	
            END;


        ALTER TABLE dbo.saDeferredRevenues CHECK CONSTRAINT ALL; 

--When Mode is 1 (BuildList) or 3 (Post)
--If there are student exceptions then that table should be returned
--Else we should return the transcodes table
        IF (
             @Mode = 1
             OR @Mode = 3
           )
            BEGIN
                IF (
                     SELECT ISNULL(COUNT(*),0)
                     FROM   #StudentExceptions
                   ) > 0
                    BEGIN
                        SELECT  *
                        FROM    #StudentExceptions;	
                    END;
                ELSE
                    BEGIN
                        SELECT  *
                        FROM    #TransCodes;
                    END;	
            END;
	

--When Mode is 2 (BindTransCodeDetails)
--Return the #Transactions table
        IF @Mode = 2
            BEGIN
                SELECT  *
                FROM    #Transactions;
            END;




        DROP TABLE #Transactions;
        DROP TABLE #TransCodes;
        DROP TABLE #StuEnrollments;
        DROP TABLE #PrgVersions;
        DROP TABLE #TuitionEarnings;
        DROP TABLE #TuitionEarningsPercentageRanges;
        DROP TABLE #Results;
        DROP TABLE #Reqs;
        DROP TABLE #StudentExceptions;
        DROP TABLE #DeferredRevenues;
    END;
--=================================================================================================
-- END  --  USP_SA_GetDeferredRevenueResults
--=================================================================================================
GO

