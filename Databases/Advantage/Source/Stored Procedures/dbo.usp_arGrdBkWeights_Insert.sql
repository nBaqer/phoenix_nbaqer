SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/* 
DE10009 Mismatch between scores posted through Grades by student(Course level) and scores in transcript

CREATED: 
8/28/13 WP

PURPOSE: 
Insert arGrdBkWeights, clearing final scores where appropriate
	1. determine class sections affected by new grade book weight
	2. calculate existing effective date for each class section
	3. insert new grade book weight
	4. recalculate effective dates for each class section
	5. clear scores for class sections where the effective date has changed

MODIFIED:


*/

CREATE PROCEDURE [dbo].[usp_arGrdBkWeights_Insert]
    @InstrGrdBkWgtId UNIQUEIDENTIFIER
   ,@InstructorId UNIQUEIDENTIFIER = NULL
   ,@Descrip VARCHAR(50)
   ,@StatusFlag BIT = 1
   ,@ModUser VARCHAR(50)
   ,@ReqId UNIQUEIDENTIFIER
   ,@EffectiveDate DATETIME
AS
    BEGIN

        SET NOCOUNT ON;

        DECLARE @StatusId UNIQUEIDENTIFIER;		
        DECLARE @i INT; 
        DECLARE @count INT;
        DECLARE @selected UNIQUEIDENTIFIER;	
	
        IF @StatusFlag = 0
            SELECT  @StatusId = StatusId
            FROM    syStatuses
            WHERE   Status = 'Inactive';
        ELSE
            SELECT  @StatusId = StatusId
            FROM    syStatuses
            WHERE   Status = 'Active';
										
--	Create list of class sections affected by new grade book weight
        CREATE TABLE #DateList
            (
             Id INT IDENTITY(1,1)
            ,ClsSectionId UNIQUEIDENTIFIER
            ,OldEffectiveDate DATETIME
            ,NewEffectiveDate DATETIME
            );

		-- insert all class sections in selected course
        INSERT  INTO #DateList
                (
                 ClsSectionId
                )
                SELECT  ClsSectionId
                FROM    arClassSections
                WHERE   ReqId = @ReqId;

		-- calculate effective dates currently used for each class section in selected course
        SET @i = 1;			
        SET @count = (
                       SELECT   COUNT(*)
                       FROM     #DateList
                     );
        IF @count > 0
            WHILE ( @i <= @count )
                BEGIN
                    SET @selected = (
                                      SELECT    ClsSectionId
                                      FROM      #DateList
                                      WHERE     Id = @i
                                    );
                    UPDATE  #DateList
                    SET     OldEffectiveDate = (
                                                 SELECT TOP 1
                                                        gbw.EffectiveDate
                                                 FROM   arGrdBkWeights gbw
                                                 JOIN   arClassSections cs ON gbw.ReqId = cs.ReqId
                                                 WHERE  gbw.EffectiveDate <= cs.StartDate
                                                        AND cs.ClsSectionId = @selected
                                                 ORDER BY gbw.EffectiveDate DESC
                                               )
                    WHERE   Id = @i;
                    SET @i = @i + 1;	    	
                END;
		
-- Insert new grade book weight												
        INSERT  INTO arGrdBkWeights
                (
                 InstrGrdBkWgtId
                ,InstructorId
                ,Descrip
                ,StatusId
                ,ModUser
                ,ModDate
                ,ReqId
                ,EffectiveDate
                )
        VALUES  (
                 @InstrGrdBkWgtId
                ,@InstructorId
                ,@Descrip
                ,@StatusId
                ,@ModUser
                ,GETDATE()
                ,@ReqId
                ,@EffectiveDate
                );

		-- calculate effective dates to be used after new grade book weight for each class section in selected course
        SET @i = 1;	
        SET @count = (
                       SELECT   COUNT(*)
                       FROM     #DateList
                     );
        IF @count > 0
            WHILE ( @i <= @count )
                BEGIN
                    SET @selected = (
                                      SELECT    ClsSectionId
                                      FROM      #DateList
                                      WHERE     Id = @i
                                    );
                    UPDATE  #DateList
                    SET     NewEffectiveDate = (
                                                 SELECT TOP 1
                                                        gbw.EffectiveDate
                                                 FROM   arGrdBkWeights gbw
                                                 JOIN   arClassSections cs ON gbw.ReqId = cs.ReqId
                                                 WHERE  gbw.EffectiveDate <= cs.StartDate
                                                        AND cs.ClsSectionId = @selected
                                                 ORDER BY gbw.EffectiveDate DESC
                                               )
                    WHERE   Id = @i;
                    SET @i = @i + 1;	    	
                END;

--	Clear final scores for class sections affected by new grade book weight and return list of class sections
	
		--disable triggers that crash with bulk updates
        ALTER TABLE dbo.arResults DISABLE TRIGGER Trigger_arResults_IsClinicsSatisified;
        ALTER TABLE dbo.arResults DISABLE TRIGGER TR_InsertCreditSummary;

	-- update affected final scores
        UPDATE  arResults
        SET     IsCourseCompleted = 0
               ,Score = NULL
               ,GrdSysDetailId = NULL
        WHERE   TestId IN ( SELECT  ClsSectionId
                            FROM    #DateList
                            WHERE   ISNULL(OldEffectiveDate,'1/1/1900') <> ISNULL(NewEffectiveDate,'1/1/1900') );						
		--re-enable triggers 										
        ALTER TABLE dbo.arResults ENABLE TRIGGER Trigger_arResults_IsClinicsSatisified;
        ALTER TABLE dbo.arResults ENABLE TRIGGER TR_InsertCreditSummary;

	-- return list of class sections affected
        SELECT  dl.ClsSectionId
               ,ClsSection
        FROM    #DateList dl
        JOIN    arClassSections cs ON dl.ClsSectionId = cs.ClsSectionId
        WHERE   ISNULL(OldEffectiveDate,'1/1/1900') <> ISNULL(NewEffectiveDate,'1/1/1900');
						
        DROP TABLE #DateList;		
		
    END;	



---------------------------------------------------------------------------------------------------
-- DE10009 : Faculty / Edit Pending Punches page crashes when trying to get punches for the student
-- End QA: Mismatch between scores posted through Grades by student(Course level) and scores in transcript
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
-- DE9826 :  The program version definitions include course groups and also course equivalents. 
-- However, the SP is not taking these into account. It does a join against the syCreditSummary and arProgVerDef table
--We remove the join on arProgVerDef table
---------------------------------------------------------------------------------------------------



GO
