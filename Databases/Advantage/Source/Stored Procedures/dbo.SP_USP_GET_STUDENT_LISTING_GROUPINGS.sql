SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create PROCEDURE [dbo].[SP_USP_GET_STUDENT_LISTING_GROUPINGS]
AS
    DECLARE @EnrollmentCounts TABLE
        (
            StatusCodeId UNIQUEIDENTIFIER NULL
           ,TotalCount INT NULL
        );
    DECLARE @StudentListingGroups TABLE
        (
            LeadGrpId UNIQUEIDENTIFIER NULL
           ,Descrip VARCHAR(100) NULL
           ,StatusCodeDescrip VARCHAR(100) NULL
           ,StatusCodeId UNIQUEIDENTIFIER NULL
           ,EnrollmentCount INT NULL
           ,TotalCount INT NULL
        );
    INSERT INTO @EnrollmentCounts (
                                  StatusCodeId
                                 ,TotalCount
                                  )
                SELECT     codes.StatusCodeId
                          ,CAST(COUNT(enrollments.StuEnrollId) AS INTEGER) TotalCount
                FROM       dbo.syStatusCodes codes
                INNER JOIN dbo.sySysStatus statuses ON statuses.SysStatusId = codes.SysStatusId
                INNER JOIN dbo.syStatuses status ON status.StatusId = codes.StatusId
                INNER JOIN dbo.arStuEnrollments enrollments ON enrollments.StatusCodeId = codes.StatusCodeId
                WHERE      statuses.StatusLevelId = 2
                GROUP BY   codes.StatusCodeId
                          ,codes.StatusCodeDescrip;
    INSERT INTO @StudentListingGroups (
                                      LeadGrpId
                                     ,Descrip
                                     ,StatusCodeDescrip
                                     ,StatusCodeId
                                     ,EnrollmentCount
                                     ,TotalCount
                                      )
                SELECT     leadGroups.LeadGrpId
                          ,leadGroups.Descrip
                          ,codes.StatusCodeDescrip
                          ,codes.StatusCodeId
                          ,CAST(COUNT(studentGroups.StuEnrollId) AS INTEGER) AS EnrollmentCount
                          ,totalOfEnrollments.TotalCount
                FROM       dbo.adLeadByLeadGroups studentGroups
                INNER JOIN dbo.arStuEnrollments enrollments ON enrollments.StuEnrollId = studentGroups.StuEnrollId
                INNER JOIN dbo.adLeadGroups leadGroups ON leadGroups.LeadGrpId = studentGroups.LeadGrpId
                INNER JOIN dbo.syStatusCodes codes ON codes.StatusCodeId = enrollments.StatusCodeId
                INNER JOIN dbo.sySysStatus statuses ON statuses.SysStatusId = codes.SysStatusId
                INNER JOIN @EnrollmentCounts totalOfEnrollments ON totalOfEnrollments.StatusCodeId = codes.StatusCodeId
                WHERE      statuses.StatusLevelId = 2
                GROUP BY   leadGroups.LeadGrpId
                          ,leadGroups.Descrip
                          ,codes.StatusCodeDescrip
                          ,codes.StatusCodeId
                          ,totalOfEnrollments.TotalCount;

SELECT * FROM @StudentListingGroups
WHERE EnrollmentCount > 0
ORDER BY StatusCodeDescrip, Descrip
GO
