SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_AR_TransferCompletedComponentsByCourse]
    @NewClsSectionId UNIQUEIDENTIFIER
   ,@OldClsSectionId UNIQUEIDENTIFIER
   ,@StuEnrollId UNIQUEIDENTIFIER
   ,@User NVARCHAR(50)
AS
    BEGIN
        INSERT  INTO arGrdBkResults
                (
                 GrdBkResultId
                ,ClsSectionId
                ,InstrGrdBkWgtDetailId
                ,Score
                ,Comments
                ,StuEnrollId
                ,ModUser
                ,ModDate
                ,ResNum
                ,PostDate
                ,IsCompGraded
                ,isCourseCredited
                )
                SELECT  NEWID()
                       ,@NewClsSectionId
                       ,GR.InstrGrdBkWgtDetailId
                       ,GR.Score
                       ,Comments
                       ,GR.StuEnrollId
                       ,@User
                       ,GETDATE()
                       ,ResNum
                       ,PostDate
                       ,IsCompGraded
                       ,isCourseCredited
                FROM    arGrdBkResults GR
                INNER JOIN arResults R ON GR.StuEnrollId = R.StuEnrollId
                                          AND GR.ClsSectionId = R.TestId
                WHERE   GR.StuEnrollId = @StuEnrollId
                        AND R.TestId = @OldClsSectionId;

    END;




GO
