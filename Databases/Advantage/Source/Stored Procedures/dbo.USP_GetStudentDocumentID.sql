SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
 
CREATE PROCEDURE [dbo].[USP_GetStudentDocumentID]
    (
     @StudentID AS UNIQUEIDENTIFIER
    ,@DocumentID AS UNIQUEIDENTIFIER
    )
AS
    BEGIN

        SELECT  StudentDocID
        FROM    dbo.plStudentDocs
        WHERE   studentid = @StudentID
                AND DocumentId = @DocumentID;


    END;



GO
