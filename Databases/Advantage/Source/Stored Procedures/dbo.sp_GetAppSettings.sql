SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






        -- =============================================
        -- Author: Dennis Dreissigmayer
        -- Create date: 9/18/09
        -- Description: Retrieves the Key/Value information for Application Settings. 
        --
        -- This sp can be passed a CampusId to return the values for that campus AND any default settings if no 
		--campus specific setting exists. 
        -- Only 1 key value will be returned per key in this instance. This is used for loading App Settings on 
		--Startup based on the 
        -- Users assigned Campus.
        --
        -- If NO CampusId is passed, then all key/values will be returned, regardless of campus. This is used for 
		--the UI Grid.
        -- =============================================
CREATE PROCEDURE [dbo].[sp_GetAppSettings]
            --Set up the parameters
    @CampusId AS VARCHAR(50) = ''
AS
    BEGIN
            -- SET NOCOUNT ON added to prevent extra result sets from
            -- interfering with SELECT statements.
        SET NOCOUNT ON;
            
        IF @CampusId = ''
            BEGIN
                SELECT  V.ValueId
                       ,S.SettingId
                       ,COALESCE((
                                   SELECT   CampDescrip
                                   FROM     syCampuses
                                   WHERE    CampusId = V.CampusId
                                 ),'ALL') AS Campus
                       ,S.KeyName
                       ,V.Value
                       ,S.Description
                       ,S.CampusSpecific
                FROM    syConfigAppSettings S
                LEFT JOIN syConfigAppSetValues V ON S.SettingId = V.SettingId
                ORDER BY S.KeyName
                       ,COALESCE((
                                   SELECT   CampDescrip
                                   FROM     syCampuses
                                   WHERE    CampusId = V.CampusId
                                 ),'ALL');
            END;
        ELSE
            BEGIN
                DECLARE @SettingId AS INT;
                DECLARE @Campus AS VARCHAR(50);
                DECLARE @KeyName AS VARCHAR(100);
                DECLARE @Value AS VARCHAR(200);
                DECLARE @Description AS VARCHAR(2000);
                DECLARE @CashedSettingId AS INT;
                DECLARE @CampusDescription AS VARCHAR(50);
                DECLARE @ValueId AS UNIQUEIDENTIFIER;
                DECLARE @CampusSpecific AS BIT;

                SET @CampusDescription = (
                                           SELECT   CampDescrip
                                           FROM     syCampuses
                                           WHERE    CampusId = @CampusId
                                         );

                CREATE TABLE #TEMPSETTINGS
                    (
                     ValueId UNIQUEIDENTIFIER
                    ,SettingId INT
                    ,Campus VARCHAR(50)
                    ,KeyName VARCHAR(100)
                    ,Value VARCHAR(200)
                    ,Description VARCHAR(2000)
                    ,CampusSpecific BIT
                    );


                DECLARE GetAppSettings_cursor CURSOR
                FOR
                    SELECT  V.ValueId
                           ,S.SettingId
                           ,COALESCE((
                                       SELECT   CampDescrip
                                       FROM     syCampuses
                                       WHERE    CampusId = V.CampusId
                                     ),'ALL') AS Campus
                           ,S.KeyName
                           ,V.Value
                           ,S.Description
                           ,S.CampusSpecific
                    FROM    syConfigAppSettings S
                    LEFT JOIN syConfigAppSetValues V ON S.SettingId = V.SettingId
                    ORDER BY S.KeyName
                           ,COALESCE((
                                       SELECT   CampDescrip
                                       FROM     syCampuses
                                       WHERE    CampusId = V.CampusId
                                     ),'ALL');
                            --Order By S.SettingId


                OPEN GetAppSettings_cursor;

                FETCH NEXT FROM GetAppSettings_cursor
                    INTO @ValueId,@SettingId,@Campus,@KeyName,@Value,@Description,@CampusSpecific;

                WHILE @@FETCH_STATUS = 0
                    BEGIN

                        IF @CampusId <> ''
                            BEGIN
                                IF (
                                     SELECT COUNT(SettingId)
                                     FROM   syConfigAppSetValues
                                     WHERE  SettingId = @SettingId
                                            AND CampusId = @CampusId
                                   ) > 0
                                    BEGIN
                                        IF @Campus = @CampusDescription
                                            BEGIN
                                                INSERT  INTO #TEMPSETTINGS
                                                VALUES  ( @ValueId,@SettingId,@CampusDescription,@KeyName,@Value,@Description,@CampusSpecific );
                                            END;
                                    END;
                                ELSE
                                    BEGIN
                                        IF @Campus = 'ALL'
                                            BEGIN
                                                INSERT  INTO #TEMPSETTINGS
                                                VALUES  ( @ValueId,@SettingId,@Campus,@KeyName,@Value,@Description,@CampusSpecific );
                                            END;
                                    END;
                            END;

                        FETCH NEXT FROM GetAppSettings_cursor
                    INTO @ValueId,@SettingId,@Campus,@KeyName,@Value,@Description,@CampusSpecific;

                    END;

                CLOSE GetAppSettings_cursor;
                DEALLOCATE GetAppSettings_cursor;


                SELECT  *
                FROM    #TEMPSETTINGS
                ORDER BY KeyName
                       ,Campus;


                DROP TABLE #TEMPSETTINGS;
            END;

    END; 





/****** Object:  StoredProcedure [dbo].[sp_RetrieveCampuses]    Script Date: 07/16/2010 11:51:03 ******/
    SET ANSI_NULLS ON;



GO
