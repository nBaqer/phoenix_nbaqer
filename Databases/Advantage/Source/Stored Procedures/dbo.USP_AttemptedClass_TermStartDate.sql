SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_AttemptedClass_TermStartDate]
    @StuEnrollId UNIQUEIDENTIFIER
   ,@ClsSectionId UNIQUEIDENTIFIER
AS
    BEGIN
        SELECT TOP 1
                TermStartDate
        FROM    (
                  SELECT TOP 1
                            Term.Startdate AS TermStartDate
                  FROM      arresults results
                  INNER JOIN arClassSections Class ON results.testid = Class.ClsSectionId
                  INNER JOIN arTerm Term ON Class.TermId = Term.TermId
                  WHERE     results.stuenrollid = @StuEnrollId
                            AND results.TestId = @ClsSectionId
                  UNION
                  SELECT TOP 1
                            Term.startDate AS termStartDate
                  FROM      arTransferGrades t9
                  INNER JOIN arTerm Term ON t9.TermId = Term.termId
                  WHERE     t9.StuEnrollId = @StuEnrollId
                            AND t9.ReqId IN ( SELECT    ReqId
                                              FROM      arClassSections
                                              WHERE     ClsSectionId = @ClsSectionId )
                ) tblDerived;
    END;



GO
