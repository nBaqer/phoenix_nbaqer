SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_getStudentScheduleByTermClasses]
    (
     @StudentID AS UNIQUEIDENTIFIER
    ,@StuEnrollID AS UNIQUEIDENTIFIER
    ,@TermID AS UNIQUEIDENTIFIER  
    )
AS /*----------------------------------------------------------------------------------------------------  
    Author          :	Janet Robinson  
      
    Create date		:	06/22/2011  
      
    Procedure Name	:	[USP_getStudentScheduleByTermClasses]  
      
    Objective		:	Gets the Student Schedule Classes by Term for selected student  
      
    Parameters		:	Name			Type	Data Type			Required?  
    =====			====	=========			=========  
    @StudentID      Input   UNIQUEIDENTIFIER        Y  
    @StuEnrollID    Input   UNIQUEIDENTIFIER        Y  
    @TermID         Input   UNIQUEIDENTIFIER        Y  
      
      
      
    Output			:	Returns 1 dataset - class info  
      
    */----------------------------------------------------------------------------------------------------  
    BEGIN  
      
      
        DECLARE @Tab1 TABLE ( TestId VARCHAR(100) );  
      
      
        BEGIN  
      
    -- Get all testids for the student to retrieve the classes  
            INSERT  INTO @Tab1
                    SELECT DISTINCT
                            CAST(arResults.TestId AS VARCHAR(100)) AS TestId
                    FROM    arResults
                    INNER JOIN arStuEnrollments ON arResults.StuEnrollId = arStuEnrollments.StuEnrollId
                    INNER JOIN arStudent ON arStuEnrollments.StudentId = arStudent.StudentId
                    INNER JOIN arClassSections ON arResults.TestId = arClassSections.ClsSectionId
                    INNER JOIN arClassSectionTerms ON arClassSections.ClsSectionId = arClassSectionTerms.ClsSectionId
                    INNER JOIN arTerm ON arClassSectionTerms.TermId = arTerm.TermId
                    INNER JOIN arReqs ON arClassSections.ReqId = arReqs.ReqId
                    INNER JOIN syCmpGrpCmps ON arClassSections.CampusId = syCmpGrpCmps.CampusId
                    INNER JOIN syCampGrps ON syCmpGrpCmps.CampGrpId = syCampGrps.CampGrpId
                    LEFT OUTER JOIN arClsSectMeetings ON arResults.TestId = arClsSectMeetings.ClsSectionId
                    WHERE   arStudent.StudentId = @StudentID
                            AND arTerm.Termid = @TermID
                            AND arStuEnrollments.StuEnRollId = @StuEnrollID
                    ORDER BY TestId;  
      
    -- Get classes for the student for the term selected  
            SELECT  A.ClsSectionId
                   ,B.Descrip AS RoomDescrip
                   ,WorkDaysDescrip = ( CASE WHEN ( A.TimeIntervalId IS NOT NULL ) THEN (
                                                                                          SELECT    WorkDaysDescrip
                                                                                          FROM      plWorkDays
                                                                                          WHERE     WorkDaysId = A.WorkDaysId
                                                                                        )
                                             ELSE (
                                                    SELECT  PeriodDescrip
                                                    FROM    syPeriods
                                                    WHERE   PeriodId = A.PeriodId
                                                  )
                                        END )
                   ,TimeIn = ( CASE WHEN ( A.TimeIntervalId IS NOT NULL ) THEN (
                                                                                 SELECT TimeIntervalDescrip
                                                                                 FROM   cmTimeInterval
                                                                                 WHERE  TimeIntervalId = A.TimeIntervalId
                                                                               )
                                    ELSE (
                                           SELECT   TimeIntervalDescrip
                                           FROM     syPeriods
                                                   ,cmTimeInterval
                                           WHERE    PeriodId = A.PeriodId
                                                    AND StartTimeId = TimeIntervalId
                                         )
                               END )
                   ,TimeOut = ( CASE WHEN ( A.TimeIntervalId IS NOT NULL ) THEN (
                                                                                  SELECT    TimeIntervalDescrip
                                                                                  FROM      cmTimeInterval
                                                                                  WHERE     TimeIntervalId = A.EndIntervalId
                                                                                )
                                     ELSE (
                                            SELECT  TimeIntervalDescrip
                                            FROM    syPeriods
                                                   ,cmTimeInterval
                                            WHERE   PeriodId = A.PeriodId
                                                    AND EndTimeId = TimeIntervalId
                                          )
                                END )
                   ,MeetingType = ( CASE WHEN ( A.TimeIntervalId IS NOT NULL ) THEN ( 0 )
                                         ELSE ( 1 )
                                    END )
                   ,ViewOrder = ( CASE WHEN ( A.TimeIntervalId IS NOT NULL ) THEN (
                                                                                    SELECT  ViewOrder
                                                                                    FROM    plWorkDays
                                                                                    WHERE   WorkDaysId = A.WorkDaysId
                                                                                  )
                                       ELSE ( 0 )
                                  END )
                   ,A.StartDate AS ClassStartDate
                   ,A.EndDate AS ClassEndDate
            FROM    arClsSectMeetings A
                   ,arRooms B
            WHERE   A.RoomId = B.RoomID
                    AND A.ClsSectionId IN ( SELECT  TestId
                                            FROM    @Tab1 )
            UNION
            SELECT  A.ClsSectionId
                   ,B.Descrip AS RoomDescrip
                   ,WorkDaysDescrip = (
                                        SELECT  PeriodDescrip
                                        FROM    syPeriods
                                        WHERE   PeriodId = A.AltPeriodId
                                      )
                   ,TimeIn = (
                               SELECT   TimeIntervalDescrip
                               FROM     syPeriods
                                       ,cmTimeInterval
                               WHERE    PeriodId = A.AltPeriodId
                                        AND StartTimeId = TimeIntervalId
                             )
                   ,TimeOut = (
                                SELECT  TimeIntervalDescrip
                                FROM    syPeriods
                                       ,cmTimeInterval
                                WHERE   PeriodId = A.AltPeriodId
                                        AND EndTimeId = TimeIntervalId
                              )
                   ,MeetingType = ( 1 )
                   ,ViewOrder = ( CASE WHEN ( A.TimeIntervalId IS NOT NULL ) THEN (
                                                                                    SELECT  ViewOrder
                                                                                    FROM    plWorkDays
                                                                                    WHERE   WorkDaysId = A.WorkDaysId
                                                                                  )
                                       ELSE ( 0 )
                                  END )
                   ,A.StartDate AS ClassStartDate
                   ,A.EndDate AS ClassEndDate
            FROM    arClsSectMeetings A
                   ,arRooms B
            WHERE   A.RoomId = B.RoomID
                    AND A.AltPeriodId IS NOT NULL
                    AND A.ClsSectionId IN ( SELECT  TestId
                                            FROM    @Tab1 )
                    AND EXISTS ( SELECT *
                                 FROM   syPeriods
                                 WHERE  PeriodId = A.AltPeriodId )
            ORDER BY ViewOrder
                   ,ClassStartDate
                   ,TimeIn;  
      
        END;  
      
    END;  
      
      
      



GO
