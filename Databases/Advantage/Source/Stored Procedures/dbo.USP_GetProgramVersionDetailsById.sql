SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_GetProgramVersionDetailsById]
    (
        @PrgVerId UNIQUEIDENTIFIER
       ,@CampusId UNIQUEIDENTIFIER
    )
AS
    SET NOCOUNT ON;
    BEGIN
        SELECT     DISTINCT programVersion.PrgVerId
                           ,programVersion.PrgVerDescrip AS PrgVerDescrip
                           ,programVersion.StatusId
                           ,campusProgramVersion.IsTitleIV
                           ,campusProgramVersion.IsFAMEApproved
                           ,campusProgramVersion.AllowExcusAbsPerPayPrd
                           ,campusProgramVersion.TermSubEqualInLen
                           ,campusProgramVersion.CalculationPeriodTypeId
                           ,systemStatus.Status
                           ,programVersion.CampGrpId
                           ,programVersion.ProgramRegistrationType
						   ,programVersion.ThGrdScaleId AS GradeScaleId
        FROM       dbo.arPrgVersions programVersion
        INNER JOIN dbo.syStatuses systemStatus ON systemStatus.StatusId = programVersion.StatusId
		LEFT JOIN dbo.arCampusPrgVersions campusProgramVersion ON programVersion.PrgVerId = campusProgramVersion.PrgVerId AND campusProgramVersion.CampusId = @CampusId
        WHERE      programVersion.PrgVerId = @PrgVerId
                   AND systemStatus.StatusCode = 'A'
        ORDER BY   PrgVerDescrip;
    END;


GO
