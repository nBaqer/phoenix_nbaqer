SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[USP_IL_AddImportLeadsMappingInfo]
    @MappingId UNIQUEIDENTIFIER
   ,@MapName NVARCHAR(200)
   ,@User NVARCHAR(50)
AS
    DECLARE @StatusId UNIQUEIDENTIFIER;
    DECLARE @CampGrpId UNIQUEIDENTIFIER;
    DECLARE @UserId UNIQUEIDENTIFIER;
    DECLARE @CreateDate AS DATETIME;
    DECLARE @OldMappingId AS UNIQUEIDENTIFIER;
	
    SET @StatusId = (
                      SELECT    StatusId
                      FROM      syStatuses
                      WHERE     Status = 'Active'
                    );
    SET @CampGrpId = (
                       SELECT   CampGrpId
                       FROM     syCampGrps
                       WHERE    CampGrpDescrip = 'all'
                     ); 
    SET @UserId = (
                    SELECT  UserId
                    FROM    syUsers
                    WHERE   UserName = @User
                  );
    SET @CreateDate = CONVERT(VARCHAR(2),DATEPART(M,GETDATE())) + '/' + CONVERT(VARCHAR(2),DATEPART(D,GETDATE())) + '/' + CONVERT(VARCHAR(4),DATEPART(YYYY,
                                                                                                                                                GETDATE()))
        + ' ' + CONVERT(VARCHAR(2),DATEPART(hh,GETDATE())) + ':' + CONVERT(VARCHAR(2),DATEPART(mi,GETDATE())) + ':' + CONVERT(VARCHAR(2),DATEPART(s,GETDATE()));


	--If the mapping already exists then we need to remove it and all the related child records.
    IF EXISTS ( SELECT  MappingId
                FROM    adImportLeadsMappings
                WHERE   MapName = @MapName )
        BEGIN
            SET @OldMappingId = (
                                  SELECT    MappingId
                                  FROM      adImportLeadsMappings
                                  WHERE     MapName = @MapName
                                );
            DELETE  FROM adImportLeadsAdvFldMappings
            WHERE   MappingId = @OldMappingId;
            DELETE  FROM adImportLeadsLookupValsMap
            WHERE   MappingId = @OldMappingId;
            DELETE  FROM adImportLeadsMappings
            WHERE   MappingId = @OldMappingId;
					
        END;
		
	--Insert the parent record
    INSERT  INTO adImportLeadsMappings
            (
             MappingId
            ,MapName
            ,StatusId
            ,CampGrpId
            ,ModUser
            ,ModDate
			)
            SELECT  @MappingId AS MappingId
                   ,@MapName AS MapName
                   ,@StatusId AS StatusId
                   ,@CampGrpId AS CampGrpId
                   ,@User AS ModUser
                   ,@CreateDate AS ModDate;         
	





GO
