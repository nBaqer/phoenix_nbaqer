SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jose Torres
-- Create date: 11/20/2014
-- Description: Get List of active Program Versions that are attached a Billing Method 
--              Also get if a Payment Period of the BillinMethod is al being used to charge students
--              if it is true the Billing Method should not deatach from the Program Version
--                            the Payment Period could not be update or deleted
-- EXECUTE USP_PrgVersionsGetListActiveAvailableForABillingMethod    @BillingMethodId = N'F335D044-F05B-4F4F-8590-39552C6FDC3F'              
--                 
-- =============================================

/*
IncrementType
	<Description("Not Apply")>         NotApply         = -1
	<Description("Actual Hours")>      ActualHours      = 0
	<Description("Scheduled Hours")>   ScheduledHours   = 1
	<Description("Credits Attempted")> CreditsAttempted = 2
	<Description("Credits Earned")>    CreditsEarned    = 3
*/

CREATE PROCEDURE [dbo].[USP_PrgVersionsGetListActiveAvailableForABillingMethod]
    @BillingMethodId NVARCHAR(50)
AS
    DECLARE @BillingMethod AS INTEGER;
    DECLARE @IncrementType AS INTEGER;

    BEGIN   

        SELECT  @BillingMethod = SBM.BillingMethod
        FROM    dbo.saBillingMethods AS SBM
        WHERE   SBM.BillingMethodId = @BillingMethodId;
 
        SELECT  @IncrementType = SI.IncrementType
        FROM    dbo.saIncrements AS SI
        WHERE   SI.BillingMethodId = @BillingMethodId;
 
        SELECT DISTINCT
                APV.PrgVerId
               ,APV.PrgVerDescrip AS PrgVerDescrip
               ,CASE WHEN APV.IsBillingMethodCharged = 1 THEN 'True'
                     ELSE 'False'
                END AS IsBillingMethodCharged
               ,IsUsed = 'False'
        FROM    arPrgVersions AS APV
        INNER JOIN syStatuses AS SS ON APV.StatusId = SS.StatusId
        INNER JOIN arAttUnitType AS AAT ON APV.UnitTypeId = AAT.UnitTypeId
        WHERE   SS.StatusCode = 'A'
                AND (
                      (
                        @BillingMethod <> 3
                        AND ISNULL(@IncrementType,-1) = -1
                      )
                      OR (
                           @BillingMethod = 3
                           AND ISNULL(@IncrementType,-1) IN ( 0,1 )
                           AND AAT.UnitTypeDescrip = 'Clock Hours'    --WHEN ISNULL(SI.IncrementType, -1) IN (0,1)  AND  APV.HOURS > 0 
                         )
                      OR (
                           @BillingMethod = 3
                           AND ISNULL(@IncrementType,-1) IN ( 2,3 )
                           AND APV.Credits > 0
                         )
                    )
                AND ISNULL(APV.BillingMethodId,@BillingMethodId) = @BillingMethodId
        ORDER BY APV.PrgVerDescrip; 


    END;




GO
