SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_PassingGradeFromPrgVersion]
    (
     @prgVerId UNIQUEIDENTIFIER
    ,@grade VARCHAR(100)
    )
AS
    SET NOCOUNT ON;
    SELECT  IsPass
    FROM    arGradeSystemDetails
    INNER JOIN arPrgVersions ON arGradeSystemDetails.GrdSystemId = arPrgVersions.GrdSystemId
                                AND arGradeSystemDetails.Grade = @grade; 



GO
