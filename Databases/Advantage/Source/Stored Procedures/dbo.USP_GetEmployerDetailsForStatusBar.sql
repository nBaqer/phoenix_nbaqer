SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
----------------------------------------------------------------------------------------------------------
--DE7587  Balaji Date 5/26/2012   End
----------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------
--DE7585  Balaji Date 5/26/2012   Start
----------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[USP_GetEmployerDetailsForStatusBar]
    @EmployerId UNIQUEIDENTIFIER
AS
    SELECT  S.EmployerDescrip AS Name
           ,S.EmployerId AS EmployerId
           ,S.Address1 AS Address1
           ,S.Address2 AS Address2
           ,S.City AS City
           ,S.StateId AS StateId
           ,(
              SELECT    StateDescrip
              FROM      syStates
              WHERE     StateId = S.StateId
            ) AS StateDescrip
           ,(
              SELECT TOP 1
                        t1.CampusId
              FROM      syCmpGrpCmps t1
                       ,syCampuses t2
              WHERE     t1.CampusId = t2.CampusId
                        AND t1.CampGrpId = S.CampGrpId
                        AND t2.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
              ORDER BY  t2.CampDescrip
            ) AS CampusId
           ,CG.CampGrpdescrip
           ,S.Zip
           ,S.Phone
    FROM    plEmployers S
    INNER JOIN syCampGrps CG ON S.CampGrpId = CG.CampGrpId
    WHERE   S.EmployerId = @EmployerId;



GO
