SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_IsStudentEligibleForRegistration_Count]
    @StuEnrollId UNIQUEIDENTIFIER
   ,@ClsSectionId UNIQUEIDENTIFIER
AS
    DECLARE @intStudentAlreadyRegistered INT
       ,@intStudentAlreadyGraded INT
       ,@intStudentFailedorDropped INT;
    SET @intStudentAlreadyRegistered = (
                                         SELECT COUNT(t1.ResultId) AS isStudentAlreadyResgistered
                                         FROM   arResults t1
                                               ,dbo.arClassSections t2
                                               ,arTerm t3
                                               ,arReqs t4
                                         WHERE  t1.TestId = t2.ClsSectionId
                                                AND t2.ReqId = t4.ReqId
                                                AND t2.TermId = t3.TermId
                                                AND t1.StuEnrollId = @StuEnrollId
                                                AND t4.ReqId IN ( SELECT DISTINCT
                                                                            ReqId
                                                                  FROM      arClassSections
                                                                  WHERE     ClsSectionId = @ClsSectionId )
                                       );
	
    IF @intStudentAlreadyRegistered >= 1  -- If student was already registered for this course, check if he failed or dropped
        BEGIN
		-- Check if student was already graded
            SET @intStudentAlreadyGraded = (
                                             SELECT COUNT(t1.ResultId) AS isStudentAlreadyResgistered
                                             FROM   arResults t1
                                                   ,dbo.arClassSections t2
                                                   ,arTerm t3
                                                   ,arReqs t4
                                             WHERE  t1.TestId = t2.ClsSectionId
                                                    AND t2.ReqId = t4.ReqId
                                                    AND t2.TermId = t3.TermId
                                                    AND t1.StuEnrollId = @StuEnrollId
                                                    AND t4.ReqId IN ( SELECT DISTINCT
                                                                                ReqId
                                                                      FROM      arClassSections
                                                                      WHERE     ClsSectionId = @ClsSectionId )
                                                    AND (
                                                          t1.Score IS NOT NULL
                                                          OR t1.GrdSysDetailId IS NOT NULL
                                                        )
                                           );
										
            IF @intStudentAlreadyGraded >= 1 -- Student already graded
                BEGIN
                    SET @intStudentFailedorDropped = (
                                                       SELECT   COUNT(t1.ResultId) AS isStudentEligibleForRegistration
                                                       FROM     arResults t1
                                                               ,dbo.arClassSections t2
                                                               ,arTerm t3
                                                               ,arReqs t4
                                                               ,dbo.arGradeSystemDetails t5
                                                       WHERE    t1.TestId = t2.ClsSectionId
                                                                AND t2.ReqId = t4.ReqId
                                                                AND t2.TermId = t3.TermId
                                                                AND t1.GrdSysDetailId = t5.GrdSysDetailId
                                                                AND t1.StuEnrollId = @StuEnrollId
                                                                AND t4.ReqId IN ( SELECT DISTINCT
                                                                                            ReqId
                                                                                  FROM      arClassSections
                                                                                  WHERE     ClsSectionId = @ClsSectionId )
                                                                AND (
                                                                      IsPass = 0
                                                                      OR IsDrop = 1
                                                                    )
                                                     ); -- Student should have failed or dropped the course
										  
                    IF @intStudentFailedorDropped >= 1  -- Student failed or dropped in an earlier attempt
                        BEGIN
                            SELECT  'Yes' AS IsStudentEligibleForRegistration;
                        END; 
                    ELSE
                        BEGIN
                            SELECT  'No' AS IsStudentEligibleForRegistration; -- Student passed in earlier attempt
                        END; 
                END; 
            ELSE
                BEGIN --Student not graded, so don't register
                    SELECT  'No' AS IsStudentEligibleForRegistration;
                END; 
        END; 
    ELSE
        BEGIN
            SELECT  'Yes' AS IsStudentEligibleForRegistration;
        END; 




GO
