SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_Classes_GetList]
    @StartDate DATETIME
   ,@EndDate DATETIME
   ,@CampusId VARCHAR(50)
   ,@InstructorId VARCHAR(50) = NULL
   ,@CourseId VARCHAR(50) = NULL
   ,@TermId VARCHAR(50) = NULL
AS -- Classes should have started before the class end date and ended after class start date
    SELECT  b.ClsSectionId
           ,b.StartDate
           ,b.EndDate
           ,b.TermId
           ,b.ReqId
           ,b.InstructorId
           ,b.ClsSection
           ,c.Code
           ,c.Descrip
           ,d.TermDescrip
           ,(
              SELECT    FullName
              FROM      syUsers
              WHERE     UserId = b.InstructorId
            ) AS FullName
    FROM    arClassSections b
    INNER JOIN arReqs c ON b.ReqId = c.ReqId
    INNER JOIN arTerm d ON b.TermId = d.TermId
    INNER JOIN syStatuses e ON d.StatusId = e.StatusId
    WHERE   b.EndDate >= @StartDate
            AND b.StartDate <= @EndDate
            AND e.Status = 'Active'
            AND b.CampusId = @CampusId
            AND (
                  @InstructorId IS NULL
                  OR b.InstructorId = @InstructorId
                )
            AND (
                  @CourseId IS NULL
                  OR c.ReqId = @CourseId
                )
            AND (
                  @TermId IS NULL
                  OR d.TermId = @TermId
                );



GO
