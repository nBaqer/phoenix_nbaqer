SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[usp_GetCorporateInfoFromCampusId]
    (
     @campusId UNIQUEIDENTIFIER
    )
AS
    SET NOCOUNT ON;

    SELECT  CampDescrip AS CorporateName
           ,Address1
           ,Address2
           ,City
           ,Zip
           ,TranscriptAuthZnTitle
           ,TranscriptAuthZnName
           ,Website
           ,CampDescrip
           ,(
              SELECT    StateDescrip
              FROM      syStates
              WHERE     StateId = C.StateId
            ) AS State
           ,(
              SELECT    CountryDescrip
              FROM      adCountries
              WHERE     CountryId = C.CountryId
            ) AS Country
           ,Fax
           ,Phone = CASE WHEN ( Phone1 IS NULL ) THEN ( CASE WHEN ( Phone2 IS NULL ) THEN ( CASE WHEN ( Phone3 IS NULL ) THEN ''
                                                                                                 ELSE Phone3
                                                                                            END )
                                                             ELSE Phone2
                                                        END )
                         ELSE Phone1
                    END
           ,(
              SELECT    COUNT(*) AS cnt
              FROM      arPrograms
                       ,syAcademicCalendars
              WHERE     arPrograms.ACId = syAcademicCalendars.ACId
                        AND LOWER(syAcademicCalendars.ACDescrip) = 'clock hour'
            ) AS clockHour
    FROM    syCampuses C
    WHERE   CampusId = @campusId;




GO
