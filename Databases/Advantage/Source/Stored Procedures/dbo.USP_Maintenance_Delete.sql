SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
----------------------------------------------------------------------------------------
-- USP_Maintenance_Delete
----------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[USP_Maintenance_Delete]
    @pResourceId INT
   ,@field1 VARCHAR(50)
AS
    DECLARE @ResourceId INT
       ,@SummListName VARCHAR(50)
       ,@TableName VARCHAR(50)
       ,@DisplayText VARCHAR(50)
       ,@DisplayValue VARCHAR(50);
    DECLARE @strSQL NVARCHAR(1000)
       ,@FilterStatusId UNIQUEIDENTIFIER
       ,@SortOrder INT; 
    DECLARE @ResDefId INT
       ,@TblFldsId INT
       ,@FldId INT
       ,@TblName VARCHAR(50)
       ,@Required INT;
    DECLARE @DDLId INT
       ,@FldLen INT
       ,@FldType VARCHAR(50)
       ,@TblPK INT
       ,@Caption VARCHAR(50)
       ,@OrderBy VARCHAR(50);
    DECLARE @FldName VARCHAR(500)
       ,@DynamicFieldName VARCHAR(1000)
       ,@DisplayValueField VARCHAR(50)
       ,@DynamicHeaderFields VARCHAR(1000);
    DECLARE @intLoop INT
       ,@DynamicFieldNameVariable VARCHAR(1000)
       ,@DataType VARCHAR(1000);
    DECLARE @FldCaption VARCHAR(50);
    DECLARE @DegCertSeekingId UNIQUEIDENTIFIER
       ,@Code VARCHAR(50)
       ,@Descrip VARCHAR(50)
       ,@StatusId UNIQUEIDENTIFIER
       ,@CampGrpId UNIQUEIDENTIFIER;
    DECLARE contact_cursor CURSOR
    FOR
        SELECT  t1.ResDefId
               ,t2.TblFldsId
               ,t2.FldId
               ,t5.TblName
               ,t3.FldName
               ,t1.Required
               ,t3.DDLId
               ,t3.FldLen
               ,t4.FldType
               ,t5.TblPK
               ,t6.Caption
               ,CASE WHEN t2.FldId = t5.TblPK THEN 1 -- Need this as the order of fields in syResTblFlds is inconsistent between pages
                     ELSE CASE WHEN CHARINDEX('code',t3.FldName) > 0 THEN 2 -- Code 
                               ELSE CASE WHEN CHARINDEX('desc',t3.FldName) > 0
                                              OR CHARINDEX('name',t3.FldName) > 0 THEN 3 -- Descrip 
                                         ELSE CASE WHEN CHARINDEX('statusid',t3.FldName) > 0 THEN 4 -- StatusId 
                                                   ELSE CASE WHEN CHARINDEX('status',t3.FldName) > 0 THEN 5 -- Status 
                                                             ELSE CASE WHEN CHARINDEX('campgrpid',t3.FldName) > 0 THEN 6 -- CampGrpId 
                                                                       ELSE 7
                                                                  END
                                                        END
                                              END
                                    END
                          END
                END AS SortOrder
        FROM    syResTblFlds t1
               ,syTblFlds t2
               ,syFields t3
               ,syFieldTypes t4
               ,syTables t5
               ,syFldCaptions t6
               ,syLangs t7
        WHERE   t1.TblFldsId = t2.TblFldsId
                AND t2.FldId = t3.FldId
                AND t2.TblId = t5.TblId
                AND t3.FldTypeId = t4.FldTypeId
                AND t2.FldId = t6.FldId
                AND t6.LangId = t7.LangId
                AND t1.ResourceId = @pResourceId
					--AND t7.LangName=?
ORDER BY        SortOrder; 
    OPEN contact_cursor;

-- Perform the first fetch.
    SET @intLoop = 1;
    FETCH NEXT FROM contact_cursor INTO @ResDefId,@TblFldsId,@FldId,@TblName,@DisplayValueField,@Required,@DDLId,@FldLen,@FldType,@TblPK,@Caption,@SortOrder;

    SET @DynamicFieldName = '';
    SET @DynamicFieldNameVariable = '';
    SET @DataType = '';
-- Check @@FETCH_STATUS to see if there are any more rows to fetch.
    WHILE @@FETCH_STATUS = 0
        BEGIN
            IF @SortOrder = 1
                BEGIN
                    SET @DynamicFieldName = @DisplayValueField;
                END;
	
            FETCH NEXT FROM contact_cursor INTO @ResDefId,@TblFldsId,@FldId,@TblName,@DisplayValueField,@Required,@DDLId,@FldLen,@FldType,@TblPK,@Caption,
                @SortOrder;
            SET @intLoop = @intLoop + 1; 
        END;
    SET @TableName = @TblName; -- Get the Table Name

-- Add Mod User and ModDate
    SET @strSQL = 'DELETE FROM  ' + @TableName + ' where ' + @DynamicFieldName + '=' + '''' + @field1 + '''';
    EXEC (@strSQL);
    CLOSE contact_cursor;
    DEALLOCATE contact_cursor;
	
	



GO
