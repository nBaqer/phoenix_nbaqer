SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[USP_IL_GetSavedMappingsWithNumberOfFields]
    @NumberOfFields INT
   ,@CampusId UNIQUEIDENTIFIER
AS
    SELECT  lm.MappingId
           ,lm.MapName
           ,COUNT(*)
    FROM    adImportLeadsMappings lm
           ,adImportLeadsAdvFldMappings fm
           ,syStatuses s
    WHERE   lm.MappingId = fm.MappingId
            AND lm.StatusId = s.StatusId
            AND s.Status = 'Active'
            AND (
                  lm.CampGrpId = (
                                   SELECT   CampGrpId
                                   FROM     syCampGrps
                                   WHERE    CampGrpDescrip = 'All'
                                 )
                  OR (
                       SELECT   COUNT(*)
                       FROM     syCmpGrpCmps cgc
                       WHERE    cgc.CampGrpId = lm.CampGrpId
                                AND cgc.CampusId = @CampusId
                     ) > 0
                )
    GROUP BY lm.MappingId
           ,lm.MapName
    HAVING  COUNT(*) = @NumberOfFields;






GO
