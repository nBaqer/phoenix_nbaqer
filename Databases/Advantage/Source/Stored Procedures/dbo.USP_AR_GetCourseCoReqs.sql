SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[USP_AR_GetCourseCoReqs]
    (
     @ReqID UNIQUEIDENTIFIER
    )
AS /*----------------------------------------------------------------------------------------------------
	Author          :	Saraswathi Lakshmanan
    
    Create date		:	02/03/2010
    
	Procedure Name	:	USP_AR_GetCourseCoReqs

	Objective		:	get the Course CoReqs
	
	Parameters		:	Name			Type	Data Type			Required? 	
						=====			====	=========			=========	
						@ReqID			In		UniqueIdentifier	Required
	
	Output			:	Returns the rowcount					
						
*/-----------------------------------------------------------------------------------------------------

    BEGIN
	
        SELECT  a.PreCoReqId
               ,b.descrip
        FROM    arCourseReqs a
        INNER JOIN arReqs b ON a.PreCoReqId = b.ReqId
        WHERE   a.CourseReqTypId = 2
                AND a.ReqId = @ReqID;
    END;



GO
