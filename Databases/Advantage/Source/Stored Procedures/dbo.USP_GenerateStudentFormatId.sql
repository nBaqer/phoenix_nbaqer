SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_GenerateStudentFormatId]
AS
    BEGIN
        SET NOCOUNT ON; 
        DECLARE @stuId INT;
        SET @stuId = (
                       SELECT   MAX(Student_SeqId) AS Student_SeqID
                       FROM     syGenerateStudentFormatID
                     ) + 1;
        INSERT  INTO syGenerateStudentFormatID
                ( Student_SeqId,ModDate )
        VALUES  ( @stuId,GETDATE() );
        SELECT  @stuId AS SeqId;
    END;	
GO
