SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[USP_IL_GetAdvFileFldMappings] @MappingId VARCHAR(50)
AS
    SELECT  fm.FieldNumber
           ,lf.FldName
    FROM    adImportLeadsAdvFldMappings fm
    INNER JOIN adImportLeadsFields lf ON fm.ILFieldId = lf.ILFieldId
    WHERE   fm.MappingId = @MappingId
    ORDER BY fm.FieldNumber;	







GO
