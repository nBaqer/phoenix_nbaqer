SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[USP_AR_DefaultCharges_Update]
    (
     @PrgVerPmtId UNIQUEIDENTIFIER
    ,@PrgVerID UNIQUEIDENTIFIER
    ,@FeeLevelID TINYINT
    ,@SysTransCodeID INT
    ,@PeriodCanChange BIT
    ,@UserName VARCHAR(50)
    )
AS /*----------------------------------------------------------------------------------------------------
	Author          :	Saraswathi Lakshmanan
    
    Create date		:	05/05/2010
    
	Procedure Name	:	[USP_AR_DefaultCharges_Update]

	Objective		:	Updates table saPrgVerDefaultChargePeriods
	
	Parameters		:	Name				Type	Data Type				Required? 	
						=====				====	=========				=========	
						@PrgVerPmtId		In		UniqueIDENTIFIER		Required
						@PrgVerID			In		UniqueIDENTIFIER		Required
						@FeeLevelID			In		tinyint					Required
						@SysTransCodeID		In		int						Required
						@PeriodCanChange	In		bit						Required
						@UserName			In		varchar					Required
	Output			:			
						
*/-----------------------------------------------------------------------------------------------------

    BEGIN

        UPDATE  saPrgVerDefaultChargePeriods
        SET     FeeLevelID = @FeeLevelID
               ,SysTransCodeID = @SysTransCodeID
               ,PeriodCanChange = @PeriodCanChange
               ,ModUser = @UserName
               ,ModDate = GETDATE()
        WHERE   PrgVerID = @PrgVerID
                AND PrgVerPmtId = @PrgVerPmtId;
		 
    END;




GO
