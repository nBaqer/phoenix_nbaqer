SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_Report_CreateReportRecord]
    @ResourceId INT
   ,@ReportName VARCHAR(MAX)
   ,@ReportTableLayout BIT
   ,@Return_ReportId UNIQUEIDENTIFIER OUTPUT
   ,@RDLDir VARCHAR(MAX) = NULL
AS
    IF @ResourceId > 0
       AND LTRIM(RTRIM(@ReportName)) <> ''
        BEGIN
            DECLARE @ReportNameNoSpaces VARCHAR(MAX) = (
                                                       SELECT REPLACE(@ReportName, ' ', '')
                                                       );
            DECLARE @RDL VARCHAR(MAX);
            IF @RDLDir IS NULL
                BEGIN
                    SET @RDL = @ReportNameNoSpaces + '/' + @ReportNameNoSpaces;
                END;
            ELSE
                BEGIN
                    SET @RDL = @RDLDir + '/' + @ReportNameNoSpaces;
                END;

            DECLARE @ReportCLass VARCHAR(MAX) = 'FAME.Advantage.Reporting.Logic.' + @ReportNameNoSpaces;


            IF NOT EXISTS (
                          SELECT TOP 1 ReportId
                          FROM   dbo.syReports
                          WHERE  ResourceId = @ResourceId
                          )
                BEGIN
                    INSERT INTO dbo.syReports (
                                              ReportId
                                             ,ResourceId
                                             ,ReportName
                                             ,ReportDescription
                                             ,RDLName
                                             ,AssemblyFilePath
                                             ,ReportClass
                                             ,CreationMethod
                                             ,WebServiceUrl
                                             ,RecordLimit
                                             ,AllowedExportTypes
                                             ,ReportTabLayout
                                             ,ShowPerformanceMsg
                                             ,ShowFilterMode
                                              )
                    VALUES ( NEWID()               -- ReportId - uniqueidentifier
                            ,@ResourceId           -- ResourceId - int
                            ,@ReportName           -- ReportName - varchar(50)
                            ,@ReportName           -- ReportDescription - varchar(500)
                            ,@RDL                  -- RDLName - varchar(200)
                            ,'~/Bin/Reporting.dll' -- AssemblyFilePath - varchar(200)
                            ,@ReportCLass          -- ReportClass - varchar(200)
                            ,'BuildReport'         -- CreationMethod - varchar(200)
                            ,'futurefield'         -- WebServiceUrl - varchar(200)
                            ,400                   -- RecordLimit - bigint
                            ,0                     -- AllowedExportTypes - int
                            ,@ReportTableLayout    -- ReportTabLayout - int
                            ,0                     -- ShowPerformanceMsg - bit
                            ,0                     -- ShowFilterMode - bit
                        );
                END;



        END;

    SET @Return_ReportId = (
                           SELECT TOP 1 ReportId
                           FROM   syReports
                           WHERE  ResourceId = @ResourceId
                           );


GO
