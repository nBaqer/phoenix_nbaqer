SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[USP_AR_RegStd_GetClsSectMeetings]
    (
     @ClsSectID UNIQUEIDENTIFIER
    )
AS /*----------------------------------------------------------------------------------------------------
	Author          :	Saraswathi Lakshmanan
    
    Create date		:	02/02/2010
    
	Procedure Name	:	[USP_AR_RegStd_GetClsSectMeetings]

	Objective		:	fetches the Class Section Meetings for the given class section
	
	Parameters		:	Name			Type	Data Type			
						=====			====	=========			
						@ClsSectID		In		UniqueIdentifier
							
	Output			:	Returns the list of class section meetings
	
	Calling Procedure: 		USP_AR_RegStd_CheckStdScheduleConflict @ClsSectID			
						
*/-----------------------------------------------------------------------------------------------------
/*
Procedure created by Saraswathi Lakshmanan on Jan 29 2010
--To fix issue 17548: ENH: All: Register Students Page is Taking Too Long to Populate Available Students 
This query fetches the Class Section Meetings for the given class section

*/

    BEGIN
        SELECT  a.ClsSectMeetingId
               ,a.WorkDaysId
               ,a.RoomId
               ,a.TimeIntervalId
               ,a.ClsSectionId
               ,a.EndIntervalId
               ,a.ModDate
               ,(
                  SELECT    TimeIntervalDescrip
                  FROM      cmTimeInterval b
                  WHERE     b.TimeIntervalId = a.TimeIntervalId
                ) AS StartTime
               ,(
                  SELECT    TimeIntervalDescrip
                  FROM      cmTimeInterval c
                  WHERE     c.TimeIntervalId = a.EndIntervalId
                ) AS EndTime
               ,e.StartDate
               ,e.EndDate
               ,a.PeriodId
               ,a.AltPeriodId
               ,d.ViewOrder
        FROM    arClsSectMeetings a
        INNER JOIN plWorkdays d ON a.WorkDaysId = d.WorkDaysId
        INNER JOIN arClassSections e ON a.ClsSectionId = e.ClsSectionId
        WHERE   a.ClsSectionId = @ClsSectID
        UNION
        SELECT  a.ClsSectMeetingId
               ,a.WorkDaysId
               ,a.RoomId
               ,a.TimeIntervalId
               ,a.ClsSectionId
               ,a.EndIntervalId
               ,a.ModDate
               ,(
                  SELECT    TimeIntervalDescrip
                  FROM      cmTimeInterval b
                  WHERE     b.TimeIntervalId = a.TimeIntervalId
                ) AS StartTime
               ,(
                  SELECT    TimeIntervalDescrip
                  FROM      cmTimeInterval c
                  WHERE     c.TimeIntervalId = a.EndIntervalId
                ) AS EndTime
               ,a.StartDate
               ,a.EndDate
               ,a.PeriodId
               ,a.AltPeriodId
               ,0 AS vieworder
        FROM    arClsSectMeetings a
        WHERE   a.ClsSectionId = @ClsSectID
                AND a.PeriodId IS NOT NULL
                AND a.PeriodId <> '00000000-0000-0000-0000-000000000000'
        ORDER BY vieworder; 

 
    END;



GO
