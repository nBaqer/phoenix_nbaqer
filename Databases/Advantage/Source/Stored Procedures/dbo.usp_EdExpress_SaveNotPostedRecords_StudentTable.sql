SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[usp_EdExpress_SaveNotPostedRecords_StudentTable]
    @ParentId VARCHAR(50)
   ,@StudentAwardID VARCHAR(50)
   ,@AwardTypeID VARCHAR(50)
   ,@DbIn VARCHAR(50)
   ,@Filter VARCHAR(50)
   ,@FirstName VARCHAR(50)
   ,@LastName VARCHAR(50)
   ,@SSN VARCHAR(50)
   ,@CampusName VARCHAR(500)
   ,@CampusId VARCHAR(50)
   ,@StuEnrollmentId VARCHAR(50)
   ,@AwardAmount DECIMAL(19,4)
   ,@AwardId VARCHAR(50)
   ,@GrantType VARCHAR(50)
   ,@AddDate VARCHAR(50)
   ,@AddTime VARCHAR(50)
   ,@OriginalSSN VARCHAR(50)
   ,@UpdateDate VARCHAR(50)
   ,@UpdateTime VARCHAR(50)
   ,@OriginationStatus VARCHAR(50)
   ,@AcademicYearId VARCHAR(50)
   ,@AwardYearStartDate VARCHAR(50)
   ,@AwardYearEndDate VARCHAR(50)
   ,@ModDate VARCHAR(50)
   ,@FileName VARCHAR(250)
   ,@IsInSchool BIT
   ,@MsgType VARCHAR(50)
AS
    IF @MsgType = 'PELL'
        BEGIN
            INSERT  INTO syEDExpNotPostPell_ACG_SMART_Teach_StudentTable
                    (
                     ParentId
                    ,StudentAwardID
                    ,AwardTypeID
                    ,DbIn
                    ,Filter
                    ,FirstName
                    ,LastName
                    ,SSN
                    ,CampusName
                    ,CampusId
                    ,StuEnrollmentId
                    ,AwardAmount
                    ,AwardId
                    ,GrantType
                    ,AddDate
                    ,AddTime
                    ,OriginalSSN
                    ,UpdateDate
                    ,UpdateTime
                    ,OriginationStatus
                    ,AcademicYearId
                    ,AwardYearStartDate
                    ,AwardYearEndDate
                    ,ModDate
                    ,FileName
                    ,IsInSchool
                    )
            VALUES  (
                     @ParentId
                    ,@StudentAwardID
                    ,@AwardTypeID
                    ,@DbIn
                    ,@Filter
                    ,@FirstName
                    ,@LastName
                    ,@SSN
                    ,@CampusName
                    ,@CampusId
                    ,@StuEnrollmentId
                    ,@AwardAmount
                    ,@AwardId
                    ,@GrantType
                    ,@AddDate
                    ,@AddTime
                    ,@OriginalSSN
                    ,@UpdateDate
                    ,@UpdateTime
                    ,@OriginationStatus
                    ,@AcademicYearId
                    ,@AwardYearStartDate
                    ,@AwardYearEndDate
                    ,@ModDate
                    ,@FileName
                    ,@IsInSchool
                    ); 
        END;




GO
