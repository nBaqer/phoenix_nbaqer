SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_GetStudentsWithMissingItemsReport_Enroll]
    (
     @CampusID UNIQUEIDENTIFIER
    ,@CampGrpID VARCHAR(8000)
    ,@ShiftID VARCHAR(8000) = NULL
    ,@StatusCodeID VARCHAR(8000) = NULL
    ,@LeadGrpID VARCHAR(8000) = NULL   
    )
AS 
    DECLARE @ActiveStatusID AS UNIQUEIDENTIFIER;  
    SET @ActiveStatusID = (
                            SELECT  StatusId
                            FROM    syStatuses
                            WHERE   Status = 'Active'
                          );   
    
    DECLARE @AllCampGrpId AS UNIQUEIDENTIFIER;  
    SET @AllCampGrpId = (
                          SELECT    CampGrpId
                          FROM      dbo.syCampGrps
                          WHERE     IsAllCampusGrp = 1
                                    AND CampGrpCode = 'All'
                        );  

/*
	List of Mandatory Requirements, Mandatory Requirements for Program Version and Mandatory Requirements for Lead Groups
*/
    SELECT DISTINCT
            requirements.adReqId
           ,requirements.Descrip
    INTO    #MandatoryRequirements
    FROM    adReqs requirements
    INNER JOIN adReqsEffectiveDates effectiveDates ON effectiveDates.adReqId = requirements.adReqId
    LEFT JOIN adReqLeadGroups requiremenLeadGroup ON requiremenLeadGroup.adReqEffectiveDateId = effectiveDates.adReqEffectiveDateId
    LEFT JOIN adPrgVerTestDetails requirementProgramVersion ON requirementProgramVersion.adReqId = requirements.adReqId
    WHERE   requirements.adReqTypeId IN ( 1,3,7 )
            AND requirements.StatusId = @ActiveStatusID
            AND (
                  CampGrpId IN ( SELECT strval
                                 FROM   dbo.SPLIT(@CampGrpID) )
                  OR CampGrpId = @AllCampGrpId
                )
            AND (
                  (
                    effectiveDates.MandatoryRequirement = 1
                    OR requiremenLeadGroup.IsRequired = 1
                  )
                  OR requirementProgramVersion.adReqId IS NOT NULL
                )
            AND (
                  @LeadGrpID IS NULL
				  OR requiremenLeadGroup.LeadGrpId IS NULL
                  OR requiremenLeadGroup.LeadGrpId IN ( SELECT  strval
                                                        FROM    dbo.SPLIT(@LeadGrpID) )
                );
	
    SELECT  MIN(StartDate) AS StartDate
           ,StudentId
    INTO    #StartDateStudentEnrollments
    FROM    arStuEnrollments
    GROUP BY StudentId;

    SELECT DISTINCT
            requirementGroup.ReqGrpId
           ,leadGroupRequirementGroup.LeadGrpId
           ,leadGroupRequirementGroup.NumReqs
           ,requirementGroup.Descrip
    INTO    #RequirementGroups
    FROM    adReqGroups requirementGroup
    INNER JOIN adReqGrpDef requirementGroupDefinition ON requirementGroupDefinition.ReqGrpId = requirementGroup.ReqGrpId
    LEFT JOIN adLeadGrpReqGroups leadGroupRequirementGroup ON leadGroupRequirementGroup.LeadGrpId = requirementGroupDefinition.LeadGrpId
    WHERE   leadGroupRequirementGroup.StatusId = @ActiveStatusID
            AND (
                  CampGrpId IN ( SELECT strval
                                 FROM   dbo.SPLIT(@CampGrpID) )
                  OR CampGrpId = @AllCampGrpId
                )
            AND (
                  @LeadGrpID IS NULL
				  OR leadGroupRequirementGroup.LeadGrpId IS NULL
                  OR leadGroupRequirementGroup.LeadGrpId IN ( SELECT    strval
                                                              FROM      dbo.SPLIT(@LeadGrpID) )
                );

    SELECT  campusGroups.CampGrpId
           ,campusGroups.CampGrpDescrip
           ,enrollments.CampusId
           ,campus.CampDescrip
           ,enrollments.StuEnrollId
           ,lead.StudentId
           ,lead.LastName
           ,lead.FirstName
           ,lead.SSN
           ,enrollments.PrgVerId
           ,programVersion.PrgVerDescrip
           ,enrollments.StatusCodeId
           ,statusCodes.StatusCodeDescrip
           ,enrollments.ShiftId
           ,shifts.ShiftDescrip
           ,enrollments.EdLvlId
           ,levels.EdLvlDescrip
           ,requirements.adReqId
           ,tempRequirementGroups.Descrip AS ReqDescrip
           ,'Requirement Group' AS ReqType
           ,NULL AS Required
           ,0 AS Fullfilled
    INTO    #MissingRequirementsInRequirementGroups
    FROM    adLeads lead
    INNER JOIN arStuEnrollments enrollments ON enrollments.StudentId = lead.StudentId
    INNER JOIN arPrgVersions programVersion ON programVersion.PrgVerId = enrollments.PrgVerId
    INNER JOIN syCampuses campus ON campus.CampusId = enrollments.CampusId
    INNER JOIN syCmpGrpCmps campusGroupCampus ON campusGroupCampus.CampusId = campus.CampusId
    INNER JOIN syCampGrps campusGroups ON campusGroups.CampGrpId = campusGroupCampus.CampGrpId
    INNER JOIN syStatusCodes statusCodes ON statusCodes.StatusCodeId = enrollments.StatusCodeId
    LEFT JOIN arShifts shifts ON shifts.ShiftId = enrollments.ShiftId
    LEFT JOIN adEdLvls levels ON levels.EdLvlId = enrollments.EdLvlId
    INNER JOIN adLeadByLeadGroups leadByLeadGroups ON leadByLeadGroups.LeadId = lead.LeadId
    INNER JOIN #RequirementGroups tempRequirementGroups ON tempRequirementGroups.LeadGrpId = leadByLeadGroups.LeadGrpId
    INNER JOIN #StartDateStudentEnrollments startDateEnrollments ON startDateEnrollments.StudentId = lead.StudentId
    INNER JOIN adReqGrpDef requirementGroupDefinition ON requirementGroupDefinition.LeadGrpId = leadByLeadGroups.LeadGrpId
    INNER JOIN adReqGroups requirementGroup ON requirementGroup.ReqGrpId = tempRequirementGroups.ReqGrpId
    INNER JOIN plStudentDocs documentRequirements ON documentRequirements.StudentId = lead.StudentId
    INNER JOIN adReqs requirements ON requirements.adReqId = documentRequirements.DocumentId
    INNER JOIN adReqTypes requirementTypes ON requirementTypes.adReqTypeId = requirements.adReqTypeId
    INNER JOIN adLeadDocsReceived documentsReceived ON documentsReceived.LeadId = lead.LeadId
    INNER JOIN adReqsEffectiveDates effectiveDates ON effectiveDates.adReqId = requirements.adReqId
    WHERE   requirementGroup.StatusId = @ActiveStatusID
            AND requirementGroupDefinition.ReqGrpId = tempRequirementGroups.ReqGrpId
            AND requirements.adReqId = requirementGroupDefinition.adReqId
            AND startDateEnrollments.StartDate >= effectiveDates.StartDate
            AND (
                  startDateEnrollments.StartDate <= effectiveDates.EndDate
                  OR effectiveDates.EndDate IS NULL
                )
            AND (
                  ( documentsReceived.Override = 1 )
                  AND documentsReceived.DocumentId = documentRequirements.DocumentId
                )
            AND lead.CampusId = @CampusID
            AND (
                  campusGroups.CampGrpId IN ( SELECT    strval
                                              FROM      dbo.SPLIT(@CampGrpID) )
                  OR campusGroups.CampGrpId = @AllCampGrpId
                )
            AND (
                  @ShiftID IS NULL
				  OR shifts.ShiftId IS NULL
                  OR shifts.ShiftId IN ( SELECT strval
                                         FROM   dbo.SPLIT(@ShiftID) )
                )
            AND (
                  @StatusCodeID IS NULL
                  OR statusCodes.StatusCodeId IN ( SELECT   strval
                                                   FROM     dbo.SPLIT(@StatusCodeID) )
                );

    SELECT  campusGroups.CampGrpId
           ,campusGroups.CampGrpDescrip
           ,enrollments.CampusId
           ,campus.CampDescrip
           ,enrollments.StuEnrollId
           ,lead.StudentId
           ,lead.LastName
           ,lead.FirstName
           ,lead.SSN
           ,enrollments.PrgVerId
           ,programVersion.PrgVerDescrip
           ,enrollments.StatusCodeId
           ,statusCodes.StatusCodeDescrip
           ,enrollments.ShiftId
           ,shifts.ShiftDescrip
           ,enrollments.EdLvlId
           ,levels.EdLvlDescrip
           ,requirements.adReqId
           ,requirements.Descrip AS ReqDescrip
           ,requirementTypes.Descrip AS ReqType
           ,NULL AS Required
           ,0 AS Fullfilled
    FROM    adLeads lead
    INNER JOIN arStuEnrollments enrollments ON enrollments.StudentId = lead.StudentId
    INNER JOIN arPrgVersions programVersion ON programVersion.PrgVerId = enrollments.PrgVerId
    INNER JOIN syCampuses campus ON campus.CampusId = enrollments.CampusId
    INNER JOIN syCmpGrpCmps campusGroupCampus ON campusGroupCampus.CampusId = campus.CampusId
    INNER JOIN syCampGrps campusGroups ON campusGroups.CampGrpId = campusGroupCampus.CampGrpId
    INNER JOIN syStatusCodes statusCodes ON statusCodes.StatusCodeId = enrollments.StatusCodeId
    LEFT JOIN arShifts shifts ON shifts.ShiftId = enrollments.ShiftId
    LEFT JOIN adEdLvls levels ON levels.EdLvlId = enrollments.EdLvlId
    INNER JOIN plStudentDocs documentRequirements ON documentRequirements.StudentId = lead.StudentId
    INNER JOIN #MandatoryRequirements mandatoryRequirements ON mandatoryRequirements.adReqId = documentRequirements.DocumentId
    INNER JOIN adReqs requirements ON requirements.adReqId = documentRequirements.DocumentId
    INNER JOIN adReqTypes requirementTypes ON requirementTypes.adReqTypeId = requirements.adReqTypeId
    INNER JOIN adLeadDocsReceived documentsReceived ON documentsReceived.LeadId = lead.LeadId
    INNER JOIN adReqsEffectiveDates effectiveDates ON effectiveDates.adReqId = requirements.adReqId
    INNER JOIN #StartDateStudentEnrollments startDateEnrollments ON startDateEnrollments.StudentId = lead.StudentId
    WHERE   requirements.adReqId NOT IN ( SELECT    adReqId
                                          FROM      #MissingRequirementsInRequirementGroups )
            AND (
                  requirements.CampGrpId = @CampGrpID
                  OR requirements.CampGrpId = @AllCampGrpId
                )
            AND startDateEnrollments.StartDate >= effectiveDates.StartDate
            AND (
                  startDateEnrollments.StartDate <= effectiveDates.EndDate
                  OR effectiveDates.EndDate IS NULL
                )
            AND (
                  ( documentsReceived.Override = 1 )
                  AND documentsReceived.DocumentId = documentRequirements.DocumentId
                )
            AND lead.CampusId = @CampusID
            AND (
                  campusGroups.CampGrpId IN ( SELECT    strval
                                              FROM      dbo.SPLIT(@CampGrpID) )
                  OR campusGroups.CampGrpId = @AllCampGrpId
                )
            AND (
                  @ShiftID IS NULL
				  OR shifts.ShiftId IS NULL
                  OR shifts.ShiftId IN ( SELECT strval
                                         FROM   dbo.SPLIT(@ShiftID) )
                )
            AND (
                  @StatusCodeID IS NULL
                  OR statusCodes.StatusCodeId IN ( SELECT   strval
                                                   FROM     dbo.SPLIT(@StatusCodeID) )
                )
    UNION
    SELECT  campusGroups.CampGrpId
           ,campusGroups.CampGrpDescrip
           ,enrollments.CampusId
           ,campus.CampDescrip
           ,enrollments.StuEnrollId
           ,lead.StudentId
           ,lead.LastName
           ,lead.FirstName
           ,lead.SSN
           ,enrollments.PrgVerId
           ,programVersion.PrgVerDescrip
           ,enrollments.StatusCodeId
           ,statusCodes.StatusCodeDescrip
           ,enrollments.ShiftId
           ,shifts.ShiftDescrip
           ,enrollments.EdLvlId
           ,levels.EdLvlDescrip
           ,requirements.adReqId
           ,requirements.Descrip AS ReqDescrip
           ,requirementTypes.Descrip AS ReqType
           ,NULL AS Required
           ,0 AS Fullfilled
    FROM    adLeads lead
    INNER JOIN arStuEnrollments enrollments ON enrollments.StudentId = lead.StudentId
    INNER JOIN arPrgVersions programVersion ON programVersion.PrgVerId = enrollments.PrgVerId
    INNER JOIN syCampuses campus ON campus.CampusId = enrollments.CampusId
    INNER JOIN syCmpGrpCmps campusGroupCampus ON campusGroupCampus.CampusId = campus.CampusId
    INNER JOIN syCampGrps campusGroups ON campusGroups.CampGrpId = campusGroupCampus.CampGrpId
    INNER JOIN syStatusCodes statusCodes ON statusCodes.StatusCodeId = enrollments.StatusCodeId
    INNER JOIN arShifts shifts ON shifts.ShiftId = enrollments.ShiftId
    INNER JOIN adEdLvls levels ON levels.EdLvlId = enrollments.EdLvlId
    INNER JOIN adLeadEntranceTest testRequirement ON testRequirement.LeadId = lead.LeadId
    INNER JOIN #MandatoryRequirements mandatoryRequirements ON mandatoryRequirements.adReqId = testRequirement.EntrTestId
    INNER JOIN adReqs requirements ON requirements.adReqId = testRequirement.EntrTestId
    INNER JOIN adReqTypes requirementTypes ON requirementTypes.adReqTypeId = requirements.adReqTypeId
    INNER JOIN adReqsEffectiveDates effectiveDates ON effectiveDates.adReqId = requirements.adReqId
    INNER JOIN #StartDateStudentEnrollments startDateEnrollments ON startDateEnrollments.StudentId = lead.StudentId
    WHERE   requirements.adReqId NOT IN ( SELECT    adReqId
                                          FROM      #MissingRequirementsInRequirementGroups )
            AND (
                  requirements.CampGrpId = @CampGrpID
                  OR requirements.CampGrpId = @AllCampGrpId
                )
            AND startDateEnrollments.StartDate >= effectiveDates.StartDate
            AND (
                  startDateEnrollments.StartDate <= effectiveDates.EndDate
                  OR effectiveDates.EndDate IS NULL
                )
            AND (
                  ( testRequirement.Override = 1 )
                  AND testRequirement.EntrTestId = requirements.adReqId
                )
            AND lead.CampusId = @CampusID
            AND (
                  campusGroups.CampGrpId IN ( SELECT    strval
                                              FROM      dbo.SPLIT(@CampGrpID) )
                  OR campusGroups.CampGrpId = @AllCampGrpId
                )
            AND (
                  @ShiftID IS NULL
				  OR shifts.ShiftId IS NULL
                  OR shifts.ShiftId IN ( SELECT strval
                                         FROM   dbo.SPLIT(@ShiftID) )
                )
            AND (
                  @StatusCodeID IS NULL
                  OR statusCodes.StatusCodeId IN ( SELECT   strval
                                                   FROM     dbo.SPLIT(@StatusCodeID) )
                )
    UNION
    SELECT  campusGroups.CampGrpId
           ,campusGroups.CampGrpDescrip
           ,enrollments.CampusId
           ,campus.CampDescrip
           ,enrollments.StuEnrollId
           ,lead.StudentId
           ,lead.LastName
           ,lead.FirstName
           ,lead.SSN
           ,enrollments.PrgVerId
           ,programVersion.PrgVerDescrip
           ,enrollments.StatusCodeId
           ,statusCodes.StatusCodeDescrip
           ,enrollments.ShiftId
           ,shifts.ShiftDescrip
           ,enrollments.EdLvlId
           ,levels.EdLvlDescrip
           ,requirements.adReqId
           ,requirements.Descrip AS ReqDescrip
           ,requirementTypes.Descrip AS ReqType
           ,NULL AS Required
           ,0 AS Fullfilled
    FROM    adLeads lead
    INNER JOIN arStuEnrollments enrollments ON enrollments.StudentId = lead.StudentId
    INNER JOIN arPrgVersions programVersion ON programVersion.PrgVerId = enrollments.PrgVerId
    INNER JOIN syCampuses campus ON campus.CampusId = enrollments.CampusId
    INNER JOIN syCmpGrpCmps campusGroupCampus ON campusGroupCampus.CampusId = campus.CampusId
    INNER JOIN syCampGrps campusGroups ON campusGroups.CampGrpId = campusGroupCampus.CampGrpId
    INNER JOIN syStatusCodes statusCodes ON statusCodes.StatusCodeId = enrollments.StatusCodeId
    INNER JOIN adLeadTranReceived feeRequirementReceived ON feeRequirementReceived.LeadId = lead.LeadId
    INNER JOIN #MandatoryRequirements mandatoryRequirements ON mandatoryRequirements.adReqId = feeRequirementReceived.DocumentId
    INNER JOIN adReqs requirements ON requirements.adReqId = feeRequirementReceived.DocumentId
    INNER JOIN adReqTypes requirementTypes ON requirementTypes.adReqTypeId = requirements.adReqTypeId
    INNER JOIN adReqsEffectiveDates effectiveDates ON effectiveDates.adReqId = requirements.adReqId
    INNER JOIN #StartDateStudentEnrollments startDateEnrollments ON startDateEnrollments.StudentId = lead.StudentId
    LEFT JOIN arShifts shifts ON shifts.ShiftId = enrollments.ShiftId
    LEFT JOIN adEdLvls levels ON levels.EdLvlId = enrollments.EdLvlId
    WHERE   requirements.adReqId NOT IN ( SELECT    adReqId
                                          FROM      #MissingRequirementsInRequirementGroups )
            AND (
                  requirements.CampGrpId = @CampGrpID
                  OR requirements.CampGrpId = @AllCampGrpId
                )
            AND startDateEnrollments.StartDate >= effectiveDates.StartDate
            AND (
                  startDateEnrollments.StartDate <= effectiveDates.EndDate
                  OR effectiveDates.EndDate IS NULL
                )
            AND feeRequirementReceived.Override = 1
            AND lead.CampusId = @CampusID
            AND (
                  campusGroups.CampGrpId IN ( SELECT    strval
                                              FROM      dbo.SPLIT(@CampGrpID) )
                  OR campusGroups.CampGrpId = @AllCampGrpId
                )
            AND (
                  @ShiftID IS NULL
				  OR shifts.ShiftId IS NULL
                  OR shifts.ShiftId IN ( SELECT strval
                                         FROM   dbo.SPLIT(@ShiftID) )
                )
            AND (
                  @StatusCodeID IS NULL
                  OR statusCodes.StatusCodeId IN ( SELECT   strval
                                                   FROM     dbo.SPLIT(@StatusCodeID) )
                )
    UNION
    SELECT  *
    FROM    #MissingRequirementsInRequirementGroups
    ORDER BY LastName;

	
    DROP TABLE #MandatoryRequirements;
    DROP TABLE #StartDateStudentEnrollments;
    DROP TABLE #RequirementGroups;
    DROP TABLE #MissingRequirementsInRequirementGroups;

GO
