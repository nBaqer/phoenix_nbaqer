SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[USP_FL_GetFlSourcePath] @CampusId VARCHAR(50)
AS
    SELECT  FLSourcePath
           ,FLTargetPath
           ,FLExceptionPath
           ,RemoteServerUsrNmFL
           ,RemoteServerPwdFL
    FROM    syCampuses
    WHERE   CampusId = @CampusId;




GO
