SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--=================================================================================================
-- USP_WeeklyAttendanceReport
--=================================================================================================
CREATE PROCEDURE [dbo].[USP_WeeklyAttendanceReport]
    @WeekStartDate DATETIME
   ,@WeekEndDate DATETIME
   ,@CampusID UNIQUEIDENTIFIER
   ,@TermId VARCHAR(8000) = NULL
   ,@CourseId VARCHAR(8000) = NULL
   ,@InstructorId VARCHAR(8000) = NULL
   ,@ShiftId VARCHAR(8000) = NULL
   ,@ClassId VARCHAR(8000) = NULL
   ,@EnrollmentId VARCHAR(8000) = NULL
AS
    BEGIN        
        DECLARE @StudentIdentifier AS NVARCHAR(1000);      
          
        SELECT  @StudentIdentifier = SCASV.Value
        FROM    syConfigAppSettings AS SCAS
        INNER JOIN syConfigAppSetValues AS SCASV ON SCASV.SettingId = SCAS.SettingId
        WHERE   SCAS.KeyName = 'StudentIdentifier';     -- @StudentIdentifier --> 'SSN', 'EnrollmentId', 'StudentId'      
                      
        SELECT  C.StuEnrollId
               ,C.StudentIdentifier
               ,C.StudentName
               ,SUM(C.Mon) AS Mon
               ,SUM(C.Tue) AS Tue
               ,SUM(C.Wed) AS Wed
               ,SUM(C.Thur) AS Thur
               ,SUM(C.Fri) AS Fri
               ,SUM(C.Sat) AS Sat
               ,SUM(C.Sun) AS Sun
               ,SUM(C.WeekTotal) AS WeekTotal
               ,SUM(C.NewTotal) AS NewTotal
               ,SUM(C.NewTotalSched) AS NewTotalSched
               ,SUM(C.NewTotalDaysAbsent) AS NewTotalDaysAbsent
               ,SUM(C.NewTotalMakeUpDays) AS NewTotalMakeUpDays
               ,SUM(C.NewNetDaysAbsent) AS NewNetDaysAbsent
               ,SUM(C.NewSchedDaysatLDA) AS NewSchedDaysatLDA
               ,C.LDA                    
 --,C.CampGrpDescrip                    
               ,C.AttTypeID
               ,C.SuppressDate
               ,C.ClsSectionId
               ,C.ClsSection
               ,Descrip
               ,FullName
               ,TermId
               ,TermDescrip
               ,ReqId
               ,Code
               ,InstructorId
               ,shiftid
               ,StatusCodeId
               ,StatusCodeDescrip
        FROM    (
                  SELECT    B.*
                  FROM      (
                              SELECT    A.StuEnrollId
                                       ,CASE WHEN @StudentIdentifier = 'SSN' THEN '***-**-' + SUBSTRING(S.SSN,6,4)
                                             WHEN @StudentIdentifier = 'EnrollmentId' THEN SE.EnrollmentId
                                             WHEN @StudentIdentifier = 'StudentId' THEN S.StudentNumber
                                             ELSE S.StudentNumber
                                        END AS StudentIdentifier     
          --S.SSN AS StudentIdentifier                
                                       ,ISNULL((
                                                 SELECT LastName
                                                 FROM   arStudent
                                                 WHERE  StudentId IN ( SELECT   StudentId
                                                                       FROM     arStuEnrollments
                                                                       WHERE    StuEnrollId = A.StuEnrollId )
                                               ),' ') + ',' + ISNULL((
                                                                       SELECT   FirstName
                                                                       FROM     arStudent
                                                                       WHERE    StudentId IN ( SELECT   StudentId
                                                                                               FROM     arStuEnrollments
                                                                                               WHERE    StuEnrollId = A.StuEnrollId )
                                                                     ),' ') + ' ' + ISNULL((
                                                                                             SELECT MiddleName
                                                                                             FROM   arStudent
                                                                                             WHERE  StudentId IN ( SELECT   StudentId
                                                                                                                   FROM     arStuEnrollments
                                                                                                                   WHERE    StuEnrollId = A.StuEnrollId )
                                                                                           ),' ') + ' ' AS StudentName
                                       ,(
                                          SELECT    SUM(Actual)
                                          FROM      atClsSectAttendance
                                          WHERE     StuEnrollId = A.StuEnrollId
                                                    AND ClsSectionId = A.ClsSectionId
                                                    AND CONVERT(DATE,MeetDate,101) = @WeekStartDate
                                                    AND Actual <> 999.0
                                                    AND Actual <> 9999.0
                                        ) AS Mon
                                       ,(
                                          SELECT    SUM(Actual)
                                          FROM      atClsSectAttendance
                                          WHERE     StuEnrollId = A.StuEnrollId
                                                    AND ClsSectionId = A.ClsSectionId
                                                    AND CONVERT(DATE,MeetDate,101) = DATEADD(DAY,1,@WeekStartDate)
                                                    AND Actual <> 999.0
                                                    AND Actual <> 9999.0
                                        ) AS Tue
                                       ,(
                                          SELECT    SUM(Actual)
                                          FROM      atClsSectAttendance
                                          WHERE     StuEnrollId = A.StuEnrollId
                                                    AND ClsSectionId = A.ClsSectionId
                                                    AND CONVERT(DATE,MeetDate,101) = DATEADD(DAY,2,@WeekStartDate)
                                                    AND Actual <> 999.0
                                                    AND Actual <> 9999.0
                                        ) AS Wed
                                       ,(
                                          SELECT    SUM(Actual)
                                          FROM      atClsSectAttendance
                                          WHERE     StuEnrollId = A.StuEnrollId
                                                    AND ClsSectionId = A.ClsSectionId
                                                    AND CONVERT(DATE,MeetDate,101) = DATEADD(DAY,3,@WeekStartDate)
                                                    AND Actual <> 999.0
                                                    AND Actual <> 9999.0
                                        ) AS Thur
                                       ,(
                                          SELECT    SUM(Actual)
                                          FROM      atClsSectAttendance
                                          WHERE     StuEnrollId = A.StuEnrollId
                                                    AND ClsSectionId = A.ClsSectionId
                                                    AND CONVERT(DATE,MeetDate,101) = DATEADD(DAY,4,@WeekStartDate)
                                                    AND Actual <> 999.0
                                                    AND Actual <> 9999.0
                                        ) AS Fri
                                       ,(
                                          SELECT    SUM(Actual)
                                          FROM      atClsSectAttendance
                                          WHERE     StuEnrollId = A.StuEnrollId
                                                    AND ClsSectionId = A.ClsSectionId
                                                    AND CONVERT(DATE,MeetDate,101) = DATEADD(DAY,5,@WeekStartDate)
                                                    AND Actual <> 999.0
                                                    AND Actual <> 9999.0
                                        ) AS Sat
                                       ,(
                                          SELECT    SUM(Actual)
                                          FROM      atClsSectAttendance
                                          WHERE     StuEnrollId = A.StuEnrollId
                                                    AND ClsSectionId = A.ClsSectionId
                                                    AND CONVERT(DATE,MeetDate,101) = @WeekEndDate
                                                    AND Actual <> 999.0
                                                    AND Actual <> 9999.0
                                        ) AS Sun
                                       ,ISNULL((
                                                 SELECT SUM(Actual)
                                                 FROM   atClsSectAttendance
                                                 WHERE  ( CONVERT(DATE,MeetDate,101) ) <= @WeekEndDate
                                                        AND ( CONVERT(DATE,MeetDate,101) ) >= @WeekStartDate
                                                        AND StuEnrollId = A.StuEnrollId
                                                        AND ClsSectionId = A.ClsSectionId
                                                        AND Actual <> 999.0
                                                        AND Actual <> 9999.0
                                               ),0.00) AS WeekTotal
                                       ,ISNULL((
                                                 SELECT SUM(Actual)
                                                 FROM   atClsSectAttendance
                                                 WHERE  ( CONVERT(DATE,MeetDate,101) ) <= @WeekEndDate
                                                        AND StuEnrollId = A.StuEnrollId
                                                        AND ClsSectionId = A.ClsSectionId
                                                        AND Actual <> 999.0
                                                        AND Actual <> 9999.0
                                               ),0.00) AS NewTotal
                                       ,ISNULL((
                                                 SELECT SUM(Scheduled)
                                                 FROM   atClsSectAttendance
                                                 WHERE  ( CONVERT(DATE,MeetDate,101) ) <= @WeekEndDate
                                                        AND StuEnrollId = A.StuEnrollId
                                                        AND ClsSectionId = A.ClsSectionId
                                                        AND Actual <> 999.0
                                                        AND Actual <> 9999.0
                                               ),0.00) AS NewTotalSched
                                       ,ISNULL((
                                                 SELECT SUM(Scheduled - Actual)
                                                 FROM   atClsSectAttendance
                                                 WHERE  ( CONVERT(DATE,MeetDate,101) ) <= @WeekEndDate
                                                        AND StuEnrollId = A.StuEnrollId
                                                        AND ClsSectionId = A.ClsSectionId
                                                        AND Actual <> 999.0
                                                        AND Actual <> 9999.0
                                                        AND Scheduled IS NOT NULL
                                                        AND Scheduled <> 0
                                                        AND Actual IS NOT NULL
                                                        AND Scheduled > Actual
                                               ),0.00) AS NewTotalDaysAbsent
                                       ,ISNULL((
                                                 SELECT SUM(Actual - Scheduled)
                                                 FROM   atClsSectAttendance
                                                 WHERE  ( CONVERT(DATE,MeetDate,101) ) <= @WeekEndDate
                                                        AND StuEnrollId = A.StuEnrollId
                                                        AND ClsSectionId = A.ClsSectionId
                                                        AND Actual <> 999.0
                                                        AND Actual <> 9999.0
                                                        AND Scheduled IS NOT NULL
                                                        AND Actual IS NOT NULL
                                                        AND Actual <> 0
                                                        AND Scheduled < Actual
                                               ),0.00) AS NewTotalMakeUpDays
                                       ,ISNULL(( (
                                                   SELECT   SUM(Scheduled - Actual)
                                                   FROM     atClsSectAttendance
                                                   WHERE    ( CONVERT(DATE,MeetDate,101) ) <= @WeekEndDate
                                                            AND StuEnrollId = A.StuEnrollId
                                                            AND ClsSectionId = A.ClsSectionId
                                                            AND Actual <> 999.0
                                                            AND Actual <> 9999.0
                                                            AND Scheduled IS NOT NULL
                                                            AND Scheduled <> 0
                                                            AND Actual IS NOT NULL
                                                            AND Scheduled > Actual
                                                 ) - (
                                                       SELECT   SUM(Actual - Scheduled)
                                                       FROM     atClsSectAttendance
                                                       WHERE    ( CONVERT(DATE,MeetDate,101) ) <= @WeekEndDate
                                                                AND StuEnrollId = A.StuEnrollId
                                                                AND ClsSectionId = A.ClsSectionId
                                                                AND Actual <> 999.0
                                                                AND Actual <> 9999.0
                                                                AND Scheduled IS NOT NULL
                                                                AND Actual IS NOT NULL
                                                                AND Actual <> 0
                                                                AND Scheduled < Actual
                                                     ) ),0.00) AS NewNetDaysAbsent
                                       ,ISNULL((
                                                 SELECT SUM(Scheduled)
                                                 FROM   atClsSectAttendance
                                                 WHERE  StuEnrollId = A.StuEnrollId
                                                        AND ClsSectionId = A.ClsSectionId
                                                        AND Actual <> 999.0
                                                        AND Actual <> 9999.0
                                                        AND CONVERT(DATE,MeetDate,101) <= dbo.GetLDAFromAttendance(A.StuEnrollId)
                                               ),0.00) AS NewSchedDaysatLDA
                                       ,(
                                          SELECT    CONVERT(VARCHAR,MAX(MeetDate),101)
                                          FROM      atClsSectAttendance
                                          WHERE     StuEnrollId = A.StuEnrollId
                                                    AND ClsSectionId = A.ClsSectionId
                                                    AND Actual <> 999.0
                                                    AND Actual <> 9999.0
                                                    AND Actual <> 0.00
                                        ) AS LDA                    
                               --,CG.CampGrpDescrip                    
                                       ,F.CampDescrip
                                       ,(
                                          SELECT    UnitTypeId
                                          FROM      arPrgVersions PV
                                          WHERE     PV.PrgVerId IN ( SELECT PrgVerId
                                                                     FROM   arStuEnrollments
                                                                     WHERE  StuEnrollId = A.StuEnrollId )
                                        ) AS AttTypeID
                                       ,'yes' AS SuppressDate
                                       ,A.ClsSectionId
                                       ,CS.ClsSection
                                       ,R.Descrip
                                       ,U.FullName
                                       ,CS.TermId
                                       ,T.TermDescrip
                                       ,CS.ReqId
                                       ,R.Code
                                       ,CS.InstructorId
                                       ,CS.shiftid
                                       ,SE.StatusCodeId
                                       ,SC.StatusCodeDescrip
                              FROM      atClsSectAttendance A
                              INNER JOIN arStuEnrollments SE ON SE.StuEnrollId = A.StuEnrollId
                              INNER JOIN arStudent S ON S.StudentId = SE.StudentId
                              INNER JOIN syCampuses F ON F.CampusId = SE.CampusId
                              INNER JOIN dbo.arClassSections CS ON CS.ClsSectionId = A.ClsSectionId
                              INNER JOIN dbo.arTerm T ON CS.TermId = T.TermId
                              INNER JOIN dbo.arReqs R ON R.ReqId = CS.ReqId
                              INNER JOIN dbo.syUsers U ON U.UserId = CS.InstructorId
                              INNER JOIN dbo.syStatusCodes SC ON SE.StatusCodeId = SC.StatusCodeId
                              WHERE     ( CONVERT(DATE,MeetDate,101) ) >= @WeekStartDate
                                        AND ( CONVERT(DATE,MeetDate,101) ) <= @WeekEndDate
                                        AND SE.CampusId = F.CampusId
                                        AND A.ClsSectionId = CS.ClsSectionId
                                        AND F.CampusId = @CampusID
                                        AND (
                                              @TermId IS NULL
                                              OR @TermId = ''
                                              OR T.TermId IN ( SELECT   strval
                                                               FROM     dbo.SPLIT(@TermId) )
                                            )
                                        AND (
                                              @CourseId IS NULL
                                              OR @CourseId = ''
                                              OR R.ReqId IN ( SELECT    strval
                                                              FROM      dbo.SPLIT(@CourseId) )
                                            )
                                        AND (
                                              @InstructorId IS NULL
                                              OR @InstructorId = ''
                                              OR CS.InstructorId IN ( SELECT    strval
                                                                      FROM      dbo.SPLIT(@InstructorId) )
                                            )
                                        AND (
                                              @ShiftId IS NULL
                                              OR @ShiftId = ''
                                              OR CS.shiftid IN ( SELECT strval
                                                                 FROM   dbo.SPLIT(@ShiftId) )
                                            )
                                        AND (
                                              @ClassId IS NULL
                                              OR @ClassId = ''
                                              OR CS.ClsSectionId IN ( SELECT    strval
                                                                      FROM      dbo.SPLIT(@ClassId) )
                                            )
                                        AND (
                                              @EnrollmentId IS NULL
                                              OR @EnrollmentId = ''
                                              OR SE.StatusCodeId IN ( SELECT    strval
                                                                      FROM      dbo.SPLIT(@EnrollmentId) )
                                            )
                            ) B
                  GROUP BY  B.StuEnrollId
                           ,B.StudentName
                           ,B.StudentIdentifier                    
       --,B.CampGrpDescrip                    
                           ,B.CampDescrip
                           ,B.Mon
                           ,B.Tue
                           ,B.Wed
                           ,B.Thur
                           ,B.Fri
                           ,B.Sat
                           ,B.Sun
                           ,B.WeekTotal
                           ,B.NewTotal
                           ,B.NewTotalSched
                           ,B.NewTotalDaysAbsent
                           ,B.NewTotalMakeUpDays
                           ,B.NewNetDaysAbsent
                           ,B.NewSchedDaysatLDA
                           ,B.LDA
                           ,B.AttTypeID
                           ,B.SuppressDate
                           ,B.ClsSectionId
                           ,B.ClsSection
                           ,B.Descrip
                           ,B.FullName
                           ,B.TermId
                           ,B.TermDescrip
                           ,B.ReqId
                           ,B.Code
                           ,B.InstructorId
                           ,B.shiftid
                           ,B.StatusCodeId
                           ,B.StatusCodeDescrip
                ) C
        GROUP BY C.StuEnrollId
               ,C.StudentIdentifier
               ,C.StudentName
               ,C.LDA                    
       --,C.CampGrpDescrip                    
               ,C.AttTypeID
               ,C.SuppressDate
               ,C.ClsSectionId
               ,C.ClsSection
               ,C.Descrip
               ,C.FullName
               ,TermId
               ,TermDescrip
               ,C.ReqId
               ,C.Code
               ,C.InstructorId
               ,C.shiftid
               ,C.StatusCodeId
               ,C.StatusCodeDescrip
        ORDER BY C.StudentName
               ,C.StudentIdentifier
               ,C.ClsSectionId;       
    END;  
--=================================================================================================
-- END  --  USP_WeeklyAttendanceReport
--=================================================================================================
GO
