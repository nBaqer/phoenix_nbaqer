SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_FallPartB4_FullTimeUnderGraduate_GetList_Summary]
    @CampusId VARCHAR(50)
   ,@OfficialReportingDate DATETIME = NULL
   ,@ProgId VARCHAR(4000) = NULL
AS --Risk: If the student's race is null, student may not show up on the report
    SELECT  Gender
           ,Race
           ,GenderSequence
           ,RaceSequence
           ,SUM(ISNULL(FirstTimeCount,0)) AS FirstTime
           ,SUM(ISNULL(TransferInCount,0)) AS TransferIn
           ,SUM(ISNULL(NonDegreeSeekingCount,0)) AS NonDegreeCerticateSeeking
           ,SUM(ISNULL(CountinuingCount,0)) AS Continuing
           ,SUM(ISNULL(AllOtherCount,0)) AS AllOther
    FROM    (
              SELECT TOP 1000
                        Gender
                       ,Race
                       ,FirstTime AS FirstTimeCount
                       ,ISNULL(TransferIn,0) AS TransferInCount
                       ,NonDegreeSeeking AS NonDegreeSeekingCount
                       ,Continuing AS CountinuingCount
                       ,AllOther AS AllOtherCount
                       ,GenderSequence
                       ,RaceSequence
              FROM      (
                          SELECT    NULL AS SSN
                                   ,NULL AS StudentNumber
                                   ,NULL AS StudentName
                                   ,'Men' AS Gender
                                   ,CASE WHEN IPEDSValue IS NULL
                                      --THEN 'Race/ethnicity unknown' --DE9087
                                              THEN ''
                                         ELSE (
                                                SELECT DISTINCT
                                                        AgencyDescrip
                                                FROM    syRptAgencyFldValues
                                                WHERE   RptAgencyFldValId = IPEDSValue
                                              )
                                    END AS Race
                                   ,NULL AS FirstTime
                                   ,NULL AS TransferIn
                                   ,NULL AS NonDegreeSeeking
                                   ,NULL AS Continuing
                                   ,NULL AS AllOther
                                   ,1 AS GenderSequence
                                   ,IPEDSSequence AS RaceSequence
                          FROM      adEthCodes
                          WHERE     EthCodeId NOT IN ( SELECT DISTINCT
                                                                t1.Race
                                                       FROM     arStudent t1
                                                       INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                                                       INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                                                       INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                                                                    AND t6.SysStatusId NOT IN ( 7,8 )
                                                       INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                                                       INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                                                       INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                                                       LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                                                       INNER JOIN adGenders t11 ON t1.Gender = t11.GenderId
                                                       WHERE    t2.CampusId = @CampusId
                                                                AND (
                                                                      @ProgId IS NULL
                                                                      OR t8.ProgId IN ( SELECT  Val
                                                                                        FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                                                    )
                                                                AND LOWER(t9.IPEDSValue) = 58
                                                                AND LOWER(t10.IPEDSValue) = 61
                                                                AND t11.IPEDSValue = 30
                                                                AND t1.Race IS NOT NULL )
                          UNION
                          SELECT    t1.SSN
                                   ,t1.StudentNumber
                                   ,t1.LastName + ', ' + t1.FirstName + ' ' + ISNULL(t1.MiddleName,'') AS StudentName
                                   ,CASE WHEN t3.IPEDSValue IS NULL THEN 'unknown'
                                         ELSE (
                                                SELECT DISTINCT
                                                        AgencyDescrip
                                                FROM    syRptAgencyFldValues
                                                WHERE   RptAgencyFldValId = t3.IPEDSValue
                                              )
                                    END AS Gender
                                   ,CASE WHEN t4.IPEDSValue IS NULL
                                         --THEN 'Race/ethnicity unknown' - DE7087
                                              THEN ''
                                         ELSE (
                                                SELECT DISTINCT
                                                        AgencyDescrip
                                                FROM    syRptAgencyFldValues
                                                WHERE   RptAgencyFldValId = t4.IPEDSValue
                                              )
                                    END AS Race
                                   ,(
                                      SELECT    COUNT(*)
                                      FROM      arStuEnrollments SQ1
                                               ,adDegCertSeeking SQ2
                                      WHERE     SQ1.StuEnrollId = t2.StuEnrollId
                                                AND 
						--SQ1.StartDate <= '01/01/2010' and 
                                                SQ1.degcertseekingid = SQ2.DegCertSeekingId
                                                AND SQ2.IPEDSValue = 11
                                    ) AS FirstTime
                                   ,NULL AS TransferIn
                                   ,(
                                      SELECT    COUNT(*)
                                      FROM      arStuEnrollments SQ1
                                               ,adDegCertSeeking SQ2
                                      WHERE     SQ1.StuEnrollId = t2.StuEnrollId
                                                AND 
						--SQ1.StartDate <= '01/01/2010' and 
                                                SQ1.degcertseekingid = SQ2.DegCertSeekingId
                                                AND SQ2.IPEDSValue = 10
                                    ) AS NonDegreeSeeking
                                   ,(
                                      SELECT    COUNT(*)
                                      FROM      arStuEnrollments SQ1
                                               ,adDegCertSeeking SQ2
                                               ,syStatusCodes SQ3
                                               ,sySysStatus SQ4
                                      WHERE     SQ1.StuEnrollId = t2.StuEnrollId
                                                AND 
						--SQ1.StartDate <= '01/01/2010' and 
                                                SQ1.degcertseekingid = SQ2.DegCertSeekingId
                                                AND SQ1.StatusCodeId = SQ3.StatusCodeId
                                                AND SQ3.SysStatusId = SQ4.SysStatusId
                                                AND SQ4.InSchool = 1
                                    ) AS Continuing
                                   ,(
                                      SELECT    COUNT(*)
                                      FROM      arStuEnrollments SQ1
                                               ,adDegCertSeeking SQ2
                                      WHERE     SQ1.StuEnrollId = t2.StuEnrollId
                                                AND 
						--SQ1.StartDate <= '01/01/2010' and 
                                                SQ1.degcertseekingid = SQ2.DegCertSeekingId
                                                AND SQ2.IPEDSValue = 12
                                    ) AS AllOther
                                   ,t3.IPEDSSequence AS GenderSequence
                                   ,t4.IPEDSSequence AS RaceSequence
                          FROM      adGenders t3
                          LEFT JOIN arStudent t1 ON t3.GenderId = t1.Gender
                          LEFT JOIN adEthCodes t4 ON t4.EthCodeId = t1.Race
                          INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                          INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                          INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                                       AND t6.SysStatusId NOT IN ( 7,8 )
                          INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                          INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                          INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                          LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                          WHERE     t2.CampusId = @CampusId
                                    AND (
                                          @ProgId IS NULL
                                          OR t8.ProgId IN ( SELECT  Val
                                                            FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                        )
                                    AND LOWER(t9.IPEDSValue) = 58
                                    AND LOWER(t10.IPEDSValue) = 61
                                    AND t3.IPEDSValue = 30
                                    AND t1.Race IS NOT NULL
                          UNION
                          SELECT    NULL AS SSN
                                   ,NULL AS StudentNumber
                                   ,NULL AS StudentName
                                   ,'Women' AS Gender
                                   ,CASE WHEN IPEDSValue IS NULL
                                   --THEN 'Race/ethnicity unknown' --DE9087
                                              THEN ''
                                         ELSE (
                                                SELECT DISTINCT
                                                        AgencyDescrip
                                                FROM    syRptAgencyFldValues
                                                WHERE   RptAgencyFldValId = IPEDSValue
                                              )
                                    END AS Race
                                   ,NULL AS FirstTime
                                   ,NULL AS TransferIn
                                   ,NULL AS NonDegreeSeeking
                                   ,NULL AS Continuing
                                   ,NULL AS AllOther
                                   ,2 AS GenderSequence
                                   ,IPEDSSequence AS RaceSequence
                          FROM      adEthCodes
                          WHERE     IPEDSValue IS NOT NULL
                                    AND EthCodeId NOT IN ( SELECT DISTINCT
                                                                    t1.Race
                                                           FROM     arStudent t1
                                                           INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                                                           INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                                                           INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                                                                        AND t6.SysStatusId NOT IN ( 7,8 )
                                                           INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                                                           INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                                                           INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                                                           LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                                                           INNER JOIN adGenders t11 ON t1.Gender = t11.GenderId
                                                           WHERE    t2.CampusId = @CampusId
                                                                    AND (
                                                                          @ProgId IS NULL
                                                                          OR t8.ProgId IN ( SELECT  Val
                                                                                            FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                                                        )
                                                                    AND LOWER(t9.IPEDSValue) = 58
                                                                    AND LOWER(t10.IPEDSValue) = 61
                                                                    AND t11.IPEDSValue = 31
                                                                    AND t1.Race IS NOT NULL )
                          UNION
                          SELECT    t1.SSN
                                   ,t1.StudentNumber
                                   ,t1.LastName + ', ' + t1.FirstName + ' ' + ISNULL(t1.MiddleName,'') AS StudentName
                                   ,CASE WHEN t3.IPEDSValue IS NULL THEN 'unknown'
                                         ELSE (
                                                SELECT DISTINCT
                                                        AgencyDescrip
                                                FROM    syRptAgencyFldValues
                                                WHERE   RptAgencyFldValId = t3.IPEDSValue
                                              )
                                    END AS Gender
                                   ,CASE WHEN t4.IPEDSValue IS NULL
                                        --THEN 'Race/ethnicity unknown'
                                              THEN '' --DE9087
                                         ELSE (
                                                SELECT DISTINCT
                                                        AgencyDescrip
                                                FROM    syRptAgencyFldValues
                                                WHERE   RptAgencyFldValId = t4.IPEDSValue
                                              )
                                    END AS Race
                                   ,(
                                      SELECT    COUNT(*)
                                      FROM      arStuEnrollments SQ1
                                               ,adDegCertSeeking SQ2
                                      WHERE     SQ1.StuEnrollId = t2.StuEnrollId
                                                AND 
						--SQ1.StartDate <= '01/01/2010' and 
                                                SQ1.degcertseekingid = SQ2.DegCertSeekingId
                                                AND SQ2.IPEDSValue = 11
                                    ) AS FirstTime
                                   ,NULL AS TransferIn
                                   ,(
                                      SELECT    COUNT(*)
                                      FROM      arStuEnrollments SQ1
                                               ,adDegCertSeeking SQ2
                                      WHERE     SQ1.StuEnrollId = t2.StuEnrollId
                                                AND 
						--SQ1.StartDate <= '01/01/2010' and 
                                                SQ1.degcertseekingid = SQ2.DegCertSeekingId
                                                AND SQ2.IPEDSValue = 10
                                    ) AS NonDegreeSeeking
                                   ,(
                                      SELECT    COUNT(*)
                                      FROM      arStuEnrollments SQ1
                                               ,adDegCertSeeking SQ2
                                               ,syStatusCodes SQ3
                                               ,sySysStatus SQ4
                                      WHERE     SQ1.StuEnrollId = t2.StuEnrollId
                                                AND 
						--SQ1.StartDate <= '01/01/2010' and 
                                                SQ1.degcertseekingid = SQ2.DegCertSeekingId
                                                AND SQ1.StatusCodeId = SQ3.StatusCodeId
                                                AND SQ3.SysStatusId = SQ4.SysStatusId
                                                AND SQ4.InSchool = 1
                                    ) AS Continuing
                                   ,(
                                      SELECT    COUNT(*)
                                      FROM      arStuEnrollments SQ1
                                               ,adDegCertSeeking SQ2
                                      WHERE     SQ1.StuEnrollId = t2.StuEnrollId
                                                AND 
						--SQ1.StartDate <= '01/01/2010' and 
                                                SQ1.degcertseekingid = SQ2.DegCertSeekingId
                                                AND SQ2.IPEDSValue = 12
                                    ) AS AllOther
                                   ,t3.IPEDSSequence AS GenderSequence
                                   ,t4.IPEDSSequence AS RaceSequence
                          FROM      adGenders t3
                          LEFT JOIN arStudent t1 ON t3.GenderId = t1.Gender
                          LEFT JOIN adEthCodes t4 ON t4.EthCodeId = t1.Race
                          INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                          INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                          INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                                       AND t6.SysStatusId NOT IN ( 7,8 )
                          INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                          INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                          INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                          LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                          WHERE     t2.CampusId = @CampusId
                                    AND (
                                          @ProgId IS NULL
                                          OR t8.ProgId IN ( SELECT  Val
                                                            FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                        )
                                    AND LOWER(t9.IPEDSValue) = 58
                                    AND LOWER(t10.IPEDSValue) = 61
                                    AND t3.IPEDSValue = 31
                                    AND t1.Race IS NOT NULL
                        ) dt
            ) dtGroup
    GROUP BY Gender
           ,Race
           ,GenderSequence
           ,RaceSequence
    ORDER BY GenderSequence
           ,RaceSequence;




GO
