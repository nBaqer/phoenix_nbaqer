SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[USP_GetStudentsWithMissingRequirementsAndRequirementGroup_Grad]
    (
     @CampusID UNIQUEIDENTIFIER
    ,@ProgID UNIQUEIDENTIFIER = NULL
    ,@ShiftID UNIQUEIDENTIFIER = NULL
    ,@StatusCodeID UNIQUEIDENTIFIER = NULL
    ,@LeadgrpID UNIQUEIDENTIFIER = NULL 

    )
AS
    BEGIN

        DECLARE @ActiveStatusID AS UNIQUEIDENTIFIER;

        SET @ActiveStatusID = (
                                SELECT  StatusId
                                FROM    syStatuses
                                WHERE   Status = 'Active'
                              ); 
        SELECT DISTINCT
                Descrip
               ,StudentId
        FROM    (
                  SELECT DISTINCT
                            adReqId
                           ,Descrip
                           ,adReqTypeId
                           ,CampGrpId
                  FROM      adReqs
                  WHERE     adReqTypeId IN ( 1,3 )
                            AND ReqforGraduation = 1
                            AND StatusId = @ActiveStatusID
                ) t1
               ,(
                  SELECT DISTINCT
                            StartDate
                           ,EndDate
                           ,MinScore
                           ,adReqId
                  FROM      adReqsEffectiveDates
                  WHERE     MandatoryRequirement = 1
                ) t2
               ,(
                  SELECT DISTINCT
                            DocumentId
                           ,DocStatusId
                           ,PL.StudentId
                           ,(
                              SELECT    MIN(SE.StartDate)
                              FROM      arStuEnrollments SE
                              WHERE     StudentId = PL.StudentId
                              GROUP BY  StudentId
                            ) AS StudentStartDate
                  FROM      plStudentDocs PL
                  WHERE     DocStatusId IN ( SELECT DISTINCT
                                                    DocStatusId
                                             FROM   syDocStatuses A2
                                                   ,sySysDocStatuses A3
                                                   ,syCmpGrpCmps A4
                                             WHERE  A2.SysDocStatusId = A3.SysDocStatusId
                                                    AND A2.CampGrpId = A4.CampGrpId
                                                    AND A4.CampusId = @CampusID
                                                    AND A3.SysDocStatusId = 2
                                                    AND A2.StatusId = @ActiveStatusID )
                ) t3
        WHERE   t1.adReqId = t2.adReqId
                AND t1.adReqId = t3.DocumentId
                AND StudentStartDate >= StartDate
                AND (
                      StudentStartDate <= EndDate
                      OR EndDate IS NULL
                    )
                AND t1.CampGrpId IN ( SELECT DISTINCT
                                                B2.CampGrpId
                                      FROM      syCmpGrpCmps B1
                                               ,syCampGrps B2
                                      WHERE     B1.CampGrpId = B2.CampGrpId
                                                AND B1.CampusId = @CampusID
                                                AND B2.StatusId = @ActiveStatusID )
        UNION
        SELECT DISTINCT
                Descrip
               ,StudentId
        FROM    (
                  SELECT DISTINCT
                            adReqId
                           ,Descrip
                           ,adReqTypeId
                           ,CampGrpId
                  FROM      adReqs
                  WHERE     adReqTypeId IN ( 1,3 )
                            AND ReqforGraduation = 1
                            AND StatusId = @ActiveStatusID
                ) t1
               ,(
                  SELECT DISTINCT
                            StartDate
                           ,EndDate
                           ,MinScore
                           ,adReqId
                  FROM      adReqsEffectiveDates
                  WHERE     MandatoryRequirement = 1
                ) t2
               ,(
                  SELECT    EntrTestId
                           ,ET.StudentId
                           ,(
                              SELECT    MIN(StartDate)
                              FROM      arStuEnrollments SE
                              WHERE     StudentId = ET.StudentId
                              GROUP BY  StudentId
                            ) AS StudentStartDate
                  FROM      adLeadEntranceTest ET
                  WHERE     Pass = 0
                            AND ET.StudentId IS NOT NULL
                ) t4
        WHERE   t1.adReqId = t2.adReqId
                AND t1.adReqId = t4.EntrTestId
                AND StudentStartDate >= StartDate
                AND (
                      StudentStartDate <= EndDate
                      OR EndDate IS NULL
                    )
                AND t1.CampGrpId IN ( SELECT DISTINCT
                                                B2.CampGrpId
                                      FROM      syCmpGrpCmps B1
                                               ,syCampGrps B2
                                      WHERE     B1.CampGrpId = B2.CampGrpId
                                                AND B1.CampusId = @CampusID
                                                AND B2.StatusId = @ActiveStatusID )
        UNION
        SELECT DISTINCT
                Descrip
               ,StudentId
        FROM    (
                  SELECT DISTINCT
                            adReqId
                           ,Descrip
                           ,adReqTypeId
                           ,CampGrpId
                  FROM      adReqs
                  WHERE     adReqTypeId IN ( 1,3 )
                            AND ReqforGraduation = 1
                            AND StatusId = @ActiveStatusID
                ) t1
               ,(
                  SELECT DISTINCT
                            StartDate
                           ,EndDate
                           ,MinScore
                           ,adReqId
                  FROM      adReqsEffectiveDates
                  WHERE     MandatoryRequirement = 1
                ) t2
               ,(
                  SELECT DISTINCT
                            EntrTestId
                           ,ETO.StudentId
                           ,(
                              SELECT    MIN(StartDate)
                              FROM      arStuEnrollments
                              WHERE     StudentId = ETO.StudentId
                              GROUP BY  StudentId
                            ) AS StudentStartDate
                  FROM      adEntrTestOverRide ETO
                  WHERE     OverRide = 1
                            AND ETO.StudentId IS NOT NULL
                ) t4
        WHERE   t1.adReqId = t2.adReqId
                AND t1.adReqId = t4.EntrTestId
                AND StudentStartDate >= StartDate
                AND (
                      StudentStartDate <= EndDate
                      OR EndDate IS NULL
                    )
                AND t1.CampGrpId IN ( SELECT DISTINCT
                                                B2.CampGrpId
                                      FROM      syCmpGrpCmps B1
                                               ,syCampGrps B2
                                      WHERE     B1.CampGrpId = B2.CampGrpId
                                                AND B1.CampusId = @CampusID
                                                AND B2.StatusId = @ActiveStatusID )
        UNION
        SELECT DISTINCT
                Descrip
               ,StudentId
        FROM    (
                  SELECT DISTINCT
                            adReqId
                           ,Descrip
                           ,adReqTypeId
                           ,CampGrpId
                  FROM      adReqs
                  WHERE     adReqTypeId IN ( 1,3 )
                            AND ReqforGraduation = 1
                            AND StatusId = @ActiveStatusID
                ) t1
               ,(
                  SELECT DISTINCT
                            StartDate
                           ,EndDate
                           ,MinScore
                           ,adReqId
                  FROM      adReqsEffectiveDates
                ) t2
               ,(
                  SELECT DISTINCT
                            adReqId
                  FROM      adPrgVerTestDetails t1
                           ,arStuEnrollments t2
                           --,arStudent t3
                  WHERE     t1.PrgVerId = t2.PrgVerId
                           -- AND t2.StudentId = t3.StudentId
                            AND ReqGrpId IS NULL
                            AND t2.PrgVerId IN ( SELECT DISTINCT
                                                        PrgVerId
                                                 FROM   arPrgVersions
                                                 WHERE  ProgId = @ProgID
                                                        OR @ProgID IS NULL )
                            AND (
                                  t2.ShiftId = @ShiftID
                                  OR @ShiftID IS NULL
                                )
                            AND (
                                  t2.StatusCodeId = @StatusCodeID
                                  OR @StatusCodeID IS NULL
                                )
                            AND t1.adReqId NOT IN ( SELECT DISTINCT
                                                            s1.adReqId
                                                    FROM    adReqGrpDef s1
                                                           ,adPrgVerTestDetails s2
                                                           ,arStuEnrollments s3
                                                         --  ,arStudent s4
                                                    WHERE   s1.ReqGrpId = s2.ReqGrpId
                                                            AND s2.PrgVerId = s3.PrgVerId
                                                     --      AND s3.StudentId = s4.StudentId
                                                            AND s2.adReqId IS NULL
                                                            AND s3.PrgVerId IN ( SELECT DISTINCT
                                                                                        PrgVerId
                                                                                 FROM   arPrgVersions
                                                                                 WHERE  (
                                                                                          ProgId = @ProgID
                                                                                          OR @ProgID IS NULL
                                                                                        ) )
                                                            AND (
                                                                  s3.ShiftId = @ShiftID
                                                                  OR @ShiftID IS NULL
                                                                )
                                                            AND (
                                                                  s3.StatusCodeId = @StatusCodeID
                                                                  OR @StatusCodeID IS NULL
                                                                ) )
                ) t5
               ,(
                  SELECT DISTINCT
                            DocumentId
                           ,DocStatusId
                           ,StudentId
                           ,(
                              SELECT    MIN(StartDate)
                              FROM      arStuEnrollments
                              WHERE     StudentId = SD.StudentId
                              GROUP BY  StudentId
                            ) AS StudentStartDate
                  FROM      plStudentDocs SD
                  WHERE     DocStatusId IN ( SELECT DISTINCT
                                                    DocStatusId
                                             FROM   syDocStatuses A2
                                                   ,sySysDocStatuses A3
                                                   ,syCmpGrpCmps A4
                                             WHERE  A2.SysDocStatusId = A3.SysDocStatusId
                                                    AND A2.CampGrpId = A4.CampGrpId
                                                    AND A4.CampusId = @CampusID
                                                    AND A3.SysDocStatusId = 2
                                                    AND A2.StatusId = @ActiveStatusID )
                ) t6
        WHERE   t1.adReqId = t2.adReqId
                AND t1.adReqId = t5.adReqId
                AND t1.adReqId = t6.DocumentId
                AND StudentStartDate >= StartDate
                AND (
                      StudentStartDate <= EndDate
                      OR EndDate IS NULL
                    )
                AND t1.CampGrpId IN ( SELECT DISTINCT
                                                B2.CampGrpId
                                      FROM      syCmpGrpCmps B1
                                               ,syCampGrps B2
                                      WHERE     B1.CampGrpId = B2.CampGrpId
                                                AND B1.CampusId = @CampusID
                                                AND B2.StatusId = @ActiveStatusID )
        UNION
        SELECT DISTINCT
                Descrip
               ,StudentId
        FROM    (
                  SELECT DISTINCT
                            adReqId
                           ,Descrip
                           ,adReqTypeId
                           ,CampGrpId
                  FROM      adReqs
                  WHERE     adReqTypeId IN ( 1,3 )
                            AND ReqforGraduation = 1
                            AND StatusId = @ActiveStatusID
                ) t1
               ,(
                  SELECT DISTINCT
                            StartDate
                           ,EndDate
                           ,MinScore
                           ,adReqId
                  FROM      adReqsEffectiveDates
                ) t2
               ,(
                  SELECT DISTINCT
                            adReqId
                  FROM      adPrgVerTestDetails t1
                           ,arStuEnrollments t2
                     --      ,arStudent t3
                  WHERE     t1.PrgVerId = t2.PrgVerId
                      --      AND t2.StudentId = t3.StudentId
                            AND ReqGrpId IS NULL
                            AND t2.PrgVerId IN ( SELECT DISTINCT
                                                        PrgVerId
                                                 FROM   arPrgVersions
                                                 WHERE  (
                                                          ProgId = @ProgID
                                                          OR @ProgID IS NULL
                                                        ) )
                            AND (
                                  t2.ShiftId = @ShiftID
                                  OR @ShiftID IS NULL
                                )
                            AND (
                                  t2.StatusCodeId = @StatusCodeID
                                  OR @StatusCodeID IS NULL
                                )
                            AND t1.adReqId NOT IN ( SELECT DISTINCT
                                                            s1.adReqId
                                                    FROM    adReqGrpDef s1
                                                           ,adPrgVerTestDetails s2
                                                           ,arStuEnrollments s3
                                                    --       ,arStudent s4
                                                    WHERE   s1.ReqGrpId = s2.ReqGrpId
                                                            AND s2.PrgVerId = s3.PrgVerId
                                                    --        AND s3.StudentId = s4.StudentId
                                                            AND s2.adReqId IS NULL
                                                            AND s3.PrgVerId IN ( SELECT DISTINCT
                                                                                        PrgVerId
                                                                                 FROM   arPrgVersions
                                                                                 WHERE  ProgId = @ProgID
                                                                                        OR @ProgID IS NULL )
                                                            AND (
                                                                  s3.ShiftId = @ShiftID
                                                                  OR @ShiftID IS NULL
                                                                )
                                                            AND (
                                                                  s3.StatusCodeId = @StatusCodeID
                                                                  OR @StatusCodeID IS NULL
                                                                ) )
                ) t5
               ,(
                  SELECT DISTINCT
                            EntrTestId
                           ,StudentId
                           ,(
                              SELECT    MIN(StartDate)
                              FROM      dbo.arStuEnrollments
                              WHERE     StudentId = LT.StudentId
                              GROUP BY  StudentId
                            ) AS StudentStartDate
                  FROM      adLeadEntranceTest LT
                  WHERE     Pass = 0
                            AND StudentId IS NOT NULL
                ) t7
        WHERE   t1.adReqId = t2.adReqId
                AND t1.adReqId = t5.adReqId
                AND t1.adReqId = t7.EntrTestId
                AND StudentStartDate >= StartDate
                AND (
                      StudentStartDate <= EndDate
                      OR EndDate IS NULL
                    )
                AND t1.CampGrpId IN ( SELECT DISTINCT
                                                B2.CampGrpId
                                      FROM      syCmpGrpCmps B1
                                               ,syCampGrps B2
                                      WHERE     B1.CampGrpId = B2.CampGrpId
                                                AND B1.CampusId = @CampusID
                                                AND B2.StatusId = @ActiveStatusID )
        UNION
        SELECT DISTINCT
                Descrip
               ,StudentId
        FROM    (
                  SELECT DISTINCT
                            adReqId
                           ,Descrip
                           ,adReqTypeId
                           ,CampGrpId
                  FROM      adReqs
                  WHERE     adReqTypeId IN ( 1,3 )
                            AND ReqforGraduation = 1
                            AND StatusId = @ActiveStatusID
                ) t1
               ,(
                  SELECT DISTINCT
                            StartDate
                           ,EndDate
                           ,MinScore
                           ,adReqId
                  FROM      adReqsEffectiveDates
                ) t2
               ,(
                  SELECT DISTINCT
                            adReqId
                  FROM      adPrgVerTestDetails t1
                           ,arStuEnrollments t2
                        --   ,arStudent t3
                  WHERE     t1.PrgVerId = t2.PrgVerId
                       --     AND t2.StudentId = t3.StudentId
                            AND ReqGrpId IS NULL
                            AND ( t2.PrgVerId IN ( SELECT DISTINCT
                                                            PrgVerId
                                                   FROM     arPrgVersions
                                                   WHERE    ProgId = @ProgID
                                                            OR @ProgID IS NULL ) )
                            AND (
                                  t2.ShiftId = @ShiftID
                                  OR @ShiftID IS NULL
                                )
                            AND t2.StatusCodeId = @StatusCodeID
                            OR @StatusCodeID IS NULL
                            AND t1.adReqId NOT IN ( SELECT DISTINCT
                                                            s1.adReqId
                                                    FROM    adReqGrpDef s1
                                                           ,adPrgVerTestDetails s2
                                                           ,arStuEnrollments s3
                                                --           ,arStudent s4
                                                    WHERE   s1.ReqGrpId = s2.ReqGrpId
                                                            AND s2.PrgVerId = s3.PrgVerId
                                                  --          AND s3.StudentId = s4.StudentId
                                                            AND s2.adReqId IS NULL
                                                            AND s3.PrgVerId IN ( SELECT DISTINCT
                                                                                        PrgVerId
                                                                                 FROM   arPrgVersions
                                                                                 WHERE  ProgId = @ProgID
                                                                                        AND @ProgID IS NULL )
                                                            AND (
                                                                  s3.ShiftId = @ShiftID
                                                                  OR @ShiftID IS NULL
                                                                )
                                                            AND (
                                                                  s3.StatusCodeId = @StatusCodeID
                                                                  OR @StatusCodeID IS NULL
                                                                ) )
                ) t5
               ,(
                  SELECT DISTINCT
                            EntrTestId
                           ,StudentId
                           ,(
                              SELECT    MIN(StartDate)
                              FROM      arStuEnrollments
                              WHERE     StudentId = ETO.StudentId
                              GROUP BY  StudentId
                            ) AS StudentStartDate
                  FROM      adEntrTestOverRide ETO
                  WHERE     OverRide = 1
                            AND StudentId IS NOT NULL
                ) t7
        WHERE   t1.adReqId = t2.adReqId
                AND t1.adReqId = t5.adReqId
                AND t1.adReqId = t7.EntrTestId
                AND StudentStartDate >= StartDate
                AND (
                      StudentStartDate <= EndDate
                      OR EndDate IS NULL
                    )
                AND t1.CampGrpId IN ( SELECT DISTINCT
                                                B2.CampGrpId
                                      FROM      syCmpGrpCmps B1
                                               ,syCampGrps B2
                                      WHERE     B1.CampGrpId = B2.CampGrpId
                                                AND B1.CampusId = @CampusID
                                                AND B2.StatusId = @ActiveStatusID )
        UNION
        SELECT DISTINCT
                Descrip
               ,StudentId
        FROM    (
                  SELECT DISTINCT
                            adReqId
                           ,Descrip
                           ,adReqTypeId
                           ,CampGrpId
                  FROM      adReqs
                  WHERE     adReqTypeId IN ( 1,3 )
                            AND ReqforGraduation = 1
                            AND StatusId = @ActiveStatusID
                ) t1
               ,(
                  SELECT DISTINCT
                            StartDate
                           ,EndDate
                           ,MinScore
                           ,adReqId
                           ,adReqEffectiveDateId
                  FROM      adReqsEffectiveDates
                ) t2
               ,adReqLeadGroups t3
               ,(
                  SELECT DISTINCT
                            DocumentId
                           ,DocStatusId
                           ,StudentId
                           ,(
                              SELECT    MIN(StartDate)
                              FROM      arStuEnrollments
                              WHERE     StudentId = SD.StudentId
                              GROUP BY  StudentId
                            ) AS StudentStartDate
                  FROM      plStudentDocs SD
                  WHERE     DocStatusId IN ( SELECT DISTINCT
                                                    DocStatusId
                                             FROM   syDocStatuses A2
                                                   ,sySysDocStatuses A3
                                                   ,syCmpGrpCmps A4
                                             WHERE  A2.SysDocStatusId = A3.SysDocStatusId
                                                    AND A2.CampGrpId = A4.CampGrpId
                                                    AND A4.CampusId = @CampusID
                                                    AND A3.SysDocStatusId = 2
                                                    AND A2.StatusId = @ActiveStatusID )
                ) t4
        WHERE   t1.adReqId = t2.adReqId
                AND t2.adReqEffectiveDateId = t3.adReqEffectiveDateId
                AND t1.adReqId NOT IN ( SELECT DISTINCT
                                                s1.adReqId
                                        FROM    adReqGrpDef s1
                                               ,adPrgVerTestDetails s2
                                               ,arStuEnrollments s3
                                         --      ,arStudent s4
                                        WHERE   s1.ReqGrpId = s2.ReqGrpId
                                                AND s2.PrgVerId = s3.PrgVerId
                                         --       AND s3.StudentId = s4.StudentId
                                                AND s2.adReqId IS NULL
                                                AND s3.PrgVerId IN ( SELECT DISTINCT
                                                                            PrgVerId
                                                                     FROM   arPrgVersions
                                                                     WHERE  ProgId = @ProgID
                                                                            OR @ProgID IS NULL )
                                                AND (
                                                      s3.ShiftId = @ShiftID
                                                      OR @ShiftID IS NULL
                                                    )
                                                AND (
                                                      s3.StatusCodeId = @StatusCodeID
                                                      OR @StatusCodeID IS NULL
                                                    )
                                                AND t3.LeadGrpId IN ( SELECT DISTINCT
                                                                                LeadGrpId
                                                                      FROM      adLeadByLeadGroups t1
                                                                               ,arStuEnrollments t2
                                                                --               ,arStudent t3
                                                                      WHERE     t1.StuEnrollId = t2.StuEnrollId
                                                                 --               AND t2.StudentId = t3.StudentId
                                                                                AND t2.PrgVerId IN ( SELECT DISTINCT
                                                                                                            PrgVerId
                                                                                                     FROM   arPrgVersions
                                                                                                     WHERE  ProgId = @ProgID
                                                                                                            OR @ProgID IS NULL )
                                                                                AND (
                                                                                      t2.ShiftId = @ShiftID
                                                                                      OR @ShiftID IS NULL
                                                                                    )
                                                                                AND t2.StatusCodeId = @StatusCodeID
                                                                                OR @StatusCodeID IS NULL ) )
                AND t1.adReqId NOT IN ( SELECT DISTINCT
                                                adReqId
                                        FROM    adPrgVerTestDetails t1
                                               ,arStuEnrollments t2
                                           --    ,arStudent t3
                                        WHERE   t1.PrgVerId = t2.PrgVerId
                                          --      AND t2.StudentId = t3.StudentId
                                                AND ReqGrpId IS NOT NULL
                                                AND t2.PrgVerId IN ( SELECT DISTINCT
                                                                            PrgVerId
                                                                     FROM   arPrgVersions
                                                                     WHERE  ProgId = @ProgID
                                                                            OR @ProgID IS NULL )
                                                AND (
                                                      t2.ShiftId = @ShiftID
                                                      OR @ShiftID IS NULL
                                                    )
                                                AND t2.StatusCodeId = @StatusCodeID
                                                OR @StatusCodeID IS NULL )
                AND StudentStartDate >= StartDate
                AND (
                      StudentStartDate <= EndDate
                      OR EndDate IS NULL
                    )
                AND t1.CampGrpId IN ( SELECT DISTINCT
                                                B2.CampGrpId
                                      FROM      syCmpGrpCmps B1
                                               ,syCampGrps B2
                                      WHERE     B1.CampGrpId = B2.CampGrpId
                                                AND B1.CampusId = @CampusID
                                                AND B2.StatusId = @ActiveStatusID )
                AND t3.IsRequired = 1
                AND t1.adReqId = t4.DocumentId
        UNION
        SELECT DISTINCT
                Descrip
               ,StudentId
        FROM    (
                  SELECT DISTINCT
                            adReqId
                           ,Descrip
                           ,adReqTypeId
                           ,CampGrpId
                  FROM      adReqs
                  WHERE     adReqTypeId IN ( 1,3 )
                            AND ReqforGraduation = 1
                            AND StatusId = @ActiveStatusID
                ) t1
               ,(
                  SELECT DISTINCT
                            StartDate
                           ,EndDate
                           ,MinScore
                           ,adReqId
                           ,adReqEffectiveDateId
                  FROM      adReqsEffectiveDates
                ) t2
               ,adReqLeadGroups t3
               ,(
                  SELECT    EntrTestId
                           ,StudentId
                           ,(
                              SELECT    MIN(StartDate)
                              FROM      arStuEnrollments
                              WHERE     StudentId = ET.StudentId
                              GROUP BY  StudentId
                            ) AS StudentStartDate
                  FROM      adLeadEntranceTest ET
                  WHERE     Pass = 0
                            AND StudentId IS NOT NULL
                ) t5
        WHERE   t1.adReqId = t2.adReqId
                AND t2.adReqEffectiveDateId = t3.adReqEffectiveDateId
                AND t1.adReqId NOT IN ( SELECT DISTINCT
                                                s1.adReqId
                                        FROM    adReqGrpDef s1
                                               ,adPrgVerTestDetails s2
                                               ,arStuEnrollments s3
                                         --      ,arStudent s4
                                        WHERE   s1.ReqGrpId = s2.ReqGrpId
                                                AND s2.PrgVerId = s3.PrgVerId
                                         --       AND s3.StudentId = s4.StudentId
                                                AND s2.adReqId IS NULL
                                                AND s3.PrgVerId IN ( SELECT DISTINCT
                                                                            PrgVerId
                                                                     FROM   arPrgVersions
                                                                     WHERE  ProgId = @ProgID
                                                                            OR @ProgID IS NULL )
                                                AND (
                                                      s3.ShiftId = @ShiftID
                                                      OR @ShiftID IS NULL
                                                    )
                                                AND (
                                                      s3.StatusCodeId = @StatusCodeID
                                                      OR @StatusCodeID IS NULL
                                                    ) )
                AND t3.LeadGrpId IN ( SELECT DISTINCT
                                                LeadGrpId
                                      FROM      adLeadByLeadGroups t1
                                               ,arStuEnrollments t2
                                         --      ,arStudent t3
                                      WHERE     t1.StuEnrollId = t2.StuEnrollId
                                         --       AND t2.StudentId = t3.StudentId 
												)
                AND t1.adReqId NOT IN ( SELECT DISTINCT
                                                adReqId
                                        FROM    adPrgVerTestDetails t1
                                               ,arStuEnrollments t2
                                        --       ,arStudent t3
                                        WHERE   t1.PrgVerId = t2.PrgVerId
                                         --       AND t2.StudentId = t3.StudentId
                                                AND ReqGrpId IS NULL
                                                AND t2.PrgVerId IN ( SELECT DISTINCT
                                                                            PrgVerId
                                                                     FROM   arPrgVersions
                                                                     WHERE  ProgId = @ProgID
                                                                            OR @ProgID IS NULL )
                                                AND (
                                                      t2.ShiftId = @ShiftID
                                                      OR @ShiftID IS NULL
                                                    )
                                                AND (
                                                      t2.StatusCodeId = @StatusCodeID
                                                      OR @StatusCodeID IS NULL
                                                    ) )
                AND StudentStartDate >= StartDate
                AND (
                      StudentStartDate <= EndDate
                      OR EndDate IS NULL
                    )
                AND t1.CampGrpId IN ( SELECT DISTINCT
                                                B2.CampGrpId
                                      FROM      syCmpGrpCmps B1
                                               ,syCampGrps B2
                                      WHERE     B1.CampGrpId = B2.CampGrpId
                                                AND B1.CampusId = @CampusID
                                                AND B2.StatusId = @ActiveStatusID )
                AND t3.IsRequired = 1
                AND t1.adReqId = t5.EntrTestId
        UNION
        SELECT DISTINCT
                Descrip
               ,StudentId
        FROM    (
                  SELECT DISTINCT
                            adReqId
                           ,Descrip
                           ,adReqTypeId
                           ,CampGrpId
                  FROM      adReqs
                  WHERE     adReqTypeId IN ( 1,3 )
                            AND ReqforGraduation = 1
                            AND StatusId = @ActiveStatusID
                ) t1
               ,(
                  SELECT DISTINCT
                            StartDate
                           ,EndDate
                           ,MinScore
                           ,adReqId
                           ,adReqEffectiveDateId
                  FROM      adReqsEffectiveDates
                ) t2
               ,adReqLeadGroups t3
               ,(
                  SELECT DISTINCT
                            EntrTestId
                           ,StudentId
                           ,(
                              SELECT    MIN(StartDate)
                              FROM      arStuEnrollments
                              WHERE     StudentId = ETO.StudentId
                              GROUP BY  StudentId
                            ) AS StudentStartDate
                  FROM      adEntrTestOverRide ETO
                  WHERE     OverRide = 1
                            AND StudentId IS NOT NULL
                ) t5
        WHERE   t1.adReqId = t2.adReqId
                AND t2.adReqEffectiveDateId = t3.adReqEffectiveDateId
                AND t1.adReqId NOT IN ( SELECT DISTINCT
                                                s1.adReqId
                                        FROM    adReqGrpDef s1
                                               ,adPrgVerTestDetails s2
                                               ,arStuEnrollments s3
                                           --    ,arStudent s4
                                        WHERE   s1.ReqGrpId = s2.ReqGrpId
                                                AND s2.PrgVerId = s3.PrgVerId
                                           --     AND s3.StudentId = s4.StudentId
                                                AND s2.adReqId IS NULL
                                                AND s3.PrgVerId IN ( SELECT DISTINCT
                                                                            PrgVerId
                                                                     FROM   arPrgVersions
                                                                     WHERE  ProgId = @ProgID
                                                                            OR @ProgID IS NULL )
                                                AND (
                                                      s3.ShiftId = @ShiftID
                                                      OR @ShiftID IS NULL
                                                    )
                                                AND (
                                                      s3.StatusCodeId = @StatusCodeID
                                                      OR @StatusCodeID IS NULL
                                                    ) )
                AND t3.LeadGrpId IN ( SELECT DISTINCT
                                                LeadGrpId
                                      FROM      adLeadByLeadGroups t1
                                               ,arStuEnrollments t2
                                       --        ,arStudent t3
                                      WHERE     t1.StuEnrollId = t2.StuEnrollId
                                        --        AND t2.StudentId = t3.StudentId
												 )
                AND t1.adReqId NOT IN ( SELECT DISTINCT
                                                adReqId
                                        FROM    adPrgVerTestDetails t1
                                               ,arStuEnrollments t2
                                          --     ,arStudent t3
                                        WHERE   t1.PrgVerId = t2.PrgVerId
                                         --       AND t2.StudentId = t3.StudentId
                                                AND ReqGrpId IS NULL
                                                AND t2.PrgVerId IN ( SELECT DISTINCT
                                                                            PrgVerId
                                                                     FROM   arPrgVersions
                                                                     WHERE  ProgId = @ProgID
                                                                            OR @ProgID IS NULL )
                                                AND (
                                                      t2.ShiftId = @ShiftID
                                                      OR @ShiftID IS NULL
                                                    )
                                                AND (
                                                      t2.StatusCodeId = @StatusCodeID
                                                      OR @StatusCodeID IS NULL
                                                    ) )
                AND StudentStartDate >= StartDate
                AND (
                      StudentStartDate <= EndDate
                      OR EndDate IS NULL
                    )
                AND t1.CampGrpId IN ( SELECT DISTINCT
                                                B2.CampGrpId
                                      FROM      syCmpGrpCmps B1
                                               ,syCampGrps B2
                                      WHERE     B1.CampGrpId = B2.CampGrpId
                                                AND B1.CampusId = @CampusID
                                                AND B2.StatusId = @ActiveStatusID )
                AND t3.IsRequired = 1
                AND t1.adReqId = t5.EntrTestId
        UNION
        SELECT DISTINCT
                Descrip
               ,StudentId
        FROM    (
                  SELECT    ReqGrpId
                           ,R5.Descrip
                           ,R5.Numreqs
                           ,ISNULL(R5.TestPassed,0) + ISNULL(R5.DocsApproved,0) AS AttemptedReqs
                           ,CASE WHEN LeadGrpId IS NULL THEN '00000000-0000-0000-0000-000000000000'
                                 ELSE LeadGrpId
                            END AS LeadGrpId
                           ,StudentId
                  FROM      (
                              SELECT  DISTINCT
                                        t1.ReqGrpId
                                       ,t2.Descrip
                                       ,t2.CampGrpId
                                       ,t3.NumReqs AS Numreqs
                                       ,t6.StudentId
                                       ,(
                                          SELECT    COUNT(*)
                                          FROM      (
                                                      SELECT DISTINCT
                                                                A2.adReqId
                                                               ,GETDATE() AS CurrentDate
                                                               ,A3.StartDate
                                                               ,A3.EndDate
                                                               ,A4.LeadGrpId
                                                      FROM      adReqGrpDef A1
                                                               ,adReqs A2
                                                               ,adReqsEffectiveDates A3
                                                               ,adReqLeadGroups A4
                                                      WHERE     A1.adReqId = A2.adReqId
                                                                AND A2.adReqId = A3.adReqId
                                                                AND A3.adReqEffectiveDateId = A4.adReqEffectiveDateId
                                                                AND A4.LeadGrpId IN ( SELECT DISTINCT
                                                                                                LeadGrpId
                                                                                      FROM      adLeadByLeadGroups t1
                                                                                               ,arStuEnrollments t2
                                                                                       --        ,arStudent t3
                                                                                      WHERE     t1.StuEnrollId = t2.StuEnrollId
                                                                                       --         AND t2.StudentId = t3.StudentId
                                                                                                AND (
                                                                                                      t1.LeadGrpId = @LeadgrpID
                                                                                                      OR @LeadgrpID IS NULL
                                                                                                    ) )
                                                                AND ReqGrpId = t1.ReqGrpId
                                                                AND A2.adReqTypeId = 1
                                                                AND ReqforGraduation = 1
                                                                AND StatusId = @ActiveStatusID
                                                    ) R1
                                                   ,adLeadEntranceTest R2
                                          WHERE     R1.adReqId = R2.EntrTestId
                                                    AND R2.StudentId = t6.StudentId
                                                    AND R1.CurrentDate >= R1.StartDate
                                                    AND LEN(R2.TestTaken) >= 4
                                                    AND R2.Pass = 1
                                                    AND R2.EntrTestId NOT IN ( SELECT DISTINCT
                                                                                        EntrTestId
                                                                               FROM     adEntrTestOverRide t1
                                                                               WHERE    t1.StudentId = t6.StudentId
                                                                                        AND OverRide = 1 )
                                                    AND (
                                                          R1.CurrentDate <= R1.EndDate
                                                          OR R1.EndDate IS NULL
                                                        )
                                        ) AS TestPassed
                                       ,(
                                          SELECT    COUNT(*)
                                          FROM      (
                                                      SELECT DISTINCT
                                                                A2.adReqId
                                                               ,GETDATE() AS CurrentDate
                                                               ,A3.StartDate
                                                               ,A3.EndDate
                                                               ,A4.LeadGrpId
                                                      FROM      adReqGrpDef A1
                                                               ,adReqs A2
                                                               ,adReqsEffectiveDates A3
                                                               ,adReqLeadGroups A4
                                                      WHERE     A1.adReqId = A2.adReqId
                                                                AND A2.adReqId = A3.adReqId
                                                                AND A3.adReqEffectiveDateId = A4.adReqEffectiveDateId
                                                                AND A4.LeadGrpId IN ( SELECT DISTINCT
                                                                                                LeadGrpId
                                                                                      FROM      adLeadByLeadGroups t1
                                                                                               ,arStuEnrollments t2
                                                                                        --       ,arStudent t3
                                                                                      WHERE     t1.StuEnrollId = t2.StuEnrollId
                                                                                       --         AND t2.StudentId = t3.StudentId
                                                                                                AND (
                                                                                                      t1.LeadGrpId = @LeadgrpID
                                                                                                      OR @LeadgrpID IS NULL
                                                                                                    ) )
                                                                AND ReqGrpId = t1.ReqGrpId
                                                                AND A2.adReqTypeId = 3
                                                                AND A2.ReqforGraduation = 1
                                                                AND StatusId = @ActiveStatusID
                                                    ) R1
                                                   ,plStudentDocs R2
                                                   ,syDocStatuses R3
                                          WHERE     R1.adReqId = R2.DocumentId
                                                    AND R2.StudentId = t6.StudentId
                                                    AND R2.DocumentId NOT IN ( SELECT DISTINCT
                                                                                        EntrTestId
                                                                               FROM     adEntrTestOverRide t1
                                                                               WHERE    t1.StudentId = t6.StudentId
                                                                                        AND OverRide = 1 )
                                                    AND R2.DocStatusId = R3.DocStatusId
                                                    AND R3.SysDocStatusId = 1
                                                    AND R1.CurrentDate >= R1.StartDate
                                                    AND (
                                                          R1.CurrentDate <= R1.EndDate
                                                          OR R1.EndDate IS NULL
                                                        )
                                        ) AS DocsApproved
                                       ,t4.LeadGrpId
                              FROM      adPrgVerTestDetails t1
                                       ,adReqGroups t2
                                       ,adLeadGrpReqGroups t3
                                       ,adLeadGroups t4
                                       ,arStuEnrollments t5
                                       ,arStudent t6
                              WHERE     t1.ReqGrpId = t2.ReqGrpId
                                        AND t2.ReqGrpId = t3.ReqGrpId
                                        AND t3.LeadGrpId = t4.LeadGrpId
                                        AND t1.PrgVerId = t5.PrgVerId
                                        AND t5.StudentId = t6.StudentId
                                        AND t5.PrgVerId IN ( SELECT DISTINCT
                                                                    PrgVerId
                                                             FROM   arPrgVersions
                                                             WHERE  ProgId = @ProgID
                                                                    OR @ProgID IS NULL )
                                        AND (
                                              t5.ShiftId = @ShiftID
                                              OR @ShiftID IS NULL
                                            )
                                        AND (
                                              t5.StatusCodeId = @StatusCodeID
                                              OR @StatusCodeID IS NULL
                                            )
                                        AND t2.IsMandatoryReqGrp <> 1
                                        AND t3.LeadGrpId IN ( SELECT DISTINCT
                                                                        LeadGrpId
                                                              FROM      adLeadByLeadGroups t1
                                                                       ,arStuEnrollments t2
                                                                 --      ,arStudent t3
                                                              WHERE     t1.StuEnrollId = t2.StuEnrollId
                                                                 --       AND t2.StudentId = t3.StudentId
                                                                        AND (
                                                                              t1.LeadGrpId = @LeadgrpID
                                                                              OR @LeadgrpID IS NULL
                                                                            ) )
                            ) R5
                  WHERE     R5.CampGrpId IN ( SELECT DISTINCT
                                                        B2.CampGrpId
                                              FROM      syCmpGrpCmps B1
                                                       ,syCampGrps B2
                                              WHERE     B1.CampGrpId = B2.CampGrpId
                                                        AND B1.CampusId = @CampusID
                                                        AND B2.StatusId = @ActiveStatusID )
                ) R8
        WHERE   (
                  AttemptedReqs < Numreqs
                  OR (
                       SELECT   COUNT(*)
                       FROM     adReqGrpDef
                       WHERE    ReqGrpId = R8.ReqGrpId
                                AND LeadGrpId IN ( SELECT   LeadGrpId
                                                   FROM     adLeadByLeadGroups
                                                   WHERE    StuEnrollId IN ( SELECT DISTINCT
                                                                                    StuEnrollId
                                                                             FROM   arStuEnrollments
                                                                             WHERE  StudentId = R8.StudentId ) )
                                AND IsRequired = 1
                                AND (
                                      adReqId IN ( SELECT DISTINCT
                                                            EntrTestId
                                                   FROM     adLeadEntranceTest
                                                   WHERE    StudentId = R8.StudentId
                                                            AND Pass = 0
                                                            AND EntrTestId NOT IN ( SELECT  EntrTestId
                                                                                    FROM    adEntrTestOverRide
                                                                                    WHERE   StudentId = R8.StudentId
                                                                                            AND OverRide = 1 ) )
                                      OR adReqId IN ( SELECT DISTINCT
                                                                DocumentId
                                                      FROM      plStudentDocs t1
                                                               ,syDocStatuses t2
                                                      WHERE     t1.StudentId = R8.StudentId
                                                                AND t1.DocStatusId = t2.DocStatusId
                                                                AND t2.SysDocStatusId <> 1
                                                                AND DocumentId NOT IN ( SELECT  EntrTestId
                                                                                        FROM    adEntrTestOverRide
                                                                                        WHERE   StudentId = R8.StudentId
                                                                                                AND OverRide = 1 ) )
                                      OR adReqId IN ( SELECT DISTINCT
                                                                EntrTestId
                                                      FROM      adEntrTestOverRide
                                                      WHERE     StudentId = R8.StudentId
                                                                AND OverRide = 1 )
                                    )
                     ) >= 1
                );
    END;




GO
