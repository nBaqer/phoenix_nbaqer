SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[USP_PostedServicesByTermandCourse_GetList]
    (
     @StuEnrollId UNIQUEIDENTIFIER
    ,@TermId UNIQUEIDENTIFIER
    ,@CourseId UNIQUEIDENTIFIER
    )
AS
    SET NOCOUNT ON;
    

    DECLARE @ClsSectionId AS UNIQUEIDENTIFIER;   
    SELECT TOP 1
            @ClsSectionId = cs.ClsSectionId
    FROM    arClassSections cs
    JOIN    arResults ar ON ar.TestId = cs.ClsSectionId
    WHERE   ar.StuEnrollId = @StuEnrollId
            AND cs.TermId = @TermId
            AND cs.ReqId = @CourseId
    ORDER BY cs.StartDate DESC;
        
    SELECT  *
    INTO    #PostedServices
    FROM    arGrdBkResults
    WHERE   StuEnrollId = @StuEnrollId
            AND ClsSectionId = @ClsSectionId
            AND InstrGrdBkWgtDetailId IN ( SELECT   InstrGrdBkWgtDetailId
                                           FROM     arGrdBkWgtDetails posted_gbwd
                                           JOIN     arGrdComponentTypes posted_gct ON posted_gct.GrdComponentTypeId = posted_gbwd.GrdComponentTypeId
                                           WHERE    posted_gct.SysComponentTypeId IN ( 500,503 ) );
   
    SELECT  t.TermId
           ,t.TermDescrip
           ,r.ReqId
           ,cs.ClsSection
           ,c.Resource AS Component
           ,gct.Descrip AS ComponentDescrip
           ,ISNULL(gbwd.Number,0) AS Required
           ,ISNULL((
                     SELECT SUM(ISNULL(Score,0))
                     FROM   #PostedServices
                     WHERE  InstrGrdBkWgtDetailId = gbwd.InstrGrdBkWgtDetailId
                   ),0) AS Score
           ,CASE WHEN ( ISNULL(gbwd.Number,0) - ISNULL((
                                                         SELECT SUM(ISNULL(Score,0))
                                                         FROM   #PostedServices
                                                         WHERE  InstrGrdBkWgtDetailId = gbwd.InstrGrdBkWgtDetailId
                                                       ),0) ) <= 0 THEN 0
                 ELSE ( ISNULL(gbwd.Number,0) - ISNULL((
                                                         SELECT SUM(ISNULL(Score,0))
                                                         FROM   #PostedServices
                                                         WHERE  InstrGrdBkWgtDetailId = gbwd.InstrGrdBkWgtDetailId
                                                       ),0) )
            END AS Remaining
    FROM    arGrdBkWeights gbw
    JOIN    arGrdBkWgtDetails gbwd ON gbwd.InstrGrdBkWgtId = gbw.InstrGrdBkWgtId
    JOIN    arGrdComponentTypes gct ON gct.GrdComponentTypeId = gbwd.GrdComponentTypeId
    JOIN    arClassSections cs ON cs.ReqId = gbw.ReqId
    JOIN    arReqs r ON r.ReqId = gbw.ReqId
    JOIN    arTerm t ON t.TermId = cs.TermId
    JOIN    arResults ar ON ar.TestId = cs.ClsSectionId
    JOIN    (
              SELECT    *
              FROM      syResources
              WHERE     ResourceTypeID = 10
            ) c ON c.ResourceID = gct.SysComponentTypeId  --components			
    WHERE   gct.SysComponentTypeId IN ( 500,503 )
            AND gbw.ReqId = @CourseId
            AND ar.TestId = @ClsSectionId
            AND ar.StuEnrollId = @StuEnrollId;
			 
    DROP TABLE #PostedServices;

GO
