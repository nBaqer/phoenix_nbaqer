SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[USP_GetRequirementsGroupsByStudent_Grad]
    (
     @StudentID UNIQUEIDENTIFIER
    ,@CampusID UNIQUEIDENTIFIER
    ,@ProgID UNIQUEIDENTIFIER = NULL
    ,@ShiftID UNIQUEIDENTIFIER = NULL
    ,@StatusCodeID UNIQUEIDENTIFIER = NULL 
    )
AS /*----------------------------------------------------------------------------------------------------
	Author          :	Saraswathi Lakshmanan
    
    Create date		:	03/03/2011
    
	Procedure Name	:	[USP_GetRequirementsGroupsByStudent_Grad]

	Objective		:	Get the docs for a student
			
	Parameters		:	Name			Type	Data Type			Required? 	
						=====			====	=========			=========	
					
					
	
	Output			:	Returns the requested details	
						
*/-----------------------------------------------------------------------------------------------------
    BEGIN

        DECLARE @StudentStartDate AS DATETIME;
        SET @StudentStartDate = (
                                  SELECT    MIN(StartDate)
                                  FROM      arStuEnrollments
                                  WHERE     Studentid = @StudentID
                                  GROUP BY  studentID
                                );
 
        DECLARE @ActiveStatusID AS UNIQUEIDENTIFIER;
        SET @ActiveStatusID = (
                                SELECT  StatusID
                                FROM    syStatuses
                                WHERE   Status = 'Active'
                              ); 
 
 
        SELECT  ReqGrpId
               ,Descrip
        FROM    (
                  SELECT    ReqGrpId
                           ,R5.Descrip
                           ,R5.NumReqs
                           ,ISNULL(R5.TestPassed,0) + ISNULL(R5.DocsApproved,0) AS AttemptedReqs
                           ,CASE WHEN LeadGrpId IS NULL THEN '00000000-0000-0000-0000-000000000000'
                                 ELSE LeadGrpId
                            END AS LeadGrpId
                  FROM      (
                              SELECT  DISTINCT
                                        t1.ReqGrpId
                                       ,t2.Descrip
                                       ,t2.CampGrpId
                                       ,t3.NumReqs AS Numreqs
                                       ,(
                                          SELECT    COUNT(*)
                                          FROM      (
                                                      SELECT DISTINCT
                                                                A2.adReqId
                                                               ,@StudentStartDate AS CurrentDate
                                                               ,A3.StartDate
                                                               ,A3.EndDate
                                                               ,A4.LeadGrpId
                                                      FROM      adReqGrpDef A1
                                                               ,adReqs A2
                                                               ,adReqsEffectiveDates A3
                                                               ,adReqLeadGroups A4
                                                      WHERE     A1.adReqId = A2.adReqId
                                                                AND A2.adReqId = A3.adReqId
                                                                AND A3.adReqEffectiveDateId = A4.adReqEffectiveDateId
                                                                AND ReqforGraduation = 1
                                                                AND StatusId = @ActiveStatusID
                                                                AND A4.LeadGrpId IN ( SELECT DISTINCT
                                                                                                LeadGrpId
                                                                                      FROM      adLeadByLeadGroups t1
                                                                                               ,arStuEnrollments t2
                                                                                               ,arStudent t3
                                                                                      WHERE     t1.StuEnrollId = t2.StuEnrollId
                                                                                                AND t2.StudentId = t3.StudentId
                                                                                                AND t3.StudentId = @StudentID )
                                                                AND ReqGrpId = t1.ReqGrpId
                                                                AND A2.adreqTypeId = 1
                                                    ) R1
                                                   ,adLeadEntranceTest R2
                                                   ,arStudent R3
                                          WHERE     R1.adReqId = R2.EntrTestId
                                                    AND R2.StudentId = R3.StudentId
                                                    AND R3.StudentId = @StudentID
                                                    AND R1.CurrentDate >= R1.StartDate
                                                    AND LEN(R2.TestTaken) >= 4
                                                    AND R2.Pass = 1
                                                    AND R2.EntrTestId NOT IN ( SELECT DISTINCT
                                                                                        EntrTestId
                                                                               FROM     adEntrTestOverRide t1
                                                                                       ,arStudent t2
                                                                               WHERE    t1.StudentId = t2.StudentId
                                                                                        AND OverRide = 1
                                                                                        AND t2.StudentId = @StudentID )
                                                    AND (
                                                          R1.CurrentDate <= R1.EndDate
                                                          OR R1.EndDate IS NULL
                                                        )
                                        ) AS TestPassed
                                       ,(
                                          SELECT    COUNT(*)
                                          FROM      (
                                                      SELECT DISTINCT
                                                                A2.adReqId
                                                               ,@StudentStartDate AS CurrentDate
                                                               ,A3.StartDate
                                                               ,A3.EndDate
                                                               ,A4.LeadGrpId
                                                      FROM      adReqGrpDef A1
                                                               ,adReqs A2
                                                               ,adReqsEffectiveDates A3
                                                               ,adReqLeadGroups A4
                                                      WHERE     A1.adReqId = A2.adReqId
                                                                AND A2.adReqId = A3.adReqId
                                                                AND A3.adReqEffectiveDateId = A4.adReqEffectiveDateId
                                                                AND ReqforGraduation = 1
                                                                AND A4.LeadGrpId IN ( SELECT DISTINCT
                                                                                                LeadGrpId
                                                                                      FROM      adLeadByLeadGroups t1
                                                                                               ,arStuEnrollments t2
                                                                                               ,arStudent t3
                                                                                      WHERE     t1.StuEnrollId = t2.StuEnrollId
                                                                                                AND t2.StudentId = t3.StudentId
                                                                                                AND t3.StudentId = @StudentID )
                                                                AND ReqGrpId = t1.ReqGrpId
                                                                AND A2.adreqTypeId = 3
                                                    ) R1
                                                   ,plStudentDocs R2
                                                   ,syDocStatuses R3
                                                   ,arStudent R4
                                          WHERE     R1.adReqId = R2.DocumentId
                                                    AND R2.StudentId = R4.StudentId
                                                    AND R4.StudentId = @StudentID
                                                    AND R2.DocumentId NOT IN ( SELECT DISTINCT
                                                                                        EntrTestId
                                                                               FROM     adEntrTestOverRide t1
                                                                                       ,arStudent t2
                                                                               WHERE    t1.StudentId = t2.StudentId
                                                                                        AND OverRide = 1
                                                                                        AND t2.StudentId = @StudentID )
                                                    AND R2.DocStatusId = R3.DocStatusId
                                                    AND R3.SysDocStatusId = 1
                                                    AND R1.CurrentDate >= R1.StartDate
                                                    AND (
                                                          R1.CurrentDate <= R1.EndDate
                                                          OR R1.EndDate IS NULL
                                                        )
                                        ) AS DocsApproved
                                       ,t4.LeadGrpId
                              FROM      adPrgVerTestDetails t1
                                       ,adReqGroups t2
                                       ,adLeadGrpReqGroups t3
                                       ,adLeadGroups t4
                                       ,arStuEnrollments t5
                                       ,arStudent t6
                                       ,adreqs t7
                                       ,adreqgrpdef t8
                              WHERE     t1.ReqGrpId = t2.ReqGrpId
                                        AND t2.ReqGrpId = t3.ReqGrpId
                                        AND t3.LeadGrpId = t4.LeadGrpId
                                        AND t1.PrgVerId = t5.PrgVerId
                                        AND t5.StudentId = t6.StudentId
                                        AND t6.StudentId = @StudentID
                                        AND t8.ReqGrpId = t2.ReqGrpId
                                        AND t8.adReqId = t7.adReqId
                                        AND Reqforenrollment = 1
                                        AND t5.PrgVerId IN ( SELECT DISTINCT
                                                                    PrgVerId
                                                             FROM   arPrgVersions
                                                             WHERE  ProgId = @ProgID
                                                                    OR @ProgID = NULL )
                                        AND (
                                              t5.ShiftId = @ShiftID
                                              OR @ShiftID = NULL
                                            )
                                        AND (
                                              t5.StatusCodeId = @StatusCodeID
                                              OR @StatusCodeID = NULL
                                            )
                                        AND t2.IsMandatoryReqGrp <> 1
                                        AND t3.LeadGrpId IN ( SELECT DISTINCT
                                                                        LeadGrpId
                                                              FROM      adLeadByLeadGroups t1
                                                                       ,arStuEnrollments t2
                                                                       ,arStudent t3
                                                              WHERE     t1.StuEnrollId = t2.StuEnrollId
                                                                        AND t2.StudentId = t3.StudentId
                                                                        AND t3.StudentId = @StudentID )
                            ) R5
                  WHERE     R5.CampGrpId IN ( SELECT DISTINCT
                                                        B2.CampGrpId
                                              FROM      syCmpGrpCmps B1
                                                       ,syCampGrps B2
                                              WHERE     B1.CampGrpId = B2.CampGrpId
                                                        AND B1.CampusId = @CampusID
                                                        AND B2.StatusId = @ActiveStatusID )
                ) R8
        WHERE   (
                  AttemptedReqs < NumReqs
                  OR (
                       SELECT   COUNT(*)
                       FROM     adReqGrpDef
                       WHERE    ReqGrpId = R8.ReqGrpId
                                AND LeadGrpId IN ( SELECT   LeadGrpId
                                                   FROM     adLeadByLeadGroups
                                                   WHERE    StuEnrollId IN ( SELECT DISTINCT
                                                                                    StuEnrollId
                                                                             FROM   arStuEnrollments
                                                                             WHERE  StudentId = @StudentID ) )
                                AND IsRequired = 1
                                AND (
                                      adReqId IN ( SELECT DISTINCT
                                                            EntrTestId
                                                   FROM     adLeadEntranceTest
                                                   WHERE    StudentId = @StudentID
                                                            AND Pass = 0
                                                            AND EntrTestId NOT IN ( SELECT  EntrTestId
                                                                                    FROM    adEntrTestOverride
                                                                                    WHERE   StudentId = @StudentID
                                                                                            AND override = 1 ) )
                                      OR adReqId IN ( SELECT DISTINCT
                                                                DocumentId
                                                      FROM      plStudentDocs t1
                                                               ,syDocStatuses t2
                                                      WHERE     t1.StudentId = @StudentID
                                                                AND t1.DocStatusId = t2.DocStatusId
                                                                AND t2.SysDocStatusId <> 1
                                                                AND DocumentId NOT IN ( SELECT  EntrTestId
                                                                                        FROM    adEntrTestOverride
                                                                                        WHERE   StudentId = @StudentID
                                                                                                AND override = 1 ) )
                                      OR adReqId IN ( SELECT DISTINCT
                                                                EntrTestId
                                                      FROM      adEntrTestOverride
                                                      WHERE     StudentId = @StudentID
                                                                AND override = 1 )
                                    )
                     ) >= 1
                );
        
    END;






GO
