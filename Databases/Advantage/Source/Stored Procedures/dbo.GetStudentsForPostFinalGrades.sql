SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Ginzo, John
-- Create date: 09/03/2015
-- Description:	Get list of Students for Post Final
--				grades page
-- =============================================
CREATE PROCEDURE [dbo].[GetStudentsForPostFinalGrades]
    @ClsSectId VARCHAR(50)
   ,@UserId VARCHAR(50)
AS
    BEGIN

        SET NOCOUNT ON;

        -----------------------------------------------------------------------------------------
        -- Determine what the school uses as a student identifier based on the config value
        -----------------------------------------------------------------------------------------
        DECLARE @StudentIdentifier VARCHAR(50);
        SET @StudentIdentifier = (
                                 SELECT     v.Value
                                 FROM       dbo.syConfigAppSettings c
                                 INNER JOIN dbo.syConfigAppSetValues v ON c.SettingId = v.SettingId
                                 WHERE      KeyName = 'StudentIdentifier'
                                 );

        -----------------------------------------------------------------------------------------
        -- Initialize table hold students
        -----------------------------------------------------------------------------------------
        DECLARE @InitResults TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,StudentIdentifier VARCHAR(50)
               ,IsSSNStudentIdentifier INT
               ,LastName VARCHAR(50)
               ,FirstName VARCHAR(50)
               ,PrgVerId UNIQUEIDENTIFIER
               ,IsContinuingEd BIT
               ,StatusCodeDescrip VARCHAR(50)
               ,PostAcademics BIT
               ,StudentId UNIQUEIDENTIFIER
               ,SysStatusId INT
               ,StartDate DATETIME
               ,GraduatedDate DATETIME
               ,DateCompleted DATETIME
            );

        -----------------------------------------------------------------------------------------
        -- We have two fields that will come from different values depending on the config value,
        -- so we need to construct a different query based on the value
        -----------------------------------------------------------------------------------------
        IF @StudentIdentifier = 'EnrollmentId'
            BEGIN
                INSERT INTO @InitResults
                            SELECT   DISTINCT r.StuEnrollId
                                             ,e.EnrollmentId AS StudentIdentifier --use enrollment id as student identifier
                                             ,0 AS IsSSNStudentIdentifier         -- use 0 as ssn identifier
                                             ,s.LastName
                                             ,s.FirstName
                                             ,pv.PrgVerId
                                             ,pv.IsContinuingEd
                                             ,st.StatusCodeDescrip
                                             ,ss.PostAcademics
                                             ,s.StudentId
                                             ,ss.SysStatusId
                                             ,e.StartDate
                                             ,e.ExpGradDate
                                             ,CASE WHEN CONVERT(DATE, ISNULL(r.DateCompleted,'1900-01-01'))  <= '1900-01-01' THEN NULL ELSE r.DateCompleted END AS DateCompleted
                            FROM     arResults r
                            JOIN     dbo.arStuEnrollments e ON r.StuEnrollId = e.StuEnrollId
                            JOIN     dbo.arStudent s ON e.StudentId = s.StudentId
                            JOIN     dbo.arPrgVersions pv ON e.PrgVerId = pv.PrgVerId
                            JOIN     dbo.syStatusCodes st ON e.StatusCodeId = st.StatusCodeId
                            JOIN     dbo.sySysStatus ss ON st.SysStatusId = ss.SysStatusId
                            WHERE    r.TestId = @ClsSectId
                            ORDER BY s.LastName
                                    ,s.FirstName;
            END;
        ELSE IF @StudentIdentifier = 'StudentId'
                 BEGIN
                     INSERT INTO @InitResults
                                 SELECT   DISTINCT r.StuEnrollId
                                                  ,s.StudentNumber AS StudentIdentifier --use student Number as the student identifier
                                                  ,0 AS IsSSNStudentIdentifier          -- use 0 as the ssn identifier
                                                  ,s.LastName
                                                  ,s.FirstName
                                                  ,pv.PrgVerId
                                                  ,pv.IsContinuingEd
                                                  ,st.StatusCodeDescrip
                                                  ,ss.PostAcademics
                                                  ,s.StudentId
                                                  ,ss.SysStatusId
                                                  ,e.StartDate
                                                  ,e.ExpGradDate
                                                  ,CASE WHEN CONVERT(DATE, ISNULL(r.DateCompleted,'1900-01-01'))  <= '1900-01-01' THEN NULL ELSE r.DateCompleted END AS DateCompleted
                                 FROM     arResults r
                                 JOIN     dbo.arStuEnrollments e ON r.StuEnrollId = e.StuEnrollId
                                 JOIN     dbo.arStudent s ON e.StudentId = s.StudentId
                                 JOIN     dbo.arPrgVersions pv ON e.PrgVerId = pv.PrgVerId
                                 JOIN     dbo.syStatusCodes st ON e.StatusCodeId = st.StatusCodeId
                                 JOIN     dbo.sySysStatus ss ON st.SysStatusId = ss.SysStatusId
                                 WHERE    r.TestId = @ClsSectId
                                 ORDER BY s.LastName
                                         ,s.FirstName;
                 END;
        ELSE
                 BEGIN
                     INSERT INTO @InitResults
                                 SELECT   DISTINCT r.StuEnrollId
                                                  ,s.SSN AS StudentIdentifier  -- use ssn as the student identifier
                                                  ,1 AS IsSSNStudentIdentifier -- use 1 as the ssn identifier
                                                  ,s.LastName
                                                  ,s.FirstName
                                                  ,pv.PrgVerId
                                                  ,pv.IsContinuingEd
                                                  ,st.StatusCodeDescrip
                                                  ,ss.PostAcademics
                                                  ,s.StudentId
                                                  ,ss.SysStatusId
												  ,e.StartDate
                                                  ,e.ExpGradDate
                                                  ,CASE WHEN CONVERT(DATE, ISNULL(r.DateCompleted,'1900-01-01'))  <= '1900-01-01' THEN NULL ELSE r.DateCompleted END AS DateCompleted
                                 FROM     arResults r
                                 JOIN     dbo.arStuEnrollments e ON r.StuEnrollId = e.StuEnrollId
                                 JOIN     dbo.arStudent s ON e.StudentId = s.StudentId
                                 JOIN     dbo.arPrgVersions pv ON e.PrgVerId = pv.PrgVerId
                                 JOIN     dbo.syStatusCodes st ON e.StatusCodeId = st.StatusCodeId
                                 JOIN     dbo.sySysStatus ss ON st.SysStatusId = ss.SysStatusId
                                 WHERE    r.TestId = @ClsSectId
                                 ORDER BY s.LastName
                                         ,s.FirstName;
                 END;

        -----------------------------------------------------------------------------------------
        -- Check the user id to see if they are either support, sa, system admin, or director of
        -- academics.  If so, return records regardless of in or out of school status.
        -- If not, return records filtered for status 
        -----------------------------------------------------------------------------------------
        IF EXISTS (
                  SELECT     r.RoleId
                  FROM       dbo.syRoles r
                  INNER JOIN dbo.sySysRoles sr ON r.SysRoleId = sr.SysRoleId
                  INNER JOIN dbo.syUsersRolesCampGrps ur ON r.RoleId = ur.RoleId
                  INNER JOIN dbo.syUsers u ON ur.UserId = u.UserId
                  WHERE      ur.UserId = @UserId
                             AND (
                                 u.UserName IN ( 'sa', 'support' )
                                 OR sr.SysRoleId IN ( 1, 13 )
                                 )
                  )
            BEGIN
                SELECT StuEnrollId
                      ,StudentIdentifier
                      ,IsSSNStudentIdentifier
                      ,LastName
                      ,FirstName
                      ,PrgVerId
                      ,IsContinuingEd
                      ,StatusCodeDescrip
                      ,SysStatusId
                      ,StudentId
                      ,PostAcademics
                      ,SysStatusId
					  ,StartDate
					  ,GraduatedDate
					  ,DateCompleted
                FROM   @InitResults;
            END;
        ELSE
            BEGIN
                SELECT StuEnrollId
                      ,StudentIdentifier
                      ,IsSSNStudentIdentifier
                      ,LastName
                      ,FirstName
                      ,PrgVerId
                      ,IsContinuingEd
                      ,StatusCodeDescrip
                      ,SysStatusId
                      ,StudentId
                      ,PostAcademics
                      ,SysStatusId
					  ,StartDate
					  ,GraduatedDate
					  ,DateCompleted
                FROM   @InitResults
                WHERE  PostAcademics = 1
                       OR SysStatusId = 7;
            END;

    END;




GO
