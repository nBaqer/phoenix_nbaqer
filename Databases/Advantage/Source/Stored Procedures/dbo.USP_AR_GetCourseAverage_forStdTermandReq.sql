SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[USP_AR_GetCourseAverage_forStdTermandReq]
    (
     @StuEnrollID UNIQUEIDENTIFIER
    ,@TermId UNIQUEIDENTIFIER
    ,@ReqID UNIQUEIDENTIFIER
    ,@Score INT OUTPUT
    )
AS /*----------------------------------------------------------------------------------------------------
	Author          :	Saraswathi Lakshmanan
    
    Create date		:	02/03/2010
    
	Procedure Name	:	[USP_AR_GetCourseAverage_forStdTermandReq]

	Objective		:	get the average score
	
	Parameters		:	Name			Type	Data Type			Required? 	
						=====			====	=========			=========	
						@StuEnrollId	In		Uniqueidentifier
						@TermID			In		UniqueIdentifier	
						@ReqID			In		UniqueIdentifier
						@Score			Out					
					
	Output			:	returns the score
	
	Calling procedure	: 	DoesStdHavePassingGrdForNumeric			
						
*/-----------------------------------------------------------------------------------------------------
/*
Procedure created by Saraswathi Lakshmanan on Jan 29 2010
--To fix issue 17548: ENH: All: Register Students Page is Taking Too Long to Populate Available Students 
This query is used get the minimum passing score

*/

    BEGIN
        SET @Score = (
                       SELECT   ISNULL(score,0)
                       FROM     arResults
                       INNER JOIN arClasssections ON TestID = ClsSectionId
                       WHERE    stuEnrollId = @StuEnrollID
                                AND Termid = @TermId
                                AND reqid = @ReqID
                       UNION
                       SELECT   ISNULL(score,0)
                       FROM     arTransferGrades
                       WHERE    stuEnrollId = @StuEnrollID
                                AND Termid = @TermId
                                AND reqid = @ReqID
                     );
        RETURN @Score;				
    END;



GO
