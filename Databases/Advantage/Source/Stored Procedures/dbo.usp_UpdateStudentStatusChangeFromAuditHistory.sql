SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_UpdateStudentStatusChangeFromAuditHistory]
AS
    BEGIN
	    ------------------------------------------------------------
	    -- Table to store student status changes
	    -------------------------------------------------------------
        DECLARE @syStudentStatusChangesFromAudit AS TABLE
            (
             StuEnrollId UNIQUEIDENTIFIER NOT NULL
            ,OrigStatusCodeId UNIQUEIDENTIFIER NULL
            ,NewStatusCodeId UNIQUEIDENTIFIER NULL
            ,CampusId UNIQUEIDENTIFIER NOT NULL
            ,ModDate DATETIME NULL
            ,ModUser VARCHAR(50) NULL
            ,IsReversal BIT NOT NULL
            ,DropReasonId UNIQUEIDENTIFIER NULL
            ,DateOfChange DATETIME NULL
            );

        INSERT  INTO @syStudentStatusChangesFromAudit
                SELECT  SAHD.RowId AS StuEnrollId
                       ,SSC.StatusCodeId AS OrigStatusCodeId
                       ,SSC1.StatusCodeId AS NewStatusCodeId
                       ,ASE.CampusId AS CampusID
                       ,SAH.EventDate AS ModDate
                       ,SAH.UserName AS ModUser
                       ,0 AS IsReversal
                       ,(
                          SELECT TOP 1
                                    SAHD1.NewValue
                          FROM      dbo.syAuditHistDetail AS SAHD1
                          WHERE     SAHD1.AuditHistId = SAH.AuditHistId
                                    AND SAHD1.ColumnName = 'DropReasonId'
                        ) AS DropReasonId
                       ,CASE WHEN SSC1.SysStatusId = 7    -- Future Start
                                  THEN SAH.EventDate -- Date in which the school decide the status
                             WHEN SSC1.SysStatusId = 9   -- Current Atencance -- Reenrollment
                                  THEN CASE WHEN (
                                                   SELECT TOP 1
                                                            SAHD1.NewValue
                                                   FROM     dbo.syAuditHistDetail AS SAHD1
                                                   WHERE    SAHD1.AuditHistId = SAH.AuditHistId
                                                            AND SAHD1.ColumnName IN ( 'ReEnrollmentDate' )
                                                   ORDER BY SAHD1.NewValue DESC
                                                 ) IS NOT NULL THEN (
                                                                      SELECT TOP 1
                                                                                SAHD1.NewValue
                                                                      FROM      dbo.syAuditHistDetail AS SAHD1
                                                                      WHERE     SAHD1.AuditHistId = SAH.AuditHistId
                                                                                AND SAHD1.ColumnName IN ( 'ReEnrollmentDate' )
                                                                      ORDER BY  SAHD1.NewValue DESC
                                                                    )
                                            WHEN (
                                                   SELECT TOP 1
                                                            SAHD1.NewValue
                                                   FROM     dbo.syAuditHistDetail AS SAHD1
                                                   WHERE    SAHD1.AuditHistId = SAH.AuditHistId
                                                            AND SAHD1.ColumnName IN ( 'StartDate','EnrollDate' )
                                                   ORDER BY SAHD1.NewValue DESC
                                                 ) IS NOT NULL THEN (
                                                                      SELECT TOP 1
                                                                                SAHD1.NewValue
                                                                      FROM      dbo.syAuditHistDetail AS SAHD1
                                                                      WHERE     SAHD1.AuditHistId = SAH.AuditHistId
                                                                                AND SAHD1.ColumnName IN ( 'StartDate','EnrollDate' )
                                                                      ORDER BY  SAHD1.NewValue DESC
                                                                    )
                                            WHEN ASE.ReEnrollmentDate IS NOT NULL THEN ASE.ReEnrollmentDate
                                            ELSE ASE.StartDate
                                       END
                             WHEN SSC1.SysStatusId = 10   -- Leave Of Absence
                                  THEN CASE WHEN (
                                                   SELECT TOP 1
                                                            ASLA.StartDate
                                                   FROM     dbo.arStudentLOAs AS ASLA
                                                   INNER JOIN dbo.syStudentStatusChanges AS SSSC ON SSSC.StudentStatusChangeId = ASLA.StudentStatusChangeId
                                                   INNER JOIN (
                                                                SELECT  SAHD1.RowId AS StuEnrollId
                                                                       ,SAHD1.OldValue AS OrigStatusCodeId
                                                                       ,SAHD1.NewValue AS NewStatusCodeId
                                                                       ,SAH1.Event AS Event
                                                                       ,SAH1.EventDate AS ModDate
                                                                       ,SAH1.UserName AS ModUser
                                                                FROM    dbo.syAuditHist AS SAH1
                                                                INNER JOIN dbo.syAuditHistDetail AS SAHD1 ON SAHD1.AuditHistId = SAH1.AuditHistId
                                                                LEFT JOIN dbo.syStatusCodes AS SSC ON ISNULL(CONVERT(NVARCHAR(40),SSC.StatusCodeId),'') = ISNULL(SAHD1.OldValue,
                                                                                                                                                '')
                                                                INNER JOIN dbo.syStatusCodes AS SSC1 ON ISNULL(CONVERT(NVARCHAR(40),SSC1.StatusCodeId),'') = ISNULL(SAHD1.NewValue,
                                                                                                                                                '')
                                                              ) AS T2 ON T2.StuEnrollId = SSSC.StuEnrollId
                                                                         AND ISNULL(CONVERT(NVARCHAR(40),T2.OrigStatusCodeId),'') = ISNULL(CONVERT(NVARCHAR(40),SSSC.OrigStatusId),
                                                                                                                                           '')
                                                                         AND ISNULL(CONVERT(NVARCHAR(40),T2.NewStatusCodeId),'') = ISNULL(CONVERT(NVARCHAR(40),SSSC.NewStatusId),
                                                                                                                                          '')
                                                                         AND T2.ModUser = SSSC.ModUser
                                                                         AND T2.ModDate = SSSC.ModDate
                                                   WHERE    T2.Event <> 'D'
                                                   ORDER BY ASLA.StartDate DESC
                                                 ) IS NOT NULL
                                            THEN (
                                                   SELECT TOP 1
                                                            ASLA.StartDate
                                                   FROM     dbo.arStudentLOAs AS ASLA
                                                   INNER JOIN dbo.syStudentStatusChanges AS SSSC ON SSSC.StudentStatusChangeId = ASLA.StudentStatusChangeId
                                                   INNER JOIN (
                                                                SELECT  SAHD1.RowId AS StuEnrollId
                                                                       ,SAHD1.OldValue AS OrigStatusCodeId
                                                                       ,SAHD1.NewValue AS NewStatusCodeId
                                                                       ,SAH1.Event AS Event
                                                                       ,SAH1.EventDate AS ModDate
                                                                       ,SAH1.UserName AS ModUser
                                                                FROM    dbo.syAuditHist AS SAH1
                                                                INNER JOIN dbo.syAuditHistDetail AS SAHD1 ON SAHD1.AuditHistId = SAH1.AuditHistId
                                                                LEFT JOIN dbo.syStatusCodes AS SSC ON ISNULL(CONVERT(NVARCHAR(40),SSC.StatusCodeId),'') = ISNULL(SAHD1.OldValue,
                                                                                                                                                '')
                                                                INNER JOIN dbo.syStatusCodes AS SSC1 ON ISNULL(CONVERT(NVARCHAR(40),SSC1.StatusCodeId),'') = ISNULL(SAHD1.NewValue,
                                                                                                                                                '')
                                                              ) AS T2 ON T2.StuEnrollId = SSSC.StuEnrollId
                                                                         AND ISNULL(CONVERT(NVARCHAR(40),T2.OrigStatusCodeId),'') = ISNULL(CONVERT(NVARCHAR(40),SSSC.OrigStatusId),
                                                                                                                                           '')
                                                                         AND ISNULL(CONVERT(NVARCHAR(40),T2.NewStatusCodeId),'') = ISNULL(CONVERT(NVARCHAR(40),SSSC.NewStatusId),
                                                                                                                                          '')
                                                                         AND T2.ModUser = SSSC.ModUser
                                                                         AND T2.ModDate = SSSC.ModDate
                                                   WHERE    T2.Event <> 'D'
                                                   ORDER BY ASLA.StartDate DESC
                                                 )
                                            ELSE SAH.EventDate
                                       END
                             WHEN SSC1.SysStatusId = 11    -- Suspended
                                  THEN CASE WHEN (
                                                   SELECT TOP 1
                                                            ASS.StartDate
                                                   FROM     dbo.arStdSuspensions AS ASS
                                                   INNER JOIN dbo.syStudentStatusChanges AS SSSC ON SSSC.StudentStatusChangeId = ASS.StudentStatusChangeId
                                                   INNER JOIN (
                                                                SELECT  SAHD1.RowId AS StuEnrollId
                                                                       ,SAHD1.OldValue AS OrigStatusCodeId
                                                                       ,SAHD1.NewValue AS NewStatusCodeId
                                                                       ,SAH1.Event AS Event
                                                                       ,SAH1.EventDate AS ModDate
                                                                       ,SAH1.UserName AS ModUser
                                                                FROM    dbo.syAuditHist AS SAH1
                                                                INNER JOIN dbo.syAuditHistDetail AS SAHD1 ON SAHD1.AuditHistId = SAH1.AuditHistId
                                                                LEFT JOIN dbo.syStatusCodes AS SSC ON ISNULL(CONVERT(NVARCHAR(40),SSC.StatusCodeId),'') = ISNULL(SAHD1.OldValue,
                                                                                                                                                '')
                                                                INNER JOIN dbo.syStatusCodes AS SSC1 ON ISNULL(CONVERT(NVARCHAR(40),SSC1.StatusCodeId),'') = ISNULL(SAHD1.NewValue,
                                                                                                                                                '')
                                                              ) AS T2 ON T2.StuEnrollId = SSSC.StuEnrollId
                                                                         AND ISNULL(CONVERT(NVARCHAR(40),T2.OrigStatusCodeId),'') = ISNULL(CONVERT(NVARCHAR(40),SSSC.OrigStatusId),
                                                                                                                                           '')
                                                                         AND ISNULL(CONVERT(NVARCHAR(40),T2.NewStatusCodeId),'') = ISNULL(CONVERT(NVARCHAR(40),SSSC.NewStatusId),
                                                                                                                                          '')
                                                                         AND T2.ModUser = SSSC.ModUser
                                                                         AND T2.ModDate = SSSC.ModDate
                                                   WHERE    T2.Event <> 'D'
                                                   ORDER BY ASS.StartDate DESC
                                                 ) IS NOT NULL
                                            THEN (
                                                   SELECT TOP 1
                                                            ASS.StartDate
                                                   FROM     dbo.arStdSuspensions AS ASS
                                                   INNER JOIN dbo.syStudentStatusChanges AS SSSC ON SSSC.StudentStatusChangeId = ASS.StudentStatusChangeId
                                                   INNER JOIN (
                                                                SELECT  SAHD1.RowId AS StuEnrollId
                                                                       ,SAHD1.OldValue AS OrigStatusCodeId
                                                                       ,SAHD1.NewValue AS NewStatusCodeId
                                                                       ,SAH1.Event AS Event
                                                                       ,SAH1.EventDate AS ModDate
                                                                       ,SAH1.UserName AS ModUser
                                                                FROM    dbo.syAuditHist AS SAH1
                                                                INNER JOIN dbo.syAuditHistDetail AS SAHD1 ON SAHD1.AuditHistId = SAH1.AuditHistId
                                                                LEFT JOIN dbo.syStatusCodes AS SSC ON ISNULL(CONVERT(NVARCHAR(40),SSC.StatusCodeId),'') = ISNULL(SAHD1.OldValue,
                                                                                                                                                '')
                                                                INNER JOIN dbo.syStatusCodes AS SSC1 ON ISNULL(CONVERT(NVARCHAR(40),SSC1.StatusCodeId),'') = ISNULL(SAHD1.NewValue,
                                                                                                                                                '')
                                                              ) AS T2 ON T2.StuEnrollId = SSSC.StuEnrollId
                                                                         AND ISNULL(CONVERT(NVARCHAR(40),T2.OrigStatusCodeId),'') = ISNULL(CONVERT(NVARCHAR(40),SSSC.OrigStatusId),
                                                                                                                                           '')
                                                                         AND ISNULL(CONVERT(NVARCHAR(40),T2.NewStatusCodeId),'') = ISNULL(CONVERT(NVARCHAR(40),SSSC.NewStatusId),
                                                                                                                                          '')
                                                                         AND T2.ModUser = SSSC.ModUser
                                                                         AND T2.ModDate = SSSC.ModDate
                                                   WHERE    T2.Event <> 'D'
                                                   ORDER BY ASS.StartDate DESC
                                                 )
                                            ELSE SAH.EventDate
                                       END
                             WHEN SSC1.SysStatusId = 12   -- Drop or Wildraw
                                  THEN CASE WHEN (
                                                   SELECT TOP 1
                                                            SAHD1.NewValue
                                                   FROM     dbo.syAuditHistDetail AS SAHD1
                                                   WHERE    SAHD1.AuditHistId = SAH.AuditHistId
                                                            AND SAHD1.ColumnName = 'DateDetermined'
                                                   ORDER BY SAHD1.NewValue DESC
                                                 ) IS NOT NULL THEN (
                                                                      SELECT TOP 1
                                                                                SAHD1.NewValue
                                                                      FROM      dbo.syAuditHistDetail AS SAHD1
                                                                      WHERE     SAHD1.AuditHistId = SAH.AuditHistId
                                                                                AND SAHD1.ColumnName = 'DateDetermined'
                                                                      ORDER BY  SAHD1.NewValue DESC
                                                                    )
                                            WHEN ASE.DateDetermined IS NOT NULL THEN ASE.DateDetermined
                                            ELSE SAH.EventDate
                                       END
                             WHEN SSC1.SysStatusId = 14 -- Graduated
                                  THEN CASE WHEN (
                                                   SELECT TOP 1
                                                            SAHD1.NewValue
                                                   FROM     dbo.syAuditHistDetail AS SAHD1
                                                   WHERE    SAHD1.AuditHistId = SAH.AuditHistId
                                                            AND SAHD1.ColumnName IN ( 'ExpGradDate' )
                                                   ORDER BY SAHD1.NewValue
                                                 ) IS NOT NULL THEN (
                                                                      SELECT TOP 1
                                                                                SAHD1.NewValue
                                                                      FROM      dbo.syAuditHistDetail AS SAHD1
                                                                      WHERE     SAHD1.AuditHistId = SAH.AuditHistId
                                                                                AND SAHD1.ColumnName IN ( 'ExpGradDate' )
                                                                      ORDER BY  SAHD1.NewValue
                                                                    )
                                            WHEN ASE.ExpGradDate IS NOT NULL THEN ASE.ExpGradDate
                                            ELSE SAH.EventDate
                                       END
                             WHEN SSC1.SysStatusId = 19   -- transfer
                                  THEN CASE WHEN (
                                                   SELECT TOP 1
                                                            SAHD1.NewValue
                                                   FROM     dbo.syAuditHistDetail AS SAHD1
                                                   WHERE    SAHD1.AuditHistId = SAH.AuditHistId
                                                            AND SAHD1.ColumnName IN ( 'TransferDate','DateDetermined' )
                                                   ORDER BY SAHD1.NewValue
                                                 ) IS NOT NULL THEN (
                                                                      SELECT TOP 1
                                                                                SAHD1.NewValue
                                                                      FROM      dbo.syAuditHistDetail AS SAHD1
                                                                      WHERE     SAHD1.AuditHistId = SAH.AuditHistId
                                                                                AND SAHD1.ColumnName IN ( 'TransferDate','DateDetermined' )
                                                                      ORDER BY  SAHD1.NewValue
                                                                    )
                                            ELSE SAH.EventDate
                                       END
                             WHEN SSC1.SysStatusId = 20  -- Academic Probation  
                                  THEN CASE WHEN (
                                                   SELECT TOP 1
                                                            ASPW.StartDate
                                                   FROM     dbo.syStudentStatusChanges AS SSSC
                                                   INNER JOIN arStuProbWarnings AS ASPW ON ASPW.StudentStatusChangeId = SSSC.StudentStatusChangeId
                                                   INNER JOIN (
                                                                SELECT  SAHD1.RowId AS StuEnrollId
                                                                       ,SAHD1.OldValue AS OrigStatusCodeId
                                                                       ,SAHD1.NewValue AS NewStatusCodeId
                                                                       ,SAH1.Event AS Event
                                                                       ,SAH1.EventDate AS ModDate
                                                                       ,SAH1.UserName AS ModUser
                                                                FROM    dbo.syAuditHist AS SAH1
                                                                INNER JOIN dbo.syAuditHistDetail AS SAHD1 ON SAHD1.AuditHistId = SAH1.AuditHistId
                                                                INNER JOIN arStuEnrollments AS ASE ON ASE.StuEnrollId = SAHD1.RowId
                                                                LEFT JOIN dbo.syStatusCodes AS SSC ON ISNULL(CONVERT(NVARCHAR(40),SSC.StatusCodeId),'') = ISNULL(SAHD1.OldValue,
                                                                                                                                                '')
                                                                INNER JOIN dbo.syStatusCodes AS SSC1 ON ISNULL(CONVERT(NVARCHAR(40),SSC1.StatusCodeId),'') = ISNULL(SAHD1.NewValue,
                                                                                                                                                '')
                                                              ) AS T2 ON T2.StuEnrollId = SSSC.StuEnrollId
                                                                         AND ISNULL(CONVERT(NVARCHAR(40),T2.OrigStatusCodeId),'') = ISNULL(CONVERT(NVARCHAR(40),SSSC.OrigStatusId),
                                                                                                                                           '')
                                                                         AND ISNULL(CONVERT(NVARCHAR(40),T2.NewStatusCodeId),'') = ISNULL(CONVERT(NVARCHAR(40),SSSC.NewStatusId),
                                                                                                                                          '')
                                                                         AND T2.ModUser = SSSC.ModUser
                                                                         AND T2.ModDate = SSSC.ModDate
                                                   WHERE    SSSC.StuEnrollId = ASE.StuEnrollId  --           'EDE40D25-F130-41B7-9E3C-54DC48DAE729'  --
                                                            AND T2.Event <> 'D'
                                                            AND ASPW.ProbWarningTypeId = 1
                                                   ORDER BY SSSC.StuEnrollId
                                                           ,ASPW.StartDate DESC
                                                 ) IS NOT NULL
                                            THEN (
                                                   SELECT TOP 1
                                                            ASPW.StartDate
                                                   FROM     dbo.syStudentStatusChanges AS SSSC
                                                   INNER JOIN arStuProbWarnings AS ASPW ON ASPW.StudentStatusChangeId = SSSC.StudentStatusChangeId
                                                   INNER JOIN (
                                                                SELECT  SAHD1.RowId AS StuEnrollId
                                                                       ,SAHD1.OldValue AS OrigStatusCodeId
                                                                       ,SAHD1.NewValue AS NewStatusCodeId
                                                                       ,SAH1.Event AS Event
                                                                       ,SAH1.EventDate AS ModDate
                                                                       ,SAH1.UserName AS ModUser
                                                                FROM    dbo.syAuditHist AS SAH1
                                                                INNER JOIN dbo.syAuditHistDetail AS SAHD1 ON SAHD1.AuditHistId = SAH1.AuditHistId
                                                                INNER JOIN arStuEnrollments AS ASE ON ASE.StuEnrollId = SAHD1.RowId
                                                                LEFT JOIN dbo.syStatusCodes AS SSC ON ISNULL(CONVERT(NVARCHAR(40),SSC.StatusCodeId),'') = ISNULL(SAHD1.OldValue,
                                                                                                                                                '')
                                                                INNER JOIN dbo.syStatusCodes AS SSC1 ON ISNULL(CONVERT(NVARCHAR(40),SSC1.StatusCodeId),'') = ISNULL(SAHD1.NewValue,
                                                                                                                                                '')
                                                              ) AS T2 ON T2.StuEnrollId = SSSC.StuEnrollId
                                                                         AND ISNULL(CONVERT(NVARCHAR(40),T2.OrigStatusCodeId),'') = ISNULL(CONVERT(NVARCHAR(40),SSSC.OrigStatusId),
                                                                                                                                           '')
                                                                         AND ISNULL(CONVERT(NVARCHAR(40),T2.NewStatusCodeId),'') = ISNULL(CONVERT(NVARCHAR(40),SSSC.NewStatusId),
                                                                                                                                          '')
                                                                         AND T2.ModUser = SSSC.ModUser
                                                                         AND T2.ModDate = SSSC.ModDate
                                                   WHERE    SSSC.StuEnrollId = ASE.StuEnrollId  --           'EDE40D25-F130-41B7-9E3C-54DC48DAE729'  --
                                                            AND T2.Event <> 'D'
                                                            AND ASPW.ProbWarningTypeId = 1
                                                   ORDER BY SSSC.StuEnrollId
                                                           ,ASPW.StartDate DESC
                                                 )
                                            ELSE SAH.EventDate
                                       END
                             WHEN SSC1.SysStatusId = 22   -- Externship
                                  THEN SAH.EventDate -- '' --> 1900-01-01  or SAH.EventDate                                           
                        END AS DateOfChange
                FROM    dbo.syAuditHist AS SAH
                INNER JOIN dbo.syAuditHistDetail AS SAHD ON SAHD.AuditHistId = SAH.AuditHistId
                INNER JOIN dbo.arStuEnrollments AS ASE ON ASE.StuEnrollId = SAHD.RowId
                LEFT JOIN dbo.syStatusCodes AS SSC ON ISNULL(CONVERT(NVARCHAR(40),SSC.StatusCodeId),'') = ISNULL(SAHD.OldValue,'')
                LEFT JOIN dbo.syStatusCodes AS SSC1 ON ISNULL(CONVERT(NVARCHAR(40),SSC1.StatusCodeId),'') = ISNULL(SAHD.NewValue,'')
                WHERE   SAH.TableName = 'arStuEnrollments'
                        AND SAHD.ColumnName = 'StatusCodeId'
                ORDER BY SAHD.RowId
                       ,SAH.AuditHistId
                       ,SAH.EventDate;

  
        ----------------------------------------------------------------------------------
        -- Update SSSC  syStudentStatusChanges AS SSSC  from View data
        ----------------------------------------------------------------------------------
        UPDATE  SSSC
        SET     SSSC.DateOfChange = V.DateOfChange
        FROM    dbo.syStudentStatusChanges AS SSSC
        INNER JOIN dbo.syStatusCodes AS SSC1 ON CONVERT(NVARCHAR(40),SSC1.StatusCodeId) = SSSC.NewStatusId
        INNER JOIN (
                     SELECT DISTINCT
                            D.StuEnrollId
                           ,D.OrigStatusCodeId
                           ,D.NewStatusCodeId
                           ,D.ModUser
                           ,D.ModDate
                           ,D.DateOfChange
                     FROM   @syStudentStatusChangesFromAudit AS D
                   ) AS V ON V.StuEnrollId = SSSC.StuEnrollId
                             AND ISNULL(CONVERT(NVARCHAR(40),V.OrigStatusCodeId),'') = ISNULL(CONVERT(NVARCHAR(40),SSSC.OrigStatusId),'')
                             AND ISNULL(CONVERT(NVARCHAR(40),V.NewStatusCodeId),'') = ISNULL(CONVERT(NVARCHAR(40),SSSC.NewStatusId),'')
                             AND V.ModUser = SSSC.ModUser
                             AND V.ModDate = SSSC.ModDate;
        --------------------------------------------------------------------------------
  
  
        ----------------------------------------------------------------------------------
        -- Insert new records in  SSSC  syStudentStatusChanges AS SSSC  from View data
        ----------------------------------------------------------------------------------
        DECLARE @CurStudentStatusChangeId UNIQUEIDENTIFIER; 
        DECLARE @CurStuEnrollId UNIQUEIDENTIFIER; 
        DECLARE @CurOrigStatusCodeId UNIQUEIDENTIFIER; 
        DECLARE @CurNewStatusCodeId UNIQUEIDENTIFIER; 
        DECLARE @CurCampusId UNIQUEIDENTIFIER; 
        DECLARE @CurModDate DATETIME; 
        DECLARE @CurModUser NVARCHAR(50); 
        DECLARE @CurIsReversal BIT; 
        DECLARE @CurDropReasonId UNIQUEIDENTIFIER;
        DECLARE @CurDateOfChange DATETIME; 

        DECLARE ToInsertCursor CURSOR
        FOR
            SELECT  NEWID()
                   ,V.StuEnrollId
                   ,V.OrigStatusCodeId
                   ,V.NewStatusCodeId
                   ,V.CampusId
                   ,V.ModDate
                   ,V.ModUser
                   ,0
                   ,V.DropReasonId
                   ,V.DateOfChange
            FROM    dbo.syStudentStatusChanges AS SSSC
            INNER JOIN dbo.syStatusCodes AS SSC1 ON CONVERT(NVARCHAR(40),SSC1.StatusCodeId) = SSSC.NewStatusId
            RIGHT JOIN (
                         SELECT DISTINCT
                                D.StuEnrollId
                               ,D.OrigStatusCodeId
                               ,D.NewStatusCodeId
                               ,D.CampusId
                               ,D.DropReasonId
                               ,D.ModUser
                               ,D.ModDate
                               ,D.DateOfChange
                         FROM   @syStudentStatusChangesFromAudit AS D
                       ) AS V ON V.StuEnrollId = SSSC.StuEnrollId
                                 AND ISNULL(CONVERT(NVARCHAR(40),V.OrigStatusCodeId),'') = ISNULL(CONVERT(NVARCHAR(40),SSSC.OrigStatusId),'')
                                 AND ISNULL(CONVERT(NVARCHAR(40),V.NewStatusCodeId),'') = ISNULL(CONVERT(NVARCHAR(40),SSSC.NewStatusId),'')
                                 AND V.ModUser = SSSC.ModUser
                                 AND V.ModDate = SSSC.ModDate
            WHERE   SSSC.StuEnrollId IS NULL;
		
        OPEN ToInsertCursor; 
        FETCH NEXT FROM ToInsertCursor  INTO @CurStudentStatusChangeId,@CurStuEnrollId,@CurOrigStatusCodeId,@CurNewStatusCodeId,@CurCampusId,@CurModDate,
            @CurModUser,@CurIsReversal,@CurDropReasonId,@CurDateOfChange; 
	
        WHILE @@FETCH_STATUS = 0
            BEGIN
                -- INSERT NEW

                INSERT  INTO dbo.syStudentStatusChanges
                        (
                         StudentStatusChangeId
                        ,StuEnrollId
                        ,OrigStatusId
                        ,NewStatusId
                        ,CampusId
                        ,ModDate
                        ,ModUser
                        ,IsReversal
                        ,DropReasonId
                        ,DateOfChange
                        )
                VALUES  (
                         @CurStudentStatusChangeId
                        ,@CurStuEnrollId
                        ,@CurOrigStatusCodeId
                        ,@CurNewStatusCodeId
                        ,@CurCampusId
                        ,@CurModDate
                        ,@CurModUser
                        ,@CurIsReversal
                        ,@CurDropReasonId
                        ,@CurDateOfChange
                        ); 

                FETCH NEXT FROM ToInsertCursor 
                INTO @CurStudentStatusChangeId,@CurStuEnrollId,@CurOrigStatusCodeId,@CurNewStatusCodeId,@CurCampusId,@CurModDate,@CurModUser,@CurIsReversal,
                    @CurDropReasonId,@CurDateOfChange; 
            END;
		
        CLOSE ToInsertCursor;
        DEALLOCATE ToInsertCursor;	

       --------------------------------------------------------------------------------

       ----------------------------------------------------------------------------------
       -- UPDATE SSSC.DateOfChange WITH SSSC.ModDate if DateOfChange is NULL
       ----------------------------------------------------------------------------------
        UPDATE  SSSC
        SET     SSSC.DateOfChange = SSSC.ModDate
        FROM    dbo.syStudentStatusChanges AS SSSC
        WHERE   SSSC.DateOfChange IS NULL;

    --   -----------------------------------------------------------------------
    --   ----------------------------------------------------------------------------------
    --   -- INSERT  Probation Status Changes for Type = 2,3 which are not Audit Detail or Studen Status Change 
    --   -- Update  arStuProbWarnings with StudentStatusChangeId
    --   ----------------------------------------------------------------------------------
    --    DECLARE @CuPStuProbWarningId        UNIQUEIDENTIFIER
    --    DECLARE @CuPStuEnrollId             UNIQUEIDENTIFIER
    --    DECLARE @CuPStartDate               DATETIME
    --    DECLARE @CuPReason	                NVARCHAR(300)
    --    DECLARE @CupProbWarningTypeId       INTEGER
    --    DECLARE @CuPStudentStatusChangeId   UNIQUEIDENTIFIER
    --    DECLARE @CuPModDate                 DATETIME

    --    DECLARE @PrevStatusId               UNIQUEIDENTIFIER 
    --    DECLARE @NextStatusId               UNIQUEIDENTIFIER
    --    DECLARE @NextStudentStatusChangeId  UNIQUEIDENTIFIER

    --    DECLARE @ProWarningId               UNIQUEIDENTIFIER
    --    DECLARE @ProDiciplinaryId           UNIQUEIDENTIFIER
    --    DECLARE @ProNewStatusId             UNIQUEIDENTIFIER
    --    DECLARE @ProCampusId                UNIQUEIDENTIFIER
    --    DECLARE @ProUser                    NVARCHAR(50)
    --    DECLARE @TransDate                  DATETIME
    --    DECLARE @Error                      INTEGER

    --    SET @ProUser   = 'support'
    --    SET @Error     = 0
        
    --    SELECT TOP 1  @ProWarningId = SSC.StatusCodeId  
    --    FROM dbo.syStatusCodes AS SSC 
    --        INNER JOIN dbo.sySysStatus AS SSS ON SSC.SysStatusId = SSS.SysStatusId 
    --    WHERE SSS.SysStatusId = 23
    --    ORDER BY SSC.StatusCode DESC 
        
    --    SELECT TOP 1  @ProDiciplinaryId = SSC.StatusCodeId  
    --    FROM dbo.syStatusCodes AS SSC 
    --        INNER JOIN dbo.sySysStatus AS SSS ON SSC.SysStatusId = SSS.SysStatusId 
    --    WHERE SSS.SysStatusId = 24
    --    ORDER BY SSC.StatusCode DESC 

    --    DECLARE ProbationCursor CURSOR FOR
    --                                         SELECT StuProbWarningId, StuEnrollId, StartDate, Reason, ASPW.ProbWarningTypeId, ASPW.ModDate
    --                                         FROM arStuProbWarnings AS ASPW 
    --                                         WHERE ASPW.ProbWarningTypeId IN (2, 3) 
    --                                         ORDER BY aspw.StuEnrollId, ASPW.StartDate, ASPW.ModDate

    --    OPEN ProbationCursor 
	   -- FETCH NEXT FROM ProbationCursor  INTO   @CuPStuProbWarningId, @CuPStuEnrollId, @CuPStartDate, @CuPReason, @CupProbWarningTypeId, @CuPModDate
                
	   -- WHILE @@FETCH_STATUS = 0
		  --  BEGIN
    --            SET @Error = 0
    --            SET @PrevStatusId = NULL
    --            SET @NextStudentStatusChangeId = '00000000-0000-0000-0000-000000000000'

    --            SELECT TOP 1 
    --                  --@PrevStatusId = ISNULL(SSSC.NewStatusId, '00000000-0000-0000-0000-000000000000') 
    --                  @PrevStatusId = SSSC.NewStatusId
    --            FROM dbo.syStudentStatusChanges AS SSSC 
    --            WHERE SSSC.StuEnrollId = @CuPStuEnrollId 
    --              AND (    SSSC.DateOfChange < @CuPStartDate 
    --                   OR (SSSC.DateOfChange = @CuPStartDate AND SSSC.ModDate <= @CuPStartDate)
    --                  )
    --            ORDER BY SSSC.DateOfChange DESC, SSSC.ModDate DESC 

    --            SELECT TOP 1 
    --                   @NextStudentStatusChangeId = ISNULL(SSSC.StudentStatusChangeId, '00000000-0000-0000-0000-000000000000')
    --            FROM dbo.syStudentStatusChanges AS SSSC 
    --            WHERE SSSC.StuEnrollId = @CuPStuEnrollId
    --              AND (    SSSC.DateOfChange > @CuPStartDate 
    --                   OR (SSSC.DateOfChange = @CuPStartDate AND SSSC.ModDate > @CuPStartDate)
    --                  )
    --            ORDER BY SSSC.DateOfChange, SSSC.ModDate 

    --            SELECT @ProCampusId = ASE.CampusId
    --            FROM dbo.arStuEnrollments AS ASE
    --            WHERE ASE.StuEnrollId = @CuPStuEnrollId 

    --            IF (@CupProbWarningTypeId = 2)
    --                BEGIN
    --                    SET @ProNewStatusId = @ProWarningId
    --                END
    --            IF (@CupProbWarningTypeId = 2)
    --                BEGIN
    --                    SET @ProNewStatusId = @ProDiciplinaryId
    --                END
                    

    --            -- INSERT NEW
    --            -- UPDATE NEXT
    --            -- UPDATE arStuProbWarning with new SSSC.StudentStatusChangeId
    --            BEGIN TRANSACTION INSERT_StudentStatusChange
    --            BEGIN TRY
    --                SET @TransDate = GETDATE()
    --                IF @Error = 0
    --                    BEGIN
    --                        -- INSERT NEW syStudentStatusChanges record
    --                        SET @CuPStudentStatusChangeId = NEWID()
    --                        INSERT INTO dbo.syStudentStatusChanges 
    --                               (StudentStatusChangeId,     StuEnrollId,     OrigStatusId,  NewStatusId,     CampusId,     ModDate,    ModUser,  IsReversal, DropReasonId, DateOfChange)
    --                        VALUES (@CuPStudentStatusChangeId, @CuPStuEnrollId, @PrevStatusId, @ProNewStatusId, @ProCampusId, @TransDate, @ProUser, 0,          NULL,         @CuPStartDate) 
    --                        SET @Error = @@ERROR
    --                    END 
    --                IF @Error = 0
    --                    BEGIN
    --                        -- Update Next syStudentStatusChanges
    --                        IF (@NextStudentStatusChangeId IS NOT NULL AND @NextStudentStatusChangeId <> '00000000-0000-0000-0000-000000000000')
    --                            BEGIN
    --                                UPDATE SSSC
    --                                   SET SSSC.OrigStatusId = @ProNewStatusId
    --                                FROM dbo.syStudentStatusChanges AS SSSC
    --                                WHERE SSSC.StudentStatusChangeId = @NextStudentStatusChangeId
    --                                SET @Error = @@ERROR
    --                            END
    --                    END
    --                IF @Error = 0
    --                    BEGIN
    --                        UPDATE ASPW
    --                           SET ASPW.StudentStatusChangeId = @CuPStudentStatusChangeId
    --                        FROM dbo.arStuProbWarnings AS ASPW
    --                        WHERE ASPW.StuProbWarningId = @CuPStuProbWarningId
    --                        SET @Error = @@ERROR
    --                    END
    --                IF @Error = 0
    --                    BEGIN
    --                        COMMIT TRANSACTION INSERT_StudentStatusChange 
    --                    END
    --                ELSE
    --                    BEGIN
    --                        ROLLBACK TRANSACTION INSERT_StudentStatusChange
    --                    END
    --            END TRY
    --            BEGIN CATCH
    --                ROLLBACK TRANSACTION  INSERT_StudentStatusChange
    --                SELECT ERROR_NUMBER() AS ErrorNumber
    --                     , ERROR_SEVERITY() AS ErrorSeverity
    --                     , ERROR_STATE() AS ErrorState
    --                     , ERROR_PROCEDURE() AS ErrorProcedure
    --                     , ERROR_LINE() AS ErrorLine
    --                     , ERROR_MESSAGE() AS ErrorMessage;
    --                SELECT StuProbWarningId, StuEnrollId, StartDate, Reason, ASPW.ProbWarningTypeId, ASPW.ModDate
    --                                         FROM arStuProbWarnings AS ASPW 
    --                                         WHERE ASPW.StuProbWarningId = @CuPStuProbWarningId
    --                SELECT @PrevStatusId
    --             END CATCH

    --            FETCH NEXT FROM ProbationCursor 
    --            INTO   @CuPStuProbWarningId, @CuPStuEnrollId, @CuPStartDate, @CuPReason, @CupProbWarningTypeId, @CuPModDate
    --        END
		
	   --CLOSE ProbationCursor
	   --DEALLOCATE ProbationCursor	
	

    END;

GO
