SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[usp_GetAvailableDeposits]
    (
     @BankAcct VARCHAR(200)
    ,@CampusId VARCHAR(200)
    ,@StartDate AS DATETIME
    ,@EndDate AS DATETIME
    ,@IsDeposited BIT = 0
    )
AS
    BEGIN
	
        DECLARE @outputTable TABLE
            (
             TransactionId UNIQUEIDENTIFIER
            ,Description VARCHAR(500)
            ,Name VARCHAR(500)
            ,StudentNumber VARCHAR(250)
            ,BankDescrip VARCHAR(500)
            ,BankAcctDescrip VARCHAR(500)
            ,TransDate DATETIME
            ,TransReference VARCHAR(500)
            ,TransAmount DECIMAL(18,2)
            ,Available INTEGER
            ,Selected INTEGER
            ,IsLead INTEGER
            ,EntityType VARCHAR(100)
            ,DepositState VARCHAR(100)
            );

        INSERT  INTO @outputTable
                SELECT  T.TransactionId
                       ,T.TransDescrip AS DESCRIPTION
                       ,( L.LastName + ', ' + L.FirstName + ' ' + COALESCE(L.MiddleName,' ') ) AS Name
                       ,NULL AS StudentNumber
                       ,(
                          SELECT    BankDescrip
                          FROM      saBankCodes
                          WHERE     BankId = (
                                               SELECT   BankId
                                               FROM     saBankAccounts
                                               WHERE    BankAcctId = @BankAcct
                                             )
                        ) BankDescrip
                       ,(
                          SELECT    BankAcctDescrip
                          FROM      saBankAccounts
                          WHERE     BankAcctId = @BankAcct
                        ) AS BankAcctDescrip
                       ,T.TransDate
                       ,T.TransReference
                       ,( T.TransAmount * ( -1 ) ) AS TransAmount
                       ,1 AS Available
                       ,0 AS Selected
                       ,1 AS IsLead
                       ,'Lead' AS EntityType
                       ,( CASE WHEN @IsDeposited = 1 THEN 'Deposited'
                               ELSE 'Undeposited'
                          END ) AS DepositState
                FROM    (
                          SELECT    *
                          FROM      adLeads
                          WHERE     LeadId IN ( SELECT  LeadId
                                                FROM    adLeadByLeadGroups
                                                WHERE   StuEnrollId IS NULL )
                                    OR adLeads.LeadId NOT IN ( SELECT   LeadId
                                                               FROM     arStuEnrollments )
                        ) E
                INNER JOIN adLeadTransactions T ON E.LeadId = T.LeadId
                INNER JOIN adLeads L ON E.LeadId = L.LeadId
                INNER JOIN adLeadPayments P ON P.TransactionId = T.TransactionId
											--INNER JOIN saTransCodes S ON S.TransCodeId = T.TransCodeId
											--INNER JOIN saSysTransCodes C ON C.SysTransCodeId = S.SysTransCodeId
                WHERE   T.TransTypeId IN ( 0,2 )
                        AND T.Voided = 0
                        AND T.CampusId = @CampusId
                        AND T.TransDate >= @StartDate
                        AND T.TransDate <= @EndDate
                        AND P.IsDeposited = @IsDeposited
                        AND P.PaymentTypeId IN ( 1,2,5 )
                UNION ALL
                SELECT  T.TransactionId
                       ,T.TransDescrip AS Description
                       ,(
                          SELECT    L.LastName + ', ' + L.FirstName + ' ' + COALESCE(L.MiddleName,' ')
                          FROM      arStudent L
                          WHERE     StudentId = (
                                                  SELECT    StudentId
                                                  FROM      arStuEnrollments
                                                  WHERE     StuEnrollId = T.StuEnrollId
                                                )
                        ) Name
                       ,(
                          SELECT    CASE (
                                           SELECT TOP 1
                                                    Value
                                           FROM     syConfigAppSetValues
                                           WHERE    SettingId = 69
                                         )
                                      WHEN 'SSN' THEN St.SSN
                                      WHEN 'StudentId' THEN St.StudentNumber
                                      WHEN 'EnrollmentId' THEN (
                                                                 SELECT TOP 1
                                                                        EnrollmentId
                                                                 FROM   arStuEnrollments SE
                                                                 WHERE  SE.StudentId = St.StudentId
                                                                 ORDER BY SE.StartDate DESC
                                                               )
                                    END
                        ) AS StudentNumber
                       ,(
                          SELECT    BankDescrip
                          FROM      saBankCodes
                          WHERE     BankId = (
                                               SELECT   BankId
                                               FROM     saBankAccounts
                                               WHERE    BankAcctId = @BankAcct
                                             )
                        ) BankDescrip
                       ,(
                          SELECT    BankAcctDescrip
                          FROM      saBankAccounts
                          WHERE     BankAcctId = @BankAcct
                        ) AS BankAcctDescrip
                       ,T.TransDate
                       ,T.TransReference
                       ,( T.TransAmount * ( -1 ) ) AS TransAmount
                       ,1 AS Available
                       ,0 AS Selected
                       ,0 AS IsLead
                       ,'Student' AS EntityType
                       ,( CASE WHEN @IsDeposited = 1 THEN 'Deposited'
                               ELSE 'Undeposited'
                          END ) AS DepositState
                FROM    saPayments P
                INNER JOIN saTransactions T ON P.TransactionId = T.TransactionId
	--INNER JOIN		saTransCodes S ON S.TransCodeId = T.TransCodeId			
	--INNER JOIN		saSysTransCodes C ON C.SysTransCodeId = S.SysTransCodeId 
                INNER JOIN arStuEnrollments se ON se.StuEnrollId = T.StuEnrollId
                INNER JOIN arStudent st ON st.StudentId = se.StudentId
                WHERE   P.IsDeposited = @IsDeposited
                        AND T.Voided = 0
                        AND T.CampusId = @CampusId
                        AND T.TransDate >= @StartDate
                        AND T.TransDate <= @EndDate
                        AND P.PaymentTypeId IN ( 1,2,5 );

        SELECT  *
        FROM    @outputTable
        ORDER BY TransDate
               ,Name
               ,TransAmount;

    END;



GO
