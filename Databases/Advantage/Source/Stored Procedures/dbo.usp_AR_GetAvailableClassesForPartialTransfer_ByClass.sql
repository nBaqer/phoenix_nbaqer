SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/* 
US3466 Transfer Partially Completed Components By Class

CREATED: 
9/9/2013 WMP

PURPOSE: 
Select classes available for a course to transfer partially completed components
	Requirements:
	- same course but different class section
	- current or future term
	- seats remaining

MODIFIED:
9/30/2013 WMP	DE10111 make campus specific

*/

CREATE PROCEDURE [dbo].[usp_AR_GetAvailableClassesForPartialTransfer_ByClass]
    @ReqId UNIQUEIDENTIFIER = NULL
   ,@CampusId UNIQUEIDENTIFIER = NULL
AS
    BEGIN

        SELECT  cs.ClsSectionId
               ,cs.ClsSection
               ,rq.Code AS CourseCode
               ,cs.MaxStud AS MaxStudents
               ,(
                  SELECT    COUNT(0)
                  FROM      arResults rs
                  WHERE     rs.TestId = cs.ClsSectionId
                ) AS Assigned
               ,cs.MaxStud - (
                               SELECT   COUNT(0)
                               FROM     arResults rs
                               WHERE    rs.TestId = cs.ClsSectionId
                             ) AS Remaining
               ,cs.ClsSection + ' (Max Students ' + CAST(cs.MaxStud AS VARCHAR) + ')' AS Display
               ,cs.StartDate AS ClsSectionStartDate
               ,cs.EndDate AS ClsSectionEndDate
               ,t.TermCode
               ,t.TermDescrip
        FROM    arClassSections cs
        INNER JOIN arReqs rq ON rq.ReqId = cs.ReqId
        INNER JOIN arTerm t ON t.TermId = cs.TermId
        WHERE   cs.ReqId = @ReqId
                AND cs.CampusId = @CampusId
                AND cs.EndDate >= GETDATE()
                AND ( cs.MaxStud - (
                                     SELECT COUNT(0)
                                     FROM   arResults rs
                                     WHERE  rs.TestId = cs.ClsSectionId
                                   ) ) > 0
        ORDER BY cs.StartDate
               ,cs.ClsSection;

    END;



GO
