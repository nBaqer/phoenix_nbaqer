SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_GetAvailableStudentsForClassRestrictedByStartDate]
    @clsSectionId UNIQUEIDENTIFIER
   ,@leadGrpId UNIQUEIDENTIFIER
   ,@campusId UNIQUEIDENTIFIER
AS
    SET NOCOUNT ON;  
  
    IF @leadGrpId = '00000000-0000-0000-0000-000000000000'
        BEGIN  
            EXEC dbo.usp_GetAvailableStudents_WithNoLeadGroups @clsSectionId,@campusId;  
        END;  
    ELSE
        BEGIN  
            EXEC dbo.usp_GetAvailableStudents_WithLeadGroups @clsSectionId,@leadGrpId,@campusId;  
        END;  



GO
