SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_GetInstructors]
AS
    SET NOCOUNT ON;
    SELECT  T.UserId
           ,Fullname
    FROM    syUsers T
           ,syUsersRolesCampGrps b
           ,syRoles c
    WHERE   T.AccountActive = 1
            AND T.UserId = b.UserId
            AND b.RoleId = c.RoleId
            AND c.SysRoleId = 2
    ORDER BY T.Fullname;





GO
