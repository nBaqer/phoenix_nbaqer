SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_MentorProctoredTest_GetList]
    @EffectiveDate DATETIME
   ,@ReqId UNIQUEIDENTIFIER
AS
    SELECT  MentorRequirement
           ,MentorOperator
           ,ModDate
    FROM    arMentor_GradeComponentTypes_Courses
    WHERE   ReqId = @ReqId
            AND EffectiveDate = @EffectiveDate
    ORDER BY ModDate; 



GO
