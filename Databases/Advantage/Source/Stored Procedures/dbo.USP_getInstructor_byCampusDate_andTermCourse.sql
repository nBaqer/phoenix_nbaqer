SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--=================================================================================================
-- USP_getInstructor_byCampusDate_andTermCourse
--=================================================================================================
CREATE PROCEDURE [dbo].[USP_getInstructor_byCampusDate_andTermCourse]
    @Campus UNIQUEIDENTIFIER
   ,@WeekEndDate DATE
   ,@TermId VARCHAR(8000) = NULL
   ,@CourseId VARCHAR(8000) = NULL
   ,@ShowInactive BIT
AS --    
    BEGIN    
        IF LEN(@TermId) = 0
            BEGIN
             
                SET @TermId = NULL;
            END;
        IF LEN(@CourseId) = 0
            BEGIN
             
                SET @CourseId = NULL;
            END;
        DECLARE @ClassesThatFallWithinDateRange TABLE
            (
             InstructorId UNIQUEIDENTIFIER
            );    
    
        INSERT  INTO @ClassesThatFallWithinDateRange
                SELECT DISTINCT
                        InstructorId UNIQUEIDENTIFIER
                FROM    dbo.arClassSections ARC
                WHERE   @WeekEndDate BETWEEN ARC.StartDate AND ARC.EndDate
                        AND ARC.CampusId = @Campus
                        AND (
                              @TermId IS NULL
                              OR ARC.TermId IN ( SELECT strval
                                                 FROM   dbo.SPLIT(@TermId) )
                            )
                        AND (
                              @CourseId IS NULL
                              OR ARC.ReqId IN ( SELECT  strval
                                                FROM    dbo.SPLIT(@CourseId) )
                            );     
    
        SELECT DISTINCT
                Inst.UserId InstructorId
               ,Inst.FullName
        FROM    syUsers Inst
        INNER JOIN @ClassesThatFallWithinDateRange CDR ON Inst.UserId = CDR.InstructorId
        WHERE   (
                  @ShowInactive = 0
                  AND Inst.AccountActive = 1
                )
                OR @ShowInactive = 1
        ORDER BY Inst.FullName;     
    END;    
--=================================================================================================
-- END  --  USP_getInstructor_byCampusDate_andTermCourse
--=================================================================================================
GO
