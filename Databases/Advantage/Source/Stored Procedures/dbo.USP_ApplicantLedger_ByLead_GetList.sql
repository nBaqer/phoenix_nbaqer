SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_ApplicantLedger_ByLead_GetList]
    @CampusId UNIQUEIDENTIFIER
   ,@TransStartDate DATE
   ,@TransEndDate DATE
   ,@Transaction VARCHAR(50)
   ,@LeadId UNIQUEIDENTIFIER
AS --Set @LeadId='43D6FD75-5453-4F22-9283-BC7A213128FB'  
    IF LOWER(@Transaction) = 'all'
        BEGIN
            SELECT  LT.TransactionId
                   ,LT.LeadId
                   ,(
                      SELECT    L.Firstname + ' ' + L.LastName
                    ) AS LeadName
                   ,LT.TransCodeId
                   ,LT.IsEnrolled
                   ,(
                      SELECT DISTINCT
                                TransCodeDescrip
                      FROM      saTransCodes
                      WHERE     TransCodeId = LT.TransCodeId
                    ) AS TransactionCode
                   ,LT.TransDescrip AS TransDescription
                   ,CASE WHEN Voided = 1 THEN 'Payment (Voided)'
                         ELSE (
                                SELECT DISTINCT
                                        Description
                                FROM    saTransTypes
                                WHERE   TransTypeId = LT.TransTypeId
                              )
                    END AS TransactionType
                   ,LT.ModUser AS Postedby
                   ,LT.TransAmount AS TransactionAmount
                   ,CONVERT(CHAR(10),LT.CreatedDate,101) AS TransDate
                   ,TransReference
                   ,(
                      SELECT TOP 1
                                CheckNumber
                      FROM      adLeadPayments
                      WHERE     TransactionId = LT.TransactionId
                    ) AS DocumentId
                   ,LT.Voided AS Voided
                   ,LT.ReversalReason
            FROM    adLeadTransactions LT
            INNER JOIN adLeads L ON LT.LeadId = L.LeadId
            WHERE   L.CampusId = @CampusId
                    AND L.LeadId = @LeadId
                    AND LT.TransTypeId IN ( 2 )
                    AND LT.Voided = 0
                    AND -- show only non-voided transactions
			--(LT.ReversalReason is null) and -- show only items not reversed 
                    (
                      CONVERT(CHAR(10),LT.CreatedDate,101) >= @TransStartDate
                      AND CONVERT(CHAR(10),LT.CreatedDate,101) <= @TransEndDate
                    )
            ORDER BY LT.DisplaySequence
                   ,LT.SecondDisplaySequence;
		
		--Select * from adLeadTransactions where (ReversalReason is not null or TransDescrip like '%-Rever%')
		--Select * from saTransTypes
        END;

    IF LOWER(@Transaction) = 'voided'
        BEGIN
            SELECT  LT.TransactionId
                   ,LT.LeadId
                   ,(
                      SELECT    L.Firstname + ' ' + L.LastName
                    ) AS LeadName
                   ,LT.TransCodeId
                   ,LT.IsEnrolled
                   ,(
                      SELECT DISTINCT
                                TransCodeDescrip
                      FROM      saTransCodes
                      WHERE     TransCodeId = LT.TransCodeId
                    ) AS TransactionCode
                   ,LT.TransDescrip AS TransDescription
                   ,CASE WHEN Voided = 1 THEN 'Payment (Voided)'
                         ELSE (
                                SELECT DISTINCT
                                        Description
                                FROM    saTransTypes
                                WHERE   TransTypeId = LT.TransTypeId
                              )
                    END AS TransactionType
                   ,LT.ModUser AS Postedby
                   ,LT.TransAmount AS TransactionAmount
                   ,CONVERT(CHAR(10),LT.CreatedDate,101) AS TransDate
                   ,TransReference
                   ,(
                      SELECT TOP 1
                                CheckNumber
                      FROM      adLeadPayments
                      WHERE     TransactionId = LT.TransactionId
                    ) AS DocumentId
                   ,LT.Voided AS Voided
                   ,LT.ReversalReason
            FROM    adLeadTransactions LT
            INNER JOIN adLeads L ON LT.LeadId = L.LeadId
            WHERE   L.CampusId = @CampusId
                    AND L.LeadId = @LeadId
                    AND LT.TransTypeId = 2
                    AND (
                          CONVERT(CHAR(10),LT.CreatedDate,101) >= @TransStartDate
                          AND CONVERT(CHAR(10),LT.CreatedDate,101) <= @TransEndDate
                        )
                    AND LT.Voided = 1
            ORDER BY LT.DisplaySequence
                   ,LT.SecondDisplaySequence;
		
        END;

    IF LOWER(@Transaction) = 'reversed'
        BEGIN
            SELECT  LT.TransactionId
                   ,LT.LeadId
                   ,(
                      SELECT    L.Firstname + ' ' + L.LastName
                    ) AS LeadName
                   ,LT.TransCodeId
                   ,LT.IsEnrolled
                   ,(
                      SELECT DISTINCT
                                TransCodeDescrip
                      FROM      saTransCodes
                      WHERE     TransCodeId = LT.TransCodeId
                    ) AS TransactionCode
                   ,LT.TransDescrip AS TransDescription
                   ,CASE WHEN Voided = 1 THEN 'Payment (Voided)'
                         ELSE (
                                SELECT DISTINCT
                                        Description
                                FROM    saTransTypes
                                WHERE   TransTypeId = LT.TransTypeId
                              )
                    END AS TransactionType
                   ,LT.ModUser AS Postedby
                   ,LT.TransAmount AS TransactionAmount
                   ,CONVERT(CHAR(10),LT.CreatedDate,101) AS TransDate
                   ,TransReference
                   ,(
                      SELECT TOP 1
                                CheckNumber
                      FROM      adLeadPayments
                      WHERE     TransactionId = LT.TransactionId
                    ) AS DocumentId
                   ,LT.Voided AS Voided
                   ,LT.ReversalReason
            FROM    adLeadTransactions LT
            INNER JOIN adLeads L ON LT.LeadId = L.LeadId
            WHERE   L.CampusId = @CampusId
                    AND L.LeadId = @LeadId
                    AND LT.TransTypeId IN ( 1,2 )
                    AND (
                          CONVERT(CHAR(10),LT.CreatedDate,101) >= @TransStartDate
                          AND CONVERT(CHAR(10),LT.CreatedDate,101) <= @TransEndDate
                        )
                    AND LT.Voided = 0
                    AND (
                          LT.ReversalReason IS NOT NULL
                          OR LT.TransDescrip LIKE '%- Adjustment'
                        )
            ORDER BY LT.DisplaySequence
                   ,LT.SecondDisplaySequence;
        END;



GO
