SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_FallPartA_DistanceED_GetList]
    @CampusId VARCHAR(50)
   ,@ProgId VARCHAR(4000) = NULL
   ,@StartDate DATETIME = NULL
   ,@EndDate DATETIME = NULL
   ,@DateRangeText VARCHAR(100) = NULL
   ,@OrderBy VARCHAR(100)
AS
    DECLARE @ContinuingStartDate DATETIME;
    DECLARE @Value VARCHAR(50);
    DECLARE @State VARCHAR(12);

   -- SET @Value = 'letter'
    SET @Value = (
                   SELECT   dbo.GetAppSettingValue(47,@CampusId)
                 );
    SET @ContinuingStartDate = @StartDate;
-- For Academic Year Reporter, the End Date should be 10/15/Cohort Year
-- and Start Date should be blank
    --IF DAY(@EndDate) = 15
    --    AND MONTH(@EndDate) = 10 
    IF SUBSTRING(LOWER(@DateRangeText),1,10) = 'enrollment'
        BEGIN
            SET @EndDate = @StartDate; -- it will always be 10/15/cohort year example: for cohort year 2009, it is 10/15/2009
            SET @StartDate = @StartDate; -- it will always be 10/15/cohort year example: for cohort year 2009, it is 10/15/2009
        END;
			
	
    SET @State = (
                   SELECT   StateCode
                   FROM     Systates
                   WHERE    StateId IN ( SELECT StateId
                                         FROM   SyCampuses
                                         WHERE  Campusid = @CampusId )
                 );

	
--Create Table #FallPartADistanceEd
--			(RowNumber uniqueidentifier,SSN varchar(50),StudentNumber varchar(50),StudentName varchar(100),
--			UGDegreeSeeking varchar(10),UGNonDegreeSeeking varchar(10),
--			UGDegreeSeekingCount int,UGNonDegreeSeekingCount int,
--			StudentId UNIQUEIDENTIFIER,DistanceEdStatus varchar(10),UnderGrad int, Grad Int,Location Int,stateCode varchar(12),Enrollid Varchar(50))

--Insert into #FallPartADistanceEd
    SELECT  NEWID() AS RowNumber
           ,dbo.UDF_FormatSSN(SSN) AS SSN
           ,StudentNumber
           ,StudentName
           ,
		--Case When DegreeSeeking>=1 Then 'X' else '' end as DegreeSeeking,
            (
              SELECT    CASE WHEN UnderGrad >= 1
                                  AND FirstTime >= 1 THEN 'X'
                             WHEN (
                                    UnderGrad >= 1
                                    AND (
                                          Continuing = 0
                                          OR Continuing IS NULL
                                        )
                                    AND TransferIn >= 1
                                  ) THEN 'X'
                             WHEN UnderGrad >= 1
                                  AND (
                                        Continuing >= 1
                                        AND (
                                              TransferIn = 0
                                              OR TransferIn IS NULL
                                              OR TransferIn = 1
                                            )
                                        AND (
                                              FirstTime = 0
                                              OR FirstTime IS NULL
                                            )
                                      ) THEN 'X'
                             ELSE ''
                        END
            ) AS UGDegreeseeking
           ,CASE WHEN (
                        UnderGrad >= 1
                        AND NonDegreeSeeking >= 1
                        AND (
                              TransferIn = 0
                              OR TransferIn IS NULL
                            )
                        AND (
                              FirstTime = 0
                              OR FirstTime IS NULL
                            )
                      ) THEN 'X'
                 ELSE ''
            END AS UGNonDegreeSeeking
           ,
		 --Case When DegreeSeeking>=1 then 1 else 0 end as DegreeSeekingCount,
            (
              SELECT    CASE WHEN UnderGrad >= 1
                                  AND FirstTime >= 1 THEN 1
                             WHEN (
                                    UnderGrad >= 1
                                    AND (
                                          Continuing = 0
                                          OR Continuing IS NULL
                                        )
                                    AND TransferIn >= 1
                                  ) THEN 1
                             WHEN UnderGrad >= 1
                                  AND (
                                        Continuing >= 1
                                        AND (
                                              TransferIn = 0
                                              OR TransferIn IS NULL
                                              OR TransferIn = 1
                                            )
                                        AND (
                                              FirstTime = 0
                                              OR FirstTime IS NULL
                                            )
                                      ) THEN 1
                             ELSE 0
                        END
            ) AS UGDegreeseekingCount
           ,CASE WHEN (
                        NonDegreeSeeking >= 1
                        AND (
                              TransferIn = 0
                              OR TransferIn IS NULL
                            )
                        AND (
                              FirstTime = 0
                              OR FirstTime IS NULL
                            )
                      ) THEN 1
                 ELSE 0
            END AS UGNonDegreeSeekingCount
           ,StudentId
           ,DistanceEdStatus
           ,UnderGrad
           ,Grad
           ,Location
           ,@State AS StateCode
           ,EnrollmentId
    FROM    (
              SELECT    t1.SSN
                       ,t1.StudentNumber
                       ,t1.LastName + ', ' + t1.FirstName + ' ' + ISNULL(t1.MiddleName,'') AS StudentName
                       ,CASE WHEN ( SUBSTRING(LOWER(@DateRangeText),1,10) = 'enrollment' ) THEN  -- IF ACADEMIC PROGRAM
                                  (
                                    SELECT  COUNT(*)
                                    FROM    arStuEnrollments SQ1
                                           ,adDegCertSeeking SQ2
                                    WHERE   SQ1.StuEnrollId = t2.StuEnrollId
                                            AND SQ1.StartDate <= @EndDate
                                            AND SQ1.degcertseekingid = SQ2.DegCertSeekingId
                                            AND SQ2.IPEDSValue = 11
                                  )
                             ELSE -- If PROGRAM Reporter
                                  (
                                    SELECT  COUNT(*)
                                    FROM    arStuEnrollments SQ1
                                           ,adDegCertSeeking SQ2
                                    WHERE   SQ1.StuEnrollId = t2.StuEnrollId
                                            AND (
                                                  SQ1.StartDate >= @StartDate
                                                  AND SQ1.StartDate <= @EndDate
                                                )
                                            AND SQ1.degcertseekingid = SQ2.DegCertSeekingId
                                            AND SQ2.IPEDSValue = 11
                                  )
                        END AS FirstTime
                       ,
	-- If Student is a transfer-in from another school, then there should not be any previous enrollments
	-- and the current enrollment we are dealing with at this point, should be the first enrollment

	-- Check if student has multiple enrollments and also if there any previous enrollments
	-- In addition, check if TransferHours>0, works only for clock hour schools (or)
	-- If student received any grade marked as Transfer Grade in GRade systems page (or)
	-- If IsTransferred column is set to 1 in arResults page
	--(Select count(*) from arStuEnrollments where StudentId=SQ1.StudentId and LeadId is not null)=1

	-- Consider only students who transferred from another institution
	-- So, consider only the first enrollment, lead id will have a value for 1st enrollment
                        (
                          SELECT    COUNT(*)
                          FROM      arStuEnrollments SQ1
                                   ,adDegCertSeeking SQ2
                          WHERE     SQ1.StuEnrollId = t2.StuEnrollId
                                    AND SQ1.LeadId IS NOT NULL
                                    AND SQ1.degcertseekingid = SQ2.DegCertSeekingId
                                    AND SQ2.IPEDSValue = 11
                                    AND (
                                          SQ1.TransferHours > 0
                                          OR SQ1.StuEnrollId = CASE WHEN @Value = 'letter'
                                                                    THEN (
                                                                           SELECT TOP 1
                                                                                    StuEnrollId
                                                                           FROM     arTransferGrades
                                                                           WHERE    StuEnrollId = SQ1.StuEnrollId
                                                                                    AND GrdSysDetailId IN ( SELECT  GrdSysDetailId
                                                                                                            FROM    arGradeSystemDetails
                                                                                                            WHERE   isTransferGrade = 1 )
                                                                         )
                                                                    ELSE (
                                                                           SELECT TOP 1
                                                                                    StuEnrollId
                                                                           FROM     arTransferGrades
                                                                           WHERE    IsTransferred = 1
                                                                                    AND StuEnrollId = SQ1.StuEnrollId
                                                                         )
                                                               END
                                        )
                                    AND SQ1.StartDate <= @EndDate
                                    AND NOT EXISTS ( SELECT StuEnrollId
                                                     FROM   arStuEnrollments
                                                     WHERE  StudentId = SQ1.StudentId
                                                            AND StartDate < SQ1.StartDate )
                        ) AS TransferIn
                       ,
				--(Select Count(*) from arStuEnrollments SQ1,adDegCertSeeking  SQ2--,arProgTypes SQ3
				--	where 
				--		SQ1.StuEnrollId = t2.StuEnrollId and 
				--		SQ1.degcertseekingid = SQ2.DegCertSeekingId  and
				--		SQ2.IPEDSValue = 11  
				--) as DegreeSeeking,
                        (
                          SELECT    COUNT(*)
                          FROM      arStuEnrollments SQ1
                                   ,adDegCertSeeking SQ2--,arProgTypes SQ3
                          WHERE     SQ1.StuEnrollId = t2.StuEnrollId
                                    AND SQ1.degcertseekingid = SQ2.DegCertSeekingId
                                    AND SQ2.IPEDSValue = 10
                        ) AS NonDegreeSeeking
                       ,
				--(Select Count(*) from arStuEnrollments SQ1,adDegCertSeeking SQ2,syStatusCodes SQ3,sySysStatus SQ4
				--	where 
				--		SQ1.StuEnrollId = t2.StuEnrollId and 
				--		SQ1.degcertseekingid = SQ2.DegCertSeekingId and
				--		-- The StartDate condition modified to < @StartDate due to Rally case DE868
				--		-- originally it was SQ1.StartDate<=@StartDate
				--		(SQ2.IPEDSValue = 12 or (SQ2.IPEDSValue=11 and SQ1.StartDate<@ContinuingStartDate)) 
				--		 and SQ1.StatusCodeId = SQ3.StatusCodeId and SQ3.SysStatusId = SQ4.SysStatusId  -- and SQ4.InSchool=1
				--	) as Continuing,
                        CASE WHEN ( SUBSTRING(LOWER(@DateRangeText),1,10) = 'enrollment' ) THEN  -- IF ACADEMIC PROGRAM
                                  (
                                    SELECT  COUNT(*)
                                    FROM    arStuEnrollments SQ1
                                           ,adDegCertSeeking SQ2
                                           ,syStatusCodes SQ3
                                           ,sySysStatus SQ4
                                    WHERE   SQ1.StuEnrollId = t2.StuEnrollId
                                            AND SQ1.degcertseekingid = SQ2.DegCertSeekingId
                                            AND
						-- The StartDate condition modified to < @StartDate due to Rally case DE868
						-- originally it was SQ1.StartDate<=@StartDate
                                            ( SQ2.IPEDSValue = 12 )
                                            AND SQ1.StatusCodeId = SQ3.StatusCodeId
                                            AND SQ3.SysStatusId = SQ4.SysStatusId  -- and SQ4.InSchool=1
                                  )
                             ELSE (
                                    SELECT  COUNT(*)
                                    FROM    arStuEnrollments SQ1
                                           ,adDegCertSeeking SQ2
                                           ,syStatusCodes SQ3
                                           ,sySysStatus SQ4
                                    WHERE   SQ1.StuEnrollId = t2.StuEnrollId
                                            AND SQ1.degcertseekingid = SQ2.DegCertSeekingId
                                            AND
						-- The StartDate condition modified to < @StartDate due to Rally case DE868
						-- originally it was SQ1.StartDate<=@StartDate
                                            (
                                              SQ2.IPEDSValue = 12
                                              OR (
                                                   SQ2.IPEDSValue = 11
                                                   AND SQ1.StartDate < @ContinuingStartDate
                                                 )
                                            )
                                            AND SQ1.StatusCodeId = SQ3.StatusCodeId
                                            AND SQ3.SysStatusId = SQ4.SysStatusId  -- and SQ4.InSchool=1
                                  )
                        END AS Continuing
                       ,t1.StudentId
                       ,CASE WHEN t2.DistanceEdStatus IS NULL THEN 'None'
                             ELSE t2.DistanceEdStatus
                        END AS DistanceEdStatus
                       ,
				--(Select Case When t9.IPEDSValue=58 THEN 1 else 0 end) as UnderGrad,
				--(Select Case When t9.IPEDSValue=59 THEN 1 else 0 end) as Grad,
                        1 AS UnderGrad
                       ,0 AS Grad
                       ,(
                          SELECT    CASE WHEN 
					--(select TOP 1 CountryCode from adCountries 
					--	where Countryid = (select Top 1 Countryid from arStudAddresses
					--					 where Studentid= t1.StudentId) )='usa' Then
                                              (
                                                SELECT TOP 1
                                                        ForeignZip
                                                FROM    arstudaddresses
                                                WHERE   Studentid = t1.StudentId
                                                        AND default1 = 1
                                              ) = 0 THEN ( CASE WHEN (
                                                                       SELECT TOP 1
                                                                                StateCode
                                                                       FROM     Systates
                                                                       WHERE    stateid = (
                                                                                            SELECT TOP 1
                                                                                                    stateid
                                                                                            FROM    arStudAddresses
                                                                                            WHERE   Studentid = t1.StudentId
                                                                                                    AND default1 = 1
                                                                                          )
                                                                     ) = @State THEN 1
                                                                WHEN (
                                                                       (
                                                                         SELECT TOP 1
                                                                                StateCode
                                                                         FROM   Systates
                                                                         WHERE  stateid = (
                                                                                            SELECT TOP 1
                                                                                                    stateid
                                                                                            FROM    arStudAddresses
                                                                                            WHERE   Studentid = t1.StudentId
                                                                                                    AND default1 = 1
                                                                                          )
                                                                       ) <> @State
                                                                       AND (
                                                                             SELECT TOP 1
                                                                                    StateCode
                                                                             FROM   Systates
                                                                             WHERE  stateid = (
                                                                                                SELECT TOP 1
                                                                                                        stateid
                                                                                                FROM    arStudAddresses
                                                                                                WHERE   Studentid = t1.StudentId
                                                                                                        AND default1 = 1
                                                                                              )
                                                                           ) IS NOT NULL
                                                                     ) THEN 2
                                                                ELSE 3
                                                           END )
                                         ELSE 4
                                    END
                        ) AS Location
                       ,t2.Enrollmentid AS EnrollmentId
              FROM      arStudent t1
              INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
              INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
              INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                           AND t6.SysStatusId NOT IN ( 8 )
              INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
              INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
              INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
              LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
              INNER JOIN adCitizenships t11 ON t1.Citizen = t11.CitizenshipId
              INNER JOIN adDegCertSeeking t12 ON t2.DegCertSeekingid = t12.DegCertSeekingid
              WHERE     t2.CampusId = LTRIM(RTRIM(@CampusId))
                        AND 
				-- (@ProgId is null or t8.ProgId in (Select Val from [MultipleValuesForReportParameters](@ProgId,',',1))) and
                        (
                          (
                            t8.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
                            AND @ProgId IS NULL
                          )
                          OR t8.ProgId IN ( SELECT  Val
                                            FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                        )
                        AND t9.IPEDSValue = 58
                        AND t12.IPEDSValue IS NOT NULL
                        AND t9.IPEDSValue IS NOT NULL
                        AND (
                              t10.IPEDSValue = 61
                              OR t10.IPEDSValue = 62
                            )
                        AND -- Full Time or Part Time
				-- t10.IPEDSValue=61 -- and 
				-- and (t11.IPEDSValue <> 65 or t11.IPEDSValue is NULL) -- Ignore Non US Citizens (Foreign Students)
				--and (@StartDate is null or t2.StartDate>=@StartDate) and (@EndDate is null or t2.StartDate<=@EndDate) 
                        t2.StartDate <= @EndDate
                        AND t2.StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
						( SELECT    t1.StuEnrollId
                          FROM      arStuEnrollments t1
                                   ,SyStatusCodes t2
                          WHERE     t1.StatusCodeId = t2.StatusCodeId
                                    AND StartDate <= @EndDate
                                    AND -- Student started before the end date range
                                    LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                    AND
							--(@ProgId is null or t8.ProgId in (Select Val from [MultipleValuesForReportParameters](@ProgId,',',1)))   
                                    (
                                      (
                                        t8.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
                                        AND @ProgId IS NULL
                                      )
                                      OR t8.ProgId IN ( SELECT  Val
                                                        FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                    )
                                    AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                    AND (
                                          t1.DateDetermined < @StartDate
                                          OR ExpGradDate < @StartDate
                                          OR LDA < @StartDate
                                        ) )
							-- If Student is enrolled in only program version and if that program version 
						-- happens to be a continuing ed program exclude the student
                        AND t2.StudentId NOT IN ( SELECT DISTINCT
                                                            StudentId
                                                  FROM      (
                                                              SELECT    StudentId
                                                                       ,COUNT(*) AS RowCounter
                                                              FROM      arStuENrollments
                                                              WHERE     PrgVerId IN ( SELECT    PrgVerId
                                                                                      FROM      arPrgVersions
                                                                                      WHERE     IsContinuingEd = 1 )
                                                              GROUP BY  StudentId
                                                              HAVING    COUNT(*) = 1
                                                            ) dtStudent_ContinuingEd )
                        AND t2.StuEnrollId IN (
                        --SELECT TOP 1
                        --        StuEnrollId
                        --FROM    arStuEnrollments A1 ,
                        --        arPrgVersions A2 ,
                        --        arProgTypes A3
                        --WHERE   A1.PrgVerId = A2.PrgVerId
                        --        AND A2.ProgTypId = A3.ProgTypId
                        --        AND A1.StudentId = t1.StudentId
                        --        AND A3.IPEDSValue = 58
                        --ORDER BY StartDate ,
                        --        EnrollDate ASC
                        SELECT  dbo.GetIPEDS_Spring_GetEnrollmentfromMultipleEnrollment(@CampusId,@ProgId,@EndDate,'undergraduate',t1.studentid) )
              UNION
              SELECT    t1.SSN
                       ,t1.StudentNumber
                       ,t1.LastName + ', ' + t1.FirstName + ' ' + ISNULL(t1.MiddleName,'') AS StudentName
                       ,CASE WHEN ( SUBSTRING(LOWER(@DateRangeText),1,10) = 'enrollment' ) THEN  -- IF ACADEMIC PROGRAM (Rally Case: DE5121)
                                  (
                                    SELECT  COUNT(*)
                                    FROM    arStuEnrollments SQ1
                                           ,adDegCertSeeking SQ2
                                    WHERE   SQ1.StuEnrollId = t2.StuEnrollId
                                            AND SQ1.StartDate <= @EndDate
                                            AND SQ1.degcertseekingid = SQ2.DegCertSeekingId
                                            AND SQ2.IPEDSValue = 11
                                  )
                             ELSE -- If PROGRAM Reporter
                                  (
                                    SELECT  COUNT(*)
                                    FROM    arStuEnrollments SQ1
                                           ,adDegCertSeeking SQ2
                                    WHERE   SQ1.StuEnrollId = t2.StuEnrollId
                                            AND (
                                                  SQ1.StartDate >= @StartDate
                                                  AND SQ1.StartDate <= @EndDate
                                                )
                                            AND SQ1.degcertseekingid = SQ2.DegCertSeekingId
                                            AND SQ2.IPEDSValue = 11
                                  )
                        END AS FirstTime
                       ,
--				(Select Count(*) from arStuEnrollments SQ1
--					where 
--						SQ1.StuEnrollId = t2.StuEnrollId and
--						(SQ1.TransferHours>0) and 
--						SQ1.StartDate<=@EndDate and 
--						not exists (select StuEnrollId from arStuEnrollments where StudentId=SQ1.StudentId and
--						StartDate<SQ1.StartDate)
--				) as TransferIn,
-- If Student is a transfer-in from another school, then there should not be any previous enrollments
	-- and the current enrollment we are dealing with at this point, should be the first enrollment

	-- Check if student has multiple enrollments and also if there any previous enrollments
	-- In addition, check if TransferHours>0, works only for clock hour schools (or)
	-- If student received any grade marked as Transfer Grade in GRade systems page (or)
	-- If IsTransferred column is set to 1 in arResults page
	--(Select count(*) from arStuEnrollments where StudentId=SQ1.StudentId and LeadId is not null)=1

	-- Consider only students who transferred from another institution
	-- So, consider only the first enrollment, lead id will have a value for 1st enrollment
                        (
                          SELECT    COUNT(*)
                          FROM      arStuEnrollments SQ1
                                   ,adDegCertSeeking SQ2
                          WHERE     SQ1.StuEnrollId = t2.StuEnrollId
                                    AND SQ1.LeadId IS NOT NULL
                                    AND SQ1.degcertseekingid = SQ2.DegCertSeekingId
                                    AND SQ2.IPEDSValue = 11
                                    AND (
                                          SQ1.TransferHours > 0
                                          OR
--							SQ1.StuEnrollId in 
--									(Select StuEnrollId from arResults where GrdSysDetailId in (select GrdSysDetailId from arGradeSystemDetails where isTransferGrade=1) and StuEnrollId=SQ1.StuEnrollId
--									 union
--									 Select StuEnrollId from arResults where IsTransfered=1 and StuEnrollId=SQ1.StuEnrollId)
                                          SQ1.StuEnrollId = CASE WHEN @Value = 'letter' THEN (
                                                                                               SELECT TOP 1
                                                                                                        StuEnrollId
                                                                                               FROM     arTransferGrades
                                                                                               WHERE    StuEnrollId = SQ1.StuEnrollId
                                                                                                        AND GrdSysDetailId IN ( SELECT  GrdSysDetailId
                                                                                                                                FROM    arGradeSystemDetails
                                                                                                                                WHERE   isTransferGrade = 1 )
                                                                                             )
                                                                 ELSE (
                                                                        SELECT TOP 1
                                                                                StuEnrollId
                                                                        FROM    arTransferGrades
                                                                        WHERE   IsTransferred = 1
                                                                                AND StuEnrollId = SQ1.StuEnrollId
                                                                      )
                                                            END
                                        )
                                    AND SQ1.StartDate <= @EndDate
                                    AND NOT EXISTS ( SELECT StuEnrollId
                                                     FROM   arStuEnrollments
                                                     WHERE  StudentId = SQ1.StudentId
                                                            AND StartDate < SQ1.StartDate )
                        ) AS TransferIn
                       ,(
                          SELECT    COUNT(*)
                          FROM      arStuEnrollments SQ1
                                   ,adDegCertSeeking SQ2
                          WHERE     SQ1.StuEnrollId = t2.StuEnrollId
                                    AND SQ1.degcertseekingid = SQ2.DegCertSeekingId
                                    AND SQ2.IPEDSValue = 10
                        ) AS NonDegreeSeeking
                       ,CASE WHEN ( SUBSTRING(LOWER(@DateRangeText),1,10) = 'enrollment' ) THEN  -- IF ACADEMIC PROGRAM
                                  (
                                    SELECT  COUNT(*)
                                    FROM    arStuEnrollments SQ1
                                           ,adDegCertSeeking SQ2
                                           ,syStatusCodes SQ3
                                           ,sySysStatus SQ4
                                    WHERE   SQ1.StuEnrollId = t2.StuEnrollId
                                            AND SQ1.degcertseekingid = SQ2.DegCertSeekingId
                                            AND
						-- The StartDate condition modified to < @StartDate due to Rally case DE868
						-- originally it was SQ1.StartDate<=@StartDate
                                            ( SQ2.IPEDSValue = 12 )
                                            AND SQ1.StatusCodeId = SQ3.StatusCodeId
                                            AND SQ3.SysStatusId = SQ4.SysStatusId  -- and SQ4.InSchool=1
                                  )
                             ELSE (
                                    SELECT  COUNT(*)
                                    FROM    arStuEnrollments SQ1
                                           ,adDegCertSeeking SQ2
                                           ,syStatusCodes SQ3
                                           ,sySysStatus SQ4
                                    WHERE   SQ1.StuEnrollId = t2.StuEnrollId
                                            AND SQ1.degcertseekingid = SQ2.DegCertSeekingId
                                            AND
						-- The StartDate condition modified to < @StartDate due to Rally case DE868
						-- originally it was SQ1.StartDate<=@StartDate
                                            (
                                              SQ2.IPEDSValue = 12
                                              OR (
                                                   SQ2.IPEDSValue = 11
                                                   AND SQ1.StartDate < @ContinuingStartDate
                                                 )
                                            )
                                            AND SQ1.StatusCodeId = SQ3.StatusCodeId
                                            AND SQ3.SysStatusId = SQ4.SysStatusId  -- and SQ4.InSchool=1
                                  )
                        END AS Continuing
                       ,t1.StudentId
                       ,CASE WHEN t2.DistanceEdStatus IS NULL THEN 'None'
                             ELSE t2.DistanceEdStatus
                        END AS DistanceEdStatus
                       ,
				--(Select Case When t9.IPEDSValue=58 THEN 1 else 0 end) as UnderGrad,
				--(Select Case When t9.IPEDSValue=59 THEN 1 else 0 end) as Grad,
                        0 AS UnderGrad
                       ,1 AS Grad
                       ,(
                          SELECT    CASE WHEN 
					--(select TOP 1 CountryCode from adCountries 
					--	where Countryid = (select Top 1 Countryid from arStudAddresses
					--					 where Studentid= t1.StudentId) )='usa' Then
                                              (
                                                SELECT TOP 1
                                                        ForeignZip
                                                FROM    arstudaddresses
                                                WHERE   Studentid = t1.StudentId
                                                        AND default1 = 1
                                              ) = 0 THEN ( CASE WHEN (
                                                                       SELECT TOP 1
                                                                                StateCode
                                                                       FROM     Systates
                                                                       WHERE    stateid = (
                                                                                            SELECT TOP 1
                                                                                                    stateid
                                                                                            FROM    arStudAddresses
                                                                                            WHERE   Studentid = t1.StudentId
                                                                                                    AND default1 = 1
                                                                                          )
                                                                     ) = @State THEN 1
                                                                WHEN (
                                                                       (
                                                                         SELECT TOP 1
                                                                                StateCode
                                                                         FROM   Systates
                                                                         WHERE  stateid = (
                                                                                            SELECT TOP 1
                                                                                                    stateid
                                                                                            FROM    arStudAddresses
                                                                                            WHERE   Studentid = t1.StudentId
                                                                                                    AND default1 = 1
                                                                                          )
                                                                       ) <> @State
                                                                       AND (
                                                                             SELECT TOP 1
                                                                                    StateCode
                                                                             FROM   Systates
                                                                             WHERE  stateid = (
                                                                                                SELECT TOP 1
                                                                                                        stateid
                                                                                                FROM    arStudAddresses
                                                                                                WHERE   Studentid = t1.StudentId
                                                                                                        AND default1 = 1
                                                                                              )
                                                                           ) IS NOT NULL
                                                                     ) THEN 2
                                                                ELSE 3
                                                           END )
                                         ELSE 4
                                    END
                        ) AS Location
                       ,t2.Enrollmentid AS EnrollmentId
              FROM      arStudent t1
              INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
              INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
              INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                           AND t6.SysStatusId NOT IN ( 8 )
              INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
              INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
              INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
              LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
              INNER JOIN adCitizenships t11 ON t1.Citizen = t11.CitizenshipId
              LEFT JOIN adDegCertSeeking t12 ON t2.degcertseekingid = t12.degcertseekingid
              WHERE     t2.CampusId = LTRIM(RTRIM(@CampusId))
                        AND 
				--(@ProgId is null or t8.ProgId in (Select Val from [MultipleValuesForReportParameters](@ProgId,',',1))) and
                        (
                          (
                            t8.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
                            AND @ProgId IS NULL
                          )
                          OR t8.ProgId IN ( SELECT  Val
                                            FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                        )
                        AND (
                              t9.IPEDSValue = 59
                              OR t9.IPEDSValue = 60
                              OR t12.IPEDSValue = 11
                            )
                        AND -- graduate or first professional
                        (
                          t10.IPEDSValue = 61
                          OR t10.IPEDSValue = 62
                        )
                        AND -- Full Time or Part Time
				--t12.IPEDSValue is not null and 
                        t9.IPEDSValue IS NOT NULL
                        AND
				
				-- and (t11.IPEDSValue <> 65 or t11.IPEDSValue is NULL) -- Ignore Non US Citizens (Foreign Students)
				--and (@StartDate is null or t2.StartDate>=@StartDate) and (@EndDate is null or t2.StartDate<=@EndDate) 
                        t2.StartDate <= @EndDate
                        AND t2.StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
						( SELECT    t1.StuEnrollId
                          FROM      arStuEnrollments t1
                                   ,SyStatusCodes t2
                          WHERE     t1.StatusCodeId = t2.StatusCodeId
                                    AND StartDate <= @EndDate
                                    AND -- Student started before the end date range
                                    LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                    AND
							-- (@ProgId is null or t8.ProgId in (Select Val from [MultipleValuesForReportParameters](@ProgId,',',1)))   
                                    (
                                      (
                                        t8.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
                                        AND @ProgId IS NULL
                                      )
                                      OR t8.ProgId IN ( SELECT  Val
                                                        FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                    )
                                    AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                    AND (
                                          t1.DateDetermined < @StartDate
                                          OR ExpGradDate < @StartDate
                                          OR LDA < @StartDate
                                        ) )
						
							-- If Student is enrolled in only program version and if that program version 
						-- happens to be a continuing ed program exclude the student
                        AND t2.StudentId NOT IN ( SELECT DISTINCT
                                                            StudentId
                                                  FROM      (
                                                              SELECT    StudentId
                                                                       ,COUNT(*) AS RowCounter
                                                              FROM      arStuENrollments
                                                              WHERE     PrgVerId IN ( SELECT    PrgVerId
                                                                                      FROM      arPrgVersions
                                                                                      WHERE     IsContinuingEd = 1 )
                                                              GROUP BY  StudentId
                                                              HAVING    COUNT(*) = 1
                                                            ) dtStudent_ContinuingEd )
				-- If Student is enrolled in two undergraduate programs or two graduate programs the student should be shown only once
                        AND t2.StuEnrollId IN (
                        --SELECT TOP 1
                        --        StuEnrollId
                        --FROM    arStuEnrollments A1 ,
                        --        arPrgVersions A2 ,
                        --        arProgTypes A3
                        --WHERE   A1.PrgVerId = A2.PrgVerId
                        --        AND A2.ProgTypId = A3.ProgTypId
                        --        AND A1.StudentId = t1.StudentId
                        --        AND ( A3.IPEDSValue = 59
                        --              OR A3.IPEDSValue = 60
                        --            )
                        --ORDER BY StartDate ,
                        --        EnrollDate ASC
                        SELECT  dbo.GetIPEDS_Spring_GetEnrollmentfromMultipleEnrollment(@CampusId,@ProgId,@EndDate,'graduate',t1.studentid) )
              UNION
              SELECT    '' AS SSN
                       ,'' AS StudentNumber
                       ,'' AS StudentName
                       ,'' AS FirstTime
                       ,'' AS TransferIn
                       ,'' AS NonDegreeSeeking
                       ,'' AS Continuing
                       ,NEWID() AS StudentId
                       ,'Only' AS DistanceEdStatus
                       ,0 AS UnderGrad
                       ,0 AS Grad
                       ,0 AS Location
                       ,'' AS EnrollId
              UNION
              SELECT    '' AS SSN
                       ,'' AS StudentNumber
                       ,'' AS StudentName
                       ,'' AS FirstTime
                       ,'' AS TransferIn
                       ,'' AS NonDegreeSeeking
                       ,'' AS Continuing
                       ,NEWID() AS StudentId
                       ,'Some' AS DistanceEdStatus
                       ,0 AS UnderGrad
                       ,0 AS Grad
                       ,0 AS Location
                       ,'' AS EnrollId
              UNION
              SELECT    '' AS SSN
                       ,'' AS StudentNumber
                       ,'' AS StudentName
                       ,'' AS FirstTime
                       ,'' AS TransferIn
                       ,'' AS NonDegreeSeeking
                       ,'' AS Continuing
                       ,NEWID() AS StudentId
                       ,'None' AS DistanceEdStatus
                       ,0 AS UnderGrad
                       ,0 AS Grad
                       ,0 AS Location
                       ,'' AS EnrollId
              UNION
              SELECT    '' AS SSN
                       ,'' AS StudentNumber
                       ,'' AS StudentName
                       ,'' AS FirstTime
                       ,'' AS TransferIn
                       ,'' AS NonDegreeSeeking
                       ,'' AS Continuing
                       ,NEWID() AS StudentId
                       ,'Only' AS DistanceEdStatus
                       ,0 AS UnderGrad
                       ,0 AS Grad
                       ,1 AS Location
                       ,'' AS EnrollId
              UNION
              SELECT    '' AS SSN
                       ,'' AS StudentNumber
                       ,'' AS StudentName
                       ,'' AS FirstTime
                       ,'' AS TransferIn
                       ,'' AS NonDegreeSeeking
                       ,'' AS Continuing
                       ,NEWID() AS StudentId
                       ,'Only' AS DistanceEdStatus
                       ,0 AS UnderGrad
                       ,0 AS Grad
                       ,2 AS Location
                       ,'' AS EnrollId
              UNION
              SELECT    '' AS SSN
                       ,'' AS StudentNumber
                       ,'' AS StudentName
                       ,'' AS FirstTime
                       ,'' AS TransferIn
                       ,'' AS NonDegreeSeeking
                       ,'' AS Continuing
                       ,NEWID() AS StudentId
                       ,'Only' AS DistanceEdStatus
                       ,0 AS UnderGrad
                       ,0 AS Grad
                       ,3 AS Location
                       ,'' AS EnrollId
              UNION
              SELECT    '' AS SSN
                       ,'' AS StudentNumber
                       ,'' AS StudentName
                       ,'' AS FirstTime
                       ,'' AS TransferIn
                       ,'' AS NonDegreeSeeking
                       ,'' AS Continuing
                       ,NEWID() AS StudentId
                       ,'Only' AS DistanceEdStatus
                       ,0 AS UnderGrad
                       ,0 AS Grad
                       ,4 AS Location
                       ,'' AS EnrollId
            ) dt


----select * from #FallPartADistanceEd
--Select * from
--(
--Select *,
--		(select Top 1 EnrollmentId from arStuEnrollments C1,arPrgVersions C2,arProgTypes C3
--		where C1.PrgVerId=C2.PrgVerId and C2.progTypId=C3.ProgTypId and (C3.IPEDSValue=58 or C3.IPEDSValue=59 or c3.IPEDSValue=60) and 
--		C1.StudentId=#FallPartADistanceEd.StudentId order by C1.StartDate,C1.EnrollDate) as EnrollmentId
--from  
--		#FallPartADistanceEd
--) dt
ORDER BY    CASE WHEN @OrderBy = 'SSN' THEN SSN
            END
           ,CASE WHEN @OrderBy = 'LastName' THEN StudentName
            END
           ,CASE WHEN @OrderBy = 'Student Number' THEN StudentNumber
            END
           ,CASE WHEN @OrderBy = 'EnrollmentId' THEN EnrollmentId
            END;
--Drop Table #FallPartADistanceEd



GO
