SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

--=================================================================================================
-- USP_CR_Step01_CreateTableSyCreditSummary
--=================================================================================================
CREATE PROCEDURE [dbo].[USP_CR_Step01_CreateTableSyCreditSummary]
AS -- 
    -- Step 1  --  Create_Table  or Clean Table syCreditSummary
    BEGIN -- Step 1  --  Create_Table
        IF NOT EXISTS (
                      SELECT 1
                      FROM   sys.objects
                      WHERE  object_id = OBJECT_ID(N'syCreditSummary')
                             AND type IN ( N'U', N'PC' )
                      )
            BEGIN
                CREATE TABLE syCreditSummary
                    (
                        StuEnrollId UNIQUEIDENTIFIER NULL
                       ,TermId UNIQUEIDENTIFIER NULL
                       ,TermDescrip VARCHAR(50) NULL
                       ,ReqId UNIQUEIDENTIFIER NULL
                       ,ReqDescrip VARCHAR(50) NULL
                       ,ClsSectionId VARCHAR(50) NULL
                       ,CreditsEarned DECIMAL(18, 2) NULL
                       ,CreditsAttempted DECIMAL(18, 2) NULL
                       ,CurrentScore DECIMAL(18, 2) NULL
                       ,CurrentGrade VARCHAR(50) NULL
                       ,FinalScore DECIMAL(18, 2) NULL
                       ,FinalGrade VARCHAR(50) NULL
                       ,Completed BIT NULL
                       ,FinalGPA DECIMAL(18, 2) NULL
                       ,Product_WeightedAverage_Credits_GPA DECIMAL(18, 2) NULL
                       ,Count_WeightedAverage_Credits DECIMAL(18, 2) NULL
                       ,Product_SimpleAverage_Credits_GPA DECIMAL(18, 2) NULL
                       ,Count_SimpleAverage_Credits DECIMAL(18, 2) NULL
                       ,ModUser VARCHAR(50) NULL
                       ,ModDate DATETIME NULL
                       ,TermGPA_Simple DECIMAL(18, 2) NULL
                       ,TermGPA_Weighted DECIMAL(18, 2) NULL
                       ,coursecredits DECIMAL(18, 2) NULL
                       ,CumulativeGPA DECIMAL(18, 2) NULL
                       ,CumulativeGPA_Simple DECIMAL(18, 2) NULL
                       ,FACreditsEarned DECIMAL(18, 2) NULL
                       ,Average DECIMAL(18, 2) NULL
                       ,CumAverage DECIMAL(18, 2) NULL
                       ,TermStartDate DATETIME NULL
                       ,[Id] [int] NOT NULL IDENTITY(1, 1)
                    );
					  
                ALTER TABLE [dbo].[syCreditSummary] ADD CONSTRAINT [PK_syCreditSummary] PRIMARY KEY NONCLUSTERED  ([Id]) ON [PRIMARY];
            END;

        TRUNCATE TABLE syCreditSummary;
    END;
--=================================================================================================
-- END  --  USP_CR_Step01_CreateTableSyCreditSummary
--=================================================================================================
GO
