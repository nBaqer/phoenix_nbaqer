SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RS_TranscriptIndividualStudentGeneralInfo]
	-- Parameters
    @StudentId UNIQUEIDENTIFIER = '5E4ECE30-4778-4E60-98D9-0D0519266135'
   , --'CBA8BDE5-4C38-4A35-80FC-5E378C583194',
    @EnrollmentId VARCHAR(MAX) = 'C0FA8725-F408-472E-B1D0-0BDC024C876F,C95F5726-946D-4D3A-A57B-A83BE50CA116' --'00000000-0000-0000-0000-000000000000'--'E10A69EA-BF98-4D61-B0C3-D918D0E3EAE9'
AS
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;

        SELECT DISTINCT
                arStuEnrollments.StuEnrollId
               ,arStuEnrollments.PrgVerId
               ,(
                  SELECT    PrgVerDescrip
                  FROM      arPrgVersions
                  WHERE     PrgVerId = arStuEnrollments.PrgVerId
                ) AS PrgVerDescrip
               ,(
                  SELECT    IsContinuingEd
                  FROM      arPrgVersions
                  WHERE     PrgVerId = arStuEnrollments.PrgVerId
                ) AS IsContinuingEd
               ,(
                  SELECT    dg.DegreeDescrip
                  FROM      arPrgVersions pv
                           ,arDegrees dg
                  WHERE     pv.PrgVerId = arStuEnrollments.PrgVerId
                            AND pv.DegreeId = dg.DegreeId
                ) AS DegreeDescrip
               ,(
                  SELECT    ProgDescrip
                  FROM      arPrograms X
                           ,arPrgVersions Y
                  WHERE     Y.PrgVerId = arStuEnrollments.PrgVerId
                            AND Y.ProgId = X.ProgId
                ) AS ProgramDescrip
               ,(
                  SELECT    arPrgGrp.PrgGrpDescrip
                  FROM      arPrgGrp
                  JOIN      arPrgVersions ON arPrgVersions.PrgGrpId = arPrgGrp.PrgGrpId
                  WHERE     arPrgVersions.PrgVerId = arStuEnrollments.PrgVerId
                ) AS PrgGrpDescrip
               ,A.LastName
               ,A.FirstName
               ,A.MiddleName
               ,A.SSN
               ,A.StudentNumber
               ,A.SSN AS StudentIdentifier
               ,arStuEnrollments.StatusCodeId
               ,(
                  SELECT    StatusCodeDescrip
                  FROM      syStatusCodes
                  WHERE     StatusCodeId = arStuEnrollments.StatusCodeId
                ) AS StatusCodeDescrip
               ,(
                  SELECT    SysStatusId
                  FROM      syStatusCodes
                  WHERE     StatusCodeId = arStuEnrollments.StatusCodeId
                ) AS SysStatusId
               ,arStuEnrollments.StartDate
               ,arStuEnrollments.ExpGradDate
               ,arStuEnrollments.EnrollmentId
               ,arStuEnrollments.LDA
               ,arStuEnrollments.DateDetermined
               ,(
                  SELECT    arProgramVersionType.DescriptionProgramVersionType
                  FROM      arProgramVersionType
                  WHERE     arProgramVersionType.ProgramVersionTypeId = arStuEnrollments.PrgVersionTypeId
                ) AS ProgramVersionType
               ,A.DOB
               ,(
                  SELECT TOP 1
                            Address1
                  FROM      arStudAddresses T
                           ,syStatuses Y
                  WHERE     T.StudentId = A.StudentId
                            AND T.StatusId = Y.StatusId
                            AND Y.Status = 'Active'
                  ORDER BY  T.Default1 DESC
                ) AS Address1
               ,(
                  SELECT TOP 1
                            Address2
                  FROM      arStudAddresses T
                           ,syStatuses Y
                  WHERE     T.StudentId = A.StudentId
                            AND T.StatusId = Y.StatusId
                            AND Y.Status = 'Active'
                  ORDER BY  T.Default1 DESC
                ) AS Address2
               ,(
                  SELECT TOP 1
                            City
                  FROM      arStudAddresses T
                           ,syStatuses Y
                  WHERE     T.StudentId = A.StudentId
                            AND T.StatusId = Y.StatusId
                            AND Y.Status = 'Active'
                  ORDER BY  T.Default1 DESC
                ) AS City
               ,(
                  SELECT TOP 1
                            StateDescrip
                  FROM      arStudAddresses T
                           ,syStates S
                           ,syStatuses Y
                  WHERE     T.StudentId = A.StudentId
                            AND T.StateId = S.StateId
                            AND T.StatusId = Y.StatusId
                            AND Y.Status = 'Active'
                  ORDER BY  T.Default1 DESC
                ) AS StateDescrip
               ,(
                  SELECT TOP 1
                            Zip
                  FROM      arStudAddresses T
                           ,syStatuses Y
                  WHERE     T.StudentId = A.StudentId
                            AND T.StatusId = Y.StatusId
                            AND Y.Status = 'Active'
                  ORDER BY  Default1 DESC
                ) AS Zip
--,(SELECT TOP 1 ForeignZip 
--		FROM arStudAddresses T,syStatuses Y 
--		WHERE T.StudentId=A.StudentId AND T.StatusId=Y.StatusId AND Y.Status='Active' ORDER BY T.Default1 DESC) AS ForeignZip
--,(SELECT TOP 1 OtherState 
--		FROM arStudAddresses T,syStatuses Y 
--		WHERE T.StudentId=A.StudentId AND T.StatusId=Y.StatusId AND Y.Status='Active' ORDER BY T.Default1 DESC) AS OtherState
--,(SELECT TOP 1 CountryDescrip 
--		FROM arStudAddresses T,syStatuses Y,adCountries C 
--		WHERE T.StudentId=A.StudentId AND T.StatusId=Y.StatusId AND Y.Status='Active' AND T.CountryId=C.CountryId 
--		ORDER BY T.Default1 DESC) AS CountryDescrip
--,(SELECT TOP 1 Phone 
--		FROM arStudentPhone T,syStatuses Y 
--		WHERE T.StudentId=A.StudentId AND T.StatusId=Y.StatusId AND Y.Status='Active' ORDER BY T.Default1 DESC) AS Phone
--,(SELECT TOP 1 ForeignPhone 
--		FROM arStudentPhone T,syStatuses Y 
--		WHERE T.StudentId=A.StudentId AND T.StatusId=Y.StatusId AND Y.Status='Active' ORDER BY T.Default1 DESC) AS ForeignPhone
               ,(
                  SELECT    MAX(PostDate)
                  FROM      arGrdBkResults
                  WHERE     StuEnrollId = arStuEnrollments.stuEnrollId
                ) AS ExtDate
               ,(
                  SELECT    MAX(LDA)
                  FROM      (
                              SELECT    MAX(AttendedDate) AS LDA
                              FROM      arExternshipAttendance
                              WHERE     StuEnrollId = arStuEnrollments.stuEnrollId
                              UNION ALL
                              SELECT    MAX(MeetDate) AS LDA
                              FROM      atClsSectAttendance
                              WHERE     StuEnrollId = arStuEnrollments.stuEnrollId
                                        AND Actual >= 1
                              UNION ALL
                              SELECT    MAX(AttendanceDate) AS LDA
                              FROM      atAttendance
                              WHERE     EnrollId = arStuEnrollments.stuEnrollId
                                        AND Actual >= 1
                              UNION ALL
                              SELECT    MAX(RecordDate) AS LDA
                              FROM      arStudentClockAttendance
                              WHERE     StuEnrollId = arStuEnrollments.stuEnrollId
                                        AND (
                                              ActualHours >= 1.00
                                              AND ActualHours <> 99.00
                                              AND ActualHours <> 999.00
                                              AND ActualHours <> 9999.00
                                            )
                              UNION ALL
                              SELECT    MAX(MeetDate) AS LDA
                              FROM      atConversionAttendance
                              WHERE     StuEnrollId = arStuEnrollments.stuEnrollId
                                        AND (
                                              Actual >= 1.00
                                              AND Actual <> 99.00
                                              AND Actual <> 999.00
                                              AND Actual <> 9999.00
                                            )
                              UNION ALL
                              SELECT    LDA
                              FROM      arStuEnrollments S
                              WHERE     S.StuEnrollId = arStuEnrollments.stuEnrollId
                            ) TR
                ) AS AttDate
               ,'' AS TermCond
               ,'' AS ClassCond
               ,0 AS ShowDateIssue
               ,0 AS ShowClassDate
               ,0 AS ShowLegalDisc
               ,1 AS ShowTermOrModule
        FROM    arStuEnrollments
               ,arStudent A
        WHERE   arStuEnrollments.StudentId = A.StudentId
                AND (
                      (
                        @EnrollmentId = '00000000-0000-0000-0000-000000000000'
                        AND arStuEnrollments.StudentId = @StudentId
                      )
                      OR (
                           @EnrollmentId <> '00000000-0000-0000-0000-000000000000'
                           AND arStuEnrollments.StuEnrollId IN ( SELECT *
                                                                 FROM   dbo.MultipleValuesForReportParameters(@EnrollmentId,',',0) )
                         )
                    );
    END;




GO
