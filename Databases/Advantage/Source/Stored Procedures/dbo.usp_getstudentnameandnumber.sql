SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
     
       
     
CREATE PROCEDURE [dbo].[usp_getstudentnameandnumber]
    @StuEnrollId VARCHAR(50)
AS
    BEGIN  
        DECLARE @StudentNumber VARCHAR(50);  
        DECLARE @Name VARCHAR(50);  
      
        SELECT  ( S.FirstName + ' ' + ISNULL(S.MiddleName,'') + ' ' + S.LastName ) AS Name
               ,S.StudentNumber
        FROM    arStudent S
        INNER JOIN arStuEnrollments SE ON S.StudentId = SE.StudentId
        WHERE   SE.StuEnrollId = @StuEnrollId;  
      
    END;  
      

GO
