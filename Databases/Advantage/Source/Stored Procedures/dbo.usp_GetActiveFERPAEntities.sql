SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_GetActiveFERPAEntities] @campusId VARCHAR(50)
AS
    SET NOCOUNT ON;        
    SELECT  FC.FERPAEntityID
           ,FC.FERPAEntityCode
           ,FC.FERPAEntityDescrip
           ,FC.StatusId
           ,FC.CampGrpId
           ,ST.StatusId
           ,ST.Status
    FROM    arFERPAEntity FC
           ,syStatuses ST
    WHERE   FC.statusid = ST.StatusId
            AND ST.Status = 'Active'
            AND FC.CampGrpId IN ( SELECT    CampGrpId
                                  FROM      syCmpGrpCmps
                                  WHERE     CampusId = @campusId )
    ORDER BY FC.FERPAEntityDescrip;



GO
