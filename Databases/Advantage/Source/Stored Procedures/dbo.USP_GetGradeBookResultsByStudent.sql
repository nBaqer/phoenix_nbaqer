SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_GetGradeBookResultsByStudent]
    @stuEnrollid UNIQUEIDENTIFIER
AS
    SELECT DISTINCT
            1 AS Tag
           ,NULL AS Parent
           ,(
              SELECT TOP 1
                        PrgVerId
              FROM      arStuEnrollments
              WHERE     StuEnrollId = GBR.StuEnrollId
            ) AS [ProgramVersion!1!PrgVerId]
           ,(
              SELECT    PrgVerDescrip
              FROM      arPrgVersions
              WHERE     PrgVerId = (
                                     SELECT PrgVerId
                                     FROM   arStuEnrollments
                                     WHERE  StuEnrollId = GBR.StuEnrollId
                                   )
            ) AS [ProgramVersion!1!PrgVerDescrip]
           ,(
              SELECT    Credits
              FROM      arPrgVersions
              WHERE     PrgVerId = (
                                     SELECT PrgVerId
                                     FROM   arStuEnrollments
                                     WHERE  StuEnrollId = GBR.StuEnrollId
                                   )
            ) AS [ProgramVersion!1!ProgramCredits]
           ,NULL AS [Module!2!GrpId]
           ,NULL AS [Module!2!GrpDescrip]
           ,NULL AS [Module!2!StartDate]
           ,NULL AS [Module!2!EndDate]
           ,NULL AS [Course!3!ReqId]
           ,NULL AS [Course!3!CourseDescrip]
           ,NULL AS [Course!3!CourseCodeDescrip]
           ,NULL AS [Course!3!Credits]
           ,NULL AS [Course!3!FinAidCredits]
           ,NULL AS [Course!3!PassingGrade]
           ,NULL AS [Course!3!Score]
           ,NULL AS [Course!3!Grade]
           ,NULL AS [GradeBook!4!GrdBkResultId]
           ,NULL AS [GradeBook!4!Comments]
           ,NULL AS [GradeBook!4!Score]
           ,NULL AS [GradeBook!4!PostDate]
           ,NULL AS [GradeBook!4!PassingGrade]
           ,NULL AS [GradeBook!4!Weight]
           ,NULL AS [GradeBook!4!Required]
           ,NULL AS [GradeBook!4!MustPass]
           ,NULL AS [GradeBook!4!SysComponentTypeId]
           ,NULL AS [GradeBook!4!HoursRequired]
           ,NULL AS [GradeBook!4!HoursCompleted]
    FROM    arGrdBkResults GBR
    WHERE   GBR.StuEnrollId = @stuEnrollid
    UNION
    SELECT DISTINCT
            1
           ,NULL
           ,(
              SELECT TOP 1
                        PrgVerId
              FROM      arStuEnrollments
              WHERE     StuEnrollId = GBCR.StuEnrollId
            ) AS PrgVerId
           ,(
              SELECT    PrgVerDescrip
              FROM      arPrgVersions
              WHERE     PrgVerId = (
                                     SELECT PrgVerId
                                     FROM   arStuEnrollments
                                     WHERE  StuEnrollId = GBCR.StuEnrollId
                                   )
            ) AS PrgVerDescrip
           ,(
              SELECT    Credits
              FROM      arPrgVersions
              WHERE     PrgVerId = (
                                     SELECT PrgVerId
                                     FROM   arStuEnrollments
                                     WHERE  StuEnrollId = GBCR.StuEnrollId
                                   )
            ) AS ProgramCredits
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
    FROM    arGrdBkConversionResults GBCR
    WHERE   GBCR.StuEnrollId = @stuEnrollid
    UNION
    SELECT DISTINCT
            1 AS Tag
           ,NULL AS Parent
           ,(
              SELECT TOP 1
                        PrgVerId
              FROM      arStuEnrollments
              WHERE     StuEnrollId = GBR.StuEnrollId
            ) AS [ProgramVersion!1!PrgVerId]
           ,(
              SELECT    PrgVerDescrip
              FROM      arPrgVersions
              WHERE     PrgVerId = (
                                     SELECT PrgVerId
                                     FROM   arStuEnrollments
                                     WHERE  StuEnrollId = GBR.StuEnrollId
                                   )
            ) AS [ProgramVersion!1!PrgVerDescrip]
           ,(
              SELECT    Credits
              FROM      arPrgVersions
              WHERE     PrgVerId = (
                                     SELECT PrgVerId
                                     FROM   arStuEnrollments
                                     WHERE  StuEnrollId = GBR.StuEnrollId
                                   )
            ) AS [ProgramVersion!1!ProgramCredits]
           ,NULL AS [Module!2!GrpId]
           ,NULL AS [Module!2!GrpDescrip]
           ,NULL AS [Module!2!StartDate]
           ,NULL AS [Module!2!EndDate]
           ,NULL AS [Course!3!ReqId]
           ,NULL AS [Course!3!CourseDescrip]
           ,NULL AS [Course!3!CourseCodeDescrip]
           ,NULL AS [Course!3!Credits]
           ,NULL AS [Course!3!FinAidCredits]
           ,NULL AS [Course!3!PassingGrade]
           ,NULL AS [Course!3!Score]
           ,NULL AS [Course!3!Grade]
           ,NULL AS [GradeBook!4!GrdBkResultId]
           ,NULL AS [GradeBook!4!Comments]
           ,NULL AS [GradeBook!4!Score]
           ,NULL AS [GradeBook!4!PostDate]
           ,NULL AS [GradeBook!4!PassingGrade]
           ,NULL AS [GradeBook!4!Weight]
           ,NULL AS [GradeBook!4!Required]
           ,NULL AS [GradeBook!4!MustPass]
           ,NULL AS [GradeBook!4!SysComponentTypeId]
           ,NULL AS [GradeBook!4!HoursRequired]
           ,NULL AS [GradeBook!4!HoursCompleted]
    FROM    arResults GBR
    WHERE   GBR.StuEnrollId = @stuEnrollid
    UNION
    SELECT DISTINCT
            1 AS Tag
           ,NULL AS Parent
           ,(
              SELECT TOP 1
                        PrgVerId
              FROM      arStuEnrollments
              WHERE     StuEnrollId = GBR.StuEnrollId
            ) AS [ProgramVersion!1!PrgVerId]
           ,(
              SELECT    PrgVerDescrip
              FROM      arPrgVersions
              WHERE     PrgVerId = (
                                     SELECT PrgVerId
                                     FROM   arStuEnrollments
                                     WHERE  StuEnrollId = GBR.StuEnrollId
                                   )
            ) AS [ProgramVersion!1!PrgVerDescrip]
           ,(
              SELECT    Credits
              FROM      arPrgVersions
              WHERE     PrgVerId = (
                                     SELECT PrgVerId
                                     FROM   arStuEnrollments
                                     WHERE  StuEnrollId = GBR.StuEnrollId
                                   )
            ) AS [ProgramVersion!1!ProgramCredits]
           ,NULL AS [Module!2!GrpId]
           ,NULL AS [Module!2!GrpDescrip]
           ,NULL AS [Module!2!StartDate]
           ,NULL AS [Module!2!EndDate]
           ,NULL AS [Course!3!ReqId]
           ,NULL AS [Course!3!CourseDescrip]
           ,NULL AS [Course!3!CourseCodeDescrip]
           ,NULL AS [Course!3!Credits]
           ,NULL AS [Course!3!FinAidCredits]
           ,NULL AS [Course!3!PassingGrade]
           ,NULL AS [Course!3!Score]
           ,NULL AS [Course!3!Grade]
           ,NULL AS [GradeBook!4!GrdBkResultId]
           ,NULL AS [GradeBook!4!Comments]
           ,NULL AS [GradeBook!4!Score]
           ,NULL AS [GradeBook!4!PostDate]
           ,NULL AS [GradeBook!4!PassingGrade]
           ,NULL AS [GradeBook!4!Weight]
           ,NULL AS [GradeBook!4!Required]
           ,NULL AS [GradeBook!4!MustPass]
           ,NULL AS [GradeBook!4!SysComponentTypeId]
           ,NULL AS [GradeBook!4!HoursRequired]
           ,NULL AS [GradeBook!4!HoursCompleted]
    FROM    arTransferGrades GBR
    WHERE   GBR.StuEnrollId = @stuEnrollid
    UNION ALL
    SELECT DISTINCT
            2
           ,1
           ,NULL
           ,(
              SELECT    PrgVerDescrip
              FROM      arPrgVersions
              WHERE     PrgVerId = (
                                     SELECT PrgVerId
                                     FROM   arStuEnrollments
                                     WHERE  StuEnrollId = GBR.StuEnrollId
                                   )
            )
           ,NULL
           ,CS.termId
           ,(
              SELECT    TermDescrip
              FROM      arTerm
              WHERE     TermId = CS.TermId
            )
           ,(
              SELECT    StartDate
              FROM      arTerm
              WHERE     TermId = CS.TermId
            ) AS StartDate
           ,(
              SELECT    EndDate
              FROM      arTerm
              WHERE     TermId = CS.TermId
            ) AS EndDate
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
    FROM    arClassSections CS
           ,arGrdBkResults GBR
    WHERE   CS.ClsSectionId = GBR.ClsSectionId
            AND GBR.StuEnrollId = @stuEnrollid
    UNION
    SELECT DISTINCT
            2
           ,1
           ,NULL
           ,(
              SELECT    PrgVerDescrip
              FROM      arPrgVersions
              WHERE     PrgVerId = (
                                     SELECT PrgVerId
                                     FROM   arStuEnrollments
                                     WHERE  StuEnrollId = GBCR.STuEnrollId
                                   )
            ) AS PrgVerDescrip
           ,NULL
           ,GBCR.TermId
           ,(
              SELECT    TermDescrip
              FROM      arTerm
              WHERE     TermId = GBCR.TermId
            )
           ,(
              SELECT    StartDate
              FROM      arTerm
              WHERE     TermId = GBCR.TermId
            ) AS StartDate
           ,(
              SELECT    EndDate
              FROM      arTerm
              WHERE     TermId = GBCR.TermId
            ) AS EndDate
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
    FROM    arGrdBkConversionResults GBCR
    WHERE   GBCR.StuEnrollId = @stuEnrollid
    UNION
    SELECT DISTINCT
            2
           ,1
           ,NULL
           ,(
              SELECT    PrgVerDescrip
              FROM      arPrgVersions
              WHERE     PrgVerId = (
                                     SELECT PrgVerId
                                     FROM   arStuEnrollments
                                     WHERE  StuEnrollId = GBR.StuEnrollId
                                   )
            )
           ,NULL
           ,CS.termId
           ,CS.TermDescrip
           ,CS.StartDate
           ,CS.EndDate
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
    FROM    arTerm CS
           ,arTransferGrades GBR
    WHERE   CS.Termid = GBR.Termid
            AND GBR.StuEnrollId = @stuEnrollid
    UNION ALL
    SELECT DISTINCT
            3
           ,2
           ,NULL
           ,(
              SELECT    PrgVerDescrip
              FROM      arPrgVersions
              WHERE     PrgVerId = (
                                     SELECT PrgVerId
                                     FROM   arStuEnrollments
                                     WHERE  StuEnrollId = GBR.StuEnrollId
                                   )
            )
           ,NULL
           ,NULL
           ,(
              SELECT    TermDescrip
              FROM      arTerm
              WHERE     TermId = CS.TermId
            )
           ,(
              SELECT    StartDate
              FROM      arTerm
              WHERE     TermId = CS.TermId
            ) AS StartDate
           ,NULL
           ,CS.ReqId AS ReqId
           ,(
              SELECT    Descrip
              FROM      arReqs
              WHERE     ReqId = CS.ReqId
            ) AS CourseDescrip
           ,'(' + (
                    SELECT  Code
                    FROM    arReqs
                    WHERE   ReqId = CS.ReqId
                  ) + ' ) ' + (
                                SELECT  Descrip
                                FROM    arReqs
                                WHERE   ReqId = CS.ReqId
                              ) AS CourseCodeDescrip
           ,(
              SELECT    Credits
              FROM      arReqs
              WHERE     ReqId = CS.ReqId
            ) AS Credits
           ,(
              SELECT    FinAidCredits
              FROM      arReqs
              WHERE     ReqId = CS.ReqId
            ) AS FinAidCredits
           ,(
              SELECT    MIN(MinVal)
              FROM      arGradeScaleDetails GCD
                       ,arGradeSystemDetails GSD
              WHERE     GCD.GrdSysDetailId = GSD.GrdSysDetailId
                        AND GSD.IsPass = 1
                        AND GCD.GrdScaleId = CS.GrdScaleId
            )
           ,(
              SELECT    score
              FROM      arResults
              WHERE     stuEnrollid = GBR.StuEnrollId
                        AND Testid = CS.ClsSEctionid
            )
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
    FROM    arClassSections CS
           ,arGrdBkResults GBR
    WHERE   CS.ClsSectionId = GBR.ClsSectionId
            AND GBR.StuEnrollId = @stuEnrollid
    UNION ALL
    SELECT DISTINCT
            3
           ,2
           ,NULL
           ,(
              SELECT    PrgVerDescrip
              FROM      arPrgVersions
              WHERE     PrgVerId = (
                                     SELECT PrgVerId
                                     FROM   arStuEnrollments
                                     WHERE  StuEnrollId = TR.StuEnrollId
                                   )
            )
           ,NULL
           ,NULL
           ,(
              SELECT    TermDescrip
              FROM      arTerm
              WHERE     TermId = TR.TermId
            )
           ,(
              SELECT    StartDate
              FROM      arTerm
              WHERE     TermId = TR.TermId
            ) AS StartDate
           ,NULL
           ,TR.ReqId AS ReqId
           ,(
              SELECT    Descrip
              FROM      arReqs
              WHERE     ReqId = TR.ReqId
            ) AS CourseDescrip
           ,'(' + (
                    SELECT  Code
                    FROM    arReqs
                    WHERE   ReqId = TR.ReqId
                  ) + ' ) ' + (
                                SELECT  Descrip
                                FROM    arReqs
                                WHERE   ReqId = TR.ReqId
                              ) AS CourseCodeDescrip
           ,(
              SELECT    Credits
              FROM      arReqs
              WHERE     ReqId = TR.ReqId
            ) AS Credits
           ,(
              SELECT    FinAidCredits
              FROM      arReqs
              WHERE     ReqId = TR.ReqId
            ) AS FinAidCredits
           ,CASE WHEN TR.Score IS NULL THEN (
                                              SELECT    ispass
                                              FROM      arGradeSystemDetails
                                              WHERE     GrdSysDetailId = TR.GrdSysDetailId
                                            )
                 ELSE (
                        SELECT  MIN(MinVal)
                        FROM    arGradeScaleDetails GCD
                               ,arGradeSystemDetails GSD
                        WHERE   GCD.GrdSysDetailId = GSD.GrdSysDetailId
                                AND GSD.IsPass = 1
                      )
            END
           ,TR.Score
           ,(
              SELECT    Grade
              FROM      arGradeSystemDetails
              WHERE     GrdSysDetailId = TR.GrdSysDetailId
            )
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
    FROM    arTransferGrades TR
    WHERE   TR.StuEnrollId = @stuEnrollid
    UNION ALL
    SELECT DISTINCT
            3
           ,2
           ,NULL
           ,(
              SELECT    PrgVerDescrip
              FROM      arPrgVersions
              WHERE     PrgVerId = (
                                     SELECT PrgVerId
                                     FROM   arStuEnrollments
                                     WHERE  StuEnrollId = GBCR.StuEnrollId
                                   )
            ) AS PrgVerDescrip
           ,NULL
           ,NULL
           ,(
              SELECT    TermDescrip
              FROM      arTerm
              WHERE     TermId = GBCR.TermId
            )
           ,(
              SELECT    StartDate
              FROM      arTerm
              WHERE     TermId = GBCR.TermId
            ) AS StartDate
           ,NULL
           ,GBCR.ReqId
           ,(
              SELECT    Descrip
              FROM      arReqs
              WHERE     ReqId = GBCR.ReqId
            ) AS CourseDescrip
           ,'(' + (
                    SELECT  Code
                    FROM    arReqs
                    WHERE   ReqId = GBCR.ReqId
                  ) + ' ) ' + (
                                SELECT  Descrip
                                FROM    arReqs
                                WHERE   ReqId = GBCR.ReqId
                              ) AS CourseCodeDescrip
           ,(
              SELECT    Credits
              FROM      arReqs
              WHERE     ReqId = GBCR.ReqId
            ) AS Credits
           ,(
              SELECT    FinAidCredits
              FROM      arReqs
              WHERE     ReqId = GBCR.ReqId
            ) AS FinAidCredits
           ,(
              SELECT    MIN(MinVal)
              FROM      arGradeScaleDetails GCD
                       ,arGradeSystemDetails GSD
              WHERE     GCD.GrdSysDetailId = GSD.GrdSysDetailId
                        AND GSD.IsPass = 1
            )
           ,(
              SELECT    score
              FROM      arResults R
                       ,arClassSections C
              WHERE     R.Testid = C.ClsSectionid
                        AND R.stuEnrollid = GBCR.StuEnrollId
                        AND C.Reqid = GBCR.Reqid
                        AND C.TermId = GBCR.Termid
            )
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
    FROM    arGrdBkConversionResults GBCR
    WHERE   GBCR.StuEnrollId = @stuEnrollid
    UNION ALL
    SELECT  4
           ,3
           ,NULL
           ,(
              SELECT    PrgVerDescrip
              FROM      arPrgVersions
              WHERE     PrgVerId = (
                                     SELECT PrgVerId
                                     FROM   arStuEnrollments
                                     WHERE  StuEnrollId = GBR.StuEnrollId
                                   )
            )
           ,NULL
           ,NULL
           ,(
              SELECT    TermDescrip
              FROM      arTerm
              WHERE     TermId = CS.TermId
            )
           ,(
              SELECT    StartDate
              FROM      arTerm
              WHERE     TermId = CS.TermId
            ) AS StartDate
           ,NULL
           ,NULL
           ,(
              SELECT    Descrip
              FROM      arReqs
              WHERE     ReqId = CS.ReqId
            ) AS CourseDescrip
           ,'(' + (
                    SELECT  Code
                    FROM    arReqs
                    WHERE   ReqId = CS.ReqId
                  ) + ' ) ' + (
                                SELECT  Descrip
                                FROM    arReqs
                                WHERE   ReqId = CS.ReqId
                              ) AS CourseCodeDescrip
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,GBR.GrdBkResultId
           ,(
              SELECT    RTRIM(Descrip)
              FROM      arGrdComponentTypes
              WHERE     GrdComponentTypeId = (
                                               SELECT   GrdComponentTypeId
                                               FROM     arGrdBkWgtDetails
                                               WHERE    InstrGrdBkwgtDetailId = GBR.InstrGrdBkwgtDetailId
                                             )
            ) AS comments
           ,GBR.Score
           ,( CASE (
                     SELECT TOP 1
                            SysComponentTypeId
                     FROM   arGrdComponentTypes
                     WHERE  GrdComponentTypeId = (
                                                   SELECT   GrdComponentTypeId
                                                   FROM     arGrdBkWgtDetails
                                                   WHERE    InstrGrdBkWgtDetailId = GBR.InstrGrdBkWgtDetailId
                                                 )
                   )
                WHEN 544 THEN (
                                SELECT  MAX(AttendedDate)
                                FROM    arExternshipAttendance
                                WHERE   StuEnrollId = GBR.StuEnrollId
                              )
                ELSE GBR.PostDate
              END ) AS PostDate
           ,(
              SELECT    MIN(MinVal)
              FROM      arGradeScaleDetails GCD
                       ,arGradeSystemDetails GSD
              WHERE     GCD.GrdSysDetailId = GSD.GrdSysDetailId
                        AND GSD.IsPass = 1
            ) AS PassingGrade
           ,(
              SELECT    Weight
              FROM      arGrdBkWgtDetails
              WHERE     InstrGrdBkWgtDetailId = GBR.InstrGrdBkWgtDetailId
            ) AS Weight
           ,(
              SELECT    Required
              FROM      arGrdBkWgtDetails
              WHERE     InstrGrdBkWgtDetailId = GBR.InstrGrdBkWgtDetailId
            ) AS Required
           ,(
              SELECT    MustPass
              FROM      arGrdBkWgtDetails
              WHERE     InstrGrdBkWgtDetailId = GBR.InstrGrdBkWgtDetailId
            ) AS MustPass
           ,(
              SELECT    SysComponentTypeId
              FROM      arGrdComponentTypes
              WHERE     GrdComponentTypeId = (
                                               SELECT   GrdComponentTypeId
                                               FROM     arGrdBkWgtDetails
                                               WHERE    InstrGrdBkWgtDetailId = GBR.InstrGrdBkWgtDetailId
                                             )
            ) AS SysComponentTypeId
           ,(
              SELECT    Number
              FROM      arGrdBkWgtDetails
              WHERE     InstrGrdBkWgtDetailId = GBR.InstrGrdBkWgtDetailId
            ) AS HoursRequired
           ,( CASE (
                     SELECT TOP 1
                            SysComponentTypeId
                     FROM   arGrdComponentTypes
                     WHERE  GrdComponentTypeId = (
                                                   SELECT   GrdComponentTypeId
                                                   FROM     arGrdBkWgtDetails
                                                   WHERE    InstrGrdBkWgtDetailId = GBR.InstrGrdBkWgtDetailId
                                                 )
                   )
                WHEN 544 THEN (
                                SELECT  SUM(HoursAttended)
                                FROM    arExternshipAttendance
                                WHERE   StuEnrollId = GBR.StuEnrollId
                              )
                ELSE COALESCE(GBR.Score,0.00)
              END ) AS HoursCompleted
    FROM    arClassSections CS
           ,arGrdBkResults GBR
    WHERE   CS.ClsSectionId = GBR.ClsSectionId
            AND GBR.StuEnrollId = @stuEnrollid
    UNION ALL
    SELECT  4
           ,3
           ,NULL
           ,(
              SELECT    PrgVerDescrip
              FROM      arPrgVersions
              WHERE     PrgVerId = (
                                     SELECT PrgVerId
                                     FROM   arStuEnrollments
                                     WHERE  StuEnrollId = GBCR.StuEnrollId
                                   )
            ) AS PrgVerDescrip
           ,NULL
           ,NULL
           ,(
              SELECT    TermDescrip
              FROM      arTerm
              WHERE     TermId = GBCR.TermId
            )
           ,(
              SELECT    StartDate
              FROM      arTerm
              WHERE     TermId = GBCR.TermId
            ) AS StartDate
           ,NULL
           ,NULL
           ,(
              SELECT    Descrip
              FROM      arReqs
              WHERE     ReqId = GBCR.ReqId
            ) AS CourseDescrip
           ,'(' + (
                    SELECT  Code
                    FROM    arReqs
                    WHERE   ReqId = GBCR.ReqId
                  ) + ' ) ' + (
                                SELECT  Descrip
                                FROM    arReqs
                                WHERE   ReqId = GBCR.ReqId
                              ) AS CourseCodeDescrip
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,NULL
           ,GBCR.ConversionResultId AS GrdBkResultId
           ,(
              SELECT    RTRIM(Descrip)
              FROM      arGrdComponentTypes
              WHERE     GrdComponentTypeId = GBCR.GrdComponentTypeId
            ) AS Comments
           ,GBCR.Score
           ,GBCR.PostDate
           ,GBCR.MinResult AS PassingGrade
           ,(
              SELECT    Weight
              FROM      arGrdBkWgtDetails GBWD
                       ,arGrdBkWeights GBW
              WHERE     GBWD.InstrGrdBkWgtId = GBW.InstrGrdBkWgtId
                        AND GBWD.GrdComponentTypeId = GBCR.GrdComponentTypeId
                        AND GBCR.ReqId = GBW.ReqId
                        AND GBW.EffectiveDate = (
                                                  SELECT    MAX(EffectiveDate)
                                                  FROM      arGrdBkWeights
                                                  WHERE     ReqId = GBCR.ReqId
                                                            AND EffectiveDate <= (
                                                                                   SELECT   StartDate
                                                                                   FROM     arTerm
                                                                                   WHERE    TermId = GBCR.TermId
                                                                                 )
                                                )
            ) AS Weight
           ,(
              SELECT    Required
              FROM      arGrdBkWgtDetails GBWD
                       ,arGrdBkWeights GBW
              WHERE     GBWD.InstrGrdBkWgtId = GBW.InstrGrdBkWgtId
                        AND GBWD.GrdComponentTypeId = GBCR.GrdComponentTypeId
                        AND GBCR.ReqId = GBW.ReqId
                        AND GBW.EffectiveDate = (
                                                  SELECT    MAX(EffectiveDate)
                                                  FROM      arGrdBkWeights
                                                  WHERE     ReqId = GBCR.ReqId
                                                            AND EffectiveDate <= (
                                                                                   SELECT   StartDate
                                                                                   FROM     arTerm
                                                                                   WHERE    TermId = GBCR.TermId
                                                                                 )
                                                )
            ) AS Required
           ,(
              SELECT    MustPass
              FROM      arGrdBkWgtDetails GBWD
                       ,arGrdBkWeights GBW
              WHERE     GBWD.InstrGrdBkWgtId = GBW.InstrGrdBkWgtId
                        AND GBWD.GrdComponentTypeId = GBCR.GrdComponentTypeId
                        AND GBCR.ReqId = GBW.ReqId
                        AND GBW.EffectiveDate = (
                                                  SELECT    MAX(EffectiveDate)
                                                  FROM      arGrdBkWeights
                                                  WHERE     ReqId = GBCR.ReqId
                                                            AND EffectiveDate <= (
                                                                                   SELECT   StartDate
                                                                                   FROM     arTerm
                                                                                   WHERE    TermId = GBCR.TermId
                                                                                 )
                                                )
            ) AS MustPass
           ,(
              SELECT    SysComponentTypeId
              FROM      arGrdComponentTypes
              WHERE     GrdComponentTypeId = GBCR.GrdComponentTypeId
            ) AS SysComponentTypeId
           ,(
              SELECT    Number
              FROM      arGrdBkWgtDetails GBWD
                       ,arGrdBkWeights GBW
              WHERE     GBWD.InstrGrdBkWgtId = GBW.InstrGrdBkWgtId
                        AND GBWD.GrdComponentTypeId = GBCR.GrdComponentTypeId
                        AND GBCR.ReqId = GBW.ReqId
                        AND GBW.EffectiveDate = (
                                                  SELECT    MAX(EffectiveDate)
                                                  FROM      arGrdBkWeights
                                                  WHERE     ReqId = GBCR.ReqId
                                                            AND EffectiveDate <= (
                                                                                   SELECT   StartDate
                                                                                   FROM     arTerm
                                                                                   WHERE    TermId = GBCR.TermId
                                                                                 )
                                                )
            ) AS HoursRequired
           ,COALESCE(GBCR.Score,0) AS HoursCompleted
    FROM    arGrdBkConversionResults GBCR
    WHERE   GBCR.StuEnrollId = @stuEnrollid
            AND GBCR.TermId = GBCR.TermId
            AND GBCR.ReqId = GBCR.ReqId
            AND GBCR.StuEnrollId = GBCR.StuEnrollId
    ORDER BY [ProgramVersion!1!PrgVerDescrip]
           ,[Module!2!StartDate]
           ,[Module!2!GrpDescrip]
           ,[Course!3!CourseDescrip]
           ,[Course!3!CourseCodeDescrip]
           ,[GradeBook!4!Comments]
    FOR     XML EXPLICIT;



GO
