SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

--=================================================================================================
-- USP_MedianLoanDebt_Details
--=================================================================================================
CREATE PROCEDURE [dbo].[USP_MedianLoanDebt_Details]
    (
     @StartDate AS DATETIME
    ,@EndDate AS DATETIME
    ,@RevGradDateType AS VARCHAR(50)
    ,@CampusId AS VARCHAR(50) = NULL
    ,@ProgramIdList AS VARCHAR(MAX) = NULL
    )
AS
    BEGIN
        IF ( @RevGradDateType = 'fromprogram' )
            BEGIN
                EXEC dbo.USP_MedianLoanDebt_ProgramWeeks @StartDate,@EndDate,@CampusId,@ProgramIdList;
            END;
        ELSE
            BEGIN
                EXEC dbo.USP_MedianLoanDebt_RevisedGradDate @StartDate,@EndDate,@CampusId,@ProgramIdList;
            END;
    
    END;
--=================================================================================================
-- END  --  USP_MedianLoanDebt_Details
--=================================================================================================    
GO
