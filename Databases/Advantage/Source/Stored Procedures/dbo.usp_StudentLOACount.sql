SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_StudentLOACount]
    (
     @stuEnrollId UNIQUEIDENTIFIER
          
    )
AS
    SET NOCOUNT ON;
    SELECT  COUNT(*)
    FROM    dbo.arStudentLOAs
    WHERE   StartDate > GETDATE()
            AND StuEnrollId = @stuEnrollId;





GO
