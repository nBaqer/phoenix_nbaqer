SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_WinterIPEDS_FallPartC4_FTPTUG_GetList_Summary]
    @CampusId VARCHAR(50)
   ,@ProgId VARCHAR(4000) = NULL
   ,@StartDate DATETIME = NULL
   ,@EndDate DATETIME = NULL
   ,@DateRangeText VARCHAR(100) = NULL
   ,@OrderBy VARCHAR(100)
AS
    DECLARE @ReturnValue VARCHAR(100);
    DECLARE @ContinuingStartDate DATETIME;

   -- DECLARE @HSGradDate_Program DATETIME ,
   --     @HSGradDate_Academic DATETIME 
    DECLARE @Value VARCHAR(50);

--2/07/2012 - Academic year options should have a cutoff start date For First Time 
    DECLARE @AcadInstFirstTimeStartDate DATETIME;

    --SET @HSGradDate_Program = DATEADD(YY, -1, @StartDate)
    --SET @HSGradDate_Academic = DATEADD(YY, -1, @EndDate)

--PRINT @HSGradDate_Program

--set @Value='letter'
-- Check if School tracks grades by letter or numeric
    SET @Value = (
                   SELECT   dbo.GetAppSettingValue(47,@CampusId)
                 );
--SET @Value = (SELECT TOP 1 Value FROM dbo.syConfigAppSetValues WHERE SettingId=47)
    SET @ContinuingStartDate = @StartDate;
-- For Academic Year Reporter, the End Date should be 10/15/Cohort Year
-- and Start Date should be blank
    IF SUBSTRING(LOWER(@DateRangeText),1,10) = 'enrollment'
        BEGIN
            SET @EndDate = @StartDate; -- it will always be 10/15/cohort year example: for cohort year 2009, it is 10/15/2009
            SET @StartDate = @StartDate; -- it will always be 10/15/cohort year example: for cohort year 2009, it is 10/15/2009
            SET @AcadInstFirstTimeStartDate = DATEADD(YEAR,-1,@EndDate);
        END;

    IF @ProgId IS NOT NULL
        BEGIN
            SELECT  @ReturnValue = COALESCE(@ReturnValue,'') + ProgDescrip + ','
            FROM    arPrograms t1
            WHERE   t1.ProgId IN ( SELECT   Val
                                   FROM     MultipleValuesForReportParameters(@ProgId,',',1) );
            SET @ReturnValue = SUBSTRING(@ReturnValue,1,LEN(@ReturnValue) - 1);
        END;
    ELSE
        BEGIN
            SELECT  @ReturnValue = COALESCE(@ReturnValue,'') + ProgDescrip + ','
            FROM    arPrograms t1
            WHERE   t1.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
                    AND t1.CampGrpId IN ( SELECT    CampGrpId
                                          FROM      syCmpGrpCmps
                                          WHERE     CampusId = @CampusId );
            SET @ReturnValue = SUBSTRING(@ReturnValue,1,LEN(@ReturnValue) - 1);
        END;

    CREATE TABLE #FallPartB4FTUG
        (
         RowNumber UNIQUEIDENTIFIER
        ,SSN VARCHAR(50)
        ,StudentNumber VARCHAR(50)
        ,StudentName VARCHAR(100)
        ,StateDescrip VARCHAR(100)
        ,fipscode INT
        ,FirstTime VARCHAR(10)
        ,FirstTimeCount INT
        ,GraduatedOrGED VARCHAR(10)
        ,GraduatedOrGEDCount INT
        ,ProgDescrip VARCHAR(100)
        ,StudentId UNIQUEIDENTIFIER
        ); 

    INSERT  INTO #FallPartB4FTUG
            SELECT  NEWID() AS RowNumber
                   ,dbo.UDF_FormatSSN(SSN) AS SSN
                   ,StudentNumber
                   ,StudentName
                   ,StateDescrip
                   ,fipscode
                   ,CASE WHEN FirstTime >= 1 THEN 'X'
                         ELSE ''
                    END AS FirstTime
                   ,CASE WHEN FirstTime >= 1 THEN 1
                         ELSE 0
                    END AS FirstTimeCount
                   ,CASE WHEN GraduatedOrGED >= 1 THEN 'X'
                         ELSE ''
                    END AS GraduatedOrGED
                   ,CASE WHEN GraduatedOrGED >= 1 THEN 1
                         ELSE 0
                    END AS GraduatedOrGEDCount
                   ,@ReturnValue AS ProgDescrip
                   ,StudentId
            FROM    (
                      SELECT TOP 1
                                NULL AS SSN
                               ,NULL AS StudentNumber
                               ,NULL AS StudentName
                               ,'State unknown' AS StateDescrip
                               ,57 AS fipscode
                               ,NULL AS FirstTime
                               ,NULL AS GraduatedOrGED
                               ,NULL AS StudentId
                      FROM      arStudent
                      WHERE     (
                                  SELECT    COUNT(t1.FirstName)
                                  FROM      arStudent t1
                                  LEFT OUTER JOIN arStudAddresses t4 ON t4.StudentId = t1.StudentId
				--LEFT OUTER JOIN syStates t12 on t12.StateId=t4.StateId  
                                  INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                                  INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                                  INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                                               AND t6.SysStatusId NOT IN ( 8 )
                                  INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                                  INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                                  INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                                  LEFT JOIN arAttendTypes t10 ON t2.AttendTypeId = t10.AttendTypeId 
				--INNER JOIN adCitizenships t11 on t1.Citizen  = t11.CitizenshipId 
                                  INNER JOIN adDegCertSeeking t12 ON t2.DegCertSeekingId = t12.DegCertSeekingId
                                  WHERE     LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                                            AND (
                                                  @ProgId IS NULL
                                                  OR t8.ProgId IN ( SELECT  Val
                                                                    FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                                )
                                            AND t9.IPEDSValue = 58
                                            AND -- undergraduate
                                            t10.IPEDSValue IN ( 61,62 )  -- Full Time And Part Time
                                            AND t4.default1 = 1
                                            AND t4.StateId IS NULL
                                            AND t4.ForeignZip = 0
                                            AND StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
				-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
				( SELECT    t1.StuEnrollId
                  FROM      arStuEnrollments t1
                           ,syStatusCodes t2
                  WHERE     t1.StatusCodeId = t2.StatusCodeId
                            AND StartDate <= @EndDate
                            AND -- Student started before the end date range
                            LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                            AND (
                                  @ProgId IS NULL
                                  OR t8.ProgId IN ( SELECT  Val
                                                    FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                )
                            AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
					-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                            AND (
                                  t1.DateDetermined < @StartDate
                                  OR ExpGradDate < @StartDate
                                  OR LDA < @StartDate
                                ) )
                                                        	-- If Student is enrolled in only program version and if that program version 
						-- happens to be a continuing ed program exclude the student
                                            AND t2.StudentId NOT IN ( SELECT DISTINCT
                                                                                StudentId
                                                                      FROM      (
                                                                                  SELECT    StudentId
                                                                                           ,COUNT(*) AS RowCounter
                                                                                  FROM      arStuEnrollments
                                                                                  WHERE     PrgVerId IN ( SELECT    PrgVerId
                                                                                                          FROM      arPrgVersions
                                                                                                          WHERE     IsContinuingEd = 1 )
                                                                                  GROUP BY  StudentId
                                                                                  HAVING    COUNT(*) = 1
                                                                                ) dtStudent_ContinuingEd )
                                            --AND t2.StuEnrollId IN (
                                            --SELECT TOP 1
                                            --        StuEnrollId
                                            --FROM    arStuEnrollments A1 ,
                                            --        arPrgVersions A2 ,
                                            --        arProgTypes A3
                                            --WHERE   A1.PrgVerId = A2.PrgVerId
                                            --        AND A2.ProgTypId = A3.ProgTypId
                                            --        AND A1.StudentId = t1.StudentId
                                            --        AND A3.IPEDSValue = 58
                                            --ORDER BY StartDate ,
                                            --        EnrollDate ASC )
                                            AND t2.StuEnrollId IN (
                                            SELECT  dbo.GetIPEDS_Spring_GetEnrollmentfromMultipleEnrollment(@CampusId,@ProgId,@EndDate,'undergraduate',
                                                                                                            t1.StudentId) )
                                            AND ( CASE WHEN ( SUBSTRING(LOWER(@DateRangeText),1,10) = 'enrollment' ) THEN  -- IF ACADEMIC PROGRAM
                                                            (
                                                              SELECT    COUNT(*)
                                                              FROM      arStuEnrollments SQ1
                                                                       ,adDegCertSeeking SQ2
                                                              WHERE     SQ1.StuEnrollId = t2.StuEnrollId
                                                                        AND 
							--SQ1.StartDate<=@EndDate AND
								--2/07/2012 - DE9062 - Academic year options should have a cutoff start date For First Time 
                                                                        (
                                                                          t2.StartDate > @AcadInstFirstTimeStartDate
                                                                          AND t2.StartDate <= @EndDate
                                                                        )
                                                                        AND SQ1.degcertseekingid = SQ2.DegCertSeekingId
                                                                        AND SQ2.IPEDSValue = 11
                                                            )
                                                       ELSE -- If PROGRAM Reporter
                                                            (
                                                              SELECT    COUNT(*)
                                                              FROM      arStuEnrollments SQ1
                                                                       ,adDegCertSeeking SQ2
                                                              WHERE     SQ1.StuEnrollId = t2.StuEnrollId
                                                                        AND (
                                                                              SQ1.StartDate >= @StartDate
                                                                              AND SQ1.StartDate <= @EndDate
                                                                            )
                                                                        AND SQ1.degcertseekingid = SQ2.DegCertSeekingId
                                                                        AND SQ2.IPEDSValue = 11
                                                            )
                                                  END ) >= 1
                                ) = 0
	--Union
	--		Select 
	--			t1.SSN, t1.StudentNumber,t1.LastName + ', ' + t1.FirstName + ' ' + ISNULL(t1.MiddleName,'') as StudentName,
	--			'State unknown' as StateDescrip,
	--			57 as fipscode,
	--			1 as FirstTime,
	--			CASE WHEN (SUBSTRING(LOWER(@DateRangeText),1,10)= 'enrollment') THEN --Academic Reporter
	--					(
	--					Select Count(*) from arStuEnrollments SQ1,adDegCertSeeking  SQ2
	--					where 
	--						SQ1.StuEnrollId = t2.StuEnrollId and 
	--						SQ1.StartDate<=@EndDate and
	--						SQ1.degcertseekingid = SQ2.DegCertSeekingId  and
	--						SQ2.IPEDSValue=11 AND 
	--						-- The Student should have a graduatedorreceived in the past 12 months from the report end date
	--						(SQ1.graduatedorreceiveddate>=@HSGradDate_Academic  AND SQ1.graduatedorreceiveddate<=@EndDate)
	--				)
	--			ELSE -- Program Reporter	
	--					(
	--					Select Count(*) from arStuEnrollments SQ1,adDegCertSeeking  SQ2
	--					where 
	--						SQ1.StuEnrollId = t2.StuEnrollId and 
	--						(SQ1.StartDate>=@StartDate AND SQ1.StartDate<=@EndDate) and
	--						SQ1.degcertseekingid = SQ2.DegCertSeekingId  and
	--						SQ2.IPEDSValue=11 AND
	--						-- The Student should have a graduatedorreceived in the past 12 months from the report start date
	--						(SQ1.graduatedorreceiveddate>=@HSGradDate_Program   AND SQ1.graduatedorreceiveddate<=@StartDate)
	--					)
	--			END	
	--			as GraduatedOrGED,
	--			t1.StudentId
	--	from
	--			arStudent t1 LEFT OUTER JOIN arStudAddresses t4 on t4.StudentId=t1.StudentId
	--			--LEFT OUTER JOIN syStates t12 on t12.StateId=t4.StateId  
	--			INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId  
	--			INNER JOIN syStatusCodes t5 on t2.StatusCodeId=t5.StatusCodeId 
	--			INNER JOIN sySysStatus t6 on t5.SysStatusId=t6.SysStatusId and t6.SysStatusId not in (8)
	--			INNER JOIN arPrgVersions t7 on t2.PrgVerId = t7.PrgVerId 
	--			INNER JOIN arPrograms t8 on t7.ProgId = t8.ProgId  
	--			INNER JOIN arProgTypes t9 on t7.ProgTypId = t9.ProgTypId 
	--			LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId 
	--			--INNER JOIN adCitizenships t11 on t1.Citizen  = t11.CitizenshipId 
	--			--INNER JOIN adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId 
	--	where
	--			LTRIM(RTRIM(t2.CampusId))= LTRIM(RTRIM(@CampusId)) and 
	--			(@ProgId is null or t8.ProgId in (Select Val from [MultipleValuesForReportParameters](@ProgId,',',1))) and
	--			t9.IPEDSValue=58 and -- undergraduate
	--			t10.IPEDSValue in (61,62)  -- Full Time And Part Time
	--			-- and t11.IPEDSValue = 65 -- Non-Citizen
	--			--and t2.StartDate<=@EndDate
	--			--and t12.IPEDSValue=11
	--			and t4.default1=1
	--			and t4.StateId IS NULL
	--			AND t4.ForeignZip=0
	--			and StuEnrollId not in
	--			-- Exclude students who are Dropped out/Transferred/Graduated/No Start 
	--			-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
	--			(
	--				Select t1.StuEnrollId from arStuEnrollments t1,SyStatusCodes t2 
	--				where t1.StatusCodeId=t2.StatusCodeId and StartDate <=@EndDate and -- Student started before the end date range
	--				LTRIM(RTRIM(t1.CampusId))= LTRIM(RTRIM(@CampusId)) and
	--				(@ProgId is null or t8.ProgId in (Select Val from [MultipleValuesForReportParameters](@ProgId,',',1)))   
	--				and t2.SysStatusId in (12,14,19,8) -- Dropped out/Transferred/Graduated/No Start
	--				-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
	--				and (t1.DateDetermined<@StartDate or ExpGradDate<@StartDate or LDA<@StartDate)
	--			)
	--			and t2.StuEnrollId in (select Top 1 StuEnrollId 
    --                                 from arStuEnrollments A1,arPrgVersions A2,arProgTypes A3 
    --                                 where A1.PrgVerId=A2.PrgVerId and A2.ProgTypId=A3.ProgTypId and A1.StudentId=t1.StudentId 
    --                                   and A3.IPEDSValue=58 order by StartDate,EnrollDate asc
    --                                )
	--			and
	--			(CASE WHEN (SUBSTRING(LOWER(@DateRangeText),1,10)= 'enrollment') THEN  -- IF ACADEMIC PROGRAM
	--				(
	--					Select Count(*) from arStuEnrollments SQ1,adDegCertSeeking  SQ2
	--					where 
	--						SQ1.StuEnrollId = t2.StuEnrollId and 
	--						SQ1.StartDate<=@EndDate and
	--						SQ1.degcertseekingid = SQ2.DegCertSeekingId  
	--						and	SQ2.IPEDSValue=11
	--				)
	--			ELSE -- If PROGRAM Reporter
	--				(
	--					Select Count(*) from arStuEnrollments SQ1,adDegCertSeeking  SQ2
	--					where 
	--						SQ1.StuEnrollId = t2.StuEnrollId and 
	--						(SQ1.StartDate>=@StartDate AND SQ1.StartDate<=@EndDate) and
	--						SQ1.degcertseekingid = SQ2.DegCertSeekingId 
	--						and	SQ2.IPEDSValue=11
	--				)
	--			END) >=1
                      UNION
                      SELECT TOP 1
                                NULL AS SSN
                               ,NULL AS StudentNumber
                               ,NULL AS StudentName
                               ,'Residence unknown/unreported' AS StateDescrip
                               ,98 AS fipscode
                               ,NULL AS FirstTime
                               ,NULL AS GraduatedOrGED
                               ,NULL AS StudentId
                      FROM      arStudent
                      UNION
                      SELECT TOP 1
                                NULL AS SSN
                               ,NULL AS StudentNumber
                               ,NULL AS StudentName
                               ,'Foreign Countries' AS StateDescrip
                               ,90 AS fipscode
                               ,NULL AS FirstTime
                               ,NULL AS GraduatedOrGED
                               ,NULL AS StudentId
                      FROM      arStudent
                      WHERE     (
                                  SELECT    COUNT(t1.FirstName)
                                  FROM      arStudent t1
                                  LEFT OUTER JOIN arStudAddresses t4 ON t4.StudentId = t1.StudentId
				--LEFT OUTER JOIN syStates t12 on t12.StateId=t4.StateId  
                                  INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                                  INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                                  INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                                               AND t6.SysStatusId NOT IN ( 8 )
                                  INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                                  INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                                  INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                                  LEFT JOIN arAttendTypes t10 ON t2.AttendTypeId = t10.AttendTypeId 
				--INNER JOIN adCitizenships t11 on t1.Citizen  = t11.CitizenshipId 
                                  INNER JOIN adDegCertSeeking t12 ON t2.DegCertSeekingId = t12.DegCertSeekingId
                                  WHERE     LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                                            AND (
                                                  @ProgId IS NULL
                                                  OR t8.ProgId IN ( SELECT  Val
                                                                    FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                                )
                                            AND t9.IPEDSValue = 58
                                            AND -- undergraduate
                                            t10.IPEDSValue IN ( 61,62 )  -- Full Time And Part Time
                                            AND t4.default1 = 1
                                            AND t4.ForeignZip = 1
                                            AND StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
				-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
				( SELECT    t1.StuEnrollId
                  FROM      arStuEnrollments t1
                           ,syStatusCodes t2
                  WHERE     t1.StatusCodeId = t2.StatusCodeId
                            AND StartDate <= @EndDate
                            AND -- Student started before the end date range
                            LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                            AND (
                                  @ProgId IS NULL
                                  OR t8.ProgId IN ( SELECT  Val
                                                    FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                )
                            AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
					-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                            AND (
                                  t1.DateDetermined < @StartDate
                                  OR ExpGradDate < @StartDate
                                  OR LDA < @StartDate
                                ) )
                                                        	-- If Student is enrolled in only program version and if that program version 
						-- happens to be a continuing ed program exclude the student
                                            AND t2.StudentId NOT IN ( SELECT DISTINCT
                                                                                StudentId
                                                                      FROM      (
                                                                                  SELECT    StudentId
                                                                                           ,COUNT(*) AS RowCounter
                                                                                  FROM      arStuEnrollments
                                                                                  WHERE     PrgVerId IN ( SELECT    PrgVerId
                                                                                                          FROM      arPrgVersions
                                                                                                          WHERE     IsContinuingEd = 1 )
                                                                                  GROUP BY  StudentId
                                                                                  HAVING    COUNT(*) = 1
                                                                                ) dtStudent_ContinuingEd )
                                            --AND t2.StuEnrollId IN (
                                            --SELECT TOP 1
                                            --        StuEnrollId
                                            --FROM    arStuEnrollments A1 ,
                                            --        arPrgVersions A2 ,
                                            --        arProgTypes A3
                                            --WHERE   A1.PrgVerId = A2.PrgVerId
                                            --        AND A2.ProgTypId = A3.ProgTypId
                                            --        AND A1.StudentId = t1.StudentId
                                            --        AND A3.IPEDSValue = 58
                                            --ORDER BY StartDate ,
                                            --        EnrollDate ASC )
                                            AND t2.StuEnrollId IN (
                                            SELECT  dbo.GetIPEDS_Spring_GetEnrollmentfromMultipleEnrollment(@CampusId,@ProgId,@EndDate,'undergraduate',
                                                                                                            t1.StudentId) )
                                            AND ( CASE WHEN ( SUBSTRING(LOWER(@DateRangeText),1,10) = 'enrollment' ) THEN  -- IF ACADEMIC PROGRAM
                                                            (
                                                              SELECT    COUNT(*)
                                                              FROM      arStuEnrollments SQ1
                                                                       ,adDegCertSeeking SQ2
                                                              WHERE     SQ1.StuEnrollId = t2.StuEnrollId
                                                                        AND 
							--SQ1.StartDate<=@EndDate and
									--2/07/2012 - DE9062 - Academic year options should have a cutoff start date For First Time 
                                                                        (
                                                                          t2.StartDate > @AcadInstFirstTimeStartDate
                                                                          AND t2.StartDate <= @EndDate
                                                                        )
                                                                        AND SQ1.degcertseekingid = SQ2.DegCertSeekingId
                                                                        AND SQ2.IPEDSValue = 11
                                                            )
                                                       ELSE -- If PROGRAM Reporter
                                                            (
                                                              SELECT    COUNT(*)
                                                              FROM      arStuEnrollments SQ1
                                                                       ,adDegCertSeeking SQ2
                                                              WHERE     SQ1.StuEnrollId = t2.StuEnrollId
                                                                        AND (
                                                                              SQ1.StartDate >= @StartDate
                                                                              AND SQ1.StartDate <= @EndDate
                                                                            )
                                                                        AND SQ1.degcertseekingid = SQ2.DegCertSeekingId
                                                                        AND SQ2.IPEDSValue = 11
                                                            )
                                                  END ) >= 1
                                ) = 0
                      UNION
                      SELECT    t1.SSN
                               ,t1.StudentNumber
                               ,t1.LastName + ', ' + t1.FirstName + ' ' + ISNULL(t1.MiddleName,'') AS StudentName
                               ,'Foreign Countries' AS StateDescrip
                               ,90 AS fipscode
                               ,1 AS FirstTime
                               ,CASE WHEN ( SUBSTRING(LOWER(@DateRangeText),1,10) = 'enrollment' )
                                     THEN --Academic Reporter
                                          (
                                            SELECT  COUNT(*)
                                            FROM    arStuEnrollments SQ1
                                                   ,adDegCertSeeking SQ2
                                            WHERE   SQ1.StuEnrollId = t2.StuEnrollId
                                                    AND 
							--SQ1.StartDate<=@EndDate and
									--2/07/2012 - DE9062 - Academic year options should have a cutoff start date For First Time 
                                                    (
                                                      t2.StartDate > @AcadInstFirstTimeStartDate
                                                      AND t2.StartDate <= @EndDate
                                                    )
                                                    AND SQ1.degcertseekingid = SQ2.DegCertSeekingId
                                                    AND SQ2.IPEDSValue = 11
                                                    AND 
							-- The Student should have a graduatedorreceived in the past 12 months from the report end date
                                                    --( SQ1.graduatedorreceiveddate >= @HSGradDate_Academic
                                                    --  AND SQ1.graduatedorreceiveddate <= @EndDate
                                                    --)
                                                     -- DE9068 - QA: Condition to mark the student under the second column on the Spring- Part C- detail and summary report is incorrect.
                                                    (
                                                      SQ1.graduatedorreceiveddate >= DATEADD(YY,-1,t2.StartDate)
                                                      AND SQ1.graduatedorreceiveddate <= t2.StartDate
                                                    )
                                          )
                                     ELSE -- Program Reporter	
                                          (
                                            SELECT  COUNT(*)
                                            FROM    arStuEnrollments SQ1
                                                   ,adDegCertSeeking SQ2
                                            WHERE   SQ1.StuEnrollId = t2.StuEnrollId
                                                    AND (
                                                          SQ1.StartDate >= @StartDate
                                                          AND SQ1.StartDate <= @EndDate
                                                        )
                                                    AND SQ1.degcertseekingid = SQ2.DegCertSeekingId
                                                    AND SQ2.IPEDSValue = 11
                                                    AND
							-- The Student should have a graduatedorreceived in the past 12 months from the report start date
                                                    --( SQ1.graduatedorreceiveddate >= @HSGradDate_Program
                                                    --  AND SQ1.graduatedorreceiveddate <= @StartDate
                                                    --)
                                                     -- DE9068 - QA: Condition to mark the student under the second column on the Spring- Part C- detail and summary report is incorrect.
                                                    (
                                                      SQ1.graduatedorreceiveddate >= DATEADD(YY,-1,t2.StartDate)
                                                      AND SQ1.graduatedorreceiveddate <= t2.StartDate
                                                    )
                                          )
                                END AS GraduatedOrGED
                               ,t1.StudentId
                      FROM      arStudent t1
                      LEFT OUTER JOIN arStudAddresses t4 ON t4.StudentId = t1.StudentId
				--LEFT OUTER JOIN syStates t12 on t12.StateId=t4.StateId  
                      INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                      INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                      INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                                   AND t6.SysStatusId NOT IN ( 8 )
                      INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                      INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                      INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                      LEFT JOIN arAttendTypes t10 ON t2.AttendTypeId = t10.AttendTypeId 
				--INNER JOIN adCitizenships t11 on t1.Citizen  = t11.CitizenshipId 
				--INNER JOIN adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId 
                      WHERE     LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                                AND (
                                      @ProgId IS NULL
                                      OR t8.ProgId IN ( SELECT  Val
                                                        FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                    )
                                AND t9.IPEDSValue = 58
                                AND -- undergraduate
                                t10.IPEDSValue IN ( 61,62 )  -- Full Time And Part Time
				-- and t11.IPEDSValue = 65 -- Non-Citizen
				--and t2.StartDate<=@EndDate
				--and t12.IPEDSValue=11
                                AND t4.default1 = 1
                                AND t4.ForeignZip = 1
                                AND StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
				-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
				( SELECT    t1.StuEnrollId
                  FROM      arStuEnrollments t1
                           ,syStatusCodes t2
                  WHERE     t1.StatusCodeId = t2.StatusCodeId
                            AND StartDate <= @EndDate
                            AND -- Student started before the end date range
                            LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                            AND (
                                  @ProgId IS NULL
                                  OR t8.ProgId IN ( SELECT  Val
                                                    FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                )
                            AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
					-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                            AND (
                                  t1.DateDetermined < @StartDate
                                  OR ExpGradDate < @StartDate
                                  OR LDA < @StartDate
                                ) )
                                            	-- If Student is enrolled in only program version and if that program version 
						-- happens to be a continuing ed program exclude the student
                                AND t2.StudentId NOT IN ( SELECT DISTINCT
                                                                    StudentId
                                                          FROM      (
                                                                      SELECT    StudentId
                                                                               ,COUNT(*) AS RowCounter
                                                                      FROM      arStuEnrollments
                                                                      WHERE     PrgVerId IN ( SELECT    PrgVerId
                                                                                              FROM      arPrgVersions
                                                                                              WHERE     IsContinuingEd = 1 )
                                                                      GROUP BY  StudentId
                                                                      HAVING    COUNT(*) = 1
                                                                    ) dtStudent_ContinuingEd )
                                --AND t2.StuEnrollId IN (
                                --SELECT TOP 1
                                --        StuEnrollId
                                --FROM    arStuEnrollments A1 ,
                                --        arPrgVersions A2 ,
                                --        arProgTypes A3
                                --WHERE   A1.PrgVerId = A2.PrgVerId
                                --        AND A2.ProgTypId = A3.ProgTypId
                                --        AND A1.StudentId = t1.StudentId
                                --        AND A3.IPEDSValue = 58
                                --ORDER BY StartDate ,
                                --        EnrollDate ASC )
                                AND t2.StuEnrollId IN (
                                SELECT  dbo.GetIPEDS_Spring_GetEnrollmentfromMultipleEnrollment(@CampusId,@ProgId,@EndDate,'undergraduate',t1.StudentId) )
                                AND ( CASE WHEN ( SUBSTRING(LOWER(@DateRangeText),1,10) = 'enrollment' ) THEN  -- IF ACADEMIC PROGRAM
                                                (
                                                  SELECT    COUNT(*)
                                                  FROM      arStuEnrollments SQ1
                                                           ,adDegCertSeeking SQ2
                                                  WHERE     SQ1.StuEnrollId = t2.StuEnrollId
                                                            AND 
							--SQ1.StartDate<=@EndDate and
									--2/07/2012 - DE9062 - Academic year options should have a cutoff start date For First Time 
                                                            (
                                                              t2.StartDate > @AcadInstFirstTimeStartDate
                                                              AND t2.StartDate <= @EndDate
                                                            )
                                                            AND SQ1.degcertseekingid = SQ2.DegCertSeekingId
                                                            AND SQ2.IPEDSValue = 11
                                                )
                                           ELSE -- If PROGRAM Reporter
                                                (
                                                  SELECT    COUNT(*)
                                                  FROM      arStuEnrollments SQ1
                                                           ,adDegCertSeeking SQ2
                                                  WHERE     SQ1.StuEnrollId = t2.StuEnrollId
                                                            AND (
                                                                  SQ1.StartDate >= @StartDate
                                                                  AND SQ1.StartDate <= @EndDate
                                                                )
                                                            AND SQ1.degcertseekingid = SQ2.DegCertSeekingId
                                                            AND SQ2.IPEDSValue = 11
                                                )
                                      END ) >= 1
                      UNION
                      SELECT    NULL AS SSN
                               ,NULL AS StudentNumber
                               ,NULL AS StudentName
                               ,ST.StateDescrip
                               ,ST.fipscode
                               ,NULL AS FirstTime
                               ,NULL AS GraduatedOrGED
                               ,NULL AS StudentId
                      FROM      syStates ST
                      LEFT OUTER JOIN arStudAddresses SA ON ST.StateId = SA.StateId
                      LEFT OUTER JOIN arStudent S ON S.StudentId = SA.StudentId
                      WHERE     ST.StateId NOT IN (
                                SELECT DISTINCT
                                        t13.StateId
                                FROM    arStudent t1
                                INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                                LEFT OUTER JOIN arStudAddresses t4 ON t4.StudentId = t1.StudentId
                                LEFT OUTER JOIN syStates t13 ON t13.StateId = t4.StateId
                                INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                                INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                                             AND t6.SysStatusId NOT IN ( 8 )
                                INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                                INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                                INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                                LEFT JOIN arAttendTypes t10 ON t2.AttendTypeId = t10.AttendTypeId
                                INNER JOIN adCitizenships t12 ON t1.Citizen = t12.CitizenshipId
								--INNER JOIN adDegCertSeeking t17 ON t2.degcertseekingid = t17.DegCertSeekingId  
                                WHERE   LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                                        AND (
                                              @ProgId IS NULL
                                              OR t8.ProgId IN ( SELECT  Val
                                                                FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                            )
                                        AND t9.IPEDSValue = 58
                                        AND t10.IPEDSValue IN ( 61,62 ) 
								--and t2.StartDate<=@EndDate
                                        AND t4.default1 = 1
                                        AND
								--and t17.IPEDSValue=11
                                        ( CASE WHEN ( SUBSTRING(LOWER(@DateRangeText),1,10) = 'enrollment' ) THEN  -- IF ACADEMIC PROGRAM
                                                    (
                                                      SELECT    COUNT(*)
                                                      FROM      arStuEnrollments SQ1
                                                               ,adDegCertSeeking SQ2
                                                      WHERE     SQ1.StuEnrollId = t2.StuEnrollId
                                                                AND 
											--SQ1.StartDate<=@EndDate and
													--2/07/2012 - DE9062 - Academic year options should have a cutoff start date For First Time 
                                                                (
                                                                  t2.StartDate > @AcadInstFirstTimeStartDate
                                                                  AND t2.StartDate <= @EndDate
                                                                )
                                                                AND SQ1.degcertseekingid = SQ2.DegCertSeekingId
                                                                AND SQ2.IPEDSValue = 11
                                                    )
                                               ELSE -- If PROGRAM Reporter
                                                    (
                                                      SELECT    COUNT(*)
                                                      FROM      arStuEnrollments SQ1
                                                               ,adDegCertSeeking SQ2
                                                      WHERE     SQ1.StuEnrollId = t2.StuEnrollId
                                                                AND (
                                                                      SQ1.StartDate >= @StartDate
                                                                      AND SQ1.StartDate <= @EndDate
                                                                    )
                                                                AND SQ1.degcertseekingid = SQ2.DegCertSeekingId
                                                                AND SQ2.IPEDSValue = 11
                                                    )
                                          END ) >= 1
                                        AND StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
								-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
								( SELECT    t1.StuEnrollId
                                  FROM      arStuEnrollments t1
                                           ,syStatusCodes t2
                                  WHERE     t1.StatusCodeId = t2.StatusCodeId
                                            AND StartDate <= @EndDate
                                            AND -- Student started before the end date range
                                            LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                            AND (
                                                  @ProgId IS NULL
                                                  OR t8.ProgId IN ( SELECT  Val
                                                                    FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                                )
                                            AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
									-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                            AND (
                                                  t1.DateDetermined < @StartDate
                                                  OR ExpGradDate < @StartDate
                                                  OR LDA < @StartDate
                                                ) )
                                                    	-- If Student is enrolled in only program version and if that program version 
						-- happens to be a continuing ed program exclude the student
                                        AND t2.StudentId NOT IN ( SELECT DISTINCT
                                                                            StudentId
                                                                  FROM      (
                                                                              SELECT    StudentId
                                                                                       ,COUNT(*) AS RowCounter
                                                                              FROM      arStuEnrollments
                                                                              WHERE     PrgVerId IN ( SELECT    PrgVerId
                                                                                                      FROM      arPrgVersions
                                                                                                      WHERE     IsContinuingEd = 1 )
                                                                              GROUP BY  StudentId
                                                                              HAVING    COUNT(*) = 1
                                                                            ) dtStudent_ContinuingEd )
                                        AND t13.StateId IS NOT NULL
                                        --AND t2.StuEnrollId IN (
                                        --SELECT TOP 1
                                        --        StuEnrollId
                                        --FROM    arStuEnrollments A1 ,
                                        --        arPrgVersions A2 ,
                                        --        arProgTypes A3
                                        --WHERE   A1.PrgVerId = A2.PrgVerId
                                        --        AND A2.ProgTypId = A3.ProgTypId
                                        --        AND A1.StudentId = t1.StudentId
                                        --        AND A3.IPEDSValue = 58
                                        --ORDER BY StartDate ,
                                        --        EnrollDate ASC )
                                        AND t2.StuEnrollId IN (
                                        SELECT  dbo.GetIPEDS_Spring_GetEnrollmentfromMultipleEnrollment(@CampusId,@ProgId,@EndDate,'undergraduate',t1.StudentId) ) )
                      UNION
                      SELECT    t1.SSN
                               ,t1.StudentNumber
                               ,t1.LastName + ', ' + t1.FirstName + ' ' + ISNULL(t1.MiddleName,'') AS StudentName
                               ,
				--t14.StateDescrip as StateDescrip, t14.fipscode AS fipscode,
                                CASE WHEN t14.StateDescrip IS NULL THEN 'State unknown'
                                     ELSE t14.StateDescrip
                                END AS StateDescrip
                               ,CASE WHEN t14.StateDescrip IS NULL THEN 57
                                     ELSE t14.fipscode
                                END AS fipscode
                               ,1 AS FirstTime
                               ,CASE WHEN ( SUBSTRING(LOWER(@DateRangeText),1,10) = 'enrollment' )
                                     THEN --Academic Reporter
                                          (
                                            SELECT  COUNT(*)
                                            FROM    arStuEnrollments SQ1
                                                   ,adDegCertSeeking SQ2
                                            WHERE   SQ1.StuEnrollId = t2.StuEnrollId
                                                    AND 
							--SQ1.StartDate<=@EndDate and
									--2/07/2012 - DE9062 - Academic year options should have a cutoff start date For First Time 
                                                    (
                                                      t2.StartDate > @AcadInstFirstTimeStartDate
                                                      AND t2.StartDate <= @EndDate
                                                    )
                                                    AND SQ1.degcertseekingid = SQ2.DegCertSeekingId
                                                    AND SQ2.IPEDSValue = 11
                                                    AND 
							-- The Student should have a graduatedorreceived in the past 12 months from the report end date
                                  --( SQ1.graduatedorreceiveddate >= @HSGradDate_Academic
                                                    --  AND SQ1.graduatedorreceiveddate <= @EndDate
                                                    --)
                                                     -- DE9068 - QA: Condition to mark the student under the second column on the Spring- Part C- detail and summary report is incorrect.
                                                    (
                                                      SQ1.graduatedorreceiveddate >= DATEADD(YY,-1,t2.StartDate)
                                                      AND SQ1.graduatedorreceiveddate <= t2.StartDate
                                                    )
                                          )
                                     ELSE -- Program Reporter	
                                          (
                                            SELECT  COUNT(*)
                                            FROM    arStuEnrollments SQ1
                                                   ,adDegCertSeeking SQ2
                                            WHERE   SQ1.StuEnrollId = t2.StuEnrollId
                                                    AND (
                                                          SQ1.StartDate >= @StartDate
                                                          AND SQ1.StartDate <= @EndDate
                                                        )
                                                    AND SQ1.degcertseekingid = SQ2.DegCertSeekingId
                                                    AND SQ2.IPEDSValue = 11
                                                    AND
							-- The Student should have a graduatedorreceived in the past 12 months from the report start date
                                                    --( SQ1.graduatedorreceiveddate >= @HSGradDate_Program
                                                    --  AND SQ1.graduatedorreceiveddate <= @StartDate
                                                    --)
                                                     -- DE9068 - QA: Condition to mark the student under the second column on the Spring- Part C- detail and summary report is incorrect.
                                                    (
                                                      SQ1.graduatedorreceiveddate >= DATEADD(YY,-1,t2.StartDate)
                                                      AND SQ1.graduatedorreceiveddate <= t2.StartDate
                                                    )
                                          )
                                END AS GraduatedOrGED
                               ,t1.StudentId
                      FROM      arStudent t1
                      LEFT OUTER JOIN arStudAddresses t4 ON t4.StudentId = t1.StudentId
                      LEFT OUTER JOIN syStates t14 ON t14.StateId = t4.StateId
                      INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                      INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                      INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                                   AND t6.SysStatusId NOT IN ( 8 )
                      INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                      INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                      INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                      LEFT JOIN arAttendTypes t10 ON t2.AttendTypeId = t10.AttendTypeId
                      INNER JOIN adCitizenships t12 ON t1.Citizen = t12.CitizenshipId 
				--INNER JOIN adDegCertSeeking t13 ON t2.degcertseekingid = t13.DegCertSeekingId  
                      WHERE     t2.CampusId = LTRIM(RTRIM(@CampusId))
                                AND (
                                      @ProgId IS NULL
                                      OR t8.ProgId IN ( SELECT  Val
                                                        FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                    )
                                AND t9.IPEDSValue = 58
                                AND t10.IPEDSValue IN ( 61,62 ) 
				--and t1.Race is not null 
				--and (t12.IPEDSValue <> 65 or t12.IPEDSValue is NULL) -- Ignore Non US Citizens (Foreign Students)
				--and t2.StartDate<=@EndDate
                                AND t4.default1 = 1
                                AND t4.ForeignZip = 0
				--and t13.IPEDSValue=11
                                AND ( CASE WHEN ( SUBSTRING(LOWER(@DateRangeText),1,10) = 'enrollment' ) THEN  -- IF ACADEMIC PROGRAM
                                                (
                                                  SELECT    COUNT(*)
                                                  FROM      arStuEnrollments SQ1
                                                           ,adDegCertSeeking SQ2
                                                  WHERE     SQ1.StuEnrollId = t2.StuEnrollId
                                                            AND 
							--SQ1.StartDate<=@EndDate and
									--2/07/2012 - DE9062 - Academic year options should have a cutoff start date For First Time 
                                                            (
                                                              t2.StartDate > @AcadInstFirstTimeStartDate
                                                              AND t2.StartDate <= @EndDate
                                                            )
                                                            AND SQ1.degcertseekingid = SQ2.DegCertSeekingId
                                                            AND SQ2.IPEDSValue = 11
                                                )
                                           ELSE -- If PROGRAM Reporter
                                                (
                                                  SELECT    COUNT(*)
                                                  FROM      arStuEnrollments SQ1
                                                           ,adDegCertSeeking SQ2
                                                  WHERE     SQ1.StuEnrollId = t2.StuEnrollId
                                                            AND (
                                                                  SQ1.StartDate >= @StartDate
                                                                  AND SQ1.StartDate <= @EndDate
                                                                )
                                                            AND SQ1.degcertseekingid = SQ2.DegCertSeekingId
                                                            AND SQ2.IPEDSValue = 11
                                                )
                                      END ) >= 1
                                AND StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
				-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
				( SELECT    t1.StuEnrollId
                  FROM      arStuEnrollments t1
                           ,syStatusCodes t2
                  WHERE     t1.StatusCodeId = t2.StatusCodeId
                            AND StartDate <= @EndDate
                            AND -- Student started before the end date range
                            LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                            AND (
                                  @ProgId IS NULL
                                  OR t8.ProgId IN ( SELECT  Val
                                                    FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                )
                            AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
					-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                            AND (
                                  t1.DateDetermined < @StartDate
                                  OR ExpGradDate < @StartDate
                                  OR LDA < @StartDate
                                ) )
                                            	-- If Student is enrolled in only program version and if that program version 
						-- happens to be a continuing ed program exclude the student
                                AND t2.StudentId NOT IN ( SELECT DISTINCT
                                                                    StudentId
                                                          FROM      (
                                                                      SELECT    StudentId
                                                                               ,COUNT(*) AS RowCounter
                                                                      FROM      arStuEnrollments
                                                                      WHERE     PrgVerId IN ( SELECT    PrgVerId
                                                                                              FROM      arPrgVersions
                                                                                              WHERE     IsContinuingEd = 1 )
                                                                      GROUP BY  StudentId
                                                                      HAVING    COUNT(*) = 1
                                                                    ) dtStudent_ContinuingEd )
                                --AND t2.StuEnrollId IN (
                                --SELECT TOP 1
                                --        StuEnrollId
                                --FROM    arStuEnrollments A1 ,
                                --        arPrgVersions A2 ,
                                --        arProgTypes A3
                                --WHERE   A1.PrgVerId = A2.PrgVerId
                                --        AND A2.ProgTypId = A3.ProgTypId
                                --        AND A1.StudentId = t1.StudentId
                                --        AND A3.IPEDSValue = 58
                                --ORDER BY StartDate ,
                                --        EnrollDate ASC )
                                AND t2.StuEnrollId IN (
                                SELECT  dbo.GetIPEDS_Spring_GetEnrollmentfromMultipleEnrollment(@CampusId,@ProgId,@EndDate,'undergraduate',t1.StudentId) )
                    ) dt;



    SELECT  StateDescrip
           ,fipscode
           ,SUM(FirstTimeCount) AS FirstTimeCount
           ,SUM(GraduatedOrGEDCount) AS GraduatedOrGEDCount
    FROM    (
              --	Select Top 1 'State unknown' as StateDescrip, 98 as fipscode,0 as FirstTimeCount,0 as GraduatedOrGEDCount,null as studentid
	--	from
	--	arStudent 
	--	where (select COUNT(fipscode) from #FallPartB4FTUG where  fipscode=98)=0
		
	--union all
              SELECT    StateDescrip
                       ,fipscode
                       ,CASE ( FirstTime )
                          WHEN 'X' THEN 1
                          ELSE 0
                        END FirstTimeCount
                       ,CASE ( GraduatedOrGED )
                          WHEN 'X' THEN 1
                          ELSE 0
                        END GraduatedOrGEDCount
                       ,NULL AS StudentId
              FROM      #FallPartB4FTUG
            ) dt
    GROUP BY StateDescrip
           ,fipscode
           ,StudentId
    ORDER BY fipscode;
    DROP TABLE #FallPartB4FTUG;



GO
