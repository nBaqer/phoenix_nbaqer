SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[GetActiveEnrollmentsWithClassAssociatedForRegisterClass]
    @StuEnrollId UNIQUEIDENTIFIER
   ,@ClassId UNIQUEIDENTIFIER
AS
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;

        DECLARE @reqId VARCHAR(50) = (
                                       SELECT   ReqId
                                       FROM     arClassSections
                                       WHERE    ClsSectionId = @ClassId
                                     );

        DECLARE @StudentId VARCHAR(50) = (
                                           SELECT   StudentId
                                           FROM     dbo.arStuEnrollments
                                           WHERE    StuEnrollId = @StuEnrollId
                                         );
        DECLARE @BeginDate DATETIME = (
                                        SELECT  StartDate
                                        FROM    arClassSections
                                        WHERE   ClsSectionId = @ClassId
                                      );

        DECLARE @FinalTable TABLE
            (
             StuEnrollId UNIQUEIDENTIFIER
            );

        INSERT  INTO @FinalTable
                SELECT  @StuEnrollId;
       
        INSERT  INTO @FinalTable
                SELECT  enroll.StuEnrollId
                FROM    arStuEnrollments enroll
                JOIN    dbo.arPrgVersions ver ON ver.PrgVerId = enroll.PrgVerId
                JOIN    dbo.arProgVerDef def ON def.PrgVerId = ver.PrgVerId
                LEFT JOIN dbo.arTransferGrades tgrades ON tgrades.StuEnrollId = enroll.StuEnrollId
                                                          AND tgrades.ReqId = @reqId
                WHERE   enroll.StudentId = @StudentId
                        AND (
                              def.ReqId IN ( SELECT GrpId
                                             FROM   dbo.arReqGrpDef
                                             WHERE  ReqId = @reqId )
                              OR def.ReqId = @reqId
                            )
                        AND (
                              enroll.StatusCodeId IN ( SELECT DISTINCT
                                                                StatusCodeId
                                                       FROM     dbo.syStatusCodes
                                                       JOIN     dbo.sySysStatus ss ON ss.SysStatusId = syStatusCodes.SysStatusId
                                                       WHERE    ss.PostAcademics = 1 )
                              OR ( enroll.StatusCodeId IN ( SELECT  StatusCodeId
                                                            FROM    dbo.syStatusCodes
                                                            WHERE   SysStatusId = 7 )  -- Future Start
                               --    AND CAST(enroll.StartDate AS DATE) >= CAST(@BeginDate AS DATE)
                                                            )
                            )
                        AND tgrades.StuEnrollId IS NULL -- exclude transferred enrollment
                        AND enroll.StuEnrollId <> @StuEnrollId;


        SELECT  StuEnrollId AS StuEnrollId
        FROM    @FinalTable;

    END;


GO
