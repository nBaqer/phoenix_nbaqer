SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

------------------------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[USP_ApplyFeesByCourse]
    (
     @TermId UNIQUEIDENTIFIER
    ,@User VARCHAR(50)
    ,@CampusID UNIQUEIDENTIFIER
    ,@PostFeesDate DATETIME
    ,@Result INTEGER OUTPUT
    )
AS
    SET NOCOUNT ON;
    BEGIN 
    
        DECLARE @StuEnrollID UNIQUEIDENTIFIER;
        DECLARE @CourseFeeID AS UNIQUEIDENTIFIER;
        DECLARE @TransCodeID AS UNIQUEIDENTIFIER;
        DECLARE @TransCodeDescrip AS VARCHAR(50);
        DECLARE @StudentName AS VARCHAR(50);
        DECLARE @TransDescrip AS VARCHAR(50);
        DECLARE @TransAmount DECIMAL(19,4);
        SET @TransAmount = 0;
         
        BEGIN TRANSACTION;
       
        DECLARE c1 CURSOR READ_ONLY
        FOR
            (
              SELECT    R.StuEnrollId
                       ,CF.CourseFeeId
                       ,CF.TransCodeId
                       ,( (
                            SELECT  TransCodeDescrip
                            FROM    saTransCodes
                            WHERE   TransCodeId = CF.TransCodeId
                          ) + '/' + RQ.Descrip ) TransCodeDescrip
                       ,RQ.Descrip + ' ' + (
                                             SELECT LastName + ' ' + FirstName
                                             FROM   arStudent
                                             WHERE  StudentId = (
                                                                  SELECT    StudentId
                                                                  FROM      arStuEnrollments
                                                                  WHERE     StuEnrollId = SE.StuEnrollId
                                                                )
                                           ) TransDescrip
                       ,( CASE LEN(CF.RateScheduleId)
                            WHEN 36 THEN (
                                           SELECT   CASE RSD.FlatAmount
                                                      WHEN 0.00 THEN CASE RSD.UnitId
                                                                       WHEN 0 THEN Rate * RQ.Credits
                                                                       WHEN 1 THEN Rate * RQ.Hours
                                                                     END
                                                      ELSE FlatAmount
                                                    END
                                           FROM     saRateSchedules RS
                                                   ,saRateScheduleDetails RSD
                                           WHERE    RS.RateScheduleId = RSD.RateScheduleId
                                                    AND RS.RateScheduleId = CF.RateScheduleId
                                                    AND (
                                                          RSD.TuitionCategoryId = SE.TuitionCategoryId
                                                          OR (
                                                               RSD.TuitionCategoryId IS NULL
                                                               AND SE.TuitionCategoryId IS NULL
                                                             )
                                                        )
                                                    AND (
                                                          (
                                                            RSD.FlatAmount = 0.00
                                                            AND RSD.UnitId = 0
							 --AND RSD.MaxUnits=(SELECT MIN(MaxUnits) 
								--			   FROM saRateScheduleDetails
								--			   WHERE MaxUnits>=RQ.Credits
								--			   AND RateScheduleId=RSD.RateScheduleId
								--			   AND (
								--						RSD.TuitionCategoryId=SE.TuitionCategoryId
								--										 OR
								--					   (RSD.TuitionCategoryId IS NULL AND SE.TuitionCategoryId IS NULL)
								--					)
								--			)
                                                            AND RSD.MinUnits <= RQ.Credits
                                                            AND RSD.MaxUnits >= RQ.Credits
                                                          )
                                                          OR (
                                                               RSD.FlatAmount = 0.00
                                                               AND RSD.UnitId = 1
						    --AND RSD.MaxUnits=(SELECT MIN(MaxUnits)
										--	  FROM saRateScheduleDetails
										--	  WHERE MaxUnits>=RQ.Hours 
										--	  AND RateScheduleId=RSD.RateScheduleId
										--	  AND (
										--			RSD.TuitionCategoryId=SE.TuitionCategoryId
										--							 OR 
										--			(RSD.TuitionCategoryId IS NULL AND SE.TuitionCategoryId IS NULL)
										--		  )
										--	)
                                                               AND RSD.MinUnits <= RQ.Hours
                                                               AND RSD.MaxUnits >= RQ.Hours
                                                             )
                                                          OR (
                                                               RSD.FlatAmount > 0.00
						    --AND RSD.MaxUnits=(SELECT MIN(MaxUnits)
										--	  FROM saRateScheduleDetails
										--	  WHERE MaxUnits>=RQ.Credits
										--	  AND RateScheduleId=RSD.RateScheduleId
										--	  AND (
										--				RSD.TuitionCategoryId=SE.TuitionCategoryId
										--									OR 
										--				(RSD.TuitionCategoryId IS NULL AND SE.TuitionCategoryId IS NULL)
										--		  )
										--	 )
                                                               AND RSD.MinUnits <= RQ.Credits
                                                               AND RSD.MaxUnits >= RQ.Credits
                                                             )
                                                        )
                                         )
                            ELSE ( CASE CF.UnitId
                                     WHEN 0 THEN CF.Amount * RQ.Credits
                                     WHEN 1 THEN CF.Amount * RQ.Hours
                                     WHEN 2 THEN CF.Amount
                                   END )
                          END ) TransAmount
              FROM      arResults R
                       ,arStuEnrollments SE
                       ,arClassSections CS
                       ,arClassSectionTerms CST
                       ,arReqs RQ
                       ,saCourseFees CF
                       ,syStatusCodes SC
              WHERE     R.TestId = CS.ClsSectionId
                        AND R.StuEnrollId = SE.StuEnrollId
                        AND CS.ReqId = CF.CourseId
                        AND CF.CourseId = RQ.reqId
                        AND SE.CampusId = @CampusID
                        AND EXISTS ( SELECT *
                                     FROM   saCourseFees CF2
                                     WHERE  CF2.CourseId = CF.CourseId
                                            AND CF2.CourseFeeId = CF.CourseFeeId
                                            AND CF2.StartDate = (
                                                                  SELECT    MAX(startdate)
                                                                  FROM      saCourseFees CF3
                                                                  WHERE     CF2.CourseId = CF3.CourseId
                                                                            AND CF2.TransCodeId = CF3.TransCodeId
                                                                            AND CF3.StartDate <= GETDATE()
                                                                            AND (
                                                                                  (
                                                                                    (
                                                                                      CF3.RateScheduleId IS NULL
                                                                                      AND SE.TuitionCategoryId IS NULL
                                                                                      AND CF3.TuitionCategoryId IS NULL
                                                                                    )
                                                                                    OR (
                                                                                         CF3.RateScheduleId IS NULL
                                                                                         AND SE.TuitionCategoryId = CF3.TuitionCategoryId
                                                                                       )
                                                                                  )
                                                                                  OR (
                                                                                       CF3.RateScheduleId IS NOT NULL
                                                                                       AND EXISTS ( SELECT  rsd.TuitionCategoryId
                                                                                                    FROM    saRateScheduleDetails rsd
                                                                                                    WHERE   rsd.RateScheduleId = CF3.RateScheduleId
                                                                                                            AND rsd.TuitionCategoryId = SE.TuitionCategoryId )
                                                                                     )
                                                                                  OR (
                                                                                       CF3.RateScheduleId IS NOT NULL
                                                                                       AND SE.TuitionCategoryId IS NULL
                                                                                       AND EXISTS ( SELECT  rsd.*
                                                                                                    FROM    saRateScheduleDetails rsd
                                                                                                    WHERE   rsd.RateScheduleId = CF3.RateScheduleId
                                                                                                            AND rsd.TuitionCategoryId IS NULL )
                                                                                     )
                                                                                )
                                                                ) )
                        AND CST.ClsSectionId = CS.ClsSectionId
                        AND CST.TermId = @TermId
                        AND SE.StatusCodeId = SC.StatusCodeId
                        AND SC.SysStatusId IN ( 7,9,13,20 )
                        AND (
                              (
                                (
                                  CF.RateScheduleId IS NULL
                                  AND SE.TuitionCategoryId IS NULL
                                  AND CF.TuitionCategoryId IS NULL
                                )
                                OR (
                                     CF.RateScheduleId IS NULL
                                     AND SE.TuitionCategoryId = CF.TuitionCategoryId
                                   )
                              )
                              OR (
                                   CF.RateScheduleId IS NOT NULL
                                   AND EXISTS ( SELECT  rsd.TuitionCategoryId
                                                FROM    saRateScheduleDetails rsd
                                                WHERE   rsd.RateScheduleId = CF.RateScheduleId
                                                        AND rsd.TuitionCategoryId = SE.TuitionCategoryId )
                                 )
                              OR (
                                   CF.RateScheduleId IS NOT NULL
                                   AND SE.TuitionCategoryId IS NULL
                                   AND EXISTS ( SELECT  rsd.*
                                                FROM    saRateScheduleDetails rsd
                                                WHERE   rsd.RateScheduleId = CF.RateScheduleId
                                                        AND rsd.TuitionCategoryId IS NULL )
                                 )
                            )
            );
         
  
        OPEN c1; 
  
        FETCH NEXT FROM c1 
        INTO @StuEnrollID,@CourseFeeID,@TransCodeID,@TransCodeDescrip,@TransDescrip,@TransAmount;
  
        WHILE @@FETCH_STATUS = 0
            BEGIN 
  
                IF NOT @TransAmount IS NULL
                    BEGIN


                        INSERT  INTO dbo.saTransactions
                                (
                                 TransactionId
                                ,StuEnrollId
                                ,TermId
                                ,CampusId
                                ,TransDate
                                ,TransCodeId
                                ,TransReference
                                ,AcademicYearId
                                ,TransDescrip
                                ,TransAmount
                                ,TransTypeId
                                ,IsPosted
                                ,CreateDate
                                ,BatchPaymentId
                                ,ViewOrder
                                ,IsAutomatic
                                ,ModUser
                                ,ModDate
                                ,Voided
                                ,FeeLevelId
                                ,FeeId
                                ,PaymentCodeId
                                ,FundSourceId
                                ,StuEnrollPayPeriodId
                                )
                        VALUES  (
                                 NEWID()
                                , -- TransactionId - uniqueidentifier
                                 @StuEnrollID
                                , -- StuEnrollId - uniqueidentifier
                                 @TermId
                                , -- TermId - uniqueidentifier
                                 @CampusID
                                , -- CampusId - uniqueidentifier
                                 @PostFeesDate
                                , -- TransDate - datetime
                                 @TransCodeID
                                , -- TransCodeId - uniqueidentifier
                                 @TransDescrip
                                , -- TransReference - varchar(50)
                                 NULL
                                , -- AcademicYearId - uniqueidentifier
                                 @TransCodeDescrip
                                , -- TransDescrip - varchar(50)
                                 ISNULL(@TransAmount,0)
                                , -- TransAmount - decimal
                                 0
                                , -- TransTypeId - int
                                 1
                                , -- IsPosted - bit
                                 GETDATE()
                                , -- CreateDate - datetime
                                 NULL
                                , -- BatchPaymentId - uniqueidentifier
                                 0
                                , -- ViewOrder - int
                                 1
                                , -- IsAutomatic - bit
                                 @User
                                , -- ModUser - varchar(50)
                                 GETDATE()
                                ,-- ModDate - datetime
                                 0
                                , -- Voided - bit
                                 3
                                , -- FeeLevelId - tinyint
                                 @CourseFeeID
                                , -- FeeId - uniqueidentifier
                                 NULL
                                , -- PaymentCodeId - uniqueidentifier
                                 NULL
                                ,NULL  -- FundSourceId - uniqueidentifier
                     
                                );
  
                        IF @@ERROR <> 0
                            BEGIN
    -- Rollback the transaction
                                ROLLBACK; 
                                SET @Result = 0;
                                RETURN 0;--"Error while inserting"
                            END;

                    END;
                FETCH NEXT FROM c1 
            INTO @StuEnrollID,@CourseFeeID,@TransCodeID,@TransCodeDescrip,@TransDescrip,@TransAmount;
            END; 
        CLOSE c1; 
        DEALLOCATE c1; 
 
        IF @@ERROR = 0
            BEGIN
                COMMIT TRANSACTION;
                SET @Result = 1;
                RETURN 1;
            END;
        ELSE
            BEGIN
					-- Rollback the transaction
                ROLLBACK TRANSACTION;
                SET @Result = 0;	
                RETURN 0;--"Error while inserting"	
            END;
	  
    END;







GO
