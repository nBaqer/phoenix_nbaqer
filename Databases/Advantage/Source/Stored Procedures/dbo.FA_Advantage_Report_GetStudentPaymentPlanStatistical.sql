SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[FA_Advantage_Report_GetStudentPaymentPlanStatistical]
	-- Add the parameters for the stored procedure here
    @paymentPlanId UNIQUEIDENTIFIER = '5E47400D-7C7E-47EF-B405-0041054F81B1'
   ,@voided BIT = 0
AS
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;
        DECLARE @NumberOfPayments INT;
        SELECT  @NumberOfPayments = COUNT(st.PaymentPlanId)
        FROM    faStuPaymentPlanSchedule st
        WHERE   PaymentPlanId = @paymentPlanId;
-- Calculate the balance.   
        DECLARE @balance DECIMAL(18,4);
        SELECT  @balance = SUM(Balance)
        FROM    (
                  SELECT    ( PPS.Amount + COALESCE((
                                                      SELECT    SUM(PDR.Amount) * -1
                                                      FROM      saPmtDisbRel PDR
                                                      INNER JOIN saTransactions T ON PayPlanScheduleId = PPS.PayPlanScheduleId
                                                                                     AND PDR.TransactionId = T.TransactionId
                                                      WHERE     T.Voided = @voided
                                                    ),0) ) AS Balance
                  FROM      faStuPaymentPlanSchedule PPS
                  WHERE     PPS.PaymentPlanId = @paymentPlanId
                ) T1;
									  
        SELECT  @NumberOfPayments AS NumberOfPayments
               ,@balance AS Balance;									  
 
    END;




GO
