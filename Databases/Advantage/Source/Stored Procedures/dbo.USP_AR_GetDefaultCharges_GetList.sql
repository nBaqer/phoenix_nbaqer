SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[USP_AR_GetDefaultCharges_GetList]
    (
     @PrgVerID UNIQUEIDENTIFIER
    )
AS /*----------------------------------------------------------------------------------------------------
	Author          :	Saraswathi Lakshmanan
    
    Create date		:	05/05/2010
    
	Procedure Name	:	[USP_AR_GetDefaultCharges_GetList]

	Objective		:	Get the default charge Periods
	
	Parameters		:	Name			Type	Data Type			Required? 	
						=====			====	=========			=========	
						@PrgVerID		In		UniqueIDENTIFIER			Required
	
	Output			:	Returns default charge periods for a given ProgVerID		
						
*/-----------------------------------------------------------------------------------------------------

    BEGIN

        SELECT  A.SysTransCodeID
               ,A.FeeLevelID
               ,B.Description AS TransDescription
               ,C.Description AS feeDescription
               ,A.PeriodCanChange
               ,A.PrgVerID
               ,A.PrgVerPmtId
               ,0 AS CmdType
        FROM    saPrgVerDefaultChargePeriods A
               ,saSysTransCodes B
               ,saFeeLevels C
        WHERE   A.SysTransCodeID = B.SysTransCodeId
                AND A.FeeLevelID = C.FeeLevelId
                AND A.PrgVerID = @PrgVerID;
    END;




GO
