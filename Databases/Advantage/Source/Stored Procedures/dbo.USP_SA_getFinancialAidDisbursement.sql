SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_SA_getFinancialAidDisbursement]
(
    @CampusId AS UNIQUEIDENTIFIER,
    @StartDate AS DATETIME,
    @EndDate AS DATETIME,
    @TransTypes AS UNIQUEIDENTIFIER
)
AS
SELECT 
       enrollments.StudentId,
       students.StudentNumber AS StudentIdentifier,
       students.LastName + ', ' + students.FirstName + ISNULL(' ' + students.MiddleName, '') AS StudentName,
       CASE transactions.TransTypeId
           WHEN 2 THEN
               transactions.PaymentCodeId
           ELSE
               transactions.TransCodeId
       END AS TC,
       CASE transactions.TransTypeId
           WHEN 2 THEN
               paymentCodes.TransCodeDescrip
           ELSE
               transationCodes.TransCodeDescrip
       END AS TransCodeDescrip,
       transactions.TransDescrip,
       transactions.TransDate,
       transactions.TransAmount * -1 AS TransAmount,
       transactions.TransReference,
       transactionTypes.Description AS TransTypeDescrip,
       CASE payments.PaymentTypeId
           WHEN 2 THEN
               'Check Number: ' + payments.CheckNumber
           WHEN 3 THEN
               'C/C Authorization: ' + payments.CheckNumber
           WHEN 4 THEN
               'EFT Number: ' + payments.CheckNumber
           WHEN 5 THEN
               'Money Order Number: ' + payments.CheckNumber
           WHEN 6 THEN
               'Non Cash Reference #: ' + payments.CheckNumber
           ELSE
               payments.CheckNumber
       END AS CheckNumber,
       payments.PaymentTypeId,
       programVeresion.PrgVerDescrip,
       academicYears.AcademicYearDescrip,
       terms.TermDescrip,
       transactions.Voided,
       transactions.CampusId,
       transactions.ModUser,
       transactions.ModDate
FROM dbo.saTransactions AS transactions
    INNER JOIN dbo.arStuEnrollments enrollments
        ON enrollments.StuEnrollId = transactions.StuEnrollId
    INNER JOIN dbo.adLeads students
        ON students.LeadId = enrollments.LeadId
    INNER JOIN dbo.arPrgVersions programVeresion
        ON programVeresion.PrgVerId = enrollments.PrgVerId
    INNER JOIN dbo.saTransTypes transactionTypes
        ON transactionTypes.TransTypeId = transactions.TransTypeId
    LEFT JOIN saAcademicYears academicYears
        ON academicYears.AcademicYearId = transactions.AcademicYearId
    LEFT JOIN dbo.arTerm terms
        ON terms.TermId = transactions.TermId
    LEFT JOIN dbo.saTransCodes transationCodes
        ON transationCodes.TransCodeId = transactions.TransCodeId
    LEFT JOIN dbo.saTransCodes paymentCodes
        ON paymentCodes.TransCodeId = transactions.PaymentCodeId
    LEFT JOIN saPayments payments
        ON payments.TransactionId = transactions.TransactionId
    LEFT JOIN saAppliedPayments appliedPayments
        ON appliedPayments.TransactionId = transactions.TransactionId
WHERE (
          transactions.TransDate >= @StartDate AND 
          transactions.TransDate <= @EndDate
      )
      AND transactions.CampusId = @CampusId
      AND transactions.IsPosted = 1
      AND transactions.Voided = 0
      AND transationCodes.TransCodeId = @TransTypes

ORDER BY transactions.TransDescrip,
         students.LastName,
         transactions.TransDate;
GO
