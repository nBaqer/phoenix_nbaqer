SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--usp_GetExtershipHours '34CBB976-E737-4B37-860F-3841BBA71E6D' ,'80bf3639-a17d-4434-9bc4-66c9347b5db4'
-----------------------------------------------------------------------------------------------
--DE9852 QA: Unable to transfer a externship course that has been completed
--End Added by Theresa G on 7/16/13
-----------------------------------------------------------------------------------------------             
-----------------------------------------------------------------------------------------------
--DE9870 Transcript page should display transferred courses that are Graded
--Start  Added by Balaji G on 7/19/13
-----------------------------------------------------------------------------------------------    
CREATE PROCEDURE [dbo].[USP_IsCourseCompleted]
    @ResultId UNIQUEIDENTIFIER
AS
    BEGIN
        DECLARE @IsCourseComplete INT;
        IF EXISTS ( SELECT  *
                    FROM    arResults
                    WHERE   ResultId = @ResultId )
            BEGIN
                SET @IsCourseComplete = (
                                          SELECT    COUNT(*) AS RowCounter
                                          FROM      arResults
                                          WHERE     ResultId = @ResultId
                                                    AND IsCourseCompleted = 1
                                        );
            END;
        ELSE
            BEGIN
                SET @IsCourseComplete = (
                                          SELECT    COUNT(*) AS RowCounter
                                          FROM      arTransferGrades
                                          WHERE     TransferId = @ResultId
                                                    AND IsCourseCompleted = 1
                                        );
            END;
        SELECT  @IsCourseComplete;
    END;



GO
