SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_GetAllActiveAndInActiveCourses]
    (
     @campusId VARCHAR(50)
    ,@TermId VARCHAR(50)
    )
AS
    SET NOCOUNT ON;
    SELECT  T.ReqId
           ,'(' + T.Code + ') ' + T.Descrip AS Descrip
           ,( CASE S.Status
                WHEN 'Active' THEN 1
                ELSE 0
              END ) AS STATUS
           ,T.Descrip AS ShortDescr
           ,T.Code
    FROM    arReqs T
           ,syStatuses S
    WHERE   T.StatusId = S.StatusId
            AND T.CampGrpId IN ( SELECT CampGrpId
                                 FROM   syCmpGrpCmps
                                 WHERE  CampusId = @campusId )
            AND T.ReqTypeId = 1
    ORDER BY T.Descrip
           ,T.Code; 


    SELECT  StartDate
           ,EndDate
    FROM    arTerm
    WHERE   TermId = @TermId;
    
    
    
/****** Object:  StoredProcedure [dbo].[usp_GetAllActiveCourses]    Script Date: 02/11/2013 13:58:33 ******/
    SET ANSI_NULLS ON;



GO
