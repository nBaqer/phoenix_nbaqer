SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--DE9785 fixed
CREATE PROCEDURE [dbo].[USP_AR_RegStd_IsPassingGrade]
    @Grade VARCHAR(50)
   ,@PrgVerId UNIQUEIDENTIFIER
AS --select IsPass from arGradeSystemDetails where Grade=@Grade
    SELECT TOP 1
            IsPass
    FROM    arGradeSystemDetails A
    INNER JOIN arPrgVersions B ON A.GrdSystemId = B.GrdSystemId
    WHERE   Grade IN ( @Grade )
            AND PrgVerId = @PrgVerId
    ORDER BY ispass DESC;



GO
