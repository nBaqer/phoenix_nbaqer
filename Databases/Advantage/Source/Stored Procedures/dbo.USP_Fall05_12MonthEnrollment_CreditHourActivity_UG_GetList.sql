SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--sp_helptext USP_Fall05_12MonthEnrollment_ClockHourActivity_GetList
--sp_helptext USP_Fall05_12MonthEnrollment_CreditHourActivity_UG_GetList
--sp_helptext USP_Fall05_12MonthEnrollment_CreditHourActivity_Grad_GetList
--exec USP_Fall05_12MonthEnrollment_CreditHourActivity_UG_GetList '1BB47DF1-E6F9-4467-A5D6-8FA5495BB571',null,'09/01/2009','08/31/2010','LastName'
CREATE PROCEDURE [dbo].[USP_Fall05_12MonthEnrollment_CreditHourActivity_UG_GetList]
    @CampusId VARCHAR(50)
   ,@ProgId VARCHAR(4000) = NULL
   ,@StartDate DATETIME = NULL
   ,@EndDate DATETIME = NULL
   ,@OrderBy VARCHAR(100)
AS
    SELECT  *
    INTO    #tempclock
    FROM    (
              SELECT    StuEnrollId
                       ,dbo.UDF_FormatSSN(SSN) AS SSN
                       ,StudentNumber
                       ,StudentName
                       ,ProgCode
                       ,ProgramName
                       ,ProgLength
                       ,CONVERT(VARCHAR,StartDate,101) AS StartDate
                       ,CreditHour
                       ,PrgVerCode
                       ,PrgVerDescrip
                       ,PrgVerId
                       ,EnrollmentId
              FROM      (
                          SELECT    t2.StuEnrollId
                                   ,t1.SSN
                                   ,t1.StudentNumber
                                   ,t2.EnrollmentId
                                   ,t1.LastName + ', ' + t1.FirstName + ' ' + ISNULL(t1.MiddleName,'') AS StudentName
                                   ,t8.ProgCode
                                   ,t8.ProgDescrip AS ProgramName
                                   ,t7.Hours AS ProgLength
                                   ,t2.StartDate
                                   ,
				-- Balaji's comments
				-- To be considered for credits, 
				-- 1. The course should have been successfully completed
				-- 2. The class should have started before the start date (09/01/cohortyear)
				-- and should have ended in the date range (between 09/01/cohortyear and 08/31/nextcohortyear)
				--				Or 
				-- 3. The course should have started and ended in the date range
				-- For getting credits earned use syCreditSummary Table.This table already takes care of all
				-- Credit Logic.
                                    (
                                      SELECT    SUM(CS.CreditsAttempted)
                                      FROM      syCreditSummary CS
                                               ,arClassSections C
                                      WHERE     CS.ClsSectionId = C.ClsSectionId
                                                AND CS.StuEnrollId = t2.StuEnrollId
                                                AND (
                                                      (
                                                        C.StartDate <= @StartDate
                                                        AND (
                                                              C.EndDate >= @StartDate
                                                              AND C.EndDate <= @EndDate
                                                            )
                                                      )
                                                      OR (
                                                           C.StartDate >= @StartDate
                                                           AND C.EndDate <= @EndDate
                                                         )
                                                    ) -- Started before date range and ended during the date range
				--and CS.Completed=1
                                    ) AS CreditHour
                                   ,t7.PrgVerId
                                   ,t7.PrgVerCode
                                   ,t7.PrgVerDescrip   --As per spec get only completed courses
                          FROM      arStudent t1
                          INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                          INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                          INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                                       AND t6.SysStatusId NOT IN ( 8 )
                          INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                          INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                          INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                          LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                          WHERE     t2.CampusId = @CampusId
                                    AND (
                                          @ProgId IS NULL
                                          OR t8.ProgId IN ( SELECT  Val
                                                            FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                        )
                                    AND LOWER(t9.IPEDSValue) = 58
                                    AND (
                                          LOWER(t10.IPEDSValue) = 61
                                          OR LOWER(t10.IPEDSValue) = 62
                                        )
                                    AND t8.ACID <> 5 -- Non Clock Hour 
                                    AND t2.StartDate <= @EndDate -- Check if Student started before the report end date
                                    AND StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
						( SELECT    t1.StuEnrollId
                          FROM      arStuEnrollments t1
                                   ,SyStatusCodes t2
                          WHERE     t1.StatusCodeId = t2.StatusCodeId
                                    AND StartDate <= @EndDate
                                    AND -- Student started before the end date range
                                    LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                    AND (
                                          @ProgId IS NULL
                                          OR t8.ProgId IN ( SELECT  Val
                                                            FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                        )
                                    AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before Report Start Date
                                    AND (
                                          t1.DateDetermined < @StartDate
                                          OR ExpGradDate < @StartDate
                                          OR LDA < @StartDate
                                        ) ) 
						-- If Student is enrolled in only one program version and if that program version 
						-- happens to be a continuing ed program exclude the student
                                    AND t2.StudentId NOT IN ( SELECT DISTINCT
                                                                        StudentId
                                                              FROM      (
                                                                          SELECT    StudentId
                                                                                   ,COUNT(*) AS RowCounter
                                                                          FROM      arStuENrollments
                                                                          WHERE     PrgVerId IN ( SELECT    PrgVerId
                                                                                                  FROM      arPrgVersions
                                                                                                  WHERE     IsContinuingEd = 1 )
                                                                          GROUP BY  StudentId
                                                                          HAVING    COUNT(*) = 1
                                                                        ) dtStudent_ContinuingEd )
						-- If student is enrolled in program versions that belong to same program
						-- then ignore the second enrollment

						-- If student is only enrolled in one program show the program
                                    AND ( t2.StuEnrollId IN ( SELECT TOP 1
                                                                        StuEnrollId
                                                              FROM      arStuEnrollments
                                                              WHERE     StudentId IN ( SELECT DISTINCT
                                                                                                StudentId
                                                                                       FROM     (
                                                                                                  SELECT    StudentId
                                                                                                           ,A3.ProgId
                                                                                                           ,COUNT(*) AS RowCounter
                                                                                                  FROM      arStuENrollments A1
                                                                                                           ,arPrgVersions A2
                                                                                                           ,arPrograms A3
                                                                                                  WHERE     A1.PrgVerId = A2.PrgVerId
                                                                                                            AND A2.ProgId = A3.ProgId
                                                                                                            AND A1.StudentId = t1.StudentId
                                                                                                  GROUP BY  StudentId
                                                                                                           ,A3.ProgId
                                                                                                  HAVING    COUNT(*) > 1
                                                                                                ) dtStudentInMultiplePrograms )
                                                              ORDER BY  StartDate
                                                                       ,EnrollDate ASC
                                                              UNION
                                                              SELECT    StuEnrollId
                                                              FROM      arStuEnrollments
                                                              WHERE     StudentId IN ( SELECT DISTINCT
                                                                                                StudentId
                                                                                       FROM     (
                                                                                                  SELECT    StudentId
                                                                                                           ,A3.ProgId
                                                                                                           ,COUNT(*) AS RowCounter
                                                                                                  FROM      arStuENrollments A1
                                                                                                           ,arPrgVersions A2
                                                                                                           ,arPrograms A3
                                                                                                  WHERE     A1.PrgVerId = A2.PrgVerId
                                                                                                            AND A2.ProgId = A3.ProgId
                                                                                                            AND A1.StudentId = t1.StudentId
                                                                                                  GROUP BY  StudentId
                                                                                                           ,A3.ProgId
                                                                                                  HAVING    COUNT(*) = 1
                                                                                                ) dtStudentInMultiplePrograms ) ) )
                        ) dt
            ) dt1;
    DECLARE @GrandTotal DECIMAL(18,2)
       ,@PrgVerTotal DECIMAL(18,2);
    SET @GrandTotal = (
                        SELECT  SUM(ISNULL(CreditHour,0)) AS TotalContactHours
                        FROM    #TempClock
                      );
    SELECT  t1.*
           ,(
              SELECT    SUM(ISNULL(CreditHour,0))
              FROM      #TempClock
              WHERE     PrgVerId = t1.PrgVerId
            ) AS PrgVersion_SubTotal
           ,(
              SELECT    COUNT(StudentName)
              FROM      #TempClock
              WHERE     PrgVerId = t1.PrgVerId
            ) AS StudentCount_SubTotal
           ,@GrandTotal AS GrandTotal
    FROM    #TempClock t1
--where StudentName like '%Kristen%'
ORDER BY    ProgCode
           ,ProgramName
           ,PrgVerCode
           ,PrgVerDescrip
           ,CASE WHEN @OrderBy = 'SSN' THEN SSN
            END
           ,CASE WHEN @OrderBy = 'LastName' THEN StudentName
            END
           ,CASE WHEN @OrderBy = 'Student Number' THEN StudentNumber
            END
           ,CASE WHEN @OrderBy = 'EnrollmentId' THEN EnrollmentId
            END;
    DROP TABLE #TempClock;



GO
