SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =============================================
-- Author:		Jose Torres
-- Create date: 11/25/2014
-- Description: Get Charges Auto Posted to Student Ledger between dates
--
-- EXECUTE USP_ChargesAutoPostedToStudentLedger    @StartDate = '2014-11-01', @EndDate = '2014-11-31'
--   default to today's date.              
-- =============================================

CREATE PROCEDURE [dbo].[USP_ChargesAutoPostedToStudentLedger]
    @StartDate DATETIME = NULL
   ,@EndDate DATETIME = NULL
AS
    BEGIN
	-- if default date is null change it for TodayDate
        IF @StartDate = NULL
            BEGIN
                SET @StartDate = ISNULL(@StartDate,GETDATE());
            END;    
        IF @EndDate = NULL
            BEGIN
                SET @EndDate = ISNULL(@EndDate,GETDATE());
            END; 
	 -- Make StartDate as time 00:00:00
	 -- Meke EndDate as time nextDate 00:00:00           
        SET @StartDate = DATEADD(dd,0,DATEDIFF(dd,0,@StartDate));
        SET @EndDate = DATEADD(dd,1,DATEDIFF(dd,0,@EndDate));

        SELECT  AST.LastName
               ,AST.FirstName
               ,AST.StudentNumber
               ,LTRIM(RTRIM(REPLACE(ST.TransReference,'Payment Period ',''))) AS Period
               ,ST.TransAmount
               ,ST.TransDate AS DatePosted
        FROM    dbo.saTransactions AS ST
        INNER JOIN dbo.arStuEnrollments AS ASE ON ST.StuEnrollId = ASE.StuEnrollId
        INNER JOIN dbo.arStudent AS AST ON ASE.StudentId = AST.StudentId
        WHERE   ST.TransDate BETWEEN @StartDate AND @EndDate
                AND ST.PmtPeriodId IS NOT NULL
        ORDER BY AST.LastName
               ,AST.FirstName;
    END;




GO
