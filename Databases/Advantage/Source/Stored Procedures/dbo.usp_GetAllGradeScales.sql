SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_GetAllGradeScales]
    @showActiveOnly BIT
   ,@campusId VARCHAR(50)
AS
    SET NOCOUNT ON;
    DECLARE @sql VARCHAR(8000);


    IF @showActiveOnly = 1
        BEGIN
            EXEC usp_GetAllActiveGradeScales @campusId; 
        END;

    ELSE
        BEGIN
            EXEC usp_GetAllActiveAndInActiveGradeScales @campusId;
        END;




GO
