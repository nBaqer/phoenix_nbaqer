SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

--=================================================================================================
-- USP_GetAttendancePercentageDTForReport
--=================================================================================================
CREATE PROCEDURE [dbo].[USP_GetAttendancePercentageDTForReport]
    (
     @stuEnrollId UNIQUEIDENTIFIER
    ,@cutOffDate DATETIME  
    )
AS
    BEGIN
        SET NOCOUNT ON;  
  
  
        SET DATEFIRST 7;    
  
        SELECT    DISTINCT
                CSA.ClsSectionId
               ,NULL AS startTime
               ,NULL AS endtime
               ,CSA.MeetDate
               ,CSA.Actual
               ,CSA.Tardy
               ,CSA.Excused
               ,(
                  SELECT    t3.UnitTypeDescrip
                  FROM      arClassSections t1
                           ,arReqs t2
                           ,arAttUnitType t3
                  WHERE     t1.ClsSectionId = CSA.ClsSectionId
                            AND t1.ReqId = t2.ReqId
                            AND t2.UnitTypeId = t3.UnitTypeId
                ) AS AttendanceType
               ,DATEDIFF(MI,(
                              SELECT    T1.TimeIntervalDescrip
                              FROM      cmTimeInterval T1
                                       ,cmTimeInterval T2
                              WHERE     T1.TimeIntervalId = P.StartTimeId
                                        AND T2.TimeIntervalId = P.EndTimeId
                                        AND LEFT(DATENAME(dw,CSA.MeetDate),3) = WD.WorkDaysDescrip
                                        AND CSA.ClsSectionId = CSM.ClsSectionId
                            ),(
                                SELECT  T2.TimeIntervalDescrip
                                FROM    cmTimeInterval T1
                                       ,cmTimeInterval T2
                                WHERE   T1.TimeIntervalId = P.StartTimeId
                                        AND T2.TimeIntervalId = P.EndTimeId
                                        AND LEFT(DATENAME(dw,CSA.MeetDate),3) = WD.WorkDaysDescrip
                                        AND CSA.ClsSectionId = CSM.ClsSectionId
                              )) AS duration
               ,CSA.ClsSectMeetingId
               ,CSA.Scheduled
               ,CSM.InstructionTypeID
        FROM    atClsSectAttendance CSA
               ,arClsSectMeetings CSM
               ,syPeriods P
               ,arClassSections a
               ,syPeriodsWorkDays PWD
               ,plWorkDays WD
        WHERE   CSM.PeriodId = P.PeriodId
                AND a.ClsSectionId = CSA.ClsSectionId
                AND P.PeriodId = PWD.PeriodId
                AND PWD.WorkDayId = WD.WorkDaysId
                AND CSA.ClsSectionId = CSM.ClsSectionId
                AND CSA.Actual <> 999
                AND StuEnrollId = @stuEnrollId
                AND CSA.MeetDate <= @cutOffDate
                AND DATEDIFF(MI,(
                                  SELECT    T1.TimeIntervalDescrip
                                  FROM      cmTimeInterval T1
                                           ,cmTimeInterval T2
                                  WHERE     T1.TimeIntervalId = P.StartTimeId
                                            AND T2.TimeIntervalId = P.EndTimeId
                                            AND LEFT(DATENAME(dw,CSA.MeetDate),3) = WD.WorkDaysDescrip
                                            AND CSA.ClsSectionId = CSM.ClsSectionId
                                ),(
                                    SELECT  T2.TimeIntervalDescrip
                                    FROM    cmTimeInterval T1
                                           ,cmTimeInterval T2
                                    WHERE   T1.TimeIntervalId = P.StartTimeId
                                            AND T2.TimeIntervalId = P.EndTimeId
                                            AND LEFT(DATENAME(dw,CSA.MeetDate),3) = WD.WorkDaysDescrip
                                            AND CSA.ClsSectionId = CSM.ClsSectionId
                                  )) IS NOT NULL
                AND CONVERT(DATE,CSA.MeetDate,111) >= CONVERT(DATE,CSM.StartDate,111)
                AND CONVERT(DATE,CSA.MeetDate,111) <= CONVERT(DATE,CSM.EndDate,111)
                AND a.StartDate = (
                                    SELECT  MIN(StartDate)
                                    FROM    arClassSections b
                                           ,atClsSectAttendance bt
                                    WHERE   ReqId = a.ReqId
                                            AND StuEnrollId = @stuEnrollId
                                            AND b.ClsSectionId = bt.ClsSectionId
                                  )
                AND (
                      SELECT    DATEPART(hh,TimeIntervalDescrip)
                      FROM      syPeriods
                               ,cmTimeInterval
                      WHERE     syPeriods.StartTimeId = cmTimeInterval.TimeIntervalId
                                AND PeriodId = CSM.PeriodId
                    ) = DATEPART(hh,MeetDate)
                AND (
                      SELECT    DATEPART(Mi,TimeIntervalDescrip)
                      FROM      syPeriods
                               ,cmTimeInterval
                      WHERE     syPeriods.StartTimeId = cmTimeInterval.TimeIntervalId
                                AND PeriodId = CSM.PeriodId
                    ) = DATEPART(mi,MeetDate)
        ORDER BY CSA.ClsSectionId
               ,CSA.MeetDate;   
              
 --exec usp_GetAttendancePercentageDTForReport 'a3f2743d-ea5e-4c9c-97d9-8ee8e909e6d8','11/6/2009'  

    END;
--=================================================================================================
-- END  --  USP_GetAttendancePercentageDTForReport
--=================================================================================================

GO
