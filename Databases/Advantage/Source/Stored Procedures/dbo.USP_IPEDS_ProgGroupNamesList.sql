SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_IPEDS_ProgGroupNamesList]
    @AreasOfInterest AS VARCHAR(8000)
AS /*----------------------------------------------------------------------------------------------------
	Author          :	Vijay Ramteke
    Create date		:	06/21/2010
	Procedure Name	:	USP_IPEDS_ProgGroupNamesList
	Objective		:	
	Parameters		:	Name			Type	Data Type			Required? 	
						=====			====	=========			=========	
	Output			:					
*/-----------------------------------------------------------------------------------------------------
    BEGIN
        SELECT  PrgGrpDescrip
        FROM    arPrgGrp
        WHERE   PrgGrpId IN ( SELECT    strval
                              FROM      dbo.SPLIT(@AreasOfInterest) );
    END;



GO
