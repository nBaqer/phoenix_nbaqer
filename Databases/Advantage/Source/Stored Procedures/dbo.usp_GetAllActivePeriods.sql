SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_GetAllActivePeriods] @campusId VARCHAR(50)
AS
    SET NOCOUNT ON;
    SELECT  P.PeriodId
           ,P.PeriodDescrip
           ,(
              SELECT    TimeIntervalDescrip
              FROM      dbo.cmTimeInterval
              WHERE     TimeIntervalId = StartTimeID
            ) StartTime
           ,(
              SELECT    TimeIntervalDescrip
              FROM      dbo.cmTimeInterval
              WHERE     TimeIntervalId = EndTimeId
            ) EndTime
           ,1 AS Status
    FROM    syPeriods P
           ,syStatuses S
    WHERE   P.StatusId = S.StatusId
            AND P.CampGrpId IN ( SELECT CGC.CampGrpId
                                 FROM   syCmpGrpCmps CGC
                                       ,syCampGrps CG
                                 WHERE  CGC.CampusId = @campusId
                                        AND CGC.CampGrpId = CG.CampGrpId )
            AND S.STATUS = 'Active'
    ORDER BY P.PeriodDescrip; 





GO
