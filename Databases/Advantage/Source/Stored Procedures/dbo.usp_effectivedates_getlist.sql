SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_effectivedates_getlist]
    @reqid UNIQUEIDENTIFIER
AS -- A rule may be set for many effective dates and our goal
-- is to pick ONLY ONE effective date that falls on or before the
-- class start date.

-- The min, max and mentor proctored values are for prerequisite course.
-- If Course A is a prereq for Course B, the min and max values are specific 
-- to Course A and not Course B, thats the logic behind the self join
-- on table arPrerequisites_GradeComponentTypes_Courses.

-- Sort by DateDiff(day,t1.EffectiveDate,t2.StartDate) eliminates the 
-- need for grouping or any other logic to get the effective date that falls 
-- before class start date
    SELECT TOP 1
            t1.EffectiveDate
           ,t2.StartDate AS ClassStartDate
           ,DATEDIFF(DAY,t1.EffectiveDate,t2.StartDate) AS DaysSinceClassStarted
           ,
--			t3.MinPerCategory,t3.MaxPerCategory,
--			t3.MinPerCombination,t3.MaxPerCombination,
            (
              SELECT    MinperCategory
              FROM      arPrerequisites_GradeComponentTypes_Courses
              WHERE     PreReqId = t1.PreReqId
                        AND ReqId = t1.ReqId
                        AND EffectiveDate = t1.EffectiveDate
            ) AS MinPerCategory
           ,(
              SELECT    MaxperCategory
              FROM      arPrerequisites_GradeComponentTypes_Courses
              WHERE     PreReqId = t1.PreReqId
                        AND ReqId = t1.ReqId
                        AND EffectiveDate = t1.EffectiveDate
            ) AS MaxPerCategory
           ,(
              SELECT    MinperCombination
              FROM      arPrerequisites_GradeComponentTypes_Courses
              WHERE     PreReqId = t1.PreReqId
                        AND ReqId = t1.ReqId
                        AND EffectiveDate = t1.EffectiveDate
            ) AS MinPerCombination
           ,(
              SELECT    MaxperCombination
              FROM      arPrerequisites_GradeComponentTypes_Courses
              WHERE     PreReqId = t1.PreReqId
                        AND ReqId = t1.ReqId
                        AND EffectiveDate = t1.EffectiveDate
            ) AS MaxPerCombination
           ,t1.mentorproctored
           ,t1.PreReqId
           ,t1.ReqId
    FROM    arPrerequisites_GradeComponentTypes_Courses t1
           ,arClassSections t2
    WHERE   t1.ReqId = t2.ReqId
            AND t1.ReqId = @reqid
            AND DATEDIFF(DAY,t1.EffectiveDate,t2.StartDate) >= 0
    ORDER BY DATEDIFF(DAY,t1.EffectiveDate,t2.StartDate) ASC;



GO
