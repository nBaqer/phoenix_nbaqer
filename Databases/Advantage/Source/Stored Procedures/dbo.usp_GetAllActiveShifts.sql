SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_GetAllActiveShifts]
AS
    SET NOCOUNT ON;
    SELECT  T.Shiftid
           ,T.ShiftDescrip
           ,1 AS Status
    FROM    arShifts T
           ,syStatuses S
    WHERE   T.StatusId = S.StatusId
            AND S.Status = 'Active'
    ORDER BY T.ShiftDescrip; 




GO
