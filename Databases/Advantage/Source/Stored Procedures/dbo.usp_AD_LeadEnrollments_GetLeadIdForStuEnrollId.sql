SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =====================================================================================================================
-- Author: Troy
-- Create date: 3/31/2010
-- Description: Return the LeadId for a specified StuEnrollID
-- ====================================================================================================================
CREATE PROCEDURE [dbo].[usp_AD_LeadEnrollments_GetLeadIdForStuEnrollId]
    @StuEnrollID UNIQUEIDENTIFIER
AS
    BEGIN
    -- SET NOCOUNT ON added to prevent extra result sets from
    -- interfering with SELECT statements.
        SET NOCOUNT ON;

        SELECT  LeadId
        FROM    arStuEnrollments
        WHERE   StuEnrollId = @StuEnrollID;
    
    
    END;




GO
