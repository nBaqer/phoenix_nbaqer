SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_PostScores_Update]
    @EffectiveDate DATETIME
   ,@RuleValues NTEXT
AS
    DECLARE @hDoc INT;
    --Prepare input values as an XML document
    EXEC sp_xml_preparedocument @hDoc OUTPUT,@RuleValues;


    UPDATE  arPostScoreDictationSpeedTest
    SET     termid = XMLPostScores.termid
           ,datepassed = @EffectiveDate
           ,grdcomponenttypeid = XMLPostScores.grdcomponenttypeid
           ,accuracy = XMLPostScores.accuracy
           ,speed = XMLPostScores.speed
           ,GrdSysDetailId = XMLPostScores.GrdSysDetailId
           ,InstructorId = XMLPostScores.InstructorId
           ,ModUser = XMLPostScores.ModUser
           ,ModDate = GETDATE()
           ,istestmentorproctored = XMLPostScores.istestmentorproctored
    FROM    OPENXML(@hDoc,'/NewDataSet/PostScores') 
        WITH ( id UNIQUEIDENTIFIER,stuenrollid UNIQUEIDENTIFIER,termid UNIQUEIDENTIFIER,
                datepassed DATETIME,grdcomponenttypeid UNIQUEIDENTIFIER,
                accuracy DECIMAL,speed DECIMAL,GrdSysDetailId UNIQUEIDENTIFIER,
                InstructorId UNIQUEIDENTIFIER,ModUser VARCHAR(50),ModDate DATETIME,istestmentorproctored VARCHAR(10)) XMLPostScores
    WHERE   arPostScoreDictationSpeedTest.id = XMLPostScores.id;
    EXEC sp_xml_removedocument @hDoc;



GO
