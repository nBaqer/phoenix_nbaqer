SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_NumberofTimesStudentRegisteredInSHCourses_getcount]
    @stuenrollid UNIQUEIDENTIFIER
   ,@reqid UNIQUEIDENTIFIER
   ,@coursecode VARCHAR(50)
AS
    SELECT  COUNT(t1.Hours) AS NumberofTimesStudentRegisteredInSHCourses
    FROM    arResults t1
    INNER JOIN arClassSections t2 ON t1.TestId = t2.ClsSectionId
    INNER JOIN arReqs t3 ON t2.ReqId = t3.ReqId
    WHERE   t1.StuEnrollId = @stuenrollid
            AND t2.ReqId = @reqid
            AND t3.Code LIKE @coursecode + '%';



GO
