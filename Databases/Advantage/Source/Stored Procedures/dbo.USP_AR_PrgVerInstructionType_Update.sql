SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[USP_AR_PrgVerInstructionType_Update]
    (
     @PrgVerInstructionTypeId UNIQUEIDENTIFIER
    ,@PrgVerID UNIQUEIDENTIFIER
    ,@InstructionTypeId UNIQUEIDENTIFIER
    ,@Hours DECIMAL(6,0)
    ,@UserName VARCHAR(50)
    )
AS /*----------------------------------------------------------------------------------------------------
	Author          :	Janet Robinson
    
    Create date		:	08/11/2011
    
	Procedure Name	:	[USP_AR_PrgVerInstructionType_Update]

	Objective		:	Updates table arPrgVerInstructionType
	
	Parameters		:	Name						Type	Data Type				Required? 	
						=====						====	=========				=========	
						@PrgVerInstructionTypeId	In		UniqueIDENTIFIER		Required
						@PrgVerID					In		UniqueIDENTIFIER		Required						
						@InstructionTypeId			In		UniqueIDENTIFIER		Required
						@Hours						In		decimal					Required
						@UserName					In		varchar					Required
	Output			:			
						
*/-----------------------------------------------------------------------------------------------------

    BEGIN

        UPDATE  arPrgVerInstructionType
        SET     Hours = @Hours
               ,InstructionTypeId = @InstructionTypeId
               ,ModUser = @UserName
               ,ModDate = GETDATE()
        WHERE   PrgVerID = @PrgVerID
                AND PrgVerInstructionTypeId = @PrgVerInstructionTypeId;
		 
    END;




GO
