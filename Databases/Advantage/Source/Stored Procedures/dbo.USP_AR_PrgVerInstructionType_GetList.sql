SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[USP_AR_PrgVerInstructionType_GetList]
    (
     @PrgVerID UNIQUEIDENTIFIER
    )
AS /*----------------------------------------------------------------------------------------------------
	Author          :	Janet Robinson
    
    Create date		:	08/11/2011
    
	Procedure Name	:	[USP_AR_PrgVerInstructionType_GetList]

	Objective		:	Get the Program Version Instruction Types list
	
	Parameters		:	Name			Type	Data Type			Required? 	
						=====			====	=========			=========	
						@PrgVerID		In		UniqueIDENTIFIER	Required
	
	Output			:	Returns the Program Version Instruction Types for a given ProgVerID		
						
*/-----------------------------------------------------------------------------------------------------

    BEGIN

        SELECT  PrgVerInstructionTypeId
               ,PrgVerId
               ,InstructionTypeId
               ,(
                  SELECT    IT.InstructionTypeDescrip
                  FROM      arInstructionType IT
                  WHERE     IT.InstructionTypeId = PV.InstructionTypeId
                ) AS InstructionTypeDescrip
               ,Hours
               ,0 AS CmdType
        FROM    arPrgVerInstructionType PV
        WHERE   PrgVerID = @PrgVerID
        ORDER BY InstructionTypeDescrip;
    END;





GO
