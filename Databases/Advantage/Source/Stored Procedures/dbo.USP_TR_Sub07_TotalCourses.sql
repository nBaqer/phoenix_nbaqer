SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- =========================================================================================================  
-- USP_TR_Sub07_TotalCourses   
-- =========================================================================================================  
CREATE PROCEDURE [dbo].[USP_TR_Sub07_TotalCourses]
    @CoursesTakenTableName NVARCHAR(200)
   ,@GPASummaryTableName NVARCHAR(200)
   ,@StuEnrollIdList NVARCHAR(MAX)
   ,@TermId UNIQUEIDENTIFIER = NULL
   ,@ShowMultipleEnrollments BIT = 0
AS --  
    BEGIN

        -- 0) Declare varialbles and Set initial values  
        BEGIN
            --DECLARE @SQL01 AS NVARCHAR(MAX);  
            --DECLARE @SQL02 AS NVARCHAR(MAX);  

            IF EXISTS (
                      SELECT 1
                      FROM   tempdb.sys.objects AS O
                      WHERE  O.type IN ( 'U' )
                             AND O.object_id = OBJECT_ID(N'tempdb..#CoursesTaken')
                      )
                BEGIN
                    DROP TABLE #CoursesTaken;
                END;
            CREATE TABLE #CoursesTaken
                (
                    CoursesTakenId INTEGER NOT NULL PRIMARY KEY
                   ,StudentId UNIQUEIDENTIFIER
                   ,StuEnrollId UNIQUEIDENTIFIER
                   ,TermId UNIQUEIDENTIFIER
                   ,TermDescrip VARCHAR(100)
                   ,TermStartDate DATETIME
                   ,ReqId UNIQUEIDENTIFIER
                   ,ReqCode VARCHAR(100)
                   ,ReqDescription VARCHAR(100)
                   ,ReqCodeDescrip VARCHAR(100)
                   ,ReqCredits DECIMAL(18, 2)
                   ,MinVal DECIMAL(18, 2)
                   ,ClsSectionId VARCHAR(50)
                   ,CourseStarDate DATETIME
                   ,CourseEndDate DATETIME
                   ,SCS_CreditsAttempted DECIMAL(18, 2)
                   ,SCS_CreditsEarned DECIMAL(18, 2)
                   ,SCS_CurrentScore DECIMAL(18, 2)
                   ,SCS_CurrentGrade VARCHAR(10)
                   ,SCS_FinalScore DECIMAL(18, 2)
                   ,SCS_FinalGrade VARCHAR(10)
                   ,SCS_Completed BIT
                   ,SCS_FinalGPA DECIMAL(18, 2)
                   ,SCS_Product_WeightedAverage_Credits_GPA DECIMAL(18, 2)
                   ,SCS_Count_WeightedAverage_Credits DECIMAL(18, 2)
                   ,SCS_Product_SimpleAverage_Credits_GPA DECIMAL(18, 2)
                   ,SCS_Count_SimpleAverage_Credits DECIMAL(18, 2)
                   ,SCS_ModUser VARCHAR(50)
                   ,SCS_ModDate DATETIME
                   ,SCS_TermGPA_Simple DECIMAL(18, 2)
                   ,SCS_TermGPA_Weighted DECIMAL(18, 2)
                   ,SCS_coursecredits DECIMAL(18, 2)
                   ,SCS_CumulativeGPA DECIMAL(18, 2)
                   ,SCS_CumulativeGPA_Simple DECIMAL(18, 2)
                   ,SCS_FACreditsEarned DECIMAL(18, 2)
                   ,SCS_Average DECIMAL(18, 2)
                   ,SCS_CumAverage DECIMAL(18, 2)
                   ,ScheduleDays DECIMAL(18, 2)
                   ,ActualDay DECIMAL(18, 2)
                   ,FinalGPA_Calculations DECIMAL(18, 2)
                   ,FinalGPA_TermCalculations DECIMAL(18, 2)
                   ,CreditsEarned_Calculations DECIMAL(18, 2)
                   ,ActualDay_Calculations DECIMAL(18, 2)
                   ,GrdBkWgtDetailsCount INTEGER
                   ,CountMultipleEnrollment INTEGER
                   ,RowNumberMultipleEnrollment INTEGER
                   ,CountRepeated INTEGER
                   ,RowNumberRetaked INTEGER
                   ,SameTermCountRetaken INTEGER
                   ,RowNumberSameTermRetaked INTEGER
                   ,RowNumberMultEnrollNoRetaken INTEGER
                   ,RowNumberOverAllByClassSection INTEGER
                   ,IsCreditsEarned BIT
                );

            IF EXISTS (
                      SELECT 1
                      FROM   tempdb.sys.objects AS O
                      WHERE  O.type IN ( 'U' )
                             AND O.object_id = OBJECT_ID(N'tempdb..#GPASummary')
                      )
                BEGIN
                    DROP TABLE #GPASummary;
                END;
            CREATE TABLE #GPASummary
                (
                    GPASummaryId INTEGER NOT NULL PRIMARY KEY
                   ,StudentId UNIQUEIDENTIFIER
                   ,LastName VARCHAR(50)
                   ,FirstName VARCHAR(50)
                   ,MiddleName VARCHAR(50)
                   ,SSN VARCHAR(11)
                   ,StudentNumber VARCHAR(50)
                   ,StuEnrollId UNIQUEIDENTIFIER
                   ,EnrollmentID VARCHAR(50)
                   ,PrgVerId UNIQUEIDENTIFIER
                   ,PrgVerDescrip VARCHAR(50)
                   ,PrgVersionTrackCredits BIT
                   ,GrdSystemId UNIQUEIDENTIFIER
                   ,AcademicType VARCHAR(50)
                   ,ClockHourProgram VARCHAR(10)
                   ,IsMakingSAP BIT
                   ,TermId UNIQUEIDENTIFIER
                   ,TermDescrip VARCHAR(100)
                   ,TermStartDate DATETIME
                   ,TermEndDate DATETIME
                   ,DescripXTranscript VARCHAR(250)
                   ,TermAverage DECIMAL(18, 2)
                   ,EnroTermSimple_CourseCredits DECIMAL(18, 2)
                   ,EnroTermSimple_GPACredits DECIMAL(18, 2)
                   ,EnroTermSimple_GPA DECIMAL(18, 2)
                   ,EnroTermWeight_CoursesCredits DECIMAL(18, 2)
                   ,EnroTermWeight_GPACredits DECIMAL(18, 2)
                   ,EnroTermWeight_GPA DECIMAL(18, 2)
                   ,StudTermSimple_CourseCredits DECIMAL(18, 2)
                   ,StudTermSimple_GPACredits DECIMAL(18, 2)
                   ,StudTermSimple_GPA DECIMAL(18, 2)
                   ,StudTermWeight_CoursesCredits DECIMAL(18, 2)
                   ,StudTermWeight_GPACredits DECIMAL(18, 2)
                   ,StudTermWeight_GPA DECIMAL(18, 2)
                   ,EnroSimple_CourseCredits DECIMAL(18, 2)
                   ,EnroSimple_GPACredits DECIMAL(18, 2)
                   ,EnroSimple_GPA DECIMAL(18, 2)
                   ,EnroWeight_CoursesCredits DECIMAL(18, 2)
                   ,EnroWeight_GPACredits DECIMAL(18, 2)
                   ,EnroWeight_GPA DECIMAL(18, 2)
                   ,StudSimple_CourseCredits DECIMAL(18, 2)
                   ,StudSimple_GPACredits DECIMAL(18, 2)
                   ,StudSimple_GPA DECIMAL(18, 2)
                   ,StudWeight_CoursesCredits DECIMAL(18, 2)
                   ,StudWeight_GPACredits DECIMAL(18, 2)
                   ,StudWeight_GPA DECIMAL(18, 2)
                   ,GradesFormat VARCHAR(50)
                   ,GPAMethod VARCHAR(50)
                   ,GradeBookAt VARCHAR(50)
                   ,RetakenPolicy VARCHAR(50)
                );
        END;
        -- END --  0) Declare varialbles and Set initial values  

        --  1)Fill temp tables  
        BEGIN
            INSERT INTO #CoursesTaken
            EXEC USP_TR_Sub03_GetPreparedGPATable @TableName = @CoursesTakenTableName;

            INSERT INTO #GPASummary
            EXEC USP_TR_Sub03_GetPreparedGPATable @TableName = @GPASummaryTableName;
        END;
        -- END  --  1)Fill temp tables  

        -- to Test  
        -- SELECT * FROM #CoursesTaken AS CT ORDER BY CT.StudentId, CT.StuEnrollId, CT.TermId  
        -- SELECT * FROM #GPASummary AS GS ORDER BY GS.StudentId, GS.StuEnrollId, GS.TermId  

        DECLARE @includeRepeatedCourses VARCHAR(50) = (
                                                      SELECT dbo.GetAppSettingValueByKeyName('IncludeCreditsAttemptedForRepeatedCourses', NULL)
                                                      );

        --  2) Selet Totals  
        BEGIN
            IF ( @ShowMultipleEnrollments = 0 )
                BEGIN
           
                    SELECT     GS.StuEnrollId AS Id
                              ,MIN(GS.GradesFormat) AS GradesFormat
                              ,MIN(GS.GPAMethod) AS GPAMethod
                              ,MIN(   CASE WHEN (
                                                dbo.GetACIdForStudent(GS.StuEnrollId) = 5
                                                AND dbo.GetGradesFormatGivenStudentEnrollId(GS.StuEnrollId) = 'numeric'
                                                ) THEN dbo.CalculateStudentAverage(GS.StuEnrollId, NULL, NULL, NULL, @TermId,NULL)
                                           ELSE GS.EnroSimple_GPA
                                      END
                                  ) AS EnrollmentSimpleGPA
                              ,MIN(   CASE WHEN (
                                                dbo.GetACIdForStudent(GS.StuEnrollId) = 5
                                                AND dbo.GetGradesFormatGivenStudentEnrollId(GS.StuEnrollId) = 'numeric'
                                                ) THEN dbo.CalculateStudentAverage(GS.StuEnrollId, NULL, NULL, NULL, @TermId,NULL)
                                           ELSE GS.EnroWeight_GPA
                                      END
                                  ) AS EnrollmentWeightedGPA
                              ,MAX(CONVERT(INTEGER, GS.IsMakingSAP)) AS IsMakingSAP
                              ,MIN(GS.AcademicType) AS AcademicType
                              ,MIN(CONVERT(INTEGER, GS.PrgVersionTrackCredits)) AS PrgVersionTrackCredits
                              ,CASE WHEN @includeRepeatedCourses = 'Yes' THEN COUNT(CT.CoursesTakenId)
                                    ELSE COUNT(DISTINCT CT.ReqId)
                               END AS CoursesTaked
                              ,CASE WHEN @includeRepeatedCourses = 'Yes' THEN SUM(ISNULL(CT.SCS_CreditsAttempted, 0))
                                    ELSE (
                                         SELECT SUM(CA.SCS_CreditsAttempted)
                                         FROM   (
                                                SELECT DISTINCT CAT.ReqId
                                                               ,CAT.SCS_CreditsAttempted
                                                FROM   #CoursesTaken CAT
                                                WHERE  CAT.StuEnrollId = GS.StuEnrollId
                                                ) CA
                                         )
                               END AS CreditsAttempted
                              ,SUM(ISNULL(CT.CreditsEarned_Calculations, 0)) AS CreditsEarned
                              ,CASE WHEN @includeRepeatedCourses = 'Yes' THEN SUM(ISNULL(CT.ScheduleDays, 0))
                                    ELSE (
                                         SELECT SUM(SD.ScheduleDays)
                                         FROM   (
                                                SELECT DISTINCT SDT.ReqId
                                                               ,SDT.ScheduleDays
                                                FROM   #CoursesTaken SDT
                                                WHERE  SDT.StuEnrollId = GS.StuEnrollId
                                                ) SD
                                         )
                               END AS ScheduleDays
                              ,SUM(ISNULL(CT.ActualDay_Calculations, 0)) AS ActualDay
                              ,SUM(ISNULL(CT.FinalGPA_Calculations, 0)) AS GradePoints
                    FROM       #GPASummary AS GS
                    INNER JOIN #CoursesTaken AS CT ON CT.StuEnrollId = GS.StuEnrollId
                                                      AND CT.TermId = GS.TermId
                    WHERE      EXISTS (
                                      SELECT Val
                                      FROM   dbo.MultipleValuesForReportParameters(@StuEnrollIdList, ',', 1)
                                      WHERE  CONVERT(UNIQUEIDENTIFIER, Val) = CT.StuEnrollId
                                      )
                               AND (
                                   @TermId IS NULL
                                   OR GS.TermId = @TermId
                                   )
                    GROUP BY   GS.StuEnrollId;
                END;
            ELSE
                BEGIN
                    SELECT     GS.StudentId AS Id
                              ,MIN(GS.GradesFormat) AS GradesFormat
                              ,MIN(GS.GPAMethod) AS GPAMethod
                              ,MIN(GS.StudSimple_GPA) AS EnrollmentSimpleGPA
                              ,MIN(GS.StudWeight_GPA) AS EnrollmentWeightedGPA
                              ,MAX(CONVERT(INTEGER, GS.IsMakingSAP)) AS IsMakingSAP
                              ,MIN(GS.AcademicType) AS AcademicType
                              ,MIN(CONVERT(INTEGER, GS.PrgVersionTrackCredits)) AS PrgVersionTrackCredits
                              ,CASE WHEN @includeRepeatedCourses = 'Yes' THEN COUNT(CT.CoursesTakenId)
                                    ELSE COUNT(DISTINCT CT.ReqId)
                               END AS CoursesTaked
                              ,(
                               SELECT SUM(CA.SCS_CreditsAttempted)
                               FROM   (
                                      SELECT DISTINCT CAT.ReqId
                                                     ,CAT.SCS_CreditsAttempted
                                      FROM   #CoursesTaken CAT
                                      WHERE  CAT.StudentId = GS.StudentId
                                      ) CA
                               ) AS CreditsAttempted
                              ,SUM(ISNULL(CT.CreditsEarned_Calculations, 0)) AS CreditsEarned
                              ,CASE WHEN @includeRepeatedCourses = 'Yes' THEN SUM(ISNULL(CT.ScheduleDays, 0))
                                    ELSE (
                                         SELECT SUM(SD.ScheduleDays)
                                         FROM   (
                                                SELECT DISTINCT SDT.ReqId
                                                               ,SDT.ScheduleDays
                                                FROM   #CoursesTaken SDT
                                                WHERE  SDT.StudentId = GS.StudentId
                                                ) SD
                                         )
                               END AS ScheduleDays
                              ,SUM(ISNULL(CT.ActualDay_Calculations, 0)) AS ActualDay
                              ,SUM(ISNULL(CT.FinalGPA_Calculations, 0)) AS GradePoints
                    FROM       #GPASummary AS GS
                    INNER JOIN #CoursesTaken AS CT ON CT.StuEnrollId = GS.StuEnrollId
                                                      AND CT.TermId = GS.TermId
                    WHERE      CT.RowNumberMultEnrollNoRetaken = 1
                               AND EXISTS (
                                          SELECT Val
                                          FROM   dbo.MultipleValuesForReportParameters(@StuEnrollIdList, ',', 1)
                                          WHERE  CONVERT(UNIQUEIDENTIFIER, Val) = CT.StuEnrollId
                                          )
                               AND (
                                   @TermId IS NULL
                                   OR GS.TermId = @TermId
                                   )
                    GROUP BY   GS.StudentId;
                END;
        END;
        -- END  --  2) Selet Totals  

        -- 3) drop temp tables  
        BEGIN
            -- Drop temp tables  
            IF EXISTS (
                      SELECT 1
                      FROM   tempdb.sys.objects AS O
                      WHERE  O.type IN ( 'U' )
                             AND O.object_id = OBJECT_ID(N'tempdb..#CoursesTaken')
                      )
                BEGIN
                    DROP TABLE #CoursesTaken;
                END;
            IF EXISTS (
                      SELECT 1
                      FROM   tempdb.sys.objects AS O
                      WHERE  O.type IN ( 'U' )
                             AND O.object_id = OBJECT_ID(N'tempdb..#GPASummary')
                      )
                BEGIN
                    DROP TABLE #GPASummary;
                END;
        END;
    -- END  --  3) drop temp tables  
    END;
-- =========================================================================================================  
-- END  --  USP_TR_Sub07_TotalCourses   
-- =========================================================================================================  



GO
