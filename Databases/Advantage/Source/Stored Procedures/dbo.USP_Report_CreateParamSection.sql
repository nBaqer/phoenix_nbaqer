SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_Report_CreateParamSection]
    @SectionName AS VARCHAR(MAX)
   ,@Caption AS VARCHAR(MAX)
   ,@SetId AS INT,@Sequence AS INT,
   @Description AS VARCHAR(Max)
-- WITH ENCRYPTION, RECOMPILE, EXECUTE AS CALLER|SELF|OWNER| 'user_name'
AS
    DECLARE @Return_Section INT = 0;
     IF NOT EXISTS (
                  SELECT TOP 1 SectionId
                  FROM   dbo.ParamSection
                  WHERE  SectionName = @SectionName
                  )
        BEGIN
            INSERT INTO dbo.ParamSection (
                                         SectionName
                                        ,SectionCaption
                                        ,SetId
                                        ,SectionSeq
                                        ,SectionDescription
                                        ,SectionType
                                         )
            VALUES ( @SectionName                              -- SectionName - nvarchar(50)
                    ,@Caption                             -- SectionCaption - nvarchar(100)
                    ,@SetId                                -- SetId - bigint
                    ,@Sequence                                                      -- SectionSeq - int
                    ,@Description -- SectionDescription - nvarchar(500)
                    ,0                                                      -- SectionType - int
                );
        END;

    SET @Return_Section = (
                                      SELECT TOP 1 SectionId
                                      FROM   dbo.ParamSection
                                      WHERE  SectionName = @SectionName
                                      );



    RETURN @Return_Section;
GO
