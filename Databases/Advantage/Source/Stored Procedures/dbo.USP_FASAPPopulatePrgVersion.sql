SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_FASAPPopulatePrgVersion]
    (
     @CampID UNIQUEIDENTIFIER
    ,@UserID UNIQUEIDENTIFIER = NULL
	)
AS
    IF @UserID IS NULL
        BEGIN
    
            SELECT  t1.PrgVerId
                   ,t1.PrgVerDescrip
            FROM    arPrgVersions t1
                   ,arSap t3
            WHERE   t1.FASAPId = t3.SAPID
                    AND t3.FASAPPolicy = 1
                    AND (
                          t1.CampGrpId IN ( SELECT  CampGrpId
                                            FROM    syCmpGrpCmps
                                            WHERE   CampusId = @CampID
                                                    AND CampGrpId <> (
                                                                       SELECT   CampGrpId
                                                                       FROM     syCampGrps
                                                                       WHERE    CampGrpDescrip = 'ALL'
                                                                     ) )
                          OR t1.CampGrpId = (
                                              SELECT    CampGrpId
                                              FROM      syCampGrps
                                              WHERE     CampGrpDescrip = 'ALL'
                                            )
                        )
            ORDER BY t1.PrgVerDescrip; 
        END;
    ELSE
        BEGIN          
   
            SELECT  t1.PrgVerId
                   ,t1.PrgVerDescrip
            FROM    arPrgVersions t1
                   ,arSap t3
            WHERE   t1.FASAPId = t3.SAPID
                    AND t3.FASAPPolicy = 1
                    AND (
                          t1.CampGrpId IN ( SELECT  CampGrpId
                                            FROM    syCmpGrpCmps
                                            WHERE   CampusId = @CampID
                                                    AND CampGrpId <> (
                                                                       SELECT   CampGrpId
                                                                       FROM     syCampGrps
                                                                       WHERE    CampGrpDescrip = 'ALL'
                                                                     ) )
                          OR t1.CampGrpId = (
                                              SELECT    CampGrpId
                                              FROM      syCampGrps
                                              WHERE     CampGrpDescrip = 'ALL'
                                            )
                        )
                    AND t1.CampGrpId IN ( SELECT DISTINCT
                                                    A.CampGrpId
                                          FROM      syUsersRolesCampGrps A
                                                   ,syCampGrps B
                                          WHERE     A.CampGrpId = B.CampGrpId
                                                    AND A.UserId = @UserID )
            ORDER BY t1.PrgVerDescrip; 
        END;





GO
