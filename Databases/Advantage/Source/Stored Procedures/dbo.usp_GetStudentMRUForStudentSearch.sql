SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_GetStudentMRUForStudentSearch]
    @UserId UNIQUEIDENTIFIER
   ,@CampusId UNIQUEIDENTIFIER
AS
    SET NOCOUNT ON;
-- For new users pull 20 students from the user's default campus
-- For existing users pull 20 students from the syMRU table, students can be from multiple campuses
    DECLARE @CountMRU INT;
    DECLARE @StudentIdentifier VARCHAR(50);
    SET @StudentIdentifier = (
                               SELECT   Value
                               FROM     dbo.syConfigAppSetValues
                               WHERE    SettingId = 69
                                        AND CampusId IS NULL
                             );


-- used to return MRU results
    DECLARE @MRUList TABLE
        (
         MRUId UNIQUEIDENTIFIER NOT NULL
        ,Counter INT NOT NULL
        ,ChildId UNIQUEIDENTIFIER NOT NULL
        ,MRUTypeId TINYINT NOT NULL
        ,UserId UNIQUEIDENTIFIER NOT NULL
        ,CampusId UNIQUEIDENTIFIER NULL
        ,SortOrder INT NULL
        ,IsSticky BIT NULL
        ,StudentId UNIQUEIDENTIFIER NOT NULL
        ,FullName VARCHAR(200) NOT NULL
        ,CampusDescrip VARCHAR(100) NOT NULL
        ,LeadId UNIQUEIDENTIFIER NULL
        ,ModDate DATETIME
        );
-- used to create default MRU list for the user if one doesn't exist
    DECLARE @DefaultMRU TABLE
        (
         ChildId UNIQUEIDENTIFIER NOT NULL
        ,MRUTypeId TINYINT NOT NULL
        ,UserId UNIQUEIDENTIFIER NOT NULL
        ,CampusId UNIQUEIDENTIFIER NULL
        ,SortOrder INT NOT NULL
                       IDENTITY
        ,IsSticky BIT NULL
        ,ModDate DATETIME
        );
    SELECT  @CountMRU = COUNT(*)
    FROM    dbo.syMRUS
    WHERE   UserId = @UserId
            AND MRUTypeId = 1;

    PRINT @CountMRU;
    IF ( @CountMRU > 0 )
        BEGIN
-- Has MRU rows 
            INSERT  INTO @MRUList
                    (
                     MRUId
                    ,Counter
                    ,ChildId
                    ,MRUTypeId
                    ,UserId
                    ,CampusId
                    ,SortOrder
                    ,IsSticky
                    ,StudentId
                    ,FullName
                    ,CampusDescrip
                    ,LeadId
                    ,ModDate
					)
                    SELECT DISTINCT TOP 20
                            M.MRUId
                           ,M.Counter
                           ,M.ChildId
                           ,M.MRUTypeId
                           ,M.UserId
                           ,M.CampusId
                           ,M.SortOrder
                           ,M.IsSticky
                           ,S.StudentId
                           ,CASE WHEN S.MiddleName IS NULL
                                      OR S.MiddleName = '' THEN S.FirstName + ' ' + S.LastName
                                 ELSE S.FirstName + ' ' + S.MiddleName + ' ' + S.LastName
                            END AS FullName
                           ,C.CampDescrip
                           ,(
                              SELECT TOP 1
                                        LeadId
                              FROM      arStuEnrollments SQ2
                              WHERE     SQ2.StudentId = S.StudentId
                                        AND LeadId IS NOT NULL
                            ) AS LeadId
                           ,M.ModDate
                    FROM    dbo.syMRUS AS M
                    INNER JOIN dbo.syMRUTypes AS T ON M.MRUTypeId = T.MRUTypeId
                    INNER JOIN dbo.arStudent AS S ON M.ChildId = S.StudentId
--INNER  JOIN dbo.arStuEnrollments AS E ON S.StudentId = E.StudentId
--INNER  JOIN dbo.adLeads AS L ON E.LeadId = L.LeadId
                    INNER  JOIN dbo.syCampuses AS C ON M.CampusId = C.CampusId
                    WHERE   M.MRUTypeId = 1
                            AND UserId = @UserId
      AND
		-- MRU List will show leads from the campuses user has access to
                            C.CampusId IN ( SELECT DISTINCT
                                                    C.CampusId
                                            FROM    syUsers U
                                            INNER JOIN syUsersRolesCampGrps URC ON U.UserId = URC.UserId
                                            INNER JOIN syCmpGrpCmps CGC ON URC.CampGrpId = CGC.CampGrpId
                                            INNER JOIN syCampuses C ON C.CampusId = CGC.CampusId
                                            WHERE   U.UserId = @UserId )
                    ORDER BY M.ModDate DESC;
--M.SortOrder

        END;
    ELSE
        BEGIN
-- no rows so we must fill the MRUs with a default MRU list
            INSERT  INTO @DefaultMRU
                    (
                     ChildId
                    ,MRUTypeId
                    ,UserId
                    ,CampusId
                    ,IsSticky
                    ,ModDate 
					)
                    SELECT DISTINCT TOP 20
                            S.StudentId AS ChildId
                           ,1 AS MRUTypeId
                           ,@UserId AS UserId
                           ,C.CampusId
                           ,0 AS IsSticky
                           ,S.ModDate --From arStudent table
                    FROM    dbo.arStudent AS S
                    INNER JOIN dbo.arStuEnrollments AS E ON S.StudentId = E.StudentId
                    INNER JOIN dbo.syCampuses AS C ON E.CampusId = C.CampusId
                    INNER JOIN dbo.adLeads AS L ON E.LeadId = L.LeadId
                    WHERE   -- MRU List will show leads from the campuses user has access to
                            C.CampusId IN ( SELECT DISTINCT
                                                    C.CampusId
                                            FROM    syUsers U
                                            INNER JOIN syUsersRolesCampGrps URC ON U.UserId = URC.UserId
                                            INNER JOIN syCmpGrpCmps CGC ON URC.CampGrpId = CGC.CampGrpId
                                            INNER JOIN syCampuses C ON C.CampusId = CGC.CampusId
                                            WHERE   U.UserId = @UserId )
                    ORDER BY S.ModDate DESC;
-- insert records into the MRU table
            INSERT  INTO dbo.syMRUS
                    (
                     ChildId
                    ,MRUTypeId
                    ,UserId
                    ,CampusId
                    ,SortOrder
                    ,IsSticky
                    ,ModDate 
					)
                    SELECT  ChildId
                           ,MRUTypeId
                           ,UserId
                           ,CampusId
                           ,SortOrder
                           ,IsSticky
                           ,ModDate
                    FROM    @DefaultMRU;


-- Fill the MRU results into the @MRUList table variable
            INSERT  INTO @MRUList
                    (
                     MRUId
                    ,Counter
                    ,ChildId
                    ,MRUTypeId
                    ,UserId
                    ,CampusId
                    ,SortOrder
                    ,IsSticky
                    ,StudentId
                    ,FullName
                    ,CampusDescrip
                    ,LeadId
                    ,ModDate
					)
                    SELECT DISTINCT TOP 20
                            M.MRUId
                           ,M.Counter
                           ,M.ChildId
                           ,M.MRUTypeId
                           ,M.UserId
                           ,M.CampusId
                           ,M.SortOrder
                           ,M.IsSticky
                           ,S.StudentId
                           ,CASE WHEN S.MiddleName IS NULL
                                      OR S.MiddleName = '' THEN S.FirstName + ' ' + S.LastName
                                 ELSE S.FirstName + ' ' + S.MiddleName + ' ' + S.LastName
                            END AS FullName
                           ,C.CampDescrip
                           ,(
                              SELECT TOP 1
                                        LeadId
                              FROM      arStuEnrollments SQ2
                              WHERE     SQ2.StudentId = S.StudentId
                                        AND LeadId IS NOT NULL
                            ) AS LeadId
                           ,M.ModDate
                    FROM    dbo.syMRUS AS M
                    INNER JOIN dbo.syMRUTypes AS T ON M.MRUTypeId = T.MRUTypeId
                    INNER JOIN dbo.arStudent AS S ON M.ChildId = S.StudentId
                    INNER JOIN dbo.syCampuses AS C ON M.CampusId = C.CampusId
                    WHERE   M.MRUTypeId = 1
                            AND UserId = @UserId
                    ORDER BY M.ModDate DESC;
--M.SortOrder
        END;


-- return the @MRUList table variable
    SELECT  MRUId
           ,Counter
           ,ChildId
           ,MRUTypeId
           ,UserId
           ,m.CampusId AS CampusId
           ,SortOrder
           ,IsSticky
           ,m.StudentId
           ,FullName
           ,CampusDescrip
           ,m.LeadId
           ,m.ModDate AS ModDate
           ,FirstName
           ,LastName
           ,SSN
    FROM    @MRUList m
    INNER JOIN dbo.arStudent s ON s.StudentId = m.StudentId
    ORDER BY ModDate DESC;







GO
