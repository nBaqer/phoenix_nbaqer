SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_PostTermFees] @AttValues NTEXT
AS
    DECLARE @hDoc INT;         
        --Prepare input values as an XML document         
    EXEC sp_xml_preparedocument @hDoc OUTPUT,@AttValues;         
                   
    --       ( SELECT    StuEnrollId      
    --  FROM      OPENXML(@hDoc,'/dsPostTermFees/InsertPostTermFees',1)           
    --                       WITH (StuEnrollId VARCHAR(50))      
    --)         
          
          

          
    IF EXISTS ( SELECT  *
                FROM    sysobjects
                WHERE   type = 'TR'
                        AND name = 'Trigger_Insert_Check_saTransactions' )
        BEGIN      
            DISABLE TRIGGER Trigger_Insert_Check_saTransactions ON dbo.saTransactions;      
        END;      
          
    INSERT  INTO dbo.saTransactions
            (
             TransactionId
            ,StuEnrollId
            ,TermId
            ,CampusId
            ,TransDate
            ,TransCodeId
            ,TransReference
            ,AcademicYearId
            ,TransDescrip
            ,TransAmount
            ,TransTypeId
            ,IsPosted
            ,CreateDate
            ,BatchPaymentId
            ,ViewOrder
            ,IsAutomatic
            ,ModUser
            ,ModDate
            ,Voided
            ,FeeLevelId
            ,FeeId
            ,PaymentCodeId
            ,FundSourceId
            ,StuEnrollPayPeriodId
            )
            SELECT  NEWID()
                   ,StuEnrollId
                   ,TermId
                   ,CampusId
                   ,TransDate
                   ,TransCodeId
                   ,TransReference
                   ,NULL
                   ,TransDescrip
                   ,TransAmount
                   ,0
                   ,1
                   ,GETDATE()
                   ,NULL
                   ,0
                   ,1
                   ,ModUser
                   ,GETDATE()
                   ,0
                   ,FeeLevelId
                   ,FeeId
                   ,NULL
                   ,NULL
                   ,NULL
            FROM    OPENXML(@hDoc,'/dsPostTermFees/InsertPostTermFees',1)         
                    WITH (StuEnrollId UNIQUEIDENTIFIER,TermId UNIQUEIDENTIFIER,      
                    CampusId UNIQUEIDENTIFIER,TransDate VARCHAR(10),TransCodeId UNIQUEIDENTIFIER,      
                    TransReference VARCHAR(50),AcademicYearId UNIQUEIDENTIFIER,TransDescrip VARCHAR(50),      
                    TransAmount DECIMAL(19,4),TransTypeId INT,IsPosted BIT,CreateDate VARCHAR(10),      
                    BatchPaymentId UNIQUEIDENTIFIER,ViewOrder INT,      
                    IsAutomatic BIT,      
                    ModUser VARCHAR(50),      
                    ModDate VARCHAR(10),      
                    Voided BIT,      
                    FeeLevelId TINYINT,      
                    FeeId UNIQUEIDENTIFIER,      
                    PaymentCodeId UNIQUEIDENTIFIER,      
                    FundSourceId UNIQUEIDENTIFIER,      
                   StuEnrollPayPeriodId UNIQUEIDENTIFIER);                         
                          
                            
    IF EXISTS ( SELECT  *
                FROM    sysobjects
                WHERE   type = 'TR'
                        AND name = 'Trigger_Insert_Check_saTransactions' )
        BEGIN      
            ENABLE TRIGGER Trigger_Insert_Check_saTransactions ON dbo.saTransactions;      
        END;       
    EXEC sp_xml_removedocument @hDoc;         



GO
