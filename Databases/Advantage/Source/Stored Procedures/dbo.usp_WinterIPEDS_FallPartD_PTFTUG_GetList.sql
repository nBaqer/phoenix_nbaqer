SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_WinterIPEDS_FallPartD_PTFTUG_GetList]
    @CampusId VARCHAR(50)
   ,@ProgId VARCHAR(4000) = NULL
   ,@StartDate DATETIME = NULL
   ,@EndDate DATETIME = NULL
   ,@DateRangeText VARCHAR(100) = NULL
   ,@OrderBy VARCHAR(100)
AS
    DECLARE @ReturnValue VARCHAR(100);
    DECLARE @ContinuingStartDate DATETIME;
    DECLARE @Value VARCHAR(50);
    SET @Value = (
                   SELECT   dbo.GetAppSettingValue(47,@CampusId)
                 );
    SET @ContinuingStartDate = @StartDate;
-- For Academic Year Reporter, the End Date should be 10/15/Cohort Year
-- and Start Date should be blank
    IF SUBSTRING(LOWER(@DateRangeText),1,10) = 'enrollment'
        BEGIN
            SET @EndDate = @StartDate; -- it will always be 10/15/cohort year example: for cohort year 2009, it is 10/15/2009
            SET @StartDate = @StartDate; -- it will always be 10/15/cohort year example: for cohort year 2009, it is 10/15/2009
        END;

    IF @ProgId IS NOT NULL
        BEGIN
            SELECT  @ReturnValue = COALESCE(@ReturnValue,'') + ProgDescrip + ','
            FROM    arPrograms t1
            WHERE   t1.ProgId IN ( SELECT   Val
                                   FROM     MultipleValuesForReportParameters(@ProgId,',',1) );
            SET @ReturnValue = SUBSTRING(@ReturnValue,1,LEN(@ReturnValue) - 1);
        END;
    ELSE
        BEGIN
            SELECT  @ReturnValue = COALESCE(@ReturnValue,'') + ProgDescrip + ','
            FROM    arPrograms t1
            WHERE   t1.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
                    AND t1.CampGrpId IN ( SELECT    CampGrpId
                                          FROM      syCmpGrpCmps
                                          WHERE     CampusId = @CampusId );
            SET @ReturnValue = SUBSTRING(@ReturnValue,1,LEN(@ReturnValue) - 1);
        END;

    CREATE TABLE #FallPartB4FTUG
        (
         RowNumber UNIQUEIDENTIFIER
        ,SSN VARCHAR(50)
        ,StudentNumber VARCHAR(50)
        ,StudentName VARCHAR(100)
        ,NonDegCertSeeking VARCHAR(10)
        ,NonDegCertSeekingCount INT
        ,FirstTimeAndUG VARCHAR(10)
        ,FirstTimeAndUGCount INT
        ,ProgDescrip VARCHAR(100)
        ,StudentId UNIQUEIDENTIFIER
        ); 

    INSERT  INTO #FallPartB4FTUG
            SELECT  NEWID() AS RowNumber
                   ,dbo.UDF_FormatSSN(SSN) AS SSN
                   ,StudentNumber
                   ,StudentName
                   ,CASE WHEN NonDegCertSeeking >= 1 THEN 'X'
                         ELSE ''
                    END AS NonDegCertSeekingTime
                   ,CASE WHEN NonDegCertSeeking >= 1 THEN 1
                         ELSE 0
                    END AS NonDegCertSeekingCount
                   ,CASE WHEN FirstTimeAndUG >= 1 THEN 'X'
                         ELSE ''
                    END AS FirstTimeAndUG
                   ,CASE WHEN FirstTimeAndUG >= 1 THEN 1
                         ELSE 0
                    END AS FirstTimeAndUGCount
                   ,@ReturnValue AS ProgDescrip
                   ,StudentId
            FROM    (
                      SELECT    t1.SSN
                               ,t1.StudentNumber
                               ,t1.LastName + ', ' + t1.FirstName + ' ' + ISNULL(t1.MiddleName,'') AS StudentName
                               ,
				--(Select Count(*) from arStuEnrollments SQ1,adDegCertSeeking  SQ2
				--	where 
				--		SQ1.StuEnrollId = t2.StuEnrollId and 
				--		SQ1.StartDate<=@EndDate and 
				--		SQ1.degcertseekingid = SQ2.DegCertSeekingId  and
				--		SQ2.IPEDSValue=10
				--) 
                                1 AS NonDegCertSeeking
                               ,
				
				 --When Academic year is chosen since it is an as of date and if both the enrollments 
				 --started before that date then student is not marked under First time. This option is pretty straightforward.
                                CASE WHEN ( SUBSTRING(LOWER(@DateRangeText),1,10) = 'enrollment' ) THEN --IF Academic Program
                                          CASE WHEN (
                                                      SELECT    COUNT(*)
                                                      FROM      arStuEnrollments SQ1
                                                               ,arPrgVersions SQ3
                                                               ,arProgTypes SQ4
                                                      WHERE     SQ3.PrgVerId = SQ1.PrgVerId
                                                                AND SQ3.ProgTypId = SQ4.ProgTypId
                                                                AND SQ4.IPEDSValue = 58
                                                                AND SQ1.StudentId = t1.StudentId
                                                    ) = 1 THEN 1
                                               ELSE
								-- Student has multiple enrollements
								-- Check if the enrollment returned has any prior enrollments
								-- If there are prior enrollments then student is not first time in institution
                                                    CASE WHEN (
                                                                SELECT  COUNT(*)
                                                                FROM    arStuEnrollments SQ1
                                                                       ,arPrgVersions SQ3
                                                                       ,arProgTypes SQ4
                                                                WHERE   SQ1.StuEnrollId = t2.StuEnrollId
                                                                        AND SQ3.PrgVerId = SQ1.PrgVerId
                                                                        AND SQ3.ProgTypId = SQ4.ProgTypId
                                                                        AND SQ4.IPEDSValue = 58
                                                                        AND EXISTS ( SELECT StuEnrollId
                                                                                     FROM   arStuEnrollments Q1
                                                                                     WHERE  StudentId = t1.StudentId
                                                                                            AND Q1.StartDate < SQ1.StartDate )
                                                              ) >= 1 THEN 0
                                                         ELSE 1
                                                    END
                                          END
						
					--(Select Count(*) from arStuEnrollments SQ1,adDegCertSeeking  SQ2, 
					--arPrgVersions SQ3, arProgTypes SQ4
					--	where 
					--		SQ1.StuEnrollId = t2.StuEnrollId and 
					--		SQ3.PrgVerId=SQ1.PrgVerId and 
					--		SQ3.ProgTypId = SQ4.ProgTypId and
					--		SQ1.StartDate<=@EndDate and 
					--		SQ1.degcertseekingid = SQ2.DegCertSeekingId  and
					--		SQ2.IPEDSValue=11 and 
					--		SQ4.IPEDSValue = 58 AND
					--		--When Academic year is chosen since it is an as of date and 
					--		--if both the enrollments started before that date then student is not marked under First time
					--		SQ1.StudentId NOT IN
					--		(SELECT DISTINCT StudentId from
					--			(SELECT StudentId,COUNT(*) AS RowCounter FROM arStuEnrollments t9 
					--			WHERE t9.StartDate<=@EndDate GROUP BY StudentId HAVING COUNT(*)>1
					--		) dtMultipleEnrollments)
					--	) 
                                     ELSE CASE WHEN (
                                                      SELECT    COUNT(*)
                                                      FROM      arStuEnrollments SQ1
                                                               ,arPrgVersions SQ3
                                                               ,arProgTypes SQ4
                                                      WHERE     SQ3.PrgVerId = SQ1.PrgVerId
                                                                AND SQ3.ProgTypId = SQ4.ProgTypId
                                                                AND SQ4.IPEDSValue = 58
                                                                AND SQ1.StudentId = t1.StudentId
                                                    ) = 1 THEN 1
                                               ELSE
							-- Student has multiple enrollements
							-- Check if the enrollment returned has any prior enrollments
							-- If there are prior enrollments then student is not first time in institution
                                                    CASE WHEN (
                                                                SELECT  COUNT(*)
                                                                FROM    arStuEnrollments SQ1
                                                                       ,arPrgVersions SQ3
                                                                       ,arProgTypes SQ4
                                                                WHERE   SQ1.StuEnrollId = t2.StuEnrollId
                                                                        AND SQ3.PrgVerId = SQ1.PrgVerId
                                                                        AND SQ3.ProgTypId = SQ4.ProgTypId
                                                                        AND SQ4.IPEDSValue = 58
                                                                        AND EXISTS ( SELECT StuEnrollId
                                                                                     FROM   arStuEnrollments Q1
                                                                                     WHERE  StudentId = t1.StudentId
                                                                                            AND Q1.StartDate < SQ1.StartDate )
                                                              ) >= 1 THEN 0
                                                         ELSE 1
                                                    END
                                          END
                                END AS FirstTimeAndUG
                               ,t1.StudentId
                      FROM      arStudent t1
                      INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                      INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                      INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                                   AND t6.SysStatusId NOT IN ( 8 )
                      INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                      INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                      INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                      LEFT JOIN arAttendTypes t10 ON t2.AttendTypeId = t10.AttendTypeId
                      INNER JOIN adCitizenships t12 ON t1.Citizen = t12.CitizenshipId
                      INNER JOIN adDegCertSeeking t17 ON t2.DegCertSeekingId = t17.DegCertSeekingId
                      WHERE     t2.CampusId = LTRIM(RTRIM(@CampusId))
                                AND (
                                      @ProgId IS NULL
                                      OR t8.ProgId IN ( SELECT  Val
                                                        FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                    )
                                AND t9.IPEDSValue = 58
                                AND ----- Undergraduate
                                t10.IPEDSValue IN ( 61,62 ) ----PartTime And Full Time
                                AND t17.IPEDSValue = 10 --Bring in only non-degree cert seeking id
                                AND t2.StartDate <= @EndDate
                                AND StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
						( SELECT    t1.StuEnrollId
                          FROM      arStuEnrollments t1
                                   ,syStatusCodes t2
                          WHERE     t1.StatusCodeId = t2.StatusCodeId
                                    AND StartDate <= @EndDate
                                    AND -- Student started before the end date range
                                    LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                    AND (
                                          @ProgId IS NULL
                                          OR t8.ProgId IN ( SELECT  Val
                                                            FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                        )
                                    AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                    AND (
                                          t1.DateDetermined < @StartDate
                                          OR ExpGradDate < @StartDate
                                          OR LDA < @StartDate
                                        ) )
                                            	-- If Student is enrolled in only program version and if that program version 
						-- happens to be a continuing ed program exclude the student
                                AND t2.StudentId NOT IN ( SELECT DISTINCT
                                                                    StudentId
                                                          FROM      (
                                                                      SELECT    StudentId
                                                                               ,COUNT(*) AS RowCounter
                                                                      FROM      arStuEnrollments
                                                                      WHERE     PrgVerId IN ( SELECT    PrgVerId
                                                                                              FROM      arPrgVersions
                                                                                              WHERE     IsContinuingEd = 1 )
                                                                      GROUP BY  StudentId
                                                                      HAVING    COUNT(*) = 1
                                                                    ) dtStudent_ContinuingEd )
                                AND t2.StuEnrollId IN (
                                --SELECT TOP 1
                                --        StuEnrollId
                                --FROM    arStuEnrollments A1 ,
                                --        arPrgVersions A2 ,
                                --        arProgTypes A3 ,
                                --        adDegCertSeeking A4
                                --WHERE   A1.PrgVerId = A2.PrgVerId
                                --        AND A2.ProgTypId = A3.ProgTypId
                                --        AND A1.StudentId = t1.StudentId
                                --        AND A1.DegCertSeekingId = A4.DegCertSeekingId
                                --        AND A1.CampusId = LTRIM(RTRIM(@CampusId))
                                --        AND ( @ProgId IS NULL
                                --              OR A2.ProgId IN (
                                --              SELECT    Val
                                --              FROM      [MultipleValuesForReportParameters](@ProgId,
                                --                              ',', 1) )
                                --            )
                                --        AND A3.IPEDSValue = 58 ----- Undergraduate
                                --        AND A4.IPEDSValue = 10 --Bring in only non-degree cert seeking id
                                --        AND A1.StartDate <= @EndDate
                                --ORDER BY StartDate ,
                                --        EnrollDate ASC
                                SELECT  dbo.GetIPEDS_Spring_GetEnrollmentfromMultipleEnrollment(@CampusId,@ProgId,@EndDate,'undergraduate',t1.StudentId) )
                    ) dt;


    SELECT  SSN
           ,StudentNumber
           ,StudentName
           ,NonDegCertSeeking
           ,FirstTimeAndUG
           ,StudentId
           ,EnrollmentId
           ,NonDegCertSeekingCount
           ,FirstTimeAndUGCount
    FROM    (
              SELECT    SSN
                       ,StudentNumber
                       ,StudentName
                       ,NonDegCertSeeking
                       ,FirstTimeAndUG
                       ,StudentId
                       ,(
                          SELECT TOP 1
                                    EnrollmentId
                          FROM      arStuEnrollments C1
                                   ,arPrgVersions C2
                                   ,arProgTypes C3
                          WHERE     C1.PrgVerId = C2.PrgVerId
                                    AND C2.ProgTypId = C3.ProgTypId
                                    AND C3.IPEDSValue = 58
                                    AND C1.StudentId = #FallPartB4FTUG.StudentId
                          ORDER BY  C1.StartDate
                                   ,C1.EnrollDate
                        ) AS EnrollmentId
                       ,CASE ( NonDegCertSeeking )
                          WHEN 'X' THEN 1
                          ELSE 0
                        END NonDegCertSeekingCount
                       ,CASE ( FirstTimeAndUG )
                          WHEN 'X' THEN 1
                          ELSE 0
                        END FirstTimeAndUGCount
              FROM      #FallPartB4FTUG
            ) dt
    ORDER BY CASE WHEN @OrderBy = 'SSN' THEN SSN
             END
           ,CASE WHEN @OrderBy = 'LastName' THEN StudentName
            END
           ,CASE WHEN @OrderBy = 'Student Number' THEN StudentNumber
            END
           ,CASE WHEN @OrderBy = 'EnrollmentId' THEN EnrollmentId
            END;

    DROP TABLE #FallPartB4FTUG;



GO
