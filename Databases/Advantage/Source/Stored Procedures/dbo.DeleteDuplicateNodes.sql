SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[DeleteDuplicateNodes]
AS
    DECLARE @ResourceId INT
       ,@ParentId UNIQUEIDENTIFIER
       ,@Counter INT
       ,@times INT
       ,@HierarchyId UNIQUEIDENTIFIER;
    DECLARE getNodes_Cursor CURSOR
    FOR
        SELECT  ResourceId
               ,ParentId
               ,COUNT(*)
        FROM    syNavigationNodes
	   --where ResourceId=538
        GROUP BY ResourceId
               ,ParentId
        HAVING  COUNT(*) > 1;

    OPEN getNodes_Cursor;
    FETCH NEXT FROM getNodes_Cursor
INTO @ResourceId,@ParentId,@Counter;
    WHILE @@FETCH_STATUS = 0
        BEGIN
            SET @times = @Counter - 1;
            WHILE @times > 0
                BEGIN
			--Print @times
                    SET @HierarchyId = (
                                         SELECT TOP 1
                                                HierarchyId
                                         FROM   syNavigationNodes
                                         WHERE  ResourceId = @ResourceId
                                                AND ParentId = @ParentId
                                       );
                    IF NOT EXISTS ( SELECT  *
                                    FROM    syNavigationNodes
                                    WHERE   ParentId = @HierarchyId )
                        BEGIN
					--Print @HierarchyId
                            DELETE  FROM syNavigationNodes
                            WHERE   HierarchyId = @HierarchyId;
                        END;
                    ELSE
                        BEGIN
                            IF (
                                 @ResourceId = 401
                                 OR @ResourceId = 406
                               )
                                BEGIN
							--Print @HierarchyId
                                    SET @HierarchyId = (
                                                         SELECT TOP 1
                                                                HierarchyId
                                                         FROM   syNavigationNodes
                                                         WHERE  ResourceId = @ResourceId
                                                                AND ParentId = @ParentId
                                                                AND (
                                                                      ModUser = 'sa'
                                                                      OR ModUser = 'super'
                                                                    )
                                                       );
                                    DELETE  FROM syNavigationNodes
                                    WHERE   ParentId = @HierarchyId
                                            AND (
                                                  ModUser = 'sa'
                                                  OR ModUser = 'super'
                                                );	
                                    DELETE  FROM syNavigationNodes
                                    WHERE   HierarchyId = @HierarchyId
                                            AND (
                                                  ModUser = 'sa'
                                                  OR ModUser = 'super'
                                                );			
                                END;
                            IF ( @ResourceId = 468 )
                                BEGIN
							-- Delete Duplicate Child Rows
                                    DELETE  FROM syNavigationNodes
                                    WHERE   ResourceId IN ( 1,11,100,101 )
                                            AND ParentId NOT IN ( SELECT TOP 1
                                                                            ParentId AS ParentId
                                                                  FROM      (
                                                                              SELECT    ParentId
                                                                                       ,COUNT(*) AS RowCounter
                                                                              FROM      syNavigationNodes
                                                                              WHERE     ResourceId IN ( 1,11,100,101 )
                                                                              GROUP BY  ParentId
                                                                              HAVING    COUNT(*) >= 4
                                                                            ) t1 );
							 
							 -- Delete Duplicate 468
                                    DELETE  FROM syNavigationNodes
                                    WHERE   ResourceId = 468
                                            AND HierarchyId NOT IN ( SELECT TOP 1
                                                                            ParentId AS ParentId
                                                                     FROM   (
                                                                              SELECT    ParentId
                                                                                       ,COUNT(*) AS RowCounter
                                                                              FROM      syNavigationNodes
                                                                              WHERE     ResourceId IN ( 1,11,100,101 )
                                                                              GROUP BY  ParentId
                                                                              HAVING    COUNT(*) >= 4
                                                                            ) t1 );
                                END;
                            IF ( @ResourceId = 6 )
                                BEGIN
                                    DECLARE @topHierarchyId UNIQUEIDENTIFIER;
                                    SET @topHierarchyId = (
                                                            SELECT TOP 1
                                                                    HierarchyId
                                                            FROM    syNavigationNodes
                                                            WHERE   ResourceId = 6
                                                                    AND moduser = 'sa'
                                                          );
                                    DELETE  FROM syNavigationNodes
                                    WHERE   ParentId = @topHierarchyId;
                                    DELETE  FROM syNavigationNodes
                                    WHERE   HierarchyId = @topHierarchyId;
                                END;
                            IF ( @ResourceId = 40 )
                                BEGIN
                                    DECLARE @TopParentId UNIQUEIDENTIFIER;
                                    SET @TopParentId = (
                                                         SELECT TOP 1
                                                                ParentId
                                                         FROM   syNavigationNOdes
                                                         WHERE  ResourceId = 284
                                                       );
							-- Delete Duplicate Child Rows
                                    DELETE  FROM syNavigationNodes
                                    WHERE   ResourceId = 284
                                            AND ParentId = @TopParentId;
                                    DELETE  FROM syNavigationNodes
                                    WHERE   ResourceId = 40
                                            AND HierarchyId = @TopParentId;
                                END;
                            IF ( @ResourceId = 469 )
                                BEGIN
                                    DELETE  FROM syNavigationNodes
                                    WHERE   ResourceId IN ( 7,350,44,38 )
                                            AND ParentId NOT IN ( SELECT TOP 1
                                                                            ParentId AS ParentId
                                                                  FROM      (
                                                                              SELECT    ParentId
                                                                                       ,COUNT(*) AS RowCounter
                                                                              FROM      syNavigationNodes
                                                                              WHERE     ResourceId IN ( 7,350,44,38 )
                                                                              GROUP BY  ParentId
                                                                              HAVING    COUNT(*) >= 4
                                                                            ) t1 );

                                    DELETE  FROM syNavigationNodes
                                    WHERE   ResourceId = 469
                                            AND HierarchyId NOT IN ( SELECT TOP 1
                                                                            ParentId AS ParentId
                                                                     FROM   (
                                                                              SELECT    ParentId
                                                                                       ,COUNT(*) AS RowCounter
                                                                              FROM      syNavigationNodes
                                                                              WHERE     ResourceId IN ( 7,350,44,38 )
                                                                              GROUP BY  ParentId
                                                                              HAVING    COUNT(*) >= 4
                                                                            ) t1 );
                                END;
                        END;
                    SET @times = @times - 1;
                END;
            FETCH NEXT FROM getNodes_Cursor
INTO @ResourceId,@ParentId,@Counter;
        END;
    CLOSE getNodes_Cursor;
    DEALLOCATE getNodes_Cursor;



GO
