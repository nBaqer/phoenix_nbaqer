SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

-- SFA Part B Report
--EXEC usp_IPEDS_Spring_PartB_DetailAndSummary '3F5E839A-589A-4B2A-B258-35A1A8B3B819',NULL,'2010','ssn','07/01/2010','06/30/2011','10/15/2010','academic'
CREATE PROCEDURE [dbo].[usp_IPEDS_Spring_PartB_DetailAndSummary]
    @CampusId VARCHAR(50)
   ,@ProgId VARCHAR(4000) = NULL
   ,@CohortYear VARCHAR(10) = NULL
   ,@OrderBy VARCHAR(100)
   ,@StartDate DATETIME
   ,@EndDate DATETIME
   ,@EndDate_Academic DATETIME = NULL
   ,@SchoolType VARCHAR(50)
AS
    DECLARE @ReturnValue VARCHAR(100)
       ,@Value VARCHAR(100);
    DECLARE @SSN VARCHAR(10)
       ,@FirstName VARCHAR(100)
       ,@LastName VARCHAR(100)
       ,@StudentNumber VARCHAR(50)
       ,@TransferredOut INT
       ,@Exclusions INT;
    DECLARE @CitizenShip_IPEDSValue INT
       ,@ProgramType_IPEDSValue INT
       ,@Gender_IPEDSValue INT;
    DECLARE @FullTimePartTime_IPEDSValue INT
       ,@StudentId UNIQUEIDENTIFIER; 
    DECLARE @AcademicEndDate DATETIME
       ,@AcademicStartDate DATETIME; 

-- Check if School tracks grades by letter or numeric 
    SET @Value = (
                   SELECT TOP 1
                            Value
                   FROM     dbo.syConfigAppSetValues
                   WHERE    SettingId = 47
                 );

--Here, we're determining if the school type is academic. If so, we will change the new var
--@AcademicEndDate to the @EndDate_Academic, if not, than use @EndDate. We will use the new
--@AcademicEndDate variable to help select the student list.  DD Rev 01/10/12
    IF LOWER(@SchoolType) = 'academic'
        BEGIN
            SET @AcademicEndDate = @EndDate_Academic;
            SET @AcademicStartDate = @EndDate_Academic;
        END;
    ELSE
        BEGIN
            SET @AcademicEndDate = @EndDate;
            SET @AcademicStartDate = @StartDate;
        END;


    IF @ProgId IS NOT NULL
        BEGIN
            SELECT  @ReturnValue = COALESCE(@ReturnValue,'') + ProgDescrip + ','
            FROM    arPrograms t1
            WHERE   t1.ProgId IN ( SELECT   Val
                                   FROM     MultipleValuesForReportParameters(@ProgId,',',1) );
            SET @ReturnValue = SUBSTRING(@ReturnValue,1,LEN(@ReturnValue) - 1);
        END;
    ELSE
        BEGIN
            SELECT  @ReturnValue = COALESCE(@ReturnValue,'') + ProgDescrip + ','
            FROM    arPrograms t1
            WHERE   t1.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
                    AND t1.CampGrpId IN ( SELECT    CampGrpId
                                          FROM      syCmpGrpCmps
                                          WHERE     CampusId = @CampusId );
            SET @ReturnValue = SUBSTRING(@ReturnValue,1,LEN(@ReturnValue) - 1);
        END;
 
/*********Get the list of students that will be shown in the report - Starts Here ******************/			
    CREATE TABLE #StudentsList
        (
         StudentId UNIQUEIDENTIFIER
        ,SSN VARCHAR(10)
        ,FirstName VARCHAR(100)
        ,LastName VARCHAR(100)
        ,MiddleName VARCHAR(50)
        ,StudentNumber VARCHAR(50)
        ,
          --DE8814
          --TransferredOut INT ,
          --Exclusions INT ,
         CitizenShip_IPEDSValue INT
        ,
          --DE8814
          --ProgramType_IPEDSValue INT ,
         Gender_IPEDSValue INT
        ,
          --DE8814
          --FullTimePartTime_IPEDSValue INT ,
         GenderId UNIQUEIDENTIFIER
        ,RaceId UNIQUEIDENTIFIER
        ,CitizenId UNIQUEIDENTIFIER
        ,GenderDescription VARCHAR(50)
        ,RaceDescription VARCHAR(50) -- ,
           --DE8814
          --StudentGraduatedStatusCount INT ,
          --ClockHourProgramCount INT
        );
		
-- The following table will store only Full Time First Time UnderGraduate Students
--Commented not used anywhere
    --CREATE TABLE #UnderGraduateStudentsWhoReceivedFinancialAid
    --    (
    --      StudentId UNIQUEIDENTIFIER ,
    --      FinancialAidType INT ,
    --      TitleIV BIT
    --    )
		
-- The following table will store only Full Time First Time UnderGraduate Students
    CREATE TABLE #FinalResult
        (
         RowNumber UNIQUEIDENTIFIER
        ,SSN VARCHAR(20)
        ,StudentNumber VARCHAR(50)
        ,StudentName VARCHAR(100)
        ,FederalGov DECIMAL(18,4)
        ,StateLocalGov DECIMAL(18,4)
        ,InstSource DECIMAL(18,4)
        ,OtherSource DECIMAL(18,4)
        ,PellGrant DECIMAL(18,4)
        ,FederalLoan DECIMAL(18,4)
        ,IPEDSValue INT
        );


-- Get the list of UnderGraduate Students
    INSERT  INTO #StudentsList
            SELECT DISTINCT
                    t1.StudentId
                   ,t1.SSN
                   ,t1.FirstName
                   ,t1.LastName
                   ,t1.MiddleName
                   ,t1.StudentNumber
                   ,
                     --DE8814
  --                  ( SELECT    COUNT(*)
  --                    FROM      arStuEnrollments SQ1 ,
  --                              syStatusCodes SQ2 ,
  --                              dbo.sySysStatus SQ3
  --                    WHERE     SQ1.StatusCodeId = SQ2.StatusCodeId
  --                              AND SQ2.SysStatusId = SQ3.SysStatusId
  --                              AND SQ1.StudentId = t1.StudentId
  --                              AND SQ3.SysStatusId = 19
  --                              AND SQ1.TransferDate <= @AcademicEndDate
  --                  ) AS TransferredOut ,
  --                  (
		--			-- Check if the student was either dropped and if the drop reason is either
			
		---- deceased, active duty, foreign aid service, church mission
  --                    CASE WHEN ( SELECT    COUNT(*)
  --                                FROM      arStuEnrollments SQ1 ,
  --                                          syStatusCodes SQ2 ,
  --                                          dbo.sySysStatus SQ3 ,
  --                                          arDropReasons SQ44
  --                                WHERE     SQ1.StatusCodeId = SQ2.StatusCodeId
  --                                          AND SQ2.SysStatusId = SQ3.SysStatusId
  --                                          AND SQ1.DropReasonId = SQ44.DropReasonId
  --                                          AND SQ1.StudentId = t1.StudentId
  --                                          AND SQ3.SysStatusId IN ( 12 ) -- Dropped
  --                                          AND SQ1.DateDetermined <= @AcademicEndDate
  --                                          AND SQ44.IPEDSValue IN ( 15, 16,
  --                                                            17, 18, 19 )
  --                              ) >= 1 THEN 1
  --                         ELSE 0
  --                    END ) AS Exclusions ,
                    t11.IPEDSValue AS CitizenShip_IPEDSValue
                   ,
                    --DE8814
                    --t9.IPEDSValue AS ProgramType_IPEDSValue ,
                    t3.IPEDSValue AS Gender_IPEDSValue
                   ,
                    --DE8814
                    --t10.IPEDSValue AS FullTimePartTime_IPEDSValue ,
                    t1.Gender
                   ,t1.Race
                   ,t1.Citizen
                   ,(
                      SELECT DISTINCT
                                AgencyDescrip
                      FROM      syRptAgencyFldValues
                      WHERE     RptAgencyFldValId = t3.IPEDSValue
                    ) AS GenderDescription
                   ,(
                      SELECT DISTINCT
                                AgencyDescrip
                      FROM      syRptAgencyFldValues
                      WHERE     RptAgencyFldValId = t4.IPEDSValue
                    ) AS RaceDescription --,
                    --DE8814
                    --( SELECT    COUNT(*)
                    --  FROM      arStuEnrollments SEC ,
                    --            syStatusCodes SC
                    --  WHERE     SEC.StudentId = t1.StudentId
                    --            AND SEC.StatusCodeId = SC.StatusCodeId
                    --            AND SC.SysStatusId = 14
                    --) AS StudentGraduatedStatusCount ,
                    --( SELECT    COUNT(*)
                    --  FROM      arStuEnrollments SEC ,
                    --            arPrograms P ,
                    --            arPrgVersions PV
                    --  WHERE     SEC.PrgVerId = Pv.PrgVerId
                    --            AND P.ProgId = PV.ProgId
                    --            AND SEC.StudentId = t1.StudentId
                    --            AND P.ACId = 5
                    --) AS ClockHourProgramCount
            FROM    adGenders t3
            LEFT JOIN arStudent t1 ON t3.GenderId = t1.Gender
            LEFT JOIN adEthCodes t4 ON t4.EthCodeId = t1.Race
            INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
            INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
            INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                         AND t6.SysStatusId NOT IN ( 8 ) -- Ignore "No Start" Students
            INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
            INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
            INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
            LEFT JOIN arAttendTypes t10 ON t2.AttendTypeId = t10.AttendTypeId
            LEFT JOIN dbo.adDegCertSeeking t12 ON t2.DegCertSeekingId = t12.DegCertSeekingId
            INNER JOIN adCitizenships t11 ON t1.Citizen = t11.CitizenshipId
            WHERE   LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                    AND (
                          @ProgId IS NULL
                          OR t8.ProgId IN ( SELECT  Val
                                            FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                        )
                    AND
                        --t10.IPEDSValue=61 AND -- Full Time
                        --t12.IPEDSValue=11 AND -- First Time
                    t9.IPEDSValue = 58
                    AND -- Under Graduate
                    t2.StartDate <= @AcademicEndDate
                    AND StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
						( SELECT    t1.StuEnrollId
                          FROM      arStuEnrollments t1
                                   ,syStatusCodes t2
                          WHERE     t1.StatusCodeId = t2.StatusCodeId
                                    AND StartDate <= @AcademicEndDate
                                    AND -- Student started before the end date range
                                    LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                    AND (
                                          @ProgId IS NULL
                                          OR t8.ProgId IN ( SELECT  Val
                                                            FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                        )
                                    AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                    AND (
                                          t1.DateDetermined < @AcademicStartDate
                                          OR ExpGradDate < @AcademicStartDate
                                          OR LDA < @AcademicStartDate
                                        ) ) 
                       -- If Student is enrolled in only one program version and if that program version 

                       -- happens to be a continuing ed program exclude the student
                    AND t2.StudentId NOT IN ( SELECT DISTINCT
                                                        StudentId
                                              FROM      (
                                                          SELECT    StudentId
                                                                   ,COUNT(*) AS RowCounter
                                                          FROM      arStuEnrollments
                                                          WHERE     PrgVerId IN ( SELECT    PrgVerId
                                                                                  FROM      arPrgVersions
                                                                                  WHERE     IsContinuingEd = 1 )
                                                          GROUP BY  StudentId
                                                          HAVING    COUNT(*) = 1
                                                        ) dtStudent_ContinuingEd );
/*********Get the list of students that will be sho
wn in the report - Ends Here *****************8*/

-- Get all Full Time, First Time UnderGraduate Students
-- Use #StudentsList as it pulls UnderGraduate Students
--Commented not used anywhere
    --INSERT  INTO #UnderGraduateStudentsWhoReceivedFinancialAid
    --        SELECT  t1.StudentId ,
    --                FinancialAidType ,
    --                TitleIV
    --        FROM    #StudentsList t1
    --                INNER JOIN ( SELECT SE.StudentId ,
    --                                    ( SAS.Amount
    --                                      + COALESCE(( SELECT SUM(TransAmount)
    --                                                   FROM   saPmtDisbRel PDR ,
    --                                                          saTransactions T
    --                                                   WHERE  AwardScheduleId = SAS.AwardScheduleId
    --                                                          AND PDR.TransactionId = T.TransactionId
    --                                                          AND T.Voided = 0
    --                                                 ), 0) ) AS Balance ,
    --                                    ( COALESCE(( SELECT SUM(TransAmount
    --                                                          * -1)
    --                                                 FROM   saPmtDisbRel PDR ,
    --                                                        saTransactions T
    --                                                 WHERE  AwardScheduleId = SAS.AwardScheduleId
    --                                                        AND PDR.TransactionId = T.TransactionId
    --                                                        AND T.Voided = 0
    --                                               ), 0) ) AS Received ,
    --                                    FS.IPEDSValue AS FinancialAidType ,
    --                                    FS.TitleIV AS TitleIV
    --                             FROM   faStudentAwardSchedule SAS
    --                                    INNER JOIN faStudentAwards SA ON SAS.StudentAwardId = SA.StudentAwardId
    --                                    INNER JOIN dbo.arStuEnrollments SE ON SA.StuEnrollId = SE.StuEnrollId
    --                                    INNER JOIN saFundSources FS ON SA.AwardTypeId = FS.FundSourceId
    --                                    LEFT JOIN arAttendTypes t10 ON SE.attendtypeid = t10.AttendTypeId
    --                                    LEFT JOIN dbo.adDegCertSeeking t12 ON SE.DegCertSeekingId = t12.DegCertSeekingId
    --                             WHERE  SE.StudentId IN ( SELECT DISTINCT
    --                                                          StudentId
    --                                                      FROM
    --                                                          #StudentsList )
    --                                    AND -- UnderGraduates
    --                                    t10.IPEDSValue = 61
    --                                    AND -- Full Time 
    --                                    t12.IPEDSValue = 11 -- First Time
                                 
    --                           ) getStudentsThatReceivedFinancialAid ON t1.StudentId = getStudentsThatReceivedFinancialAid.StudentId
    --        WHERE   Received > 0

    INSERT  INTO #FinalResult
            SELECT  RowNumber
                   ,SSN
                   ,StudentNumber
                   ,StudentName
                   ,FederalGov
                   ,StateLocalGov
                   ,InstSource
                   ,OtherSource
                   ,PellGrant
                   ,FederalLoan
                   ,NULL AS IPEDSValue
            FROM    (
                      -- DE5217 - Need to get the NetAmount from the Transactions table
                      SELECT    NEWID() AS RowNumber
                               ,dbo.UDF_FormatSSN(SSN) AS SSN
                               ,StudentNumber
                               ,LastName + ', ' + FirstName + ' ' + ISNULL(MiddleName,'') AS StudentName
                               ,(
                                  SELECT    SUM(TransAmount * -1) AS NetAmount
                                  FROM      dbo.saTransactions t1
                                  INNER JOIN saFundSources t2 ON t1.FundSourceId = t2.FundSourceId
                                  INNER JOIN arStuEnrollments t3 ON t1.StuEnrollId = t3.StuEnrollId
                                                                    AND t3.StudentId = SL.StudentId
                                  WHERE     t2.IPEDSValue = 66
                                            AND ( t1.TransDate BETWEEN @StartDate AND @EndDate )
                                            AND t1.Voided = 0
                                            AND ISNULL(t2.AdvFundSourceId,0) NOT IN ( 5,6,9 ) -- Exclude FWS
                                            AND t2.AdvFundSourceId NOT IN ( 1,19,23 )
                                            AND t3.StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
						( SELECT    t1.StuEnrollId
                          FROM      arStuEnrollments t1
                                   ,syStatusCodes t2
                          WHERE     t1.StatusCodeId = t2.StatusCodeId
                                    AND StartDate <= @AcademicEndDate
                                    AND -- Student started before the end date range
                                    LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                    AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                    AND (
                                          t1.DateDetermined < @AcademicStartDate
                                          OR ExpGradDate < @AcademicStartDate
                                          OR LDA < @AcademicStartDate
                                        ) )
                                ) AS FederalGov
                               ,(
                                  SELECT    SUM(TransAmount * -1) AS NetAmount
                                  FROM      dbo.saTransactions t1
                                  INNER JOIN saFundSources t2 ON t1.FundSourceId = t2.FundSourceId
                                  INNER JOIN arStuEnrollments t3 ON t1.StuEnrollId = t3.StuEnrollId
                                                                    AND t3.StudentId = SL.StudentId
                                  WHERE     t2.IPEDSValue = 67
                                            AND ( t1.TransDate BETWEEN @StartDate AND @EndDate )
                                            AND t1.Voided = 0
                                            AND ISNULL(t2.AdvFundSourceId,0) NOT IN ( 5,6,9 ) -- Exclude FWS -- FFEL PLUS (9)(added for DE8816)
                                            AND t2.AdvFundSourceId NOT IN ( 1,19,23 )
                                            AND t3.StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
						( SELECT    t1.StuEnrollId
                          FROM      arStuEnrollments t1
                                   ,syStatusCodes t2
                          WHERE     t1.StatusCodeId = t2.StatusCodeId
                                    AND StartDate <= @AcademicEndDate
                                    AND -- Student started before the end date range
                                    LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                    AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                    AND (
                                          t1.DateDetermined < @AcademicStartDate
                                          OR ExpGradDate < @AcademicStartDate
                                          OR LDA < @AcademicStartDate
                                        ) )
                                ) AS StateLocalGov
                               ,(
                                  SELECT    SUM(TransAmount * -1) AS NetAmount
                                  FROM      dbo.saTransactions t1
                                  INNER JOIN saFundSources t2 ON t1.FundSourceId = t2.FundSourceId
                                  INNER JOIN arStuEnrollments t3 ON t1.StuEnrollId = t3.StuEnrollId
                                                                    AND t3.StudentId = SL.StudentId
                                  WHERE     t2.IPEDSValue = 68
                                            AND ( t1.TransDate BETWEEN @StartDate AND @EndDate )
                                            AND t1.Voided = 0
                                            AND ISNULL(t2.AdvFundSourceId,0) NOT IN ( 5,6,9 ) -- Exclude FWS -- FFEL PLUS (9)(added for DE8816)
                                            AND t2.AdvFundSourceId NOT IN ( 1,19,23 )
                                            AND t3.StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
						( SELECT    t1.StuEnrollId
                          FROM      arStuEnrollments t1
                                   ,syStatusCodes t2
                          WHERE     t1.StatusCodeId = t2.StatusCodeId
                                    AND StartDate <= @AcademicEndDate
                                    AND -- Student started before the end date range
                                    LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                    AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                    AND (
                                          t1.DateDetermined < @AcademicStartDate
                                          OR ExpGradDate < @AcademicStartDate
                                          OR LDA < @AcademicStartDate
                                        ) )
                                ) AS InstSource
                               ,(
                                  SELECT    SUM(TransAmount * -1) AS NetAmount
                                  FROM      dbo.saTransactions t1
                                  INNER JOIN saFundSources t2 ON t1.FundSourceId = t2.FundSourceId
                                  INNER JOIN arStuEnrollments t3 ON t1.StuEnrollId = t3.StuEnrollId
                                                                    AND t3.StudentId = SL.StudentId
                                  WHERE     t2.IPEDSValue = 71
                                            AND ( t1.TransDate BETWEEN @StartDate AND @EndDate )
                                            AND t1.Voided = 0
                                            AND ISNULL(t2.AdvFundSourceId,0) NOT IN ( 5,6,9 ) -- Exclude FWS -- FFEL PLUS (9)(added for DE8816)
                                            AND t2.AdvFundSourceId NOT IN ( 1,19,23 )
                                            AND t3.StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
						( SELECT    t1.StuEnrollId
                          FROM      arStuEnrollments t1
                                   ,syStatusCodes t2
                          WHERE     t1.StatusCodeId = t2.StatusCodeId
                                    AND StartDate <= @AcademicEndDate
                                    AND -- Student started before the end date range
                                    LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                    AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                    AND (
                                          t1.DateDetermined < @AcademicStartDate
                                          OR ExpGradDate < @AcademicStartDate
                                          OR LDA < @AcademicStartDate
                                        ) )
                                ) AS OtherSource
                               ,(
                                  SELECT    SUM(TransAmount * -1) AS NetAmount
                                  FROM      dbo.saTransactions t1
                                  INNER JOIN saFundSources t2 ON t1.FundSourceId = t2.FundSourceId
                                  INNER JOIN arStuEnrollments t3 ON t1.StuEnrollId = t3.StuEnrollId
                                                                    AND t3.StudentId = SL.StudentId
                                  WHERE     t2.IPEDSValue = 66
                                            AND ( t1.TransDate BETWEEN @StartDate AND @EndDate )
                                            AND t1.Voided = 0
                                            AND ISNULL(t2.AdvFundSourceId,0) NOT IN ( 5,6,9 ) -- Exclude FWS -- FFEL PLUS (9)(added for DE8816)
                                            AND t2.AdvFundSourceId NOT IN ( 1,19,23 )
                                            AND t3.StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
						( SELECT    t1.StuEnrollId
                          FROM      arStuEnrollments t1
                                   ,syStatusCodes t2
                          WHERE     t1.StatusCodeId = t2.StatusCodeId
                                    AND StartDate <= @AcademicEndDate
                                    AND -- Student started before the end date range
                                    LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                    AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                    AND (
                                          t1.DateDetermined < @AcademicStartDate
                                          OR ExpGradDate < @AcademicStartDate
                                          OR LDA < @AcademicStartDate
                                        ) )
                                            AND t2.AdvFundSourceId = 2
                                ) AS PellGrant
                               ,(
                                  SELECT    SUM(TransAmount * -1) AS NetAmount
                                  FROM      dbo.saTransactions t1
                                  INNER JOIN saFundSources t2 ON t1.FundSourceId = t2.FundSourceId
                                  INNER JOIN arStuEnrollments t3 ON t1.StuEnrollId = t3.StuEnrollId
                                                                    AND t3.StudentId = SL.StudentId
                                  WHERE     --t2.IPEDSValue=70 and 
                                            ( t1.TransDate BETWEEN @StartDate AND @EndDate )
                                            AND t1.Voided = 0
                                            AND ISNULL(t2.AdvFundSourceId,0) NOT IN ( 5,6,9 ) -- Exclude FWS 
                                            AND t2.AdvFundSourceId NOT IN ( 1,19,23 )
                                            AND t3.StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
						( SELECT    t1.StuEnrollId
                          FROM      arStuEnrollments t1
                                   ,syStatusCodes t2
                          WHERE     t1.StatusCodeId = t2.StatusCodeId
                                    AND StartDate <= @AcademicEndDate
                                    AND -- Student started before the end date range
                                    LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                    AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                    AND (
                                          t1.DateDetermined < @AcademicStartDate
                                          OR ExpGradDate < @AcademicStartDate
                                          OR LDA < @AcademicStartDate
                                        ) ) 
		--And t2.AdvFundSourceId in (7,8,21)
                                            AND t2.AwardTypeId = 2
                                            AND t2.TitleIV = 1
                                ) AS FederalLoan
                      FROM      #StudentsList SL
                    ) dt
            ORDER BY CASE WHEN @OrderBy = 'SSN' THEN dt.SSN
                     END
                   ,CASE WHEN @OrderBy = 'LastName' THEN dt.StudentName
                    END
                   ,
                    -- Updated 11/27/2012 - sort by student number not working
					--Case When @OrderBy='StudentNumber' Then Convert(int,dt.StudentNumber)
                    CASE WHEN @OrderBy = 'Student Number' THEN dt.StudentNumber
                    END;
		

    SELECT  SSN
           ,StudentNumber
           ,StudentName
           ,ISNULL(SUM(FederalGov),0) AS FederalGov
           ,ISNULL(SUM(StateLocalGov),0) AS StateLocalGov
           ,ISNULL(SUM(InstSource),0) AS InstSource
           ,ISNULL(SUM(OtherSource),0) AS OtherSource
           ,ISNULL(SUM(PellGrant),0) AS PellGrant
           ,ISNULL(SUM(FederalLoan),0) AS FederalLoan
           ,CASE WHEN SUM(FederalGov) > 0 THEN 1
                 ELSE 0
            END AS FederalGovCount
           ,CASE WHEN SUM(StateLocalGov) > 0 THEN 1
                 ELSE 0
            END AS StateLocalGovCount
           ,CASE WHEN SUM(InstSource) > 0 THEN 1
                 ELSE 0
            END AS InstSourceCount
           ,CASE WHEN SUM(OtherSource) > 0 THEN 1
                 ELSE 0
            END AS OtherSourceCount
           ,CASE WHEN SUM(PellGrant) > 0 THEN 1
                 ELSE 0
            END AS PellGrantCount
           ,CASE WHEN SUM(FederalLoan) > 0 THEN 1
                 ELSE 0
            END AS FederalLoanCount
           ,CASE WHEN (
                        ISNULL(SUM(FederalGov),0) > 0
                        OR ISNULL(SUM(InstSource),0) > 0
                        OR ISNULL(SUM(StateLocalGov),0) > 0
                        OR ISNULL(SUM(OtherSource),0) > 0
                      ) THEN 1
                 ELSE 0
            END AS StudAwardedAid
    FROM    #FinalResult
    GROUP BY SSN
           ,StudentNumber
           ,StudentName
    ORDER BY CASE WHEN @OrderBy = 'SSN' THEN SSN
             END
           ,CASE WHEN @OrderBy = 'LastName' THEN StudentName
            END
           ,
		-- Updated 11/27/2012 - sort by student number not working
		--Case When @OrderBy='StudentNumber' Then Convert(int,StudentNumber)
            CASE WHEN @OrderBy = 'Student Number' THEN StudentNumber
            END;
	--Commented not used anywhere
    --DROP TABLE #UnderGraduateStudentsWhoReceivedFinancialAid
    DROP TABLE #StudentsList;
    DROP TABLE #FinalResult;



GO
