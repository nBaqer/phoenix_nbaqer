SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_MentorProctoredTestRequirement_GetList]
AS
    SELECT DISTINCT
            '1 mentor/proctored test in ' + RTRIM(CONVERT(CHAR(5),t1.MinimumScore)) + t3.Code AS MentorProctoredRequirement
           ,t1.MinimumScore
           ,t3.Code
           ,t3.Descrip
           ,CONVERT(CHAR(36),t3.GrdComponentTypeId) + ':' + RTRIM(CONVERT(CHAR(5),t1.MinimumScore)) AS MentorValue
    FROM    arRules_GradeComponentTypes_Courses t1
    INNER JOIN arBridge_GradeComponentTypes_Courses t2 ON t1.GrdComponentTypeId_ReqId = t2.GrdComponentTypeId_ReqId
    INNER JOIN arGrdComponentTypes t3 ON t2.GrdComponentTypeId = t3.GrdComponentTypeId
    ORDER BY t3.Code
           ,t1.MinimumScore;     



GO
