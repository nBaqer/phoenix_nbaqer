SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[usp_IPEDS_Spring_PartD_DetailAndSummary]
    @CampusId VARCHAR(50)
   ,@ProgId VARCHAR(4000) = NULL
   ,@CohortYear VARCHAR(10) = NULL
   ,@OrderBy VARCHAR(100)
   ,@StartDate DATETIME
   ,@EndDate DATETIME
   ,@SchoolType VARCHAR(20) = NULL
   ,  --NEW DD Rev 12/12/11
    @EndDate_Academic DATETIME = NULL
   ,@LargestProgramID VARCHAR(50) = NULL --NEW DD Rev 12/12/11
AS
    DECLARE @ReturnValue VARCHAR(100)
       ,@Value VARCHAR(100);
    DECLARE @SSN VARCHAR(10)
       ,@FirstName VARCHAR(100)
       ,@LastName VARCHAR(100)
       ,@StudentNumber VARCHAR(50)
       ,@TransferredOut INT
       ,@Exclusions INT;
    DECLARE @CitizenShip_IPEDSValue INT
       ,@ProgramType_IPEDSValue INT
       ,@Gender_IPEDSValue INT
       ,@FullTimePartTime_IPEDSValue INT
       ,@StudentId UNIQUEIDENTIFIER;
    DECLARE @ProgramName VARCHAR(100)
       ,@StudentCount INT;
    DECLARE @LargestProgramName VARCHAR(100);
    DECLARE @AcademicEndDate DATETIME;
    --12/05/2012 DE8824 - Academic year options should have a cutoff start date
    DECLARE @AcademicStartDate DATETIME;  
--DECLARE @LargestProgramID VARCHAR(50)  --NEW DD Rev 12/12/11

--SET @LargestProgramID = NULL

-- Check if School tracks grades by letter or numeric 
    SET @Value = (
                   SELECT TOP 1
                            Value
                   FROM     dbo.syConfigAppSetValues
                   WHERE    SettingId = 47
                 );


--Here, we're determining if the school type is academic. If so, we will change the new var
--@AcademicEndDate to the @EndDate_Academic, if not, than use @EndDate. We will use the new
--@AcademicEndDate variable to help select the student list.  DD Rev 01/10/12
    IF LOWER(@SchoolType) = 'academic'
        BEGIN
            SET @AcademicEndDate = @EndDate_Academic;
            --12/05/2012 DE8824 - Academic year options should have a cutoff start date
            SET @AcademicStartDate = DATEADD(YEAR,-1,@EndDate_Academic); 
        END;
    ELSE
        BEGIN
            SET @AcademicEndDate = @EndDate;
        END;
	
	
-- For Testing Purpose Only
--SET @StartDate = '0'
--if LOWER(@CohortPossible)='program reporter'
--		begin
--			-- the default report date ranges are 9/1/Cohort Year to 8/31/next year. 
--			set @StartDate = '9/1/'+@CohortYear
--			set @EndDate = '8/31/'+Convert(varchar,(Convert(int,@CohortYear)+1))
--		end
--	else
--		begin
--			-- the default report date ranges are 9/1/Cohort Year to 12/15/Cohort year. 
--			set @StartDate = '9/1/'+@CohortYear
--			set @EndDate = '12/15/'+@CohortYear
--		end

--CREATE TABLE #LargestProgram
--(LargestProgramID VARCHAR(50), LargeProgramName VARCHAR(100), StudentCount INT)

--Here, we want to get the largest program from the programs selected.
    BEGIN
        IF @LargestProgramID IS NOT NULL
            BEGIN
                SET @LargestProgramName = (
                                            SELECT  ProgDescrip
                                            FROM    dbo.arPrograms
                                            WHERE   ProgId = @LargestProgramID
                                          );
            END;
        ELSE
            BEGIN
                SET @LargestProgramName = '';
            END;
		--INSERT INTO #LargestProgram
		--	SELECT TOP 1 * FROM
		--		(
		--		SELECT t3.ProgId, t3.ProgDescrip, COUNT(t1.StuEnrollId) AS StudentCount
		--		FROM dbo.arStuEnrollments t1, dbo.arPrgVersions t2, dbo.arPrograms t3
		--		WHERE t1.PrgVerId = t2.PrgVerId AND t2.ProgId = t3.ProgId AND
		--		(@progId IS NULL or t3.ProgId IN (Select Val from [MultipleValuesForReportParameters](@ProgId,',',1))) AND
		--		LTRIM(RTRIM(t1.CampusId))= LTRIM(RTRIM(@CampusId)) AND 
	 -- t1.StartDate<=@EndDate   
  --    and StuEnrollId not in  
  --    -- Exclude students who are Dropped out/Transferred/Graduated/No Start   
  --    -- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate  
  --    (  
  --     Select t1.StuEnrollId from arStuEnrollments t1,SyStatusCodes t2, dbo.arPrgVersions t3, dbo.arPrograms t4   
  --     where t1.StatusCodeId=t2.StatusCodeId and StartDate <=@EndDate and -- Student started before the end date range  
  --     LTRIM(RTRIM(t1.CampusId))= LTRIM(RTRIM(@CampusId)) and  
  --     t1.PrgVerId = t3.PrgVerId AND t3.ProgId = t4.ProgId AND
	 --  (@progId IS NULL or t3.ProgId IN (Select Val from [MultipleValuesForReportParameters](@ProgId,',',1))) AND    
  --     t2.SysStatusId in (12,14,19,8) -- Dropped out/Transferred/Graduated/No Start  
  --     -- Date Determined or ExpGradDate or LDA falls before 08/31/2010  
  --     and (t1.DateDetermined<@StartDate or ExpGradDate<@StartDate or LDA<@StartDate)  
  --    )   
  --    -- If Student is enrolled in only one program version and if that program version   
  --    -- happens to be a continuing ed program exclude the student  
  --                      and t1.StudentId not in   
  --                                 (  
  --                                        Select Distinct StudentId from  
  --                                        (  
  --                                              select StudentId,Count(*) as RowCounter from arStuENrollments   
  --                                              where PrgVerId in (select PrgVerId from arPrgVersions where IsContinuingEd=1)  
  --                                              Group by StudentId Having Count(*)=1  
  --                                        ) dtStudent_ContinuingEd  
  --                                  )
		--		GROUP BY t3.ProgId, t3.ProgDescrip
		--		) D1
		--	ORDER BY D1.StudentCount DESC	
			
		--	SELECT * FROM #LargestProgram
        SELECT  @LargestProgramName AS LargestProgramName1;
			
    END;
    
--SET @LargestProgramID = (SELECT TOP 1 LargestProgramID FROM #LargestProgram)



    IF @ProgId IS NOT NULL
        BEGIN
            SELECT  @ReturnValue = COALESCE(@ReturnValue,'') + ProgDescrip + ','
            FROM    arPrograms t1
            WHERE   t1.ProgId IN ( SELECT   Val
                                   FROM     MultipleValuesForReportParameters(@ProgId,',',1) );
            SET @ReturnValue = SUBSTRING(@ReturnValue,1,LEN(@ReturnValue) - 1);
        END;
    ELSE
        BEGIN
            SELECT  @ReturnValue = COALESCE(@ReturnValue,'') + ProgDescrip + ','
            FROM    arPrograms t1
            WHERE   t1.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
                    AND t1.CampGrpId IN ( SELECT    CampGrpId
                                          FROM      syCmpGrpCmps
                                          WHERE     CampusId = @CampusId );
            SET @ReturnValue = SUBSTRING(@ReturnValue,1,LEN(@ReturnValue) - 1);
        END;

/*********Get the list of students that will be shown in the report - Starts Here ******************/			
    CREATE TABLE #StudentsList
        (
         StudentId UNIQUEIDENTIFIER
        ,SSN VARCHAR(10)
        ,FirstName VARCHAR(100)
        ,LastName VARCHAR(100)
        ,MiddleName VARCHAR(50)
        ,StudentNumber VARCHAR(50)
        ,TransferredOut INT
        ,Exclusions INT
        ,CitizenShip_IPEDSValue INT
        ,ProgramType_IPEDSValue INT
        ,Gender_IPEDSValue INT
        ,FullTimePartTime_IPEDSValue INT
        ,GenderId UNIQUEIDENTIFIER
        ,RaceId UNIQUEIDENTIFIER
        ,CitizenId UNIQUEIDENTIFIER
        ,GenderDescription VARCHAR(50)
        ,RaceDescription VARCHAR(50)
        ,StudentGraduatedStatusCount INT
        ,ClockHourProgramCount INT
        ,StartDate DATETIME
        ,HoustingType UNIQUEIDENTIFIER
        );
		
-- The following table will store only Full Time First Time UnderGraduate Students
    CREATE TABLE #UnderGraduateStudentsWhoReceivedFinancialAid
        (
         StudentId UNIQUEIDENTIFIER
        ,FinancialAidType INT
        ,TitleIV BIT
        );

    IF LOWER(@SchoolType) = 'academic' --Added By DD Rev 12/12/11 Run the Original SELECT
        BEGIN --Added By DD Rev 12/12/11
	-- Get the list of UnderGraduate Students
            INSERT  INTO #StudentsList
                    SELECT DISTINCT
                            t1.StudentId
                           ,t1.SSN
                           ,t1.FirstName
                           ,t1.LastName
                           ,t1.MiddleName
                           ,t1.StudentNumber
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SQ1
                                       ,syStatusCodes SQ2
                                       ,dbo.sySysStatus SQ3
                              WHERE     SQ1.StatusCodeId = SQ2.StatusCodeId
                                        AND SQ2.SysStatusId = SQ3.SysStatusId
                                        AND SQ1.StudentId = t1.StudentId
                                        AND SQ3.SysStatusId = 19
                                        AND SQ1.TransferDate <= @AcademicEndDate
                            ) AS TransferredOut
                           ,(
						-- Check if the student was either dropped and if the drop reason is either
						-- deceased, active duty, foreign aid service, church mission
                              CASE WHEN (
                                          SELECT    COUNT(*)
                                          FROM      arStuEnrollments SQ1
                                                   ,syStatusCodes SQ2
                                                   ,dbo.sySysStatus SQ3
                                                   ,arDropReasons SQ44
                                          WHERE     SQ1.StatusCodeId = SQ2.StatusCodeId
                                                    AND SQ2.SysStatusId = SQ3.SysStatusId
                                                    AND SQ1.DropReasonId = SQ44.DropReasonId
                                                    AND SQ1.StudentId = t1.StudentId
                                                    AND SQ3.SysStatusId IN ( 12 ) -- Dropped
                                                    AND SQ1.DateDetermined <= @AcademicEndDate
                                                    AND SQ44.IPEDSValue IN ( 15,16,17,18,19 )
                                        ) >= 1 THEN 1
                                   ELSE 0
                              END ) AS Exclusions
                           ,t11.IPEDSValue AS CitizenShip_IPEDSValue
                           ,t9.IPEDSValue AS ProgramType_IPEDSValue
                           ,t3.IPEDSValue AS Gender_IPEDSValue
                           ,t10.IPEDSValue AS FullTimePartTime_IPEDSValue
                           ,t1.Gender
                           ,t1.Race
                           ,t1.Citizen
                           ,(
                              SELECT DISTINCT
                                        AgencyDescrip
                              FROM      syRptAgencyFldValues
                              WHERE     RptAgencyFldValId = t3.IPEDSValue
                            ) AS GenderDescription
                           ,(
                              SELECT DISTINCT
                                        AgencyDescrip
                              FROM      syRptAgencyFldValues
                              WHERE     RptAgencyFldValId = t4.IPEDSValue
                            ) AS RaceDescription
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SEC
                                       ,syStatusCodes SC
                              WHERE     SEC.StudentId = t1.StudentId
                                        AND SEC.StatusCodeId = SC.StatusCodeId
                                        AND SC.SysStatusId = 14
                            ) AS StudentGraduatedStatusCount
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SEC
                                       ,arPrograms P
                                       ,arPrgVersions PV
                              WHERE     SEC.PrgVerId = PV.PrgVerId
                                        AND P.ProgId = PV.ProgId
                                        AND SEC.StudentId = t1.StudentId
                                        AND P.ACId = 5
                            ) AS ClockHourProgramCount
                           ,t2.StartDate
                           ,t1.HousingId
                    FROM    adGenders t3
                    LEFT JOIN arStudent t1 ON t3.GenderId = t1.Gender
                    LEFT JOIN adEthCodes t4 ON t4.EthCodeId = t1.Race
                    INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                    INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                    INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                                 AND t6.SysStatusId NOT IN ( 8 ) -- Ignore "No Start" Students
                    INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                    INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                    INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                    LEFT JOIN arAttendTypes t10 ON t2.AttendTypeId = t10.AttendTypeId
                    LEFT JOIN dbo.adDegCertSeeking t12 ON t2.DegCertSeekingId = t12.DegCertSeekingId
                    INNER JOIN adCitizenships t11 ON t1.Citizen = t11.CitizenshipId
                    WHERE   LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                            AND (
                                  @ProgId IS NULL
                                  OR t8.ProgId IN ( SELECT  Val
                                                    FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                )
                            AND
							--t10.IPEDSValue=61 AND -- Full Time
							--t12.IPEDSValue=11 AND -- First Time
                            t9.IPEDSValue = 58
                            AND -- Under Graduate
                            --12/05/2012 DE8824 - Academic year options should have a cutoff start date
                            --t2.StartDate <= @AcademicEndDate
                            (
                              t2.StartDate > @AcademicStartDate
                              AND t2.StartDate <= @AcademicEndDate
                            )
                            AND StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
							-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
							( SELECT    t1.StuEnrollId
                              FROM      arStuEnrollments t1
                                       ,syStatusCodes t2
                              WHERE     t1.StatusCodeId = t2.StatusCodeId
                                        AND StartDate <= @AcademicEndDate
                                        AND -- Student started before the end date range
                                        LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                        AND (
                                              @ProgId IS NULL
                                              OR t8.ProgId IN ( SELECT  Val
                                                                FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                            )
                                        AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
								-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                        AND (
                                              t1.DateDetermined < @AcademicEndDate
                                              OR ExpGradDate < @AcademicEndDate
                                              OR LDA < @AcademicEndDate
                                            ) ) 
                                   
						   -- If Student is enrolled in only one program version and if that program version 
						   -- happens to be a continuing ed program exclude the student
                            AND t2.StudentId NOT IN ( SELECT DISTINCT
                                                                StudentId
                                                      FROM      (
                                                                  SELECT    StudentId
                                                                           ,COUNT(*) AS RowCounter
                                                                  FROM      arStuEnrollments
                                                                  WHERE     PrgVerId IN ( SELECT    PrgVerId
                                                                                          FROM      arPrgVersions
                                                                                          WHERE     IsContinuingEd = 1 )
                                                                  GROUP BY  StudentId
                                                                  HAVING    COUNT(*) = 1
                                                                ) dtStudent_ContinuingEd );                               
        END; --Added By DD Rev 12/12/11
    ELSE --Added By DD Rev 12/12/11
        BEGIN --Added By DD Rev 12/12/11  The below SELECT/INSERT was added. There is a filter here on ProgramID. 
	--Also, when the @SchoolType = program,
	--we're going to execute the SQL below for the latgest program.
	-- Get the list of UnderGraduate Students
            INSERT  INTO #StudentsList
                    SELECT DISTINCT
                            t1.StudentId
                           ,t1.SSN
                           ,t1.FirstName
                           ,t1.LastName
                           ,t1.MiddleName
                           ,t1.StudentNumber
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SQ1
                                       ,syStatusCodes SQ2
                                       ,dbo.sySysStatus SQ3
                              WHERE     SQ1.StatusCodeId = SQ2.StatusCodeId
                                        AND SQ2.SysStatusId = SQ3.SysStatusId
                                        AND SQ1.StudentId = t1.StudentId
                                        AND SQ3.SysStatusId = 19
                                        AND SQ1.TransferDate <= @EndDate
                            ) AS TransferredOut
                           ,(
						-- Check if the student was either dropped and if the drop reason is either
						-- deceased, active duty, foreign aid service, church mission
                              CASE WHEN (
                                          SELECT    COUNT(*)
                                          FROM      arStuEnrollments SQ1
                                                   ,syStatusCodes SQ2
                                                   ,dbo.sySysStatus SQ3
                                                   ,arDropReasons SQ44
                                          WHERE     SQ1.StatusCodeId = SQ2.StatusCodeId
                                                    AND SQ2.SysStatusId = SQ3.SysStatusId
                                                    AND SQ1.DropReasonId = SQ44.DropReasonId
                                                    AND SQ1.StudentId = t1.StudentId
                                                    AND SQ3.SysStatusId IN ( 12 ) -- Dropped
                                                    AND SQ1.DateDetermined <= @EndDate
                                                    AND SQ44.IPEDSValue IN ( 15,16,17,18,19 )
                                        ) >= 1 THEN 1
                                   ELSE 0
                              END ) AS Exclusions
                           ,t11.IPEDSValue AS CitizenShip_IPEDSValue
                           ,t9.IPEDSValue AS ProgramType_IPEDSValue
                           ,t3.IPEDSValue AS Gender_IPEDSValue
                           ,t10.IPEDSValue AS FullTimePartTime_IPEDSValue
                           ,t1.Gender
                           ,t1.Race
                           ,t1.Citizen
                           ,(
                              SELECT DISTINCT
                                        AgencyDescrip
                              FROM      syRptAgencyFldValues
                              WHERE     RptAgencyFldValId = t3.IPEDSValue
                            ) AS GenderDescription
                           ,(
                              SELECT DISTINCT
                                        AgencyDescrip
                              FROM      syRptAgencyFldValues
                              WHERE     RptAgencyFldValId = t4.IPEDSValue
                            ) AS RaceDescription
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SEC
                                       ,syStatusCodes SC
                              WHERE     SEC.StudentId = t1.StudentId
                                        AND SEC.StatusCodeId = SC.StatusCodeId
                                        AND SC.SysStatusId = 14
                            ) AS StudentGraduatedStatusCount
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SEC
                                       ,arPrograms P
                                       ,arPrgVersions PV
                              WHERE     SEC.PrgVerId = PV.PrgVerId
                                        AND P.ProgId = PV.ProgId
                                        AND SEC.StudentId = t1.StudentId
                                        AND P.ACId = 5
                            ) AS ClockHourProgramCount
                           ,t2.StartDate
                           ,t1.HousingId
                    FROM    adGenders t3
                    LEFT JOIN arStudent t1 ON t3.GenderId = t1.Gender
                    LEFT JOIN adEthCodes t4 ON t4.EthCodeId = t1.Race
                    INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                    INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                    INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                                 AND t6.SysStatusId NOT IN ( 8 ) -- Ignore "No Start" Students
                    INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                    INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                    INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                    LEFT JOIN arAttendTypes t10 ON t2.AttendTypeId = t10.AttendTypeId
                    LEFT JOIN dbo.adDegCertSeeking t12 ON t2.DegCertSeekingId = t12.DegCertSeekingId
                    INNER JOIN adCitizenships t11 ON t1.Citizen = t11.CitizenshipId
                    WHERE   LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                            AND (
                                  @ProgId IS NULL
                                  OR t8.ProgId IN ( SELECT  Val
                                                    FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                )
                            AND
							--t10.IPEDSValue=61 AND -- Full Time
							--t12.IPEDSValue=11 AND -- First Time
                            t9.IPEDSValue = 58
                            AND -- Under Graduate
                            --t2.StartDate <= @EndDate
                            (
                              t2.StartDate >= @StartDate
                              AND t2.StartDate <= @EndDate
                            )
                            AND t2.PrgVerId IN ( SELECT PrgVerId
                                                 FROM   arPrgVersions
                                                 WHERE  ProgId = @LargestProgramID ) --Added By DD Rev 12/12/11
                            AND StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
							-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
							( SELECT    t1.StuEnrollId
                              FROM      arStuEnrollments t1
                                       ,syStatusCodes t2
                              WHERE     t1.StatusCodeId = t2.StatusCodeId
                                        AND StartDate <= @EndDate
                                        AND -- Student started before the end date range
                                        LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                        AND (
                                              @ProgId IS NULL
                                              OR t8.ProgId IN ( SELECT  Val
                                                                FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                            )
                                        AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
								-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                        AND (
                                              t1.DateDetermined < @StartDate
                                              OR ExpGradDate < @StartDate
                                              OR LDA < @StartDate
                                            ) ) 
						   -- If Student is enrolled in only one program version and if that program version 
						   -- happens to be a continuing ed program exclude the student
                            AND t2.StudentId NOT IN ( SELECT DISTINCT
                                                                StudentId
                                                      FROM      (
                                                                  SELECT    StudentId
                                                                           ,COUNT(*) AS RowCounter
                                                                  FROM      arStuEnrollments
                                                                  WHERE     PrgVerId IN ( SELECT    PrgVerId
                                                                                          FROM      arPrgVersions
                                                                                          WHERE     IsContinuingEd = 1 )
                                                                  GROUP BY  StudentId
                                                                  HAVING    COUNT(*) = 1
                                                                ) dtStudent_ContinuingEd );                               
        END; --Added By DD Rev 12/12/11
                    
/*********Get the list of students that will be shown in the report - Ends Here *****************8*/

-- Get all Full Time, First Time UnderGraduate Students
-- Use #StudentsList as it pulls UnderGraduate Students
    INSERT  INTO #UnderGraduateStudentsWhoReceivedFinancialAid
            SELECT  t1.StudentId
                   ,FinancialAidType
                   ,TitleIV
            FROM    #StudentsList t1
            INNER JOIN (
                         SELECT SE.StudentId
                               ,0 AS Balance
                               ,SUM(TransAmount * -1) AS Received
                               ,FS.IPEDSValue AS FinancialAidType
                               ,FS.TitleIV AS TitleIV
                         FROM   saTransactions SA
                         INNER JOIN dbo.arStuEnrollments SE ON SA.StuEnrollId = SE.StuEnrollId
                         INNER JOIN saFundSources FS ON SA.FundSourceId = FS.FundSourceId
                         LEFT JOIN arAttendTypes t10 ON SE.attendtypeid = t10.AttendTypeId
                         LEFT JOIN dbo.adDegCertSeeking t12 ON SE.degcertseekingid = t12.DegCertSeekingId
                         WHERE  SE.StudentId IN ( SELECT DISTINCT
                                                            StudentId
                                                  FROM      #StudentsList )
                                AND -- UnderGraduates
                                t10.IPEDSValue = 61
                                AND -- Full Time 
                                t12.IPEDSValue = 11 -- First Time
                                AND SA.Voided = 0
                                AND FS.AdvFundSourceId NOT IN ( 1,19,23 )
                         GROUP BY SE.StudentId
                               ,FS.IPEDSValue
                               ,FS.TitleIV
                       ) getStudentsThatReceivedFinancialAid ON t1.StudentId = getStudentsThatReceivedFinancialAid.StudentId
            WHERE   Received > 0;


    SELECT  RowNumber
           ,SSN
           ,StudentNumber
           ,StudentName
           ,CASE WHEN Group3CountCY >= 1 THEN 'X'
                 ELSE ''
            END AS Group3CountCYX
           ,Group3CountCY
           ,CASE WHEN OnCampusCountCY >= 1 THEN 'X'
                 ELSE ''
            END AS OnCampusCountCYX
           ,OnCampusCountCY
           ,CASE WHEN OffCampusCountCY >= 1 THEN 'X'
                 ELSE ''
            END AS OffCampusCountCYX
           ,OffCampusCountCY
           ,CASE WHEN WithParentCountCY >= 1 THEN 'X'
                 ELSE ''
            END AS WithParentCountCYX
           ,WithParentCountCY
           ,FederalGovCY
           ,StateLocalGovCY
           ,InstSourceCY
           ,CASE WHEN Group3Count1YP >= 1 THEN 'X'
                 ELSE ''
            END AS Group3Count1YPX
           ,Group3Count1YP
           ,CASE WHEN OnCampusCount1PY >= 1 THEN 'X'
                 ELSE ''
            END AS OnCampusCount1PYX
           ,OnCampusCount1PY
           ,CASE WHEN OffCampusCount1PY >= 1 THEN 'X'
                 ELSE ''
            END AS OffCampusCount1PYX
           ,OffCampusCount1PY
           ,CASE WHEN WithParentCount1PY >= 1 THEN 'X'
                 ELSE ''
            END AS WithParentCount1PYX
           ,WithParentCount1PY
           ,FederalGov1YP
           ,StateLocalGov1YP
           ,InstSource1YP
           ,CASE WHEN Group3Count2YP >= 1 THEN 'X'
                 ELSE ''
            END AS Group3Count2YPX
           ,Group3Count2YP
           ,CASE WHEN OnCampusCount2PY >= 1 THEN 'X'
                 ELSE ''
            END AS OnCampusCount2PYX
           ,OnCampusCount2PY
           ,CASE WHEN OffCampusCount2PY >= 1 THEN 'X'
                 ELSE ''
            END AS OffCampusCount2PYX
           ,OffCampusCount2PY
           ,CASE WHEN WithParentCount2PY >= 1 THEN 'X'
                 ELSE ''
            END AS WithParentCount2PYX
           ,WithParentCount2PY
           ,FederalGov2YP
           ,StateLocalGov2YP
           ,InstSource2YP
           ,LargestProgramName
    INTO    #Result
    FROM    (
              SELECT    NEWID() AS RowNumber
                       ,dbo.UDF_FormatSSN(SSN) AS SSN
                       ,StudentNumber
                       ,LastName + ', ' + FirstName + ' ' + ISNULL(MiddleName,'') AS StudentName
                       ,
		
		
		-- From FULL TIME First Time UnderGraduates Student List, Select Students who received those who received any Federal Work Study, loans to students, 
		-- or grant or scholarship aid from the federal government, state/local government, the institution, or other sources known to the institution. 
		
		--FinancialAidType (66) - Federal grants(grants/educational assistance funds)
		--FinancialAidType (67) - State/local government grants(grants/scholarships/waivers)
		--FinancialAidType (68) - Institutional grants
		--FinancialAidType (69) - Private grants or scholarships
		--FinancialAidType (70) - Loans to students
		--FinancialAidType (71) - Other sources
		-- Values are coming from saFundSources - IPEDSValue Field
                        (
                          SELECT    COUNT(DISTINCT t2.StudentId)
                          FROM      arStuEnrollments t2
                          INNER JOIN #UnderGraduateStudentsWhoReceivedFinancialAid t3 ON t2.StudentId = t3.StudentId
                                                                                         AND t2.StudentId = SL.StudentId
                          WHERE     t3.FinancialAidType IN ( 66,67,68 )
                                    AND SL.StartDate >= @StartDate
                                    AND SL.StartDate <= @EndDate
                        ) AS Group3CountCY
                       ,(
                          SELECT    COUNT(DISTINCT t2.StudentId)
                          FROM      arStuEnrollments t2
                          INNER JOIN #UnderGraduateStudentsWhoReceivedFinancialAid t3 ON t2.StudentId = t3.StudentId
                                                                                         AND t2.StudentId = SL.StudentId
                          INNER JOIN arHousing H ON SL.HoustingType = H.HousingId
                                                    AND H.IPEDSValue = 35
                          WHERE     t3.FinancialAidType IN ( 66,67,68 )
                                    AND SL.StartDate >= @StartDate
                                    AND SL.StartDate <= @EndDate
                        ) AS OnCampusCountCY
                       ,(
                          SELECT    COUNT(DISTINCT t2.StudentId)
                          FROM      arStuEnrollments t2
                          INNER JOIN #UnderGraduateStudentsWhoReceivedFinancialAid t3 ON t2.StudentId = t3.StudentId
                                                                                         AND t2.StudentId = SL.StudentId
                          INNER JOIN arHousing H ON SL.HoustingType = H.HousingId
                                                    AND H.IPEDSValue = 36
                          WHERE     t3.FinancialAidType IN ( 66,67,68 )
                                    AND SL.StartDate >= @StartDate
                                    AND SL.StartDate <= @EndDate
                        ) AS OffCampusCountCY
                       ,(
                          SELECT    COUNT(DISTINCT t2.StudentId)
                          FROM      arStuEnrollments t2
                          INNER JOIN #UnderGraduateStudentsWhoReceivedFinancialAid t3 ON t2.StudentId = t3.StudentId
                                                                                         AND t2.StudentId = SL.StudentId
                          INNER JOIN arHousing H ON SL.HoustingType = H.HousingId
                                                    AND H.IPEDSValue = 37
                          WHERE     t3.FinancialAidType IN ( 66,67,68 )
                                    AND SL.StartDate >= @StartDate
                                    AND SL.StartDate <= @EndDate
                        ) AS WithParentCountCY
                       ,( COALESCE((
                                     SELECT SUM(TransAmount * -1)
                                     FROM   saTransactions T
                                           ,dbo.saFundSources FS
                                           ,#UnderGraduateStudentsWhoReceivedFinancialAid UG
                                     WHERE  T.FundSourceId = FS.FundSourceId
                                            AND T.Voided = 0
                                            AND T.StuEnrollId = SE.StuEnrollId
                                            AND UG.StudentId = SL.StudentId
                                            AND FS.IPEDSValue = 66
                                            AND SL.StartDate >= @StartDate
                                            AND SL.StartDate <= @EndDate
                                            AND FS.AdvFundSourceId NOT IN ( 1,19,23 )
                                   ),0) ) AS FederalGovCY
                       ,( COALESCE((
                                     SELECT SUM(TransAmount * -1)
                                     FROM   saTransactions T
                                           ,dbo.saFundSources FS
                                           ,#UnderGraduateStudentsWhoReceivedFinancialAid UG
                                     WHERE  T.FundSourceId = FS.FundSourceId
                                            AND T.Voided = 0
                                            AND T.StuEnrollId = SE.StuEnrollId
                                            AND UG.StudentId = SL.StudentId
                                            AND FS.IPEDSValue = 67
                                            AND SL.StartDate >= @StartDate
                                            AND SL.StartDate <= @EndDate
                                            AND FS.AdvFundSourceId NOT IN ( 1,19,23 )
                                   ),0) ) AS StateLocalGovCY
                       ,( COALESCE((
                                     SELECT SUM(TransAmount * -1)
                                     FROM   saTransactions T
                                           ,dbo.saFundSources FS
                                           ,#UnderGraduateStudentsWhoReceivedFinancialAid UG
                                     WHERE  T.FundSourceId = FS.FundSourceId
                                            AND T.Voided = 0
                                            AND T.StuEnrollId = SE.StuEnrollId
                                            AND UG.StudentId = SL.StudentId
                                            AND FS.IPEDSValue = 68
                                            AND SL.StartDate >= @StartDate
                                            AND SL.StartDate <= @EndDate
                                            AND FS.AdvFundSourceId NOT IN ( 1,19,23 )
                                   ),0) ) AS InstSourceCY
                       ,(
                          SELECT    COUNT(DISTINCT t2.StudentId)
                          FROM      arStuEnrollments t2
                          INNER JOIN #UnderGraduateStudentsWhoReceivedFinancialAid t3 ON t2.StudentId = t3.StudentId
                                                                                         AND t2.StudentId = SL.StudentId
                          WHERE     t3.FinancialAidType IN ( 66,67,68 )
                                    AND SL.StartDate >= DATEADD(YEAR,-1,@StartDate)
                                    AND SL.StartDate <= DATEADD(YEAR,-1,@EndDate)
                        ) AS Group3Count1YP
                       ,(
                          SELECT    COUNT(DISTINCT t2.StudentId)
                          FROM      arStuEnrollments t2
                          INNER JOIN #UnderGraduateStudentsWhoReceivedFinancialAid t3 ON t2.StudentId = t3.StudentId
                                                                                         AND t2.StudentId = SL.StudentId
                          INNER JOIN arHousing H ON SL.HoustingType = H.HousingId
                                                    AND H.IPEDSValue = 35
                          WHERE     t3.FinancialAidType IN ( 66,67,68 )
                                    AND SL.StartDate >= DATEADD(YEAR,-1,@StartDate)
                                    AND SL.StartDate <= DATEADD(YEAR,-1,@EndDate)
                        ) AS OnCampusCount1PY
                       ,(
                          SELECT    COUNT(DISTINCT t2.StudentId)
                          FROM      arStuEnrollments t2
                          INNER JOIN #UnderGraduateStudentsWhoReceivedFinancialAid t3 ON t2.StudentId = t3.StudentId
                                                                                         AND t2.StudentId = SL.StudentId
                          INNER JOIN arHousing H ON SL.HoustingType = H.HousingId
                                                    AND H.IPEDSValue = 36
                          WHERE     t3.FinancialAidType IN ( 66,67,68 )
                                    AND SL.StartDate >= DATEADD(YEAR,-1,@StartDate)
                                    AND SL.StartDate <= DATEADD(YEAR,-1,@EndDate)
                        ) AS OffCampusCount1PY
                       ,(
                          SELECT    COUNT(DISTINCT t2.StudentId)
                          FROM      arStuEnrollments t2
                          INNER JOIN #UnderGraduateStudentsWhoReceivedFinancialAid t3 ON t2.StudentId = t3.StudentId
                                                                                         AND t2.StudentId = SL.StudentId
                          INNER JOIN arHousing H ON SL.HoustingType = H.HousingId
                                                    AND H.IPEDSValue = 37
                          WHERE     t3.FinancialAidType IN ( 66,67,68 )
                                    AND SL.StartDate >= DATEADD(YEAR,-1,@StartDate)
                                    AND SL.StartDate <= DATEADD(YEAR,-1,@EndDate)
                        ) AS WithParentCount1PY
                       ,( COALESCE((
                                     SELECT SUM(TransAmount * -1)
                                     FROM   saTransactions T
                                           ,dbo.saFundSources FS
                                           ,#UnderGraduateStudentsWhoReceivedFinancialAid UG
                                     WHERE  T.FundSourceId = FS.FundSourceId
                                            AND T.Voided = 0
                                            AND T.StuEnrollId = SE.StuEnrollId
                                            AND UG.StudentId = SL.StudentId
                                            AND FS.IPEDSValue = 66
                                            AND FS.AdvFundSourceId NOT IN ( 1,19,23 )
                                            AND SL.StartDate >= DATEADD(YEAR,-1,@StartDate)
                                            AND SL.StartDate <= DATEADD(YEAR,-1,@EndDate)
                                   ),0) ) AS FederalGov1YP
                       ,( COALESCE((
                                     SELECT SUM(TransAmount * -1)
                                     FROM   saTransactions T
                                           ,dbo.saFundSources FS
                                           ,#UnderGraduateStudentsWhoReceivedFinancialAid UG
                                     WHERE  T.FundSourceId = FS.FundSourceId
                                            AND T.Voided = 0
                                            AND T.StuEnrollId = SE.StuEnrollId
                                            AND UG.StudentId = SL.StudentId
                                            AND FS.IPEDSValue = 67
                                            AND FS.AdvFundSourceId NOT IN ( 1,19,23 )
                                            AND SL.StartDate >= DATEADD(YEAR,-1,@StartDate)
                                            AND SL.StartDate <= DATEADD(YEAR,-1,@EndDate)
                                   ),0) ) AS StateLocalGov1YP
                       ,( COALESCE((
                                     SELECT SUM(TransAmount * -1)
                                     FROM   saTransactions T
                                           ,dbo.saFundSources FS
                                           ,#UnderGraduateStudentsWhoReceivedFinancialAid UG
                                     WHERE  T.FundSourceId = FS.FundSourceId
                                            AND T.Voided = 0
                                            AND T.StuEnrollId = SE.StuEnrollId
                                            AND UG.StudentId = SL.StudentId
                                            AND FS.IPEDSValue = 68
                                            AND FS.AdvFundSourceId NOT IN ( 1,19,23 )
                                            AND SL.StartDate >= DATEADD(YEAR,-1,@StartDate)
                                            AND SL.StartDate <= DATEADD(YEAR,-1,@EndDate)
                                   ),0) ) AS InstSource1YP
                       ,(
                          SELECT    COUNT(DISTINCT t2.StudentId)
                          FROM      arStuEnrollments t2
                          INNER JOIN #UnderGraduateStudentsWhoReceivedFinancialAid t3 ON t2.StudentId = t3.StudentId
                                                                                         AND t2.StudentId = SL.StudentId
                          WHERE     t3.FinancialAidType IN ( 66,67,68 )
                                    AND SL.StartDate >= DATEADD(YEAR,-2,@StartDate)
                                    AND SL.StartDate <= DATEADD(YEAR,-2,@EndDate)
                        ) AS Group3Count2YP
                       ,(
                          SELECT    COUNT(DISTINCT t2.StudentId)
                          FROM      arStuEnrollments t2
                          INNER JOIN #UnderGraduateStudentsWhoReceivedFinancialAid t3 ON t2.StudentId = t3.StudentId
                                                                                         AND t2.StudentId = SL.StudentId
                          INNER JOIN arHousing H ON SL.HoustingType = H.HousingId
                                                    AND H.IPEDSValue = 35
                          WHERE     t3.FinancialAidType IN ( 66,67,68 )
                                    AND SL.StartDate >= DATEADD(YEAR,-2,@StartDate)
                                    AND SL.StartDate <= DATEADD(YEAR,-2,@EndDate)
                        ) AS OnCampusCount2PY
                       ,(
                          SELECT    COUNT(DISTINCT t2.StudentId)
                          FROM      arStuEnrollments t2
                          INNER JOIN #UnderGraduateStudentsWhoReceivedFinancialAid t3 ON t2.StudentId = t3.StudentId
                                                                                         AND t2.StudentId = SL.StudentId
                          INNER JOIN arHousing H ON SL.HoustingType = H.HousingId
                                                    AND H.IPEDSValue = 36
                          WHERE     t3.FinancialAidType IN ( 66,67,68 )
                                    AND SL.StartDate >= DATEADD(YEAR,-2,@StartDate)
                                    AND SL.StartDate <= DATEADD(YEAR,-2,@EndDate)
                        ) AS OffCampusCount2PY
                       ,(
                          SELECT    COUNT(DISTINCT t2.StudentId)
                          FROM      arStuEnrollments t2
                          INNER JOIN #UnderGraduateStudentsWhoReceivedFinancialAid t3 ON t2.StudentId = t3.StudentId
                                                                                         AND t2.StudentId = SL.StudentId
                          INNER JOIN arHousing H ON SL.HoustingType = H.HousingId
                                                    AND H.IPEDSValue = 37
                          WHERE     t3.FinancialAidType IN ( 66,67,68 )
                                    AND SL.StartDate >= DATEADD(YEAR,-2,@StartDate)
                                    AND SL.StartDate <= DATEADD(YEAR,-2,@EndDate)
                        ) AS WithParentCount2PY
                       ,( COALESCE((
                                     SELECT SUM(TransAmount * -1)
                                     FROM   saTransactions T
                                           ,dbo.saFundSources FS
                                           ,#UnderGraduateStudentsWhoReceivedFinancialAid UG
                                     WHERE  T.FundSourceId = FS.FundSourceId
                                            AND T.Voided = 0
                                            AND T.StuEnrollId = SE.StuEnrollId
                                            AND UG.StudentId = SL.StudentId
                                            AND FS.IPEDSValue = 66
                                            AND FS.AdvFundSourceId NOT IN ( 1,19,23 )
                                            AND SL.StartDate >= DATEADD(YEAR,-2,@StartDate)
                                            AND SL.StartDate <= DATEADD(YEAR,-2,@EndDate)
                                   ),0) ) AS FederalGov2YP
                       ,( COALESCE((
                                     SELECT SUM(TransAmount * -1)
                                     FROM   saTransactions T
                                           ,dbo.saFundSources FS
                                           ,#UnderGraduateStudentsWhoReceivedFinancialAid UG
                                     WHERE  T.FundSourceId = FS.FundSourceId
                                            AND T.Voided = 0
                                            AND T.StuEnrollId = SE.StuEnrollId
                                            AND UG.StudentId = SL.StudentId
                                            AND FS.IPEDSValue = 67
                                            AND FS.AdvFundSourceId NOT IN ( 1,19,23 )
                                            AND SL.StartDate >= DATEADD(YEAR,-2,@StartDate)
                                            AND SL.StartDate <= DATEADD(YEAR,-2,@EndDate)
                                   ),0) ) AS StateLocalGov2YP
                       ,( COALESCE((
                                     SELECT SUM(TransAmount * -1)
                                     FROM   saTransactions T
                                           ,dbo.saFundSources FS
                                           ,#UnderGraduateStudentsWhoReceivedFinancialAid UG
                                     WHERE  T.FundSourceId = FS.FundSourceId
                                            AND T.Voided = 0
                                            AND T.StuEnrollId = SE.StuEnrollId
                                            AND UG.StudentId = SL.StudentId
                                            AND FS.IPEDSValue = 68
                                            AND FS.AdvFundSourceId NOT IN ( 1,19,23 )
                                            AND SL.StartDate >= DATEADD(YEAR,-2,@StartDate)
                                            AND SL.StartDate <= DATEADD(YEAR,-2,@EndDate)
                                   ),0) ) AS InstSource2YP
                       ,@LargestProgramName AS LargestProgramName
              FROM      #StudentsList SL
                       ,arStuEnrollments SE
              WHERE     SE.StudentId = SL.StudentId
            ) dt
    ORDER BY CASE WHEN @OrderBy = 'SSN' THEN dt.SSN
             END
           ,CASE WHEN @OrderBy = 'LastName' THEN dt.StudentName
            END
           ,
		-- Updated 11/27/2012 - sort by student number not working
		--Case When @OrderBy='StudentNumber' Then Convert(int,dt.StudentNumber)
            CASE WHEN @OrderBy = 'Student Number' THEN dt.StudentNumber
            END;


    SELECT  SSN
           ,StudentNumber
           ,StudentName
           ,Group3CountCYX
           ,Group3CountCY
           ,OnCampusCountCYX
           ,OnCampusCountCY
           ,OffCampusCountCYX
           ,OffCampusCountCY
           ,WithParentCountCYX
           ,WithParentCountCY
           ,SUM(FederalGovCY) AS FederalGovCY
           ,SUM(StateLocalGovCY) AS StateLocalGovCY
           ,SUM(InstSourceCY) AS InstSourceCY
           ,Group3Count1YPX
           ,Group3Count1YP
           ,OnCampusCount1PYX
           ,OnCampusCount1PY
           ,OffCampusCount1PYX
           ,OffCampusCount1PY
           ,WithParentCount1PYX
           ,WithParentCount1PY
           ,SUM(FederalGov1YP) AS FederalGov1YP
           ,SUM(StateLocalGov1YP) AS StateLocalGov1YP
           ,SUM(InstSource1YP) AS InstSource1YP
           ,Group3Count2YPX
           ,Group3Count2YP
           ,OnCampusCount2PYX
           ,OnCampusCount2PY
           ,OffCampusCount2PYX
           ,OffCampusCount2PY
           ,WithParentCount2PYX
           ,WithParentCount2PY
           ,SUM(FederalGov2YP) AS FederalGov2YP
           ,SUM(StateLocalGov2YP) AS StateLocalGov2YP
           ,SUM(InstSource2YP) AS InstSource2YP
           ,@LargestProgramName AS LargestProgramName
    FROM    #Result
    GROUP BY SSN
           ,StudentNumber
           ,StudentName
           ,Group3CountCYX
           ,Group3CountCY
           ,OnCampusCountCYX
           ,OnCampusCountCY
           ,OffCampusCountCYX
           ,OffCampusCountCY
           ,WithParentCountCYX
           ,WithParentCountCY
           ,Group3Count1YPX
           ,Group3Count1YP
           ,OnCampusCount1PYX
           ,OnCampusCount1PY
           ,OffCampusCount1PYX
           ,OffCampusCount1PY
           ,WithParentCount1PYX
           ,WithParentCount1PY
           ,Group3Count2YPX
           ,Group3Count2YP
           ,OnCampusCount2PYX
           ,OnCampusCount2PY
           ,OffCampusCount2PYX
           ,OffCampusCount2PY
           ,WithParentCount2PYX
           ,WithParentCount2PY
           ,LargestProgramName;


		 
    DROP TABLE #UnderGraduateStudentsWhoReceivedFinancialAid;
    DROP TABLE #StudentsList;
    DROP TABLE #Result;
--DROP TABLE #LargestProgram



GO
