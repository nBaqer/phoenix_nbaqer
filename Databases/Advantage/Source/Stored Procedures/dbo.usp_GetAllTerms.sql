SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_GetAllTerms]
    @showActiveOnly BIT
   ,@campusId VARCHAR(50)
AS
    SET NOCOUNT ON;

    IF @showActiveOnly = 1
        BEGIN
            EXEC usp_GetAllActiveTerms @campusId;
        END;
    ELSE
        BEGIN
            EXEC usp_GetAllActiveAndInActiveTerms @campusId;
        END;
    








GO
