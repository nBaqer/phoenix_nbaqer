SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[USP_SA_getTransactionActivityReport]
    (
     @CampusId AS VARCHAR(200)
    ,@StartDate AS DATETIME
    ,@EndDate AS DATETIME
    ,@ShowSSN AS BIT = 0
    ,@OrderBY AS BIT = 1
    ,@TransTypes AS VARCHAR(8000)
    )
AS /*----------------------------------------------------------------------------------------------------
	Author          :	Janet Robinson
	
	Create date		:	01/31/2012
	
	Procedure Name	:	[USP_SA_getTransactionActivityReport]

	Objective		:	Gets the Transaction Activity Report
		
	Parameters		:	Name			Type	Data Type			Required? 	
						=====			====	=========			=========	
						@CampusId       Input   VARCHAR(200)            Y 
						@StartDate      Input   DATETIME		        Y
						@EndDate   	    Input   DATETIME		        Y   
						@ShowSSN        Input   BIT                     N 
						@OrderBY        Input   BIT                     N    0=Detail Report)                   
						@TransTypes     Input   VARCHAR(8000)           Y 
					
	
	Output			:	Returns 1 dataset - transaction activity info 	
						
*/----------------------------------------------------------------------------------------------------
    BEGIN
	
	
        SELECT  RecordType
               ,StudentId
               ,StudentIdentifier
               ,StudentName
               ,CheckNumber
               ,PaymentTypeId
               ,TransactionId
               ,StuEnrollId
               ,TC
               ,TransCodeDescrip
               ,TransDescrip
               ,TransDate
               ,TransAmount
               ,TransReference
               ,TransTypeId
               ,TransTypeDescrip
               ,CampusId
        FROM    (
                  SELECT    RecordType
                           ,StudentId
                           ,(
                              SELECT    CASE @ShowSSN
                                          WHEN 0 THEN StudentNumber
                                          ELSE SSN
                                        END
                            ) AS StudentIdentifier
                           ,StudentName
                           ,CASE PaymentTypeId
                              WHEN 2 THEN 'Check Number: ' + CheckNumber
                              WHEN 3 THEN 'C/C Authorization: ' + CheckNumber
                              WHEN 4 THEN 'EFT Number: ' + CheckNumber
                              WHEN 5 THEN 'Money Order Number: ' + CheckNumber
                              WHEN 6 THEN 'Non Cash Reference #: ' + CheckNumber
                              ELSE CheckNumber
                            END AS CheckNumber
                           ,PaymentTypeId
                           ,TransactionId
                           ,StuEnrollId
                           ,TC
                           ,TransCodeDescrip
                           ,TransDescrip
                           ,TransDate
                           ,TransAmount
                           ,TransReference
                           ,TransTypeId
                           ,TransTypeDescrip
                           ,CampusId
                  FROM      (
                              SELECT    (
                                          SELECT    COUNT(*)
                                          FROM      saAppliedPayments
                                          WHERE     TransactionId = T.TransactionId
                                        ) AS RecordType
                                       ,(
                                          SELECT    StudentId
                                          FROM      arStuEnrollments
                                          WHERE     StuEnrollId = T.StuEnrollId
                                        ) StudentId
                                       ,'' AS StudentIdentifier
                                       ,(
                                          SELECT    StudentNumber
                                          FROM      arStudent
                                          WHERE     StudentId = (
                                                                  SELECT    StudentId
                                                                  FROM      arStuEnrollments
                                                                  WHERE     StuEnrollId = T.StuEnrollId
                                                                )
                                        ) StudentNumber
                                       ,(
                                          SELECT    SSN
                                          FROM      arStudent
                                          WHERE     StudentId = (
                                                                  SELECT    StudentId
                                                                  FROM      arStuEnrollments
                                                                  WHERE     StuEnrollId = T.StuEnrollId
                                                                )
                                        ) SSN
                                       ,(
                                          SELECT    ( LastName + ', ' + FirstName + ISNULL(' ' + MiddleName,'') ) AS StudentName
                                          FROM      arStudent
                                          WHERE     StudentId = (
                                                                  SELECT    StudentId
                                                                  FROM      arStuEnrollments
                                                                  WHERE     StuEnrollId = T.StuEnrollId
                                                                )
                                        ) StudentName
                                       ,ISNULL((
                                                 SELECT CheckNumber
                                                 FROM   saPayments
                                                 WHERE  TransactionId = T.TransactionId
                                               ),NULL) CheckNumber
                                       ,ISNULL((
                                                 SELECT PaymentTypeId
                                                 FROM   saPayments
                                                 WHERE  TransactionId = T.TransactionId
                                               ),NULL) PaymentTypeId
                                       ,T.TransactionId
                                       ,T.StuEnrollId
                                       ,(
                                          SELECT    PrgVerDescrip
                                          FROM      arPrgVersions
                                          WHERE     PrgVerId = (
                                                                 SELECT PrgVerId
                                                                 FROM   arStuEnrollments
                                                                 WHERE  StuEnrollId = T.StuEnrollId
                                                               )
                                        ) AS EnrollmentDescrip
                                       ,CASE T.TransTypeId
                                          WHEN 2 THEN T.PaymentCodeId
                                          ELSE T.TransCodeId
                                        END AS TC
                                       ,CASE T.TransTypeId
                                          WHEN 2 THEN (
                                                        SELECT TOP 1
                                                                TransCodeDescrip
                                                        FROM    saTransCodes
                                                        WHERE   TransCodeId = T.PaymentCodeId
                                                      )
                                          ELSE (
                                                 SELECT TOP 1
                                                        TransCodeDescrip
                                                 FROM   saTransCodes
                                                 WHERE  TransCodeId = T.TransCodeId
                                               )
                                        END AS TransCodeDescrip
                                       ,T.TransDescrip
                                       ,T.TransDate
                                       ,T.TransAmount
                                       ,T.TransReference
                                       ,T.AcademicYearId
                                       ,(
                                          SELECT    AcademicYearDescrip
                                          FROM      saAcademicYears
                                          WHERE     AcademicYearId = T.AcademicYearId
                                        ) AS AcademicYearDescrip
                                       ,T.TermId
                                       ,(
                                          SELECT    TermDescrip
                                          FROM      arTerm
                                          WHERE     TermId = T.TermId
                                        ) AS TermDescrip
                                       ,T.TransTypeId
                                       ,(
                                          SELECT    CASE T.TransTypeId
                                                      WHEN 0 THEN 'Charge'
                                                      WHEN 2 THEN 'Payment'
                                                      WHEN 1 THEN 'Adjustment'
                                                    END
                                        ) AS TransTypeDescrip
                                       ,T.Voided
                                       ,T.CampusId
                                       ,T.ModUser
                                       ,T.ModDate
                              FROM      saTransactions T
                              WHERE     (
                                          CONVERT(VARCHAR(10),T.TransDate,101) >= @StartDate
                                          AND CONVERT(VARCHAR(10),T.TransDate,101) <= @EndDate
                                        )
                                        AND T.CampusId = @CampusId
                                        AND T.IsPosted = 1
                                        AND T.Voided = 0
                                        AND (
                                              T.TransCodeId IN ( SELECT DISTINCT
                                                                        TC.TransCodeId
                                                                 FROM   saTransCodes TC
                                                                 WHERE  TC.TransCodeId IN ( SELECT  strval
                                                                                            FROM    dbo.SPLIT(@TransTypes) ) )
                                              OR T.PaymentCodeId IN ( SELECT DISTINCT
                                                                                TC1.TransCodeId
                                                                      FROM      saTransCodes TC1
                                                                      WHERE     TC1.TransCodeId IN ( SELECT strval
                                                                                                     FROM   dbo.SPLIT(@TransTypes) ) )
                                            )
                            ) TP
                ) AS result1
        ORDER BY CASE WHEN @OrderBY = 1 THEN TransCodeDescrip
                 END
               ,StudentName
               ,TransDate;
		

    END;




GO
