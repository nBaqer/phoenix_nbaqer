SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[USP_FASAPChkResults_getList]
    (
        @CampGrpId VARCHAR(MAX)
       ,@PrgVerId VARCHAR(MAX)
       ,@StatusCodeId VARCHAR(MAX) = NULL
       ,@StartDate AS DATETIME = NULL
       ,@CampusId VARCHAR(MAX)
    )
AS
    BEGIN

        SELECT DISTINCT 
                       pv.PrgVerId
                       ,pv.PrgVerDescrip
                       ,e.CampusId
                       ,(
                        SELECT CampDescrip
                        FROM   syCampuses C
                        WHERE  C.CampusId = e.CampusId
                        ) AS CampDescrip
                       
						
        FROM   dbo.arFASAPChkResults fasap
        JOIN   dbo.arStuEnrollments e ON e.StuEnrollId = fasap.StuEnrollId
        JOIN   dbo.arStudent s ON e.StudentId = s.StudentId
        JOIN   dbo.arSAPDetails d ON d.SAPDetailId = fasap.SAPDetailId
        JOIN   dbo.arPrgVersions pv ON pv.PrgVerId = e.PrgVerId
    --JOIN dbo.syCmpGrpCmps cgc ON cgc.CampusId = e.CampusId
    --JOIN dbo.syCampGrps cg ON cg.CampGrpId = cgc.CampGrpId
      WHERE    
    fasap.PreviewSapCheck = 0
                AND (
                    @CampusId IS NULL
                    OR ( e.CampusId IN (
                                                      SELECT Val
                                                      FROM   MultipleValuesForReportParameters(@CampusId, ',', 1)
                                                      )
                       )
                    )
                AND (
                    @PrgVerId IS NULL
                    OR ( e.PrgVerId IN (
                                                      SELECT Val
                                                      FROM   MultipleValuesForReportParameters(@PrgVerId, ',', 1)
                                                      )
                       )
                    )
                AND (
                    @StatusCodeId IS NULL
                    OR ( e.StatusCodeId IN (
                                                          SELECT Val
                                                          FROM   MultipleValuesForReportParameters(@StatusCodeId, ',', 1)
                                                          )
                       )
                    )
                AND (
                    @StartDate IS NULL
                    OR e.StartDate <= @StartDate
                    )
		GROUP BY e.CampusId,
		pv.PrgVerId,
		pv.PrgVerDescrip
       ORDER BY e.CampusId;

    END;




GO
