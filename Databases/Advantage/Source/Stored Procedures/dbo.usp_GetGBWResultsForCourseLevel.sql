SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_GetGBWResultsForCourseLevel]
    (
     @stuEnrollId UNIQUEIDENTIFIER
    ,@clsSectionId UNIQUEIDENTIFIER
 

    )
AS
    SET NOCOUNT ON;
    SELECT  A.InstrGrdBkWgtDetailId
           ,A.Score
           ,A.ResNum
           ,B.Weight
           ,B.GrdPolicyId
           ,B.Parameter
           ,A.PostDate
    FROM    arGrdBkResults A
           ,arGrdBkWgtDetails B
    WHERE   B.InstrGrdBkWgtDetailId = A.InstrGrdBkWgtDetailId
            AND StuEnrollId = @stuEnrollId
            AND ClsSectionId = @clsSectionId
    ORDER BY A.InstrGrdBkWgtDetailId; 



GO
