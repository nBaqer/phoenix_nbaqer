SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[USP_GetUsernames_fortasks]
    (
     @CampusID AS VARCHAR(8000) = NULL
    ,@CurrentUserID AS VARCHAR(8000) = NULL
    ,@AssignedByID AS VARCHAR(8000) = NULL
    ,@OwnerID AS VARCHAR(8000) = NULL
    ,@REID AS VARCHAR(8000) = NULL
    ,@Priority AS VARCHAR(50) = NULL
    ,@Status AS INTEGER = NULL
    ,@EndDate AS DATETIME = NULL
    ,@StartDate AS DATETIME = NULL
    ,@OwnerIDForAssignedtoMe AS VARCHAR(8000) = NULL 
     
    )
AS
    SELECT  userID
           ,UserName
           ,FullName
    FROM    Syusers
    WHERE   (
              @OwnerID IS NULL
              OR UserID IN ( SELECT strval
                             FROM   dbo.SPLIT(@OwnerID) )
            )
    UNION ALL
    SELECT  userID
           ,UserName
           ,FullName
    FROM    Syusers
    WHERE   (
              @CurrentUserID IS NULL
              OR UserID IN ( SELECT strval
                             FROM   dbo.SPLIT(@CurrentUserID) )
            );                           
                                     
                                     
                                   --  ORDER BY FullName


--SELECT 
--        DISTINCT
--        --UT.ReId ,
--        --( ( SELECT  FirstName + ' ' + LastName
--        --    FROM    arStudent S
--        --    WHERE   S.StudentID = UT.ReId
--        --  )
--        --  UNION
--        --  ( SELECT  FirstName + ' ' + LastName
--        --    FROM    adLeads
--        --    WHERE   LeadId = UT.ReId
--        --  )
--        --  UNION
--        --  ( SELECT  FirstName + ' ' + LastName
--        --    FROM    hrEmployees
--        --    WHERE   EmpId = UT.ReId
--        --  )
--        --  UNION
--        --  ( SELECT  EmployerDescrip
--        --    FROM    plEmployers
--        --    WHERE   EmployerId = UT.ReId
--        --  )
--        --  UNION
--        --  ( SELECT  LenderDescrip
--        --    FROM    faLenders
--        --    WHERE   LenderId = UT.ReId
--        --  )
--        --) AS ReName ,
--       UT.OwnerId AS UserID,
--        ( SELECT    username
--          FROM      syUsers
--          WHERE     UserId = UT.OwnerId
--        ) AS UserName,
--         ( SELECT    Fullname
--          FROM      syUsers
--          WHERE     UserId = UT.OwnerId
--        ) AS FullName
        
--        -- ,
--        --UT.AssignedById ,
--        --( SELECT    FullName
--        --  FROM      syUsers
--        --  WHERE     UserId = UT.AssignedById
--        --) AS AssignedByName ,
--        -- E.ResourceId AS EntityId ,
--        --E.Resource AS EntityName 
     
--FROM    tmUserTasks UT ,
--        tmTasks T ,
--        syCmpGrpCmps CC ,
--        syAdvantageResourceRelations R ,
--        syResources E ,
--        syResources M
--WHERE   UT.TaskId = T.TaskId
--        AND CC.CampGrpId = T.CampGroupId
--        AND R.ResourceId = E.ResourceId
--        AND R.RelatedResourceId = M.ResourceId
--        AND E.ResourceTypeId = 8
--        AND M.ResourceTypeId = 1
--        AND R.ResRelId = T.ModuleEntityId 
          
--        AND (cc.CampusId=@CampusId OR @CampusID IS NULL)
--        AND (ut.AssignedById=@AssignedByID OR @AssignedByID IS NULL)
        
--      And  ( @OwnerID IS NULL
--                                     OR ut.OwnerId IN (
--                                     SELECT strval
--                                     FROM   dbo.SPLIT(@OwnerID) ))
                                     
     
--        AND (UT.ReID =@REID OR @REID IS NULL )
--        AND (UT.Priority= @Priority OR @Priority is NULL)
--        AND (UT.StartDAte>=@StartDate OR @StartDate IS NULL)
--     AND (UT.EndDAte<=@EndDate OR @EndDAte IS NULL)
--     AND ((ut.OwnerId<>UT.AssignedById AND ut.OwnerId<>@OwnerIdForAssignedtoMe) OR @OwnerIDForAssignedtoMe IS NULL)
        
     
     



GO
