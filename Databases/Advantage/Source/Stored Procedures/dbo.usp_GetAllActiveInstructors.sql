SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_GetAllActiveInstructors] @campusId VARCHAR(50)
AS
    SET NOCOUNT ON;
    SELECT DISTINCT
            T.UserId AS Instr
           ,T.FullName
           ,T.AccountActive AS Status
    FROM    SyUsers T
           ,syUsersRolesCampGrps b
           ,syRoles c
    WHERE   T.AccountActive = 1
            AND b.CampGrpId IN ( SELECT CampGrpId
                                 FROM   syCmpGrpCmps
                                 WHERE  CampusId = @campusId )
            AND T.UserId = b.UserId
            AND b.RoleId = c.RoleId
            AND c.SysRoleId = 2
    ORDER BY Status
           ,T.FullName;




GO
