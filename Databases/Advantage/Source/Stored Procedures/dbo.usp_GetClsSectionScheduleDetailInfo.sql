SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [dbo].[usp_GetClsSectionScheduleDetailInfo]
    (
     @ClsSectMeetingID VARCHAR(50)
    )
AS
    BEGIN 

        SELECT  DISTINCT
                CSM.ClsSectMeetingId
               ,WD.ViewOrder dw
               ,CONVERT(VARCHAR,C1.TimeIntervalDescrip,108) timein
               ,CONVERT(VARCHAR,C2.TimeIntervalDescrip,108) timeout
               ,DATEDIFF(MINUTE,C1.TimeIntervalDescrip,C2.TimeIntervalDescrip) - ( ISNULL(CSM.BreakDuration,0) ) total
               ,CSTCP.AllowEarlyIn allow_earlyin
               ,CSTCP.AllowLateOut allow_lateout
               ,CSTCP.AllowExtraHours allow_extrahours
               ,CSTCP.CheckTardyIn Check_tardyin
               ,CSTCP.MaxInBeforeTardy max_beforetardy
               ,CSTCP.AssignTardyInTime tardy_intime
        FROM    dbo.arClsSectMeetings CSM
               ,dbo.arClassSections CS
               ,dbo.syPeriods Sy
               ,syPeriodsWorkDays SYWD
               ,dbo.plWorkDays WD
               ,dbo.cmTimeInterval C1
               ,dbo.cmTimeInterval C2
               ,dbo.arClsSectionTimeClockPolicy CSTCP
        WHERE   CS.ClsSectionId = CSM.ClsSectionId
                AND CSTCP.ClsSectionId = CS.ClsSectionId
                AND Sy.PeriodId = CSM.PeriodId
                AND C1.TimeIntervalId = Sy.StartTimeId
                AND C2.TimeIntervalId = Sy.EndTimeId
                AND Sy.PeriodId = SYWD.PeriodId
                AND SYWD.WorkDayId = WD.WorkDaysId
                AND CSM.ClsSectMeetingId = @ClsSectMeetingID;
			 
    END;
	

GO
