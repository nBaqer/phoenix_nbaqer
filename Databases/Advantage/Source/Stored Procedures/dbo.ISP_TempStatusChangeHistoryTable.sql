SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[ISP_TempStatusChangeHistoryTable]
    @StudentEnrollmentId VARCHAR(50)
   ,@EffectiveDate DATETIME
   ,@StatusDescription VARCHAR(50)
   ,@Reason VARCHAR(100)
   ,@RequestedBy VARCHAR(50)
   ,@ChangedBy VARCHAR(50)
   ,@CaseNo VARCHAR(50)
   ,@DateOfChange DATETIME
AS
    BEGIN
        SET NOCOUNT ON; 
	
        INSERT  INTO TempStatusChangeHistoryTbl
        VALUES  ( @StudentEnrollmentId,@EffectiveDate,@StatusDescription,@Reason,@RequestedBy,@ChangedBy,@CaseNo,@DateOfChange );
    
    END;

GO
