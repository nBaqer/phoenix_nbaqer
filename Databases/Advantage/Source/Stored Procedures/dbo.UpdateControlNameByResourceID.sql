SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[UpdateControlNameByResourceID] @ResourceId INT
AS
    DECLARE @strControlName VARCHAR(200);
    DECLARE @ResDefId INT;
    DECLARE @FldName VARCHAR(100);
    DECLARE GetDDL_Cursor CURSOR
    FOR
        SELECT  t1.ResDefId
               ,t3.FldName
        FROM    syResTblFlds t1
               ,syTblFlds t2
               ,syFields t3
               ,syDDLS t4
               ,syTables t5
               ,syFields t6
               ,syFields t7
        WHERE   t1.TblFldsId = t2.TblFldsId
                AND t2.FldId = t3.FldId
                AND t3.DDLId = t4.DDLId
                AND t4.TblId = t5.TblId
                AND t4.DispFldId = t6.FldId
                AND t4.ValFldId = t7.FldId
                AND t1.ResourceId = @ResourceId;

    OPEN GetDDL_Cursor;

    FETCH NEXT FROM GetDDL_Cursor
INTO @ResDefId,@FldName;

    WHILE @@FETCH_STATUS = 0
        BEGIN
            SET @strControlName = 'ddl' + @FldName;
            UPDATE  syResTblFlds
            SET     ControlName = @strControlName
            WHERE   ResDefId = @ResDefId; 	
            FETCH NEXT FROM GetDDL_Cursor INTO @ResDefId,@FldName;
        END;
    CLOSE GetDDL_Cursor;
    DEALLOCATE GetDDL_Cursor;



GO
