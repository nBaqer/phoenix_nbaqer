SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Ginzo, John
-- Create date: 06/24/2015
-- Description:	Determine using roles and/or enrollment
--				status whether the user can change the
--				grade for this enrollment
-- =============================================
CREATE PROCEDURE [dbo].[GetEnrollmentChangeGradeEnabled]
    @StuEnrollId VARCHAR(50)
   ,@UserId VARCHAR(50)
AS
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;

        DECLARE @CanChange BIT;
        SET @CanChange = 0;

		------------------------------------------------------------------------
		-- First check if user is support, system admin or director of academics
		-- IF SO set canchange to true
		------------------------------------------------------------------------
        IF EXISTS ( SELECT  r.RoleId
                    FROM    dbo.syRoles r
                    INNER JOIN dbo.sySysRoles sr ON r.SysRoleId = sr.SysRoleId
                    INNER JOIN dbo.syUsersRolesCampGrps ur ON r.RoleId = ur.RoleId
                    INNER JOIN dbo.syUsers u ON ur.UserId = u.UserId
                    WHERE   ur.UserId = @UserId
                            AND (
                                  u.UserName IN ( 'sa','support' )
                                  OR sr.SysRoleId IN ( 1,13 )
                                ) )
            BEGIN
                SET @CanChange = 1;
            END;
        ELSE
			------------------------------------------------------------------------
			-- If user is not one of the priveleged roles, decide whether they can
			-- change the grade based on the status of the enrollment.  Allowed statues
			-- is based on whether it is either a future start or postacademics is true
			------------------------------------------------------------------------
            BEGIN
                SET @CanChange = (
                                   SELECT   CASE WHEN ss.SysStatusId = 7 THEN 1
                                                 ELSE ss.PostAcademics
                                            END
                                   FROM     dbo.arStuEnrollments se
                                   INNER JOIN dbo.syStatusCodes sc ON se.StatusCodeId = sc.StatusCodeId
                                   INNER JOIN dbo.sySysStatus ss ON sc.SysStatusId = ss.SysStatusId
                                   WHERE    StuEnrollId = @StuEnrollId
                                 );

            END;	

        SELECT  @CanChange;

    END;


GO
