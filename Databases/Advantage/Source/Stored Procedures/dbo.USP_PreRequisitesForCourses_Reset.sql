SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_PreRequisitesForCourses_Reset]
    @EffectiveDate DATETIME
   ,@ReqId UNIQUEIDENTIFIER
AS
    DECLARE @hDoc INT;
        -- Delete all records from bridge table based on GrdComponentTypeId
    IF YEAR(@EffectiveDate) <> 1900
        BEGIN
            DELETE  FROM arPrerequisites_GradeComponentTypes_Courses
            WHERE   EffectiveDate = @EffectiveDate
                    AND ReqId = @ReqId;
        END;



GO
