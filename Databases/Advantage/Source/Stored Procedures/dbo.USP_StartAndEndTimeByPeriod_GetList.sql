SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_StartAndEndTimeByPeriod_GetList]
    @PeriodId VARCHAR(50) = NULL
AS
    SELECT DISTINCT
            PeriodId
           ,(
              SELECT    CONVERT(VARCHAR,TimeIntervalDescrip,108)
              FROM      dbo.cmTimeInterval
              WHERE     TimeIntervalId = t5.StartTimeId
            ) AS StartTime
           ,(
              SELECT    CONVERT(VARCHAR,TimeIntervalDescrip,108)
              FROM      dbo.cmTimeInterval
              WHERE     TimeIntervalId = t5.EndTimeId
            ) AS EndTime
    FROM    syPeriods t5
    WHERE   (
              @PeriodId IS NULL
              OR t5.PeriodId = @PeriodId
            );



GO
