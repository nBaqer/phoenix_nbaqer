SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Jose Astudillo
-- Create date: 10/1/2018
-- Description:	Get list of enrollments for notices
-- =============================================
CREATE PROCEDURE [dbo].[USP_GetStudentEnrollmentListForNotices]
    @StudentEnrollmentList VARCHAR(MAX) = NULL
   ,@UseEnrollment BIT = 1
   ,@CampusIdList VARCHAR(MAX) = NULL
   ,@StatusCodesIdList VARCHAR(MAX) = NULL
   ,@ProgramIdList VARCHAR(MAX) = NULL
AS
    BEGIN
        IF @StudentEnrollmentList IS NOT NULL
           AND @UseEnrollment = 1
            BEGIN
                SELECT Val AS EnrollmentId
                FROM   MultipleValuesForReportParameters(@StudentEnrollmentList, ',', 1);
            END;
        ELSE
            BEGIN
                --use other parameter to get list
                SELECT NULL AS EnrollmentId;
            END;


    END;
GO
