SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--=================================================================================================
-- USP_getOnTimeGraduationDetails_ProgramWeeks
--=================================================================================================
CREATE  PROCEDURE [dbo].[USP_getOnTimeGraduationDetails_ProgramWeeks]
    (
     @StartDate AS DATETIME
    ,@EndDate AS DATETIME
    ,@CampusId AS VARCHAR(50)
    ,@ProgramIdList AS VARCHAR(MAX) = NULL  
    )
AS
    BEGIN  
        DECLARE @StudentIdentifier AS NVARCHAR(1000);
    
        SELECT  @StudentIdentifier = SCASV.Value
        FROM    syConfigAppSettings AS SCAS
        INNER JOIN syConfigAppSetValues AS SCASV ON SCASV.SettingId = SCAS.SettingId
        WHERE   SCAS.KeyName = 'StudentIdentifier';					-- @StudentIdentifier --> 'SSN', 'EnrollmentId', 'StudentId'

        BEGIN /** Step 1: Declare all temp tables in use **/
            DECLARE @getStudentsbyGEProgramsandDate TABLE
                (
                 StudentId UNIQUEIDENTIFIER
                ,StuEnrollId UNIQUEIDENTIFIER
                ,StatusCodeId UNIQUEIDENTIFIER
                ,PrgVerId UNIQUEIDENTIFIER
                ,CampusId UNIQUEIDENTIFIER
                ,StartDate DATETIME
                ,SysStatusDescrip VARCHAR(50)
                ,CampDescrip VARCHAR(50)
                ,OPEID VARCHAR(10)
                ,ProgId UNIQUEIDENTIFIER
                ,ProgDescrip VARCHAR(100)
                ,CIPCode VARCHAR(50)
                ,CredentialCode VARCHAR(50)
                ,CredentialDescription VARCHAR(50)
                ,PrgVerDescrip VARCHAR(100)
                ,Weeks DECIMAL(18,2)
                ,SSN VARCHAR(50)
                ,EnrollmentId VARCHAR(50)
                ,StudentNumber VARCHAR(50)
                ,LastName VARCHAR(50)
                ,FirstName VARCHAR(50)
                ,MiddleName VARCHAR(50)
                ,DOB DATETIME
                ,ContractedGradDate DATETIME
                ,RevisedGradDate DATETIME
                ,StudentProgramStartDate DATETIME
                ,GraduationDate DATETIME
                ,RevisedGradDateCalculated DATETIME
                ,StudentName VARCHAR(100)
                ,StudentIdentification VARCHAR(50)
                ,HasTitleIV_Aid BIT
                ,OnTimeCompleter VARCHAR(5)
                ,RowNumber INTEGER
                ,PRIMARY KEY ( StuEnrollId )
                );

            DECLARE @getTransactionsSummary TABLE
                (
                 TransactionId UNIQUEIDENTIFIER
                ,TransAmount DECIMAL(18,2)
                ,TitleIV INT
                ,Voided INT
                ,StudentId UNIQUEIDENTIFIER
                ,StuEnrollId UNIQUEIDENTIFIER
                ,ProgId UNIQUEIDENTIFIER
                );
        END; /** END  Step 1 **/

        BEGIN /** Step 2: Get Students enrolled in GE Program and who started in date range STUDENT PROGRAM START DATE **/
            INSERT  INTO @getStudentsbyGEProgramsandDate
                    SELECT  GS.StudentId
                           ,GS.StuEnrollId
                           ,GS.StatusCodeId
                           ,GS.PrgVerId
                           ,GS.CampusId
                           ,GS.StartDate
                           ,GS.SysStatusDescrip
                           ,GS.CampDescrip
                           ,GS.OPEID
                           ,GS.ProgId
                           ,GS.ProgDescrip
                           ,GS.CIPCode
                           ,GS.CredentialCode
                           ,GS.CredentialDescription
                           ,GS.PrgVerDescrip
                           ,GS.Weeks
                           ,GS.SSN
                           ,GS.EnrollmentId
                           ,GS.StudentNumber
                           ,GS.LastName
                           ,GS.FirstName
                           ,GS.MiddleName
                           ,GS.DOB
                           ,GS.ContractedGradDate
                           ,GS.RevisedGradDate
                           ,GS.StudentProgramStartDate
                           ,GS.GraduationDate
                           ,GS.RevisedGradDateCalculated
                           ,GS.StudentName
                           ,GS.StudentIdentification
                           ,0  -- False so far
                           ,''
                           ,GS.RowNumber
                    FROM    (
                              SELECT    ASE.StudentId
                                       ,ASE.StuEnrollId
                                       ,ASE.StatusCodeId
                                       ,ASE.PrgVerId
                                       ,ASE.CampusId
                                       ,ASE.StartDate
                                       ,SSS.SysStatusDescrip
                                       ,SC.CampDescrip
                                       ,SC.OPEID
                                       ,AP.ProgId
                                       ,AP.ProgDescrip
                                       ,AP.CIPCode
                                       ,APC.CredentialCode
                                       ,APC.CredentialDescription
                                       ,APV.PrgVerDescrip
                                       ,APV.Weeks
                                       ,AST.SSN
                                       ,ASE.EnrollmentId
                                       ,AST.StudentNumber
                                       ,AST.LastName
                                       ,AST.FirstName
                                       ,AST.MiddleName
                                       ,AST.DOB
                                       ,ASE.ContractedGradDate
                                       ,ASE.ExpGradDate AS RevisedGradDate
                                       ,MIN(ISNULL(ASE.StartDate,'2099-12-31')) OVER ( PARTITION BY AP.ProgId,ASE.StudentId ) AS StudentProgramStartDate -- -- used only to filter student with at least one enrollments out of rangedat
                                       ,DATEADD(DAY,
                                                dbo.GetTotalNoofHolidaysbtwGivenDatesandCampus(MIN(ASE.StartDate) OVER ( PARTITION BY AP.ProgId,ASE.StudentId ),
                                                                                               DATEADD(WEEK,APV.Weeks,
                                                                                                       MIN(ASE.StartDate) OVER ( PARTITION BY AP.ProgId,
                                                                                                                                 ASE.StudentId )),ASE.CampusId),
                                                DATEADD(WEEK,APV.Weeks,MIN(ASE.StartDate) OVER ( PARTITION BY AP.ProgId,ASE.StudentId ))) AS GraduationDate
                                       ,MIN(CASE WHEN SSS.SysStatusDescrip = 'Graduated' THEN ASE.ExpGradDate
                                                 ELSE '2099-12-31'
                                            END) OVER ( PARTITION BY AP.ProgId,ASE.StudentId ) AS RevisedGradDateCalculated
                                       ,RTRIM(AST.LastName + ', ' + AST.FirstName + ' ' + ISNULL(UPPER(SUBSTRING(AST.MiddleName,1,1)),'')) AS StudentName
                                       ,CASE WHEN @StudentIdentifier = 'SSN' THEN '***-**-' + SUBSTRING(AST.SSN,6,4)
                                             WHEN @StudentIdentifier = 'EnrollmentId' THEN ASE.EnrollmentId
                                             WHEN @StudentIdentifier = 'StudentId' THEN AST.StudentNumber
                                             ELSE AST.StudentNumber
                                        END AS StudentIdentification
                                       ,ROW_NUMBER() OVER ( PARTITION BY AP.ProgId,ASE.StudentId ORDER BY ASE.StartDate ASC ) AS RowNumber
                              FROM      arStudent AS AST
                              INNER JOIN arStuEnrollments AS ASE ON ASE.StudentId = AST.StudentId
                              INNER JOIN syStatusCodes AS SSC ON SSC.StatusCodeId = ASE.StatusCodeId
                              INNER JOIN sySysStatus AS SSS ON SSS.SysStatusId = SSC.SysStatusId
                              INNER JOIN arPrgVersions AS APV ON APV.PrgVerId = ASE.PrgVerId
                              INNER JOIN arPrograms AS AP ON AP.ProgId = APV.ProgId
                              LEFT JOIN arProgCredential AS APC ON APC.CredentialId = AP.CredentialLvlId
                              INNER JOIN syCampuses AS SC ON SC.CampusId = ASE.CampusId
                              WHERE     SC.CampusId = @CampusId
                                        AND (
                                              ISNULL(@ProgramIdList,'') = ''
                                              OR AP.ProgId IN ( SELECT  Val
                                                                FROM    MultipleValuesForReportParameters(@ProgramIdList,',',1) )
                                            )
                                        AND AP.IsGEProgram = 1
                                        AND SSS.SysStatusId NOT IN ( 7,8 )
                            ) AS GS
                    WHERE   StudentProgramStartDate BETWEEN @StartDate AND @EndDate
                    ORDER BY StudentId;
        END;  /** END  --  Step 2 **/

        BEGIN /** Step 3: Get Total of Transaction for those studnent selected in Step 1 (Join saTransactions, saFundSources and getStudentsbyGEProgramsandDate) **/
            INSERT  INTO @getTransactionsSummary
                    SELECT  ST.TransactionId
                           ,ISNULL(ST.TransAmount,0) AS TransAmount
                           ,ISNULL(SFS.TitleIV,0) AS TitleIV
                           ,ISNULL(ST.Voided,0) AS Voided
                           ,GS.StudentId
                           ,GS.StuEnrollId
                           ,GS.ProgId
                    FROM    saTransactions AS ST
                    INNER JOIN @getStudentsbyGEProgramsandDate AS GS ON GS.StuEnrollId = ST.StuEnrollId
                    LEFT JOIN saFundSources AS SFS ON SFS.FundSourceId = ST.FundSourceId
                    WHERE   ISNULL(SFS.TitleIV,0) = 1
                            AND ISNULL(ST.Voided,0) = 0;
        END;  /** END  --  Step 3 **/
		 
        BEGIN /** Step 4: From Step 1 results, get TitleIV Aid for each student  **/
            UPDATE  GS
            SET     GS.HasTitleIV_Aid = (
                                          SELECT    CASE WHEN SUM(ISNULL(ST.TransAmount,0.00) * -1) <> 0 THEN 1
                                                         ELSE 0
                                                    END
                                          FROM      @getTransactionsSummary AS ST
                                          WHERE     ST.Voided = 0
                                                    AND ST.TitleIV = 1
                                                    AND ST.StudentId = GS.StudentId
                                                    AND ST.ProgId = GS.ProgId
                                        )
            FROM    @getStudentsbyGEProgramsandDate GS;

        END;  /** END  --  Step 4:  **/
        
        BEGIN /** Step 5: Return data to the report **/
            SELECT  GS.StudentIdentification
                   ,GS.StudentName
                   ,GS.CampDescrip
                   ,GS.OPEID
                   ,GS.ProgId
                   ,GS.ProgDescrip
                   ,GS.CIPCode
                   ,GS.CredentialCode
                   ,GS.CredentialDescription
                   ,GS.PrgVerId
                   ,GS.PrgVerDescrip
                   ,GS.GraduationDate
                   ,CASE WHEN GS.RevisedGradDateCalculated = '2099-12-31' THEN GS.RevisedGradDate
                         ELSE GS.RevisedGradDateCalculated
                    END AS RevisedGradDate
                   ,( CASE WHEN GS.RevisedGradDateCalculated <= GS.GraduationDate THEN 'Yes'
                           ELSE 'Not'
                      END ) AS OnTimeCompleter
            FROM    @getStudentsbyGEProgramsandDate AS GS
            WHERE   GS.RowNumber = 1
                    AND GS.HasTitleIV_Aid > 0
            ORDER BY GS.ProgDescrip
                   ,GS.PrgVerDescrip
                   ,GS.StudentName;

        END;  /** END  --  Step 5 **/

    END;
--=================================================================================================
-- END  --  USP_getOnTimeGraduationDetails_ProgramWeeks
--=================================================================================================    
GO
GO
GO
GO
GO
GO
