SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_IPEDS_Finance_AllInstitutions_Detail]
    @CampusId VARCHAR(50)
   ,@StartDate DATETIME = NULL
   ,@EndDate DATETIME = NULL
   ,@OrderBy VARCHAR(100)
AS
    SET NOCOUNT ON;
	--SET @StartDate = (DATEADD(YEAR,-1,@EndDate)+1)
	
    CREATE TABLE #TempFinanceDetails
        (
         SSN VARCHAR(50)
        ,StudentName VARCHAR(100)
        ,StudentNumber VARCHAR(50)
        ,EnrollmentId VARCHAR(50)
        ,DupEnrollCount INT
        ,Tuition DECIMAL(18,2)
        ,RegistrationFee DECIMAL(18,2)
        ,PELLDisb DECIMAL(18,2)
        ,PELLRefunds DECIMAL(18,2)
        ,SEOGDisb DECIMAL(18,2)
        ,SEOGRefunds DECIMAL(18,2)
        ,TotalStudents INT
        ,TotalStudentsMTOE INT
        ,TotalTuition DECIMAL(18,2)
        ,TotalTuitionMTOE DECIMAL(18,2)
        ,TotalFees DECIMAL(18,2)
        ,TotalFeesMTOE DECIMAL(18,2)
        ,TotalPellRefs DECIMAL(18,2)
        ,TotalPellDisb DECIMAL(18,2)
        ,TotalSEOGRefs DECIMAL(18,2)
        ,TotalSEOGDisb DECIMAL(18,2)
        ,
		-- New Columns --
         SSIGDisb DECIMAL(18,2)
        ,TotalSSIGDisb DECIMAL(18,2)
        ,SSIGRefund DECIMAL(18,2)
        ,TotalSSIGRefund DECIMAL(18,2)
        ,ScholarshipsDisb DECIMAL(18,2)
        ,TotalScholarshipsDisb DECIMAL(18,2)
        ,ScholarshipsRefund DECIMAL(18,2)
        ,TotalScholarshipsRefund DECIMAL(18,2)
        ,FellowshipsDisb DECIMAL(18,2)
        ,TotalFellowshipsDisb DECIMAL(18,2)
        ,FellowshipsRefund DECIMAL(18,2)
        ,TotalFellowshipsRefund DECIMAL(18,2)
        ,TuitionWaiverDisb DECIMAL(18,2)
        ,TotalTuitionWaiverDisb DECIMAL(18,2)
        ,TuitionWaiverRefund DECIMAL(18,2)
        ,TotalTuitionWaiverRefund DECIMAL(18,2)
        ,
		-- New Columns Per Revision 2/15/12
         StateLocalGrantDisb DECIMAL(18,2)
        ,TotalStateLocalGrantDisb DECIMAL(18,2)
        ,StateLocalGrantRefund DECIMAL(18,2)
        ,TotalStateLocalGrantRefund DECIMAL(18,2)
        ,InstGrantDisb DECIMAL(18,2)
        ,TotalInstGrantDisb DECIMAL(18,2)
        ,InstGrantRefund DECIMAL(18,2)
        ,TotalInstGrantRefund DECIMAL(18,2)
        ,StudentId UNIQUEIDENTIFIER
        ,StuEnrollId UNIQUEIDENTIFIER
        );

    SELECT  dbo.UDF_FormatSSN(SSN) AS SSN
           ,S.LastName + ', ' + S.FirstName + ' ' + ISNULL(S.MiddleName,'') + CASE WHEN (
                                                                                          SELECT    COUNT(*)
                                                                                          FROM      arStuEnrollments SEI
                                                                                          WHERE     SEI.StudentId = S.StudentId
                                                                                        ) > 1 THEN ' *'
                                                                                   ELSE ''
                                                                              END AS StudentName
           ,S.StudentNumber
           ,SE.EnrollmentId
           ,CASE WHEN (
                        SELECT  COUNT(*)
                        FROM    arStuEnrollments SEI
                        WHERE   SEI.StudentId = S.StudentId
                      ) > 1 THEN 1
                 ELSE 0
            END AS DupEnrollCount
           ,1 AS StudentCount
        --   ,
		--Tuition
		--(Select ISNULL(SUM(T.TransAmount),0) 
        -- FROM saTransactions T, saTransCodes TC 
        -- WHERE T.TransCodeId=TC.TransCodeId And TC.SysTransCodeId In (1,2,3,4) And T.StuEnrollId=SE.StuEnrollId 
        --   And T.Voided=0 And T.TransDate >=@StartDate And T.TransDate <=@EndDate 
        --) As Tuition 
           ,(
              SELECT    ISNULL(SUM(T.TransAmount),0)
              FROM      saTransactions T
                       ,saTransCodes TC
              WHERE     T.TransCodeId = TC.TransCodeId
                        AND TC.SysTransCodeId IN ( 1 )
                        AND T.StuEnrollId = SE.StuEnrollId
                        AND T.Voided = 0
                        --AND T.TransTypeId IN ( 0 )
                        AND T.TransTypeId IN ( 0,1 )
                        AND T.TransDate >= @StartDate
                        AND T.TransDate <= @EndDate
            ) AS Tuition
        --   ,
		--Registration Fee
		--(Select ISNULL(SUM(T.TransAmount),0) 
        -- FROM saTransactions T, saTransCodes TC 
        -- WHERE T.TransCodeId=TC.TransCodeId And TC.SysTransCodeId In (2) And T.StuEnrollId=SE.StuEnrollId And T.Voided=0 
        -- And T.TransDate >=@StartDate And T.TransDate <=@EndDate 
        --) As Re            gistrationFee
           ,(
              SELECT    ISNULL(SUM(T.TransAmount),0)
              FROM      saTransactions T
                       ,saTransCodes TC
              WHERE     T.TransCodeId = TC.TransCodeId
                        AND TC.SysTransCodeId IN ( 2,3,4,17 )
                        AND T.StuEnrollId = SE.StuEnrollId
                        AND T.Voided = 0
                        AND T.TransTypeId IN ( 0,1 )
                        --AND T.TransTypeId IN ( 0)
                        AND T.TransDate >= @StartDate
                        AND T.TransDate <= @EndDate
            ) AS RegistrationFee
           ,
		
		--PELL (AdvFundSourceId=2)
		--PELL Disbursement
            (
              SELECT    ISNULL(SUM(-1 * T.TransAmount),0)
              FROM      saTransactions T
                       ,saPmtDisbRel PDR
                       ,faStudentAwards SA
                       ,faStudentAwardSchedule SAS
                       ,saFundSources FS
              WHERE     T.StuEnrollId = SE.StuEnrollId
                        AND T.Voided = 0
                        AND T.TransTypeId IN ( 2 )
                        AND PDR.TransactionId = T.TransactionId
                        AND SA.StudentAwardId = SAS.StudentAwardId
                        AND (
                              PDR.AwardScheduleId = SAS.AwardScheduleId
                              OR PDR.StuAwardId = SA.StudentAwardId
                            )
                        AND FS.FundSourceId = SA.AwardTypeId
                        AND FS.AdvFundSourceId IN ( 2 )
                        AND T.TransDate >= @StartDate
                        AND T.TransDate <= @EndDate
            ) AS PELLDisb
           ,
		--PELL Refunds
            (
              SELECT    ISNULL(SUM(-1 * T.TransAmount),0)
              FROM      saTransactions T
                       ,saRefunds R
                       ,saFundSources FS
              WHERE     T.StuEnrollId = SE.StuEnrollId
                        AND T.Voided = 0
                        AND T.TransTypeId IN ( 1,2 )
                        AND R.TransactionId = T.TransactionId
                        AND FS.FundSourceId = R.FundSourceId
                        AND FS.AdvFundSourceId IN ( 2 )
                        AND T.TransDate >= @StartDate
                        AND T.TransDate <= @EndDate
            ) AS PELLRefunds
           ,

		--Other Federal Grants (Bring in Non Pell Federal grants that are Title IV or Non Title IV). Do not allow FWS either.
            (
              SELECT    ISNULL(SUM(-1 * T.TransAmount),0)
              FROM      saTransactions T
                       ,saPmtDisbRel PDR
                       ,faStudentAwards SA
                       ,faStudentAwardSchedule SAS
                       ,saFundSources FS
              WHERE     T.StuEnrollId = SE.StuEnrollId
                        AND T.Voided = 0
                        AND T.TransTypeId IN ( 2 )
                        AND PDR.TransactionId = T.TransactionId
                        AND SA.StudentAwardId = SAS.StudentAwardId
                        AND (
                              PDR.AwardScheduleId = SAS.AwardScheduleId
                              OR PDR.StuAwardId = SA.StudentAwardId
                            )
                        AND FS.FundSourceId = SA.AwardTypeId
                        AND FS.AdvFundSourceId NOT IN ( 2,5 )
                        AND FS.IPEDSValue IN ( 66 )
                        AND T.TransDate >= @StartDate
                        AND T.TransDate <= @EndDate
            ) AS SEOGDisb
           ,
		--SEOG Refunds
            (
              SELECT    ISNULL(SUM(-1 * T.TransAmount),0)
              FROM      saTransactions T
                       ,saRefunds R
                       ,saFundSources FS
              WHERE     T.StuEnrollId = SE.StuEnrollId
                        AND T.Voided = 0
                        AND T.TransTypeId IN ( 1,2 )
                        AND R.TransactionId = T.TransactionId
                        AND FS.FundSourceId = R.FundSourceId
                        AND FS.AdvFundSourceId NOT IN ( 2,5 )
                        AND FS.IPEDSValue IN ( 66 )
                        AND T.TransDate >= @StartDate
                        AND T.TransDate <= @EndDate
            ) AS SEOGRefunds
           ,

		-- New Columns --
		-- SSIG Disb
            (
              SELECT    ISNULL(SUM(-1 * T.TransAmount),0)
              FROM      saTransactions T
                       ,saPmtDisbRel PDR
                       ,faStudentAwards SA
                       ,faStudentAwardSchedule SAS
                       ,saFundSources FS
              WHERE     T.StuEnrollId = SE.StuEnrollId
                        AND T.Voided = 0
                        AND T.TransTypeId IN ( 2 )
                        AND PDR.TransactionId = T.TransactionId
                        AND SA.StudentAwardId = SAS.StudentAwardId
                        AND (
                              PDR.AwardScheduleId = SAS.AwardScheduleId
                              OR PDR.StuAwardId = SA.StudentAwardId
                            )
                        AND FS.FundSourceId = SA.AwardTypeId
                        AND FS.IPEDSValue IN ( 67 )
                        AND T.TransDate >= @StartDate
                        AND T.TransDate <= @EndDate
            ) AS SSIGDisb
           ,
		
		-- SSIG Refund
            (
              SELECT    ISNULL(SUM(-1 * T.TransAmount),0)
              FROM      saTransactions T
                       ,saRefunds R
                       ,saFundSources FS
              WHERE     T.StuEnrollId = SE.StuEnrollId
                        AND T.Voided = 0
                        AND T.TransTypeId IN ( 1,2 )
                        AND R.TransactionId = T.TransactionId
                        AND FS.FundSourceId = R.FundSourceId
                        AND FS.IPEDSValue IN ( 67 )
                        AND T.TransDate >= @StartDate
                        AND T.TransDate <= @EndDate
            ) AS SSIGRefund
           ,
		
		-- Scholarships Disb
            (
              SELECT    ISNULL(SUM(-1 * T.TransAmount),0)
              FROM      saTransactions T
                       ,saPmtDisbRel PDR
                       ,faStudentAwards SA
                       ,faStudentAwardSchedule SAS
                       ,saFundSources FS
              WHERE     T.StuEnrollId = SE.StuEnrollId
                        AND T.Voided = 0
                        AND T.TransTypeId IN ( 2 )
                        AND PDR.TransactionId = T.TransactionId
                        AND SA.StudentAwardId = SAS.StudentAwardId
                        AND (
                              PDR.AwardScheduleId = SAS.AwardScheduleId
                              OR PDR.StuAwardId = SA.StudentAwardId
                            )
                        AND FS.FundSourceId = SA.AwardTypeId
                        AND FS.AdvFundSourceId IN ( 14 )
                        AND T.TransDate >= @StartDate
                        AND T.TransDate <= @EndDate
            ) AS ScholarshipsDisb
           ,
		
		-- Scholarships Refund
            (
              SELECT    ISNULL(SUM(-1 * T.TransAmount),0)
              FROM      saTransactions T
                       ,saRefunds R
                       ,saFundSources FS
              WHERE     T.StuEnrollId = SE.StuEnrollId
                        AND T.Voided = 0
                        AND T.TransTypeId IN ( 1,2 )
                        AND R.TransactionId = T.TransactionId
                        AND FS.FundSourceId = R.FundSourceId
                        AND FS.AdvFundSourceId IN ( 14 )
                        AND T.TransDate >= @StartDate
                        AND T.TransDate <= @EndDate
            ) AS ScholarshipsRefund
           ,
		
		-- Fellowships Disb
            0.00 AS FellowshipsDisb
           ,
		
		-- Fellowships Refund
            0.00 AS FellowshipsRefund
           ,
		
		-- TuitionWaiver Disb
            0.00 AS TuitionWaiverDisb
           ,
		
		-- TuitionWaiver Refund
            0.00 AS TuitionWaiverRefund
           ,
		-- New Columns --
		
		
		
		
		--State/Local Grant Disb
		--(SELECT COALESCE(SUM(GrossAmount), 0.00)
		--FROM faStudentAwards t1 
		--INNER JOIN saFundSources t2 ON t1.AwardTypeId=t2.FundSourceId 
		--INNER JOIN arStuEnrollments t3 ON t1.StuEnrollId = t3.StuEnrollId AND t3.StuEnrollId = SE.StuEnrollId
		--WHERE t2.IPEDSValue=67 and (t1.AwardStartDate>=@StartDate AND t1.AwardstartDate<=@EndDate)
		--AND ISNULL(t2.AdvFundSourceId,0) NOT IN (5,6) ) as StateLocalGrantDisb,
            (
              SELECT    ISNULL(SUM(-1 * T.TransAmount),0)
              FROM      saTransactions T
                       ,saPmtDisbRel PDR
                       ,faStudentAwards SA
                       ,faStudentAwardSchedule SAS
                       ,saFundSources FS
              WHERE     T.StuEnrollId = SE.StuEnrollId
                        AND T.Voided = 0
                        AND T.TransTypeId IN ( 2 )
                        AND PDR.TransactionId = T.TransactionId
                        AND SA.StudentAwardId = SAS.StudentAwardId
                        AND (
                              PDR.AwardScheduleId = SAS.AwardScheduleId
                              OR PDR.StuAwardId = SA.StudentAwardId
                            )
                        AND FS.FundSourceId = SA.AwardTypeId
                        AND FS.AdvFundSourceId NOT IN ( 5,6 )
                        AND FS.IPEDSValue = 67
                        AND T.TransDate >= @StartDate
                        AND T.TransDate <= @EndDate
            ) AS StateLocalGrantDisb
           ,

		--State/Local Grant Refunds
            (
              SELECT    ISNULL(SUM(-1 * T.TransAmount),0)
              FROM      saTransactions T
                       ,saRefunds R
                       ,saFundSources FS
              WHERE     T.StuEnrollId = SE.StuEnrollId
                        AND T.Voided = 0
                        AND T.TransTypeId IN ( 1,2 )
                        AND R.TransactionId = T.TransactionId
                        AND FS.FundSourceId = R.FundSourceId
                        AND FS.IPEDSValue IN ( 67 )
                        AND FS.AdvFundSourceId NOT IN ( 5 )
                        AND T.TransDate >= @StartDate
                        AND T.TransDate <= @EndDate
            ) AS StateLocalGrantRefund
           ,
		
		
		
		--Inst Grants Funded/Unfunded Disb
		--(SELECT COALESCE(SUM(GrossAmount), 0.00)
		--FROM faStudentAwards t1 
		--INNER JOIN saFundSources t2 ON t1.AwardTypeId=t2.FundSourceId 
		--INNER JOIN arStuEnrollments t3 ON t1.StuEnrollId = t3.StuEnrollId AND t3.StuEnrollId = SE.StuEnrollId
		--WHERE t2.IPEDSValue=68 and (t1.AwardStartDate>=@StartDate AND t1.AwardstartDate<=@EndDate)
		--AND ISNULL(t2.AdvFundSourceId,0) NOT IN (5,6) ) as InstGrantDisb,
            (
              SELECT    ISNULL(SUM(-1 * T.TransAmount),0)
              FROM      saTransactions T
                       ,saPmtDisbRel PDR
                       ,faStudentAwards SA
                       ,faStudentAwardSchedule SAS
                       ,saFundSources FS
              WHERE     T.StuEnrollId = SE.StuEnrollId
                        AND T.Voided = 0
                        AND T.TransTypeId IN ( 2 )
                        AND PDR.TransactionId = T.TransactionId
                        AND SA.StudentAwardId = SAS.StudentAwardId
                        AND (
                              PDR.AwardScheduleId = SAS.AwardScheduleId
                              OR PDR.StuAwardId = SA.StudentAwardId
                            )
                        AND FS.FundSourceId = SA.AwardTypeId
                        AND FS.AdvFundSourceId NOT IN ( 5,6 )
                        AND FS.IPEDSValue = 68
                        AND T.TransDate >= @StartDate
                        AND T.TransDate <= @EndDate
            ) AS InstGrantDisb
           ,
		
		--Inst Grants Funded/Unfunded Refunds
            (
              SELECT    ISNULL(SUM(-1 * T.TransAmount),0)
              FROM      saTransactions T
                       ,saRefunds R
                       ,saFundSources FS
              WHERE     T.StuEnrollId = SE.StuEnrollId
                        AND T.Voided = 0
                        AND T.TransTypeId IN ( 1,2 )
                        AND R.TransactionId = T.TransactionId
                        AND FS.FundSourceId = R.FundSourceId
                        AND FS.IPEDSValue IN ( 68 )
                        AND FS.AdvFundSourceId NOT IN ( 5 )
                        AND T.TransDate >= @StartDate
                        AND T.TransDate <= @EndDate
            ) AS InstGrantRefund
           ,SE.StuEnrollId
           ,(
              SELECT    COUNT(*)
              FROM      saTransactions T
              WHERE     T.StuEnrollId = SE.StuEnrollId
                        AND T.Voided = 0
                        AND T.TransDate >= @StartDate
                        AND T.TransDate <= @EndDate
            ) AS TransCount
           ,S.StudentId
    INTO    #Temp
    FROM    arStudent S
           ,arStuEnrollments SE
    WHERE   S.StudentId = SE.StudentId
            AND SE.CampusId = @CampusId; --and SE.StuEnrollId='58D82744-DD9C-46D5-A4AC-835DECABF666'
		
		

    INSERT  INTO #TempFinanceDetails
            SELECT  SSN
                   ,StudentName
                   ,StudentNumber
                   ,EnrollmentId
                   ,DupEnrollCount
                   ,Tuition
                   ,RegistrationFee
                   ,PELLDisb
                   ,PELLRefunds
                   ,SEOGDisb
                   ,SEOGRefunds
                   ,(
                      SELECT    SUM(StudentCount)
                      FROM      #Temp
                      WHERE     StuEnrollId IS NOT NULL
                                AND TransCount > 0
                    ) AS TotalStudents
                   ,(
                      SELECT    SUM(DupEnrollCount)
                      FROM      #Temp
                      WHERE     DupEnrollCount = 1
                                AND StuEnrollId IS NOT NULL
                                AND TransCount > 0
                    ) AS TotalStudentsMTOE
                   ,(
                      SELECT    SUM(Tuition)
                      FROM      #Temp
                    ) TotalTuition
                   ,(
                      SELECT    SUM(Tuition)
                      FROM      #Temp
                      WHERE     DupEnrollCount = 1
                    ) AS TotalTuitionMTOE
                   ,(
                      SELECT    SUM(RegistrationFee)
                      FROM      #Temp
                    ) AS TotalFees
                   ,(
                      SELECT    SUM(RegistrationFee)
                      FROM      #Temp
                      WHERE     DupEnrollCount = 1
                    ) AS TotalFeesMTOE
                   ,(
                      SELECT    SUM(PELLRefunds)
                      FROM      #Temp
                    ) AS TotalPellRefs
                   ,(
                      SELECT    SUM(PELLDisb)
                      FROM      #Temp
                    ) AS TotalPellDisb
                   ,(
                      SELECT    SUM(SEOGRefunds)
                      FROM      #Temp
                    ) AS TotalSEOGRefs
                   ,(
                      SELECT    SUM(SEOGDisb)
                      FROM      #Temp
                    ) AS TotalSEOGDisb
                   ,
		-- New Columns --
                    SSIGDisb
                   ,(
                      SELECT    SUM(SSIGDisb)
                      FROM      #Temp
                    ) AS TotalSSIGDisb
                   ,SSIGRefund
                   ,(
                      SELECT    SUM(SSIGRefund)
                      FROM      #Temp
                    ) AS TotalSSIGRefund
                   ,ScholarshipsDisb
                   ,(
                      SELECT    SUM(ScholarshipsDisb)
                      FROM      #Temp
                    ) AS TotalScholarshipsDisb
                   ,ScholarshipsRefund
                   ,(
                      SELECT    SUM(ScholarshipsRefund)
                      FROM      #Temp
                    ) AS TotalScholarshipsRefund
                   ,FellowshipsDisb
                   ,(
                      SELECT    SUM(FellowshipsDisb)
                      FROM      #Temp
                    ) AS TotalFellowshipsDisb
                   ,FellowshipsRefund
                   ,(
                      SELECT    SUM(FellowshipsRefund)
                      FROM      #Temp
                    ) AS TotalFellowshipsRefund
                   ,TuitionWaiverDisb
                   ,(
                      SELECT    SUM(TuitionWaiverDisb)
                      FROM      #Temp
                    ) AS TotalTuitionWaiverDisb
                   ,TuitionWaiverRefund
                   ,(
                      SELECT    SUM(TuitionWaiverRefund)
                      FROM      #Temp
                    ) AS TotalTuitionWaiverRefund
                   ,StateLocalGrantDisb
                   ,(
                      SELECT    SUM(StateLocalGrantDisb)
                      FROM      #Temp
                    ) AS TotalStateLocalGrantDisb
                   ,StateLocalGrantRefund
                   ,(
                      SELECT    SUM(StateLocalGrantRefund)
                      FROM      #Temp
                    ) AS TotalStateLocalGrantRefund
                   ,InstGrantDisb
                   ,(
                      SELECT    SUM(InstGrantDisb)
                      FROM      #Temp
                    ) AS TotalInstGrantDisb
                   ,InstGrantRefund
                   ,(
                      SELECT    SUM(InstGrantRefund)
                      FROM      #Temp
                    ) AS TotalInstGrantRefund
                   ,StudentId
                   ,StuEnrollId
		-- New Columns -- 
            FROM    #Temp
            WHERE   StuEnrollId IS NOT NULL
                    AND TransCount > 0
                    AND StuEnrollId NOT IN ( SELECT StuEnrollId
                                             FROM   #Temp
                                             WHERE  Tuition = 0
                                                    AND RegistrationFee = 0
                                                    AND PELLDisb = 0
                                                    AND PELLRefunds = 0
                                                    AND SEOGDisb = 0
                                                    AND SEOGRefunds = 0
                                                    AND SSIGDisb = 0
                                                    AND SSIGDisb = 0
                                                    AND SSIGRefund = 0
                                                    AND ScholarshipsDisb = 0
                                                    AND ScholarshipsRefund = 0
                                                    AND FellowshipsDisb = 0
                                                    AND FellowshipsRefund = 0
                                                    AND TuitionWaiverDisb = 0
                                                    AND TuitionWaiverRefund = 0
                                                    AND StateLocalGrantDisb = 0
                                                    AND StateLocalGrantRefund = 0
                                                    AND InstGrantDisb = 0
                                                    AND InstGrantRefund = 0 )
            ORDER BY CASE WHEN @OrderBy = 'SSN' THEN SSN
                     END
                   ,CASE WHEN @OrderBy = 'Student Number' THEN StudentNumber
                    END
                   ,CASE WHEN @OrderBy = 'Student Enrollment' THEN EnrollmentId
                    END;
		--Group by SSN, StudentName,StudentNumber, DupEnrollCount, Tuition ,RegistrationFee , PELLDisb ,
		--PELLRefunds , SEOGDisb , SEOGRefunds
		
    DECLARE @StudentCount INT
       ,@StudentEnrollmentCount INT;
    SET @StudentCount = (
                          SELECT    COUNT(DISTINCT StudentId)
                          FROM      #TempFinanceDetails
                        );
    SET @StudentEnrollmentCount = (
                                    SELECT  COUNT(StudentId) AS RowCounter1
                                    FROM    (
                                              SELECT    StudentId
                                                       ,COUNT(*) AS RowCounter
                                              FROM      #Temp
                                              WHERE     StudentId IN ( SELECT DISTINCT
                                                                                StudentId
                                                                       FROM     #TempFinanceDetails )
                                              GROUP BY  StudentId
                                              HAVING    COUNT(*) > 1
                                            ) dt
                                  );
		
			
		--Declare @StudentCount int
		--Set @StudentCount=(Select COUNT(*) from (
		--			       Select COUNT(*) as CountEnroll, StudentId from #Temp where dupEnrollCount>0 and transcount>0 and Stuenrollid is not null
		--			       Group by StudentId )dt )
    UPDATE  #TempFinanceDetails
    SET     TotalStudents = @StudentCount
           ,TotalStudentsMTOE = @StudentEnrollmentCount;
		
    SELECT  *
    FROM    #TempFinanceDetails
    ORDER BY CASE WHEN @OrderBy = 'SSN' THEN SSN
             END
           ,CASE WHEN @OrderBy = 'LastName' THEN StudentName
            END
           ,CASE WHEN @OrderBy = 'Student Number' THEN StudentNumber
            END
           ,CASE WHEN @OrderBy = 'EnrollmentId' THEN EnrollmentId
            END;
	
    DROP TABLE #TempFinanceDetails;
    DROP TABLE #Temp;





GO
