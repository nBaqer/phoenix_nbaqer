SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_StudentPagesByModuleAndPageName_List]
    @ModuleId INT = 0
   ,@PageName VARCHAR(50) = NULL
AS
    SELECT DISTINCT
            t5.ResourceID
           ,t5.Resource
    FROM    syNavigationNodes t1
    INNER JOIN syNavigationNodes t2 ON t1.ParentId = t2.HierarchyId
    INNER JOIN syNavigationNodes t3 ON t2.ParentId = t3.HierarchyId
    INNER JOIN syNavigationNodes t4 ON t3.ParentId = t4.HierarchyId
    INNER JOIN syResources t5 ON t1.ResourceId = t5.ResourceID
    WHERE   t3.ResourceId = 394
            AND (
                  @ModuleId = 0
                  OR t4.ResourceId = @ModuleId
                )
            AND (
                  @PageName IS NULL
                  OR t5.Resource LIKE '' + @PageName + '%'
                )
            AND t5.ResourceTypeID = 3
    ORDER BY t5.Resource;



GO
