SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_AdmissionReps_GetList] @UserName VARCHAR(50)
AS
    SELECT  UserId
    FROM    syUsers
    WHERE   UserName = @UserName;



GO
