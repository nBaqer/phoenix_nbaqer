SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[usp_GetGeneralInfo]
    (
     @stuEnrollId UNIQUEIDENTIFIER
    ,@cutOffDate DATETIME
    )
AS
    SET NOCOUNT ON;

    SELECT  StuEnrollId
           ,StartDate
           ,LDA
           ,ExpGradDate
           ,(
              SELECT    SUM(ST.TransAmount)
              FROM      saTransactions ST
                       ,saTransCodes SC
              WHERE     ST.StuEnrollId = SE.StuEnrollId
                        AND ST.TranscodeId = SC.TransCodeId
                        AND ST.Voided = 0
                        AND SC.IsInstCharge = 1
                        AND TransDate <= @cutOffDate
            ) AS TotalCost
           ,(
              SELECT    SUM(TransAmount)
              FROM      saTransactions
              WHERE     StuEnrollId = SE.StuEnrollId
                        AND Voided = 0
                        AND TransDate <= @cutOffDate
            ) AS CurrentBalance
           ,(
              SELECT    SUM(Actual)
              FROM      atConversionAttendance
              WHERE     StuEnrollId = SE.StuEnrollId
                        AND Actual > 0.0
                        AND MeetDate <= @cutOffDate
            ) AS TotalDaysAttended
           ,(
              SELECT    SUM(Schedule)
              FROM      atConversionAttendance
              WHERE     StuEnrollId = SE.StuEnrollId
                        AND Schedule > 0.0
                        AND MeetDate <= @cutOffDate
            ) AS ScheduledDays
           ,(
              SELECT    SUM(Schedule - Actual)
              FROM      atConversionAttendance
              WHERE     StuEnrollId = SE.StuEnrollId
                        AND Schedule > 0
                        AND MeetDate <= @cutOffDate
            ) AS DaysAbsent
           ,(
              SELECT    SUM(Actual)
              FROM      atConversionAttendance
              WHERE     StuEnrollId = SE.StuEnrollId
                        AND Actual > 0
                        AND Schedule = 0
                        AND MeetDate <= @cutOffDate
            ) AS MakeupDays
           ,(
              SELECT    SUM(PSD.Total)
              FROM      arStudentSchedules SS
                       ,arProgScheduleDetails PSD
              WHERE     StuEnrollId = SE.StuEnrollId
                        AND SS.ScheduleId = PSD.ScheduleId
            ) AS WeeklySchedHours
           ,PV.UnitTypeId
           ,(
              SELECT    UnitTypeDescrip
              FROM      arAttUnitType
              WHERE     arAttUnitType.UnitTypeId = PV.UnitTypeId
            ) AS UnitTypeDescrip
           ,PV.TrackTardies
           ,PV.TardiesMakingAbsence
           ,arStudent.FirstName
           ,arStudent.LastName
           ,arStudent.MiddleName
           ,SE.transferHours
           ,(
              SELECT    ACId
              FROM      arPrograms
              WHERE     arPrograms.ProgId = PV.ProgId
            ) AS ACID
    FROM    arStuEnrollments SE
           ,arStudent
           ,arPrgVersions PV
    WHERE   SE.StudentId = arStudent.StudentId
            AND SE.PrgVerId = PV.PrgVerId
            AND EXISTS ( SELECT DISTINCT
                                arStuEnrollments.StuEnrollId
                         FROM   arStuEnrollments
                               ,arStudent A
                               ,syCampuses C
                               ,syCmpGrpCmps
                               ,syCampGrps
                         WHERE  arStuEnrollments.StudentId = A.StudentId
                                AND arStuEnrollments.CampusId = C.CampusId
                                AND arStuEnrollments.StuEnrollId = SE.StuEnrollId
                                AND syCmpGrpCmps.CampusId = arStuEnrollments.CampusId
                                AND syCampGrps.CampGrpId = syCmpGrpCmps.CampGrpId )
            AND SE.StuEnrollId = @stuEnrollId;





GO
