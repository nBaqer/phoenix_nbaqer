SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_pr_main_subreport4_showunderterm_LabHours503]
    @StuEnrollId VARCHAR(50)
   ,@TermId VARCHAR(50) = NULL
   ,@SysComponentTypeId VARCHAR(50) = NULL
   ,@ShowWorkUnitGrouping BIT = 0
   ,@SetGradeBookAt VARCHAR(50)
AS
    SELECT  GradeBookDescription
           ,GradeBookScore
           ,MinResult
           ,GradeBookSysComponentTypeId
           ,GradeComponentDescription
           ,CampDescrip
           ,FirstName
           ,LastName
           ,MiddleName
           ,rownumber
           ,GrdBkResultId
           ,TermStartDate
           ,TermEndDate
           ,TermDescription
           ,CourseDescription
           ,PrgVerDescrip
    FROM    (
              SELECT    4 AS Tag
                       ,3 AS Parent
                       ,PV.PrgVerId
                       ,PV.PrgVerDescrip
                       ,NULL AS ProgramCredits
                       ,T.TermId
                       ,T.TermDescrip AS TermDescription
                       ,T.StartDate AS TermStartDate
                       ,T.EndDate AS TermEndDate
                       ,R.ReqId AS CourseId
                       ,R.Descrip AS CourseDescription
                       ,'(' + R.Code + ' ) ' + R.Descrip AS CourseCodeDescrip
                       ,NULL AS CourseCredits
                       ,NULL AS CourseFinAidCredits
                       ,NULL AS CoursePassingGrade
                       ,NULL AS CourseScore
                       ,GBR.GrdBkResultId
                       ,CASE WHEN LOWER(@SetGradeBookAt) = 'instructorlevel' THEN RTRIM(GBWD.Descrip)
                             ELSE GCT.Descrip
                        END AS GradeBookDescription
                       ,( CASE GCT.SysComponentTypeId
                            WHEN 544 THEN (
                                            SELECT  SUM(HoursAttended)
                                            FROM    arExternshipAttendance
                                            WHERE   StuEnrollId = SE.StuEnrollId
                                          )
                            ELSE GBR.Score
                          END ) AS GradeBookScore
                       ,NULL AS GradeBookPostDate
                       ,NULL AS GradeBookPassingGrade
                       ,NULL AS GradeBookWeight
                       ,NULL AS GradeBookRequired
                       ,NULL AS GradeBookMustPass
                       ,GCT.SysComponentTypeId AS GradeBookSysComponentTypeId
                       ,NULL AS GradeBookHoursRequired
                       ,NULL AS GradeBookHoursCompleted
                       ,SE.StuEnrollId
                       ,( CASE WHEN GCT.SysComponentTypeId IN ( 500,503,504,544 ) THEN GBWD.Number
                               ELSE (
                                      SELECT    MIN(MinVal)
                                      FROM      arGradeScaleDetails GSD
                                               ,arGradeSystemDetails GSS
                                      WHERE     GSD.GrdSysDetailId = GSS.GrdSysDetailId
                                                AND GSS.IsPass = 1
                                                AND GSD.GrdScaleId = CS.GrdScaleId
                                    )
                          END ) AS MinResult
                       ,SYRES.Resource AS GradeComponentDescription
                       ,NULL AS CreditsAttempted
                       ,NULL AS CreditsEarned
                       ,NULL AS Completed
                       ,NULL AS CurrentScore
                       ,NULL AS CurrentGrade
                       ,GCT.SysComponentTypeId AS FinalScore
                       ,NULL AS FinalGrade
                       ,NULL AS WeightedAverage_GPA
                       ,NULL AS SimpleAverage_GPA
                       ,NULL AS WeightedAverage_CumGPA
                       ,NULL AS SimpleAverage_CumGPA
                       ,C.CampusId
                       ,C.CampDescrip
                       ,ROW_NUMBER() OVER ( PARTITION BY SE.StuEnrollId,GCT.SysComponentTypeId ORDER BY C.CampDescrip, PV.PrgVerDescrip, T.StartDate, T.EndDate, T.TermId, T.TermDescrip, R.ReqId, R.Descrip, GCT.SysComponentTypeId, GCT.Descrip ) AS rownumber
                       ,S.FirstName AS FirstName
                       ,S.LastName AS LastName
                       ,S.MiddleName
              FROM      arGrdBkResults GBR
              INNER JOIN arStuEnrollments SE ON GBR.StuEnrollId = SE.StuEnrollId
              INNER JOIN (
                           SELECT   StudentId
                                   ,FirstName
                                   ,LastName
                                   ,MiddleName
                           FROM     arStudent
                         ) S ON S.StudentId = SE.StudentId
              INNER JOIN arClassSections CS ON CS.ClsSectionId = GBR.ClsSectionId
              INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
              INNER JOIN arTerm T ON CS.TermId = T.TermId
              INNER JOIN arReqs R ON CS.ReqId = R.ReqId
              INNER JOIN arResults RES ON RES.StuEnrollId = GBR.StuEnrollId
                                          AND RES.TestId = CS.ClsSectionId
              INNER JOIN arGrdBkWgtDetails GBWD ON GBR.InstrGrdBkWgtDetailId = GBWD.InstrGrdBkWgtDetailId
              INNER JOIN arGrdComponentTypes GCT ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
              INNER JOIN (
                           SELECT   Resource
                                   ,ResourceID
                           FROM     syResources
                           WHERE    ResourceTypeID = 10
                         ) SYRES ON SYRES.ResourceID = GCT.SysComponentTypeId
              INNER JOIN syCampuses C ON SE.CampusId = C.CampusId
              WHERE     SE.StuEnrollId = @StuEnrollId
                        AND T.TermId = @TermId
                        AND --R.ReqId = @ReqId and 
                        (
                          @SysComponentTypeId IS NULL
                          OR GCT.SysComponentTypeId IN ( SELECT Val
                                                         FROM   MultipleValuesForReportParameters(@SysComponentTypeId,',',1) )
                        )
              UNION
              SELECT    4 AS Tag
                       ,3
                       ,PV.PrgVerId
                       ,PV.PrgVerDescrip
                       ,NULL
                       ,T.TermId
                       ,T.TermDescrip
                       ,T.StartDate AS termStartdate
                       ,T.EndDate AS TermEndDate
                       ,GBCR.ReqId
                       ,R.Descrip AS CourseDescrip
                       ,'(' + R.Code + ' ) ' + R.Descrip AS CourseCodeDescrip
                       ,NULL
                       ,NULL
                       ,NULL
                       ,NULL
                       ,GBCR.ConversionResultId AS GrdBkResultId
                       ,CASE WHEN LOWER(@SetGradeBookAt) = 'instructorlevel' THEN RTRIM(GBWD.Descrip)
                             ELSE GCT.Descrip
                        END AS GradeBookDescription
                       ,GBCR.Score AS GradeBookResult
                       ,NULL
                       ,NULL
                       ,NULL
                       ,NULL
                       ,NULL
                       ,GCT.SysComponentTypeId
                       ,NULL
                       ,NULL
                       ,SE.StuEnrollId
                       ,GBCR.MinResult
                       ,SYRES.Resource -- Student data  
                       ,NULL AS CreditsAttempted
                       ,NULL AS CreditsEarned
                       ,NULL AS Completed
                       ,NULL AS CurrentScore
                       ,NULL AS CurrentGrade
                       ,NULL AS FinalScore
                       ,NULL AS FinalGrade
                       ,NULL AS WeightedAverage_GPA
                       ,NULL AS SimpleAverage_GPA
                       ,NULL
                       ,NULL
                       ,C.CampusId
                       ,C.CampDescrip
                       ,ROW_NUMBER() OVER ( PARTITION BY SE.StuEnrollId,GCT.SysComponentTypeId ORDER BY C.CampDescrip, PV.PrgVerDescrip, T.StartDate, T.EndDate, T.TermId, T.TermDescrip, R.ReqId, R.Descrip, GCT.SysComponentTypeId, GCT.Descrip ) AS rownumber
                       ,S.FirstName AS FirstName
                       ,S.LastName AS LastName
                       ,S.MiddleName
              FROM      arGrdBkConversionResults GBCR
              INNER JOIN arStuEnrollments SE ON GBCR.StuEnrollId = SE.StuEnrollId
              INNER JOIN (
                           SELECT   StudentId
                                   ,FirstName
                                   ,LastName
                                   ,MiddleName
                           FROM     arStudent
                         ) S ON S.StudentId = SE.StudentId
              INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
              INNER JOIN arTerm T ON GBCR.TermId = T.TermId
              INNER JOIN arReqs R ON GBCR.ReqId = R.ReqId
              INNER JOIN arGrdComponentTypes GCT ON GCT.GrdComponentTypeId = GBCR.GrdComponentTypeId
              INNER JOIN arGrdBkWgtDetails GBWD ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
                                                   AND GBWD.GrdComponentTypeId = GBCR.GrdComponentTypeId
              INNER JOIN arGrdBkWeights GBW ON GBWD.InstrGrdBkWgtId = GBW.InstrGrdBkWgtId
                                               AND GBCR.ReqId = GBW.ReqId
              INNER JOIN (
                           SELECT   ReqId
                                   ,MAX(EffectiveDate) AS EffectiveDate
                           FROM     arGrdBkWeights
                           GROUP BY ReqId
                         ) AS MaxEffectiveDatesByCourse ON GBCR.ReqId = MaxEffectiveDatesByCourse.ReqId
              INNER JOIN (
                           SELECT   Resource
                                   ,ResourceID
                           FROM     syResources
                           WHERE    ResourceTypeID = 10
                         ) SYRES ON SYRES.ResourceID = GCT.SysComponentTypeId
              INNER JOIN syCampuses C ON SE.CampusId = C.CampusId
              WHERE     MaxEffectiveDatesByCourse.EffectiveDate <= T.StartDate
                        AND SE.StuEnrollId = @StuEnrollId
                        AND T.TermId = @TermId
                        AND --R.ReqId = @ReqId and 
                        (
                          @SysComponentTypeId IS NULL
                          OR GCT.SysComponentTypeId IN ( SELECT Val
                                                         FROM   MultipleValuesForReportParameters(@SysComponentTypeId,',',1) )
                        )
            ) dt
    WHERE   GradeBookSysComponentTypeId = 503
    ORDER BY GradeComponentDescription
           ,rownumber
           ,GradeBookDescription;

GO
