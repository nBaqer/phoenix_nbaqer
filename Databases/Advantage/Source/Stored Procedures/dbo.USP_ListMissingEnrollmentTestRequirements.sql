SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_ListMissingEnrollmentTestRequirements]
    (
        @StuEnrollID UNIQUEIDENTIFIER
    )
AS 
/*----------------------------------------------------------------------------------------------------  
 Author          : Saraswathi Lakshmanan  
      
    Create date  : 28/09/2010  
      
 Procedure Name : USP_ListMissingEnrollmentTestRequirements  
  
 Objective  : Get the count of test not approved for Enrollment  
    
 Parameters  : Name   Type Data Type   Required?    
      =====   ==== =========   =========   
      @StuEnrollId IN  Uniqueuidentifier Yes  
       
   
 Output   : Returns the requested details   
        
*/
    -----------------------------------------------------------------------------------------------------  
    BEGIN


        --Declare @StuEnrollId as UniqueIdentifier  
        --Declare @CampusID as UniqueIdentifier  

        --Set @StuEnrollId='60DB3379-3DAD-4443-ABC2-F9E211C9AAB8'  
        --Set @CampusID='2CD3849A-DECD-48C5-8C9E-3FBE11351437'  
        DECLARE @StudentId AS UNIQUEIDENTIFIER;
        DECLARE @PrgVerId AS UNIQUEIDENTIFIER;
        DECLARE @CampusID AS UNIQUEIDENTIFIER;
        DECLARE @LeadID AS UNIQUEIDENTIFIER;
        DECLARE @StudentStartDate AS DATETIME;

        SET @StudentId = (
                         SELECT StudentId
                         FROM   arStuEnrollments
                         WHERE  StuEnrollId = @StuEnrollID
                         );
        SET @PrgVerId = (
                        SELECT PrgVerId
                        FROM   arStuEnrollments
                        WHERE  StuEnrollId = @StuEnrollID
                        );
        SET @CampusID = (
                        SELECT CampusId
                        FROM   arStuEnrollments
                        WHERE  StuEnrollId = @StuEnrollID
                        );
        SET @StudentStartDate = (
                                SELECT StartDate
                                FROM   arStuEnrollments
                                WHERE  StuEnrollId = @StuEnrollID
                                );
        SET @LeadID = (
                      SELECT LeadId
                      FROM   arStuEnrollments
                      WHERE  StuEnrollId = @StuEnrollID
                      );
        DECLARE @ActiveStatusID AS UNIQUEIDENTIFIER;
        SET @ActiveStatusID = (
                              SELECT StatusId
                              FROM   syStatuses
                              WHERE  Status = 'Active'
                              );



        SELECT adReqId
              ,Descrip
        FROM   (
               SELECT DISTINCT adReqId
                     ,Descrip
                     ,ReqGrpId
                     ,StartDate
                     ,EndDate
                     ,ActualScore
                     ,TestTaken
                     ,Comments
                     ,CASE WHEN override >= 1 THEN 'True'
                           ELSE 'False'
                      END AS OverRide
                     ,CASE WHEN (
                                SELECT COUNT(*)
                                FROM   adPrgVerTestDetails
                                WHERE  adReqId = R2.adReqId
                                       AND PrgVerId = @PrgVerId
                                ) >= 1 THEN (
                                            SELECT MinScore
                                            FROM   adPrgVerTestDetails
                                            WHERE  adReqId = R2.adReqId
                                                   AND PrgVerId = @PrgVerId
                                            )
                           ELSE MinScore
                      END AS MinScore
                     ,Required
                     ,DocSubmittedCount
                     ,DocStatusDescrip
                     ,CASE WHEN (
                                TestTaken IS NOT NULL
                                AND ActualScore >= 1
                                ) THEN 1
                           ELSE 0
                      END AS TestTakenCount
                     ,Pass
                     ,CampGrpId
               FROM   (
                      SELECT DISTINCT adReqId
                            ,Descrip
                            ,ReqGrpId
                            ,StartDate
                            ,EndDate
                            ,ActualScore
                            ,TestTaken
                            ,Comments
                            ,override
                            ,MinScore
                            ,Required
                            ,CASE WHEN ActualScore >= MinScore THEN 'True'
                                  ELSE 'False'
                             END AS Pass
                            ,DocSubmittedCount
                            ,DocStatusDescrip
                            ,CampGrpId
                      FROM   (
                             SELECT DISTINCT t1.adReqId
                                   ,t1.Descrip
                                   ,CASE WHEN (
                                              SELECT COUNT(*)
                                              FROM   adReqGroups
                                              WHERE  IsMandatoryReqGrp = 1
                                              ) >= 1 THEN (
                                                          SELECT ReqGrpId
                                                          FROM   adReqGroups
                                                          WHERE  IsMandatoryReqGrp = 1
                                                          )
                                         ELSE '00000000-0000-0000-0000-000000000000'
                                    END AS ReqGrpId
                                   -- GETDATE() AS CurrentDate ,  
                                   ,@StudentStartDate AS CurrentDate
                                   ,t2.StartDate
                                   ,t2.EndDate
                                   ,(
                                    SELECT ActualScore
                                    FROM   adLeadEntranceTest
                                    WHERE  EntrTestId = t1.adReqId
                                           AND LeadId = @LeadID
                                    ) AS ActualScore
                                   ,(
                                    SELECT TestTaken
                                    FROM   adLeadEntranceTest
                                    WHERE  EntrTestId = t1.adReqId
                                           AND LeadId = @LeadID
                                    ) AS TestTaken
                                   ,(
                                    SELECT Comments
                                    FROM   adLeadEntranceTest
                                    WHERE  EntrTestId = t1.adReqId
                                           AND LeadId = @LeadID
                                    ) AS Comments
                                   ,(
                                    SELECT OverRide
                                    FROM   adEntrTestOverRide
                                    WHERE  EntrTestId = t1.adReqId
                                           AND LeadId = @LeadID
                                    ) AS override
                                   ,(
                                    SELECT SUM(Res1.DocSubmittedCount)
                                    FROM   (
                                           (SELECT COUNT(*) AS DocSubmittedCount
                                            FROM   dbo.adLeadDocsReceived
                                            WHERE  DocumentId = t1.adReqId
                                                   AND LeadId = @LeadID)
                                           UNION ALL
                                           (SELECT COUNT(*) AS DocSubmittedCount
                                            FROM   plStudentDocs
                                            WHERE  DocumentId = t1.adReqId
                                                   AND StudentId = @StudentId)
                                           ) AS Res1
                                    ) AS DocSubmittedCount
                                   ,t2.MinScore
                                   ,1 AS Required
                                   ,(
                                    SELECT DISTINCT s1.DocStatusDescrip
                                    FROM   sySysDocStatuses s1
                                          ,syDocStatuses s2
                                          ,dbo.adLeadDocsReceived s3
                                    WHERE  s1.SysDocStatusId = s2.SysDocStatusId
                                           AND s2.DocStatusId = s3.DocStatusId
                                           AND s3.LeadId = @LeadID
                                           AND s3.DocumentId = t1.adReqId
                                    ) AS DocStatusDescrip
                                   ,t1.CampGrpId
                             FROM   adReqs t1
                                   ,adReqsEffectiveDates t2
                             WHERE  t1.adReqId = t2.adReqId
                                    AND t2.MandatoryRequirement = 1
                                    AND t1.ReqforEnrollment = 1
                                    AND t1.adReqTypeId IN ( 1 )
                                    AND t1.StatusId = @ActiveStatusID
                             ) R1
                      WHERE  R1.CurrentDate >= R1.StartDate
                             AND (
                                 R1.CurrentDate <= R1.EndDate
                                 OR R1.EndDate IS NULL
                                 )
                      UNION
                      SELECT DISTINCT adReqId
                            ,Descrip
                            ,reqGrpId
                            ,StartDate
                            ,EndDate
                            ,ActualScore
                            ,TestTaken
                            ,Comments
                            ,override
                            ,MinScore
                            ,Required
                            ,CASE WHEN ActualScore >= MinScore THEN 'True'
                                  ELSE 'False'
                             END AS Pass
                            ,DocSubmittedCount
                            ,DocStatusDescrip
                            ,CampGrpId
                      FROM   (
                             SELECT DISTINCT t1.adReqId
                                   ,t1.Descrip
                                   ,'00000000-0000-0000-0000-000000000000' AS reqGrpId
                                   -- GETDATE() AS CurrentDate ,  
                                   ,@StudentStartDate AS CurrentDate
                                   ,t2.StartDate
                                   ,t2.EndDate
                                   ,(
                                    SELECT ActualScore
                                    FROM   adLeadEntranceTest
                                    WHERE  EntrTestId = t1.adReqId
                                           AND LeadId = @LeadID
                                    ) AS ActualScore
                                   ,(
                                    SELECT TestTaken
                                    FROM   adLeadEntranceTest
                                    WHERE  EntrTestId = t1.adReqId
                                           AND LeadId = @LeadID
                                    ) AS TestTaken
                                   ,(
                                    SELECT Comments
                                    FROM   adLeadEntranceTest
                                    WHERE  EntrTestId = t1.adReqId
                                           AND LeadId = @LeadID
                                    ) AS Comments
                                   ,(
                                    SELECT OverRide
                                    FROM   adEntrTestOverRide
                                    WHERE  EntrTestId = t1.adReqId
                                           AND LeadId = @LeadID
                                    ) AS override
                                   ,(
                                    SELECT SUM(Res1.DocSubmittedCount)
                                    FROM   (
                                           (SELECT COUNT(*) AS DocSubmittedCount
                                            FROM   dbo.adLeadDocsReceived
                                            WHERE  DocumentId = t1.adReqId
                                                   AND LeadId = @LeadID)
                                           UNION ALL
                                           (SELECT COUNT(*) AS DocSubmittedCount
                                            FROM   plStudentDocs
                                            WHERE  DocumentId = t1.adReqId
                                                   AND StudentId = @StudentId)
                                           ) AS Res1
                                    ) AS DocSubmittedCount
                                   ,CASE WHEN (
                                              SELECT COUNT(*)
                                              FROM   adPrgVerTestDetails
                                              WHERE  adReqId = t1.adReqId
                                                     AND PrgVerId = @PrgVerId
                                              ) >= 1 THEN (
                                                          SELECT MinScore
                                                          FROM   adPrgVerTestDetails
                                                          WHERE  adReqId = t1.adReqId
                                                                 AND PrgVerId = @PrgVerId
                                                          )
                                         ELSE t2.MinScore
                                    END AS MinScore
                                   ,1 AS Required
                                   ,(
                                    SELECT DISTINCT s1.DocStatusDescrip
                                    FROM   sySysDocStatuses s1
                                          ,syDocStatuses s2
                                          ,dbo.adLeadDocsReceived s3
                                    WHERE  s1.SysDocStatusId = s2.SysDocStatusId
                                           AND s2.DocStatusId = s3.DocStatusId
                                           AND s3.LeadId = @LeadID
                                           AND s3.DocumentId = t1.adReqId
                                    ) AS DocStatusDescrip
                                   ,t1.CampGrpId
                             FROM   adReqs t1
                                   ,adReqsEffectiveDates t2
                                   ,adReqLeadGroups t3
                                   ,adLeads t4
                                   ,adPrgVerTestDetails t5
                             WHERE  t1.adReqId = t2.adReqId
                                    AND t2.MandatoryRequirement <> 1
                                    AND t1.ReqforEnrollment = 1
                                    AND t1.adReqTypeId IN ( 1 )
                                    AND t1.StatusId = @ActiveStatusID
                                    AND t2.adReqEffectiveDateId = t3.adReqEffectiveDateId
                                    AND t4.PrgVerId = t5.PrgVerId
                                    AND t1.adReqId = t5.adReqId
                                    AND t5.PrgVerId = @PrgVerId
                                    --AND t3.LeadGrpId IN (  
                                    --SELECT DISTINCT  
                                    --        LeadGrpId  
                                    --FROM    adLeadByLeadGroups  
                                    --WHERE   LeadId = @LeadID )  
                                    AND t5.adReqId IS NOT NULL
                             ) R1
                      WHERE  R1.CurrentDate >= R1.StartDate
                             AND (
                                 R1.CurrentDate <= R1.EndDate
                                 OR R1.EndDate IS NULL
                                 )
                      UNION
                      SELECT DISTINCT adReqId
                            ,Descrip
                            ,reqGrpId
                            ,StartDate
                            ,EndDate
                            ,ActualScore
                            ,TestTaken
                            ,Comments
                            ,override
                            ,MinScore
                            ,Required
                            ,CASE WHEN ActualScore >= MinScore THEN 'True'
                                  ELSE 'False'
                             END AS Pass
                            ,DocSubmittedCount
                            ,DocStatusDescrip
                            ,CampGrpId
                      FROM   (
                             SELECT DISTINCT t1.adReqId
                                   ,t1.Descrip
                                   ,'00000000-0000-0000-0000-000000000000' AS reqGrpId
                                   -- GETDATE() AS CurrentDate ,  
                                   ,@StudentStartDate AS CurrentDate
                                   ,t2.StartDate
                                   ,t2.EndDate
                                   ,(
                                    SELECT ActualScore
                                    FROM   adLeadEntranceTest
                                    WHERE  EntrTestId = t1.adReqId
                                           AND LeadId = @LeadID
                                    ) AS ActualScore
                                   ,(
                                    SELECT TestTaken
                                    FROM   adLeadEntranceTest
                                    WHERE  EntrTestId = t1.adReqId
                                           AND LeadId = @LeadID
                                    ) AS TestTaken
                                   ,(
                                    SELECT Comments
                                    FROM   adLeadEntranceTest
                                    WHERE  EntrTestId = t1.adReqId
                                           AND LeadId = @LeadID
                                    ) AS Comments
                                   ,(
                                    SELECT OverRide
                                    FROM   adEntrTestOverRide
                                    WHERE  EntrTestId = t1.adReqId
                                           AND LeadId = @LeadID
                                    ) AS override
                                   ,(
                                    SELECT SUM(Res1.DocSubmittedCount)
                                    FROM   (
                                           (SELECT COUNT(*) AS DocSubmittedCount
                                            FROM   dbo.adLeadDocsReceived
                                            WHERE  DocumentId = t1.adReqId
                                                   AND LeadId = @LeadID)
                                           UNION ALL
                                           (SELECT COUNT(*) AS DocSubmittedCount
                                            FROM   plStudentDocs
                                            WHERE  DocumentId = t1.adReqId
                                                   AND StudentId = @StudentId)
                                           ) AS Res1
                                    ) AS DocSubmittedCount
                                   ,t2.MinScore AS MinScore
                                   ,t3.IsRequired AS Required
                                   ,(
                                    SELECT DISTINCT s1.DocStatusDescrip
                                    FROM   sySysDocStatuses s1
                                          ,syDocStatuses s2
                                          ,dbo.adLeadDocsReceived s3
                                    WHERE  s1.SysDocStatusId = s2.SysDocStatusId
                                           AND s2.DocStatusId = s3.DocStatusId
                                           AND s3.LeadId = @LeadID
                                           AND s3.DocumentId = t1.adReqId
                                    ) AS DocStatusDescrip
                                   ,t1.CampGrpId
                             FROM   adReqs t1
                                   ,adReqsEffectiveDates t2
                                   ,adReqLeadGroups t3
                                   ,adPrgVerTestDetails t5
                                   ,adReqGrpDef t6
                                   ,adLeadByLeadGroups t7
                             WHERE  t1.adReqId = t2.adReqId
                                    AND t1.adReqTypeId IN ( 1 )
                                    AND t1.StatusId = @ActiveStatusID
                                    AND t1.ReqforEnrollment = 1
                                    AND t2.adReqEffectiveDateId = t3.adReqEffectiveDateId
                                    AND t5.ReqGrpId = t6.ReqGrpId
                                    AND t3.LeadGrpId = t7.LeadGrpId
                                    AND t6.LeadGrpId = t7.LeadGrpId
                                    AND t1.adReqId = t6.adReqId
                                    AND t5.ReqGrpId IS NOT NULL
                                    AND ( LeadId = @LeadID )
                                    AND t5.PrgVerId = @PrgVerId
                             ) R1
                      WHERE  R1.CurrentDate >= R1.StartDate
                             AND (
                                 R1.CurrentDate <= R1.EndDate
                                 OR R1.EndDate IS NULL
                                 )
                      UNION
                      SELECT DISTINCT adReqId
                            ,Descrip
                            ,ReqGrpId
                            ,StartDate
                            ,EndDate
                            ,ActualScore
                            ,TestTaken
                            ,Comments
                            ,override
                            ,MinScore
                            ,Required
                            ,CASE WHEN ActualScore >= MinScore THEN 'True'
                                  ELSE 'False'
                             END AS Pass
                            ,DocSubmittedCount
                            ,DocStatusDescrip
                            ,CampGrpId
                      FROM   (
                             SELECT DISTINCT t1.adReqId
                                   ,t1.Descrip
                                   ,'00000000-0000-0000-0000-000000000000' AS ReqGrpId
                                   -- GETDATE() AS CurrentDate ,  
                                   ,@StudentStartDate AS CurrentDate
                                   ,t2.StartDate
                                   ,t2.EndDate
                                   ,(
                                    SELECT ActualScore
                                    FROM   adLeadEntranceTest
                                    WHERE  EntrTestId = t1.adReqId
                                           AND LeadId = @LeadID
                                    ) AS ActualScore
                                   ,(
                                    SELECT TestTaken
                                    FROM   adLeadEntranceTest
                                    WHERE  EntrTestId = t1.adReqId
                                           AND LeadId = @LeadID
                                    ) AS TestTaken
                                   ,(
                                    SELECT Comments
                                    FROM   adLeadEntranceTest
                                    WHERE  EntrTestId = t1.adReqId
                                           AND LeadId = @LeadID
                                    ) AS Comments
                                   ,(
                                    SELECT OverRide
                                    FROM   adEntrTestOverRide
                                    WHERE  EntrTestId = t1.adReqId
                                           AND LeadId = @LeadID
                                    ) AS override
                                   ,(
                                    SELECT SUM(Res1.DocSubmittedCount)
                                    FROM   (
                                           (SELECT COUNT(*) AS DocSubmittedCount
                                            FROM   dbo.adLeadDocsReceived
                                            WHERE  DocumentId = t1.adReqId
                                                   AND LeadId = @LeadID)
                                           UNION ALL
                                           (SELECT COUNT(*) AS DocSubmittedCount
                                            FROM   plStudentDocs
                                            WHERE  DocumentId = t1.adReqId
                                                   AND StudentId = @StudentId)
                                           ) AS Res1
                                    ) AS DocSubmittedCount
                                   ,t2.MinScore AS MinScore
                                   ,t3.IsRequired AS Required
                                   ,(
                                    SELECT DISTINCT s1.DocStatusDescrip
                                    FROM   sySysDocStatuses s1
                                          ,syDocStatuses s2
                                          ,dbo.adLeadDocsReceived s3
                                    WHERE  s1.SysDocStatusId = s2.SysDocStatusId
                                           AND s2.DocStatusId = s3.DocStatusId
                                           AND s3.LeadId = @LeadID
                                           AND s3.DocumentId = t1.adReqId
                                    ) AS DocStatusDescrip
                                   ,t1.CampGrpId
                             FROM   adReqs t1
                                   ,adReqsEffectiveDates t2
                                   ,adReqLeadGroups t3
                             WHERE  t1.adReqId = t2.adReqId
                                    AND t2.adReqEffectiveDateId = t3.adReqEffectiveDateId
                                    AND t1.StatusId = @ActiveStatusID
                                    AND t1.ReqforEnrollment = 1
                                    AND t1.adReqId NOT IN (
                                                          SELECT DISTINCT s1.adReqId
                                                          FROM   adReqGrpDef s1
                                                                ,adPrgVerTestDetails s2
                                                          WHERE  s1.ReqGrpId = s2.ReqGrpId
                                                                 AND s2.PrgVerId = @PrgVerId
                                                                 AND s2.adReqId IS NULL
                                                          )
                                    AND t3.LeadGrpId IN (
                                                        SELECT LeadGrpId
                                                        FROM   adLeadByLeadGroups
                                                        WHERE  LeadId = @LeadID
                                                        )
                                    AND t1.adReqTypeId IN ( 1 )
                                    AND t2.MandatoryRequirement <> 1
                                    AND t1.adReqId NOT IN (
                                                          SELECT DISTINCT adReqId
                                                          FROM   adPrgVerTestDetails
                                                          WHERE  adReqId IS NOT NULL
                                                                 AND PrgVerId = @PrgVerId
                                                          )
                             ) R1
                      WHERE  R1.CurrentDate >= R1.StartDate
                             AND (
                                 R1.CurrentDate <= R1.EndDate
                                 OR R1.EndDate IS NULL
                                 )
                      ) R2
               WHERE  R2.Required = 1
                      AND R2.Pass = 'False'
               ) R3
        WHERE  R3.CampGrpId IN (
                               SELECT DISTINCT t2.CampGrpId
                               FROM   syCmpGrpCmps t1
                                     ,syCampGrps t2
                               WHERE  t1.CampGrpId = t2.CampGrpId
                                      AND t1.CampusId = @CampusID
                                      AND t2.StatusId = @ActiveStatusID
                               );
    --        AND R3.OverRide <> 'True';  -- DE13873 -- US10746
    --GROUP BY OverRide  
    --HAVING  R3.OverRide = 'False'   

    END;
GO
