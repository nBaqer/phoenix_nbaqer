SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[USP_AR_InstructionType_Update]
    (
     @InstructionTypeId UNIQUEIDENTIFIER
    ,@InstructionTypeCode VARCHAR(10)
    ,@InstructionTypeDescrip VARCHAR(50)
    ,@StatusId UNIQUEIDENTIFIER
    ,@IsDefault BIT
    ,@UserName VARCHAR(50)
    )
AS /*----------------------------------------------------------------------------------------------------
	Author          :	Janet Robinson
    
    Create date		:	08/25/2011
    
	Procedure Name	:	[USP_AR_InstructionType_Update]

	Objective		:	Updates table arInstructionType
	
	Parameters		:	Name						Type	Data Type				Required? 	
						=====						====	=========				=========	
						@InstructionTypeId			In		UniqueIDENTIFIER		Required
						@InstructionTypeCode		In		varchar					Required						
						@InstructionTypeDescrip		In		varchar					Required
						@StatusId					In		UniqueIDENTIFIER		Required
						@IsDefault					In		bit						Required
						@UserName					In		varchar					Required
	Output			:			
						
*/-----------------------------------------------------------------------------------------------------

    BEGIN

        UPDATE  arInstructionType
        SET     InstructionTypeCode = @InstructionTypeCode
               ,InstructionTypeDescrip = @InstructionTypeDescrip
               ,StatusId = @StatusId
               ,IsDefault = @IsDefault
               ,ModUser = @UserName
               ,ModDate = GETDATE()
        WHERE   InstructionTypeId = @InstructionTypeId;
		 
    END;




GO
