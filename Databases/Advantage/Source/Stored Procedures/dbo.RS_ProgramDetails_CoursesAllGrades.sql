SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[RS_ProgramDetails_CoursesAllGrades]
	-- Add the parameters for the stored procedure here
    @CampusId VARCHAR(36) = 'DC42A60A-5EB9-49B6-ADF6-535966F2E34A' -- Hialeah
   ,@PrgVerId VARCHAR(36) = 'F185CD68-5BBE-4EF8-A85A-2D9641C914EC' -- Cosmetology-Aft (Historical)
   ,@StatusId VARCHAR(36) = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' -- Active
   ,@CoursesId VARCHAR(MAX) = '' -- List of Courses
AS
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;
       

        DECLARE @PrgVerGuid UNIQUEIDENTIFIER = @PrgVerId; 
        DECLARE @CampusGuid UNIQUEIDENTIFIER = @CampusId;
        DECLARE @StatusGuid UNIQUEIDENTIFIER = @StatusId;
        IF @CoursesId = ''
            SET @CoursesId = NULL;

		-- Status ID if NULL or EMPTY select something different to A
        DECLARE @StatusCode VARCHAR(2);
        IF @StatusId = ''
            OR @StatusId IS NULL
            BEGIN
                SET @StatusCode = 'Z'; -- Alls
            END;
        ELSE
            BEGIN
                SET @StatusCode = (
                                    SELECT TOP 1
                                            StatusCode
                                    FROM    dbo.syStatuses
                                    WHERE   StatusId = @StatusId
                                  );
            END;

        IF @StatusCode = 'A' -- Active
            BEGIN

	
                SELECT  def.IsRequired
                       ,req.Code
                       ,req.Descrip AS CourseDescript
                       ,req.Credits
                       ,req.Hours
                       ,det.Weight
                       ,typ.Descrip AS GradeDescript
                       ,grd.EffectiveDate
                       ,det.Number
                       ,res.Resource
                       ,res.ResourceID
                       ,req.FinAidCredits
                FROM    arReqs req
                JOIN    syStatuses ON syStatuses.StatusId = req.StatusId
                JOIN    arProgVerDef def ON def.ReqId = req.ReqId
                LEFT JOIN arGrdBkWeights grd ON grd.ReqId = req.ReqId
                LEFT JOIN dbo.arGrdBkWgtDetails det ON det.InstrGrdBkWgtId = grd.InstrGrdBkWgtId
                LEFT JOIN dbo.arGrdComponentTypes typ ON typ.GrdComponentTypeId = det.GrdComponentTypeId
                LEFT JOIN dbo.syResources res ON res.ResourceID = typ.SysComponentTypeId
                WHERE   syStatuses.StatusCode = @StatusCode  -- Course Active
                        AND def.PrgVerId = @PrgVerGuid
                        AND req.ReqTypeId = 1 -- Type Course
                        AND req.CampGrpId IN ( SELECT   CampGrpId
                                               FROM     syCmpGrpCmps
                                               WHERE    CampusId = @CampusGuid )
                        AND (
                              @CoursesId IS NULL
                              OR req.ReqId IN ( SELECT  *
                                                FROM    dbo.MultipleValuesForReportParameters(@CoursesId,',',0) )
                            )
                UNION
                SELECT  0 AS IsRequired
                       ,T5.Code
                       ,T5.Descrip + ' [' + req.Descrip + ']' AS CourseDescript
                       ,T5.Credits
                       ,T5.Hours
                       ,det.Weight
                       ,typ.Descrip AS GradeDescript
                       ,grd.EffectiveDate
                       ,det.Number
                       ,res.Resource
                       ,res.ResourceID
                       ,T5.FinAidCredits 
                        --T5.CourseCategoryId
                FROM    arReqs req
                JOIN    arProgVerDef def ON def.ReqId = req.ReqId
                JOIN    arReqGrpDef t4 ON t4.GrpId = req.ReqId
                JOIN    arReqs T5 ON T5.ReqId = t4.ReqId
						--JOIN arProgVerDef def ON def.ReqId = T5.ReqId
                LEFT JOIN arGrdBkWeights grd ON grd.ReqId = T5.ReqId
                LEFT JOIN dbo.arGrdBkWgtDetails det ON det.InstrGrdBkWgtId = grd.InstrGrdBkWgtId
                LEFT JOIN dbo.arGrdComponentTypes typ ON typ.GrdComponentTypeId = det.GrdComponentTypeId
                LEFT JOIN dbo.syResources res ON res.ResourceID = typ.SysComponentTypeId
                WHERE   req.StatusId = @StatusId
                        AND T5.StatusId = @StatusId
                        AND req.ReqTypeId = 2
                        AND def.PrgVerId = @PrgVerId
                        AND req.CampGrpId IN ( SELECT   CampGrpId
                                               FROM     syCmpGrpCmps
                                               WHERE    CampusId = @CampusId )
                        AND (
                              @CoursesId IS NULL
                              OR T5.ReqId IN ( SELECT   *
                                               FROM     dbo.MultipleValuesForReportParameters(@CoursesId,',',0) )
                            );
            END;
        ELSE
            BEGIN

                SELECT  def.IsRequired
                       ,req.Code
                       ,req.Descrip AS CourseDescript
                       ,req.Credits
                       ,req.Hours
                       ,det.Weight
                       ,typ.Descrip AS GradeDescript
                       ,grd.EffectiveDate
                       ,det.Number
                       ,res.Resource
                       ,res.ResourceID
                       ,req.FinAidCredits 
                        --req.CourseCategoryId --, det.seq
                FROM    arReqs req
                JOIN    arProgVerDef def ON def.ReqId = req.ReqId
                LEFT JOIN arGrdBkWeights grd ON grd.ReqId = req.ReqId
                LEFT JOIN dbo.arGrdBkWgtDetails det ON det.InstrGrdBkWgtId = grd.InstrGrdBkWgtId
                LEFT JOIN dbo.arGrdComponentTypes typ ON typ.GrdComponentTypeId = det.GrdComponentTypeId
                LEFT JOIN dbo.syResources res ON res.ResourceID = typ.SysComponentTypeId
                WHERE   def.PrgVerId = @PrgVerGuid
                        AND req.ReqTypeId = 1 -- Type Course
                        AND req.CampGrpId IN ( SELECT   CampGrpId
                                               FROM     syCmpGrpCmps
                                               WHERE    CampusId = @CampusGuid )
                        AND (
                              @CoursesId IS NULL
                              OR req.ReqId IN ( SELECT  *
                                                FROM    dbo.MultipleValuesForReportParameters(@CoursesId,',',0) )
                            )
                UNION
                SELECT  0 AS IsRequired
                       ,T5.Code
                       ,T5.Descrip + ' [' + req.Descrip + ']' AS CourseDescript
                       ,T5.Credits
                       ,T5.Hours
                       ,det.Weight
                       ,typ.Descrip AS GradeDescript
                       ,grd.EffectiveDate
                       ,det.Number
                       ,res.Resource
                       ,res.ResourceID
                       ,T5.FinAidCredits 
                        --T5.CourseCategoryId
                FROM    arReqs req
                JOIN    arProgVerDef def ON def.ReqId = req.ReqId
                JOIN    arReqGrpDef t4 ON t4.GrpId = req.ReqId
                JOIN    arReqs T5 ON T5.ReqId = t4.ReqId
						--JOIN arProgVerDef def ON def.ReqId = T5.ReqId
                LEFT JOIN arGrdBkWeights grd ON grd.ReqId = T5.ReqId
                LEFT JOIN dbo.arGrdBkWgtDetails det ON det.InstrGrdBkWgtId = grd.InstrGrdBkWgtId
                LEFT JOIN dbo.arGrdComponentTypes typ ON typ.GrdComponentTypeId = det.GrdComponentTypeId
                LEFT JOIN dbo.syResources res ON res.ResourceID = typ.SysComponentTypeId
                WHERE   req.ReqTypeId = 2
                        AND PrgVerId = @PrgVerId
                        AND req.CampGrpId IN ( SELECT   CampGrpId
                                               FROM     syCmpGrpCmps
                                               WHERE    CampusId = @CampusId )
                        AND (
                              @CoursesId IS NULL
                              OR T5.ReqId IN ( SELECT   *
                                               FROM     dbo.MultipleValuesForReportParameters(@CoursesId,',',0) )
                            ); 
            END;
    END;





GO
