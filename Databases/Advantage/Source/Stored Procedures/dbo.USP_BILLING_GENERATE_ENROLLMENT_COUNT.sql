SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_BILLING_GENERATE_ENROLLMENT_COUNT]
(
    @StartDate AS DATETIME,
    @EndDate AS DATETIME
)
AS
DECLARE @YEAR AS VARCHAR(4);
------------------------------------------------------------------
--SET YEAR VARIABLE
------------------------------------------------------------------
SET @YEAR = '2018';
----------------------------------------------------------------------

----------------------------------------------------------------
--VARIABLE DECLARATION: DO NOT CHANGE ANYTHING FROM HERE
---------------------------------------------------------------
--DECLARE @StartDate AS DATETIME;
--DECLARE @EndDate AS DATETIME;
DECLARE @NoTrackAttendanceType AS UNIQUEIDENTIFIER;
------------------------------------------------------------------

--SET @StartDate = '2/12/' + @YEAR + '';
--SET @EndDate = '3/20/' + @YEAR + '';
SET @NoTrackAttendanceType =
(
    SELECT UnitTypeId FROM arAttUnitType WHERE UnitTypeDescrip = 'None'
);

DECLARE @EnrollmentsLDA AS TABLE
(
    StuEnrollId UNIQUEIDENTIFIER,
    LDA DATETIME
);

INSERT INTO @EnrollmentsLDA
SELECT StuEnrollId,
       MAX(LDA) AS LDA
FROM
(
    SELECT StuEnrollId,
           MAX(AttendedDate) AS LDA
    FROM arExternshipAttendance
    WHERE HoursAttended > 0
    GROUP BY StuEnrollId,
             AttendedDate
    UNION ALL
    SELECT StuEnrollId,
           MAX(MeetDate) AS LDA
    FROM atClsSectAttendance
    WHERE Actual >= 1
          AND
          (
              Actual <> 99.00
              AND Actual <> 999.00
              AND Actual <> 9999.00
          )
    GROUP BY StuEnrollId,
             MeetDate
    UNION ALL
    SELECT EnrollId,
           MAX(AttendanceDate) AS LDA
    FROM atAttendance
    WHERE Actual >= 1
    GROUP BY EnrollId,
             AttendanceDate
    UNION ALL
    SELECT StuEnrollId,
           MAX(RecordDate) AS LDA
    FROM arStudentClockAttendance
    WHERE (
              ActualHours >= 1.00
              AND ActualHours <> 99.00
              AND ActualHours <> 999.00
              AND ActualHours <> 9999.00
          )
    GROUP BY StuEnrollId,
             RecordDate
    UNION ALL
    SELECT StuEnrollId,
           MAX(MeetDate) AS LDA
    FROM atConversionAttendance
    WHERE (
              Actual >= 1.00
              AND Actual <> 99.00
              AND Actual <> 999.00
              AND Actual <> 9999.00
          )
    GROUP BY StuEnrollId,
             MeetDate
    UNION ALL
    SELECT StuEnrollId,
           LDA
    FROM dbo.arStuEnrollments
    WHERE LDA IS NOT NULL
) AS enrolmentLDAS
WHERE enrolmentLDAS.LDA >= @StartDate
GROUP BY StuEnrollId;


DECLARE @DetailedResults AS TABLE
(
    EnrollmentId VARCHAR(50),
    StudentId UNIQUEIDENTIFIER,
    FirstName VARCHAR(100),
    LastName VARCHAR(100),
    EnrollmentStartDate DATETIME,
    EnrollmentEndDate DATETIME,
    EnrollmentLda DATETIME,
    EarliestClassStartDate DATETIME,
    LatestClassStartDate DATETIME,
    DropDate DATETIME,
    Status VARCHAR(50),
    GraduationDate DATETIME,
    TransferOutDate DATETIME,
    UnitTypeId UNIQUEIDENTIFIER,
    NoTrackAttendanceType UNIQUEIDENTIFIER,
    DateDetermined DATETIME,
    EndDate DATETIME,
    StartDate DATETIME,
    CampCode VARCHAR(10),
    CampDescription VARCHAR(100)
);

INSERT INTO @DetailedResults
SELECT enrollments.EnrollmentId,
       leads.StudentId,
       leads.FirstName,
       leads.LastName,
       enrollments.StartDate AS EnrollmentStartDate,
       CASE
           WHEN enrollments.ContractedGradDate >= enrollments.ExpGradDate THEN
               enrollments.ContractedGradDate
           ELSE
               enrollments.ExpGradDate
       END AS EnrollmentEndDate,
       enrollmentLda.LDA AS EnrollmentLda,
       MIN(class.StartDate) AS EarliestClassStartDate,
       MAX(class.StartDate) AS LatestClassStartDate,
       enrollments.DateDetermined AS DropDate,
       SC.StatusCodeDescrip AS Status,
       enrollments.ContractedGradDate AS GraduationDate,
       enrollments.TransferDate AS TransferOutDate,
       programVersion.UnitTypeId,
       @NoTrackAttendanceType,
       enrollments.DateDetermined,
       @EndDate,
       @StartDate,
       CM.CampCode,
       CM.CampDescrip
--,enrollmentLda.LDA, enrollments.DropReasonId, 
--enrollments.DateDetermined,StudentNumber,enrollments.TransferDate, class.StartDate, class.EndDate, course.ResultId 
FROM adLeads leads
    INNER JOIN arStuEnrollments enrollments
        ON leads.StudentId = enrollments.StudentId
    INNER JOIN syStatusCodes SC
        ON enrollments.StatusCodeId = SC.StatusCodeId
    INNER JOIN arPrgVersions programVersion
        ON programVersion.PrgVerId = enrollments.PrgVerId
    INNER JOIN syCampuses CM
        ON CM.CampusId = enrollments.CampusId
    INNER JOIN arResults course
        ON course.StuEnrollId = enrollments.StuEnrollId
    INNER JOIN arClassSections class
        ON class.ClsSectionId = course.TestId
    LEFT JOIN @EnrollmentsLDA enrollmentLda
        ON enrollmentLda.StuEnrollId = enrollments.StuEnrollId
WHERE leads.StudentId <> '00000000-0000-0000-0000-000000000000'
      AND enrollments.StartDate <= @EndDate
      AND enrollments.ExpGradDate >= @StartDate
      /*CHECK IF PROGRAM VERSION TRACK ATTENDANCE*/
      AND
      (
          (
              programVersion.UnitTypeId = @NoTrackAttendanceType
              --IF PROGRAM VERSION DOES NO TRACK ATTENDANCE, THEM FIND IF ENROLLMENT IS SCHEDULE IN CLASS WITHIN DATE RANGE
              AND enrollments.StuEnrollId IN
                  (
                      SELECT DISTINCT
                             course.StuEnrollId
                      FROM arResults course
                          INNER JOIN arStuEnrollments enrollments
                              ON course.StuEnrollId = enrollments.StuEnrollId
                          INNER JOIN arPrgVersions programVersion
                              ON programVersion.PrgVerId = enrollments.PrgVerId
                          INNER JOIN arClassSections class
                              ON class.ClsSectionId = course.TestId
                      WHERE class.StartDate <= @EndDate
                            AND class.EndDate >= @StartDate
                  )
              AND
              (
                  enrollments.DropReasonId IS NULL
                  OR
                  (
                      enrollments.DropReasonId IS NOT NULL
                      AND enrollments.DateDetermined < @EndDate
                      AND enrollments.DateDetermined > @StartDate
                  )
              )
          )
          OR
          (
              enrollmentLda.LDA IS NOT NULL
              AND leads.StudentId <> '00000000-0000-0000-0000-000000000000'
              AND enrollments.StartDate <= @EndDate
              AND enrollments.ExpGradDate >= @StartDate
          )
      )
GROUP BY enrollments.EnrollmentId,
         leads.StudentId,
         leads.FirstName,
         leads.LastName,
         enrollments.StartDate,
         enrollments.ContractedGradDate,
         enrollments.ExpGradDate,
         enrollmentLda.LDA,
         enrollments.DateDetermined,
         SC.StatusCodeDescrip,
         enrollments.TransferDate,
         programVersion.UnitTypeId,
         CM.CampCode,
         CM.CampDescrip,
         enrollments.DateDetermined
ORDER BY CM.CampCode,
         leads.LastName,
         leads.FirstName;

SELECT COUNT(DISTINCT StudentId) AS 'Total count of Students that are considered for billing purpose. The second list is the detailed data with the enrollment(s)'
FROM @DetailedResults;

SELECT CampCode,
       CampDescription,
       EnrollmentId,
       StudentId,
       FirstName,
       LastName,
       EnrollmentStartDate,
       EnrollmentEndDate,
       EnrollmentLda,
       EarliestClassStartDate,
       LatestClassStartDate,
       DropDate,
       Status,
       GraduationDate,
       TransferOutDate,
       DateDetermined,
       StartDate AS BillingStartDate,
       EndDate AS BillingEndDate
FROM @DetailedResults;
GO
