SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[USP_StudentGroup_GET]
    @CampusId UNIQUEIDENTIFIER
   ,@ShowActiveOnly BIT = 1
AS
    BEGIN
        IF ( @ShowActiveOnly = 1 )
            BEGIN
                SELECT DISTINCT
                        LG.LeadGrpId
                       ,LG.Descrip
                FROM    dbo.adLeadGroups LG
                INNER JOIN syCampGrps CG ON CG.CampGrpId = LG.CampGrpId
                INNER JOIN dbo.syCmpGrpCmps CGC ON CG.CampGrpId = CGC.CampGrpId
                WHERE   CGC.CampusId = @CampusId
                        AND LG.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965';
            END;
        ELSE
            BEGIN
                SELECT DISTINCT
                        LG.LeadGrpId
                       ,LG.Descrip
                FROM    dbo.adLeadGroups LG
                INNER JOIN syCampGrps CG ON CG.CampGrpId = LG.CampGrpId
                INNER JOIN dbo.syCmpGrpCmps CGC ON CG.CampGrpId = CGC.CampGrpId
                WHERE   CGC.CampusId = @CampusId;
            END;
    END;

GO
