SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--=================================================================================================
-- USP_PostClsSectAttendance_Insert
--=================================================================================================
CREATE PROCEDURE [dbo].[USP_PostClsSectAttendance_Insert]
    (
        @AttValues NTEXT
       ,@ModUser VARCHAR(50)
    )
AS
    BEGIN
        BEGIN TRAN;

        BEGIN TRY
            SET NOCOUNT ON;
            DECLARE @hDoc INT;
            --Prepare input values as an XML document        
            EXEC sp_xml_preparedocument @hDoc OUTPUT
                                       ,@AttValues;
            -- Delete all records from arStudentClockAttendance table based on StuEnrollId,ScheduleId, RecordDate        

            SELECT StuEnrollId
            FROM
                   OPENXML(@hDoc, '/NewDataSet/atClsSectAttendance', 1)
                       WITH (
                            StuEnrollId VARCHAR(50)
                            );

            SELECT ClsSectionId
            FROM
                   OPENXML(@hDoc, '/NewDataSet/atClsSectAttendance', 1)
                       WITH (
                            ClsSectionId VARCHAR(50)
                            );

            SELECT ClsSectMeetingId
            FROM
                   OPENXML(@hDoc, '/NewDataSet/atClsSectAttendance', 1)
                       WITH (
                            ClsSectMeetingId VARCHAR(50)
                            );

            SELECT CONVERT(VARCHAR(16), REPLACE(MeetDate, 'T', ' ')) MeetDate
            FROM
                   OPENXML(@hDoc, '/NewDataSet/atClsSectAttendance', 1)
                       WITH (
                            MeetDate VARCHAR(16)
                            );


            SELECT *
            FROM   atClsSectAttendance
            WHERE  StuEnrollId IN (
                                  SELECT StuEnrollId
                                  FROM
                                         OPENXML(@hDoc, '/NewDataSet/atClsSectAttendance', 1)
                                             WITH (
                                                  StuEnrollId VARCHAR(50)
                                                  )
                                  )
                   AND ClsSectionId IN (
                                       SELECT ClsSectionId
                                       FROM
                                              OPENXML(@hDoc, '/NewDataSet/atClsSectAttendance', 1)
                                                  WITH (
                                                       ClsSectionId VARCHAR(50)
                                                       )
                                       )
                   AND ClsSectMeetingId IN (
                                           SELECT ClsSectMeetingId
                                           FROM
                                                  OPENXML(@hDoc, '/NewDataSet/atClsSectAttendance', 1)
                                                      WITH (
                                                           ClsSectMeetingId VARCHAR(50)
                                                           )
                                           )
                   AND MeetDate IN (
                                   SELECT CONVERT(VARCHAR(16), REPLACE(MeetDate, 'T', ' '))
                                   FROM
                                          OPENXML(@hDoc, '/NewDataSet/atClsSectAttendance', 1)
                                              WITH (
                                                   MeetDate VARCHAR(16)
                                                   )
                                   );


            --Division by zero for testing purposes 
            --SELECT 1/0		 

            DELETE FROM atClsSectAttendance
            WHERE StuEnrollId IN (
                                 SELECT StuEnrollId
                                 FROM
                                        OPENXML(@hDoc, '/NewDataSet/atClsSectAttendance', 1)
                                            WITH (
                                                 StuEnrollId VARCHAR(50)
                                                 )
                                 )
                  AND ClsSectionId IN (
                                      SELECT ClsSectionId
                                      FROM
                                             OPENXML(@hDoc, '/NewDataSet/atClsSectAttendance', 1)
                                                 WITH (
                                                      ClsSectionId VARCHAR(50)
                                                      )
                                      )
                  AND ClsSectMeetingId IN (
                                          SELECT ClsSectMeetingId
                                          FROM
                                                 OPENXML(@hDoc, '/NewDataSet/atClsSectAttendance', 1)
                                                     WITH (
                                                          ClsSectMeetingId VARCHAR(50)
                                                          )
                                          )
                  AND MeetDate IN (
                                  SELECT CONVERT(VARCHAR(16), REPLACE(MeetDate, 'T', ' '))
                                  FROM
                                         OPENXML(@hDoc, '/NewDataSet/atClsSectAttendance', 1)
                                             WITH (
                                                  MeetDate VARCHAR(16)
                                                  )
                                  );

            SELECT 'DELETED';



            IF EXISTS (
                      SELECT *
                      FROM   sysobjects
                      WHERE  type = 'TR'
                             AND name = 'TR_InsertAttendanceSummary_ByClass_PA'
                      )
                BEGIN
                    DISABLE TRIGGER TR_InsertAttendanceSummary_ByClass_PA ON dbo.atClsSectAttendance;
                END;

            IF EXISTS (
                      SELECT *
                      FROM   sysobjects
                      WHERE  type = 'TR'
                             AND name = 'TR_InsertAttendanceSummary_ByClass_Minutes'
                      )
                BEGIN
                    DISABLE TRIGGER TR_InsertAttendanceSummary_ByClass_Minutes ON dbo.atClsSectAttendance;
                END;


            IF EXISTS (
                      SELECT *
                      FROM   sysobjects
                      WHERE  type = 'TR'
                             AND name = 'TR_InsertClsSectionAttendance_Class_ClockHour'
                      )
                BEGIN
                    DISABLE TRIGGER TR_InsertClsSectionAttendance_Class_ClockHour ON dbo.atClsSectAttendance;
                END;


            ---end code by Balaji to insert into the summary table for progress reports       
            -- Insert records into arStudentClockAttendance Table        
            SELECT 'Deleted Trigger';

            SELECT NEWID()
                  ,StuEnrollId
                  ,ClsSectionId
                  ,ClsSectMeetingId
                  ,CONVERT(VARCHAR(16), REPLACE(MeetDate, 'T', ' '))
                  ,Scheduled
                  ,Actual
                  ,Tardy
                  ,Excused
            FROM
                   OPENXML(@hDoc, '/NewDataSet/atClsSectAttendance', 1)
                       WITH (
                            StuEnrollId UNIQUEIDENTIFIER
                           ,ClsSectionId UNIQUEIDENTIFIER
                           ,ClsSectMeetingId UNIQUEIDENTIFIER
                           ,MeetDate VARCHAR(16)
                           ,Scheduled DECIMAL(18, 0)
                           ,Actual DECIMAL(18, 0)
                           ,Tardy BIT
                           ,Excused BIT
                            );



            --Division by zero for testing purposes		 
            --SELECT 1/0 

            INSERT INTO dbo.atClsSectAttendance (
                                                ClsSectAttId
                                               ,StuEnrollId
                                               ,ClsSectionId
                                               ,ClsSectMeetingId
                                               ,MeetDate
                                               ,Scheduled
                                               ,Actual
                                               ,Tardy
                                               ,Excused
                                                )
                        SELECT NEWID()
                              ,StuEnrollId
                              ,ClsSectionId
                              ,ClsSectMeetingId
                              ,CONVERT(VARCHAR(16), REPLACE(MeetDate, 'T', ' '))
                              ,Scheduled
                              ,Actual
                              ,Tardy
                              ,Excused
                        FROM
                               OPENXML(@hDoc, '/NewDataSet/atClsSectAttendance', 1)
                                   WITH (
                                        StuEnrollId UNIQUEIDENTIFIER
                                       ,ClsSectionId UNIQUEIDENTIFIER
                                       ,ClsSectMeetingId UNIQUEIDENTIFIER
                                       ,MeetDate VARCHAR(16)
                                       ,Scheduled DECIMAL(18, 0)
                                       ,Actual DECIMAL(18, 0)
                                       ,Tardy BIT
                                       ,Excused BIT
                                        );




            ------ AD-5116 Enter Class Section Attendance to use default Currently Attending Status  ------    

            -- temp table to find current default statuses per campus group
            DECLARE @CampusGroupStatuses TABLE
                (
                    StuEnrollId UNIQUEIDENTIFIER NOT NULL
                   ,StatusCodeId UNIQUEIDENTIFIER NOT NULL
                   ,CampusId UNIQUEIDENTIFIER NOT NULL
                );

            INSERT INTO @CampusGroupStatuses
                        SELECT campGrpsWithStuEnrollments.StuEnrollId
                              ,dbo.GetCurrentlyAttendingDefaultStatusIdForCampusGroup(campGrpsWithStuEnrollments.CampGrpId) AS StatusCodeId
                              ,campGrpsWithStuEnrollments.CampusId
                        FROM   (
                               SELECT     arPV.CampGrpId
                                         ,arSE.StuEnrollId
                                         ,arSE.CampusId
                               FROM       dbo.arPrgVersions arPV
                               INNER JOIN dbo.arStuEnrollments arSE ON arSE.PrgVerId = arPV.PrgVerId
                               INNER JOIN (
                                          SELECT DISTINCT StuEnrollId
                                          FROM
                                                 OPENXML(@hDoc, '/NewDataSet/atClsSectAttendance', 1)
                                                     WITH (
                                                          StuEnrollId VARCHAR(50)
                                                          )
                                          ) AS distinctStuEnrollments ON distinctStuEnrollments.StuEnrollId = arSE.StuEnrollId
                               WHERE      arSE.StatusCodeId IN (
                                                               SELECT DISTINCT StatusCodeId
                                                               FROM   dbo.syStatusCodes
                                                               WHERE  SysStatusId = 7
                                                               )
                               ) AS campGrpsWithStuEnrollments;

            UPDATE     b
            SET        StatusCodeId = cgs.StatusCodeId
                      ,ModUser = @ModUser
            FROM       dbo.arStuEnrollments b
            INNER JOIN dbo.atClsSectAttendance a ON a.StuEnrollId = b.StuEnrollId
            INNER JOIN @CampusGroupStatuses cgs ON cgs.StuEnrollId = b.StuEnrollId
            WHERE      a.Actual <> 9999.00
                       AND a.Actual <> 999.00;

            INSERT INTO dbo.syStudentStatusChanges
                        SELECT NEWID()
                              ,cgs.StuEnrollId
                              ,(
                               SELECT   TOP 1 NewStatusId
                               FROM     syStudentStatusChanges
                               WHERE    StuEnrollId = cgs.StuEnrollId
                               ORDER BY DateOfChange DESC
                               )
                              ,cgs.StatusCodeId
                              ,cgs.CampusId
                              ,GETDATE()
                              ,@ModUser
                              ,0
                              ,NULL
                              ,GETDATE()
                              ,NULL
                              ,NULL
                              ,NULL
                              ,NULL
                              ,NULL
                        FROM   @CampusGroupStatuses cgs
                        WHERE  cgs.StatusCodeId NOT IN (
                                                       SELECT NewStatusId
                                                       FROM   dbo.syStudentStatusChanges
                                                       WHERE  StuEnrollId = cgs.StuEnrollId
                                                       );
            ------ END AD-5116: Enter Class Section Attendance to use default Currently Attending Status  ------                                                

            IF EXISTS (
                      SELECT *
                      FROM   sysobjects
                      WHERE  type = 'TR'
                             AND name = 'TR_InsertAttendanceSummary_ByClass_PA'
                      )
                BEGIN
                    ENABLE TRIGGER TR_InsertAttendanceSummary_ByClass_PA ON dbo.atClsSectAttendance;
                END;


            IF EXISTS (
                      SELECT *
                      FROM   sysobjects
                      WHERE  type = 'TR'
                             AND name = 'TR_InsertAttendanceSummary_ByClass_Minutes'
                      )
                BEGIN
                    ENABLE TRIGGER TR_InsertAttendanceSummary_ByClass_Minutes ON dbo.atClsSectAttendance;
                END;

            IF EXISTS (
                      SELECT *
                      FROM   sysobjects
                      WHERE  type = 'TR'
                             AND name = 'TR_InsertClsSectionAttendance_Class_ClockHour'
                      )
                BEGIN
                    ENABLE TRIGGER TR_InsertClsSectionAttendance_Class_ClockHour ON dbo.atClsSectAttendance;
                END;


            EXEC sp_xml_removedocument @hDoc;

            COMMIT TRAN;
            RETURN '';

        END TRY
        BEGIN CATCH
            ROLLBACK TRAN;
            RETURN ERROR_MESSAGE();
        END CATCH;
    END;
--=================================================================================================
-- END  --  USP_PostClsSectAttendance_Insert
--=================================================================================================
GO
