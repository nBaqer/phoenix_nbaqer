SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_DependencyTypes_GetList] @Descrip VARCHAR(50)
AS
    SELECT  DependencyTypeId
    FROM    adDependencyTypes
    WHERE   Descrip LIKE @Descrip + '%';



GO
