SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[USP_IsProgramClockHourType]
    (
     @ProgID UNIQUEIDENTIFIER
    )
AS /*----------------------------------------------------------------------------------------------------
	Author          :	Saraswathi Lakshmanan
    
    Create date		:	06/07/2010
    
	Procedure Name	:	[USP_IsProgramClockHourType]

	Objective		:	Find if the program is a clock hour program type
	
	Parameters		:	Name			Type	Data Type			Required? 	
						=====			====	=========			=========	
						@ProgID			In		UniqueIdentifier	Required
	
	Output			: Returns if the program is a clock hour program type 
						
*/-----------------------------------------------------------------------------------------------------

    BEGIN


        SELECT  *
        FROM    arprograms
               ,syAcademicCalendars
        WHERE   arPrograms.ProgId = @ProgID
                AND arPrograms.ACId = syAcademicCalendars.ACId
                AND ACDescrip LIKE 'Clock%';
	
	
    END;




GO
