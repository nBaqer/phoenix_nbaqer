SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[USP_AR_IsStdRegFrClsSect]
    (
     @StuEnrollId UNIQUEIDENTIFIER
    ,@ClsSectID UNIQUEIDENTIFIER
    )
AS /*----------------------------------------------------------------------------------------------------
	Author          :	Saraswathi Lakshmanan
    
    Create date		:	02/03/2010
    
	Procedure Name	:	USP_AR_IsStdRegFrClsSect

	Objective		:	find if the Student has been registered for any course
	
	Parameters		:	Name			Type	Data Type			Required? 	
						=====			====	=========			=========	
						@StuEnrollid	In		UniqueIdentifier	Required
						@ClsSect		In		UniqueIdentifier	Required
	
	Output			:	Returns the rowcount					
						
*/-----------------------------------------------------------------------------------------------------

    BEGIN
	
        SELECT  COUNT(*) AS Count
        FROM    arResults a
        WHERE   a.StuEnrollId = @StuEnrollId
                AND a.TestId = @ClsSectID;
    END;



GO
