SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:        FAME Inc.
-- Create date: 12/4/2018
-- Description:    Returns true or false whether a students status should be set to currently attending status.
-- Student should be set to currently attending if student has posted grade or actual posted attendance. Or current date >= studentStartDate
-- =============================================
CREATE PROCEDURE [dbo].[USP_ShouldStudentBeCurrentlyAttending]
    (
        @EnrollmentId VARCHAR(1000) = NULL
       ,@ShouldStudentBeCurrentlyAttending AS BIT OUTPUT
    )
AS
    BEGIN
        SET @ShouldStudentBeCurrentlyAttending = 0;

        DECLARE @executingTime DATETIME2 = GETDATE();

		IF EXISTS (
                  SELECT TOP 1 StuEnrollId
                  FROM   dbo.arStudentLOAs
                  WHERE  StuEnrollId = @EnrollmentId
                         AND @executingTime >= StartDate
                         AND @executingTime <= EndDate
                  )
            BEGIN
                SET @ShouldStudentBeCurrentlyAttending = 0;
                RETURN;
            END;

        --student is already currently attending, does not need to be set
        IF (
           SELECT TOP 1 StatusCodeId
           FROM   dbo.arStuEnrollments
           WHERE  StuEnrollId = @EnrollmentId
           ) = (
               SELECT TOP 1 StatusCodeId = dbo.GetCurrentlyAttendingDefaultStatusIdForCampusGroup(pv.CampGrpId)
               FROM   dbo.arStuEnrollments b
               JOIN   dbo.arPrgVersions pv ON b.PrgVerId = pv.PrgVerId
               WHERE  b.StuEnrollId = @EnrollmentId
               )
            BEGIN
                SET @ShouldStudentBeCurrentlyAttending = 0;
                RETURN;
            END;

        -- student has posted attendance
        IF EXISTS (
                  SELECT TOP 1 ActualHours
                  FROM   dbo.arStudentClockAttendance
                  WHERE  StuEnrollId = @EnrollmentId
                         AND ActualHours > 0
                         AND ActualHours <> 9999.00
                  )
            BEGIN
                SET @ShouldStudentBeCurrentlyAttending = 1;
                RETURN;
            END;

        -- student has posted grades
        IF EXISTS (
                  SELECT TOP 1 GrdBkResultId
                  FROM   dbo.arGrdBkResults
                  WHERE  PostDate IS NOT NULL
                         AND Score IS NOT NULL
                         AND StuEnrollId = @EnrollmentId
                  )
            BEGIN
                SET @ShouldStudentBeCurrentlyAttending = 1;
                RETURN;
            END;

        --todays date is >= student startdate
        IF EXISTS (
                  SELECT TOP 1 StuEnrollId
                  FROM   dbo.arStuEnrollments
                  WHERE  StuEnrollId = @EnrollmentId
                         AND @executingTime >= StartDate
                  )
            BEGIN
                SET @ShouldStudentBeCurrentlyAttending = 1;
                RETURN;
            END;


        
    END;
GO
