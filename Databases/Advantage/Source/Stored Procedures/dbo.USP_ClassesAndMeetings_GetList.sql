SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_ClassesAndMeetings_GetList]
    @StartDate DATETIME
   ,@EndDate DATETIME
   ,@CampusId VARCHAR(50)
   ,@InstructorId VARCHAR(50) = NULL
   ,@CourseId VARCHAR(50) = NULL
   ,@TermId VARCHAR(50) = NULL
AS -- Classes should have started before the class end date and ended after class start date
    SELECT  b.ClsSectionId
           ,b.StartDate
           ,b.EndDate
           ,b.TermId
           ,b.ReqId
           ,b.InstructorId
           ,b.ClsSection
           ,c.Code
           ,c.Descrip
           ,d.TermDescrip
           ,(
              SELECT    FullName
              FROM      syUsers
              WHERE     UserId = b.InstructorId
            ) AS FullName
           ,tblDerivedMeetings.Schedule
           ,tblDerivedMeetings.InstructionTypeDescrip
           ,tblDerivedMeetings.ClsSectMeetingId
           ,tblDerivedMeetings.InstructionTypeId
    FROM    arClassSections b
    INNER JOIN arReqs c ON b.ReqId = c.ReqId
    INNER JOIN arTerm d ON b.TermId = d.TermId
    INNER JOIN syStatuses e ON d.StatusId = e.StatusId
    LEFT OUTER JOIN (
                      SELECT DISTINCT
                                t1.PeriodDescrip AS Schedule
                               ,t2.ClsSectMeetingId
                               ,(
                                  SELECT    RIGHT(CONVERT(VARCHAR,TimeIntervalDescrip,100),7)
                                  FROM      cmTimeInterval
                                  WHERE     TimeIntervalId = t1.StartTimeId
                                ) AS StartTime
                               ,(
                                  SELECT    RIGHT(CONVERT(VARCHAR,TimeIntervalDescrip,100),7)
                                  FROM      cmTimeInterval
                                  WHERE     TimeIntervalId = t1.EndTimeId
                                ) AS EndTime
                               ,t3.InstructionTypeDescrip
                               ,ClsSectionId
                               ,t3.InstructionTypeId
                      FROM      syPeriods t1
                      INNER JOIN arClsSectMeetings t2 ON t1.PeriodId = t2.PeriodId
                      INNER JOIN arInstructionType t3 ON t2.InstructionTypeId = t3.InstructionTypeId
                    ) tblDerivedMeetings ON b.ClsSectionId = tblDerivedMeetings.ClsSectionId
    WHERE   b.EndDate >= @StartDate
            AND b.StartDate <= @EndDate
            AND e.Status = 'Active'
            AND b.CampusId = @CampusId
            AND (
                  @InstructorId IS NULL
                  OR b.InstructorId = @InstructorId
                )
            AND (
                  @CourseId IS NULL
                  OR c.ReqId = @CourseId
                )
            AND (
                  @TermId IS NULL
                  OR d.TermId = @TermId
                )
            AND tblDerivedMeetings.ClsSectMeetingId NOT IN ( SELECT ClsSectMeetingId
                                                             FROM   arClsSectMeetings
                                                             WHERE  IsMeetingRescheduled = 1 )
    ORDER BY c.Descrip
           ,c.Code;



GO
