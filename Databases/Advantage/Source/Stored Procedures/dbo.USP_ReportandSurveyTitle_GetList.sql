SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_ReportandSurveyTitle_GetList] @ResourceId INT
AS
    SELECT  SurveyTitle
           ,ReportTitle
    FROM    syIPEDS_Resources_ReportTitle
    WHERE   ResourceId = @ResourceId;   



GO
