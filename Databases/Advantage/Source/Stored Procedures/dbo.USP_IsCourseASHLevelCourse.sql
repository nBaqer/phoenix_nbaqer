SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_IsCourseASHLevelCourse]
    @ReqId UNIQUEIDENTIFIER
   ,@coursecode VARCHAR(50)
AS
    SELECT  COUNT(*) AS RowCounter
    FROM    arReqs
    WHERE   ReqId = @ReqId
            AND ReqId IN ( SELECT DISTINCT
                                    ReqId
                           FROM     arBridge_GradeComponentTypes_Courses );



GO
