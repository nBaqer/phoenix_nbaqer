SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Kimberly Befera
-- Create date: 11/09/2018
-- Description:	Deletes Classroom Work/Services from arGrdBkResults
-- =============================================
CREATE PROCEDURE [dbo].[USP_Delete_ClassroomWorkServices]
    @GrdBkResultId UNIQUEIDENTIFIER
AS
    BEGIN

        SET NOCOUNT ON;

        DELETE FROM dbo.arGrdBkResults
        WHERE GrdBkResultId = @GrdBkResultId;

    END;
GO
