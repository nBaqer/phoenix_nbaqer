SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
    
  
  
  
CREATE PROCEDURE [dbo].[usp_GetGeneralInfoForMultipleStudentsWithNoLeadGrps]
    (
     @campGrpId VARCHAR(8000)
    ,@campusId UNIQUEIDENTIFIER
    ,@prgVerId VARCHAR(8000)
    ,@statusCodeId VARCHAR(8000) = NULL
    ,@cutOffDate DATETIME  
    )
AS
    SET NOCOUNT ON;  
  
    SELECT  StuEnrollId
           ,StartDate
           ,LDA
           ,ExpGradDate
           ,(
              SELECT    SUM(ST.TransAmount)
              FROM      saTransactions ST
                       ,saTransCodes SC
              WHERE     ST.StuEnrollId = SE.StuEnrollId
                        AND ST.TranscodeId = SC.TransCodeId
                        AND ST.Voided = 0
                        AND SC.IsInstCharge = 1
                        AND TransDate <= @cutOffDate
            ) AS TotalCost
           ,(
              SELECT    SUM(TransAmount)
              FROM      saTransactions
              WHERE     StuEnrollId = SE.StuEnrollId
                        AND Voided = 0
                        AND TransDate <= @cutOffDate
            ) AS CurrentBalance
           ,(
              SELECT    SUM(Actual)
              FROM      atConversionAttendance
              WHERE     StuEnrollId = SE.StuEnrollId
                        AND Actual > 0.0
                        AND MeetDate <= @cutOffDate
            ) AS TotalDaysAttended
           ,(
              SELECT    SUM(Schedule)
              FROM      atConversionAttendance
              WHERE     StuEnrollId = SE.StuEnrollId
                        AND Schedule > 0.0
                        AND MeetDate <= @cutOffDate
            ) AS ScheduledDays
           ,(
              SELECT    SUM(Schedule - Actual)
              FROM      atConversionAttendance
              WHERE     StuEnrollId = SE.StuEnrollId
                        AND Schedule > 0
                        AND MeetDate <= @cutOffDate
            ) AS DaysAbsent
           ,(
              SELECT    SUM(Actual)
              FROM      atConversionAttendance
              WHERE     StuEnrollId = SE.StuEnrollId
                        AND Actual > 0
                        AND Schedule = 0
                        AND MeetDate <= @cutOffDate
            ) AS MakeupDays
           ,(
              SELECT    SUM(PSD.Total)
              FROM      arStudentSchedules SS
                       ,arProgScheduleDetails PSD
              WHERE     StuEnrollId = SE.StuEnrollId
                        AND SS.ScheduleId = PSD.ScheduleId
            ) AS WeeklySchedHours
           ,PV.UnitTypeId
           ,(
              SELECT    UnitTypeDescrip
              FROM      arAttUnitType
              WHERE     arAttUnitType.UnitTypeId = PV.UnitTypeId
            ) AS UnitTypeDescrip
           ,PV.TrackTardies
           ,PV.TardiesMakingAbsence
           ,arStudent.FirstName
           ,arStudent.LastName
           ,arStudent.MiddleName
           ,SE.TransferHours
           ,(
              SELECT    ACId
              FROM      arPrograms
              WHERE     arPrograms.ProgId = PV.ProgId
            ) AS ACID
    FROM    arStuEnrollments SE
           ,arStudent
           ,arPrgVersions PV
    WHERE   SE.StudentId = arStudent.StudentId
            AND SE.PrgVerId = PV.PrgVerId
            AND SE.CampusId = @campusId
            AND EXISTS ( SELECT DISTINCT
                                arStuEnrollments.StuEnrollId
                         FROM   arStuEnrollments
                               ,arStudent A
                               ,syCampuses C
                               ,syCmpGrpCmps
                               ,syCampGrps
                         WHERE  arStuEnrollments.StudentId = A.StudentId
                                AND arStuEnrollments.CampusId = C.CampusId
                                AND arStuEnrollments.StuEnrollId = SE.StuEnrollId
                                AND syCmpGrpCmps.CampusId = arStuEnrollments.CampusId
                                AND syCampGrps.CampGrpId = syCmpGrpCmps.CampGrpId
                                AND syCampGrps.campgrpid IN ( SELECT DISTINCT
                                                                        t1.campgrpid
                                                              FROM      sycmpgrpcmps t1
                                                              WHERE     t1.campgrpid IN ( SELECT    strval
                                                                                          FROM      dbo.SPLIT(@campGrpId) ) )
                                AND arStuEnrollments.prgverid IN ( SELECT   strval
                                                                   FROM     dbo.SPLIT(@prgVerId) )
                                AND (
                                      @statusCodeId IS NULL
                                      OR arStuEnrollments.statuscodeid IN ( SELECT  strval
                                                                            FROM    dbo.SPLIT(@statusCodeId) )
                                    ) );  
                                   
  
--exec usp_GetGeneralInfoForMultipleStudentsWithNoLeadGrps '4d582fa8-b29b-4cd7-8024-adec7ca8a284',  
-- '58b5af64-58d5-4712-8b24-c57bcfac5a7f',null,'11/6/2009'  
  



GO
