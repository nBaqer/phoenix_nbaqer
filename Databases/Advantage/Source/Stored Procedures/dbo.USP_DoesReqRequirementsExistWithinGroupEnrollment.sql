SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [dbo].[USP_DoesReqRequirementsExistWithinGroupEnrollment]
    (
     @StuEnrollID UNIQUEIDENTIFIER
    ,@ReqGrpID UNIQUEIDENTIFIER
    )
AS /*----------------------------------------------------------------------------------------------------
	Author          :	Saraswathi Lakshmanan
    
    Create date		:	28/09/2010
    
	Procedure Name	:	[USP_DoesReqRequirementsExistWithinGroupEnrollment]

	Objective		:	Does Req Requitrement Exist Within the group 
		
	Parameters		:	Name			Type	Data Type			Required? 	
						=====			====	=========			=========	
						@StuEnrollId	IN		Uniqueuidentifier	Yes
						@ReqGrpID		IN		UniqueIdentifier	Yes
	
	Output			:	Returns the requested details	
						
*/-----------------------------------------------------------------------------------------------------
    BEGIN
 
 
 --Declare @StuEnrollId as UniqueIdentifier
 --Declare @CampusID as UniqueIdentifier
 
 --Set @StuEnrollId='60DB3379-3DAD-4443-ABC2-F9E211C9AAB8'
 --Set @CampusID='2CD3849A-DECD-48C5-8C9E-3FBE11351437'
        DECLARE @LeadID AS UNIQUEIDENTIFIER;
 
        SET @LeadID = (
                        SELECT  LeadID
                        FROM    arStuEnrollments
                        WHERE   StuEnrollId = @StuEnrollID
                      );
 
 --    'If count is 0 then all the required requirements with in a group are satisfied.
        SELECT  COUNT(*)
        FROM    adReqGrpDef
        WHERE   ReqGrpId = @ReqGrpID
                AND LeadGrpId IN ( SELECT DISTINCT
                                            LeadGrpId
                                   FROM     adLeadByLeadGroups
                                   WHERE    LeadID = @LeadID )
                AND IsRequired = 1
                AND (
                      -- 'Test should have been failed
                      adReqId IN ( SELECT DISTINCT
                                            EntrTestId
                                   FROM     adLeadEntranceTest
                                   WHERE    LeadID = @LeadID
                                            AND Pass = 0
                                            AND EntrTestId NOT IN ( SELECT DISTINCT
                                                                            EntrTestId
                                                                    FROM    adEntrTestOverride
                                                                    WHERE   LeadID = @LeadID
                                                                            AND override = 1 ) )
                      OR  
                --'or document should be not approved
                      adReqId IN ( SELECT DISTINCT
                                            DocumentId
                                   FROM     dbo.adLeadDocsReceived t1
                                           ,syDocStatuses t2
                                   WHERE    t1.LeadID = @LeadID
                                            AND t1.DocStatusId = t2.DocStatusId
                                            AND t2.SysDocStatusId <> 1
                                            AND DocumentId NOT IN ( SELECT  EntrTestId
                                                                    FROM    adEntrTestOverride
                                                                    WHERE   LeadID = @LeadID
                                                                            AND override = 1 ) )
                                        --Commented by Saraswathi lakshmanan on March 15 2011
               --       OR 
               ---- ' or requirement should not be overriden
               --       adReqId IN ( SELECT DISTINCT
               --                             EntrTestId
               --                    FROM     adEntrTestOverride
               --                    WHERE    LeadID = @LeadID
               --                             AND override = 0 )
                    ); 
    END;






GO
