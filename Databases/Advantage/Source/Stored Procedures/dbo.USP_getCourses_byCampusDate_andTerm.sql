SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--=================================================================================================
-- USP_getCourses_byCampusDate_andTerm
--=================================================================================================
CREATE PROCEDURE [dbo].[USP_getCourses_byCampusDate_andTerm]
    @Campus UNIQUEIDENTIFIER
   ,@WeekEndDate DATE
   ,@TermId VARCHAR(8000) = NULL
   ,@ShowInactive BIT
AS
    BEGIN    
        IF LEN(@TermId) = 0
            BEGIN
             
                SET @TermId = NULL;
            END;
        
        DECLARE @ClassesThatFallWithinDateRange TABLE
            (
             ReqId UNIQUEIDENTIFIER
            ,CampusId UNIQUEIDENTIFIER
            ,TermId UNIQUEIDENTIFIER
            );    
    
        INSERT  INTO @ClassesThatFallWithinDateRange
                SELECT DISTINCT
                        ReqId
                       ,CampusId
                       ,TermId
                FROM    dbo.arClassSections ARC
                WHERE   @WeekEndDate BETWEEN ARC.StartDate AND ARC.EndDate
                        AND ARC.CampusId = @Campus;     
    
        SELECT DISTINCT
                R.Descrip
               ,'(' + R.Code + ')' + ' ' + R.Descrip DescripCode
               ,R.ReqId
        FROM    arReqs R
        INNER JOIN @ClassesThatFallWithinDateRange CDR ON R.ReqId = CDR.ReqId
        WHERE   (
                  @TermId IS NULL
                  OR CDR.TermId IN ( SELECT strval
                                     FROM   dbo.SPLIT(@TermId) )
                )
                AND (
                      (
                        @ShowInactive = 0
                        AND R.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
                      )
                      OR @ShowInactive = 1
                    )
        ORDER BY R.Descrip;     
    END; 
--=================================================================================================
-- END  --  USP_getCourses_byCampusDate_andTerm
--=================================================================================================
GO
