SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_AdvPortalCampuses]
AS
    BEGIN

        DECLARE @PortalCampuses TABLE
            (
             SerialNum INT
            ,CampusId VARCHAR(255)
            );

        INSERT  INTO @PortalCampuses
                SELECT	DISTINCT
                        ROW_NUMBER() OVER ( ORDER BY CampusId )
                       ,AppSettingValue.CampusId
                FROM    syConfigAppSettings Appsetting
                INNER JOIN dbo.syConfigAppSetValues AppSettingValue ON Appsetting.SettingId = AppSettingValue.SettingId
                WHERE   KeyName = 'PortalCustomer'
                        AND AppSettingValue.Value = 'Yes';

        DECLARE @PortalCampusDescrip TABLE
            (
             CampusId VARCHAR(255)
            ,CampusDescrip VARCHAR(255)
            );

        DECLARE @i INT;
        SET @i = 1;

        WHILE ( @i <= (
                        SELECT  MAX(SerialNum)
                        FROM    @PortalCampuses
                      ) )
            BEGIN

                IF (
                     SELECT CampusId
                     FROM   @PortalCampuses
                     WHERE  SerialNum = @i
                   ) IS NULL
                    BEGIN

                        INSERT  INTO @PortalCampusDescrip
                                SELECT DISTINCT
                                        CampusId
                                       ,CampDescrip
                                FROM    dbo.syCampuses Campus
                                INNER JOIN dbo.syStatuses Statuses ON Campus.StatusId = Statuses.StatusId
                                WHERE   Statuses.Status = 'Active'
                                        AND Campus.LeadStatusId IS NOT NULL
                                        AND Campus.AdmRepId IS NOT NULL;
                        SET @i = @i + 1;
                    END; 
                ELSE
                    IF (
                         SELECT CampusId
                         FROM   @PortalCampuses
                         WHERE  SerialNum = @i
                       ) IS NOT NULL
                        BEGIN

                            INSERT  INTO @PortalCampusDescrip
                                    SELECT DISTINCT
                                            PC.CampusId
                                           ,Camp.CampDescrip
                                    FROM    @PortalCampuses PC
                                    INNER JOIN dbo.syCampuses Camp ON PC.CampusId = Camp.CampusId
                                    INNER JOIN dbo.syStatuses Statuses ON Camp.StatusId = Statuses.StatusId
                                    WHERE   Statuses.Status = 'Active'
                                            AND Camp.LeadStatusId IS NOT NULL
                                            AND Camp.AdmRepId IS NOT NULL;
                            SET @i = @i + 1;
                        END; 
            END;

        SELECT DISTINCT
                *
        FROM    @PortalCampusDescrip
        ORDER BY CampusDescrip;

    END;



GO
