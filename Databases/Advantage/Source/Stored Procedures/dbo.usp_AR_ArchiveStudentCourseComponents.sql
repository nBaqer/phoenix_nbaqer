SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_AR_ArchiveStudentCourseComponents]
    @ClsSectionId UNIQUEIDENTIFIER
   ,@StuEnrollId UNIQUEIDENTIFIER
   ,@User NVARCHAR(50)
AS
    BEGIN
        INSERT  INTO _archive_arGrdBkResults
                (
                 Archive_User
                ,GrdBkResultId
                ,ClsSectionId
                ,InstrGrdBkWgtDetailId
                ,Score
                ,Comments
                ,StuEnrollId
                ,ModUser
                ,ModDate
                ,ResNum
                ,PostDate
                ,IsCompGraded
                ,IsCourseCredited
                )
                SELECT  @User
                       ,GrdBkResultId
                       ,ClsSectionId
                       ,InstrGrdBkWgtDetailId
                       ,GR.Score
                       ,Comments
                       ,GR.StuEnrollId
                       ,GR.ModUser
                       ,GR.ModDate
                       ,ResNum
                       ,PostDate
                       ,IsCompGraded
                       ,IsCourseCredited
                FROM    arGrdBkResults GR
                INNER JOIN arResults R ON GR.StuEnrollId = R.StuEnrollId
                WHERE   GR.StuEnrollId = @StuEnrollId
                        AND R.TestId = @ClsSectionId;

        DELETE  arGrdBkResults
        WHERE   StuEnrollId = @StuEnrollId
                AND ClsSectionId IN ( SELECT DISTINCT
                                                TestId
                                      FROM      dbo.arResults
                                      WHERE     TestId = @ClsSectionId );
    END;



GO
