SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
------------------------------------------------------------------------------------------------------------------------------
--Balaji  DE7532 04/30/2012 End
------------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------------
--Balaji  DE7563 05/02/2012 Start
------------------------------------------------------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[USP_PageLevelPermissions_Insert] @RuleValues NTEXT
AS
    DECLARE @hDoc INT;
		--Prepare input values as an XML document
    EXEC sp_xml_preparedocument @hDoc OUTPUT,@RuleValues;

		 --Delete all records from bridge table based on GrdComponentTypeId
    DELETE  FROM syRlsResLvls
    WHERE   RoleId IN ( SELECT  RoleId
                        FROM    OPENXML(@hDoc,'/NewDataSet/SetupPageLevelPermissions',1) 
							WITH (RoleId VARCHAR(50)) )
            AND ResourceID IN ( SELECT  ResourceId
                                FROM    OPENXML(@hDoc,'/NewDataSet/SetupPageLevelPermissions',1) 
							WITH (ResourceId INT) );
							 
		-- Insert records into Bridge Table 
    INSERT  INTO syRlsResLvls
            SELECT DISTINCT
                    RRLID
                   ,RoleId
                   ,ResourceId
                   ,AccessLevel
                   ,GETDATE()
                   ,ModUser
                   ,NULL AS ParentId --,convert(char(10),ModDate,101) 
            FROM    OPENXML(@hDoc,'/NewDataSet/SetupPageLevelPermissions',1) 
					WITH (RRLID UNIQUEIDENTIFIER,RoleId UNIQUEIDENTIFIER,ResourceId INT,
					AccessLevel INT,ModDate DATETIME,ModUser VARCHAR(50),ParentId INT);
		
	  
    EXEC sp_xml_removedocument @hDoc;



GO
