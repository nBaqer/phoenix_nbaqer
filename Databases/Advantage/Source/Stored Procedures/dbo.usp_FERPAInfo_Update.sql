SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_FERPAInfo_Update]
    (
     @FERPACategoryID UNIQUEIDENTIFIER
    ,@FERPACategoryCode VARCHAR(12)
    ,@StatusId UNIQUEIDENTIFIER
    ,@FERPACategoryDescrip VARCHAR(50)
    ,@CampGrpId UNIQUEIDENTIFIER
    ,@ModUser VARCHAR(50)
    ,@moddate DATETIME
    )
AS
    SET NOCOUNT ON;
    UPDATE  arFERPACategory
    SET     FERPACategoryCode = @FERPACategoryCode
           ,StatusId = @StatusId
           ,FERPACategoryDescrip = @FERPACategoryDescrip
           ,CampGrpId = @CampGrpId
           ,ModUser = @ModUser
           ,moddate = @moddate
    WHERE   FERPACategoryID = @FERPACategoryID;



GO
