SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_IPEDS_Spring_MilitaryBenefits_DetailAndSummary]
    @CampusId VARCHAR(50)
   ,@ProgId VARCHAR(4000) = NULL
   ,@CohortYear VARCHAR(10) = NULL
   ,@OrderBy VARCHAR(100)
   ,@StartDate DATETIME
   ,@EndDate DATETIME
   ,@SchoolType VARCHAR(50)
   ,@EndDate_Academic DATETIME = NULL
AS
    DECLARE @ReturnValue VARCHAR(100)
       ,@Value VARCHAR(100);
    DECLARE @SSN VARCHAR(10)
       ,@FirstName VARCHAR(100)
       ,@LastName VARCHAR(100)
       ,@StudentNumber VARCHAR(50)
       ,@TransferredOut INT
       ,@Exclusions INT;
    DECLARE @CitizenShip_IPEDSValue INT
       ,@ProgramType_IPEDSValue INT
       ,@Gender_IPEDSValue INT
       ,@FullTimePartTime_IPEDSValue INT
       ,@StudentId UNIQUEIDENTIFIER; 
    DECLARE @AcademicEndDate DATETIME
       ,@AcademicStartDate DATETIME; 
    DECLARE @AcadInstFirstTimeStartDate DATETIME;
-- Check if School tracks grades by letter or numeric 
    SET @Value = (
                   SELECT TOP 1
                            Value
                   FROM     dbo.syConfigAppSetValues
                   WHERE    SettingId = 47
                 );

--Here, we're determining if the school type is academic. If so, we will change the new var
--@AcademicEndDate to the @EndDate_Academic, if not, than use @EndDate. We will use the new
--@AcademicEndDate variable to help select the student list.  DD Rev 01/10/12

    DECLARE @DeptOfDefenseStartDate VARCHAR(50)
       ,@DeptofDefenseEndDate VARCHAR(50);
	
    IF LOWER(@SchoolType) = 'academic'
        BEGIN
            SET @AcademicEndDate = @EndDate_Academic;
            SET @AcademicStartDate = @EndDate_Academic;
			 --12/05/2012 DE8824 - Academic year options should have a cutoff start date
            SET @AcadInstFirstTimeStartDate = DATEADD(YEAR,-1,@EndDate_Academic);

            SET @DeptOfDefenseStartDate = '10/1/' + CONVERT(VARCHAR,YEAR(@AcademicStartDate));
            SET @DeptofDefenseEndDate = '9/30/' + CONVERT(VARCHAR,YEAR(@AcademicEndDate));

        END;
    ELSE
        BEGIN
            SET @AcademicEndDate = @EndDate;
            SET @AcademicStartDate = @StartDate;
            SET @DeptOfDefenseStartDate = '10/1/' + CONVERT(VARCHAR,YEAR(@AcademicStartDate));
            SET @DeptofDefenseEndDate = '9/30/' + CONVERT(VARCHAR,YEAR(@AcademicEndDate));
        END;


    IF @ProgId IS NOT NULL
        BEGIN
	
            SELECT  @ReturnValue = COALESCE(@ReturnValue,'') + ProgDescrip + ','
            FROM    arPrograms t1
            WHERE   t1.ProgId IN ( SELECT   Val
                                   FROM     MultipleValuesForReportParameters(@ProgId,',',1) );
            SET @ReturnValue = SUBSTRING(@ReturnValue,1,LEN(@ReturnValue) - 1);
	
        END;
    ELSE
        BEGIN
            SELECT  @ReturnValue = COALESCE(@ReturnValue,'') + ProgDescrip + ','
            FROM    arPrograms t1
            WHERE   t1.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
                    AND t1.CampGrpId IN ( SELECT    CampGrpId
                                          FROM      syCmpGrpCmps
                                          WHERE     CampusId = @CampusId );

            SET @ReturnValue = SUBSTRING(@ReturnValue,1,LEN(@ReturnValue) - 1);
        END;
 
/*********Get the list of students that will be shown in the report - Starts Here ******************/			
    CREATE TABLE #StudentsList
        (
         StudentId UNIQUEIDENTIFIER
        ,SSN VARCHAR(10)
        ,FirstName VARCHAR(100)
        ,LastName VARCHAR(100)
        ,MiddleName VARCHAR(50)
        ,StudentNumber VARCHAR(50)
        ,TransferredOut INT
        ,Exclusions INT
        ,CitizenShip_IPEDSValue INT
        ,ProgramType_IPEDSValue INT
        ,Gender_IPEDSValue INT
        ,FullTimePartTime_IPEDSValue INT
        ,GenderId UNIQUEIDENTIFIER
        ,RaceId UNIQUEIDENTIFIER
        ,CitizenId UNIQUEIDENTIFIER
        ,GenderDescription VARCHAR(50)
        ,RaceDescription VARCHAR(50)
        ,StudentGraduatedStatusCount INT
        ,ClockHourProgramCount INT
        );
		
		
		
    IF LOWER(@SchoolType) = 'program'
        BEGIN
            INSERT  INTO #StudentsList
                    SELECT DISTINCT
                            t1.StudentId
                           ,t1.SSN
                           ,t1.FirstName
                           ,t1.LastName
                           ,t1.MiddleName
                           ,t1.StudentNumber
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SQ1
                                       ,syStatusCodes SQ2
                                       ,dbo.sySysStatus SQ3
                              WHERE     SQ1.StatusCodeId = SQ2.StatusCodeId
                                        AND SQ2.SysStatusId = SQ3.SysStatusId
                                        AND SQ1.StudentId = t1.StudentId
                                        AND SQ3.SysStatusId = 19
                                        AND SQ1.TransferDate <= @AcademicEndDate
                            ) AS TransferredOut
                           ,(
						-- Check if the student was either dropped and if the drop reason is either
						-- deceased, active duty, foreign aid service, church mission
                              CASE WHEN (
                                          SELECT    COUNT(*)
                                          FROM      arStuEnrollments SQ1
                                                   ,syStatusCodes SQ2
                                                   ,dbo.sySysStatus SQ3
                                                   ,arDropReasons SQ44
                                          WHERE     SQ1.StatusCodeId = SQ2.StatusCodeId
                                                    AND SQ2.SysStatusId = SQ3.SysStatusId
                                                    AND SQ1.DropReasonId = SQ44.DropReasonId
                                                    AND SQ1.StudentId = t1.StudentId
                                                    AND SQ3.SysStatusId IN ( 12 ) -- Dropped
                                                    AND SQ1.DateDetermined <= @AcademicEndDate
                                                    AND SQ44.IPEDSValue IN ( 15,16,17,18,19 )
                                        ) >= 1 THEN 1
                                   ELSE 0
                              END ) AS Exclusions
                           ,t11.IPEDSValue AS CitizenShip_IPEDSValue
                           ,t9.IPEDSValue AS ProgramType_IPEDSValue
                           ,t3.IPEDSValue AS Gender_IPEDSValue
                           ,t10.IPEDSValue AS FullTimePartTime_IPEDSValue
                           ,t1.Gender
                           ,t1.Race
                           ,t1.Citizen
                           ,(
                              SELECT DISTINCT
                                        AgencyDescrip
                              FROM      syRptAgencyFldValues
                              WHERE     RptAgencyFldValId = t3.IPEDSValue
                            ) AS GenderDescription
                           ,(
                              SELECT DISTINCT
                                        AgencyDescrip
                              FROM      syRptAgencyFldValues
                              WHERE     RptAgencyFldValId = t4.IPEDSValue
                            ) AS RaceDescription
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SEC
                                       ,syStatusCodes SC
                              WHERE     SEC.StudentId = t1.StudentId
                                        AND SEC.StatusCodeId = SC.StatusCodeId
                                        AND SC.SysStatusId = 14
                            ) AS StudentGraduatedStatusCount
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SEC
                                       ,arPrograms P
                                       ,arPrgVersions PV
                              WHERE     SEC.PrgVerId = PV.PrgVerId
                                        AND P.ProgId = PV.ProgId
                                        AND SEC.StudentId = t1.StudentId
                                        AND P.ACId = 5
                            ) AS ClockHourProgramCount
                    FROM    adGenders t3
                    LEFT JOIN arStudent t1 ON t3.GenderId = t1.Gender
                    LEFT JOIN adEthCodes t4 ON t4.EthCodeId = t1.Race
                    INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                    INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                    INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                                 AND t6.SysStatusId NOT IN ( 8 ) -- Ignore "No Start" Students
                    INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                    INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                    INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                    LEFT JOIN arAttendTypes t10 ON t2.AttendTypeId = t10.AttendTypeId
                    LEFT JOIN dbo.adDegCertSeeking t12 ON t2.DegCertSeekingId = t12.DegCertSeekingId
                    INNER JOIN adCitizenships t11 ON t1.Citizen = t11.CitizenshipId
                    WHERE   LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                            AND (
                                  @ProgId IS NULL
                                  OR t8.ProgId IN ( SELECT  Val
                                                    FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                )
                            AND t10.IPEDSValue = 61
                            AND -- Full Time
                            t12.IPEDSValue = 11
                            AND -- First Time
                            t9.IPEDSValue = 58
                            OR t9.IPEDSValue = 59 -- Under Graduate or Graduate
                            AND (
                                  t2.StartDate >= @AcademicStartDate
                                  AND t2.StartDate <= @AcademicEndDate
                                ) 
							--(t2.StartDate<=@AcademicEndDate) 
                            AND StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
							-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
							( SELECT    t1.StuEnrollId
                              FROM      arStuEnrollments t1
                                       ,syStatusCodes t2
                              WHERE     t1.StatusCodeId = t2.StatusCodeId
                                        AND StartDate <= @AcademicEndDate
                                        AND -- Student started before the end date range
                                        LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                        AND (
                                              @ProgId IS NULL
                                              OR t8.ProgId IN ( SELECT  Val
                                                                FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                            )
                                        AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
								-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                        AND (
                                              t1.DateDetermined < @AcademicStartDate
                                              OR ExpGradDate < @AcademicStartDate
                                              OR LDA < @AcademicStartDate
                                            ) ) 
						   -- If Student is enrolled in only one program version and if that program version 
						   -- happens to be a continuing ed program exclude the student
                            AND t2.StudentId NOT IN ( SELECT DISTINCT
                                                                StudentId
                                                      FROM      (
                                                                  SELECT    StudentId
                                                                           ,COUNT(*) AS RowCounter
                                                                  FROM      arStuEnrollments
                                                                  WHERE     PrgVerId IN ( SELECT    PrgVerId
                                                                                          FROM      arPrgVersions
                                                                                          WHERE     IsContinuingEd = 1 )
                                                                  GROUP BY  StudentId
                                                                  HAVING    COUNT(*) = 1
                                                                ) dtStudent_ContinuingEd );
        END;
    ELSE
        BEGIN
            INSERT  INTO #StudentsList
                    SELECT DISTINCT
                            t1.StudentId
                           ,t1.SSN
                           ,t1.FirstName
                           ,t1.LastName
                           ,t1.MiddleName
                           ,t1.StudentNumber
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SQ1
                                       ,syStatusCodes SQ2
                                       ,dbo.sySysStatus SQ3
                              WHERE     SQ1.StatusCodeId = SQ2.StatusCodeId
                                        AND SQ2.SysStatusId = SQ3.SysStatusId
                                        AND SQ1.StudentId = t1.StudentId
                                        AND SQ3.SysStatusId = 19
                                        AND SQ1.TransferDate <= @AcademicEndDate
                            ) AS TransferredOut
                           ,(
						-- Check if the student was either dropped and if the drop reason is either
						-- deceased, active duty, foreign aid service, church mission
                              CASE WHEN (
                                          SELECT    COUNT(*)
                                          FROM      arStuEnrollments SQ1
                                                   ,syStatusCodes SQ2
                                                   ,dbo.sySysStatus SQ3
                                                   ,arDropReasons SQ44
                                          WHERE     SQ1.StatusCodeId = SQ2.StatusCodeId
                                                    AND SQ2.SysStatusId = SQ3.SysStatusId
                                                    AND SQ1.DropReasonId = SQ44.DropReasonId
                                                    AND SQ1.StudentId = t1.StudentId
                                                    AND SQ3.SysStatusId IN ( 12 ) -- Dropped
                                                    AND SQ1.DateDetermined <= @AcademicEndDate
                                                    AND SQ44.IPEDSValue IN ( 15,16,17,18,19 )
                                        ) >= 1 THEN 1
                                   ELSE 0
                              END ) AS Exclusions
                           ,t11.IPEDSValue AS CitizenShip_IPEDSValue
                           ,t9.IPEDSValue AS ProgramType_IPEDSValue
                           ,t3.IPEDSValue AS Gender_IPEDSValue
                           ,t10.IPEDSValue AS FullTimePartTime_IPEDSValue
                           ,t1.Gender
                           ,t1.Race
                           ,t1.Citizen
                           ,(
                              SELECT DISTINCT
                                        AgencyDescrip
                              FROM      syRptAgencyFldValues
                              WHERE     RptAgencyFldValId = t3.IPEDSValue
                            ) AS GenderDescription
                           ,(
                              SELECT DISTINCT
                                        AgencyDescrip
                              FROM      syRptAgencyFldValues
                              WHERE     RptAgencyFldValId = t4.IPEDSValue
                            ) AS RaceDescription
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SEC
                                       ,syStatusCodes SC
                              WHERE     SEC.StudentId = t1.StudentId
                                        AND SEC.StatusCodeId = SC.StatusCodeId
                                        AND SC.SysStatusId = 14
                            ) AS StudentGraduatedStatusCount
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SEC
                                       ,arPrograms P
                                       ,arPrgVersions PV
                              WHERE     SEC.PrgVerId = PV.PrgVerId
                                        AND P.ProgId = PV.ProgId
                                        AND SEC.StudentId = t1.StudentId
                                        AND P.ACId = 5
                            ) AS ClockHourProgramCount
                    FROM    adGenders t3
                    LEFT JOIN arStudent t1 ON t3.GenderId = t1.Gender
                    LEFT JOIN adEthCodes t4 ON t4.EthCodeId = t1.Race
                    INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                    INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                    INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                                 AND t6.SysStatusId NOT IN ( 8 ) -- Ignore "No Start" Students
                    INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                    INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                    INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                    LEFT JOIN arAttendTypes t10 ON t2.AttendTypeId = t10.AttendTypeId
                    LEFT JOIN dbo.adDegCertSeeking t12 ON t2.DegCertSeekingId = t12.DegCertSeekingId
                    INNER JOIN adCitizenships t11 ON t1.Citizen = t11.CitizenshipId
                    WHERE   LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                            AND (
                                  @ProgId IS NULL
                                  OR t8.ProgId IN ( SELECT  Val
                                                    FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                )
                            AND t10.IPEDSValue = 61
                            AND -- Full Time
                            t12.IPEDSValue = 11
                            AND -- First Time
                            t9.IPEDSValue = 58
                            OR t9.IPEDSValue = 59 -- Under Graduate or Graduate
                            AND 
							--12/05/2012 DE8824 - Academic year options should have a cutoff start date
							--t2.StartDate <= @AcademicEndDate
                            (
                              t2.StartDate > @AcadInstFirstTimeStartDate
                              AND t2.StartDate <= @AcademicEndDate
                            )
                            AND StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
							-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
							( SELECT    t1.StuEnrollId
                              FROM      arStuEnrollments t1
                                       ,syStatusCodes t2
                              WHERE     t1.StatusCodeId = t2.StatusCodeId
                                        AND StartDate <= @AcademicEndDate
                                        AND -- Student started before the end date range
                                        LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                        AND (
                                              @ProgId IS NULL
                                              OR t8.ProgId IN ( SELECT  Val
                                                                FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                            )
                                        AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
								-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                        AND (
                                              t1.DateDetermined < @AcademicStartDate
                                              OR ExpGradDate < @AcademicStartDate
                                              OR LDA < @AcademicStartDate
                                            ) ) 
						   -- If Student is enrolled in only one program version and if that program version 
						   -- happens to be a continuing ed program exclude the student
                            AND t2.StudentId NOT IN ( SELECT DISTINCT
                                                                StudentId
                                                      FROM      (
                                                                  SELECT    StudentId
                                                                           ,COUNT(*) AS RowCounter
                                                                  FROM      arStuEnrollments
                                                                  WHERE     PrgVerId IN ( SELECT    PrgVerId
                                                                                          FROM      arPrgVersions
                                                                                          WHERE     IsContinuingEd = 1 )
                                                                  GROUP BY  StudentId
                                                                  HAVING    COUNT(*) = 1
                                                                ) dtStudent_ContinuingEd );
        END;	


--SELECT * FROM #StudentsList		
		
    CREATE TABLE #StudentsWhoReceivedPost911AndDefenseTuition
        (
         StudentId UNIQUEIDENTIFIER
        ,StudentName VARCHAR(500)
        ,SSN VARCHAR(50)
        ,StudentNumber VARCHAR(50)
        ,PmtDisbRelId UNIQUEIDENTIFIER
        ,Amount DECIMAL(18,2)
        ,AdvFundSourceId INT
        ,IPEDSValue INT
        ,TransDate DATETIME
        );
		
    INSERT  INTO #StudentsWhoReceivedPost911AndDefenseTuition
            SELECT DISTINCT
                    S.StudentId
                   ,S.LastName + ', ' + S.FirstName + ' ' + ISNULL(MiddleName,'') AS StudentName
                   ,dbo.UDF_FormatSSN(S.SSN) AS SSN
                   ,S.StudentNumber
                   ,PDR.PmtDisbRelId
                   ,PDR.Amount
                   ,FS.AdvFundSourceId
                   ,PT.IPEDSValue
                   ,ST.TransDate
            FROM    saPmtDisbRel PDR
            INNER JOIN faStudentAwardSchedule SAS ON PDR.AwardScheduleId = SAS.AwardScheduleId
            INNER JOIN faStudentAwards SA ON SAS.StudentAwardId = SAS.StudentAwardId
            INNER JOIN saTransactions ST ON ST.TransactionId = PDR.TransactionId
            INNER JOIN arStuEnrollments SE ON SE.StuEnrollId = SA.StuEnrollId
                                              AND SE.StuEnrollId = ST.StuEnrollId
            INNER JOIN #StudentsList S ON SE.StudentId = S.StudentId
            INNER JOIN saFundSources FS ON SA.AwardTypeId = FS.FundSourceId
                                           AND FS.FundSourceId = ST.FundSourceId
            INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
            INNER JOIN arProgTypes PT ON PV.ProgTypId = PT.ProgTypId
            INNER JOIN arPrograms t8 ON PV.ProgId = t8.ProgId
            WHERE   FS.AdvFundSourceId IN ( 24,25 ) -- Post 9/11, Dept of Defense
                    AND PT.IPEDSValue IN ( 58,59 ) -- Undergraduate and Graduate Students
                    AND SE.StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
							-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
							( SELECT    t1.StuEnrollId
                              FROM      arStuEnrollments t1
                                       ,syStatusCodes t2
                              WHERE     t1.StatusCodeId = t2.StatusCodeId
                                        AND StartDate <= @AcademicEndDate
                                        AND -- Student started before the end date range
                                        LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                        AND (
                                              @ProgId IS NULL
                                              OR t8.ProgId IN ( SELECT  Val
                                                                FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                            )
                                        AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
								-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                        AND (
                                              t1.DateDetermined < @AcademicStartDate
                                              OR ExpGradDate < @AcademicStartDate
                                              OR LDA < @AcademicStartDate
                                            ) ); 
	 



    CREATE TABLE #FinalOutput
        (
         StudentName VARCHAR(200)
        ,SSN VARCHAR(50)
        ,StudentNumber VARCHAR(50)
        ,Post911_Undergraduate_DisbCount INT
        ,Post911_Undergraduate_DisbAmount DECIMAL(18,2)
        ,Post911_Graduate_DisbCount INT
        ,Post911_Graduate_DisbAmount INT
        ,DeptofDefense_Undergraduate_DisbCount INT
        ,DeptofDefense_Undergraduate_DisbAmount DECIMAL(18,2)
        ,DeptofDefense_Graduate_DisbCount INT
        ,DeptofDefense_Graduate_DisbAmount DECIMAL(18,2)
        );

    INSERT  INTO #FinalOutput
            SELECT 
	DISTINCT        StudentName
                   ,SSN
                   ,StudentNumber
                   ,(
                      SELECT    CASE WHEN COUNT(*) >= 1 THEN 1
                                     ELSE 0
                                END
                      FROM      #StudentsWhoReceivedPost911AndDefenseTuition SQ1
                      WHERE     SQ1.StudentId = PT.StudentId
                                AND AdvFundSourceId = 24 --Post 9/11
                                AND IPEDSValue = 58 -- Undergraduate
                                AND TransDate >= @StartDate
                                AND TransDate <= @EndDate
                    ) AS Post911_Undergraduate_DisbCount
                   ,(
                      SELECT    SUM(Amount)
                      FROM      #StudentsWhoReceivedPost911AndDefenseTuition SQ1
                      WHERE     SQ1.StudentId = PT.StudentId
                                AND AdvFundSourceId = 24 --Post 9/11
                                AND IPEDSValue = 58 -- Undergraduate
                                AND TransDate >= @StartDate
                                AND TransDate <= @EndDate
                    ) AS Post911_Undergraduate_DisbAmount
                   ,(
                      SELECT    CASE WHEN COUNT(*) >= 1 THEN 1
                                     ELSE 0
                                END
                      FROM      #StudentsWhoReceivedPost911AndDefenseTuition SQ1
                      WHERE     SQ1.StudentId = PT.StudentId
                                AND AdvFundSourceId = 24 --Post 9/11
                                AND IPEDSValue = 59 -- Graduate
                                AND TransDate >= @StartDate
                                AND TransDate <= @EndDate
                    ) AS Post911_Graduate_DisbCount
                   ,(
                      SELECT    SUM(Amount)
                      FROM      #StudentsWhoReceivedPost911AndDefenseTuition SQ1
                      WHERE     SQ1.StudentId = PT.StudentId
                                AND AdvFundSourceId = 24 --Post 9/11
                                AND IPEDSValue = 59 -- Graduate
                                AND TransDate >= @StartDate
                                AND TransDate <= @EndDate
                    ) AS Post911_Graduate_DisbAmount
                   ,(
                      SELECT    CASE WHEN COUNT(*) >= 1 THEN 1
                                     ELSE 0
                                END
                      FROM      #StudentsWhoReceivedPost911AndDefenseTuition SQ1
                      WHERE     SQ1.StudentId = PT.StudentId
                                AND AdvFundSourceId = 25 --Dept. of Defense Tuition
                                AND IPEDSValue = 58 -- Undergraduate
                                AND TransDate >= @DeptOfDefenseStartDate
                                AND TransDate <= @DeptofDefenseEndDate
                    ) AS DeptofDefense_Undergraduate_DisbCount
                   ,(
                      SELECT    SUM(Amount)
                      FROM      #StudentsWhoReceivedPost911AndDefenseTuition SQ1
                      WHERE     SQ1.StudentId = PT.StudentId
                                AND AdvFundSourceId = 25 --Dept. of Defense Tuition
                                AND IPEDSValue = 58 -- Undergraduate
                                AND TransDate >= @DeptOfDefenseStartDate
                                AND TransDate <= @DeptofDefenseEndDate
                    ) AS DeptofDefense_Undergraduate_DisbAmount
                   ,(
                      SELECT    CASE WHEN COUNT(*) >= 1 THEN 1
                                     ELSE 0
                                END
                      FROM      #StudentsWhoReceivedPost911AndDefenseTuition SQ1
                      WHERE     SQ1.StudentId = PT.StudentId
                                AND AdvFundSourceId = 25 --Dept. of Defense Tuition
                                AND IPEDSValue = 59 -- Graduate
                                AND TransDate >= @DeptOfDefenseStartDate
                                AND TransDate <= @DeptofDefenseEndDate
                    ) AS DeptofDefense_Graduate_DisbCount
                   ,(
                      SELECT    SUM(Amount)
                      FROM      #StudentsWhoReceivedPost911AndDefenseTuition SQ1
                      WHERE     SQ1.StudentId = PT.StudentId
                                AND AdvFundSourceId = 25 --Dept. of Defense Tuition
                                AND IPEDSValue = 59 -- Graduate
                                AND TransDate >= @DeptOfDefenseStartDate
                                AND TransDate <= @DeptofDefenseEndDate
                    ) AS DeptofDefense_Graduate_DisbAmount
            FROM    #StudentsWhoReceivedPost911AndDefenseTuition PT;

    SELECT  *
    FROM    #FinalOutput
    ORDER BY CASE WHEN @OrderBy = 'SSN' THEN SSN
             END
           ,CASE WHEN @OrderBy = 'LastName' THEN StudentName
            END; 
    DROP TABLE #StudentsWhoReceivedPost911AndDefenseTuition;	
GO
