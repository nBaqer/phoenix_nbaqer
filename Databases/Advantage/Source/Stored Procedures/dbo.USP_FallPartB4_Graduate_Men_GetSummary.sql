SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_FallPartB4_Graduate_Men_GetSummary]
    @CampusId VARCHAR(50)
   ,@ProgId VARCHAR(4000) = NULL
   ,@StartDate DATETIME = NULL
   ,@EndDate DATETIME = NULL
   ,@DateRangeText VARCHAR(100) = NULL
   ,@OrderBy VARCHAR(100)
AS
    DECLARE @AcadInstFirstTimeStartDate DATETIME;
    DECLARE @Value VARCHAR(50);
-- set @Value='letter'
-- 2/07/2013 - updated the @value 
    SET @Value = (
                   SELECT   dbo.GetAppSettingValue(47,@CampusId)
                 );
    DECLARE @ContinuingStartDate DATETIME;
    SET @ContinuingStartDate = @StartDate;
-- For Academic Year Reporter, the End Date should be 10/15/Cohort Year
-- and Start Date should be blank
	-- if day(@EndDate)=15 and month(@EndDate)=10 
    IF SUBSTRING(LOWER(@DateRangeText),1,10) = 'enrollment'
        BEGIN
            SET @EndDate = @StartDate; -- it will always be 10/15/cohort year example: for cohort year 2009, it is 10/15/2009
            SET @StartDate = @StartDate;
					--2/07/2012 - DE9062 - Academic year options should have a cutoff start date For First Time 
            SET @AcadInstFirstTimeStartDate = DATEADD(YEAR,-1,@EndDate);
        END;

    CREATE TABLE #FallPartB4FTUG
        (
         RowNumber UNIQUEIDENTIFIER
        ,SSN VARCHAR(50)
        ,StudentNumber VARCHAR(50)
        ,StudentName VARCHAR(100)
        ,Gender VARCHAR(50)
        ,Race VARCHAR(50)
        ,FirstTime VARCHAR(10)
        ,TransferIn VARCHAR(10)
        ,Continuing VARCHAR(10)
        ,NonDegreeSeeking VARCHAR(10)
        ,AllOther VARCHAR(10)
        ,FirstTimeCount INT
        ,TransferInCount INT
        ,CountinuingCount INT
        ,NonDegreeSeekingCount INT
        ,AllOtherCount INT
        ,GenderSequence INT
        ,RaceSequence INT
        ,Other VARCHAR(10)
        ,OtherCount INT
        );

    INSERT  INTO #FallPartB4FTUG
            SELECT  NEWID() AS RowNumber
                   ,dbo.UDF_FormatSSN(SSN) AS SSN
                   ,StudentNumber
                   ,StudentName
                   ,Gender
                   ,Race
                   ,CASE WHEN FirstTime >= 1 THEN 'X'
                         ELSE ''
                    END AS FirstTime
                   ,CASE WHEN (
                                (
                                  Continuing = 0
                                  OR Continuing IS NULL
                                )
                                AND TransferIn >= 1
                              ) THEN 'X'
                         ELSE ''
                    END AS TransferIn
                   ,CASE WHEN (
                                Continuing >= 1
                                AND (
                                      TransferIn = 0
                                      OR TransferIn IS NULL
                                      OR TransferIn = 1
                                    )
                                AND (
                                      FirstTime = 0
                                      OR FirstTime IS NULL
                                    )
                              ) THEN 'X'
                         ELSE ''
                    END AS Continuing
                   ,CASE WHEN (
                                NonDegreeSeeking >= 1
                                AND (
                                      TransferIn = 0
                                      OR TransferIn IS NULL
                                    )
                                AND (
                                      FirstTime = 0
                                      OR FirstTime IS NULL
                                    )
                              ) THEN 'X'
                         ELSE ''
                    END AS NonDegreeSeeking
                   ,CASE WHEN ( TransferIn >= 1 ) THEN 1
                         WHEN (
                                FirstTime = 0
                                AND TransferIn = 0
                                AND NonDegreeSeeking >= 1
                                AND Continuing = 0
                              ) THEN 1
                         WHEN (
                                FirstTime = 0
                                AND TransferIn = 0
                                AND NonDegreeSeeking = 0
                                AND Continuing >= 1
                              ) THEN 1
                         WHEN (
                                FirstTime = 0
                                AND TransferIn = 0
                                AND NonDegreeSeeking >= 1
                                AND Continuing >= 1
                              ) THEN 2
                         ELSE NULL
                    END AS AllOther
                   ,CASE WHEN FirstTime >= 1 THEN 1
                         ELSE 0
                    END AS FirstTimeCount
                   ,CASE WHEN (
                                (
                                  Continuing = 0
                                  OR Continuing IS NULL
                                )
                                AND TransferIn >= 1
                              ) THEN 1
                         ELSE 0
                    END AS TransferInCount
                   ,CASE WHEN (
                                Continuing >= 1
                                AND (
                                      TransferIn = 0
                                      OR TransferIn IS NULL
                                      OR TransferIn = 1
                                    )
                                AND (
                                      FirstTime = 0
                                      OR FirstTime IS NULL
                                    )
                              ) THEN 1
                         ELSE 0
                    END AS CountinuingCount
                   ,CASE WHEN (
                                NonDegreeSeeking >= 1
                                AND (
                                      TransferIn = 0
                                      OR TransferIn IS NULL
                                    )
                                AND (
                                      FirstTime = 0
                                      OR FirstTime IS NULL
                                    )
                              ) THEN 1
                         ELSE 0
                    END AS NonDegreeSeekingCount
                   ,CASE WHEN ( TransferIn >= 1 ) THEN 1
                         WHEN (
                                FirstTime = 0
                                AND TransferIn = 0
                                AND NonDegreeSeeking >= 1
                                AND Continuing = 0
                              ) THEN 1
                         WHEN (
                                FirstTime = 0
                                AND TransferIn = 0
                                AND NonDegreeSeeking = 0
                                AND Continuing >= 1
                              ) THEN 1
                         WHEN (
                                FirstTime = 0
                                AND TransferIn = 0
                                AND NonDegreeSeeking >= 1
                                AND Continuing >= 1
                              ) THEN 2
                         ELSE NULL
                    END AS AllOtherCount
                   ,GenderSequence
                   ,RaceSequence
                   ,
		-- 2010-2011 (New Field)
		-- Condition: If 15a or 16 has 'X' then Other should also have a 'X'
                    CASE WHEN (
                                ( TransferIn >= 1 )
                                OR (
                                     (Continuing >= 1
                                     AND (
                                           TransferIn = 0
                                           OR TransferIn IS NULL
                                         )
                                     AND (
                                           FirstTime = 0
                                           OR FirstTime IS NULL
                                         ))
                                   )
                              ) THEN 'X'
                         ELSE ''
                    END AS Other
                   ,CASE WHEN (
                                ( TransferIn >= 1 )
                                OR (
                                     (Continuing >= 1
                                     AND (
                                           TransferIn = 0
                                           OR TransferIn IS NULL
                                         )
                                     AND (
                                           FirstTime = 0
                                           OR FirstTime IS NULL
                                         ))
                                   )
                              ) THEN 1
                         ELSE 0
                    END AS OtherCount
            FROM    (
                      -- Check if there are any Foreign Male Students who are Full-Time Undergraduates
		-- If there are no foreign students in that category, insert a blank record
		-- with race 'Nonresident Alien'
		-- This race does not exist in adEthCodes anymore 
		-- Needs to be deleted from adEthCodes
                      SELECT TOP 1
                                NULL AS SSN
                               ,NULL AS StudentNumber
                               ,NULL AS StudentName
                               ,'Men' AS Gender
                               ,'Nonresident Alien' AS Race
                               ,NULL AS FirstTime
                               ,NULL AS TransferIn
                               ,NULL AS NonDegreeSeeking
                               ,NULL AS Continuing
                               ,NULL AS AllOther
                               ,1 AS GenderSequence
                               ,1 AS RaceSequence
                      FROM      arStudent
                      WHERE     (
                                  SELECT    COUNT(t1.FirstName)
                                  FROM      adGenders t3
                                  LEFT JOIN arStudent t1 ON t3.GenderId = t1.Gender
                                  INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                                  INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                                  INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                                               AND t6.SysStatusId NOT IN ( 8 )
                                  INNER JOIN adCitizenships t11 ON t1.Citizen = t11.CitizenshipId
                                  INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                                  INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                                  INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                                  LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                                  LEFT JOIN adDegCertSeeking t16 ON t2.degcertseekingid = t16.degcertseekingid
                                  WHERE     LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                                            AND t11.IPEDSValue = 65
                                            AND -- non citizen
                                            (
                                              t9.IPEDSValue = 59
                                              OR t9.IPEDSValue = 60
                                              OR t16.IPEDSValue = 11
                                            )
                                            AND -- graduate or first professional
                                            t3.IPEDSValue = 30
                                            AND --Men
                                            (
                                              t10.IPEDSValue = 61
                                              OR t10.IPEDSValue = 62
                                            )
                                            AND -- Full Time or Part Time
                                            (
                                              @ProgId IS NULL
                                              OR t8.ProgId IN ( SELECT  Val
                                                                FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                            ) 
							--and (@StartDate is null or t2.StartDate>=@StartDate) and (@EndDate is null or t2.StartDate<=@EndDate) 
                                            AND t2.StartDate <= @EndDate
                                            AND StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
						( SELECT    t1.StuEnrollId
                          FROM      arStuEnrollments t1
                                   ,SyStatusCodes t2
                          WHERE     t1.StatusCodeId = t2.StatusCodeId
                                    AND StartDate <= @EndDate
                                    AND -- Student started before the end date range
                                    LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                    AND (
                                          @ProgId IS NULL
                                          OR t8.ProgId IN ( SELECT  Val
                                                            FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                        )
                                    AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                    AND (
                                          t1.DateDetermined < @StartDate
                                          OR ExpGradDate < @StartDate
                                          OR LDA < @StartDate
                                        ) )
							-- If Student is enrolled in only program version and if that program version 
						-- happens to be a continuing ed program exclude the student
                                            AND t2.StudentId NOT IN ( SELECT DISTINCT
                                                                                StudentId
                                                                      FROM      (
                                                                                  SELECT    StudentId
                                                                                           ,COUNT(*) AS RowCounter
                                                                                  FROM      arStuENrollments
                                                                                  WHERE     PrgVerId IN ( SELECT    PrgVerId
                                                                                                          FROM      arPrgVersions
                                                                                                          WHERE     IsContinuingEd = 1 )
                                                                                  GROUP BY  StudentId
                                                                                  HAVING    COUNT(*) = 1
                                                                                ) dtStudent_ContinuingEd )
						-- If Student is enrolled in two undergraduate programs or two graduate programs the student should be shown only once
                                            AND t2.StuEnrollId IN (
                                            --SELECT TOP 1
                                            --        StuEnrollId
                                            --FROM    arStuEnrollments A1 ,
                                            --        arPrgVersions A2 ,
                                            --        arProgTypes A3
                                            --WHERE   A1.PrgVerId = A2.PrgVerId
                                            --        AND A2.ProgTypId = A3.ProgTypId
                                            --        AND A1.StudentId = t1.StudentId
                                            --        AND ( A3.IPEDSValue = 59
                                            --              OR A3.IPEDSValue = 60
                                            --            )
                                            --ORDER BY StartDate ,
                                            --        EnrollDate ASC
                                            SELECT  dbo.GetIPEDS_Spring_GetEnrollmentfromMultipleEnrollment(@CampusId,@ProgId,@EndDate,'graduate',t1.studentid) )
                                ) = 0
                      UNION
			-- Check if there are any Foreign Students who are Full-Time Undergraduates
			-- If there are no foreign students in that category, insert a blank record
			-- with race 'Nonresident Alien'
                      SELECT    t1.SSN
                               ,t1.StudentNumber
                               ,t1.LastName + ', ' + t1.FirstName + ' ' + ISNULL(t1.MiddleName,'') AS StudentName
                               ,(
                                  SELECT DISTINCT
                                            AgencyDescrip
                                  FROM      syRptAgencyFldValues
                                  WHERE     RptAgencyFldValId = t3.IPEDSValue
                                ) AS Gender
                               ,'Nonresident Alien' AS Race
                               ,
				--(Select Count(*) from arStuEnrollments SQ1,adDegCertSeeking  SQ2
				--	where 
				--		SQ1.StuEnrollId = t2.StuEnrollId and 
				--		SQ1.StartDate<=@EndDate and 
				--		SQ1.degcertseekingid = SQ2.DegCertSeekingId  and
				--		SQ2.IPEDSValue=11
				--) as FirstTime,
                                CASE WHEN ( SUBSTRING(LOWER(@DateRangeText),1,10) = 'enrollment' ) THEN  -- IF ACADEMIC PROGRAM
                                          (
                                            SELECT  COUNT(*)
                                            FROM    arStuEnrollments SQ1
                                                   ,adDegCertSeeking SQ2
                                            WHERE   SQ1.StuEnrollId = t2.StuEnrollId
                                                    AND 
							-- SQ1.StartDate<=@EndDate and
							 --2/07/2012 - DE9062 - Academic year options should have a cutoff start date For First Time 
                                                    (
                                                      t2.StartDate > @AcadInstFirstTimeStartDate
                                                      AND t2.StartDate <= @EndDate
                                                    )
                                                    AND SQ1.degcertseekingid = SQ2.DegCertSeekingId
                                                    AND SQ2.IPEDSValue = 11
                                          )
                                     ELSE -- If PROGRAM Reporter
                                          (
                                            SELECT  COUNT(*)
                                            FROM    arStuEnrollments SQ1
                                                   ,adDegCertSeeking SQ2
                                            WHERE   SQ1.StuEnrollId = t2.StuEnrollId
                                                    AND (
                                                          SQ1.StartDate >= @StartDate
                                                          AND SQ1.StartDate <= @EndDate
                                                        )
                                                    AND SQ1.degcertseekingid = SQ2.DegCertSeekingId
                                                    AND SQ2.IPEDSValue = 11
                                          )
                                END AS FirstTime
                               ,
				-- If Student is a transfer-in from another school, then there should not be any previous enrollments
	-- and the current enrollment we are dealing with at this point, should be the first enrollment

	-- Check if student has multiple enrollments and also if there any previous enrollments
	-- In addition, check if TransferHours>0, works only for clock hour schools (or)
	-- If student received any grade marked as Transfer Grade in GRade systems page (or)
	-- If IsTransferred column is set to 1 in arResults page
	--(Select count(*) from arStuEnrollments where StudentId=SQ1.StudentId and LeadId is not null)=1

	-- Consider only students who transferred from another institution
	-- So, consider only the first enrollment, lead id will have a value for 1st enrollment
                                (
                                  SELECT    COUNT(*)
                                  FROM      arStuEnrollments SQ1
                                           ,adDegCertSeeking SQ2
                                  WHERE     SQ1.StuEnrollId = t2.StuEnrollId
                                            AND SQ1.LeadId IS NOT NULL
                                            AND SQ1.degcertseekingid = SQ2.DegCertSeekingId
                                            AND SQ2.IPEDSValue = 11
                                            AND (
                                                  SQ1.TransferHours > 0
                                                  OR SQ1.StuEnrollId = CASE WHEN @Value = 'letter'
                                                                            THEN (
                                                                                   SELECT TOP 1
                                                                                            StuEnrollId
                                                                                   FROM     arTransferGrades
                                                                                   WHERE    StuEnrollId = SQ1.StuEnrollId
                                                                                            AND GrdSysDetailId IN ( SELECT  GrdSysDetailId
                                                                                                                    FROM    arGradeSystemDetails
                                                                                                                    WHERE   isTransferGrade = 1 )
                                                                                 )
                                                                            ELSE (
                                                                                   SELECT TOP 1
                                                                                            StuEnrollId
                                                                                   FROM     arTransferGrades
                                                                                   WHERE    IsTransferred = 1
                                                                                            AND StuEnrollId = SQ1.StuEnrollId
                                                                                 )
                                                                       END
                                                )
                                            AND SQ1.StartDate <= @EndDate
                                            AND NOT EXISTS ( SELECT StuEnrollId
                                                             FROM   arStuEnrollments
                                                             WHERE  StudentId = SQ1.StudentId
                                                                    AND StartDate < SQ1.StartDate )
                                ) AS TransferIn
                               ,(
                                  SELECT    COUNT(*)
                                  FROM      arStuEnrollments SQ1
                                           ,adDegCertSeeking SQ2
                                  WHERE     SQ1.StuEnrollId = t2.StuEnrollId
                                            AND SQ1.degcertseekingid = SQ2.DegCertSeekingId
                                            AND SQ2.IPEDSValue = 10
                                ) AS NonDegreeSeeking
                               ,CASE WHEN ( SUBSTRING(LOWER(@DateRangeText),1,10) = 'enrollment' ) THEN  -- IF ACADEMIC PROGRAM
                                          (
                                            SELECT  COUNT(*)
                                            FROM    arStuEnrollments SQ1
                                                   ,adDegCertSeeking SQ2
                                                   ,syStatusCodes SQ3
                                                   ,sySysStatus SQ4
                                            WHERE   SQ1.StuEnrollId = t2.StuEnrollId
                                                    AND SQ1.degcertseekingid = SQ2.DegCertSeekingId
                                                    AND
						-- The StartDate condition modified to < @StartDate due to Rally case DE868
						-- originally it was SQ1.StartDate<=@StartDate
                                                   --DE9062
                                                    (
                                                      ( SQ2.IPEDSValue = 12 )
                                                      OR (
                                                           SQ2.IPEDSValue = 11
                                                           AND SQ1.StartDate <= @AcadInstFirstTimeStartDate
                                                         )
                                                    )
                                                    AND SQ1.StatusCodeId = SQ3.StatusCodeId
                                                    AND SQ3.SysStatusId = SQ4.SysStatusId  -- and SQ4.InSchool=1
                                          )
                                     ELSE (
                                            SELECT  COUNT(*)
                                            FROM    arStuEnrollments SQ1
                                                   ,adDegCertSeeking SQ2
                                                   ,syStatusCodes SQ3
                                                   ,sySysStatus SQ4
                                            WHERE   SQ1.StuEnrollId = t2.StuEnrollId
                                                    AND SQ1.degcertseekingid = SQ2.DegCertSeekingId
                                                    AND
						-- The StartDate condition modified to < @StartDate due to Rally case DE868
						-- originally it was SQ1.StartDate<=@StartDate
                                                    (
                                                      SQ2.IPEDSValue = 12
                                                      OR (
                                                           SQ2.IPEDSValue = 11
                                                           AND SQ1.StartDate < @ContinuingStartDate
                                                         )
                                                    )
                                                    AND SQ1.StatusCodeId = SQ3.StatusCodeId
                                                    AND SQ3.SysStatusId = SQ4.SysStatusId  -- and SQ4.InSchool=1
                                          )
                                END AS Continuing
                               ,NULL AS AllOther
                               ,t3.IPEDSSequence AS GenderSequence
                               ,1 AS RaceSequence
                      FROM      adGenders t3
                      LEFT JOIN arStudent t1 ON t3.GenderId = t1.Gender
                      LEFT JOIN adEthCodes t4 ON t4.EthCodeId = t1.Race
                      INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                      INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                      INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                                   AND t6.SysStatusId NOT IN ( 8 )
                      INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                      INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                      INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                      LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                      INNER JOIN adCitizenships t11 ON t1.Citizen = t11.CitizenshipId
                      LEFT JOIN adDegCertSeeking t16 ON t2.degcertseekingid = t16.degcertseekingid
                      WHERE     LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                                AND (
                                      @ProgId IS NULL
                                      OR t8.ProgId IN ( SELECT  Val
                                                        FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                    )
                                AND (
                                      t9.IPEDSValue = 59
                                      OR t9.IPEDSValue = 60
                                      OR t16.IPEDSValue = 11
                                    )
                                AND -- graduate or first professional
                                (
                                  t10.IPEDSValue = 61
                                  OR t10.IPEDSValue = 62
                                )
                                AND -- Full Time or Part Time
                                t3.IPEDSValue = 30
                                AND -- Men
				--t1.Race is not null	 
                                t11.IPEDSValue = 65 -- Non-Citizen
				--and (@StartDate is null or t2.StartDate>=@StartDate) and (@EndDate is null or t2.StartDate<=@EndDate) 
                                AND t2.StartDate <= @EndDate
                                AND StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
						( SELECT    t1.StuEnrollId
                          FROM      arStuEnrollments t1
                                   ,SyStatusCodes t2
                          WHERE     t1.StatusCodeId = t2.StatusCodeId
                                    AND StartDate <= @EndDate
                                    AND -- Student started before the end date range
                                    LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                    AND (
                                          @ProgId IS NULL
                                          OR t8.ProgId IN ( SELECT  Val
                                                            FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                        )
                                    AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                    AND (
                                          t1.DateDetermined < @StartDate
                                          OR ExpGradDate < @StartDate
                                          OR LDA < @StartDate
                                        ) )
	-- If Student is enrolled in only program version and if that program version 
						-- happens to be a continuing ed program exclude the student
                                AND t2.StudentId NOT IN ( SELECT DISTINCT
                                                                    StudentId
                                                          FROM      (
                                                                      SELECT    StudentId
                                                                               ,COUNT(*) AS RowCounter
                                                                      FROM      arStuENrollments
                                                                      WHERE     PrgVerId IN ( SELECT    PrgVerId
                                                                                              FROM      arPrgVersions
                                                                                              WHERE     IsContinuingEd = 1 )
                                                                      GROUP BY  StudentId
                                                                      HAVING    COUNT(*) = 1
                                                                    ) dtStudent_ContinuingEd )
				-- If Student is enrolled in two undergraduate programs or two graduate programs the student should be shown only once
                                AND t2.StuEnrollId IN (
                                --SELECT TOP 1
                                --        StuEnrollId
                                --FROM    arStuEnrollments A1 ,
                                --        arPrgVersions A2 ,
                                --        arProgTypes A3
                                --WHERE   A1.PrgVerId = A2.PrgVerId
                                --        AND A2.ProgTypId = A3.ProgTypId
                                --        AND A1.StudentId = t1.StudentId
                                --        AND ( A3.IPEDSValue = 59
                                --              OR A3.IPEDSValue = 60
                                --            )
                                --ORDER BY StartDate ,
                                --        EnrollDate ASC
                                SELECT  dbo.GetIPEDS_Spring_GetEnrollmentfromMultipleEnrollment(@CampusId,@ProgId,@EndDate,'graduate',t1.studentid) )
                      UNION				
		-- Check if there are any US Students who are Full-Time Undergraduates
		-- If there are no US Citizens in that category, insert a blank record
		-- for corresponding races
		-- Ignore NonResident Alien
                      SELECT    NULL AS SSN
                               ,NULL AS StudentNumber
                               ,NULL AS StudentName
                               ,'Men' AS Gender
                               ,(
                                  SELECT DISTINCT
                                            AgencyDescrip
                                  FROM      syRptAgencyFldValues
                                  WHERE     RptAgencyFldValId = IPEDSValue
                                ) AS Race
                               ,NULL AS FirstTime
                               ,NULL AS TransferIn
                               ,NULL AS NonDegreeSeeking
                               ,NULL AS Continuing
                               ,NULL AS AllOther
                               ,1 AS GenderSequence
                               ,IPEDSSequence AS RaceSequence
                      FROM      adEthCodes
                      WHERE     EthCodeDescrip <> 'Nonresident Alien'
                                AND IPEDSValue IS NOT NULL
                                AND EthCodeId NOT IN (
                                SELECT DISTINCT
                                        t1.Race
                                FROM    arStudent t1
                                INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                                INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                                INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                                             AND t6.SysStatusId NOT IN ( 8 )
                                INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                                INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                                INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                                LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                                INNER JOIN adGenders t11 ON t1.Gender = t11.GenderId
                                INNER JOIN adCitizenships t12 ON t1.Citizen = t12.CitizenshipId
                                LEFT JOIN adDegCertSeeking t16 ON t2.degcertseekingid = t16.degcertseekingid
                                WHERE   LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                                        AND (
                                              @ProgId IS NULL
                                              OR t8.ProgId IN ( SELECT  Val
                                                                FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                            )
                                        AND (
                                              t9.IPEDSValue = 59
                                              OR t9.IPEDSValue = 60
                                              OR t16.IPEDSValue = 11
                                            )
                                        AND -- graduate or first professional
                                        (
                                          t10.IPEDSValue = 62
                                          OR t10.IPEDSValue = 61
                                        )
                                        AND --Part Time and
                                        t11.IPEDSValue = 30
                                        AND t1.Race IS NOT NULL
                                        AND t12.IPEDSValue <> 65 -- Ignore Non US Citizens (Foreign Students)
								--and (@StartDate is null or t2.StartDate>=@StartDate) and (@EndDate is null or t2.StartDate<=@EndDate) 
                                        AND t2.StartDate <= @EndDate
                                        AND StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
						( SELECT    t1.StuEnrollId
                          FROM      arStuEnrollments t1
                                   ,SyStatusCodes t2
                          WHERE     t1.StatusCodeId = t2.StatusCodeId
                                    AND StartDate <= @EndDate
                                    AND -- Student started before the end date range
                                    LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                    AND (
                                          @ProgId IS NULL
                                          OR t8.ProgId IN ( SELECT  Val
                                                            FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                        )
                                    AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                    AND (
                                          t1.DateDetermined < @StartDate
                                          OR ExpGradDate < @StartDate
                                          OR LDA < @StartDate
                                        ) )
							-- If Student is enrolled in only program version and if that program version 
						-- happens to be a continuing ed program exclude the student
                                        AND t2.StudentId NOT IN ( SELECT DISTINCT
                                                                            StudentId
                                                                  FROM      (
                                                                              SELECT    StudentId
                                                                                       ,COUNT(*) AS RowCounter
                                                                              FROM      arStuENrollments
                                                                              WHERE     PrgVerId IN ( SELECT    PrgVerId
                                                                                                      FROM      arPrgVersions
                                                                                                      WHERE     IsContinuingEd = 1 )
                                                                              GROUP BY  StudentId
                                                                              HAVING    COUNT(*) = 1
                                                                            ) dtStudent_ContinuingEd )
					-- If Student is enrolled in two undergraduate programs or two graduate programs the student should be shown only once
                                        AND t2.StuEnrollId IN (
                                        --SELECT TOP 1
                                        --        StuEnrollId
                                        --FROM    arStuEnrollments A1 ,
                                        --        arPrgVersions A2 ,
                                        --        arProgTypes A3
                                        --WHERE   A1.PrgVerId = A2.PrgVerId
                                        --        AND A2.ProgTypId = A3.ProgTypId
                                        --        AND A1.StudentId = t1.StudentId
                                        --        AND ( A3.IPEDSValue = 59
                                        --              OR A3.IPEDSValue = 60
                                        --            )
                                        --ORDER BY StartDate ,
                                        --        EnrollDate ASC
                                        SELECT  dbo.GetIPEDS_Spring_GetEnrollmentfromMultipleEnrollment(@CampusId,@ProgId,@EndDate,'graduate',t1.studentid) ) )
                      UNION
                      SELECT    t1.SSN
                               ,t1.StudentNumber
                               ,t1.LastName + ', ' + t1.FirstName + ' ' + ISNULL(t1.MiddleName,'') AS StudentName
                               ,(
                                  SELECT DISTINCT
                                            AgencyDescrip
                                  FROM      syRptAgencyFldValues
                                  WHERE     RptAgencyFldValId = t3.IPEDSValue
                                ) AS Gender
                               ,CASE WHEN t4.IPEDSValue IS NULL
                                     -- THEN 'Race/ethnicity unknown' --de9087
                                          THEN ''
                                     ELSE (
                                            SELECT DISTINCT
                                                    AgencyDescrip
                                            FROM    syRptAgencyFldValues
                                            WHERE   RptAgencyFldValId = t4.IPEDSValue
                                          )
                                END AS Race
                               ,
				--(Select Count(*) from arStuEnrollments SQ1,adDegCertSeeking  SQ2
				--	where 
				--		SQ1.StuEnrollId = t2.StuEnrollId and 
				--		SQ1.StartDate<=@EndDate and 
				--		SQ1.degcertseekingid = SQ2.DegCertSeekingId  and
				--		SQ2.IPEDSValue=11
				--) as FirstTime,
                                CASE WHEN ( SUBSTRING(LOWER(@DateRangeText),1,10) = 'enrollment' ) THEN  -- IF ACADEMIC PROGRAM
                                          (
                                            SELECT  COUNT(*)
                                            FROM    arStuEnrollments SQ1
                                                   ,adDegCertSeeking SQ2
                                            WHERE   SQ1.StuEnrollId = t2.StuEnrollId
                                                    AND 
							-- SQ1.StartDate<=@EndDate and
							 --2/07/2012 - DE9062 - Academic year options should have a cutoff start date For First Time 
                                                    (
                                                      t2.StartDate > @AcadInstFirstTimeStartDate
                                                      AND t2.StartDate <= @EndDate
                                                    )
                                                    AND SQ1.degcertseekingid = SQ2.DegCertSeekingId
                                                    AND SQ2.IPEDSValue = 11
                                          )
                                     ELSE -- If PROGRAM Reporter
                                          (
                                            SELECT  COUNT(*)
                                            FROM    arStuEnrollments SQ1
                                                   ,adDegCertSeeking SQ2
                                            WHERE   SQ1.StuEnrollId = t2.StuEnrollId
                                                    AND (
                                                          SQ1.StartDate >= @StartDate
                                                          AND SQ1.StartDate <= @EndDate
                                                        )
                                                    AND SQ1.degcertseekingid = SQ2.DegCertSeekingId
                                                    AND SQ2.IPEDSValue = 11
                                          )
                                END AS FirstTime
                               ,
--				(Select Count(*) from arStuEnrollments SQ1
--					where 
--						SQ1.StuEnrollId = t2.StuEnrollId and
--						(SQ1.TransferHours>0) and 
--						SQ1.StartDate<=@EndDate and 
--						not exists (select StuEnrollId from arStuEnrollments where StudentId=SQ1.StudentId and
--						StartDate<SQ1.StartDate)
--				) as TransferIn,
-- If Student is a transfer-in from another school, then there should not be any previous enrollments
	-- and the current enrollment we are dealing with at this point, should be the first enrollment

	-- Check if student has multiple enrollments and also if there any previous enrollments
	-- In addition, check if TransferHours>0, works only for clock hour schools (or)
	-- If student received any grade marked as Transfer Grade in GRade systems page (or)
	-- If IsTransferred column is set to 1 in arResults page
	--(Select count(*) from arStuEnrollments where StudentId=SQ1.StudentId and LeadId is not null)=1

	-- Consider only students who transferred from another institution
	-- So, consider only the first enrollment, lead id will have a value for 1st enrollment
                                (
                                  SELECT    COUNT(*)
                                  FROM      arStuEnrollments SQ1
                                           ,adDegCertSeeking SQ2
                                  WHERE     SQ1.StuEnrollId = t2.StuEnrollId
                                            AND SQ1.LeadId IS NOT NULL
                                            AND SQ1.degcertseekingid = SQ2.DegCertSeekingId
                                            AND SQ2.IPEDSValue = 11
                                            AND (
                                                  SQ1.TransferHours > 0
                                                  OR SQ1.StuEnrollId = CASE WHEN @Value = 'letter'
                                                                            THEN (
                                                                                   SELECT TOP 1
                                                                                            StuEnrollId
                                                                                   FROM     arTransferGrades
                                                                                   WHERE    StuEnrollId = SQ1.StuEnrollId
                                                                                            AND GrdSysDetailId IN ( SELECT  GrdSysDetailId
                                                                                                                    FROM    arGradeSystemDetails
                                                                                                                    WHERE   isTransferGrade = 1 )
                                                                                 )
                                                                            ELSE (
                                                                                   SELECT TOP 1
                                                                                            StuEnrollId
                                                                                   FROM     arTransferGrades
                                                                                   WHERE    IsTransferred = 1
                                                                                            AND StuEnrollId = SQ1.StuEnrollId
                                                                                 )
                                                                       END
                                                )
                                            AND SQ1.StartDate <= @EndDate
                                            AND NOT EXISTS ( SELECT StuEnrollId
                                                             FROM   arStuEnrollments
                                                             WHERE  StudentId = SQ1.StudentId
                                                                    AND StartDate < SQ1.StartDate )
                                ) AS TransferIn
                               ,(
                                  SELECT    COUNT(*)
                                  FROM      arStuEnrollments SQ1
                                           ,adDegCertSeeking SQ2
                                  WHERE     SQ1.StuEnrollId = t2.StuEnrollId
                                            AND SQ1.degcertseekingid = SQ2.DegCertSeekingId
                                            AND SQ2.IPEDSValue = 10
                                ) AS NonDegreeSeeking
                               ,CASE WHEN ( SUBSTRING(LOWER(@DateRangeText),1,10) = 'enrollment' ) THEN  -- IF ACADEMIC PROGRAM
                                          (
                                            SELECT  COUNT(*)
                                            FROM    arStuEnrollments SQ1
                                                   ,adDegCertSeeking SQ2
                                                   ,syStatusCodes SQ3
                                                   ,sySysStatus SQ4
                                            WHERE   SQ1.StuEnrollId = t2.StuEnrollId
                                                    AND SQ1.degcertseekingid = SQ2.DegCertSeekingId
                                                    AND
						-- The StartDate condition modified to < @StartDate due to Rally case DE868
						-- originally it was SQ1.StartDate<=@StartDate
                                                   --DE9062
                                                    (
                                                      ( SQ2.IPEDSValue = 12 )
                                                      OR (
                                                           SQ2.IPEDSValue = 11
                                                           AND SQ1.StartDate <= @AcadInstFirstTimeStartDate
                                                         )
                                                    )
                                                    AND SQ1.StatusCodeId = SQ3.StatusCodeId
                                                    AND SQ3.SysStatusId = SQ4.SysStatusId  -- and SQ4.InSchool=1
                                          )
                                     ELSE (
                                            SELECT  COUNT(*)
                                            FROM    arStuEnrollments SQ1
                                                   ,adDegCertSeeking SQ2
                                                   ,syStatusCodes SQ3
                                                   ,sySysStatus SQ4
                                            WHERE   SQ1.StuEnrollId = t2.StuEnrollId
                                                    AND SQ1.degcertseekingid = SQ2.DegCertSeekingId
                                                    AND
						-- The StartDate condition modified to < @StartDate due to Rally case DE868
						-- originally it was SQ1.StartDate<=@StartDate
                                                    (
                                                      SQ2.IPEDSValue = 12
                                                      OR (
                                                           SQ2.IPEDSValue = 11
                                                           AND SQ1.StartDate < @ContinuingStartDate
                                                         )
                                                    )
                                                    AND SQ1.StatusCodeId = SQ3.StatusCodeId
                                                    AND SQ3.SysStatusId = SQ4.SysStatusId  -- and SQ4.InSchool=1
                                          )
                                END AS Continuing
                               ,NULL AS AllOther
                               ,t3.IPEDSSequence AS GenderSequence
                               ,
                                --De9087
                                --CASE WHEN t4.IPEDSValue IS NULL THEN 9
                                --     ELSE t4.[IPEDSSequence]
                                --END AS RaceSequence
                                t4.IPEDSSequence AS RaceSequence
                      FROM      adGenders t3
                      LEFT JOIN arStudent t1 ON t3.GenderId = t1.Gender
                      LEFT JOIN adEthCodes t4 ON t4.EthCodeId = t1.Race
                      INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                      INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                      INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                                   AND t6.SysStatusId NOT IN ( 8 )
                      INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                      INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                      INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                      LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                      INNER JOIN adCitizenships t12 ON t1.Citizen = t12.CitizenshipId
                      LEFT JOIN adDegCertSeeking t16 ON t2.degcertseekingid = t16.degcertseekingid
                      WHERE     t2.CampusId = LTRIM(RTRIM(@CampusId))
                                AND (
                                      @ProgId IS NULL
                                      OR t8.ProgId IN ( SELECT  Val
                                                        FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                    )
                                AND (
                                      t9.IPEDSValue = 59
                                      OR t9.IPEDSValue = 60
                                      OR t16.IPEDSValue = 11
                                    )
                                AND -- graduate or first professional
                                (
                                  t10.IPEDSValue = 61
                                  OR t10.IPEDSValue = 62
                                )
                                AND -- Full Time or Part Time
                                t3.IPEDSValue = 30
                                AND t1.Race IS NOT NULL
                                AND (
                                      t12.IPEDSValue <> 65
                                      OR t12.IPEDSValue IS NULL
                                    ) -- Ignore Non US Citizens (Foreign Students)
				--and (@StartDate is null or t2.StartDate>=@StartDate) and (@EndDate is null or t2.StartDate<=@EndDate) 
                                AND t2.StartDate <= @EndDate
                                AND StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
						( SELECT    t1.StuEnrollId
                          FROM      arStuEnrollments t1
                                   ,SyStatusCodes t2
                          WHERE     t1.StatusCodeId = t2.StatusCodeId
                                    AND StartDate <= @EndDate
                                    AND -- Student started before the end date range
                                    LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                    AND (
                                          @ProgId IS NULL
                                          OR t8.ProgId IN ( SELECT  Val
                                                            FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                        )
                                    AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                    AND (
                                          t1.DateDetermined < @StartDate
                                          OR ExpGradDate < @StartDate
                                          OR LDA < @StartDate
                                        ) )
					-- If Student is enrolled in only program version and if that program version 
						-- happens to be a continuing ed program exclude the student
                                AND t2.StudentId NOT IN ( SELECT DISTINCT
                                                                    StudentId
                                                          FROM      (
                                                                      SELECT    StudentId
                                                                               ,COUNT(*) AS RowCounter
                                                                      FROM      arStuENrollments
                                                                      WHERE     PrgVerId IN ( SELECT    PrgVerId
                                                                                              FROM      arPrgVersions
                                                                                              WHERE     IsContinuingEd = 1 )
                                                                      GROUP BY  StudentId
                                                                      HAVING    COUNT(*) = 1
                                                                    ) dtStudent_ContinuingEd )
				-- If Student is enrolled in two undergraduate programs or two graduate programs the student should be shown only once
                                AND t2.StuEnrollId IN (
                                --SELECT TOP 1
                                --        StuEnrollId
                                --FROM    arStuEnrollments A1 ,
                                --        arPrgVersions A2 ,
                                --        arProgTypes A3
                                --WHERE   A1.PrgVerId = A2.PrgVerId
                                --        AND A2.ProgTypId = A3.ProgTypId
                                --        AND A1.StudentId = t1.StudentId
                                --        AND ( A3.IPEDSValue = 59
                                --              OR A3.IPEDSValue = 60
                                --            )
                                --ORDER BY StartDate ,
                                --        EnrollDate ASC 
                                SELECT  dbo.GetIPEDS_Spring_GetEnrollmentfromMultipleEnrollment(@CampusId,@ProgId,@EndDate,'graduate',t1.studentid) )
                    ) dt;
--Order by 
--GenderSequence, RaceSequence,
--Case when @OrderBy='SSN' Then dt.SSN end,
--Case when @OrderBy='LastName' Then dt.StudentName end,
--Case When @OrderBy='StudentNumber' Then Convert(int,dt.StudentNumber) end

    DECLARE @FirstTime_MenCount INT
       ,@FirstTime_WomenCount INT
       ,@TransferIn_MenCount INT
       ,@TransferIn_WomenCount INT;
    DECLARE @Continuing_MenCount INT
       ,@Continuing_WomenCount INT
       ,@NonDegreeSeeking_MenCount INT;
    DECLARE @NonDegreeSeeking_WomenCount INT
       ,@AllOther_MenCount INT
       ,@AllOther_WomenCount INT
       ,@Other_MenCount INT
       ,@Other_WomenCount INT;

    SET @FirstTime_MenCount = 0;
    SET @FirstTime_WomenCount = 0;
    SET @TransferIn_MenCount = 0;
    SET @TransferIn_WomenCount = 0;
    SET @Continuing_MenCount = 0;
    SET @Continuing_WomenCount = 0;
    SET @NonDegreeSeeking_MenCount = 0;
    SET @NonDegreeSeeking_WomenCount = 0;
    SET @AllOther_MenCount = 0;
    SET @AllOther_WomenCount = 0;
    SET @Other_MenCount = 0;
    SET @Other_WomenCount = 0;


    SET @FirstTime_MenCount = (
                                SELECT  SUM(ISNULL(FirstTimeCount,0))
                                FROM    #FallPartB4FTUG
                                WHERE   LOWER(Gender) = 'men'
                              );
--set @FirstTime_WomenCount = (select SUM(IsNULL(FirstTimeCount,0))from #FallPartB4FTUG where LOWER(Gender)='women')
    SET @TransferIn_MenCount = (
                                 SELECT SUM(ISNULL(TransferInCount,0))
                                 FROM   #FallPartB4FTUG
                                 WHERE  LOWER(Gender) = 'men'
                               );
--set @TransferIn_WomenCount = (select SUM(IsNULL(TransferInCount,0)) from #FallPartB4FTUG where LOWER(Gender)='women')
    SET @Continuing_MenCount = (
                                 SELECT SUM(ISNULL(CountinuingCount,0))
                                 FROM   #FallPartB4FTUG
                                 WHERE  LOWER(Gender) = 'men'
                               );
--set @Continuing_WomenCount = (select SUM(IsNULL(CountinuingCount,0)) from #FallPartB4FTUG where LOWER(Gender)='women')
    SET @NonDegreeSeeking_MenCount = (
                                       SELECT   SUM(ISNULL(NonDegreeSeekingCount,0))
                                       FROM     #FallPartB4FTUG
                                       WHERE    LOWER(Gender) = 'men'
                                     );
--set @NonDegreeSeeking_WomenCount = (select SUM(IsNULL(NonDegreeSeekingCount,0)) from #FallPartB4FTUG where LOWER(Gender)='women')
    SET @AllOther_MenCount = (
                               SELECT   SUM(ISNULL(AllOtherCount,0))
                               FROM     #FallPartB4FTUG
                               WHERE    LOWER(Gender) = 'men'
                             );
--set @AllOther_WomenCount = (select SUM(IsNULL(AllOtherCount,0)) from #FallPartB4FTUG where LOWER(Gender)='women')
    SET @Other_MenCount = (
                            SELECT  SUM(ISNULL(OtherCount,0))
                            FROM    #FallPartB4FTUG
                            WHERE   LOWER(Gender) = 'men'
                          );
    SET @Other_WomenCount = (
                              SELECT    SUM(ISNULL(OtherCount,0))
                              FROM      #FallPartB4FTUG
                              WHERE     LOWER(Gender) = 'women'
                            );

--Print @FirstTime_MenCount
    SELECT  Gender
           ,Race
           ,GenderSequence
           ,RaceSequence
           ,
		--SUM(FirstTimeCount) as FirstTimeCount,SUM(TransferInCount) as TransferInCount,SUM(CountinuingCount) as CountinuingCount,
		--SUM(NonDegreeSeekingCount) as NonDegreeSeekingCount,SUM(AllOtherCount) as AllOtherCount,
            SUM(ISNULL(FirstTimeCount,0)) AS FirstTimeCount
           ,SUM(ISNULL(TransferInCount,0)) AS TransferInCount
           ,SUM(ISNULL(CountinuingCount,0)) AS CountinuingCount
           ,SUM(ISNULL(NonDegreeSeekingCount,0)) AS NonDegreeSeekingCount
           ,SUM(ISNULL(AllOtherCount,0)) AS AllOtherCount
           ,@FirstTime_MenCount AS FirstTime_MenCount
           ,--@FirstTime_WomenCount as FirstTime_WomenCount,
            @TransferIn_MenCount AS TransferInMenCOunt
           ,--@TransferIn_WomenCount as TransferInWomenCount,
            @Continuing_MenCount AS ContinuingMenCount
           ,--@Continuing_WomenCount as ContinuingWomenCount,
            @NonDegreeSeeking_MenCount AS NonDegreeSeekingMenCount
           ,--@NonDegreeSeeking_WomenCount as NonDegreeSeekingWomenCount,
            @AllOther_MenCount AS AllOtherMenCount
           , --@AllOther_WomenCount as AllOtherWomenCount
            ISNULL(@Other_MenCount,0) AS OtherMenCount
           ,ISNULL(@Other_WomenCount,0) AS OtherWomenCount
           ,SUM(ISNULL(OtherCount,0)) AS OtherCount
    FROM    #FallPartB4FTUG
    GROUP BY Gender
           ,Race
           ,GenderSequence
           ,RaceSequence
    ORDER BY GenderSequence
           ,RaceSequence;
    DROP TABLE #FallPartB4FTUG;



GO
