SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


--=================================================================================================
-- USP_SY_KlassApp_InitializeConfigurationSettings
--=================================================================================================
-- Author:		JAGG
-- Create date: 2/20/17 3/22/17 Modification: 12/4/2017 -- Added Credit Earned
-- Description:	Truncate table syKlassAppConfigurationSetting and add again the values in insert state
-- =============================================
--=================================================================================================
-- USP_SY_KlassApp_InitializeConfigurationSettings
--=================================================================================================
-- Author:		JAGG
-- Create date: 2/20/17 3/22/17 Modification: 12/11/2017 -- Added Credit Earned & Credit Attemped
-- Description:	Truncate table syKlassAppConfigurationSetting and add again the values in insert state
-- =============================================
CREATE PROCEDURE [dbo].[USP_SY_KlassApp_InitializeConfigurationSettings] @UserName VARCHAR(50)
AS
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;

    -- Truncate syKlassAppConfigurationSetting Table to eliminate actual settings
        TRUNCATE TABLE dbo.syKlassAppConfigurationSetting;
	
	-- Insert locations, 
        INSERT  INTO dbo.syKlassAppConfigurationSetting
                (
                 -- Id
                 AdvantageId
                ,KlassAppId
                ,KlassEntityId
                ,KlassOperationTypeId
                ,IsCustomField
                ,IsActive
                ,ItemStatus
                ,ItemValue
                ,ItemLabel
                ,ItemValueType
                ,CreationDate
                ,ModDate
                ,ModUser
                )
                SELECT 
      --   0  -- Id - int
                        CampusId  -- AdvantageIdentification - varchar(38)
                       ,0  -- KlassAppIdentification - int
                       ,1  -- KlassEntityId - int
                       ,1  -- KlassOperationTypeId - int
                       ,0  -- IsCustomField - bit
                       ,1  -- IsActive - bit
                       ,'I'  -- ItemStatus - char(1)
                       ,CampDescrip  -- ItemValue - varchar(150)
                       ,'Campus'  -- ItemLabel - varchar(50)
                       ,'string'  -- ItemValueType - varchar(50)
                       ,GETDATE()  -- CreationDate - datetime
                       ,GETDATE()  -- ModDate - datetime
                       ,@UserName  -- ModUser - varchar(50)
                FROM    dbo.syCampuses
                WHERE   syCampuses.CampusId NOT IN ( SELECT CampusId
                                                     FROM   syCampuses
                                                     JOIN   dbo.syKlassAppConfigurationSetting CON ON CON.AdvantageId = syCampuses.CampusId
                                                     WHERE  CON.KlassOperationTypeId = 1 );

--  Student Status
        INSERT  INTO dbo.syKlassAppConfigurationSetting
                (
                 AdvantageId
                ,KlassAppId
                ,KlassEntityId
                ,KlassOperationTypeId
                ,IsCustomField
                ,IsActive
                ,ItemStatus
                ,ItemValue
                ,ItemLabel
                ,ItemValueType
                ,CreationDate
                ,ModDate
                ,ModUser
                )
                SELECT  SCC.StatusCodeId  -- AdvantageIdentification - varchar(38)
                       ,0  -- KlassAppIdentification - int
                       ,1  -- KlassEntityId - int
                       ,2  -- KlassOperationTypeId - int
                       ,0  -- IsCustomField - bit
                       ,1  -- IsActive - bit
                       ,'I'  -- ItemStatus - char(1)
                       ,SCC.StatusCodeDescrip  -- ItemValue - varchar(150)
                       ,'Status'  -- ItemLabel - varchar(50)
                       ,'string'  -- ItemValueType - varchar(50)
                       ,GETDATE()  -- CreationDate - datetime
                       ,GETDATE()  -- ModDate - datetime
                       ,@UserName  -- ModUser - varchar(50)
                FROM    dbo.syStatusCodes SCC
                JOIN    dbo.sySysStatus AS SSS ON SSS.SysStatusId = SCC.SysStatusId
                JOIN    syStatuses AS SS ON SS.StatusId = SSS.StatusId
                WHERE   SSS.StatusLevelId = 2
                        AND SS.StatusCode = 'A'
                        AND SCC.StatusCodeId NOT IN ( SELECT    StatusCodeId
                                                      FROM      syStatusCodes
                                                      JOIN      dbo.syKlassAppConfigurationSetting CON ON CON.AdvantageId = syStatusCodes.StatusCodeId
                                                      WHERE     CON.KlassOperationTypeId = 2 );

        INSERT  INTO dbo.syKlassAppConfigurationSetting
                (
                 AdvantageId
                ,KlassAppId
                ,KlassEntityId
                ,KlassOperationTypeId
                ,IsCustomField
                ,IsActive
                ,ItemStatus
                ,ItemValue
                ,ItemLabel
                ,ItemValueType
                ,CreationDate
                ,ModDate
                ,ModUser
                )
                SELECT DISTINCT
                        SCC.PrgVerId  -- AdvantageIdentification - varchar(38)
                       ,0  -- KlassAppIdentification - int
                       ,1  -- KlassEntityId - int
                       ,3  -- KlassOperationTypeId - int
                       ,0  -- IsCustomField - bit
                       ,1  -- IsActive - bit
                       ,'I'  -- ItemStatus - char(1)
                       ,SCC.PrgVerDescrip  -- ItemValue - varchar(150)
                       ,'program'  -- ItemLabel - varchar(50)
                       ,'string'  -- ItemValueType - varchar(50)
                       ,GETDATE()  -- CreationDate - datetime
                       ,GETDATE()  -- ModDate - datetime
                       ,@UserName  -- ModUser - varchar(50)
                FROM    dbo.arPrgVersions SCC
                JOIN    dbo.arStuEnrollments enroll ON enroll.PrgVerId = SCC.PrgVerId
                JOIN    dbo.syStatusCodes schoolStatus ON schoolStatus.StatusCodeId = enroll.StatusCodeId
                JOIN    dbo.sySysStatus stat ON stat.SysStatusId = schoolStatus.SysStatusId
                --JOIN    syStatuses AS SS ON SS.StatusId = SCC.StatusId
                WHERE   --SS.StatusCode = 'A'
                        stat.InSchool = 1
						-- This part exclude the values that are in the table yet
                        AND SCC.PrgVerId NOT IN ( SELECT    PrgVerId
                                                  FROM      arPrgVersions
                                                  JOIN      dbo.syKlassAppConfigurationSetting CON ON CON.AdvantageId = arPrgVersions.PrgVerId
                                                  WHERE     CON.KlassOperationTypeId = 3 );

-- ENTER Custom_Fields
-- Total Hours
        IF NOT EXISTS ( SELECT  Id
                        FROM    dbo.syKlassAppConfigurationSetting
                        WHERE   KlassOperationTypeId = 4 )
            BEGIN
                INSERT  INTO dbo.syKlassAppConfigurationSetting
                        (
                         AdvantageId
                        ,KlassAppId
                        ,KlassEntityId
                        ,KlassOperationTypeId
                        ,IsCustomField
                        ,IsActive
                        ,ItemStatus
                        ,ItemValue
                        ,ItemLabel
                        ,ItemValueType
                        ,CreationDate
                        ,ModDate
                        ,ModUser
                        )
                VALUES  (
                         '00000000-0000-0000-0000-000000000000'  -- AdvantageIdentification - varchar(38)
                        ,0  -- KlassAppIdentification - int
                        ,1  -- KlassEntityId - int
                        ,4  -- KlassOperationTypeId - int
                        ,1  -- IsCustomField - bit
                        ,1  -- IsActive - bit
                        ,'I'  -- ItemStatus - char(1)
                        ,''  -- ItemValue - varchar(150)
                        ,'Total Hours'  -- ItemLabel - varchar(50)
                        ,'numeric'  -- ItemValueType - varchar(50)
                        ,GETDATE()  -- CreationDate - datetime
                        ,GETDATE()  -- ModDate - datetime
                        ,@UserName  -- ModUser - varchar(50)
                        );
            END;

-- Absent Hours
        IF NOT EXISTS ( SELECT  Id
                        FROM    dbo.syKlassAppConfigurationSetting
                        WHERE   KlassOperationTypeId = 5 )
            BEGIN
                INSERT  INTO dbo.syKlassAppConfigurationSetting
                        (
                         AdvantageId
                        ,KlassAppId
                        ,KlassEntityId
                        ,KlassOperationTypeId
                        ,IsCustomField
                        ,IsActive
                        ,ItemStatus
                        ,ItemValue
                        ,ItemLabel
                        ,ItemValueType
                        ,CreationDate
                        ,ModDate
                        ,ModUser
                        )
                VALUES  (
                         '00000000-0000-0000-0000-000000000000'  -- AdvantageIdentification - varchar(38)
                        ,0  -- KlassAppIdentification - int
                        ,1  -- KlassEntityId - int
                        ,5  -- KlassOperationTypeId - int
                        ,1  -- IsCustomField - bit
                        ,1  -- IsActive - bit
                        ,'I'  -- ItemStatus - char(1)
                        ,''  -- ItemValue - varchar(150)
                        ,'Absent Hours'  -- ItemLabel - varchar(50)
                        ,'numeric'  -- ItemValueType - varchar(50)
                        ,GETDATE()  -- CreationDate - datetime
                        ,GETDATE()  -- ModDate - datetime
                        ,@UserName  -- ModUser - varchar(50)
                        );
            END;

-- Make-Up Hours
        IF NOT EXISTS ( SELECT  Id
                        FROM    dbo.syKlassAppConfigurationSetting
                        WHERE   KlassOperationTypeId = 6 )
            BEGIN
                INSERT  INTO dbo.syKlassAppConfigurationSetting
                        (
                         AdvantageId
                        ,KlassAppId
                        ,KlassEntityId
                        ,KlassOperationTypeId
                        ,IsCustomField
                        ,IsActive
                        ,ItemStatus
                        ,ItemValue
                        ,ItemLabel
                        ,ItemValueType
                        ,CreationDate
                        ,ModDate
                        ,ModUser
                        )
                VALUES  (
                         '00000000-0000-0000-0000-000000000000'  -- AdvantageIdentification - varchar(38)
                        ,0  -- KlassAppIdentification - int
                        ,1  -- KlassEntityId - int
                        ,6  -- KlassOperationTypeId - int
                        ,1  -- IsCustomField - bit
                        ,1  -- IsActive - bit
                        ,'I'  -- ItemStatus - char(1)
                        ,''  -- ItemValue - varchar(150)
                        ,'MAke-Up Hours'  -- ItemLabel - varchar(50)
                        ,'numeric'  -- ItemValueType - varchar(50)
                        ,GETDATE()  -- CreationDate - datetime
                        ,GETDATE()  -- ModDate - datetime
                        ,@UserName  -- ModUser - varchar(50)
                        );
            END;

-- Last Date of Attendance (lda)
        IF NOT EXISTS ( SELECT  Id
                        FROM    dbo.syKlassAppConfigurationSetting
                        WHERE   KlassOperationTypeId = 7 )
            BEGIN
                INSERT  INTO dbo.syKlassAppConfigurationSetting
                        (
                         AdvantageId
                        ,KlassAppId
                        ,KlassEntityId
                        ,KlassOperationTypeId
                        ,IsCustomField
                        ,IsActive
                        ,ItemStatus
                        ,ItemValue
                        ,ItemLabel
                        ,ItemValueType
                        ,CreationDate
                        ,ModDate
                        ,ModUser
                        )
                VALUES  (
                         '00000000-0000-0000-0000-000000000000'  -- AdvantageIdentification - varchar(38)
                        ,0  -- KlassAppIdentification - int
                        ,1  -- KlassEntityId - int
                        ,7  -- KlassOperationTypeId - int
                        ,1  -- IsCustomField - bit
                        ,1  -- IsActive - bit
                        ,'I'  -- ItemStatus - char(1)
                        ,''  -- ItemValue - varchar(150)
                        ,'Last Day of Attendance'  -- ItemLabel - varchar(50)
                        ,'date'  -- ItemValueType - varchar(50)
                        ,GETDATE()  -- CreationDate - datetime
                        ,GETDATE()  -- ModDate - datetime
                        ,@UserName  -- ModUser - varchar(50)
                        );
            END;

-- Attendance %
        IF NOT EXISTS ( SELECT  Id
                        FROM    dbo.syKlassAppConfigurationSetting
                        WHERE   KlassOperationTypeId = 8 )
            BEGIN
                INSERT  INTO dbo.syKlassAppConfigurationSetting
                        (
                         AdvantageId
                        ,KlassAppId
                        ,KlassEntityId
                        ,KlassOperationTypeId
                        ,IsCustomField
                        ,IsActive
                        ,ItemStatus
                        ,ItemValue
                        ,ItemLabel
                        ,ItemValueType
                        ,CreationDate
                        ,ModDate
                        ,ModUser
                        )
                VALUES  (
                         '00000000-0000-0000-0000-000000000000'  -- AdvantageIdentification - varchar(38)
                        ,0  -- KlassAppIdentification - int
                        ,1  -- KlassEntityId - int
                        ,8  -- KlassOperationTypeId - int
                        ,1  -- IsCustomField - bit
                        ,1  -- IsActive - bit
                        ,'I'  -- ItemStatus - char(1)
                        ,''  -- ItemValue - varchar(150)
                        ,'Attendance Percent'  -- ItemLabel - varchar(50)
                        ,'numeric'  -- ItemValueType - varchar(50)
                        ,GETDATE()  -- CreationDate - datetime
                        ,GETDATE()  -- ModDate - datetime
                        ,@UserName  -- ModUser - varchar(50)
                        );
            END;

-- GPA Exams
        IF NOT EXISTS ( SELECT  Id
                        FROM    dbo.syKlassAppConfigurationSetting
                        WHERE   KlassOperationTypeId = 9 )
            BEGIN
                INSERT  INTO dbo.syKlassAppConfigurationSetting
                        (
                         AdvantageId
                        ,KlassAppId
                        ,KlassEntityId
                        ,KlassOperationTypeId
                        ,IsCustomField
                        ,IsActive
                        ,ItemStatus
                        ,ItemValue
                        ,ItemLabel
                        ,ItemValueType
                        ,CreationDate
                        ,ModDate
                        ,ModUser
                        )
                VALUES  (
                         '00000000-0000-0000-0000-000000000000'  -- AdvantageIdentification - varchar(38)
                        ,0  -- KlassAppIdentification - int
                        ,1  -- KlassEntityId - int
                        ,9  -- KlassOperationTypeId - int
                        ,1  -- IsCustomField - bit
                        ,1  -- IsActive - bit
                        ,'I'  -- ItemStatus - char(1)
                        ,''  -- ItemValue - varchar(150)
                        ,'Exams'  -- ItemLabel - varchar(50)
                        ,'numeric'  -- ItemValueType - varchar(50)
                        ,GETDATE()  -- CreationDate - datetime
                        ,GETDATE()  -- ModDate - datetime
                        ,@UserName  -- ModUser - varchar(50)
                        );
            END;

-- Credit Earned 
        IF NOT EXISTS ( SELECT  Id
                        FROM    dbo.syKlassAppConfigurationSetting
                        WHERE   KlassOperationTypeId = 10 )
            BEGIN
                INSERT  INTO dbo.syKlassAppConfigurationSetting
                        (
                         AdvantageId
                        ,KlassAppId
                        ,KlassEntityId
                        ,KlassOperationTypeId
                        ,IsCustomField
                        ,IsActive
                        ,ItemStatus
                        ,ItemValue
                        ,ItemLabel
                        ,ItemValueType
                        ,CreationDate
                        ,ModDate
                        ,ModUser
                        )
                VALUES  (
                         '00000000-0000-0000-0000-000000000000'  -- AdvantageIdentification - varchar(38)
                        ,0  -- KlassAppIdentification - int
                        ,1  -- KlassEntityId - int
                        ,10  -- KlassOperationTypeId - int
                        ,1  -- IsCustomField - bit
                        ,1  -- IsActive - bit
                        ,'I'  -- ItemStatus - char(1)
                        ,''  -- ItemValue - varchar(150)
                        ,'Credit Earned'  -- ItemLabel - varchar(50)
                        ,'numeric'  -- ItemValueType - varchar(50)
                        ,GETDATE()  -- CreationDate - datetime
                        ,GETDATE()  -- ModDate - datetime
                        ,@UserName  -- ModUser - varchar(50)
                        );
            END;
   
-- Credit Attemped 
        IF NOT EXISTS ( SELECT  Id
                        FROM    dbo.syKlassAppConfigurationSetting
                        WHERE   KlassOperationTypeId = 11 )
            BEGIN
                INSERT  INTO dbo.syKlassAppConfigurationSetting
                        (
                         AdvantageId
                        ,KlassAppId
                        ,KlassEntityId
                        ,KlassOperationTypeId
                        ,IsCustomField
                        ,IsActive
                        ,ItemStatus
                        ,ItemValue
                        ,ItemLabel
                        ,ItemValueType
                        ,CreationDate
                        ,ModDate
                        ,ModUser
                        )
                VALUES  (
                         '00000000-0000-0000-0000-000000000000'  -- AdvantageIdentification - varchar(38)
                        ,0  -- KlassAppIdentification - int
                        ,1  -- KlassEntityId - int
                        ,11  -- KlassOperationTypeId - int
                        ,1  -- IsCustomField - bit
                        ,1  -- IsActive - bit
                        ,'I'  -- ItemStatus - char(1)
                        ,''  -- ItemValue - varchar(150)
                        ,'Credit Attempted'  -- ItemLabel - varchar(50)
                        ,'numeric'  -- ItemValueType - varchar(50)
                        ,GETDATE()  -- CreationDate - datetime
                        ,GETDATE()  -- ModDate - datetime
                        ,@UserName  -- ModUser - varchar(50)
                        );
            END;
    END;

GO
