SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =========================================================================================================
-- USP_GetStatusCodeForAGivenList
-- =========================================================================================================
CREATE PROCEDURE [dbo].[USP_GetStatusCodeForAGivenList]
    @SysStatusIdList NVARCHAR(MAX)
   ,@ShowActiveOnly BIT = 0
   ,@CampusId UNIQUEIDENTIFIER = NULL
AS
    BEGIN
        SELECT     SSC.StatusCodeId
                  ,SSC.StatusCode
                  ,SSC.StatusCodeDescrip
                  ,SSC.SysStatusId
                  ,SSC.StatusId
                  ,SS.Status
        FROM       dbo.syStatusCodes AS SSC
        INNER JOIN dbo.syStatuses AS SS ON SS.StatusId = SSC.StatusId
        INNER JOIN dbo.syCmpGrpCmps CGC ON CGC.CampGrpId = SSC.CampGrpId
        WHERE      EXISTS (
                          SELECT Val
                          FROM   dbo.MultipleValuesForReportParameters (@SysStatusIdList, ',', 1) AS L
                          WHERE  L.Val = SSC.SysStatusId
                          )
                   AND (ISNULL(@ShowActiveOnly, 0) = 0
                       OR (
                          @ShowActiveOnly = 1
                          AND SS.Status = 'Active'
                          )
                       )
                   AND (
                       @CampusId IS NULL
                       OR CGC.CampusId = @CampusId
                       );
    END;
-- =========================================================================================================
-- END  --  USP_GetStatusCodeForAGivenList
-- =========================================================================================================
GO
