SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--delete from arSAP_ShortHandSkillRequirement
--select * from arSAP_ShortHandSkillRequirement
CREATE PROCEDURE [dbo].[USP_SAPShortHandSkillRequirement_Insert]
    @SAPDetailId UNIQUEIDENTIFIER
   ,@RuleValues NTEXT
   ,@ModUser VARCHAR(50)
   ,@ModDate DATETIME
AS
    DECLARE @hDoc INT;
--Prepare input values as an XML document
    EXEC sp_xml_preparedocument @hDoc OUTPUT,@RuleValues;

-- Delete all records from bridge table based on GrdComponentTypeId
    DELETE  FROM arSAP_ShortHandSkillRequirement
    WHERE   SAPDetailId IN ( SELECT SAPDetailId
                             FROM   OPENXML(@hDoc,'/NewDataSet/SetupShortHandSkill',1) 
			WITH (SAPDetailId VARCHAR(50)) );

-- Insert records into Bridge Table 
    INSERT  INTO arSAP_ShortHandSkillRequirement
            SELECT DISTINCT
                    ShortHandSkillRequirementId
                   ,SAPDetailId
                   ,GrdComponentTypeId
                   ,Speed
                   ,@ModUser
                   ,@ModDate
                   ,Operator
                   ,OperatorSequence
            FROM    OPENXML(@hDoc,'/NewDataSet/SetupShortHandSkill',1
) 
WITH (ShortHandSkillRequirementId UNIQUEIDENTIFIER,SAPDetailId UNIQUEIDENTIFIER,
GrdComponentTypeId UNIQUEIDENTIFIER,Speed INT,ModUser VARCHAR(50),ModDate DATETIME,Operator VARCHAR(10),OperatorSequence INT);
    EXEC sp_xml_removedocument @hDoc;



GO
