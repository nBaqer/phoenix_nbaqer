SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_CampusAddress_GetList]
    @StuEnrollId VARCHAR(8000) = NULL
AS
    SELECT DISTINCT
            C.CampusId
           ,CampCode
           ,CampDescrip
           ,Address1
           ,Address2
           ,City
           ,S.StateDescrip
           ,Phone1
           ,Phone2
           ,Phone3
           ,Fax
           ,Website
           ,Zip
    FROM    syCampuses C
    LEFT OUTER JOIN syStates S ON C.StateId = S.StateId
    INNER JOIN arStuEnrollments SE ON SE.CampusId = C.CampusId
    WHERE   (
              @StuEnrollId IS NULL
              OR SE.StuEnrollId = @StuEnrollId
            )
    ORDER BY CampDescrip;



GO
