SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		FAME Inc.
-- Create date: 11/1/2018
-- Description:	Calculated Student GPA Based on a set of parameters - Numeric ( Weighted & Unweighted currently implemented)
--Referenced in :
--[Usp_PR_Sub2_Enrollment_Summary]
--[Usp_TR_Sub4_GetCumGPAandAverageforAGivenStuEnrollId]
--[USP_TitleIV_QualitativeAndQuantitative]
--CreditSummaryDB.vb
--Core Web API - EnrollmentService.GetEnrollmentProgramSummary
--[Usp_PR_Sub4_Courses_forOneTermAllEnrolmnents] -> via db function CalculateStudentAverage
--[Usp_PR_Sub3_Terms_forAllEnrolmnents] -> via db function CalculateStudentAverage
-- =============================================
CREATE PROCEDURE [dbo].[USP_GPACalculator]
    (
        @EnrollmentId VARCHAR(50) = NULL
       ,@BeginDate DATETIME = NULL
       ,@EndDate DATETIME = NULL
       ,@ClassId UNIQUEIDENTIFIER = NULL
       ,@TermId UNIQUEIDENTIFIER = NULL
       ,@StudentGPA AS DECIMAL(16, 2) OUTPUT
    )
AS
    BEGIN
        DECLARE @GPAResult AS DECIMAL(16, 2);
        -----------------Numeric GPA Grades Format------------------------

        SET @GPAResult = (
                         SELECT TOP 1 dbo.CalculateStudentAverage(@EnrollmentId, @BeginDate, @EndDate, @ClassId, @TermId,NULL)
                         );

        --END;
        SET @StudentGPA = @GPAResult;
    END;





GO
