SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_GetNeverAttemptedWUForInstructorLevel]
    (
     @stuEnrollId UNIQUEIDENTIFIER
    )
AS
    SET NOCOUNT ON;

    SELECT  d.reqid
           ,d.termId
           ,a.Descrip
           ,( CASE e.SysComponentTypeId
                WHEN 500 THEN a.Number
                WHEN 503 THEN a.Number
                WHEN 544 THEN a.Number
                ELSE (
                       SELECT   MIN(MinVal)
                       FROM     arGradeScaleDetails GSD
                               ,arGradeSystemDetails GSS
                               ,arClassSections CSR
                       WHERE    GSD.GrdSysDetailId = GSS.GrdSysDetailId
                                AND GSS.IsPass = 1
                                AND GSD.GrdScaleId = CSR.GrdScaleId
                                AND CSR.ClsSectionId = d.ClsSectionId
                     )
              END ) AS MinResult
           ,a.required
           ,a.mustpass
           ,ISNULL(e.SysComponentTypeId,0) AS WorkUnitType
           ,a.number
           ,(
              SELECT    Resource
              FROM      syResources
              WHERE     Resourceid = e.SysComponentTypeId
            ) AS WorkUnitDescrip
           ,a.InstrGrdBkWgtDetailId
           ,c.stuenrollid
           ,0 AS IsExternShip
    FROM    arGrdBkWgtDetails a
           ,arGrdBkWeights b
           ,arResults c
           ,arClassSections d
           ,arGrdComponentTypes e
    WHERE   a.InstrGrdBkWgtId = b.InstrGrdBkWgtId
            AND c.testid = d.clsSectionid
            AND b.InstrGrdBkWgtId = d.InstrGrdBkWgtId
            AND e.GrdComponentTypeId = a.GrdComponentTypeId
            AND c.StuEnrollid = @stuEnrollId
            AND a.InstrGrdBkWgtDetailId NOT IN ( SELECT InstrGrdBkWgtDetailId
                                                 FROM   arGrdBkResults
                                                 WHERE  StuEnrollId = @stuEnrollId
                                                        AND clsSectionid IN ( SELECT    clsSectionid
                                                                              FROM      arClassSections
                                                                              WHERE     Reqid = d.reqid
                                                                                        AND Termid = d.TermId ) )
    UNION
    SELECT  d.reqid
           ,d.termId
           ,e.Descrip
           ,( CASE e.SysComponentTypeId
                WHEN 500 THEN A.Number
                WHEN 503 THEN A.Number
                WHEN 544 THEN A.Number
                ELSE (
                       SELECT   MIN(MinVal)
                       FROM     arGradeScaleDetails GSD
                               ,arGradeSystemDetails GSS
                       WHERE    GSD.GrdSysDetailId = GSS.GrdSysDetailId
                                AND GSS.IsPass = 1
                     )
              END ) AS MinResult
           ,A.required
           ,A.mustpass
           ,ISNULL(e.SysComponentTypeId,0) AS WorkUnitType
           ,A.number
           ,(
              SELECT    Resource
              FROM      syResources
              WHERE     Resourceid = e.SysComponentTypeId
            ) AS WorkUnitDescrip
           ,A.InstrGrdBkWgtDetailId
           ,c.stuenrollid
           ,f.IsExternship
    FROM    arGrdBkWgtDetails A
           ,arGrdBkWeights B
           ,arResults c
           ,arClassSections d
           ,arGrdComponentTypes e
           ,arReqs f
    WHERE   A.InstrGrdBkWgtId = B.InstrGrdBkWgtId
            AND B.Reqid = d.reqid
            AND c.testid = d.clsSectionid
            AND e.GrdComponentTypeId = A.GrdComponentTypeId
            AND d.Reqid = f.Reqid
            AND f.IsExternship = 1
            AND B.EffectiveDate <= d.StartDate
            AND c.StuEnrollid = @stuEnrollId
            AND A.number IS NOT NULL
            AND A.InstrGrdBkWgtDetailId NOT IN ( SELECT InstrGrdBkWgtDetailId
                                                 FROM   arGrdBkResults
                                                 WHERE  StuEnrollId = @stuEnrollId
                                                        AND clsSectionid IN ( SELECT    clsSectionid
                                                                              FROM      arClassSections
                                                                              WHERE     Reqid = d.reqid
                                                                                        AND Termid = d.TermId ) )
            AND B.EffectiveDate = (
                                    SELECT DISTINCT TOP 1
                                            A.EffectiveDate
                                    FROM    arGrdBkWeights A
                                           ,arClassSections B
                                    WHERE   A.ReqId = B.ReqId
                                            AND A.EffectiveDate <= B.StartDate
                                            AND B.ClsSectionId = d.ClsSectionId
                                    ORDER BY A.EffectiveDate DESC
                                  ); 




GO
