SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_AdvPortalSettings]
    @CampusId UNIQUEIDENTIFIER
   ,@chkSourceCategoryAll BIT
   ,@SourceCategoryId VARCHAR(50) = NULL
   ,@chkSourceTypeAll BIT
   ,@SourceTypeId VARCHAR(50) = NULL
   ,@chkAdmRepAll BIT
   ,@AdmRepId VARCHAR(50) = NULL
   ,@chkLeadStatusAll BIT
   ,@LeadStatusId VARCHAR(50) = NULL
   ,@chkPhoneType1All BIT
   ,@PhoneType1Id VARCHAR(50) = NULL
   ,@chkPhoneType2All BIT
   ,@PhoneType2Id VARCHAR(50) = NULL
   ,@chkAddTypeAll BIT
   ,@AddTypeId VARCHAR(50) = NULL
   ,@chkGenderAll BIT
   ,@GenderId VARCHAR(50) = NULL
   ,@chkPortalCountryAll BIT
   ,@PortalCountryId VARCHAR(50) = NULL
   ,@chkPortalContactEmailAll BIT
   ,@PortalContactEmail VARCHAR(50) = NULL
   ,@chkEmailSubjectAll BIT
   ,@EmailSubject VARCHAR(250)
   ,@chkEmailBodyAll BIT
   ,@EmailBody TEXT
AS
    BEGIN

        BEGIN TRY

            SET NOCOUNT ON;

            BEGIN

                BEGIN TRANSACTION;



/* SET SourceCategoryId according to the campus selection made by user */



                DECLARE @SourceCampuses TABLE
                    (
                     CampusId UNIQUEIDENTIFIER
                    );



                INSERT  INTO @SourceCampuses
                        (
                         CampusId
                        )
                        SELECT DISTINCT
                                CampusId
                        FROM    syCmpGrpCmps
                        WHERE   CampGrpId IN ( SELECT DISTINCT
                                                        CampGrpId
                                               FROM     syCmpGrpCmps
                                               WHERE    CampusId = @CampusId );

				 

                IF @chkSourceCategoryAll = 0
                    BEGIN

                        UPDATE  syCampuses
                        SET     SourceCategoryId = NULL
                               ,SourceCategoryAll = 0
                        WHERE   CampusId IN ( SELECT    *
                                              FROM      @SourceCampuses );



                        UPDATE  syCampuses
                        SET     SourceCategoryId = @SourceCategoryId
                               ,SourceCategoryAll = 0
                        WHERE   CampusId = @CampusId;

                    END;

                ELSE
                    BEGIN

                        UPDATE  syCampuses
                        SET     SourceCategoryId = @SourceCategoryId
                               ,SourceCategoryAll = 1
                        WHERE   CampusId IN ( SELECT    *
                                              FROM      @SourceCampuses );

                    END;



/* SET SourceTypeId according to the campus selection made by user */

	

                DECLARE @SourceTypeCampuses TABLE
                    (
                     CampusId UNIQUEIDENTIFIER
                    );



                INSERT  INTO @SourceTypeCampuses
                        (
                         CampusId
                        )
                        SELECT DISTINCT
                                CampusId
                        FROM    syCmpGrpCmps
                        WHERE   CampGrpId IN ( SELECT DISTINCT
                                                        CampGrpId
                                               FROM     syCmpGrpCmps
                                               WHERE    CampusId = @CampusId );		

	

                IF @chkSourceTypeAll = 0
                    BEGIN

                        UPDATE  syCampuses
                        SET     SourceTypeId = NULL
                               ,SourceTypeAll = 0
                        WHERE   CampusId IN ( SELECT    *
                                              FROM      @SourceTypeCampuses );



                        UPDATE  syCampuses
                        SET     SourceTypeId = @SourceTypeId
                               ,SourceTypeAll = 0
                        WHERE   CampusId = @CampusId;

                    END;

                ELSE
                    BEGIN

                        UPDATE  syCampuses
                        SET     SourceTypeId = @SourceTypeId
                               ,SourceTypeAll = 1
                        WHERE   CampusId IN ( SELECT    *
                                              FROM      @SourceTypeCampuses );

                    END;



/* SET AdmRepId according to the campus selection made by user */



                DECLARE @AdmRepCampuses TABLE
                    (
                     CampusId UNIQUEIDENTIFIER
                    );



                INSERT  INTO @AdmRepCampuses
                        (
                         CampusId
                        )
                        SELECT DISTINCT
                                CampusId
                        FROM    syCmpGrpCmps
                        WHERE   CampGrpId IN ( SELECT DISTINCT
                                                        CampGrpId
                                               FROM     syCmpGrpCmps
                                               WHERE    CampusId = @CampusId );		

	

                IF @chkAdmRepAll = 0
                    BEGIN

                        UPDATE  syCampuses
                        SET     AdmRepId = NULL
                               ,AdmRepAll = 0
                        WHERE   CampusId IN ( SELECT    *
                                              FROM      @AdmRepCampuses );



                        UPDATE  syCampuses
                        SET     AdmRepId = @AdmRepId
                               ,AdmRepAll = 0
                        WHERE   CampusId = @CampusId;

                    END;

                ELSE
                    BEGIN

                        UPDATE  syCampuses
                        SET     AdmRepId = @AdmRepId
                               ,AdmRepAll = 1
                        WHERE   CampusId IN ( SELECT    *
                                              FROM      @AdmRepCampuses );

                    END;



/* SET LeadStatusId according to the campus selection made by user */



                DECLARE @LeadStatusCampuses TABLE
                    (
                     CampusId UNIQUEIDENTIFIER
                    );



                INSERT  INTO @LeadStatusCampuses
                        (
                         CampusId
                        )
                        SELECT DISTINCT
                                CampusId
                        FROM    syCmpGrpCmps
                        WHERE   CampGrpId IN ( SELECT DISTINCT
                                                        CampGrpId
                                               FROM     syCmpGrpCmps
                                               WHERE    CampusId = @CampusId );	

                IF @chkLeadStatusAll = 0
                    BEGIN

                        UPDATE  syCampuses
                        SET     LeadStatusId = NULL
                               ,LeadStatusAll = 0
                        WHERE   CampusId IN ( SELECT    *
                                              FROM      @LeadStatusCampuses );



                        UPDATE  syCampuses
                        SET     LeadStatusId = @LeadStatusId
                               ,LeadStatusAll = 0
                        WHERE   CampusId = @CampusId;

                    END;

                ELSE
                    BEGIN

                        UPDATE  syCampuses
                        SET     LeadStatusId = @LeadStatusId
                               ,LeadStatusAll = 1
                        WHERE   CampusId IN ( SELECT    *
                                              FROM      @LeadStatusCampuses );

                    END;



/* SET PhoneType1Id according to the campus selection made by user */



                DECLARE @PhoneType1Campuses TABLE
                    (
                     CampusId UNIQUEIDENTIFIER
                    );



                INSERT  INTO @PhoneType1Campuses
                        (
                         CampusId
                        )
                        SELECT DISTINCT
                                CampusId
                        FROM    syCmpGrpCmps
                        WHERE   CampGrpId IN ( SELECT DISTINCT
                                                        CampGrpId
                                               FROM     syCmpGrpCmps
                                               WHERE    CampusId = @CampusId );	

                IF @chkPhoneType1All = 0
                    BEGIN

                        UPDATE  syCampuses
                        SET     PhoneType1Id = NULL
                               ,PhoneType1All = 0
                        WHERE   CampusId IN ( SELECT    *
                                              FROM      @PhoneType1Campuses );



                        UPDATE  syCampuses
                        SET     PhoneType1Id = @PhoneType1Id
                               ,PhoneType1All = 0
                        WHERE   CampusId = @CampusId;

                    END;

                ELSE
                    BEGIN

                        UPDATE  syCampuses
                        SET     PhoneType1Id = @PhoneType1Id
                               ,PhoneType1All = 1
                        WHERE   CampusId IN ( SELECT    *
                                              FROM      @PhoneType1Campuses );

                    END;



/* SET PhoneType2Id according to the campus selection made by user */



                DECLARE @PhoneType2Campuses TABLE
                    (
                     CampusId UNIQUEIDENTIFIER
                    );



                INSERT  INTO @PhoneType2Campuses
                        (
                         CampusId
                        )
                        SELECT DISTINCT
                                CampusId
                        FROM    syCmpGrpCmps
                        WHERE   CampGrpId IN ( SELECT DISTINCT
                                                        CampGrpId
                                               FROM     syCmpGrpCmps
                                               WHERE    CampusId = @CampusId );	

                IF @chkPhoneType2All = 0
                    BEGIN

                        UPDATE  syCampuses
                        SET     PhoneType2Id = NULL
                               ,PhoneType2All = 0
                        WHERE   CampusId IN ( SELECT    *
                                              FROM      @PhoneType2Campuses );



                        UPDATE  syCampuses
                        SET     PhoneType2Id = @PhoneType2Id
                               ,PhoneType2All = 0
                        WHERE   CampusId = @CampusId;

                    END;

                ELSE
                    BEGIN

                        UPDATE  syCampuses
                        SET     PhoneType2Id = @PhoneType2Id
                               ,PhoneType2All = 1
                        WHERE   CampusId IN ( SELECT    *
                                              FROM      @PhoneType2Campuses );

                    END;



/* SET AddTypeId according to the campus selection made by user */



                DECLARE @AddTypeIdCampuses TABLE
                    (
                     CampusId UNIQUEIDENTIFIER
                    );



                INSERT  INTO @AddTypeIdCampuses
                        (
                         CampusId
                        )
                        SELECT DISTINCT
                                CampusId
                        FROM    syCmpGrpCmps
                        WHERE   CampGrpId IN ( SELECT DISTINCT
                                                        CampGrpId
                                               FROM     syCmpGrpCmps
                                               WHERE    CampusId = @CampusId );	

                IF @chkAddTypeAll = 0
                    BEGIN

                        UPDATE  syCampuses
                        SET     AddTypeId = NULL
                               ,AddTypeAll = 0
                        WHERE   CampusId IN ( SELECT    *
                                              FROM      @AddTypeIdCampuses );



                        UPDATE  syCampuses
                        SET     AddTypeId = @AddTypeId
                               ,AddTypeAll = 0
                        WHERE   CampusId = @CampusId;

                    END;

                ELSE
                    BEGIN

                        UPDATE  syCampuses
                        SET     AddTypeId = @AddTypeId
                               ,AddTypeAll = 1
                        WHERE   CampusId IN ( SELECT    *
                                              FROM      @AddTypeIdCampuses );

                    END;



/* SET GenderId according to the campus selection made by user */



                DECLARE @GenderIdCampuses TABLE
                    (
                     CampusId UNIQUEIDENTIFIER
                    );



                INSERT  INTO @GenderIdCampuses
                        (
                         CampusId
                        )
                        SELECT DISTINCT
                                CampusId
                        FROM    syCmpGrpCmps
                        WHERE   CampGrpId IN ( SELECT DISTINCT
                                                        CampGrpId
                                               FROM     syCmpGrpCmps
                                               WHERE    CampusId = @CampusId );	

                IF @chkGenderAll = 0
                    BEGIN

                        UPDATE  syCampuses
                        SET     GenderId = NULL
                               ,GenderAll = 0
                        WHERE   CampusId IN ( SELECT    *
                                              FROM      @GenderIdCampuses );



                        UPDATE  syCampuses
                        SET     GenderId = @GenderId
                               ,GenderAll = 0
                        WHERE   CampusId = @CampusId;

                    END;

                ELSE
                    BEGIN

                        UPDATE  syCampuses
                        SET     GenderId = @GenderId
                               ,GenderAll = 1
                        WHERE   CampusId IN ( SELECT    *
                                              FROM      @GenderIdCampuses );

                    END;



/* SET PortalCountryId according to the campus selection made by user */



                DECLARE @PortalCountryIdCampuses TABLE
                    (
                     CampusId UNIQUEIDENTIFIER
                    );



                INSERT  INTO @PortalCountryIdCampuses
                        (
                         CampusId
                        )
                        SELECT DISTINCT
                                CampusId
                        FROM    syCmpGrpCmps
                        WHERE   CampGrpId IN ( SELECT DISTINCT
                                                        CampGrpId
                                               FROM     syCmpGrpCmps
                                               WHERE    CampusId = @CampusId );	

                IF @chkPortalCountryAll = 0
                    BEGIN

                        UPDATE  syCampuses
                        SET     PortalCountryId = NULL
                               ,PortalCountryAll = 0
                        WHERE   CampusId IN ( SELECT    *
                                              FROM      @PortalCountryIdCampuses );



                        UPDATE  syCampuses
                        SET     PortalCountryId = @PortalCountryId
                               ,PortalCountryAll = 0
                        WHERE   CampusId = @CampusId;

                    END;

                ELSE
                    BEGIN

                        UPDATE  syCampuses
                        SET     PortalCountryId = @PortalCountryId
                               ,PortalCountryAll = 1
                        WHERE   CampusId IN ( SELECT    *
                                              FROM      @PortalCountryIdCampuses );

                    END;



/* SET PortalContactEmail according to the campus selection made by user */



                DECLARE @PortalContactEmailCampuses TABLE
                    (
                     CampusId UNIQUEIDENTIFIER
                    );



                INSERT  INTO @PortalContactEmailCampuses
                        (
                         CampusId
                        )
                        SELECT DISTINCT
                                CampusId
                        FROM    syCmpGrpCmps
                        WHERE   CampGrpId IN ( SELECT DISTINCT
                                                        CampGrpId
                                               FROM     syCmpGrpCmps
                                               WHERE    CampusId = @CampusId );	

                IF @chkPortalContactEmailAll = 0
                    BEGIN

                        UPDATE  syCampuses
                        SET     PortalContactEmail = NULL
                               ,PortalContactEmailAll = 0
                        WHERE   CampusId IN ( SELECT    *
                                              FROM      @PortalContactEmailCampuses );



                        UPDATE  syCampuses
                        SET     PortalContactEmail = @PortalContactEmail
                               ,PortalCountryAll = 0
                        WHERE   CampusId = @CampusId;

                    END;

                ELSE
                    BEGIN

                        UPDATE  syCampuses
                        SET     PortalContactEmail = @PortalContactEmail
                               ,PortalContactEmailAll = 1
                        WHERE   CampusId IN ( SELECT    *
                                              FROM      @PortalContactEmailCampuses );

                    END;



/* SET EmailSubject according to the campus selection made by user */



                DECLARE @EmailSubjectCampuses TABLE
                    (
                     CampusId UNIQUEIDENTIFIER
                    );



                INSERT  INTO @EmailSubjectCampuses
                        (
                         CampusId
                        )
                        SELECT DISTINCT
                                CampusId
                        FROM    syCmpGrpCmps
                        WHERE   CampGrpId IN ( SELECT DISTINCT
                                                        CampGrpId
                                               FROM     syCmpGrpCmps
                                               WHERE    CampusId = @CampusId );	

                IF @chkEmailSubjectAll = 0
                    BEGIN

                        UPDATE  syCampuses
                        SET     EmailSubject = NULL
                               ,EmailSubjectAll = 0
                        WHERE   CampusId IN ( SELECT    *
                                              FROM      @EmailSubjectCampuses );



                        UPDATE  syCampuses
                        SET     EmailSubject = @EmailSubject
                               ,EmailSubjectAll = 0
                        WHERE   CampusId = @CampusId;

                    END;

                ELSE
                    BEGIN

                        UPDATE  syCampuses
                        SET     EmailSubject = @EmailSubject
                               ,EmailSubjectAll = 1
                        WHERE   CampusId IN ( SELECT    *
                                              FROM      @EmailSubjectCampuses );

                    END;



/* SET EmailBody according to the campus selection made by user */



                DECLARE @EmailBodyCampuses TABLE
                    (
                     CampusId UNIQUEIDENTIFIER
                    );



                INSERT  INTO @EmailBodyCampuses
                        (
                         CampusId
                        )
                        SELECT DISTINCT
                                CampusId
                        FROM    syCmpGrpCmps
                        WHERE   CampGrpId IN ( SELECT DISTINCT
                                                        CampGrpId
                                               FROM     syCmpGrpCmps
                                               WHERE    CampusId = @CampusId );	

                IF @chkEmailBodyAll = 0
                    BEGIN

                        UPDATE  syCampuses
                        SET     EmailBody = NULL
                               ,EmailBodyAll = 0
                        WHERE   CampusId IN ( SELECT    *
                                              FROM      @EmailBodyCampuses );



                        UPDATE  syCampuses
                        SET     EmailBody = @EmailBody
                               ,EmailBodyAll = 0
                        WHERE   CampusId = @CampusId;

                    END;

                ELSE
                    BEGIN

                        UPDATE  syCampuses
                        SET     EmailBody = @EmailBody
                               ,EmailBodyAll = 1
                        WHERE   CampusId IN ( SELECT    *
                                              FROM      @EmailBodyCampuses );

                    END;

                COMMIT TRANSACTION;

            END;

        END TRY
        BEGIN CATCH
            ROLLBACK TRANSACTION;
        END CATCH;	
    END;




GO
