SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[Sp_GetReqsForProgramVersion_Report]
    (
     @CampusId UNIQUEIDENTIFIER
    ,@PrgVerId UNIQUEIDENTIFIER
    ,@StatusId UNIQUEIDENTIFIER
    )
AS
    IF @StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
        BEGIN
            IF @PrgVerId = (
                             SELECT CAST(CAST(0 AS BINARY) AS UNIQUEIDENTIFIER)
                           )
                BEGIN
                    SELECT  *
                    FROM    arReqs
                    WHERE   arReqs.StatusId = @StatusId
                            AND arReqs.ReqTypeId = 1
                            AND arReqs.CampGrpId IN ( SELECT    CampGrpId
                                                      FROM      syCmpGrpCmps
                                                      WHERE     CampusId = @CampusId ); 
                END;
            ELSE
                BEGIN
                    SELECT  *
                    FROM    (
                              SELECT    arReqs.*
                              FROM      arReqs
                                       ,arProgVerDef
                              WHERE     arReqs.StatusId = @StatusId
                                        AND arReqs.ReqTypeId = 1
                                        AND arReqs.ReqId = arProgVerDef.ReqId
                                        AND arProgVerDef.PrgVerId = @PrgVerId
                                        AND arReqs.CampGrpId IN ( SELECT    CampGrpId
                                                                  FROM      syCmpGrpCmps
                                                                  WHERE     CampusId = @CampusId )
                              UNION
                              SELECT    T5.*
                              FROM      arReqs t1
                                       ,arProgVerDef t2
                                       ,arReqGrpDef t4
                                       ,arReqs T5
                              WHERE     t1.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
                                        AND T5.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
                                        AND t1.ReqId = t4.GrpId
                                        AND t1.ReqTypeId = 2
                                        AND t1.ReqId = t2.ReqId
                                        AND PrgVerId = @PrgVerId
                                        AND t1.CampGrpId IN ( SELECT    CampGrpId
                                                              FROM      syCmpGrpCmps
                                                              WHERE     CampusId = @CampusId )
                                        AND t4.ReqId = T5.ReqId
                            ) R
                    ORDER BY R.Descrip;
                END;
        END;
    ELSE
        BEGIN
            IF @PrgVerId = (
                             SELECT CAST(CAST(0 AS BINARY) AS UNIQUEIDENTIFIER)
                           )
                BEGIN
                    SELECT  *
                    FROM    arReqs
                    WHERE   arReqs.ReqTypeId = 1
                            AND arReqs.CampGrpId IN ( SELECT    CampGrpId
                                                      FROM      syCmpGrpCmps
                                                      WHERE     CampusId = @CampusId ); 
                END;
            ELSE
                BEGIN
                    SELECT  *
                    FROM    (
                              SELECT    arReqs.*
                              FROM      arReqs
                                       ,arProgVerDef
                              WHERE     arReqs.ReqTypeId = 1
                                        AND arReqs.ReqId = arProgVerDef.ReqId
                                        AND arProgVerDef.PrgVerId = @PrgVerId
                                        AND arReqs.CampGrpId IN ( SELECT    CampGrpId
                                                                  FROM      syCmpGrpCmps
                                                                  WHERE     CampusId = @CampusId )
                              UNION
                              SELECT    T5.*
                              FROM      arReqs t1
                                       ,arProgVerDef t2
                                       ,arReqGrpDef t4
                                       ,arReqs T5
                              WHERE     t1.ReqId = t4.GrpId
                                        AND t1.ReqTypeId = 2
                                        AND t1.ReqId = t2.ReqId
                                        AND PrgVerId = @PrgVerId
                                        AND t1.CampGrpId IN ( SELECT    CampGrpId
                                                              FROM      syCmpGrpCmps
                                                              WHERE     CampusId = @CampusId )
                                        AND t4.ReqId = T5.ReqId
                            ) R
                    ORDER BY R.Descrip;
                END;
        END;



GO
