SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[USP_GenerateStudentSeqId] ( @startSeqNumber INT )
AS
    BEGIN

        SET NOCOUNT ON; 

        DECLARE @stuId INT;
        IF EXISTS ( SELECT  *
                    FROM    syGenerateStudentSeq )
            BEGIN
                SET @stuId = (
                               SELECT   MAX(Student_SeqId) AS Student_SeqID
                               FROM     syGenerateStudentSeq
                             ) + 1;
            END;
        ELSE
            BEGIN
                SET @stuId = @startSeqNumber;
            END;

        INSERT  INTO syGenerateStudentSeq
                ( Student_SeqId,ModDate )
        VALUES  ( @stuId,GETDATE() );

        SELECT  @stuId AS SeqId;

    END;
GO
