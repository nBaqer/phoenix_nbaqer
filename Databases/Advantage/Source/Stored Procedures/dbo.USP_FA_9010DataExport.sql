SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		FAME Inc.
-- Create date: 1/7/2019
-- Description:	Generates data for 90/10 report export. Each sheet for excel export is returned as data table. Two tables returned, first one is sheet 1, second one sheet 2. 
--Referenced in :DataExport9010Service
-- =============================================
CREATE PROCEDURE [dbo].[USP_FA_9010DataExport]
    (
        @StuEnrollmentId UNIQUEIDENTIFIER
       ,@CampusIds VARCHAR(MAX)
       ,@FiscalYearStart DATETIME2
       ,@FiscalYearEnd DATETIME2
       ,@GroupIds VARCHAR(MAX)
       ,@ProgramIds VARCHAR(MAX)
       ,@ActivitiesConductedAmount DECIMAL(16, 2)
    )
AS
    BEGIN
        IF ( @CampusIds IS NULL )
            BEGIN
                SET @CampusIds = '';
            END;

        IF ( @GroupIds IS NULL )
            BEGIN
                SET @GroupIds = ''; -- default parameter value to empty
            END;

        IF ( @ProgramIds IS NULL )
            BEGIN
                SET @ProgramIds = ''; -- default parameter value to empty
            END;

        -- declaring filter variables
        DECLARE @tblCampusIds TABLE
            (
                CampusId VARCHAR(50)
            );

        DECLARE @tblGroupIds TABLE
            (
                GroupId VARCHAR(50)
            );

        DECLARE @tblProgramIds TABLE
            (
                ProgramId VARCHAR(50)
            );

        DECLARE @IsAllGroups BIT = 1;
        DECLARE @IsAllPrograms BIT = 1;
        DECLARE @IsAllStudents BIT = 1;

        INSERT INTO @tblCampusIds (
                                  CampusId
                                  )
                    (SELECT strval
                     FROM   dbo.SPLIT(@CampusIds) );

        INSERT INTO @tblGroupIds (
                                 GroupId
                                 )
                    (SELECT strval
                     FROM   dbo.SPLIT(@GroupIds) );

        INSERT INTO @tblProgramIds (
                                   ProgramId
                                   )
                    (SELECT strval
                     FROM   dbo.SPLIT(@ProgramIds) );

        IF (
           @StuEnrollmentId IS NOT NULL
           AND @StuEnrollmentId <> '00000000-0000-0000-0000-000000000000'
           ) --if not default guid or null, parameter was passed
            BEGIN
                SET @IsAllStudents = 0;
            END;

        IF NOT EXISTS (
                      SELECT TOP 1 GroupId
                      FROM   @tblGroupIds
                      WHERE  GroupId LIKE '%All%'
                      ) --if 'all' group is not found
            SET @IsAllGroups = 0;
        ELSE
            DELETE FROM @tblGroupIds;


        IF NOT EXISTS (
                      SELECT TOP 1 ProgramId
                      FROM   @tblProgramIds
                      WHERE  ProgramId LIKE '%All%'
                      ) --if 'all' program is not found
            SET @IsAllPrograms = 0;
        ELSE
            DELETE FROM @tblProgramIds;


        --************************************* BEGIN - List of enrollments********************************************
        CREATE TABLE #EnrollmentList
            (
                RowNumber INT
               ,FirstName VARCHAR(50)
               ,MiddleName VARCHAR(50)
               ,LastName VARCHAR(50)
               ,StudentNumber NVARCHAR(50)
               ,PrgVerDescrip VARCHAR(50)
               ,StuEnrollId UNIQUEIDENTIFIER
               ,IsTitleIVProgram BIT
            );

        -- student list is all the students that had a transaction within the fiscal year and filtered by parameters
        INSERT INTO #EnrollmentList
                    SELECT   ROW_NUMBER() OVER ( ORDER BY ( StudentNumber )) AS RowNumber
                            ,FirstName
                            ,MiddleName
                            ,LastName
                            ,adLeads.StudentNumber
                            ,PrgVerDescrip
                            ,arStuEnrollments.StuEnrollId
                            ,(
                             SELECT TOP 1 IsTitleIV
                             FROM   dbo.arCampusPrgVersions
                             WHERE  arCampusPrgVersions.PrgVerId = arStuEnrollments.PrgVerId
                             ) AS IsTitleIVProgram
                    FROM     dbo.arStuEnrollments
                    JOIN     dbo.adLeads ON adLeads.LeadId = arStuEnrollments.LeadId
                    JOIN     dbo.arPrgVersions ON arPrgVersions.PrgVerId = dbo.arStuEnrollments.PrgVerId
                    JOIN     (
                             SELECT   StuEnrollId
                             FROM     dbo.saTransactions
                             WHERE    TransDate >= @FiscalYearStart
                                      AND TransDate <= @FiscalYearEnd
                             GROUP BY StuEnrollId
                             ) saTransactions ON saTransactions.StuEnrollId = arStuEnrollments.StuEnrollId
                    WHERE    adLeads.CampusId IN (
                                                 SELECT CampusId
                                                 FROM   @tblCampusIds
                                                 )
                             AND (
                                 @IsAllPrograms = 1 -- filter programs if not all
                                 OR dbo.arPrgVersions.ProgId IN (
                                                                SELECT ProgramId
                                                                FROM   @tblProgramIds
                                                                )
                                 )
                             AND (
                                 @IsAllGroups = 1 -- filter student groups if not all 
                                 OR LeadgrpId IN (
                                                 SELECT GroupId
                                                 FROM   @tblGroupIds
                                                 )
                                 )
                             AND (
                                 @IsAllStudents = 1
                                 OR arStuEnrollments.StuEnrollId = @StuEnrollmentId
                                 )
                    ORDER BY StudentNumber;

        --************************************* END - List of students********************************************

        --************************************* 9010 Mapping Id Variables********************************************

        DECLARE @OutsideGrantId UNIQUEIDENTIFIER = (
                                                   SELECT AwardType9010Id
                                                   FROM   dbo.syAwardTypes9010
                                                   WHERE  Name = 'Outside Grant'
                                                   );
        DECLARE @JobTrainingGrantId UNIQUEIDENTIFIER = (
                                                       SELECT AwardType9010Id
                                                       FROM   dbo.syAwardTypes9010
                                                       WHERE  Name = 'Job Training Grant'
                                                       );
        DECLARE @EducationalSavingsId UNIQUEIDENTIFIER = (
                                                         SELECT AwardType9010Id
                                                         FROM   dbo.syAwardTypes9010
                                                         WHERE  Name = 'Educational Savings'
                                                         );
        DECLARE @SchoolGrantId UNIQUEIDENTIFIER = (
                                                  SELECT AwardType9010Id
                                                  FROM   dbo.syAwardTypes9010
                                                  WHERE  Name = 'School Grant'
                                                  );

        DECLARE @TuitionDiscount9010Id UNIQUEIDENTIFIER = (
                                                          SELECT AwardType9010Id
                                                          FROM   dbo.syAwardTypes9010
                                                          WHERE  Name = 'Tuition Discount'
                                                          );

        DECLARE @NoneAwardId UNIQUEIDENTIFIER = (
                                                SELECT AwardType9010Id
                                                FROM   dbo.syAwardTypes9010
                                                WHERE  Name = 'None'
                                                );

        DECLARE @SchoolLoanAwardId UNIQUEIDENTIFIER = (
                                                      SELECT AwardType9010Id
                                                      FROM   dbo.syAwardTypes9010
                                                      WHERE  Name = 'School Loan'
                                                      );

        DECLARE @SEOGScholarshipMatchAwardId UNIQUEIDENTIFIER = (
                                                                SELECT AwardType9010Id
                                                                FROM   dbo.syAwardTypes9010
                                                                WHERE  Name = 'SEOGScholarshipMatch'
                                                                );

        DECLARE @OutsideLoanAwardId UNIQUEIDENTIFIER = (
                                                       SELECT AwardType9010Id
                                                       FROM   dbo.syAwardTypes9010
                                                       WHERE  Name = 'Outside Loan'
                                                       );

        DECLARE @StudentPayment9010MappingId UNIQUEIDENTIFIER = (
                                                                SELECT AwardType9010Id
                                                                FROM   dbo.syAwardTypes9010
                                                                WHERE  Name = 'Student Payment'
                                                                );

        DECLARE @StudentStipend9010MappingId UNIQUEIDENTIFIER = (
                                                                SELECT AwardType9010Id
                                                                FROM   dbo.syAwardTypes9010
                                                                WHERE  Name = 'Student Stipend'
                                                                );

        DECLARE @RefundToStudent9010MappingId UNIQUEIDENTIFIER = (
                                                                 SELECT AwardType9010Id
                                                                 FROM   dbo.syAwardTypes9010
                                                                 WHERE  Name = 'Refund to Student'
                                                                 );


        --*************************************END 9010 Mapping Id Variables********************************************

        --*******************************************Column Calculations ***********************************************

        --@TitleIVCreditBalance
        CREATE TABLE #TitleIVCreditBalance
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,Amount DECIMAL(16, 2)
            );

        INSERT INTO #TitleIVCreditBalance -- negated instutional charge balance being used
                    SELECT StuEnrollId
                          ,0
                    FROM   #EnrollmentList;

        --@InstitutionalChargeBalance
        CREATE TABLE #InstitutionalChargeBalance
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,Amount DECIMAL(16, 2)
            );

        INSERT INTO #InstitutionalChargeBalance
                    SELECT StuEnrollId
                          ,(
                           SELECT (
                                  SELECT    ISNULL(SUM(TransAmount), 0) AS InstitutionalChargeBalance
                                  FROM      dbo.saTransactions
                                  LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                                  WHERE     StuEnrollId = [#EnrollmentList].StuEnrollId
                                            AND (
                                                (
                                                IsInstCharge = 1
                                                AND PaymentCodeId IS NULL
                                                )
                                                OR SysTransCodeId = 15
                                                )
                                            AND Voided = 0
                                            AND TransDate < @FiscalYearStart
                                  )
                                  - ((
                                     SELECT    ISNULL(SUM(TransAmount) * -1, 0) AS TitleIVFunds
                                     FROM      dbo.saTransactions
                                     LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                                     WHERE     StuEnrollId = [#EnrollmentList].StuEnrollId
                                               AND TitleIV = 1
                                               AND Voided = 0
                                               AND TransDate < @FiscalYearStart
                                     )
                                     + (
                                       SELECT    ISNULL(SUM(TransAmount) * -1, 0) AS FirstAppliedAmount
                                       FROM      dbo.saTransactions
                                       LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                                       JOIN      dbo.syAwardTypes9010Mapping ON syAwardTypes9010Mapping.FundSourceId = saFundSources.FundSourceId
                                       JOIN      dbo.syAwardTypes9010 ON syAwardTypes9010.AwardType9010Id = syAwardTypes9010Mapping.AwardType9010Id
                                       WHERE     StuEnrollId = [#EnrollmentList].StuEnrollId
                                                 AND Voided = 0
                                                 AND dbo.syAwardTypes9010.AwardType9010Id IN ( @SchoolGrantId, @OutsideGrantId, @JobTrainingGrantId
                                                                                              ,@EducationalSavingsId
                                                                                             )
                                                 AND TransDate < @FiscalYearStart
                                       ) + (
                                           SELECT    ISNULL( --add back tuition is counts here for previous year
                                                               SUM(TransAmount) * -1, 0
                                                           ) AS InstitutionalMaxChargeLimitThisYear
                                           FROM      dbo.saTransactions
                                           LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                                           JOIN      dbo.syAwardTypes9010Mapping ON syAwardTypes9010Mapping.FundSourceId = saFundSources.FundSourceId
                                           JOIN      dbo.syAwardTypes9010 ON syAwardTypes9010.AwardType9010Id = syAwardTypes9010Mapping.AwardType9010Id
                                           WHERE     StuEnrollId = [#EnrollmentList].StuEnrollId
                                                     AND Voided = 0
                                                     AND dbo.syAwardTypes9010.AwardType9010Id IN ( @TuitionDiscount9010Id )
                                                     AND TransDate < @FiscalYearStart
                                           )
                                    ) AS InstitutionalChargeBalance
                           ) AS Amount
                    FROM   #EnrollmentList;

        --@InstitutionalMaxChargeLimitThisYear
        CREATE TABLE #InstitutionalMaxChargeLimitThisYear
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,Amount DECIMAL(16, 2)
            );

        INSERT INTO #InstitutionalMaxChargeLimitThisYear
                    SELECT [#EnrollmentList].StuEnrollId
                          ,(
                           SELECT    ( ISNULL(SUM(TransAmount), 0))
                                     - (
                                       SELECT    ISNULL(SUM(TransAmount) * -1, 0) AS InstitutionalMaxChargeLimitThisYear
                                       FROM      dbo.saTransactions
                                       LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                                       LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                                       LEFT JOIN dbo.syAwardTypes9010Mapping ON syAwardTypes9010Mapping.FundSourceId = saFundSources.FundSourceId
                                       LEFT JOIN dbo.syAwardTypes9010 ON syAwardTypes9010.AwardType9010Id = syAwardTypes9010Mapping.AwardType9010Id
                                       WHERE     StuEnrollId = [#EnrollmentList].StuEnrollId
                                                 AND Voided = 0
                                                 AND dbo.syAwardTypes9010.AwardType9010Id IN ( @TuitionDiscount9010Id )
                                                 AND TransDate >= @FiscalYearStart
                                                 AND TransDate <= @FiscalYearEnd
                                                 AND dbo.syAwardTypes9010Mapping.CampusId = dbo.saTransactions.CampusId
                                       ) AS InstitutionalMaxChargeLimitThisYear
                           FROM      dbo.saTransactions
                           LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                           LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                           WHERE     StuEnrollId = [#EnrollmentList].StuEnrollId
                                     AND Voided = 0
                                     AND (
                                         (
                                         IsInstCharge = 1
                                         AND PaymentCodeId IS NULL
                                         )
                                         OR SysTransCodeId = 15
                                         )
                                     AND TransDate >= @FiscalYearStart
                                     AND TransDate <= @FiscalYearEnd
                           ) AS Amount
                    FROM   #EnrollmentList;

        --@TotalInstitutionalMaxChargeLimit
        CREATE TABLE #TotalInstitutionalMaxChargeLimit
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,ExcelFormula VARCHAR(200)
            );

        INSERT INTO #TotalInstitutionalMaxChargeLimit
                    SELECT StuEnrollId
                          ,( 'SUM(E' + LTRIM(RTRIM(STR(RowNumber + 1))) + ':F' + LTRIM(RTRIM(STR(RowNumber + 1))) + ')' ) AS ExcelFormula
                    FROM   #EnrollmentList;

        --Sub Loan calculation
        CREATE TABLE #SubLoan
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,Amount DECIMAL(16, 2)
            );

        INSERT INTO #SubLoan
                    SELECT [#EnrollmentList].StuEnrollId
                          ,(( CASE WHEN ( [#InstitutionalChargeBalance].Amount * -1 ) <= 0 THEN 0 -- if titleiv credit balance is less than or equal to 0 no carry over subloan
                                   WHEN ((
                                         SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) AS SubLoan -- if subloan in previous year <= titleivcreditbalance add carry over to subloan of this year
                                         FROM      dbo.saTransactions
                                         LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                                         LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                                         LEFT JOIN dbo.syAdvFundSources ON syAdvFundSources.AdvFundSourceId = saFundSources.AdvFundSourceId
                                         LEFT JOIN dbo.saRefunds ON saRefunds.TransactionId = saTransactions.TransactionId
                                         WHERE     StuEnrollId = [#EnrollmentList].StuEnrollId
                                                   AND RefundAmount IS NULL
                                                   AND TitleIV = 1
                                                   AND Voided = 0
                                                   AND Descrip IN ( 'DL - SUB', 'FFEL - SUB' )
                                                   AND TransDate < @FiscalYearStart
                                         ) <= ( [#InstitutionalChargeBalance].Amount * -1 )
                                        ) THEN (
                                               SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) AS SubLoan -- subloan in previous year <= titleivcreditbalance
                                               FROM      dbo.saTransactions
                                               LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                                               LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                                               LEFT JOIN dbo.syAdvFundSources ON syAdvFundSources.AdvFundSourceId = saFundSources.AdvFundSourceId
                                               LEFT JOIN dbo.saRefunds ON saRefunds.TransactionId = saTransactions.TransactionId
                                               WHERE     StuEnrollId = [#EnrollmentList].StuEnrollId
                                                         AND RefundAmount IS NULL
                                                         AND TitleIV = 1
                                                         AND Voided = 0
                                                         AND Descrip IN ( 'DL - SUB', 'FFEL - SUB' )
                                                         AND TransDate < @FiscalYearStart
                                               )
                                   ELSE ( [#InstitutionalChargeBalance].Amount * -1 )             -- title iv credit balance > 0 but less than subloan previous total, take all of credit iv credit balance as sub loan
                              END
                            ) + (
                                SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) AS SubLoan
                                FROM      dbo.saTransactions
                                LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                                LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                                LEFT JOIN dbo.syAdvFundSources ON syAdvFundSources.AdvFundSourceId = saFundSources.AdvFundSourceId
                                LEFT JOIN dbo.saRefunds ON saRefunds.TransactionId = saTransactions.TransactionId
                                WHERE     StuEnrollId = [#EnrollmentList].StuEnrollId
                                          AND RefundAmount IS NULL
                                          AND TitleIV = 1
                                          AND Voided = 0
                                          AND Descrip IN ( 'DL - SUB', 'FFEL - SUB' )
                                          AND TransDate >= @FiscalYearStart
                                          AND TransDate <= @FiscalYearEnd
                                )
                           ) AS Amount
                    FROM   #EnrollmentList
                    JOIN   #InstitutionalChargeBalance ON [#InstitutionalChargeBalance].StuEnrollId = [#EnrollmentList].StuEnrollId;


        --Unsub Loan calculation
        CREATE TABLE #UnSubLoan
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,Amount DECIMAL(16, 2)
            );

        INSERT INTO #UnSubLoan
                    SELECT [#EnrollmentList].StuEnrollId
                          ,(( CASE WHEN (( [#InstitutionalChargeBalance].Amount * -1 )
                                         - (
                                           SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) AS SubLoan -- subloan in previous year <= titleivcreditbalance
                                           FROM      dbo.saTransactions
                                           LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                                           LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                                           LEFT JOIN dbo.syAdvFundSources ON syAdvFundSources.AdvFundSourceId = saFundSources.AdvFundSourceId
                                           LEFT JOIN dbo.saRefunds ON saRefunds.TransactionId = saTransactions.TransactionId
                                           WHERE     StuEnrollId = [#EnrollmentList].StuEnrollId
                                                     AND RefundAmount IS NULL
                                                     AND TitleIV = 1
                                                     AND Voided = 0
                                                     AND Descrip IN ( 'DL - SUB', 'FFEL - SUB' )
                                                     AND TransDate < @FiscalYearStart
                                           )
                                        ) <= 0 THEN 0
                                   WHEN ((
                                         SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) -- Subloan + Unsubloan previous <= titleivcreditbalance then take full unsub
                                         FROM      dbo.saTransactions
                                         LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                                         LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                                         LEFT JOIN dbo.syAdvFundSources ON syAdvFundSources.AdvFundSourceId = saFundSources.AdvFundSourceId
                                         LEFT JOIN dbo.saRefunds ON saRefunds.TransactionId = saTransactions.TransactionId
                                         WHERE     StuEnrollId = [#EnrollmentList].StuEnrollId
                                                   AND RefundAmount IS NULL
                                                   AND TitleIV = 1
                                                   AND Voided = 0
                                                   AND Descrip IN ( 'DL - SUB', 'FFEL - SUB', 'DL - UNSUB', 'FFEL - UNSUB', 'DL – Additional UNSUB'
                                                                   ,'FFEL – Additional UNSUB'
                                                                  )
                                                   AND TransDate < @FiscalYearStart
                                         ) <= ( [#InstitutionalChargeBalance].Amount * -1 )
                                        ) THEN (
                                               SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) AS SubLoan
                                               FROM      dbo.saTransactions
                                               LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                                               LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                                               LEFT JOIN dbo.syAdvFundSources ON syAdvFundSources.AdvFundSourceId = saFundSources.AdvFundSourceId
                                               LEFT JOIN dbo.saRefunds ON saRefunds.TransactionId = saTransactions.TransactionId
                                               WHERE     StuEnrollId = [#EnrollmentList].StuEnrollId
                                                         AND RefundAmount IS NULL
                                                         AND TitleIV = 1
                                                         AND Voided = 0
                                                         AND Descrip IN ( 'DL - UNSUB', 'FFEL - UNSUB', 'DL – Additional UNSUB', 'FFEL – Additional UNSUB' )
                                                         AND TransDate < @FiscalYearStart
                                               )
                                   ELSE -- TitleIvCreditBalance - subloan  = amount of unsub to carry over
                          (( [#InstitutionalChargeBalance].Amount * -1 )
                           - (
                             SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) AS SubLoan -- subloan in previous year <= titleivcreditbalance
                             FROM      dbo.saTransactions
                             LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                             LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                             LEFT JOIN dbo.syAdvFundSources ON syAdvFundSources.AdvFundSourceId = saFundSources.AdvFundSourceId
                             LEFT JOIN dbo.saRefunds ON saRefunds.TransactionId = saTransactions.TransactionId
                             WHERE     StuEnrollId = [#EnrollmentList].StuEnrollId
                                       AND RefundAmount IS NULL
                                       AND TitleIV = 1
                                       AND Voided = 0
                                       AND Descrip IN ( 'DL - SUB', 'FFEL - SUB' )
                                       AND TransDate < @FiscalYearStart
                             )
                          ) -- title iv credit balance > 0 but less than subloan previous total, take all of credit iv credit balance as sub loan
                              END
                            ) + (
                                SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) AS SubLoan
                                FROM      dbo.saTransactions
                                LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                                LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                                LEFT JOIN dbo.syAdvFundSources ON syAdvFundSources.AdvFundSourceId = saFundSources.AdvFundSourceId
                                LEFT JOIN dbo.saRefunds ON saRefunds.TransactionId = saTransactions.TransactionId
                                WHERE     StuEnrollId = [#EnrollmentList].StuEnrollId
                                          AND RefundAmount IS NULL
                                          AND TitleIV = 1
                                          AND Voided = 0
                                          AND Descrip IN ( 'DL - UNSUB', 'FFEL - UNSUB', 'DL – Additional UNSUB', 'FFEL – Additional UNSUB' )
                                          AND TransDate >= @FiscalYearStart
                                          AND TransDate <= @FiscalYearEnd
                                )
                           ) AS Amount
                    FROM   #EnrollmentList
                    JOIN   #InstitutionalChargeBalance ON [#InstitutionalChargeBalance].StuEnrollId = [#EnrollmentList].StuEnrollId;

        --Plus calculation
        CREATE TABLE #Plus
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,Amount DECIMAL(16, 2)
            );

        INSERT INTO #Plus
                    SELECT [#EnrollmentList].StuEnrollId
                          ,(( CASE WHEN (( [#InstitutionalChargeBalance].Amount * -1 )
                                         - (
                                           SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) AS SubLoan -- subloan in previous year <= titleivcreditbalance
                                           FROM      dbo.saTransactions
                                           LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                                           LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                                           LEFT JOIN dbo.syAdvFundSources ON syAdvFundSources.AdvFundSourceId = saFundSources.AdvFundSourceId
                                           LEFT JOIN dbo.saRefunds ON saRefunds.TransactionId = saTransactions.TransactionId
                                           WHERE     StuEnrollId = [#EnrollmentList].StuEnrollId
                                                     AND RefundAmount IS NULL
                                                     AND TitleIV = 1
                                                     AND Voided = 0
                                                     AND Descrip IN ( 'DL - SUB', 'FFEL - SUB', 'DL - UNSUB', 'FFEL - UNSUB', 'DL – Additional UNSUB'
                                                                     ,'FFEL – Additional UNSUB'
                                                                    )
                                                     AND TransDate < @FiscalYearStart
                                           )
                                        ) <= 0 THEN 0
                                   WHEN ((
                                         SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) -- Subloan + Unsubloan + plus  <= titleivcreditbalance then take full plus
                                         FROM      dbo.saTransactions
                                         LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                                         LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                                         LEFT JOIN dbo.syAdvFundSources ON syAdvFundSources.AdvFundSourceId = saFundSources.AdvFundSourceId
                                         LEFT JOIN dbo.saRefunds ON saRefunds.TransactionId = saTransactions.TransactionId
                                         WHERE     StuEnrollId = [#EnrollmentList].StuEnrollId
                                                   AND RefundAmount IS NULL
                                                   AND TitleIV = 1
                                                   AND Voided = 0
                                                   AND Descrip IN ( 'DL - SUB', 'FFEL - SUB', 'DL - UNSUB', 'FFEL - UNSUB', 'DL – Additional UNSUB'
                                                                   ,'FFEL – Additional UNSUB', 'DL - PLUS', 'FFEL - PLUS'
                                                                  )
                                                   AND TransDate < @FiscalYearStart
                                         ) <= ( [#InstitutionalChargeBalance].Amount * -1 )
                                        ) THEN (
                                               SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) AS SubLoan
                                               FROM      dbo.saTransactions
                                               LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                                               LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                                               LEFT JOIN dbo.syAdvFundSources ON syAdvFundSources.AdvFundSourceId = saFundSources.AdvFundSourceId
                                               LEFT JOIN dbo.saRefunds ON saRefunds.TransactionId = saTransactions.TransactionId
                                               WHERE     StuEnrollId = [#EnrollmentList].StuEnrollId
                                                         AND RefundAmount IS NULL
                                                         AND TitleIV = 1
                                                         AND Voided = 0
                                                         AND Descrip IN ( 'DL - PLUS', 'FFEL - PLUS' )
                                                         AND TransDate < @FiscalYearStart
                                               )
                                   ELSE -- TitleIvCreditBalance - sub, unsub  = amount of unsub to carry over
                          (( [#InstitutionalChargeBalance].Amount * -1 )
                           - (
                             SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) AS SubLoan -- subloan in previous year <= titleivcreditbalance
                             FROM      dbo.saTransactions
                             LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                             LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                             LEFT JOIN dbo.syAdvFundSources ON syAdvFundSources.AdvFundSourceId = saFundSources.AdvFundSourceId
                             LEFT JOIN dbo.saRefunds ON saRefunds.TransactionId = saTransactions.TransactionId
                             WHERE     StuEnrollId = [#EnrollmentList].StuEnrollId
                                       AND RefundAmount IS NULL
                                       AND TitleIV = 1
                                       AND Voided = 0
                                       AND Descrip IN ( 'DL - SUB', 'FFEL - SUB', 'DL - UNSUB', 'FFEL - UNSUB', 'DL – Additional UNSUB', 'FFEL – Additional UNSUB' )
                                       AND TransDate < @FiscalYearStart
                             )
                          ) -- title iv credit balance > 0 but less than subloan + sunsub previous total, take all of credit iv credit balance as plus loan
                              END
                            ) + (
                                SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) AS SubLoan
                                FROM      dbo.saTransactions
                                LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                                LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                                LEFT JOIN dbo.syAdvFundSources ON syAdvFundSources.AdvFundSourceId = saFundSources.AdvFundSourceId
                                LEFT JOIN dbo.saRefunds ON saRefunds.TransactionId = saTransactions.TransactionId
                                WHERE     StuEnrollId = [#EnrollmentList].StuEnrollId
                                          AND RefundAmount IS NULL
                                          AND TitleIV = 1
                                          AND Voided = 0
                                          AND Descrip IN ( 'DL - PLUS', 'FFEL - PLUS' )
                                          AND TransDate >= @FiscalYearStart
                                          AND TransDate <= @FiscalYearEnd
                                )
                           ) AS Amount
                    FROM   #EnrollmentList
                    JOIN   #InstitutionalChargeBalance ON [#InstitutionalChargeBalance].StuEnrollId = [#EnrollmentList].StuEnrollId;


        --Pell grant calculation
        CREATE TABLE #PellGrant
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,Amount DECIMAL(16, 2)
            );

        INSERT INTO #PellGrant
                    SELECT [#EnrollmentList].StuEnrollId
                          ,(( CASE WHEN (( [#InstitutionalChargeBalance].Amount * -1 )
                                         - (
                                           SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) AS SubLoan
                                           FROM      dbo.saTransactions
                                           LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                                           LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                                           LEFT JOIN dbo.syAdvFundSources ON syAdvFundSources.AdvFundSourceId = saFundSources.AdvFundSourceId
                                           LEFT JOIN dbo.saRefunds ON saRefunds.TransactionId = saTransactions.TransactionId
                                           WHERE     StuEnrollId = [#EnrollmentList].StuEnrollId
                                                     AND RefundAmount IS NULL
                                                     AND TitleIV = 1
                                                     AND Voided = 0
                                                     AND Descrip IN ( 'DL - SUB', 'FFEL - SUB', 'DL - UNSUB', 'FFEL - UNSUB', 'DL – Additional UNSUB'
                                                                     ,'FFEL – Additional UNSUB', 'DL - PLUS', 'FFEL - PLUS'
                                                                    )
                                                     AND TransDate < @FiscalYearStart
                                           )
                                        ) <= 0 THEN 0
                                   WHEN ((
                                         SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) -- Subloan + Unsubloan + plus + pell  <= titleivcreditbalance then take full pell
                                         FROM      dbo.saTransactions
                                         LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                                         LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                                         LEFT JOIN dbo.syAdvFundSources ON syAdvFundSources.AdvFundSourceId = saFundSources.AdvFundSourceId
                                         LEFT JOIN dbo.saRefunds ON saRefunds.TransactionId = saTransactions.TransactionId
                                         WHERE     StuEnrollId = [#EnrollmentList].StuEnrollId
                                                   AND RefundAmount IS NULL
                                                   AND TitleIV = 1
                                                   AND Voided = 0
                                                   AND Descrip IN ( 'DL - SUB', 'FFEL - SUB', 'DL - UNSUB', 'FFEL - UNSUB', 'DL – Additional UNSUB'
                                                                   ,'FFEL – Additional UNSUB', 'DL - PLUS', 'FFEL - PLUS', 'PELL'
                                                                  )
                                                   AND TransDate < @FiscalYearStart
                                         ) <= ( [#InstitutionalChargeBalance].Amount * -1 )
                                        ) THEN (
                                               SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) AS SubLoan
                                               FROM      dbo.saTransactions
                                               LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                                               LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                                               LEFT JOIN dbo.syAdvFundSources ON syAdvFundSources.AdvFundSourceId = saFundSources.AdvFundSourceId
                                               LEFT JOIN dbo.saRefunds ON saRefunds.TransactionId = saTransactions.TransactionId
                                               WHERE     StuEnrollId = [#EnrollmentList].StuEnrollId
                                                         AND RefundAmount IS NULL
                                                         AND TitleIV = 1
                                                         AND Voided = 0
                                                         AND Descrip IN ( 'PELL' )
                                                         AND TransDate < @FiscalYearStart
                                               )
                                   ELSE -- TitleIvCreditBalance - sub, unsub, plus  = amount of pell to carry over
                          (( [#InstitutionalChargeBalance].Amount * -1 )
                           - (
                             SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) AS SubLoan
                             FROM      dbo.saTransactions
                             LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                             LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                             LEFT JOIN dbo.syAdvFundSources ON syAdvFundSources.AdvFundSourceId = saFundSources.AdvFundSourceId
                             LEFT JOIN dbo.saRefunds ON saRefunds.TransactionId = saTransactions.TransactionId
                             WHERE     StuEnrollId = [#EnrollmentList].StuEnrollId
                                       AND RefundAmount IS NULL
                                       AND TitleIV = 1
                                       AND Voided = 0
                                       AND Descrip IN ( 'DL - SUB', 'FFEL - SUB', 'DL - UNSUB', 'FFEL - UNSUB', 'DL – Additional UNSUB', 'FFEL – Additional UNSUB'
                                                       ,'DL - PLUS', 'FFEL - PLUS'
                                                      )
                                       AND TransDate < @FiscalYearStart
                             )
                          )
                              END
                            ) + (
                                SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) AS SubLoan
                                FROM      dbo.saTransactions
                                LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                                LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                                LEFT JOIN dbo.syAdvFundSources ON syAdvFundSources.AdvFundSourceId = saFundSources.AdvFundSourceId
                                LEFT JOIN dbo.saRefunds ON saRefunds.TransactionId = saTransactions.TransactionId
                                WHERE     StuEnrollId = [#EnrollmentList].StuEnrollId
                                          AND TitleIV = 1
                                          AND Voided = 0
                                          AND RefundAmount IS NULL
                                          AND Descrip IN ( 'PELL' )
                                          AND TransDate >= @FiscalYearStart
                                          AND TransDate <= @FiscalYearEnd
                                )
                           ) AS Amount
                    FROM   #EnrollmentList
                    JOIN   #InstitutionalChargeBalance ON [#InstitutionalChargeBalance].StuEnrollId = [#EnrollmentList].StuEnrollId;

        --FSEOG  calculation
        CREATE TABLE #FSEOG
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,Amount DECIMAL(16, 2)
            );

        INSERT INTO #FSEOG
                    SELECT [#EnrollmentList].StuEnrollId
                          ,(( CASE WHEN (( [#InstitutionalChargeBalance].Amount * -1 )
                                         - (
                                           SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) AS SubLoan
                                           FROM      dbo.saTransactions
                                           LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                                           LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                                           LEFT JOIN dbo.syAdvFundSources ON syAdvFundSources.AdvFundSourceId = saFundSources.AdvFundSourceId
                                           LEFT JOIN dbo.saRefunds ON saRefunds.TransactionId = saTransactions.TransactionId
                                           WHERE     StuEnrollId = [#EnrollmentList].StuEnrollId
                                                     AND RefundAmount IS NULL
                                                     AND TitleIV = 1
                                                     AND Voided = 0
                                                     AND Descrip IN ( 'DL - SUB', 'FFEL - SUB', 'DL - UNSUB', 'FFEL - UNSUB', 'DL – Additional UNSUB'
                                                                     ,'FFEL – Additional UNSUB', 'DL - PLUS', 'FFEL - PLUS', 'PELL'
                                                                    )
                                                     AND TransDate < @FiscalYearStart
                                           )
                                        ) <= 0 THEN 0
                                   WHEN ((
                                         SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) -- Subloan + Unsubloan + plus + pell + SEOG  <= titleivcreditbalance then take full SEOG
                                         FROM      dbo.saTransactions
                                         LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                                         LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                                         LEFT JOIN dbo.syAdvFundSources ON syAdvFundSources.AdvFundSourceId = saFundSources.AdvFundSourceId
                                         LEFT JOIN dbo.saRefunds ON saRefunds.TransactionId = saTransactions.TransactionId
                                         WHERE     StuEnrollId = [#EnrollmentList].StuEnrollId
                                                   AND RefundAmount IS NULL
                                                   AND TitleIV = 1
                                                   AND Voided = 0
                                                   AND Descrip IN ( 'DL - SUB', 'FFEL - SUB', 'DL - UNSUB', 'FFEL - UNSUB', 'DL – Additional UNSUB'
                                                                   ,'FFEL – Additional UNSUB', 'DL - PLUS', 'FFEL - PLUS', 'PELL', 'SEOG'
                                                                  )
                                                   AND TransDate < @FiscalYearStart
                                         ) <= ( [#InstitutionalChargeBalance].Amount * -1 )
                                        ) THEN (
                                               SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) AS SubLoan
                                               FROM      dbo.saTransactions
                                               LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                                               LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                                               LEFT JOIN dbo.syAdvFundSources ON syAdvFundSources.AdvFundSourceId = saFundSources.AdvFundSourceId
                                               LEFT JOIN dbo.saRefunds ON saRefunds.TransactionId = saTransactions.TransactionId
                                               WHERE     StuEnrollId = [#EnrollmentList].StuEnrollId
                                                         AND RefundAmount IS NULL
                                                         AND TitleIV = 1
                                                         AND Voided = 0
                                                         AND Descrip IN ( 'SEOG' )
                                                         AND TransDate < @FiscalYearStart
                                               )
                                   ELSE -- TitleIvCreditBalance - SUM(sub, unsub, plus, pell)  = amount of SEOG to carry over
                          (( [#InstitutionalChargeBalance].Amount * -1 )
                           - (
                             SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) AS SubLoan
                             FROM      dbo.saTransactions
                             LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                             LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                             LEFT JOIN dbo.syAdvFundSources ON syAdvFundSources.AdvFundSourceId = saFundSources.AdvFundSourceId
                             LEFT JOIN dbo.saRefunds ON saRefunds.TransactionId = saTransactions.TransactionId
                             WHERE     StuEnrollId = [#EnrollmentList].StuEnrollId
                                       AND RefundAmount IS NULL
                                       AND TitleIV = 1
                                       AND Voided = 0
                                       AND Descrip IN ( 'DL - SUB', 'FFEL - SUB', 'DL - UNSUB', 'FFEL - UNSUB', 'DL – Additional UNSUB', 'FFEL – Additional UNSUB'
                                                       ,'DL - PLUS', 'FFEL - PLUS', 'PELL'
                                                      )
                                       AND TransDate < @FiscalYearStart
                             )
                          )
                              END
                            ) + (
                                SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) AS SubLoan
                                FROM      dbo.saTransactions
                                LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                                LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                                LEFT JOIN dbo.syAdvFundSources ON syAdvFundSources.AdvFundSourceId = saFundSources.AdvFundSourceId
                                LEFT JOIN dbo.saRefunds ON saRefunds.TransactionId = saTransactions.TransactionId
                                WHERE     StuEnrollId = [#EnrollmentList].StuEnrollId
                                          AND RefundAmount IS NULL
                                          AND TitleIV = 1
                                          AND Voided = 0
                                          AND Descrip IN ( 'SEOG' )
                                          AND TransDate >= @FiscalYearStart
                                          AND TransDate <= @FiscalYearEnd
                                )
                           ) AS Amount
                    FROM   #EnrollmentList
                    JOIN   #InstitutionalChargeBalance ON [#InstitutionalChargeBalance].StuEnrollId = [#EnrollmentList].StuEnrollId;

        --FWS  calculation
        CREATE TABLE #FWS
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,Amount DECIMAL(16, 2)
            );

        INSERT INTO #FWS
                    SELECT [#EnrollmentList].StuEnrollId
                          ,(( CASE WHEN (( [#InstitutionalChargeBalance].Amount * -1 )
                                         - (
                                           SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) AS SubLoan
                                           FROM      dbo.saTransactions
                                           LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                                           LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                                           LEFT JOIN dbo.syAdvFundSources ON syAdvFundSources.AdvFundSourceId = saFundSources.AdvFundSourceId
                                           LEFT JOIN dbo.saRefunds ON saRefunds.TransactionId = saTransactions.TransactionId
                                           WHERE     StuEnrollId = [#EnrollmentList].StuEnrollId
                                                     AND RefundAmount IS NULL
                                                     AND TitleIV = 1
                                                     AND Voided = 0
                                                     AND Descrip IN ( 'DL - SUB', 'FFEL - SUB', 'DL - UNSUB', 'FFEL - UNSUB', 'DL – Additional UNSUB'
                                                                     ,'FFEL – Additional UNSUB', 'DL - PLUS', 'FFEL - PLUS', 'PELL', 'SEOG'
                                                                    )
                                                     AND TransDate < @FiscalYearStart
                                           )
                                        ) <= 0 THEN 0
                                   WHEN ((
                                         SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) -- SUM(Subloan + Unsubloan + plus + pell + SEOG + FWS)  <= titleivcreditbalance then take full FWS
                                         FROM      dbo.saTransactions
                                         LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                                         LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                                         LEFT JOIN dbo.syAdvFundSources ON syAdvFundSources.AdvFundSourceId = saFundSources.AdvFundSourceId
                                         LEFT JOIN dbo.saRefunds ON saRefunds.TransactionId = saTransactions.TransactionId
                                         WHERE     StuEnrollId = [#EnrollmentList].StuEnrollId
                                                   AND RefundAmount IS NULL
                                                   AND TitleIV = 1
                                                   AND Voided = 0
                                                   AND Descrip IN ( 'DL - SUB', 'FFEL - SUB', 'DL - UNSUB', 'FFEL - UNSUB', 'DL – Additional UNSUB'
                                                                   ,'FFEL – Additional UNSUB', 'DL - PLUS', 'FFEL - PLUS', 'PELL', 'SEOG', 'FWS'
                                                                  )
                                                   AND TransDate < @FiscalYearStart
                                         ) <= ( [#InstitutionalChargeBalance].Amount * -1 )
                                        ) THEN (
                                               SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) AS SubLoan
                                               FROM      dbo.saTransactions
                                               LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                                               LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                                               LEFT JOIN dbo.syAdvFundSources ON syAdvFundSources.AdvFundSourceId = saFundSources.AdvFundSourceId
                                               LEFT JOIN dbo.saRefunds ON saRefunds.TransactionId = saTransactions.TransactionId
                                               WHERE     StuEnrollId = [#EnrollmentList].StuEnrollId
                                                         AND RefundAmount IS NULL
                                                         AND TitleIV = 1
                                                         AND Voided = 0
                                                         AND Descrip IN ( 'FWS' )
                                                         AND TransDate < @FiscalYearStart
                                               )
                                   ELSE -- TitleIvCreditBalance - SUM(sub, unsub, plus, pell, SEOG)  = amount of FWS to carry over
                          (( [#InstitutionalChargeBalance].Amount * -1 )
                           - (
                             SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) AS SubLoan
                             FROM      dbo.saTransactions
                             LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                             LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                             LEFT JOIN dbo.syAdvFundSources ON syAdvFundSources.AdvFundSourceId = saFundSources.AdvFundSourceId
                             LEFT JOIN dbo.saRefunds ON saRefunds.TransactionId = saTransactions.TransactionId
                             WHERE     StuEnrollId = [#EnrollmentList].StuEnrollId
                                       AND RefundAmount IS NULL
                                       AND TitleIV = 1
                                       AND Voided = 0
                                       AND Descrip IN ( 'DL - SUB', 'FFEL - SUB', 'DL - UNSUB', 'FFEL - UNSUB', 'DL – Additional UNSUB', 'FFEL – Additional UNSUB'
                                                       ,'DL - PLUS', 'FFEL - PLUS', 'PELL', 'SEOG'
                                                      )
                                       AND TransDate < @FiscalYearStart
                             )
                          )
                              END
                            ) + (
                                SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) AS SubLoan
                                FROM      dbo.saTransactions
                                LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                                LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                                LEFT JOIN dbo.syAdvFundSources ON syAdvFundSources.AdvFundSourceId = saFundSources.AdvFundSourceId
                                LEFT JOIN dbo.saRefunds ON saRefunds.TransactionId = saTransactions.TransactionId
                                WHERE     StuEnrollId = [#EnrollmentList].StuEnrollId
                                          AND RefundAmount IS NULL
                                          AND TitleIV = 1
                                          AND Voided = 0
                                          AND Descrip IN ( 'FWS' )
                                          AND TransDate >= @FiscalYearStart
                                          AND TransDate <= @FiscalYearEnd
                                )
                           ) AS Amount
                    FROM   #EnrollmentList
                    JOIN   #InstitutionalChargeBalance ON [#InstitutionalChargeBalance].StuEnrollId = [#EnrollmentList].StuEnrollId;

        --Title IV Amt Disbursed  calculation
        CREATE TABLE #TitleIVAmtDisbursed
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,ExcelFormula VARCHAR(200)
            );

        INSERT INTO #TitleIVAmtDisbursed
                    SELECT StuEnrollId
                          ,( 'SUM(H' + LTRIM(RTRIM(STR(RowNumber + 1))) + ':M' + LTRIM(RTRIM(STR(RowNumber + 1))) + ')' ) AS ExcelFormula
                    FROM   #EnrollmentList;

        --@AdjSubLoan  calculation
        CREATE TABLE #AdjSubLoan
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,Amount DECIMAL(16, 2)
            );

        --@Adj Ubsub Loan  calculation
        CREATE TABLE #AdjUnSubLoan
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,Amount DECIMAL(16, 2)
            );


        --@Adj Plus Loan  calculation
        CREATE TABLE #AdjPlus
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,Amount DECIMAL(16, 2)
            );

        --@Adj Pell grant  calculation
        CREATE TABLE #AdjPellGrant
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,Amount DECIMAL(16, 2)
            );

        --@Ad FSEOG  calculation
        CREATE TABLE #AdjFSEOG
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,Amount DECIMAL(16, 2)
            );

        INSERT INTO #AdjFSEOG
                    SELECT StuEnrollId
                          ,(
                           SELECT    CASE WHEN (((
                                                 SELECT TOP 1 FSEOGMatchType
                                                 FROM   dbo.syCampuses
                                                 WHERE  CampusId IN (
                                                                    SELECT CampusId
                                                                    FROM   @tblCampusIds
                                                                    )
                                                 ) = 'C'
                                                )
                                               ) THEN ISNULL(( SUM(TransAmount) * -.75 ), 0)
                                          ELSE ISNULL(( SUM(TransAmount) * -1 ), 0)
                                     END AS Amount
                           FROM      dbo.saTransactions
                           LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                           LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                           LEFT JOIN dbo.syAdvFundSources ON syAdvFundSources.AdvFundSourceId = saFundSources.AdvFundSourceId
                           LEFT JOIN dbo.saRefunds ON saRefunds.TransactionId = saTransactions.TransactionId
                           WHERE     RefundTypeId IS NULL
                                     AND saTransactions.StuEnrollId = [#EnrollmentList].StuEnrollId
                                     AND TitleIV = 1
                                     AND Voided = 0
                                     AND Descrip IN ( 'SEOG' )
                                     AND TransDate >= @FiscalYearStart
                                     AND TransDate <= @FiscalYearEnd
                           ) AS Amount
                    FROM   #EnrollmentList;

        --@AdjFWS calculation
        CREATE TABLE #AdjFWS
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,Amount DECIMAL(16, 2)
            );

        --@TitleIVAmtAdjusted calculation
        CREATE TABLE #TitleIVAmtAdjusted
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,ExcelFormula VARCHAR(200)
            );

        INSERT INTO #TitleIVAmtAdjusted
                    SELECT StuEnrollId
                          ,( 'SUM(O' + LTRIM(RTRIM(STR(RowNumber + 1))) + ':T' + LTRIM(RTRIM(STR(RowNumber + 1))) + ')' ) AS ExcelFormula
                    FROM   #EnrollmentList;

        --@RevenueAdjustment calculation
        CREATE TABLE #RevenueAdjustment
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,ExcelFormula VARCHAR(200)
            );

        INSERT INTO #RevenueAdjustment
                    SELECT StuEnrollId
                          ,( 'IF((U' + LTRIM(RTRIM(STR(RowNumber + 1))) + '-W' + LTRIM(RTRIM(STR(RowNumber + 1))) + '>(G' + LTRIM(RTRIM(STR(RowNumber + 1)))
                             + '-AJ' + LTRIM(RTRIM(STR(RowNumber + 1))) + ')),U' + LTRIM(RTRIM(STR(RowNumber + 1))) + '-(G' + LTRIM(RTRIM(STR(RowNumber + 1)))
                             + '-AJ' + LTRIM(RTRIM(STR(RowNumber + 1))) + ')-W' + LTRIM(RTRIM(STR(RowNumber + 1))) + ',0)'
                           ) AS ExcelFormula
                    FROM   #EnrollmentList;

        --@TitleIVRefunds calculation
        CREATE TABLE #TitleIVRefunds
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,Amount DECIMAL(16, 2)
            );

        INSERT INTO #TitleIVRefunds
                    SELECT StuEnrollId
                          ,(
                           SELECT    ISNULL(( SUM(TransAmount)), 0) AS TitleIVRefunds
                           FROM      dbo.saTransactions
                           LEFT JOIN (
                                     SELECT    ( CASE WHEN saTransactions.FundSourceId IS NULL THEN saRefunds.FundSourceId
                                                      ELSE saTransactions.FundSourceId
                                                 END
                                               ) AS FundSourceId
                                              ,saTransactions.TransactionId
                                              ,TransCodeId
                                     FROM      dbo.saTransactions
                                     LEFT JOIN dbo.saRefunds ON saRefunds.TransactionId = saTransactions.TransactionId
                                     ) fundSourceTransactionIds ON fundSourceTransactionIds.TransactionId = saTransactions.TransactionId
                           LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = fundSourceTransactionIds.FundSourceId
                           LEFT JOIN dbo.saRefunds ON saRefunds.TransactionId = fundSourceTransactionIds.TransactionId
                           WHERE     StuEnrollId = [#EnrollmentList].StuEnrollId
                                     AND TitleIV = 1
                                     AND RefundTypeId IS NOT NULL
                                     AND Voided = 0
                                     AND TransDate >= @FiscalYearStart
                                     AND TransDate <= @FiscalYearEnd
                           ) AS Amount
                    FROM   #EnrollmentList;

        --@AdjTitleIVRevenue calculation
        CREATE TABLE #AdjTitleIVRevenue
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,ExcelFormula VARCHAR(200)
            );

        INSERT INTO #AdjTitleIVRevenue
                    SELECT StuEnrollId
                          ,'IF(U' + LTRIM(RTRIM(STR(RowNumber + 1))) + '-V' + LTRIM(RTRIM(STR(RowNumber + 1))) + '-W' + LTRIM(RTRIM(STR(RowNumber + 1)))
                           + '>0,U' + LTRIM(RTRIM(STR(RowNumber + 1))) + '-V' + LTRIM(RTRIM(STR(RowNumber + 1))) + '-W' + LTRIM(RTRIM(STR(RowNumber + 1)))
                           + ',0)' AS ExcelFormula
                    FROM   #EnrollmentList;


        --@OutsideGrant calculation
        CREATE TABLE #OutsideGrant
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,Amount DECIMAL(16, 2)
            );

        INSERT INTO #OutsideGrant
                    SELECT StuEnrollId
                          ,(
                           SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) AS Amount
                           FROM      dbo.saTransactions
                           LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                           LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                           LEFT JOIN dbo.syAwardTypes9010Mapping ON syAwardTypes9010Mapping.FundSourceId = saFundSources.FundSourceId
                           WHERE     Voided = 0
                                     AND saTransactions.StuEnrollId = [#EnrollmentList].StuEnrollId
                                     AND AwardType9010Id = @OutsideGrantId
                                     AND TransDate >= @FiscalYearStart
                                     AND TransDate <= @FiscalYearEnd
                                     AND dbo.syAwardTypes9010Mapping.CampusId = dbo.saTransactions.CampusId
                           ) AS Amount
                    FROM   #EnrollmentList;


        --@JobTrainingGrant calculation
        CREATE TABLE #JobTrainingGrant
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,Amount DECIMAL(16, 2)
            );

        INSERT INTO #JobTrainingGrant
                    SELECT StuEnrollId
                          ,(
                           SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) AS Amount
                           FROM      dbo.saTransactions
                           LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                           LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                           LEFT JOIN dbo.syAwardTypes9010Mapping ON syAwardTypes9010Mapping.FundSourceId = saFundSources.FundSourceId
                           WHERE     Voided = 0
                                     AND saTransactions.StuEnrollId = [#EnrollmentList].StuEnrollId
                                     AND AwardType9010Id = @JobTrainingGrantId
                                     AND TransDate >= @FiscalYearStart
                                     AND TransDate <= @FiscalYearEnd
                                     AND dbo.syAwardTypes9010Mapping.CampusId = dbo.saTransactions.CampusId
                           ) AS Amount
                    FROM   #EnrollmentList;




        --@EducationSavingPlan calculation
        CREATE TABLE #EducationSavingPlan
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,Amount DECIMAL(16, 2)
            );

        INSERT INTO #EducationSavingPlan
                    SELECT StuEnrollId
                          ,(
                           SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) AS Amount
                           FROM      dbo.saTransactions
                           LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                           LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                           LEFT JOIN dbo.syAwardTypes9010Mapping ON syAwardTypes9010Mapping.FundSourceId = saFundSources.FundSourceId
                           WHERE     Voided = 0
                                     AND saTransactions.StuEnrollId = [#EnrollmentList].StuEnrollId
                                     AND AwardType9010Id = @EducationalSavingsId
                                     AND TransDate >= @FiscalYearStart
                                     AND TransDate <= @FiscalYearEnd
                                     AND dbo.syAwardTypes9010Mapping.CampusId = dbo.saTransactions.CampusId
                           ) AS Amount
                    FROM   #EnrollmentList;

        --@SchoolGrant calculation
        CREATE TABLE #SchoolGrant
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,Amount DECIMAL(16, 2)
            );

        INSERT INTO #SchoolGrant
                    SELECT StuEnrollId
                          ,(
                           SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) AS Amount
                           FROM      dbo.saTransactions
                           LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                           LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                           LEFT JOIN dbo.syAwardTypes9010Mapping ON syAwardTypes9010Mapping.FundSourceId = saFundSources.FundSourceId
                           WHERE     Voided = 0
                                     AND saTransactions.StuEnrollId = [#EnrollmentList].StuEnrollId
                                     AND AwardType9010Id = @SchoolGrantId
                                     AND TransDate >= @FiscalYearStart
                                     AND TransDate <= @FiscalYearEnd
                                     AND dbo.syAwardTypes9010Mapping.CampusId = dbo.saTransactions.CampusId
                           )
                    FROM   #EnrollmentList;


        --@FirstAppliedFund calculation
        CREATE TABLE #FirstAppliedFund
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,ExcelFormula VARCHAR(200)
            );

        INSERT INTO #FirstAppliedFund
                    SELECT StuEnrollId
                          ,( 'SUM(Y' + LTRIM(RTRIM(STR(RowNumber + 1))) + ':AB' + LTRIM(RTRIM(STR(RowNumber + 1))) + ')' ) AS ExcelFormula
                    FROM   #EnrollmentList;

        --@StuPayandOutsideLn calculation
        CREATE TABLE #StuPayandOutsideLn
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,Amount DECIMAL(16, 2)
            );

        INSERT INTO #StuPayandOutsideLn -- All student payments(anything with 90/10 mapping for student payment, if no fund source systranscodeid 13) - (RefundsToStudents + StudentStipdends
                    SELECT [#EnrollmentList].StuEnrollId
                          ,((
                            SELECT ISNULL(( SUM(TransAmount)), 0) * -1
                            FROM   dbo.saTransactions
                            WHERE  StuEnrollId = [#EnrollmentList].StuEnrollId
                                   AND TransDate >= @FiscalYearStart
                                   AND TransDate <= @FiscalYearEnd
                                   AND Voided = 0
                                   AND (
                                       (
                                       saTransactions.FundSourceId IS NOT NULL
                                       AND saTransactions.FundSourceId IN (
                                                                          SELECT saFundSources.FundSourceId
                                                                          FROM   dbo.saFundSources
                                                                          JOIN   dbo.syAwardTypes9010Mapping ON syAwardTypes9010Mapping.FundSourceId = saFundSources.FundSourceId
                                                                          JOIN   dbo.syAwardTypes9010 ON syAwardTypes9010.AwardType9010Id = syAwardTypes9010Mapping.AwardType9010Id
                                                                          WHERE  syAwardTypes9010.AwardType9010Id IN ( @OutsideLoanAwardId
                                                                                                                      ,@StudentPayment9010MappingId
                                                                                                                      ,@SchoolLoanAwardId
                                                                                                                     )
                                                                                 AND dbo.syAwardTypes9010Mapping.CampusId = dbo.saTransactions.CampusId
                                                                          )
                                       )
                                       OR (
                                          saTransactions.FundSourceId IS NULL
                                          AND saTransactions.TransCodeId IN (
                                                                            SELECT saTransCodes.TransCodeId
                                                                            FROM   dbo.saTransCodes
                                                                            JOIN   dbo.syAwardTypes9010Mapping ON syAwardTypes9010Mapping.TransCodeId = saTransCodes.TransCodeId
                                                                            JOIN   dbo.syAwardTypes9010 ON syAwardTypes9010.AwardType9010Id = syAwardTypes9010Mapping.AwardType9010Id
                                                                            WHERE  syAwardTypes9010.AwardType9010Id = @StudentPayment9010MappingId
                                                                                   AND dbo.syAwardTypes9010Mapping.CampusId = dbo.saTransactions.CampusId
                                                                            )
                                          )
                                       )
                            )
                            - (
                              SELECT    ISNULL(( SUM(TransAmount)), 0)
                              FROM      dbo.saTransactions
                              LEFT JOIN (
                                        SELECT    ( CASE WHEN saTransactions.FundSourceId IS NULL THEN saRefunds.FundSourceId
                                                         ELSE saTransactions.FundSourceId
                                                    END
                                                  ) AS FundSourceId
                                                 ,saTransactions.TransactionId
                                                 ,TransCodeId
                                        FROM      dbo.saTransactions
                                        LEFT JOIN dbo.saRefunds ON saRefunds.TransactionId = saTransactions.TransactionId
                                        ) fundSourceTransactionIds ON fundSourceTransactionIds.TransactionId = saTransactions.TransactionId
                              WHERE     StuEnrollId = [#EnrollmentList].StuEnrollId
                                        AND TransDate >= @FiscalYearStart
                                        AND TransDate <= @FiscalYearEnd
                                        AND Voided = 0
                                        AND (
                                            (
                                            fundSourceTransactionIds.FundSourceId IS NOT NULL
                                            AND fundSourceTransactionIds.FundSourceId IN (
                                                                                         SELECT saFundSources.FundSourceId
                                                                                         FROM   dbo.saFundSources
                                                                                         JOIN   dbo.syAwardTypes9010Mapping ON syAwardTypes9010Mapping.FundSourceId = saFundSources.FundSourceId
                                                                                         JOIN   dbo.syAwardTypes9010 ON syAwardTypes9010.AwardType9010Id = syAwardTypes9010Mapping.AwardType9010Id
                                                                                         WHERE  syAwardTypes9010.AwardType9010Id IN ( @StudentStipend9010MappingId
                                                                                                                                     ,@RefundToStudent9010MappingId
                                                                                                                                    )
                                                                                                AND dbo.syAwardTypes9010Mapping.CampusId = dbo.saTransactions.CampusId
                                                                                         )
                                            )
                                            OR (
                                               fundSourceTransactionIds.FundSourceId IS NULL
                                               AND fundSourceTransactionIds.TransCodeId IN (
                                                                                           SELECT saTransCodes.TransCodeId
                                                                                           FROM   dbo.saTransCodes
                                                                                           JOIN   dbo.syAwardTypes9010Mapping ON syAwardTypes9010Mapping.TransCodeId = saTransCodes.TransCodeId
                                                                                           JOIN   dbo.syAwardTypes9010 ON syAwardTypes9010.AwardType9010Id = syAwardTypes9010Mapping.AwardType9010Id
                                                                                           WHERE  syAwardTypes9010.AwardType9010Id IN ( @StudentStipend9010MappingId
                                                                                                                                       ,@RefundToStudent9010MappingId
                                                                                                                                      )
                                                                                                  AND dbo.syAwardTypes9010Mapping.CampusId = dbo.saTransactions.CampusId
                                                                                           )
                                               )
                                            )
                              )
                            - (
                              SELECT    ISNULL(( SUM(TransAmount)), 0)
                              FROM      dbo.saTransactions
                              LEFT JOIN (
                                        SELECT    ( CASE WHEN saTransactions.FundSourceId IS NULL THEN saRefunds.FundSourceId
                                                         ELSE saTransactions.FundSourceId
                                                    END
                                                  ) AS FundSourceId
                                                 ,saTransactions.TransactionId
                                                 ,TransCodeId
                                        FROM      dbo.saTransactions
                                        LEFT JOIN dbo.saRefunds ON saRefunds.TransactionId = saTransactions.TransactionId
                                        ) fundSourceTransactionIds ON fundSourceTransactionIds.TransactionId = saTransactions.TransactionId
                              WHERE     StuEnrollId = [#EnrollmentList].StuEnrollId
                                        AND TransDate >= @FiscalYearStart
                                        AND TransDate <= @FiscalYearEnd
                                        AND Voided = 0
                                        AND (
                                            (
                                            fundSourceTransactionIds.FundSourceId IS NOT NULL
                                            AND fundSourceTransactionIds.FundSourceId IN (
                                                                                         SELECT saFundSources.FundSourceId
                                                                                         FROM   dbo.saFundSources
                                                                                         JOIN   dbo.syAwardTypes9010Mapping ON syAwardTypes9010Mapping.FundSourceId = saFundSources.FundSourceId
                                                                                         JOIN   dbo.syAwardTypes9010 ON syAwardTypes9010.AwardType9010Id = syAwardTypes9010Mapping.AwardType9010Id
                                                                                         WHERE  syAwardTypes9010.AwardType9010Id IN ( @OutsideLoanAwardId
                                                                                                                                     ,@SchoolLoanAwardId
                                                                                                                                    )
                                                                                                AND dbo.syAwardTypes9010Mapping.CampusId = dbo.saTransactions.CampusId
                                                                                         )
                                            )
                                            OR (
                                               fundSourceTransactionIds.FundSourceId IS NULL
                                               AND fundSourceTransactionIds.TransCodeId IN (
                                                                                           SELECT saTransCodes.TransCodeId
                                                                                           FROM   dbo.saTransCodes
                                                                                           JOIN   dbo.syAwardTypes9010Mapping ON syAwardTypes9010Mapping.TransCodeId = saTransCodes.TransCodeId
                                                                                           JOIN   dbo.syAwardTypes9010 ON syAwardTypes9010.AwardType9010Id = syAwardTypes9010Mapping.AwardType9010Id
                                                                                           WHERE  syAwardTypes9010.AwardType9010Id IN ( @RefundToStudent9010MappingId )
                                                                                           )
                                               )
                                            )
                              )
                           )
                    FROM   #EnrollmentList;

        --@TotalNonTitleIVRevenue calculation
        CREATE TABLE #TotalNonTitleIVRevenue
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,ExcelFormula VARCHAR(200)
            );

        INSERT INTO #TotalNonTitleIVRevenue
                    SELECT StuEnrollId
                          ,( 'SUM(AC' + LTRIM(RTRIM(STR(RowNumber + 1))) + ':AD' + LTRIM(RTRIM(STR(RowNumber + 1))) + ')' ) AS ExcelFormula
                    FROM   #EnrollmentList;

        --@AdjOutsideGrant calculation
        CREATE TABLE #AdjOutsideGrant
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,Amount DECIMAL(16, 2)
            );

        INSERT INTO #AdjOutsideGrant
                    SELECT [#EnrollmentList].StuEnrollId
                          ,CASE WHEN (((( CASE WHEN [#InstitutionalChargeBalance].Amount >= 0 THEN [#InstitutionalChargeBalance].Amount
                                               ELSE 0
                                          END
                                        ) + [#InstitutionalMaxChargeLimitThisYear].Amount
                                       )
                                      ) <= 0
                                     ) THEN ( 0 )
                                WHEN ((( CASE WHEN [#InstitutionalChargeBalance].Amount >= 0 THEN [#InstitutionalChargeBalance].Amount
                                              ELSE 0
                                         END
                                       ) + [#InstitutionalMaxChargeLimitThisYear].Amount
                                      ) < (
                                          SELECT    ISNULL( -- Total Institutional Max Charge Limit < First Applied Funds then adjust else nothing
                                                              SUM(TransAmount) * -1, 0
                                                          ) AS FirstAppliedAmount
                                          FROM      dbo.saTransactions
                                          LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                                          LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                                          LEFT JOIN dbo.syAwardTypes9010Mapping ON syAwardTypes9010Mapping.FundSourceId = saFundSources.FundSourceId
                                          LEFT JOIN dbo.syAwardTypes9010 ON syAwardTypes9010.AwardType9010Id = syAwardTypes9010Mapping.AwardType9010Id
                                          WHERE     StuEnrollId = [#EnrollmentList].StuEnrollId
                                                    AND Voided = 0
                                                    AND dbo.syAwardTypes9010Mapping.CampusId = dbo.saTransactions.CampusId
                                                    AND dbo.syAwardTypes9010.AwardType9010Id IN ( @SchoolGrantId, @OutsideGrantId, @JobTrainingGrantId
                                                                                                 ,@EducationalSavingsId
                                                                                                )
                                                    AND TransDate < @FiscalYearStart
                                          )
                                     ) THEN -- adjust 
                                    CASE WHEN (((( CASE WHEN [#InstitutionalChargeBalance].Amount >= 0 THEN -- total max inst - outsidegrantamount > 0 then take full outside grant amount
                                                            [#InstitutionalChargeBalance].Amount
                                                        ELSE 0
                                                   END
                                                 ) + [#InstitutionalMaxChargeLimitThisYear].Amount
                                                )
                                               ) - (
                                                   SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) AS Amount
                                                   FROM      dbo.saTransactions
                                                   LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                                                   LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                                                   LEFT JOIN dbo.syAwardTypes9010Mapping ON syAwardTypes9010Mapping.FundSourceId = saFundSources.FundSourceId
                                                   WHERE     Voided = 0
                                                             AND saTransactions.StuEnrollId = [#EnrollmentList].StuEnrollId
                                                             AND AwardType9010Id = @OutsideGrantId
                                                             AND TransDate >= @FiscalYearStart
                                                             AND TransDate <= @FiscalYearEnd
                                                             AND dbo.syAwardTypes9010Mapping.CampusId = dbo.saTransactions.CampusId
                                                   ) > 0
                                              ) THEN (
                                                     SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) AS Amount
                                                     FROM      dbo.saTransactions
                                                     LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                                                     LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                                                     LEFT JOIN dbo.syAwardTypes9010Mapping ON syAwardTypes9010Mapping.FundSourceId = saFundSources.FundSourceId
                                                     WHERE     Voided = 0
                                                               AND saTransactions.StuEnrollId = [#EnrollmentList].StuEnrollId
                                                               AND AwardType9010Id = @OutsideGrantId
                                                               AND TransDate >= @FiscalYearStart
                                                               AND TransDate <= @FiscalYearEnd
                                                               AND dbo.syAwardTypes9010Mapping.CampusId = dbo.saTransactions.CampusId
                                                     )
                                         ELSE
                          (((( CASE WHEN [#InstitutionalChargeBalance].Amount >= 0 THEN -- total max inst - outsidegrantamount > 0 then take full outside grant amount
                                        [#InstitutionalChargeBalance].Amount
                                    ELSE 0
                               END
                             ) + [#InstitutionalMaxChargeLimitThisYear].Amount
                            )
                           )
                          )
                                    END
                                ELSE (
                                     SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) AS Amount
                                     FROM      dbo.saTransactions
                                     LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                                     LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                                     LEFT JOIN dbo.syAwardTypes9010Mapping ON syAwardTypes9010Mapping.FundSourceId = saFundSources.FundSourceId
                                     WHERE     Voided = 0
                                               AND saTransactions.StuEnrollId = [#EnrollmentList].StuEnrollId
                                               AND AwardType9010Id = @OutsideGrantId
                                               AND TransDate >= @FiscalYearStart
                                               AND TransDate <= @FiscalYearEnd
                                               AND dbo.syAwardTypes9010Mapping.CampusId = dbo.saTransactions.CampusId
                                     )
                           END AS TotalInstitutionalMaxChargeLimit
                    FROM   #EnrollmentList
                    JOIN   #InstitutionalMaxChargeLimitThisYear ON [#InstitutionalMaxChargeLimitThisYear].StuEnrollId = [#EnrollmentList].StuEnrollId
                    JOIN   #InstitutionalChargeBalance ON [#InstitutionalChargeBalance].StuEnrollId = [#EnrollmentList].StuEnrollId;

        --@AdjJobTrainingGrant calculation
        CREATE TABLE #AdjJobTrainingGrant
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,Amount DECIMAL(16, 2)
            );

        INSERT INTO #AdjJobTrainingGrant
                    SELECT [#EnrollmentList].StuEnrollId
                          ,CASE WHEN (((( CASE WHEN [#InstitutionalChargeBalance].Amount >= 0 THEN [#InstitutionalChargeBalance].Amount
                                               ELSE 0
                                          END
                                        ) + [#InstitutionalMaxChargeLimitThisYear].Amount
                                       )
                                      ) <= 0
                                     ) THEN ( 0 )
                                WHEN ((( CASE WHEN [#InstitutionalChargeBalance].Amount >= 0 THEN [#InstitutionalChargeBalance].Amount
                                              ELSE 0
                                         END
                                       ) + [#InstitutionalMaxChargeLimitThisYear].Amount
                                      ) < (
                                          SELECT    ISNULL( -- Total Institutional Max Charge Limit < First Applied Funds then adjust else nothing
                                                              SUM(TransAmount) * -1, 0
                                                          ) AS FirstAppliedAmount
                                          FROM      dbo.saTransactions
                                          LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                                          LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                                          LEFT JOIN dbo.syAwardTypes9010Mapping ON syAwardTypes9010Mapping.FundSourceId = saFundSources.FundSourceId
                                          LEFT JOIN dbo.syAwardTypes9010 ON syAwardTypes9010.AwardType9010Id = syAwardTypes9010Mapping.AwardType9010Id
                                          WHERE     StuEnrollId = [#EnrollmentList].StuEnrollId
                                                    AND Voided = 0
                                                    AND dbo.syAwardTypes9010.AwardType9010Id IN ( @SchoolGrantId, @OutsideGrantId, @JobTrainingGrantId
                                                                                                 ,@EducationalSavingsId
                                                                                                )
                                                    AND TransDate < @FiscalYearStart
                                                    AND dbo.syAwardTypes9010Mapping.CampusId = dbo.saTransactions.CampusId
                                          )
                                     ) THEN -- adjust 
                                    CASE WHEN (((( CASE WHEN [#InstitutionalChargeBalance].Amount >= 0 THEN -- total max inst - sum(outsidegrantamount, jobtraining) > 0 then take full job training grant amount
                                                            [#InstitutionalChargeBalance].Amount
                                                        ELSE 0
                                                   END
                                                 ) + [#InstitutionalMaxChargeLimitThisYear].Amount
                                                )
                                               ) - (
                                                   SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) AS Amount
                                                   FROM      dbo.saTransactions
                                                   LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                                                   LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                                                   LEFT JOIN dbo.syAwardTypes9010Mapping ON syAwardTypes9010Mapping.FundSourceId = saFundSources.FundSourceId
                                                   WHERE     Voided = 0
                                                             AND saTransactions.StuEnrollId = [#EnrollmentList].StuEnrollId
                                                             AND AwardType9010Id IN ( @OutsideGrantId, @JobTrainingGrantId )
                                                             AND TransDate >= @FiscalYearStart
                                                             AND TransDate <= @FiscalYearEnd
                                                             AND dbo.syAwardTypes9010Mapping.CampusId = dbo.saTransactions.CampusId
                                                   ) > 0
                                              ) THEN (
                                                     SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) AS Amount
                                                     FROM      dbo.saTransactions
                                                     LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                                                     LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                                                     LEFT JOIN dbo.syAwardTypes9010Mapping ON syAwardTypes9010Mapping.FundSourceId = saFundSources.FundSourceId
                                                     WHERE     Voided = 0
                                                               AND saTransactions.StuEnrollId = [#EnrollmentList].StuEnrollId
                                                               AND AwardType9010Id = @JobTrainingGrantId
                                                               AND TransDate >= @FiscalYearStart
                                                               AND TransDate <= @FiscalYearEnd
                                                               AND dbo.syAwardTypes9010Mapping.CampusId = dbo.saTransactions.CampusId
                                                     )
                                         ELSE
                          (((( CASE WHEN [#InstitutionalChargeBalance].Amount >= 0 THEN -- total max inst - outsidegrantamount
                                        [#InstitutionalChargeBalance].Amount
                                    ELSE 0
                               END
                             ) + [#InstitutionalMaxChargeLimitThisYear].Amount
                            )
                           ) - (
                               SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) AS Amount
                               FROM      dbo.saTransactions
                               LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                               LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                               LEFT JOIN dbo.syAwardTypes9010Mapping ON syAwardTypes9010Mapping.FundSourceId = saFundSources.FundSourceId
                               WHERE     Voided = 0
                                         AND saTransactions.StuEnrollId = [#EnrollmentList].StuEnrollId
                                         AND AwardType9010Id = @OutsideGrantId
                                         AND TransDate >= @FiscalYearStart
                                         AND TransDate <= @FiscalYearEnd
                                         AND dbo.syAwardTypes9010Mapping.CampusId = dbo.saTransactions.CampusId
                               )
                          )
                                    END
                                ELSE (
                                     SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) AS Amount
                                     FROM      dbo.saTransactions
                                     LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                                     LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                                     LEFT JOIN dbo.syAwardTypes9010Mapping ON syAwardTypes9010Mapping.FundSourceId = saFundSources.FundSourceId
                                     WHERE     Voided = 0
                                               AND saTransactions.StuEnrollId = [#EnrollmentList].StuEnrollId
                                               AND AwardType9010Id = @JobTrainingGrantId
                                               AND TransDate >= @FiscalYearStart
                                               AND TransDate <= @FiscalYearEnd
                                               AND dbo.syAwardTypes9010Mapping.CampusId = dbo.saTransactions.CampusId
                                     )
                           END AS TotalInstitutionalMaxChargeLimit
                    FROM   #EnrollmentList
                    JOIN   #InstitutionalMaxChargeLimitThisYear ON [#InstitutionalMaxChargeLimitThisYear].StuEnrollId = [#EnrollmentList].StuEnrollId
                    JOIN   #InstitutionalChargeBalance ON [#InstitutionalChargeBalance].StuEnrollId = [#EnrollmentList].StuEnrollId;



        --@AdjEducationSavingPlan calculation
        CREATE TABLE #AdjEducationSavingPlan
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,Amount DECIMAL(16, 2)
            );

        INSERT INTO #AdjEducationSavingPlan
                    SELECT [#EnrollmentList].StuEnrollId
                          ,CASE WHEN (((( CASE WHEN [#InstitutionalChargeBalance].Amount >= 0 THEN [#InstitutionalChargeBalance].Amount
                                               ELSE 0
                                          END
                                        ) + [#InstitutionalMaxChargeLimitThisYear].Amount
                                       )
                                      ) <= 0
                                     ) THEN ( 0 )
                                WHEN ((( CASE WHEN [#InstitutionalChargeBalance].Amount >= 0 THEN [#InstitutionalChargeBalance].Amount
                                              ELSE 0
                                         END
                                       ) + [#InstitutionalMaxChargeLimitThisYear].Amount
                                      ) < (
                                          SELECT    ISNULL( -- Total Institutional Max Charge Limit < First Applied Funds then adjust else nothing
                                                              SUM(TransAmount) * -1, 0
                                                          ) AS FirstAppliedAmount
                                          FROM      dbo.saTransactions
                                          LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                                          LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                                          LEFT JOIN dbo.syAwardTypes9010Mapping ON syAwardTypes9010Mapping.FundSourceId = saFundSources.FundSourceId
                                          LEFT JOIN dbo.syAwardTypes9010 ON syAwardTypes9010.AwardType9010Id = syAwardTypes9010Mapping.AwardType9010Id
                                          WHERE     StuEnrollId = [#EnrollmentList].StuEnrollId
                                                    AND Voided = 0
                                                    AND dbo.syAwardTypes9010.AwardType9010Id IN ( @SchoolGrantId, @OutsideGrantId, @JobTrainingGrantId
                                                                                                 ,@EducationalSavingsId
                                                                                                )
                                                    AND TransDate < @FiscalYearStart
                                                    AND dbo.syAwardTypes9010Mapping.CampusId = dbo.saTransactions.CampusId
                                          )
                                     ) THEN -- adjust 
                                    CASE WHEN (((( CASE WHEN [#InstitutionalChargeBalance].Amount >= 0 THEN -- total max inst - sum(outsidegrantamount, jobtraining, education saving) > 0 then take full job training grant amount
                                                            [#InstitutionalChargeBalance].Amount
                                                        ELSE 0
                                                   END
                                                 ) + [#InstitutionalMaxChargeLimitThisYear].Amount
                                                )
                                               ) - (
                                                   SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) AS Amount
                                                   FROM      dbo.saTransactions
                                                   LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                                                   LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                                                   LEFT JOIN dbo.syAwardTypes9010Mapping ON syAwardTypes9010Mapping.FundSourceId = saFundSources.FundSourceId
                                                   WHERE     Voided = 0
                                                             AND saTransactions.StuEnrollId = [#EnrollmentList].StuEnrollId
                                                             AND AwardType9010Id IN ( @OutsideGrantId, @JobTrainingGrantId, @EducationalSavingsId )
                                                             AND TransDate >= @FiscalYearStart
                                                             AND TransDate <= @FiscalYearEnd
                                                             AND dbo.syAwardTypes9010Mapping.CampusId = dbo.saTransactions.CampusId
                                                   ) > 0
                                              ) THEN (
                                                     SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) AS Amount
                                                     FROM      dbo.saTransactions
                                                     LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                                                     LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                                                     LEFT JOIN dbo.syAwardTypes9010Mapping ON syAwardTypes9010Mapping.FundSourceId = saFundSources.FundSourceId
                                                     WHERE     Voided = 0
                                                               AND saTransactions.StuEnrollId = [#EnrollmentList].StuEnrollId
                                                               AND AwardType9010Id = @EducationalSavingsId
                                                               AND TransDate >= @FiscalYearStart
                                                               AND TransDate <= @FiscalYearEnd
                                                               AND dbo.syAwardTypes9010Mapping.CampusId = dbo.saTransactions.CampusId
                                                     )
                                         ELSE
                          (((( CASE WHEN [#InstitutionalChargeBalance].Amount >= 0 THEN -- total max inst - sum(outsidegrantamount, job traininggrant)
                                        [#InstitutionalChargeBalance].Amount
                                    ELSE 0
                               END
                             ) + [#InstitutionalMaxChargeLimitThisYear].Amount
                            )
                           ) - (
                               SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) AS Amount
                               FROM      dbo.saTransactions
                               LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                               LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                               LEFT JOIN dbo.syAwardTypes9010Mapping ON syAwardTypes9010Mapping.FundSourceId = saFundSources.FundSourceId
                               WHERE     Voided = 0
                                         AND saTransactions.StuEnrollId = [#EnrollmentList].StuEnrollId
                                         AND AwardType9010Id IN ( @OutsideGrantId, @JobTrainingGrantId )
                                         AND TransDate >= @FiscalYearStart
                                         AND TransDate <= @FiscalYearEnd
                                         AND dbo.syAwardTypes9010Mapping.CampusId = dbo.saTransactions.CampusId
                               )
                          )
                                    END
                                ELSE (
                                     SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) AS Amount
                                     FROM      dbo.saTransactions
                                     LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                                     LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                                     LEFT JOIN dbo.syAwardTypes9010Mapping ON syAwardTypes9010Mapping.FundSourceId = saFundSources.FundSourceId
                                     WHERE     Voided = 0
                                               AND saTransactions.StuEnrollId = [#EnrollmentList].StuEnrollId
                                               AND AwardType9010Id = @EducationalSavingsId
                                               AND TransDate >= @FiscalYearStart
                                               AND TransDate <= @FiscalYearEnd
                                               AND dbo.syAwardTypes9010Mapping.CampusId = dbo.saTransactions.CampusId
                                     )
                           END AS TotalInstitutionalMaxChargeLimit
                    FROM   #EnrollmentList
                    JOIN   #InstitutionalMaxChargeLimitThisYear ON [#InstitutionalMaxChargeLimitThisYear].StuEnrollId = [#EnrollmentList].StuEnrollId
                    JOIN   #InstitutionalChargeBalance ON [#InstitutionalChargeBalance].StuEnrollId = [#EnrollmentList].StuEnrollId;


        --@AdjSchoolGrant calculation
        CREATE TABLE #AdjSchoolGrant
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,Amount DECIMAL(16, 2)
            );

        INSERT INTO #AdjSchoolGrant
                    SELECT [#EnrollmentList].StuEnrollId
                          ,CASE WHEN (((( CASE WHEN [#InstitutionalChargeBalance].Amount >= 0 THEN [#InstitutionalChargeBalance].Amount
                                               ELSE 0
                                          END
                                        ) + [#InstitutionalMaxChargeLimitThisYear].Amount
                                       )
                                      ) <= 0
                                     ) THEN ( 0 )
                                WHEN ((( CASE WHEN [#InstitutionalChargeBalance].Amount >= 0 THEN [#InstitutionalChargeBalance].Amount
                                              ELSE 0
                                         END
                                       ) + [#InstitutionalMaxChargeLimitThisYear].Amount
                                      ) < (
                                          SELECT    ISNULL( -- Total Institutional Max Charge Limit < First Applied Funds then adjust else nothing
                                                              SUM(TransAmount) * -1, 0
                                                          ) AS FirstAppliedAmount
                                          FROM      dbo.saTransactions
                                          LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                                          LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                                          LEFT JOIN dbo.syAwardTypes9010Mapping ON syAwardTypes9010Mapping.FundSourceId = saFundSources.FundSourceId
                                          LEFT JOIN dbo.syAwardTypes9010 ON syAwardTypes9010.AwardType9010Id = syAwardTypes9010Mapping.AwardType9010Id
                                          WHERE     StuEnrollId = [#EnrollmentList].StuEnrollId
                                                    AND Voided = 0
                                                    AND dbo.syAwardTypes9010.AwardType9010Id IN ( @SchoolGrantId, @OutsideGrantId, @JobTrainingGrantId
                                                                                                 ,@EducationalSavingsId
                                                                                                )
                                                    AND TransDate < @FiscalYearStart
                                                    AND dbo.syAwardTypes9010Mapping.CampusId = dbo.saTransactions.CampusId
                                          )
                                     ) THEN -- adjust 
                                    CASE WHEN (((( CASE WHEN [#InstitutionalChargeBalance].Amount >= 0 THEN -- total max inst - sum(outsidegrantamount, jobtraining, education saving, school grant) > 0 then take full job training grant amount
                                                            [#InstitutionalChargeBalance].Amount
                                                        ELSE 0
                                                   END
                                                 ) + [#InstitutionalMaxChargeLimitThisYear].Amount
                                                )
                                               ) - (
                                                   SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) AS Amount
                                                   FROM      dbo.saTransactions
                                                   LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                                                   LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                                                   LEFT JOIN dbo.syAwardTypes9010Mapping ON syAwardTypes9010Mapping.FundSourceId = saFundSources.FundSourceId
                                                   WHERE     Voided = 0
                                                             AND saTransactions.StuEnrollId = [#EnrollmentList].StuEnrollId
                                                             AND AwardType9010Id IN ( @OutsideGrantId, @JobTrainingGrantId, @EducationalSavingsId, @SchoolGrantId )
                                                             AND TransDate >= @FiscalYearStart
                                                             AND TransDate <= @FiscalYearEnd
                                                             AND dbo.syAwardTypes9010Mapping.CampusId = dbo.saTransactions.CampusId
                                                   ) > 0
                                              ) THEN (
                                                     SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) AS Amount
                                                     FROM      dbo.saTransactions
                                                     LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                                                     LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                                                     LEFT JOIN dbo.syAwardTypes9010Mapping ON syAwardTypes9010Mapping.FundSourceId = saFundSources.FundSourceId
                                                     WHERE     Voided = 0
                                                               AND saTransactions.StuEnrollId = [#EnrollmentList].StuEnrollId
                                                               AND AwardType9010Id = @SchoolGrantId
                                                               AND TransDate >= @FiscalYearStart
                                                               AND TransDate <= @FiscalYearEnd
                                                               AND dbo.syAwardTypes9010Mapping.CampusId = dbo.saTransactions.CampusId
                                                     )
                                         ELSE
                          (((( CASE WHEN [#InstitutionalChargeBalance].Amount >= 0 THEN -- total max inst - sum(outsidegrantamount, job traininggrant, educationsaving)
                                        [#InstitutionalChargeBalance].Amount
                                    ELSE 0
                               END
                             ) + [#InstitutionalMaxChargeLimitThisYear].Amount
                            )
                           ) - (
                               SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) AS Amount
                               FROM      dbo.saTransactions
                               LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                               LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                               LEFT JOIN dbo.syAwardTypes9010Mapping ON syAwardTypes9010Mapping.FundSourceId = saFundSources.FundSourceId
                               WHERE     Voided = 0
                                         AND saTransactions.StuEnrollId = [#EnrollmentList].StuEnrollId
                                         AND AwardType9010Id IN ( @OutsideGrantId, @JobTrainingGrantId, @EducationalSavingsId )
                                         AND TransDate >= @FiscalYearStart
                                         AND TransDate <= @FiscalYearEnd
                                         AND dbo.syAwardTypes9010Mapping.CampusId = dbo.saTransactions.CampusId
                               )
                          )
                                    END
                                ELSE (
                                     SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) AS Amount
                                     FROM      dbo.saTransactions
                                     LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                                     LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                                     LEFT JOIN dbo.syAwardTypes9010Mapping ON syAwardTypes9010Mapping.FundSourceId = saFundSources.FundSourceId
                                     WHERE     Voided = 0
                                               AND saTransactions.StuEnrollId = [#EnrollmentList].StuEnrollId
                                               AND AwardType9010Id = @SchoolGrantId
                                               AND TransDate >= @FiscalYearStart
                                               AND TransDate <= @FiscalYearEnd
                                               AND dbo.syAwardTypes9010Mapping.CampusId = dbo.saTransactions.CampusId
                                     )
                           END AS TotalInstitutionalMaxChargeLimit
                    FROM   #EnrollmentList
                    JOIN   #InstitutionalMaxChargeLimitThisYear ON [#InstitutionalMaxChargeLimitThisYear].StuEnrollId = [#EnrollmentList].StuEnrollId
                    JOIN   #InstitutionalChargeBalance ON [#InstitutionalChargeBalance].StuEnrollId = [#EnrollmentList].StuEnrollId;

        --@AdjFirstAppliedFund calculation
        CREATE TABLE #AdjFirstAppliedFund
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,ExcelFormula VARCHAR(200)
            );

        INSERT INTO #AdjFirstAppliedFund
                    SELECT StuEnrollId
                          ,( 'SUM(AF' + LTRIM(RTRIM(STR(RowNumber + 1))) + ':AI' + LTRIM(RTRIM(STR(RowNumber + 1))) + ')' ) AS ExcelFormula
                    FROM   #EnrollmentList;

        --@AdjStuPayandOutsideLn calculation
        CREATE TABLE #AdjStuPayandOutsideLn
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,ExcelFormula VARCHAR(200)
            );

        INSERT INTO #AdjStuPayandOutsideLn
                    SELECT StuEnrollId
                          ,'IF(AD' + LTRIM(RTRIM(STR(RowNumber + 1))) + '>0,IF(G' + LTRIM(RTRIM(STR(RowNumber + 1))) + '-(AJ'
                           + LTRIM(RTRIM(STR(RowNumber + 1))) + '+X' + LTRIM(RTRIM(STR(RowNumber + 1))) + ')>AD' + LTRIM(RTRIM(STR(RowNumber + 1))) + ',AD'
                           + LTRIM(RTRIM(STR(RowNumber + 1))) + ',G' + LTRIM(RTRIM(STR(RowNumber + 1))) + '-(AJ' + LTRIM(RTRIM(STR(RowNumber + 1))) + '+X'
                           + LTRIM(RTRIM(STR(RowNumber + 1))) + ')),0)' AS ExcelFormula
                    FROM   #EnrollmentList;

        --@AdjTotalNonTitleIVRevenue calculation
        CREATE TABLE #AdjTotalNonTitleIVRevenue
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,ExcelFormula VARCHAR(200)
            );

        INSERT INTO #AdjTotalNonTitleIVRevenue
                    SELECT StuEnrollId
                          ,( 'SUM(AJ' + LTRIM(RTRIM(STR(RowNumber + 1))) + ':AK' + LTRIM(RTRIM(STR(RowNumber + 1))) + ')' ) AS ExcelFormula
                    FROM   #EnrollmentList;

        --@ActivitiesConducted calculation
        CREATE TABLE #ActivitiesConducted
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,Amount DECIMAL(16, 2)
            );

        INSERT INTO #ActivitiesConducted
                    SELECT StuEnrollId
                          ,0
                    FROM   #EnrollmentList;

        --@PaymentsOutsideLoanAndOtherSourceNonTitleIV calculation
        CREATE TABLE #PaymentsOutsideLoanAndOtherSourceNonTitleIV
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,Amount DECIMAL(16, 2)
            );

        INSERT INTO #PaymentsOutsideLoanAndOtherSourceNonTitleIV
                    SELECT [#EnrollmentList].StuEnrollId
                          ,((
                            SELECT ISNULL(( SUM(TransAmount)), 0) * -1
                            FROM   dbo.saTransactions
                            WHERE  StuEnrollId = [#EnrollmentList].StuEnrollId
                                   AND TransDate >= @FiscalYearStart
                                   AND TransDate <= @FiscalYearEnd
                                   AND IsTitleIVProgram = 0
                                   AND Voided = 0
                                   AND (
                                       (
                                       saTransactions.FundSourceId IS NOT NULL
                                       AND dbo.saTransactions.FundSourceId IN (
                                                                              SELECT saFundSources.FundSourceId
                                                                              FROM   dbo.saFundSources
                                                                              JOIN   dbo.syAwardTypes9010Mapping ON syAwardTypes9010Mapping.FundSourceId = saFundSources.FundSourceId
                                                                              JOIN   dbo.syAwardTypes9010 ON syAwardTypes9010.AwardType9010Id = syAwardTypes9010Mapping.AwardType9010Id
                                                                              WHERE  syAwardTypes9010.AwardType9010Id IN ( @OutsideLoanAwardId
                                                                                                                          ,@StudentPayment9010MappingId
                                                                                                                          ,@SchoolLoanAwardId
                                                                                                                         )
                                                                                     AND dbo.syAwardTypes9010Mapping.CampusId = dbo.saTransactions.CampusId
                                                                              )
                                       )
                                       OR (
                                          saTransactions.FundSourceId IS NULL
                                          AND saTransactions.TransCodeId IN (
                                                                            SELECT saTransCodes.TransCodeId
                                                                            FROM   dbo.saTransCodes
                                                                            JOIN   dbo.syAwardTypes9010Mapping ON syAwardTypes9010Mapping.TransCodeId = saTransCodes.TransCodeId
                                                                            JOIN   dbo.syAwardTypes9010 ON syAwardTypes9010.AwardType9010Id = syAwardTypes9010Mapping.AwardType9010Id
                                                                            WHERE  syAwardTypes9010.AwardType9010Id = @StudentPayment9010MappingId
                                                                                   AND dbo.syAwardTypes9010Mapping.CampusId = dbo.saTransactions.CampusId
                                                                            )
                                          )
                                       )
                            )
                            - (
                              SELECT    ISNULL(( SUM(TransAmount)), 0)
                              FROM      dbo.saTransactions
                              LEFT JOIN (
                                        SELECT    ( CASE WHEN saTransactions.FundSourceId IS NULL THEN saRefunds.FundSourceId
                                                         ELSE saTransactions.FundSourceId
                                                    END
                                                  ) AS FundSourceId
                                                 ,saTransactions.TransactionId
                                                 ,TransCodeId
                                        FROM      dbo.saTransactions
                                        LEFT JOIN dbo.saRefunds ON saRefunds.TransactionId = saTransactions.TransactionId
                                        ) fundSourceTransactionIds ON fundSourceTransactionIds.TransactionId = saTransactions.TransactionId
                              WHERE     StuEnrollId = [#EnrollmentList].StuEnrollId
                                        AND TransDate >= @FiscalYearStart
                                        AND TransDate <= @FiscalYearEnd
                                        AND IsTitleIVProgram = 0
                                        AND Voided = 0
                                        AND (
                                            (
                                            fundSourceTransactionIds.FundSourceId IS NOT NULL
                                            AND fundSourceTransactionIds.FundSourceId IN (
                                                                                         SELECT saFundSources.FundSourceId
                                                                                         FROM   dbo.saFundSources
                                                                                         JOIN   dbo.syAwardTypes9010Mapping ON syAwardTypes9010Mapping.FundSourceId = saFundSources.FundSourceId
                                                                                         JOIN   dbo.syAwardTypes9010 ON syAwardTypes9010.AwardType9010Id = syAwardTypes9010Mapping.AwardType9010Id
                                                                                         WHERE  syAwardTypes9010.AwardType9010Id IN ( @StudentStipend9010MappingId
                                                                                                                                     ,@RefundToStudent9010MappingId
                                                                                                                                    )
                                                                                                AND dbo.syAwardTypes9010Mapping.CampusId = dbo.saTransactions.CampusId
                                                                                         )
                                            )
                                            OR (
                                               fundSourceTransactionIds.FundSourceId IS NULL
                                               AND fundSourceTransactionIds.TransCodeId IN (
                                                                                           SELECT saTransCodes.TransCodeId
                                                                                           FROM   dbo.saTransCodes
                                                                                           JOIN   dbo.syAwardTypes9010Mapping ON syAwardTypes9010Mapping.TransCodeId = saTransCodes.TransCodeId
                                                                                           JOIN   dbo.syAwardTypes9010 ON syAwardTypes9010.AwardType9010Id = syAwardTypes9010Mapping.AwardType9010Id
                                                                                           WHERE  syAwardTypes9010.AwardType9010Id IN ( @StudentStipend9010MappingId
                                                                                                                                       ,@RefundToStudent9010MappingId
                                                                                                                                      )
                                                                                                  AND dbo.syAwardTypes9010Mapping.CampusId = dbo.saTransactions.CampusId
                                                                                           )
                                               )
                                            )
                              )
                            - (
                              SELECT    ISNULL(( SUM(TransAmount)), 0)
                              FROM      dbo.saTransactions
                              LEFT JOIN (
                                        SELECT    ( CASE WHEN saTransactions.FundSourceId IS NULL THEN saRefunds.FundSourceId
                                                         ELSE saTransactions.FundSourceId
                                                    END
                                                  ) AS FundSourceId
                                                 ,saTransactions.TransactionId
                                                 ,TransCodeId
                                        FROM      dbo.saTransactions
                                        LEFT JOIN dbo.saRefunds ON saRefunds.TransactionId = saTransactions.TransactionId
                                        ) fundSourceTransactionIds ON fundSourceTransactionIds.TransactionId = saTransactions.TransactionId
                              LEFT JOIN dbo.saRefunds ON saRefunds.TransactionId = fundSourceTransactionIds.TransactionId
                              WHERE     StuEnrollId = [#EnrollmentList].StuEnrollId
                                        AND TransDate >= @FiscalYearStart
                                        AND TransDate <= @FiscalYearEnd
                                        AND IsTitleIVProgram = 0
                                        AND Voided = 0
                                        AND RefundTypeId IS NOT NULL
                                        AND (
                                            (
                                            fundSourceTransactionIds.FundSourceId IS NOT NULL
                                            AND fundSourceTransactionIds.FundSourceId IN (
                                                                                         SELECT saFundSources.FundSourceId
                                                                                         FROM   dbo.saFundSources
                                                                                         JOIN   dbo.syAwardTypes9010Mapping ON syAwardTypes9010Mapping.FundSourceId = saFundSources.FundSourceId
                                                                                         JOIN   dbo.syAwardTypes9010 ON syAwardTypes9010.AwardType9010Id = syAwardTypes9010Mapping.AwardType9010Id
                                                                                         WHERE  syAwardTypes9010.AwardType9010Id IN ( @OutsideLoanAwardId
                                                                                                                                     ,@SchoolLoanAwardId
                                                                                                                                    )
                                                                                                AND dbo.syAwardTypes9010Mapping.CampusId = dbo.saTransactions.CampusId
                                                                                         )
                                            )
                                            OR (
                                               fundSourceTransactionIds.FundSourceId IS NULL
                                               AND fundSourceTransactionIds.TransCodeId IN (
                                                                                           SELECT saTransCodes.TransCodeId
                                                                                           FROM   dbo.saTransCodes
                                                                                           JOIN   dbo.syAwardTypes9010Mapping ON syAwardTypes9010Mapping.TransCodeId = saTransCodes.TransCodeId
                                                                                           JOIN   dbo.syAwardTypes9010 ON syAwardTypes9010.AwardType9010Id = syAwardTypes9010Mapping.AwardType9010Id
                                                                                           WHERE  syAwardTypes9010.AwardType9010Id IN ( @StudentPayment9010MappingId )
                                                                                                  AND dbo.syAwardTypes9010Mapping.CampusId = dbo.saTransactions.CampusId
                                                                                           )
                                               )
                                            )
                              )
                           )
                    FROM   #EnrollmentList;

        --@TotalOtherSourceRevenue calculation
        CREATE TABLE #TotalOtherSourceRevenue
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,ExcelFormula VARCHAR(200)
            );

        INSERT INTO #TotalOtherSourceRevenue
                    SELECT StuEnrollId
                          ,( 'SUM(AM' + LTRIM(RTRIM(STR(RowNumber + 1))) + ':AN' + LTRIM(RTRIM(STR(RowNumber + 1))) + ')' ) AS ExcelFormula
                    FROM   #EnrollmentList;

        --@AdjActivitiesConducted calculation
        CREATE TABLE #AdjActivitiesConducted
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,Amount DECIMAL(16, 2)
            );

        INSERT INTO #AdjActivitiesConducted
                    SELECT StuEnrollId
                          ,0
                    FROM   #EnrollmentList;

        --@AdjPaymentsOutsideLoanAndOtherSourceNonTitleIV calculation
        CREATE TABLE #AdjPaymentsOutsideLoanAndOtherSourceNonTitleIV
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,ExcelFormula VARCHAR(200)
            );

        INSERT INTO #AdjPaymentsOutsideLoanAndOtherSourceNonTitleIV
                    SELECT StuEnrollId
                          ,'IF(AN' + LTRIM(RTRIM(STR(RowNumber + 1))) + '>0,IF(G' + LTRIM(RTRIM(STR(RowNumber + 1))) + '-(AJ'
                           + LTRIM(RTRIM(STR(RowNumber + 1))) + '+X' + LTRIM(RTRIM(STR(RowNumber + 1))) + ')>AN' + LTRIM(RTRIM(STR(RowNumber + 1))) + ',AN'
                           + LTRIM(RTRIM(STR(RowNumber + 1))) + ',G' + LTRIM(RTRIM(STR(RowNumber + 1))) + '-(AJ' + LTRIM(RTRIM(STR(RowNumber + 1))) + '+X'
                           + LTRIM(RTRIM(STR(RowNumber + 1))) + ')),0)' AS ExcelFormula
                    FROM   #EnrollmentList;

        --@@AdjTotalOtherSourceRevenue calculation
        CREATE TABLE #AdjTotalOtherSourceRevenue
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,ExcelFormula VARCHAR(200)
            );

        INSERT INTO #AdjTotalOtherSourceRevenue
                    SELECT StuEnrollId
                          ,( 'SUM(AP' + LTRIM(RTRIM(STR(RowNumber + 1))) + ':AQ' + LTRIM(RTRIM(STR(RowNumber + 1))) + ')' ) AS ExcelFormula
                    FROM   #EnrollmentList;

        --#Numerator calculation
        CREATE TABLE #Numerator
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,ExcelFormula VARCHAR(200)
            );

        INSERT INTO #Numerator
                    SELECT StuEnrollId
                          ,( 'X' + LTRIM(RTRIM(STR(RowNumber + 1)))) AS ExcelFormula
                    FROM   #EnrollmentList;

        --@AdjTotalOtherSourceRevenue calculation
        CREATE TABLE #Denominator
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,ExcelFormula VARCHAR(200)
            );

        INSERT INTO #Denominator
                    SELECT StuEnrollId
                          ,( 'X' + LTRIM(RTRIM(STR(RowNumber + 1))) + '+AL' + LTRIM(RTRIM(STR(RowNumber + 1))) + '+AR' + LTRIM(RTRIM(STR(RowNumber + 1))) + '' ) AS ExcelFormula
                    FROM   #EnrollmentList;

        --@AdjTotalOtherSourceRevenue calculation
        CREATE TABLE #Percentage
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,ExcelFormula VARCHAR(200)
            );

        INSERT INTO #Percentage
                    SELECT StuEnrollId
                          ,( 'IF(AT' + LTRIM(RTRIM(STR(RowNumber + 1))) + '>0, AS' + LTRIM(RTRIM(STR(RowNumber + 1))) + '/AT'
                             + LTRIM(RTRIM(STR(RowNumber + 1))) + ',0)'
                           ) AS ExcelFormula
                    FROM   #EnrollmentList;

        CREATE TABLE #Sheet1
            (
                Column1 VARCHAR(100)
               ,Column2 VARCHAR(100)
               ,Column3 VARCHAR(100)
               ,Column4 VARCHAR(100)
            );

        INSERT INTO #Sheet1
        VALUES ( '', 'Amount Disbursed', '', 'AdjustedAmount' )
              ,( 'Adjusted Student Title IV Revenue', '', '', '' )
              ,( '', '', '', '' )
              ,( 'Subsidized Loan', 'SUM(''90-10 Student Ledger''!H2:H100000)', '', 'SUM(''90-10 Student Ledger''!O2:O10000)' )
              ,( 'Unsubsidized Loan', 'SUM(''90-10 Student Ledger''!I2:I10000)', '', 'SUM(''90-10 Student Ledger''!P2:P10000)' )
              ,( 'PLUS', 'SUM(''90-10 Student Ledger''!J2:J10000)', '', 'SUM(''90-10 Student Ledger''!Q2:Q10000)' )
              ,( 'Federal Pell Grants', 'SUM(''90-10 Student Ledger''!K2:K10000)', '', 'SUM(''90-10 Student Ledger''!R2:R10000)' )
              ,( 'FSEOG(subject to matching deduction)', 'SUM(''90-10 Student Ledger''!L2:L10000)', '', 'SUM(''90-10 Student Ledger''!S2:S10000)' )
              ,( 'FWS(subject to matching deduction', 'SUM(''90-10 Student Ledger''!M2:M10000)', '', 'SUM(''90-10 Student Ledger''!T2:T10000)' )
              ,( 'Student Title IV Revenue', 'SUM(B4:B9)', '', 'SUM(D4:D9)' )
              ,( '', '', '', '' )
              ,( 'Revenue Adjustment', '', '', 'SUM(''90-10 Student Ledger''!V2:V10000)' )
              ,( 'Title IV funds returned for a student under 34 C.F.R. 668.22', '', '', 'SUM(''90-10 Student Ledger''!W2:W10000)' )
              ,( '', '', '', '' )
              ,( '', '', '', '' )
              ,( 'Adjusted Student Title IV Revenue', '', '', 'D10-D12-D13' )
              ,( '', '', '', '' )
              ,( 'Student Non-Title IV Revenue', '', '', '' )
              ,( '', '', '', '' )
              ,( 'Grant funds for the student from non-Federal public', '', '', '' )
              ,( 'agencies or private sources independent of the institution', 'SUM(''90-10 Student Ledger''!AF2:AF10000)', '', '' )
              ,( '', '', '', '' )
              ,( 'Funds provided for the student under a contractual', '', '', '' )
              ,( 'arrangement with a Federal, State, or local government', '', '', '' )
              ,( 'agency for the purpose of providing job training to low-', '', '', '' )
              ,( 'income individuals', 'SUM(''90-10 Student Ledger''!AG2:AG10000)', '', '' )
              ,( '', '', '', '' )
              ,( 'Funds used by a student from savings plans for educational', '', '', '' )
              ,( 'expenses established by or on behalf of the student that', '', '', '' )
              ,( 'quality for special tax treatment under the Internal', '', '', '' )
              ,( 'Revenue Code', 'SUM(''90-10 Student Ledger''!AH2:AH10000)', '', '' )
              ,( '', '', '', '' )
              ,( 'Institutional Scholarships disbursed to the student', 'SUM(''90-10 Student Ledger''!AI2:AI10000)', '', '' )
              ,( '', '', '', '' )
              ,( '', '', '', '' )
              ,( 'Student Payments on current charges', 'SUM(''90-10 Student Ledger''!AK2:AK10000)', '', '' )
              ,( '', '', '', '' )
              ,( 'Total Student Non-Title IV Revenue', 'SUM(B21:B36)', '', '' )
              ,( '', '', '', '' )
              ,( 'Revenue from Other Sources', '', '', '' )
              ,( '', '', '', '' )
              ,( 'Activities conducted by the Institution that are necessary for', '', '', '' )
              ,( 'education and training (34 CFR 668.28(a)(3)(ii))', (
                                                                     SELECT CONVERT(VARCHAR, CONVERT(DECIMAL(16, 2), @ActivitiesConductedAmount))
                                                                     ) , '', '' )
              ,( '', '', '', '' )
              ,( 'Funds paid by a student, on or behalf of a student, by a party', '', '', '' )
              ,( 'other than the school for an education or training program', '', '', '' )
              ,( 'that is not Title IV eligible (34 CFR 668.28(a)(3)(iii))', 'SUM(''90-10 Student Ledger''!AQ2:AQ10000)', '', '' )
              ,( '', '', '', '' )
              ,( 'Allowable student payments and amounts from accounts', '', '', '' )
              ,( 'receivable or institutional loan sales net of any required', '', '', '' )
              ,( 'payments under a recourse agreement', '', '', '' )
              ,( '', '', '', '' )
              ,( 'Total Revenue from Other Sources', 'SUM(B43:B51)', '', '' )
              ,( '', '', '', '' )
              ,( '', '', '', '' )
              ,( '', '', '', '' )
              ,( 'Numerator', 'D16', '', 'IF(B58>0,B57/B58,0)' )
              ,( 'Denominator', 'D16+B38+B53', '', '' );


        SELECT *
        FROM   #Sheet1;

        SELECT    [#EnrollmentList].StudentNumber AS 'Student Id'
                 ,ISNULL(LastName, '') + ', ' + ISNULL(FirstName, '') + ' ' + ISNULL(MiddleName, '') AS 'Name'
                 ,( CASE WHEN [#EnrollmentList].IsTitleIVProgram = 1 THEN 'Yes - ' + PrgVerDescrip
                         ELSE 'No - ' + PrgVerDescrip
                    END
                  ) AS 'Title IV Program'
                 ,( CASE WHEN ( ISNULL([#InstitutionalChargeBalance].Amount, 0) > 0 ) THEN 0
                         ELSE ISNULL([#InstitutionalChargeBalance].Amount, 0) * -1
                    END
                  ) AS 'Title IV Credit Balance'
                                                                                                                                                          --( CASE WHEN ( ISNULL([@TitleIVCreditBalance].Amount, 0) <= 0 ) THEN
                                                                                                                                                          --                   0
                                                                                                                                                          --               ELSE ISNULL([@TitleIVCreditBalance].Amount, 0)
                                                                                                                                                          --          END ) AS 'Title IV Credit Balance' ,
                 ,( CASE WHEN ( ISNULL([#InstitutionalChargeBalance].Amount, 0) <= 0 ) THEN 0
                         ELSE ISNULL([#InstitutionalChargeBalance].Amount, 0)
                    END
                  ) AS 'Institutional Charge Balance'
                 ,( CASE WHEN ISNULL([#InstitutionalMaxChargeLimitThisYear].Amount, 0) <= 0 THEN 0
                         ELSE ISNULL([#InstitutionalMaxChargeLimitThisYear].Amount, 0)
                    END
                  ) AS 'Institutional Max Charge Limit this year'
                 ,[#TotalInstitutionalMaxChargeLimit].ExcelFormula AS 'Total Institutional Max Charge Limit'                                              --formula field
                 ,ISNULL([#SubLoan].Amount, 0) AS 'Sub Loan'
                 ,ISNULL([#UnSubLoan].Amount, 0) AS 'UnSub Loan'
                 ,ISNULL([#Plus].Amount, 0) AS 'Plus'
                 ,ISNULL([#PellGrant].Amount, 0) AS 'Pell Grant'
                 ,ISNULL([#FSEOG].Amount, 0) AS 'FSEOG'
                 ,ISNULL([#FWS].Amount, 0) AS 'FWS'
                 ,[#TitleIVAmtDisbursed].ExcelFormula AS 'Title IV Amt Disbursed'                                                                         -- formula field
                 ,ISNULL([#SubLoan].Amount, 0) AS 'Adj Sub Loan'
                 ,ISNULL([#UnSubLoan].Amount, 0) AS 'Adj UnSub Loan'
                 ,ISNULL([#Plus].Amount, 0) AS 'Adj Plus'
                 ,ISNULL([#PellGrant].Amount, 0) AS 'Adj Pell Grant'
                 ,ISNULL([#AdjFSEOG].Amount, 0) AS 'Adj FSEOG'
                 ,ISNULL([#FWS].Amount, 0) AS 'Adj FWS'
                 ,[#TitleIVAmtAdjusted].ExcelFormula AS 'Title IV Amt Adjusted'                                                                           --formula field
                 ,[#RevenueAdjustment].ExcelFormula AS 'Revenue Adjustment'                                                                               --formula field
                 ,ISNULL([#TitleIVRefunds].Amount, 0) AS 'Title IV Refunds'
                 ,[#AdjTitleIVRevenue].ExcelFormula AS 'Adj Title IV Revenue'                                                                             --formula field
                 ,ISNULL([#OutsideGrant].Amount, 0) AS 'Outside Grant'
                 ,ISNULL([#JobTrainingGrant].Amount, 0) AS 'Job Training Grant'
                 ,ISNULL([#EducationSavingPlan].Amount, 0) AS 'Education Saving Plan'
                 ,ISNULL([#SchoolGrant].Amount, 0) AS 'School Grant'
                 ,[#FirstAppliedFund].ExcelFormula AS 'First Applied Fund'                                                                                --formula field
                 ,CASE WHEN ISNULL([#StuPayandOutsideLn].Amount, 0) > 0 THEN ISNULL([#StuPayandOutsideLn].Amount, 0)
                       ELSE 0
                  END AS 'Stu Pay and Outside Ln'
                 ,[#TotalNonTitleIVRevenue].ExcelFormula AS 'Total Non Title IV Revenue'                                                                  --formula field
                 ,ISNULL([#AdjOutsideGrant].Amount, 0) AS 'Adj Outside Grant'
                 ,ISNULL([#AdjJobTrainingGrant].Amount, 0) AS 'Adj Job Training Grant'
                 ,ISNULL([#AdjEducationSavingPlan].Amount, 0) AS 'Adj Education Saving Plan'
                 ,ISNULL([#AdjSchoolGrant].Amount, 0) AS 'Adj School Grant'
                 ,[#AdjFirstAppliedFund].ExcelFormula AS 'Adj First Applied Fund'                                                                         --formula field
                 ,[#AdjStuPayandOutsideLn].ExcelFormula AS 'Adj Stu Pay and Outside Ln'                                                                   --formula field
                 ,[#AdjTotalNonTitleIVRevenue].ExcelFormula AS 'Adj Total Non Title IV Revenue'                                                           --formula field
                 ,0 AS 'Activities Conducted'
                 ,ISNULL([#PaymentsOutsideLoanAndOtherSourceNonTitleIV].Amount, 0) AS 'Payments, Outside Loan and Other Source (Non Title IV Program)'
                 ,[#TotalOtherSourceRevenue].ExcelFormula AS 'Total Other Source Revenue'                                                                 --formula field
                 ,0 AS 'Adj Activities conducted'
                 ,[#AdjPaymentsOutsideLoanAndOtherSourceNonTitleIV].ExcelFormula AS 'Adj Payments, Outside Loan, and Other Source (Non Title IV Program)' --formula field
                 ,[#AdjTotalOtherSourceRevenue].ExcelFormula AS 'Adj Total Other Source Revenue'                                                          --formula field
                 ,[#Numerator].ExcelFormula AS 'Numerator'                                                                                                --formula field
                 ,[#Denominator].ExcelFormula AS 'Denominator'                                                                                            --formula field
                 ,[#Percentage].ExcelFormula AS 'Percentage'                                                                                              --formula field
        FROM      #EnrollmentList
        LEFT JOIN #TitleIVCreditBalance ON [#TitleIVCreditBalance].StuEnrollId = [#EnrollmentList].StuEnrollId
        LEFT JOIN #InstitutionalChargeBalance ON [#InstitutionalChargeBalance].StuEnrollId = [#EnrollmentList].StuEnrollId
        LEFT JOIN #InstitutionalMaxChargeLimitThisYear ON [#InstitutionalMaxChargeLimitThisYear].StuEnrollId = [#EnrollmentList].StuEnrollId
        LEFT JOIN #TotalInstitutionalMaxChargeLimit ON [#TotalInstitutionalMaxChargeLimit].StuEnrollId = [#EnrollmentList].StuEnrollId
        LEFT JOIN #SubLoan ON [#SubLoan].StuEnrollId = [#EnrollmentList].StuEnrollId
        LEFT JOIN #UnSubLoan ON [#UnSubLoan].StuEnrollId = [#EnrollmentList].StuEnrollId
        LEFT JOIN #Plus ON [#Plus].StuEnrollId = [#EnrollmentList].StuEnrollId
        LEFT JOIN #PellGrant ON [#PellGrant].StuEnrollId = [#EnrollmentList].StuEnrollId
        LEFT JOIN #FSEOG ON [#FSEOG].StuEnrollId = [#EnrollmentList].StuEnrollId
        LEFT JOIN #FWS ON [#FWS].StuEnrollId = [#EnrollmentList].StuEnrollId
        LEFT JOIN #TitleIVAmtDisbursed ON [#TitleIVAmtDisbursed].StuEnrollId = [#EnrollmentList].StuEnrollId
        LEFT JOIN #AdjSubLoan ON [#AdjSubLoan].StuEnrollId = [#EnrollmentList].StuEnrollId
        LEFT JOIN #AdjUnSubLoan ON [#AdjUnSubLoan].StuEnrollId = [#EnrollmentList].StuEnrollId
        LEFT JOIN #AdjPlus ON [#AdjPlus].StuEnrollId = [#EnrollmentList].StuEnrollId
        LEFT JOIN #AdjPellGrant ON [#AdjPellGrant].StuEnrollId = [#EnrollmentList].StuEnrollId
        LEFT JOIN #AdjFSEOG ON [#AdjFSEOG].StuEnrollId = [#EnrollmentList].StuEnrollId
        LEFT JOIN #AdjFWS ON [#AdjFWS].StuEnrollId = [#EnrollmentList].StuEnrollId
        LEFT JOIN #TitleIVAmtAdjusted ON [#TitleIVAmtAdjusted].StuEnrollId = [#EnrollmentList].StuEnrollId
        LEFT JOIN #RevenueAdjustment ON [#RevenueAdjustment].StuEnrollId = [#EnrollmentList].StuEnrollId
        LEFT JOIN #TitleIVRefunds ON [#TitleIVRefunds].StuEnrollId = [#EnrollmentList].StuEnrollId
        LEFT JOIN #AdjTitleIVRevenue ON [#AdjTitleIVRevenue].StuEnrollId = [#EnrollmentList].StuEnrollId
        LEFT JOIN #OutsideGrant ON [#OutsideGrant].StuEnrollId = [#EnrollmentList].StuEnrollId
        LEFT JOIN #JobTrainingGrant ON [#JobTrainingGrant].StuEnrollId = [#EnrollmentList].StuEnrollId
        LEFT JOIN #EducationSavingPlan ON [#EducationSavingPlan].StuEnrollId = [#EnrollmentList].StuEnrollId
        LEFT JOIN #SchoolGrant ON [#SchoolGrant].StuEnrollId = [#EnrollmentList].StuEnrollId
        LEFT JOIN #FirstAppliedFund ON [#FirstAppliedFund].StuEnrollId = [#EnrollmentList].StuEnrollId
        LEFT JOIN #StuPayandOutsideLn ON [#StuPayandOutsideLn].StuEnrollId = [#EnrollmentList].StuEnrollId
        LEFT JOIN #TotalNonTitleIVRevenue ON [#TotalNonTitleIVRevenue].StuEnrollId = [#EnrollmentList].StuEnrollId
        LEFT JOIN #AdjOutsideGrant ON [#AdjOutsideGrant].StuEnrollId = [#EnrollmentList].StuEnrollId
        LEFT JOIN #AdjJobTrainingGrant ON [#AdjJobTrainingGrant].StuEnrollId = [#EnrollmentList].StuEnrollId
        LEFT JOIN #AdjEducationSavingPlan ON [#AdjEducationSavingPlan].StuEnrollId = [#EnrollmentList].StuEnrollId
        LEFT JOIN #AdjSchoolGrant ON [#AdjSchoolGrant].StuEnrollId = [#EnrollmentList].StuEnrollId
        LEFT JOIN #AdjFirstAppliedFund ON [#AdjFirstAppliedFund].StuEnrollId = [#EnrollmentList].StuEnrollId
        LEFT JOIN #AdjStuPayandOutsideLn ON [#AdjStuPayandOutsideLn].StuEnrollId = [#EnrollmentList].StuEnrollId
        LEFT JOIN #AdjTotalNonTitleIVRevenue ON [#AdjTotalNonTitleIVRevenue].StuEnrollId = [#EnrollmentList].StuEnrollId
        LEFT JOIN #ActivitiesConducted ON [#ActivitiesConducted].StuEnrollId = [#EnrollmentList].StuEnrollId
        LEFT JOIN #PaymentsOutsideLoanAndOtherSourceNonTitleIV ON [#PaymentsOutsideLoanAndOtherSourceNonTitleIV].StuEnrollId = [#EnrollmentList].StuEnrollId
        LEFT JOIN #TotalOtherSourceRevenue ON [#TotalOtherSourceRevenue].StuEnrollId = [#EnrollmentList].StuEnrollId
        LEFT JOIN #AdjActivitiesConducted ON [#AdjActivitiesConducted].StuEnrollId = [#EnrollmentList].StuEnrollId
        LEFT JOIN #AdjPaymentsOutsideLoanAndOtherSourceNonTitleIV ON [#AdjPaymentsOutsideLoanAndOtherSourceNonTitleIV].StuEnrollId = [#EnrollmentList].StuEnrollId
        LEFT JOIN #AdjTotalOtherSourceRevenue ON [#AdjTotalOtherSourceRevenue].StuEnrollId = [#EnrollmentList].StuEnrollId
        LEFT JOIN #Numerator ON [#Numerator].StuEnrollId = [#EnrollmentList].StuEnrollId
        LEFT JOIN #Denominator ON [#Denominator].StuEnrollId = [#EnrollmentList].StuEnrollId
        LEFT JOIN #Percentage ON [#Percentage].StuEnrollId = [#EnrollmentList].StuEnrollId;
        DROP TABLE #EnrollmentList;
        DROP TABLE #TitleIVCreditBalance;
        DROP TABLE #InstitutionalChargeBalance;
        DROP TABLE #InstitutionalMaxChargeLimitThisYear;
        DROP TABLE #TotalInstitutionalMaxChargeLimit;
        DROP TABLE #SubLoan;
        DROP TABLE #UnSubLoan;
        DROP TABLE #Plus;
        DROP TABLE #PellGrant;
        DROP TABLE #FSEOG;
        DROP TABLE #FWS;
        DROP TABLE #TitleIVAmtDisbursed;
        DROP TABLE #AdjSubLoan;
        DROP TABLE #AdjUnSubLoan;
        DROP TABLE #AdjPlus;
        DROP TABLE #AdjPellGrant;
        DROP TABLE #AdjFSEOG;
        DROP TABLE #AdjFWS;
        DROP TABLE #TitleIVAmtAdjusted;
        DROP TABLE #RevenueAdjustment;
        DROP TABLE #TitleIVRefunds;
        DROP TABLE #AdjTitleIVRevenue;
        DROP TABLE #OutsideGrant;
        DROP TABLE #JobTrainingGrant;
        DROP TABLE #EducationSavingPlan;
        DROP TABLE #SchoolGrant;
        DROP TABLE #FirstAppliedFund;
        DROP TABLE #StuPayandOutsideLn;
        DROP TABLE #TotalNonTitleIVRevenue;
        DROP TABLE #AdjOutsideGrant;
        DROP TABLE #AdjJobTrainingGrant;
        DROP TABLE #AdjEducationSavingPlan;
        DROP TABLE #AdjSchoolGrant;
        DROP TABLE #AdjFirstAppliedFund;
        DROP TABLE #AdjStuPayandOutsideLn;
        DROP TABLE #AdjTotalNonTitleIVRevenue;
        DROP TABLE #ActivitiesConducted;
        DROP TABLE #PaymentsOutsideLoanAndOtherSourceNonTitleIV;
        DROP TABLE #TotalOtherSourceRevenue;
        DROP TABLE #AdjActivitiesConducted;
        DROP TABLE #AdjPaymentsOutsideLoanAndOtherSourceNonTitleIV;
        DROP TABLE #AdjTotalOtherSourceRevenue;
        DROP TABLE #Numerator;
        DROP TABLE #Denominator;
        DROP TABLE #Percentage;
        DROP TABLE #Sheet1;
    END;

GO
