SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_GetFASAPDetailsCount]
    (
     @prgVerId UNIQUEIDENTIFIER = NULL
    )
AS
    BEGIN 
        SELECT  COUNT(*)
        FROM    dbo.arSAPDetails A
               ,dbo.arSAP B
               ,dbo.arPrgVersions C
        WHERE   A.SAPId = B.SAPId
                AND B.SAPId = C.FASAPId
                AND (
                      @prgVerId IS NULL
                      OR C.PrgVerId = @prgVerId
                    );
    END;



GO
