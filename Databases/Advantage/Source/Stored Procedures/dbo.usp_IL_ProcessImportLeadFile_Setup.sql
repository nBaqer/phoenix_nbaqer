SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =========================================================================================================
-- usp_IL_ProcessImportLeadFile_Setup
-- =========================================================================================================  
CREATE PROCEDURE [dbo].[usp_IL_ProcessImportLeadFile_Setup] @EnableTriggers BIT
AS
    DECLARE @TMLeads AS NVARCHAR(500);
    DECLARE @TMLeadAddresses AS NVARCHAR(500);
    DECLARE @TMLeadEmail AS NVARCHAR(500);
    DECLARE @TMLeadPhone AS NVARCHAR(500);
    DECLARE @CurFKName AS NVARCHAR(MAX);
    DECLARE @CompFKName AS NVARCHAR(MAX);
    DECLARE @CurTableName AS NVARCHAR(500);
    DECLARE @CurRefTableName AS NVARCHAR(500);
    DECLARE @CurColName AS NVARCHAR(500);
    DECLARE @CurRefColumn AS NVARCHAR(500);
    DECLARE @SQL001 AS NVARCHAR(MAX);
    DECLARE @SQL002 AS NVARCHAR(MAX);  
    BEGIN
        SET @TMLeads = 'adLeads';
        SET @TMLeadAddresses = 'adLeadAddresses';
        SET @TMLeadEmail = 'adLeadEmail';
        SET @TMLeadPhone = 'adLeadPhone';
        SET @SQL001 = '';
        SET @SQL002 = '';

        IF ( @EnableTriggers = 0 )
            BEGIN
  ------------------------------------------------------------------------------------------------------------------------------------------------------------
  --Disable Triggers
  -------------------------------------------------------------------------------------------------------------------------------------------------------------
                PRINT 'START disable trigger ' + CAST(CONVERT (TIME,GETDATE()) AS VARCHAR(50));
                IF EXISTS ( SELECT  *
                            FROM    sys.triggers
                            WHERE   name = 'adLeads_Audit_Insert' )
                    DISABLE TRIGGER adLeads_Audit_Insert ON dbo.adLeads;
                PRINT 'END disable trigger ' + CAST(CONVERT (TIME,GETDATE()) AS VARCHAR(50));	
  ----------------------------------------------------------------------------------------------------------------------------------------------------------
  --Disable all constraints
  ----------------------------------------------------------------------------------------------------------------------------------------------------------
  --- drop constraints 
                PRINT 'START drop constraints ' + CAST(CONVERT (TIME,GETDATE()) AS VARCHAR(50));
                IF OBJECT_ID('ForeingKeyAdLeadsList') IS NULL
                    BEGIN
                        CREATE TABLE ForeingKeyAdLeadsList
                            (
                             FKName NVARCHAR(500)
                            ,TableName NVARCHAR(500)
                            ,RefTableName NVARCHAR(500)
                            ,ColName NVARCHAR(500)
                            ,RefColumn NVARCHAR(500)
                            );                      
                    END;

                INSERT  INTO ForeingKeyAdLeadsList
                        (
                         FKName
                        ,TableName
                        ,RefTableName
                        ,ColName
                        ,RefColumn
                        )
                        SELECT  FK.name AS FKName
                               ,OBJECT_NAME(FK.parent_object_id) AS TableName
                               ,OBJECT_NAME(FK.referenced_object_id) AS RefTableName
                               ,COL_NAME(FKC.parent_object_id,FKC.parent_column_id) AS ColName
                               ,C.name AS RefColumn
                        FROM    sys.foreign_keys AS FK
                        INNER JOIN sys.foreign_key_columns AS FKC ON FKC.constraint_object_id = FK.object_id
                        INNER JOIN sys.objects AS O ON O.object_id = FK.referenced_object_id
                        INNER JOIN sys.columns AS C ON C.object_id = O.object_id
                        INNER JOIN sys.index_columns AS IC ON IC.object_id = C.object_id
                                                              AND IC.column_id = C.column_id
                        INNER JOIN sys.indexes AS I ON I.object_id = IC.object_id
                                                       AND I.index_id = IC.index_id
                        WHERE   (
                                  FK.parent_object_id = OBJECT_ID(@TMLeads)
                                  OR FK.parent_object_id = OBJECT_ID(@TMLeadAddresses)
                                  OR FK.parent_object_id = OBJECT_ID(@TMLeadEmail)
                                  OR FK.parent_object_id = OBJECT_ID(@TMLeadPhone)
                                )
                                AND FK.type = 'F'
                                AND I.is_primary_key = 1
                        ORDER BY TableName
                               ,FKName;

                DECLARE ForeingKeysDrop_Cursor CURSOR FORWARD_ONLY STATIC
                FOR
                    SELECT  FKT.FKName
                           ,FKT.TableName
                           ,FKT.RefTableName
                           ,FKT.ColName
                           ,FKT.RefColumn
                    FROM    ForeingKeyAdLeadsList AS FKT
                    ORDER BY FKT.TableName
                           ,FKT.FKName;
                  
                OPEN ForeingKeysDrop_Cursor;
                FETCH NEXT FROM ForeingKeysDrop_Cursor INTO @CurFKName,@CurTableName,@CurRefTableName,@CurColName,@CurRefColumn; 

                WHILE @@FETCH_STATUS = 0
                    BEGIN 
                        SET @SQL001 = '';                                                                                                
                        SET @SQL001 = @SQL001 + 'IF EXISTS ( SELECT  1 ' + CHAR(10);
                        SET @SQL001 = @SQL001 + '            FROM    sys.foreign_keys   ' + CHAR(10);
                        SET @SQL001 = @SQL001 + '            WHERE   object_id = OBJECT_ID(N''[' + @CurFKName + ']'') ' + CHAR(10);
                        SET @SQL001 = @SQL001 + '                    AND parent_object_id = OBJECT_ID(N''[' + @CurTableName + ']'') )' + CHAR(10);
                        SET @SQL001 = @SQL001 + '    BEGIN ' + CHAR(10);
                        SET @SQL001 = @SQL001 + '        ALTER TABLE [dbo].[' + @CurTableName + '] DROP CONSTRAINT [' + @CurFKName + ']; ' + CHAR(10);
                        SET @SQL001 = @SQL001 + '    END; ' + CHAR(10);
                    
                        EXECUTE sp_executesql @SQL001;               --, N'@CurFKName NVARCHAR(500)',  @CurFKName

                        FETCH NEXT FROM ForeingKeysDrop_Cursor INTO @CurFKName,@CurTableName,@CurRefTableName,@CurColName,@CurRefColumn; 
                    END;
                CLOSE ForeingKeysDrop_Cursor;
                DEALLOCATE ForeingKeysDrop_Cursor;
  
                PRINT 'END drop constraints ' + CAST(CONVERT (TIME,GETDATE()) AS VARCHAR(50));
  --SELECT * FROM sys.foreign_keys WHERE parent_object_id = OBJECT_ID(N'[dbo].[adLeads]')
  --PRINT 'START drop constraints ' + CAST(CONVERT (time, GETDATE()) AS VARCHAR(50))
  --EXEC sp_msforeachtable 'ALTER TABLE adLeads NOCHECK CONSTRAINT all'
  --PRINT 'END drop constraints ' + CAST(CONVERT (time, GETDATE()) AS VARCHAR(50))
  --------------------------------------------------------------------------------------------------------------------------------------------------------------------
  --Disable Non-Unique Non-Clustered Indexes
  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------
  --PRINT 'START disable Non-Unique Non-Clustered Indexes ' + CAST(CONVERT (time, GETDATE()) AS VARCHAR(50))
  --ALTER INDEX [IX_syAuditHist_Event] ON [dbo].[syAuditHist] DISABLE
  --ALTER INDEX [IX_syAuditHist_EventDate] ON [dbo].[syAuditHist] DISABLE
  --ALTER INDEX [IX_syAuditHist_TableName] ON [dbo].[syAuditHist] DISABLE
  --ALTER INDEX [Ind_AuditHistDetail] ON [dbo].[syAuditHistDetail] DISABLE
  --ALTER INDEX [IX_syAuditHistDetail_AuditHistId] ON [dbo].[syAuditHistDetail] DISABLE
  --ALTER INDEX [IX_syAuditHistDetail_ColumnName] ON [dbo].[syAuditHistDetail] DISABLE
  --ALTER INDEX [IX_syAuditHistDetail_NewValue] ON [dbo].[syAuditHistDetail] DISABLE
  --ALTER INDEX [IX_syAuditHistDetail_OldValue] ON [dbo].[syAuditHistDetail] DISABLE
  --ALTER INDEX [IX_syAuditHistDetail_RowId] ON [dbo].[syAuditHistDetail] DISABLE
  --PRINT 'END disable Non-Unique Non-Clustered Indexes ' + CAST(CONVERT (time, GETDATE()) AS VARCHAR(50))  
            END;
        ELSE
            BEGIN
  ------------------------------------------------------------------------------------------------------------------------------------------------------------
  --Enable Triggers
  -------------------------------------------------------------------------------------------------------------------------------------------------------------
                PRINT 'START enable trigger ' + CAST(CONVERT (TIME,GETDATE()) AS VARCHAR(50));  
                IF EXISTS ( SELECT  *
                            FROM    sys.triggers
                            WHERE   name = 'adLeads_Audit_Insert' )
                    ENABLE TRIGGER adLeads_Audit_Insert ON dbo.adLeads;
                PRINT 'END enable trigger ' + CAST(CONVERT (TIME,GETDATE()) AS VARCHAR(50));  	
  --------------------------------------------------------------------------------------------------------------------------------------------------------------------
  --Enable Non-Unique Non-Clustered Indexes
  ---------------------------------------------------------------------------------------------------------------------------------------------------------------------
  --PRINT 'START rebuild Non-Unique Non-Clustered Indexes ' + CAST(CONVERT (time, GETDATE()) AS VARCHAR(50))
  --ALTER INDEX [IX_syAuditHist_Event] ON [dbo].[syAuditHist] REBUILD
  --ALTER INDEX [IX_syAuditHist_EventDate] ON [dbo].[syAuditHist] REBUILD
  --ALTER INDEX [IX_syAuditHist_TableName] ON [dbo].[syAuditHist] REBUILD
  --ALTER INDEX [Ind_AuditHistDetail] ON [dbo].[syAuditHistDetail] REBUILD
  --ALTER INDEX [IX_syAuditHistDetail_AuditHistId] ON [dbo].[syAuditHistDetail] REBUILD
  --ALTER INDEX [IX_syAuditHistDetail_ColumnName] ON [dbo].[syAuditHistDetail] REBUILD
  --ALTER INDEX [IX_syAuditHistDetail_NewValue] ON [dbo].[syAuditHistDetail] REBUILD
  --ALTER INDEX [IX_syAuditHistDetail_OldValue] ON [dbo].[syAuditHistDetail] REBUILD
  --ALTER INDEX [IX_syAuditHistDetail_RowId] ON [dbo].[syAuditHistDetail] REBUILD	
  --PRINT 'END rebuild Non-Unique Non-Clustered Indexes ' + CAST(CONVERT (time, GETDATE()) AS VARCHAR(50))
  ----------------------------------------------------------------------------------------------------------------------------------------------------------
  --Enable all constraints
  ----------------------------------------------------------------------------------------------------------------------------------------------------------
  --PRINT 'START restore constraints ' + CAST(CONVERT (time, GETDATE()) AS VARCHAR(50))
  --exec sp_msforeachtable 'ALTER TABLE adLeads WITH CHECK CHECK CONSTRAINT ALL'	
  --PRINT 'END restore constraints ' + CAST(CONVERT (time, GETDATE()) AS VARCHAR(50))
  ---- create constraints
                PRINT 'START restore constraints ' + CAST(CONVERT (TIME,GETDATE()) AS VARCHAR(50));
                IF OBJECT_ID('ForeingKeyAdLeadsList') IS NOT NULL
                    BEGIN
                        DECLARE ForeingKeysAdd_Cursor CURSOR FORWARD_ONLY STATIC
                        FOR
                            SELECT  FKT.FKName
                                   ,FKT.TableName
                                   ,FKT.RefTableName
                                   ,FKT.ColName
                                   ,FKT.RefColumn
                            FROM    ForeingKeyAdLeadsList AS FKT
                            ORDER BY FKT.TableName
                                   ,FKT.FKName;  

                        OPEN ForeingKeysAdd_Cursor;
                        FETCH NEXT FROM ForeingKeysAdd_Cursor INTO @CurFKName,@CurTableName,@CurRefTableName,@CurColName,@CurRefColumn; 
    
                        WHILE @@FETCH_STATUS = 0
                            BEGIN    
                                SET @SQL002 = '';  
                                SET @CompFKName = 'FK_' + @CurTableName + '_' + @CurRefTableName + '_' + @CurColName + '_' + @CurRefColumn;                                                                                         
                                SET @SQL002 = @SQL002 + 'IF NOT EXISTS ( SELECT  1 ' + CHAR(10);
                                SET @SQL002 = @SQL002 + '                FROM    sys.foreign_keys   ' + CHAR(10);
                                SET @SQL002 = @SQL002 + '                WHERE   object_id = OBJECT_ID(N''[' + @CompFKName + ']'') ' + CHAR(10);
                                SET @SQL002 = @SQL002 + '                        AND parent_object_id = OBJECT_ID(N''[' + @CurTableName + ']'') )' + CHAR(10);
                                SET @SQL002 = @SQL002 + '    BEGIN ' + CHAR(10);
                                SET @SQL002 = @SQL002 + '        ALTER TABLE [dbo].[' + @CurTableName + '] WITH CHECK ADD  ' + CHAR(10);    
                                SET @SQL002 = @SQL002 + '            CONSTRAINT [' + @CompFKName + '] FOREIGN KEY([' + @CurColName + ']) ' + CHAR(10);  
                                SET @SQL002 = @SQL002 + '            REFERENCES [dbo].[' + @CurRefTableName + '] ([' + @CurRefColumn + ']) ' + CHAR(10);
                                SET @SQL002 = @SQL002 + '        ALTER TABLE [dbo].[' + @CurTableName + '] CHECK CONSTRAINT [' + @CompFKName + ']; ' + CHAR(10);    
                                SET @SQL002 = @SQL002 + '    END; ' + CHAR(10);
                                SET @SQL002 = @SQL002 + ' ' + CHAR(10);
                            
                                EXECUTE sp_executesql @SQL002;

                                FETCH NEXT FROM ForeingKeysAdd_Cursor INTO @CurFKName,@CurTableName,@CurRefTableName,@CurColName,@CurRefColumn; 
                            END;
                        CLOSE ForeingKeysAdd_Cursor;
                        DEALLOCATE ForeingKeysAdd_Cursor;

                        DROP TABLE ForeingKeyAdLeadsList;
                    END;
  
                PRINT 'END restore constraints ' + CAST(CONVERT (TIME,GETDATE()) AS VARCHAR(50));
            END;
    END;
-- =========================================================================================================
-- usp_IL_ProcessImportLeadFile_Setup
-- ========================================================================================================= 


GO
