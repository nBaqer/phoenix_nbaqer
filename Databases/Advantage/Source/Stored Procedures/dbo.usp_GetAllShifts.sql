SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_GetAllShifts] @showActiveOnly BIT
AS
    SET NOCOUNT ON;
    DECLARE @sql VARCHAR(8000);

    IF @showActiveOnly = 1
        BEGIN
            EXEC usp_GetAllActiveShifts;
                
        END; 
    ELSE
        BEGIN
            EXEC usp_GetAllActiveAndInActiveShifts;
        END; 
 





GO
