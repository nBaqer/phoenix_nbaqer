SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[USP_AR_RegStd_DoesStdHavePassingGradeforNumeric]
    (
     @StuEnrollId UNIQUEIDENTIFIER
    ,@ReqId UNIQUEIDENTIFIER
    ,@PrgverId UNIQUEIDENTIFIER
    ,@ClsSectId UNIQUEIDENTIFIER
    ,@ReturnValue BIT OUTPUT
    )
AS /*----------------------------------------------------------------------------------------------------
	Author          :	Saraswathi Lakshmanan
    
    Create date		:	02/03/2010
    
	Procedure Name	:	[[USP_AR_RegStd_DoesStdHavePassingGradeforNumeric]]

	Objective		:	returns if a student has a passing grade or not
		
	Parameters		:	Name			Type	Data Type			Required? 	
						=====			====	=========			=========	
						@StuEnrollId	In		Uniqueidentifier
						@ReqId			In		UniqueIdentifier	
						@PrgverId		In		UniqueIdentifier
						@ClsSectId		In		UniqueIdentifier
						@ReturnValue	Out		bit			
					
	Output			:	returns one or zero
	
	dependent procedures	: 	[USP_AR_GetWorkUnitResults]	
								[USP_AR_GetMinPassingScore]		
								[USP_AR_GetCourseAverage_forStdandClsSect]
*/-----------------------------------------------------------------------------------------------------
/*
--To fix issue 17548: ENH: All: Register Students Page is Taking Too Long to Populate Available Students 
This query is used to find if the student has passing Grades for Numeric schools
*/

    BEGIN
--Get the work Units

--A Variabletable is defined to hold the results from the Storedprocedure USP_GetCoursesFortheCourseGroups
        DECLARE @VariableWorkUnitsRestable1 TABLE
            (
             ReqId UNIQUEIDENTIFIER
            ,TermId UNIQUEIDENTIFIER
            ,Descrip VARCHAR
            ,Score DECIMAL
            ,MinResult DECIMAL
            ,Required BIT
            ,MustPass BIT
            ,Remaining DECIMAL
            ); 

        BEGIN TRY
            INSERT  INTO @VariableWorkUnitsRestable1
                    EXEC USP_AR_GetWorkUnitResults @StuEnrollId,@ReqId;

            DECLARE @ReqIdforCur UNIQUEIDENTIFIER
               ,@TermIDforCur UNIQUEIDENTIFIER
               ,@ScoreforCur DECIMAL
               ,@MinResultforCur DECIMAL
               ,@RequiredforCur BIT
               ,@MustPassforCur BIT;
            DECLARE @Result BIT;
            SET @Result = 1;
            DECLARE @AvgScore DECIMAL;

            DECLARE @MinPassingScore DECIMAL;
	--Get the minimum passing Score
            EXEC USP_AR_GetMinPassingScore @MinPassingScore;
            IF (
                 SELECT COUNT(ReqId)
                 FROM   @VariableWorkUnitsRestable1
               ) > 0
                BEGIN
                    DECLARE CurWorkUnitResults CURSOR READ_ONLY
                    FOR
                        (
                          SELECT    ReqId
                                   ,TermId
                                   ,Score
                                   ,MinResult
                                   ,Required
                                   ,MustPass
                          FROM      @VariableWorkUnitsRestable1
                        ); 

                    OPEN CurWorkUnitResults;

                    FETCH NEXT FROM CurWorkUnitResults
		INTO @ReqIdforCur,@TermIDforCur,@ScoreforCur,@MinResultforCur,@RequiredforCur,@MustPassforCur;

                    WHILE @@FETCH_STATUS = 0
                        BEGIN
		
                            IF @ScoreforCur IS NULL
                                BEGIN
                                    IF @RequiredforCur = 1
                                        OR @MustPassforCur = 1
                                        BEGIN
                                            SET @Result = 0;  -- Return False
					--Exit while--Exit loop
                                        END;
                                END;
                            ELSE
                                IF @ScoreforCur >= 0
                                    BEGIN
                                        IF @ScoreforCur < @MinResultforCur
                                            BEGIN
                                                IF @RequiredforCur = 1
                                                    OR @MustPassforCur = 1
                                                    BEGIN
                                                        SET @Result = 0;  -- Return False
                                                        RETURN @Result;--Exit while--Exit loop
                                                    END;
                                            END;
                                        ELSE
                                            BEGIN
                                                EXEC USP_AR_GetCourseAverage_forStdTermandReq @StuEnrollID,@TermIDforCur,@ReqIdforCur,@AvgScore;
					--Round the avg Score if Grade rounding is set to yes in the webconfig 
                                                IF @AvgScore <> 0
                                                    AND @AvgScore < @MinPassingScore
                                                    BEGIN
                                                        SET @Result = 0;
                                                        RETURN @Result;--Exit while
                                                    END;
                                            END;
                                    END;
			
                            FETCH NEXT FROM CurWorkUnitResults
			INTO @ReqIdforCur,@TermIDforCur,@ScoreforCur,@MinResultforCur,@RequiredforCur,@MustPassforCur;
                        END;
                    CLOSE CurWorkUnitResults;
                    DEALLOCATE CurWorkUnitResults;	
                    SET @ReturnValue = @Result;
                    RETURN;
                END;
            ELSE
                BEGIN
		--get the average Score
                    EXEC USP_AR_GetCourseAverage_forStdandClsSect @StuEnrollID,@clsSectId,@AvgScore;
		--Round the avg Score if Grade rounding is set to yes in the webconfig 
                    IF @AvgScore < @MinPassingScore
                        BEGIN
                            SET @ReturnValue = 0;
                            RETURN @ReturnValue;
                        END;		
                END;
            SET @ReturnValue = 1;
            RETURN;
        END TRY
        BEGIN CATCH
            SET @ReturnValue = 0;
            RETURN;
        END CATCH;
	 
    END;



GO
