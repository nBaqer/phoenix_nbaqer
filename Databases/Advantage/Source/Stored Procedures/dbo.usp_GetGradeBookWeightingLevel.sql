SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [dbo].[usp_GetGradeBookWeightingLevel]
    (
     @clsSectionId UNIQUEIDENTIFIER
 

    )
AS
    SET NOCOUNT ON;
    SELECT  COUNT(*)
    FROM    (
              SELECT    Reqid
              FROM      arGrdBkWeights
              WHERE     ReqId IN ( SELECT   ReqId
                                   FROM     arClassSections
                                   WHERE    ClsSectionid = @clsSectionId )
              GROUP BY  ReqId
            ) GradeWeights;  



GO
